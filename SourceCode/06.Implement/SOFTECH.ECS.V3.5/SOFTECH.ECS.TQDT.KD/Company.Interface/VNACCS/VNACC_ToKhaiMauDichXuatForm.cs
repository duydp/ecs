﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.Interface.VNACCS;
using Company.Interface.DanhMucChuan;
#if SXXK_V4
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
using Company.Interface.Report.VNACCS;
#endif
#if KD_V4
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
using Company.Interface.Report.VNACCS;
#endif
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
using Company.Interface.Report.VNACCS;
#endif

namespace Company.Interface
{
    public partial class VNACC_ToKhaiMauDichXuatForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
        List<KDT_VNACC_ToKhaiMauDich> listTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
        List<KDT_VNACC_TK_SoVanDon> listVanDon = new List<KDT_VNACC_TK_SoVanDon>();

        public VNACC_HangMauDichXuatForm hmdForm = new VNACC_HangMauDichXuatForm();
        public VNACC_TK_GiayPhepForm gpForm = new VNACC_TK_GiayPhepForm();
        public VNACC_TK_DinhKiemDTForm dkForm = new VNACC_TK_DinhKiemDTForm();
        public VNACC_TK_TrungChuyenForm trungchuyenForm = new VNACC_TK_TrungChuyenForm();
        VNACC_TK_Container containerForm = new VNACC_TK_Container();

        public string soHD = "";
        public DateTime ngayHD;
        private string MaTienTe = "";
        private string loaiHangHoa="";
        public string msg;
        public bool isCopy = false;
        public String Caption;
        public bool IsChange;

        public decimal TongSoLuongHang = 0;
        public decimal TongSoLuongHangTK = 0;
        public decimal TongTriGiaHD = 0;
        public decimal TongTriGiaTinhThue = 0;
        public decimal TongHeSoPhanBo = 0;
        public List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
#if GC_V4
        public HopDong HD;
#endif
        public long HopDong_ID;
        public VNACC_ToKhaiMauDichXuatForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.EDA.ToString());
            BoSungNghiepVu();
        }
        private void LoadDuLieuChuan()
        {
#if KD_V4
            lblNgayHD.Visible = false;
            lblNgayHD1.Visible = false;
            lblSoHD.Visible = false;
            txtSoHD.Visible = false;

#elif SXXK_V4 
             rdbSanPham.Visible = true;
             rdbNPL.Visible = true;
             if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                rdbThietBi.Visible = true;
            lblNgayHD.Visible = false;
            lblNgayHD1.Visible = false;
            lblSoHD.Visible = false;
            txtSoHD.Visible = false;
            
#elif GC_V4

            rdbSanPham.Visible = true;
            rdbNPL.Visible = true;
            rdbThietBi.Visible = true;
            lblNgayHD.Visible = true;
            lblNgayHD1.Visible = true;
            lblSoHD.Visible = true;
            txtSoHD.Visible = true;
            txtSoHD.ReadOnly = true;
#endif

            if (TKMD.ID == 0 && !isCopy)
            {
                //ctrMaNuocDoiTac.Code = GlobalSettings.NUOC;
                //ctrDiaDiemXepHang.Where = string.Format("CountryCode = '{0}'", "VN");
                //ctrDiaDiemXepHang.ReLoadData();
                // Người nhập khẩu

                txtMaDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                txtMaDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDonVi.Text = GlobalSettings.TEN_DON_VI.ToUpper();
                txtTenDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiDonVi.Text = GlobalSettings.DIA_CHI.ToUpper();
                txtDiaChiDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDienThoaiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDienThoaiDonVi.Text = GlobalSettings.SoDienThoaiDN;
                txtSoDienThoaiDonVi.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                //txtTenDonVi.Text = GlobalSettings.TEN_DON_VI.ToUpper();
                //txtDiaChiDonVi.Text = GlobalSettings.DIA_CHI.ToUpper();
                //txtSoDienThoaiDonVi.Text = GlobalSettings.SoDienThoaiDN;
                // Hai quan
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ctrCoQuanHaiQuan.TextChanged -= new EventHandler(txt_TextChanged);
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                ctrCoQuanHaiQuan.TextChanged += new EventHandler(txt_TextChanged);

                //ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ");// MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                //Ma tien te 
                MaTienTe = GlobalSettings.NGUYEN_TE_MAC_DINH;
                ctrMaTTHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaTTHoaDon.Code = MaTienTe;
                ctrMaTTHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaTTHoaDon.Code = MaTienTe;
               

#if KD_V4
                ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaLoaiHinh.Code = "B11";
                ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaLoaiHinh.Code = "B11";
#elif SXXK_V4
                if(Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                    ctrMaLoaiHinh.Code = "E42";
                    ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                    //ctrMaLoaiHinh.Code = "E42";
                }
                else
                {
                    ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                    ctrMaLoaiHinh.Code = "E62";
                    ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                    //ctrMaLoaiHinh.Code = "E62";
                }

                //if(Company.KDT.SHARE.Components.Globals.LaDNCX)
                //    ctrMaLoaiHinh.Code = "E42";
                //else
                //    ctrMaLoaiHinh.Code = "E62";
#elif GC_V4
                ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaLoaiHinh.Code = "E52";
                ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaLoaiHinh.Code = "E52";
                if (HD != null)
                {
                    SetAutomaticGiayPhep();
                }
#endif
#if SXXK_V4 || GC_V4

                rdbSanPham.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbSanPham.Checked = true;
                rdbSanPham.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbSanPham.Checked = true;
#endif
            }
            else
            {
                MaTienTe = ctrMaTTHoaDon.Code;

            }
        }
        private void SetAutomaticGiayPhep()
        {
#if GC_V4
            KDT_VNACC_TK_GiayPhep gp = new KDT_VNACC_TK_GiayPhep();
            gp.SoTT = 1;
            gp.PhanLoai = "HDGC";
            gp.SoGiayPhep = HD.SoTiepNhan.ToString();
            TKMD.GiayPhepCollection.Add(gp);
#endif
        }
        private void check() 
        {
            if (txtSoToKhai.Text == "0")
            {
                txtSoToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhai.Text = "";
                txtSoToKhai.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoToKhai.Text = "";
            }
            if (txtTongSoTKChiaNho.Text == "0")
            {
                txtTongSoTKChiaNho.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongSoTKChiaNho.Text = "";
                txtTongSoTKChiaNho.TextChanged += new EventHandler(txt_TextChanged);

                //txtTongSoTKChiaNho.Text = "";
            }
            txtSoToKhaiTNTX.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            if (txtSoToKhaiTNTX.Text == "0")
            {
                txtSoToKhaiTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhaiTNTX.Text = "";
                txtSoToKhaiTNTX.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoToKhaiTNTX.Text = "";
            }
            if (txtSoNhanhToKhai.Text == "0")
            {
                txtSoNhanhToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoNhanhToKhai.Text = "";
                txtSoNhanhToKhai.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoNhanhToKhai.Text = "";
            }
            if (txtNamPhatHanhBL.Text == "0")
            {
                txtNamPhatHanhBL.TextChanged -= new EventHandler(txt_TextChanged);
                txtNamPhatHanhBL.Text = "";
                txtNamPhatHanhBL.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhBL.Text = "";
            }
            if (txtNanPhatHanhHM.Text == "0")
            {
                txtNanPhatHanhHM.TextChanged -= new EventHandler(txt_TextChanged);
                txtNanPhatHanhHM.Text = "";
                txtNanPhatHanhHM.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhHM.Text = "";
            }
            if (txtTongHeSoPhanBoTG.Text == "0")
            {
                txtTongHeSoPhanBoTG.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongHeSoPhanBoTG.Text = "";
                txtTongHeSoPhanBoTG.TextChanged += new EventHandler(txt_TextChanged);

                //txtTongHeSoPhanBo.Text = "";
            }
            txtSoTiepNhanHD.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            if (txtSoTiepNhanHD.Text == "0")
            {
                txtSoTiepNhanHD.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTiepNhanHD.Text = "";
                txtSoTiepNhanHD.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTiepNhanHD.Text = "";
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
          this.SetChange(true);
        }
        private void VNACC_ToKhaiMauDichXuatForm_Load(object sender, EventArgs e)
        {
            
            ctrDiaDiemXepHang.Where = "I"; // hiển thị 3 ký tự danh mục cảng dở hàng tại việt nam đối với tờ khai xuất
            this.SetEDControl();
            Caption = this.Text;
            if (TKMD != null)
            {

                SetToKhai();
                TuDongCapNhatThongTin();
                cmbMain.Commands["cmdChiThiHQ"].Visible = Janus.Windows.UI.InheritableBoolean.True;
                setCommandStatus();
            }
            if (TKMD.MaPhuongThucVT == "2")
            {
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                cmdBSContainer.Text = cmdBSContainer1.Text = "Lấy phản hồi danh sách hàng hóa đủ điều kiện qua KVGS";
            }
            //if (TKMD.CoQuanHaiQuan == null)
            LoadDuLieuChuan();
            LoadGrid();
            ctrDiaDiemXepHang.CategoryType = ECategory.A620;

            check();
            SetMaxLengthControl();
            //ValidateForm(false);

            SetAutoRemoveUnicodeAndUpperCaseControl();
            ctrCoQuanHaiQuan.Leave += new EventHandler(ctrCoQuanHaiQuan_Leave);

            ctrCoQuanHaiQuan_Leave(null, null);
            ctrMaPhuongThucVT_Leave(null, null);

            //if (Company.KDT.SHARE.Components.Globals.IsMaPhanLoaiHangHoa)
                ctrMaPhanLoaiHH.Enabled = true;
            //else
            //    ctrMaPhanLoaiHH.Enabled = false;

            cmdCanDoiNhapXuat.Visible = cmdCanDoiNhapXuat1.Visible = Janus.Windows.UI.InheritableBoolean.False;
#if GC_V4 
            cmdCanDoiNhapXuat.Visible = cmdCanDoiNhapXuat1.Visible = Janus.Windows.UI.InheritableBoolean.True;
#endif
            GetLoaiHangHoa();
        }

        private void LoadGrid()
        {
            try
            {
                grListTyGia.DataSource = TKMD.TyGiaCollection;
                grListTyGia.Refresh();
                grListTyGia.Refetch();
            }
            catch (System.Exception ex)
            {
                grListTyGia.Refresh();
                grListTyGia.Refetch();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void GetToKhai()
        {

            //TKMD.SoToKhai = Convert.ToDecimal(txtSoToKhai.Value);
            

            TKMD.SoToKhaiDauTien = txtSoToKhaiDauTien.Text;
            if (txtSoNhanhToKhai.Text == "")
                TKMD.SoNhanhToKhai = 0;
            else
                TKMD.SoNhanhToKhai = Convert.ToDecimal(txtSoNhanhToKhai.Value);
            if (txtTongSoTKChiaNho.Text == "")
                TKMD.TongSoTKChiaNho = 0;
            else
                TKMD.TongSoTKChiaNho = Convert.ToDecimal(txtTongSoTKChiaNho.Value);
            if (txtSoToKhaiTNTX.Text == "")
                TKMD.SoToKhaiTNTX = 0;
            else
                TKMD.SoToKhaiTNTX = Convert.ToDecimal(txtSoToKhaiTNTX.Value);
            TKMD.MaLoaiHinh = ctrMaLoaiHinh.Code;
            TKMD.MaPhanLoaiHH = ctrMaPhanLoaiHH.Code;
            TKMD.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;

          

            TKMD.NhomXuLyHS = ctrNhomXuLyHS.Code;
            TKMD.ThoiHanTaiNhapTaiXuat = clcThoiHanTaiNhap.Value;
            TKMD.NgayDangKy = clcNgayDangKy.Value;
            TKMD.MaPhuongThucVT = ctrMaPhuongThucVT.Code;
            //don vi
            TKMD.MaDonVi = txtMaDonVi.Text;
            TKMD.TenDonVi = txtTenDonVi.Text;
            TKMD.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
            TKMD.DiaChiDonVi = txtDiaChiDonVi.Text;
            TKMD.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;
            TKMD.MaUyThac = txtMaUyThac.Text;
            TKMD.TenUyThac = txtTenUyThac.Text;
            // doi tac
            TKMD.MaDoiTac = txtMaDoiTac.Text;
            TKMD.TenDoiTac = txtTenDoiTac.Text;
            TKMD.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
            TKMD.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
            TKMD.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
            TKMD.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
            TKMD.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
            //uy thac
            TKMD.MaNuocDoiTac = ctrMaNuocDoiTac.Code;
            TKMD.MaDaiLyHQ = txtMaDaiLyHQ.Text;
            //so kien va trong luong
            TKMD.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            TKMD.MaDVTSoLuong = ctrMaDVTSoLuong.Code;
            TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Value);
            TKMD.MaDVTTrongLuong = ctrMaDVTTrongLuong.Code;
            // van don
            TKMD.MaDDLuuKho = ctrMaDDLuuKho.Code;
            TKMD.SoHieuKyHieu = txtSoHieuKyHieu.Text;
            TKMD.MaPTVC = txtMaPTVC.Text;
            TKMD.TenPTVC = txtTenPTVC.Text;
            TKMD.NgayHangDen = clcNgayHangDen.Value;
            TKMD.MaDiaDiemDoHang = ctrDiaDiemNhanHang.Code;
            TKMD.TenDiaDiemDohang = ctrDiaDiemNhanHang.Name_VN;
            TKMD.MaDiaDiemXepHang = ctrDiaDiemXepHang.Code;
            TKMD.TenDiaDiemXepHang = ctrDiaDiemXepHang.Name_VN;
            //hoa don
            TKMD.PhanLoaiHD = ctrPhanLoaiHD.Code;
            TKMD.SoTiepNhanHD = Convert.ToDecimal(txtSoTiepNhanHD.Value);
            TKMD.SoHoaDon = txtSoHoaDon.Text;
            TKMD.NgayPhatHanhHD = clcNgayPhatHanhHD.Value;
            TKMD.PhuongThucTT = ctrPhuongThucTT.Code;
            TKMD.PhanLoaiGiaHD = ctrPhanLoaiGiaHD.Code;
            TKMD.MaDieuKienGiaHD = ctrMaDieuKienGiaHD.Code;
            TKMD.TongTriGiaHD = Convert.ToDecimal(txtTongTriGiaHD.Value);
            TKMD.MaTTHoaDon = ctrMaTTHoaDon.Code;



            //tri gia tinh thue
            TKMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            TKMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            TKMD.PhanLoaiKhongQDVND = txtPhanLoaiKhongQDVND.Text;
            //han muc
            TKMD.NguoiNopThue = ctrNguoiNopThue.Code;
            TKMD.MaNHTraThueThay = ctrMaNHTraThueThay.Code;
            if (txtNanPhatHanhHM.Text == "")
                TKMD.NamPhatHanhHM = 0;
            else
                TKMD.NamPhatHanhHM = Convert.ToDecimal(txtNanPhatHanhHM.Value);
            TKMD.KyHieuCTHanMuc = txtKyHieuCTHanMuc.Text;
            TKMD.SoCTHanMuc = txtSoCTHanMuc.Text;
            //bao lanh thue
            TKMD.MaXDThoiHanNopThue = ctrMaXDThoiHanNopThue.Code;
            TKMD.MaNHBaoLanh = ctrMaNHBaoLanh.Text;
            if (txtNamPhatHanhBL.Text == "")
                TKMD.NamPhatHanhBL = 0;
            else
                TKMD.NamPhatHanhBL = Convert.ToDecimal(txtNamPhatHanhBL.Value);
            TKMD.KyHieuCTBaoLanh = txtKyHieuCTBaoLanh.Text;
            TKMD.SoCTBaoLanh = txtSoCTBaoLanh.Text;
            //
            TKMD.NgayKhoiHanhVC = clcNgayKhoiHanhVC.Value;
            TKMD.DiaDiemDichVC = ctrDiaDiemDichVC.Code;
            TKMD.NgayDen = clcNgayDen.Value;

#if KD_V4 || SXXK_V4
            TKMD.GhiChu = txtGhiChu.Text;
#elif GC_V4
            //if (TKMD.MaDonVi == "4000395355" || TKMD.MaDonVi == "0400101556")
            //{
            if (HD == null)
            {
                HopDong hopDong = new HopDong();
                hopDong = HopDong.Load(HopDong.GetID(txtSoHD.Text));
                DateTime NgayHetHan = hopDong.NgayGiaHan.Year == 1900 ? hopDong.NgayHetHan : hopDong.NgayGiaHan;

                TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + hopDong.TenDonViDoiTac + "#&" + txtGhiChu.Text;
                // Cảnh báo hết hạn hợp đồng gia công
                if (NgayHetHan < DateTime.Now)
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + hopDong.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " đã hết hạn hợp đồng .\nDoanh nghiệp kiểm tra thông tin Hợp đồng trước khi khai báo tờ khai", false);
                }
                else if (NgayHetHan <= DateTime.Now.AddDays(30))
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + hopDong.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " chỉ còn :  " + (NgayHetHan - DateTime.Now).TotalDays.ToString("N0") + " ngày là hết hạn .\nDoanh nghiệp thực hiện Khai báo phụ kiện Gia hạn hợp đồng trước khi Hợp đồng hết hạn", false);
                }
            }
            else
            {
                DateTime NgayHetHan = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
                TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + HD.TenDonViDoiTac + "#&" + txtGhiChu.Text;
                // Cảnh báo hết hạn hợp đồng gia công
                if (NgayHetHan < DateTime.Now)
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + HD.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " đã hết hạn hợp đồng .\nDoanh nghiệp kiểm tra thông tin Hợp đồng trước khi khai báo tờ khai", false);
                }
                else if (NgayHetHan <= DateTime.Now.AddDays(30))
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + HD.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " chỉ còn :  " + (NgayHetHan - DateTime.Now).TotalDays.ToString("N0") + " ngày là hết hạn .\nDoanh nghiệp thực hiện Khai báo phụ kiện Gia hạn hợp đồng trước khi Hợp đồng hết hạn", false);
                }
            }
            //}
            //else
            //{
            //    if (HD == null)
            //    {
            //        DateTime NgayHetHan = HopDong.GetDate(txtSoHD.Text);
            //        TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + txtGhiChu.Text;
            //    }
            //    else
            //    {
            //        TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + HD.NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + txtGhiChu.Text;
            //    }
            //    //TKMD.GhiChu = txtGhiChu.Text;
            //}
            //FIX
            //TKMD.GhiChu = txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + txtGhiChu.Text;
            //TKMD.GhiChu = txtGhiChu.Text;
            //FIX
#endif
            TKMD.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
            //Van don
            
            if (!String.IsNullOrEmpty(txtSoVanDon.Text.ToString().Trim()))
            {
                TKMD.VanDonCollection =  KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic("TKMD_ID =" + TKMD.ID, "ID");
                foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                {
                    if (item.ID > 0)
                    {
                        item.Delete();
                    }
                }
                TKMD.VanDonCollection.Clear();                
                if (TKMD.VanDonCollection.Count == 0)
                    TKMD.VanDonCollection.Add(vd);
                TKMD.VanDonCollection[0].SoTT = 1;
                TKMD.VanDonCollection[0].SoDinhDanh = txtSoVanDon.Text;
            }
            else
            {
                if (TKMD.VanDonCollection.Count == 1)
                {
                    TKMD.VanDonCollection[0].SoTT = 1;
                    TKMD.VanDonCollection[0].SoDinhDanh = txtSoVanDon.Text;
                }
            }
            TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongHeSoPhanBoTG.Value);
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongTriGiaHD.Value);
            if (ctrMaPhuongThucVT.Code == "9")
            {
                TKMD.TenDiaDiemDohang = txtTenDiaDiemDoHang.Text.Trim();
                TKMD.MaDiaDiemDoHang = txtMaDiaDiemDoHang.Text.Trim();
                TKMD.TenDiaDiemXepHang = txtTenDiaDiemXepHang.Text.Trim();
                TKMD.MaDiaDiemXepHang = txtMaDiaDiemXepHang.Text.Trim();
            }
            #region get loai hang cho to khai GC và SXXK
#if SXXK_V4 || GC_V4
            if (rdbNPL.Checked)
                TKMD.LoaiHang = "N";
            else if (rdbThietBi.Checked)
                TKMD.LoaiHang = "T";
            //else if (rdbHangMau.Checked)
            //    TKMD.LoaiHang = "H";
            else if (rdbSanPham.Checked)
                TKMD.LoaiHang = "S";
#endif
            #endregion

        }

        private void SetToKhai()
        {
            txtSoToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            txtSoToKhai.TextChanged += new EventHandler(txt_TextChanged);

            txtSoToKhaiDauTien.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoToKhaiDauTien.Text = TKMD.SoToKhaiDauTien;
            txtSoToKhaiDauTien.TextChanged += new EventHandler(txt_TextChanged);

            if (TKMD.SoNhanhToKhai == 0)
            {
                txtSoNhanhToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoNhanhToKhai.Text = "";
                txtSoNhanhToKhai.TextChanged += new EventHandler(txt_TextChanged);
            }
            else
            {
                txtSoNhanhToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoNhanhToKhai.Text = Convert.ToString(TKMD.SoNhanhToKhai);
                txtSoNhanhToKhai.TextChanged += new EventHandler(txt_TextChanged);
            }
            if (TKMD.TongSoTKChiaNho == 0)
            {
                txtTongSoTKChiaNho.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongSoTKChiaNho.Text = "";
                txtTongSoTKChiaNho.TextChanged += new EventHandler(txt_TextChanged);
            }
            else
            {
                txtTongSoTKChiaNho.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongSoTKChiaNho.Text = Convert.ToString(TKMD.TongSoTKChiaNho);
                txtTongSoTKChiaNho.TextChanged += new EventHandler(txt_TextChanged);
            }
            if (TKMD.SoToKhaiTNTX == 0)
            {
                txtSoToKhaiTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhaiTNTX.Text = "";
                txtSoToKhaiTNTX.TextChanged += new EventHandler(txt_TextChanged);
            }
            else
            {
                txtSoToKhaiTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhaiTNTX.Text = Convert.ToString(TKMD.SoToKhaiTNTX);
                txtSoToKhaiTNTX.TextChanged += new EventHandler(txt_TextChanged);
            }
            ctrMaLoaiHinh.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            ctrMaLoaiHinh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaPhanLoaiHH.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            ctrMaPhanLoaiHH.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrCoQuanHaiQuan.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
            ctrCoQuanHaiQuan.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            //ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            //ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            //ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;

            //Phan luong
            if (string.IsNullOrEmpty(TKMD.MaPhanLoaiKiemTra))
            {
                lblPhanLuong.Text = string.Empty;
            }
            else
            {
                string pl = TKMD.MaPhanLoaiKiemTra.Trim();
                if (pl == "1")
                {
                    lblPhanLuong.Text = "Luồng Xanh";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Green;
                }
                if (pl == "2")
                {
                    lblPhanLuong.Text = "Luồng Vàng";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Yellow;
                }
                if (pl == "3")
                {
                    lblPhanLuong.Text = "Luồng Đỏ";
                    this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
                }
            }
            ctrNhomXuLyHS.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy.EditValueChangedHandle(txt_TextChanged);
            ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            ctrNhomXuLyHS.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy.EditValueChangedHandle(txt_TextChanged);

            clcThoiHanTaiNhap.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcThoiHanTaiNhap.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            clcThoiHanTaiNhap.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            clcNgayDangKy.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayDangKy.Value = TKMD.NgayDangKy;
            clcNgayDangKy.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrMaPhuongThucVT.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
            ctrMaPhuongThucVT.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            //clcThoiHanTaiNhap.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            //clcNgayDangKy.Value = TKMD.NgayDangKy;
            //ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
            //Don vi
            txtMaDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDonVi.Text = TKMD.MaDonVi;
            txtMaDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtTenDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDonVi.Text = TKMD.TenDonVi;
            txtTenDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtMaBuuChinhDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            txtMaBuuChinhDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            txtDiaChiDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDienThoaiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;
            txtSoDienThoaiDonVi.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaDonVi.Text = TKMD.MaDonVi;
            //txtTenDonVi.Text = TKMD.TenDonVi;
            //txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            //txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            //txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;

            //uy thac
            txtMaUyThac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaUyThac.Text = TKMD.MaUyThac;
            txtMaUyThac.TextChanged += new EventHandler(txt_TextChanged);

            txtTenUyThac.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenUyThac.Text = TKMD.TenUyThac;
            txtTenUyThac.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaUyThac.Text = TKMD.MaUyThac;
            //txtTenUyThac.Text = TKMD.TenUyThac;
            //doi tac
            txtMaDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDoiTac.Text = TKMD.MaDoiTac;
            txtMaDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtTenDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDoiTac.Text = TKMD.TenDoiTac;
            txtTenDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtMaBuuChinhDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            txtMaBuuChinhDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac1.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            txtDiaChiDoiTac1.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac2.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            txtDiaChiDoiTac2.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac3.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            txtDiaChiDoiTac3.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac4.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            txtDiaChiDoiTac4.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaNuocDoiTac.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaNuocDoiTac.Code = TKMD.MaNuocDoiTac;
            ctrMaNuocDoiTac.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtMaDaiLyHQ.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            txtMaDaiLyHQ.TextChanged += new EventHandler(txt_TextChanged);

            txtTenDaiLyHaiQuan.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDaiLyHaiQuan.Text = TKMD.TenDaiLyHaiQuan;
            txtTenDaiLyHaiQuan.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaDoiTac.Text = TKMD.MaDoiTac;
            //txtTenDoiTac.Text = TKMD.TenDoiTac;
            //txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            //txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            //txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            //txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            //txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            //ctrMaNuocDoiTac.Code = TKMD.MaNuocDoiTac;
            //txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            //txtTenDaiLyHaiQuan.Text = TKMD.TenDaiLyHaiQuan;

            //so kien va trong luong
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTSoLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            ctrMaDVTSoLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTrongLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            txtTrongLuong.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTTrongLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;
            ctrMaDVTTrongLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            //ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            //txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            //ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;

            //van don
            ctrMaDDLuuKho.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
            ctrMaDDLuuKho.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoHieuKyHieu.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            txtSoHieuKyHieu.TextChanged += new EventHandler(txt_TextChanged);

            txtMaPTVC.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaPTVC.Text = TKMD.MaPTVC;
            txtMaPTVC.TextChanged += new EventHandler(txt_TextChanged);

            txtTenPTVC.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenPTVC.Text = TKMD.TenPTVC;
            txtTenPTVC.TextChanged += new EventHandler(txt_TextChanged);

            clcNgayHangDen.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayHangDen.Value = TKMD.NgayHangDen;
            clcNgayHangDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemNhanHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemNhanHang.Code = TKMD.MaDiaDiemDoHang;
            ctrDiaDiemNhanHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemNhanHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemNhanHang.Ten = TKMD.TenDiaDiemDohang;
            ctrDiaDiemNhanHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemXepHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemXepHang.Code = (TKMD.MaDiaDiemXepHang != null && TKMD.MaDiaDiemXepHang.Length > 3 && TKMD.MaPhuongThucVT != "4") ? TKMD.MaDiaDiemXepHang.Substring(2, TKMD.MaDiaDiemXepHang.Length - 2) : TKMD.MaDiaDiemXepHang;
            ctrDiaDiemXepHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemXepHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemXepHang.Ten = TKMD.TenDiaDiemXepHang;
            ctrDiaDiemXepHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            //ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
            //txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            //txtMaPTVC.Text = TKMD.MaPTVC;
            //txtTenPTVC.Text = TKMD.TenPTVC;
            //clcNgayHangDen.Value = TKMD.NgayHangDen;
            //ctrDiaDiemNhanHang.Code = TKMD.MaDiaDiemDoHang;
            //ctrDiaDiemNhanHang.Ten = TKMD.TenDiaDiemDohang;
            //ctrDiaDiemXepHang.Code = (TKMD.MaDiaDiemXepHang != null && TKMD.MaDiaDiemXepHang.Length > 3 && TKMD.MaPhuongThucVT != "4") ? TKMD.MaDiaDiemXepHang.Substring(2, TKMD.MaDiaDiemXepHang.Length - 2) : TKMD.MaDiaDiemXepHang;
            //ctrDiaDiemXepHang.Ten = TKMD.TenDiaDiemXepHang;

            //hoa don
            ctrPhanLoaiHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            ctrPhanLoaiHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoTiepNhanHD.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            txtSoTiepNhanHD.TextChanged += new EventHandler(txt_TextChanged);

            txtSoHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoHoaDon.Text = TKMD.SoHoaDon;
            txtSoHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            clcNgayPhatHanhHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            clcNgayPhatHanhHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrPhuongThucTT.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhuongThucTT.Code = TKMD.PhuongThucTT;
            ctrPhuongThucTT.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrPhanLoaiGiaHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhanLoaiGiaHD.Code = TKMD.PhanLoaiGiaHD;
            ctrPhanLoaiGiaHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaDieuKienGiaHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDieuKienGiaHD.Code = TKMD.MaDieuKienGiaHD;
            ctrMaDieuKienGiaHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTHoaDon.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            ctrMaTTHoaDon.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTongTriGiaHD.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);
            txtTongTriGiaHD.TextChanged += new EventHandler(txt_TextChanged);

            //ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            //txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            //txtSoHoaDon.Text = TKMD.SoHoaDon;
            //clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            //ctrPhuongThucTT.Code = TKMD.PhuongThucTT;
            //ctrPhanLoaiGiaHD.Code = TKMD.PhanLoaiGiaHD;
            //ctrMaDieuKienGiaHD.Code = TKMD.MaDieuKienGiaHD;
            //ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            //txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);

            //foreach (KDT_VNACC_TK_PhanHoi_TyGia item in TKMD.TyGiaCollection )
            //    {
            //        if (item.MaTTTyGiaTinhThue == TKMD.MaTTTriGiaTinhThue)
            //        {
            //            txtTriGiaTinhThue.Text = Convert.ToString(Convert.ToDecimal(TKMD.TriGiaTinhThue) * Convert.ToDecimal(item.TyGiaTinhThue));
            //            ctrMaTTTriGiaTinhThue.Code = "VND";
            //            break;
            //        }
            //        else 
            //        {
            //            txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            //            ctrMaTTTriGiaTinhThue.Code = TKMD.MaTTTriGiaTinhThue;
            //        }

            //    }

            txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            txtPhanLoaiKhongQDVND.TextChanged -= new EventHandler(txt_TextChanged);
            txtPhanLoaiKhongQDVND.Text = TKMD.PhanLoaiKhongQDVND;
            txtPhanLoaiKhongQDVND.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTTriGiaTinhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTriGiaTinhThue.Code = TKMD.MaTTTriGiaTinhThue;
            ctrMaTTTriGiaTinhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTongHeSoPhanBoTG.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
            txtTongHeSoPhanBoTG.TextChanged += new EventHandler(txt_TextChanged);

            lblMaPhanLoaiTongGiaCoBan.TextChanged -= new EventHandler(txt_TextChanged);
            lblMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiDieuChinhTriGia;
            lblMaPhanLoaiTongGiaCoBan.TextChanged += new EventHandler(txt_TextChanged);


            //txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            //txtPhanLoaiKhongQDVND.Text = TKMD.PhanLoaiKhongQDVND;
            //ctrMaTTTriGiaTinhThue.Code = TKMD.MaTTTriGiaTinhThue;
            //txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
            //lblMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiDieuChinhTriGia;

            //han muc
            ctrNguoiNopThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            ctrNguoiNopThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaNHTraThueThay.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaNHTraThueThay.Code = TKMD.MaNHTraThueThay;
            ctrMaNHTraThueThay.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            //ctrMaNHTraThueThay.Code = TKMD.MaNHTraThueThay;

            if (TKMD.NamPhatHanhHM == 0)
            {
                txtNanPhatHanhHM.TextChanged -= new EventHandler(txt_TextChanged);
                txtNanPhatHanhHM.Text = "";
                txtNanPhatHanhHM.TextChanged += new EventHandler(txt_TextChanged);

                //txtNanPhatHanhHM.Text = "";
            }
            else
            {
                txtNanPhatHanhHM.TextChanged -= new EventHandler(txt_TextChanged);
                txtNanPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
                txtNanPhatHanhHM.TextChanged += new EventHandler(txt_TextChanged);

                //txtNanPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
            }
            txtKyHieuCTHanMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            txtKyHieuCTHanMuc.TextChanged += new EventHandler(txt_TextChanged);

            txtSoCTHanMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;
            txtSoCTHanMuc.TextChanged += new EventHandler(txt_TextChanged);

            //txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            //txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;
            //bao lanh thue
            ctrMaXDThoiHanNopThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            ctrMaXDThoiHanNopThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaNHBaoLanh.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaNHBaoLanh.Code = TKMD.MaNHBaoLanh;
            ctrMaNHBaoLanh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            //ctrMaNHBaoLanh.Code = TKMD.MaNHBaoLanh;

            if (TKMD.NamPhatHanhBL == 0)
            {
                txtNamPhatHanhBL.TextChanged -= new EventHandler(txt_TextChanged);
                txtNamPhatHanhBL.Text = "";
                txtNamPhatHanhBL.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhBL.Text = "";
            }
            else
            {
                txtNamPhatHanhBL.TextChanged -= new EventHandler(txt_TextChanged);
                txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
                txtNamPhatHanhBL.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
            }
            txtKyHieuCTBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            txtKyHieuCTBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            txtSoCTBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            txtSoCTBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            //txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            //txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            //----
            clcNgayKhoiHanhVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            clcNgayKhoiHanhVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemDichVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemDichVC.Code = TKMD.DiaDiemDichVC;
            ctrDiaDiemDichVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            clcNgayDen.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayDen.Value = TKMD.NgayDen;
            clcNgayDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            //clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            //ctrDiaDiemDichVC.Code = TKMD.DiaDiemDichVC;
            //clcNgayDen.Value = TKMD.NgayDen;

#if KD_V4 || SXXK_V4
            txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
            txtGhiChu.Text = TKMD.GhiChu;
            txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            //txtGhiChu.Text = TKMD.GhiChu;
#elif GC_V4

            //if (TKMD.MaDonVi == "4000395355" || TKMD.MaDonVi == "0400101556")
            //{
            //    string ghichu = TKMD.GhiChu;
            //    if (ghichu != null)
            //    {
            //        int a = ghichu.LastIndexOf("#&");
            //        if (a > 1)
            //            txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
            //    }
            //}
            //else
            //{
            //    txtGhiChu.Text = TKMD.GhiChu;
            //}
            ////FIX HOATHO////FIX HOATHO
            string ghichu = TKMD.GhiChu;
            if (ghichu != null)
            {
                int a = ghichu.LastIndexOf("#&");
                if (a > 1)
                {
                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                    //txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
                }
                else
                {
                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = TKMD.GhiChu;
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                    //txtGhiChu.Text = TKMD.GhiChu;
                }
            }
            //FIX HOATHO
            //txtGhiChu.Text = TKMD.GhiChu;
#endif
            txtSoQuanLyNoiBoDN.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            txtSoQuanLyNoiBoDN.TextChanged += new EventHandler(txt_TextChanged);

            //txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            //van don

            if (TKMD.VanDonCollection.Count >= 1)
            {
                txtSoVanDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
                txtSoVanDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
            }
            // thông tin thue và lệ phi

            txtPhanLoaiNopThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtPhanLoaiNopThue.Text = TKMD.PhanLoaiNopThue;
            txtPhanLoaiNopThue.TextChanged += new EventHandler(txt_TextChanged);

            txtTongSoTienThueXuatKhau.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoTienThueXuatKhau.Text = TKMD.TongSoTienThueXuatKhau.ToString();
            txtTongSoTienThueXuatKhau.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTTongTienThueXuatKhau.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTongTienThueXuatKhau.Code = TKMD.MaTTTongTienThueXuatKhau;
            ctrMaTTTongTienThueXuatKhau.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTongSoTienLePhi.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoTienLePhi.Text = TKMD.TongSoTienLePhi.ToString();
            txtTongSoTienLePhi.TextChanged += new EventHandler(txt_TextChanged);

            txtSoTienBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTienBaoLanh.Text = TKMD.SoTienBaoLanh.ToString();
            txtSoTienBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTCuaSoTienBaoLanh.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTCuaSoTienBaoLanh.Code = TKMD.MaTTCuaSoTienBaoLanh;;
            ctrMaTTCuaSoTienBaoLanh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);


            txtTongSoTrangCuaToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoTrangCuaToKhai.Text = TKMD.TongSoTrangCuaToKhai.ToString();
            txtTongSoTrangCuaToKhai.TextChanged += new EventHandler(txt_TextChanged);

            txtTongSoDongHangCuaToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoDongHangCuaToKhai.Text = TKMD.TongSoDongHangCuaToKhai.ToString();
            txtTongSoDongHangCuaToKhai.TextChanged += new EventHandler(txt_TextChanged);


            //txtPhanLoaiNopThue.Text = TKMD.PhanLoaiNopThue;
            //txtTongSoTienThueXuatKhau.Text = TKMD.TongSoTienThueXuatKhau.ToString();
            //ctrMaTTTongTienThueXuatKhau.Code = TKMD.MaTTTongTienThueXuatKhau;
            //txtTongSoTienLePhi.Text = TKMD.TongSoTienLePhi.ToString();
            //txtSoTienBaoLanh.Text = TKMD.SoTienBaoLanh.ToString();
            //ctrMaTTCuaSoTienBaoLanh.Code = TKMD.MaTTCuaSoTienBaoLanh;
            //txtTongSoDongHangCuaToKhai.Text = TKMD.TongSoDongHangCuaToKhai.ToString();
            //txtTongSoTrangCuaToKhai.Text = TKMD.TongSoTrangCuaToKhai.ToString();

            ctrCoQuanHaiQuan_Leave(null, null);
            LoadGrid();
            //Loai hop dong
#if GC_V4
            if (TKMD.HopDong_ID > 0)
            {
                HopDong hd = HopDong.Load(TKMD.HopDong_ID);
                txtSoHD.Text = hd.SoHopDong;
                lblNgayHD1.Text = hd.NgayKy.ToString("dd/MM/yyyy");

            }
#endif

            if (TKMD.MaPhuongThucVT == "9")
            {
                ctrDiaDiemXepHang.Visible = ctrDiaDiemNhanHang.Visible = false;
                txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = true;

                txtMaDiaDiemXepHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDiaDiemXepHang.Text = TKMD.MaDiaDiemXepHang;
                txtMaDiaDiemXepHang.TextChanged += new EventHandler(txt_TextChanged);

                txtMaDiaDiemDoHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDiaDiemDoHang.Text = TKMD.MaDiaDiemDoHang;
                txtMaDiaDiemDoHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDiaDiemXepHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDiaDiemXepHang.Text = TKMD.TenDiaDiemXepHang;
                txtTenDiaDiemXepHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDiaDiemDoHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDiaDiemDoHang.Text = TKMD.TenDiaDiemDohang;
                txtTenDiaDiemDoHang.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaDiaDiemXepHang.Text = TKMD.MaDiaDiemXepHang;
                //txtMaDiaDiemDoHang.Text = TKMD.MaDiaDiemDoHang;
                //txtTenDiaDiemXepHang.Text = TKMD.TenDiaDiemXepHang;
                //txtTenDiaDiemDoHang.Text = TKMD.TenDiaDiemDohang;
            }
            #region get loai hang cho to khai GC và SXXK
#if SXXK_V4 || GC_V4
            if (TKMD.LoaiHang == "N")
            {
                rdbNPL.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbNPL.Checked = true;
                rdbNPL.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbNPL.Checked = true;
            }
            else if (TKMD.LoaiHang == "T")
            {
                rdbThietBi.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbThietBi.Checked = true;
                rdbThietBi.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbThietBi.Checked = true;
            }
            //else if (TKMD.LoaiHang == "H")
            //    rdbHangMau.Checked = true;
            else if (TKMD.LoaiHang == "S")
            {
                rdbSanPham.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbSanPham.Checked = true;
                rdbSanPham.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbSanPham.Checked = true;
            }
#endif
            #endregion
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showThemHang();
                    break;
                case "cmdThemHangDon":
                    showThemHang();
                    break;
                case "cmdThemHangExcel":
                    showThemHangExcel();
                    break;
                case "cmdChiThiHQ":
                    showChiThiHQ();
                    break;
                case "cmdGiayPhep":
                    showGiayPhep();
                    break;
                case "cmdDinhKemDT":
                    showDinhKem();
                    break;
                case "cmdTrungChuyen":
                    showTrungChuyen();
                    break;
                case "cmdContainer":
                    showContainer();
                    break;
                case "cmdLuu":
                    saveToKhai();
                    break;
                case "cmdEDA":
                    SendVnaccs(false);
                    break;
                case "cmdEDA01":
                    SendVnaccs(true);
                    break;
                case "cmdEDC":
                    SendVnaccsEDC(false);
                    break;
                case "cmdEDE":
                    SendVnaccsEDC(true);
                    break;
                case "cmdKetQuaTraVe":
                    ShowKetQuaTraVe();
                    break;
                case "cmdEDB":
                    GetIEX(false);
                    break;
                case "cmdIEX":
                    GetIEX(true);
                    break;
                case "cmdEDD":
                    GetEDD(true);
                    break;
                case "cmdKhaiBaoSua":
                    if (this.ShowMessage("Bạn muốn chuyển sang trạng thái sửa tờ khai", true) == "Yes")
                    {
                        TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                        TKMD.InsertUpdateFull();
                        SetToKhai();
                        setCommandStatus();
                    }
                    break;
                case "cmdIN":
                    InToKhaiTam();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdReloadData":
                    ReloadData();
                    break;
#if GC_V4
                case "cmdCanDoiNhapXuat":
                    ShowCanDoiNhapXuat();
                    break;
#endif
                case "cmdBSContainer":
                    showDanhSachCont();
                    break;
                case "cmdMau30":
                    Mau30();
                    break;
                case "cmdPrint":
                    ReportViewVNACCSToKhaiXuat();
                    break;
                case "cmdSendVouchers":
                    SendVNACCS_Vouchers();
                    break;
                case "cmdToKhaiNhanh":
                    ShowDanhSachTKNhanh();
                    break;
            }
        }
        private void ShowDanhSachTKNhanh()
        {
            VNACC_ToKhaiMauDichNhanhManagerForm f = new VNACC_ToKhaiMauDichNhanhManagerForm();
            if (TKMD.SoToKhaiDauTien.ToString() == "F")
            {
                if (ToKhaiMauDichCollection.Count > 0)
                {
                    f.TKMDCollection = ToKhaiMauDichCollection;
                }
                else
                {
                    if (TKMD.SoToKhai != 0)
                    {
                        f.TKMDCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoToKhaiDauTien='" + TKMD.SoToKhai + "'", "ID");
                    }
                    else
                    {
                        f.TKMDCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoHoaDon='" + TKMD.SoHoaDon.ToString() + "' AND SoToKhaiDauTien NOT IN ('F')", "ID");
                    }
                }
            }
            f.ShowDialog(this);
        }
        private void SendVNACCS_Vouchers()
        {

            VNACC_VouchersForm f = new VNACC_VouchersForm();
            f.FormType = "TKMD";
            f.TKMD = TKMD;
            f.ShowDialog(this);

        }
        private void ReportViewVNACCSToKhaiXuat()
        {
            ProcessReport.ShowReport(TKMD, "ReportViewVNACCSToKhaiXuat");
        }
        private void Mau30()
        {
            //if (TKMD.ContainerTKMD.SoContainerCollection.Count > 0) 
            //{
            //    MessageBox.Show("Mẫu 30 hàng lẻ không sử dụng cho tờ khai xuất container", "Mẫu 30", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //else
            //{
            //    Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38();
            //    bangkeCont.BindReport(TKMD.ID);
            //    bangkeCont.ShowRibbonPreview();
            //}
            if (TKMD.ContainerTKMD.SoContainerCollection.Count == 0 && (TKMD.MaPhuongThucVT == "3" || TKMD.MaPhuongThucVT == "1"))
            {
                Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38();
                bangkeCont.BindReport(TKMD.ID);
                bangkeCont.ShowRibbonPreview();

            }
            else
            {
                ShowMessage("Mẫu 30 không sử dụng cho tờ khai này", false);
                //MessageBox.Show("Mẫu 30 không sử dụng cho tờ khai này", "Thông báo", MessageBoxButtons.OK);
            }
        }
        private void showDanhSachCont()
        {
            ContainerBSManagerForm frm = new ContainerBSManagerForm();
            frm.MaDoanhNghiep = TKMD.MaDonVi;
            frm.TKMD = TKMD;
            //if (TKMD.DiaDiemDichVC.Trim() != "")
            //    frm.MaHaiQuan = TKMD.DiaDiemDichVC.Substring(0, 4);
            //else
            frm.MaHaiQuan = TKMD.CoQuanHaiQuan;
            frm.ShowDialog();

        }
        private void GetLoaiHangHoa()
        {

            if (rdbNPL.Checked)
            {
                loaiHangHoa = "N";
            }
            else if (rdbThietBi.Checked)
            {
                loaiHangHoa = "T";
            }
            else if (rdbSanPham.Checked)
            {
                loaiHangHoa = "S";
            }


        }

        private void showThemHang()
        {
            VNACC_HangMauDichXuatForm f = new VNACC_HangMauDichXuatForm();
            f.TKMD = this.TKMD;
#if SXXK_V4 || GC_V4
            GetLoaiHangHoa();
            f.loaiHangHoa = loaiHangHoa;
#endif
            f.MaTienTe = ctrMaTTHoaDon.Code;
            f.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }
        private void showThemHangExcel()
        {

            ReadExcelFormVNACCS f = new ReadExcelFormVNACCS();
            f.LoaiHinh = "X";
            f.TKMD = this.TKMD;
            f.MaTienTen_HMD = ctrMaTTHoaDon.Code;
            f.ShowDialog();
            this.SetChange(f.IsSuccess);
        }

        private void showChiThiHQ()
        {
            if (TKMD.SoToKhai != 0)
            {
                VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
                f.Master_ID = this.TKMD.ID;
                f.LoaiThongTin = ELoaiThongTin.TK;
                f.ShowDialog();
            }
        }

        private void showGiayPhep()
        {
            VNACC_TK_GiayPhepForm f = new VNACC_TK_GiayPhepForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }

        private void showDinhKem()
        {
            VNACC_TK_DinhKiemDTForm f = new VNACC_TK_DinhKiemDTForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }

        private void showTrungChuyen()
        {
            VNACC_TK_TrungChuyenForm f = new VNACC_TK_TrungChuyenForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);

        }

        private void showContainer()
        {
            if (TKMD.ID != 0)
            {
                VNACC_TK_Container f = new VNACC_TK_Container();
                f.Cont = TKMD.ContainerTKMD;
                f.TKMD = this.TKMD;
                f.ShowDialog();
                this.SetChange(f.IsChange);
            }
            else
                ShowMessage("Lưu tờ khai trước thêm Container", false);
        }

        private void saveToKhai()
        {
            try
            {
               // ctrMaPhanLoaiHH.Code = "";
                this.Cursor = Cursors.WaitCursor;
#if SXXK_V4 ||GC_V4
                List<string> mahang = new List<string>();
                string mahangloi = "";
                string checkMaQuanLy = string.Empty;
                foreach(KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                {
                    if (!checkMaHang(hmd.MaHangHoa,loaiHangHoa))
                    {
                        mahang.Add(hmd.MaHangHoa);
                    }
                    if (hmd.MaQuanLy == "" || hmd.MaQuanLy == null)
                    {
                        checkMaQuanLy = checkMaQuanLy + " - " + hmd.MaHangHoa; 
                    }
                }

                if (!string.IsNullOrEmpty(checkMaQuanLy))
                    if (ShowMessage("THÔNG TIN HÀNG [" + checkMaQuanLy + " ] KHÔNG CÓ MÃ QUẢN LÝ RIÊNG. DOANH NGHIỆP CÓ MUỐN KIỂM TRA LẠI THÔNG TIN HÀNG.", true) == "Yes")
                        return;
                if (loaiHangHoa == "N" && mahang.Count > 0)
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ NGUYÊN PHỤ LIỆU]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + (i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐẾN HQ. DOANH NGHIỆP HÃY KHAI BÁO NPL TRƯỚC KHI KHAI TỜ KHAI.", "MSG_PUB06", "", false);
                       
                    }

                }
                else if (loaiHangHoa == "S")
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ SẢN PHẨM]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + (i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        if (MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐỊNH MỨC. DOANH NGHIỆP XÁC ĐỊNH  KHAI TỜ KHAI CÓ NHỮNG SẢN PHẨM NÀY. ", "MSG_PUB06", "", true) == "No")
                            return;
                    }
                }
                else if (loaiHangHoa == "T")
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ THIẾT BỊ]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + (i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐẾN HQ. DOANH NGHIỆP HÃY KHAI BÁO THIẾT BỊ NÀY TRƯỚC KHI KHAI TỜ KHAI. ", "MSG_PUB06", "", false);
                        return;
                    }
                }

#endif
                GetToKhai();
                if (TKMD.HangCollection.Count == 0)
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }
                else
                {
#if GC_V4
                    if (string.IsNullOrEmpty(TKMD.DiaDiemDichVC) || TKMD.DiaDiemDichVC.Trim() == "")
                    {
                        if (this.ShowMessage("Tờ khai chưa nhập ĐỊA ĐIỂM ĐÍCH CHO VẬN CHUYỂN BẢO THUẾ. Bạn có muốn tiếp tục?", true) == "No")
                            return;
                    }
                    //if (GlobalSettings.MA_DON_VI != "4000395355")
                    //{
                        if (!KiemTraLuongTon())
                            return;
                    //}
#endif
                    try
                    {
                        if (!String.IsNullOrEmpty(TKMD.SoHoaDon) || TKMD.VanDonCollection.Count > 0)
                        {
                            string listVanDon = "";
                            string SoHoaDon = "";
                            for (int i = 0; i < TKMD.VanDonCollection.Count; i++)
                            {
                                listVanDon += "'" + TKMD.VanDonCollection[i].SoDinhDanh.ToString() + "',";
                            }
                            if (!String.IsNullOrEmpty(listVanDon))
                            {
                                listVanDon = listVanDon.Substring(0, listVanDon.Length - 1);
                            }
                            else
                            {
                                listVanDon = "''";
                            }
                            if (!String.IsNullOrEmpty(TKMD.SoHoaDon))
                            {
                                SoHoaDon = TKMD.SoHoaDon;
                            }
                            else
                            {
                                SoHoaDon = "''";
                            }
                            string result = KDT_VNACC_ToKhaiMauDich.CheckBillAndInvoice(SoHoaDon, listVanDon, TKMD.SoToKhai);
                            if (!String.IsNullOrEmpty(result))
                            {
                                if (result != "0")
                                {
                                    if (ShowMessage("Thông tin Bill và Invoce của tờ khai trùng với tờ khai có số tờ khai : " + result + "\nBạn có muốn kiểm tra lại không ?", true) == "Yes")
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    ValidateForm(true);
                    if (TKMD.MaPhuongThucVT == "2")
                    {
                        if (TKMD.ContainerTKMD.SoContainerCollection.Count == 0)
                        {
                            if (this.ShowMessage("Mã hiệu phương thức vận chuyển đã chọn là :  Đường biển (container) nhưng Bạn chưa nhập danh sách Container của tờ khai . Bạn có muốn tiếp tục khai báo?", true) == "No")
                                return;
                        }
                    }
                    if (TKMD.HangCollection.Count > 0)
                    {
                        // Tách thành các tờ khai nhánh .
                        if (TKMD.HangCollection.Count >= 51)
                        {
                            foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                            {
                                TongSoLuongHang += item.SoLuong1;
                                TongTriGiaHD += item.TriGiaHoaDon;
                            }
                            int SoToKhaiNhanh = TKMD.HangCollection.Count / 50;
                            if (TKMD.HangCollection.Count % 50 != 0)
                                SoToKhaiNhanh += 1;
                            if (ShowMessage("Tổng số dòng hàng nhập vào là : " + TKMD.HangCollection.Count + " > 50 dòng hàng . Phần mềm sẽ tự động tách ra thành  : " + SoToKhaiNhanh + " tờ khai nhánh", true) == "Yes")
                            {
                                ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
                                TKMD.SoToKhaiDauTien = "F";
                                TKMD.SoNhanhToKhai = 1;
                                TKMD.TongSoTKChiaNho = SoToKhaiNhanh;
                                ToKhaiMauDichCollection = CopyToKhai(TKMD, true, SoToKhaiNhanh);

                                TKMD.HangCollection = TKMD.HangCollection.GetRange(0, 50);
                                foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                                {
                                    TongSoLuongHangTK += item.SoLuong1;
                                    TongHeSoPhanBo += item.TriGiaHoaDon;
                                }
                                TKMD.TongTriGiaHD = TongTriGiaHD;
                                TKMD.TongHeSoPhanBoTG = TongHeSoPhanBo;
                                TKMD.TriGiaTinhThue = TongHeSoPhanBo;
                                TKMD.MaTTTriGiaTinhThue = TKMD.MaTTHoaDon;
                                //Phân bổ Phí và các khoản điều chỉnh .
                                if (TKMD.PhiVanChuyen != 0)
                                    TKMD.PhiVanChuyen = (TKMD.PhiVanChuyen / TongSoLuongHang) * TongSoLuongHangTK;
                                if (TKMD.PhiBaoHiem != 0)
                                    TKMD.PhiBaoHiem = (TKMD.PhiBaoHiem / TongSoLuongHang) * TongSoLuongHangTK;

                                if (TKMD.KhoanDCCollection.Count > 0)
                                {
                                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in TKMD.KhoanDCCollection)
                                    {
                                        item.TriGiaKhoanDieuChinh = (item.TriGiaKhoanDieuChinh / TongSoLuongHang) * TongSoLuongHangTK;
                                        item.TongHeSoPhanBo = TongHeSoPhanBo;
                                    }
                                }
                                VNACC_ToKhaiMauDichNhanhManagerForm f = new VNACC_ToKhaiMauDichNhanhManagerForm();
                                f.TKMDCollection = ToKhaiMauDichCollection;
                                f.ShowDialog(this);
                            }
                        }
                    }
                    // Kiểm tra Số lượng 2 và ĐVT số lượng 2
                    int count = 0;
                    foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                    {
                        if (item.SoLuong2 == 0 || String.IsNullOrEmpty(item.DVTLuong2))
                        {
                            count++;
                        }
                    }
                    if (count > 0)
                    {
                        if (this.ShowMessage("Tờ khai này có : " + count + " dòng hàng chưa nhập Số lượng 2 hoặc ĐVT Số lượng 2 .Doanh nghiệp có muốn kiểm tra lại tờ khai không ?", true) == "Yes")
                        {
                            return;
                        }
                    }
                    if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
                    {
                        if (this.ShowMessage("Tờ khai này đã được khai báo, bạn muốn thay đổi thông tin và khai báo lại ?", true) == "Yes")
                        {
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                            //TKMD.SoToKhai = 0;
                            TKMD.InsertUpdateFull();
                            ShowMessage("Lưu tờ khai thành công", false);
                            LoadGrid();
                            SetToKhai();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        TKMD.InsertUpdateFull();
                        ShowMessage("Lưu tờ khai thành công", false);
                        LoadGrid();
                        SetToKhai();
                    }
                    IsChange = false;
                    //TKMD.InsertUpdateFull();
                    //ShowMessage("Lưu tờ khai thành công", false);
                    //LoadGrid();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }
        private List<KDT_VNACC_ToKhaiMauDich> CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa, int number)
        {
            try
            {
                List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
                int index = 50;
                int count = 50;
                for (int i = 1; i < number; i++)
                {
                    KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
                    HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
                    tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
                    tkmdcopy.SoToKhai = 0;
                    tkmdcopy.SoNhanhToKhai = 0;
                    tkmdcopy.SoToKhaiDauTien = "";
                    tkmdcopy.SoNhanhToKhai = i + 1;
                    tkmdcopy.TongSoTKChiaNho = number;
                    tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
                    tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                    tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    tkmdcopy.TrangThaiXuLy = "0";
                    tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    tkmdcopy.InputMessageID = string.Empty;
                    tkmdcopy.MessageTag = string.Empty;
                    tkmdcopy.IndexTag = string.Empty;
                    tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
                    if (tkmd.HangCollection.Count >= 50 + count)
                    {
                        tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, count);
                    }
                    else
                    {
                        tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, tkmd.HangCollection.Count - index);
                    }
                    //Phân bổ Phí và các khoản điều chỉnh .
                    TongSoLuongHangTK = 0;
                    TongHeSoPhanBo = 0;
                    foreach (KDT_VNACC_HangMauDich item in tkmdcopy.HangCollection)
                    {
                        TongSoLuongHangTK += item.SoLuong1;
                        TongHeSoPhanBo += item.TriGiaHoaDon;
                    }
                    tkmdcopy.TongTriGiaHD = TongTriGiaHD;
                    tkmdcopy.TongHeSoPhanBoTG = TongHeSoPhanBo;
                    tkmdcopy.TriGiaTinhThue = TongHeSoPhanBo;
                    tkmdcopy.MaTTTriGiaTinhThue = tkmd.MaTTHoaDon;
                    if (TKMD.PhiVanChuyen != 0)
                        tkmdcopy.PhiVanChuyen = (TKMD.PhiVanChuyen / TongSoLuongHang) * TongSoLuongHangTK;
                    if (TKMD.PhiBaoHiem != 0)
                        tkmdcopy.PhiBaoHiem = (TKMD.PhiBaoHiem / TongSoLuongHang) * TongSoLuongHangTK;
                    tkmdcopy.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    if (TKMD.KhoanDCCollection.Count > 0)
                    {
                        tkmdcopy.KhoanDCCollection = TKMD.KhoanDCCollection;
                        foreach (KDT_VNACC_TK_KhoanDieuChinh item in tkmdcopy.KhoanDCCollection)
                        {
                            item.TriGiaKhoanDieuChinh = (item.TriGiaKhoanDieuChinh / TongSoLuongHang) * TongSoLuongHangTK;
                            item.TongHeSoPhanBo = TongHeSoPhanBo;
                        }
                    }

                    index += 50;
                    count += 49;
                    tkmdcopy.InsertUpdateFull();
                    tkmdcopy.LoadFull();
                    ToKhaiMauDichCollection.Add(tkmdcopy);
                }
                return ToKhaiMauDichCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }            
        }
        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdEDA;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDB;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDC;
        private Janus.Windows.UI.CommandBars.UICommand cmdIEX;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDA01;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDD;
        private Janus.Windows.UI.CommandBars.UICommand cmdEDE;
        /* private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe;*/
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoSua;
        private Janus.Windows.UI.CommandBars.UICommand Separator;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand Separator3;
        private void BoSungNghiepVu()
        {


            this.cmdEDA = new Janus.Windows.UI.CommandBars.UICommand("cmdEDA");
            this.cmdEDA.Key = "cmdEDA";
            this.cmdEDA.Name = "cmdEDA";
            this.cmdEDA.Text = "nghiệp vụ EDA (Khai báo tờ khai tạm)";

            this.cmdEDB = new Janus.Windows.UI.CommandBars.UICommand("cmdEDB");
            this.cmdEDB.Key = "cmdEDB";
            this.cmdEDB.Name = "cmdEDB";
            this.cmdEDB.Text = "nghiệp vụ EDB (Gọi tờ khai tạm) ";

            this.cmdEDC = new Janus.Windows.UI.CommandBars.UICommand("cmdEDC");
            this.cmdEDC.Key = "cmdEDC";
            this.cmdEDC.Name = "cmdEDC";
            this.cmdEDC.Text = "Nghiệp vụ EDC (Khai báo chính thức tờ khai)";

            this.cmdEDA01 = new Janus.Windows.UI.CommandBars.UICommand("cmdEDA01");
            this.cmdEDA01.Key = "cmdEDA01";
            this.cmdEDA01.Name = "cmdEDA01";
            this.cmdEDA01.Text = "nghiệp vụ EDA01 (khai báo sửa tạm)";

            this.cmdEDD = new Janus.Windows.UI.CommandBars.UICommand("cmdEDD");
            this.cmdEDD.Key = "cmdEDD";
            this.cmdEDD.Name = "cmdEDD";
            this.cmdEDD.Text = "nghiệp vụ EDD (Gọi tờ khai sửa) ";

            this.cmdIEX = new Janus.Windows.UI.CommandBars.UICommand("cmdIEX");
            this.cmdIEX.Key = "cmdIEX";
            this.cmdIEX.Name = "cmdIEX";
            this.cmdIEX.Text = "nghiệp vụ IEX (tham chiếu tờ khai) ";

            this.cmdEDE = new Janus.Windows.UI.CommandBars.UICommand("cmdEDE");
            this.cmdEDE.Key = "cmdEDE";
            this.cmdEDE.Name = "cmdEDE";
            this.cmdEDE.Text = "nghiệp vụ EDE (Xác nhận sửa tờ khai) ";

            //             this.cmdKetQuaTraVe = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            //             this.cmdKetQuaTraVe.Key = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Name = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Text = "Thông tin từ HQ";
            // 
            //             this.cmbMain.Commands.Add(cmdKetQuaTraVe);

            this.cmdKhaiBaoSua = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoSua");
            this.cmdKhaiBaoSua.Key = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Name = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Text = "Sửa TK Trong thông quan";

            this.Separator = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            Separator.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator2");
            Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator3");
            Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA01,
                this.cmdEDE,
                this.cmdEDD
                ,});


            /*this.cmd.Text = "Cập nhật thông tin ";*/
            this.cmdKhaiBao1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA,
                this.cmdEDB,
                this.Separator,
                this.cmdEDC,
                Separator2,
                this.cmdKhaiBaoSua,
                Separator3,
                this.cmdIEX
            });
        }

        #endregion Bổ sung nghiệp vụ

        #region Send VNACCS
        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                saveToKhai();
                if (string.IsNullOrEmpty(TKMD.DiaDiemDichVC)|| TKMD.DiaDiemDichVC.Trim() == "") 
                {
                    if (this.ShowMessage("Tờ khai chưa nhập ĐỊA ĐIỂM ĐÍCH CHO VẬN CHUYỂN BẢO THUẾ. Bạn có muốn tiếp tục khai báo?", true)=="No")
                        return;
                }
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (TKMD.MaPhuongThucVT=="2")
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection.Count ==0)
                    {
                        if (this.ShowMessage("Mã hiệu phương thức vận chuyển đã chọn là :  Đường biển (container) nhưng Bạn chưa nhập danh sách Container của tờ khai . Bạn có muốn tiếp tục khai báo?", true)=="No")
                            return;                        
                    }
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    if (TKMD.HangCollection != null && TKMD.HangCollection.Count > 0)
                    {
                        
                        if (string.IsNullOrEmpty(TKMD.SoToKhaiDauTien))
                        {
                            decimal triGiaHang = 0;
                            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                            {
                                triGiaHang += hmd.TriGiaHoaDon;
                            }

                            if (triGiaHang != TKMD.TongTriGiaHD)
                            {
                                if (this.ShowMessage("Tổng trị giá hàng là : " + triGiaHang.ToString("###.###.###.###,####") + " Trong khi đó tổng trị giá hóa đơn là :" + TKMD.TongTriGiaHD.ToString("###.###.###.###,####") + " . Bạn có chắc chắn muốn khai báo  ? ", true) != "Yes")
                                    return;
                            }
                        }
                         
                    }
                    EDA EDA = VNACCMaperFromObject.EDAMapper(TKMD);
                    if (EDA == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKMD.InsertUpdateFull();
                        msg = MessagesSend.Load<EDA>(EDA, TKMD.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<EDA>(EDA, true, EnumNghiepVu.EDA01, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                        if (f.columnErrors != null)
                        {
                            ShowValidateFormErrorSend(f.columnErrors);
                        }
                    }
                    else
                    {
                        string ketqua = "Khai báo thông tin thành công";
                        CapNhatThongTin(f.feedback.ResponseData);
                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai: " + SoToKhai;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        this.ShowMessageTQDT(ketqua, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        public bool CheckVouchers()
        {
            bool isExits = true;
            List<KDT_VNACCS_License> LicenseCollection = new List<KDT_VNACCS_License>();
            List<KDT_VNACCS_ContractDocument> ContractDocumentCollection = new List<KDT_VNACCS_ContractDocument>();
            List<KDT_VNACCS_CommercialInvoice> CommercialInvoiceCollection = new List<KDT_VNACCS_CommercialInvoice>();
            List<KDT_VNACCS_CertificateOfOrigin> CertificateOfOriginCollection = new List<KDT_VNACCS_CertificateOfOrigin>();
            List<KDT_VNACCS_BillOfLading> BillOfLadingCollection = new List<KDT_VNACCS_BillOfLading>();
            List<KDT_VNACCS_Container_Detail> ContainerCollection = new List<KDT_VNACCS_Container_Detail>();
            List<KDT_VNACCS_AdditionalDocument> AdditionalDocumentCollection = new List<KDT_VNACCS_AdditionalDocument>();
            int CountAll = 0;

            LicenseCollection = KDT_VNACCS_License.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            ContractDocumentCollection = KDT_VNACCS_ContractDocument.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            BillOfLadingCollection = KDT_VNACCS_BillOfLading.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            ContainerCollection = KDT_VNACCS_Container_Detail.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            if (LicenseCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_License item in LicenseCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (ContractDocumentCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_ContractDocument item in ContractDocumentCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CommercialInvoiceCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_CommercialInvoice item in CommercialInvoiceCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CertificateOfOriginCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_CertificateOfOrigin item in CertificateOfOriginCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (BillOfLadingCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_BillOfLading item in BillOfLadingCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (ContainerCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_Container_Detail item in ContainerCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (AdditionalDocumentCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_AdditionalDocument item in AdditionalDocumentCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CountAll == 0)
            {
                isExits = false;
            }
            return isExits;
        }
        private void SendVnaccsEDC(bool KhaiBaoSua)
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                #region Kiểm tra Chứng từ đã khai báo trước khi khai chính thức tờ khai
                //if (!CheckVouchers())
                //{
                //    if (this.ShowMessageTQDT("\nThực hiện ý kiến chỉ đạo của Lãnh đạo Tổng cục, Cục CNTT & Thống kê Hải quan sẽ thực hiện nâng cấp chức năng kiểm tra tờ khai qua khu vực giám sát có đính kèm chứng từ điện tử lên Hệ thống hải quan với nội dung như sau:\n\n- Đối với những tờ khai có ngày đăng ký từ ngày 05/06/2018 khi Thông tư số 39/2018/TT-BTC ngày 20/04/2018 có hiệu lực sẽ bổ sung điều kiện kiểm tra chỉ cho phép xác nhận hàng qua khu vực giám sát khi tờ khai có chứng từ điện tử khai lên hệ thống Hải quan (áp dụng đối với tờ khai xuất khẩu và tờ khai nhập khẩu)\n\n- Thời gian thực hiện nâng cấp: từ ngày 17/09/2018.\n\nCho phép và yêu cầu khai chứng từ điện tử ngay sau khi IDA/EDA (ko cần chờ khai IDC/EDC như trước) --> Hải quan sẽ kiểm tra điều kiện xác nhận hàng qua KVGS nếu có khai chứng từ theo CV 1318/CNTT-PTUD. Về các loại chứng từ đính kèm các bạn tham khảo Điều 16 TT39 sửa đổi .\n\nTờ khai chưa khai báo Thành công bất kỳ chứng từ đính kèm nào ! Tờ khai sẽ không được cấp phép qua KVGS .Doanh nghiệp chắc chắn muốn khai báo tờ khai này không !", true) == "Yes")
                //    {
                //    }
                //    else
                //    {
                //        return;
                //    }
                //}
                #endregion

                if (TKMD.MaPhuongThucVT == "2")
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection.Count == 0)
                    {
                        if (this.ShowMessage("Mã hiệu phương thức vận chuyển đã chọn là :  Đường biển (container) nhưng Bạn chưa nhập danh sách Container của tờ khai . Bạn có muốn tiếp tục khai báo?", true) == "No")
                            return;
                    }
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
#if GC_V4
                    if (this.ShowMessage("Bạn có muốn tính toán lại lượng tồn trước khi khai báo ? ", true) == "Yes")
                    {
                        Company.Interface.GC.XuLyHopDongFrm fx = new Company.Interface.GC.XuLyHopDongFrm();
                        fx.StartPosition = FormStartPosition.CenterScreen;

                        //False: không đồng bộ tờ khai chưa thông quan
                        //True: ngược lại
                        fx.isDongBoThongQuan = ShowMessage("Bạn có muốn đồng bộ những tờ khai CHƯA THÔNG QUAN", true) == "Yes";

                        fx.HD = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        fx.ShowDialog(this);
                        if (fx.DialogResult != DialogResult.OK && fx.ExceptionForm != null)
                        {
                            this.ShowMessage("Lỗi cân đối hợp đồng + " + fx.ExceptionForm.Message, false);
                            return;
                        }
                        if (!KiemTraLuongTon())
                            return;
                    }
#endif
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        EDC EDC = VNACCMaperFromObject.EDCMapper(TKMD.SoToKhai, string.Empty);
                        if (EDC == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<EDC>(EDC, TKMD.InputMessageID);
                    }
                    else
                    {
                        EDE EDE = VNACCMaperFromObject.EDEMapper(TKMD.SoToKhai, string.Empty);
                        if (EDE == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<EDE>(EDE, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void GetEDD(bool edd)
        {
            try
            {

                VNACC_TK_Reference f1 = new VNACC_TK_Reference();
                f1.NghiepVu = EnumNghiepVu.EDD;
                if (TKMD.ID != 0)
                {
                    f1.SoToKhai = TKMD.SoToKhai;
                    f1.SoHoaDon = TKMD.SoTiepNhanHD;
                }
                f1.ShowDialog(this);
                if (f1.DialogResult == DialogResult.OK)
                {

                    if (TKMD.ID > 0 && this.ShowMessage("Bạn chắc chắn muốn nhận thông tin tờ khai này ? Tờ khai bạn vừa nhập sẽ bị thay đổi thông tin theo tờ khai bạn nhận về từ HQ", true) != "Yes")
                    {
                        return;
                    }
                    TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                    MessagesSend msg;
                    EDD EDD;

                    EDD = VNACCMaperFromObject.EDDMapper(f1.SoToKhai);
                    if (EDD == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                    msg = MessagesSend.Load<EDD>(EDD, TKMD.InputMessageID);
                    

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        TKMD.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            string maNV = string.Empty;
                            if (f.feedback.ResponseData.header == null)
                            {
                                maNV = f.feedback.header.VungDuTru_1.GetValue().ToString().Substring(0, 7);
                            }
                            //if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            //{
                            CapNhatThongTin(f.feedback.ResponseData, maNV);
                            f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            //}
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void GetIEX(bool iex)
        {
            try
            {

                VNACC_TK_Reference f1 = new VNACC_TK_Reference();
                f1.NghiepVu = iex ? EnumNghiepVu.IEX : EnumNghiepVu.EDB;
                if (TKMD.ID != 0)
                {
                    f1.SoToKhai = TKMD.SoToKhai;
                    f1.SoHoaDon = TKMD.SoTiepNhanHD;
                }
                f1.ShowDialog(this);
                if (f1.DialogResult == DialogResult.OK)
                {

                    if (TKMD.ID > 0 && this.ShowMessage("Bạn chắc chắn muốn nhận thông tin tờ khai này ? Tờ khai bạn vừa nhập sẽ bị thay đổi thông tin theo tờ khai bạn nhận về từ HQ", true) != "Yes")
                    {
                        return;
                    }
                    TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                    MessagesSend msg;
                    IEX IEX;
                    EDB EDB;
                    if (iex)
                    {
                        IEX = VNACCMaperFromObject.IEXMapper(f1.SoToKhai);
                        if (IEX == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<IEX>(IEX, TKMD.InputMessageID);
                    }
                    else
                    {
                        EDB = VNACCMaperFromObject.EDBMapper(f1.SoToKhai, f1.SoHoaDon);
                        if (EDB == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<EDB>(EDB, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        TKMD.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            string maNV = string.Empty;
                            if (f.feedback.ResponseData.header == null)
                            {
                                maNV = f.feedback.header.VungDuTru_1.GetValue().ToString().Substring(0, 7);
                            }
                            //if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            //{
                                CapNhatThongTin(f.feedback.ResponseData, maNV);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            //}
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.ShowMessage(ex.Message, false);
            }
        }
        #endregion

        #region Cập nhật thông tin
       // private void CapNhatThongTin(ReturnMessages msgResult)
        //{
            //ProcessMessages.GetDataResult_TKMD(msgResult,string.Empty);
            //TKMD.InsertUpdateFull();
            //SetToKhai();
            //setCommandStatus();

        //}
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            ProcessMessages.GetDataResult_TKMD(msgResult, MaNV, TKMD);
           // TKMD.CoQuanHaiQuan = GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
            TKMD.InsertUpdateFull();
            SetToKhai();
            setCommandStatus();
        }
        private void TuDongCapNhatThongTin()
        {
            if (TKMD != null && TKMD.SoToKhai > 0 && TKMD.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan like '{0}%' and TrangThai = 'C'", TKMD.SoToKhai.ToString().Substring(0, 11)), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        if (msgPb.TrangThai == EnumTrangThaiXuLyMessage.ChuaXuLy)
                        {
                            ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                            CapNhatThongTin(msgReturn);
                            //msgPb.Delete();
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                            msgPb.InsertUpdate();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }

        }
        #endregion

        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = TKMD.InputMessageID;
            f.SoToKhai = this.TKMD.SoToKhai.ToString().Length > 11 ? this.TKMD.SoToKhai.ToString().Substring(0, 11) : string.Empty;
            f.ShowDialog(this);
        }

        private void setCommandStatus()
        {
            if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdEDC.Visible = cmdEDE.Visible = cmdEDA01.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDA.Visible = cmdEDB.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.False;

            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
            {
                cmdIN.Visible = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdEDA.Visible = cmdEDE.Visible = cmdEDA01.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDC.Visible = cmdEDB.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
            {
                cmdIN.Visible = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEDA.Visible = cmdEDB.Visible = cmdEDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdEDA01.Visible = cmdEDE.Visible = cmdEDD.Visible = Janus.Windows.UI.InheritableBoolean.True; cmdKhaiBaoSua.Commands.Clear();
                this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdEDA01,
                this.cmdEDE,
                this.cmdEDD
                ,});
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDA01.Visible = cmdEDE.Visible = cmdEDD.Visible = cmdEDA.Visible = cmdEDB.Visible = cmdEDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdIEX.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Commands.Clear();
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                cmdKhaiBaoSua.Visible =Janus.Windows.UI.InheritableBoolean.False;
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdEDA.Visible = cmdEDE.Visible = cmdEDA01.Visible = cmdEDD.Visible = cmdIEX.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdEDC.Visible = cmdEDB.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdBSContainer.Visible = cmdBSContainer1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (string.IsNullOrEmpty(TKMD.InputMessageID))
                cmdKetQuaTraVe1.Visible = cmdKetQuaTraVe2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            else
                cmdKetQuaTraVe1.Visible = cmdKetQuaTraVe2.Visible = Janus.Windows.UI.InheritableBoolean.True;

        }

        private bool SetEDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhai.Tag = "ECN"; //Số tờ khai
                txtSoToKhaiDauTien.Tag = "FIC"; //Số tờ khai đầu tiên
                txtSoNhanhToKhai.Tag = "BNO"; //Số nhánh của tờ khai chia nhỏ
                txtTongSoTKChiaNho.Tag = "DNO"; //Tổng số tờ khai chia nhỏ
                txtSoToKhaiTNTX.Tag = "TDN"; //Số tờ khai tạm nhập tái xuất tương ứng
                clcThoiHanTaiNhap.TagName = "RID"; // Thời hạn tạm nhập tái xuất tờ khai
                ctrMaLoaiHinh.TagName = "ECB"; //Mã loại hình
                ctrMaPhanLoaiHH.TagName = "CCC"; //Mã phân loại hàng hóa
                ctrMaPhuongThucVT.TagName = "MTC"; //Mã hiệu phương thức vận chuyển
                clcThoiHanTaiNhapTaiXuat.TagName = "RID"; //Thời hạn tái nhập khẩu
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                ctrNhomXuLyHS.TagName = "CHB"; //Nhóm xử lý hồ sơ
                clcNgayDangKy.TagName = "ECD"; //Ngày khai báo (Dự kiến)
                txtMaDonVi.Tag = "EPC"; //Mã người xuất khẩu
                txtTenDonVi.Tag = "EPN"; //Tên người xuất khẩu
                txtMaBuuChinhDonVi.Tag = "EPP"; //Mã bưu chính
                txtDiaChiDonVi.Tag = "EPA"; //Địa chỉ người xuất khẩu
                txtSoDienThoaiDonVi.Tag = "EPT"; //Số điện thoại người xuất khẩu
                txtMaUyThac.Tag = "EXC"; //Mã người ủy thác xuất khẩu
                txtTenUyThac.Tag = "EXN"; //Tên người ủy thác xuất khẩu
                txtMaDoiTac.Tag = "CGC"; //Mã người nhập khẩu
                txtTenDoiTac.Tag = "CGN"; //Tên người nhập khẩu
                txtMaBuuChinhDoiTac.Tag = "CGP"; //Mã bưu chính
                txtDiaChiDoiTac1.Tag = "CGA"; //Địa chỉ 1(Street and number/P.O.BOX)
                txtDiaChiDoiTac2.Tag = "CAT"; //Địa chỉ 2(Street and number/P.O.BOX)
                txtDiaChiDoiTac3.Tag = "CAC"; //Địa chỉ 3(City name)
                txtDiaChiDoiTac4.Tag = "CAS"; //Địa chỉ 4(Country sub-entity, name)
                ctrMaNuocDoiTac.TagName = "CGK"; //Mã nước(Country, coded)
                txtMaDaiLyHQ.Tag = "ECC"; //Mã đại lý Hải quan
                txtPhanLoaiKhongQDVND.Tag = "CNV";// Phân loại không cần quy đổi VNĐ
                txtSoLuong.Tag = "NO"; //Số lượng
                ctrMaDVTSoLuong.TagName = "NOT"; //Mã đơn vị tính
                txtTrongLuong.Tag = "GW"; //Tổng trọng lượng hàng (Gross)
                ctrMaDVTTrongLuong.TagName = "GWT"; //Mã đơn vị tính trọng lượng (Gross)
                ctrMaDDLuuKho.TagName = "ST"; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                ctrDiaDiemNhanHang.TagCode = "DSC"; //Mã địa điểm nhận hàng cuối cùng
                ctrDiaDiemNhanHang.TagName = "DSN"; //Tên địa điểm nhận hàng cuối cùng
                ctrDiaDiemXepHang.TagCode = "PSC"; //Mã địa điểm xếp hàng
                ctrDiaDiemXepHang.TagName = "PSN"; //Tên địa điểm xếp hàng
                txtMaPTVC.Tag = "VSC"; //Mã phương tiện vận chuyển dự kiến
                txtTenPTVC.Tag = "VSN"; //Tên phương tiện vận chuyển dự kiến
                clcNgayHangDen.TagName = "SYM"; //Ngày hàng đi dự kiến
                txtSoHieuKyHieu.Tag = "MRK"; //Ký hiệu và số hiệu

                txtSoVanDon.Tag = "EKN"; // Số vận đơn
                txtTriGiaTinhThue.Tag = "FKK";// Trị giá tính thuế
                txtTongHeSoPhanBoTG.Tag = "TP"; // Tổng hệ số phân bổ
                ctrPhanLoaiHD.TagName = "IV1"; //Phân loại hình thức hóa đơn
                txtSoTiepNhanHD.Tag = "IV2"; //Số tiếp nhận hóa đơn điện tử
                txtSoHoaDon.Tag = "IV3"; //Số hóa đơn
                clcNgayPhatHanhHD.TagName = "IVD"; //Ngày phát hành
                ctrPhuongThucTT.TagName = "IVP"; //Phương thức thanh toán
                ctrMaDieuKienGiaHD.TagName = "IP2"; //Mã điều kiện giá hóa đơn
                ctrMaTTHoaDon.TagName = "IP3"; //Mã đồng tiền của hóa đơn
                txtTongTriGiaHD.Tag = "IP4"; //Tổng trị giá hóa đơn
                ctrPhanLoaiGiaHD.TagName = "IP1"; //Mã phân loại giá hóa đơn
                ctrNguoiNopThue.TagName = "TPM"; //Người nộp thuế
                ctrMaNHTraThueThay.TagName = "BRC"; //Mã ngân hàng trả thuế thay
                txtNanPhatHanhHM.Tag = "BYA"; //Năm phát hành hạn mức
                txtKyHieuCTHanMuc.Tag = "BCM"; //Kí hiệu chứng từ hạn mức
                txtSoCTHanMuc.Tag = "BCN"; //Số chứng từ hạn mức
                txtKyHieuCTBaoLanh.Tag = "SCM";//Kí hiệu chứng từ bảo lãnh
                ctrMaXDThoiHanNopThue.TagName = "ENC"; //Mã xác định thời hạn nộp thuế
                ctrMaNHBaoLanh.TagName = "SBC"; //Mã ngân hàng bảo lãnh
                txtNamPhatHanhBL.Tag = "RYA"; //Năm phát hành bảo lãnh 
                txtSoCTBaoLanh.Tag = "SCN"; //Số chứng từ bảo lãnh
                clcNgayKhoiHanhVC.TagName = "DPD"; //Ngày khởi hành vận chuyển
                ctrDiaDiemDichVC.TagName = "ARP"; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                clcNgayDen.TagName = "ADT"; //Ngày dự kiến đến
                txtGhiChu.Tag = "NT2"; //Phần ghi chú
                txtSoQuanLyNoiBoDN.Tag = "REF"; //Số quản lý của nội bộ doanh nghiệp


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void ctrMaPhuongThucVT_Leave(object sender, EventArgs e)
        {
            ctrDiaDiemNhanHang.Visible = ctrDiaDiemXepHang.Visible = true;
            txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = false;
            if (ctrMaPhuongThucVT.Code == "4")
                ctrDiaDiemXepHang.CategoryType = ECategory.A620;
            else if (ctrMaPhuongThucVT.Code == "5")
                ctrDiaDiemXepHang.CategoryType = ECategory.A601;
            else if (ctrMaPhuongThucVT.Code == "9")
            {
                // ctrDiaDiemDoHang.CategoryType = ECategory.A016T;
                // ctrDiaDiemDoHang.IsXNKTaiCho = true;
                //ctrDiaDiemDoHang.IsRequimentMa = true;
                // //ctrDiaDiemDoHang.Code = "VNZZZ";
                // ctrDiaDiemXepHang.CategoryType = ECategory.A016T;
                // ctrDiaDiemXepHang.IsXNKTaiCho = true;
                ctrDiaDiemNhanHang.Visible = ctrDiaDiemXepHang.Visible = false;
                txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = true;
            }
            else
                ctrDiaDiemXepHang.CategoryType = ECategory.A016;
            ctrDiaDiemXepHang.Where = "I";
            ctrDiaDiemXepHang.ReLoadData();
            ctrDiaDiemNhanHang.ReLoadData();
           
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhai.MaxLength = 12;
                txtSoToKhaiDauTien.MaxLength = 12;
                txtSoNhanhToKhai.MaxLength = 2;
                txtTongSoTKChiaNho.MaxLength = 2;
                txtSoToKhaiTNTX.MaxLength = 12;
                //txtMaLoaiHinh.MaxLength = 3;
                //txtMaPhanLoaiHH.MaxLength = 1;
                //txtMaPhuongThucVT.MaxLength = 1;
                //txtThoiHanTaiNhapTaiXuat.MaxLength = 8;
                //txtCoQuanHaiQuan.MaxLength = 6;
                //txtNhomXuLyHS.MaxLength = 2;
                //txtNgayDangKy.MaxLength = 8;
                txtMaDonVi.MaxLength = 13;
                txtTenDonVi.MaxLength = 300;
                txtMaBuuChinhDonVi.MaxLength = 7;
                txtDiaChiDonVi.MaxLength = 300;
                txtSoDienThoaiDonVi.MaxLength = 20;
                txtMaUyThac.MaxLength = 13;
                txtTenUyThac.MaxLength = 300;
                txtMaDoiTac.MaxLength = 13;
                txtTenDoiTac.MaxLength = 70;
                txtMaBuuChinhDoiTac.MaxLength = 9;
                txtDiaChiDoiTac1.MaxLength = 35;
                txtDiaChiDoiTac2.MaxLength = 35;
                txtDiaChiDoiTac3.MaxLength = 35;
                txtDiaChiDoiTac4.MaxLength = 35;
                //txtMaNuocDoiTac.MaxLength = 2;
                txtMaDaiLyHQ.MaxLength = 5;
                txtSoVanDon.MaxLength = 35;
                txtSoLuong.MaxLength = 8;
                //txtMaDVTSoLuong.MaxLength = 3;
                txtTrongLuong.MaxLength = 10;
                //txtMaDVTTrongLuong.MaxLength = 3;
                //txtMaDDLuuKho.MaxLength = 7;
                //txtMaDiaDiemDoHang.MaxLength = 5;
                //txtTenDiaDiemDohang.MaxLength = 35;
                //txtMaDiaDiemXepHang.MaxLength = 6;
                //txtTenDiaDiemXepHang.MaxLength = 35;
                txtMaPTVC.MaxLength = 9;
                txtTenPTVC.MaxLength = 35;
                //txtNgayHangDen.MaxLength = 8;
                txtSoHieuKyHieu.MaxLength = 140;
                //txtPhanLoai.MaxLength = 4;
                //txtSoGiayPhep.MaxLength = 20;
                //txtPhanLoaiHD.MaxLength = 1;
                txtSoTiepNhanHD.MaxLength = 12;
                txtSoHoaDon.MaxLength = 35;
                //txtNgayPhatHanhHD.MaxLength = 8;
                //txtPhuongThucTT.MaxLength = 7;
                //txtMaDieuKienGiaHD.MaxLength = 3;
                //txtMaTTHoaDon.MaxLength = 3;
                txtTongTriGiaHD.MaxLength = 20;
                //txtPhanLoaiGiaHD.MaxLength = 1;
                //txtMaTTTriGiaTinhThue.MaxLength = 3;
                txtTriGiaTinhThue.MaxLength = 20;
                txtPhanLoaiKhongQDVND.MaxLength = 1;
                txtTongHeSoPhanBoTG.MaxLength = 20;
                //txtNguoiNopThue.MaxLength = 1;
                //txtMaNHTraThueThay.MaxLength = 11;
                txtNanPhatHanhHM.MaxLength = 4;
                txtKyHieuCTHanMuc.MaxLength = 10;
                txtSoCTHanMuc.MaxLength = 10;
                //txtMaXDThoiHanNopThue.MaxLength = 1;
                //txtMaNHBaoLanh.MaxLength = 11;
                txtNamPhatHanhBL.MaxLength = 4;
                txtSoCTBaoLanh.MaxLength = 10;
                //txtPhanLoai.MaxLength = 3;
                //txtSoDinhKemKhaiBaoDT.MaxLength = 12;
                //txtNgayKhoiHanhVC.MaxLength = 8;
                //txtMaDiaDiem.MaxLength = 7;
                //txtNgayDen.MaxLength = 8;
                //txtNgayKhoiHanh.MaxLength = 8;
                //txtDiaDiemDichVC.MaxLength = 7;
                //txtNgayDen.MaxLength = 8;
                txtGhiChu.MaxLength = 300;
                txtSoQuanLyNoiBoDN.MaxLength = 20;
                //txtMaDiaDiem.MaxLength = 7;
                //txtTenDiaDiem.MaxLength = 70;
                //txtDiaChiDiaDiem.MaxLength = 300;
                //txtSoContainer.MaxLength = 12;
                //txtPhanLoai.MaxLength = 1;
                //txtNgay.MaxLength = 8;
                //txtTen.MaxLength = 120;
                //txtNoiDung.MaxLength = 420;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ctrDiaDiemNhanHang.SetValidate = !isOnlyWarning; ctrDiaDiemNhanHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDiaDiemNhanHang.IsValidate; //"Mã địa điểm nhận hàng cuối cùng");
                ctrMaDieuKienGiaHD.SetValidate = !isOnlyWarning; ctrMaDieuKienGiaHD.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaDieuKienGiaHD.IsValidate; //"Mã điều kiện giá hóa đơn");
                ctrMaDVTSoLuong.SetValidate = !isOnlyWarning; ctrMaDVTSoLuong.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaDVTSoLuong.IsValidate; //"Mã đơn vị tính");
                ctrMaLoaiHinh.SetValidate = !isOnlyWarning; ctrMaLoaiHinh.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaLoaiHinh.IsValidate; //"Mã loại hình");
                ctrMaPhuongThucVT.SetValidate = !isOnlyWarning; ctrMaPhuongThucVT.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaPhuongThucVT.IsValidate; //"Mã hiệu phương thức vận chuyển");

                //isValid &= ValidateControl.ValidateNull(ctrMaTTDonGia, errorProvider, "Mã đồng tiền của đơn giá hóa đơn");
                ctrMaTTHoaDon.SetValidate = !isOnlyWarning; ctrMaTTHoaDon.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaTTHoaDon.IsValidate; //"Mã đồng tiền của hóa đơn");
                //isValid &= ValidateControl.ValidateNull(clcNgayHangDen, errorProvider, "Ngày hàng đi dự kiến");
                ctrPhanLoaiGiaHD.SetValidate = !isOnlyWarning; ctrPhanLoaiGiaHD.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrPhanLoaiGiaHD.IsValidate; //"Mã phân loại giá hóa đơn");
                //isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "Số lượng");
                //isValid &= ValidateControl.ValidateNull(txtTongTriGiaHD, errorProvider, "Tổng trị giá hóa đơn");
                //isValid &= ValidateControl.ValidateNull(txtTriGiaTinhThue, errorProvider, "Trị giá tính thuế");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ShowValidateFormErrorSend(List<ColumnError> columnErrors)
        {

            bool isValid = true;
            foreach (ColumnError item in columnErrors)
            {
                string TagName = item.ErrorColumnCode.ToString();
                switch (TagName)
                {
                    case "ECN":
                        //Số tờ khai
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateZero(txtSoToKhai, errorProvider, "", item.ErrorString);
                        break;
                    case "FIC":
                        //Số tờ khai đầu tiên
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoToKhaiDauTien, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BNO":
                        //Số nhánh của tờ khai chia nhỏ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoNhanhToKhai, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DNO":
                        //Tổng số tờ khai chia nhỏ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongSoTKChiaNho, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TDN":
                        //Số tờ khai tạm nhập tái xuất tương ứng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoToKhaiTNTX, errorProvider, "", false, item.ErrorString);
                        break;
                    case "RID":
                        //Thời hạn tái nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcThoiHanTaiNhapTaiXuat, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ECB":
                        //Mã loại hình
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaLoaiHinh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CCC":
                        //Mã phân loại hàng hóa
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaPhanLoaiHH, errorProvider, "", false, item.ErrorString);
                        break;
                    case "MTC":
                        //Mã hiệu phương thức vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaPhuongThucVT, errorProvider, "", false, item.ErrorString);
                        break;
                    //case "SKB":
                    //    //Phân loại cá nhân/tổ chức
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhanLoaiToChuc, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "CH":
                        //Cơ quan Hải quan
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrCoQuanHaiQuan, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CHB":
                        //Nhóm xử lý hồ sơ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseNhomXuLy(ctrNhomXuLyHS, errorProvider, "", false, item.ErrorString);
                        break;
                    case "RED":
                        //Thời hạn tái xuất
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcThoiHanTaiNhapTaiXuat, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ECD":
                        //Ngày đăng ký tờ khai (Dự kiến)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayDangKy, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPC":
                        //Mã người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPN":
                        //Tên người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPP":
                        //Mã bưu chính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaBuuChinhDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPA":
                        //Địa chỉ người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPT":
                        //Số điện thoại người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoDienThoaiDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EXC":
                        //Mã người ủy thác xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaUyThac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EXN":
                        //Tên người ủy thác xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenUyThac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CGC":
                        //Mã người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CGN":
                        //Tên người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CGP":
                        //Mã bưu chính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaBuuChinhDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CGA":
                        //Địa chỉ 1(Street and number/P.O.BOX)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac1, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CAT":
                        //Địa chỉ 2(Street and number/P.O.BOX)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac2, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CAC":
                        //Địa chỉ 3(City name)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac3, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CAS":
                        //Địa chỉ 4(Country sub-entity, name)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac4, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CGK":
                        //Mã nước(Country, coded)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaNuocDoiTac, errorProvider, "", item.ErrorString);
                        break;
                    //case "ENM":
                    //    //Người ủy thác xuất khẩu
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtUyThacXK, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "ECC":
                        //Mã đại lý Hải quan
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDaiLyHQ, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EKN":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NO":
                        //Số lượng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NOT":
                        //Mã đơn vị tính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDVTSoLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "GW":
                        //Tổng trọng lượng hàng (Gross)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTrongLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "GWT":
                        //Mã đơn vị tính trọng lượng (Gross)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDVTTrongLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ST":
                        //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDDLuuKho, errorProvider, "", false, item.ErrorString);
                        break;
                    case "MRK":
                        //Ký hiệu và số hiệu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoHieuKyHieu, errorProvider, "", false, item.ErrorString);
                        break;
                    case "VSC":
                        //Mã phương tiện vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaPTVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "VSN":
                        //Tên tầu (máy bay) chở hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenPTVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SYM":
                        //Ngày hàng đi dự kiến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayHangDen, errorProvider, "", false, item.ErrorString);
                        break;
                    //case "MRK":
                    //    //Ký hiệu và số hiệu
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoHieuKyHieu, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "DSC":
                        //Mã địa điểm nhận hàng cuối cùng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrDiaDiemNhanHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DSN":
                        //Tên địa điểm dỡ hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDiaDiemDoHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "PSC":
                        //Mã địa điểm xếp hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrDiaDiemXepHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "PSN":
                        //Tên địa điểm xếp hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDiaDiemXepHang, errorProvider, "", false, item.ErrorString);
                        break;
                    //case "COC":
                    //    //Số lượng container
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoLuongCont, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "N4":
                        //Mã kết quả kiểm tra nội dung
                        //isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(txtkt, errorProvider, "", true, item.ErrorString);
                        break;
                    //case "OL_":
                    //    //Mã văn bản pháp quy khác
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy1, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "IV1":
                        //Phân loại hình thức hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhanLoaiHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IV2":
                        //Số tiếp nhận hóa đơn điện tử
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoTiepNhanHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IV3":
                        //Số hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoHoaDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IVD":
                        //Ngày phát hành
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayPhatHanhHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IVP":
                        //Phương thức thanh toán
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhuongThucTT, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP1":
                        //Mã phân loại giá hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhanLoaiGiaHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP2":
                        //Mã điều kiện giá hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDieuKienGiaHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP3":
                        //Mã đồng tiền của hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaTTHoaDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP4":
                        //Tổng trị giá  hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongTriGiaHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "FCD":
                        // Mã đồng tiền của tổng trị giá tính thuế
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaTTTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "FKK":
                        // Tổng trị giá tính thuế
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CNV":
                        // Phân loại không cần quy đổi VND
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtPhanLoaiKhongQDVND, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TP":
                        // Tổng hệ số phân bổ trị giá tính thuế
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongHeSoPhanBoTG, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TPM":
                        //Người nộp thuế 
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrNguoiNopThue, errorProvider, "", false, item.ErrorString);
                        break;
                    //case "BP":
                    //    //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaLyDoDeNghi, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "BRC":
                        //Mã ngân hàng trả thuế thay
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaNHTraThueThay, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BYA":
                        //Năm phát hành hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtNanPhatHanhHM, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BCM":
                        //Kí hiệu chứng từ hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtKyHieuCTHanMuc, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BCN":
                        //Số chứng từ hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoCTHanMuc, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ENC":
                        //Mã xác định thời hạn nộp thuế
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaXDThoiHanNopThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SBC":
                        //Mã ngân hàng bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaNHBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "RYA":
                        //Năm phát hành bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtNamPhatHanhBL, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SCM":
                        //Kí hiệu chứng từ bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtKyHieuCTBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SCN":
                        //Số chứng từ bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoCTBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ARP":
                        //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrDiaDiemDichVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ADT":
                        //Ngày dự kiến đến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayDen, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DPD":
                        // Ngày khởi hành vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayKhoiHanhVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NT2":
                        //Phần ghi chú
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtGhiChu, errorProvider, "", false, item.ErrorString);
                        break;
                    case "REF":
                        //Số quản lý của nội bộ doanh nghiệp
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoQuanLyNoiBoDN, errorProvider, "", false, item.ErrorString);
                        break;


                    // Hàng mậu dịch VNACCS
                    case "CMD":
                        //Mã số hàng hóa
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaSoHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaSoHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "GZC":
                        //Mã quản lý riêng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaQuanLy, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaQuanLy, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TXA":
                        //Thuế suất
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuat, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuat, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TXB":
                        //Mức thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuatTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuatTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TXC":
                        //Mã đơn vị tính thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaDVTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaDVTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TXD":
                        //Mã đồng tiền của mức thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "CMN":
                        //Mô tả hàng hóa
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTenHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTenHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "OR":
                        //Mã nước xuất xứ
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrNuocXuatXu, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrNuocXuatXu, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    //case "ORS":
                    //    //Mã biểu thuế nhập khẩu 
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaBieuThueNK, errorProvider, "", false, item.ErrorString);
                    //    break;
                    //case "KWS":
                    //    //Mã ngoài hạn ngạch
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaHanNgach, errorProvider, "", false, item.ErrorString);
                    //    break;
                    //case "SPD":
                    //    //Mã xác định mức thuế nhập khẩu theo lượng
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaThueNKTheoLuong, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "QN1":
                        //Số lượng (1)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "QT1":
                        //Mã đơn vị tính (1)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "QN2":
                        //Số lượng (2)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "QT2":
                        //Mã đơn vị tính (2)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "BPR":
                        //Trị giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "UPR":
                        //Đơn giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtDonGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtDonGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "UPC":
                        //Mã đồng tiền của đơn giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TSC":
                        //Đơn vị của đơn giá hóa đơn và số lượng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "BPC":
                        //Mã đồng tiền trị giá tính thuế
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "DPR":
                        //Trị giá tính thuế
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    //case "VN1":
                    //    //Số của mục khai khoản điều chỉnh
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC1, errorProvider, item.ErrorString);
                    //    break;
                    case "TDL":
                        //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTTDongHangTKTNTX, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTTDongHangTKTNTX, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "TXN":
                        //Số đăng ký danh mục miễn thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {

                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();

                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDMMienThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TXR":
                        //Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDongDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDongDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "RE":
                        ///Mã miễn / Giảm / Không chịu thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaMienGiamThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaMienGiamThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    case "REG":
                        //Số tiền giảm thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTienMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichXuatForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTienMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.ctrMaTTHoaDon.Code;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            GetLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        
                        break;
                    //case "TX":
                    //    //Mã áp dụng thuế suất / Mức thuế và thu khác
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                    //    break;
                    //case "TR":
                    //    //Mã miễn / Giảm / Không chịu thuế và thu khác
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                    //    break;


                    //Đính kèm điện tử khai báo
                    case "EA1":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA2":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA3":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA4":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA5":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB1":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB2":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB3":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB4":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB5":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    // Giấy phép
                    case "SS1":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SS2":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SS3":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN1":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN2":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN3":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    // Trung chuyển
                    case "ST1":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST2":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST3":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST4":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST5":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD1":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD2":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD3":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD4":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD5":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD1":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD2":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD3":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD4":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD5":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;

                        //Container tờ khai xuất 
                    case "VC1":
                        //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem1, errorProvider, "", false, item.ErrorString);
                            containerForm.TKMD = this.TKMD;
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem1, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VC2":
                        //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem2, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem2, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VC3":
                        //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem3, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem3, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VC4":
                        //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem4, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem4, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VC5":
                        //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem5, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(containerForm.ctrMaDiaDiem5, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VN":
                        //Tên địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(containerForm.txtTenDiaDiem, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(containerForm.txtTenDiaDiem, errorProvider, "", false, item.ErrorString); containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "VA":
                        //Địa chỉ địa điểm xếp hàng lên xe chở hàng (Vanning)
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(containerForm.txtDiaChiDiaDiem, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(containerForm.txtDiaChiDiaDiem, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "C1":
                        //Số container
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "C2":
                        //Số container
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "C3":
                        //Số container
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "C4":
                        //Số container
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;
                    case "C5":
                        //Số container
                        if (!containerForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        else
                        {
                            containerForm.Dispose();
                            containerForm = new VNACC_TK_Container();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(containerForm.grList, errorProvider, "", false, item.ErrorString);
                            containerForm.Cont = TKMD.ContainerTKMD;
                            containerForm.TKMD = this.TKMD;
                            containerForm.Show();
                        }
                        break;

                }
            }
            return isValid;
        }
        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            //txtSoToKhai.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai
            //txtSoToKhaiDauTien.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
            //txtSoNhanhToKhai.TextChanged += new EventHandler(SetTextChanged_Handler); //Số nhánh của tờ khai chia nhỏ
            //txtTongSoTKChiaNho.TextChanged += new EventHandler(SetTextChanged_Handler); //Tổng số tờ khai chia nhỏ
            //txtSoToKhaiTNTX.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai tạm nhập tái xuất tương ứng
            //ctrMaLoaiHinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình
            //ctrMaPhanLoaiHH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại hàng hóa
            //ctrMaPhuongThucVT.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển
            ////clcThoiHanTaiNhapTaiXuat.TextChanged += new EventHandler(SetTextChanged_Handler); //Thời hạn tái nhập khẩu
            //ctrCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler); //Cơ quan Hải quan
            //ctrNhomXuLyHS.TextChanged += new EventHandler(SetTextChanged_Handler); //Nhóm xử lý hồ sơ
            ////clcNgayDangKy.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày khai báo (Dự kiến)
            //txtMaDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người xuất khẩu
            //txtMaBuuChinhDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            //txtSoDienThoaiDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Số điện thoại người xuất khẩu
            //txtMaUyThac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người ủy thác xuất khẩu
            //txtMaDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người nhập khẩu
            //txtTenDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên người nhập khẩu
            //txtMaBuuChinhDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            //txtDiaChiDoiTac1.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 1(Street and number/P.O.BOX)
            //txtDiaChiDoiTac2.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 2(Street and number/P.O.BOX)
            //txtDiaChiDoiTac3.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 3(City name)
            //txtDiaChiDoiTac4.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 4(Country sub-entity, name)
            //ctrMaNuocDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã nước(Country, coded)
            //txtMaDaiLyHQ.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đại lý Hải quan
            //txtSoVanDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số vận đơn (Số B/L, số AWB v.v. …)
            //txtSoLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Số lượng
            //ctrMaDVTSoLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính
            //txtTrongLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Tổng trọng lượng hàng (Gross)
            //ctrMaDVTTrongLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính trọng lượng (Gross)
            //ctrMaDDLuuKho.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            //ctrDiaDiemNhanHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm nhận hàng cuối cùng
            ////txtTenDiaDiemDohang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm nhận hàng cuối cùng
            //ctrDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm xếp hàng
            //txtMaPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phương tiện vận chuyển dự kiến
            //txtTenPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên phương tiện vận chuyển dự kiến
            ////clcNgayHangDen.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày hàng đi dự kiến
            //txtSoHieuKyHieu.TextChanged += new EventHandler(SetTextChanged_Handler); //Ký hiệu và số hiệu

            ////txtPhanLoai.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại giấy phép xuất khẩu
            ////txtSoGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler); //Số của giấy phép

            //ctrPhanLoaiHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại hình thức hóa đơn
            //txtSoTiepNhanHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tiếp nhận hóa đơn điện tử
            //txtSoHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số hóa đơn
            ////clcNgayPhatHanhHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày phát hành
            //ctrPhuongThucTT.TextChanged += new EventHandler(SetTextChanged_Handler); //Phương thức thanh toán
            //ctrMaDieuKienGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã điều kiện giá hóa đơn
            //ctrMaTTHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của hóa đơn
            //txtTongTriGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Tổng trị giá hóa đơn
            //ctrPhanLoaiGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại giá hóa đơn
            //ctrMaTTTriGiaTinhThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của tổng trị giá tính thuế
            //txtTriGiaTinhThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Tổng trị giá tính thuế
            //txtPhanLoaiKhongQDVND.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại không cần quy đổi VND
            //txtTongHeSoPhanBoTG.TextChanged += new EventHandler(SetTextChanged_Handler); //Tổng hệ số phân bổ trị giá tính thuế
            //ctrNguoiNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Người nộp thuế
            //txtMaNHTraThueThay.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng trả thuế thay
            //txtNanPhatHanhHM.TextChanged += new EventHandler(SetTextChanged_Handler); //Năm phát hành hạn mức
            //txtKyHieuCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ hạn mức
            //ctrMaXDThoiHanNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng bảo lãnh
            //txtNamPhatHanhBL.TextChanged += new EventHandler(SetTextChanged_Handler); //Năm phát hành bảo lãnh 
            //txtSoCTBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ bảo lãnh

            ////txtPhanLoai.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại đính kèm khai báo điện tử
            ////txtSoDinhKemKhaiBaoDT.TextChanged += new EventHandler(SetTextChanged_Handler); //Số đính kèm khai báo điện tử
            ////txtNgayKhoiHanhVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày khởi hành vận chuyển
            ////txtMaDiaDiem.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)

            ////clcNgayDen.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày dự kiến đến(Địa điểm trung chuyển)
            ////clcNgayKhoiHanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
            ////ctrDiaDiemDichVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)

            ////clcNgayDen.TextChanged += new EventHandler(SetTextChanged_Handler); //Ngày dự kiến đến
            //txtSoQuanLyNoiBoDN.TextChanged += new EventHandler(SetTextChanged_Handler); //Số quản lý của nội bộ doanh nghiệp

            ////txtMaDiaDiem.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
            ////txtTenDiaDiem.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm xếp hàng lên xe chở hàng (Vanning)
            ////txtSoContainer.TextChanged += new EventHandler(SetTextChanged_Handler); //Số container


            //txtSoToKhai.CharacterCasing = CharacterCasing.Upper; //Số tờ khai
            //txtSoToKhaiDauTien.CharacterCasing = CharacterCasing.Upper; //Số tờ khai đầu tiên
            //txtSoNhanhToKhai.CharacterCasing = CharacterCasing.Upper; //Số nhánh của tờ khai chia nhỏ
            //txtTongSoTKChiaNho.CharacterCasing = CharacterCasing.Upper; //Tổng số tờ khai chia nhỏ
            //txtSoToKhaiTNTX.CharacterCasing = CharacterCasing.Upper; //Số tờ khai tạm nhập tái xuất tương ứng
            //ctrMaLoaiHinh.IsUpperCase = true; //Mã loại hình
            //ctrMaPhanLoaiHH.IsUpperCase = true; //Mã phân loại hàng hóa
            //ctrMaPhuongThucVT.IsUpperCase = true; //Mã hiệu phương thức vận chuyển
            ////txtThoiHanTaiNhapTaiXuat.CharacterCasing = CharacterCasing.Upper; //Thời hạn tái nhập khẩu
            //ctrCoQuanHaiQuan.IsUpperCase = true; //Cơ quan Hải quan
            ////ctrNhomXuLyHS.IsUpperCase = true; //Nhóm xử lý hồ sơ
            ////txtNgayDangKy.CharacterCasing = CharacterCasing.Upper; //Ngày khai báo (Dự kiến)
            //txtMaDonVi.CharacterCasing = CharacterCasing.Upper; //Mã người xuất khẩu
            //txtMaBuuChinhDonVi.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtSoDienThoaiDonVi.CharacterCasing = CharacterCasing.Upper; //Số điện thoại người xuất khẩu
            //txtMaUyThac.CharacterCasing = CharacterCasing.Upper; //Mã người ủy thác xuất khẩu
            //txtMaDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã người nhập khẩu
            //txtTenDoiTac.CharacterCasing = CharacterCasing.Upper; //Tên người nhập khẩu
            //txtMaBuuChinhDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtDiaChiDoiTac1.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 1(Street and number/P.O.BOX)
            //txtDiaChiDoiTac2.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 2(Street and number/P.O.BOX)
            //txtDiaChiDoiTac3.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 3(City name)
            //txtDiaChiDoiTac4.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 4(Country sub-entity, name)
            //ctrMaNuocDoiTac.IsUpperCase = true; //Mã nước(Country, coded)
            //txtMaDaiLyHQ.CharacterCasing = CharacterCasing.Upper; //Mã đại lý Hải quan
            //txtSoVanDon.CharacterCasing = CharacterCasing.Upper; //Số vận đơn (Số B/L, số AWB v.v. …)
            //txtSoLuong.CharacterCasing = CharacterCasing.Upper; //Số lượng
            //ctrMaDVTSoLuong.IsUpperCase = true; //Mã đơn vị tính
            //txtTrongLuong.CharacterCasing = CharacterCasing.Upper; //Tổng trọng lượng hàng (Gross)
            //ctrMaDVTTrongLuong.IsUpperCase = true; //Mã đơn vị tính trọng lượng (Gross)
            //ctrMaDDLuuKho.IsUpperCase = true; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            //ctrDiaDiemNhanHang.IsUpperCase = true; //Mã địa điểm nhận hàng cuối cùng
            ////txtTenDiaDiemDohang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm nhận hàng cuối cùng
            //ctrDiaDiemXepHang.IsUpperCase = true; //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm xếp hàng
            //txtMaPTVC.CharacterCasing = CharacterCasing.Upper; //Mã phương tiện vận chuyển dự kiến
            //txtTenPTVC.CharacterCasing = CharacterCasing.Upper; //Tên phương tiện vận chuyển dự kiến
            ////txtNgayHangDen.CharacterCasing = CharacterCasing.Upper; //Ngày hàng đi dự kiến
            //txtSoHieuKyHieu.CharacterCasing = CharacterCasing.Upper; //Ký hiệu và số hiệu
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại giấy phép xuất khẩu
            ////txtSoGiayPhep.CharacterCasing = CharacterCasing.Upper; //Số của giấy phép
            ////txtPhanLoaiHD.CharacterCasing = CharacterCasing.Upper; //Phân loại hình thức hóa đơn
            //txtSoTiepNhanHD.CharacterCasing = CharacterCasing.Upper; //Số tiếp nhận hóa đơn điện tử
            //txtSoHoaDon.CharacterCasing = CharacterCasing.Upper; //Số hóa đơn
            ////txtNgayPhatHanhHD.CharacterCasing = CharacterCasing.Upper; //Ngày phát hành
            //ctrPhuongThucTT.IsUpperCase = true; //Phương thức thanh toán
            //ctrMaDieuKienGiaHD.IsUpperCase = true; //Mã điều kiện giá hóa đơn
            //ctrMaTTHoaDon.IsUpperCase = true; //Mã đồng tiền của hóa đơn
            //txtTongTriGiaHD.CharacterCasing = CharacterCasing.Upper; //Tổng trị giá hóa đơn
            //ctrPhanLoaiGiaHD.IsUpperCase = true; //Mã phân loại giá hóa đơn
            //ctrMaTTTriGiaTinhThue.IsUpperCase = true; //Mã đồng tiền của tổng trị giá tính thuế
            //txtTriGiaTinhThue.CharacterCasing = CharacterCasing.Upper; //Tổng trị giá tính thuế
            //txtPhanLoaiKhongQDVND.CharacterCasing = CharacterCasing.Upper; //Phân loại không cần quy đổi VND
            //txtTongHeSoPhanBoTG.CharacterCasing = CharacterCasing.Upper; //Tổng hệ số phân bổ trị giá tính thuế
            //ctrNguoiNopThue.IsUpperCase = true; //Người nộp thuế
            //txtMaNHTraThueThay.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng trả thuế thay
            //txtNanPhatHanhHM.CharacterCasing = CharacterCasing.Upper; //Năm phát hành hạn mức
            //txtKyHieuCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Số chứng từ hạn mức
            //ctrMaXDThoiHanNopThue.IsUpperCase = true; //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng bảo lãnh
            //txtNamPhatHanhBL.CharacterCasing = CharacterCasing.Upper; //Năm phát hành bảo lãnh 
            //txtSoCTBaoLanh.CharacterCasing = CharacterCasing.Upper; //Số chứng từ bảo lãnh
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại đính kèm khai báo điện tử
            ////txtSoDinhKemKhaiBaoDT.CharacterCasing = CharacterCasing.Upper; //Số đính kèm khai báo điện tử
            ////txtNgayKhoiHanhVC.CharacterCasing = CharacterCasing.Upper; //Ngày khởi hành vận chuyển
            ////txtMaDiaDiem.CharacterCasing = CharacterCasing.Upper; //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
            ////txtNgayDen.CharacterCasing = CharacterCasing.Upper; //Ngày dự kiến đến(Địa điểm trung chuyển)
            ////txtNgayKhoiHanh.CharacterCasing = CharacterCasing.Upper; //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
            //ctrDiaDiemDichVC.IsUpperCase = true; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            ////txtNgayDen.CharacterCasing = CharacterCasing.Upper; //Ngày dự kiến đến
            //txtSoQuanLyNoiBoDN.CharacterCasing = CharacterCasing.Upper; //Số quản lý của nội bộ doanh nghiệp
            ////txtMaDiaDiem.CharacterCasing = CharacterCasing.Upper; //Mã địa điểm xếp hàng lên xe chở hàng (Vanning)
            ////txtTenDiaDiem.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm xếp hàng lên xe chở hàng (Vanning)
            ////txtSoContainer.CharacterCasing = CharacterCasing.Upper; //Số container
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại chỉ thị của Hải quan
            ////txtNgay.CharacterCasing = CharacterCasing.Upper; //Ngày chỉ thị của Hải quan

        }

        private void ctrCoQuanHaiQuan_Leave(object sender, EventArgs e)
        {
            ctrNhomXuLyHS.CustomsCode = ctrCoQuanHaiQuan.Code;
            ctrNhomXuLyHS.ReLoadData();
            if (TKMD != null && !string.IsNullOrEmpty(TKMD.NhomXuLyHS))
                ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            //if(ctrCoQuanHaiQuan.Code=="33PD")
            //    ctrNhomXuLyHS.Code="00";
        }

        private void txtTenDoiTac_ButtonClick(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    txtMaDoiTac.Text = string.IsNullOrEmpty(f.doiTac.MaCongTy) ? "" : f.doiTac.MaCongTy.Trim().ToUpper();
                    txtTenDoiTac.Text = string.IsNullOrEmpty(f.doiTac.TenCongTy) ? "" : f.doiTac.TenCongTy.Trim().ToUpper();
                    txtDiaChiDoiTac1.Text = string.IsNullOrEmpty(f.doiTac.DiaChi) ? "" : f.doiTac.DiaChi.Trim().ToUpper();
                    txtDiaChiDoiTac2.Text = string.IsNullOrEmpty(f.doiTac.DiaChi2) ? "" : f.doiTac.DiaChi2.ToUpper();
                    txtMaBuuChinhDoiTac.Text = string.IsNullOrEmpty(f.doiTac.BuuChinh) ? "" : f.doiTac.BuuChinh.Trim().ToUpper();
                    txtDiaChiDoiTac3.Text = string.IsNullOrEmpty(f.doiTac.TinhThanh) ? "" : f.doiTac.TinhThanh.Trim().ToUpper();
                    txtDiaChiDoiTac4.Text = string.IsNullOrEmpty(f.doiTac.QuocGia) ? "" : f.doiTac.QuocGia.Trim().ToUpper();
                    ctrMaNuocDoiTac.Code = string.IsNullOrEmpty(f.doiTac.MaNuoc) ? "" : f.doiTac.MaNuoc.Trim().ToUpper();
                    txtMaDaiLyHQ.Text = string.IsNullOrEmpty(f.doiTac.MaDaiLyHQ) ? "" : f.doiTac.MaDaiLyHQ.Trim().ToUpper();
                    txtTenDaiLyHaiQuan.Text = string.IsNullOrEmpty(f.doiTac.TenDaiLyHQ) ? "" : f.doiTac.TenDaiLyHQ.Trim().ToUpper();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void txt_Leave(object sender, EventArgs e)
        {
            Control ctr = (Control)sender;
            ctr.Text = ctr.Text.ToString().Trim().ToUpper();

        }

        private void InToKhaiTam()
        {
            MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKMD.InputMessageID, "EDA");
            if (msg != null && msg.ID > 0)
                ProcessReport.ShowReport(msg.ID, "VAE1LD0", new ResponseVNACCS(msg.Log_Messages).ResponseData);
            else
                ShowMessage("Không tìm thấy bản khai tạm", false);
        }

        private void txtSoHD_ButtonClick(object sender, EventArgs e)
        {
#if GC_V4
            if (TKMD.HangCollection.Count == 0)
            {
                Company.Interface.KDT.GC.HopDongManageForm f = new Company.Interface.KDT.GC.HopDongManageForm();
                f.IsBrowseForm = true;
                f.IsDaDuyet = true;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
                {
                    TKMD.HopDong_ID = f.HopDongSelected.ID;
                    string sohopdong = string.Empty;
                    if (f.HopDongSelected.ActionStatus == 999)
                        sohopdong = "1";
                    else
                        sohopdong = "2";
                    if (f.HopDongSelected.MaHaiQuan != "" && f.HopDongSelected.MaHaiQuan.Length >= 4)
                        sohopdong = sohopdong + f.HopDongSelected.MaHaiQuan.Substring(1, 2);
                    if (f.HopDongSelected.NgayDangKy.Year > 1900)
                        sohopdong = sohopdong + f.HopDongSelected.NgayDangKy.Year.ToString().Trim();
                    if (f.HopDongSelected.SoTiepNhan != 0)
                        sohopdong = sohopdong + f.HopDongSelected.SoTiepNhan.ToString().Trim();
                    TKMD.HopDong_So = sohopdong;
                    txtSoHD.Text = f.HopDongSelected.SoHopDong;
                    lblNgayHD1.Text = f.HopDongSelected.NgayKy.ToString("dd/MM/yyyy");
                }
            }
            else
            {
            ShowMessage("Xóa thông tin hàng trước khi thay đổi hợp đồng", false);
            }
#endif
        }
        private void ShowKetQuaXuLy()
        {
            VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKMD.ID, "TKMD");
            f.ShowDialog(this);
        }
        private long IDTKTamNhapTaiXuat = 0;
        private void txtSoToKhaiTNTX_ButtonClick(object sender, EventArgs e)
        {
            VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
            f.IsShowChonToKhai = true;
            f.ShowDialog(this);
            if (f.DialogResult == DialogResult.OK)
            {
                IDTKTamNhapTaiXuat = f.TKMDDuocChon.ID;
                txtSoToKhaiTNTX.Text = f.TKMDDuocChon.SoToKhai.ToString();
            }
        }

        private void txtSoToKhaiTNTX_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSoToKhaiTNTX.Text) && txtSoToKhaiTNTX.Text != "0")
            {
                List<KDT_VNACC_ToKhaiMauDich> listtkmd = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoToKhai = " + txtSoToKhaiTNTX.Text, null);
                if (listtkmd.Count == 1)
                {
                    IDTKTamNhapTaiXuat = listtkmd[0].ID;
                }

            }
        }
        private bool checkMaHang(string MaHang, string loai)
        {
            bool ketqua = true;
            try
            {


                switch (loai)
                {
#if SXXK_V4
                    case "N":
                        Company.BLL.SXXK.NguyenPhuLieu Npl = new Company.BLL.SXXK.NguyenPhuLieu();
                        Npl.Ma = MaHang;
                        Npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        Npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        ketqua = Npl.Load();
                        
                        break;
                    case "S":
                        ketqua = Company.BLL.SXXK.DinhMuc.checkIsExist(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, MaHang);
                        break;
#elif GC_V4
                case "N":

                    Company.GC.BLL.GC.NguyenPhuLieu nplGC = new Company.GC.BLL.GC.NguyenPhuLieu();
                    nplGC.HopDong_ID = TKMD.HopDong_ID;
                    nplGC.Ma = MaHang;
                    ketqua = nplGC.Load();

                    break;
                case "S":
                    Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                    dinhmuc.MaSanPham = MaHang;
                    dinhmuc.HopDong_ID = TKMD.HopDong_ID;
                    ketqua= dinhmuc.CheckExitsDinhMucSanPham();

                    //Company.GC.BLL.GC.SanPham spGC = new Company.GC.BLL.GC.SanPham();
                    //spGC.HopDong_ID = TKMD.HopDong_ID;
                    //spGC.Ma = MaHang;
                    //ketqua = spGC.Load();

                    break;
                case "T":
                    Company.GC.BLL.GC.ThietBi tbGC = new Company.GC.BLL.GC.ThietBi();
                    tbGC.HopDong_ID = TKMD.HopDong_ID;
                    tbGC.Ma = MaHang;
                    ketqua = tbGC.Load();
                    break;
#endif
                    case "":
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.ShowMessage(ex.Message, false);

            }
            return ketqua;
        }

        private void btnMaLoaiHinh_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "MaLoaiHinh";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrMaLoaiHinh.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void btnMaHQ_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "MaHQ";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrCoQuanHaiQuan.Code = f.mapper.CodeV5.Trim();
            }
        }
#if GC_V4
        private void ShowCanDoiNhapXuat()
        {
            var f = new Company.Interface.KDT.GC.CanDoiNhapXuatForm { tableCanDoi = GetTableCanDoi() , HopDongID = TKMD.HopDong_ID};
            if (f.tableCanDoi == null)
                return;
            f.ShowDialog();
        }


        private DataTable GetTableCanDoi()
        {
            DataTable tableResult = new DataTable();
            tableResult.Columns.Add("MaSP");
            tableResult.Columns.Add("MaNPL");
            tableResult.Columns.Add("DinhMucChung", typeof(decimal));
            tableResult.Columns.Add("LuongTonDau", typeof(decimal));
            tableResult.Columns.Add("LuongSuDung", typeof(decimal));
            tableResult.Columns.Add("LuongTonCuoi", typeof(decimal));

            if (rdbSanPham.Checked)
            {
                DataSet dsLuongTon = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.HopDong_ID);
                foreach (KDT_VNACC_HangMauDich  hangMd in TKMD.HangCollection)
                {
                    if (hangMd.ID > 0)
                    {
                        DataTable dtLuongNPLTheoDM = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaHangHoa, hangMd.SoLuong1, TKMD.HopDong_ID).Tables[0];
                        foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                TKMD.HangCollection.Sort(
                    delegate(KDT_VNACC_HangMauDich hang1, KDT_VNACC_HangMauDich hang2)
                    {
                        return hang1.MaHangHoa.CompareTo(hang2.MaHangHoa);
                    }
                    );
                foreach (KDT_VNACC_HangMauDich hangMD in TKMD.HangCollection)
                {
                    DataTable dtLuongNPLTheoDM = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMD.MaHangHoa, hangMD.SoLuong1, TKMD.HopDong_ID).Tables[0];
                    foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                    {
                        try
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];
                            if (rowTon != null)
                            {
                                DataRow rowNew = tableResult.NewRow();
                                rowNew["MaSP"] = hangMD.MaHangHoa.Trim();
                                rowNew["MaNPL"] = row["MaNguyenPhuLieu"].ToString().Trim();
                                rowNew["DinhMucChung"] = row["dinhMucChung"];
                                rowNew["LuongTonDau"] = rowTon["LuongTon"];
                                rowNew["LuongSuDung"] = row["LuongCanDung"];
                                rowNew["LuongTonCuoi"] = Convert.ToDecimal(rowTon["LuongTon"]) - Convert.ToDecimal(row["LuongCanDung"]);
                                rowTon["LuongTon"] = rowNew["LuongTonCuoi"];
                                tableResult.Rows.Add(rowNew);
                            }
                        }
                        catch
                        {
                            //MLMessages("Nguyên phụ liệu có mã : " + row["MaNguyenPhuLieu"] + " không có trong hệ thống.Hãy cập nhật lại mã này.", "MSG_240247", row["MaNguyenPhuLieu"].ToString(), false);
                            //showMsg("MSG_240247", row["MaNguyenPhuLieu"].ToString());
                            Globals.ShowMessageTQDT(row["MaNguyenPhuLieu"].ToString(), false);
                            return null;
                        }
                    }
                }
            }
            return tableResult;

        }
#endif
        private void ReloadData()
        {
            try
            {
                this.UseWaitCursor = true;
                MsgPhanBo.UpdateMsgPhanBo(this.TKMD.SoToKhai.ToString());
                TuDongCapNhatThongTin();
                this.UseWaitCursor = false;
                ShowMessage("Cập nhật thành công tờ khai", false);
            }
            catch (System.Exception ex)
            {
                ShowMessage("Lỗi khi cập nhật tờ khai " + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                //System.Diagnostics.Process.Start("http://pus.customs.gov.vn/faces/Home");
                CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
                f.ShowDialog(this);
                txtSoVanDon.Text = f.capSoDinhDanh.SoDinhDanh;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

#if GC_V4
        private bool KiemTraLuongTon()
        {
            if (rdbSanPham.Checked)
            {
                DataSet dsLuongTon = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.HopDong_ID);
                //foreach (KDT_VNACC_HangMauDich hangMd in TKMD.HangCollection)
                //{
                //    if (hangMd.ID > 0)
                //    {
                //        DataTable dtLuongNplTheoDm = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaHangHoa, hangMd.SoLuong1, TKMD.HopDong_ID).Tables[0];
                //        foreach (DataRow row in dtLuongNplTheoDm.Rows)
                //        {
                //            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                //            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                //        }
                //    }
                //}
                //LanNT Sort by maphu
                //TKMD.HangCollection.Sort(
                //    delegate(KDT_VNACC_HangMauDich hang1, KDT_VNACC_HangMauDich hang2)
                //    {
                //        return hang1.MaHangHoa.CompareTo(hang2.MaHangHoa);
                //    }
                //    );
                string tenHangBiAm = "";
                Decimal LuongTon;
                Decimal LuongCanDung;
                Decimal LuongAm;
                int i = 1;
                foreach (KDT_VNACC_HangMauDich hangMd in TKMD.HangCollection)
                {
                    DataSet dsLuongNplTheoDm = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaHangHoa, hangMd.SoLuong1, TKMD.HopDong_ID);
                    foreach (DataRow rowNplTon in dsLuongTon.Tables[0].Rows)
                    {
                        bool ok = false;
                        foreach (DataRow rowNplTheoDm in dsLuongNplTheoDm.Tables[0].Rows)
                        {
                            if (rowNplTon["MaNguyenPhuLieu"].ToString().Trim() == rowNplTheoDm["MaNguyenPhuLieu"].ToString().Trim())
                            {
                                LuongTon = Convert.ToDecimal(rowNplTon["LuongTon"]);
                                LuongCanDung = Convert.ToDecimal(rowNplTheoDm["LuongCanDung"]);
                                rowNplTon["LuongTon"] = Convert.ToDecimal(rowNplTon["LuongTon"]) - Convert.ToDecimal(rowNplTheoDm["LuongCanDung"]);
                                LuongAm = LuongTon - LuongCanDung;
                                if (Convert.ToDecimal(rowNplTon["LuongTon"]) < 0)
                                {
                                    if (tenHangBiAm == "")
                                    {
                                        tenHangBiAm = "[STT]-[MÃ SP]-[MÃ NPL]-[LƯỢNG TỒN]-[LƯỢNG SD]-[LƯỢNG ÂM]\n\n";
                                        tenHangBiAm += "[" + i + "] - [" + hangMd.MaHangHoa + "]-[" + rowNplTon["MaNguyenPhuLieu"].ToString() + "]-[" + Decimal.Round(LuongTon, 4) + "]-[" + Decimal.Round(LuongCanDung, 4) + "]-[" + Decimal.Round(LuongAm, 4) + "]\n";
                                    }
                                    else
                                        tenHangBiAm += "[" + i + "] - [" + hangMd.MaHangHoa + "]-[" + rowNplTon["MaNguyenPhuLieu"].ToString() + "]-[" + Decimal.Round(LuongTon, 4) + "]-[" + Decimal.Round(LuongCanDung, 4) + "]-[" + Decimal.Round(LuongAm, 4) + "]\n";
                                    ok = true;
                                    i++;
                                }
                            }
                        }
                        if (ok)
                            break;
                    }
                }
                if (tenHangBiAm != "")
                {
                    if (Globals.ShowMessageTQDT(" Cảnh báo lượng tồn không đủ để sản xuất hàng hóa ","Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau :\r\n " + tenHangBiAm + "\r\n \nDoanh nghiệp có muốn lưu không?\n", true) != "Yes")
                    {
                        return false;
                    }
                    return true;
                }
            }
            else if (rdbNPL.Checked) 
            {
                {
                    Decimal LuongTon;
                    Decimal LuongCanDung;
                    Decimal LuongAm;
                    DataSet dsLuongTon = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.HopDong_ID);
                    string tenHangBiAm = "";
                    int i = 0;
                    foreach (KDT_VNACC_HangMauDich hangMd in TKMD.HangCollection)
                    {
                        
                        //DataSet dsLuongNplTheoDm = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaHangHoa, hangMd.SoLuong1, TKMD.HopDong_ID);
                        foreach (DataRow rowNplTon in dsLuongTon.Tables[0].Rows)
                        {
                            bool ok = false;
                            if (hangMd.MaHangHoa.ToUpper() == rowNplTon["MaNguyenPhuLieu"].ToString().ToUpper())
                            {
                                LuongTon = Convert.ToDecimal(rowNplTon["LuongTon"]);
                                LuongCanDung = hangMd.SoLuong1;
                                rowNplTon["LuongTon"] = Convert.ToDecimal(rowNplTon["LuongTon"]) - hangMd.SoLuong1;
                                LuongAm = LuongTon - LuongCanDung;
                                if (Convert.ToDecimal(rowNplTon["LuongTon"].ToString()) < 0)
                                {
                                    if (tenHangBiAm == "")
                                    {
                                        tenHangBiAm = "[STT]-[MÃ NPL]-[LƯỢNG TỒN]-[LƯỢNG XUẤT]-[LƯỢNG ÂM]\n\n";
                                        tenHangBiAm += "[" + i + "] - [" + hangMd.MaHangHoa + "]-[" + Decimal.Round(LuongTon, 4) + "]-[" + Decimal.Round(LuongCanDung, 4) + "]-[" + Decimal.Round(LuongAm, 4) + "]\n";
                                    }
                                    else
                                        tenHangBiAm += "[" + i + "] - [" + hangMd.MaHangHoa + "]-[" + Decimal.Round(LuongTon, 4) + "]-[" + Decimal.Round(LuongCanDung, 4) + "]-[" + Decimal.Round(LuongAm, 4) + "]\n";
                                }
                                i++;
                                ok = true;
                                break;
                            }
                            
                            
                        }
                    }
                    if (tenHangBiAm != "")
                    {
                        if (Globals.ShowMessageTQDT(" Cảnh báo lượng tồn không đủ để xuất NPL ", "Nguyên phụ liệu tồn trong hệ thống không đủ  để xuất :\r\n " + tenHangBiAm + "\r\n \nDoanh nghiệp có muốn lưu không?\n", true) != "Yes")
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return true;

        }
#endif
        private void linkPrint_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ReportViewDinhDanhHangHoaToKhai f = new ReportViewDinhDanhHangHoaToKhai();
            //ReportViewDinhDanhHangHoa f = new ReportViewDinhDanhHangHoa();
            f.TKMD = TKMD;
            f.BindReport();
            f.ShowPreview();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SoThamChieuNoiBoForm f = new SoThamChieuNoiBoForm();
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void txtSoVanDon_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                DinhDanhHangHoaManagerForm f = new DinhDanhHangHoaManagerForm();
                f.isBrower = true;
                f.ShowDialog(this);
                if (f.capSoDinhDanh != null)
                {
                    txtSoVanDon.Text = f.capSoDinhDanh.SoDinhDanh;
                }
                VNACCS_BranchDetailRegistedForm form = new VNACCS_BranchDetailRegistedForm();
                form.ShowDialog(this);
                if (form.branchDetail.SoVanDon != null)
                {
                    txtSoVanDon.Text = form.branchDetail.SoVanDon.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void VNACC_ToKhaiMauDichXuatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Tờ khai Nhập khẩu  có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.saveToKhai();
                    IsChange = false;
                    this.Close();
                }
            }
        }
    }
}
