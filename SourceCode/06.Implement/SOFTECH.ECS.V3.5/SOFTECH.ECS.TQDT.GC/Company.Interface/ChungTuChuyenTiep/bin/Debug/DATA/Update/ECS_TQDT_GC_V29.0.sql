GO
 IF OBJECT_ID(N'[dbo].[t_KDT_VNACC_ContainerDinhKem]') IS NOT NULL
	DROP TABLE [dbo].[t_KDT_VNACC_ContainerDinhKem]
GO
CREATE TABLE [dbo].[t_KDT_VNACC_ContainerDinhKem]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[ChungTuDinhKem_ID] [bigint]  NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_VNACC_ChungTuDinhKem(ID),
[SoVanDon] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoContainer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoSeal] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteBy_ChungTuDinhKem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteBy_ChungTuDinhKem_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Insert]
	@ChungTuDinhKem_ID bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ContainerDinhKem]
(
	[ChungTuDinhKem_ID],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu]
)
VALUES 
(
	@ChungTuDinhKem_ID,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Update]
	@ID bigint,
	@ChungTuDinhKem_ID bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ContainerDinhKem]
SET
	[ChungTuDinhKem_ID] = @ChungTuDinhKem_ID,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_InsertUpdate]
	@ID bigint,
	@ChungTuDinhKem_ID bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ContainerDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ContainerDinhKem] 
		SET
			[ChungTuDinhKem_ID] = @ChungTuDinhKem_ID,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ContainerDinhKem]
		(
			[ChungTuDinhKem_ID],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[GhiChu]
		)
		VALUES 
		(
			@ChungTuDinhKem_ID,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ContainerDinhKem]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteBy_ChungTuDinhKem_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteBy_ChungTuDinhKem_ID]
	@ChungTuDinhKem_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ContainerDinhKem]
WHERE
	[ChungTuDinhKem_ID] = @ChungTuDinhKem_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ContainerDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuDinhKem_ID],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_ContainerDinhKem]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectBy_ChungTuDinhKem_ID]
	@ChungTuDinhKem_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuDinhKem_ID],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_ContainerDinhKem]
WHERE
	[ChungTuDinhKem_ID] = @ChungTuDinhKem_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuDinhKem_ID],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu]
FROM [dbo].[t_KDT_VNACC_ContainerDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ContainerDinhKem_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuDinhKem_ID],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_ContainerDinhKem]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '29.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('29.0',GETDATE(), N'CẬP NHẬT DANH SÁCH CONTAINER ĐÍNH KÈM')
END