﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ToolCreateAccount
{
    public partial class CreateHQ : Form
    {
        public bool isCreate = false;

        public CreateHQ()
        {
            InitializeComponent();
        }

        private void CreateHQ_Load(object sender, EventArgs e)
        {
            txtSXXK.Text = "Server=?;Database=?;Uid=?;Pwd=?";
            txtSLXNK.Text = "Server=?;Database=?;Uid=?;Pwd=?";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            string path = Properties.Settings.Default.PathFileDatabase;
            if (path == "")
            {
                DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ database chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (rel == DialogResult.Yes)
                {
                    DialogResult ok = openFileDialog1.ShowDialog();
                    if (ok == DialogResult.OK)
                    {
                        path = openFileDialog1.FileName;
                        DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (luuTHongTin == DialogResult.Yes)
                        {
                            Properties.Settings.Default.PathFileDatabase = path;
                            Properties.Settings.Default.Save();
                        }
                    }
                }
                else
                    return;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
                return;
            }
            XmlNode node = doc.SelectSingleNode("Root/ChiCuc[@MaChiCuc=\"" + txtMaHQ.Text.Trim() + "\"]");
            if (node != null)
            {
                MessageBox.Show("Đã có mã hải quan này rồi.");
                return;
            }
            XmlElement newHQ = doc.CreateElement("ChiCuc");
            XmlAttribute attMaChiCuc = doc.CreateAttribute("MaChiCuc");
            attMaChiCuc.Value = txtMaHQ.Text.Trim();
            XmlElement newSLXNK = doc.CreateElement("SLXNK");
            newSLXNK.InnerText = txtSLXNK.Text.Trim();
            XmlElement newSXXK = doc.CreateElement("SXXK");
            newSXXK.InnerText = txtSXXK.Text.Trim();

            newHQ.Attributes.Append(attMaChiCuc);
            newHQ.AppendChild(newSLXNK);
            newHQ.AppendChild(newSXXK);

            XmlNode root = doc.SelectSingleNode("Root");
            root.AppendChild(newHQ);

            doc.Save(path);
            isCreate = true;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}