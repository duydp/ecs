using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
//using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;

namespace Company.Interface.Report.GC
{
    public partial class BangKe03H_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private int STT = 0;
        
        public BangKe03H_HQGC()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP;
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            DataTable  dtToKhai = new Company.GC.BLL.KDT.ToKhaiMauDich().GetToKhaiXuatThietBiInfo(this.HD.ID).Tables[0];
            dtToKhai.TableName = "BangKe03H_HQGC"; 
            this.DataSource = dtToKhai ;
            //lblSoToKhai.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoToKhai","{0:N0}");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".NgayDangKy", "{0:dd/MM/yyyy}"); 
            lblMaHang.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".MaHang" );
            lblTenHang.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".TenHang");
            lblLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            lblTongLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            lblDVT.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".DVT");
            lblSumLuong.DataBindings.Add("Text", this.DataSource, dtToKhai.TableName + ".SoLuong", "{0:N2}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;
            
        }

        private void BangKe01H_HQGC_AfterPrint(object sender, EventArgs e)
        {

        }

        private void BangKe01H_HQGC_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT1.Text = this.STT + "";
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
        }

        private void lblSoToKhai1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblSoToKhai1.Text = GetCurrentColumnValue("SoToKhai").ToString() + "/" + GetCurrentColumnValue("MaLoaiHinh");
            }
            catch { }
        }

        private void lblSoToKhai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblSoToKhai.Text = GetCurrentColumnValue("SoToKhai").ToString() + "/" + GetCurrentColumnValue("MaLoaiHinh");
            }
            catch { }
        }
        
    }
}
