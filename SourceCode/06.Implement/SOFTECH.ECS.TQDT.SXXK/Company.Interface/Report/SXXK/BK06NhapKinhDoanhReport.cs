using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using Company.BLL.DuLieuChuan;
using Company.BLL.SXXK;

namespace Company.Interface.Report.SXXK
{
    public partial class BK06NhapKinhDoanhReport : DevExpress.XtraReports.UI.XtraReport
    {
        public Company.BLL.KDT.SXXK.HoSoThanhLyDangKy HSTL;
        private int STT = 0;
        public BK06NhapKinhDoanhReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection;
            lblTenDoanhNghiep.Text =GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (this.HSTL.SoHoSo > 0)
                lblSHSTK.Text = this.HSTL.SoHoSo + "";
            ////lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource,"NgayDangKy","{0:dd/MM/yyyy}");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, "NgayDangKyXuat", "{0:dd/MM/yyyy}");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNPL");
            lblMaSP.DataBindings.Add("Text", this.DataSource, "MaSP");
            lblMaHaiQuan.DataBindings.Add("Text", this.DataSource, "MaHaiQuanXuat");
            lblLuongNPL.DataBindings.Add("Text", this.DataSource, "LuongSuDung");
            xrLabel77.Text = GlobalSettings.TieudeNgay;

        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblToKhaiNhap.Text = GetCurrentColumnValue("SoToKhai").ToString() + "/" + LoaiHinhMauDich.GetTenVietTac(GetCurrentColumnValue("MaLoaiHinh").ToString());
        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.Ma = GetCurrentColumnValue("MaNPL").ToString();
            npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            npl.Load();
            lblTenNPL.Text = npl.Ten;
        }

        private void lblToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblToKhaiXuat.Text = GetCurrentColumnValue("SoToKhaiXuat").ToString() + "/" + LoaiHinhMauDich.GetTenVietTac(GetCurrentColumnValue("MaLoaiHinhXuat").ToString());
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.Ma = GetCurrentColumnValue("MaNPL").ToString();
            npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            npl.Load();
            lblDVT.Text = DonViTinh.GetName(npl.DVT_ID);
        }

        
    }
}
