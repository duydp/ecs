﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace Company.KDT.SHARE.Components
{

    public class Config
    {
        [XmlElement("Key")]
        public string Key { get; set; }
        [XmlElement("Value")]
        public string Value { get; set; }
        [XmlElement("Note")]
        public string Note { get; set; }

    }

    /// <summary>
    /// Nạp cấu hình 
    /// </summary>
    [XmlRoot("Configs")]
    public class DoanhNghiepCfgs : List<Config>
    {
        static DoanhNghiepCfgs _install;
        public static DoanhNghiepCfgs Install
        {
            get
            {
                if (_install == null) Load();
                return _install;
            }
        }
        public static DoanhNghiepCfgs Load()
        {
            return Load(AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
        }
        public static DoanhNghiepCfgs Load(string file)
        {
            if (!System.IO.File.Exists(file)) throw new NotSupportedException("Tệp tin cấu hình doanh nghiệp không tồn tại");
            XmlSerializer xs = new XmlSerializer(typeof(DoanhNghiepCfgs));
            StreamReader sr = new StreamReader(file);
            _install = xs.Deserialize(sr) as DoanhNghiepCfgs;
            sr.Close();

            return _install;
        }
        public void SaveConfig(string file)
        {
            if (_install == null) throw new Exception("Chưa nạp dữ liệu");
            XmlSerializer xs = new XmlSerializer(typeof(DoanhNghiepCfgs));
            StreamWriter sw = new StreamWriter(file);
            xs.Serialize(sw, _install);
            sw.Close();
        }
    }
}
