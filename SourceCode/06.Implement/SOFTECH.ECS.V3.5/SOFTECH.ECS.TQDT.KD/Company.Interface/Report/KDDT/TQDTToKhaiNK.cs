﻿//Lypt createdb
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
#if KD_V3 || KD_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif KD_V2
using Company.KD.BLL.DuLieuChuan;
using Company.BLL.KDT.SXXK;
#endif

namespace Company.Interface.Report.KDDT
{
    public partial class TQDTToKhaiNK : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";

        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKNTQDTForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public bool isCuaKhau = false;
        public float FontReport = 8;

        public TQDTToKhaiNK()
        {
            InitializeComponent();
           
        }
        public void BindReport()
        {
            if (!float.TryParse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport"), out FontReport)) FontReport = 8;
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            double tongTienThueXNK = 0;
            double tongTienThueTatCa = 0;
            double tongTriGiaTT = 0;
            double tongTriGiaTTGTGT = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;

            lblMienThueNK.Visible = MienThue1;
            lblMienThueGTGT.Visible = MienThue2;
            lblMienThueNK.Text = Properties.Settings.Default.TieuDeInDinhMuc;
            lblMienThueGTGT.Text = Properties.Settings.Default.MienThueGTGT;
            //lblMienThueGTGT.Font = new Font("Times New Roman", FontReport);
            //lblMienThueNK.Font = new Font("Times New Roman", FontReport);

            DateTime minDate = new DateTime(1900, 1, 1);

            if (this.TKMD.SoTiepNhan > 0)
                this.lblThamChieu.Text = this.TKMD.SoTiepNhan.ToString();
            else
                this.lblThamChieu.Text = this.TKMD.ID.ToString();

            //Thông tin Hải quan
            lblChiCucHQ.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
            lblChiCucHQ.Font = new Font("Times New Roman", FontReport);
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQCK.Text = chicuc;
                lblChiCucHQCK.Font = new Font("Times New Roman", FontReport);
            }
            else
                lblChiCucHQCK.Text = "";

            //Ngày tiếp nhận
            if (this.TKMD.NgayTiepNhan > minDate)
                lblNgayGui.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            else
                lblNgayGui.Text = "";

            //Số tờ khai
            if (this.TKMD.SoToKhai > 0)
                lblToKhai.Text = this.TKMD.SoToKhai + "";

            //Ngày đăng ký
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";
            //lblNgayGui.Text = "";
            //if (this.TKMD.SoLuongPLTK > 0)
            //   lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();

            //Người nhập khẩu
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);
            if (Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap")))
                lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
            else
                lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI;
            lblNguoiNK.Font = new Font("Times New Roman", FontReport);
            //Đơn vị ủy thác
            if ((!TKMD.MaDonViUT.Equals("")) && (!TKMD.TenDonViDoiTac.Equals("")))
                lblNguoiUyThac.Text = this.TKMD.MaDonViUT + " - " + this.TKMD.TenDonViUT;
            else
                lblNguoiUyThac.Text = "";
            lblNguoiUyThac.Font = new Font("Times New Roman", FontReport); 
            //lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            //lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            //DATLMQ update Font GiayPhep 17/12/2010
            if (TKMD.SoGiayPhep != "")
                lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
            else
                lblGiayPhep.Text = "";
            lblGiayPhep.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayGiayPhep.Text = "";
            lblNgayGiayPhep.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayHetHanGiayPhep.Text = "";
            lblNgayHetHanGiayPhep.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font HD 17/12/2010
            lblHopDong.Text = "" + this.TKMD.SoHopDong;
            lblHopDong.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHopDong.Text = " ";
            lblNgayHopDong.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHetHanHopDong.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHetHanHopDong.Text = " ";
            lblNgayHetHanHopDong.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font 17/12/2010 HoaDonTM
            lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;
            lblHoaDonThuongMai.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayHoaDonThuongMai > minDate)
                lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            else
                lblNgayHoaDon.Text = "";
            lblNgayHoaDon.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font MaPTVT 17/12/2010
            lblPhuongTienVanTai.Text = " " + this.TKMD.SoHieuPTVT;
            lblPhuongTienVanTai.Font = new Font("Times New Roman", FontReport);
            //if (this.TKMD.NgayDenPTVT > minDate)
            //     lblNgayDenPTVT.Text = "Ngày đến: " + this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
            //else
            //    lblNgayDenPTVT.Text = "Ngày đến: ";

            //DATLMQ update Font LyDoTK 17/12/2010
            //if (TKMD.PhanLuong == "1")
            //    lblLyDoTK.Text = "Tờ khai được thông quan";
            //else if (TKMD.PhanLuong == "2")
            //    lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
            //else if (TKMD.PhanLuong == "3")
            //    lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
            //else
            //    lblLyDoTK.Text = "";
            //lblLyDoTK.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font VanTaiDon 17/12/2010
            lblVanTaiDon.Text = this.TKMD.SoVanDon;
            lblVanTaiDon.Font = new Font("Times New Roman", FontReport);
            if (this.TKMD.NgayVanDon > minDate)
                lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            else
                lblNgayVanTaiDon.Text = "";
            lblNgayVanTaiDon.Font = new Font("Times New Roman", FontReport);

            if (TKMD.SoContainer20 > 0)
                lblCon20.Text = TKMD.SoContainer20 + "";
            else
            {
                lblCon20.Text = "";
                xrLabel51.Visible = false;
            }
            if (TKMD.SoContainer40 > 0)
                lblCon40.Text = TKMD.SoContainer40 + "";
            else
            {
                lblCon40.Text = "";
                xrLabel41.Visible = false;
            }
            if (TKMD.SoKien > 0)
                lblTongsoKien.Text = TKMD.SoKien + " Kiện";
            else
                lblTongsoKien.Text = "";
            if (TKMD.TrongLuong > 0)
                lbltongtrongluong.Text = TKMD.TrongLuong + " kg ";
            else
                lbltongtrongluong.Text = "";

            if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                lblChitietCon.Text = "chi tiết phụ lục đính kèm";
            else
            {
                //DATLMQ bo sung So hieu kien, cont 26/10/2010
                lblChitietCon.Text = "";
                if (TKMD.VanTaiDon != null)
                {
                    if (TKMD.VanTaiDon.ContainerCollection.Count != 0)
                    {
                        for (int i = 0; i < TKMD.VanTaiDon.ContainerCollection.Count; i++)
                        {
                            //if (i == TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            //    lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No;
                            //else
                            //    lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No + ";";

                            lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No;

                            if (i < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            {
                                lblChitietCon.Text += "; ";
                            }
                        }
                    }
                    else
                    {
                        lblChitietCon.Text = "";
                    }
                }
                else
                {
                    lblChitietCon.Text = "";
                }
            }
            lblChitietCon.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font NuocXK
            lblNuocXK.Text = this.TKMD.NuocXK_ID;
            lblNuocXK.Font = new Font("Times New Roman", FontReport);
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);
            lblTenNuoc.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font CangXepHang 17/12/2010
            lblCangXepHang.Text = this.TKMD.DiaDiemXepHang;
            lblCangXepHang.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font CangDoHang 17/12/2010
            lblCangDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblCangDoHang.Font = new Font("Times New Roman", FontReport);
            lblMaCangdoHang.Text = this.TKMD.CuaKhau_ID;
            lblMaCangdoHang.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font DieuKienGiaoHang 17/12/2010
            lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;
            lblDieuKienGiaoHang.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font DongTienThanhToan 17/12/2010
            lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;
            lblDongTienThanhToan.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font TGTT 17/12/2010
            lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblTyGiaTinhThue.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font PTTT 17/12/2010
            lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;
            lblPhuongThucThanhToan.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font 17/12/2010 PTVT
            lblTenPTVT.Text = PhuongThucVanTai.getName(TKMD.PTVT_ID);
            lblTenPTVT.Font = new Font("Times New Roman", FontReport);

            //DATLMQ update Font 17/12/2010 NguoiXK
            lblNguoiXK.Font = new Font("Times New Roman", FontReport);
            lblNguoiXK.Text = TKMD.TenDonViDoiTac.ToString();

            //DATLMQ update Font 17/12/2010 LoaiHinh
            string stlh = "";
            stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
            lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
            lblLoaiHinh.Font = new Font("Times New Roman", FontReport);

            string st = "";
            if (this.TKMD.PhiBaoHiem > 0)
                st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
            if (this.TKMD.PhiVanChuyen > 0)
                st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
            if (this.TKMD.PhiKhac > 0)
                st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
            lblPhiBaoHiem.Text = st;
            //lblTrongLuong.Text = "TC " + this.TKMD.SoKien.ToString("n0") + " kiện  " + this.TKMD.TrongLuong + " kg";
            //lblTrongLuong.Text = "         TC: " + TKMD.SoKien.ToString("n0") + " Kiện " + "    NW : " + this.TKMD.TrongLuongNet + " Kg    GW : " + this.TKMD.TrongLuong + " Kg ";
            //lblTrongLuong.Text  = this.TKMD.TrongLuong.ToString();

            #region <= 3 hàng
            if (this.TKMD.HMDCollection.Count <= 3)
            {
                #region 1 hàng
                if (this.TKMD.HMDCollection.Count >= 1)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[0];
                    if (!inMaHang)
                        TenHang1.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang1.Text = hmd.TenHang;
                    }
                    TenHang1.WordWrap = true;
                    TenHang1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.NuocXX_ID;
                    Luong1.Text = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.SoLuongHMD);
                    DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT1.Text = "F.O.C";
                        TriGiaNT1.Text = "F.O.C";
                        TriGiaTT1.Text = "";
                        ThueSuatXNK1.Text = "";
                        TienThueXNK1.Text = "";
                        TriGiaTTGTGT1.Text = "";
                        ThueSuatGTGT1.Text = "";
                        TienThueGTGT1.Text = "";
                        TyLeThuKhac1.Text = "";
                        TriGiaThuKhac1.Text = "";
                    }
                    else
                    {
                        DonGiaNT1.Text = hmd.DonGiaKB.ToString("N" + GlobalSettings.SoThapPhan.DonGiaNT);
                        TriGiaNT1.Text = hmd.TriGiaKB.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);
                        TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK1.Text = "??";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.TriGiaThuKhac;
                            TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2");
                            //TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");

                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2");
                            //TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac1.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac1.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }

                    }
                    if (MienThue1)
                    {
                        TriGiaTT1.Text = "";
                        ThueSuatXNK1.Text = "";
                        TienThueXNK1.Text = "";
                    }
                    if (MienThue2)
                    {

                    }
                    //if (hmd.MienThue == 1)
                    //{
                    //    TriGiaTT1.Text = "";
                    //    ThueSuatXNK1.Text = "";
                    //    TienThueXNK1.Text = "";
                    //    TriGiaTTGTGT1.Text = "";
                    //    ThueSuatGTGT1.Text = "";
                    //    TienThueGTGT1.Text = "";
                    //    TyLeThuKhac1.Text = "";
                    //    TriGiaThuKhac1.Text = "";
                    //}
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.SoThapPhan.TriGiaNT, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
                #region 2 hàng
                if (this.TKMD.HMDCollection.Count >= 2)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[1];
                    if (!inMaHang)
                        TenHang2.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang2.Text = hmd.TenHang;
                    }
                    TenHang2.WordWrap = true;
                    TenHang2.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.NuocXX_ID;
                    Luong2.Text = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.SoLuongHMD);
                    DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT2.Text = "F.O.C";
                        TriGiaNT2.Text = "F.O.C";
                        TriGiaTT2.Text = "";
                        ThueSuatXNK2.Text = "";
                        TienThueXNK2.Text = "";
                        TriGiaTTGTGT2.Text = "";
                        ThueSuatGTGT2.Text = "";
                        TienThueGTGT2.Text = "";
                        TyLeThuKhac2.Text = "";
                        TriGiaThuKhac2.Text = "";
                    }
                    else
                    {
                        DonGiaNT2.Text = hmd.DonGiaKB.ToString("N" + GlobalSettings.SoThapPhan.DonGiaNT);
                        TriGiaNT2.Text = hmd.TriGiaKB.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);
                        TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK2.Text = "";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK2.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2"); 
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac2.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                    }
                    //if (hmd.MienThue == 1)
                    //{
                    //    TriGiaTT2.Text = "";
                    //    ThueSuatXNK2.Text = "";
                    //    TienThueXNK2.Text = "";
                    //    TriGiaTTGTGT2.Text = "";
                    //    ThueSuatGTGT2.Text = "";
                    //    TienThueGTGT2.Text = "";
                    //    TyLeThuKhac2.Text = "";
                    //    TriGiaThuKhac2.Text = "";
                    //}

                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.SoThapPhan.TriGiaNT, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
                #region 3 hàng
                if (this.TKMD.HMDCollection.Count == 3)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[2];
                    if (!inMaHang)
                        TenHang3.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang3.Text = hmd.TenHang;
                    }
                    TenHang3.WordWrap = true;
                    TenHang3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.NuocXX_ID;
                    Luong3.Text = hmd.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.SoLuongHMD);
                    DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT3.Text = "F.O.C";
                        TriGiaNT3.Text = "F.O.C";
                        TriGiaTT3.Text = "";
                        ThueSuatXNK3.Text = "";
                        TienThueXNK3.Text = "";
                        TriGiaTTGTGT3.Text = "";
                        ThueSuatGTGT3.Text = "";
                        TienThueGTGT3.Text = "";
                        TyLeThuKhac3.Text = "";
                        TriGiaThuKhac3.Text = "";


                    }
                    else
                    {
                        DonGiaNT3.Text = hmd.DonGiaKB.ToString("N" + GlobalSettings.SoThapPhan.DonGiaNT);
                        TriGiaNT3.Text = hmd.TriGiaKB.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);
                        TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK3.Text = "";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK3.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac == 0 ? string.Empty : hmd.TyLeThuKhac.ToString("N2");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac3.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                    }
                    //if (hmd.MienThue == 1)
                    //{
                    //    TriGiaTT3.Text = "";
                    //    ThueSuatXNK3.Text = "";
                    //    TienThueXNK3.Text = "";
                    //    TriGiaTTGTGT3.Text = "";
                    //    ThueSuatGTGT3.Text = "";
                    //    TienThueGTGT3.Text = "";
                    //    TyLeThuKhac3.Text = "";
                    //    TriGiaThuKhac3.Text = "";
                    //}

                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.SoThapPhan.TriGiaNT, MidpointRounding.AwayFromZero);

                    tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
            }
            #endregion
            #region nhiều hàng
            else
            {
                //TenHang1.Text = "HÀNG HÓA NHẬP";
                TenHang1.Text = "(Chi tiết theo phụ lục đính kèm)";
                //TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    if (!hmd.FOC)
                    {
                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.SoThapPhan.TriGiaNT, MidpointRounding.AwayFromZero);
                        tongTienThueXNK += hmd.ThueXNK;
                        tongTriGiaTT += hmd.TriGiaTT;

                        tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;

                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }

                    }
                }
                //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);

                /* 
                 //Khi co phu luc thi khong hien thi :
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");

                TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");
                 */

            }
            #endregion

            // - Linhhtn mark 20100622 - Tổng trị giá NT không được cộng các loại phí
            //tongTriGiaNT += Convert.ToDouble(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);

            lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            if (tongThueGTGT > 0)
                lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            else
                lblTongTienThueGTGT.Text = "";
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            lblTongThueXNKSo.Text = tongTienThueTatCa.ToString("N0");// this.TinhTongThueHMD().ToString("N0");

            string s = Company.KD.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            //DATLMQ update Font HuongDan 17/12/2010
            lblHuongDan.Text = TKMD.HUONGDAN;
            lblHuongDan.Font = new Font("Times New Roman", FontReport);

            if (MienThue1)
            {
                TriGiaTT1.Text = "";
                ThueSuatXNK1.Text = "";
                TienThueXNK1.Text = "";
                TriGiaTT2.Text = "";
                ThueSuatXNK2.Text = "";
                TienThueXNK2.Text = "";
                TriGiaTT3.Text = "";
                ThueSuatXNK3.Text = "";
                TienThueXNK3.Text = "";
                lblTongThueXNK.Text = "";
            }
            if (MienThue2)
            {
                TriGiaTTGTGT1.Text = "";
                ThueSuatGTGT1.Text = "";
                TienThueGTGT1.Text = "";
                TriGiaTTGTGT2.Text = "";
                ThueSuatGTGT2.Text = "";
                TienThueGTGT2.Text = "";
                TriGiaTTGTGT3.Text = "";
                ThueSuatGTGT3.Text = "";
                TienThueGTGT3.Text = "";
                lblTongTienThueGTGT.Text = "";
            }
            if (MienThue1 && MienThue2)
            {
                lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
            }

            //datlmq update 29072010
            lblDeXuatKhac.Text = "32.Ghi chép khác " + TKMD.DeXuatKhac; // ghi chép khác
            //if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Length > 0)
            //    lblDeXuatKhac.Text += "; " + "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
            //else if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Equals(""))
            //    lblDeXuatKhac.Text += "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
            lblDeXuatKhac.Font = new Font("Times New Roman", FontReport);

            //Ngay thang nam in to khai
            if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                lblNgayIn.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
            else
                lblNgayIn.Text = "Ngày " + TKMD.NgayDangKy.Day + " tháng " + TKMD.NgayDangKy.Month + " năm " + TKMD.NgayDangKy.Year;

            // XRControl control = new XRControl();
            //int i = 5;
            //foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            //{
            //    if (ct.LoaiCT < 5)
            //    {
            //        control = this.Detail.Controls["lblSoBanChinh" + ct.LoaiCT];
            //        control.Text = ct.SoBanChinh + "";
            //        control = this.Detail.Controls["lblSoBanSao" + ct.LoaiCT];
            //        control.Text = ct.SoBanSao + "";
            //    }
            //    else
            //    {
            //        if (i == 7) return;
            //        control = this.Detail.Controls["lblTenChungTu" + i];
            //        control.Text = ct.TenChungTu;
            //        control = this.Detail.Controls["lblSoBanChinh" + i];
            //        control.Text = ct.SoBanChinh + "";
            //        control = this.Detail.Controls["lblSoBanSao" + i];
            //        control.Text = ct.SoBanSao + "";
            //        i++;
            //    }
            //}
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        public double TinhTongThueHMD()
        {
            double tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (!hmd.FOC)
                    tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            }
            return tong;
        }
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }

        public void setThongTin(XRControl cell, string thongTin)
        {
            if (cell.Name.Equals("lblDeXuatKhac"))
                cell.Text = "32. Đề xuất khác: " + thongTin;
            else
                cell.Text = thongTin;
        }
        //datlmq update 29072010
        public void setDeXuatKhac(string deXuatKhac)
        {
            lblDeXuatKhac.Text = "32. Đề xuất khác: " + deXuatKhac;
        }

        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThue2.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TriGiaTT1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblDeXuatKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtDeXuatKhac.Text = cell.Text;

        }

        private void label_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtDeXuatKhac.Text = cell.Text;
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblThamChieu_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
