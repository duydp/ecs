﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Data.OleDb;
using System.Data;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucSendForm_OLD : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();
        private string maSP = "";
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        //-----------------------------------------------------------------------------------------
        public DinhMucSendForm_OLD()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                if (dmDangKy.ID > 0)
                {
                    txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                    dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                    cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc.ToString();
                }
                else
                {
                    cbbIsSXXK.SelectedValue = "1";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //-----------------------------------------------------------------------------------------
        private void DinhMucSendForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            this.khoitao_DuLieuChuan();
            if (dmDangKy.ID == 0)
            {
                if (isEdit)
                {
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                }
                else if (isCancel)
                {
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                }
                else
                {
                    if (dmDangKy.ID == 0)
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                }
            }
            SetCommandNew();
            BindData();
        }
        private int CheckNPLVaSPDuyet()
        {
            foreach (DinhMuc dm in this.dmDangKy.DMCollection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void Add()
        {
            try
            {
                DinhMucDangKy dmDangKy = new DinhMucDangKy();

                DinhMucEditForm f = new DinhMucEditForm();
                f.dmDangKy = this.dmDangKy;
                f.WindowState = FormWindowState.Normal;
                f.OpenType = this.OpenType;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private int getPostion(DinhMuc dm)
        {
            for (int i = 0; i < this.dmDangKy.DMCollection.Count; i++)
            {
                if (this.dmDangKy.DMCollection[i].MaSanPham == dm.MaSanPham && this.dmDangKy.DMCollection[i].MaNguyenPhuLieu == dm.MaNguyenPhuLieu) return i;
            }
            return -1;
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucEditForm f = new DinhMucEditForm();
                    f.DMDetail = (DinhMuc)e.Row.DataRow;
                    f.dmDangKy = dmDangKy;
                    f.dmDangKy.LoaiDinhMuc = Convert.ToInt32(cbbIsSXXK.SelectedValue.ToString());
                    f.OpenType = this.OpenType;
                    f.ShowDialog();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void Save()
        {
            try
            {

                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    if (this.dmDangKy.ID == 0)
                    {
                        this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                        this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        if (isEdit)
                        {
                            this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                        }
                        else if (isCancel)
                        {
                            this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        }
                        else
                        {
                            this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        }
                        this.dmDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                        this.dmDangKy.NgayTiepNhan = DateTime.Now;
                        this.dmDangKy.NgayDangKy = DateTime.Now;
                        this.dmDangKy.NgayApDung = DateTime.Now;
                    }
                    else
                    {
                        this.dmDangKy.NgayTiepNhan = dtpNgayTN.Value;
                        this.dmDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                        this.dmDangKy.NgayDangKy = dtpNgayTN.Value;
                        this.dmDangKy.NgayApDung = dtpNgayTN.Value;
                    }
                    this.dmDangKy.LoaiDinhMuc = Convert.ToInt32(cbbIsSXXK.SelectedValue.ToString());
                    int sttHang = 1;
                    dmDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                    foreach (DinhMuc nplD in this.dmDangKy.DMCollection)
                    {
                        nplD.STTHang = sttHang++;
                    }
                    dmDangKy.InsertUpdateFull();
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "LƯU THÀNH CÔNG", false);
                    Log.LogDinhMuc(dmDangKy, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdImportExcel":
                    this.ShowExcelForm();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY ĐỊNH MỨC NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        dmDangKy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                        dmDangKy.Update();
                        SetCommandNew();
                        this.SendV5(true);
                    }
                    break;
                case "cmdEdit":
                    this.SendV5(false);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "SaoChep":
                    this.SaoChepDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "cmdResult":
                    btnResultHistory_Click(null, null);
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaDM":
                    this.ChuyenTrangThaiSua();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageDinhMuc(dmDangKy);
                    dmDangKy.InsertUpdateFull();
                    if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        dmDangKy.TransferDataToSXXK();
                    }
                    Log.LogDinhMuc(dmDangKy, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    SetCommandNew();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = dmDangKy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.DinhMuc;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, LoaiKhaiBao.DinhMuc), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_SXXK_DinhMucDangKy", "", Convert.ToInt32(dmDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.dmDangKy.SoTiepNhan == 0)
                {
                    return;
                }
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
                phieuTN.phieu = "ĐỊNH MỨC";
                phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void InDinhMuc()
        {
            try
            {
                if (dmDangKy.ID == 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "LƯU THÔNG TIN TRƯỚC KHI IN.", false);
                    return;
                }
                Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
                f.DMDangKy = this.dmDangKy;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ShowExcelForm()
        {
            try
            {
                ImportKDTDMForm importExcel = new ImportKDTDMForm();
                importExcel.dmDangKy = dmDangKy;
                importExcel.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaoChepDinhMuc()
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();
            f.ShowDialog(this);
            BindData();

        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG?", true) == "Yes")
                {
                    DinhMucCollection dmDeleteColl = new DinhMucCollection();
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                            // Thực hiện xóa trong CSDL.
                            if (dmSelected.ID > 0)
                            {
                                try
                                {
                                    // XÓA ĐỊNH MỨC ĐÃ ĐĂNG KÝ
                                    Company.BLL.SXXK.DinhMuc DM = new Company.BLL.SXXK.DinhMuc();
                                    DM.MaSanPHam = dmSelected.MaSanPham;
                                    DM.MaNguyenPhuLieu = dmSelected.MaNguyenPhuLieu;
                                    DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                    DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                    DM.Delete();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }

                                dmSelected.Delete();
                                dmDeleteColl.Add(dmSelected);
                            }
                            dmDangKy.DMCollection.Remove(dmSelected);
                        }
                    }
                    Log.LogDinhMuc(dmDangKy, dmDeleteColl, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindData();
                    ShowMessage("Xóa thành công", false);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaDinhMuc(GridEXSelectedItemCollection items)
        {
            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<DinhMuc> itemsDelete = new List<DinhMuc>();
            DinhMucCollection dmDeleteColl = new DinhMucCollection();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMuc sp = (DinhMuc)i.GetRow().DataRow;
                    itemsDelete.Add(sp);
                }
            }
            if (ShowMessage(" DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
            {
                foreach (DinhMuc item in itemsDelete)
                {
                    if (item.ID > 0)
                    {
                        try
                        {
                            // XÓA ĐỊNH MỨC ĐÃ ĐĂNG KÝ
                            Company.BLL.SXXK.DinhMuc DM = new Company.BLL.SXXK.DinhMuc();
                            DM.MaSanPHam = item.MaSanPham;
                            DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                            DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            DM.Delete();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        item.Delete();
                        dmDeleteColl.Add(item);
                    }
                    dmDangKy.DMCollection.Remove(item);
                }
                Log.LogDinhMuc(dmDangKy, dmDeleteColl, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                ShowMessage("XÓA THÀNH CÔNG", false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMuc(items);
            BindData();
        }
        //----------------------------------------------------------------------------------------------
        #region Send V5 EDIT DUYDP
        private void SendV5(bool isCancel)
        {
            bool isEdit = false;
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                isEdit = true;
            }
            else
            {
                isEdit = false;
            }
            if (dmDangKy.ID == 0)
            {
                this.ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO", false);
                return;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                    return;
                }
            }
            try
            {
                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    // Master.
                    int s = CheckNPLVaSPDuyet();
                    if (s != 2)
                    {
                        string msg = "";
                        if (s == 0)
                            msg = "TRONG DANH SÁCH NÀY CÓ NGUYÊN PHỤ LIỆU CHƯA ĐƯỢC HẢI QUAN DUYỆT.";
                        else msg = "TRONG DANH SÁCH NÀY CÓ SẢN PHẨM CHƯA ĐƯỢC HẢI QUAN DUYỆT. ";
                        msg += "\n DOANH NGHIỆP CÓ MUỐN GỬI LÊN HAY KHÔNG ?";

                        string kq = MLMessages(msg, "MSG_PUB20", "", true);
                        if (kq != "Yes")
                            return;
                    }
                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    string returnMessage = string.Empty;
                    dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                    SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy, isCancel, isEdit, GlobalSettings.TEN_DON_VI);
                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = dmDangKy.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                          },
                          new SubjectBase()
                          {
                              Type = dmSp.Issuer,
                              Reference = dmDangKy.GUIDSTR,
                          },
                          dmSp
                        );
                    if (isEdit)
                        msgSend.Subject.Function = Company.KDT.SHARE.Components.DeclarationFunction.SUA;
                    else if (isCancel) msgSend.Subject.Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY;
                    else msgSend.Subject.Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO; ;

                    switch (dmDangKy.TrangThaiXuLy)
                    {
                        case -1:
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            break;
                        case -5:
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            break;
                        case 5:
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoDinhMucSua, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            break;
                        default:
                            break;
                    }

                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
                        cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        btnDelete.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                        sendXML.master_id = dmDangKy.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                dmDangKy.DeleteFull();
                            }
                            else
                            {
                                Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQDuyetDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                dmDangKy.TransferDataToSXXK();
                            }
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedBackV5();
                            SetCommandNew();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQTuChoiDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandNew();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN HOẶC HỆ THỐNG HQ BỊ QUÁ TẢI KHÔNG NHẬN ĐƯỢC PHẢN HỒI . NẾU TRƯỜNG HỢP HỆ THỐNG HQ BỊ QUÁ TẢI BẠN CHỌN YES ĐỂ TẠO MESSAGE NHẬN PHẢN HỒI . CHỌN NO ĐỂ BỎ QUA", true) == "Yes")
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                    return;
                }
            }
            while (isFeedBack)
            {
                string reference = dmDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                                                  //Identity = dmDangKy.MaHaiQuan
                                              }, subjectBase, null);
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQHuyDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                dmDangKy.DeleteFull();
                            }
                            else
                            {
                                Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQDuyetDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                dmDangKy.TransferDataToSXXK();
                            }
                            dmDangKy.Update();
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            Log.LogDinhMuc(dmDangKy, MessageTitle.KhaiBaoHQTuChoiDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            dmDangKy.Update();
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                        }
                    }
                }

            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);
        }
        /// <summary>
        /// 11/12/2014 minhnd
        /// </summary>
        private void ChuyenTrangThaiSua()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                string msg = "";
                msg += "---------THÔNG TIN ĐỊNH MỨC ĐÃ KHAI BÁO---------";
                msg += "\nSỐ TIẾP NHẬN : " + dmDangKy.SoTiepNhan.ToString();
                msg += "\nNGÀY TIẾP NHẬN : " + dmDangKy.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                msg += "\nHẢI QUAN TIẾP NHẬN : " + dmDangKy.MaHaiQuan.ToString();
                msg += "\n----------------THÔNG TIN XÁC NHẬN----------------";
                msg += "\nBẠN CÓ MUỐN CHUYỂN SANG TRẠNG THÁI KHAI BÁO SỬA KHÔNG ?";
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", msg, true) == "Yes")
                {
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    dmDangKy.InsertUpdate();
                    dmDangKy.DeleteDinhMucSXXK();
                    SetCommandNew();
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", dmDangKy.ID, dmDangKy.GUIDSTR, (MessageTypes)int.Parse(DeclarationIssuer.GC_DINH_MUC), MessageFunctions.SuaToKhai, "Chuyển trạng thái sửa", "Chuyển khai báo định mức sang trạng thái sửa");
                    Log.LogDinhMuc(dmDangKy, MessageTitle.ChuyenKhaiBaoSua, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            else
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC CHƯA ĐƯỢC DUYỆT", false);
        }
        private void SetCommandNew()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
                this.OpenType = OpenFormType.View;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdImportExcel.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = dmDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Chờ duyệt", "Wait approved");
                this.OpenType = OpenFormType.View;
                cbbIsSXXK.SelectedValue = dmDangKy.LoaiDinhMuc;
            }

        }
        #endregion

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = dmDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.SXXK_DINH_MUC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
            {

                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                gridEXExporter1.GridEX = dgList;
                try
                {
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();
                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }


    }




}
