﻿using System;
using System.Windows.Forms;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
namespace Company.Interface.KDT.GC
{
    public partial class HopDongEditForm : BaseForm
    {

        public HopDong HD = new HopDong();
        private string xmlCurrent = "";
        public HopDongEditForm()
        {
            InitializeComponent();

            btnThemSanPham.Click += new EventHandler(btnClickEvent);
        }

        private void HopDongEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (HD.ID > 0)
                {
                    HD.Load();
                    HD.LoadCollection();
                    txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                    txtSoHopDong.Text = HD.SoHopDong;                    
                    ccNgayKetThucHD.Value = HD.NgayHetHan;
                    ccNgayKyHD.Value = HD.NgayKy;
                    nuocHControl1.Ma = HD.NuocThue_ID;
                    nguyenTeControl1.Ma = HD.NguyenTe_ID;
                    txtDCDT.Text = HD.DiaChiDoiTac.Trim();
                    txtDVDT.Text = HD.DonViDoiTac.Trim();
                    donViHaiQuanControl1.Ma = HD.MaHaiQuan;
                    if (HD.NgayGiaHan.Year <= 1900)
                    {
                        ccNgayGiaHan.Text = "";
                    }
                    if (HD.NgayTiepNhan.Year <= 1900)
                    {
                        ccNgayTiepNhan.Text = "";
                    }


                }
                setCommandStatus();
                if (HD.ID > 0)
                {
                    Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    msg.master_id = HD.ID;
                    msg.LoaiHS = "HD";
                    if (msg.Load())
                    {
                        lblTrangThai.Text = setText("Chờ xác nhận ","Wait for confirmation");
                        NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                dgNguyenPhuLieu.Tables[0].Columns["SoLuongDangKy"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                dgSanPham.Tables[0].Columns["SoLuongDangKy"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                dgNhomSanPham.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;

                dgNhomSanPham.DataSource = HD.NhomSPCollection;
                dgNguyenPhuLieu.DataSource = HD.NPLCollection;
                dgSanPham.DataSource = HD.SPCollection;
                dgThietBi.DataSource = HD.TBCollection;
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Có lỗi : " + ex.StackTrace, false);
            }
        }
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            {
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HD.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Hợp đồng không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                   // xmlCurrent = HD.LayPhanHoi(password, sendXML.msg);
                    xmlCurrent = HD.TQDTLayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            cmdAddLoaiSanPham.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", HD.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HD.SoTiepNhan, false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //ShowMessage("Đã hủy hợp đồng này", false);
                    lblTrangThai.Text = setText("Chưa khai báo","Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        lblTrangThai.Text = setText("Đã duyệt","Approved");


                    }
                    else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        //ShowMessage("Hải quan chưa xử lý hợp đồng này!", false);
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                    }
                    else if (HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                       // ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        lblTrangThai.Text = setText("Không phê duyệt","Not approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void setCommandStatus()
        {
            if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnThemNguyenPhuLieu.Enabled = false;
                btnThemSPGC.Enabled = false;
                btnThemSanPham.Enabled = false;
                btnThemThietBi.Enabled = false;
                btnImportNPL.Enabled = btnImportSP.Enabled = btnImportTB.Enabled = false;

                btnXoa.Enabled = false;
                btnXoaNPL.Enabled = false;
                btnXoaTB.Enabled = false;
                uiButton1.Enabled = false;


                lblTrangThai.Text =setText( "Đã duyệt","Approved");
            }
            else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnThemNguyenPhuLieu.Enabled = false;
                btnThemSPGC.Enabled = false;
                btnThemSanPham.Enabled = false;
                btnThemThietBi.Enabled = false;
                btnImportNPL.Enabled = btnImportSP.Enabled = btnImportTB.Enabled = false;

                btnXoa.Enabled = false;
                btnXoaNPL.Enabled = false;
                btnXoaTB.Enabled = false;
                uiButton1.Enabled = false;
            }
            else
            {
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                btnThemNguyenPhuLieu.Enabled = true;
                btnThemSPGC.Enabled = true;
                btnThemSanPham.Enabled = true;
                btnThemThietBi.Enabled = true;
                btnImportNPL.Enabled = btnImportSP.Enabled = btnImportTB.Enabled = true;

                btnXoa.Enabled = true;
                btnXoaNPL.Enabled = true;
                btnXoaTB.Enabled = true;
                uiButton1.Enabled = true;
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            try
            {
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HD.ID;
                sendXML.Load();

                //HUNGTQ BỔ SUNG XML LAYPHANHOIDADUYET 10/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, HD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                //this.Update();
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = HD.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);


               // xmlCurrent = HD.LayPhanHoi(pass, sendXML.msg);
                xmlCurrent = HD.TQDTLayPhanHoi(pass, xml.InnerXml);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        else
                        {
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", HD.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HD.SoTiepNhan, false);
                    txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //ShowMessage("Đã hủy hợp đồng này", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo","Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {                       
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        lblTrangThai.Text =setText("Đã duyệt","Approved");
                    }
                    else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //ShowMessage("Hải quan chưa xử lý hợp đồng này!", false);
                    }
                    else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        ShowMessage("Hải quan từ chối tiếp nhận.\nLý do: " + HD.HUONGDAN, false);
                        txtSoTiepNhan.Text = "";
                        lblTrangThai.Text = "Chưa khai báo";
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi Lấy phản hồi hợp đồng từ hải quan. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAddLoaiSanPham":
                    showFormLoaiSanPham();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdAddThietBi":
                    showFormThietBi();
                    tabHopDong.SelectedIndex = 3;
                    break;
                case "cmdAddNguyenPhuLieu":
                    showFormNguyenPhuLieu();
                    tabHopDong.SelectedIndex = 1;
                    break;
                case "cmdAddSanPham":
                    showFormSanPham();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "cmdSave":
                    save();
                    break;
                case "cmdSend":
                    send(); break;
                case "HuyKhaiBao":
                    Huy();
                    break;
                case "NhanDuLieuHD":
                    NhanDuLieuHD12();
                    break;
                case "XacNhan":
                    LaySoTiepNhanDT();
                    break;
                case "cmdNPLExcel":
                    NguyenPhuLieuImportExcel();
                    tabHopDong.SelectedIndex = 1;
                    break;
                case "cmdSPExcel":
                    SanPhamImportExcel();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "cmdThietBi":
                    ThietBiImportExcel();
                    tabHopDong.SelectedIndex = 3;
                    break;
                case "cmdLoaiSPGCExcel":
                    LoaiSPGCImportExcel();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdCopyHD":
                    showFormHopDongRegisted();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdCopyDM":
                    showFormDangKyDM();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.HD.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "HỢP ĐỒNG";
            phieuTN.soTN = this.HD.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.HD.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = donViHaiQuanControl1.Ma;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void LoaiSPGCImportExcel()
        {
            Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm f = new Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm();
            f.HD = this.HD;
            f.ShowDialog();
            try
            {
                dgNguyenPhuLieu.Refetch();
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }

        private void NguyenPhuLieuImportExcel()
        {
            Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm f = new Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm();
            f.HD = this.HD;
            f.ShowDialog();
            try
            {
                dgNguyenPhuLieu.Refetch();
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }

        private void SanPhamImportExcel()
        {
            if (HD.NhomSPCollection.Count == 0)
            {
                showMsg("MSG_2702047");
                //ShowMessage("Bạn chưa nhập danh sách loại sản phẩm gia công.", false);
                return;
            }
            Company.Interface.KDT.SXXK.SanPhamReadExcelForm f = new Company.Interface.KDT.SXXK.SanPhamReadExcelForm();
            f.HD = this.HD;
            f.ShowDialog();
            try
            {
                dgSanPham.Refetch();
            }
            catch
            {
                dgSanPham.Refresh();
            }
        }

        private void ThietBiImportExcel()
        {
            Company.Interface.KDT.SXXK.ThietBiReadExcelForm f = new Company.Interface.KDT.SXXK.ThietBiReadExcelForm();
            f.HD = this.HD;
            f.ShowDialog();
            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }
        }
        public void NhanDuLieuHD12()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HD.ID;
            string password = "";
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Hợp đồng đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                // TAM THOI COMMENT ngay 11/10/2010 by TUNGNT co mat HUNGTQ va DATLMQ
                //xmlCurrent = HD.WSDownLoad(password);

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HD.ID;
            string password = "";
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //Message("MSG_WRN05", "", false);
                //ShowMessage("Hợp đồng đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HD.WSCancel(password);

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        public void send()
        {

           
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HD.ID;

            if (sendXML.Load())
            {
                          
                showMsg("MSG_WRN05");
                //ShowMessage("Hợp đồng đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            if (HD.NhomSPCollection.Count == 0)
            {
                showMsg("MSG_2702048");
                //ShowMessage("Bạn chưa nhập thông tin về nhóm sản phẩm.", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();

            try
            {
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = HD.WSSend(password);

                lblTrangThai.Text = setText("Chờ xác nhận", "Wait for confirmation");
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
               
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();

                sendXML.LoaiHS = "HD";
                sendXML.master_id = HD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private bool save()
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return false;
                if (ccNgayKyHD.Value >= ccNgayKetThucHD.Value)
                {
                    error.SetError(ccNgayKetThucHD, setText("Ngày kết thúc phải lớn hơn ngày ký.", "Date of ending contract must be greater than signing's date"));
                    return false;
                }
                else
                {
                    error.SetError(ccNgayKetThucHD, null);
                }

                if (HD.NhomSPCollection.Count == 0)
                {
                    showMsg("MSG_2702049");
                    //ShowMessage("Chưa chọn loại sản phẩm gia công", false);
                    return false; 
                }
                if (HD.SoHopDong == "")
                {
                    if (HD.checkSoHopDongExit(txtSoHopDong.Text.Trim(), donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI))
                    {
                        showMsg("MSG_2702050");
                        //ShowMessage("Đã có số hợp đồng này rồi", false);
                        return false;
                    }
                }

                if (HD.SoHopDong.ToUpper() != txtSoHopDong.Text.Trim().ToUpper())
                {
                    if (HD.checkSoHopDongExit(txtSoHopDong.Text.Trim(), donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI))
                    {
                        showMsg("MSG_2702050");
                        //ShowMessage("Đã có số hợp đồng này rồi", false);
                        return false;
                    }
                }

            
                this.Cursor = Cursors.WaitCursor;
                HD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                HD.MaHaiQuan = donViHaiQuanControl1.Ma;
                HD.NgayGiaHan = new DateTime(1900, 1, 1);
                HD.NgayTiepNhan = new DateTime(1900, 1, 1);
                if (HD.SoTiepNhan > 0)
                    HD.NgayTiepNhan = ccNgayTiepNhan.Value;
                HD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                HD.NgayHetHan = Convert.ToDateTime(ccNgayKetThucHD.Text);
                HD.NgayKy = Convert.ToDateTime(ccNgayKyHD.Text);
                HD.NguyenTe_ID = nguyenTeControl1.Ma;
                HD.NuocThue_ID = nuocHControl1.Ma;
                HD.SoHopDong = txtSoHopDong.Text.Trim();
                HD.NgayDangKy = ccNgayDangKy.Value;
                HD.DonViDoiTac = txtDVDT.Text.Trim();
                HD.DiaChiDoiTac = txtDCDT.Text.Trim();
                if(string.IsNullOrEmpty(HD.GUIDSTR))
                HD.GUIDSTR = Guid.NewGuid().ToString();
                if (HD.InsertUpdateHopDong())
                {
                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", HD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.HopDong);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.HopDong;
                            log.ID_DK = HD.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", HD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.HopDong);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    #endregion

                    showMsg("MSG_SAV02");
                    //ShowMessage("Đã cập nhật thành công", false);
                }
                else
                    showMsg("MSG_2702002");
                    //ShowMessage("Có lỗi khi cập nhật", false);
                setCommandStatus();
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            catch (Exception ex)
            {
               // showMsg("MSG_2702004", ex.Message);
               string   msg = setText("Dữ liệu không hợp lệ.\r\n Lỗi:  \n","Invalid input value.Error: \n");
               ShowMessage(msg  + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            return true;
        }
        private void showForm(string formName)
        {
            System.Windows.Forms.Form f = null;
            switch (formName)
            {
                case "LoaiSanPhamGCEditForm":
                    f = new LoaiSanPhamGCEditForm();
                    f.ShowDialog();
                    break;
                case "NguyenPhuLieuGCEditForm":
                    f = new NguyenPhuLieuGCEditForm();
                    f.ShowDialog();
                    break;
                case "SanPhamGCEditForm":
                    f = new SanPhamGCEditForm();
                    f.ShowDialog();
                    break;
                case "ThietBiGCEditForm":
                    f = new ThietBiGCEditForm();
                    f.ShowDialog();
                    break;
                //case "PhuKienGCEditForm":
                //    f = new PhuKienGCSendForm();
                //    f.MdiParent = this.ParentForm;
                //    f.Show(); ;
                //    break;
                //case "PhuKienGCEditForm":
                //    f = new PhuKienGCEditForm();
                //    f.Show();
                //    break;
            }
        }
        private void showFormLoaiSanPham()
        {
            LoaiSanPhamGCEditForm f = null;
            f = new LoaiSanPhamGCEditForm();
            f.HD = HD;
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.Edit;
            else
                f.OpenType = OpenFormType.View;
            f.ShowDialog();
            try
            {
                dgNhomSanPham.Refetch();
                try { dgSanPham.Refetch(); }
                catch { dgSanPham.Refresh(); }
            }
            catch
            {
                dgNhomSanPham.Refresh();
            }
        }
        private void showFormThietBi()
        {
            ThietBiGCEditForm f = null;
            f = new ThietBiGCEditForm();
            f.HD = HD;
            if (HD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();

            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }

        }
        private void showFormSanPham()
        {
            if (HD.NhomSPCollection.Count == 0)
            {
                showMsg("MSG_2702047");
                //ShowMessage("Bạn chưa nhập danh sách loại sản phẩm gia công.", false);
                return;
            }
            SanPhamGCEditForm f = null;
            f = new SanPhamGCEditForm();
            f.HD = HD;
            if (HD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();

            try
            {
                dgSanPham.Refetch();
            }
            catch
            {
                dgSanPham.Refresh();
            }
        }
        private void showFormNguyenPhuLieu()
        {
            NguyenPhuLieuGCEditForm npl = new NguyenPhuLieuGCEditForm();
            npl.HD = HD;
            if (HD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                npl.OpenType = OpenFormType.View;
            else
                npl.OpenType = OpenFormType.Edit;
            npl.ShowDialog();
            try
            {
                dgNguyenPhuLieu.Refetch();
            }
            catch
            {
                dgNguyenPhuLieu.Refresh();
            }
        }

        /*
         * Hiển thị form Hợp đồng đã đăng ký
         * 
         */
        private void showFormHopDongRegisted()
        {
            HopDongRegistedCopyForm registedHD = new HopDongRegistedCopyForm();
            registedHD.ShowDialog();
            this.HD = registedHD.HopDongSelected.copyHD();
            dgNhomSanPham.DataSource = this.HD.NhomSPCollection;
            dgSanPham.DataSource = this.HD.SPCollection;
            dgNguyenPhuLieu.DataSource = this.HD.NPLCollection;
            dgThietBi.DataSource = this.HD.TBCollection;
        }

        private void showFormDangKyDM()
        {
            DinhMucGCSendForm dm = new DinhMucGCSendForm();
            dm.ShowDialog();
        }

        //private void showFormPhuKien()
        //{
        //    if (HD.ID == 0)
        //    {
        //        ShowMessage("Phải lưu trữ hợp đồng trước", false);
        //        return;
        //    }
        //    Form[] forms = this.ParentForm.MdiChildren;
        //    for (int i = 0; i < forms.Length; i++)
        //    {
        //        if (forms[i].Name.ToString().Equals("ThemPK" + HD.ID))
        //        {
        //            forms[i].Activate();
        //            return;
        //        }
        //    }
        //    Company.Interface.KDT.GC.PhuKienGCSendForm pk = new Company.Interface.KDT.GC.PhuKienGCSendForm();
        //    pk.HD = HD;
        //    pk.phukien.HopDong_ID = HD.ID;
        //    pk.Name = "ThemPK" + HD.ID.ToString();
        //    pk.MdiParent = this.ParentForm; ;
        //    pk.Show();

        //}
        private void dgNhomSanPham_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            LoaiSanPhamGCEditForm f = null;
            f = new LoaiSanPhamGCEditForm();
            f.HD = HD;
            NhomSanPham nhom = (NhomSanPham)e.Row.DataRow;
            f.nhom = nhom;
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.Edit;
            else
                f.OpenType = OpenFormType.View;
            f.ShowDialog();
            try
            {
                dgNhomSanPham.Refetch();
                try { dgSanPham.Refetch(); }
                catch { dgSanPham.Refresh(); }
            }
            catch { dgNhomSanPham.Refresh(); }

        }

    

        private void dgNguyenPhuLieu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            NguyenPhuLieu npl = (NguyenPhuLieu)e.Row.DataRow;
            NguyenPhuLieuGCEditForm nplForm = new NguyenPhuLieuGCEditForm();
            nplForm.HD = HD;
            nplForm.npl = npl;
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                nplForm.OpenType = OpenFormType.Edit;
            else
                nplForm.OpenType = OpenFormType.View;
            nplForm.ShowDialog();
            try
            {
                dgNguyenPhuLieu.Refetch();
            }
            catch { dgNguyenPhuLieu.Refresh(); }
        }

        private void dgSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
        }

        private void dgSanPham_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhamGCEditForm f = null;
                f = new SanPhamGCEditForm();
                SanPham SPDetail = (SanPham)e.Row.DataRow;
                f.HD = HD;
                f.SPDetail = SPDetail;
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    f.OpenType = OpenFormType.Edit;
                else
                    f.OpenType = OpenFormType.View;
                f.ShowDialog();
                try
                {
                    dgSanPham.Refetch();
                }
                catch
                {
                    dgSanPham.Refresh();
                }
            }
        }

    

        private void dgThietBi_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ThietBi tbDetail = (ThietBi)e.Row.DataRow;
            tbDetail.HopDong_ID = HD.ID;

            ThietBiGCEditForm f = null;
            f = new ThietBiGCEditForm();
            f.HD = HD;
            f.tbDetail = tbDetail;
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.Edit;
            else
                f.OpenType = OpenFormType.View;
            f.ShowDialog();
            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }

        }

        private void dgThietBi_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                showMsg("MSG_2702051");
                //ShowMessage("Hợp đồng đã duyệt chính thức. Không được chỉnh sửa", false);
                e.Cancel = true;
                return;
            }
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa thiết bị này không?", true) == "Yes")
                {
                    ThietBi tb = (ThietBi)e.Row.DataRow;
                    tb.Delete();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgThietBi_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
            e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
        }

        private void dgNguyenPhuLieu_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

   
      

        private void dgNhomSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string st = "";
            GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
            foreach (GridEXSelectedItem row in items)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                foreach (SanPham sp in HD.SPCollection)
                {
                    if (sp.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                    {
                        if (st.IndexOf(sp.Ma) < 0)
                            st += sp.Ma + ";";
                    }
                }
            }
            bool ok = false;
            if (st == "")
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                {
                    ok = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                if (showMsg("MSG_240239", st, true) == "Yes")
                //if (ShowMessage("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không ?", true) == "Yes")
                {
                    ok = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            if (ok)
            {
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    if (HD.SPCollection.Count > 0)
                    {
                        SanPham sp = new SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                        SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                        foreach (SanPham spDelete in HD.SPCollection)
                        {
                            if (spDelete.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                            {
                                SPCollectionTMP.Add(spDelete);
                            }
                        }
                        foreach (SanPham spDelete in SPCollectionTMP)
                        {
                            foreach (SanPham SPHopDong in HD.SPCollection)
                            {
                                if (SPHopDong.Ma.Trim().ToUpper() == spDelete.Ma.Trim().ToUpper())
                                {
                                    HD.SPCollection.Remove(SPHopDong);
                                    break;
                                }
                            }
                        }
                    }
                    nhom.Delete();
                }
                dgSanPham.DataSource = HD.SPCollection;
                try { dgSanPham.Refetch(); }
                catch { dgSanPham.Refresh(); }
            }
            else e.Cancel = true;
        }

        private void dgNguyenPhuLieu_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgNguyenPhuLieu.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                    if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, this.HD.ID))
                    {
                        if (ShowMessage("nguyên phụ liệu " + nplDelete.Ma + " đã khai định mức. Bạn có muốn xóa không" , true) != "Yes")
                        {
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            e.Cancel = true; 
                            continue;
                        }
                    }
                    nplDelete.Delete();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa thiết bị này không?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                        tbDelete.Delete();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgSanPham.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        SanPham sp1 = (SanPham)row.GetRow().DataRow;
                        if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI,HD.ID))
                        {
                            if (showMsg("MSG_0203076", sp1.Ma, true) != "Yes")
                            {
                                e.Cancel = true;
                                return;
                            }
                            else
                            {
                                //e.Cancel = true;
                                continue;
                            }
                        }
                        sp1.Delete();
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void HopDongEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnClickEvent(object sender, EventArgs e)
        {
            Janus.Windows.EditControls.UIButton button = (Janus.Windows.EditControls.UIButton)(sender);
            switch (button.Name)
            {
                case "btnThemSPGC":
                    showFormLoaiSanPham();
                    break;
                case "btnThemNguyenPhuLieu":
                    showFormNguyenPhuLieu();
                    break;
                case "btnThemSanPham":
                    showFormSanPham();
                    break;
                case "btnThemThietBi":
                    showFormThietBi();
                    break;
                case "btnImportNPL":
                    NguyenPhuLieuImportExcel();
                    break;
                case "btnImportSP":
                    SanPhamImportExcel();
                    break;
                case "btnImportTB":
                    ThietBiImportExcel();
                    break;
            }
        }

     
        //private void uiButton1_Click(object sender, EventArgs e)
        //{
        //    GridEXSelectedItemCollection items = dgNguyenPhuLieu.SelectedItems;
        //    if (items.Count > 0)
        //    {
        //        if (showMsg("MSG_DEL01", true) == "Yes")
        //        //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
        //        {

        //            foreach (GridEXSelectedItem row in items)
        //            {
        //                Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
        //                if (HD.ID > 0)
        //                    try
        //                    {
        //                        nplDelete.Delete();
        //                    }
        //                    catch { }
        //                try
        //                {
        //                    HD.NPLCollection.Remove(nplDelete);
        //                }
        //                catch { }

        //            }
        //            try
        //            {
        //                dgNguyenPhuLieu.DataSource = HD.NPLCollection;
        //                dgNguyenPhuLieu.Refetch();
        //            }
        //            catch { dgNguyenPhuLieu.Refresh(); }
        //        }
        //    }
        //}

        private void btnXoaSP_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgSanPham.SelectedItems;
            SanPhamCollection SPCollectionTMP = new SanPhamCollection();
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        SanPham sp1 = (SanPham)row.GetRow().DataRow;
                        if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI,HD.ID))
                        {
                            if (showMsg("MSG_0203076",sp1.Ma, true) != "Yes")
                            {
                                return;
                            }
                            else
                                continue;
                        }
                        sp1.Delete();
                        SPCollectionTMP.Add(sp1);                            
                    }
                }
                try
                {
                    foreach (SanPham spDelete in SPCollectionTMP)
                        HD.SPCollection.Remove(spDelete);
                }
                catch { }
                try
                {
                    dgSanPham.DataSource = HD.SPCollection;
                    dgSanPham.Refetch();
                }
                catch { dgSanPham.Refresh(); }
            }

        }

    
        private void btnXoa_Click_1(object sender, EventArgs e)
        {
            string st = "";
            GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
            if (items.Count < 0) return;
            foreach (GridEXSelectedItem row in items)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                foreach (SanPham sp in HD.SPCollection)
                {
                    if (sp.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                    {
                        if (st.IndexOf(sp.Ma) < 0)
                            st += sp.Ma + ";";
                    }
                }
            }
            bool ok = false;
            if (st == "")
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                {
                    ok = true;
                }
            }
            else
            {
                if (showMsg("MSG_240239", st, true) == "Yes")
                //if (ShowMessage("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không ?", true) == "Yes")
                {
                    ok = true;
                }

            }
            if (ok)
            {
                NhomSanPhamCollection NhomSPCollection = new NhomSanPhamCollection();
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    NhomSPCollection.Add(nhom);
                    if (HD.SPCollection.Count > 0)
                    {
                        SanPham sp = new SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                        SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                        foreach (SanPham spDelete in HD.SPCollection)
                        {
                            if (spDelete.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                            {
                                SPCollectionTMP.Add(spDelete);
                            }
                        }
                        foreach (SanPham spDelete in SPCollectionTMP)
                        {
                            foreach (SanPham SPHopDong in HD.SPCollection)
                            {
                                if (SPHopDong.Ma.Trim().ToUpper() == spDelete.Ma.ToUpper().Trim())
                                {
                                    HD.SPCollection.Remove(SPHopDong);
                                    break;
                                }
                            }
                        }
                    }
                    nhom.Delete();
                }
                if (NhomSPCollection.Count > 0)
                {
                    foreach (NhomSanPham nhomSP in NhomSPCollection)
                    {
                        foreach (NhomSanPham nhomSPHopDong in HD.NhomSPCollection)
                        {
                            if (nhomSPHopDong.MaSanPham.Trim().ToUpper() == nhomSP.MaSanPham.Trim().ToUpper())
                            {
                                HD.NhomSPCollection.Remove(nhomSPHopDong);
                                break;
                            }
                        }

                    }
                }
                try
                {
                    dgNhomSanPham.Refetch();
                }
                catch { dgNhomSanPham.Refresh(); }
                dgSanPham.DataSource = HD.SPCollection;
                try { dgSanPham.Refetch(); }
                catch { dgSanPham.Refresh(); }
            }
        }

        private void btnXoaNPL_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgNguyenPhuLieu.SelectedItems;
            NguyenPhuLieuCollection NPLCollectionTMP = new NguyenPhuLieuCollection();
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
                {

                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                        if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, this.HD.ID))
                        {
                            if (showMsg("MSG_0203077", nplDelete.Ma, true) != "Yes")
                            {
                                return;
                            }
                            else
                                continue;
                        }
                        try
                        {
                            nplDelete.Delete();

                        }
                        catch { }
                        NPLCollectionTMP.Add(nplDelete);
                    }
                    try
                    {
                        foreach (NguyenPhuLieu NPL in NPLCollectionTMP)
                        {
                            HD.NPLCollection.Remove(NPL);
                        }
                    }
                    catch { }
                    try
                    {
                        dgNguyenPhuLieu.DataSource = HD.NPLCollection;
                        dgNguyenPhuLieu.Refetch();
                    }
                    catch { dgNguyenPhuLieu.Refresh(); }
                }
            }

        }

        //private void uiButton1_Click_1(object sender, EventArgs e)//Spham
        //{
        //    GridEXSelectedItemCollection items = dgSanPham.SelectedItems;
        //    if (items.Count < 0) return;
        //    if (showMsg("MSG_DEL01", true) == "Yes")
        //    //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
        //    {


        //        foreach (GridEXSelectedItem row in items)
        //        {
        //            SanPham sp1 = (SanPham)row.GetRow().DataRow;
        //            if (row.RowType == RowType.Record)
        //            {
        //                if (HD.ID > 0)
        //                    sp1.Delete();
        //                try
        //                {
        //                    HD.SPCollection.Remove(sp1);
        //                }
        //                catch { }
        //            }
        //        }
        //        try
        //        {
        //            dgSanPham.DataSource = HD.SPCollection;
        //            dgSanPham.Refetch();
        //        }
        //        catch { dgSanPham.Refresh(); }
        //    }
        //}

        private void btnXoaTB_Click_1(object sender, EventArgs e)
        {
            Company.GC.BLL.KDT.GC.ThietBiCollection tbColl = new ThietBiCollection();
            GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
            if (items.Count > 0)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa thiết bị này không?", true) == "Yes")
                {

                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                        if (HD.ID > 0)
                            try
                            {
                                tbDelete.Delete();
                            }
                            catch { }
                        tbColl.Add(tbDelete);
                    }
                    foreach (Company.GC.BLL.KDT.GC.ThietBi tbt in tbColl)
                    {
                        HD.TBCollection.Remove(tbt);
                    }

                    try
                    {
                        dgThietBi.DataSource = HD.TBCollection;
                        dgThietBi.Refetch();
                    }
                    catch { dgThietBi.Refresh(); }
                }
            }


        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            TinhToanNhuCauNguyenPhuLieuForm f = new TinhToanNhuCauNguyenPhuLieuForm();
            f.HD = HD;
            f.ShowDialog();
        }
    }
}
