﻿namespace Company.Interface
{
    partial class BaoCaoThongKeNuoc_Hang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoThongKeNuoc_Hang));
            Janus.Windows.GridEX.GridEXLayout grdList2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cboNuoc = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CbTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.CbDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.btnXemBC = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbLuuMacdinh = new Janus.Windows.EditControls.UICheckBox();
            this.txtDVBC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNguoiDB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNguoiLB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grdList = new Janus.Windows.GridEX.GridEX();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage_MatHang = new Janus.Windows.UI.Tab.UITabPage();
            this.btnViewChart = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage_DoiTac = new Janus.Windows.UI.Tab.UITabPage();
            this.btnView2 = new Janus.Windows.EditControls.UIButton();
            this.grdList2 = new Janus.Windows.GridEX.GridEX();
            this.compareValidator1 = new Company.Controls.CustomValidation.CompareValidator();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage_MatHang.SuspendLayout();
            this.uiTabPage_DoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(841, 438);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cboNuoc);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.CbTuNgay);
            this.uiGroupBox1.Controls.Add(this.CbDenNgay);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 10);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(374, 126);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Kỳ thực hiện thống kê-báo cáo";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cboNuoc
            // 
            this.cboNuoc.Location = new System.Drawing.Point(76, 22);
            this.cboNuoc.Name = "cboNuoc";
            this.cboNuoc.Size = new System.Drawing.Size(284, 21);
            this.cboNuoc.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nước NK";
            // 
            // CbTuNgay
            // 
            // 
            // 
            // 
            this.CbTuNgay.DropDownCalendar.Name = "";
            this.CbTuNgay.Location = new System.Drawing.Point(76, 47);
            this.CbTuNgay.Name = "CbTuNgay";
            this.CbTuNgay.Size = new System.Drawing.Size(103, 21);
            this.CbTuNgay.TabIndex = 3;
            // 
            // CbDenNgay
            // 
            // 
            // 
            // 
            this.CbDenNgay.DropDownCalendar.Name = "";
            this.CbDenNgay.Location = new System.Drawing.Point(257, 49);
            this.CbDenNgay.Name = "CbDenNgay";
            this.CbDenNgay.Size = new System.Drawing.Size(103, 21);
            this.CbDenNgay.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(188, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Đến Ngày";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Từ Ngày";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.Location = new System.Drawing.Point(319, 89);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(102, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Thống kê";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.WordWrap = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnXemBC
            // 
            this.btnXemBC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemBC.Enabled = false;
            this.btnXemBC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemBC.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXemBC.Location = new System.Drawing.Point(688, 4);
            this.btnXemBC.Name = "btnXemBC";
            this.btnXemBC.Size = new System.Drawing.Size(102, 23);
            this.btnXemBC.TabIndex = 1;
            this.btnXemBC.Text = "Xem Báo cáo";
            this.btnXemBC.VisualStyleManager = this.vsmMain;
            this.btnXemBC.WordWrap = false;
            this.btnXemBC.Click += new System.EventHandler(this.btnXemBC_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ckbLuuMacdinh);
            this.uiGroupBox2.Controls.Add(this.txtDVBC);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.btnSearch);
            this.uiGroupBox2.Controls.Add(this.txtNguoiDB);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtNguoiLB);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(392, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(437, 128);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ckbLuuMacdinh
            // 
            this.ckbLuuMacdinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbLuuMacdinh.Location = new System.Drawing.Point(12, 94);
            this.ckbLuuMacdinh.Name = "ckbLuuMacdinh";
            this.ckbLuuMacdinh.Size = new System.Drawing.Size(279, 18);
            this.ckbLuuMacdinh.TabIndex = 6;
            this.ckbLuuMacdinh.Text = "Lưu mặc định";
            this.ckbLuuMacdinh.CheckedChanged += new System.EventHandler(this.ckbLuuMacdinh_CheckedChanged);
            // 
            // txtDVBC
            // 
            this.txtDVBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDVBC.Location = new System.Drawing.Point(117, 14);
            this.txtDVBC.Name = "txtDVBC";
            this.txtDVBC.Size = new System.Drawing.Size(303, 21);
            this.txtDVBC.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Đơn vị báo cáo";
            // 
            // txtNguoiDB
            // 
            this.txtNguoiDB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiDB.Location = new System.Drawing.Point(118, 62);
            this.txtNguoiDB.Name = "txtNguoiDB";
            this.txtNguoiDB.Size = new System.Drawing.Size(303, 21);
            this.txtNguoiDB.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Người duyệt biểu";
            // 
            // txtNguoiLB
            // 
            this.txtNguoiLB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiLB.Location = new System.Drawing.Point(118, 38);
            this.txtNguoiLB.Name = "txtNguoiLB";
            this.txtNguoiLB.Size = new System.Drawing.Size(303, 21);
            this.txtNguoiLB.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Người lập biểu";
            // 
            // grdList
            // 
            this.grdList.AllowColumnDrag = false;
            this.grdList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            grdList_DesignTimeLayout.LayoutString = resources.GetString("grdList_DesignTimeLayout.LayoutString");
            this.grdList.DesignTimeLayout = grdList_DesignTimeLayout;
            this.grdList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdList.GroupByBoxFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdList.GroupByBoxFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grdList.GroupByBoxVisible = false;
            this.grdList.GroupRowFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.grdList.GroupRowFormatStyle.BackColorGradient = System.Drawing.Color.Empty;
            this.grdList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grdList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdList.Location = new System.Drawing.Point(7, 32);
            this.grdList.Name = "grdList";
            this.grdList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdList.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.False;
            this.grdList.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.grdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.Size = new System.Drawing.Size(783, 229);
            this.grdList.TabIndex = 2;
            this.grdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdList.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.Location = new System.Drawing.Point(12, 142);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(817, 290);
            this.uiTab1.TabIndex = 3;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage_MatHang,
            this.uiTabPage_DoiTac});
            this.uiTab1.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiTab1.TabsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.DimGray;
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage_MatHang
            // 
            this.uiTabPage_MatHang.Controls.Add(this.btnViewChart);
            this.uiTabPage_MatHang.Controls.Add(this.grdList);
            this.uiTabPage_MatHang.Controls.Add(this.btnXemBC);
            this.uiTabPage_MatHang.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage_MatHang.Name = "uiTabPage_MatHang";
            this.uiTabPage_MatHang.Size = new System.Drawing.Size(797, 268);
            this.uiTabPage_MatHang.TabStop = true;
            this.uiTabPage_MatHang.Text = "Mỗi Mặt Hàng Của Mỗi Nước";
            // 
            // btnViewChart
            // 
            this.btnViewChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewChart.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewChart.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnViewChart.Location = new System.Drawing.Point(580, 4);
            this.btnViewChart.Name = "btnViewChart";
            this.btnViewChart.Size = new System.Drawing.Size(102, 23);
            this.btnViewChart.TabIndex = 0;
            this.btnViewChart.Text = "&Xem Biểu đồ";
            this.btnViewChart.Visible = false;
            this.btnViewChart.VisualStyleManager = this.vsmMain;
            this.btnViewChart.WordWrap = false;
            this.btnViewChart.Click += new System.EventHandler(this.btnViewChart_Click);
            // 
            // uiTabPage_DoiTac
            // 
            this.uiTabPage_DoiTac.Controls.Add(this.btnView2);
            this.uiTabPage_DoiTac.Controls.Add(this.grdList2);
            this.uiTabPage_DoiTac.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage_DoiTac.Name = "uiTabPage_DoiTac";
            this.uiTabPage_DoiTac.Size = new System.Drawing.Size(815, 268);
            this.uiTabPage_DoiTac.TabStop = true;
            this.uiTabPage_DoiTac.Text = " Mỗi Đối Tác Của Mỗi Nước";
            // 
            // btnView2
            // 
            this.btnView2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView2.Enabled = false;
            this.btnView2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView2.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnView2.Location = new System.Drawing.Point(705, 3);
            this.btnView2.Name = "btnView2";
            this.btnView2.Size = new System.Drawing.Size(102, 23);
            this.btnView2.TabIndex = 0;
            this.btnView2.Text = "Xem Báo cáo";
            this.btnView2.VisualStyleManager = this.vsmMain;
            this.btnView2.WordWrap = false;
            this.btnView2.Click += new System.EventHandler(this.btnView2_Click);
            // 
            // grdList2
            // 
            this.grdList2.AllowColumnDrag = false;
            this.grdList2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdList2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            grdList2_DesignTimeLayout.LayoutString = resources.GetString("grdList2_DesignTimeLayout.LayoutString");
            this.grdList2.DesignTimeLayout = grdList2_DesignTimeLayout;
            this.grdList2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdList2.GroupByBoxVisible = false;
            this.grdList2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList2.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grdList2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdList2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdList2.Location = new System.Drawing.Point(8, 32);
            this.grdList2.Name = "grdList2";
            this.grdList2.RowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.False;
            this.grdList2.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdList2.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.False;
            this.grdList2.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.grdList2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList2.Size = new System.Drawing.Size(800, 228);
            this.grdList2.TabIndex = 1;
            this.grdList2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdList2.VisualStyleManager = this.vsmMain;
            // 
            // compareValidator1
            // 
            this.compareValidator1.ControlToCompare = this.CbTuNgay;
            this.compareValidator1.ControlToValidate = this.CbDenNgay;
            this.compareValidator1.ErrorMessage = "Đến Ngày phải lớn hơn Từ ngày";
            this.compareValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("compareValidator1.Icon")));
            this.compareValidator1.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            // 
            // BaoCaoThongKeNuoc_Hang
            // 
            this.ClientSize = new System.Drawing.Size(841, 438);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoThongKeNuoc_Hang";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thống Kê Kim Ngạch Nhập Khẩu Mỗi Nước";
            this.Load += new System.EventHandler(this.BaoCaoThongKeNuoc_Hang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage_MatHang.ResumeLayout(false);
            this.uiTabPage_DoiTac.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.CalendarCombo.CalendarCombo CbTuNgay;
        private Janus.Windows.EditControls.UIButton btnXemBC;
        private Janus.Windows.CalendarCombo.CalendarCombo CbDenNgay;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.TextBox txtNguoiDB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNguoiLB;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.GridEX grdList;
        private Janus.Windows.EditControls.UIComboBox cboNuoc;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage_MatHang;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage_DoiTac;
        private Janus.Windows.GridEX.GridEX grdList2;
        private Janus.Windows.EditControls.UIButton btnView2;
        private Company.Controls.CustomValidation.CompareValidator compareValidator1;
        private Janus.Windows.EditControls.UIButton btnViewChart;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private System.Windows.Forms.TextBox txtDVBC;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UICheckBox ckbLuuMacdinh;
    }
}
