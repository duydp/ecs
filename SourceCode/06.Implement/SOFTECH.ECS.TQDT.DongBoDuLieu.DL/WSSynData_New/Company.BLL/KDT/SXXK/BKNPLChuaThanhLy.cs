using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.BLL.KDT.SXXK
{
    public partial class BKNPLChuaThanhLy
    {
        public DonViTinhQuyDoi DVT_QuyDoi { get; set; }
        public decimal SoToKhaiVNACCS { get; set; }
        public override bool Equals(object obj)
        {
            BKNPLChuaThanhLy item = (BKNPLChuaThanhLy)obj;
            bool isEqual = item.SoToKhai == SoToKhai && item.MaLoaiHinh.Trim() == MaLoaiHinh.Trim() &&
                item.MaHaiQuan == MaHaiQuan && item.NamDangKy == NamDangKy;
            return isEqual;
        }
    }
}