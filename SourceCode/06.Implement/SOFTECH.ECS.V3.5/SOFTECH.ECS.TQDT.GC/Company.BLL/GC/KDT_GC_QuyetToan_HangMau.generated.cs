﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_QuyetToan_HangMau : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public string Ma { set; get; }
		public string Ten { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal LuongNhapDK { set; get; }
		public decimal TriGiaNhapDK { set; get; }
		public decimal LuongSuDungDK { set; get; }
		public decimal TriGiaSuDungDK { set; get; }
		public decimal LuongNhapTK { set; get; }
		public decimal TriGiaNhapTK { set; get; }
		public decimal LuongSuDungTK { set; get; }
		public decimal TriGiaSuDungTK { set; get; }
		public decimal LuongSuDungNK { set; get; }
		public decimal TriGiaSuDungNK { set; get; }
		public decimal LuongNhapNK { set; get; }
		public decimal TriGiaNhapNK { set; get; }
		public decimal LuongTonCK { set; get; }
		public decimal TriGiaTonCK { set; get; }
		public string NamQT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_QuyetToan_HangMau> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_QuyetToan_HangMau> collection = new List<KDT_GC_QuyetToan_HangMau>();
			while (reader.Read())
			{
				KDT_GC_QuyetToan_HangMau entity = new KDT_GC_QuyetToan_HangMau();
				//if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapDK"))) entity.LuongNhapDK = reader.GetDecimal(reader.GetOrdinal("LuongNhapDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapDK"))) entity.TriGiaNhapDK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungDK"))) entity.LuongSuDungDK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungDK"))) entity.TriGiaSuDungDK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapTK"))) entity.LuongNhapTK = reader.GetDecimal(reader.GetOrdinal("LuongNhapTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapTK"))) entity.TriGiaNhapTK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungTK"))) entity.LuongSuDungTK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungTK"))) entity.TriGiaSuDungTK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungNK"))) entity.LuongSuDungNK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungNK"))) entity.TriGiaSuDungNK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapNK"))) entity.LuongNhapNK = reader.GetDecimal(reader.GetOrdinal("LuongNhapNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapNK"))) entity.TriGiaNhapNK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCK"))) entity.LuongTonCK = reader.GetDecimal(reader.GetOrdinal("LuongTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTonCK"))) entity.TriGiaTonCK = reader.GetDecimal(reader.GetOrdinal("TriGiaTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamQT"))) entity.NamQT = reader.GetString(reader.GetOrdinal("NamQT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_QuyetToan_HangMau> collection, long id)
        {
            foreach (KDT_GC_QuyetToan_HangMau item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_HangMau VALUES(@HopDong_ID, @Ma, @Ten, @MaHS, @DVT_ID, @LuongNhapDK, @TriGiaNhapDK, @LuongSuDungDK, @TriGiaSuDungDK, @LuongNhapTK, @TriGiaNhapTK, @LuongSuDungTK, @TriGiaSuDungTK, @LuongSuDungNK, @TriGiaSuDungNK, @LuongNhapNK, @TriGiaNhapNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_HangMau SET HopDong_ID = @HopDong_ID, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNhapDK = @LuongNhapDK, TriGiaNhapDK = @TriGiaNhapDK, LuongSuDungDK = @LuongSuDungDK, TriGiaSuDungDK = @TriGiaSuDungDK, LuongNhapTK = @LuongNhapTK, TriGiaNhapTK = @TriGiaNhapTK, LuongSuDungTK = @LuongSuDungTK, TriGiaSuDungTK = @TriGiaSuDungTK, LuongSuDungNK = @LuongSuDungNK, TriGiaSuDungNK = @TriGiaSuDungNK, LuongNhapNK = @LuongNhapNK, TriGiaNhapNK = @TriGiaNhapNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_HangMau WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_HangMau VALUES(@HopDong_ID, @Ma, @Ten, @MaHS, @DVT_ID, @LuongNhapDK, @TriGiaNhapDK, @LuongSuDungDK, @TriGiaSuDungDK, @LuongNhapTK, @TriGiaNhapTK, @LuongSuDungTK, @TriGiaSuDungTK, @LuongSuDungNK, @TriGiaSuDungNK, @LuongNhapNK, @TriGiaNhapNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_HangMau SET HopDong_ID = @HopDong_ID, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNhapDK = @LuongNhapDK, TriGiaNhapDK = @TriGiaNhapDK, LuongSuDungDK = @LuongSuDungDK, TriGiaSuDungDK = @TriGiaSuDungDK, LuongNhapTK = @LuongNhapTK, TriGiaNhapTK = @TriGiaNhapTK, LuongSuDungTK = @LuongSuDungTK, TriGiaSuDungTK = @TriGiaSuDungTK, LuongSuDungNK = @LuongSuDungNK, TriGiaSuDungNK = @TriGiaSuDungNK, LuongNhapNK = @LuongNhapNK, TriGiaNhapNK = @TriGiaNhapNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_HangMau WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_QuyetToan_HangMau Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_QuyetToan_HangMau> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_QuyetToan_HangMau> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_QuyetToan_HangMau> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
        public List<KDT_GC_QuyetToan_HangMau> SelectCollectionDynamicByHopDong_ID(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamicByHD_ID(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }
        public static IDataReader SelectReaderDynamicByHD_ID(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_SelectDynamicByHD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_QuyetToan_HangMau(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_HangMau entity = new KDT_GC_QuyetToan_HangMau();	
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_QuyetToan_HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_HangMau item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_QuyetToan_HangMau(long id, long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_HangMau entity = new KDT_GC_QuyetToan_HangMau();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_QuyetToan_HangMau_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_QuyetToan_HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_HangMau item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_QuyetToan_HangMau(long id, long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_HangMau entity = new KDT_GC_QuyetToan_HangMau();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_QuyetToan_HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_HangMau item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_QuyetToan_HangMau(long id)
		{
			KDT_GC_QuyetToan_HangMau entity = new KDT_GC_QuyetToan_HangMau();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public  int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_HangMau_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_QuyetToan_HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_HangMau item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}