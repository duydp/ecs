﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.Interface.Report;

namespace Company.Interface
{
    public class ProcessReport
    {


        public static void ShowReport(long idMsg, string MaNghiepVu)
        {
            MsgLog Log = MsgLog.Load(idMsg);
            if (Log != null)
            {
                try
                {
                ReturnMessages rMsg = new ReturnMessages(Log.Log_Messages);
                if (rMsg != null)
                {
                    if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan.Contains(MaNghiepVu))
                    {
                        VAE1LD0 vae = new VAE1LD0();
                        vae.LoadVAE1LD0(rMsg.Body.ToString());
                        ReportViewVNACCSToKhaiXuat f = new ReportViewVNACCSToKhaiXuat(false);
                        f.VAE = vae;
                        f.ShowDialog();
                    }
                    else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanXacNhan.Contains(MaNghiepVu))
                    {
                        VAD1AC0 vad = new VAD1AC0();
                        vad.LoadVAD1AC0(rMsg.Body.ToString());
                        ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(false);
                        f.Vad1ac0 = vad;
                        f.ShowDialog();

                    }
                    else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan.Contains(MaNghiepVu))
                    {
                        VAE1LD0 vae = new VAE1LD0();
                        vae.LoadVAE1LD0(rMsg.Body.ToString());
                        ReportViewVNACCSToKhaiXuat f = new ReportViewVNACCSToKhaiXuat(false);
                        f.VAE = vae;
                        f.ShowDialog();
                    }
                }
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }


            }
            



        }




    }
}
