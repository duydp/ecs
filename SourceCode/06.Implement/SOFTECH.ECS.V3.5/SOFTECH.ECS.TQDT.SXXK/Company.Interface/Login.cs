﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;

namespace Company.Interface
{
    public partial class Login : Form
    {
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        ShowMessage("Đăng nhập không thành công.", false);

                    }
                    else
                    {
                        ShowMessage("Login unsuccessfully", false);
                    }
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    //TODO: VNACCS---------------------------------------------------------------------------------------------
                    if (MainForm.isUseSettingVNACC)
                    {
                        //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                        Company.KDT.SHARE.Components.SQL.CheckUserTableIsHaveVNACCS(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                        //TODO: Chay store cap nhat trang thai cua Terminal.
                        Company.QuanTri.User userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                        userVNACC.Update_VNACC();

                        try
                        {
                            //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                            Company.KDT.SHARE.Components.SQL.CheckAndCreateProcedureCheckTerminalOnline(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                            //TODO: Chay store cap nhat trang thai cua Terminal.
                            User.CheckAndUpdateTerminalStatus();
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                        //TODO: Kiem tra cho phep 1 USER duoc phep truy cap duy nhat.
                        //userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        //if (userVNACC.isOnline && userVNACC.IP != Company.KDT.SHARE.Components.Globals.GetLocalIPAddress())
                        //{
                        //    string msg = string.Format("Tài khoản người sử dụng '{0}' này đang kết nối khai báo.\r\n\n- Máy đang kết nối: {1} - {2}\r\n\nBạn vui lòng kiểm tra lại hoặc đăng nhập với tài khoản người sử dụng khác.", userVNACC.USER_NAME, userVNACC.IP, userVNACC.HostName);
                        //    ShowMessage(msg, false);
                        //    MainForm.isLoginSuccess = false;
                        //    return;
                        //}

                        //TODO: Cap nhat IP, HostName cua PC dang nhap vao bang USER
                        userVNACC.HostName = System.Environment.MachineName;
                        userVNACC.IP = Company.KDT.SHARE.Components.Globals.GetLocalIPAddress();
                        userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                        userVNACC.Update_VNACC();


                        //TODO: Kiem tra Terminal co cung ID co dang su dung khong truoc khi dang nhap nguoi dung. Chi cho phep 1 ternimal ID duoc phep khai bao.                    
                        if (Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID) > 1
                            || Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID, userVNACC.USER_NAME) > 1)
                        {
                            ShowMessage("Phát hiện có 1 Terminal '" + userVNACC.VNACC_TerminalID + "' khác đang kết nối khai báo. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);

                            userVNACC.VNACC_TerminalID = string.Empty;
                            userVNACC.VNACC_TerminalPass = string.Empty;
                            userVNACC.VNACC_UserID = string.Empty;
                            userVNACC.VNACC_UserCode = string.Empty;
                            userVNACC.VNACC_UserPass = string.Empty;
                            userVNACC.Update_VNACC();
                        }

                        //Cap nhat bien toan cuc VNACC cho chuong trinh
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = userVNACC.VNACC_TerminalID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = userVNACC.VNACC_TerminalPass != null ? userVNACC.VNACC_TerminalPass.Trim() : "";
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = userVNACC.VNACC_UserID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = userVNACC.VNACC_UserCode;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = userVNACC.VNACC_UserPass != null ? userVNACC.VNACC_UserPass.Trim() : "";
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCAddress", @"https://ediconn.vnaccs.customs.gov.vn/test");
                    }

                    MainForm.isLoginSuccess = true;
                    GlobalSettings.UserLog = txtUser.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserLog", txtUser.Text.Trim());
                    GlobalSettings.PassLog = txtMatKhau.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassLog", txtMatKhau.Text.Trim());
                    GlobalSettings.RefreshKey();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Không kết nối tới cơ sở dữ liệu", false);
                //MessageBox.Show("Không kết nối tới cơ sở dữ liệu");
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = string.Empty;
            txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            if (GlobalSettings.NGON_NGU == "0")
            {

                linkLabel1.Text = "Cấu hình thông số kết nối ";
                uiButton3.Text = "Đăng nhập";
                uiButton2.Text = "Thoát";


                if (System.IO.File.Exists("loginv.png"))
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("loginv.png");

                }

                //  this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
            else
            {
                linkLabel1.Text = "Connected configuration";
                uiButton3.Text = "Login";
                uiButton2.Text = "Close";

                if (System.IO.File.Exists("logine.png"))
                    this.BackgroundImage = System.Drawing.Image.FromFile("logine.png");
                //this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
        }




    }
}