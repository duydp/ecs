﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    /// <summary>
    /// Doanh nghiệp được đảm bảo nghĩa vụ nộp thuế
    /// </summary>
    public class TaxGuaranteeAgent : IssueBase
    {

        ///<summary>
        /// Loại bảo lãnh. 1: bảo lãnh riêng, 2: bão lãnh chung
        ///</summary>
        [XmlElement("type")]
        public string Type { set; get; }
        ///<summary>
        /// Đơn vị bảo lãnh
        ///</summary>
        [XmlElement("identityGuarantor")]
        public string IdentityGuarantor { set; get; }
        ///<summary>
        ///Số năm được bảo lãnh
        ///</summary>
        [XmlElement("year")]
        public string Year { set; get; }
        ///<summary>
        ///Số dư tiền bảo lãnh
        ///</summary>
        [XmlElement("remainValue")]
        public string RemainValue { set; get; }
        ///<summary>
        ///Số tiền bảo lãnh
        ///</summary>
        [XmlElement("value")]
        public string Value { set; get; }
        ///<summary>
        ///Số ngày được bảo lãnh
        ///</summary>
        [XmlElement("dayValue")]
        public string DayValue { set; get; }
        ///<summary>
        ///Ngày hết hạn
        ///</summary>
        [XmlElement("expire")]
        public string Expire { set; get; }
        ///<summary>
        /// Ngày ký bảo lãnh
        ///</summary>
        [XmlElement("signDate")]
        public string SignDate { set; get; }

      

    }
}
