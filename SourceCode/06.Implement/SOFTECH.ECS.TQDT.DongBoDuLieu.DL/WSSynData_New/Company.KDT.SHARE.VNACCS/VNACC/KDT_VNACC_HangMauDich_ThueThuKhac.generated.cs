using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich_ThueThuKhac : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_id { set; get; }
		public string MaTSThueThuKhac { set; get; }
		public string MaMGThueThuKhac { set; get; }
		public decimal SoTienGiamThueThuKhac { set; get; }
		public string TenKhoanMucThueVaThuKhac { set; get; }
		public decimal TriGiaTinhThueVaThuKhac { set; get; }
		public decimal SoLuongTinhThueVaThuKhac { set; get; }
		public string MaDVTDanhThueVaThuKhac { set; get; }
		public string ThueSuatThueVaThuKhac { set; get; }
		public decimal SoTienThueVaThuKhac { set; get; }
		public string DieuKhoanMienGiamThueVaThuKhac { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangMauDich_ThueThuKhac> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangMauDich_ThueThuKhac> collection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
			while (reader.Read())
			{
				KDT_VNACC_HangMauDich_ThueThuKhac entity = new KDT_VNACC_HangMauDich_ThueThuKhac();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_id"))) entity.Master_id = reader.GetInt64(reader.GetOrdinal("Master_id"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTSThueThuKhac"))) entity.MaTSThueThuKhac = reader.GetString(reader.GetOrdinal("MaTSThueThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMGThueThuKhac"))) entity.MaMGThueThuKhac = reader.GetString(reader.GetOrdinal("MaMGThueThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienGiamThueThuKhac"))) entity.SoTienGiamThueThuKhac = reader.GetDecimal(reader.GetOrdinal("SoTienGiamThueThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoanMucThueVaThuKhac"))) entity.TenKhoanMucThueVaThuKhac = reader.GetString(reader.GetOrdinal("TenKhoanMucThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueVaThuKhac"))) entity.TriGiaTinhThueVaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThueVaThuKhac"))) entity.SoLuongTinhThueVaThuKhac = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTDanhThueVaThuKhac"))) entity.MaDVTDanhThueVaThuKhac = reader.GetString(reader.GetOrdinal("MaDVTDanhThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatThueVaThuKhac"))) entity.ThueSuatThueVaThuKhac = reader.GetString(reader.GetOrdinal("ThueSuatThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThueVaThuKhac"))) entity.SoTienThueVaThuKhac = reader.GetDecimal(reader.GetOrdinal("SoTienThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuKhoanMienGiamThueVaThuKhac"))) entity.DieuKhoanMienGiamThueVaThuKhac = reader.GetString(reader.GetOrdinal("DieuKhoanMienGiamThueVaThuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangMauDich_ThueThuKhac> collection, long id)
        {
            foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_ThueThuKhac VALUES(@Master_id, @MaTSThueThuKhac, @MaMGThueThuKhac, @SoTienGiamThueThuKhac, @TenKhoanMucThueVaThuKhac, @TriGiaTinhThueVaThuKhac, @SoLuongTinhThueVaThuKhac, @MaDVTDanhThueVaThuKhac, @ThueSuatThueVaThuKhac, @SoTienThueVaThuKhac, @DieuKhoanMienGiamThueVaThuKhac)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_ThueThuKhac SET Master_id = @Master_id, MaTSThueThuKhac = @MaTSThueThuKhac, MaMGThueThuKhac = @MaMGThueThuKhac, SoTienGiamThueThuKhac = @SoTienGiamThueThuKhac, TenKhoanMucThueVaThuKhac = @TenKhoanMucThueVaThuKhac, TriGiaTinhThueVaThuKhac = @TriGiaTinhThueVaThuKhac, SoLuongTinhThueVaThuKhac = @SoLuongTinhThueVaThuKhac, MaDVTDanhThueVaThuKhac = @MaDVTDanhThueVaThuKhac, ThueSuatThueVaThuKhac = @ThueSuatThueVaThuKhac, SoTienThueVaThuKhac = @SoTienThueVaThuKhac, DieuKhoanMienGiamThueVaThuKhac = @DieuKhoanMienGiamThueVaThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_ThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, "MaTSThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, "MaMGThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, "SoTienGiamThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, "TenKhoanMucThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, "TriGiaTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, "SoLuongTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, "MaDVTDanhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, "ThueSuatThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, "SoTienThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, "DieuKhoanMienGiamThueVaThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, "MaTSThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, "MaMGThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, "SoTienGiamThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, "TenKhoanMucThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, "TriGiaTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, "SoLuongTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, "MaDVTDanhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, "ThueSuatThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, "SoTienThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, "DieuKhoanMienGiamThueVaThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_ThueThuKhac VALUES(@Master_id, @MaTSThueThuKhac, @MaMGThueThuKhac, @SoTienGiamThueThuKhac, @TenKhoanMucThueVaThuKhac, @TriGiaTinhThueVaThuKhac, @SoLuongTinhThueVaThuKhac, @MaDVTDanhThueVaThuKhac, @ThueSuatThueVaThuKhac, @SoTienThueVaThuKhac, @DieuKhoanMienGiamThueVaThuKhac)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_ThueThuKhac SET Master_id = @Master_id, MaTSThueThuKhac = @MaTSThueThuKhac, MaMGThueThuKhac = @MaMGThueThuKhac, SoTienGiamThueThuKhac = @SoTienGiamThueThuKhac, TenKhoanMucThueVaThuKhac = @TenKhoanMucThueVaThuKhac, TriGiaTinhThueVaThuKhac = @TriGiaTinhThueVaThuKhac, SoLuongTinhThueVaThuKhac = @SoLuongTinhThueVaThuKhac, MaDVTDanhThueVaThuKhac = @MaDVTDanhThueVaThuKhac, ThueSuatThueVaThuKhac = @ThueSuatThueVaThuKhac, SoTienThueVaThuKhac = @SoTienThueVaThuKhac, DieuKhoanMienGiamThueVaThuKhac = @DieuKhoanMienGiamThueVaThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_ThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, "MaTSThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, "MaMGThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, "SoTienGiamThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, "TenKhoanMucThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, "TriGiaTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, "SoLuongTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, "MaDVTDanhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, "ThueSuatThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, "SoTienThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, "DieuKhoanMienGiamThueVaThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, "MaTSThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, "MaMGThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, "SoTienGiamThueThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, "TenKhoanMucThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, "TriGiaTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, "SoLuongTinhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, "MaDVTDanhThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, "ThueSuatThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, "SoTienThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, "DieuKhoanMienGiamThueVaThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangMauDich_ThueThuKhac Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangMauDich_ThueThuKhac> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangMauDich_ThueThuKhac> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangMauDich_ThueThuKhac> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangMauDich_ThueThuKhac(long master_id, string maTSThueThuKhac, string maMGThueThuKhac, decimal soTienGiamThueThuKhac, string tenKhoanMucThueVaThuKhac, decimal triGiaTinhThueVaThuKhac, decimal soLuongTinhThueVaThuKhac, string maDVTDanhThueVaThuKhac, string thueSuatThueVaThuKhac, decimal soTienThueVaThuKhac, string dieuKhoanMienGiamThueVaThuKhac)
		{
			KDT_VNACC_HangMauDich_ThueThuKhac entity = new KDT_VNACC_HangMauDich_ThueThuKhac();	
			entity.Master_id = master_id;
			entity.MaTSThueThuKhac = maTSThueThuKhac;
			entity.MaMGThueThuKhac = maMGThueThuKhac;
			entity.SoTienGiamThueThuKhac = soTienGiamThueThuKhac;
			entity.TenKhoanMucThueVaThuKhac = tenKhoanMucThueVaThuKhac;
			entity.TriGiaTinhThueVaThuKhac = triGiaTinhThueVaThuKhac;
			entity.SoLuongTinhThueVaThuKhac = soLuongTinhThueVaThuKhac;
			entity.MaDVTDanhThueVaThuKhac = maDVTDanhThueVaThuKhac;
			entity.ThueSuatThueVaThuKhac = thueSuatThueVaThuKhac;
			entity.SoTienThueVaThuKhac = soTienThueVaThuKhac;
			entity.DieuKhoanMienGiamThueVaThuKhac = dieuKhoanMienGiamThueVaThuKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, MaTSThueThuKhac);
			db.AddInParameter(dbCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, MaMGThueThuKhac);
			db.AddInParameter(dbCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, SoTienGiamThueThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, TenKhoanMucThueVaThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, TriGiaTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, SoLuongTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, MaDVTDanhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, ThueSuatThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, SoTienThueVaThuKhac);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, DieuKhoanMienGiamThueVaThuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangMauDich_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangMauDich_ThueThuKhac(long id, long master_id, string maTSThueThuKhac, string maMGThueThuKhac, decimal soTienGiamThueThuKhac, string tenKhoanMucThueVaThuKhac, decimal triGiaTinhThueVaThuKhac, decimal soLuongTinhThueVaThuKhac, string maDVTDanhThueVaThuKhac, string thueSuatThueVaThuKhac, decimal soTienThueVaThuKhac, string dieuKhoanMienGiamThueVaThuKhac)
		{
			KDT_VNACC_HangMauDich_ThueThuKhac entity = new KDT_VNACC_HangMauDich_ThueThuKhac();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.MaTSThueThuKhac = maTSThueThuKhac;
			entity.MaMGThueThuKhac = maMGThueThuKhac;
			entity.SoTienGiamThueThuKhac = soTienGiamThueThuKhac;
			entity.TenKhoanMucThueVaThuKhac = tenKhoanMucThueVaThuKhac;
			entity.TriGiaTinhThueVaThuKhac = triGiaTinhThueVaThuKhac;
			entity.SoLuongTinhThueVaThuKhac = soLuongTinhThueVaThuKhac;
			entity.MaDVTDanhThueVaThuKhac = maDVTDanhThueVaThuKhac;
			entity.ThueSuatThueVaThuKhac = thueSuatThueVaThuKhac;
			entity.SoTienThueVaThuKhac = soTienThueVaThuKhac;
			entity.DieuKhoanMienGiamThueVaThuKhac = dieuKhoanMienGiamThueVaThuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, MaTSThueThuKhac);
			db.AddInParameter(dbCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, MaMGThueThuKhac);
			db.AddInParameter(dbCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, SoTienGiamThueThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, TenKhoanMucThueVaThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, TriGiaTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, SoLuongTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, MaDVTDanhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, ThueSuatThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, SoTienThueVaThuKhac);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, DieuKhoanMienGiamThueVaThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangMauDich_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangMauDich_ThueThuKhac(long id, long master_id, string maTSThueThuKhac, string maMGThueThuKhac, decimal soTienGiamThueThuKhac, string tenKhoanMucThueVaThuKhac, decimal triGiaTinhThueVaThuKhac, decimal soLuongTinhThueVaThuKhac, string maDVTDanhThueVaThuKhac, string thueSuatThueVaThuKhac, decimal soTienThueVaThuKhac, string dieuKhoanMienGiamThueVaThuKhac)
		{
			KDT_VNACC_HangMauDich_ThueThuKhac entity = new KDT_VNACC_HangMauDich_ThueThuKhac();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.MaTSThueThuKhac = maTSThueThuKhac;
			entity.MaMGThueThuKhac = maMGThueThuKhac;
			entity.SoTienGiamThueThuKhac = soTienGiamThueThuKhac;
			entity.TenKhoanMucThueVaThuKhac = tenKhoanMucThueVaThuKhac;
			entity.TriGiaTinhThueVaThuKhac = triGiaTinhThueVaThuKhac;
			entity.SoLuongTinhThueVaThuKhac = soLuongTinhThueVaThuKhac;
			entity.MaDVTDanhThueVaThuKhac = maDVTDanhThueVaThuKhac;
			entity.ThueSuatThueVaThuKhac = thueSuatThueVaThuKhac;
			entity.SoTienThueVaThuKhac = soTienThueVaThuKhac;
			entity.DieuKhoanMienGiamThueVaThuKhac = dieuKhoanMienGiamThueVaThuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@MaTSThueThuKhac", SqlDbType.VarChar, MaTSThueThuKhac);
			db.AddInParameter(dbCommand, "@MaMGThueThuKhac", SqlDbType.VarChar, MaMGThueThuKhac);
			db.AddInParameter(dbCommand, "@SoTienGiamThueThuKhac", SqlDbType.Decimal, SoTienGiamThueThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucThueVaThuKhac", SqlDbType.NVarChar, TenKhoanMucThueVaThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueVaThuKhac", SqlDbType.Decimal, TriGiaTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueVaThuKhac", SqlDbType.Decimal, SoLuongTinhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@MaDVTDanhThueVaThuKhac", SqlDbType.VarChar, MaDVTDanhThueVaThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatThueVaThuKhac", SqlDbType.VarChar, ThueSuatThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueVaThuKhac", SqlDbType.Decimal, SoTienThueVaThuKhac);
			db.AddInParameter(dbCommand, "@DieuKhoanMienGiamThueVaThuKhac", SqlDbType.VarChar, DieuKhoanMienGiamThueVaThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangMauDich_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangMauDich_ThueThuKhac(long id)
		{
			KDT_VNACC_HangMauDich_ThueThuKhac entity = new KDT_VNACC_HangMauDich_ThueThuKhac();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangMauDich_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}