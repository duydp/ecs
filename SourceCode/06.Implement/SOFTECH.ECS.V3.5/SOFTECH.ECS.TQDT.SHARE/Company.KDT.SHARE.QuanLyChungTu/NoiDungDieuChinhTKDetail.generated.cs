using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class NoiDungDieuChinhTKDetail : ICloneable
	{
		#region Properties.
		
		public int ID { set; get; }
		public string NoiDungTKChinh { set; get; }
		public string NoiDungTKSua { set; get; }
		public int Id_DieuChinh { set; get; }
		public string NguoiTao { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<NoiDungDieuChinhTKDetail> ConvertToCollection(IDataReader reader)
		{
			List<NoiDungDieuChinhTKDetail> collection = new List<NoiDungDieuChinhTKDetail>();
			while (reader.Read())
			{
				NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiTao"))) entity.NguoiTao = reader.GetString(reader.GetOrdinal("NguoiTao"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<NoiDungDieuChinhTKDetail> collection, int id)
        {
            foreach (NoiDungDieuChinhTKDetail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_NoiDungDieuChinhTKDetail VALUES(@NoiDungTKChinh, @NoiDungTKSua, @Id_DieuChinh, @NguoiTao)";
            string update = "UPDATE t_KDT_NoiDungDieuChinhTKDetail SET NoiDungTKChinh = @NoiDungTKChinh, NoiDungTKSua = @NoiDungTKSua, Id_DieuChinh = @Id_DieuChinh, NguoiTao = @NguoiTao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_NoiDungDieuChinhTKDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, "NoiDungTKChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungTKSua", SqlDbType.NVarChar, "NoiDungTKSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Id_DieuChinh", SqlDbType.Int, "Id_DieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiTao", SqlDbType.NVarChar, "NguoiTao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, "NoiDungTKChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungTKSua", SqlDbType.NVarChar, "NoiDungTKSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Id_DieuChinh", SqlDbType.Int, "Id_DieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiTao", SqlDbType.NVarChar, "NguoiTao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_NoiDungDieuChinhTKDetail VALUES(@NoiDungTKChinh, @NoiDungTKSua, @Id_DieuChinh, @NguoiTao)";
            string update = "UPDATE t_KDT_NoiDungDieuChinhTKDetail SET NoiDungTKChinh = @NoiDungTKChinh, NoiDungTKSua = @NoiDungTKSua, Id_DieuChinh = @Id_DieuChinh, NguoiTao = @NguoiTao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_NoiDungDieuChinhTKDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, "NoiDungTKChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungTKSua", SqlDbType.NVarChar, "NoiDungTKSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Id_DieuChinh", SqlDbType.Int, "Id_DieuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiTao", SqlDbType.NVarChar, "NguoiTao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, "NoiDungTKChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungTKSua", SqlDbType.NVarChar, "NoiDungTKSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Id_DieuChinh", SqlDbType.Int, "Id_DieuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiTao", SqlDbType.NVarChar, "NguoiTao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NoiDungDieuChinhTKDetail Load(int id)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<NoiDungDieuChinhTKDetail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<NoiDungDieuChinhTKDetail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<NoiDungDieuChinhTKDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertKDT_NoiDungDieuChinhTKDetail(string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh, string nguoiTao)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();	
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			entity.NguoiTao = nguoiTao;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_NoiDungDieuChinhTKDetail(int id, string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh, string nguoiTao)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();			
			entity.ID = id;
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			entity.NguoiTao = nguoiTao;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_NoiDungDieuChinhTKDetail(int id, string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh, string nguoiTao)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();			
			entity.ID = id;
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			entity.NguoiTao = nguoiTao;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_NoiDungDieuChinhTKDetail(int id)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}