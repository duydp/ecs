using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAD4190 : BasicVNACC
    {
        public List<VAD4190_HANG> HangMD { get; set; }
        public void LoadVAD4190(string strResult)
        {
            try
            {

                this.GetObject<VAD4190>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD4190, false, VAD4190.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD4190.TongSoByte);
                while (true)
                {
                    VAD4190_HANG vad1agHang = new VAD4190_HANG();
                    vad1agHang.GetObject<VAD4190_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD4190_HANG", false, VAD4190_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD4190_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD4190_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD4190_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD4190_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }

}
