namespace Company.Interface.Report.VNACCS
{
    partial class BanKhaiThongTinHoaDon_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuocXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoKienHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSoLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSoLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonViDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTGiaTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTSoTienKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 6.883685F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.TopMargin.HeightF = 64.2361F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(225F, 12.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(312.4916F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Bản khai thông tin hóa đơn / kiện hàng";
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 21.18053F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLine5});
            this.Detail2.HeightF = 175.2017F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(23.95833F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10});
            this.xrTable1.SizeF = new System.Drawing.SizeF(721F, 161.66F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.lblSoDong,
            this.xrTableCell2});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.49519996234067065;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Số dòng";
            this.xrTableCell1.Weight = 0.5;
            // 
            // lblSoDong
            // 
            this.lblSoDong.Name = "lblSoDong";
            this.lblSoDong.Text = "NNE";
            this.lblSoDong.Weight = 2;
            this.lblSoDong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.5;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.lblMaHangHoa,
            this.xrTableCell4,
            this.lblMaSoHangHoa});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.49519996716309311;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Mã hàng hóa";
            this.xrTableCell3.Weight = 0.5;
            // 
            // lblMaHangHoa
            // 
            this.lblMaHangHoa.Multiline = true;
            this.lblMaHangHoa.Name = "lblMaHangHoa";
            this.lblMaHangHoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblMaHangHoa.Weight = 1.5;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Mã số hàng hóa";
            this.xrTableCell4.Weight = 0.5;
            // 
            // lblMaSoHangHoa
            // 
            this.lblMaSoHangHoa.Name = "lblMaSoHangHoa";
            this.lblMaSoHangHoa.Text = "XXXXXXXXX1XE";
            this.lblMaSoHangHoa.Weight = 0.5;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.lblMoTaHangHoa,
            this.xrTableCell6});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 2.0096220920339034;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Mô tả hàng hóa";
            this.xrTableCell5.Weight = 0.5;
            // 
            // lblMoTaHangHoa
            // 
            this.lblMoTaHangHoa.Name = "lblMoTaHangHoa";
            this.lblMoTaHangHoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            this.lblMoTaHangHoa.Weight = 2.2973607914451839;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.20263920855481629;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.lblMaNuocXuatXu,
            this.lblTenNuocXuatXu,
            this.xrTableCell8});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.49519992901612409;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Nước xuất xứ";
            this.xrTableCell7.Weight = 0.5;
            // 
            // lblMaNuocXuatXu
            // 
            this.lblMaNuocXuatXu.Name = "lblMaNuocXuatXu";
            this.lblMaNuocXuatXu.Text = "XE";
            this.lblMaNuocXuatXu.Weight = 0.25120191427377697;
            // 
            // lblTenNuocXuatXu
            // 
            this.lblTenNuocXuatXu.Name = "lblTenNuocXuatXu";
            this.lblTenNuocXuatXu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblTenNuocXuatXu.Weight = 1.7487980857262231;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 0.5;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.lblSoKienHang,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.49519992901612409;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "Số kiện hàng";
            this.xrTableCell9.Weight = 0.5;
            // 
            // lblSoKienHang
            // 
            this.lblSoKienHang.Name = "lblSoKienHang";
            this.lblSoKienHang.Weight = 1.5;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.5;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.5;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.lblSoLuong1,
            this.lblMaDVTSoLuong1,
            this.xrTableCell13,
            this.lblSoLuong2,
            this.lblMaDVTSoLuong2});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.49519992901612409;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Số lượng (1)";
            this.xrTableCell12.Weight = 0.5;
            // 
            // lblSoLuong1
            // 
            this.lblSoLuong1.Name = "lblSoLuong1";
            this.lblSoLuong1.Text = "NNNNNNNNN1NE";
            this.lblSoLuong1.Weight = 0.42067307809682974;
            this.lblSoLuong1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMaDVTSoLuong1
            // 
            this.lblMaDVTSoLuong1.Name = "lblMaDVTSoLuong1";
            this.lblMaDVTSoLuong1.Text = "XXXE";
            this.lblMaDVTSoLuong1.Weight = 0.69592171000874736;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Số lượng (2)";
            this.xrTableCell13.Weight = 0.38340521189442289;
            // 
            // lblSoLuong2
            // 
            this.lblSoLuong2.Name = "lblSoLuong2";
            this.lblSoLuong2.Text = "NNNNNNNNN1NE";
            this.lblSoLuong2.Weight = 0.5;
            this.lblSoLuong2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMaDVTSoLuong2
            // 
            this.lblMaDVTSoLuong2.Name = "lblMaDVTSoLuong2";
            this.lblMaDVTSoLuong2.Text = "XXXE";
            this.lblMaDVTSoLuong2.Weight = 0.5;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.lblDonGiaHoaDon,
            this.lblMTTDonGiaHoaDon,
            this.lblDonViDonGiaHoaDon,
            this.xrTableCell15});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.49519992901612409;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Đơn giá hóa đơn";
            this.xrTableCell14.Weight = 0.5;
            // 
            // lblDonGiaHoaDon
            // 
            this.lblDonGiaHoaDon.Name = "lblDonGiaHoaDon";
            this.lblDonGiaHoaDon.Text = "NNNNNNNNE";
            this.lblDonGiaHoaDon.Weight = 0.42067295602653709;
            this.lblDonGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTDonGiaHoaDon
            // 
            this.lblMTTDonGiaHoaDon.Name = "lblMTTDonGiaHoaDon";
            this.lblMTTDonGiaHoaDon.Text = "XXE";
            this.lblMTTDonGiaHoaDon.Weight = 0.18663466404058202;
            // 
            // lblDonViDonGiaHoaDon
            // 
            this.lblDonViDonGiaHoaDon.Name = "lblDonViDonGiaHoaDon";
            this.lblDonViDonGiaHoaDon.Text = "XXE";
            this.lblDonViDonGiaHoaDon.Weight = 0.39269233924278346;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 1.5000000406900975;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.lblTriGiaHoaDon,
            this.lblMTTGiaTien,
            this.xrTableCell18});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.49519992901612409;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "Trị giá hóa đơn";
            this.xrTableCell16.Weight = 0.5;
            // 
            // lblTriGiaHoaDon
            // 
            this.lblTriGiaHoaDon.Name = "lblTriGiaHoaDon";
            this.lblTriGiaHoaDon.Text = "NNNNNNNNN1NNNNNNNE";
            this.lblTriGiaHoaDon.Weight = 0.60730769127478978;
            this.lblTriGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTGiaTien
            // 
            this.lblMTTGiaTien.Name = "lblMTTGiaTien";
            this.lblMTTGiaTien.Text = "XXE";
            this.lblMTTGiaTien.Weight = 0.39269230872521016;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 1.5;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblLoaiKhauTru_Hang,
            this.xrTableCell20,
            this.lblSoTienKhauTru_Hang,
            this.lblMTTSoTienKhauTru_Hang,
            this.xrTableCell92});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.49519992901612409;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Loại khấu trừ";
            this.xrTableCell19.Weight = 0.5;
            // 
            // lblLoaiKhauTru_Hang
            // 
            this.lblLoaiKhauTru_Hang.Name = "lblLoaiKhauTru_Hang";
            this.lblLoaiKhauTru_Hang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiKhauTru_Hang.Weight = 1.116594661125363;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "Số tiền khấu trừ";
            this.xrTableCell20.Weight = 0.383405338874637;
            // 
            // lblSoTienKhauTru_Hang
            // 
            this.lblSoTienKhauTru_Hang.Name = "lblSoTienKhauTru_Hang";
            this.lblSoTienKhauTru_Hang.Text = "NNNNNNNNE";
            this.lblSoTienKhauTru_Hang.Weight = 0.5;
            this.lblSoTienKhauTru_Hang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTSoTienKhauTru_Hang
            // 
            this.lblMTTSoTienKhauTru_Hang.Name = "lblMTTSoTienKhauTru_Hang";
            this.lblMTTSoTienKhauTru_Hang.Text = "XXE";
            this.lblMTTSoTienKhauTru_Hang.Weight = 0.25;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.25;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.49519992901612409;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "(bao gồm cả phân loại miễn trả tiền)";
            this.xrTableCell21.Weight = 1.0000000101725244;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 1.9999999898274756;
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(10.00015F, 167.91F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(759.9999F, 2.083333F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.HeightF = 10.41667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // BanKhaiThongTinHoaDon_2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(53, 27, 64, 21);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblGioKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNuocXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell lblSoKienHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSoLuong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSoLuong2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblDonViDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTGiaTien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTSoTienKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
    }
}
