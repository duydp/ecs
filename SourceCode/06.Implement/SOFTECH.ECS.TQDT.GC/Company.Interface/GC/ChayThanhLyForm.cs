using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC
{
    public partial class ChayThanhLyForm : BaseForm
    {
        public int ProccesValue;
        public bool temp = false;
        private bool temp2 = false;
        private int limit = 99;
        public bool IsSuccess = true;
        public HopDong HD = new HopDong();
        public ChayThanhLyForm()
        {
            InitializeComponent();
        }

        private void ChayThanhLyForm_Load_1(object sender, EventArgs e)
        {
            this.ProccesValue = 1;
            backgroundWorker1.RunWorkerAsync(this.HD);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if(temp)
            {
                timer1.Enabled = false;
                Thread.Sleep(3000);
                this.Close();
            }
            DataRow dr = dtProcess.NewRow();
            if (this.ProccesValue == 1)
            {
                //DataRow dr = dtProcess.NewRow();
               // txtMessage.Text += "Đang khởi tạo dữ liệu thanh khoản...";
                dr["TenProcess"] = "Đang khởi tạo dữ liệu thanh khoản...";
                dr["TrangThai"] = "Đang thực hiện";
                dtProcess.Rows.Add(dr);
            }
            if (this.ProccesValue == 19)
            {
                dtProcess.Select("TenProcess='" + "Đang khởi tạo dữ liệu thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";
               // txtMessage.Text += "Hoàn thành!\n";
            }
            if (this.ProccesValue == 21)
            {
                dr["TenProcess"] = "Đang kiểm tra số liệu thanh khoản...";
                dr["TrangThai"] = "Đang thực hiện";
                dtProcess.Rows.Add(dr);
                //txtMessage.Text += "Đang kiểm tra số liệu thanh khoản...";
            }
            if (this.ProccesValue == 39)
            {
                dtProcess.Select("TenProcess='" + "Đang kiểm tra số liệu thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";
                //txtMessage.Text += "Hoàn thành!\n";
            }
            if (this.ProccesValue == 41)
            {
                dr["TenProcess"] = "Đang xử lý số liệu thanh khoản....";
                dr["TrangThai"] = "Đang thực hiện";
                dtProcess.Rows.Add(dr);
                //txtMessage.Text += "Đang xử lý số liệu thanh khoản...";
            }
            if (this.ProccesValue == 59)
            {
                dtProcess.Select("TenProcess='" + "Đang xử lý số liệu thanh khoản...." + "'")[0]["TrangThai"] = "Hoàn thành";
            }
            if (this.ProccesValue == 61)
            {
                dr["TenProcess"] = "Đang chạy thanh khoản...";
                dr["TrangThai"] = "Đang thực hiện";
                dtProcess.Rows.Add(dr);
                //txtMessage.Text += "Đang chạy thanh khoản...";
            }
            if (this.ProccesValue == 62)
            {
                //try
                //{
                //    this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                //}
                //catch (Exception ex)
                //{
                //    ShowMessage("Lỗi: " + ex.Message, false);
                //    this.Close();
                //}
            }
            if (this.ProccesValue == 79)
            {
                dtProcess.Select("TenProcess='" + "Đang chạy thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";

                //txtMessage.Text += "Hoàn thành!\n";
            }
            if (this.ProccesValue == 81)
            {
                dr["TenProcess"] = "Đang tạo các báo cáo thanh khoản...";
                dr["TrangThai"] = "Đang thực hiện";
                dtProcess.Rows.Add(dr);
               // txtMessage.Text += "Đang tạo các báo cáo thanh khoản...";
            }
            if (this.ProccesValue == 99 && !temp2)
            {
                dtProcess.Select("TenProcess='" + "Đang tạo các báo cáo thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";

                //txtMessage.Text += "Hoàn thành!\n";
                temp2 = true;
            }
            if (this.ProccesValue == 100)
            {
                if (pbProgress.Value == 99)
                {
                    dr["TenProcess"] = "Chạy thanh khoản thành công.";
                    dr["TrangThai"] = "Xin chờ...";
                    dtProcess.Rows.Add(dr);
                    //txtMessage.Text += "Chạy thanh khoản thành công.";
                    temp = true;
                }
                else
                {
                    this.ProccesValue = pbProgress.Value;
                    limit = 100;
                }

            }
            pbProgress.Value = this.ProccesValue;
            if(this.ProccesValue<limit)this.ProccesValue ++;
        }

        private void ChayThanhLyForm_Shown(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            bool t = ThanhKhoan();
            if(t)
            {
                this.IsSuccess = true;
            }
            else
            {
                timer1.Enabled = false;
                //ShowMessage("Chạy thanh khoản không thành công.", false);
                showMsg("MSG_2702003");
                this.IsSuccess = false;
                this.Close();
            }
        }
        private bool ThanhKhoan()
        {
            ThanhKhoanHDGC thanhkhoanHD;
            ThanhKhoanHDGCCollection thanhkhoanHDColl;

            bool kqbool = true;
            // string dk = "HopDong_ID=" + this.HD.ID;
            Company.GC.BLL.GC.NguyenPhuLieu nplHD = new Company.GC.BLL.GC.NguyenPhuLieu();
            nplHD.HopDong_ID = this.HD.ID;
            Company.GC.BLL.GC.NguyenPhuLieuCollection nplHDColl = nplHD.SelectCollectionBy_HopDong_ID();

            try
            {
                // Thanh khoản hợp đồng gia công
                // Mẫu 01 HQ-GC :

                // Mẫu 06 HQ-GC :
                thanhkhoanHD = new ThanhKhoanHDGC();
                thanhkhoanHDColl = new ThanhKhoanHDGCCollection();
                thanhkhoanHD.DeleteHDTK(this.HD.ID );
                int soTT = 1;
                foreach (Company.GC.BLL.GC.NguyenPhuLieu nplhd in nplHDColl)
                {
                    thanhkhoanHD = new ThanhKhoanHDGC();
                    thanhkhoanHD.Ten = nplhd.Ten + " (" + nplhd.Ma + ")";
                    thanhkhoanHD.TongLuongNK = nplhd.SoLuongDaNhap;
                    thanhkhoanHD.TongLuongCU = nplhd.SoLuongCungUng;
                    thanhkhoanHD.TongLuongXK = nplhd.SoLuongDaDung;
                    thanhkhoanHD.ChenhLech = nplhd.SoLuongDaNhap - (nplhd.SoLuongDaDung - nplhd.SoLuongCungUng);
                    thanhkhoanHD.DVT = Company.GC.BLL.DuLieuChuan.DonViTinh.GetName(nplhd.DVT_ID).ToString();
                    thanhkhoanHD.OldHD_ID = nplhd.HopDong_ID;
                    thanhkhoanHD.KetLuanXLCL = "";
                    thanhkhoanHD.STT = soTT ;
                    soTT++;
                    thanhkhoanHDColl.Add(thanhkhoanHD);

                }
                kqbool = thanhkhoanHD.Insert(thanhkhoanHDColl);

                //Mẫu 04 HQ-GC : 
                DataSet dsNPLCU = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNPLCU(this.HD.ID);
                BKNguyenPhuLieu bkNPLCU;
                BKNguyenPhuLieu bkNPLCUTemp;
                BKNguyenPhuLieuCollection bkNPLCUColl = new BKNguyenPhuLieuCollection();
                bkNPLCUTemp = new BKNguyenPhuLieu();
                bkNPLCUTemp.DeleteOldBK(this.HD.ID );
                int stt04 = 1;
                foreach (DataRow drCU in dsNPLCU.Tables[0].Rows)
                {
                    bkNPLCU = new BKNguyenPhuLieu();
                    if (drCU["GiaTB"].ToString() != null)
                        bkNPLCU.DonGia = Convert.ToDecimal(drCU["GiaTB"]);
                    else
                        bkNPLCU.DonGia = 0;

                    bkNPLCU.DVT = Company.GC.BLL.DuLieuChuan.DonViTinh.GetName(drCU["DVT_ID"].ToString());
                    bkNPLCU.GhiChu = "";
                    bkNPLCU.HinhThucCU = "";
                    bkNPLCU.MaNPL = drCU["Ma"].ToString();
                    if (drCU["HopDong_ID"].ToString() != null)
                        bkNPLCU.OldHD_ID = Convert.ToInt64(drCU["HopDong_ID"]);
                    else
                        bkNPLCU.OldHD_ID = 0;
                    bkNPLCU.TenNPL = drCU["Ten"].ToString() + " (" + drCU["Ma"].ToString() + ")" ;
                    if (drCU["TongLuongCung"].ToString() != null)
                        bkNPLCU.TongNPLCU = Convert.ToDecimal(drCU["TongLuongCung"]);
                    else
                        bkNPLCU.TongNPLCU = 0;
                    if (drCU["GiaTB"].ToString() != null && drCU["TongLuongCung"].ToString() != null)
                        bkNPLCU.TriGia = Convert.ToDecimal(drCU["GiaTB"]) * Convert.ToDecimal(drCU["TongLuongCung"]);
                   
                    bkNPLCU.HinhThucCU = drCU["HinhThuCungUng"].ToString() ;
                    
                        //bkNPLCU.TriGia = 0;
                    bkNPLCU.STT = stt04;
                    stt04++;
                    bkNPLCUColl.Add(bkNPLCU);
                }
                bkNPLCUTemp = new BKNguyenPhuLieu();
                bkNPLCUTemp.Insert(bkNPLCUColl);

                //Mẫu 08 HQ-GC :
                DataSet dsTK = Company.GC.BLL.KDT.ToKhaiMauDich.GetToKhaiNK(this.HD.ID);
                BKToKhai bkTK;
                BKToKhai bkTKtemp;
                bkTKtemp = new BKToKhai();
                bkTKtemp.DeleteTK(this.HD.ID);
                int stt08 = 1;
                BKToKhaiCollection bkTKColl = new BKToKhaiCollection();
                foreach (DataRow drTK in dsTK.Tables[0].Rows)
                {
                    bkTK = new BKToKhai();
                    bkTK.CanBoDuyet = "";
                    bkTK.CKXN = Company.GC.BLL.DuLieuChuan.CuaKhau.GetName(drTK["CuaKhau_ID"].ToString());
                    // bkTK.DVT = Company.GC.BLL.DuLieuChuan.DonViTinh.GetName(drTK["DVT_ID"].ToString());
                    bkTK.GhiChu = "";
                    bkTK.Luong = 0;//Convert.ToDecimal(drTK["Luong"]);
                    if (drTK["NgayDangKy"].ToString()!= null )
                        bkTK.Ngay_DK = Convert.ToDateTime(drTK["NgayDangKy"]);
                    if (drTK["NgayTiepNhan"].ToString() != null)
                        bkTK.NgayTK = Convert.ToDateTime(drTK["NgayTiepNhan"]);
                    if (drTK["SoToKhai"].ToString() != null)
                        bkTK.SoTK = Convert.ToInt64(drTK["SoToKhai"]);
                    if (drTK["SoToKhai"].ToString() != null)                       
                        bkTK.VT = Convert.ToDecimal(drTK["SoToKhai"]) + " " +  drTK["MaLoaiHinh"].ToString() + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.PadRight(3);  
                    else
                        bkTK.VT = "";                    
                    if (drTK["IDHopDong"].ToString() != null)  
                        bkTK.IDHopDong = Convert.ToInt64(drTK["IDHopDong"]);
                    bkTK.DVHQ = drTK["MaHaiQuan"].ToString();
                    bkTK.MaLoaiHinh = drTK["MaLoaiHinh"].ToString();
                    bkTK.STT = stt08;
                    stt08++;
                    bkTKColl.Add(bkTK);

                }
                bkTKtemp = new BKToKhai();
                bkTKtemp.Insert(bkTKColl);

                //Mẫu 09 HQ-GC :
                DataSet dsTKX = Company.GC.BLL.KDT.ToKhaiMauDich.GetToKhaiXK(this.HD.ID);
                BKToKhai bkTKX;
                BKToKhai bkTKXtemp;
                bkTKXtemp = new BKToKhai();
                //bkTKXtemp.DeleteTK(this.HD.ID);
                int stt09 = 1;
                BKToKhaiCollection bkTKXColl = new BKToKhaiCollection();
                foreach (DataRow drTKX in dsTKX.Tables[0].Rows)
                {
                    bkTKX = new BKToKhai();
                    bkTKX.CanBoDuyet = "";
                    bkTKX.CKXN = Company.GC.BLL.DuLieuChuan.CuaKhau.GetName(drTKX["CuaKhau_ID"].ToString());
                    // bkTK.DVT = Company.GC.BLL.DuLieuChuan.DonViTinh.GetName(drTK["DVT_ID"].ToString());
                    bkTKX.GhiChu = "";
                    bkTKX.Luong = 0;//Convert.ToDecimal(drTK["Luong"]);
                    if (drTKX["NgayDangKy"].ToString()!=null )
                        bkTKX.Ngay_DK = Convert.ToDateTime(drTKX["NgayDangKy"]);
                    if (drTKX["NgayTiepNhan"].ToString() != null)
                        bkTKX.NgayTK = Convert.ToDateTime(drTKX["NgayTiepNhan"]);
                    if (drTKX["SoToKhai"].ToString() != null)
                        bkTKX.SoTK = Convert.ToInt64(drTKX["SoToKhai"]);
                    else
                        bkTKX.SoTK = 0;
                    if (drTKX["SoToKhai"].ToString()!=null)
                        bkTKX.VT = Convert.ToDecimal(drTKX["SoToKhai"]) + " " + drTKX["MaLoaiHinh"].ToString() + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.PadRight(3);
                    else
                        bkTKX.VT = "";
                    bkTKX.DVHQ = drTKX["MaHaiQuan"].ToString();
                    if (drTKX["IDHopDong"].ToString()!=null)
                        bkTKX.IDHopDong = Convert.ToInt64(drTKX["IDHopDong"]);
                    else
                        bkTKX.IDHopDong = 0;
                    bkTKX.MaLoaiHinh = drTKX["MaLoaiHinh"].ToString();
                    bkTKX.STT = stt09;
                    stt09++;
                    bkTKXColl.Add(bkTKX);

                }
                bkTKXtemp = new BKToKhai();
                bkTKXtemp.Insert(bkTKXColl);
                //Mẫu 10 HQ-GC :
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                DataSet ds = dm.getDinhMuc(this.HD.ID);
                BKDinhMuc bkDM;
                BKDinhMucCollection bkDMColl = new BKDinhMucCollection();
                BKDinhMuc bkDMtemp;
                bkDMtemp = new BKDinhMuc();
                bkDMtemp.DeleteTK(this.HD.ID);
                int stt = 1;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    bkDM = new BKDinhMuc();
                    bkDM.TenNPL = dr["TenNPL"].ToString();
                    bkDM.MaSP = dr["MaSanPham"].ToString();
                    if (dr["DinhMucSuDung"].ToString() != null)
                        bkDM.DinhMucSD = Convert.ToDecimal(dr["DinhMucSuDung"]);
                    else
                        bkDM.DinhMucSD = 0;
                    if (dr["DinhMucChung"].ToString() != null)
                        bkDM.DinhMucTH = Convert.ToDecimal(dr["DinhMucChung"]);
                    else
                        bkDM.DinhMucSD = 0;

                    bkDM.DVT = Company.GC.BLL.DuLieuChuan.DonViTinh.GetName(dr["DVT_ID"].ToString());
                    bkDM.GhiChu = "";
                    bkDM.MaNPL = dr["MaNguyenPhuLieu"].ToString();
                    bkDM.NguonNL = "";
                    if (dr["HopDong_ID"].ToString() != null)
                        bkDM.OldHD_ID = Convert.ToInt64(dr["HopDong_ID"]);
                    else
                        bkDM.OldHD_ID = 0;

                    bkDM.STT = stt;
                    stt++;
                    if (dr["TyLeHaoHut"].ToString() != null)
                        bkDM.TyLeHH = Convert.ToDecimal(dr["TyLeHaoHut"]);
                    else
                        bkDM.TyLeHH = 0;
                    bkDMColl.Add(bkDM);

                }

                bkDMtemp = new BKDinhMuc();
                bkDMtemp.Insert(bkDMColl);
            }
            catch
            {
                kqbool = false; ;
            }
            return kqbool;
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ProccesValue = 100;
        }

    }
}