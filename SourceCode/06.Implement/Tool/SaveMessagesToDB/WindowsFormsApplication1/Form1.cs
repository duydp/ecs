﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string msg = string.Empty;

        private void button1_Click(object sender, EventArgs e)
        {

            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                
                string fileName = openFileDialog1.FileName;
                txtPath.Text = fileName;
                FileStream fs = new FileStream(fileName, FileMode.Open);
                StreamReader streamReader = new StreamReader(fs,UTF8Encoding.UTF8);
                msg = streamReader.ReadToEnd();
                streamReader.Close();

                txtMsg.Text = msg;

            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           
            try
            {
                Cursor = Cursors.WaitCursor;
                KDT_VNACCS_MsgLog MSG_LOG = new KDT_VNACCS_MsgLog();
                MSG_LOG.MaNghiepVu = txtMaNghiepVu.Text;
                MSG_LOG.TieuDeThongBao = txtTieuDe.Text;
                MSG_LOG.Log_Messages = msg;
                MSG_LOG.Item_id = String.IsNullOrEmpty(txtSTT.Text.Trim()) ? 0 : Convert.ToInt64(txtSTT.Text.Trim());
                MSG_LOG.CreatedTime = DateTime.Today;
                MSG_LOG.Insert();
                MessageBox.Show("Lưu thành công ");
            }
            catch (Exception ex)
            {
               MessageBox.Show("Lỗi: " + ex);
            }
            finally { Cursor = Cursors.Default; }


        }

        private void btnLoadDB_Click(object sender, EventArgs e)
        {
            if (txtIDINBOX.Text != "")
            {
                msg = string.Empty;
                List<INBOX> a = (List<INBOX>)INBOX.SelectCollectionDynamic("ID = " + txtIDINBOX.Text, null);
                if (a.Count > 0)
                {
                    msg = a[0].Messages;
                    txtMsg.Text = msg;
                }
                else
                    MessageBox.Show("Không có Messages ");
            }
            else if (txtID.Text != "")
            {
                msg = string.Empty;
                List<KDT_VNACCS_MsgLog> a = KDT_VNACCS_MsgLog.SelectCollectionDynamic("ID = " + txtID.Text, null);
                if (a.Count > 0)
                {
                    msg = a[0].Log_Messages;
                    txtMsg.Text = msg;
                }
                else
                    MessageBox.Show("Không có Messages ");
            }
            else
                MessageBox.Show("Nhập mã ngiệp vụ");
        }

        private void btnSaveToTXT_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                DialogResult result = saveFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string filename = saveFileDialog1.FileName;
                    FileStream fs = new FileStream(filename, FileMode.Create);
                    StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);//fs là 1 FileStream 
                    sWriter.WriteLine(msg);
                    sWriter.Close();
                    MessageBox.Show("lưu File thành công");
                    if (MessageBox.Show("Mở file vừa lưu", "Open File",MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Process.Start(filename);
                    }
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi" + ex);
                this.Cursor = Cursors.Default;
            }

        }
        private void textBox1_keyDown(object sender, KeyEventArgs e)
        {
            if (e.Control & e.KeyCode == Keys.A)
                txtMsg.SelectAll();
        }
    }
}
