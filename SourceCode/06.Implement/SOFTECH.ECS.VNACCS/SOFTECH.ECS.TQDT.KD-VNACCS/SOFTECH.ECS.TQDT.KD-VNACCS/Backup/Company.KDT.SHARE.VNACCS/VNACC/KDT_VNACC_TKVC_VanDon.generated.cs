using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TKVC_VanDon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string SoTTVanDon { set; get; }
		public string SoVanDon { set; get; }
		public DateTime NgayPhatHanhVD { set; get; }
		public string MoTaHangHoa { set; get; }
		public string MaHS { set; get; }
		public string KyHieuVaSoHieu { set; get; }
		public DateTime NgayNhapKhoHQLanDau { set; get; }
		public string PhanLoaiSanPhan { set; get; }
		public string MaNuocSanXuat { set; get; }
		public string TenNuocSanXuat { set; get; }
		public string MaDiaDiemXuatPhatVC { set; get; }
		public string TenDiaDiemXuatPhatVC { set; get; }
		public string MaDiaDiemDichVC { set; get; }
		public string TenDiaDiemDichVC { set; get; }
		public string LoaiHangHoa { set; get; }
		public string MaPhuongTienVC { set; get; }
		public string TenPhuongTienVC { set; get; }
		public DateTime NgayHangDuKienDenDi { set; get; }
		public string MaNguoiNhapKhau { set; get; }
		public string TenNguoiNhapKhau { set; get; }
		public string DiaChiNguoiNhapKhau { set; get; }
		public string MaNguoiXuatKhua { set; get; }
		public string TenNguoiXuatKhau { set; get; }
		public string DiaChiNguoiXuatKhau { set; get; }
		public string MaNguoiUyThac { set; get; }
		public string TenNguoiUyThac { set; get; }
		public string DiaChiNguoiUyThac { set; get; }
		public string MaVanBanPhapLuat1 { set; get; }
		public string MaVanBanPhapLuat2 { set; get; }
		public string MaVanBanPhapLuat3 { set; get; }
		public string MaVanBanPhapLuat4 { set; get; }
		public string MaVanBanPhapLuat5 { set; get; }
		public string MaDVTTriGia { set; get; }
		public decimal TriGia { set; get; }
		public decimal SoLuong { set; get; }
		public string MaDVTSoLuong { set; get; }
		public decimal TongTrongLuong { set; get; }
		public string MaDVTTrongLuong { set; get; }
		public decimal TheTich { set; get; }
		public string MaDVTTheTich { set; get; }
		public string MaDanhDauDDKH1 { set; get; }
		public string MaDanhDauDDKH2 { set; get; }
		public string MaDanhDauDDKH3 { set; get; }
		public string MaDanhDauDDKH4 { set; get; }
		public string MaDanhDauDDKH5 { set; get; }
		public string SoGiayPhep { set; get; }
		public DateTime NgayCapPhep { set; get; }
		public DateTime NgayHetHanCapPhep { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TKVC_VanDon> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TKVC_VanDon> collection = new List<KDT_VNACC_TKVC_VanDon>();
			while (reader.Read())
			{
				KDT_VNACC_TKVC_VanDon entity = new KDT_VNACC_TKVC_VanDon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTTVanDon"))) entity.SoTTVanDon = reader.GetString(reader.GetOrdinal("SoTTVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanhVD"))) entity.NgayPhatHanhVD = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanhVD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoa"))) entity.MoTaHangHoa = reader.GetString(reader.GetOrdinal("MoTaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("KyHieuVaSoHieu"))) entity.KyHieuVaSoHieu = reader.GetString(reader.GetOrdinal("KyHieuVaSoHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNhapKhoHQLanDau"))) entity.NgayNhapKhoHQLanDau = reader.GetDateTime(reader.GetOrdinal("NgayNhapKhoHQLanDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiSanPhan"))) entity.PhanLoaiSanPhan = reader.GetString(reader.GetOrdinal("PhanLoaiSanPhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocSanXuat"))) entity.MaNuocSanXuat = reader.GetString(reader.GetOrdinal("MaNuocSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNuocSanXuat"))) entity.TenNuocSanXuat = reader.GetString(reader.GetOrdinal("TenNuocSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXuatPhatVC"))) entity.MaDiaDiemXuatPhatVC = reader.GetString(reader.GetOrdinal("MaDiaDiemXuatPhatVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemXuatPhatVC"))) entity.TenDiaDiemXuatPhatVC = reader.GetString(reader.GetOrdinal("TenDiaDiemXuatPhatVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDichVC"))) entity.MaDiaDiemDichVC = reader.GetString(reader.GetOrdinal("MaDiaDiemDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemDichVC"))) entity.TenDiaDiemDichVC = reader.GetString(reader.GetOrdinal("TenDiaDiemDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhuongTienVC"))) entity.MaPhuongTienVC = reader.GetString(reader.GetOrdinal("MaPhuongTienVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPhuongTienVC"))) entity.TenPhuongTienVC = reader.GetString(reader.GetOrdinal("TenPhuongTienVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHangDuKienDenDi"))) entity.NgayHangDuKienDenDi = reader.GetDateTime(reader.GetOrdinal("NgayHangDuKienDenDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhapKhau"))) entity.MaNguoiNhapKhau = reader.GetString(reader.GetOrdinal("MaNguoiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhapKhau"))) entity.TenNguoiNhapKhau = reader.GetString(reader.GetOrdinal("TenNguoiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhapKhau"))) entity.DiaChiNguoiNhapKhau = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiXuatKhua"))) entity.MaNguoiXuatKhua = reader.GetString(reader.GetOrdinal("MaNguoiXuatKhua"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiXuatKhau"))) entity.TenNguoiXuatKhau = reader.GetString(reader.GetOrdinal("TenNguoiXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiXuatKhau"))) entity.DiaChiNguoiXuatKhau = reader.GetString(reader.GetOrdinal("DiaChiNguoiXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiUyThac"))) entity.MaNguoiUyThac = reader.GetString(reader.GetOrdinal("MaNguoiUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiUyThac"))) entity.TenNguoiUyThac = reader.GetString(reader.GetOrdinal("TenNguoiUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiUyThac"))) entity.DiaChiNguoiUyThac = reader.GetString(reader.GetOrdinal("DiaChiNguoiUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapLuat1"))) entity.MaVanBanPhapLuat1 = reader.GetString(reader.GetOrdinal("MaVanBanPhapLuat1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapLuat2"))) entity.MaVanBanPhapLuat2 = reader.GetString(reader.GetOrdinal("MaVanBanPhapLuat2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapLuat3"))) entity.MaVanBanPhapLuat3 = reader.GetString(reader.GetOrdinal("MaVanBanPhapLuat3"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapLuat4"))) entity.MaVanBanPhapLuat4 = reader.GetString(reader.GetOrdinal("MaVanBanPhapLuat4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapLuat5"))) entity.MaVanBanPhapLuat5 = reader.GetString(reader.GetOrdinal("MaVanBanPhapLuat5"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTriGia"))) entity.MaDVTTriGia = reader.GetString(reader.GetOrdinal("MaDVTTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTSoLuong"))) entity.MaDVTSoLuong = reader.GetString(reader.GetOrdinal("MaDVTSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTrongLuong"))) entity.TongTrongLuong = reader.GetDecimal(reader.GetOrdinal("TongTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTrongLuong"))) entity.MaDVTTrongLuong = reader.GetString(reader.GetOrdinal("MaDVTTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TheTich"))) entity.TheTich = reader.GetDecimal(reader.GetOrdinal("TheTich"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTheTich"))) entity.MaDVTTheTich = reader.GetString(reader.GetOrdinal("MaDVTTheTich"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDanhDauDDKH1"))) entity.MaDanhDauDDKH1 = reader.GetString(reader.GetOrdinal("MaDanhDauDDKH1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDanhDauDDKH2"))) entity.MaDanhDauDDKH2 = reader.GetString(reader.GetOrdinal("MaDanhDauDDKH2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDanhDauDDKH3"))) entity.MaDanhDauDDKH3 = reader.GetString(reader.GetOrdinal("MaDanhDauDDKH3"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDanhDauDDKH4"))) entity.MaDanhDauDDKH4 = reader.GetString(reader.GetOrdinal("MaDanhDauDDKH4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDanhDauDDKH5"))) entity.MaDanhDauDDKH5 = reader.GetString(reader.GetOrdinal("MaDanhDauDDKH5"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapPhep"))) entity.NgayCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanCapPhep"))) entity.NgayHetHanCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TKVC_VanDon> collection, long id)
        {
            foreach (KDT_VNACC_TKVC_VanDon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_VanDon VALUES(@Master_ID, @SoTTVanDon, @SoVanDon, @NgayPhatHanhVD, @MoTaHangHoa, @MaHS, @KyHieuVaSoHieu, @NgayNhapKhoHQLanDau, @PhanLoaiSanPhan, @MaNuocSanXuat, @TenNuocSanXuat, @MaDiaDiemXuatPhatVC, @TenDiaDiemXuatPhatVC, @MaDiaDiemDichVC, @TenDiaDiemDichVC, @LoaiHangHoa, @MaPhuongTienVC, @TenPhuongTienVC, @NgayHangDuKienDenDi, @MaNguoiNhapKhau, @TenNguoiNhapKhau, @DiaChiNguoiNhapKhau, @MaNguoiXuatKhua, @TenNguoiXuatKhau, @DiaChiNguoiXuatKhau, @MaNguoiUyThac, @TenNguoiUyThac, @DiaChiNguoiUyThac, @MaVanBanPhapLuat1, @MaVanBanPhapLuat2, @MaVanBanPhapLuat3, @MaVanBanPhapLuat4, @MaVanBanPhapLuat5, @MaDVTTriGia, @TriGia, @SoLuong, @MaDVTSoLuong, @TongTrongLuong, @MaDVTTrongLuong, @TheTich, @MaDVTTheTich, @MaDanhDauDDKH1, @MaDanhDauDDKH2, @MaDanhDauDDKH3, @MaDanhDauDDKH4, @MaDanhDauDDKH5, @SoGiayPhep, @NgayCapPhep, @NgayHetHanCapPhep, @GhiChu)";
            string update = "UPDATE t_KDT_VNACC_TKVC_VanDon SET Master_ID = @Master_ID, SoTTVanDon = @SoTTVanDon, SoVanDon = @SoVanDon, NgayPhatHanhVD = @NgayPhatHanhVD, MoTaHangHoa = @MoTaHangHoa, MaHS = @MaHS, KyHieuVaSoHieu = @KyHieuVaSoHieu, NgayNhapKhoHQLanDau = @NgayNhapKhoHQLanDau, PhanLoaiSanPhan = @PhanLoaiSanPhan, MaNuocSanXuat = @MaNuocSanXuat, TenNuocSanXuat = @TenNuocSanXuat, MaDiaDiemXuatPhatVC = @MaDiaDiemXuatPhatVC, TenDiaDiemXuatPhatVC = @TenDiaDiemXuatPhatVC, MaDiaDiemDichVC = @MaDiaDiemDichVC, TenDiaDiemDichVC = @TenDiaDiemDichVC, LoaiHangHoa = @LoaiHangHoa, MaPhuongTienVC = @MaPhuongTienVC, TenPhuongTienVC = @TenPhuongTienVC, NgayHangDuKienDenDi = @NgayHangDuKienDenDi, MaNguoiNhapKhau = @MaNguoiNhapKhau, TenNguoiNhapKhau = @TenNguoiNhapKhau, DiaChiNguoiNhapKhau = @DiaChiNguoiNhapKhau, MaNguoiXuatKhua = @MaNguoiXuatKhua, TenNguoiXuatKhau = @TenNguoiXuatKhau, DiaChiNguoiXuatKhau = @DiaChiNguoiXuatKhau, MaNguoiUyThac = @MaNguoiUyThac, TenNguoiUyThac = @TenNguoiUyThac, DiaChiNguoiUyThac = @DiaChiNguoiUyThac, MaVanBanPhapLuat1 = @MaVanBanPhapLuat1, MaVanBanPhapLuat2 = @MaVanBanPhapLuat2, MaVanBanPhapLuat3 = @MaVanBanPhapLuat3, MaVanBanPhapLuat4 = @MaVanBanPhapLuat4, MaVanBanPhapLuat5 = @MaVanBanPhapLuat5, MaDVTTriGia = @MaDVTTriGia, TriGia = @TriGia, SoLuong = @SoLuong, MaDVTSoLuong = @MaDVTSoLuong, TongTrongLuong = @TongTrongLuong, MaDVTTrongLuong = @MaDVTTrongLuong, TheTich = @TheTich, MaDVTTheTich = @MaDVTTheTich, MaDanhDauDDKH1 = @MaDanhDauDDKH1, MaDanhDauDDKH2 = @MaDanhDauDDKH2, MaDanhDauDDKH3 = @MaDanhDauDDKH3, MaDanhDauDDKH4 = @MaDanhDauDDKH4, MaDanhDauDDKH5 = @MaDanhDauDDKH5, SoGiayPhep = @SoGiayPhep, NgayCapPhep = @NgayCapPhep, NgayHetHanCapPhep = @NgayHetHanCapPhep, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_VanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTTVanDon", SqlDbType.VarChar, "SoTTVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, "NgayPhatHanhVD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, "KyHieuVaSoHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, "NgayNhapKhoHQLanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, "PhanLoaiSanPhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocSanXuat", SqlDbType.VarChar, "MaNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNuocSanXuat", SqlDbType.VarChar, "TenNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, "MaDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, "TenDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, "MaDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, "TenDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.VarChar, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTienVC", SqlDbType.VarChar, "TenPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, "NgayHangDuKienDenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, "MaNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, "TenNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, "MaNguoiXuatKhua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, "TenNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiUyThac", SqlDbType.VarChar, "MaNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, "TenNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, "DiaChiNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, "MaVanBanPhapLuat1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, "MaVanBanPhapLuat2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, "MaVanBanPhapLuat3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, "MaVanBanPhapLuat4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, "MaVanBanPhapLuat5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTriGia", SqlDbType.VarChar, "MaDVTTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTrongLuong", SqlDbType.Decimal, "TongTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TheTich", SqlDbType.Decimal, "TheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTheTich", SqlDbType.VarChar, "MaDVTTheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, "MaDanhDauDDKH1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, "MaDanhDauDDKH2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, "MaDanhDauDDKH3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, "MaDanhDauDDKH4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, "MaDanhDauDDKH5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayPhep", SqlDbType.VarChar, "SoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, "NgayHetHanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTTVanDon", SqlDbType.VarChar, "SoTTVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, "NgayPhatHanhVD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, "KyHieuVaSoHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, "NgayNhapKhoHQLanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, "PhanLoaiSanPhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocSanXuat", SqlDbType.VarChar, "MaNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNuocSanXuat", SqlDbType.VarChar, "TenNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, "MaDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, "TenDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, "MaDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, "TenDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.VarChar, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTienVC", SqlDbType.VarChar, "TenPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, "NgayHangDuKienDenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, "MaNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, "TenNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, "MaNguoiXuatKhua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, "TenNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiUyThac", SqlDbType.VarChar, "MaNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, "TenNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, "DiaChiNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, "MaVanBanPhapLuat1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, "MaVanBanPhapLuat2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, "MaVanBanPhapLuat3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, "MaVanBanPhapLuat4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, "MaVanBanPhapLuat5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTriGia", SqlDbType.VarChar, "MaDVTTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTrongLuong", SqlDbType.Decimal, "TongTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TheTich", SqlDbType.Decimal, "TheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTheTich", SqlDbType.VarChar, "MaDVTTheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, "MaDanhDauDDKH1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, "MaDanhDauDDKH2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, "MaDanhDauDDKH3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, "MaDanhDauDDKH4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, "MaDanhDauDDKH5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayPhep", SqlDbType.VarChar, "SoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, "NgayHetHanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_VanDon VALUES(@Master_ID, @SoTTVanDon, @SoVanDon, @NgayPhatHanhVD, @MoTaHangHoa, @MaHS, @KyHieuVaSoHieu, @NgayNhapKhoHQLanDau, @PhanLoaiSanPhan, @MaNuocSanXuat, @TenNuocSanXuat, @MaDiaDiemXuatPhatVC, @TenDiaDiemXuatPhatVC, @MaDiaDiemDichVC, @TenDiaDiemDichVC, @LoaiHangHoa, @MaPhuongTienVC, @TenPhuongTienVC, @NgayHangDuKienDenDi, @MaNguoiNhapKhau, @TenNguoiNhapKhau, @DiaChiNguoiNhapKhau, @MaNguoiXuatKhua, @TenNguoiXuatKhau, @DiaChiNguoiXuatKhau, @MaNguoiUyThac, @TenNguoiUyThac, @DiaChiNguoiUyThac, @MaVanBanPhapLuat1, @MaVanBanPhapLuat2, @MaVanBanPhapLuat3, @MaVanBanPhapLuat4, @MaVanBanPhapLuat5, @MaDVTTriGia, @TriGia, @SoLuong, @MaDVTSoLuong, @TongTrongLuong, @MaDVTTrongLuong, @TheTich, @MaDVTTheTich, @MaDanhDauDDKH1, @MaDanhDauDDKH2, @MaDanhDauDDKH3, @MaDanhDauDDKH4, @MaDanhDauDDKH5, @SoGiayPhep, @NgayCapPhep, @NgayHetHanCapPhep, @GhiChu)";
            string update = "UPDATE t_KDT_VNACC_TKVC_VanDon SET Master_ID = @Master_ID, SoTTVanDon = @SoTTVanDon, SoVanDon = @SoVanDon, NgayPhatHanhVD = @NgayPhatHanhVD, MoTaHangHoa = @MoTaHangHoa, MaHS = @MaHS, KyHieuVaSoHieu = @KyHieuVaSoHieu, NgayNhapKhoHQLanDau = @NgayNhapKhoHQLanDau, PhanLoaiSanPhan = @PhanLoaiSanPhan, MaNuocSanXuat = @MaNuocSanXuat, TenNuocSanXuat = @TenNuocSanXuat, MaDiaDiemXuatPhatVC = @MaDiaDiemXuatPhatVC, TenDiaDiemXuatPhatVC = @TenDiaDiemXuatPhatVC, MaDiaDiemDichVC = @MaDiaDiemDichVC, TenDiaDiemDichVC = @TenDiaDiemDichVC, LoaiHangHoa = @LoaiHangHoa, MaPhuongTienVC = @MaPhuongTienVC, TenPhuongTienVC = @TenPhuongTienVC, NgayHangDuKienDenDi = @NgayHangDuKienDenDi, MaNguoiNhapKhau = @MaNguoiNhapKhau, TenNguoiNhapKhau = @TenNguoiNhapKhau, DiaChiNguoiNhapKhau = @DiaChiNguoiNhapKhau, MaNguoiXuatKhua = @MaNguoiXuatKhua, TenNguoiXuatKhau = @TenNguoiXuatKhau, DiaChiNguoiXuatKhau = @DiaChiNguoiXuatKhau, MaNguoiUyThac = @MaNguoiUyThac, TenNguoiUyThac = @TenNguoiUyThac, DiaChiNguoiUyThac = @DiaChiNguoiUyThac, MaVanBanPhapLuat1 = @MaVanBanPhapLuat1, MaVanBanPhapLuat2 = @MaVanBanPhapLuat2, MaVanBanPhapLuat3 = @MaVanBanPhapLuat3, MaVanBanPhapLuat4 = @MaVanBanPhapLuat4, MaVanBanPhapLuat5 = @MaVanBanPhapLuat5, MaDVTTriGia = @MaDVTTriGia, TriGia = @TriGia, SoLuong = @SoLuong, MaDVTSoLuong = @MaDVTSoLuong, TongTrongLuong = @TongTrongLuong, MaDVTTrongLuong = @MaDVTTrongLuong, TheTich = @TheTich, MaDVTTheTich = @MaDVTTheTich, MaDanhDauDDKH1 = @MaDanhDauDDKH1, MaDanhDauDDKH2 = @MaDanhDauDDKH2, MaDanhDauDDKH3 = @MaDanhDauDDKH3, MaDanhDauDDKH4 = @MaDanhDauDDKH4, MaDanhDauDDKH5 = @MaDanhDauDDKH5, SoGiayPhep = @SoGiayPhep, NgayCapPhep = @NgayCapPhep, NgayHetHanCapPhep = @NgayHetHanCapPhep, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_VanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTTVanDon", SqlDbType.VarChar, "SoTTVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, "NgayPhatHanhVD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, "KyHieuVaSoHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, "NgayNhapKhoHQLanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, "PhanLoaiSanPhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocSanXuat", SqlDbType.VarChar, "MaNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNuocSanXuat", SqlDbType.VarChar, "TenNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, "MaDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, "TenDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, "MaDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, "TenDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.VarChar, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTienVC", SqlDbType.VarChar, "TenPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, "NgayHangDuKienDenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, "MaNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, "TenNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, "MaNguoiXuatKhua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, "TenNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiUyThac", SqlDbType.VarChar, "MaNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, "TenNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, "DiaChiNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, "MaVanBanPhapLuat1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, "MaVanBanPhapLuat2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, "MaVanBanPhapLuat3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, "MaVanBanPhapLuat4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, "MaVanBanPhapLuat5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTriGia", SqlDbType.VarChar, "MaDVTTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTrongLuong", SqlDbType.Decimal, "TongTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TheTich", SqlDbType.Decimal, "TheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTheTich", SqlDbType.VarChar, "MaDVTTheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, "MaDanhDauDDKH1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, "MaDanhDauDDKH2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, "MaDanhDauDDKH3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, "MaDanhDauDDKH4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, "MaDanhDauDDKH5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayPhep", SqlDbType.VarChar, "SoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, "NgayHetHanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTTVanDon", SqlDbType.VarChar, "SoTTVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, "NgayPhatHanhVD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, "KyHieuVaSoHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, "NgayNhapKhoHQLanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, "PhanLoaiSanPhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocSanXuat", SqlDbType.VarChar, "MaNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNuocSanXuat", SqlDbType.VarChar, "TenNuocSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, "MaDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, "TenDiaDiemXuatPhatVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, "MaDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, "TenDiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.VarChar, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongTienVC", SqlDbType.VarChar, "MaPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTienVC", SqlDbType.VarChar, "TenPhuongTienVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, "NgayHangDuKienDenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, "MaNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, "TenNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, "MaNguoiXuatKhua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, "TenNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiUyThac", SqlDbType.VarChar, "MaNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, "TenNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, "DiaChiNguoiUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, "MaVanBanPhapLuat1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, "MaVanBanPhapLuat2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, "MaVanBanPhapLuat3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, "MaVanBanPhapLuat4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, "MaVanBanPhapLuat5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTriGia", SqlDbType.VarChar, "MaDVTTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTrongLuong", SqlDbType.Decimal, "TongTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TheTich", SqlDbType.Decimal, "TheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTheTich", SqlDbType.VarChar, "MaDVTTheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, "MaDanhDauDDKH1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, "MaDanhDauDDKH2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, "MaDanhDauDDKH3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, "MaDanhDauDDKH4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, "MaDanhDauDDKH5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayPhep", SqlDbType.VarChar, "SoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, "NgayHetHanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TKVC_VanDon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TKVC_VanDon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TKVC_VanDon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TKVC_VanDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TKVC_VanDon(long master_ID, string soTTVanDon, string soVanDon, DateTime ngayPhatHanhVD, string moTaHangHoa, string maHS, string kyHieuVaSoHieu, DateTime ngayNhapKhoHQLanDau, string phanLoaiSanPhan, string maNuocSanXuat, string tenNuocSanXuat, string maDiaDiemXuatPhatVC, string tenDiaDiemXuatPhatVC, string maDiaDiemDichVC, string tenDiaDiemDichVC, string loaiHangHoa, string maPhuongTienVC, string tenPhuongTienVC, DateTime ngayHangDuKienDenDi, string maNguoiNhapKhau, string tenNguoiNhapKhau, string diaChiNguoiNhapKhau, string maNguoiXuatKhua, string tenNguoiXuatKhau, string diaChiNguoiXuatKhau, string maNguoiUyThac, string tenNguoiUyThac, string diaChiNguoiUyThac, string maVanBanPhapLuat1, string maVanBanPhapLuat2, string maVanBanPhapLuat3, string maVanBanPhapLuat4, string maVanBanPhapLuat5, string maDVTTriGia, decimal triGia, decimal soLuong, string maDVTSoLuong, decimal tongTrongLuong, string maDVTTrongLuong, decimal theTich, string maDVTTheTich, string maDanhDauDDKH1, string maDanhDauDDKH2, string maDanhDauDDKH3, string maDanhDauDDKH4, string maDanhDauDDKH5, string soGiayPhep, DateTime ngayCapPhep, DateTime ngayHetHanCapPhep, string ghiChu)
		{
			KDT_VNACC_TKVC_VanDon entity = new KDT_VNACC_TKVC_VanDon();	
			entity.Master_ID = master_ID;
			entity.SoTTVanDon = soTTVanDon;
			entity.SoVanDon = soVanDon;
			entity.NgayPhatHanhVD = ngayPhatHanhVD;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaHS = maHS;
			entity.KyHieuVaSoHieu = kyHieuVaSoHieu;
			entity.NgayNhapKhoHQLanDau = ngayNhapKhoHQLanDau;
			entity.PhanLoaiSanPhan = phanLoaiSanPhan;
			entity.MaNuocSanXuat = maNuocSanXuat;
			entity.TenNuocSanXuat = tenNuocSanXuat;
			entity.MaDiaDiemXuatPhatVC = maDiaDiemXuatPhatVC;
			entity.TenDiaDiemXuatPhatVC = tenDiaDiemXuatPhatVC;
			entity.MaDiaDiemDichVC = maDiaDiemDichVC;
			entity.TenDiaDiemDichVC = tenDiaDiemDichVC;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTienVC = tenPhuongTienVC;
			entity.NgayHangDuKienDenDi = ngayHangDuKienDenDi;
			entity.MaNguoiNhapKhau = maNguoiNhapKhau;
			entity.TenNguoiNhapKhau = tenNguoiNhapKhau;
			entity.DiaChiNguoiNhapKhau = diaChiNguoiNhapKhau;
			entity.MaNguoiXuatKhua = maNguoiXuatKhua;
			entity.TenNguoiXuatKhau = tenNguoiXuatKhau;
			entity.DiaChiNguoiXuatKhau = diaChiNguoiXuatKhau;
			entity.MaNguoiUyThac = maNguoiUyThac;
			entity.TenNguoiUyThac = tenNguoiUyThac;
			entity.DiaChiNguoiUyThac = diaChiNguoiUyThac;
			entity.MaVanBanPhapLuat1 = maVanBanPhapLuat1;
			entity.MaVanBanPhapLuat2 = maVanBanPhapLuat2;
			entity.MaVanBanPhapLuat3 = maVanBanPhapLuat3;
			entity.MaVanBanPhapLuat4 = maVanBanPhapLuat4;
			entity.MaVanBanPhapLuat5 = maVanBanPhapLuat5;
			entity.MaDVTTriGia = maDVTTriGia;
			entity.TriGia = triGia;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TongTrongLuong = tongTrongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.TheTich = theTich;
			entity.MaDVTTheTich = maDVTTheTich;
			entity.MaDanhDauDDKH1 = maDanhDauDDKH1;
			entity.MaDanhDauDDKH2 = maDanhDauDDKH2;
			entity.MaDanhDauDDKH3 = maDanhDauDDKH3;
			entity.MaDanhDauDDKH4 = maDanhDauDDKH4;
			entity.MaDanhDauDDKH5 = maDanhDauDDKH5;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayHetHanCapPhep = ngayHetHanCapPhep;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoTTVanDon", SqlDbType.VarChar, SoTTVanDon);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, NgayPhatHanhVD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhVD);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, KyHieuVaSoHieu);
			db.AddInParameter(dbCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, NgayNhapKhoHQLanDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoHQLanDau);
			db.AddInParameter(dbCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, PhanLoaiSanPhan);
			db.AddInParameter(dbCommand, "@MaNuocSanXuat", SqlDbType.VarChar, MaNuocSanXuat);
			db.AddInParameter(dbCommand, "@TenNuocSanXuat", SqlDbType.VarChar, TenNuocSanXuat);
			db.AddInParameter(dbCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, MaDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, TenDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, MaDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, TenDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.VarChar, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTienVC", SqlDbType.VarChar, TenPhuongTienVC);
			db.AddInParameter(dbCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, NgayHangDuKienDenDi.Year <= 1753 ? DBNull.Value : (object) NgayHangDuKienDenDi);
			db.AddInParameter(dbCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, MaNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, TenNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, DiaChiNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, MaNguoiXuatKhua);
			db.AddInParameter(dbCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, TenNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, DiaChiNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@MaNguoiUyThac", SqlDbType.VarChar, MaNguoiUyThac);
			db.AddInParameter(dbCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, TenNguoiUyThac);
			db.AddInParameter(dbCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, DiaChiNguoiUyThac);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, MaVanBanPhapLuat1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, MaVanBanPhapLuat2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, MaVanBanPhapLuat3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, MaVanBanPhapLuat4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, MaVanBanPhapLuat5);
			db.AddInParameter(dbCommand, "@MaDVTTriGia", SqlDbType.VarChar, MaDVTTriGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuong", SqlDbType.Decimal, TongTrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@TheTich", SqlDbType.Decimal, TheTich);
			db.AddInParameter(dbCommand, "@MaDVTTheTich", SqlDbType.VarChar, MaDVTTheTich);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, MaDanhDauDDKH1);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, MaDanhDauDDKH2);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, MaDanhDauDDKH3);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, MaDanhDauDDKH4);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, MaDanhDauDDKH5);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, NgayHetHanCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayHetHanCapPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TKVC_VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_VanDon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TKVC_VanDon(long id, long master_ID, string soTTVanDon, string soVanDon, DateTime ngayPhatHanhVD, string moTaHangHoa, string maHS, string kyHieuVaSoHieu, DateTime ngayNhapKhoHQLanDau, string phanLoaiSanPhan, string maNuocSanXuat, string tenNuocSanXuat, string maDiaDiemXuatPhatVC, string tenDiaDiemXuatPhatVC, string maDiaDiemDichVC, string tenDiaDiemDichVC, string loaiHangHoa, string maPhuongTienVC, string tenPhuongTienVC, DateTime ngayHangDuKienDenDi, string maNguoiNhapKhau, string tenNguoiNhapKhau, string diaChiNguoiNhapKhau, string maNguoiXuatKhua, string tenNguoiXuatKhau, string diaChiNguoiXuatKhau, string maNguoiUyThac, string tenNguoiUyThac, string diaChiNguoiUyThac, string maVanBanPhapLuat1, string maVanBanPhapLuat2, string maVanBanPhapLuat3, string maVanBanPhapLuat4, string maVanBanPhapLuat5, string maDVTTriGia, decimal triGia, decimal soLuong, string maDVTSoLuong, decimal tongTrongLuong, string maDVTTrongLuong, decimal theTich, string maDVTTheTich, string maDanhDauDDKH1, string maDanhDauDDKH2, string maDanhDauDDKH3, string maDanhDauDDKH4, string maDanhDauDDKH5, string soGiayPhep, DateTime ngayCapPhep, DateTime ngayHetHanCapPhep, string ghiChu)
		{
			KDT_VNACC_TKVC_VanDon entity = new KDT_VNACC_TKVC_VanDon();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoTTVanDon = soTTVanDon;
			entity.SoVanDon = soVanDon;
			entity.NgayPhatHanhVD = ngayPhatHanhVD;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaHS = maHS;
			entity.KyHieuVaSoHieu = kyHieuVaSoHieu;
			entity.NgayNhapKhoHQLanDau = ngayNhapKhoHQLanDau;
			entity.PhanLoaiSanPhan = phanLoaiSanPhan;
			entity.MaNuocSanXuat = maNuocSanXuat;
			entity.TenNuocSanXuat = tenNuocSanXuat;
			entity.MaDiaDiemXuatPhatVC = maDiaDiemXuatPhatVC;
			entity.TenDiaDiemXuatPhatVC = tenDiaDiemXuatPhatVC;
			entity.MaDiaDiemDichVC = maDiaDiemDichVC;
			entity.TenDiaDiemDichVC = tenDiaDiemDichVC;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTienVC = tenPhuongTienVC;
			entity.NgayHangDuKienDenDi = ngayHangDuKienDenDi;
			entity.MaNguoiNhapKhau = maNguoiNhapKhau;
			entity.TenNguoiNhapKhau = tenNguoiNhapKhau;
			entity.DiaChiNguoiNhapKhau = diaChiNguoiNhapKhau;
			entity.MaNguoiXuatKhua = maNguoiXuatKhua;
			entity.TenNguoiXuatKhau = tenNguoiXuatKhau;
			entity.DiaChiNguoiXuatKhau = diaChiNguoiXuatKhau;
			entity.MaNguoiUyThac = maNguoiUyThac;
			entity.TenNguoiUyThac = tenNguoiUyThac;
			entity.DiaChiNguoiUyThac = diaChiNguoiUyThac;
			entity.MaVanBanPhapLuat1 = maVanBanPhapLuat1;
			entity.MaVanBanPhapLuat2 = maVanBanPhapLuat2;
			entity.MaVanBanPhapLuat3 = maVanBanPhapLuat3;
			entity.MaVanBanPhapLuat4 = maVanBanPhapLuat4;
			entity.MaVanBanPhapLuat5 = maVanBanPhapLuat5;
			entity.MaDVTTriGia = maDVTTriGia;
			entity.TriGia = triGia;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TongTrongLuong = tongTrongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.TheTich = theTich;
			entity.MaDVTTheTich = maDVTTheTich;
			entity.MaDanhDauDDKH1 = maDanhDauDDKH1;
			entity.MaDanhDauDDKH2 = maDanhDauDDKH2;
			entity.MaDanhDauDDKH3 = maDanhDauDDKH3;
			entity.MaDanhDauDDKH4 = maDanhDauDDKH4;
			entity.MaDanhDauDDKH5 = maDanhDauDDKH5;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayHetHanCapPhep = ngayHetHanCapPhep;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TKVC_VanDon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoTTVanDon", SqlDbType.VarChar, SoTTVanDon);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, NgayPhatHanhVD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhVD);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, KyHieuVaSoHieu);
			db.AddInParameter(dbCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, NgayNhapKhoHQLanDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoHQLanDau);
			db.AddInParameter(dbCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, PhanLoaiSanPhan);
			db.AddInParameter(dbCommand, "@MaNuocSanXuat", SqlDbType.VarChar, MaNuocSanXuat);
			db.AddInParameter(dbCommand, "@TenNuocSanXuat", SqlDbType.VarChar, TenNuocSanXuat);
			db.AddInParameter(dbCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, MaDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, TenDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, MaDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, TenDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.VarChar, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTienVC", SqlDbType.VarChar, TenPhuongTienVC);
			db.AddInParameter(dbCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, NgayHangDuKienDenDi.Year <= 1753 ? DBNull.Value : (object) NgayHangDuKienDenDi);
			db.AddInParameter(dbCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, MaNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, TenNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, DiaChiNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, MaNguoiXuatKhua);
			db.AddInParameter(dbCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, TenNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, DiaChiNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@MaNguoiUyThac", SqlDbType.VarChar, MaNguoiUyThac);
			db.AddInParameter(dbCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, TenNguoiUyThac);
			db.AddInParameter(dbCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, DiaChiNguoiUyThac);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, MaVanBanPhapLuat1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, MaVanBanPhapLuat2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, MaVanBanPhapLuat3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, MaVanBanPhapLuat4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, MaVanBanPhapLuat5);
			db.AddInParameter(dbCommand, "@MaDVTTriGia", SqlDbType.VarChar, MaDVTTriGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuong", SqlDbType.Decimal, TongTrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@TheTich", SqlDbType.Decimal, TheTich);
			db.AddInParameter(dbCommand, "@MaDVTTheTich", SqlDbType.VarChar, MaDVTTheTich);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, MaDanhDauDDKH1);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, MaDanhDauDDKH2);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, MaDanhDauDDKH3);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, MaDanhDauDDKH4);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, MaDanhDauDDKH5);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, NgayHetHanCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayHetHanCapPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TKVC_VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_VanDon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TKVC_VanDon(long id, long master_ID, string soTTVanDon, string soVanDon, DateTime ngayPhatHanhVD, string moTaHangHoa, string maHS, string kyHieuVaSoHieu, DateTime ngayNhapKhoHQLanDau, string phanLoaiSanPhan, string maNuocSanXuat, string tenNuocSanXuat, string maDiaDiemXuatPhatVC, string tenDiaDiemXuatPhatVC, string maDiaDiemDichVC, string tenDiaDiemDichVC, string loaiHangHoa, string maPhuongTienVC, string tenPhuongTienVC, DateTime ngayHangDuKienDenDi, string maNguoiNhapKhau, string tenNguoiNhapKhau, string diaChiNguoiNhapKhau, string maNguoiXuatKhua, string tenNguoiXuatKhau, string diaChiNguoiXuatKhau, string maNguoiUyThac, string tenNguoiUyThac, string diaChiNguoiUyThac, string maVanBanPhapLuat1, string maVanBanPhapLuat2, string maVanBanPhapLuat3, string maVanBanPhapLuat4, string maVanBanPhapLuat5, string maDVTTriGia, decimal triGia, decimal soLuong, string maDVTSoLuong, decimal tongTrongLuong, string maDVTTrongLuong, decimal theTich, string maDVTTheTich, string maDanhDauDDKH1, string maDanhDauDDKH2, string maDanhDauDDKH3, string maDanhDauDDKH4, string maDanhDauDDKH5, string soGiayPhep, DateTime ngayCapPhep, DateTime ngayHetHanCapPhep, string ghiChu)
		{
			KDT_VNACC_TKVC_VanDon entity = new KDT_VNACC_TKVC_VanDon();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoTTVanDon = soTTVanDon;
			entity.SoVanDon = soVanDon;
			entity.NgayPhatHanhVD = ngayPhatHanhVD;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaHS = maHS;
			entity.KyHieuVaSoHieu = kyHieuVaSoHieu;
			entity.NgayNhapKhoHQLanDau = ngayNhapKhoHQLanDau;
			entity.PhanLoaiSanPhan = phanLoaiSanPhan;
			entity.MaNuocSanXuat = maNuocSanXuat;
			entity.TenNuocSanXuat = tenNuocSanXuat;
			entity.MaDiaDiemXuatPhatVC = maDiaDiemXuatPhatVC;
			entity.TenDiaDiemXuatPhatVC = tenDiaDiemXuatPhatVC;
			entity.MaDiaDiemDichVC = maDiaDiemDichVC;
			entity.TenDiaDiemDichVC = tenDiaDiemDichVC;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaPhuongTienVC = maPhuongTienVC;
			entity.TenPhuongTienVC = tenPhuongTienVC;
			entity.NgayHangDuKienDenDi = ngayHangDuKienDenDi;
			entity.MaNguoiNhapKhau = maNguoiNhapKhau;
			entity.TenNguoiNhapKhau = tenNguoiNhapKhau;
			entity.DiaChiNguoiNhapKhau = diaChiNguoiNhapKhau;
			entity.MaNguoiXuatKhua = maNguoiXuatKhua;
			entity.TenNguoiXuatKhau = tenNguoiXuatKhau;
			entity.DiaChiNguoiXuatKhau = diaChiNguoiXuatKhau;
			entity.MaNguoiUyThac = maNguoiUyThac;
			entity.TenNguoiUyThac = tenNguoiUyThac;
			entity.DiaChiNguoiUyThac = diaChiNguoiUyThac;
			entity.MaVanBanPhapLuat1 = maVanBanPhapLuat1;
			entity.MaVanBanPhapLuat2 = maVanBanPhapLuat2;
			entity.MaVanBanPhapLuat3 = maVanBanPhapLuat3;
			entity.MaVanBanPhapLuat4 = maVanBanPhapLuat4;
			entity.MaVanBanPhapLuat5 = maVanBanPhapLuat5;
			entity.MaDVTTriGia = maDVTTriGia;
			entity.TriGia = triGia;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TongTrongLuong = tongTrongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.TheTich = theTich;
			entity.MaDVTTheTich = maDVTTheTich;
			entity.MaDanhDauDDKH1 = maDanhDauDDKH1;
			entity.MaDanhDauDDKH2 = maDanhDauDDKH2;
			entity.MaDanhDauDDKH3 = maDanhDauDDKH3;
			entity.MaDanhDauDDKH4 = maDanhDauDDKH4;
			entity.MaDanhDauDDKH5 = maDanhDauDDKH5;
			entity.SoGiayPhep = soGiayPhep;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayHetHanCapPhep = ngayHetHanCapPhep;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoTTVanDon", SqlDbType.VarChar, SoTTVanDon);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhVD", SqlDbType.DateTime, NgayPhatHanhVD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhVD);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@KyHieuVaSoHieu", SqlDbType.VarChar, KyHieuVaSoHieu);
			db.AddInParameter(dbCommand, "@NgayNhapKhoHQLanDau", SqlDbType.DateTime, NgayNhapKhoHQLanDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoHQLanDau);
			db.AddInParameter(dbCommand, "@PhanLoaiSanPhan", SqlDbType.VarChar, PhanLoaiSanPhan);
			db.AddInParameter(dbCommand, "@MaNuocSanXuat", SqlDbType.VarChar, MaNuocSanXuat);
			db.AddInParameter(dbCommand, "@TenNuocSanXuat", SqlDbType.VarChar, TenNuocSanXuat);
			db.AddInParameter(dbCommand, "@MaDiaDiemXuatPhatVC", SqlDbType.VarChar, MaDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemXuatPhatVC", SqlDbType.VarChar, TenDiaDiemXuatPhatVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemDichVC", SqlDbType.VarChar, MaDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@TenDiaDiemDichVC", SqlDbType.VarChar, TenDiaDiemDichVC);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.VarChar, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaPhuongTienVC", SqlDbType.VarChar, MaPhuongTienVC);
			db.AddInParameter(dbCommand, "@TenPhuongTienVC", SqlDbType.VarChar, TenPhuongTienVC);
			db.AddInParameter(dbCommand, "@NgayHangDuKienDenDi", SqlDbType.DateTime, NgayHangDuKienDenDi.Year <= 1753 ? DBNull.Value : (object) NgayHangDuKienDenDi);
			db.AddInParameter(dbCommand, "@MaNguoiNhapKhau", SqlDbType.VarChar, MaNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiNhapKhau", SqlDbType.NVarChar, TenNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhapKhau", SqlDbType.NVarChar, DiaChiNguoiNhapKhau);
			db.AddInParameter(dbCommand, "@MaNguoiXuatKhua", SqlDbType.VarChar, MaNguoiXuatKhua);
			db.AddInParameter(dbCommand, "@TenNguoiXuatKhau", SqlDbType.NVarChar, TenNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatKhau", SqlDbType.NVarChar, DiaChiNguoiXuatKhau);
			db.AddInParameter(dbCommand, "@MaNguoiUyThac", SqlDbType.VarChar, MaNguoiUyThac);
			db.AddInParameter(dbCommand, "@TenNguoiUyThac", SqlDbType.NVarChar, TenNguoiUyThac);
			db.AddInParameter(dbCommand, "@DiaChiNguoiUyThac", SqlDbType.NVarChar, DiaChiNguoiUyThac);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat1", SqlDbType.VarChar, MaVanBanPhapLuat1);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat2", SqlDbType.VarChar, MaVanBanPhapLuat2);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat3", SqlDbType.VarChar, MaVanBanPhapLuat3);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat4", SqlDbType.VarChar, MaVanBanPhapLuat4);
			db.AddInParameter(dbCommand, "@MaVanBanPhapLuat5", SqlDbType.VarChar, MaVanBanPhapLuat5);
			db.AddInParameter(dbCommand, "@MaDVTTriGia", SqlDbType.VarChar, MaDVTTriGia);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuong", SqlDbType.Decimal, TongTrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@TheTich", SqlDbType.Decimal, TheTich);
			db.AddInParameter(dbCommand, "@MaDVTTheTich", SqlDbType.VarChar, MaDVTTheTich);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH1", SqlDbType.VarChar, MaDanhDauDDKH1);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH2", SqlDbType.VarChar, MaDanhDauDDKH2);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH3", SqlDbType.VarChar, MaDanhDauDDKH3);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH4", SqlDbType.VarChar, MaDanhDauDDKH4);
			db.AddInParameter(dbCommand, "@MaDanhDauDDKH5", SqlDbType.VarChar, MaDanhDauDDKH5);
			db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.VarChar, SoGiayPhep);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayHetHanCapPhep", SqlDbType.DateTime, NgayHetHanCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayHetHanCapPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TKVC_VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_VanDon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TKVC_VanDon(long id)
		{
			KDT_VNACC_TKVC_VanDon entity = new KDT_VNACC_TKVC_VanDon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TKVC_VanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_VanDon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}