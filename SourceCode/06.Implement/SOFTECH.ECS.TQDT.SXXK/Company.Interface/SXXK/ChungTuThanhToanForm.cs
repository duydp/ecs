﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SXXK;
using Company.BLL.KDT.SXXK;
using Company.BLL.KDT.SXXK.CTTT;
using Company.Interface.Report.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
namespace Company.Interface.SXXK
{
    public partial class ChungTuThanhToanForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        DataSet ds = new DataSet();
        ChungTuThanhToanCollection ChungTuCollection = new ChungTuThanhToanCollection();
        public ChungTuThanhToanForm()
        {
            InitializeComponent();
        }
        private DataTable ToKhaiXuatDataSource()
        {
            DataTable dtToKhaiXuat = new DataTable("ToKhaiXuat");
            DataColumn[] cols = new DataColumn[10];
            cols[0] = new DataColumn("ToKhaiXuat", typeof(string));
            cols[1] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
            cols[2] = new DataColumn("SoNgayHopDong", typeof(string));
            cols[3] = new DataColumn("MaHangXuat", typeof(string));
            cols[4] = new DataColumn("TriGiaHopDong", typeof(decimal));
            cols[5] = new DataColumn("TriGiaHangThucXuat", typeof(decimal));
            cols[6] = new DataColumn("GhiChu", typeof(string));
            cols[7] = new DataColumn("SoToKhai", typeof(string));
            dtToKhaiXuat.Columns.AddRange(cols);
            return dtToKhaiXuat;
        }
        private DataTable ChungTuThanhToanDataSource()
        {
            DataTable dtChungTuThanhToan = new DataTable("ChungTuThanhToan");

            DataColumn[] cols1 = new DataColumn[10];
            cols1[0] = new DataColumn("SoNgayChungTu", typeof(string));
            cols1[1] = new DataColumn("HinhThucThanhToan", typeof(string));
            cols1[2] = new DataColumn("TriGiaCT", typeof(decimal));
            cols1[3] = new DataColumn("ToKhaiXuat", typeof(string));
            cols1[4] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
            cols1[5] = new DataColumn("GhiChuCT", typeof(string));
            cols1[6] = new DataColumn("SoToKhai", typeof(string));

            dtChungTuThanhToan.Columns.AddRange(cols1);
            return dtChungTuThanhToan;

        }
        private decimal GetTongTriGiaNT(HangMauDichCollection HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.TriGiaKB;
            return d;
        }
        private void ChungTuThanhToanForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.ChungTuCollection = new ChungTuThanhToan().SelectCollectionDynamic(" LanThanhLy = " + this.HSTL.LanThanhLy, " ToKhaiXuat, NgayDangKyXuat");

                DataTable dtToKhaiXuat = ToKhaiXuatDataSource();
                DataTable dtChungTuThanhToan = ChungTuThanhToanDataSource();

                ds.Tables.Add(dtToKhaiXuat);
                ds.Tables.Add(dtChungTuThanhToan);

                ds.Relations.Add("ChungTuThanhToan", new DataColumn[] { dtToKhaiXuat.Columns["SoToKhai"] }, new DataColumn[] { dtChungTuThanhToan.Columns["SoToKhai"] }, false);

                this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].LoadChiTietBangKe();

                if (ChungTuCollection.Count == 0)
                {
                    BKToKhaiXuatCollection bkCollection = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;
                    foreach (BKToKhaiXuat bk in bkCollection)
                    {
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        TKMD.SoToKhai = bk.SoToKhai;
                        TKMD.NamDangKy = bk.NamDangKy;
                        TKMD.MaLoaiHinh = bk.MaLoaiHinh;
                        TKMD.MaHaiQuan = bk.MaHaiQuan;
                        TKMD.Load();
                        TKMD.LoadHMDCollection();

                        //Hungtq update 05/03/2011. 
                        //Tim thong tin cua chung tu co so hop dong va ngay hop dong truoc do.
                        ChungTuThanhToan cttt = FindCTTT(ChungTuCollection, TKMD.SoHopDong, TKMD.NgayHopDong.ToString("dd/MM/yyyy"));

                        DataRow dr = dtToKhaiXuat.NewRow();
                        dr["ToKhaiXuat"] = bk.SoToKhai + "/" + bk.MaLoaiHinh;
                        dr["SoToKhai"] = bk.SoToKhai;
                        dr["NgayDangKyXuat"] = bk.NgayDangKy;
                        dr["SoNgayHopDong"] = TKMD.SoHopDong + "\r\n" + "(" + TKMD.NgayHopDong.ToString("dd/MM/yy") + ")";
                        //dr["TriGiaHopDong"] = 0;
                        dr["TriGiaHopDong"] = cttt != null ? cttt.TriGiaHopDong : 0; //Hungtq update
                        dr["TriGiaHangThucXuat"] = GetTongTriGiaNT(TKMD.HMDCollection);
                        //dr["GhiChu"] = "";

                        foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
                        {
                            dr["MaHangXuat"] = dr["MaHangXuat"].ToString() + HMD.MaPhu + "\r\n";
                        }
                        dr["MaHangXuat"] = dr["MaHangXuat"].ToString().Remove(dr["MaHangXuat"].ToString().Length - 2);
                        dtToKhaiXuat.Rows.Add(dr);

                        List<ChungTuChiTiet> ctctCollections = (List<ChungTuChiTiet>)ChungTuChiTiet.SelectCollectionDynamic("SoToKhai=" + bk.SoToKhai + " and NgayDangKy='" + bk.NgayDangKy.ToString("yyyy-MM-dd") + "'", "");
                        decimal tongtrigia = 0M;
                        foreach (ChungTuChiTiet item in ctctCollections)
                        {
                            tongtrigia += item.TriGia;
                            ChungTu chungtu = ChungTu.Load(item.IDCT);
                            DataRow drChungTu = dtChungTuThanhToan.NewRow();
                            drChungTu["SoNgayChungTu"] = string.Format("{0},{1}", chungtu.SoChungTu, chungtu.NgayChungTu.ToString("dd/MM/yyyy"));
                            drChungTu["HinhThucThanhToan"] = chungtu.HinhThucThanhToan;
                            drChungTu["TriGiaCT"] = chungtu.TongTriGia;
                            //                        drChungTu["ToKhaiXuat"] = string.Format("{0}/{1}",item.SoToKhai,item.MaLoaiHinh);
                            drChungTu["SoToKhai"] = item.SoToKhai;

                            string ghichu = string.Empty;

                            List<ChungTuChiTiet> ctctsGhichu = (List<ChungTuChiTiet>)ChungTuChiTiet.SelectCollectionBy_IDCT(chungtu.ID);
                            foreach (ChungTuChiTiet itemchitiet in ctctsGhichu)
                            {
                                ghichu += string.Format("TK{0}/{1}={2}\r\n", itemchitiet.SoToKhai, itemchitiet.MaLoaiHinh, itemchitiet.TriGia.ToString("N4"));
                            }
                            List<ChiPhiKhac> chiphikhacs = (List<ChiPhiKhac>)ChiPhiKhac.SelectCollectionBy_IDCT(chungtu.ID);
                            foreach (ChiPhiKhac itemChiPhiKhac in chiphikhacs)
                            {
                                tongtrigia += itemChiPhiKhac.TriGia;
                                ghichu += string.Format("{0}={1}\r\n", Company.BLL.DuLieuChuan.LoaiPhiChungTuThanhToan.getName(itemChiPhiKhac.MaChiPhi), itemChiPhiKhac.TriGia.ToString("N4"));
                            }
                            drChungTu["GhiChuCT"] = ghichu;
                            dtChungTuThanhToan.Rows.Add(drChungTu);
                        }
                        decimal tonggiatrikhaibao = (decimal)dr["TriGiaHangThucXuat"];
                        if (tonggiatrikhaibao > tongtrigia)
                            dr["GhiChu"] = string.Format("Tổng trị giá còn dư {0}", (tonggiatrikhaibao - tongtrigia).ToString("N4"));
                        else if (tonggiatrikhaibao < tongtrigia)
                            dr["GhiChu"] = string.Format("Tổng trị giá bị âm {0}", (tonggiatrikhaibao - tongtrigia).ToString("N4"));


                    }

                }

                else
                {

                    foreach (ChungTuThanhToan ct in this.ChungTuCollection)
                    {
                        string[] sArr = ct.ToKhaiXuat.Split(new string[] { "/" }, StringSplitOptions.None);

                        DataRow dr2 = dtChungTuThanhToan.NewRow();
                        dr2["SoNgayChungTu"] = ct.SoNgayChungTu;
                        dr2["TriGiaCT"] = ct.TriGiaCT;
                        dr2["HinhThucThanhToan"] = ct.HinhThucThanhToan;
                        dr2["GhiChuCT"] = ct.GhiChu;
                        dtChungTuThanhToan.Rows.Add(dr2);
                        dr2["SoToKhai"] = sArr[0];

                        DataRow dr1 = dtToKhaiXuat.NewRow();

                        dr1["SoNgayHopDong"] = ct.SoNgayHopDong;
                        dr1["TriGiaHopDong"] = ct.TriGiaHopDong;
                        dr1["MaHangXuat"] = ct.MaHangXuat;
                        dr1["TriGiaHangThucXuat"] = ct.TriGiaHangThucXuat;
                        dr1["ToKhaiXuat"] = ct.ToKhaiXuat;
                        dr1["NgayDangKyXuat"] = ct.NgayDangKyXuat;
                        dr1["GhiChu"] = ct.GhiChu;
                        dr1["SoToKhai"] = sArr[0];
                        dtToKhaiXuat.Rows.Add(dr1);

                    }

                }

                dgList.DataSource = ds;
                dgList.DataMember = "ToKhaiXuat";
            }
            catch (Exception ex)
            { }
        }

        private ChungTuThanhToan FindCTTT(ChungTuThanhToanCollection listCTTT, string soHopDong, string ngayHopDong)
        {
            foreach (ChungTuThanhToan ctItem in listCTTT)
            {
                if (ctItem.SoNgayHopDong.Contains(soHopDong) && ctItem.SoNgayHopDong.Contains(ngayHopDong))
                {
                    return ctItem;
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listCTTT"></param>
        /// <param name="soNgayHopDong">Bao gom: So hong va ngay hop dong</param>
        /// <returns></returns>
        private ChungTuThanhToan FindCTTT2(ChungTuThanhToanCollection listCTTT, string soNgayHopDong, string soToKhai)
        {
            foreach (ChungTuThanhToan ctItem in listCTTT)
            {
                if (ctItem.SoNgayHopDong.Equals(soNgayHopDong) && ctItem.GhiChu.ToLower().Contains("tk" + soToKhai))
                {
                    return ctItem;
                }
            }

            return null;
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {

            ChungTuThanhToanCollection chungtus = new ChungTuThanhToanCollection();
            DataTable dtChungTu = ds.Tables["ChungTuThanhToan"];
            foreach (DataRow dr in ds.Tables["ToKhaiXuat"].Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                {
                    ChungTuThanhToan ct = new ChungTuThanhToan();
                    ct.LanThanhLy = this.HSTL.LanThanhLy;
                    ct.SoNgayHopDong = dr["SoNgayHopDong"].ToString();
                    ct.TriGiaHopDong = Convert.ToDecimal(dr["TriGiaHopDong"]);
                    ct.MaHangXuat = dr["MaHangXuat"].ToString();
                    ct.TriGiaHangThucXuat = Convert.ToDecimal(dr["TriGiaHangThucXuat"]);
                    ct.ToKhaiXuat = dr["ToKhaiXuat"].ToString();
                    ct.NgayDangKyXuat = Convert.ToDateTime(dr["NgayDangKyXuat"]);
                    DataRow[] arrItem = dtChungTu.Select("SoToKhai=" + dr["SoToKhai"]);
                    if (arrItem.Length > 0 && arrItem[0].RowState != DataRowState.Deleted)
                    {
                        ct.SoNgayChungTu = arrItem[0]["SoNgayChungTu"].ToString();
                        ct.TriGiaCT = Convert.ToDecimal(arrItem[0]["TriGiaCT"]);
                        ct.HinhThucThanhToan = arrItem[0]["HinhThucThanhToan"].ToString();
                    }
                    chungtus.Add(ct);
                }
            }
            #region OLD
            //foreach (DataRow dr2 in this.ds.Tables["ChungTuThanhToan"].Rows)
            //{
            //    if (dr2.RowState != DataRowState.Deleted)
            //    {
            //        ChungTuThanhToan ct = new ChungTuThanhToan();
            //        ct.LanThanhLy = this.HSTL.LanThanhLy;
            //        ct.SoNgayChungTu = dr2["SoNgayChungTu"].ToString();
            //        ct.TriGiaCT = Convert.ToDecimal(dr2["TriGiaCT"]);

            //        ct.HinhThucThanhToan = dr2["HinhThucThanhToan"].ToString();
            //        ct.GhiChu = dr2["GhiChuCT"].ToString();
            //        DataRow dr1 = GetParent(dr2["SoToKhai"].ToString(), ct.NgayDangKyXuat);
            //        if (dr1 != null && dr1.RowState != DataRowState.Deleted)
            //        {

            //            ct.SoNgayHopDong = dr1["SoNgayHopDong"].ToString();
            //            ct.TriGiaHopDong = Convert.ToDecimal(dr1["TriGiaHopDong"]);
            //            ct.MaHangXuat = dr1["MaHangXuat"].ToString();
            //            ct.TriGiaHangThucXuat = Convert.ToDecimal(dr1["TriGiaHangThucXuat"]);
            //            ct.ToKhaiXuat = dr1["ToKhaiXuat"].ToString();
            //            ct.NgayDangKyXuat = Convert.ToDateTime(dr1["NgayDangKyXuat"]);
            //            // ct.GhiChu = dr1["GhiChu"].ToString();
            //            chungtus.Add(ct);
            //        }
            //    }
            //}
            #endregion OLD

            try
            {
                if (chungtus.Count != 0)
                {
                    new ChungTuThanhToan().DeleteCollection(this.ChungTuCollection);
                    this.ChungTuCollection.Clear();

                    new ChungTuThanhToan().InsertFull(chungtus, this.HSTL.LanThanhLy);
                    //ShowMessage("Lưu thông tin bảng chứng từ thành công.", false);
                    this.ChungTuCollection.AddRange(chungtus);
                    MLMessages("Lưu thông tin bảng chứng từ thành công.", "MSG_SAV02", "", false);
                }
                else
                {
                    MLMessages("Không có dòng nào để lưu.", "MSG_SAV02", "", false);
                }

            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }
        private DataRow GetParent(string toKhaiXuat, DateTime ngayDangKyXuat)
        {
            foreach (DataRow dr in this.ds.Tables["ToKhaiXuat"].Rows)
            {
                if (dr["SoToKhai"].ToString() == toKhaiXuat /* && Convert.ToDateTime(dr["NgayDangKyXuat"]) == ngayDangKyXuat*/)
                {
                    return dr;
                }
            }
            return null;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (this.ChungTuCollection.Count == 0)
            {
                //ShowMessage("Bạn phải lưu trước khi xuất báo cáo.", false);
                MLMessages("Bạn phải lưu trước khi xuất báo cáo.", "MSG_THK37", "", false);
                return;
            }
            ChungTuThanhToanReport report = new ChungTuThanhToanReport();

            report.SoHSTK = this.HSTL.SoHoSo;
            report.CTCollection = this.ChungTuCollection;
            report.BindReport();
            report.ShowPreview();
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                // if (ShowMessage("Bạn có muốn xóa bảng chứng từ này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa bảng chứng từ này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    new ChungTuThanhToan().DeleteDynamicTransaction("LanThanhLy = " + this.HSTL.LanThanhLy, null);
                    // ShowMessage("Xóa bảng chứng từ thành công.", false);
                    MLMessages("Xóa bảng chứng từ thành công.", "MSG_THK39", "", false);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void btnHDxuatkhau_Click(object sender, EventArgs e)
        {
            FrmQuanLyHopDongXuatKhau frm = new FrmQuanLyHopDongXuatKhau();
            frm.Show();


        }

        private void uiCommandManager_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "cmdQuanLyHopDongXuatKhau")
            {
                FrmQuanLyHopDongXuatKhau frmQLHD = new FrmQuanLyHopDongXuatKhau();
                frmQLHD.HSTL = HSTL;
                frmQLHD.Show();
            }
            else if (e.Command.Key == "cmdQuanLyChungTu")
            {
                FormQuanLyChungTuThanhToan frmQLCT = new FormQuanLyChungTuThanhToan();
                frmQLCT.HSTL = HSTL;
                frmQLCT.ShowDialog();
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                //ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                //ToKhaiMauDich tkmd = new ToKhaiMauDich();
                //tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                //tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                //tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                //tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                //tkmd.Load();
                //f.TKMD = tkmd;
                //f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                //f.ShowDialog();

                
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                string[] str = e.Row.Cells[0].Text.Split(new string[] { "/" }, StringSplitOptions.None);
                tkmd.SoToKhai = Convert.ToInt32(str[0]);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKyXuat"].Value).Year);
                tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tkmd.MaLoaiHinh = str[1];
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }
    }
}

