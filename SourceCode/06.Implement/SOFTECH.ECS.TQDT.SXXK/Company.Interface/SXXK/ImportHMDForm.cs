using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ToKhai;
using System.Globalization;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportHMDForm : BaseForm
    {
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public HangMauDichDieuChinhCollection HMDDCCollection = new HangMauDichDieuChinhCollection();
        public ImportHMDForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(string str) 
        {
            if (str.Length == 1)
                return str[0] - 'A';
            else
            {
                int t = (str[0] - 'A' + 1) * 26 + (str[1] - 'A');
                return t;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSave.Enabled = false;

                if (!new HangMauDich().InsertUpdate(this.HMDCollection))
                {
                    ShowMessage("Import Error.", false);
                    return;
                }

                //if (!new HangMauDichDieuChinh().InsertUpdate(this.HMDDCCollection))
                //{
                //    ShowMessage("Import lỗi.", false);
                //    return;
                //}
                new HoSoThanhLyDangKy().FixDB(GlobalSettings.MA_DON_VI);
                MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
               // ShowMessage("Import thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Error: " + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
                this.Close();
            }

        }



        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true );
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
              //  ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        NumberFormatInfo format = new NumberFormatInfo();
                        format.NumberDecimalSeparator = ".";
                        format.NumberGroupSeparator = ",";
                        HangMauDich HMD = new HangMauDich();
                        HangMauDichDieuChinh HMDDC = new HangMauDichDieuChinh();
                        HMD.MaHaiQuan = HMDDC.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        HMDDC.LanDieuChinh = 0;
                        HMDDC.SoToKhai = HMD.SoToKhai = Convert.ToInt32(wsr.Cells[ConvertCharToInt(txtSoToKhai.Text)].Value);
                        HMDDC.MaLoaiHinh = HMD.MaLoaiHinh = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaLoaiHinh.Text)].Value);
                        HMDDC.NamDangKy = HMD.NamDangKy = Convert.ToInt16(wsr.Cells[ConvertCharToInt(txtNamDangKy.Text)].Value);
                        HMDDC.STTHang = HMD.SoThuTuHang = Convert.ToInt16(wsr.Cells[ConvertCharToInt(txtSoThuTuHang.Text)].Value);
                        HMDDC.MaHS = HMDDC.MaHSKB = HMD.MaHS = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaHS.Text)].Value);
                        HMDDC.MaHang = HMDDC.MA_NPL_SP = HMD.MaPhu = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaHang.Text)].Value);
                        HMDDC.TenHang = HMD.TenHang = Convert.ToString(wsr.Cells[ConvertCharToInt(txtTenHang.Text)].Value);
                        HMDDC.NuocXX_ID = HMD.NuocXX_ID = Convert.ToString(wsr.Cells[ConvertCharToInt(txtNuocXX.Text)].Value);
                        HMDDC.DVT_ID = HMD.DVT_ID = Convert.ToString(wsr.Cells[ConvertCharToInt(txtDVT.Text)].Value);
                        HMDDC.Luong = HMD.SoLuong = (decimal)Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtSoLuong.Text)].Value, format);
                        HMD.DonGiaKB = (decimal) Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtDGKB.Text)].Value, format);
                        try
                        {
                            HMD.DonGiaTT = (decimal)Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtDGTT.Text)].Value, format);
                        }
                        catch { }
                        HMDDC.DGIA_KB = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtDGKB.Text)].Value, format);

                        try
                        {
                            HMDDC.DGIA_TT = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtDGTT.Text)].Value, format);
                        }
                        catch { }
                        HMDDC.MA_DG = "HD";

                        HMD.TriGiaKB = (decimal)Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGKB.Text)].Value, format);
                        HMD.TriGiaTT = (decimal)Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGTT.Text)].Value, format);
                        HMD.TriGiaKB_VND = (decimal)Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGKBVND.Text)].Value, format);
                        HMDDC.TRIGIA_KB = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGKB.Text)].Value, format);
                        HMDDC.TRIGIA_TT = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGTT.Text)].Value, format);
                        HMDDC.TGKB_VND = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGKBVND.Text)].Value, format);

                        try
                        {
                            HMDDC.TS_XNK = HMD.ThueSuatXNK = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtTSXNK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.TS_TTDB = HMD.ThueSuatTTDB = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtTSTTDB.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.TS_VAT = HMD.ThueSuatGTGT = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtTSGTGT.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMD.ThueXNK = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtThueXNK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMD.ThueTTDB = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtThueTTDB.Text)].Value, format);
                        }
                        catch
                        { }
                        try
                        {
                            HMD.ThueGTGT = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtThueGTGT.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMD.PhuThu = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtPhuThu.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.THUE_XNK = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtThueXNK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.THUE_TTDB = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtThueTTDB.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.THUE_VAT = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtThueGTGT.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.PHU_THU = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtPhuThu.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.TYLE_THUKHAC = HMD.TyLeThuKhac = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtTLTK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMD.TriGiaThuKhac = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtTGTK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.TRIGIA_THUKHAC = Convert.ToDouble(wsr.Cells[ConvertCharToInt(txtTGTK.Text)].Value, format);
                        }
                        catch { }
                        try
                        {
                            HMDDC.MIENTHUE = HMD.MienThue = Convert.ToByte(wsr.Cells[ConvertCharToInt(txtMienThue.Text)].Value, format);
                        }
                        catch { }
                        this.HMDCollection.Add(HMD);
                        this.HMDDCCollection.Add(HMDDC);
                    }
                    catch
                    {
                       // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if(MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.uiButton1.Enabled = true;
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
            }
            this.uiButton1.Enabled = true;
            this.Cursor = Cursors.Default;
            //ShowMessage("Đọc file thành công, có tất cả " + this.HMDCollection.Count + " dòng hàng", false);
            MLMessages("Đọc file thành công, có tất cả " + this.HMDCollection.Count + " dòng hàng", "MSG_EXC07", "", false);
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }


        private void ImportHMDForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtNuocXX_TextChanged(object sender, EventArgs e)
        {

        }


    }
}