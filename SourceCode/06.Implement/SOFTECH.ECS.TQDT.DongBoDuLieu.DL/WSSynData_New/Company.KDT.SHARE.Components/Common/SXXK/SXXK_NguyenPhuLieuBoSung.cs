using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common.SXXK
{
    public partial class SXXK_NguyenPhuLieuBoSung
    {

        public static IList<SXXK_NguyenPhuLieuBoSung> SelectCollectionDynamicNguyenPhuLieuBoSung(string maHQ, string maDN, string maNPL)
        {
            string whereCondition = string.Format("MaHaiQuan = '{0}' and MaDoanhNghiep = '{1}' and Ma = '{2}'", maHQ, maDN, maNPL);
            IDataReader reader = SelectReaderDynamic(whereCondition, "");
            return ConvertToCollection(reader);
        }

    }
}