﻿namespace Company.Interface
{
    partial class BaoLanhThueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoLanhThueForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNamChungTuBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoNgayDuocBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoDuTienBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbLoaiBaoLanh = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ccNgayKyBaoLanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.ccNgayHetHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtSoGiayBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDonViBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(551, 215);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtNamChungTuBaoLanh);
            this.uiGroupBox2.Controls.Add(this.txtSoNgayDuocBaoLanh);
            this.uiGroupBox2.Controls.Add(this.txtSoTienBaoLanh);
            this.uiGroupBox2.Controls.Add(this.txtSoDuTienBaoLanh);
            this.uiGroupBox2.Controls.Add(this.cbLoaiBaoLanh);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.ccNgayKyBaoLanh);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.ccNgayHetHieuLuc);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Controls.Add(this.txtSoGiayBaoLanh);
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtDonViBaoLanh);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayHieuLuc);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(551, 215);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtNamChungTuBaoLanh
            // 
            this.txtNamChungTuBaoLanh.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamChungTuBaoLanh.Location = new System.Drawing.Point(428, 52);
            this.txtNamChungTuBaoLanh.MaxLength = 9999;
            this.txtNamChungTuBaoLanh.Name = "txtNamChungTuBaoLanh";
            this.txtNamChungTuBaoLanh.Size = new System.Drawing.Size(95, 21);
            this.txtNamChungTuBaoLanh.TabIndex = 28;
            this.txtNamChungTuBaoLanh.Text = "0";
            this.txtNamChungTuBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamChungTuBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamChungTuBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // txtSoNgayDuocBaoLanh
            // 
            this.txtSoNgayDuocBaoLanh.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoNgayDuocBaoLanh.Location = new System.Drawing.Point(428, 84);
            this.txtSoNgayDuocBaoLanh.Name = "txtSoNgayDuocBaoLanh";
            this.txtSoNgayDuocBaoLanh.Size = new System.Drawing.Size(95, 21);
            this.txtSoNgayDuocBaoLanh.TabIndex = 28;
            this.txtSoNgayDuocBaoLanh.Text = "0";
            this.txtSoNgayDuocBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoNgayDuocBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienBaoLanh
            // 
            this.txtSoTienBaoLanh.Location = new System.Drawing.Point(130, 114);
            this.txtSoTienBaoLanh.Name = "txtSoTienBaoLanh";
            this.txtSoTienBaoLanh.Size = new System.Drawing.Size(155, 21);
            this.txtSoTienBaoLanh.TabIndex = 27;
            this.txtSoTienBaoLanh.Text = "0.00";
            this.txtSoTienBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoTienBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDuTienBaoLanh
            // 
            this.txtSoDuTienBaoLanh.Location = new System.Drawing.Point(130, 150);
            this.txtSoDuTienBaoLanh.Name = "txtSoDuTienBaoLanh";
            this.txtSoDuTienBaoLanh.Size = new System.Drawing.Size(155, 21);
            this.txtSoDuTienBaoLanh.TabIndex = 26;
            this.txtSoDuTienBaoLanh.Text = "0.00";
            this.txtSoDuTienBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoDuTienBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbLoaiBaoLanh
            // 
            this.cbLoaiBaoLanh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Bão lãnh riêng";
            uiComboBoxItem3.Value = "1";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Bão lãnh chung";
            uiComboBoxItem4.Value = "2";
            this.cbLoaiBaoLanh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbLoaiBaoLanh.Location = new System.Drawing.Point(131, 84);
            this.cbLoaiBaoLanh.Name = "cbLoaiBaoLanh";
            this.cbLoaiBaoLanh.Size = new System.Drawing.Size(154, 21);
            this.cbLoaiBaoLanh.TabIndex = 3;
            this.cbLoaiBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Số giấy bảo lãnh";
            // 
            // ccNgayKyBaoLanh
            // 
            // 
            // 
            // 
            this.ccNgayKyBaoLanh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayKyBaoLanh.DropDownCalendar.Name = "";
            this.ccNgayKyBaoLanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKyBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKyBaoLanh.IsNullDate = true;
            this.ccNgayKyBaoLanh.Location = new System.Drawing.Point(428, 19);
            this.ccNgayKyBaoLanh.Name = "ccNgayKyBaoLanh";
            this.ccNgayKyBaoLanh.Nullable = true;
            this.ccNgayKyBaoLanh.NullButtonText = "Xóa";
            this.ccNgayKyBaoLanh.ShowNullButton = true;
            this.ccNgayKyBaoLanh.Size = new System.Drawing.Size(95, 21);
            this.ccNgayKyBaoLanh.TabIndex = 8;
            this.ccNgayKyBaoLanh.TodayButtonText = "Hôm nay";
            this.ccNgayKyBaoLanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKyBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(308, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Ngày ký bảo lãnh";
            // 
            // ccNgayHetHieuLuc
            // 
            // 
            // 
            // 
            this.ccNgayHetHieuLuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHetHieuLuc.DropDownCalendar.Name = "";
            this.ccNgayHetHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHieuLuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHetHieuLuc.IsNullDate = true;
            this.ccNgayHetHieuLuc.Location = new System.Drawing.Point(428, 150);
            this.ccNgayHetHieuLuc.Name = "ccNgayHetHieuLuc";
            this.ccNgayHetHieuLuc.Nullable = true;
            this.ccNgayHetHieuLuc.NullButtonText = "Xóa";
            this.ccNgayHetHieuLuc.ShowNullButton = true;
            this.ccNgayHetHieuLuc.Size = new System.Drawing.Size(95, 21);
            this.ccNgayHetHieuLuc.TabIndex = 1;
            this.ccNgayHetHieuLuc.TodayButtonText = "Hôm nay";
            this.ccNgayHetHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHieuLuc.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(267, 180);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtSoGiayBaoLanh
            // 
            this.txtSoGiayBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayBaoLanh.Location = new System.Drawing.Point(131, 52);
            this.txtSoGiayBaoLanh.MaxLength = 255;
            this.txtSoGiayBaoLanh.Name = "txtSoGiayBaoLanh";
            this.txtSoGiayBaoLanh.Size = new System.Drawing.Size(154, 21);
            this.txtSoGiayBaoLanh.TabIndex = 4;
            this.txtSoGiayBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoGiayBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(361, 180);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(77, 23);
            this.btnGhi.TabIndex = 10;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(308, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Năm chứng từ bảo lãnh";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(444, 180);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(308, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ngày hết hiệu lực";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Số dư tiền bảo lãnh";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 21);
            this.label2.TabIndex = 14;
            this.label2.Text = "Số tiền bảo lãnh";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(308, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Ngày hiệu lực";
            // 
            // txtDonViBaoLanh
            // 
            this.txtDonViBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViBaoLanh.Location = new System.Drawing.Point(131, 19);
            this.txtDonViBaoLanh.MaxLength = 255;
            this.txtDonViBaoLanh.Name = "txtDonViBaoLanh";
            this.txtDonViBaoLanh.Size = new System.Drawing.Size(154, 21);
            this.txtDonViBaoLanh.TabIndex = 0;
            this.txtDonViBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonViBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonViBaoLanh.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Loại bảo lãnh";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(308, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số ngày được bảo lãnh";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Đơn vị bảo lãnh";
            // 
            // ccNgayHieuLuc
            // 
            // 
            // 
            // 
            this.ccNgayHieuLuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHieuLuc.DropDownCalendar.Name = "";
            this.ccNgayHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHieuLuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHieuLuc.IsNullDate = true;
            this.ccNgayHieuLuc.Location = new System.Drawing.Point(428, 114);
            this.ccNgayHieuLuc.Name = "ccNgayHieuLuc";
            this.ccNgayHieuLuc.Nullable = true;
            this.ccNgayHieuLuc.NullButtonText = "Xóa";
            this.ccNgayHieuLuc.ShowNullButton = true;
            this.ccNgayHieuLuc.Size = new System.Drawing.Size(95, 21);
            this.ccNgayHieuLuc.TabIndex = 2;
            this.ccNgayHieuLuc.TodayButtonText = "Hôm nay";
            this.ccNgayHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHieuLuc.VisualStyleManager = this.vsmMain;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // BaoLanhThueForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 215);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "BaoLanhThueForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin chứng từ bão lãnh thuế";
            this.Load += new System.EventHandler(this.ChungTuNoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHetHieuLuc;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayBaoLanh;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonViBaoLanh;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHieuLuc;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKyBaoLanh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIComboBox cbLoaiBaoLanh;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDuTienBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNgayDuocBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamChungTuBaoLanh;
    }
}