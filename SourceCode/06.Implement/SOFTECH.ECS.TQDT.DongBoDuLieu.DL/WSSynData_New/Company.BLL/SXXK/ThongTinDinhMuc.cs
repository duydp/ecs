﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Transactions;
namespace Company.BLL.SXXK
{
  public partial  class ThongTinDinhMuc
    {
      public void Saved(ThongTinDinhMucCollection collection)
      {
          using (SqlConnection connection = (SqlConnection)db.CreateConnection())
          {
              connection.Open();
              SqlTransaction transaction = connection.BeginTransaction();
              try
              {
                  foreach (ThongTinDinhMuc item in collection)
                  {
                      InsertUpdateTransaction(transaction);
                  }
                  transaction.Commit();
              }
              catch
              {
                  transaction.Rollback();
                  throw;
              }
              finally
              {
                  connection.Close();
              }
          }
      }
      public void InsertUpdateCollections(ThongTinDinhMucCollection collection, SqlTransaction transaction)
      {
          using (SqlConnection connection = (SqlConnection)db.CreateConnection())
          {
              connection.Open();              
              try
              {
                  foreach (ThongTinDinhMuc item in collection)
                  {
                      item.InsertUpdateTransaction(transaction);
                  }                  
              }
              catch(Exception ex)
              {
                  Logger.LocalLogger.Instance().WriteMessage(ex);
                  throw;
              }
              finally
              {
                  connection.Close();
              }
          }

      }
    }
}
