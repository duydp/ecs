﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class PhanLoaiKiemTraThucTe_VAD4870 : DevExpress.XtraReports.UI.XtraReport
    {
        public string maNV = null;
        public PhanLoaiKiemTraThucTe_VAD4870()
        {
            InitializeComponent();
        }
        public void BindReport(VAD4870 vad487)
        {
            lblPhanLoaiKiemTraThucTe.Text = vad487.K01.GetValue().ToString();
            lblSoToKhai.Text = vad487.K02.GetValue().ToString();
            lblMaLoaiHinh.Text = vad487.K04.GetValue().ToString();
            lblMaPhanLoaiHangHoa.Text = vad487.ADA.GetValue().ToString();
            lblMaPhanLoaiVanChuyen.Text = vad487.ADB.GetValue().ToString();
            lblMaNguoiKhai.Text = vad487.K06.GetValue().ToString();
            lblCoQuanHaiQuan.Text = vad487.K07.GetValue().ToString();
            lblNhomXuLyHoSo.Text = vad487.K08.GetValue().ToString();
            lblSoVanDon.Text = vad487.K09.GetValue().ToString();
            lblSoMAWB.Text = vad487.K10.GetValue().ToString();
            lblMaDiaDiemLuuKho.Text = vad487.K11.GetValue().ToString();
            lbltenDiaDiemLuuKho.Text = vad487.K12.GetValue().ToString();
            lblSoLuong.Text = vad487.K13.GetValue().ToString();
            lblDonGiaSoLuong.Text = vad487.K14.GetValue().ToString();
            lblTrongLuongHang.Text = vad487.K15.GetValue().ToString();
            lblDVTTrongLuongHang.Text = vad487.K16.GetValue().ToString();
            lblMaTauDuKienChatHang.Text = vad487.K20.GetValue().ToString();
            lblTenTauDuKienChatHang.Text = vad487.K21.GetValue().ToString();
            lblMaNguoiXuatKhau.Text = vad487.K25.GetValue().ToString();
            lblTenNguoiXuatKhau.Text = vad487.K26.GetValue().ToString();
            lblSoKyHieu.Text = vad487.K29.GetValue().ToString();
            lblTongSoContainer.Text = vad487.K30.GetValue().ToString();
            lblSoCuaContainerBiKiemHoa1.Text = vad487.A1.GetValue().ToString();
            lblSoCuaContainerBiKiemHoa2.Text = vad487.A2.GetValue().ToString();
            lblSoCuaContainerBiKiemHoa3.Text = vad487.A3.GetValue().ToString();
            lblSoCuaContainerBiKiemHoa4.Text = vad487.A4.GetValue().ToString();
            lblSoCuaContainerBiKiemHoa5.Text = vad487.A5.GetValue().ToString();

            lblCoBaoCoHon5ContainerBiKiemHoa.Text = vad487.K31.GetValue().ToString();
            lblMaDiaDiemGiaoHangDich.Text = vad487.K37.GetValue().ToString();
            lblSoQuanLyNoiBoDoanhNghiep.Text = vad487.K38.GetValue().ToString();
            lblMaPhanLoaiKiemTra.Text = vad487.K39.GetValue().ToString();
            lblTenPhanLoaiKiemTra.Text = vad487.K40.GetValue().ToString();
            lblPhanLoaiKiemTraBanDau.Text = vad487.K41.GetValue().ToString();
            lblMaPhanLoaiKiemHoa.Text = vad487.K42.GetValue().ToString();
            lblNoiDungPhanLoaiKiemHoa.Text = vad487.K43.GetValue().ToString();
            lblMaPhanLoaiPhuTrach.Text = vad487.K44.GetValue().ToString();
            lblNguoiChuyenLuongKiemHoa.Text = vad487.K45.GetValue().ToString();
            lblMaDiaDiemThucHienKiemHoa.Text = vad487.K46.GetValue().ToString();
            lblTenDiaDiemThucHienKiemHoa.Text = vad487.K47.GetValue().ToString();
            lblNoiDungChiDinhKiemHoa.Text = vad487.K50.GetValue().ToString();
            lblMaPhuongPhapChiDinhKiemHoa.Text = vad487.K51.GetValue().ToString();
            lblTenPhuongPhapChiDinhKiemHoa.Text = vad487.K52.GetValue().ToString();
            lblCoBaoKiemHoaToanBo.Text = vad487.K53.GetValue().ToString();



        }
      
    }
}
