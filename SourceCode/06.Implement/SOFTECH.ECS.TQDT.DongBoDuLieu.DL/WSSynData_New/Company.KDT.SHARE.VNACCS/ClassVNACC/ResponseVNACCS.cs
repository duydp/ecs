﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class ResponseVNACCS
    {
        public VNACCHeader header { get; set; }
        public List<ErrorVNACCS> Error { get; set; }
        //public ErrorVNACCS Error1 { get; set; }
        //public ErrorVNACCS Error2 { get; set; }
        //public ErrorVNACCS Error3 { get; set; }
        //public ErrorVNACCS Error4 { get; set; }
        //public ErrorVNACCS Error5 { get; set; }
        public ReturnMessages ResponseData { get; set; }
        public ResponseVNACCS(string result)
        {
            try
            {

            int index = 0;
            string[] StringLines = result.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            if (StringLines.Length > 0)
            {
                for (int i = 0; i < StringLines.Length; i++)
                {
                    if (StringLines[i].Length == 398 || ASCIIEncoding.ASCII.GetBytes(StringLines[i]).Length == 400)
                    {
                        this.header = new VNACCHeader(StringLines[i]);
                        index = i; // 398 byte đầu tiên
                        break;
                    }
                }
                if (this.header == null)
                    {
                        string head = result.Substring(0, 398);
                        this.header = new VNACCHeader(head);
                        //byte[] ketqua = UTF8Encoding.UTF8.GetBytes(result);
                        
                        //string temp = UTF8Encoding.UTF8.GetString(UTF8Encoding.UTF8.GetBytes(result));
                        StringLines = HelperVNACCS.SubStringByBytes(result, 398).Split(new string[] { Environment.NewLine }, StringSplitOptions.None);//result.Substring(398, result.Length - 398).Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    }
                if (this.header != null)
                    index++; // 75 byte tiếp theo.
                this.Error = new List<ErrorVNACCS>();
                if (StringLines[index].Length == 75)
                {
                    ErrorVNACCS Error1 = new ErrorVNACCS(StringLines[index].Substring(0, 15));
                    if (Error1.isError)
                        this.Error.Add(Error1);
                    ErrorVNACCS Error2 = new ErrorVNACCS(StringLines[index].Substring(15, 15));
                    if (Error2.isError)
                        this.Error.Add(Error2);
                    ErrorVNACCS Error3 = new ErrorVNACCS(StringLines[index].Substring(30, 15));
                    if (Error3.isError)
                        this.Error.Add(Error3);
                    ErrorVNACCS Error4 = new ErrorVNACCS(StringLines[index].Substring(45, 15));
                    if (Error4.isError)
                        this.Error.Add(Error4);
                    ErrorVNACCS Error5 = new ErrorVNACCS(StringLines[index].Substring(60, 15));
                    if (Error5.isError)
                        this.Error.Add(Error5);
                }
                else
                    index++; // Phần respone Data
                string[] LinesMessageRespones = new string[StringLines.Length - index];
                for (int i =0;i<LinesMessageRespones.Length;i++)
                {
                    
                    LinesMessageRespones[i] = StringLines[index+i];
                }

                this.ResponseData = new ReturnMessages(LinesMessageRespones);

            }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Logger.LocalLogger.Instance().WriteMessage(new Exception(result));
               // Logger.LocalLogger.Instance().WriteMessage(new Exception(result));
                throw ex;
            }
        }
        public string GetError()
        {
            string msgError = "Khai báo thông tin thành công.";
            if(this.Error != null && this.Error.Count > 0)
            {
                msgError = "Thông tin khai báo bị lỗi: " + Environment.NewLine;
                for(int i = 0;i<this.Error.Count;i++)
                {
                    msgError = msgError + "Lỗi thứ " + (i + 1).ToString() + Environment.NewLine;
                    msgError = msgError + this.Error[i].loadError() + Environment.NewLine ;
                }

            }




            return msgError;
        }
         
    }
}
