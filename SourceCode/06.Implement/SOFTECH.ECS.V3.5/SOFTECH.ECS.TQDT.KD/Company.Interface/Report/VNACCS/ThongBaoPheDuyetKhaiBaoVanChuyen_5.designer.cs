namespace Company.Interface.Report.VNACCS
{
    partial class ThongBaoPheDuyetKhaiBaoVanChuyen_5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoBaoYeuCauXN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoBaoXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayLapTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTieuDe = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongHangTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 1.041667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 110F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(762.91F, 5.000003F);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(25F, 21.25F);
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "5";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 80F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(591.0942F, 51F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.67999999999999994;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.37311666585953396;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Số hiệu container/";
            this.xrTableCell1.Weight = 0.77872169429315141;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.79740846107801067;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 1.0507531787693039;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.67999999999999994;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Số tiêu đề";
            this.xrTableCell5.Weight = 0.37311666585953396;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "toa tàu/";
            this.xrTableCell6.Weight = 0.7787216555714308;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Số dòng hàng trên tờ khai";
            this.xrTableCell7.Weight = 0.79740834491284884;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Số seal";
            this.xrTableCell8.Weight = 1.0507533336561865;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.67999999999999994;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.37311666585953396;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "kiện hàng";
            this.xrTableCell10.Weight = 0.77872169429315141;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.79740846107801067;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 1.0507531787693039;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999855F, 38.25F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27,
            this.xrTableRow28});
            this.xrTable2.SizeF = new System.Drawing.SizeF(792.7576F, 40F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell174,
            this.lblMaCoBaoYeuCauXN,
            this.xrTableCell172,
            this.lblTenCoBaoYeuCauXN,
            this.xrTableCell177,
            this.xrTableCell176,
            this.xrTableCell179,
            this.lblCoQuanHQ});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.8;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "Cờ báo yêu cầu xác nhận/ niêm phong";
            this.xrTableCell171.Weight = 1.9558137486142411;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.050000000061550018;
            // 
            // lblMaCoBaoYeuCauXN
            // 
            this.lblMaCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaCoBaoYeuCauXN.Name = "lblMaCoBaoYeuCauXN";
            this.lblMaCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblMaCoBaoYeuCauXN.Text = "X";
            this.lblMaCoBaoYeuCauXN.Weight = 0.16366943278882012;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "-";
            this.xrTableCell172.Weight = 0.099999999713554236;
            // 
            // lblTenCoBaoYeuCauXN
            // 
            this.lblTenCoBaoYeuCauXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoBaoYeuCauXN.Name = "lblTenCoBaoYeuCauXN";
            this.lblTenCoBaoYeuCauXN.StylePriority.UseFont = false;
            this.lblTenCoBaoYeuCauXN.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblTenCoBaoYeuCauXN.Weight = 3.6414587401013137;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.050000000000035169;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "Cơ quan Hải quan";
            this.xrTableCell176.Weight = 1.0187979114347625;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.049999999999988165;
            // 
            // lblCoQuanHQ
            // 
            this.lblCoQuanHQ.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanHQ.Name = "lblCoQuanHQ";
            this.lblCoQuanHQ.StylePriority.UseFont = false;
            this.lblCoQuanHQ.Text = "XXXXXXXXXE";
            this.lblCoQuanHQ.Weight = 0.897836308643158;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181,
            this.lblSoToKhaiVC,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.lblCoBaoXNK,
            this.xrTableCell187,
            this.xrTableCell190,
            this.xrTableCell189,
            this.lblNgayLapTK});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.79999999999999993;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Số tờ khai vận chuyển";
            this.xrTableCell180.Weight = 1.1950402806143874;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.050000000049673005;
            // 
            // lblSoToKhaiVC
            // 
            this.lblSoToKhaiVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiVC.Name = "lblSoToKhaiVC";
            this.lblSoToKhaiVC.StylePriority.UseFont = false;
            this.lblSoToKhaiVC.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiVC.Weight = 1.2659251429199241;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.053198547363281334;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Cờ báo nhập khẩu/ xuất khẩu";
            this.xrTableCell184.Weight = 1.8565628051757819;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.053198852539062313;
            // 
            // lblCoBaoXNK
            // 
            this.lblCoBaoXNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoBaoXNK.Name = "lblCoBaoXNK";
            this.lblCoBaoXNK.StylePriority.UseFont = false;
            this.lblCoBaoXNK.Text = "X";
            this.lblCoBaoXNK.Weight = 1.4370162926173746;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.050000610351550789;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Ngày lập tờ khai";
            this.xrTableCell190.Weight = 1.0187982166105849;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.049999389648413994;
            // 
            // lblNgayLapTK
            // 
            this.lblNgayLapTK.Name = "lblNgayLapTK";
            this.lblNgayLapTK.Text = "dd/MM/yyyy";
            this.lblNgayLapTK.Weight = 0.89783600346738857;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(205.19F, 5F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(420F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Thông báo phê duyệt khai báo vận chuyển";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail1.HeightF = 42F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable3.SizeF = new System.Drawing.SizeF(792.76F, 42F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTieuDe,
            this.lblSoHieuContainer,
            this.lblSoDongHangTK,
            this.lblSoSeal1,
            this.lblSoSeal2,
            this.lblSoSeal3});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 0.8;
            // 
            // lblSoTieuDe
            // 
            this.lblSoTieuDe.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.lblSoTieuDe.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTieuDe.Name = "lblSoTieuDe";
            this.lblSoTieuDe.StylePriority.UseBorders = false;
            this.lblSoTieuDe.StylePriority.UseFont = false;
            this.lblSoTieuDe.Text = "XXE";
            this.lblSoTieuDe.Weight = 0.27820245025248796;
            this.lblSoTieuDe.AfterPrint += new System.EventHandler(this.lblSoTieuDe_AfterPrint);
            // 
            // lblSoHieuContainer
            // 
            this.lblSoHieuContainer.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuContainer.Name = "lblSoHieuContainer";
            this.lblSoHieuContainer.StylePriority.UseFont = false;
            this.lblSoHieuContainer.Text = "XXXXXXXXX1XXXXXXE";
            this.lblSoHieuContainer.Weight = 0.58062878550081731;
            // 
            // lblSoDongHangTK
            // 
            this.lblSoDongHangTK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongHangTK.Name = "lblSoDongHangTK";
            this.lblSoDongHangTK.StylePriority.UseFont = false;
            this.lblSoDongHangTK.Text = "XXXXE";
            this.lblSoDongHangTK.Weight = 0.59456179020596545;
            // 
            // lblSoSeal1
            // 
            this.lblSoSeal1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal1.Name = "lblSoSeal1";
            this.lblSoSeal1.StylePriority.UseFont = false;
            this.lblSoSeal1.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal1.Weight = 0.51087501065362317;
            // 
            // lblSoSeal2
            // 
            this.lblSoSeal2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal2.Name = "lblSoSeal2";
            this.lblSoSeal2.StylePriority.UseFont = false;
            this.lblSoSeal2.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal2.Weight = 0.51087501065362317;
            // 
            // lblSoSeal3
            // 
            this.lblSoSeal3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoSeal3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal3.Name = "lblSoSeal3";
            this.lblSoSeal3.StylePriority.UseBorders = false;
            this.lblSoSeal3.StylePriority.UseFont = false;
            this.lblSoSeal3.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal3.Weight = 0.524856952733483;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell16,
            this.xrTableCell17,
            this.lblSoSeal4,
            this.lblSoSeal5,
            this.lblSoSeal6});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.Weight = 0.8;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Weight = 0.27820245025248796;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.58062878550081731;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.59456179020596545;
            // 
            // lblSoSeal4
            // 
            this.lblSoSeal4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSoSeal4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal4.Name = "lblSoSeal4";
            this.lblSoSeal4.StylePriority.UseBorders = false;
            this.lblSoSeal4.StylePriority.UseFont = false;
            this.lblSoSeal4.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal4.Weight = 0.51087501065362317;
            // 
            // lblSoSeal5
            // 
            this.lblSoSeal5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblSoSeal5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal5.Name = "lblSoSeal5";
            this.lblSoSeal5.StylePriority.UseBorders = false;
            this.lblSoSeal5.StylePriority.UseFont = false;
            this.lblSoSeal5.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal5.Weight = 0.51087501065362317;
            // 
            // lblSoSeal6
            // 
            this.lblSoSeal6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblSoSeal6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal6.Name = "lblSoSeal6";
            this.lblSoSeal6.StylePriority.UseBorders = false;
            this.lblSoSeal6.StylePriority.UseFont = false;
            this.lblSoSeal6.Text = "XXXXXXXXX1XXXXE";
            this.lblSoSeal6.Weight = 0.524856952733483;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(9.999903F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(792.76F, 2F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.TongSoTrang,
            this.lblTenThongTinXuat,
            this.xrTable2,
            this.xrTable1,
            this.lblSoTrang});
            this.PageHeader.HeightF = 131.2084F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(787.91F, 5.000003F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(8F, 21.25F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "/";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // TongSoTrang
            // 
            this.TongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(795.91F, 5.000003F);
            this.TongSoTrang.Name = "TongSoTrang";
            this.TongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TongSoTrang.SizeF = new System.Drawing.SizeF(25F, 21.25F);
            this.TongSoTrang.StylePriority.UseTextAlignment = false;
            this.TongSoTrang.Text = "XX";
            this.TongSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ThongBaoPheDuyetKhaiBaoVanChuyen_5
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(14, 11, 9, 110);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoBaoYeuCauXN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanHQ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell lblCoBaoXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayLapTK;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTieuDe;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuContainer;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongHangTK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal5;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal6;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel TongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
    }
}
