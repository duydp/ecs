IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]'))
	DROP TABLE [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[T_SXXK_NPL_QUYETTOAN]'))
	DROP TABLE [dbo].[T_SXXK_NPL_QUYETTOAN]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[T_SXXK_SANPHAM_QUYETTOAN]'))
	DROP TABLE [dbo].[T_SXXK_SANPHAM_QUYETTOAN]
GO

CREATE TABLE [dbo].[T_SXXK_NPL_QUYETTOAN]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[MANPL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENNPL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGTONDK] [numeric] (18, 5) NULL,
[TRIGIATONDK] [numeric] (18, 6) NULL,
[LUONGNHAPTK] [numeric] (18, 5) NULL,
[TRIGIANHAPTK] [numeric] (18, 6) NULL,
[LUONGXUATTK] [numeric] (18, 5) NULL,
[TRIGIAXUATTK] [numeric] (18, 6) NULL,
[LUONGTONCK] [numeric] (18, 5) NULL,
[TRIGIATONCK] [numeric] (18, 6) NULL,
[NGAYBATDAU] DATETIME ,
[NGAYKETTHUC] DATETIME
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[TOKHAIXUAT] [decimal] (18, 0) NOT NULL,
[TOKHAINHAP] [decimal] (18, 0) NOT NULL,
[MANPL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENNPL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_NPL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MASP] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENSP] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_SP] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGXUAT] [numeric] (18, 5) NULL,
[TRIGIAXUAT] [numeric] (18, 6) NULL,
[LUONGNHAP] [numeric] (18, 5) NULL,
[LUONGTONDAU] [numeric] (18, 5) NULL,
[DONGIANHAP] [numeric] (18, 6) NULL,
[TRIGIANHAP] [numeric] (18, 6) NULL,
[DONGIANHAPTT] [numeric] (18, 6) NULL,
[TRIGIANHAPTT] [numeric] (18, 6) NULL,
[TYGIATINHTHUE] [int] NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_SXXK_SANPHAM_QUYETTOAN]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[MASP] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENSP] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGTONDK] [numeric] (18, 5) NULL,
[TRIGIATONDK] [numeric] (18, 6) NULL,
[LUONGNHAPTK] [numeric] (18, 5) NULL,
[TRIGIANHAPTK] [numeric] (18, 6) NULL,
[LUONGXUATTK] [numeric] (18, 5) NULL,
[TRIGIAXUATTK] [numeric] (18, 6) NULL,
[LUONGTONCK] [numeric] (18, 5) NULL,
[TRIGIATONCK] [numeric] (18, 6) NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Insert]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Update]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Delete]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Load]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Insert]
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NGAYBATDAU datetime,
	@NGAYKETTHUC datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_SXXK_NPL_QUYETTOAN]
(
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NGAYBATDAU],
	[NGAYKETTHUC]
)
VALUES 
(
	@MANPL,
	@TENNPL,
	@DVT,
	@LUONGTONDK,
	@TRIGIATONDK,
	@LUONGNHAPTK,
	@TRIGIANHAPTK,
	@LUONGXUATTK,
	@TRIGIAXUATTK,
	@LUONGTONCK,
	@TRIGIATONCK,
	@NGAYBATDAU,
	@NGAYKETTHUC
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Update]
	@ID bigint,
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NGAYBATDAU datetime,
	@NGAYKETTHUC datetime
AS

UPDATE
	[dbo].[T_SXXK_NPL_QUYETTOAN]
SET
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[DVT] = @DVT,
	[LUONGTONDK] = @LUONGTONDK,
	[TRIGIATONDK] = @TRIGIATONDK,
	[LUONGNHAPTK] = @LUONGNHAPTK,
	[TRIGIANHAPTK] = @TRIGIANHAPTK,
	[LUONGXUATTK] = @LUONGXUATTK,
	[TRIGIAXUATTK] = @TRIGIAXUATTK,
	[LUONGTONCK] = @LUONGTONCK,
	[TRIGIATONCK] = @TRIGIATONCK,
	[NGAYBATDAU] = @NGAYBATDAU,
	[NGAYKETTHUC] = @NGAYKETTHUC
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_InsertUpdate]
	@ID bigint,
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NGAYBATDAU datetime,
	@NGAYKETTHUC datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_SXXK_NPL_QUYETTOAN] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_SXXK_NPL_QUYETTOAN] 
		SET
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[DVT] = @DVT,
			[LUONGTONDK] = @LUONGTONDK,
			[TRIGIATONDK] = @TRIGIATONDK,
			[LUONGNHAPTK] = @LUONGNHAPTK,
			[TRIGIANHAPTK] = @TRIGIANHAPTK,
			[LUONGXUATTK] = @LUONGXUATTK,
			[TRIGIAXUATTK] = @TRIGIAXUATTK,
			[LUONGTONCK] = @LUONGTONCK,
			[TRIGIATONCK] = @TRIGIATONCK,
			[NGAYBATDAU] = @NGAYBATDAU,
			[NGAYKETTHUC] = @NGAYKETTHUC
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_SXXK_NPL_QUYETTOAN]
		(
			[MANPL],
			[TENNPL],
			[DVT],
			[LUONGTONDK],
			[TRIGIATONDK],
			[LUONGNHAPTK],
			[TRIGIANHAPTK],
			[LUONGXUATTK],
			[TRIGIAXUATTK],
			[LUONGTONCK],
			[TRIGIATONCK],
			[NGAYBATDAU],
			[NGAYKETTHUC]
		)
		VALUES 
		(
			@MANPL,
			@TENNPL,
			@DVT,
			@LUONGTONDK,
			@TRIGIATONDK,
			@LUONGNHAPTK,
			@TRIGIANHAPTK,
			@LUONGXUATTK,
			@TRIGIAXUATTK,
			@LUONGTONCK,
			@TRIGIATONCK,
			@NGAYBATDAU,
			@NGAYKETTHUC
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_SXXK_NPL_QUYETTOAN]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_SXXK_NPL_QUYETTOAN] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NGAYBATDAU],
	[NGAYKETTHUC]
FROM
	[dbo].[T_SXXK_NPL_QUYETTOAN]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NGAYBATDAU],
	[NGAYKETTHUC]
FROM [dbo].[T_SXXK_NPL_QUYETTOAN] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NGAYBATDAU],
	[NGAYKETTHUC]
FROM
	[dbo].[T_SXXK_NPL_QUYETTOAN]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Insert]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Update]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Delete]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Load]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Insert]
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NAMQUYETTOAN int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_SXXK_SANPHAM_QUYETTOAN]
(
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NAMQUYETTOAN]
)
VALUES 
(
	@MASP,
	@TENSP,
	@DVT,
	@LUONGTONDK,
	@TRIGIATONDK,
	@LUONGNHAPTK,
	@TRIGIANHAPTK,
	@LUONGXUATTK,
	@TRIGIAXUATTK,
	@LUONGTONCK,
	@TRIGIATONCK,
	@NAMQUYETTOAN
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Update]
	@ID bigint,
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_SXXK_SANPHAM_QUYETTOAN]
SET
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[DVT] = @DVT,
	[LUONGTONDK] = @LUONGTONDK,
	[TRIGIATONDK] = @TRIGIATONDK,
	[LUONGNHAPTK] = @LUONGNHAPTK,
	[TRIGIANHAPTK] = @TRIGIANHAPTK,
	[LUONGXUATTK] = @LUONGXUATTK,
	[TRIGIAXUATTK] = @TRIGIAXUATTK,
	[LUONGTONCK] = @LUONGTONCK,
	[TRIGIATONCK] = @TRIGIATONCK,
	[NAMQUYETTOAN] = @NAMQUYETTOAN
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_InsertUpdate]
	@ID bigint,
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_SXXK_SANPHAM_QUYETTOAN] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_SXXK_SANPHAM_QUYETTOAN] 
		SET
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[DVT] = @DVT,
			[LUONGTONDK] = @LUONGTONDK,
			[TRIGIATONDK] = @TRIGIATONDK,
			[LUONGNHAPTK] = @LUONGNHAPTK,
			[TRIGIANHAPTK] = @TRIGIANHAPTK,
			[LUONGXUATTK] = @LUONGXUATTK,
			[TRIGIAXUATTK] = @TRIGIAXUATTK,
			[LUONGTONCK] = @LUONGTONCK,
			[TRIGIATONCK] = @TRIGIATONCK,
			[NAMQUYETTOAN] = @NAMQUYETTOAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_SXXK_SANPHAM_QUYETTOAN]
		(
			[MASP],
			[TENSP],
			[DVT],
			[LUONGTONDK],
			[TRIGIATONDK],
			[LUONGNHAPTK],
			[TRIGIANHAPTK],
			[LUONGXUATTK],
			[TRIGIAXUATTK],
			[LUONGTONCK],
			[TRIGIATONCK],
			[NAMQUYETTOAN]
		)
		VALUES 
		(
			@MASP,
			@TENSP,
			@DVT,
			@LUONGTONDK,
			@TRIGIATONDK,
			@LUONGNHAPTK,
			@TRIGIANHAPTK,
			@LUONGXUATTK,
			@TRIGIAXUATTK,
			@LUONGTONCK,
			@TRIGIATONCK,
			@NAMQUYETTOAN
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_SXXK_SANPHAM_QUYETTOAN]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_SXXK_SANPHAM_QUYETTOAN] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_SXXK_SANPHAM_QUYETTOAN]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NAMQUYETTOAN]
FROM [dbo].[T_SXXK_SANPHAM_QUYETTOAN] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_SANPHAM_QUYETTOAN_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_SXXK_SANPHAM_QUYETTOAN]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Insert]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Update]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Delete]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Load]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Insert]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Insert]
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT_NPL nvarchar(10),
	@MASP varchar(500),
	@TENSP nvarchar(500),
	@DVT_SP nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@LUONGTONDAU numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@DONGIANHAPTT numeric(18, 6),
	@TRIGIANHAPTT numeric(18, 6),
	@TYGIATINHTHUE int,
	@NAMQUYETTOAN int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
(
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT_NPL],
	[MASP],
	[TENSP],
	[DVT_SP],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[LUONGTONDAU],
	[DONGIANHAP],
	[TRIGIANHAP],
	[DONGIANHAPTT],
	[TRIGIANHAPTT],
	[TYGIATINHTHUE],
	[NAMQUYETTOAN]
)
VALUES 
(
	@TOKHAIXUAT,
	@TOKHAINHAP,
	@MANPL,
	@TENNPL,
	@DVT_NPL,
	@MASP,
	@TENSP,
	@DVT_SP,
	@LUONGXUAT,
	@TRIGIAXUAT,
	@LUONGNHAP,
	@LUONGTONDAU,
	@DONGIANHAP,
	@TRIGIANHAP,
	@DONGIANHAPTT,
	@TRIGIANHAPTT,
	@TYGIATINHTHUE,
	@NAMQUYETTOAN
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Update]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Update]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT_NPL nvarchar(10),
	@MASP varchar(500),
	@TENSP nvarchar(500),
	@DVT_SP nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@LUONGTONDAU numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@DONGIANHAPTT numeric(18, 6),
	@TRIGIANHAPTT numeric(18, 6),
	@TYGIATINHTHUE int,
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
SET
	[TOKHAIXUAT] = @TOKHAIXUAT,
	[TOKHAINHAP] = @TOKHAINHAP,
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[DVT_NPL] = @DVT_NPL,
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[DVT_SP] = @DVT_SP,
	[LUONGXUAT] = @LUONGXUAT,
	[TRIGIAXUAT] = @TRIGIAXUAT,
	[LUONGNHAP] = @LUONGNHAP,
	[LUONGTONDAU] = @LUONGTONDAU,
	[DONGIANHAP] = @DONGIANHAP,
	[TRIGIANHAP] = @TRIGIANHAP,
	[DONGIANHAPTT] = @DONGIANHAPTT,
	[TRIGIANHAPTT] = @TRIGIANHAPTT,
	[TYGIATINHTHUE] = @TYGIATINHTHUE,
	[NAMQUYETTOAN] = @NAMQUYETTOAN
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_InsertUpdate]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT_NPL nvarchar(10),
	@MASP varchar(500),
	@TENSP nvarchar(500),
	@DVT_SP nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@LUONGTONDAU numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@DONGIANHAPTT numeric(18, 6),
	@TRIGIANHAPTT numeric(18, 6),
	@TYGIATINHTHUE int,
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET] 
		SET
			[TOKHAIXUAT] = @TOKHAIXUAT,
			[TOKHAINHAP] = @TOKHAINHAP,
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[DVT_NPL] = @DVT_NPL,
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[DVT_SP] = @DVT_SP,
			[LUONGXUAT] = @LUONGXUAT,
			[TRIGIAXUAT] = @TRIGIAXUAT,
			[LUONGNHAP] = @LUONGNHAP,
			[LUONGTONDAU] = @LUONGTONDAU,
			[DONGIANHAP] = @DONGIANHAP,
			[TRIGIANHAP] = @TRIGIANHAP,
			[DONGIANHAPTT] = @DONGIANHAPTT,
			[TRIGIANHAPTT] = @TRIGIANHAPTT,
			[TYGIATINHTHUE] = @TYGIATINHTHUE,
			[NAMQUYETTOAN] = @NAMQUYETTOAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
		(
			[TOKHAIXUAT],
			[TOKHAINHAP],
			[MANPL],
			[TENNPL],
			[DVT_NPL],
			[MASP],
			[TENSP],
			[DVT_SP],
			[LUONGXUAT],
			[TRIGIAXUAT],
			[LUONGNHAP],
			[LUONGTONDAU],
			[DONGIANHAP],
			[TRIGIANHAP],
			[DONGIANHAPTT],
			[TRIGIANHAPTT],
			[TYGIATINHTHUE],
			[NAMQUYETTOAN]
		)
		VALUES 
		(
			@TOKHAIXUAT,
			@TOKHAINHAP,
			@MANPL,
			@TENNPL,
			@DVT_NPL,
			@MASP,
			@TENSP,
			@DVT_SP,
			@LUONGXUAT,
			@TRIGIAXUAT,
			@LUONGNHAP,
			@LUONGTONDAU,
			@DONGIANHAP,
			@TRIGIANHAP,
			@DONGIANHAPTT,
			@TRIGIANHAPTT,
			@TYGIATINHTHUE,
			@NAMQUYETTOAN
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Delete]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Load]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT_NPL],
	[MASP],
	[TENSP],
	[DVT_SP],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[LUONGTONDAU],
	[DONGIANHAP],
	[TRIGIANHAP],
	[DONGIANHAPTT],
	[TRIGIANHAPTT],
	[TYGIATINHTHUE],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT_NPL],
	[MASP],
	[TENSP],
	[DVT_SP],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[LUONGTONDAU],
	[DONGIANHAP],
	[TRIGIANHAP],
	[DONGIANHAPTT],
	[TRIGIANHAPTT],
	[TYGIATINHTHUE],
	[NAMQUYETTOAN]
FROM [dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_VINAPOLY
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]



















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT_NPL],
	[MASP],
	[TENSP],
	[DVT_SP],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[LUONGTONDAU],
	[DONGIANHAP],
	[TRIGIANHAP],
	[DONGIANHAPTT],
	[TRIGIANHAPTT],
	[TYGIATINHTHUE],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_SXXK_NPL_QUYETTOAN_CHITIET]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '39.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('39.9',GETDATE(), N'CẬP NHẬT TABLE BÁO CÁO QUYẾT TOÁN MỚI')
END