﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{//AiNPV
    public partial class DeNghiChuyenCuaKhau
    {
        public static string ConfigDeNghiChuyenCuaKhau( ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/KD/DeNghiChuyenCuaKhau.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    
                    IList<DeNghiChuyenCuaKhau> l_CCK = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    XElement CustomsOfficeChangedRequest = Root.Element(NodeDeNghiChuyenCuaKhau.CustomsOfficeChangedRequest);
                    if (l_CCK != null && l_CCK.Count > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        foreach (XElement element in CustomsOfficeChangedRequest.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.AdditionalDocument.ToUpper()))
                            {
                                foreach (XElement AdditionalDocument_Child in element.Elements())
                                {
                                    if (AdditionalDocument_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.reference.ToUpper()))
                                        AdditionalDocument_Child.SetValue(l_CCK[0].SoVanDon);
                                    if (AdditionalDocument_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.issue.ToUpper()))
                                        AdditionalDocument_Child.SetValue(l_CCK[0].NgayVanDon);
                                }
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.AdditionalInformation.ToUpper()))
                            {
                                foreach (XElement AdditionalInformation_Child in element.Elements())
                                {
                                    if (AdditionalInformation_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.content.ToUpper()))
                                        AdditionalInformation_Child.SetValue(l_CCK[0].ThongTinKhac);
                                    if (AdditionalInformation_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.examinationPlace.ToUpper()))
                                        AdditionalInformation_Child.SetValue(l_CCK[0].DiaDiemKiemTra);
                                    if (AdditionalInformation_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.time.ToUpper()))
                                        AdditionalInformation_Child.SetValue(l_CCK[0].ThoiGianDen);
                                    if (AdditionalInformation_Child.Name.ToString().ToUpper().Equals(NodeDeNghiChuyenCuaKhau.route.ToUpper()))
                                        AdditionalInformation_Child.SetValue(l_CCK[0].TuyenDuong);
                                }
                            }
                        }
                    }
                    else
                    {
                        return doc; //return "";
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
           // docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
    }
}
