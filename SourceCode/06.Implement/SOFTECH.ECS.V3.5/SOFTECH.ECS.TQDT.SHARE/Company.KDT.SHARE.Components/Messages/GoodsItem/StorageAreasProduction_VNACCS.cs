﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.CSSX;
using Company.KDT.SHARE.Components.Messages.Vouchers;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
       [XmlRoot("Declaration")]
  public  class StorageAreasProduction_VNACCS
    {
        /// <summary>
        /// Loại chứng từ = 331
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày khai báo
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Chức năng của chứng từ = 8
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Số đăng ký
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }


        /// <summary>
        /// Tình trạng của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// <summary>
        /// Ngày đăng ký chứng từ
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }


        /// <summary>
        /// Hải quan tiếp nhận
        /// </summary>
        [XmlElement("updateType")]
        public string UpdateType { get; set; }

        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }

        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("ImporterDetail")]
        public ImporterDetail ImporterDetail { get; set; }


        /// <summary>
        /// Thông tin Cơ sở sản xuất (CSSX)
        /// </summary>
        [XmlElement("ManufactureFactories")]
        public ManufactureFactories ManufactureFactories { get; set; }

        /// <summary>
        /// Ngành nghề
        /// </summary>
        [XmlElement("Careers")]
        public Careers Careers { get; set; }

        /// <summary>
        /// Tính hình nhân lực
        /// </summary>
        [XmlElement("Staff")]
        public Staff Staff { get; set; }

        /// <summary>
        /// Số lượng máy móc, dây truyền trang thiết bị
        /// </summary>
        [XmlElement("Machine")]
        public Machine Machine { get; set; }

        /// <summary>
        /// Tuân thủ pháp luật
        /// </summary>
        [XmlElement("ComplianceWithLaws")]
        public ComplianceWithLaws ComplianceWithLaws { get; set; }


        /// <summary>
        /// Thông tin Cơ sở sản xuất (CSSX)
        /// </summary>
        [XmlElement("OutsourcingManufactureFactories")]
        public OutsourcingManufactureFactories OutsourcingManufactureFactories { get; set; }


        /// <summary>
        /// Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để gia công, sản xuất xuất khẩu cho các đơn vị thành viên trực thuộc
        /// </summary>
        [XmlElement("HoldingCompanies")]
        public HoldingCompanies HoldingCompanies { get; set; }

        /// <summary>
        /// Công ty thành viên trực thuộc Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị thành viên khác trực thuộc Công ty mẹ 
        /// </summary>
        [XmlElement("AffiliatedMemberCompanies")]
        public AffiliatedMemberCompanies AffiliatedMemberCompanies { get; set; }


        /// <summary>
        /// Công ty thành viên nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị trực thuộc Công ty thành viên có CSSX
        /// </summary>
        [XmlElement("MemberCompanies")]
        public MemberCompanies MemberCompanies { get; set; }

        /// <summary>
        /// File đính kèm
        /// </summary>
        [XmlElement("AttachedFiles")]
        public AttachedFiles AttachedFiles { get; set; }

        /// <summary>
        /// AdditionalInformation
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public Company.KDT.SHARE.Components.AdditionalDocument AdditionalInformation { get; set; }
    }
}
