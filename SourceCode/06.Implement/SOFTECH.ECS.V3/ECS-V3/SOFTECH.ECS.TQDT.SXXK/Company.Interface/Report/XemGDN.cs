﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report
{
    public partial class XemGDN: BaseForm
    {
        public ToKhaiMauDich TKMD = null;
        rpGDN objRP = null;
        public XemGDN(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            this.TKMD = tkmd;
            if (this.TKMD != null)
            {
                objRP = new rpGDN(this.TKMD);
                objRP.PrintingSystem = this.printingSystem1;
                objRP.CreateDocument();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (objRP != null)
            {
                objRP.dispBackColor(false);
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                objRP.dispBackColor(checkBoxBorder.Checked);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (objRP != null)
            {
                objRP.dispBackColor(false);
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                objRP.dispBackColor(checkBoxBorder.Checked);
            }
        }

        private void checkBoxBorder_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            objRP.dispBackColor(checkBoxBorder.Checked);
        }

        private void XemCV_Load(object sender, EventArgs e)
        {
            checkBoxBorder.Checked = true;
        }
    }
}