﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC11Form : BaseForm
    {
        public DataSet ds = new DataSet();
        private BangKe11_HQGC BC11 = new BangKe11_HQGC();
        public HopDong HD = new HopDong();
        public ReportViewBC11Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if(this.ds.Tables.Count > 0)
                cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC11.First = (cbPage.SelectedIndex == 0);
            this.BC11.Last = (cbPage.SelectedIndex == (ds.Tables.Count - 1));
            this.BC11.HD = this.HD;
            this.BC11.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BC11.PrintingSystem;
            this.BC11.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

   
    }
}