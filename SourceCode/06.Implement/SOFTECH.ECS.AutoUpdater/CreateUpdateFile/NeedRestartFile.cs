﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace CreateUpdateFile
{
    public  class FileName
    {
        private string filename;
        [XmlAttribute("name")]
        public string Name { get { return filename; } set { filename = value; } }
        public FileName(string filename)
        {
            this.filename = filename;
        }
        public FileName()
        {

        }
    }
    public class NeedRestartFile : List<FileName>
    {
       public override string ToString()
       {
           string ret = string.Empty;
           for (int i = 0; i < this.Count; i++)
           {
               ret += "\r\n" + this[i].Name;
           }
           
           return ret!=string.Empty ? ret.Remove(0,2):string.Empty;
       }
       public void Create(string text)
       {
           this.Clear();
           string[] files = text.Split(new string[]{"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
           foreach (string item in files)
           {
               if(!string.IsNullOrEmpty(item))
               this.Add(new FileName(item.Trim()));
           }
       }
    }
}
