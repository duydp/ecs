﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IAS : BasicVNACC
    {
        public PropertiesAttribute TYS { get; set; } //Loại hình bảo lãnh
        public PropertiesAttribute SBC { get; set; } //Mã ngân hàng cung cấp bảo lãnh
        public PropertiesAttribute RYA { get; set; } //Năm phát hành
        public PropertiesAttribute SCM { get; set; } //Kí hiệu chứng từ phát hành bảo lãnh
        public PropertiesAttribute SCN { get; set; } //Số chứng từ  phát hành bảo lãnh
        public PropertiesAttribute CCC { get; set; } //Mã tiền tệ

        public IAS()
        {
            TYS = new PropertiesAttribute(1, typeof(string)); //Loại hình bảo lãnh
            SBC = new PropertiesAttribute(11, typeof(string)); //Mã ngân hàng cung cấp bảo lãnh
            RYA = new PropertiesAttribute(4, typeof(int)); //Năm phát hành
            SCM = new PropertiesAttribute(10, typeof(string)); //Kí hiệu chứng từ phát hành bảo lãnh
            SCN = new PropertiesAttribute(10, typeof(string)); //Số chứng từ  phát hành bảo lãnh
            CCC = new PropertiesAttribute(3, typeof(string)); //Mã tiền tệ
        }
    }

    public partial class EnumGroupID { }

}
