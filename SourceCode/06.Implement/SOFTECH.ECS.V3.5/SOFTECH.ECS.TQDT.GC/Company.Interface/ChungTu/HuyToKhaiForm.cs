﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class HuyToKhaiForm : BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public Company.KDT.SHARE.QuanLyChungTu.HuyToKhai huyTK = null;
        public HuyToKhaiForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtLyDoHuy.Text.Trim() == "")
            {
                ShowMessage("Phải nhập lý do hủy!", false);
                return;
            }
            try
            {
                huyTK.LyDoHuy = txtLyDoHuy.Text.Trim();
                huyTK.TKMD_ID = TKMD.ID;
                huyTK.Guid = TKMD.GUIDSTR;
                if (huyTK.ID == 0)
                    huyTK.Insert();
                else
                    huyTK.Update();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Xảy ra lỗi: " + ex.ToString(), false);
            }
        }


        private void HuyToKhaiForm_Load(object sender, EventArgs e)
        {
            getThongTinToKhai();
        }

        private void getThongTinToKhai()
        {
            List<HuyToKhai> listHuyTK = Company.KDT.SHARE.QuanLyChungTu.HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
            if (listHuyTK.Count > 0)
                huyTK = listHuyTK[0];
            if (huyTK != null)
            {
                txtLyDoHuy.Text = huyTK.LyDoHuy;
                if (huyTK.SoTiepNhan > 0)
                {
                    txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Text = huyTK.NgayTiepNhan.ToShortDateString();
                }
                if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                {
                    btnKhaiBao.Enabled = false;
                    btnGhi.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                {
                    lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            else
            {
                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                huyTK = new HuyToKhai();
                btnKhaiBao.Enabled = false;
                btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            lblSoTK.Text = TKMD.SoToKhai.ToString();
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            HuyToKhai(this.TKMD);
        }
        private void HuyToKhai(ToKhaiMauDich tkmd)
        {
            WSForm wsForm = new WSForm();
            string password = string.Empty;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;

            if (sendXML.Load())
            {
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN01", "", false);
                showMsg("MSG_SEN01");
                btnLayPhanHoi.Enabled = true;
                btnKhaiBao.Enabled = false;
                return;
            }
            try
            {

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.Default;
                //KHAI BÁO HỦY TỜ KHAI
                string thanhcong;
                thanhcong = tkmd.WSHuyToKhai(password);

                if (thanhcong != string.Empty)
                {
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = this.TKMD.ID;
                    sendXML.msg = thanhcong;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();

                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = false;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa có thông tin phản hồi từ hệ thống thông quan điện tử.", false);

            }

        }
        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {

            string password = "";
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                string xmlCurr = TKMD.LayPhanHoiTQDTKhaiBao(password, sendXML.msg);
                if (string.IsNullOrEmpty(xmlCurr))
                    getThongTinToKhai();
                else
                    ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa có thông tin phản hồi từ hệ thống thông quan điện tử.", false);

            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password, string msgXML)
        {
        StartInvoke:
            try
            {
                bool thanhcong = huyTK.WSLaySoTiepNhan(password, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;

                }
                else
                {
                    try
                    {
                        // Thêm vào bảng kết quả xử lý thông tin HỦY TỜ KHAI
                        Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                        kqxl.ItemID = this.TKMD.ID;
                        kqxl.ReferenceID = new Guid(huyTK.Guid);
                        kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoHuyTK_ThanhCong;
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToShortDateString(), this.TKMD.MaLoaiHinh.Trim(), this.TKMD.MaHaiQuan.Trim());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                    }
                    catch (Exception ex)
                    {

                    }
                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                    TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                    TKMD.Update();
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
                }
                lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;

            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = huyTK.WSLayPhanHoi(password, TKMD.SoToKhai, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else// CÓ PHẢN HỒI
                {
                    string loaiThongDiep = "";
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                        lblTrangThai.Text = TrangThaiXuLy.strTUCHOI;
                        loaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;

                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        TKMD.Update();
                    }
                    else// TỜ KHAI HỦY ĐƯỢC CHẤP NHẬN
                    {
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiDaHuy;
                        TKMD.Update();

                        lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
                        loaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoHuyTK_DuocDuyet;
                    }
                    //// Thêm vào bảng kết quả xử lý thông tin HỦY TỜ KHAI
                    //Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    //kqxl.ItemID = this.TKMD.ID;
                    //kqxl.ReferenceID = new Guid(huyTK.Guid);
                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    //kqxl.LoaiThongDiep = loaiThongDiep;
                    //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToShortDateString(), this.TKMD.MaLoaiHinh.Trim(), this.TKMD.MaHaiQuan.Trim());
                    //kqxl.Ngay = DateTime.Now;
                    //kqxl.Insert();
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
                System.IO.StreamWriter write = System.IO.File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (huyTK.Guid != null && huyTK.Guid != "")
                Globals.ShowKetQuaXuLyBoSung(huyTK.Guid);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }


    }
}

