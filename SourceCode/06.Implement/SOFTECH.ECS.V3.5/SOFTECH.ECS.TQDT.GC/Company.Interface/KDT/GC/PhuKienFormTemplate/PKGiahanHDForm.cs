﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class PKGiahanHDForm : BaseForm
    {        
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
        public HopDong HD = new HopDong();
        public bool boolFlag;
        public String Caption;
        public bool IsChange;

        public PKGiahanHDForm()
        {            
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {            
            this.Close();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void PKGiahanHDForm_Load(object sender, EventArgs e)
        {
            txtSoHD.Text = HD.SoHopDong;
            txtNgayKy.Text = HD.NgayKy.ToString("dd/MM/yyyy");
            Caption = this.Text;
            if (HD.NgayGiaHan.Year > 1900)
            {
                ccNgayGiaHanCu.ValueChanged -= new EventHandler(txt_TextChanged);
                ccNgayGiaHanCu.Value = HD.NgayGiaHan;
                ccNgayGiaHanCu.ValueChanged += new EventHandler(txt_TextChanged);
            }
            else
            {
                ccNgayGiaHanCu.ValueChanged -= new EventHandler(txt_TextChanged);
                ccNgayGiaHanCu.Value = HD.NgayHetHan;
                ccNgayGiaHanCu.ValueChanged += new EventHandler(txt_TextChanged);
            }
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK in pkdk.PKCollection)
            {
                if (LoaiPK.MaPhuKien.Trim() == "201")
                {
                    ccNgayGiaHanMoi.ValueChanged -= new EventHandler(txt_TextChanged);
                    ccNgayGiaHanMoi.Value = Convert.ToDateTime(LoaiPK.ThongTinMoi);
                    ccNgayGiaHanMoi.ValueChanged += new EventHandler(txt_TextChanged);

                    if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        LoaiPK.LoadCollection();
                    }  
                    break;
                }
            }
            if (this.OpenType == OpenFormType.View)
            {
                btnAdd.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = null;
                if (ccNgayGiaHanMoi.Value <= DateTime.Today)
                {
                    showMsg("MSG_240225");
                    return;
                }
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK1 in pkdk.PKCollection)
                {
                    if (LoaiPK1.MaPhuKien.Trim() == "201")
                    {
                        LoaiPK = LoaiPK1;
                        break;
                    }
                }
                if (LoaiPK == null)
                {
                    LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "201";
                    LoaiPK.NoiDung = LoaiPhuKien_GetName("201");
                    pkdk.PKCollection.Add(LoaiPK);
                }
                LoaiPK.ThongTinMoi = ccNgayGiaHanMoi.Value.ToString("dd/MM/yyyy");
                LoaiPK.ThongTinCu = ccNgayGiaHanCu.Text;
                LoaiPK.Master_ID = pkdk.ID;
                this.SetChange(false);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void PKGiahanHDForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Phụ kiện có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.btnAdd_Click(null,null);
                }
            }
        }

       
    }
}
