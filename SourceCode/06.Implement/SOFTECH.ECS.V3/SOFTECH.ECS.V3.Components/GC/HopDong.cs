﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class HopDong
    {
        /// <summary>
        /// ConfigItem( sản phẩm gia công )
        /// </summary>
        /// <param name="Item">XElement</param>
        /// <param name="NSP">NhomSanPham</param>
        /// <returns>XElement</returns>
        public static XElement ConfigItem(XElement Item, NhomSanPham NSP)
        {
            if (Item != null && Item.Elements().Count() > 0 && NSP!=null)
            {
                foreach (XElement element in Item.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                        element.SetValue(NSP.MaSanPham);
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                        element.SetValue(NSP.TenSanPham);
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.quantity.ToUpper()))
                        element.SetValue(NSP.SoLuong);
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Products.ToUpper()))
                        element.SetValue("");
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.paymentValue.ToUpper()))
                        element.SetValue(NSP.GiaGiaCong);
                }   
            }
            return Item;
        }
        /// <summary>
        /// ConfigContractItems( Nhóm sản phẩm gia công )
        /// </summary>
        /// <param name="ContractItems">XElement</param>
        /// <param name="HD">HopDong</param>
        /// <returns>XElement</returns>
        public static XElement ConfigContractItems(XElement ContractItems, HopDong HD)
        {
            if (ContractItems != null && ContractItems.Elements().Count() > 0)
            {
                IList<NhomSanPham> lNSP = NhomSanPham.SelectCollectionBy_HopDong_ID(HD.ID);
                XElement Item = ContractItems.Element(NodeHopDong.Item);
                foreach (XElement element in ContractItems.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Item.ToUpper()))
                        Item = element;
                }
                if (lNSP != null)
                {
                    if (lNSP.Count == 1)
                    {
                        foreach (XElement element in Item.Elements())
                        {
                            ConfigItem(Item, lNSP[0]);
                        }
                    }
                    else if (lNSP.Count > 1)
                    {
                        XElement TempItem = new XElement(Item);
                        ConfigItem(Item, lNSP[0]);
                        // Add new Item
                        for (int i = 1; i < lNSP.Count; i++)
                        {
                            XElement NewItem = new XElement(TempItem);
                            ContractItems.Add(ConfigItem(NewItem, lNSP[i]));
                        }
                    }
                }
            }            
            return ContractItems;
        }
        /// <summary>
        /// ConfigProduct( Danh mục sản phẩm Gia công)
        /// </summary>
        /// <param name="Product">XElement</param>
        /// <param name="SP">SanPham</param>
        /// <returns>XElement</returns>
        public static XElement ConfigProduct(XElement Product, SanPham SP)
        {
            if (Product != null && Product.Elements().Count() > 0 && SP != null)
            {
                DonViTinh DVT = DonViTinh.Load(SP.DVT_ID);
                foreach (XElement element in Product.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.description.ToUpper()))
                                elementChild.SetValue(SP.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identification.ToUpper()))
                                elementChild.SetValue(SP.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.tariffClassification.ToUpper()))
                                elementChild.SetValue(SP.MaHS);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.productGroup.ToUpper()))
                                elementChild.SetValue(SP.NhomSanPham_ID);                    
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }                   
                }
            }
            return Product;
        }
        /// <summary>
        /// ConfigProducts ( Danh mục nhóm sản phẩm Gia công)
        /// </summary>
        /// <param name="Products">XElement</param>
        /// <param name="HD">HopDong</param>
        /// <returns>XElement</returns>
        public static XElement ConfigProducts(XElement Products, HopDong HD)
        {
            if (Products != null && Products.Elements().Count() > 0)
            {
                IList<SanPham> lSP = SanPham.SelectCollectionBy_HopDong_ID(HD.ID);
                XElement Product = Products.Element(NodeHopDong.Product);
                foreach (XElement element in Products.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Product.ToUpper()))
                        Product = element;
                }
                if (lSP != null)
                {
                    if (lSP.Count == 1)
                    {
                        foreach (XElement element in Product.Elements())
                        {
                            ConfigProduct(Product, lSP[0]);
                        }
                    }
                    else if (lSP.Count > 1)
                    {
                        XElement TempProduct = new XElement(Product);
                        ConfigProduct(Product, lSP[0]);
                        // Add new Product
                        for (int i = 1; i < lSP.Count; i++)
                        {
                            XElement NewProduct = new XElement(TempProduct);
                            Products.Add(ConfigProduct(NewProduct, lSP[i]));
                        }
                    }
                }
            }
            return Products;
        }
        /// <summary>
        /// ConfigMaterial( Danh mục nguyên phụ liệu )
        /// </summary>
        /// <param name="Material">XElement</param>
        /// <param name="NPL">NguyenPhuLieu</param>
        /// <returns>XElement</returns>
        public static XElement ConfigMaterial(XElement Material, NguyenPhuLieu NPL)
        {
            if (Material != null && Material.Elements().Count() > 0 && NPL != null)
            {
                DonViTinh DVT = DonViTinh.Load(NPL.DVT_ID);
                foreach (XElement element in Material.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.description.ToUpper()))
                                elementChild.SetValue(NPL.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identification.ToUpper()))
                                elementChild.SetValue(NPL.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.tariffClassification.ToUpper()))
                                elementChild.SetValue(NPL.MaHS);                            
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }                   
                }
            }
            return Material;
        }
        /// <summary>
        /// ConfigMaterials(Nhóm danh mục nguyên phụ liệu )
        /// </summary>
        /// <param name="Materials">XElement</param>
        /// <param name="HD">HopDong</param>
        /// <returns>XElement</returns>
        public static XElement ConfigMaterials(XElement Materials, HopDong HD)
        {
            if (Materials != null && Materials.Elements().Count() > 0)
            {
                IList<NguyenPhuLieu> lNPL = NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                XElement Material = Materials.Element(NodeHopDong.Material);
                // Check Upper
                foreach (XElement element in Materials.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Material.ToUpper()))
                        Material = element;
                }
                if (lNPL != null)
                {
                    if (lNPL.Count == 1)
                    {
                        foreach (XElement element in Material.Elements())
                        {
                            ConfigMaterial(Material, lNPL[0]);
                        }
                    }
                    else if (lNPL.Count > 1)
                    {
                        XElement TempMaterial = new XElement(Material);
                        ConfigMaterial(Material, lNPL[0]);
                        // Add new Material
                        for (int i = 1; i < lNPL.Count; i++)
                        {
                            XElement NewMaterial = new XElement(TempMaterial);
                            Materials.Add(ConfigMaterial(NewMaterial, lNPL[i]));
                        }
                    }
                }
            }
            return Materials;
        }
        /// <summary>
        /// ConfigEquipment( Danh mục thiết bị tạm nhập Gia công)
        /// </summary>
        /// <param name="Equipment">XElement</param>
        /// <param name="TB">ThietBi</param>
        /// <returns>XElement</returns>
        public static XElement ConfigEquipment(XElement Equipment, ThietBi TB)
        {
            if (Equipment != null && Equipment.Elements().Count() > 0 && TB != null)
            {
                DonViTinh DVT = DonViTinh.Load(TB.DVT_ID);
                Nuoc nuoc = Nuoc.Load(TB.NuocXX_ID);
                NguyenTe NT = NguyenTe.Load(TB.NguyenTe_ID);
                foreach (XElement element in Equipment.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.description.ToUpper()))
                                elementChild.SetValue(TB.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identification.ToUpper()))
                                elementChild.SetValue(TB.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.tariffClassification.ToUpper()))
                                elementChild.SetValue(TB.MaHS);                            
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.quantity.ToUpper()))
                                    elementChild.SetValue(TB.SoLuongDangKy);
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }
                    if (nuoc != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Origin.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.originCountry.ToUpper()))
                                    elementChild.SetValue(nuoc.Ten);
                            }
                        }
                    }
                    if (NT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.CurrencyExchange.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.CurrencyType.ToUpper()))
                                    elementChild.SetValue(NT.Ten);
                            }
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.customsValue.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.unitPrice.ToUpper()))
                                elementChild.SetValue(TB.TriGia);
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.status.ToUpper()))
                    {
                        element.SetValue("");
                    }
                }
            }
            return Equipment;
        }
        /// <summary>
        /// ConfigEquipments( Nhóm danh mục thiết bị tạm nhập Gia công)
        /// </summary>
        /// <param name="Equipments">XElement</param>
        /// <param name="HD">HopDong</param>
        /// <returns>XElement</returns>
        public static XElement ConfigEquipments(XElement Equipments, HopDong HD)
        {
            if (Equipments != null && Equipments.Elements().Count() > 0)
            {
                IList<ThietBi> lTB = ThietBi.SelectCollectionBy_HopDong_ID(HD.ID);
                XElement Equipment = Equipments.Element(NodeHopDong.Equipment);
                // Check Upper
                foreach (XElement element in Equipments.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Equipment.ToUpper()))
                        Equipment = element;
                }
                if (lTB != null)
                {
                    if (lTB.Count == 1)
                    {
                        foreach (XElement element in Equipment.Elements())
                        {
                            ConfigEquipment(Equipment, lTB[0]);
                        }
                    }
                    else if (lTB.Count > 1)
                    {
                        XElement TempEquipment = new XElement(Equipment);
                        ConfigEquipment(Equipment, lTB[0]);
                        // Add new Equipment
                        for (int i = 1; i < lTB.Count; i++)
                        {
                            XElement NewEquipment = new XElement(TempEquipment);
                            Equipments.Add(ConfigEquipment(NewEquipment, lTB[i]));
                        }
                    }
                }
            }
            return Equipments;
        }
        /// <summary>
        /// ConfigContractDocument
        /// </summary>
        /// <param name="ContractDocument">XElement</param>
        /// <param name="HD">HopDong</param>
        /// <returns>XElement</returns>
        public static XElement ConfigContractDocument(XElement ContractDocument, HopDong HD)
        {
            if (ContractDocument != null && ContractDocument.Elements().Count() > 0 && HD != null)
            {
                IList<ToKhaiMauDich> lTKMD = ToKhaiMauDich.SelectCollectionBy_IDHopDong(HD.ID);                
                XElement ContractItems = new XElement(NodeHopDong.ContractItems);
                XElement Products = new XElement(NodeHopDong.Products);
                XElement Materials = new XElement(NodeHopDong.Materials);
                XElement Equipments = new XElement(NodeHopDong.Equipments);
                XElement SampleProducts = new XElement(NodeHopDong.SampleProducts);
                NguyenTe NT = NguyenTe.Load(HD.NguyenTe_ID);
                Nuoc nuoc;
                foreach(XElement element in ContractDocument.Elements())
                {
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.reference.ToUpper()))
                        element.SetValue(HD.SoHopDong);
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.issue.ToUpper()))
                        element.SetValue(HD.NgayTiepNhan);
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.expire.ToUpper()))
                        element.SetValue(HD.NgayHetHan);
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.Payment.ToUpper()))
                    {
                        foreach(XElement elementChild in element.Elements())
                        {
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.method.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                        }
                    }
                    if (NT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.CurrencyExchange.ToUpper()))
                            element.SetValue(NT.Ten);          
                    }                    
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.Importer.ToUpper()))
                    {
                        foreach(XElement elementChild in element.Elements())
                        {
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.address.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                        }
                    }
                    if(element.Name.ToString().ToUpper().Equals(NodeHopDong.Exporter.ToUpper()))
                    {
                        foreach(XElement elementChild in element.Elements())
                        {
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                            if(elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.address.ToUpper()))
                            {
                                elementChild.SetValue("");
                            }
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.ContractItems.ToUpper()))
                        ContractItems = element;
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Products.ToUpper()))
                        Products = element;
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Materials.ToUpper()))
                        Materials = element;
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Equipments.ToUpper()))
                        Equipments = element;
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.SampleProducts.ToUpper()))
                        SampleProducts = element;
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.customsValue.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        { 
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.totalPaymentValue.ToUpper()))
                                elementChild.SetValue("");
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.totalProductValue.ToUpper()))
                                elementChild.SetValue("");
                        }
                    }
                    
                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.importationCountry.ToUpper()))
                        element.SetValue("");
                    nuoc = Nuoc.Load(HD.NuocThue_ID);
                    if (nuoc != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeHopDong.exportationCountry.ToUpper()))
                            element.SetValue(nuoc.Ten);
                    }

                    if (element.Name.ToString().ToUpper().Equals(NodeHopDong.AdditionalInformation.ToUpper()))
                    {
                        foreach(XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.content.ToUpper()))
                                elementChild.SetValue("");
                        }
                    }
                }
                ContractItems = ConfigContractItems(ContractItems, HD);
                Products = ConfigProducts(Products, HD);
                Materials = ConfigMaterials(Materials, HD);
                Equipments = ConfigEquipments(Equipments, HD);
            }
            return ContractDocument;
        }
        /// <summary>
        /// ConfigHopDong
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigHopDong(HopDong HD, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/HopDong.xml");
                if (HD != null && docXML != null)
                {
                    CuaKhau CK = CuaKhau.Load(HD.MaHaiQuan);
                    DonViHaiQuan DVHQ = DonViHaiQuan.Load(HD.MaHaiQuan);
                    XElement Root = docXML.Root;
                    if (Root != null && Root.Elements().Count() > 0)
                    {        
                        XElement ContractDocument = Root.Element(NodeHopDong.ContractDocument);
                        // Check Upper
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issuer.ToUpper()))
                                element.SetValue(issuer.ToString());
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.reference.ToUpper()))
                                element.SetValue(HD.ID);
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issue.ToUpper()))
                                element.SetValue("");    
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.function.ToUpper()))
                                element.SetValue(function);    
                            if(CK!=null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issueLocation.ToUpper()))
                                element.SetValue(CK.Ten);    
                            }                                                        
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.status.ToUpper()))
                                element.SetValue(status);
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.customsReference.ToUpper()))
                                element.SetValue(HD.GUIDSTR);        
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.acceptance.ToUpper()))
                                element.SetValue(HD.NgayDangKy);
                            if(DVHQ!=null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDong.declarationOffice.ToUpper()))
                                element.SetValue(DVHQ.Ten);
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Agent.ToUpper()))
                            {
                                foreach(XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                                        elementChild.SetValue("");
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                                        elementChild.SetValue(HD.MaDaiLy);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.status.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }                                

                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Importer.ToUpper()))
                            {
                                foreach(XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                                        elementChild.SetValue(HD.DonViDoiTac);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                                        elementChild.SetValue("");                                    
                                }
                            }                                

                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.ContractDocument.ToUpper()))
                                ContractDocument = element;
                        }
                        if (HD != null)
                        {
                            ConfigContractDocument(ContractDocument, HD);
                        }
                        docXML.Save(@"D:\NewHopDong.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
