GO
IF OBJECT_ID('t_KDT_SXXK_NguyenPhuLieuDangKy_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_NguyenPhuLieuDangKy_Log
GO
CREATE TABLE [dbo].[t_KDT_SXXK_NguyenPhuLieuDangKy_Log]
(
[ID] [bigint]  NULL,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [int] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamDK] [smallint] NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_SXXK_NguyenPhuLieuDangKy_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_NguyenPhuLieuDangKy_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_NguyenPhuLieuDangKy_Log 
ON dbo.t_KDT_SXXK_NguyenPhuLieuDangKy
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieuDangKy_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieuDangKy_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieuDangKy_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO
IF OBJECT_ID('t_KDT_SXXK_NguyenPhuLieu_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_NguyenPhuLieu_Log
GO

CREATE TABLE [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
(
[ID] [bigint]  NULL,
[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[Master_ID] [bigint] NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_SXXK_NguyenPhuLieu_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_NguyenPhuLieu_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_NguyenPhuLieu_Log 
ON dbo.t_KDT_SXXK_NguyenPhuLieu
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO

IF OBJECT_ID('t_SXXK_NguyenPhuLieu_Log') IS NOT NULL
DROP TABLE dbo.t_SXXK_NguyenPhuLieu_Log
GO
CREATE TABLE [dbo].[t_SXXK_NguyenPhuLieu_Log]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_SXXK_NguyenPhuLieu_Log') IS NOT NULL
DROP TRIGGER trg_t_SXXK_NguyenPhuLieu_Log

GO

CREATE TRIGGER trg_t_SXXK_NguyenPhuLieu_Log 
ON dbo.t_SXXK_NguyenPhuLieu
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_NguyenPhuLieu_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_SXXK_SanPhamDangKy_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_SanPhamDangKy_Log
GO
CREATE TABLE [dbo].[t_KDT_SXXK_SanPhamDangKy_Log]
(
[ID] [bigint]  NULL ,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [int] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamDK] [smallint] NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_SXXK_SanPhamDangKy_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_SanPhamDangKy_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_SanPhamDangKy_Log 
ON dbo.t_KDT_SXXK_SanPhamDangKy
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPhamDangKy_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPhamDangKy_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPhamDangKy_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO

IF OBJECT_ID('t_KDT_SXXK_SanPham_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_SanPham_Log
GO
CREATE TABLE [dbo].[t_KDT_SXXK_SanPham_Log]
(
[ID] [bigint]  NULL ,
[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[Master_ID] [bigint] NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_SXXK_SanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_SanPham_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_SanPham_Log 
ON dbo.t_KDT_SXXK_SanPham
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO

IF OBJECT_ID('t_SXXK_SanPham_Log') IS NOT NULL
DROP TABLE dbo.t_SXXK_SanPham_Log
GO
CREATE TABLE [dbo].[t_SXXK_SanPham_Log]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]

GO

IF OBJECT_ID('trg_t_SXXK_SanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_SXXK_SanPham_Log

GO

CREATE TRIGGER trg_t_SXXK_SanPham_Log 
ON dbo.t_SXXK_SanPham
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_SanPham_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO

IF OBJECT_ID('t_KDT_SXXK_DinhMucDangKy_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_DinhMucDangKy_Log
GO
CREATE TABLE [dbo].[t_KDT_SXXK_DinhMucDangKy_Log]
(
[ID] [bigint]  NULL ,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiXuLy] [int] NOT NULL,
[SoTiepNhanChungTu] [bigint] NULL,
[SoDinhMuc] [int] NULL,
[NgayDangKy] [datetime] NULL,
[NgayApDung] [datetime] NULL,
[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [int] NULL,
[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamDK] [smallint] NULL,
[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiDinhMuc] [int] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_SXXK_DinhMucDangKy_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_DinhMucDangKy_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_DinhMucDangKy_Log 
ON dbo.t_KDT_SXXK_DinhMucDangKy
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMucDangKy_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO
GO

IF OBJECT_ID('t_KDT_SXXK_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_SXXK_DinhMuc_Log
GO
CREATE TABLE [dbo].[t_KDT_SXXK_DinhMuc_Log]
(
[ID] [bigint]  NULL,
[MaSanPham] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 10) NOT NULL,
[TyLeHaoHut] [numeric] (18, 5) NOT NULL,
[GhiChu] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NOT NULL,
[Master_ID] [bigint] NOT NULL,
[IsFromVietNam] [bit] NULL DEFAULT ((0)),
[MaDinhDanhLenhSX] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_SXXK_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_SXXK_DinhMuc_Log

GO

CREATE TRIGGER trg_t_KDT_SXXK_DinhMuc_Log 
ON dbo.t_KDT_SXXK_DinhMuc
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END

GO

IF OBJECT_ID('t_SXXK_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_SXXK_DinhMuc_Log
GO

CREATE TABLE [dbo].[t_SXXK_DinhMuc_Log]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaSanPham] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [nvarchar] (254) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
[TyLeHaoHut] [numeric] (18, 5) NOT NULL,
[DinhMucChung] [numeric] (18, 8) NULL,
[GhiChu] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsFromVietNam] [bit] NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_SXXK_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_SXXK_DinhMuc_Log

GO

CREATE TRIGGER trg_t_SXXK_DinhMuc_Log 
ON dbo.t_SXXK_DinhMuc
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_SXXK_DinhMuc_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

--IF OBJECT_ID('t_KDT_CX_HangDuaRaDangKy_Log') IS NOT NULL
--DROP TABLE dbo.t_KDT_CX_HangDuaRaDangKy_Log
--GO
--CREATE TABLE [dbo].[t_KDT_CX_HangDuaRaDangKy_Log]
--(
--[ID] [bigint]  NULL ,
--[SoTiepNhan] [bigint] NOT NULL,
--[NgayTiepNhan] [datetime] NOT NULL,
--[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[TrangThaiXuLy] [int] NOT NULL,
--[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[ActionStatus] [int] NULL,
--[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[NamDK] [smallint] NULL,
--[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DateLog] DATETIME NULL,
--[Status] NVARCHAR(50) NULL
--) ON [PRIMARY]
--GO

--IF OBJECT_ID('trg_t_KDT_CX_HangDuaRaDangKy_Log') IS NOT NULL
--DROP TRIGGER trg_t_KDT_CX_HangDuaRaDangKy_Log

--GO

--CREATE TRIGGER trg_t_KDT_CX_HangDuaRaDangKy_Log 
--ON dbo.t_KDT_CX_HangDuaRaDangKy
--AFTER INSERT,UPDATE,DELETE AS
--BEGIN
--IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRaDangKy_Log
--		SELECT *,GETDATE(),'Inserted' FROM Inserted
--	END
--IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRaDangKy_Log
--		SELECT *,GETDATE(),'Updated' FROM Inserted
--	END
--IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRaDangKy_Log
--		SELECT *,GETDATE(),'Deleted' FROM Deleted
--	END
--END

--GO

--IF OBJECT_ID('t_KDT_CX_HangDuaRa_Log') IS NOT NULL
--DROP TABLE dbo.t_KDT_CX_HangDuaRa_Log
--GO
--CREATE TABLE [dbo].[t_KDT_CX_HangDuaRa_Log]
--(
--[ID] [bigint]  NULL,
--[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[STTHang] [int] NOT NULL,
--[Master_ID] [bigint] NOT NULL,
--[LoaiHang] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MucDichSuDung] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MoTaKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DateLog] DATETIME NULL,
--[Status] NVARCHAR(50) NULL
--) ON [PRIMARY]
--GO
--IF OBJECT_ID('trg_t_KDT_CX_HangDuaRa_Log') IS NOT NULL
--DROP TRIGGER trg_t_KDT_CX_HangDuaRa_Log

--GO

--CREATE TRIGGER trg_t_KDT_CX_HangDuaRa_Log 
--ON dbo.t_KDT_CX_HangDuaRa
--AFTER INSERT,UPDATE,DELETE AS
--BEGIN
--IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRa_Log
--		SELECT *,GETDATE(),'Inserted' FROM Inserted
--	END
--IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRa_Log
--		SELECT *,GETDATE(),'Updated' FROM Inserted
--	END
--IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaRa_Log
--		SELECT *,GETDATE(),'Deleted' FROM Deleted
--	END
--END

--GO

--IF OBJECT_ID('t_KDT_CX_HangDuaVaoDangKy_Log') IS NOT NULL
--DROP TABLE dbo.t_KDT_CX_HangDuaVaoDangKy_Log
--GO
--CREATE TABLE [dbo].[t_KDT_CX_HangDuaVaoDangKy_Log]
--(
--[ID] [bigint]  NULL ,
--[SoTiepNhan] [bigint] NOT NULL,
--[NgayTiepNhan] [datetime] NOT NULL,
--[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaDaiLy] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[TrangThaiXuLy] [int] NOT NULL,
--[GUIDSTR] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[ActionStatus] [int] NULL,
--[GuidReference] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[NamDK] [smallint] NULL,
--[HUONGDAN] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[PhanLuong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[Huongdan_PL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DateLog] DATETIME NULL,
--[Status] NVARCHAR(50) NULL
--) ON [PRIMARY]
--GO
--IF OBJECT_ID('trg_t_KDT_CX_HangDuaVaoDangKy_Log') IS NOT NULL
--DROP TRIGGER trg_t_KDT_CX_HangDuaVaoDangKy_Log

--GO

--CREATE TRIGGER trg_t_KDT_CX_HangDuaVaoDangKy_Log 
--ON dbo.t_KDT_CX_HangDuaVaoDangKy
--AFTER INSERT,UPDATE,DELETE AS
--BEGIN
--IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVaoDangKy_Log
--		SELECT *,GETDATE(),'Inserted' FROM Inserted
--	END
--IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVaoDangKy_Log
--		SELECT *,GETDATE(),'Updated' FROM Inserted
--	END
--IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVaoDangKy_Log
--		SELECT *,GETDATE(),'Deleted' FROM Deleted
--	END
--END

--GO

--IF OBJECT_ID('t_KDT_CX_HangDuaVao_Log') IS NOT NULL
--DROP TABLE dbo.t_KDT_CX_HangDuaVao_Log
--GO
--CREATE TABLE [dbo].[t_KDT_CX_HangDuaVao_Log]
--(
--[ID] [bigint]  NULL ,
--[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--[STTHang] [int] NOT NULL,
--[Master_ID] [bigint] NOT NULL,
--[MucDich] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[MoTaKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[LoaiNPL] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--[DateLog] DATETIME NULL,
--[Status] NVARCHAR(50) NULL
--) ON [PRIMARY]
--GO
--IF OBJECT_ID('trg_t_KDT_CX_HangDuaVao_Log') IS NOT NULL
--DROP TRIGGER trg_t_KDT_CX_HangDuaVao_Log

--GO

--CREATE TRIGGER trg_t_KDT_CX_HangDuaVao_Log 
--ON dbo.t_KDT_CX_HangDuaVao
--AFTER INSERT,UPDATE,DELETE AS
--BEGIN
--IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVao_Log
--		SELECT *,GETDATE(),'Inserted' FROM Inserted
--	END
--IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVao_Log
--		SELECT *,GETDATE(),'Updated' FROM Inserted
--	END
--IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
--	BEGIN
--        INSERT INTO t_KDT_CX_HangDuaVao_Log
--		SELECT *,GETDATE(),'Deleted' FROM Deleted
--	END
--END
--GO

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '37.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('37.9',GETDATE(), N'CẬP NHẬT GHI LOG SẢN PHẨM - NGUYÊN PHỤ LIỆU - ĐỊNH MỨC')
END