using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAHP060 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute APN { get; set; }
public GroupAttribute D01 { get; set; }

public VAHP060()
        {
APN = new PropertiesAttribute(12, typeof(int));
#region D01
List<PropertiesAttribute> listD01 = new List<PropertiesAttribute>();
listD01.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAHP060_D01, typeof(DateTime)));
listD01.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAHP060_T01, typeof(string)));
listD01.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAHP060_C01, typeof(string)));
D01 = new GroupAttribute("D01", 10, listD01);
#endregion D01
TongSoByte = 5554;
}

}public partial class EnumGroupID
    {
public static readonly string VAHP060_D01 = "VAHP060_D01";
public static readonly string VAHP060_T01 = "VAHP060_T01";
public static readonly string VAHP060_C01 = "VAHP060_C01";
}

}
