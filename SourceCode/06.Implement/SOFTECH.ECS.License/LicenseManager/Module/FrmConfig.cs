using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;

namespace Licenses_v2.Module
{
    public partial class FrmConfig : DevExpress.XtraEditors.XtraForm
    {
        Configuration config;
        public FrmConfig()
        {
            InitializeComponent();
        }

        private void FrmConfig_Load(object sender, EventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            this.LoadConfig();
        }

        private void LoadConfig()
        {
            cboxSkin.SelectedItem = config.AppSettings.Settings["Skin"].Value.ToString();
            cboxNavBar.SelectedItem = config.AppSettings.Settings["NavBarStyle"].Value.ToString();
            this.DisableButton(false);
        }

        private void SaveConfig()
        {
            config.AppSettings.Settings.Remove("Skin");
            config.AppSettings.Settings.Add("Skin", cboxSkin.SelectedItem.ToString());
            config.AppSettings.Settings.Remove("NavBarStyle");
            config.AppSettings.Settings.Add("NavBarStyle", cboxNavBar.SelectedItem.ToString());
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
            this.DisableButton(false);
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
        }
        
        private void cboxSkin_SelectedIndexChanged(object sender, EventArgs e)
        {
            FrmMain obj = (FrmMain)MdiParent;
            obj.skinLicense.LookAndFeel.SkinName = cboxSkin.SelectedItem.ToString();
            this.DisableButton(true);
        }

        private void cboxNavBar_SelectedIndexChanged(object sender, EventArgs e)
        {
            FrmMain obj = (FrmMain)MdiParent;
            if (cboxNavBar.SelectedItem.ToString() == "NavigationPane")
                obj.navBarLicenses.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            else obj.navBarLicenses.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.Default;
            this.DisableButton(true);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.LoadConfig();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.SaveConfig();
        }

    /*
        Default
        BaseView
        FlatView
        Office1View
        Office2View
        Office3View
        VSToolBoxView
        AdvExplorerBarView
        ExplorerBarView
        UltraFlatExplorerBarView
        SkinExplorerBarView
        XP1View
        XP2View
        XPExplorerBarView
        NavigationPane
        SkinNavigationPane
        Skin:Caramel
        Skin:Money Twins
        Skin:Lilian
        Skin:The Asphalt World
        Skin:iMaginary
        Skin:Black
        Skin:Blue
        Skin:Coffee
        Skin:Liquid Sky
        Skin:London Liquid Sky
        Skin:Glass Oceans
        Skin:Stardust
        Skin:Xmas 2008 Blue
        Skin:Valentine
        Skin:McSkin
        Skin:Summer 2008
        Skin:Office 2007 Blue
        Skin:Office 2007 Black
        Skin:Office 2007 Silver
        Skin:Office 2007 Green
        Skin:Office 2007 Pink
        SkinNav:Caramel
        SkinNav:Money Twins
        SkinNav:Lilian
        SkinNav:The Asphalt World
        SkinNav:iMaginary
        SkinNav:Black
        SkinNav:Blue
        SkinNav:Coffee
        SkinNav:Liquid Sky
        SkinNav:London Liquid Sky
        SkinNav:Glass Oceans
        SkinNav:Stardust
        SkinNav:Xmas 2008 Blue
        SkinNav:Valentine
        SkinNav:McSkin
        SkinNav:Summer 2008
        SkinNav:Office 2007 Blue
        SkinNav:Office 2007 Black
        SkinNav:Office 2007 Silver
        SkinNav:Office 2007 Green
        SkinNav:Office 2007 Pink
     **/
    }
}