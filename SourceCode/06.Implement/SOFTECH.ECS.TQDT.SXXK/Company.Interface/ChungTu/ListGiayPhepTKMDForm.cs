﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListGiayPhepTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListGiayPhepTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                GiayPhep gp = (GiayPhep)dgList.GetRow().DataRow;

                if (gp != null)
                {
                    TKMD.SoGiayPhep = gp.SoGiayPhep;
                    TKMD.NgayGiayPhep = gp.NgayGiayPhep;
                    TKMD.NgayHetHanGiayPhep = gp.NgayHetHan;
                }
            }

            this.Close();
        }   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}

            //HUNGTQ, Uopdate 07/06/2010
            //SetButtonStateGIAYPHEP(TKMD, isKhaiBoSung);
        } 
      
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhep gp = (GiayPhep)i.GetRow().DataRow;
                        listGiayPhep.Add(gp);
                    }
                }
            }

            foreach (GiayPhep gp in listGiayPhep)
            {
                if (gp.ID > 0)
                {
                    gp.Delete();
                }
                TKMD.GiayPhepCollection.Remove(gp);
            }

            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
              
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            GiayPhepForm f = new GiayPhepForm();
            f.OpenType = OpenFormType.Insert;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.TKMD = TKMD;
            f.ShowDialog();
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayPhepForm f = new GiayPhepForm();
            f.OpenType = OpenFormType.Edit;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.TKMD = TKMD;
            f.giayPhep = (GiayPhep)e.Row.DataRow;
            if (f.giayPhep.ID > 0 && f.giayPhep.ListHMDofGiayPhep.Count == 0)
                f.giayPhep.LoadListHMDofGiayPhep();

            f.ShowDialog();
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateGIAYPHEP(BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung)
        {
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false )
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.SoTiepNhan == 0 || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET);

                btnXoa.Enabled = status;
                btnTaoMoi.Enabled = status;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    //btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
            }

            return true;
        }

        #endregion

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            else
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhep gp = (GiayPhep)i.GetRow().DataRow;
                        if (gp.SoTiepNhan > 0)
                            btnXoa.Enabled = false;
                        else
                            btnXoa.Enabled = true;
                    }
                }
            }
        }

    }
}
