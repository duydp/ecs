using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Feedback.Components
{
	public partial class CauHoiDoanhNghiep : ICloneable
	{
		#region Properties.
		
		public long CauHoiID { set; get; }
		public string MaDonVi { set; get; }
		public string TenDonVi { set; get; }
		public string LinhVuc { set; get; }
		public string Phanloai { set; get; }
		public string TieuDe { set; get; }
		public string NoiDungCauHoi { set; get; }
		public DateTime NgayGui { set; get; }
		public long CauHoiTraLoiID { set; get; }
		public DateTime NgayTraLoi { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<CauHoiDoanhNghiep> ConvertToCollection(IDataReader reader)
		{
			IList<CauHoiDoanhNghiep> collection = new List<CauHoiDoanhNghiep>();
			while (reader.Read())
			{
				CauHoiDoanhNghiep entity = new CauHoiDoanhNghiep();
				if (!reader.IsDBNull(reader.GetOrdinal("CauHoiID"))) entity.CauHoiID = reader.GetInt64(reader.GetOrdinal("CauHoiID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonVi"))) entity.MaDonVi = reader.GetString(reader.GetOrdinal("MaDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonVi"))) entity.TenDonVi = reader.GetString(reader.GetOrdinal("TenDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc"))) entity.LinhVuc = reader.GetString(reader.GetOrdinal("LinhVuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGui"))) entity.NgayGui = reader.GetDateTime(reader.GetOrdinal("NgayGui"));
				if (!reader.IsDBNull(reader.GetOrdinal("CauHoiTraLoiID"))) entity.CauHoiTraLoiID = reader.GetInt64(reader.GetOrdinal("CauHoiTraLoiID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTraLoi"))) entity.NgayTraLoi = reader.GetDateTime(reader.GetOrdinal("NgayTraLoi"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<CauHoiDoanhNghiep> collection, long cauHoiID)
        {
            foreach (CauHoiDoanhNghiep item in collection)
            {
                if (item.CauHoiID == cauHoiID)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GopY_CauHoiDoanhNghiep VALUES(@MaDonVi, @TenDonVi, @LinhVuc, @Phanloai, @TieuDe, @NoiDungCauHoi, @NgayGui, @CauHoiTraLoiID, @NgayTraLoi)";
            string update = "UPDATE t_GopY_CauHoiDoanhNghiep SET MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, LinhVuc = @LinhVuc, Phanloai = @Phanloai, TieuDe = @TieuDe, NoiDungCauHoi = @NoiDungCauHoi, NgayGui = @NgayGui, CauHoiTraLoiID = @CauHoiTraLoiID, NgayTraLoi = @NgayTraLoi WHERE CauHoiID = @CauHoiID";
            string delete = "DELETE FROM t_GopY_CauHoiDoanhNghiep WHERE CauHoiID = @CauHoiID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGui", SqlDbType.DateTime, "NgayGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, "CauHoiTraLoiID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTraLoi", SqlDbType.DateTime, "NgayTraLoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGui", SqlDbType.DateTime, "NgayGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, "CauHoiTraLoiID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTraLoi", SqlDbType.DateTime, "NgayTraLoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GopY_CauHoiDoanhNghiep VALUES(@MaDonVi, @TenDonVi, @LinhVuc, @Phanloai, @TieuDe, @NoiDungCauHoi, @NgayGui, @CauHoiTraLoiID, @NgayTraLoi)";
            string update = "UPDATE t_GopY_CauHoiDoanhNghiep SET MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, LinhVuc = @LinhVuc, Phanloai = @Phanloai, TieuDe = @TieuDe, NoiDungCauHoi = @NoiDungCauHoi, NgayGui = @NgayGui, CauHoiTraLoiID = @CauHoiTraLoiID, NgayTraLoi = @NgayTraLoi WHERE CauHoiID = @CauHoiID";
            string delete = "DELETE FROM t_GopY_CauHoiDoanhNghiep WHERE CauHoiID = @CauHoiID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGui", SqlDbType.DateTime, "NgayGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, "CauHoiTraLoiID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTraLoi", SqlDbType.DateTime, "NgayTraLoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.NVarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGui", SqlDbType.DateTime, "NgayGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, "CauHoiTraLoiID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTraLoi", SqlDbType.DateTime, "NgayTraLoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CauHoiID", SqlDbType.BigInt, "CauHoiID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static CauHoiDoanhNghiep Load(long cauHoiID)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CauHoiID", SqlDbType.BigInt, cauHoiID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<CauHoiDoanhNghiep> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<CauHoiDoanhNghiep> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<CauHoiDoanhNghiep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<CauHoiDoanhNghiep> SelectCollectionBy_LinhVuc(string linhVuc)
		{
            IDataReader reader = SelectReaderBy_LinhVuc(linhVuc);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LinhVuc(string linhVuc)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LinhVuc(string linhVuc)
		{
			const string spName = "p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertCauHoiDoanhNghiep(string maDonVi, string tenDonVi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, DateTime ngayGui, long cauHoiTraLoiID, DateTime ngayTraLoi)
		{
			CauHoiDoanhNghiep entity = new CauHoiDoanhNghiep();	
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.NgayGui = ngayGui;
			entity.CauHoiTraLoiID = cauHoiTraLoiID;
			entity.NgayTraLoi = ngayTraLoi;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@CauHoiID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@NgayGui", SqlDbType.DateTime, NgayGui.Year <= 1753 ? DBNull.Value : (object) NgayGui);
			db.AddInParameter(dbCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, CauHoiTraLoiID);
			db.AddInParameter(dbCommand, "@NgayTraLoi", SqlDbType.DateTime, NgayTraLoi.Year <= 1753 ? DBNull.Value : (object) NgayTraLoi);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				CauHoiID = (long) db.GetParameterValue(dbCommand, "@CauHoiID");
				return CauHoiID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				CauHoiID = (long) db.GetParameterValue(dbCommand, "@CauHoiID");
				return CauHoiID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<CauHoiDoanhNghiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CauHoiDoanhNghiep item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCauHoiDoanhNghiep(long cauHoiID, string maDonVi, string tenDonVi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, DateTime ngayGui, long cauHoiTraLoiID, DateTime ngayTraLoi)
		{
			CauHoiDoanhNghiep entity = new CauHoiDoanhNghiep();			
			entity.CauHoiID = cauHoiID;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.NgayGui = ngayGui;
			entity.CauHoiTraLoiID = cauHoiTraLoiID;
			entity.NgayTraLoi = ngayTraLoi;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GopY_CauHoiDoanhNghiep_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@CauHoiID", SqlDbType.BigInt, CauHoiID);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@NgayGui", SqlDbType.DateTime, NgayGui.Year <= 1753 ? DBNull.Value : (object) NgayGui);
			db.AddInParameter(dbCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, CauHoiTraLoiID);
			db.AddInParameter(dbCommand, "@NgayTraLoi", SqlDbType.DateTime, NgayTraLoi.Year <= 1753 ? DBNull.Value : (object) NgayTraLoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<CauHoiDoanhNghiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CauHoiDoanhNghiep item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCauHoiDoanhNghiep(long cauHoiID, string maDonVi, string tenDonVi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, DateTime ngayGui, long cauHoiTraLoiID, DateTime ngayTraLoi)
		{
			CauHoiDoanhNghiep entity = new CauHoiDoanhNghiep();			
			entity.CauHoiID = cauHoiID;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.NgayGui = ngayGui;
			entity.CauHoiTraLoiID = cauHoiTraLoiID;
			entity.NgayTraLoi = ngayTraLoi;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@CauHoiID", SqlDbType.BigInt, CauHoiID);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.NVarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@NgayGui", SqlDbType.DateTime, NgayGui.Year <= 1753 ? DBNull.Value : (object) NgayGui);
			db.AddInParameter(dbCommand, "@CauHoiTraLoiID", SqlDbType.BigInt, CauHoiTraLoiID);
			db.AddInParameter(dbCommand, "@NgayTraLoi", SqlDbType.DateTime, NgayTraLoi.Year <= 1753 ? DBNull.Value : (object) NgayTraLoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<CauHoiDoanhNghiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CauHoiDoanhNghiep item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCauHoiDoanhNghiep(long cauHoiID)
		{
			CauHoiDoanhNghiep entity = new CauHoiDoanhNghiep();
			entity.CauHoiID = cauHoiID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CauHoiID", SqlDbType.BigInt, CauHoiID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LinhVuc(string linhVuc)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_DeleteBy_LinhVuc]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GopY_CauHoiDoanhNghiep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<CauHoiDoanhNghiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CauHoiDoanhNghiep item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}