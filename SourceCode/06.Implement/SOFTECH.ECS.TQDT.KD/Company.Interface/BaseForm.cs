﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Company.KD.BLL;
using Company.KD.BLL.DuLieuChuan;
using Company.Controls;
//
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;
using System.Collections.Generic;

using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;

using System;
using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
using Company.Controls.CustomValidation;

namespace Company.Interface
{
	public partial class BaseForm : Form
    {
        public string MaDoanhNghiep;
        public string MaHaiQuan;
        public Company.KDT.SHARE.Components.OpenFormType OpenType;
        public string CalledForm = string.Empty;

        protected MessageBoxControl _MsgBox;
        protected KDTMessageBoxControl _KDTMsgBox;
        protected DataTable _DonViTinh;
        protected DataTable _DonViHaiQuan;
        protected DataTable _LoaiHinhMauDich;
        protected DataTable _NguyenTe;
        protected DataTable _Nuoc;
        protected DataTable _PhuongThucThanhToan;
        protected DataTable _PhuongThucVanTai;
        protected DataTable _DieuKienGiaoHang;
        protected DataTable _NhomSanPham;
        protected DataTable _LoaiPhuKien;
        protected DataTable _LoaiCO;
		private IContainer components = null;
        public ResourceManager resource = null;
        public string errorMsg = String.Empty;

        public delegate void ValidatorDelegate() ;
        public ValidatorDelegate InitValidator;

        public string ShowMessageTQDT(string messageHQ, string messageContent, bool showYesNoButton)
        {
            this._KDTMsgBox = new KDTMessageBoxControl();
            this._KDTMsgBox.ShowYesNoButton = showYesNoButton;
            this._KDTMsgBox.HQMessageString = messageHQ;
            this._KDTMsgBox.MessageString = messageContent;
            this._KDTMsgBox.ShowDialog();
            string st = this._KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public string ShowMessageTQDT( string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            this._KDTMsgBox = new KDTMessageBoxControl();
            this._KDTMsgBox.ShowYesNoButton = showYesNoButton;
            this._KDTMsgBox.HQMessageString = messageHQ;
            this._KDTMsgBox.MessageString = messageContent;
            this._KDTMsgBox.ShowDialog();
            string st = this._KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st= this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        public string ShowMessage(string message, bool showYesNoButton, bool showErrorButton, string exceptionString)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowErrorButton = showErrorButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.exceptionString = exceptionString;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        public string MLMessages(string msg, string key, string value, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            string message = msg;
            string st = "";
            this._MsgBox.MessageString = msg;
            if (Properties.Settings.Default.NgonNgu == "1")
            {
                try{
                if (resource.GetString(key) != null && resource.GetString(key).Trim() != "")
                {
                    // resource asign in InitCulture() 
                    message = resource.GetString(key);
                    message = message.Replace("$", value);
                }
                }catch{}
                this._MsgBox.MessageString = message;
            }
            this._MsgBox.ShowDialog();
            st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            
            base.OnFormClosed(e);
            this.Dispose();
        }


        protected override void OnLoad(System.EventArgs e)
        {
            this.helpProvider1.SetHelpKeyword(this, this.Name + ".htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);

            this.helpProvider1.SetShowHelp(this,true);
            //foreach (Control ctr in this.Controls)
            //{
            //    this.helpProvider1.SetHelpKeyword(ctr, this.Name + ".htm");
            //    this.helpProvider1.SetHelpNavigator(ctr, System.Windows.Forms.HelpNavigator.Topic);
            //    this.helpProvider1.SetShowHelp(ctr, true);
 
            //}                      
            if (Thread.CurrentThread.CurrentCulture.Equals(new CultureInfo("en-US")))
            {
                this.InitCulture("en-US", "Company.Interface.LanguageResource.Language");
            }
            else
            {
                ResourceManager rm = new ResourceManager("Company.Interface.LanguageResource.Language", typeof(BaseForm).Assembly);
                this.resource = rm;
            }
            
            base.OnLoad(e);
            //if (!Program.isActivated)
            //{
            //    if(!this.inForm(this.Name))
            //    {
            //        MessageBox.Show("Phiên bản chưa được kích hoạt.\nHãy kích hoạt phần mềm để được hổ trợ nhiều hơn");
            //    }
            //}
        }


		public BaseForm()
        {
            if (this.DesignMode == false)
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
            }
			InitializeComponent();
        }

        #region Dữ liệu chuẩn.
        public string LoaiCO_GetName(object id)
        {
            if (_LoaiCO == null) _LoaiCO = LoaiCO.SelectAll().Tables[0];
            return this._LoaiCO.Select(string.Format("Ma = '{0}'", id.ToString()))[0][1].ToString();
        }
        public string Nuoc_GetName(object id)
        {
            if (_Nuoc == null) _Nuoc = Nuoc.SelectAll().Tables[0];
            return this._Nuoc.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();
        }
	    
        protected string DonViTinh_GetName(object id)
        {
            if (this._DonViTinh == null) this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            return this._DonViTinh.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();
        }
        protected string DonViTinh_GetID(string ten)
        {
            if (this._DonViTinh == null) this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            return this._DonViTinh.Select(string.Format("Ten = '{0}'", ten))[0][0].ToString();
        }
        protected string DonViHaiQuan_GetName(object id)
        {
            if (_DonViHaiQuan == null) _DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            return this._DonViHaiQuan.Select(string.Format("ID = '{0}'", id.ToString().PadRight(6)))[0][1].ToString();
        }
        protected string LoaiHinhMauDich_GetTenVT(object id)
        {
            if (_LoaiHinhMauDich == null) _LoaiHinhMauDich = LoaiHinhMauDich.SelectAll();
            return this._LoaiHinhMauDich.Select(string.Format("ID = '{0}'", id.ToString().PadRight(5)))[0][2].ToString();
        }
        protected string LoaiHinhMauDich_GetName(object id)
        {
            if (_LoaiHinhMauDich == null) _LoaiHinhMauDich = LoaiHinhMauDich.SelectAll();
            return this._LoaiHinhMauDich.Select(string.Format("ID = '{0}'", id.ToString().PadRight(5)))[0][1].ToString();
        }
        protected string NhomSanPham_GetName(object id)
        {
            if (_NhomSanPham == null) _NhomSanPham = NhomSanPham.SelectAll().Tables[0];
            return this._NhomSanPham.Select(string.Format("MaSanPham = '{0}'", id.ToString().PadRight(5)))[0][2].ToString();
        }
        protected string LoaiPhuKien_GetName(object id)
        {
            if (_LoaiPhuKien == null) _LoaiPhuKien = Company.KD.BLL.DuLieuChuan.LoaiPhuKien.SelectAll();
            return this._LoaiPhuKien.Select(string.Format("ID_LoaiPhuKien = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();
        }

        protected string NguyenTe_GetName(object id)
        {
            if (_NguyenTe == null) _NguyenTe = Company.KD.BLL.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            return this._NguyenTe.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();
        }
        protected string NguyenTe_ID(object Ten)
        {
            if (_NguyenTe == null) _NguyenTe = Company.KD.BLL.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            return this._NguyenTe.Select(string.Format("Ten = '{0}'", Ten.ToString()))[1][1].ToString();
        }

        protected string DieuKienGH_GetName(object id)
        {
            if (_DieuKienGiaoHang == null) _DieuKienGiaoHang = Company.KD.BLL.DuLieuChuan.DieuKienGiaoHang.SelectAll().Tables[0];
            return this._DieuKienGiaoHang.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();
        }
        protected string PTVT_GetName(string id)
        {
            string PTVT_Name = "";
            PTVT_Name = PhuongThucVanTai.getName(id); ;
            return PTVT_Name;          
        }
        protected string PTTT_GetName(string id)
        {
            string PTTT_Name = "";
            PTTT_Name = PhuongThucThanhToan.getName(id);
            return PTTT_Name;
        }
        #endregion

        #region setlanguage

        // Get all controls in form.
        public List<Control> GetAllControls(IList controls)
        {
            List<Control> allControls = new List<Control>();
            foreach (Control control in controls)
            {
                allControls.Add(control);
                List<Control> subControls = GetAllControls(control.Controls);
                allControls.AddRange(subControls);
            }

            return allControls;
        }
        public List<Component> GetAllComponent()
        {
            List<Component> allComponent = new List<Component>();
            int i = this.components.Components.Count;
            foreach (Component component in this.components.Components)
            {
                allComponent.Add(component);
            }
            //int j = this.
            return allComponent;
        }
        public void forGrid(Control c, ResourceManager rm, string formName)
        {
            GridEX grid = (GridEX)c;
            GridEXColumnCollection columnColl = grid.RootTable.Columns;
            string columnKey = "";
            string key = String.Empty;
            foreach (GridEXColumn column in columnColl)
            {
                try{
                key = columnKey = "";
                columnKey = column.Key;
                key = formName + "_" + c.Name + "_" + columnKey;
                if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                {
                    column.Caption = rm.GetString(key);
                }
                } catch{}
            }
            if (grid.RootTable.Groups != null)
            {
                foreach (GridEXGroup group in grid.RootTable.Groups)
                {
                    try{
                        key = "";
                        key = formName + "_" + grid.Name + "_group" + group.Index.ToString();
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            group.HeaderCaption = rm.GetString(key);
                        }
                    }catch{}
                } 
            }            
            if (grid.RootTable.ColumnSets != null)
            {
                foreach (GridEXColumnSet groupSet in grid.RootTable.ColumnSets)
                {
                    try{
                        key = "";
                        key = formName + "_" + grid.Name +"_"+ groupSet.Key;
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            groupSet.Caption = rm.GetString(key);
                        }                        
                    } catch{}
                }
            }
            if (grid.RootTable.GroupHeaderTotals != null)
            {
                foreach (GridEXGroupHeaderTotal groupHeaderTotal in grid.RootTable.GroupHeaderTotals)
                {
                    try{
                        key = "";
                        key = formName + "_" + grid.Name + "_" + groupHeaderTotal.Key;
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            groupHeaderTotal.TotalSuffix = rm.GetString(key);
                        }
                    }
                     catch{}
                }
            }
            if (grid.ContextMenuStrip != null)
            {
                forToolStripMenuItem(grid.ContextMenuStrip.Items, rm, formName, grid.ContextMenuStrip.Name);
            }
        }
        public void forToolStripMenuItem(ToolStripItemCollection itemColl, ResourceManager rm, string formName, string contextMenuName)
        {
            foreach (ToolStripMenuItem menuItem in itemColl)
            {
                try{
                    string key = "";
                    key = formName + "_" + contextMenuName + "_" + menuItem.Name;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        menuItem.Text = rm.GetString(key);

                    }
                }
                catch { }
                if (menuItem.DropDownItems != null && menuItem.DropDownItems.Count > 0) forToolStripMenuItem(menuItem.DropDownItems, rm, formName, contextMenuName);
            }
        }
        public List<UIPanelBase> getUIPanelChild(Janus.Windows.UI.Dock.UIPanelCollection panelColl, UIPanelGroup panelGroup)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            if (panelColl != null)//coll
            {
                foreach (UIPanelGroup panelG in panelColl)
                {
                    string n = panelG.Name;
                    allPanels.Add(panelG);
                    getUIPanelChild(null, panelG);
                }
            }
            if (panelGroup != null)//group
            {
                if (panelGroup.Panels.Count > 0)
                {
                    for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                    {
                        if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                        {
                            UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                            string n = tmppanelGroup.Name;
                            allPanels.Add(tmppanelGroup);
                            allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                            allPanels.AddRange(allPanels2);
                        }
                        else
                        {
                            string panelName = panelGroup.Panels[i].Name;
                            allPanels.Add(panelGroup.Panels[i]);
                        }
                    }
                }
            }
            return allPanels;
        }
        public List<UIPanelBase> getUIPanelChild2(Janus.Windows.UI.Dock.UIPanelCollection panelC, UIPanelGroup panelG)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            UIPanelGroup panelGroup;
            if (panelC != null)//coll
            {
                panelGroup = (UIPanelGroup)panelC[0];
                allPanels.Add(panelGroup);
            }
            else
            {
                panelGroup = panelG;
            }
            string na = panelGroup.Name;
            if (panelGroup.Panels.Count > 0)
            {
                for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                {
                    if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                    {
                        UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                        string n = tmppanelGroup.Name;
                        allPanels.Add(tmppanelGroup);
                        allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                        allPanels.AddRange(allPanels2);
                    }
                    else
                    {
                        string panelName = panelGroup.Panels[i].Name;
                        allPanels.Add(panelGroup.Panels[i]);
                    }
                }
            }
            return allPanels;
        }
        public void forPanel(Component c, ResourceManager rm, string formName)
        {
            if (c.GetType().ToString().Contains("PanelManager"))
            {
                Janus.Windows.UI.Dock.UIPanelManager p = (Janus.Windows.UI.Dock.UIPanelManager)c;
                Janus.Windows.UI.Dock.UIPanelCollection panelColl = p.Panels;
                List<UIPanelBase> panels = getUIPanelChild2(panelColl, null);
                foreach (Janus.Windows.UI.Dock.UIPanelBase panel in panels)
                {
                    try{
                        string panelName = panel.Name;
                        string panelText = panel.Text;
                        string key = formName + "_" + panelName;                    
                        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                        {
                            panel.Text = rm.GetString(key);
                        }
                    }
                    catch { }
                }

            }
            else
            {

            }

        }// no need
        public void forExplore(Component c, ResourceManager rm, string formName)
        {

            ExplorerBar e = (ExplorerBar)c;
            string eName = e.Name;
            foreach (ExplorerBarGroup eGroup in e.Groups)
            {
                 string groupKey = eGroup.Key;
                 try
                {
                                   
                    if (rm.GetString(formName + "_" + groupKey) != null && rm.GetString(formName + "_" + groupKey).Trim() != "")
                    {
                        eGroup.Text = rm.GetString(formName + "_" + groupKey);
                    }
                }catch{}
                foreach (ExplorerBarItem eItem in eGroup.Items)
                {
                    try
                    {
                        string itemKey = eItem.Key;                  
                        string key = formName + "_" + groupKey + "_" + itemKey;
                        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                        {
                            eItem.Text = rm.GetString(key);
                        }
                    }
                    catch { }

                }


            }
        }
        public List<UICommand> forCommandBar(Control c, UICommand cm, ResourceManager rm, string formName)
        {
            List<UICommand> allCommand = new List<UICommand>();
            List<UICommand> suballCommand = new List<UICommand>();
            UICommandBar cmdBar = (UICommandBar)c;
            if (c != null)
            {
                //contextMenu
                UICommandManager cmdMangager = cmdBar.CommandManager;
                UIContextMenuCollection contextMenuColl = cmdMangager.ContextMenus;
                if (contextMenuColl != null)
                {
                    for (int i = 0; i < cmdMangager.ContextMenus.Count; i++)
                    {
                        UICommandCollection cmdColl = cmdMangager.ContextMenus[i].Commands;
                        if (cmdColl != null && cmdColl.Count > 0)
                        {
                            foreach (UICommand cmd in cmdColl)
                            {
                                try
                                {
                                    string key = formName + "_" + cmd.Key;
                                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                                    {
                                        cmd.Text = rm.GetString(key);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
                //if (cmdMangager.Commands != null)
                //{
                //    foreach (UICommand cmd in cmdMangager.Commands)
                //    {
                //        string key = formName + "_" + cmd.Key;
                //        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                //        {
                //            cmd.Text = rm.GetString(key);

                //        }
                //    }

                //}
                //Command
                string cmdBarKey = cmdBar.Name;
                foreach (UICommand cmd in cmdBar.Commands)
                {
                    try
                    {
                        string cmdKey = cmd.Key;
                        if (rm.GetString(formName + "_" + cmdKey) != null && rm.GetString(formName + "_" + cmdKey).Trim() != "")
                        {
                            cmd.Text = rm.GetString(formName + "_" + cmdKey);
                        }
                    }
                    catch { }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = forCommandBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            if (cm != null)
            {
                string cmdKey = cm.Key;
                allCommand.Add(cm);
                foreach (UICommand cmd in cm.Commands)
                {
                    try{
                        string Key = cmd.Key;
                        //cmd.Text;
                        if (rm.GetString(formName + "_" + Key) != null && rm.GetString(formName + "_" + Key) != "")
                        {
                            cmd.Text = rm.GetString(formName + "_" + Key);
                        }
                        if (cmd.Commands.Count > 0)
                        {
                            suballCommand = forCommandBar(null, cmd, rm, formName);
                            allCommand.AddRange(suballCommand);
                        }
                        else
                        {
                            allCommand.Add(cmd);
                        }
                    }catch{}
                }
            }
            return allCommand;

        }
        public void forChildControlInpanel(UIPanelBase panel, ResourceManager rm, string formname)
        {
            if (panel.GetType() == typeof(UIPanelGroup))
            {
                UIPanelGroup panelG = (UIPanelGroup)panel;
                foreach (UIPanelBase pBase in panelG.Panels)
                {
                    forChildControlInpanel(pBase, rm, formname);
                }
            }
            else//UIPanel
            {
                string key = String.Empty;
                key = panel.Name;
                key = "";
                Control tmpCtr = panel.GetNextControl(new Control(), true);
                if (tmpCtr.GetType() == typeof(UIPanelInnerContainer))
                {
                    UIPanelInnerContainer panelContainer = (UIPanelInnerContainer)tmpCtr;
                    if (panelContainer.Controls != null && panelContainer.Controls.Count > 0 )
                    {
                        foreach (Control control in panelContainer.Controls)
                        {
                            // Label
                            if (control.GetType() == typeof(GridEX))
                            {
                                this.forGrid(control, rm, formname);
                            }
                            else if (control.GetType() == typeof(UIPanel))//UIpanel
                            {
                                if (panel.Name == control.Name) { }
                                key = String.Empty;
                                key = formname + "_" + panel.Name;
                                try
                                {
                                    if (rm.GetString(key) != null && rm.GetString(key) != "")
                                    {
                                        control.Text = rm.GetString(key);
                                    }
                                }
                                catch
                                {
                                }
                            }
                            else //UIGroupBox
                            {
                                UIGroupBox group = (UIGroupBox)control;
                                if (group.HasChildren) 
                                {
                                }
                            }
                        }
                    }
                }
                else//UiPanel 
                {
                    forChildControlInpanel((UIPanel)tmpCtr, rm, formname);
                }
            }

        }
        // Init culture and language.
        public void InitCulture(string language, string resourcefile)
        {
            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // Init control:
            ResourceManager rm = new ResourceManager(resourcefile, typeof(BaseForm).Assembly);
            this.resource = rm;
            List<Control> allControls = GetAllControls(this.Controls);
            if (this.components != null)
            {
                List<Component> allComponents = this.GetAllComponent();
            }

            int i = allControls.Count;
            string name = "";
            string key = "";
            foreach (Control c in allControls)
            {
                Form f = c.FindForm();

                Type ctrType = c.GetType();
                // if (ctrTypeName == "Janus.Window.GridEX.GridEX" )                
                if (ctrType == typeof(GridEX))
                {
                    this.forGrid(c, rm, f.Name);
                }
                else if (ctrType == typeof(Janus.Windows.ExplorerBar.ExplorerBar))
                {
                    this.forExplore(c, rm, f.Name);
                }
                else if (c.GetType() == typeof(UICommandBar))
                {
                    List<UICommand> cmdList = this.forCommandBar(c, null, rm, f.Name);
                }
                else if (c.GetType() == typeof(ContextMenuStrip))
                {

                }

                else if (c.GetType() == typeof(UIComboBox))
                {
                    UIComboBox cbo = (UIComboBox)c;
                    if (cbo.Items.Count > 0 && cbo.ValueMember == "")
                    {
                        foreach (UIComboBoxItem item in cbo.Items)
                        {
                            key = "";
                            string text = item.Text.ToString();
                            key = text;
                            switch (key.Trim())
                            {
                                case "Chưa khai báo": key = "status_No_Declared"; break;
                                case "Chờ duyệt": key = "status_Wait_Approved"; break;
                                case "Đã duyệt": key = "status_Approved"; break;
                                case "Không phê duyệt": key = "status_No_Approved"; break;
                                case "Nhập khẩu": key = "status_Import"; break;
                                case "Xuất khẩu": key = "status_Export"; break;
                                case "Tất cả": key = "method_all"; break;
                                case "Tờ khai nhập": key = "type_Import"; break;
                                case "Tờ khai xuất": key = "type_Export"; break;

                                case "Nguyên phụ liệu": key = "module_NPL"; break;
                                case "Sản phẩm": key = "module_SP"; break;
                                case "Tờ khai": key = "module_TK"; break;
                                case "Thanh khoản": key = "module_TKh"; break;
                                case "Quản trị hệ thống": key = "module_QT"; break;

                                case "Không": key = "thanhkhoan_No"; break;
                                case "Có": key = "thanhkhoan_Yes"; break;
                                case "Theo mã NPL": key = "sort_material"; break;
                                case "Theo ngày ĐK tờ khai": key = "sort_declaration"; break;
                            }
                            try
                            {
                                if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                                {
                                    item.Text = rm.GetString(key);
                                }
                            }catch{}

                        }

                    }

                }
                else if (c.GetType() == typeof(FilterEditor))
                {
                    FilterEditor filter = (FilterEditor)c;
                    key = "";
                    string columnKey = "";
                    Janus.Windows.GridEX.GridEX grid = (Janus.Windows.GridEX.GridEX)filter.SourceControl;
                    foreach (GridEXColumn column in filter.Table.Fields)
                    {
                        try
                        {                        
                            key = columnKey = "";
                            columnKey = column.Key;
                            key = f.Name + "_" + grid.Name + "_" + columnKey;
                            if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                            {
                                column.Caption = rm.GetString(key);
                            }
                        }
                        catch { }
                    }
                    //filter.Refresh();
                    filter.Reload();

                }
                else if (c.GetType() == typeof(UIAutoHideStrip))
                {
                    UIAutoHideStrip hideCtr = (UIAutoHideStrip)c;
                    UIPanelBase[] panelbase = hideCtr.Panels;
                    foreach (UIPanelBase panel in panelbase)
                    {   
                        try
                        {
                            key = String.Empty;
                            key = f.Name + "_" + panel.Name;
                            key = key.Trim();
                            
                                if (rm.GetString(key) != null && rm.GetString(key).Length >= 1)
                                {
                                    panel.Text = rm.GetString(key);
                                }
                                                    
                            if (panel.HasChildren)
                            {
                                forChildControlInpanel((UIPanelBase)panel, rm, f.Name);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                else if (c.GetType() == typeof(UIStatusBar))
                {
                    UIStatusBar statusBar = (UIStatusBar)c;
                    if (statusBar.Panels != null)
                    {
                        foreach (UIStatusBarPanel statusP in statusBar.Panels)
                        {
                            try{
                                key = "";
                                key = f.Name + "_" + c.Name + "_" + statusP.Key;
                                if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                                {
                                    statusP.Text = statusP.ToolTipText = rm.GetString(key);

                                }
                            }catch{}
                        }
                    }
                }
                else
                {
                    key = String.Empty;
                    if (c.Name.Trim() != "")
                    {
                        key = f.Name + "_" + c.Name;
                        try
                        {
                            if (rm.GetString(key) != null && rm.GetString(key).Trim() != "") { c.Text = rm.GetString(key); }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            // Init form title:                 
            try
            {
                string text = this.Name.Trim();
                if (rm.GetString(text) != null && rm.GetString(text).Trim() != "")
                {
                    this.Text = rm.GetString(text);
                }
            }
            catch
            {
            }
            if (ValidatorManager.GetValidators(this) != null)
            {
                foreach (BaseValidator baseVali in ValidatorManager.GetValidators(this))
                {
                try{
                    baseVali.ErrorMessage = rm.GetString(this.Name + "_" + baseVali.Tag);
                    baseVali.ErrorMessageEnglish = rm.GetString(this.Name + "_" + baseVali.Tag);
                    string st = baseVali.ErrorMessage;
                    }catch{}
                }
            }
        }
        public string setText(string vnText, string enText ) 
        {
            if (Properties.Settings.Default.NgonNgu == "1")
            {
                return enText;
            }
            else
            {
                return vnText;
            }            
        }

        #endregion

    }
}