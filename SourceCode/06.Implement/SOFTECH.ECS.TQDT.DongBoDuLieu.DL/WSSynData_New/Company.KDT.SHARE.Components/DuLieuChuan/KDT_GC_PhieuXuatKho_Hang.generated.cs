using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class KDT_GC_PhieuXuatKho_Hang : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string MaSoHang { set; get; }
		public string MaNPL2 { set; get; }
		public string TenHang { set; get; }
		public string Art { set; get; }
		public string NCC { set; get; }
		public string Size { set; get; }
		public string Color { set; get; }
		public decimal SoLuong1 { set; get; }
		public string DVTLuong1 { set; get; }
		public decimal DM { set; get; }
		public decimal PhanTram { set; get; }
		public decimal Nhucau { set; get; }
		public decimal ThucNhan { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhieuXuatKho_Hang> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhieuXuatKho_Hang> collection = new List<KDT_GC_PhieuXuatKho_Hang>();
			while (reader.Read())
			{
				KDT_GC_PhieuXuatKho_Hang entity = new KDT_GC_PhieuXuatKho_Hang();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHang"))) entity.MaSoHang = reader.GetString(reader.GetOrdinal("MaSoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL2"))) entity.MaNPL2 = reader.GetString(reader.GetOrdinal("MaNPL2"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Art"))) entity.Art = reader.GetString(reader.GetOrdinal("Art"));
				if (!reader.IsDBNull(reader.GetOrdinal("NCC"))) entity.NCC = reader.GetString(reader.GetOrdinal("NCC"));
				if (!reader.IsDBNull(reader.GetOrdinal("Size"))) entity.Size = reader.GetString(reader.GetOrdinal("Size"));
				if (!reader.IsDBNull(reader.GetOrdinal("Color"))) entity.Color = reader.GetString(reader.GetOrdinal("Color"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong1"))) entity.SoLuong1 = reader.GetDecimal(reader.GetOrdinal("SoLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong1"))) entity.DVTLuong1 = reader.GetString(reader.GetOrdinal("DVTLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DM"))) entity.DM = reader.GetDecimal(reader.GetOrdinal("DM"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanTram"))) entity.PhanTram = reader.GetDecimal(reader.GetOrdinal("PhanTram"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nhucau"))) entity.Nhucau = reader.GetDecimal(reader.GetOrdinal("Nhucau"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThucNhan"))) entity.ThucNhan = reader.GetDecimal(reader.GetOrdinal("ThucNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhieuXuatKho_Hang> collection, long id)
        {
            foreach (KDT_GC_PhieuXuatKho_Hang item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuXuatKho_Hang VALUES(@TKMD_ID, @MaSoHang, @MaNPL2, @TenHang, @Art, @NCC, @Size, @Color, @SoLuong1, @DVTLuong1, @DM, @PhanTram, @Nhucau, @ThucNhan, @GhiChu)";
            string update = "UPDATE t_KDT_GC_PhieuXuatKho_Hang SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, MaNPL2 = @MaNPL2, TenHang = @TenHang, Art = @Art, NCC = @NCC, Size = @Size, Color = @Color, SoLuong1 = @SoLuong1, DVTLuong1 = @DVTLuong1, DM = @DM, PhanTram = @PhanTram, Nhucau = @Nhucau, ThucNhan = @ThucNhan, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuXuatKho_Hang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL2", SqlDbType.NVarChar, "MaNPL2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Art", SqlDbType.NVarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Color", SqlDbType.NVarChar, "Color", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DM", SqlDbType.Decimal, "DM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanTram", SqlDbType.Decimal, "PhanTram", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nhucau", SqlDbType.Decimal, "Nhucau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThucNhan", SqlDbType.Decimal, "ThucNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL2", SqlDbType.NVarChar, "MaNPL2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Art", SqlDbType.NVarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Color", SqlDbType.NVarChar, "Color", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DM", SqlDbType.Decimal, "DM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanTram", SqlDbType.Decimal, "PhanTram", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nhucau", SqlDbType.Decimal, "Nhucau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThucNhan", SqlDbType.Decimal, "ThucNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuXuatKho_Hang VALUES(@TKMD_ID, @MaSoHang, @MaNPL2, @TenHang, @Art, @NCC, @Size, @Color, @SoLuong1, @DVTLuong1, @DM, @PhanTram, @Nhucau, @ThucNhan, @GhiChu)";
            string update = "UPDATE t_KDT_GC_PhieuXuatKho_Hang SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, MaNPL2 = @MaNPL2, TenHang = @TenHang, Art = @Art, NCC = @NCC, Size = @Size, Color = @Color, SoLuong1 = @SoLuong1, DVTLuong1 = @DVTLuong1, DM = @DM, PhanTram = @PhanTram, Nhucau = @Nhucau, ThucNhan = @ThucNhan, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuXuatKho_Hang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL2", SqlDbType.NVarChar, "MaNPL2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Art", SqlDbType.NVarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Color", SqlDbType.NVarChar, "Color", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DM", SqlDbType.Decimal, "DM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanTram", SqlDbType.Decimal, "PhanTram", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nhucau", SqlDbType.Decimal, "Nhucau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThucNhan", SqlDbType.Decimal, "ThucNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL2", SqlDbType.NVarChar, "MaNPL2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Art", SqlDbType.NVarChar, "Art", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NCC", SqlDbType.NVarChar, "NCC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Color", SqlDbType.NVarChar, "Color", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DM", SqlDbType.Decimal, "DM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanTram", SqlDbType.Decimal, "PhanTram", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nhucau", SqlDbType.Decimal, "Nhucau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThucNhan", SqlDbType.Decimal, "ThucNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhieuXuatKho_Hang Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhieuXuatKho_Hang> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhieuXuatKho_Hang> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhieuXuatKho_Hang> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhieuXuatKho_Hang(long tKMD_ID, string maSoHang, string maNPL2, string tenHang, string art, string nCC, string size, string color, decimal soLuong1, string dVTLuong1, decimal dM, decimal phanTram, decimal nhucau, decimal thucNhan, string ghiChu)
		{
			KDT_GC_PhieuXuatKho_Hang entity = new KDT_GC_PhieuXuatKho_Hang();	
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaNPL2 = maNPL2;
			entity.TenHang = tenHang;
			entity.Art = art;
			entity.NCC = nCC;
			entity.Size = size;
			entity.Color = color;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.DM = dM;
			entity.PhanTram = phanTram;
			entity.Nhucau = nhucau;
			entity.ThucNhan = thucNhan;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaNPL2", SqlDbType.NVarChar, MaNPL2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.NVarChar, Art);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@Color", SqlDbType.NVarChar, Color);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@DM", SqlDbType.Decimal, DM);
			db.AddInParameter(dbCommand, "@PhanTram", SqlDbType.Decimal, PhanTram);
			db.AddInParameter(dbCommand, "@Nhucau", SqlDbType.Decimal, Nhucau);
			db.AddInParameter(dbCommand, "@ThucNhan", SqlDbType.Decimal, ThucNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhieuXuatKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho_Hang item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhieuXuatKho_Hang(long id, long tKMD_ID, string maSoHang, string maNPL2, string tenHang, string art, string nCC, string size, string color, decimal soLuong1, string dVTLuong1, decimal dM, decimal phanTram, decimal nhucau, decimal thucNhan, string ghiChu)
		{
			KDT_GC_PhieuXuatKho_Hang entity = new KDT_GC_PhieuXuatKho_Hang();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaNPL2 = maNPL2;
			entity.TenHang = tenHang;
			entity.Art = art;
			entity.NCC = nCC;
			entity.Size = size;
			entity.Color = color;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.DM = dM;
			entity.PhanTram = phanTram;
			entity.Nhucau = nhucau;
			entity.ThucNhan = thucNhan;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaNPL2", SqlDbType.NVarChar, MaNPL2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.NVarChar, Art);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@Color", SqlDbType.NVarChar, Color);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@DM", SqlDbType.Decimal, DM);
			db.AddInParameter(dbCommand, "@PhanTram", SqlDbType.Decimal, PhanTram);
			db.AddInParameter(dbCommand, "@Nhucau", SqlDbType.Decimal, Nhucau);
			db.AddInParameter(dbCommand, "@ThucNhan", SqlDbType.Decimal, ThucNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhieuXuatKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho_Hang item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhieuXuatKho_Hang(long id, long tKMD_ID, string maSoHang, string maNPL2, string tenHang, string art, string nCC, string size, string color, decimal soLuong1, string dVTLuong1, decimal dM, decimal phanTram, decimal nhucau, decimal thucNhan, string ghiChu)
		{
			KDT_GC_PhieuXuatKho_Hang entity = new KDT_GC_PhieuXuatKho_Hang();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.MaNPL2 = maNPL2;
			entity.TenHang = tenHang;
			entity.Art = art;
			entity.NCC = nCC;
			entity.Size = size;
			entity.Color = color;
			entity.SoLuong1 = soLuong1;
			entity.DVTLuong1 = dVTLuong1;
			entity.DM = dM;
			entity.PhanTram = phanTram;
			entity.Nhucau = nhucau;
			entity.ThucNhan = thucNhan;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@MaNPL2", SqlDbType.NVarChar, MaNPL2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Art", SqlDbType.NVarChar, Art);
			db.AddInParameter(dbCommand, "@NCC", SqlDbType.NVarChar, NCC);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@Color", SqlDbType.NVarChar, Color);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@DM", SqlDbType.Decimal, DM);
			db.AddInParameter(dbCommand, "@PhanTram", SqlDbType.Decimal, PhanTram);
			db.AddInParameter(dbCommand, "@Nhucau", SqlDbType.Decimal, Nhucau);
			db.AddInParameter(dbCommand, "@ThucNhan", SqlDbType.Decimal, ThucNhan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhieuXuatKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho_Hang item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhieuXuatKho_Hang(long id)
		{
			KDT_GC_PhieuXuatKho_Hang entity = new KDT_GC_PhieuXuatKho_Hang();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhieuXuatKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho_Hang item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}