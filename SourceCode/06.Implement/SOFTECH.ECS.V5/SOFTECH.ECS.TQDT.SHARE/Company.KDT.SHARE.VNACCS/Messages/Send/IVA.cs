﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IVA : BasicVNACC
    {

        public List<IVA_HANGHOA> HangHoaTrongHoaDon { get; set; }

        public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<IVA>(StrBuild, true, GlobalVNACC.PathConfig, "IVA");
            foreach (IVA_HANGHOA item in HangHoaTrongHoaDon)
            {
                item.BuildEdiMessages<IVA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "IVA_HANGHOA");

            }
            return str;
        }
        

    }
}
