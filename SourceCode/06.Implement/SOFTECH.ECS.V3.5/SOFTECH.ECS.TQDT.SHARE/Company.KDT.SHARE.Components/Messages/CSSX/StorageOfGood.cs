﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public  class StorageOfGood
    {
        /// <summary>
        /// Tên 
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Mã  
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }
    }
}
