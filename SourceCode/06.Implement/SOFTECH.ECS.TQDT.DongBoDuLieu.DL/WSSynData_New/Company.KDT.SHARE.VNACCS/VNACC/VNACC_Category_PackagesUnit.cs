using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_PackagesUnit
	{
        protected static List<VNACC_Category_PackagesUnit> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_PackagesUnit> collection = new List<VNACC_Category_PackagesUnit>();
            while (reader.Read())
            {
                VNACC_Category_PackagesUnit entity = new VNACC_Category_PackagesUnit();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NumberOfPackagesUnitCode"))) entity.NumberOfPackagesUnitCode = reader.GetString(reader.GetOrdinal("NumberOfPackagesUnitCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("NumberOfPackagesUnitName"))) entity.NumberOfPackagesUnitName = reader.GetString(reader.GetOrdinal("NumberOfPackagesUnitName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_PackagesUnit> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [NumberOfPackagesUnitCode], NumberOfPackagesUnitName FROM [dbo].[t_VNACC_Category_PackagesUnit]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }
        public static string GetID(string id)
        {
            try
            {
                string spName = string.Format("SELECT NumberOfPackagesUnitCode FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE NumberOfPackagesUnitCode='{0}'", id);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
                IDataReader reader = db.ExecuteReader(dbCommand);
                if (reader.Read())
                {
                    return reader["NumberOfPackagesUnitCode"].ToString();
                }
                return string.Empty;
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }

        public static List<VNACC_Category_PackagesUnit> SelectCollectionBy(List<VNACC_Category_PackagesUnit> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_PackagesUnit o)
            {
                return o.TableID.Contains(tableID);
            });
        }
	}	
}