﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl = Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl = Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01 = Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl = Company.Interface.Controls.NguyenTeControl;
using NuocControl = Company.Interface.Controls.NuocControl;

namespace Company.Interface
{
    partial class ToKhaiMauDichForm
    {
        private Panel pnlToKhaiMauDich;
        private NumericEditBox txtTrongLuong;
        private NumericEditBox txtSoKien;
        private NumericEditBox txtTongSoContainer;
        private EditBox txtTenChuHang;
        private Label label29;
        private Label label31;
        private Label label21;
        private Label label22;
        private UIGroupBox grbNguyenTe;
        private Label label9;
        private Label label30;
        private UIGroupBox grbNuocXK;
        private UIGroupBox uiGroupBox6;
        private EditBox txtTenDaiLy;
        private EditBox txtMaDaiLy;
        private Label label7;
        private Label label8;
        private UIGroupBox uiGroupBox5;
        private EditBox txtMaDonViUyThac;
        private Label label5;
        private Label label6;
        private EditBox txtTenDonViUyThac;
        private UIGroupBox grbNguoiXK;
        private EditBox txtTenDonViDoiTac;
        private UIGroupBox uiGroupBox7;
        private CalendarCombo ccNgayHHHopDong;
        private EditBox txtSoHopDong;
        private Label label13;
        private Label label14;
        private Label label34;
        private CalendarCombo ccNgayHopDong;
        private UIGroupBox uiGroupBox18;
        private UIGroupBox uiGroupBox16;
        private UIGroupBox grbDiaDiemDoHang;
        private EditBox txtDiaDiemXepHang;
        private CalendarCombo ccNgayVanTaiDon;
        private EditBox txtSoVanTaiDon;
        private Label label19;
        private Label label20;
        private UIGroupBox uiGroupBox11;
        private UIComboBox cbPTVT;
        private Label label17;
        private Label lblSoHieuPTVT;
        private EditBox txtSoHieuPTVT;
        private Label lblNgayDenPTVT;
        private EditBox txtSoHoaDonThuongMai;
        private Label label15;
        private Label label16;
        private CalendarCombo ccNgayHHGiayPhep;
        private EditBox txtSoGiayPhep;
        private Label label11;
        private Label label12;
        private Label label33;
        private CalendarCombo ccNgayGiayPhep;
        private UIGroupBox grbHoaDonThuongMai;
        private UIGroupBox grbHopDong;
        private UIGroupBox grbDiaDiemXepHang;
        private UIGroupBox grbVanTaiDon;
        private UIGroupBox gbGiayPhep;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvSoHieuPTVT;
        private Label label1;
        private Label label2;
        private NumericEditBox txtLePhiHQ;
        private NumericEditBox txtPhiVanChuyen;
        private NumericEditBox txtPhiBaoHiem;
        private Label label4;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private Label label23;
        private NumericEditBox txtSoLuongPLTK;
        private UIGroupBox grbNguoiNK;
        private EditBox txtTenDonVi;
        private EditBox txtMaDonVi;
        private CalendarCombo ccNgayDen;
        private CalendarCombo ccNgayHDTM;
        private RequiredFieldValidator rfvNgayDen;
        private RequiredFieldValidator rfvNguoiXuatKhau;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UICommand cmdSave1;
        private UICommandBar cmbToolBar;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgToKhaiTriGia_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dglistTKTK23_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.radHangMau = new Janus.Windows.EditControls.UIRadioButton();
            this.uiDamBaoNghiaVuNT = new Janus.Windows.EditControls.UIGroupBox();
            this.ccDamBaoNopThue_NgayKetThuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccDamBaoNopThue_NgayBatDau = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDamBaoNopThue_HinhThuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkDamBaoNopThue_isValue = new Janus.Windows.EditControls.UICheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtDamBaoNopThue_TriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.radTB = new Janus.Windows.EditControls.UIRadioButton();
            this.uiAnHanThue = new Janus.Windows.EditControls.UIGroupBox();
            this.txtAnHanThue_LyDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkAnHangThue = new Janus.Windows.EditControls.UICheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtAnHanThue_ThoiGian = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.radNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.radSP = new Janus.Windows.EditControls.UIRadioButton();
            this.lblMaMid = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiNganHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grpLoaiHangHoa = new Janus.Windows.EditControls.UIGroupBox();
            this.grbNguoiNK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbDiaDiemXepHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLePhiHQ = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoContainer = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLyDoSua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDeXuatKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtChucVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.grbNguyenTe = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbNuocXK = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatKhau = new Company.Interface.Controls.NuocControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiXK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrLoaiHinhMauDich = new Company.Interface.Controls.LoaiHinhMauDichVControl();
            this.grbHopDong = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ccNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbDiaDiemDoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhau = new Company.Interface.Controls.CuaKhauControl();
            this.grbVanTaiDon = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayVanTaiDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoVanTaiDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblSoHieuPTVT = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayDenPTVT = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ccNgayGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.grbHoaDonThuongMai = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHDTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHoaDonThuongMai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnNoiDungChinhSua = new Janus.Windows.EditControls.UIButton();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvSoHieuPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayDuaVaoTK = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtSoToKhaiGiay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHDPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.ccNgayTN = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label28 = new System.Windows.Forms.Label();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.lblPhan = new System.Windows.Forms.Label();
            this.ccNgayDK = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayDangKy = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoTiepNhan = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoLuongPLTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.TKGT = new Janus.Windows.UI.Dock.UIPanel();
            this.TKGTContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgToKhaiTriGia = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.TKTG23 = new Janus.Windows.UI.Dock.UIPanel();
            this.TKTG23Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dglistTKTK23 = new Janus.Windows.GridEX.GridEX();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiXuatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.ThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdNPLCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.cmdChungTuDinhKem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDinhKem");
            this.cmdTruyenDuLieuTuXa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdXuatExcelHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatExcelHang");
            this.cmdPrint2 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.CanDoiNhapXuat1 = new Janus.Windows.UI.CommandBars.UICommand("CanDoiNhapXuat");
            this.cmdPhuKienToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhuKienToKhai");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdTinhLaiThue = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhLaiThue");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdInTKTT151 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTT15");
            this.cmdInTKTCTT1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTCTT196");
            this.cmdInTKDTSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdContainer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdAnDinhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAnDinhThue");
            this.cmdBangKeHangHoa_HD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeHangHoa_HD");
            this.cmdInPhieuKetQuaKiemTra1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInPhieuKetQuaKiemTra");
            this.cmdInPhieuKiemTraHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInPhieuKiemTraHangHoa");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.ChungTuKemTheo = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.ThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdNhieuHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhieuHang");
            this.FileDinhKem = new Janus.Windows.UI.CommandBars.UICommand("FileDinhKem");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.CanDoiNhapXuat = new Janus.Windows.UI.CommandBars.UICommand("CanDoiNhapXuat");
            this.cmdNhieuHang = new Janus.Windows.UI.CommandBars.UICommand("cmdNhieuHang");
            this.PhanBo = new Janus.Windows.UI.CommandBars.UICommand("PhanBo");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhenTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdXuatExcelHang = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatExcelHang");
            this.cmdInTKA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKA4");
            this.cmdChungTuDinhKem = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDinhKem");
            this.cmdVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDon");
            this.cmdGiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDong");
            this.cmdHoaDonThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdCO1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCO");
            this.cmDeNghiChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmDeNghiChuyenCuaKhau");
            this.cmdChungTuDangAnh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdChungTuNo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.GiayKiemTra1 = new Janus.Windows.UI.CommandBars.UICommand("GiayKiemTra");
            this.ChungThuGiamDinh1 = new Janus.Windows.UI.CommandBars.UICommand("ChungThuGiamDinh");
            this.cmdVanDon = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDon");
            this.cmdHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDong");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHoaDonThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdCO = new Janus.Windows.UI.CommandBars.UICommand("cmdCO");
            this.cmDeNghiChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmDeNghiChuyenCuaKhau");
            this.cmdTruyenDuLieuTuXa = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdSendTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendTK");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan1 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSuaToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdLayPhanHoiLai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoiLai");
            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdSuaToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdFileAnh = new Janus.Windows.UI.CommandBars.UICommand("cmdFileAnh");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdChungTuDangAnh = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdInTKDTSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdInAnDinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdInAnDinhThue");
            this.cmdChungTuNo = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.cmdNPLCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdInTKTT15 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTT15");
            this.cmdToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.ToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiTriGia");
            this.cmdToKhaiTGPP21 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP2");
            this.cmdToKhaiTGPP31 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP3");
            this.cmdToKhaiTGPP2 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP2");
            this.cmdToKhaiTGPP3 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP3");
            this.ToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiTriGia");
            this.cmdGiayNopTien = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayNopTien");
            this.cmdContainer = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.GiayKiemTra = new Janus.Windows.UI.CommandBars.UICommand("GiayKiemTra");
            this.ChungThuGiamDinh = new Janus.Windows.UI.CommandBars.UICommand("ChungThuGiamDinh");
            this.cmdInTKTCTT196 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTCTT196");
            this.cmdBangKeHangHoa_HD = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeHangHoa_HD");
            this.cmdInPhieuKetQuaKiemTra = new Janus.Windows.UI.CommandBars.UICommand("cmdInPhieuKetQuaKiemTra");
            this.cmdInPhieuKiemTraHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdInPhieuKiemTraHangHoa");
            this.cmdLayPhanHoiLai = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoiLai");
            this.cmdPhanHoiSoTiepNhan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiSoTiepNhan");
            this.cmdPhanHoiSoToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiSoToKhai");
            this.cmdPhanHoiPhanLuong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiPhanLuong");
            this.cmdPhanHoiSoTiepNhan = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiSoTiepNhan");
            this.cmdPhanHoiSoToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiSoToKhai");
            this.cmdPhanHoiPhanLuong = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoiPhanLuong");
            this.cmdPhuKienToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdPhuKienToKhai");
            this.cmdSendTK = new Janus.Windows.UI.CommandBars.UICommand("cmdSendTK");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdReadExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo2 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.rvSKH = new Company.Controls.CustomValidation.RangeValidator();
            this.rVTrongLuong = new Company.Controls.CustomValidation.RangeValidator();
            this.rVTrongLuongTinh = new Company.Controls.CustomValidation.RangeValidator();
            this.rvLPHQ = new Company.Controls.CustomValidation.RangeValidator();
            this.rVPBH = new Company.Controls.CustomValidation.RangeValidator();
            this.rVPVC = new Company.Controls.CustomValidation.RangeValidator();
            this.rvPhiKHac = new Company.Controls.CustomValidation.RangeValidator();
            this.rvSLPLTK = new Company.Controls.CustomValidation.RangeValidator();
            this.gridEXExporterHangToKhai = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.rfvHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDamBaoNghiaVuNT)).BeginInit();
            this.uiDamBaoNghiaVuNT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnHanThue)).BeginInit();
            this.uiAnHanThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).BeginInit();
            this.grbNguoiNK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).BeginInit();
            this.grbDiaDiemXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).BeginInit();
            this.grbNguyenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).BeginInit();
            this.grbNuocXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).BeginInit();
            this.grbNguoiXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).BeginInit();
            this.grbHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).BeginInit();
            this.grbDiaDiemDoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).BeginInit();
            this.grbVanTaiDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).BeginInit();
            this.grbHoaDonThuongMai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGT)).BeginInit();
            this.TKGT.SuspendLayout();
            this.TKGTContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKTG23)).BeginInit();
            this.TKTG23.SuspendLayout();
            this.TKTG23Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTK23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rvSKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVTrongLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVTrongLuongTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLPHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVPBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVPVC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvPhiKHac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvSLPLTK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvHopDong)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 37);
            this.grbMain.Size = new System.Drawing.Size(1178, 695);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2007";
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.radHangMau);
            this.pnlToKhaiMauDich.Controls.Add(this.uiDamBaoNghiaVuNT);
            this.pnlToKhaiMauDich.Controls.Add(this.radTB);
            this.pnlToKhaiMauDich.Controls.Add(this.uiAnHanThue);
            this.pnlToKhaiMauDich.Controls.Add(this.radNPL);
            this.pnlToKhaiMauDich.Controls.Add(this.txtMaMid);
            this.pnlToKhaiMauDich.Controls.Add(this.radSP);
            this.pnlToKhaiMauDich.Controls.Add(this.lblMaMid);
            this.pnlToKhaiMauDich.Controls.Add(this.label25);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuongTinh);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiNganHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label24);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox4);
            this.pnlToKhaiMauDich.Controls.Add(this.grpLoaiHangHoa);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiNK);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiBaoHiem);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemXepHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label4);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiVanChuyen);
            this.pnlToKhaiMauDich.Controls.Add(this.label2);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLePhiHQ);
            this.pnlToKhaiMauDich.Controls.Add(this.label1);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuong);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoKien);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTongSoContainer);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLyDoSua);
            this.pnlToKhaiMauDich.Controls.Add(this.label26);
            this.pnlToKhaiMauDich.Controls.Add(this.txtDeXuatKhac);
            this.pnlToKhaiMauDich.Controls.Add(this.label35);
            this.pnlToKhaiMauDich.Controls.Add(this.label3);
            this.pnlToKhaiMauDich.Controls.Add(this.txtChucVu);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTenChuHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label27);
            this.pnlToKhaiMauDich.Controls.Add(this.label29);
            this.pnlToKhaiMauDich.Controls.Add(this.label31);
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.label22);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguyenTe);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNuocXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox6);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox5);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox7);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDong);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox18);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox16);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemDoHang);
            this.pnlToKhaiMauDich.Controls.Add(this.grbVanTaiDon);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox11);
            this.pnlToKhaiMauDich.Controls.Add(this.gbGiayPhep);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHoaDonThuongMai);
            this.pnlToKhaiMauDich.Controls.Add(this.btnNoiDungChinhSua);
            this.pnlToKhaiMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(3, 98);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(1172, 591);
            this.pnlToKhaiMauDich.TabIndex = 10;
            // 
            // radHangMau
            // 
            this.radHangMau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radHangMau.Location = new System.Drawing.Point(825, 683);
            this.radHangMau.Name = "radHangMau";
            this.radHangMau.Size = new System.Drawing.Size(90, 17);
            this.radHangMau.TabIndex = 48;
            this.radHangMau.Text = "Hàng mẫu";
            this.radHangMau.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.radHangMau.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // uiDamBaoNghiaVuNT
            // 
            this.uiDamBaoNghiaVuNT.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.uiDamBaoNghiaVuNT.Controls.Add(this.ccDamBaoNopThue_NgayKetThuc);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.ccDamBaoNopThue_NgayBatDau);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.txtDamBaoNopThue_HinhThuc);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.chkDamBaoNopThue_isValue);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label38);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label41);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label40);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label39);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.txtDamBaoNopThue_TriGia);
            this.uiDamBaoNghiaVuNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiDamBaoNghiaVuNT.Location = new System.Drawing.Point(381, 582);
            this.uiDamBaoNghiaVuNT.Name = "uiDamBaoNghiaVuNT";
            this.uiDamBaoNghiaVuNT.Size = new System.Drawing.Size(432, 130);
            this.uiDamBaoNghiaVuNT.TabIndex = 278;
            this.uiDamBaoNghiaVuNT.Text = "      Doanh nghiệp được đảm bảo nghĩa vụ nộp thuế";
            this.uiDamBaoNghiaVuNT.TextOffset = 7;
            this.uiDamBaoNghiaVuNT.UseCompatibleTextRendering = true;
            this.uiDamBaoNghiaVuNT.VisualStyleManager = this.vsmMain;
            // 
            // ccDamBaoNopThue_NgayKetThuc
            // 
            // 
            // 
            // 
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.Name = "";
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccDamBaoNopThue_NgayKetThuc.Enabled = false;
            this.ccDamBaoNopThue_NgayKetThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccDamBaoNopThue_NgayKetThuc.IsNullDate = true;
            this.ccDamBaoNopThue_NgayKetThuc.Location = new System.Drawing.Point(246, 78);
            this.ccDamBaoNopThue_NgayKetThuc.Name = "ccDamBaoNopThue_NgayKetThuc";
            this.ccDamBaoNopThue_NgayKetThuc.Nullable = true;
            this.ccDamBaoNopThue_NgayKetThuc.NullButtonText = "Xóa";
            this.ccDamBaoNopThue_NgayKetThuc.ShowNullButton = true;
            this.ccDamBaoNopThue_NgayKetThuc.Size = new System.Drawing.Size(130, 21);
            this.ccDamBaoNopThue_NgayKetThuc.TabIndex = 40;
            this.ccDamBaoNopThue_NgayKetThuc.TodayButtonText = "Hôm nay";
            this.ccDamBaoNopThue_NgayKetThuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccDamBaoNopThue_NgayBatDau
            // 
            // 
            // 
            // 
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.Name = "";
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccDamBaoNopThue_NgayBatDau.Enabled = false;
            this.ccDamBaoNopThue_NgayBatDau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccDamBaoNopThue_NgayBatDau.IsNullDate = true;
            this.ccDamBaoNopThue_NgayBatDau.Location = new System.Drawing.Point(246, 38);
            this.ccDamBaoNopThue_NgayBatDau.Name = "ccDamBaoNopThue_NgayBatDau";
            this.ccDamBaoNopThue_NgayBatDau.Nullable = true;
            this.ccDamBaoNopThue_NgayBatDau.NullButtonText = "Xóa";
            this.ccDamBaoNopThue_NgayBatDau.ShowNullButton = true;
            this.ccDamBaoNopThue_NgayBatDau.Size = new System.Drawing.Size(131, 21);
            this.ccDamBaoNopThue_NgayBatDau.TabIndex = 40;
            this.ccDamBaoNopThue_NgayBatDau.TodayButtonText = "Hôm nay";
            this.ccDamBaoNopThue_NgayBatDau.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtDamBaoNopThue_HinhThuc
            // 
            this.txtDamBaoNopThue_HinhThuc.Enabled = false;
            this.txtDamBaoNopThue_HinhThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDamBaoNopThue_HinhThuc.Location = new System.Drawing.Point(12, 78);
            this.txtDamBaoNopThue_HinhThuc.MaxLength = 255;
            this.txtDamBaoNopThue_HinhThuc.Multiline = true;
            this.txtDamBaoNopThue_HinhThuc.Name = "txtDamBaoNopThue_HinhThuc";
            this.txtDamBaoNopThue_HinhThuc.Size = new System.Drawing.Size(221, 48);
            this.txtDamBaoNopThue_HinhThuc.TabIndex = 3;
            this.txtDamBaoNopThue_HinhThuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDamBaoNopThue_HinhThuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkDamBaoNopThue_isValue
            // 
            this.chkDamBaoNopThue_isValue.BackColor = System.Drawing.Color.Transparent;
            this.chkDamBaoNopThue_isValue.Location = new System.Drawing.Point(11, -1);
            this.chkDamBaoNopThue_isValue.Name = "chkDamBaoNopThue_isValue";
            this.chkDamBaoNopThue_isValue.Size = new System.Drawing.Size(17, 18);
            this.chkDamBaoNopThue_isValue.TabIndex = 39;
            this.chkDamBaoNopThue_isValue.CheckedChanged += new System.EventHandler(this.chkDamBaoNopThue_isValue_CheckedChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(9, 62);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(97, 13);
            this.label38.TabIndex = 17;
            this.label38.Text = "Hình thức đảm bảo";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(242, 62);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(118, 13);
            this.label41.TabIndex = 17;
            this.label41.Text = "Ngày kết thúc đảm bảo";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(242, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(116, 13);
            this.label40.TabIndex = 17;
            this.label40.Text = "Ngày bắt đầu đảm bảo";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(9, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 13);
            this.label39.TabIndex = 17;
            this.label39.Text = "Trị giá đảm bảo";
            // 
            // txtDamBaoNopThue_TriGia
            // 
            this.txtDamBaoNopThue_TriGia.DecimalDigits = 4;
            this.txtDamBaoNopThue_TriGia.Enabled = false;
            this.txtDamBaoNopThue_TriGia.Location = new System.Drawing.Point(12, 38);
            this.txtDamBaoNopThue_TriGia.MaxLength = 10;
            this.txtDamBaoNopThue_TriGia.Name = "txtDamBaoNopThue_TriGia";
            this.txtDamBaoNopThue_TriGia.Size = new System.Drawing.Size(221, 21);
            this.txtDamBaoNopThue_TriGia.TabIndex = 20;
            this.txtDamBaoNopThue_TriGia.Tag = "SoContainer20";
            this.txtDamBaoNopThue_TriGia.Text = "0.0000";
            this.txtDamBaoNopThue_TriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDamBaoNopThue_TriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtDamBaoNopThue_TriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // radTB
            // 
            this.radTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTB.Location = new System.Drawing.Point(825, 653);
            this.radTB.Name = "radTB";
            this.radTB.Size = new System.Drawing.Size(78, 17);
            this.radTB.TabIndex = 48;
            this.radTB.Text = "Thiết bị";
            this.radTB.VisualStyleManager = this.vsmMain;
            this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // uiAnHanThue
            // 
            this.uiAnHanThue.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.uiAnHanThue.Controls.Add(this.txtAnHanThue_LyDo);
            this.uiAnHanThue.Controls.Add(this.chkAnHangThue);
            this.uiAnHanThue.Controls.Add(this.label37);
            this.uiAnHanThue.Controls.Add(this.label45);
            this.uiAnHanThue.Controls.Add(this.txtAnHanThue_ThoiGian);
            this.uiAnHanThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiAnHanThue.Location = new System.Drawing.Point(8, 582);
            this.uiAnHanThue.Name = "uiAnHanThue";
            this.uiAnHanThue.Size = new System.Drawing.Size(371, 130);
            this.uiAnHanThue.TabIndex = 277;
            this.uiAnHanThue.Text = "      Tờ khai được ân hạn thuế";
            this.uiAnHanThue.TextOffset = 7;
            this.uiAnHanThue.UseCompatibleTextRendering = true;
            this.uiAnHanThue.VisualStyleManager = this.vsmMain;
            // 
            // txtAnHanThue_LyDo
            // 
            this.txtAnHanThue_LyDo.Enabled = false;
            this.txtAnHanThue_LyDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnHanThue_LyDo.Location = new System.Drawing.Point(44, 78);
            this.txtAnHanThue_LyDo.MaxLength = 255;
            this.txtAnHanThue_LyDo.Multiline = true;
            this.txtAnHanThue_LyDo.Name = "txtAnHanThue_LyDo";
            this.txtAnHanThue_LyDo.Size = new System.Drawing.Size(314, 48);
            this.txtAnHanThue_LyDo.TabIndex = 3;
            this.txtAnHanThue_LyDo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtAnHanThue_LyDo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkAnHangThue
            // 
            this.chkAnHangThue.BackColor = System.Drawing.Color.Transparent;
            this.chkAnHangThue.Location = new System.Drawing.Point(12, -2);
            this.chkAnHangThue.Name = "chkAnHangThue";
            this.chkAnHangThue.Size = new System.Drawing.Size(17, 18);
            this.chkAnHangThue.TabIndex = 39;
            this.chkAnHangThue.CheckedChanged += new System.EventHandler(this.chkAnHangThue_CheckedChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 62);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(96, 13);
            this.label37.TabIndex = 17;
            this.label37.Text = "Lý do được ân hạn";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(9, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(122, 13);
            this.label45.TabIndex = 17;
            this.label45.Text = "Thời gian ân hạn (Ngày)";
            // 
            // txtAnHanThue_ThoiGian
            // 
            this.txtAnHanThue_ThoiGian.DecimalDigits = 0;
            this.txtAnHanThue_ThoiGian.Enabled = false;
            this.txtAnHanThue_ThoiGian.Location = new System.Drawing.Point(44, 38);
            this.txtAnHanThue_ThoiGian.MaxLength = 10;
            this.txtAnHanThue_ThoiGian.Name = "txtAnHanThue_ThoiGian";
            this.txtAnHanThue_ThoiGian.Size = new System.Drawing.Size(313, 21);
            this.txtAnHanThue_ThoiGian.TabIndex = 20;
            this.txtAnHanThue_ThoiGian.Tag = "SoContainer20";
            this.txtAnHanThue_ThoiGian.Text = "0";
            this.txtAnHanThue_ThoiGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtAnHanThue_ThoiGian.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAnHanThue_ThoiGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // radNPL
            // 
            this.radNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNPL.Location = new System.Drawing.Point(825, 623);
            this.radNPL.Name = "radNPL";
            this.radNPL.Size = new System.Drawing.Size(124, 17);
            this.radNPL.TabIndex = 47;
            this.radNPL.Text = "Nguyên phụ liệu";
            this.radNPL.VisualStyleManager = this.vsmMain;
            this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // txtMaMid
            // 
            this.txtMaMid.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMid.Location = new System.Drawing.Point(839, 469);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.ReadOnly = true;
            this.txtMaMid.Size = new System.Drawing.Size(95, 21);
            this.txtMaMid.TabIndex = 45;
            this.txtMaMid.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaMid.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // radSP
            // 
            this.radSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radSP.Checked = true;
            this.radSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSP.Location = new System.Drawing.Point(825, 594);
            this.radSP.Name = "radSP";
            this.radSP.Size = new System.Drawing.Size(89, 17);
            this.radSP.TabIndex = 46;
            this.radSP.TabStop = true;
            this.radSP.Text = "Sản phẩm";
            this.radSP.VisualStyleManager = this.vsmMain;
            this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // lblMaMid
            // 
            this.lblMaMid.AutoSize = true;
            this.lblMaMid.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaMid.Location = new System.Drawing.Point(836, 453);
            this.lblMaMid.Name = "lblMaMid";
            this.lblMaMid.Size = new System.Drawing.Size(40, 13);
            this.lblMaMid.TabIndex = 37;
            this.lblMaMid.Text = "Mã Mid";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(727, 413);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "Trọng lượng tịnh";
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 4;
            this.txtTrongLuongTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(730, 429);
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(95, 21);
            this.txtTrongLuongTinh.TabIndex = 40;
            this.txtTrongLuongTinh.Tag = "TrongLuongTinh";
            this.txtTrongLuongTinh.Text = "0.0000";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiNganHang
            // 
            this.txtPhiNganHang.DecimalDigits = 2;
            this.txtPhiNganHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiNganHang.Location = new System.Drawing.Point(731, 469);
            this.txtPhiNganHang.Name = "txtPhiNganHang";
            this.txtPhiNganHang.Size = new System.Drawing.Size(96, 21);
            this.txtPhiNganHang.TabIndex = 44;
            this.txtPhiNganHang.Tag = "PhiKhac";
            this.txtPhiNganHang.Text = "0.00";
            this.txtPhiNganHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiNganHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiNganHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(731, 453);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Phí khác";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtChungTu);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(739, 303);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(235, 104);
            this.uiGroupBox4.TabIndex = 16;
            this.uiGroupBox4.Text = "Chứng từ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtChungTu
            // 
            this.txtChungTu.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungTu.Location = new System.Drawing.Point(8, 20);
            this.txtChungTu.MaxLength = 40;
            this.txtChungTu.Multiline = true;
            this.txtChungTu.Name = "txtChungTu";
            this.txtChungTu.Size = new System.Drawing.Size(202, 75);
            this.txtChungTu.TabIndex = 34;
            this.txtChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChungTu.ButtonClick += new System.EventHandler(this.txtChungTu_ButtonClick);
            // 
            // grpLoaiHangHoa
            // 
            this.grpLoaiHangHoa.BorderColor = System.Drawing.Color.Transparent;
            this.grpLoaiHangHoa.Location = new System.Drawing.Point(8, 736);
            this.grpLoaiHangHoa.Name = "grpLoaiHangHoa";
            this.grpLoaiHangHoa.Size = new System.Drawing.Size(508, 26);
            this.grpLoaiHangHoa.TabIndex = 39;
            this.grpLoaiHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiNK
            // 
            this.grbNguoiNK.Controls.Add(this.txtTenDonVi);
            this.grbNguoiNK.Controls.Add(this.txtMaDonVi);
            this.grbNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNK.Location = new System.Drawing.Point(12, 8);
            this.grbNguoiNK.Name = "grbNguoiNK";
            this.grbNguoiNK.Size = new System.Drawing.Size(247, 104);
            this.grbNguoiNK.TabIndex = 0;
            this.grbNguoiNK.Text = "Người nhập khẩu";
            this.grbNguoiNK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(8, 48);
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.ReadOnly = true;
            this.txtTenDonVi.Size = new System.Drawing.Size(222, 48);
            this.txtTenDonVi.TabIndex = 6;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(8, 20);
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.ReadOnly = true;
            this.txtMaDonVi.Size = new System.Drawing.Size(222, 21);
            this.txtMaDonVi.TabIndex = 5;
            this.txtMaDonVi.Text = "0400100457";
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 2;
            this.txtPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(518, 469);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(96, 21);
            this.txtPhiBaoHiem.TabIndex = 42;
            this.txtPhiBaoHiem.Tag = "LePhiBaoHiem";
            this.txtPhiBaoHiem.Text = "0.00";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // grbDiaDiemXepHang
            // 
            this.grbDiaDiemXepHang.Controls.Add(this.txtDiaDiemXepHang);
            this.grbDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemXepHang.Location = new System.Drawing.Point(739, 220);
            this.grbDiaDiemXepHang.Name = "grbDiaDiemXepHang";
            this.grbDiaDiemXepHang.Size = new System.Drawing.Size(231, 80);
            this.grbDiaDiemXepHang.TabIndex = 11;
            this.grbDiaDiemXepHang.Tag = "CangXepHang";
            this.grbDiaDiemXepHang.Text = "Địa điểm xếp hàng";
            this.grbDiaDiemXepHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtDiaDiemXepHang
            // 
            this.txtDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemXepHang.Location = new System.Drawing.Point(8, 24);
            this.txtDiaDiemXepHang.Multiline = true;
            this.txtDiaDiemXepHang.Name = "txtDiaDiemXepHang";
            this.txtDiaDiemXepHang.Size = new System.Drawing.Size(202, 48);
            this.txtDiaDiemXepHang.TabIndex = 26;
            this.txtDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(518, 453);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Phí bảo hiểm";
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 2;
            this.txtPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(626, 469);
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(92, 21);
            this.txtPhiVanChuyen.TabIndex = 43;
            this.txtPhiVanChuyen.Tag = "PhiVanChuyen";
            this.txtPhiVanChuyen.Text = "0.00";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(626, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Phí vận chuyển";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtLePhiHQ
            // 
            this.txtLePhiHQ.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtLePhiHQ.DecimalDigits = 2;
            this.txtLePhiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLePhiHQ.Location = new System.Drawing.Point(270, 469);
            this.txtLePhiHQ.Name = "txtLePhiHQ";
            this.txtLePhiHQ.Size = new System.Drawing.Size(96, 21);
            this.txtLePhiHQ.TabIndex = 41;
            this.txtLePhiHQ.Tag = "LePhiHQ";
            this.txtLePhiHQ.Text = "0.00";
            this.txtLePhiHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLePhiHQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtLePhiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLePhiHQ.Click += new System.EventHandler(this.txtLePhiHQ_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(270, 453);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Lệ phí HQ";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 4;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.Location = new System.Drawing.Point(626, 429);
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(92, 21);
            this.txtTrongLuong.TabIndex = 39;
            this.txtTrongLuong.Tag = "TrongLuong";
            this.txtTrongLuong.Text = "0.0000";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 0;
            this.txtSoKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKien.Location = new System.Drawing.Point(518, 429);
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(96, 21);
            this.txtSoKien.TabIndex = 38;
            this.txtSoKien.Tag = "SoKienHang";
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = 0;
            this.txtSoKien.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoContainer
            // 
            this.txtTongSoContainer.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTongSoContainer.DecimalDigits = 0;
            this.txtTongSoContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoContainer.Location = new System.Drawing.Point(269, 429);
            this.txtTongSoContainer.Name = "txtTongSoContainer";
            this.txtTongSoContainer.ReadOnly = true;
            this.txtTongSoContainer.Size = new System.Drawing.Size(96, 21);
            this.txtTongSoContainer.TabIndex = 36;
            this.txtTongSoContainer.Tag = "SoContainer20";
            this.txtTongSoContainer.Text = "0";
            this.txtTongSoContainer.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTongSoContainer.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoContainer.Click += new System.EventHandler(this.txtTongSoContainer_Click);
            // 
            // txtLyDoSua
            // 
            this.txtLyDoSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDoSua.Location = new System.Drawing.Point(510, 509);
            this.txtLyDoSua.Multiline = true;
            this.txtLyDoSua.Name = "txtLyDoSua";
            this.txtLyDoSua.Size = new System.Drawing.Size(388, 70);
            this.txtLyDoSua.TabIndex = 35;
            this.txtLyDoSua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDoSua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(510, 510);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Lý do sửa";
            // 
            // txtDeXuatKhac
            // 
            this.txtDeXuatKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeXuatKhac.Location = new System.Drawing.Point(8, 506);
            this.txtDeXuatKhac.Multiline = true;
            this.txtDeXuatKhac.Name = "txtDeXuatKhac";
            this.txtDeXuatKhac.Size = new System.Drawing.Size(490, 70);
            this.txtDeXuatKhac.TabIndex = 35;
            this.txtDeXuatKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDeXuatKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(518, 493);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Lý do sửa: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 490);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Đề xuất khác";
            // 
            // txtChucVu
            // 
            this.txtChucVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVu.Location = new System.Drawing.Point(52, 468);
            this.txtChucVu.Name = "txtChucVu";
            this.txtChucVu.Size = new System.Drawing.Size(190, 21);
            this.txtChucVu.TabIndex = 35;
            this.txtChucVu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChucVu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(52, 429);
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(190, 21);
            this.txtTenChuHang.TabIndex = 35;
            this.txtTenChuHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(9, 450);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(117, 13);
            this.label27.TabIndex = 17;
            this.label27.Text = "Chức vụ người đại diện";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(9, 410);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(114, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "Đại diện doanh nghiệp";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(269, 413);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 13);
            this.label31.TabIndex = 19;
            this.label31.Text = "Số container";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(517, 413);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Số kiện hàng";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(619, 413);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Trọng lượng (Kg)";
            // 
            // grbNguyenTe
            // 
            this.grbNguyenTe.Controls.Add(this.ctrNguyenTe);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaTinhThue);
            this.grbNguyenTe.Controls.Add(this.label9);
            this.grbNguyenTe.Controls.Add(this.label30);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaUSD);
            this.grbNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguyenTe.Location = new System.Drawing.Point(504, 303);
            this.grbNguyenTe.Name = "grbNguyenTe";
            this.grbNguyenTe.Size = new System.Drawing.Size(229, 104);
            this.grbNguyenTe.TabIndex = 15;
            this.grbNguyenTe.Text = "Đồng tiền thanh toán";
            this.grbNguyenTe.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(8, 20);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(200, 22);
            this.ctrNguyenTe.TabIndex = 31;
            this.ctrNguyenTe.Tag = "DongTienThanhToan";
            this.ctrNguyenTe.VisualStyleManager = null;
            this.ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.CtrNguyenTeValueChanged);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 2;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(129, 21);
            this.txtTyGiaTinhThue.TabIndex = 32;
            this.txtTyGiaTinhThue.Tag = "TyGiaTinhThue";
            this.txtTyGiaTinhThue.Text = "0.00";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Tỷ giá USD";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Tỷ giá TT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(129, 21);
            this.txtTyGiaUSD.TabIndex = 33;
            this.txtTyGiaUSD.Text = "0";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // grbNuocXK
            // 
            this.grbNuocXK.Controls.Add(this.ctrNuocXuatKhau);
            this.grbNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNuocXK.Location = new System.Drawing.Point(265, 220);
            this.grbNuocXK.Name = "grbNuocXK";
            this.grbNuocXK.Size = new System.Drawing.Size(233, 80);
            this.grbNuocXK.TabIndex = 9;
            this.grbNuocXK.Text = "Nước xuất khẩu";
            this.grbNuocXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrNuocXuatKhau
            // 
            this.ctrNuocXuatKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatKhau.ErrorMessage = "\"Nước xuất khẩu\" bắt buộc phải chọn.";
            this.ctrNuocXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuatKhau.Location = new System.Drawing.Point(7, 24);
            this.ctrNuocXuatKhau.Ma = "";
            this.ctrNuocXuatKhau.Name = "ctrNuocXuatKhau";
            this.ctrNuocXuatKhau.ReadOnly = false;
            this.ctrNuocXuatKhau.Size = new System.Drawing.Size(220, 50);
            this.ctrNuocXuatKhau.TabIndex = 24;
            this.ctrNuocXuatKhau.Tag = "NuocXK";
            this.ctrNuocXuatKhau.VisualStyleManager = null;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtTenDaiLy);
            this.uiGroupBox6.Controls.Add(this.txtMaDaiLy);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(12, 303);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(247, 104);
            this.uiGroupBox6.TabIndex = 12;
            this.uiGroupBox6.Text = "Đại lý làm TTHQ";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Location = new System.Drawing.Point(40, 47);
            this.txtTenDaiLy.Multiline = true;
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Size = new System.Drawing.Size(190, 48);
            this.txtTenDaiLy.TabIndex = 28;
            this.txtTenDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Location = new System.Drawing.Point(40, 20);
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Size = new System.Drawing.Size(190, 21);
            this.txtMaDaiLy.TabIndex = 27;
            this.txtMaDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 0;
            this.label7.Tag = "Ma";
            this.label7.Text = "Mã";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Tag = "Ten";
            this.label8.Text = "Tên";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtMaDonViUyThac);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 220);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(247, 80);
            this.uiGroupBox5.TabIndex = 8;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(40, 20);
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(190, 21);
            this.txtMaDonViUyThac.TabIndex = 22;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 2;
            this.label6.Tag = "TenDVUT";
            this.label6.Text = "Tên";
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(40, 47);
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(190, 21);
            this.txtTenDonViUyThac.TabIndex = 23;
            this.txtTenDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // grbNguoiXK
            // 
            this.grbNguoiXK.Controls.Add(this.txtTenDonViDoiTac);
            this.grbNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiXK.Location = new System.Drawing.Point(12, 114);
            this.grbNguoiXK.Name = "grbNguoiXK";
            this.grbNguoiXK.Size = new System.Drawing.Size(247, 104);
            this.grbNguoiXK.TabIndex = 4;
            this.grbNguoiXK.Tag = "NguoiXuatKhau";
            this.grbNguoiXK.Text = "Người xuất khẩu";
            this.grbNguoiXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDonViDoiTac
            // 
            this.txtTenDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViDoiTac.Location = new System.Drawing.Point(8, 20);
            this.txtTenDonViDoiTac.Multiline = true;
            this.txtTenDonViDoiTac.Name = "txtTenDonViDoiTac";
            this.txtTenDonViDoiTac.Size = new System.Drawing.Size(222, 78);
            this.txtTenDonViDoiTac.TabIndex = 14;
            this.txtTenDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonViDoiTac.Leave += new System.EventHandler(this.txtTenDonViDoiTac_Leave);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrLoaiHinhMauDich);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(265, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(233, 104);
            this.uiGroupBox7.TabIndex = 1;
            this.uiGroupBox7.Text = "Loại hình mậu dịch";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrLoaiHinhMauDich
            // 
            this.ctrLoaiHinhMauDich.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhMauDich.Location = new System.Drawing.Point(7, 24);
            this.ctrLoaiHinhMauDich.Ma = "";
            this.ctrLoaiHinhMauDich.Name = "ctrLoaiHinhMauDich";
            this.ctrLoaiHinhMauDich.Nhom = "\"N\"";
            this.ctrLoaiHinhMauDich.ReadOnly = false;
            this.ctrLoaiHinhMauDich.Size = new System.Drawing.Size(220, 50);
            this.ctrLoaiHinhMauDich.TabIndex = 7;
            this.ctrLoaiHinhMauDich.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDong
            // 
            this.grbHopDong.Controls.Add(this.ccNgayHHHopDong);
            this.grbHopDong.Controls.Add(this.txtSoHopDong);
            this.grbHopDong.Controls.Add(this.label13);
            this.grbHopDong.Controls.Add(this.label14);
            this.grbHopDong.Controls.Add(this.label34);
            this.grbHopDong.Controls.Add(this.ccNgayHopDong);
            this.grbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDong.Location = new System.Drawing.Point(739, 8);
            this.grbHopDong.Name = "grbHopDong";
            this.grbHopDong.Size = new System.Drawing.Size(231, 104);
            this.grbHopDong.TabIndex = 3;
            this.grbHopDong.Text = "Hợp đồng";
            this.grbHopDong.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDong.DropDownCalendar.Name = "";
            this.ccNgayHHHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDong.IsNullDate = true;
            this.ccNgayHHHopDong.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHHopDong.Name = "ccNgayHHHopDong";
            this.ccNgayHHHopDong.Nullable = true;
            this.ccNgayHHHopDong.NullButtonText = "Xóa";
            this.ccNgayHHHopDong.ReadOnly = true;
            this.ccNgayHHHopDong.ShowNullButton = true;
            this.ccNgayHHHopDong.Size = new System.Drawing.Size(146, 21);
            this.ccNgayHHHopDong.TabIndex = 13;
            this.ccNgayHHHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(64, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.ReadOnly = true;
            this.txtSoHopDong.Size = new System.Drawing.Size(146, 21);
            this.txtSoHopDong.TabIndex = 11;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHopDong.ButtonClick += new System.EventHandler(this.txtSoHopDong_ButtonClick);
            this.txtSoHopDong.Leave += new System.EventHandler(this.txtSoHopDong_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 0;
            this.label13.Tag = "SoHD";
            this.label13.Text = "Số";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 2;
            this.label14.Tag = "NgayHD";
            this.label14.Text = "Ngày";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(8, 83);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 4;
            this.label34.Tag = "NgayHetHanHD";
            this.label34.Text = "Ngày HH";
            // 
            // ccNgayHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDong.DropDownCalendar.Name = "";
            this.ccNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDong.IsNullDate = true;
            this.ccNgayHopDong.Location = new System.Drawing.Point(64, 48);
            this.ccNgayHopDong.Name = "ccNgayHopDong";
            this.ccNgayHopDong.Nullable = true;
            this.ccNgayHopDong.NullButtonText = "Xóa";
            this.ccNgayHopDong.ReadOnly = true;
            this.ccNgayHopDong.ShowNullButton = true;
            this.ccNgayHopDong.Size = new System.Drawing.Size(146, 21);
            this.ccNgayHopDong.TabIndex = 12;
            this.ccNgayHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.Controls.Add(this.cbPTTT);
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.Location = new System.Drawing.Point(265, 358);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(233, 49);
            this.uiGroupBox18.TabIndex = 14;
            this.uiGroupBox18.Text = "Phương thức thanh toán";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox18.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(7, 19);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(196, 21);
            this.cbPTTT.TabIndex = 30;
            this.cbPTTT.Tag = "PhuongThucThanhToan";
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(265, 303);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(233, 48);
            this.uiGroupBox16.TabIndex = 13;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 19);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(196, 21);
            this.cbDKGH.TabIndex = 29;
            this.cbDKGH.Tag = "DieuKienGiaoHang";
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // grbDiaDiemDoHang
            // 
            this.grbDiaDiemDoHang.Controls.Add(this.ctrCuaKhau);
            this.grbDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemDoHang.Location = new System.Drawing.Point(504, 220);
            this.grbDiaDiemDoHang.Name = "grbDiaDiemDoHang";
            this.grbDiaDiemDoHang.Size = new System.Drawing.Size(229, 80);
            this.grbDiaDiemDoHang.TabIndex = 10;
            this.grbDiaDiemDoHang.Tag = "CangDoHang";
            this.grbDiaDiemDoHang.Text = "Địa điểm dỡ hàng";
            this.grbDiaDiemDoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrCuaKhau
            // 
            this.ctrCuaKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhau.ErrorMessage = "\"Địa điểm dỡ hàng\" bắt buộc phải chọn.";
            this.ctrCuaKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhau.Location = new System.Drawing.Point(8, 22);
            this.ctrCuaKhau.Ma = "";
            this.ctrCuaKhau.Name = "ctrCuaKhau";
            this.ctrCuaKhau.ReadOnly = false;
            this.ctrCuaKhau.Size = new System.Drawing.Size(217, 50);
            this.ctrCuaKhau.TabIndex = 25;
            this.ctrCuaKhau.VisualStyleManager = null;
            // 
            // grbVanTaiDon
            // 
            this.grbVanTaiDon.Controls.Add(this.ccNgayVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.txtSoVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.label19);
            this.grbVanTaiDon.Controls.Add(this.label20);
            this.grbVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbVanTaiDon.Location = new System.Drawing.Point(739, 114);
            this.grbVanTaiDon.Name = "grbVanTaiDon";
            this.grbVanTaiDon.Size = new System.Drawing.Size(231, 104);
            this.grbVanTaiDon.TabIndex = 7;
            this.grbVanTaiDon.Text = "Vận tải đơn";
            this.grbVanTaiDon.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayVanTaiDon
            // 
            // 
            // 
            // 
            this.ccNgayVanTaiDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanTaiDon.DropDownCalendar.Name = "";
            this.ccNgayVanTaiDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanTaiDon.IsNullDate = true;
            this.ccNgayVanTaiDon.Location = new System.Drawing.Point(64, 47);
            this.ccNgayVanTaiDon.Name = "ccNgayVanTaiDon";
            this.ccNgayVanTaiDon.Nullable = true;
            this.ccNgayVanTaiDon.NullButtonText = "Xóa";
            this.ccNgayVanTaiDon.ShowNullButton = true;
            this.ccNgayVanTaiDon.Size = new System.Drawing.Size(146, 21);
            this.ccNgayVanTaiDon.TabIndex = 21;
            this.ccNgayVanTaiDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanTaiDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoVanTaiDon
            // 
            this.txtSoVanTaiDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanTaiDon.Location = new System.Drawing.Point(64, 20);
            this.txtSoVanTaiDon.MaxLength = 300;
            this.txtSoVanTaiDon.Name = "txtSoVanTaiDon";
            this.txtSoVanTaiDon.Size = new System.Drawing.Size(146, 21);
            this.txtSoVanTaiDon.TabIndex = 20;
            this.txtSoVanTaiDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanTaiDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanTaiDon.Leave += new System.EventHandler(this.txtSoVanTaiDon_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 0;
            this.label19.Tag = "SoVanDon";
            this.label19.Text = "Số";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 2;
            this.label20.Tag = "NgayVanDon";
            this.label20.Text = "Ngày";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.ccNgayDen);
            this.uiGroupBox11.Controls.Add(this.cbPTVT);
            this.uiGroupBox11.Controls.Add(this.label17);
            this.uiGroupBox11.Controls.Add(this.lblSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.lblNgayDenPTVT);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(265, 114);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(233, 104);
            this.uiGroupBox11.TabIndex = 5;
            this.uiGroupBox11.Text = "Phương tiện vận tải";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(65, 75);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.Nullable = true;
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.ShowNullButton = true;
            this.ccNgayDen.Size = new System.Drawing.Size(147, 21);
            this.ccNgayDen.TabIndex = 17;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(65, 20);
            this.cbPTVT.Name = "cbPTVT";
            this.helpProvider1.SetShowHelp(this.cbPTVT, true);
            this.cbPTVT.Size = new System.Drawing.Size(147, 21);
            this.cbPTVT.TabIndex = 15;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(5, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 0;
            this.label17.Tag = "LoaiPTVT";
            this.label17.Text = "PTVT";
            // 
            // lblSoHieuPTVT
            // 
            this.lblSoHieuPTVT.AutoSize = true;
            this.lblSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuPTVT.Location = new System.Drawing.Point(4, 52);
            this.lblSoHieuPTVT.Name = "lblSoHieuPTVT";
            this.lblSoHieuPTVT.Size = new System.Drawing.Size(42, 13);
            this.lblSoHieuPTVT.TabIndex = 2;
            this.lblSoHieuPTVT.Tag = "SoHieuPTVT";
            this.lblSoHieuPTVT.Text = "Số hiệu";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(65, 47);
            this.txtSoHieuPTVT.MaxLength = 300;
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(147, 21);
            this.txtSoHieuPTVT.TabIndex = 16;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.AutoSize = true;
            this.lblNgayDenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(4, 78);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(53, 13);
            this.lblNgayDenPTVT.TabIndex = 4;
            this.lblNgayDenPTVT.Tag = "NgayDenPTVT";
            this.lblNgayDenPTVT.Text = "Ngày đến";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.Controls.Add(this.ccNgayHHGiayPhep);
            this.gbGiayPhep.Controls.Add(this.txtSoGiayPhep);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Controls.Add(this.label12);
            this.gbGiayPhep.Controls.Add(this.label33);
            this.gbGiayPhep.Controls.Add(this.ccNgayGiayPhep);
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(504, 8);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(229, 104);
            this.gbGiayPhep.TabIndex = 2;
            this.gbGiayPhep.Text = "Giấy phép";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayHHGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayHHGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHGiayPhep.IsNullDate = true;
            this.ccNgayHHGiayPhep.Location = new System.Drawing.Point(64, 76);
            this.ccNgayHHGiayPhep.Name = "ccNgayHHGiayPhep";
            this.ccNgayHHGiayPhep.Nullable = true;
            this.ccNgayHHGiayPhep.NullButtonText = "Xóa";
            this.ccNgayHHGiayPhep.ShowNullButton = true;
            this.ccNgayHHGiayPhep.Size = new System.Drawing.Size(145, 21);
            this.ccNgayHHGiayPhep.TabIndex = 10;
            this.ccNgayHHGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayHHGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(64, 20);
            this.txtSoGiayPhep.MaxLength = 300;
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(145, 21);
            this.txtSoGiayPhep.TabIndex = 8;
            this.txtSoGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoGiayPhep.ButtonClick += new System.EventHandler(this.txtSoGiayPhep_ButtonClick);
            this.txtSoGiayPhep.Leave += new System.EventHandler(this.txtSoGiayPhep_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 0;
            this.label11.Tag = "SoGiayPhep";
            this.label11.Text = "Số";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 2;
            this.label12.Tag = "NgayGiayPhep";
            this.label12.Text = "Ngày";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 85);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 4;
            this.label33.Tag = "NgayHHGiay Phep";
            this.label33.Text = "Ngày HH";
            // 
            // ccNgayGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayPhep.IsNullDate = true;
            this.ccNgayGiayPhep.Location = new System.Drawing.Point(63, 48);
            this.ccNgayGiayPhep.Name = "ccNgayGiayPhep";
            this.ccNgayGiayPhep.Nullable = true;
            this.ccNgayGiayPhep.NullButtonText = "Xóa";
            this.ccNgayGiayPhep.ShowNullButton = true;
            this.ccNgayGiayPhep.Size = new System.Drawing.Size(145, 21);
            this.ccNgayGiayPhep.TabIndex = 9;
            this.ccNgayGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // grbHoaDonThuongMai
            // 
            this.grbHoaDonThuongMai.Controls.Add(this.ccNgayHDTM);
            this.grbHoaDonThuongMai.Controls.Add(this.txtSoHoaDonThuongMai);
            this.grbHoaDonThuongMai.Controls.Add(this.label15);
            this.grbHoaDonThuongMai.Controls.Add(this.label16);
            this.grbHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHoaDonThuongMai.Location = new System.Drawing.Point(504, 114);
            this.grbHoaDonThuongMai.Name = "grbHoaDonThuongMai";
            this.grbHoaDonThuongMai.Size = new System.Drawing.Size(229, 104);
            this.grbHoaDonThuongMai.TabIndex = 6;
            this.grbHoaDonThuongMai.Text = "Hóa đơn thương mại";
            this.grbHoaDonThuongMai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHDTM
            // 
            // 
            // 
            // 
            this.ccNgayHDTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHDTM.DropDownCalendar.Name = "";
            this.ccNgayHDTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHDTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHDTM.IsNullDate = true;
            this.ccNgayHDTM.Location = new System.Drawing.Point(63, 48);
            this.ccNgayHDTM.Name = "ccNgayHDTM";
            this.ccNgayHDTM.Nullable = true;
            this.ccNgayHDTM.NullButtonText = "Xóa";
            this.ccNgayHDTM.ShowNullButton = true;
            this.ccNgayHDTM.Size = new System.Drawing.Size(146, 21);
            this.ccNgayHDTM.TabIndex = 19;
            this.ccNgayHDTM.TodayButtonText = "Hôm nay";
            this.ccNgayHDTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoHoaDonThuongMai
            // 
            this.txtSoHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDonThuongMai.Location = new System.Drawing.Point(63, 20);
            this.txtSoHoaDonThuongMai.MaxLength = 300;
            this.txtSoHoaDonThuongMai.Name = "txtSoHoaDonThuongMai";
            this.txtSoHoaDonThuongMai.Size = new System.Drawing.Size(146, 21);
            this.txtSoHoaDonThuongMai.TabIndex = 18;
            this.txtSoHoaDonThuongMai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDonThuongMai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDonThuongMai.ButtonClick += new System.EventHandler(this.txtSoHoaDonThuongMai_ButtonClick);
            this.txtSoHoaDonThuongMai.Leave += new System.EventHandler(this.txtSoHoaDonThuongMai_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 0;
            this.label15.Tag = "SoHDTM";
            this.label15.Text = "Số";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 2;
            this.label16.Tag = "NgayHDTM";
            this.label16.Text = "Ngày";
            // 
            // btnNoiDungChinhSua
            // 
            this.btnNoiDungChinhSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoiDungChinhSua.Location = new System.Drawing.Point(904, 554);
            this.btnNoiDungChinhSua.Name = "btnNoiDungChinhSua";
            this.btnNoiDungChinhSua.Size = new System.Drawing.Size(45, 25);
            this.btnNoiDungChinhSua.TabIndex = 276;
            this.btnNoiDungChinhSua.Text = "...";
            this.btnNoiDungChinhSua.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnNoiDungChinhSua.Click += new System.EventHandler(this.btnNoiDungChinhSua_Click);
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieuPTVT
            // 
            this.rfvSoHieuPTVT.ControlToValidate = this.txtSoHieuPTVT;
            this.rfvSoHieuPTVT.ErrorMessage = "\"Số hiệu phương tiện vận tải\" không được để trống.";
            this.rfvSoHieuPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuPTVT.Icon")));
            this.rfvSoHieuPTVT.Tag = "rfvSoHieuPTVT";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox1.Controls.Add(this.clcNgayDuaVaoTK);
            this.uiGroupBox1.Controls.Add(this.pictureBox1);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhaiGiay);
            this.uiGroupBox1.Controls.Add(this.txtHDPL);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.ccNgayTN);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox1.Controls.Add(this.lblPhan);
            this.uiGroupBox1.Controls.Add(this.ccNgayDK);
            this.uiGroupBox1.Controls.Add(this.lblNgayDangKy);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.lblSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongPLTK);
            this.uiGroupBox1.Controls.Add(this.label36);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 37);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1178, 695);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // clcNgayDuaVaoTK
            // 
            this.clcNgayDuaVaoTK.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayDuaVaoTK.Appearance.Options.UseFont = true;
            this.clcNgayDuaVaoTK.Location = new System.Drawing.Point(487, 41);
            this.clcNgayDuaVaoTK.Name = "clcNgayDuaVaoTK";
            this.clcNgayDuaVaoTK.ReadOnly = false;
            this.clcNgayDuaVaoTK.Size = new System.Drawing.Size(122, 21);
            this.clcNgayDuaVaoTK.TabIndex = 20;
            this.clcNgayDuaVaoTK.TagName = "";
            this.clcNgayDuaVaoTK.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDuaVaoTK.WhereCondition = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(789, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Trùng tờ khai. Bấm \"Lưu\" để xem chi tiết");
            // 
            // txtSoToKhaiGiay
            // 
            this.txtSoToKhaiGiay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiGiay.Location = new System.Drawing.Point(487, 13);
            this.txtSoToKhaiGiay.MaxLength = 25;
            this.txtSoToKhaiGiay.Name = "txtSoToKhaiGiay";
            this.txtSoToKhaiGiay.Size = new System.Drawing.Size(122, 21);
            this.txtSoToKhaiGiay.TabIndex = 18;
            this.txtSoToKhaiGiay.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiGiay.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtHDPL
            // 
            this.txtHDPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHDPL.Location = new System.Drawing.Point(289, 69);
            this.txtHDPL.MaxLength = 25;
            this.txtHDPL.Name = "txtHDPL";
            this.txtHDPL.ReadOnly = true;
            this.txtHDPL.Size = new System.Drawing.Size(320, 21);
            this.txtHDPL.TabIndex = 18;
            this.txtHDPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtHDPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(209, 73);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 13);
            this.label32.TabIndex = 17;
            this.label32.Text = "Hướng dẫn PL";
            // 
            // ccNgayTN
            // 
            // 
            // 
            // 
            this.ccNgayTN.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTN.DropDownCalendar.Name = "";
            this.ccNgayTN.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTN.IsNullDate = true;
            this.ccNgayTN.Location = new System.Drawing.Point(106, 41);
            this.ccNgayTN.Name = "ccNgayTN";
            this.ccNgayTN.Nullable = true;
            this.ccNgayTN.NullButtonText = "Xóa";
            this.ccNgayTN.ShowDropDown = false;
            this.ccNgayTN.ShowNullButton = true;
            this.ccNgayTN.Size = new System.Drawing.Size(82, 21);
            this.ccNgayTN.TabIndex = 16;
            this.ccNgayTN.TodayButtonText = "Hôm nay";
            this.ccNgayTN.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 45);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 13);
            this.label28.TabIndex = 15;
            this.label28.Text = "Ngày tiếp nhận";
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            this.lblPhanLuong.Location = new System.Drawing.Point(102, 73);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(89, 13);
            this.lblPhanLuong.TabIndex = 13;
            this.lblPhanLuong.Text = "Chưa phân luồng";
            this.lblPhanLuong.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPhan
            // 
            this.lblPhan.AutoSize = true;
            this.lblPhan.BackColor = System.Drawing.Color.Transparent;
            this.lblPhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhan.Location = new System.Drawing.Point(17, 73);
            this.lblPhan.Name = "lblPhan";
            this.lblPhan.Size = new System.Drawing.Size(60, 13);
            this.lblPhan.TabIndex = 12;
            this.lblPhan.Text = "Phân luồng";
            this.lblPhan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ccNgayDK
            // 
            // 
            // 
            // 
            this.ccNgayDK.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDK.DropDownCalendar.Name = "";
            this.ccNgayDK.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDK.IsNullDate = true;
            this.ccNgayDK.Location = new System.Drawing.Point(289, 41);
            this.ccNgayDK.Name = "ccNgayDK";
            this.ccNgayDK.Nullable = true;
            this.ccNgayDK.NullButtonText = "Xóa";
            this.ccNgayDK.ShowDropDown = false;
            this.ccNgayDK.ShowNullButton = true;
            this.ccNgayDK.Size = new System.Drawing.Size(94, 21);
            this.ccNgayDK.TabIndex = 4;
            this.ccNgayDK.TodayButtonText = "Hôm nay";
            this.ccNgayDK.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.AutoSize = true;
            this.lblNgayDangKy.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy.Location = new System.Drawing.Point(209, 45);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Size = new System.Drawing.Size(73, 13);
            this.lblNgayDangKy.TabIndex = 11;
            this.lblNgayDangKy.Text = "Ngày đăng ký";
            this.lblNgayDangKy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(106, 13);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(82, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.editBox1_TextChanged);
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.AutoSize = true;
            this.lblSoTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhan.Location = new System.Drawing.Point(16, 17);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Size = new System.Drawing.Size(67, 13);
            this.lblSoTiepNhan.TabIndex = 6;
            this.lblSoTiepNhan.Text = "Số tiếp nhận";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(289, 13);
            this.txtSoToKhai.MaxLength = 25;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(94, 21);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(697, 73);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(76, 13);
            this.lblTrangThai.TabIndex = 9;
            this.lblTrangThai.Text = "Chưa khai báo";
            this.lblTrangThai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(391, 45);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(90, 13);
            this.label42.TabIndex = 8;
            this.label42.Text = "Ngày đưa vào TK";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(620, 73);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Trạng thái ";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(209, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Số tờ khai";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSoLuongPLTK
            // 
            this.txtSoLuongPLTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongPLTK.Location = new System.Drawing.Point(710, 41);
            this.txtSoLuongPLTK.MaxLength = 3;
            this.txtSoLuongPLTK.Name = "txtSoLuongPLTK";
            this.txtSoLuongPLTK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongPLTK.Size = new System.Drawing.Size(63, 21);
            this.txtSoLuongPLTK.TabIndex = 2;
            this.txtSoLuongPLTK.Text = "0";
            this.txtSoLuongPLTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongPLTK.Value = ((byte)(0));
            this.txtSoLuongPLTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Byte;
            this.txtSoLuongPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(392, 17);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(57, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Số TK giấy";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(620, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Số lượng PLTK";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.TKGT.Id = new System.Guid("a51629a0-70e2-44c1-9ad5-f60c2ac1a2ad");
            this.uiPanelManager1.Panels.Add(this.TKGT);
            this.TKTG23.Id = new System.Guid("8ee8c9c5-809c-49d4-b338-56b08059d2ca");
            this.uiPanelManager1.Panels.Add(this.TKTG23);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(908, 211), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("a51629a0-70e2-44c1-9ad5-f60c2ac1a2ad"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 200), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("8ee8c9c5-809c-49d4-b338-56b08059d2ca"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("a51629a0-70e2-44c1-9ad5-f60c2ac1a2ad"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("8ee8c9c5-809c-49d4-b338-56b08059d2ca"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 492);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(908, 211);
            this.uiPanel0.TabIndex = 1;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(908, 185);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(906, 183);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(906, 183);
            this.dgList.TabIndex = 182;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.DgListRowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.DgListLoadingRow);
            // 
            // TKGT
            // 
            this.TKGT.AutoHide = true;
            this.TKGT.InnerContainer = this.TKGTContainer;
            this.TKGT.Location = new System.Drawing.Point(0, 0);
            this.TKGT.Name = "TKGT";
            this.TKGT.Size = new System.Drawing.Size(200, 200);
            this.TKGT.TabIndex = 4;
            this.TKGT.Text = "Tờ khai trị giá";
            // 
            // TKGTContainer
            // 
            this.TKGTContainer.Controls.Add(this.dgToKhaiTriGia);
            this.TKGTContainer.Location = new System.Drawing.Point(1, 27);
            this.TKGTContainer.Name = "TKGTContainer";
            this.TKGTContainer.Size = new System.Drawing.Size(198, 172);
            this.TKGTContainer.TabIndex = 0;
            // 
            // dgToKhaiTriGia
            // 
            this.dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiTriGia.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhaiTriGia.AlternatingColors = true;
            this.dgToKhaiTriGia.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgToKhaiTriGia.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgToKhaiTriGia.ColumnAutoResize = true;
            dgToKhaiTriGia_DesignTimeLayout.LayoutString = resources.GetString("dgToKhaiTriGia_DesignTimeLayout.LayoutString");
            this.dgToKhaiTriGia.DesignTimeLayout = dgToKhaiTriGia_DesignTimeLayout;
            this.dgToKhaiTriGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgToKhaiTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhaiTriGia.GroupByBoxVisible = false;
            this.dgToKhaiTriGia.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiTriGia.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiTriGia.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhaiTriGia.ImageList = this.ImageList1;
            this.dgToKhaiTriGia.Location = new System.Drawing.Point(0, 0);
            this.dgToKhaiTriGia.Name = "dgToKhaiTriGia";
            this.dgToKhaiTriGia.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiTriGia.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhaiTriGia.Size = new System.Drawing.Size(198, 172);
            this.dgToKhaiTriGia.TabIndex = 4;
            this.dgToKhaiTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgToKhaiTriGia.VisualStyleManager = this.vsmMain;
            this.dgToKhaiTriGia.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgToKhaiTriGia_RowDoubleClick);
            this.dgToKhaiTriGia.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgToKhaiTriGia_DeletingRecords);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "Form_48px.png");
            this.ImageList1.Images.SetKeyName(1, "Close Pane_48px.png");
            this.ImageList1.Images.SetKeyName(2, "Dossier_32px.png");
            this.ImageList1.Images.SetKeyName(3, "Course_48px.png");
            this.ImageList1.Images.SetKeyName(4, "Attach_48px.png");
            this.ImageList1.Images.SetKeyName(5, "GPS Signal_32px.png");
            this.ImageList1.Images.SetKeyName(6, "Folder_48px.png");
            this.ImageList1.Images.SetKeyName(7, "Edit Property_32px.png");
            this.ImageList1.Images.SetKeyName(8, "User Manual_48px.png");
            this.ImageList1.Images.SetKeyName(9, "Print_48px.png");
            this.ImageList1.Images.SetKeyName(10, "Ok_32px.png");
            this.ImageList1.Images.SetKeyName(11, "Scales_48px.png");
            // 
            // TKTG23
            // 
            this.TKTG23.AutoHide = true;
            this.TKTG23.InnerContainer = this.TKTG23Container;
            this.TKTG23.Location = new System.Drawing.Point(0, 0);
            this.TKTG23.Name = "TKTG23";
            this.TKTG23.Size = new System.Drawing.Size(200, 200);
            this.TKTG23.TabIndex = 4;
            this.TKTG23.Text = "Tờ khai trị giá 23";
            // 
            // TKTG23Container
            // 
            this.TKTG23Container.Controls.Add(this.dglistTKTK23);
            this.TKTG23Container.Location = new System.Drawing.Point(1, 27);
            this.TKTG23Container.Name = "TKTG23Container";
            this.TKTG23Container.Size = new System.Drawing.Size(198, 172);
            this.TKTG23Container.TabIndex = 0;
            // 
            // dglistTKTK23
            // 
            this.dglistTKTK23.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dglistTKTK23.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dglistTKTK23.AlternatingColors = true;
            this.dglistTKTK23.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dglistTKTK23_DesignTimeLayout.LayoutString = resources.GetString("dglistTKTK23_DesignTimeLayout.LayoutString");
            this.dglistTKTK23.DesignTimeLayout = dglistTKTK23_DesignTimeLayout;
            this.dglistTKTK23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dglistTKTK23.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dglistTKTK23.FrozenColumns = 3;
            this.dglistTKTK23.GroupByBoxVisible = false;
            this.dglistTKTK23.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTK23.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTK23.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dglistTKTK23.ImageList = this.ImageList1;
            this.dglistTKTK23.Location = new System.Drawing.Point(0, 0);
            this.dglistTKTK23.Name = "dglistTKTK23";
            this.dglistTKTK23.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dglistTKTK23.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dglistTKTK23.Size = new System.Drawing.Size(198, 172);
            this.dglistTKTK23.TabIndex = 1;
            this.dglistTKTK23.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dglistTKTK23.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dglistTKTK23_RowDoubleClick);
            this.dglistTKTK23.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dglistTKTK23_DeletingRecords);
            this.dglistTKTK23.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dglistTKTK23_LoadingRow);
            // 
            // gridEX1
            // 
            this.gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX1.AlternatingColors = true;
            this.gridEX1.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridEX1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridEX1.ColumnAutoResize = true;
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX1.GroupByBoxVisible = false;
            this.gridEX1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX1.Location = new System.Drawing.Point(0, 0);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX1.Size = new System.Drawing.Size(752, 172);
            this.gridEX1.TabIndex = 2;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX1.VisualStyleManager = this.vsmMain;
            this.gridEX1.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.GridEx1DeletingRecord);
            this.gridEX1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.GridEx1RowDoubleClick);
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến\" không được bỏ trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // rfvNguoiXuatKhau
            // 
            this.rfvNguoiXuatKhau.ControlToValidate = this.txtTenDonViDoiTac;
            this.rfvNguoiXuatKhau.ErrorMessage = "Không được để trống trường này.";
            this.rfvNguoiXuatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiXuatKhau.Icon")));
            this.rfvNguoiXuatKhau.Tag = "rfvNguoiXuatKhau";
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá tính thuế\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "999999";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "999999";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdThemHang,
            this.cmdReadExcel,
            this.cmdTinhLaiThue,
            this.cmdPrint,
            this.cmdSend,
            this.ChungTuKemTheo,
            this.NhanDuLieu,
            this.XacNhan,
            this.ThemHang2,
            this.FileDinhKem,
            this.Huy,
            this.CanDoiNhapXuat,
            this.cmdNhieuHang,
            this.PhanBo,
            this.ToKhaiViet,
            this.InPhenTN,
            this.cmdXuatExcelHang,
            this.cmdInTKA4,
            this.cmdChungTuDinhKem,
            this.cmdVanDon,
            this.cmdHopDong,
            this.cmdGiayPhep,
            this.cmdHoaDonThuongMai,
            this.cmdCO,
            this.cmDeNghiChuyenCuaKhau,
            this.cmdTruyenDuLieuTuXa,
            this.cmdHuyToKhaiDaDuyet,
            this.cmdSuaToKhaiDaDuyet,
            this.cmdFileAnh,
            this.cmdKetQuaXuLy,
            this.cmdChungTuDangAnh,
            this.cmdInTKDTSuaDoiBoSung,
            this.cmdInAnDinhThue,
            this.cmdChungTuNo,
            this.cmdNPLCungUng,
            this.cmdInTKTT15,
            this.cmdToKhaiTriGia,
            this.cmdToKhaiTGPP2,
            this.cmdToKhaiTGPP3,
            this.ToKhaiTriGia,
            this.cmdGiayNopTien,
            this.cmdContainer,
            this.GiayKiemTra,
            this.ChungThuGiamDinh,
            this.cmdInTKTCTT196,
            this.cmdBangKeHangHoa_HD,
            this.cmdInPhieuKetQuaKiemTra,
            this.cmdInPhieuKiemTraHangHoa,
            this.cmdLayPhanHoiLai,
            this.cmdPhanHoiSoTiepNhan,
            this.cmdPhanHoiSoToKhai,
            this.cmdPhanHoiPhanLuong,
            this.cmdPhuKienToKhai,
            this.cmdSendTK});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThemHang1,
            this.cmdSave1,
            this.cmdNPLCungUng1,
            this.cmdToKhaiTriGia1,
            this.cmdChungTuDinhKem1,
            this.cmdTruyenDuLieuTuXa1,
            this.cmdPrint1,
            this.cmdXuatExcelHang1,
            this.cmdPrint2,
            this.CanDoiNhapXuat1,
            this.cmdPhuKienToKhai1});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(1184, 34);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmbToolBar.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmbToolBar.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbToolBar_CommandClick);
            // 
            // ThemHang1
            // 
            this.ThemHang1.Key = "ThemHang";
            this.ThemHang1.Name = "ThemHang1";
            this.ThemHang1.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.ThemHang1.Text = "&Thêm hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "&Lưu";
            // 
            // cmdNPLCungUng1
            // 
            this.cmdNPLCungUng1.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng1.Name = "cmdNPLCungUng1";
            // 
            // cmdToKhaiTriGia1
            // 
            this.cmdToKhaiTriGia1.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia1.Name = "cmdToKhaiTriGia1";
            // 
            // cmdChungTuDinhKem1
            // 
            this.cmdChungTuDinhKem1.ImageIndex = 4;
            this.cmdChungTuDinhKem1.Key = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem1.Name = "cmdChungTuDinhKem1";
            // 
            // cmdTruyenDuLieuTuXa1
            // 
            this.cmdTruyenDuLieuTuXa1.ImageIndex = 5;
            this.cmdTruyenDuLieuTuXa1.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa1.Name = "cmdTruyenDuLieuTuXa1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "&In";
            // 
            // cmdXuatExcelHang1
            // 
            this.cmdXuatExcelHang1.Key = "cmdXuatExcelHang";
            this.cmdXuatExcelHang1.Name = "cmdXuatExcelHang1";
            this.cmdXuatExcelHang1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdPrint2
            // 
            this.cmdPrint2.Key = "cmdPrint";
            this.cmdPrint2.Name = "cmdPrint2";
            this.cmdPrint2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // CanDoiNhapXuat1
            // 
            this.CanDoiNhapXuat1.HotImageIndex = 11;
            this.CanDoiNhapXuat1.ImageIndex = 11;
            this.CanDoiNhapXuat1.Key = "CanDoiNhapXuat";
            this.CanDoiNhapXuat1.Name = "CanDoiNhapXuat1";
            // 
            // cmdPhuKienToKhai1
            // 
            this.cmdPhuKienToKhai1.Key = "cmdPhuKienToKhai";
            this.cmdPhuKienToKhai1.Name = "cmdPhuKienToKhai1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHang.Image")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdReadExcel
            // 
            this.cmdReadExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdReadExcel.Image")));
            this.cmdReadExcel.Key = "cmdReadExcel";
            this.cmdReadExcel.Name = "cmdReadExcel";
            this.cmdReadExcel.Text = "Đọc Excel";
            // 
            // cmdTinhLaiThue
            // 
            this.cmdTinhLaiThue.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTinhLaiThue.Icon")));
            this.cmdTinhLaiThue.Key = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Name = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Text = "Tính lại thuế";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdInTKTT151,
            this.cmdInTKTCTT1961,
            this.cmdInTKDTSuaDoiBoSung1,
            this.cmdContainer1,
            this.cmdAnDinhThue1,
            this.cmdBangKeHangHoa_HD1,
            this.cmdInPhieuKetQuaKiemTra1,
            this.cmdInPhieuKiemTraHangHoa1});
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In tờ khai";
            // 
            // cmdInTKTT151
            // 
            this.cmdInTKTT151.Key = "cmdInTKTT15";
            this.cmdInTKTT151.Name = "cmdInTKTT151";
            this.cmdInTKTT151.Text = "In tờ khai thông quan (Thông tư 196)";
            // 
            // cmdInTKTCTT1961
            // 
            this.cmdInTKTCTT1961.Key = "cmdInTKTCTT196";
            this.cmdInTKTCTT1961.Name = "cmdInTKTCTT1961";
            this.cmdInTKTCTT1961.Text = "In tờ khai tại chỗ (Thông tư 196)";
            // 
            // cmdInTKDTSuaDoiBoSung1
            // 
            this.cmdInTKDTSuaDoiBoSung1.ImageIndex = 9;
            this.cmdInTKDTSuaDoiBoSung1.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung1.Name = "cmdInTKDTSuaDoiBoSung1";
            this.cmdInTKDTSuaDoiBoSung1.Text = "In tờ khai sửa đổi bổ sung";
            // 
            // cmdContainer1
            // 
            this.cmdContainer1.Image = ((System.Drawing.Image)(resources.GetObject("cmdContainer1.Image")));
            this.cmdContainer1.Key = "cmdContainer";
            this.cmdContainer1.Name = "cmdContainer1";
            // 
            // cmdAnDinhThue1
            // 
            this.cmdAnDinhThue1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAnDinhThue1.Image")));
            this.cmdAnDinhThue1.Key = "cmdAnDinhThue";
            this.cmdAnDinhThue1.Name = "cmdAnDinhThue1";
            this.cmdAnDinhThue1.Text = "In Ấn định thuế tờ khai";
            // 
            // cmdBangKeHangHoa_HD1
            // 
            this.cmdBangKeHangHoa_HD1.Key = "cmdBangKeHangHoa_HD";
            this.cmdBangKeHangHoa_HD1.Name = "cmdBangKeHangHoa_HD1";
            // 
            // cmdInPhieuKetQuaKiemTra1
            // 
            this.cmdInPhieuKetQuaKiemTra1.Key = "cmdInPhieuKetQuaKiemTra";
            this.cmdInPhieuKetQuaKiemTra1.Name = "cmdInPhieuKetQuaKiemTra1";
            this.cmdInPhieuKetQuaKiemTra1.Text = "In phiếu ghi kết quả kiểm tra chứng từ giấy";
            // 
            // cmdInPhieuKiemTraHangHoa1
            // 
            this.cmdInPhieuKiemTraHangHoa1.Key = "cmdInPhieuKiemTraHangHoa";
            this.cmdInPhieuKiemTraHangHoa1.Name = "cmdInPhieuKiemTraHangHoa1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + K)";
            // 
            // ChungTuKemTheo
            // 
            this.ChungTuKemTheo.Image = ((System.Drawing.Image)(resources.GetObject("ChungTuKemTheo.Image")));
            this.ChungTuKemTheo.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo.Name = "ChungTuKemTheo";
            this.ChungTuKemTheo.Text = "Thêm chứng từ";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Image = ((System.Drawing.Image)(resources.GetObject("NhanDuLieu.Image")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // XacNhan
            // 
            this.XacNhan.Image = ((System.Drawing.Image)(resources.GetObject("XacNhan.Image")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // ThemHang2
            // 
            this.ThemHang2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdReadExcel1,
            this.cmdNhieuHang1});
            this.ThemHang2.Image = ((System.Drawing.Image)(resources.GetObject("ThemHang2.Image")));
            this.ThemHang2.Key = "ThemHang";
            this.ThemHang2.Name = "ThemHang2";
            this.ThemHang2.Text = "Thêm hàng";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdReadExcel1
            // 
            this.cmdReadExcel1.Key = "cmdReadExcel";
            this.cmdReadExcel1.Name = "cmdReadExcel1";
            // 
            // cmdNhieuHang1
            // 
            this.cmdNhieuHang1.Key = "cmdNhieuHang";
            this.cmdNhieuHang1.Name = "cmdNhieuHang1";
            // 
            // FileDinhKem
            // 
            this.FileDinhKem.Image = ((System.Drawing.Image)(resources.GetObject("FileDinhKem.Image")));
            this.FileDinhKem.Key = "FileDinhKem";
            this.FileDinhKem.Name = "FileDinhKem";
            this.FileDinhKem.Text = "File đính kèm";
            // 
            // Huy
            // 
            this.Huy.Image = ((System.Drawing.Image)(resources.GetObject("Huy.Image")));
            this.Huy.IsEditableControl = Janus.Windows.UI.InheritableBoolean.True;
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy khai báo";
            // 
            // CanDoiNhapXuat
            // 
            this.CanDoiNhapXuat.Image = ((System.Drawing.Image)(resources.GetObject("CanDoiNhapXuat.Image")));
            this.CanDoiNhapXuat.Key = "CanDoiNhapXuat";
            this.CanDoiNhapXuat.Name = "CanDoiNhapXuat";
            this.CanDoiNhapXuat.Text = "Cân đối nhập xuất";
            // 
            // cmdNhieuHang
            // 
            this.cmdNhieuHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhieuHang.Image")));
            this.cmdNhieuHang.Key = "cmdNhieuHang";
            this.cmdNhieuHang.Name = "cmdNhieuHang";
            this.cmdNhieuHang.Text = "Thêm nhiều hàng";
            // 
            // PhanBo
            // 
            this.PhanBo.Key = "PhanBo";
            this.PhanBo.Name = "PhanBo";
            this.PhanBo.Text = "Phân bổ";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhenTN
            // 
            this.InPhenTN.Key = "InPhieuTN";
            this.InPhenTN.Name = "InPhenTN";
            this.InPhenTN.Text = "Phiếu tiếp nhận";
            // 
            // cmdXuatExcelHang
            // 
            this.cmdXuatExcelHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatExcelHang.Image")));
            this.cmdXuatExcelHang.Key = "cmdXuatExcelHang";
            this.cmdXuatExcelHang.Name = "cmdXuatExcelHang";
            this.cmdXuatExcelHang.Text = "Xuất Excel hàng TK";
            // 
            // cmdInTKA4
            // 
            this.cmdInTKA4.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTKA4.Image")));
            this.cmdInTKA4.Key = "cmdInTKA4";
            this.cmdInTKA4.Name = "cmdInTKA4";
            this.cmdInTKA4.Text = "Tờ khai A4";
            // 
            // cmdChungTuDinhKem
            // 
            this.cmdChungTuDinhKem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdVanDon1,
            this.cmdGiayPhep1,
            this.cmdHopDong1,
            this.cmdHoaDonThuongMai1,
            this.cmdCO1,
            this.cmDeNghiChuyenCuaKhau1,
            this.cmdChungTuDangAnh1,
            this.cmdChungTuNo1,
            this.GiayKiemTra1,
            this.ChungThuGiamDinh1});
            this.cmdChungTuDinhKem.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuDinhKem.Image")));
            this.cmdChungTuDinhKem.Key = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem.Name = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem.Text = "Chứng từ đính kèm";
            // 
            // cmdVanDon1
            // 
            this.cmdVanDon1.Key = "cmdVanDon";
            this.cmdVanDon1.Name = "cmdVanDon1";
            // 
            // cmdGiayPhep1
            // 
            this.cmdGiayPhep1.Key = "cmdGiayPhep";
            this.cmdGiayPhep1.Name = "cmdGiayPhep1";
            // 
            // cmdHopDong1
            // 
            this.cmdHopDong1.ImageIndex = 0;
            this.cmdHopDong1.Key = "cmdHopDong";
            this.cmdHopDong1.Name = "cmdHopDong1";
            // 
            // cmdHoaDonThuongMai1
            // 
            this.cmdHoaDonThuongMai1.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai1.Name = "cmdHoaDonThuongMai1";
            // 
            // cmdCO1
            // 
            this.cmdCO1.Key = "cmdCO";
            this.cmdCO1.Name = "cmdCO1";
            // 
            // cmDeNghiChuyenCuaKhau1
            // 
            this.cmDeNghiChuyenCuaKhau1.Key = "cmDeNghiChuyenCuaKhau";
            this.cmDeNghiChuyenCuaKhau1.Name = "cmDeNghiChuyenCuaKhau1";
            // 
            // cmdChungTuDangAnh1
            // 
            this.cmdChungTuDangAnh1.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh1.Name = "cmdChungTuDangAnh1";
            // 
            // cmdChungTuNo1
            // 
            this.cmdChungTuNo1.Key = "cmdChungTuNo";
            this.cmdChungTuNo1.Name = "cmdChungTuNo1";
            // 
            // GiayKiemTra1
            // 
            this.GiayKiemTra1.Key = "GiayKiemTra";
            this.GiayKiemTra1.Name = "GiayKiemTra1";
            // 
            // ChungThuGiamDinh1
            // 
            this.ChungThuGiamDinh1.Key = "ChungThuGiamDinh";
            this.ChungThuGiamDinh1.Name = "ChungThuGiamDinh1";
            // 
            // cmdVanDon
            // 
            this.cmdVanDon.Image = ((System.Drawing.Image)(resources.GetObject("cmdVanDon.Image")));
            this.cmdVanDon.Key = "cmdVanDon";
            this.cmdVanDon.Name = "cmdVanDon";
            this.cmdVanDon.Text = "Vận đơn";
            // 
            // cmdHopDong
            // 
            this.cmdHopDong.Image = ((System.Drawing.Image)(resources.GetObject("cmdHopDong.Image")));
            this.cmdHopDong.Key = "cmdHopDong";
            this.cmdHopDong.Name = "cmdHopDong";
            this.cmdHopDong.Text = "Hợp đồng";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("cmdGiayPhep.Image")));
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdHoaDonThuongMai
            // 
            this.cmdHoaDonThuongMai.Image = ((System.Drawing.Image)(resources.GetObject("cmdHoaDonThuongMai.Image")));
            this.cmdHoaDonThuongMai.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Name = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Text = "Hóa đơn thương mại";
            // 
            // cmdCO
            // 
            this.cmdCO.Image = ((System.Drawing.Image)(resources.GetObject("cmdCO.Image")));
            this.cmdCO.Key = "cmdCO";
            this.cmdCO.Name = "cmdCO";
            this.cmdCO.Text = "CO";
            // 
            // cmDeNghiChuyenCuaKhau
            // 
            this.cmDeNghiChuyenCuaKhau.Image = ((System.Drawing.Image)(resources.GetObject("cmDeNghiChuyenCuaKhau.Image")));
            this.cmDeNghiChuyenCuaKhau.Key = "cmDeNghiChuyenCuaKhau";
            this.cmDeNghiChuyenCuaKhau.Name = "cmDeNghiChuyenCuaKhau";
            this.cmDeNghiChuyenCuaKhau.Text = "Đề nghị chuyển cửa khẩu";
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendTK1,
            this.cmdSend1,
            this.NhanDuLieu1,
            this.XacNhan1,
            this.Separator3,
            this.cmdSuaToKhaiDaDuyet1,
            this.cmdHuyToKhaiDaDuyet1,
            this.Separator4,
            this.cmdKetQuaXuLy1,
            this.cmdLayPhanHoiLai1});
            this.cmdTruyenDuLieuTuXa.Image = ((System.Drawing.Image)(resources.GetObject("cmdTruyenDuLieuTuXa.Image")));
            this.cmdTruyenDuLieuTuXa.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Name = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSendTK1
            // 
            this.cmdSendTK1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendTK1.Image")));
            this.cmdSendTK1.Key = "cmdSendTK";
            this.cmdSendTK1.Name = "cmdSendTK1";
            this.cmdSendTK1.Text = "Khai báo TK đưa vào thanh khoản";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // NhanDuLieu1
            // 
            this.NhanDuLieu1.Key = "NhanDuLieu";
            this.NhanDuLieu1.Name = "NhanDuLieu1";
            // 
            // XacNhan1
            // 
            this.XacNhan1.Key = "XacNhan";
            this.XacNhan1.Name = "XacNhan1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdSuaToKhaiDaDuyet1
            // 
            this.cmdSuaToKhaiDaDuyet1.ImageIndex = 7;
            this.cmdSuaToKhaiDaDuyet1.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet1.Name = "cmdSuaToKhaiDaDuyet1";
            // 
            // cmdHuyToKhaiDaDuyet1
            // 
            this.cmdHuyToKhaiDaDuyet1.ImageIndex = 8;
            this.cmdHuyToKhaiDaDuyet1.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet1.Name = "cmdHuyToKhaiDaDuyet1";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.ImageIndex = 6;
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdLayPhanHoiLai1
            // 
            this.cmdLayPhanHoiLai1.Key = "cmdLayPhanHoiLai";
            this.cmdLayPhanHoiLai1.Name = "cmdLayPhanHoiLai1";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdHuyToKhaiDaDuyet.Image")));
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdSuaToKhaiDaDuyet
            // 
            this.cmdSuaToKhaiDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaToKhaiDaDuyet.Image")));
            this.cmdSuaToKhaiDaDuyet.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Name = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Text = "Sửa tờ khai đã duyệt";
            // 
            // cmdFileAnh
            // 
            this.cmdFileAnh.Image = ((System.Drawing.Image)(resources.GetObject("cmdFileAnh.Image")));
            this.cmdFileAnh.Key = "cmdFileAnh";
            this.cmdFileAnh.Name = "cmdFileAnh";
            this.cmdFileAnh.Text = "Đính kèm file dạng ảnh";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy.Image")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdChungTuDangAnh
            // 
            this.cmdChungTuDangAnh.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuDangAnh.Image")));
            this.cmdChungTuDangAnh.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Name = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Text = "Chứng từ dạng ảnh";
            // 
            // cmdInTKDTSuaDoiBoSung
            // 
            this.cmdInTKDTSuaDoiBoSung.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTKDTSuaDoiBoSung.Image")));
            this.cmdInTKDTSuaDoiBoSung.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Name = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Text = "In tờ khai điện tử sửa đổi bổ sung";
            // 
            // cmdInAnDinhThue
            // 
            this.cmdInAnDinhThue.Image = ((System.Drawing.Image)(resources.GetObject("cmdInAnDinhThue.Image")));
            this.cmdInAnDinhThue.Key = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Name = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Text = "In Ấn định thuế tờ khai";
            // 
            // cmdChungTuNo
            // 
            this.cmdChungTuNo.Image = ((System.Drawing.Image)(resources.GetObject("cmdChungTuNo.Image")));
            this.cmdChungTuNo.Key = "cmdChungTuNo";
            this.cmdChungTuNo.Name = "cmdChungTuNo";
            this.cmdChungTuNo.Text = "Chứng từ nợ";
            // 
            // cmdNPLCungUng
            // 
            this.cmdNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("cmdNPLCungUng.Image")));
            this.cmdNPLCungUng.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng.Name = "cmdNPLCungUng";
            this.cmdNPLCungUng.Text = "NPL Cung ứng";
            this.cmdNPLCungUng.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInTKTT15
            // 
            this.cmdInTKTT15.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTKTT15.Image")));
            this.cmdInTKTT15.Key = "cmdInTKTT15";
            this.cmdInTKTT15.Name = "cmdInTKTT15";
            this.cmdInTKTT15.Text = "In tờ khai thông quan TT 196";
            // 
            // cmdToKhaiTriGia
            // 
            this.cmdToKhaiTriGia.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ToKhaiTriGia1,
            this.cmdToKhaiTGPP21,
            this.cmdToKhaiTGPP31});
            this.cmdToKhaiTriGia.Image = ((System.Drawing.Image)(resources.GetObject("cmdToKhaiTriGia.Image")));
            this.cmdToKhaiTriGia.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Name = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Text = "Tờ khai trị giá";
            // 
            // ToKhaiTriGia1
            // 
            this.ToKhaiTriGia1.Key = "ToKhaiTriGia";
            this.ToKhaiTriGia1.Name = "ToKhaiTriGia1";
            // 
            // cmdToKhaiTGPP21
            // 
            this.cmdToKhaiTGPP21.Key = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP21.Name = "cmdToKhaiTGPP21";
            // 
            // cmdToKhaiTGPP31
            // 
            this.cmdToKhaiTGPP31.Key = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP31.Name = "cmdToKhaiTGPP31";
            // 
            // cmdToKhaiTGPP2
            // 
            this.cmdToKhaiTGPP2.Image = ((System.Drawing.Image)(resources.GetObject("cmdToKhaiTGPP2.Image")));
            this.cmdToKhaiTGPP2.Key = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP2.Name = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP2.Text = "Tờ khai trị giá PP2";
            // 
            // cmdToKhaiTGPP3
            // 
            this.cmdToKhaiTGPP3.Image = ((System.Drawing.Image)(resources.GetObject("cmdToKhaiTGPP3.Image")));
            this.cmdToKhaiTGPP3.Key = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP3.Name = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP3.Text = "Tờ khai trị giá PP3";
            // 
            // ToKhaiTriGia
            // 
            this.ToKhaiTriGia.Image = ((System.Drawing.Image)(resources.GetObject("ToKhaiTriGia.Image")));
            this.ToKhaiTriGia.Key = "ToKhaiTriGia";
            this.ToKhaiTriGia.Name = "ToKhaiTriGia";
            this.ToKhaiTriGia.Text = "Tờ khai trị giá";
            // 
            // cmdGiayNopTien
            // 
            this.cmdGiayNopTien.Image = ((System.Drawing.Image)(resources.GetObject("cmdGiayNopTien.Image")));
            this.cmdGiayNopTien.Key = "cmdGiayNopTien";
            this.cmdGiayNopTien.Name = "cmdGiayNopTien";
            this.cmdGiayNopTien.Text = "Giấy nộp tiền";
            // 
            // cmdContainer
            // 
            this.cmdContainer.Image = ((System.Drawing.Image)(resources.GetObject("cmdContainer.Image")));
            this.cmdContainer.Key = "cmdContainer";
            this.cmdContainer.Name = "cmdContainer";
            this.cmdContainer.Text = "In bảng kê Container";
            // 
            // GiayKiemTra
            // 
            this.GiayKiemTra.Image = ((System.Drawing.Image)(resources.GetObject("GiayKiemTra.Image")));
            this.GiayKiemTra.Key = "GiayKiemTra";
            this.GiayKiemTra.Name = "GiayKiemTra";
            this.GiayKiemTra.Text = "Giấy kiểm tra";
            // 
            // ChungThuGiamDinh
            // 
            this.ChungThuGiamDinh.Image = ((System.Drawing.Image)(resources.GetObject("ChungThuGiamDinh.Image")));
            this.ChungThuGiamDinh.Key = "ChungThuGiamDinh";
            this.ChungThuGiamDinh.Name = "ChungThuGiamDinh";
            this.ChungThuGiamDinh.Text = "Chứng thư giám định";
            // 
            // cmdInTKTCTT196
            // 
            this.cmdInTKTCTT196.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTKTCTT196.Image")));
            this.cmdInTKTCTT196.Key = "cmdInTKTCTT196";
            this.cmdInTKTCTT196.Name = "cmdInTKTCTT196";
            this.cmdInTKTCTT196.Text = "In tờ khai tại chỗ theo TT 196";
            // 
            // cmdBangKeHangHoa_HD
            // 
            this.cmdBangKeHangHoa_HD.Image = ((System.Drawing.Image)(resources.GetObject("cmdBangKeHangHoa_HD.Image")));
            this.cmdBangKeHangHoa_HD.Key = "cmdBangKeHangHoa_HD";
            this.cmdBangKeHangHoa_HD.Name = "cmdBangKeHangHoa_HD";
            this.cmdBangKeHangHoa_HD.Text = "Bảng kê hàng hóa theo hợp đồng";
            // 
            // cmdInPhieuKetQuaKiemTra
            // 
            this.cmdInPhieuKetQuaKiemTra.Image = ((System.Drawing.Image)(resources.GetObject("cmdInPhieuKetQuaKiemTra.Image")));
            this.cmdInPhieuKetQuaKiemTra.Key = "cmdInPhieuKetQuaKiemTra";
            this.cmdInPhieuKetQuaKiemTra.Name = "cmdInPhieuKetQuaKiemTra";
            this.cmdInPhieuKetQuaKiemTra.Text = "In phiếu kết quả kiểm tra chứng từ";
            // 
            // cmdInPhieuKiemTraHangHoa
            // 
            this.cmdInPhieuKiemTraHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("cmdInPhieuKiemTraHangHoa.Image")));
            this.cmdInPhieuKiemTraHangHoa.Key = "cmdInPhieuKiemTraHangHoa";
            this.cmdInPhieuKiemTraHangHoa.Name = "cmdInPhieuKiemTraHangHoa";
            this.cmdInPhieuKiemTraHangHoa.Text = "In phiếu ghi kết quả kiểm tra hàng hóa";
            // 
            // cmdLayPhanHoiLai
            // 
            this.cmdLayPhanHoiLai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdPhanHoiSoTiepNhan1,
            this.cmdPhanHoiSoToKhai1,
            this.cmdPhanHoiPhanLuong1});
            this.cmdLayPhanHoiLai.Image = ((System.Drawing.Image)(resources.GetObject("cmdLayPhanHoiLai.Image")));
            this.cmdLayPhanHoiLai.Key = "cmdLayPhanHoiLai";
            this.cmdLayPhanHoiLai.Name = "cmdLayPhanHoiLai";
            this.cmdLayPhanHoiLai.Text = "Lấy lại phản hồi";
            // 
            // cmdPhanHoiSoTiepNhan1
            // 
            this.cmdPhanHoiSoTiepNhan1.Key = "cmdPhanHoiSoTiepNhan";
            this.cmdPhanHoiSoTiepNhan1.Name = "cmdPhanHoiSoTiepNhan1";
            // 
            // cmdPhanHoiSoToKhai1
            // 
            this.cmdPhanHoiSoToKhai1.Key = "cmdPhanHoiSoToKhai";
            this.cmdPhanHoiSoToKhai1.Name = "cmdPhanHoiSoToKhai1";
            // 
            // cmdPhanHoiPhanLuong1
            // 
            this.cmdPhanHoiPhanLuong1.Key = "cmdPhanHoiPhanLuong";
            this.cmdPhanHoiPhanLuong1.Name = "cmdPhanHoiPhanLuong1";
            // 
            // cmdPhanHoiSoTiepNhan
            // 
            this.cmdPhanHoiSoTiepNhan.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhanHoiSoTiepNhan.Image")));
            this.cmdPhanHoiSoTiepNhan.Key = "cmdPhanHoiSoTiepNhan";
            this.cmdPhanHoiSoTiepNhan.Name = "cmdPhanHoiSoTiepNhan";
            this.cmdPhanHoiSoTiepNhan.Text = "Phản hồi số tiếp nhận";
            // 
            // cmdPhanHoiSoToKhai
            // 
            this.cmdPhanHoiSoToKhai.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhanHoiSoToKhai.Image")));
            this.cmdPhanHoiSoToKhai.Key = "cmdPhanHoiSoToKhai";
            this.cmdPhanHoiSoToKhai.Name = "cmdPhanHoiSoToKhai";
            this.cmdPhanHoiSoToKhai.Text = "Phản hồi số tờ khai";
            // 
            // cmdPhanHoiPhanLuong
            // 
            this.cmdPhanHoiPhanLuong.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhanHoiPhanLuong.Image")));
            this.cmdPhanHoiPhanLuong.Key = "cmdPhanHoiPhanLuong";
            this.cmdPhanHoiPhanLuong.Name = "cmdPhanHoiPhanLuong";
            this.cmdPhanHoiPhanLuong.Text = "Phản hồi phân luồng";
            // 
            // cmdPhuKienToKhai
            // 
            this.cmdPhuKienToKhai.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhuKienToKhai.Image")));
            this.cmdPhuKienToKhai.Key = "cmdPhuKienToKhai";
            this.cmdPhuKienToKhai.Name = "cmdPhuKienToKhai";
            this.cmdPhuKienToKhai.Text = "Phụ kiện";
            // 
            // cmdSendTK
            // 
            this.cmdSendTK.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendTK.Image")));
            this.cmdSendTK.Key = "cmdSendTK";
            this.cmdSendTK.Name = "cmdSendTK";
            this.cmdSendTK.Text = "Khai bao TK đưa vào thanh khoản";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1184, 34);
            // 
            // cmdReadExcel2
            // 
            this.cmdReadExcel2.Key = "cmdReadExcel";
            this.cmdReadExcel2.Name = "cmdReadExcel2";
            // 
            // ChungTuKemTheo2
            // 
            this.ChungTuKemTheo2.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo2.Name = "ChungTuKemTheo2";
            // 
            // rvSKH
            // 
            this.rvSKH.ControlToValidate = this.txtSoKien;
            this.rvSKH.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rvSKH.Icon = ((System.Drawing.Icon)(resources.GetObject("rvSKH.Icon")));
            this.rvSKH.MaximumValue = "9999999";
            this.rvSKH.MinimumValue = "0";
            this.rvSKH.Tag = "rvSKH";
            this.rvSKH.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rVTrongLuong
            // 
            this.rVTrongLuong.ControlToValidate = this.txtTrongLuong;
            this.rVTrongLuong.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rVTrongLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rVTrongLuong.Icon")));
            this.rVTrongLuong.MaximumValue = "9999999";
            this.rVTrongLuong.MinimumValue = "0";
            this.rVTrongLuong.Tag = "rVTrongLuong";
            this.rVTrongLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rVTrongLuongTinh
            // 
            this.rVTrongLuongTinh.ControlToValidate = this.txtTrongLuongTinh;
            this.rVTrongLuongTinh.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rVTrongLuongTinh.Icon = ((System.Drawing.Icon)(resources.GetObject("rVTrongLuongTinh.Icon")));
            this.rVTrongLuongTinh.MaximumValue = "9999999";
            this.rVTrongLuongTinh.MinimumValue = "0";
            this.rVTrongLuongTinh.Tag = "rVTrongLuongTinh";
            this.rVTrongLuongTinh.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvLPHQ
            // 
            this.rvLPHQ.ControlToValidate = this.txtLePhiHQ;
            this.rvLPHQ.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rvLPHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rvLPHQ.Icon")));
            this.rvLPHQ.MaximumValue = "1000000000000";
            this.rvLPHQ.MinimumValue = "0";
            this.rvLPHQ.Tag = "rvLPHQ";
            this.rvLPHQ.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rVPBH
            // 
            this.rVPBH.ControlToValidate = this.txtPhiBaoHiem;
            this.rVPBH.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rVPBH.Icon = ((System.Drawing.Icon)(resources.GetObject("rVPBH.Icon")));
            this.rVPBH.MaximumValue = "1000000000000";
            this.rVPBH.MinimumValue = "0";
            this.rVPBH.Tag = "rVPBH";
            this.rVPBH.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rVPVC
            // 
            this.rVPVC.ControlToValidate = this.txtPhiVanChuyen;
            this.rVPVC.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rVPVC.Icon = ((System.Drawing.Icon)(resources.GetObject("rVPVC.Icon")));
            this.rVPVC.MaximumValue = "1000000000000";
            this.rVPVC.MinimumValue = "0";
            this.rVPVC.Tag = "rVPVC";
            this.rVPVC.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvPhiKHac
            // 
            this.rvPhiKHac.ControlToValidate = this.txtPhiNganHang;
            this.rvPhiKHac.ErrorMessage = "\"Giá trị nhập\" không hợp lệ.";
            this.rvPhiKHac.Icon = ((System.Drawing.Icon)(resources.GetObject("rvPhiKHac.Icon")));
            this.rvPhiKHac.MaximumValue = "1000000000000";
            this.rvPhiKHac.MinimumValue = "0";
            this.rvPhiKHac.Tag = "rvPhiKHac";
            this.rvPhiKHac.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvSLPLTK
            // 
            this.rvSLPLTK.ControlToValidate = this.txtSoLuongPLTK;
            this.rvSLPLTK.ErrorMessage = "\"Số lượng PLTK\" không hợp lệ.";
            this.rvSLPLTK.Icon = ((System.Drawing.Icon)(resources.GetObject("rvSLPLTK.Icon")));
            this.rvSLPLTK.MaximumValue = "255";
            this.rvSLPLTK.MinimumValue = "0";
            this.rvSLPLTK.Tag = "rvSLPLTK";
            this.rvSLPLTK.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // gridEXExporterHangToKhai
            // 
            this.gridEXExporterHangToKhai.GridEX = this.dgList;
            this.gridEXExporterHangToKhai.SheetName = "HangTK";
            // 
            // rfvHopDong
            // 
            this.rfvHopDong.ControlToValidate = this.txtSoHopDong;
            this.rfvHopDong.ErrorMessage = "\"Số hợp đồng\" không được bỏ trống.";
            this.rfvHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvHopDong.Icon")));
            this.rfvHopDong.Tag = "rfvHopDong";
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "Dossier_48px.png");
            this.ilLarge.Images.SetKeyName(1, "Form_48px.png");
            this.ilLarge.Images.SetKeyName(2, "Course_48px.png");
            this.ilLarge.Images.SetKeyName(3, "Categorize_48px.png");
            this.ilLarge.Images.SetKeyName(4, "Plus_48px.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Plus_48px.png");
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ToKhaiMauDichForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1184, 753);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Tờ khai nhập khẩu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ToKhaiNhapKhauSxxkFormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ToKhaiMauDichForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDamBaoNghiaVuNT)).EndInit();
            this.uiDamBaoNghiaVuNT.ResumeLayout(false);
            this.uiDamBaoNghiaVuNT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnHanThue)).EndInit();
            this.uiAnHanThue.ResumeLayout(false);
            this.uiAnHanThue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).EndInit();
            this.grbNguoiNK.ResumeLayout(false);
            this.grbNguoiNK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).EndInit();
            this.grbDiaDiemXepHang.ResumeLayout(false);
            this.grbDiaDiemXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).EndInit();
            this.grbNguyenTe.ResumeLayout(false);
            this.grbNguyenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).EndInit();
            this.grbNuocXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).EndInit();
            this.grbNguoiXK.ResumeLayout(false);
            this.grbNguoiXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).EndInit();
            this.grbHopDong.ResumeLayout(false);
            this.grbHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).EndInit();
            this.grbDiaDiemDoHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).EndInit();
            this.grbVanTaiDon.ResumeLayout(false);
            this.grbVanTaiDon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).EndInit();
            this.grbHoaDonThuongMai.ResumeLayout(false);
            this.grbHoaDonThuongMai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGT)).EndInit();
            this.TKGT.ResumeLayout(false);
            this.TKGTContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKTG23)).EndInit();
            this.TKTG23.ResumeLayout(false);
            this.TKTG23Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTK23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rvSKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVTrongLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVTrongLuongTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLPHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVPBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rVPVC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvPhiKHac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvSLPLTK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvHopDong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LoaiHinhMauDichControl01 ctrLoaiHinhMauDich;
        private NguyenTeControl ctrNguyenTe;
        private NuocControl ctrNuocXuatKhau;
        private UIComboBox cbPTTT;
        private UIComboBox cbDKGH;
        private CuaKhauControl ctrCuaKhau;
        private UICommand cmdThemHang;
        private UICommand cmdTinhLaiThue;
        private UICommand cmdPrint;
        private UICommand cmdPrint1;
        private UICommand cmdSend;
        private UIGroupBox grpLoaiHangHoa;
        private UIRadioButton radNPL;
        private UIRadioButton radSP;
        private UIRadioButton radTB;
        private UICommand ChungTuKemTheo;
        private UIGroupBox uiGroupBox4;
        private EditBox txtChungTu;
        private Label label18;
        private Label label10;
        private Label lblTrangThai;
        private EditBox txtSoToKhai;
        private GridEX gridEX1;
        private UICommand cmdReadExcel;
        private UICommand NhanDuLieu;
        private UICommand ThemHang2;
        private UICommand cmdThemHang1;
        private UICommand cmdReadExcel1;
        private UICommand ThemHang1;
        private UICommand cmdReadExcel2;
        private UICommand ChungTuKemTheo2;
        private EditBox txtSoTiepNhan;
        private Label lblSoTiepNhan;
        private UICommand XacNhan;
        private UICommand FileDinhKem;
        private NumericEditBox txtPhiNganHang;
        private Label label24;
        private Label label25;
        private NumericEditBox txtTrongLuongTinh;
        private EditBox txtMaMid;
        private Label lblMaMid;
        private UICommand Huy;
        private UICommand CanDoiNhapXuat;
        private CalendarCombo ccNgayDK;
        private Label lblNgayDangKy;
        private UICommand cmdNhieuHang1;
        private UICommand cmdNhieuHang;
        private RangeValidator rvSKH;
        private RangeValidator rVTrongLuong;
        private RangeValidator rVTrongLuongTinh;
        private RangeValidator rvLPHQ;
        private RangeValidator rVPBH;
        private RangeValidator rVPVC;
        private RangeValidator rvPhiKHac;
        private RangeValidator rvSLPLTK;
        private UICommand PhanBo;
        private GridEX dgList;
        private UICommand InPhenTN;
        private UICommand cmdXuatExcelHang;
        private UICommand cmdXuatExcelHang1;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterHangToKhai;
        private RequiredFieldValidator rfvHopDong;
        private UICommand cmdInTKA4;
        private UICommand cmdChungTuDinhKem;
        private UICommand cmdVanDon;
        private UICommand cmdHopDong;
        private UICommand cmdGiayPhep;
        private UICommand cmdVanDon1;
        private UICommand cmdHopDong1;
        private UICommand cmdGiayPhep1;
        private UICommand cmdHoaDonThuongMai1;
        private UICommand cmdCO1;
        private UICommand cmDeNghiChuyenCuaKhau1;
        private UICommand cmdHoaDonThuongMai;
        private UICommand cmdCO;
        private UICommand cmDeNghiChuyenCuaKhau;
        private UICommand cmdChungTuDinhKem1;
        private UICommand cmdTruyenDuLieuTuXa;
        private UICommand cmdSend1;
        private UICommand NhanDuLieu1;
        private UICommand XacNhan1;
        private UICommand cmdTruyenDuLieuTuXa1;
        private Label lblPhanLuong;
        private Label lblPhan;
        private UICommand cmdHuyToKhaiDaDuyet;
        private UICommand cmdSuaToKhaiDaDuyet;
        private UICommand cmdFileAnh;
        private UICommand cmdKetQuaXuLy1;
        private UICommand cmdKetQuaXuLy;
        private UICommand cmdChungTuDangAnh;
        private UICommand Separator3;
        private UICommand cmdSuaToKhaiDaDuyet1;
        private UICommand cmdHuyToKhaiDaDuyet1;
        private UICommand Separator4;
        private EditBox txtLyDoSua;
        private Label label26;
        private EditBox txtDeXuatKhac;
        private Label label3;
        private UICommand cmdInTKDTSuaDoiBoSung;
        private UICommand cmdPrint2;
        private UIButton btnNoiDungChinhSua;
        private UICommand ToKhaiViet;
        private ImageList ImageList1;
        internal ImageList ilLarge;
        private ImageList imageList2;
        private UICommand cmdInAnDinhThue;
        private UIRadioButton radHangMau;
        private UICommand cmdChungTuNo1;
        private UICommand cmdChungTuNo;
        private UICommand cmdNPLCungUng;
        private UICommand cmdNPLCungUng1;
        private UICommand cmdInTKTT15;
        private UICommand cmdInTKDTSuaDoiBoSung1;
        private UICommand cmdAnDinhThue1;
        private UICommand cmdInTKTT151;
        private UIPanel TKGT;
        private UIPanelInnerContainer TKGTContainer;
        private GridEX dgToKhaiTriGia;
        private UIPanel TKTG23;
        private UIPanelInnerContainer TKTG23Container;
        private GridEX dglistTKTK23;
        private UICommand cmdToKhaiTriGia;
        private UICommand ToKhaiTriGia1;
        private UICommand cmdToKhaiTGPP21;
        private UICommand cmdToKhaiTGPP31;
        private UICommand cmdToKhaiTGPP2;
        private UICommand cmdToKhaiTGPP3;
        private UICommand ToKhaiTriGia;
        private UICommand cmdToKhaiTriGia1;
        private UICommand cmdGiayNopTien;
        private EditBox txtChucVu;
        private Label label27;
        private UICommand cmdContainer1;
        private UICommand cmdContainer;
        private UIGroupBox uiDamBaoNghiaVuNT;
        private CalendarCombo ccDamBaoNopThue_NgayKetThuc;
        private CalendarCombo ccDamBaoNopThue_NgayBatDau;
        private EditBox txtDamBaoNopThue_HinhThuc;
        private UICheckBox chkDamBaoNopThue_isValue;
        private Label label38;
        private Label label41;
        private Label label40;
        private Label label39;
        private NumericEditBox txtDamBaoNopThue_TriGia;
        private UIGroupBox uiAnHanThue;
        private EditBox txtAnHanThue_LyDo;
        private UICheckBox chkAnHangThue;
        private Label label37;
        private Label label45;
        private NumericEditBox txtAnHanThue_ThoiGian;
        private UICommand GiayKiemTra1;
        private UICommand ChungThuGiamDinh1;
        private UICommand GiayKiemTra;
        private UICommand ChungThuGiamDinh;
        private UICommand cmdInTKTCTT1961;
        private UICommand cmdInTKTCTT196;
        private CalendarCombo ccNgayTN;
        private Label label28;
        private EditBox txtHDPL;
        private Label label32;
        private PictureBox pictureBox1;
        private Timer timer1;
        //private Company.KDT.SHARE.Components.Controls.TKMDVersionControlV tkmdVersionControlV1;
        private ToolTip toolTip1;
        private Label label35;
        private UICommand cmdChungTuDangAnh1;
        private UICommand cmdBangKeHangHoa_HD1;
        private UICommand cmdBangKeHangHoa_HD;
        private UICommand cmdInPhieuKetQuaKiemTra1;
        private UICommand cmdInPhieuKetQuaKiemTra;
        private UICommand cmdInPhieuKiemTraHangHoa1;
        private UICommand cmdInPhieuKiemTraHangHoa;
        private UICommand cmdLayPhanHoiLai1;
        private UICommand cmdLayPhanHoiLai;
        private UICommand cmdPhanHoiSoTiepNhan1;
        private UICommand cmdPhanHoiSoToKhai1;
        private UICommand cmdPhanHoiPhanLuong1;
        private UICommand cmdPhanHoiSoTiepNhan;
        private UICommand cmdPhanHoiSoToKhai;
        private UICommand cmdPhanHoiPhanLuong;
        private UICommand CanDoiNhapXuat1;
        private Company.KDT.SHARE.Components.Controls.TKMDVersionControlV tkmdVersionControlV2;
        private UICommand cmdPhuKienToKhai;
        private UICommand cmdPhuKienToKhai1;
        private EditBox txtSoToKhaiGiay;
        private Label label42;
        private Label label36;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDuaVaoTK;
        private UICommand cmdSendTK1;
        private UICommand cmdSendTK;
    }
}
