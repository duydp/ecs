﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DanhMucHaiQuan;

namespace Company.Interface.KDT.GC
{
    public partial class GiamSatThieuHuyReadExcelForm : Company.Interface.BaseForm
    {
        public GiamSatTieuHuy giamSatTieuHuy;
        List<HangGSTieuHuy> HHCollection = new List<HangGSTieuHuy>();
        public bool ImportSucess = true;
        public GiamSatThieuHuyReadExcelForm()
        {
            InitializeComponent();
        }

        private void GiamSatThieuHuyReadExcelForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                errorProvider.SetError(txtRow, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                errorProvider.SetIconPadding(txtRow, 8);
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char STTColumn = Convert.ToChar(txtSTTColumn.Text.Trim());
            int STTCol = ConvertCharToInt(STTColumn);

            char MaHHColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char LuongTieuHuyColumn = Convert.ToChar(txtSLTHColumn.Text.Trim());
            int LuongTieuHuyCol = ConvertCharToInt(LuongTieuHuyColumn);

            char LoaiHHColumn = Convert.ToChar(txtLoaiHHColumn.Text.Trim());
            int LoaiHHCol = ConvertCharToInt(LoaiHHColumn);

            string errorTotal = "";
            string errorSTT = "";
            string errorMaHangHoa = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorLoaiHangHoa = "";
            string errorSoLuong = "";
            string errorSoLuongValid = "";
            IList<DonViTinh> DVTCollection = DonViTinh.SelectCollectionAll();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        HangGSTieuHuy hangGSTieuHuy = new HangGSTieuHuy();
                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            hangGSTieuHuy.STTHang = Convert.ToInt32(wsr.Cells[STTCol].Value);
                            if (hangGSTieuHuy.STTHang.ToString().Length == 0)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.MaHang + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorSTT += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.MaHang + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            hangGSTieuHuy.MaHang = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (hangGSTieuHuy.MaHang.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.MaHang + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.MaHang + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            hangGSTieuHuy.TenHang = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                            if (hangGSTieuHuy.TenHang.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.TenHang + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.TenHang + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            hangGSTieuHuy.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[DVTCol].Value).Trim());
                            if (hangGSTieuHuy.DVT_ID.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.DVT_ID + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (DonViTinh item in DVTCollection)
                                {
                                    if (item.ID == hangGSTieuHuy.DVT_ID)
                                    {
                                        isExits = true;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.DVT_ID + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.DVT_ID + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            hangGSTieuHuy.LoaiHang = Convert.ToInt32(wsr.Cells[LoaiHHCol].Value);
                            if (hangGSTieuHuy.LoaiHang.ToString().Trim().Length == 0)
                            {
                                errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.LoaiHang + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.LoaiHang + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            hangGSTieuHuy.SoLuong = Convert.ToDecimal(wsr.Cells[LuongTieuHuyCol].Value);
                            if (hangGSTieuHuy.SoLuong.ToString().Trim().Length == 0)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.SoLuong + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(hangGSTieuHuy.SoLuong, 4) != hangGSTieuHuy.SoLuong)
                                {
                                    errorSoLuongValid += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.SoLuong + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hangGSTieuHuy.SoLuong + "]\n";
                            isAdd = false;
                        }
                        if (isAdd)
                            HHCollection.Add(hangGSTieuHuy);
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorSTT))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorSTT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorLoaiHangHoa))
                errorTotal += "\n - [STT] -[LOẠI HÀNG HÓA] : \n" + errorLoaiHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG . NGUYÊN PHỤ LIỆU NHẬP : 1 , SẢN PHẨM NHẬP : 2\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ.\n";
            if (!String.IsNullOrEmpty(errorSoLuong))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG HỦY] : \n" + errorSoLuong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG HỦY] : \n" + errorSoLuongValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (HangGSTieuHuy goodItem in HHCollection)
                {
                    giamSatTieuHuy.HangGSTieuHuys.Add(goodItem);
                }
                ImportSucess = true;
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_ScrapInformation();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
