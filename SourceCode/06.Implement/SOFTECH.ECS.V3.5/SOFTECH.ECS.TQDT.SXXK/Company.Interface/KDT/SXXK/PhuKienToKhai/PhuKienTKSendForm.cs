﻿using System;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class PhuKienTKSendForm : BaseForm
    {

        public KDT_SXXK_PhuKienDangKy pkdk = new KDT_SXXK_PhuKienDangKy();
        private KDT_SXXK_PhuKienDetail pkdt = new KDT_SXXK_PhuKienDetail();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        //Comment
        //-----------------------------------------------------------------------------------------

        public PhuKienTKSendForm()
        {
            InitializeComponent();
          
        }

        private void cmMainPKHD_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.SendV5();
                    //this.send();
                    break;
                //case "HuyKhaiBao":
                //    this.CanceldV3();
                //    //this.Huy();
                //    break;
                case "NhanDuLieuPK":
                    //this.NhanDuLieuPK12();
                    FeedBackV5();
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
                //case "InPhieuTN": this.inPhieuTN(); break;
            }
        }
    
        private void KetQuaXuLyPK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.pkdk.ID;
            form.DeclarationIssuer = DeclarationIssuer.PHUKIEN_TOKHAI;
            form.ShowDialog(this);
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = pkdk.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = pkdk.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.PHUKIEN_TOKHAI,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.PHUKIEN_TOKHAI,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = pkdk.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(pkdk.MaHaiQuan.Trim())),
                                                  //Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan
                                                  Identity = pkdk.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }
      
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.PhuKienSendHandler(pkdk, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (pkdk.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    pkdk = KDT_SXXK_PhuKienDangKy.LoadFull(pkdk.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.pkdk.ListPhuKien.Count > 0)
                    {
                       
                        string returnMessage = string.Empty;
                        pkdk.GUIDSTR = Guid.NewGuid().ToString();
                        PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(pkdk, GlobalSettings.TEN_DON_VI);
                    
                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = pkdk.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(pkdk.MaHaiQuan)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan
                                  Identity = pkdk.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = phukien.Issuer,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = pkdk.GUIDSTR,
                              },
                              phukien
                            );
                        pkdk.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(pkdk.ID, MessageTitle.KhaiBaoPhuKien);
                            XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                            sendXML.master_id = pkdk.ID;
                            // sendXML.msg = msgSend;
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                          
                            pkdk.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        private void PhuKienTKSendForm_Load(object sender, EventArgs e)
        {
            khoitao_DuLieuChuan();
            ctrMaHaiQuan.Code = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(GlobalSettings.MA_HAI_QUAN).Trim();
            if (pkdk != null && pkdk.ID > 0)
            {
                SetPKDK();
            }
            dgList.DataSource = pkdk.ListPhuKien;
        }

        private void add()
        {
          
            if (cbLoaiPhuKien.Value == null || cbLoaiPhuKien.Value.ToString() == "")
            {
                ShowMessage("Bạn chưa chọn loại phụ kiện.", false);
                return;
            }
            string id_loaiphukien = cbLoaiPhuKien.Value.ToString();
           
            //long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
            if (pkdk.ID == 0)
            {
                GetPKDK();
                pkdk.TrangThaiXuLy = -1;
                pkdk.Insert();
            }
            else
                pkdk.Update();
             
            ShowForm(id_loaiphukien);
        }
        private void save()
        {
            try
            {
                GetPKDK();
                if (pkdk.ID == 0)
                {
                    pkdk.TrangThaiXuLy = -1;
                    pkdk.Insert();
                }
                else
                    pkdk.Update();
                ShowMessage("Lưu phụ kiện thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi lưu phụ kiện ", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }
        private void ShowForm(string id_loaiphukien)
        {
            switch (id_loaiphukien.Trim())
            {

                case "501": { showNgayHT(true, 0); } break;//Phụ kiện sửa thông tin ngày đưa vào thanh khoản (501)
                case "502": { showSuaHD(true, 0); } break;//Phụ kiện sửa thông tin Hợp đồng (502)
                case "503": { showSuaMaHang(true, 0); } break;
                case "504": { showSuaThongTinHang(true, 0,"504"); } break;
                case "505": { showSuaThongTinHang(true, 0,"505"); } break;


            }
        }
         private void GetPKDK()
         {
             pkdk.DeXuatKhac = txtNoiDung.Text.Trim();
             pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
             pkdk.NgayPhuKien = ccNgayPhuKien.Value;
             pkdk.MaHaiQuan = ctrMaHaiQuan.Code;
             pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
             pkdk.TKMD_ID = TKMD.ID;
             pkdk.SoToKhai = txtSoToKhai.Text.Trim();//TKMD.LoaiVanDon;
             pkdk.NgayDangKy = TKMD.NgayDangKy;
             if (TKMD.MaLoaiHinh.Contains("V"))
                 pkdk.MaLoaiHinh = TKMD.MaLoaiHinh.Substring(2);
             else
                 pkdk.MaLoaiHinh = TKMD.MaLoaiHinh;
         }
         private void SetPKDK()
         {
             txtSoPhuKien.Text = pkdk.SoPhuKien;
             txtNoiDung.Text = pkdk.DeXuatKhac;
             txtSoToKhai.Text = pkdk.SoToKhai;
             txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
             ccNgaTiepNhan.Value = pkdk.NgayTiepNhan;
             ccNgayPhuKien.Value = pkdk.NgayPhuKien;
             ctrMaHaiQuan.Code = pkdk.MaHaiQuan.Trim();
         }
        private long checkExitsLoaiPhuKien(string maPhuKien)
        {
            foreach (KDT_SXXK_LoaiPhuKien item in pkdk.ListPhuKien)
            {
                if (item.MaPhuKien.Trim() == maPhuKien)
                {
                   return item.ID;
                }
            }
            return 0;
        }
        private void showNgayHT(bool isAdd,long id)
        {
            try
            {
                
                FormNgayHoanThanh f = new FormNgayHoanThanh();

                if (checkExitsLoaiPhuKien("501") > 0)
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK = KDT_SXXK_LoaiPhuKien.LoaiPhuKien_Load(checkExitsLoaiPhuKien("501"));
                    f.loaiPK = LoaiPK;
                }
                else
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK.MaPhuKien = "501";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.Insert();
                    pkdk.ListPhuKien.Add(LoaiPK);
                    f.loaiPK = LoaiPK;

                }
            
                f.ShowDialog();

                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }

        private void showSuaHD(bool isAdd, long id)
        {
            try
            {
               
                FormSuaHopDong f = new FormSuaHopDong();

                if (checkExitsLoaiPhuKien("502") > 0)
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK = KDT_SXXK_LoaiPhuKien.LoaiPhuKien_Load(checkExitsLoaiPhuKien("502"));
                    f.loaiPK = LoaiPK;
                }
                else
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK.MaPhuKien = "502";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    pkdk.ListPhuKien.Add(LoaiPK);
                    f.loaiPK = LoaiPK;
                }
                f.ShowDialog();

                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }
        private void showSuaMaHang(bool isAdd, long id)
        {
            try
            {

                PhuKienSuaMaHang f = new PhuKienSuaMaHang();
                f.SoTKVNACCS = txtSoToKhai.Text.Trim();
                f.TKMD_ID = TKMD.ID;
                if (checkExitsLoaiPhuKien("503") > 0)
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK = KDT_SXXK_LoaiPhuKien.LoaiPhuKien_Load(checkExitsLoaiPhuKien("503"));
                    f.loaiPK = LoaiPK;
                }
                else
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK.MaPhuKien = "503";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    pkdk.ListPhuKien.Add(LoaiPK);
                    f.loaiPK = LoaiPK;
                }
                f.ShowDialog();

                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }
        private void showSuaThongTinHang(bool isAdd, long id, string maphukien)
        {
            try
            {

                PhuKienSuaThongTinHang f = new PhuKienSuaThongTinHang();
                f.SoTKVNACCS = txtSoToKhai.Text.Trim();
                if (checkExitsLoaiPhuKien(maphukien) > 0)
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK = KDT_SXXK_LoaiPhuKien.LoaiPhuKien_Load(checkExitsLoaiPhuKien(maphukien));
                    f.loaiPK = LoaiPK;
                }
                else
                {
                    KDT_SXXK_LoaiPhuKien LoaiPK = new KDT_SXXK_LoaiPhuKien();
                    LoaiPK.MaPhuKien = maphukien;
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    pkdk.ListPhuKien.Add(LoaiPK);
                    f.loaiPK = LoaiPK;
                }
                f.ShowDialog();

                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        /// 
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            createLoaiPhuKien();
            if (TKMD.MaLoaiHinh.Contains("V"))
                txtSoToKhai.Text = TKMD.LoaiVanDon;
            else
                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            ccNgayDangKy.Value = TKMD.NgayDangKy;
            if (pkdk.TrangThaiXuLy == 0 && pkdk.ID>0)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled =Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo thông tin về loại phụ kiện.
        /// </summary>
        private void createLoaiPhuKien()
        {
            cbLoaiPhuKien.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.SelectAll();
            cbLoaiPhuKien.DisplayMember = "TenLoaiPhuKien";
            cbLoaiPhuKien.ValueMember = "ID_LoaiPhuKien";
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string maloaiPK = dgList.CurrentRow.Cells["MaPhuKien"].Value.ToString().Trim();
                long id= Convert.ToUInt32(dgList.CurrentRow.Cells["ID"].Value);
                switch (maloaiPK)
                {
                    case "501":
                        showNgayHT(false, id);
                        break;
                    case "502":
                        showSuaHD(false, id);
                        break;
                    case "503":
                        showSuaMaHang(false, id);
                        break;
                    case "504":
                        showSuaThongTinHang(false, id,"504");
                        break;
                    case "505":
                        showSuaThongTinHang(false, id, "505");
                        break;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
                sendXML.master_id = pkdk.ID;
                int ID = Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value.ToString());

                if (ID > 0)
                {
                    if (pkdk.TrangThaiXuLy == 0 || pkdk.TrangThaiXuLy == 1 || sendXML.Load())
                    {
                        ShowMessage("Phụ kiện đã được khai báo không thể xóa.", false);
                        return;
                    }
                    if (ShowMessage("Bạn có muốn xóa số Container này không?", true) == "Yes")
                    {
                        KDT_SXXK_LoaiPhuKien loaipk = new KDT_SXXK_LoaiPhuKien();
                        loaipk.DeleteFull(ID);
                        dgList.CurrentRow.Delete();
                    }
                }
             
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
