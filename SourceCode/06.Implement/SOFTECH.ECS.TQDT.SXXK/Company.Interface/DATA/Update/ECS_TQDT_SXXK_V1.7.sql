/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 03/05/2012 10:37:22 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_SXXK_DinhMuc_InsertUpdate_KTX]'
GO
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, August 25, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate_KTX]  
 @MaHaiQuan char(6),  
 @MaDoanhNghiep varchar(14),  
 @MaSanPham varchar(30),  
 @MaNguyenPhuLieu varchar(30),  
 @DinhMucSuDung numeric(18, 8),  
 @TyLeHaoHut numeric(18, 5),  
 @DinhMucChung numeric(18, 8),  
 @GhiChu varchar(250)  
AS  
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)  
 BEGIN  
  UPDATE  
   [dbo].[t_SXXK_DinhMuc]   
  SET  
   [DinhMucSuDung] = @DinhMucSuDung,  
   [TyLeHaoHut] = @TyLeHaoHut,  
   [DinhMucChung] = @DinhMucChung,  
   [GhiChu] = @GhiChu  
  WHERE  
   [MaHaiQuan] = @MaHaiQuan  
   AND [MaDoanhNghiep] = @MaDoanhNghiep  
   AND [MaSanPham] = @MaSanPham  
   AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_SXXK_DinhMuc]  
 (  
   [MaHaiQuan],  
   [MaDoanhNghiep],  
   [MaSanPham],  
   [MaNguyenPhuLieu],  
   [DinhMucSuDung],  
   [TyLeHaoHut],  
   [DinhMucChung],  
   [GhiChu]  
 )  
 VALUES  
 (  
   @MaHaiQuan,  
   @MaDoanhNghiep,  
   @MaSanPham,  
   @MaNguyenPhuLieu,  
   @DinhMucSuDung,  
   @TyLeHaoHut,  
   @DinhMucChung,  
   @GhiChu  
 )   
 END  
 

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.7', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.7', getdate(), null)
	end  
 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
