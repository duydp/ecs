using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ChungTuDinhKem_ChiTiet : ICloneable
    {
        #region Properties.

        public long ID { set; get; }
        public long ChungTuKemID { set; get; }
        public string FileName { set; get; }
        public decimal FileSize { set; get; }
        public byte[] NoiDung { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static List<KDT_VNACC_ChungTuDinhKem_ChiTiet> ConvertToCollection(IDataReader reader)
        {
            List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();
            while (reader.Read())
            {
                KDT_VNACC_ChungTuDinhKem_ChiTiet entity = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTuKemID"))) entity.ChungTuKemID = reader.GetInt64(reader.GetOrdinal("ChungTuKemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
                if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static bool Find(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection, long id)
        {
            foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuDinhKem_ChiTiet VALUES(@ChungTuKemID, @FileName, @FileSize, @NoiDung)";
            string update = "UPDATE t_KDT_VNACC_ChungTuDinhKem_ChiTiet SET ChungTuKemID = @ChungTuKemID, FileName = @FileName, FileSize = @FileSize, NoiDung = @NoiDung WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuDinhKem_ChiTiet WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ChungTuKemID", SqlDbType.BigInt, "ChungTuKemID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ChungTuKemID", SqlDbType.BigInt, "ChungTuKemID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuDinhKem_ChiTiet VALUES(@ChungTuKemID, @FileName, @FileSize, @NoiDung)";
            string update = "UPDATE t_KDT_VNACC_ChungTuDinhKem_ChiTiet SET ChungTuKemID = @ChungTuKemID, FileName = @FileName, FileSize = @FileSize, NoiDung = @NoiDung WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuDinhKem_ChiTiet WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ChungTuKemID", SqlDbType.BigInt, "ChungTuKemID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ChungTuKemID", SqlDbType.BigInt, "ChungTuKemID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static KDT_VNACC_ChungTuDinhKem_ChiTiet Load(long id)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static List<KDT_VNACC_ChungTuDinhKem_ChiTiet> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static List<KDT_VNACC_ChungTuDinhKem_ChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static List<KDT_VNACC_ChungTuDinhKem_ChiTiet> SelectCollectionBy_ChungTuKemID(long chungTuKemID)
        {
            IDataReader reader = SelectReaderBy_ChungTuKemID(chungTuKemID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_ChungTuKemID(long chungTuKemID)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_ChungTuKemID(long chungTuKemID)
        {
            const string spName = "p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertKDT_VNACC_ChungTuDinhKem_ChiTiet(long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
        {
            KDT_VNACC_ChungTuDinhKem_ChiTiet entity = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
            entity.ChungTuKemID = chungTuKemID;
            entity.FileName = fileName;
            entity.FileSize = fileSize;
            entity.NoiDung = noiDung;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
            db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
            db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateKDT_VNACC_ChungTuDinhKem_ChiTiet(long id, long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
        {
            KDT_VNACC_ChungTuDinhKem_ChiTiet entity = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
            entity.ID = id;
            entity.ChungTuKemID = chungTuKemID;
            entity.FileName = fileName;
            entity.FileSize = fileSize;
            entity.NoiDung = noiDung;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
            db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
            db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateKDT_VNACC_ChungTuDinhKem_ChiTiet(long id, long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
        {
            KDT_VNACC_ChungTuDinhKem_ChiTiet entity = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
            entity.ID = id;
            entity.ChungTuKemID = chungTuKemID;
            entity.FileName = fileName;
            entity.FileSize = fileSize;
            entity.NoiDung = noiDung;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
            db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
            db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteKDT_VNACC_ChungTuDinhKem_ChiTiet(long id)
        {
            KDT_VNACC_ChungTuDinhKem_ChiTiet entity = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_ChungTuKemID(long chungTuKemID)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KDT_VNACC_ChungTuDinhKem_ChiTiet item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion


        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
    }
}