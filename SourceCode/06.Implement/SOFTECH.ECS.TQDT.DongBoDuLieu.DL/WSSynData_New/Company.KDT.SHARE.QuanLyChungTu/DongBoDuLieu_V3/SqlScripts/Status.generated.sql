-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DBDL_Status_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Insert]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
INSERT INTO [dbo].[t_KDT_GC_DBDL_Status]
(
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
)
VALUES
(
	@GUIDSTR,
	@MSG_STATUS,
	@MSG_TYPE,
	@CREATE_TIME
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Update]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS

UPDATE
	[dbo].[t_KDT_GC_DBDL_Status]
SET
	[MSG_STATUS] = @MSG_STATUS,
	[MSG_TYPE] = @MSG_TYPE,
	[CREATE_TIME] = @CREATE_TIME
WHERE
	[GUIDSTR] = @GUIDSTR

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
IF EXISTS(SELECT [GUIDSTR] FROM [dbo].[t_KDT_GC_DBDL_Status] WHERE [GUIDSTR] = @GUIDSTR)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DBDL_Status] 
		SET
			[MSG_STATUS] = @MSG_STATUS,
			[MSG_TYPE] = @MSG_TYPE,
			[CREATE_TIME] = @CREATE_TIME
		WHERE
			[GUIDSTR] = @GUIDSTR
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_DBDL_Status]
	(
			[GUIDSTR],
			[MSG_STATUS],
			[MSG_TYPE],
			[CREATE_TIME]
	)
	VALUES
	(
			@GUIDSTR,
			@MSG_STATUS,
			@MSG_TYPE,
			@CREATE_TIME
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Delete]
	@GUIDSTR nvarchar(36)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DBDL_Status] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_Load]
	@GUIDSTR nvarchar(36)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_GC_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM [dbo].[t_KDT_GC_DBDL_Status] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DBDL_Status_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_GC_DBDL_Status]	

GO

