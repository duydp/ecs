﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    partial class ToKhaiMauDichManageForm
    {
        private UIGroupBox uiGroupBox1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichManageForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.khaibaoCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.HuyCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.NhanDuLieuCTMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPhanLuong = new System.Windows.Forms.ToolStripMenuItem();
            this.xacnhanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.print = new System.Windows.Forms.ToolStripMenuItem();
            this.PhieuTNMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiA4MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiTQDTMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInTokhaiTaiCho = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiSuaDoi = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInBangKe = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInBangKeHangHoa = new System.Windows.Forms.ToolStripMenuItem();
            this.mniAnDinhThue = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInPhieuKiemTra_ChungTu = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInPhieuKiemTra_HangHoa = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaTờKhaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCSDaDuyet = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHuongDan = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSuaToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHuyToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImport = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportMSG = new System.Windows.Forms.ToolStripMenuItem();
            this.mniExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mniExportTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mniCapNhatToKhaiSua = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCapSoToKhaiGiay = new System.Windows.Forms.ToolStripMenuItem();
            this.imageListSmall = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timToKhai1 = new Company.Interface.Controls.TimToKhai();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdDelete1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.SaoChep3 = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.Export2 = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdCancel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdSingleDownload2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.XacNhan1 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdLayPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdNhanTrangThaiPhanLuong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanTrangThaiPhanLuong");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.SaoChepToKhaiHang1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.SaoChepALL1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepALL = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepToKhaiHang = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.Export1 = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import1 = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdXuatToKhaiChoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhaiChoPhongKhai");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdLayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdNhanTrangThaiPhanLuong = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanTrangThaiPhanLuong");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.ctrCoQuanHQ = new Company.Interface.Controls.DonViHaiQuanControl();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 26);
            this.grbMain.Size = new System.Drawing.Size(971, 434);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 26);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(971, 434);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.imageListSmall;
            this.dgList.Location = new System.Drawing.Point(0, 77);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(971, 316);
            this.dgList.TabIndex = 53;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.khaibaoCTMenu,
            this.HuyCTMenu,
            this.NhanDuLieuCTMenu,
            this.mnuPhanLuong,
            this.xacnhanToolStripMenuItem,
            this.SaoChepCha,
            this.print,
            this.xóaTờKhaiToolStripMenuItem,
            this.mnuCSDaDuyet,
            this.mnuHuongDan,
            this.mnuSuaToKhai,
            this.mnuHuyToKhai,
            this.mniImport,
            this.mniExport,
            this.mniCapNhatToKhaiSua,
            this.mnuCapSoToKhaiGiay});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(346, 356);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // khaibaoCTMenu
            // 
            this.khaibaoCTMenu.Image = ((System.Drawing.Image)(resources.GetObject("khaibaoCTMenu.Image")));
            this.khaibaoCTMenu.Name = "khaibaoCTMenu";
            this.khaibaoCTMenu.Size = new System.Drawing.Size(345, 22);
            this.khaibaoCTMenu.Text = "Khai báo";
            this.khaibaoCTMenu.Visible = false;
            this.khaibaoCTMenu.Click += new System.EventHandler(this.khaibaoCTMenu_Click);
            // 
            // HuyCTMenu
            // 
            this.HuyCTMenu.Image = ((System.Drawing.Image)(resources.GetObject("HuyCTMenu.Image")));
            this.HuyCTMenu.Name = "HuyCTMenu";
            this.HuyCTMenu.Size = new System.Drawing.Size(345, 22);
            this.HuyCTMenu.Text = "Hủy khai báo";
            this.HuyCTMenu.Visible = false;
            this.HuyCTMenu.Click += new System.EventHandler(this.HuyCTMenu_Click);
            // 
            // NhanDuLieuCTMenu
            // 
            this.NhanDuLieuCTMenu.Image = ((System.Drawing.Image)(resources.GetObject("NhanDuLieuCTMenu.Image")));
            this.NhanDuLieuCTMenu.Name = "NhanDuLieuCTMenu";
            this.NhanDuLieuCTMenu.Size = new System.Drawing.Size(345, 22);
            this.NhanDuLieuCTMenu.Text = "Nhận dữ liệu";
            this.NhanDuLieuCTMenu.Visible = false;
            this.NhanDuLieuCTMenu.Click += new System.EventHandler(this.NhanDuLieuCTMenu_Click);
            // 
            // mnuPhanLuong
            // 
            this.mnuPhanLuong.Image = ((System.Drawing.Image)(resources.GetObject("mnuPhanLuong.Image")));
            this.mnuPhanLuong.Name = "mnuPhanLuong";
            this.mnuPhanLuong.Size = new System.Drawing.Size(345, 22);
            this.mnuPhanLuong.Text = "Nhận phân luồng";
            this.mnuPhanLuong.Visible = false;
            this.mnuPhanLuong.Click += new System.EventHandler(this.mnuPhanLuong_Click);
            // 
            // xacnhanToolStripMenuItem
            // 
            this.xacnhanToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xacnhanToolStripMenuItem.Image")));
            this.xacnhanToolStripMenuItem.Name = "xacnhanToolStripMenuItem";
            this.xacnhanToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.xacnhanToolStripMenuItem.Text = "Xác nhận";
            this.xacnhanToolStripMenuItem.Visible = false;
            this.xacnhanToolStripMenuItem.Click += new System.EventHandler(this.xacnhanToolStripMenuItem_Click);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH});
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(345, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // saoChepTK
            // 
            this.saoChepTK.Image = ((System.Drawing.Image)(resources.GetObject("saoChepTK.Image")));
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(190, 22);
            this.saoChepTK.Text = "Sao chép tờ khai";
            this.saoChepTK.Click += new System.EventHandler(this.saoChepTK_Click);
            // 
            // saoChepHH
            // 
            this.saoChepHH.Image = ((System.Drawing.Image)(resources.GetObject("saoChepHH.Image")));
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(190, 22);
            this.saoChepHH.Text = "Sao chép cả hàng hóa";
            this.saoChepHH.Click += new System.EventHandler(this.saoChepHH_Click);
            // 
            // print
            // 
            this.print.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PhieuTNMenuItem,
            this.ToKhaiMenuItem,
            this.ToKhaiA4MenuItem,
            this.ToKhaiTQDTMenuItem,
            this.mniInTokhaiTaiCho,
            this.mniInToKhaiSuaDoi,
            this.mniInBangKe,
            this.mniInBangKeHangHoa,
            this.mniAnDinhThue,
            this.mniInPhieuKiemTra_ChungTu,
            this.mniInPhieuKiemTra_HangHoa});
            this.print.Image = ((System.Drawing.Image)(resources.GetObject("print.Image")));
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(345, 22);
            this.print.Text = "In ";
            // 
            // PhieuTNMenuItem
            // 
            this.PhieuTNMenuItem.Image = global::Company.Interface.Properties.Resources.kdeprint_printer;
            this.PhieuTNMenuItem.Name = "PhieuTNMenuItem";
            this.PhieuTNMenuItem.Size = new System.Drawing.Size(301, 22);
            this.PhieuTNMenuItem.Text = "Phiếu tiếp nhận";
            this.PhieuTNMenuItem.Visible = false;
            this.PhieuTNMenuItem.Click += new System.EventHandler(this.PhieuTNMenuItem_Click);
            // 
            // ToKhaiMenuItem
            // 
            this.ToKhaiMenuItem.Image = global::Company.Interface.Properties.Resources.kdeprint_printer;
            this.ToKhaiMenuItem.Name = "ToKhaiMenuItem";
            this.ToKhaiMenuItem.Size = new System.Drawing.Size(301, 22);
            this.ToKhaiMenuItem.Text = "Tờ khai";
            this.ToKhaiMenuItem.Visible = false;
            this.ToKhaiMenuItem.Click += new System.EventHandler(this.ToKhaiMenuItem_Click);
            // 
            // ToKhaiA4MenuItem
            // 
            this.ToKhaiA4MenuItem.Image = global::Company.Interface.Properties.Resources.kdeprint_printer;
            this.ToKhaiA4MenuItem.Name = "ToKhaiA4MenuItem";
            this.ToKhaiA4MenuItem.Size = new System.Drawing.Size(301, 22);
            this.ToKhaiA4MenuItem.Text = "Tờ khai A4";
            this.ToKhaiA4MenuItem.Visible = false;
            this.ToKhaiA4MenuItem.Click += new System.EventHandler(this.ToKhaiA4MenuItem_Click);
            // 
            // ToKhaiTQDTMenuItem
            // 
            this.ToKhaiTQDTMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ToKhaiTQDTMenuItem.Image")));
            this.ToKhaiTQDTMenuItem.Name = "ToKhaiTQDTMenuItem";
            this.ToKhaiTQDTMenuItem.Size = new System.Drawing.Size(301, 22);
            this.ToKhaiTQDTMenuItem.Text = "In tờ khai thông quan (Thông tư 196)";
            this.ToKhaiTQDTMenuItem.Click += new System.EventHandler(this.ToKhaiTQDTMenuItem_Click);
            // 
            // mniInTokhaiTaiCho
            // 
            this.mniInTokhaiTaiCho.Image = ((System.Drawing.Image)(resources.GetObject("mniInTokhaiTaiCho.Image")));
            this.mniInTokhaiTaiCho.Name = "mniInTokhaiTaiCho";
            this.mniInTokhaiTaiCho.Size = new System.Drawing.Size(301, 22);
            this.mniInTokhaiTaiCho.Text = "In tờ khai tại chỗ (Thông tư 196)";
            this.mniInTokhaiTaiCho.Click += new System.EventHandler(this.mniInTokhaiTaiCho_Click);
            // 
            // mniInToKhaiSuaDoi
            // 
            this.mniInToKhaiSuaDoi.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiSuaDoi.Image")));
            this.mniInToKhaiSuaDoi.Name = "mniInToKhaiSuaDoi";
            this.mniInToKhaiSuaDoi.Size = new System.Drawing.Size(301, 22);
            this.mniInToKhaiSuaDoi.Text = "In tờ khai sửa đổi bổ sung";
            this.mniInToKhaiSuaDoi.Click += new System.EventHandler(this.mniInToKhaiSuaDoi_Click);
            // 
            // mniInBangKe
            // 
            this.mniInBangKe.Image = ((System.Drawing.Image)(resources.GetObject("mniInBangKe.Image")));
            this.mniInBangKe.Name = "mniInBangKe";
            this.mniInBangKe.Size = new System.Drawing.Size(301, 22);
            this.mniInBangKe.Text = "In bảng kê Container kèm theo";
            this.mniInBangKe.Click += new System.EventHandler(this.mniInBangKe_Click);
            // 
            // mniInBangKeHangHoa
            // 
            this.mniInBangKeHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("mniInBangKeHangHoa.Image")));
            this.mniInBangKeHangHoa.Name = "mniInBangKeHangHoa";
            this.mniInBangKeHangHoa.Size = new System.Drawing.Size(301, 22);
            this.mniInBangKeHangHoa.Text = "In bảng kê hợp đồng/ đơn hàng";
            this.mniInBangKeHangHoa.Click += new System.EventHandler(this.mniInBangKeHangHoa_Click);
            // 
            // mniAnDinhThue
            // 
            this.mniAnDinhThue.Image = ((System.Drawing.Image)(resources.GetObject("mniAnDinhThue.Image")));
            this.mniAnDinhThue.Name = "mniAnDinhThue";
            this.mniAnDinhThue.Size = new System.Drawing.Size(301, 22);
            this.mniAnDinhThue.Text = "In ấn định thuế tờ khai";
            this.mniAnDinhThue.Click += new System.EventHandler(this.mniAnDinhThue_Click);
            // 
            // mniInPhieuKiemTra_ChungTu
            // 
            this.mniInPhieuKiemTra_ChungTu.Image = ((System.Drawing.Image)(resources.GetObject("mniInPhieuKiemTra_ChungTu.Image")));
            this.mniInPhieuKiemTra_ChungTu.Name = "mniInPhieuKiemTra_ChungTu";
            this.mniInPhieuKiemTra_ChungTu.Size = new System.Drawing.Size(301, 22);
            this.mniInPhieuKiemTra_ChungTu.Text = "In phiếu ghi kết quả kiểm tra chứng từ giấy";
            this.mniInPhieuKiemTra_ChungTu.Click += new System.EventHandler(this.mniInPhieuKiemTra_ChungTu_Click);
            // 
            // mniInPhieuKiemTra_HangHoa
            // 
            this.mniInPhieuKiemTra_HangHoa.Image = ((System.Drawing.Image)(resources.GetObject("mniInPhieuKiemTra_HangHoa.Image")));
            this.mniInPhieuKiemTra_HangHoa.Name = "mniInPhieuKiemTra_HangHoa";
            this.mniInPhieuKiemTra_HangHoa.Size = new System.Drawing.Size(301, 22);
            this.mniInPhieuKiemTra_HangHoa.Text = "In phiếu ghi kết quả kiểm tra hàng hóa";
            this.mniInPhieuKiemTra_HangHoa.Click += new System.EventHandler(this.mniInPhieuKiemTra_HangHoa_Click);
            // 
            // xóaTờKhaiToolStripMenuItem
            // 
            this.xóaTờKhaiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xóaTờKhaiToolStripMenuItem.Image")));
            this.xóaTờKhaiToolStripMenuItem.Name = "xóaTờKhaiToolStripMenuItem";
            this.xóaTờKhaiToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.xóaTờKhaiToolStripMenuItem.Text = "Xóa tờ khai";
            this.xóaTờKhaiToolStripMenuItem.Click += new System.EventHandler(this.xóaTờKhaiToolStripMenuItem_Click);
            // 
            // mnuCSDaDuyet
            // 
            this.mnuCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("mnuCSDaDuyet.Image")));
            this.mnuCSDaDuyet.Name = "mnuCSDaDuyet";
            this.mnuCSDaDuyet.Size = new System.Drawing.Size(345, 22);
            this.mnuCSDaDuyet.Text = "Chuyển Trạng Thái";
            this.mnuCSDaDuyet.Visible = false;
            this.mnuCSDaDuyet.Click += new System.EventHandler(this.mnuCSDaDuyet_Click);
            // 
            // mnuHuongDan
            // 
            this.mnuHuongDan.Name = "mnuHuongDan";
            this.mnuHuongDan.Size = new System.Drawing.Size(345, 22);
            this.mnuHuongDan.Text = "Hướng dẫn của Hải quan";
            this.mnuHuongDan.Visible = false;
            this.mnuHuongDan.Click += new System.EventHandler(this.mnuHuongDan_Click);
            // 
            // mnuSuaToKhai
            // 
            this.mnuSuaToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mnuSuaToKhai.Image")));
            this.mnuSuaToKhai.Name = "mnuSuaToKhai";
            this.mnuSuaToKhai.Size = new System.Drawing.Size(345, 22);
            this.mnuSuaToKhai.Text = "Sửa tờ khai";
            this.mnuSuaToKhai.Visible = false;
            this.mnuSuaToKhai.Click += new System.EventHandler(this.mnuSuaTK_Click);
            // 
            // mnuHuyToKhai
            // 
            this.mnuHuyToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mnuHuyToKhai.Image")));
            this.mnuHuyToKhai.Name = "mnuHuyToKhai";
            this.mnuHuyToKhai.Size = new System.Drawing.Size(345, 22);
            this.mnuHuyToKhai.Text = "Hủy tờ khai";
            this.mnuHuyToKhai.Visible = false;
            this.mnuHuyToKhai.Click += new System.EventHandler(this.mnuHuyTK_Click);
            // 
            // mniImport
            // 
            this.mniImport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniImportTK,
            this.mniImportMSG});
            this.mniImport.Name = "mniImport";
            this.mniImport.Size = new System.Drawing.Size(345, 22);
            this.mniImport.Text = "Import";
            this.mniImport.Visible = false;
            // 
            // mniImportTK
            // 
            this.mniImportTK.Name = "mniImportTK";
            this.mniImportTK.Size = new System.Drawing.Size(274, 22);
            this.mniImportTK.Text = "Import Tờ khai từ XML khai báo";
            this.mniImportTK.Click += new System.EventHandler(this.mniImportTK_Click);
            // 
            // mniImportMSG
            // 
            this.mniImportMSG.Name = "mniImportMSG";
            this.mniImportMSG.Size = new System.Drawing.Size(274, 22);
            this.mniImportMSG.Text = "Import Message phản hồi từ Hải quan";
            this.mniImportMSG.Click += new System.EventHandler(this.mniImportMSG_Click);
            // 
            // mniExport
            // 
            this.mniExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniExportTK});
            this.mniExport.Name = "mniExport";
            this.mniExport.Size = new System.Drawing.Size(345, 22);
            this.mniExport.Text = "Export";
            this.mniExport.Visible = false;
            // 
            // mniExportTK
            // 
            this.mniExportTK.Name = "mniExportTK";
            this.mniExportTK.Size = new System.Drawing.Size(189, 22);
            this.mniExportTK.Text = "Export Tờ khai ra XML";
            this.mniExportTK.Click += new System.EventHandler(this.mniExportTK_Click);
            // 
            // mniCapNhatToKhaiSua
            // 
            this.mniCapNhatToKhaiSua.Image = ((System.Drawing.Image)(resources.GetObject("mniCapNhatToKhaiSua.Image")));
            this.mniCapNhatToKhaiSua.Name = "mniCapNhatToKhaiSua";
            this.mniCapNhatToKhaiSua.Size = new System.Drawing.Size(345, 22);
            this.mniCapNhatToKhaiSua.Text = "Cập nhật thông tin tờ khai Sửa, và chuyển Đã duyệt";
            this.mniCapNhatToKhaiSua.Click += new System.EventHandler(this.mniCapNhatToKhaiSua_Click);
            // 
            // mnuCapSoToKhaiGiay
            // 
            this.mnuCapSoToKhaiGiay.Image = ((System.Drawing.Image)(resources.GetObject("mnuCapSoToKhaiGiay.Image")));
            this.mnuCapSoToKhaiGiay.Name = "mnuCapSoToKhaiGiay";
            this.mnuCapSoToKhaiGiay.Size = new System.Drawing.Size(345, 22);
            this.mnuCapSoToKhaiGiay.Text = "Cấp Số Tờ Khai Giấy";
            this.mnuCapSoToKhaiGiay.Click += new System.EventHandler(this.mnuCapSoToKhaiGiay_Click);
            // 
            // imageListSmall
            // 
            this.imageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmall.ImageStream")));
            this.imageListSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListSmall.Images.SetKeyName(0, "app_large_icons.png");
            this.imageListSmall.Images.SetKeyName(1, "page_excel.png");
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ctrCoQuanHQ);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.timToKhai1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(971, 77);
            this.uiGroupBox2.TabIndex = 52;
            this.uiGroupBox2.Text = "Thông tin tìm kiếm";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // timToKhai1
            // 
            this.timToKhai1.BackColor = System.Drawing.Color.Transparent;
            this.timToKhai1.Location = new System.Drawing.Point(45, 43);
            this.timToKhai1.Name = "timToKhai1";
            this.timToKhai1.Size = new System.Drawing.Size(793, 23);
            this.timToKhai1.TabIndex = 18;
            this.timToKhai1.Search += new System.EventHandler<Company.Interface.Controls.StringEventArgs>(this.timToKhai1_Search);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 393);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(971, 41);
            this.uiGroupBox3.TabIndex = 51;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(12, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.SaoChep,
            this.SaoChepALL,
            this.SaoChepToKhaiHang,
            this.cmdPrint,
            this.XacNhan,
            this.DongBoDuLieu,
            this.Export,
            this.Import,
            this.cmdXuatToKhaiChoPhongKhai,
            this.cmdCSDaDuyet,
            this.cmdLayPhanHoi,
            this.cmdNhanTrangThaiPhanLuong,
            this.cmdDelete,
            this.cmdHistory});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCSDaDuyet1,
            this.cmdHistory1,
            this.Separator2,
            this.cmdDelete1,
            this.SaoChep3,
            this.Separator1,
            this.Export2,
            this.cmdSend2,
            this.cmdCancel2,
            this.cmdSingleDownload2,
            this.XacNhan1,
            this.cmdLayPhanHoi1,
            this.cmdNhanTrangThaiPhanLuong1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.imageListSmall;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(971, 26);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet1.Image")));
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "Chuyển Trạng Thái";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistory1.Image")));
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdDelete1
            // 
            this.cmdDelete1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDelete1.Image")));
            this.cmdDelete1.Key = "cmdDelete";
            this.cmdDelete1.Name = "cmdDelete1";
            // 
            // SaoChep3
            // 
            this.SaoChep3.Image = ((System.Drawing.Image)(resources.GetObject("SaoChep3.Image")));
            this.SaoChep3.Key = "SaoChep";
            this.SaoChep3.Name = "SaoChep3";
            this.SaoChep3.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.SaoChep3.Text = "Sao chép";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // Export2
            // 
            this.Export2.Image = ((System.Drawing.Image)(resources.GetObject("Export2.Image")));
            this.Export2.Key = "Export";
            this.Export2.Name = "Export2";
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            this.cmdSend2.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend2.Text = "Khai báo";
            this.cmdSend2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdCancel2
            // 
            this.cmdCancel2.Key = "cmdCancel";
            this.cmdCancel2.Name = "cmdCancel2";
            this.cmdCancel2.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdCancel2.Text = "Hủy khai báo";
            this.cmdCancel2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdSingleDownload2
            // 
            this.cmdSingleDownload2.Key = "cmdSingleDownload";
            this.cmdSingleDownload2.Name = "cmdSingleDownload2";
            this.cmdSingleDownload2.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.cmdSingleDownload2.Text = "Nhận dữ liệu";
            this.cmdSingleDownload2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // XacNhan1
            // 
            this.XacNhan1.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan1.Icon")));
            this.XacNhan1.Key = "XacNhan";
            this.XacNhan1.Name = "XacNhan1";
            this.XacNhan1.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.XacNhan1.Text = "Xác nhận";
            this.XacNhan1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdLayPhanHoi1
            // 
            this.cmdLayPhanHoi1.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi1.Name = "cmdLayPhanHoi1";
            this.cmdLayPhanHoi1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdNhanTrangThaiPhanLuong1
            // 
            this.cmdNhanTrangThaiPhanLuong1.Key = "cmdNhanTrangThaiPhanLuong";
            this.cmdNhanTrangThaiPhanLuong1.Name = "cmdNhanTrangThaiPhanLuong1";
            this.cmdNhanTrangThaiPhanLuong1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSingleDownload.Icon")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu mới của tờ khai đang chọn (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + S)";
            // 
            // SaoChep
            // 
            this.SaoChep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.SaoChepToKhaiHang1,
            this.SaoChepALL1});
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép";
            // 
            // SaoChepToKhaiHang1
            // 
            this.SaoChepToKhaiHang1.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepToKhaiHang1.Image")));
            this.SaoChepToKhaiHang1.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang1.Name = "SaoChepToKhaiHang1";
            this.SaoChepToKhaiHang1.Text = "Tờ khai";
            // 
            // SaoChepALL1
            // 
            this.SaoChepALL1.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepALL1.Image")));
            this.SaoChepALL1.Key = "SaoChepALL";
            this.SaoChepALL1.Name = "SaoChepALL1";
            this.SaoChepALL1.Text = "Tờ khai + hàng hóa";
            this.SaoChepALL1.ToolTipText = "Sao chép cả hàng hóa";
            // 
            // SaoChepALL
            // 
            this.SaoChepALL.Key = "SaoChepALL";
            this.SaoChepALL.Name = "SaoChepALL";
            this.SaoChepALL.Text = "Sao chép tờ khai";
            // 
            // SaoChepToKhaiHang
            // 
            this.SaoChepToKhaiHang.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Name = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Text = "Sao chép cả hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In tờ khai";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Export1,
            this.Import1});
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // Export1
            // 
            this.Export1.Icon = ((System.Drawing.Icon)(resources.GetObject("Export1.Icon")));
            this.Export1.Key = "Export";
            this.Export1.Name = "Export1";
            // 
            // Import1
            // 
            this.Import1.Icon = ((System.Drawing.Icon)(resources.GetObject("Import1.Icon")));
            this.Import1.Key = "Import";
            this.Import1.Name = "Import1";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // Import
            // 
            this.Import.Key = "Import";
            this.Import.Name = "Import";
            this.Import.Text = "Import dữ liệu";
            // 
            // cmdXuatToKhaiChoPhongKhai
            // 
            this.cmdXuatToKhaiChoPhongKhai.Key = "cmdXuatToKhaiChoPhongKhai";
            this.cmdXuatToKhaiChoPhongKhai.Name = "cmdXuatToKhaiChoPhongKhai";
            this.cmdXuatToKhaiChoPhongKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // cmdLayPhanHoi
            // 
            this.cmdLayPhanHoi.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Name = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Text = "Lấy Phản Hồi";
            // 
            // cmdNhanTrangThaiPhanLuong
            // 
            this.cmdNhanTrangThaiPhanLuong.Key = "cmdNhanTrangThaiPhanLuong";
            this.cmdNhanTrangThaiPhanLuong.Name = "cmdNhanTrangThaiPhanLuong";
            this.cmdNhanTrangThaiPhanLuong.Text = "Nhận trạng thái phân luồng";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Kết quả xử lý";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(971, 26);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ctrCoQuanHQ
            // 
            this.ctrCoQuanHQ.AutoSize = true;
            this.ctrCoQuanHQ.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCoQuanHQ.Location = new System.Drawing.Point(125, 13);
            this.ctrCoQuanHQ.Ma = "";
            this.ctrCoQuanHQ.MaCuc = "";
            this.ctrCoQuanHQ.Name = "ctrCoQuanHQ";
            this.ctrCoQuanHQ.ReadOnly = false;
            this.ctrCoQuanHQ.Size = new System.Drawing.Size(461, 24);
            this.ctrCoQuanHQ.TabIndex = 19;
            this.ctrCoQuanHQ.VisualStyleManager = null;
            // 
            // ToKhaiMauDichManageForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(971, 460);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi tờ khai mậu dịch đã khai báo";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label label1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload2;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel2;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep3;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem khaibaoCTMenu;
        private ToolStripMenuItem NhanDuLieuCTMenu;
        private ToolStripMenuItem HuyCTMenu;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private ToolStripMenuItem print;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private ToolStripMenuItem xacnhanToolStripMenuItem;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand DongBoDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand Export1;
        private Janus.Windows.UI.CommandBars.UICommand Import1;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand Import;
        private ToolStripMenuItem xóaTờKhaiToolStripMenuItem;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhaiChoPhongKhai;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private ToolStripMenuItem mnuCSDaDuyet;
        private ToolStripMenuItem ToKhaiMenuItem;
        private ToolStripMenuItem PhieuTNMenuItem;
        private ToolStripMenuItem ToKhaiA4MenuItem;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanTrangThaiPhanLuong;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanTrangThaiPhanLuong1;
        private ToolStripMenuItem mnuPhanLuong;
        private ToolStripMenuItem ToKhaiTQDTMenuItem;
        private ToolStripMenuItem mnuHuongDan;
        private ToolStripMenuItem mniInBangKe;
        private ToolStripMenuItem mnuSuaToKhai;
        private ToolStripMenuItem mnuHuyToKhai;
        private ToolStripMenuItem mniImport;
        private ToolStripMenuItem mniExport;
        private ToolStripMenuItem mniImportTK;
        private ToolStripMenuItem mniImportMSG;
        private ToolStripMenuItem mniExportTK;
        private ImageList imageListSmall;
        private ToolStripMenuItem mniCapNhatToKhaiSua;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Company.Interface.Controls.TimToKhai timToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private ToolStripMenuItem mniInTokhaiTaiCho;
        private ToolStripMenuItem mniInToKhaiSuaDoi;
        private ToolStripMenuItem mniInBangKeHangHoa;
        private ToolStripMenuItem mniInPhieuKiemTra_ChungTu;
        private ToolStripMenuItem mniInPhieuKiemTra_HangHoa;
        private ToolStripMenuItem mniAnDinhThue;
        private Janus.Windows.UI.CommandBars.UICommand Export2;
        private ToolStripMenuItem mnuCapSoToKhaiGiay;
        private UIGroupBox uiGroupBox3;
        private Label label2;
        private GridEX dgList;
        private UIGroupBox uiGroupBox2;
        private Company.Interface.Controls.DonViHaiQuanControl ctrCoQuanHQ;
    }
}
