using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Windows.Forms;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.EditControls;
namespace Company.Interface
{
    public class DocExcel_VNACCS
    {
        public void insertThongSoHangTK(string sheetName,
                                        string beginRow,
                                        string productCode,
                                        string productName,
                                        string hSCode,
                                        string madeIn,
                                        string quantity,
                                        string quantity2,
                                        string unitCal,
                                        string unitCal2,
                                        string price,
                                        string TriGia,
                                        string ThueXNK,
                                        string ThueSuat,
                                        string MaMienGiam,
                                        string MaMucThue,
                                        string TienMienGiam )
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheet.InnerText = sheetName;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            row.InnerText = beginRow;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            code.InnerText = productCode;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            name.InnerText = productName;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hs.InnerText = hSCode;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            dvt.InnerText = unitCal;

            XmlNode dvt2 = doc.SelectSingleNode("TSTK/DVT2");
            dvt2.InnerText = unitCal;

            XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
            quan.InnerText = quantity;

            XmlNode quan2 = doc.SelectSingleNode("TSTK/Soluong2");
            quan2.InnerText = quantity;

            XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
            priceunit.InnerText = price;

            XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
            made.InnerText = madeIn;

            XmlNode tgtt = doc.SelectSingleNode("TSTK/TriGia");
            tgtt.InnerText = TriGia;

            XmlNode tsXNK = doc.SelectSingleNode("TSTK/ThueXNK");
            tsXNK.InnerText = ThueXNK;

            XmlNode tsGTGT = doc.SelectSingleNode("TSTK/ThueSuat");
            tsGTGT.InnerText = ThueSuat;

            XmlNode tsTTDB = doc.SelectSingleNode("TSTK/MaMucThue");
            tsTTDB.InnerText = MaMucThue;

            XmlNode tltk = doc.SelectSingleNode("TSTK/MaMienGiam");
            tltk.InnerText = MaMienGiam;

            XmlNode tmg = doc.SelectSingleNode("TSTK/TienMienGiam");
            tmg.InnerText = TienMienGiam;

            doc.Save(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

        }
        public void ReadDefault(UIComboBox sheetName,
                                        NumericEditBox beginRow,
                                        EditBox productCode,
                                        EditBox productName,
                                        EditBox hSCode,
                                        EditBox madeIn,
                                        EditBox quantity,
                                        EditBox quantity2,
                                        EditBox unitCal,
                                        EditBox unitCal2,
                                        EditBox price,
                                        EditBox TriGia,
                                        EditBox TSXNK,
                                        EditBox ThueSuat,
                                        EditBox MaMucThue,
                                        EditBox MaMienGiam,
                                        EditBox TienMienGiam
                                        )
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");
            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheetName.Text = sheet.InnerText;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            beginRow.Text = row.InnerText;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            productCode.Text = code.InnerText;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            productName.Text = name.InnerText;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hSCode.Text = hs.InnerText;

            XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
            madeIn.Text = made.InnerText;

            XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
            quantity.Text = quan.InnerText;

            XmlNode quan2 = doc.SelectSingleNode("TSTK/Soluong2");
            quantity2.Text = quan.InnerText;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            unitCal.Text = dvt.InnerText;

            XmlNode dvt2 = doc.SelectSingleNode("TSTK/DVT2");
            unitCal2.Text = dvt.InnerText;

            XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
            price.Text = priceunit.InnerText;

            XmlNode tgTT = doc.SelectSingleNode("TSTK/TriGia");
            TriGia.Text = tgTT.InnerText;

            XmlNode tsXNK = doc.SelectSingleNode("TSTK/ThueXNK");
            TSXNK.Text = tsXNK.InnerText;

            XmlNode tsThueSuat = doc.SelectSingleNode("TSTK/ThueSuat");
            ThueSuat.Text = tsThueSuat.InnerText;

            XmlNode tsGTGT = doc.SelectSingleNode("TSTK/MaMucThue");
            MaMucThue.Text = tsGTGT.InnerText;

            XmlNode tsTTDB = doc.SelectSingleNode("TSTK/MaMienGiam");
            MaMienGiam.Text = tsTTDB.InnerText;

            XmlNode tltk = doc.SelectSingleNode("TSTK/TienMienGiam");
            TienMienGiam.Text = tltk.InnerText;
        }


        public void insertThongSo(string sheetName,
            string beginRow, string productCode, string productName, string hSCode, string unitCal)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheet.InnerText = sheetName;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            row.InnerText = beginRow;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            code.InnerText = productCode;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            name.InnerText = productName;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hs.InnerText = hSCode;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            dvt.InnerText = unitCal;



            doc.Save(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

        }

        public void ReadDefaultCon(EditBox sheetName,
                                NumericEditBox beginRow,
                                EditBox productCode,
                                EditBox productName,
                                EditBox hSCode,
                                EditBox unitCal

                                )
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");
            XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
            sheetName.Text = sheet.InnerText;

            XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
            beginRow.Text = row.InnerText;

            XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
            productCode.Text = code.InnerText;

            XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
            productName.Text = name.InnerText;

            XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
            hSCode.Text = hs.InnerText;

            XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
            unitCal.Text = dvt.InnerText;

        }
    }
}
