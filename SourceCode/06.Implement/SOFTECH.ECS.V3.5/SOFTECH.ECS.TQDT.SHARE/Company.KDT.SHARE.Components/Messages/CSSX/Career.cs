﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class Career
    {
        /// <summary>
        /// Loại ngành nghề
        /// 1. Da giầy
        //2. May mặc
        //3. Điện tử, điện lạnh
        //4. Chế biến thực phẩm
        //5. Cơ khí
        //6. Gỗ
        //7. Nhựa
        //8. Nông sản
        //9. Loại khác
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Chu kỳ sản xuất
        /// </summary>
        [XmlElement("Period")]
        public Products Period { get; set; }

        /// <summary>
        /// Năng lực sản xuất
        /// </summary>
        [XmlElement("ProductionCapacity")]
        public Products ProductionCapacity { get; set; }
    }
}
