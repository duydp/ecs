﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.SXXK
{
    public class SXXK_Material
    {

        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }

        [XmlElement("GoodsMeasure")]
        public SXXK_GoodsMeasure GoodsMeasure { get; set; }


    }
}
