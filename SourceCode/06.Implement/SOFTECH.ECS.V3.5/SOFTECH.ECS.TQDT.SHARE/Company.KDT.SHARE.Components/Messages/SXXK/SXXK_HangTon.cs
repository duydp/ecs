﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_HangTon : DeclarationBase
    {
        ///<summary>
        ///Số lượng chứng từ, phụ lục đính kèm(n..5)
        ///</summary>
        [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem> CustomsGoodsItems { set; get; }

    }
}
