﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class NameBase
    {
        /// <summary>
        /// Tên 
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Mã  
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }
        /// <summary>
        /// Địa chỉ
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }

   
    }
}
