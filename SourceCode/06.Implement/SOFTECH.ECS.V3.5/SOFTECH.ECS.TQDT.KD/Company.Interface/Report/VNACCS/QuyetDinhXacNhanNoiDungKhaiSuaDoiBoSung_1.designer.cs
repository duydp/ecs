namespace Company.Interface.Report.VNACCS
{
    partial class QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDaiLyHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDaiLyHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNhanVienHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLyDoKhaiBS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiNopThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaXacDinhThoiHanNop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHieuLuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNgayAnHan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNgayAnHanVAT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoQuanLyNoiBo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTeTruocKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaHoiDoaiTruocKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChuTruocKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTeSauKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaHoiDoaiSauKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChuSauKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuThiSoTienTangGiamThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTienTangGiamThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTeThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThongBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoanThanhKT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioHoanThanhKT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDKKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDKKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomXuLyHoSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayToKhaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDauHieuBaoQua = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanTaiNhapXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMaKhoanMucTiepNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucTiepNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuThiSoTienTangGiamThueVaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTienTangGiamThueVaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTeThueVaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoDongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLyDo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiPhuTrach = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTruongDv = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 296.2499F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0.9999752F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17});
            this.xrTable2.SizeF = new System.Drawing.SizeF(745F, 294.9667F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell7});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Người khai";
            this.xrTableCell1.Weight = 0.63575472585067072;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 3.3167139887950694;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell8,
            this.lblMaNguoiKhai});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.12998037376373955;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Mã";
            this.xrTableCell8.Weight = 0.50577435208693111;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.Text = "XXXXXXXXX1 - XXE";
            this.lblMaNguoiKhai.Weight = 3.3167139887950694;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.lblTenNguoiKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 2.0571414221948787;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.12998037376373955;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "Tên";
            this.xrTableCell10.Weight = 0.50577435208693111;
            // 
            // lblTenNguoiKhai
            // 
            this.lblTenNguoiKhai.Name = "lblTenNguoiKhai";
            this.lblTenNguoiKhai.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblTenNguoiKhai.Weight = 3.3167139887950694;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell12,
            this.lblMaBuuChinh});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.99977072595512984;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.12998037376373955;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Mã bưu chính";
            this.xrTableCell12.Weight = 0.50577435208693111;
            // 
            // lblMaBuuChinh
            // 
            this.lblMaBuuChinh.Name = "lblMaBuuChinh";
            this.lblMaBuuChinh.Text = "XXXXXXXXX1 - XXE";
            this.lblMaBuuChinh.Weight = 3.3167139887950694;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.lblDiaChi});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 2.0571412457391007;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.12998037376373955;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Địa chỉ";
            this.xrTableCell14.Weight = 0.50577435208693111;
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblDiaChi.Weight = 3.3167139887950694;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17,
            this.lblSoDT});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.99977060526649542;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 0.12998037376373955;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "Số điện thoại";
            this.xrTableCell17.Weight = 0.50577435208693111;
            // 
            // lblSoDT
            // 
            this.lblSoDT.Name = "lblSoDT";
            this.lblSoDT.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDT.Weight = 3.3167139887950694;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblMaDaiLyHQ,
            this.lblTenDaiLyHQ,
            this.xrTableCell32,
            this.lblMaNhanVienHQ});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 0.99977060526649542;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Đại lý Hải quan";
            this.xrTableCell19.Weight = 0.51240631457737562;
            // 
            // lblMaDaiLyHQ
            // 
            this.lblMaDaiLyHQ.Name = "lblMaDaiLyHQ";
            this.lblMaDaiLyHQ.Text = "XXXXE";
            this.lblMaDaiLyHQ.Weight = 0.3057189910196092;
            // 
            // lblTenDaiLyHQ
            // 
            this.lblTenDaiLyHQ.Name = "lblTenDaiLyHQ";
            this.lblTenDaiLyHQ.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXX";
            this.lblTenDaiLyHQ.Weight = 2.1535210963269811;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "Mã nhân viên Hải quan";
            this.xrTableCell32.Weight = 0.70317682598551456;
            // 
            // lblMaNhanVienHQ
            // 
            this.lblMaNhanVienHQ.Name = "lblMaNhanVienHQ";
            this.lblMaNhanVienHQ.Text = "XXXXE";
            this.lblMaNhanVienHQ.Weight = 0.27764548673625933;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.lblMaLyDoKhaiBS,
            this.xrTableCell34,
            this.lblPhanLoaiNopThue,
            this.xrTableCell24});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.99977060526649542;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "Mã lý do bổ sung";
            this.xrTableCell22.Weight = 0.98214876724314826;
            // 
            // lblMaLyDoKhaiBS
            // 
            this.lblMaLyDoKhaiBS.Name = "lblMaLyDoKhaiBS";
            this.lblMaLyDoKhaiBS.Text = "X";
            this.lblMaLyDoKhaiBS.Weight = 0.19011627220587091;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "Phân loại nộp thuế";
            this.xrTableCell34.Weight = 0.92113767624836029;
            // 
            // lblPhanLoaiNopThue
            // 
            this.lblPhanLoaiNopThue.Name = "lblPhanLoaiNopThue";
            this.lblPhanLoaiNopThue.Text = "X";
            this.lblPhanLoaiNopThue.Weight = 0.20070900455082597;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 1.6583569943975347;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.lblMaXacDinhThoiHanNop,
            this.xrTableCell36,
            this.lblNgayHetHieuLuc,
            this.xrTableCell37,
            this.lblSoNgayAnHan});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.99977060526649542;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "Mã xác định thời hạn nộp thuế";
            this.xrTableCell25.Weight = 0.9821487267667145;
            // 
            // lblMaXacDinhThoiHanNop
            // 
            this.lblMaXacDinhThoiHanNop.Name = "lblMaXacDinhThoiHanNop";
            this.lblMaXacDinhThoiHanNop.Text = "X";
            this.lblMaXacDinhThoiHanNop.Weight = 0.19011635315873862;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Text = "Ngày hiệu lực của chứng từ";
            this.xrTableCell36.Weight = 0.92113715005472041;
            // 
            // lblNgayHetHieuLuc
            // 
            this.lblNgayHetHieuLuc.Name = "lblNgayHetHieuLuc";
            this.lblNgayHetHieuLuc.Text = "dd/MM/yyyy";
            this.lblNgayHetHieuLuc.Weight = 0.56502669959282192;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "Thời hạn nộp thuế";
            this.xrTableCell37.Weight = 0.654637476428569;
            // 
            // lblSoNgayAnHan
            // 
            this.lblSoNgayAnHan.Name = "lblSoNgayAnHan";
            this.lblSoNgayAnHan.Text = "NNE";
            this.lblSoNgayAnHan.Weight = 0.6394023086441758;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblSoNgayAnHanVAT});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.99977060526649542;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.12998037376373955;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.50577435208693111;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 2.2640486167714551;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Text = "(GTGT)";
            this.xrTableCell51.Weight = 0.41326273956796816;
            // 
            // lblSoNgayAnHanVAT
            // 
            this.lblSoNgayAnHanVAT.Name = "lblSoNgayAnHanVAT";
            this.lblSoNgayAnHanVAT.Text = "NNE";
            this.lblSoNgayAnHanVAT.Weight = 0.63940263245564632;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.lblSoQuanLyNoiBo});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 0.99977060526649542;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "Số quản lý trong nội bộ doanh nghiệp";
            this.xrTableCell39.Weight = 1.1722653227840558;
            // 
            // lblSoQuanLyNoiBo
            // 
            this.lblSoQuanLyNoiBo.Name = "lblSoQuanLyNoiBo";
            this.lblSoQuanLyNoiBo.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoQuanLyNoiBo.Weight = 2.7802033918616842;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell58,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.99977060526649542;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.63575462400526872;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Tỷ giá tính thuế";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell58.Weight = 0.71843865827500442;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell42.Weight = 0.10500122266654366;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "Phần ghi chú";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell43.Weight = 2.4932742096989231;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.lblMaTienTeTruocKhaiBao,
            this.xrTableCell52,
            this.lblTyGiaHoiDoaiTruocKhaiBao,
            this.lblGhiChuTruocKhaiBao});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 2.0571410674158344;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "Trước khi khai báo";
            this.xrTableCell44.Weight = 0.63575466448170248;
            // 
            // lblMaTienTeTruocKhaiBao
            // 
            this.lblMaTienTeTruocKhaiBao.Name = "lblMaTienTeTruocKhaiBao";
            this.lblMaTienTeTruocKhaiBao.Text = "XXE";
            this.lblMaTienTeTruocKhaiBao.Weight = 0.18237092445031911;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "-";
            this.xrTableCell52.Weight = 0.082895665303967636;
            // 
            // lblTyGiaHoiDoaiTruocKhaiBao
            // 
            this.lblTyGiaHoiDoaiTruocKhaiBao.Name = "lblTyGiaHoiDoaiTruocKhaiBao";
            this.lblTyGiaHoiDoaiTruocKhaiBao.Text = "XXXXXXXXE";
            this.lblTyGiaHoiDoaiTruocKhaiBao.Weight = 0.55817343220046245;
            // 
            // lblGhiChuTruocKhaiBao
            // 
            this.lblGhiChuTruocKhaiBao.Name = "lblGhiChuTruocKhaiBao";
            this.lblGhiChuTruocKhaiBao.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblGhiChuTruocKhaiBao.Weight = 2.4932740282092882;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47,
            this.lblMaTienTeSauKhaiBao,
            this.xrTableCell49,
            this.lblTyGiaHoiDoaiSauKhaiBao,
            this.lblGhiChuSauKhaiBao});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 2.0571410674158344;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Text = "Sau khi khai báo";
            this.xrTableCell47.Weight = 0.63575466448170248;
            // 
            // lblMaTienTeSauKhaiBao
            // 
            this.lblMaTienTeSauKhaiBao.Name = "lblMaTienTeSauKhaiBao";
            this.lblMaTienTeSauKhaiBao.Text = "XXE";
            this.lblMaTienTeSauKhaiBao.Weight = 0.18237092445031911;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "-";
            this.xrTableCell49.Weight = 0.082895665303967636;
            // 
            // lblTyGiaHoiDoaiSauKhaiBao
            // 
            this.lblTyGiaHoiDoaiSauKhaiBao.Name = "lblTyGiaHoiDoaiSauKhaiBao";
            this.lblTyGiaHoiDoaiSauKhaiBao.Text = "XXXXXXXXE";
            this.lblTyGiaHoiDoaiSauKhaiBao.Weight = 0.55817343220046245;
            // 
            // lblGhiChuSauKhaiBao
            // 
            this.lblGhiChuSauKhaiBao.Name = "lblGhiChuSauKhaiBao";
            this.lblGhiChuSauKhaiBao.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblGhiChuSauKhaiBao.Weight = 2.4932740282092882;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell57,
            this.xrTableCell56,
            this.xrTableCell59});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.9997705344393053;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.27477182282219781;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "Mã sắc thuế";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell57.Weight = 0.64106040617174309;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 0.360982831867555;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Text = "Tổng số tiền tăng/giảm thuế";
            this.xrTableCell59.Weight = 2.6756536537842441;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell61,
            this.lblBieuThiSoTienTangGiamThueXNK,
            this.lblTongSoTienTangGiamThueXNK,
            this.lblMaTienTeThueXNK});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.9997705344393053;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.27477182282219781;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Text = "Thuế xuất nhập khẩu";
            this.xrTableCell61.Weight = 1.0020433999450333;
            // 
            // lblBieuThiSoTienTangGiamThueXNK
            // 
            this.lblBieuThiSoTienTangGiamThueXNK.Name = "lblBieuThiSoTienTangGiamThueXNK";
            this.lblBieuThiSoTienTangGiamThueXNK.Text = "X";
            this.lblBieuThiSoTienTangGiamThueXNK.Weight = 0.10875945889248245;
            // 
            // lblTongSoTienTangGiamThueXNK
            // 
            this.lblTongSoTienTangGiamThueXNK.Name = "lblTongSoTienTangGiamThueXNK";
            this.lblTongSoTienTangGiamThueXNK.Text = "12,345,678,901";
            this.lblTongSoTienTangGiamThueXNK.Weight = 0.63420776550979052;
            // 
            // lblMaTienTeThueXNK
            // 
            this.lblMaTienTeThueXNK.Name = "lblMaTienTeThueXNK";
            this.lblMaTienTeThueXNK.Text = "XXE";
            this.lblMaTienTeThueXNK.Weight = 1.9326862674762357;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.lblSoPL,
            this.xrLabel1});
            this.TopMargin.HeightF = 154.4584F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.9999752F, 62.3751F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTable3.SizeF = new System.Drawing.SizeF(745F, 78F);
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.lblSoThongBao,
            this.xrTableCell4,
            this.xrTableCell5,
            this.lblNgayHoanThanhKT,
            this.lblGioHoanThanhKT});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.75;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Số thông báo";
            this.xrTableCell2.Weight = 0.4951788216583;
            // 
            // lblSoThongBao
            // 
            this.lblSoThongBao.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThongBao.Name = "lblSoThongBao";
            this.lblSoThongBao.StylePriority.UseFont = false;
            this.lblSoThongBao.Text = "XXXXXXXXX1XE";
            this.lblSoThongBao.Weight = 0.85778256881335557;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.091253611667097112;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Ngày hoàn thành kiểm tra";
            this.xrTableCell5.Weight = 0.71677001448702216;
            // 
            // lblNgayHoanThanhKT
            // 
            this.lblNgayHoanThanhKT.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHoanThanhKT.Name = "lblNgayHoanThanhKT";
            this.lblNgayHoanThanhKT.StylePriority.UseFont = false;
            this.lblNgayHoanThanhKT.Text = "dd/MM/yyyy";
            this.lblNgayHoanThanhKT.Weight = 0.41950749168711254;
            // 
            // lblGioHoanThanhKT
            // 
            this.lblGioHoanThanhKT.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioHoanThanhKT.Name = "lblGioHoanThanhKT";
            this.lblGioHoanThanhKT.StylePriority.UseFont = false;
            this.lblGioHoanThanhKT.Text = "hh:mm:ss";
            this.lblGioHoanThanhKT.Weight = 0.49801988838132744;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.lblSoToKhaiBoSung,
            this.xrTableCell92,
            this.xrTableCell107,
            this.lblNgayDKKhaiBoSung,
            this.lblGioDKKhaiBoSung});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.75;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "Số tờ khai bổ sung";
            this.xrTableCell103.Weight = 0.4951788216583;
            // 
            // lblSoToKhaiBoSung
            // 
            this.lblSoToKhaiBoSung.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiBoSung.Name = "lblSoToKhaiBoSung";
            this.lblSoToKhaiBoSung.StylePriority.UseFont = false;
            this.lblSoToKhaiBoSung.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiBoSung.Weight = 0.85778256881335557;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.091253611667097112;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "Ngày đăng ký";
            this.xrTableCell107.Weight = 0.71677001448702216;
            // 
            // lblNgayDKKhaiBoSung
            // 
            this.lblNgayDKKhaiBoSung.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDKKhaiBoSung.Name = "lblNgayDKKhaiBoSung";
            this.lblNgayDKKhaiBoSung.StylePriority.UseFont = false;
            this.lblNgayDKKhaiBoSung.Text = "dd/MM/yyyy";
            this.lblNgayDKKhaiBoSung.Weight = 0.41950749168711254;
            // 
            // lblGioDKKhaiBoSung
            // 
            this.lblGioDKKhaiBoSung.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioDKKhaiBoSung.Name = "lblGioDKKhaiBoSung";
            this.lblGioDKKhaiBoSung.StylePriority.UseFont = false;
            this.lblGioDKKhaiBoSung.Text = "hh:mm:ss";
            this.lblGioDKKhaiBoSung.Weight = 0.49801988838132744;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.lblCoQuanNhan,
            this.xrTableCell95,
            this.xrTableCell108,
            this.lblNhomXuLyHoSo});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.75;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "Cơ quan nhận";
            this.xrTableCell104.Weight = 0.4951788216583;
            // 
            // lblCoQuanNhan
            // 
            this.lblCoQuanNhan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanNhan.Name = "lblCoQuanNhan";
            this.lblCoQuanNhan.StylePriority.UseFont = false;
            this.lblCoQuanNhan.Text = "XXXXXXXXXE";
            this.lblCoQuanNhan.Weight = 0.85778256881335557;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.091253611667097112;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "Nhóm xử lý hồ sơ";
            this.xrTableCell108.Weight = 0.71677001448702216;
            // 
            // lblNhomXuLyHoSo
            // 
            this.lblNhomXuLyHoSo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhomXuLyHoSo.Name = "lblNhomXuLyHoSo";
            this.lblNhomXuLyHoSo.StylePriority.UseFont = false;
            this.lblNhomXuLyHoSo.Text = "XE";
            this.lblNhomXuLyHoSo.Weight = 0.91752738006843992;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.lblPhanLoaiXNK,
            this.xrTableCell111,
            this.lblSoToKhaiXNK,
            this.xrTableCell114,
            this.lblMaLoaiHinh,
            this.xrTableCell98,
            this.xrTableCell109,
            this.lblNgayToKhaiXNK,
            this.xrTableCell117,
            this.lblDauHieuBaoQua});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.75;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "Số tờ khai";
            this.xrTableCell105.Weight = 0.4951788216583;
            // 
            // lblPhanLoaiXNK
            // 
            this.lblPhanLoaiXNK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiXNK.Name = "lblPhanLoaiXNK";
            this.lblPhanLoaiXNK.StylePriority.UseFont = false;
            this.lblPhanLoaiXNK.Text = "X";
            this.lblPhanLoaiXNK.Weight = 0.090392648681136223;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "-";
            this.xrTableCell111.Weight = 0.064566044768026076;
            // 
            // lblSoToKhaiXNK
            // 
            this.lblSoToKhaiXNK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiXNK.Name = "lblSoToKhaiXNK";
            this.lblSoToKhaiXNK.StylePriority.UseFont = false;
            this.lblSoToKhaiXNK.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXNK.Weight = 0.48640499430254475;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "-";
            this.xrTableCell114.Weight = 0.0645661117616764;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.Weight = 0.15185276929997219;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.091253611667097112;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "Ngày tờ khai xuất nhập khẩu";
            this.xrTableCell109.Weight = 0.71677001448702216;
            // 
            // lblNgayToKhaiXNK
            // 
            this.lblNgayToKhaiXNK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayToKhaiXNK.Name = "lblNgayToKhaiXNK";
            this.lblNgayToKhaiXNK.StylePriority.UseFont = false;
            this.lblNgayToKhaiXNK.Text = "dd/MM/yyyy";
            this.lblNgayToKhaiXNK.Weight = 0.35296782186208675;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "-";
            this.xrTableCell117.Weight = 0.06456611570247929;
            // 
            // lblDauHieuBaoQua
            // 
            this.lblDauHieuBaoQua.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDauHieuBaoQua.Name = "lblDauHieuBaoQua";
            this.lblDauHieuBaoQua.StylePriority.UseFont = false;
            this.lblDauHieuBaoQua.Text = "X";
            this.lblDauHieuBaoQua.Weight = 0.49999344250387406;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.lblNgayCapPhep,
            this.xrTableCell101,
            this.xrTableCell110,
            this.lblThoiHanTaiNhapXuat});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.9;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Ngày cấp phép";
            this.xrTableCell106.Weight = 0.4951788216583;
            // 
            // lblNgayCapPhep
            // 
            this.lblNgayCapPhep.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayCapPhep.Name = "lblNgayCapPhep";
            this.lblNgayCapPhep.StylePriority.UseFont = false;
            this.lblNgayCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayCapPhep.Weight = 0.85778256881335557;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.091253611667097112;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell110.Weight = 0.71677001448702216;
            // 
            // lblThoiHanTaiNhapXuat
            // 
            this.lblThoiHanTaiNhapXuat.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiHanTaiNhapXuat.Name = "lblThoiHanTaiNhapXuat";
            this.lblThoiHanTaiNhapXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapXuat.Weight = 0.91752738006843992;
            // 
            // lblSoPL
            // 
            this.lblSoPL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPL.LocationFloat = new DevExpress.Utils.PointFloat(710.6715F, 25.62501F);
            this.lblSoPL.Name = "lblSoPL";
            this.lblSoPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPL.SizeF = new System.Drawing.SizeF(35.32855F, 23.00002F);
            this.lblSoPL.StylePriority.UseFont = false;
            this.lblSoPL.StylePriority.UseTextAlignment = false;
            this.lblSoPL.Text = "X";
            this.lblSoPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(64.25005F, 25.62501F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(584.3767F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Tờ khai bổ sung sau thông quan (được chấp nhận)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 66.66666F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 14.58333F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(30.91672F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(715.0831F, 14.58333F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMaKhoanMucTiepNhan,
            this.lblTenKhoanMucTiepNhan,
            this.xrTableCell6,
            this.lblBieuThiSoTienTangGiamThueVaThuKhac,
            this.lblTongSoTienTangGiamThueVaThuKhac,
            this.lblMaTienTeThueVaThuKhac});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UsePadding = false;
            this.xrTableRow1.Weight = 1;
            // 
            // lblMaKhoanMucTiepNhan
            // 
            this.lblMaKhoanMucTiepNhan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaKhoanMucTiepNhan.Name = "lblMaKhoanMucTiepNhan";
            this.lblMaKhoanMucTiepNhan.StylePriority.UseFont = false;
            this.lblMaKhoanMucTiepNhan.Text = "X";
            this.lblMaKhoanMucTiepNhan.Weight = 0.218749828338623;
            // 
            // lblTenKhoanMucTiepNhan
            // 
            this.lblTenKhoanMucTiepNhan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenKhoanMucTiepNhan.Name = "lblTenKhoanMucTiepNhan";
            this.lblTenKhoanMucTiepNhan.StylePriority.UseFont = false;
            this.lblTenKhoanMucTiepNhan.Text = "XXXXXXXXE";
            this.lblTenKhoanMucTiepNhan.Weight = 1.208333148956299;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.6804165469573239;
            // 
            // lblBieuThiSoTienTangGiamThueVaThuKhac
            // 
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Name = "lblBieuThiSoTienTangGiamThueVaThuKhac";
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.StylePriority.UseFont = false;
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Text = "X";
            this.lblBieuThiSoTienTangGiamThueVaThuKhac.Weight = 0.20500047574775415;
            // 
            // lblTongSoTienTangGiamThueVaThuKhac
            // 
            this.lblTongSoTienTangGiamThueVaThuKhac.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoTienTangGiamThueVaThuKhac.Name = "lblTongSoTienTangGiamThueVaThuKhac";
            this.lblTongSoTienTangGiamThueVaThuKhac.StylePriority.UseFont = false;
            this.lblTongSoTienTangGiamThueVaThuKhac.Text = "12,345,678,901";
            this.lblTongSoTienTangGiamThueVaThuKhac.Weight = 1.1954165649414064;
            // 
            // lblMaTienTeThueVaThuKhac
            // 
            this.lblMaTienTeThueVaThuKhac.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTienTeThueVaThuKhac.Name = "lblMaTienTeThueVaThuKhac";
            this.lblMaTienTeThueVaThuKhac.StylePriority.UseFont = false;
            this.lblMaTienTeThueVaThuKhac.Text = "XXE";
            this.lblMaTienTeThueVaThuKhac.Weight = 3.6429156056381391;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupFooter1.HeightF = 108.3333F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.9999752F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18,
            this.xrTableRow23,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27});
            this.xrTable4.SizeF = new System.Drawing.SizeF(745F, 102.3267F);
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell18,
            this.lblTongSoTrang,
            this.xrTableCell21,
            this.lblTongSoDongHang});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 1;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 1.55661607321999;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "Tổng số trang của tờ khai";
            this.xrTableCell18.Weight = 0.8496246871138835;
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Text = "NE";
            this.lblTongSoTrang.Weight = 0.20676889295028766;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "Tổng số dòng hàng của tờ khai";
            this.xrTableCell21.Weight = 0.968092969597189;
            // 
            // lblTongSoDongHang
            // 
            this.lblTongSoDongHang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoDongHang.Name = "lblTongSoDongHang";
            this.lblTongSoDongHang.StylePriority.UseFont = false;
            this.lblTongSoDongHang.Text = "NE";
            this.lblTongSoDongHang.Weight = 0.27056455241161825;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell33});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Text = "Mục thông báo của Hải quan";
            this.xrTableCell23.Weight = 1.6858917360487145;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 2.1657754392442543;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell27,
            this.lblLyDo});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 3.0171423309949894;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.15467023879514338;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "Lý do";
            this.xrTableCell27.Weight = 0.28542833094852638;
            // 
            // lblLyDo
            // 
            this.lblLyDo.Name = "lblLyDo";
            this.lblLyDo.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            this.lblLyDo.Weight = 3.4115686055492991;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.lblTenNguoiPhuTrach});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.99977124899357417;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "Tên của người phụ trách";
            this.xrTableCell35.Weight = 0.8780423615774744;
            // 
            // lblTenNguoiPhuTrach
            // 
            this.lblTenNguoiPhuTrach.Name = "lblTenNguoiPhuTrach";
            this.lblTenNguoiPhuTrach.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblTenNguoiPhuTrach.Weight = 2.9736248137154946;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lblTenTruongDv});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.99977124899357417;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Tên trưởng đơn vị Hải quan";
            this.xrTableCell45.Weight = 0.87804267713063933;
            // 
            // lblTenTruongDv
            // 
            this.lblTenTruongDv.Name = "lblTenTruongDv";
            this.lblTenTruongDv.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblTenTruongDv.Weight = 2.9736244981623297;
            // 
            // QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(56, 46, 154, 67);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaKhoanMucTiepNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucTiepNhan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuThiSoTienTangGiamThueVaThuKhac;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTienTangGiamThueVaThuKhac;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTeThueVaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblSoPL;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThongBao;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoanThanhKT;
        private DevExpress.XtraReports.UI.XRTableCell lblGioHoanThanhKT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanNhan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomXuLyHoSo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayToKhaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell lblDauHieuBaoQua;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanTaiNhapXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChi;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDaiLyHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDaiLyHQ;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNhanVienHQ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLyDoKhaiBS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiNopThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell lblMaXacDinhThoiHanNop;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHieuLuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNgayAnHan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNgayAnHanVAT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblSoQuanLyNoiBo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTeTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaHoiDoaiTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChuTruocKhaiBao;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTeSauKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaHoiDoaiSauKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChuSauKhaiBao;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuThiSoTienTangGiamThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTienTangGiamThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTeThueXNK;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoDongHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblLyDo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiPhuTrach;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTruongDv;
    }
}
