﻿namespace Company.Interface
{
    partial class ThongBaoFormVNACCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucRespone1 = new Company.KDT.SHARE.VNACCS.Controls.ucRespone();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTuDongLayPH = new Janus.Windows.EditControls.UIButton();
            this.btnDungTuDongLayPH = new Janus.Windows.EditControls.UIButton();
            this.txtTimer = new Janus.Windows.GridEX.EditControls.ValueListUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.btnDungLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.lblStatus = new System.Windows.Forms.Label();
            this.iconStatus = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(529, 520);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucRespone1);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(529, 520);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucRespone1
            // 
            this.ucRespone1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucRespone1.InputMsgID = null;
            this.ucRespone1.Location = new System.Drawing.Point(3, 8);
            this.ucRespone1.Name = "ucRespone1";
            this.ucRespone1.Size = new System.Drawing.Size(523, 396);
            this.ucRespone1.TabIndex = 2;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.btnTuDongLayPH);
            this.uiGroupBox3.Controls.Add(this.btnDungTuDongLayPH);
            this.uiGroupBox3.Controls.Add(this.txtTimer);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Location = new System.Drawing.Point(15, 413);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(507, 43);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnTuDongLayPH
            // 
            this.btnTuDongLayPH.Location = new System.Drawing.Point(305, 14);
            this.btnTuDongLayPH.Name = "btnTuDongLayPH";
            this.btnTuDongLayPH.Size = new System.Drawing.Size(85, 23);
            this.btnTuDongLayPH.TabIndex = 43;
            this.btnTuDongLayPH.Text = "Tự động lấy PH";
            this.btnTuDongLayPH.VisualStyleManager = this.vsmMain;
            this.btnTuDongLayPH.Click += new System.EventHandler(this.btnTuDongLayPH_Click);
            // 
            // btnDungTuDongLayPH
            // 
            this.btnDungTuDongLayPH.Location = new System.Drawing.Point(396, 14);
            this.btnDungTuDongLayPH.Name = "btnDungTuDongLayPH";
            this.btnDungTuDongLayPH.Size = new System.Drawing.Size(102, 23);
            this.btnDungTuDongLayPH.TabIndex = 43;
            this.btnDungTuDongLayPH.Text = "Dừng Tự động PH";
            this.btnDungTuDongLayPH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDungTuDongLayPH.Click += new System.EventHandler(this.btnDungTuDongLayPH_Click);
            // 
            // txtTimer
            // 
            this.txtTimer.Location = new System.Drawing.Point(218, 14);
            this.txtTimer.MaxLength = 300;
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(45, 21);
            this.txtTimer.TabIndex = 42;
            this.txtTimer.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(269, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "phút";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Khoảng cách thời gian mỗi lần lấy phản hồi";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.btnLayPhanHoi);
            this.uiGroupBox2.Controls.Add(this.btnDungLayPhanHoi);
            this.uiGroupBox2.Controls.Add(this.lblStatus);
            this.uiGroupBox2.Controls.Add(this.iconStatus);
            this.uiGroupBox2.Location = new System.Drawing.Point(15, 462);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(507, 52);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Trạng thái";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Location = new System.Drawing.Point(305, 20);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(85, 23);
            this.btnLayPhanHoi.TabIndex = 43;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // btnDungLayPhanHoi
            // 
            this.btnDungLayPhanHoi.Location = new System.Drawing.Point(396, 20);
            this.btnDungLayPhanHoi.Name = "btnDungLayPhanHoi";
            this.btnDungLayPhanHoi.Size = new System.Drawing.Size(102, 23);
            this.btnDungLayPhanHoi.TabIndex = 43;
            this.btnDungLayPhanHoi.Text = "Dừng lấy phản hồi";
            this.btnDungLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDungLayPhanHoi.Click += new System.EventHandler(this.btnDungLayPhanHoi_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(53, 25);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(11, 13);
            this.lblStatus.TabIndex = 41;
            this.lblStatus.Text = ".";
            // 
            // iconStatus
            // 
            this.iconStatus.Image = global::Company.Interface.Properties.Resources.Processing;
            this.iconStatus.Location = new System.Drawing.Point(15, 15);
            this.iconStatus.Name = "iconStatus";
            this.iconStatus.Size = new System.Drawing.Size(32, 32);
            this.iconStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconStatus.TabIndex = 40;
            this.iconStatus.TabStop = false;
            this.iconStatus.Visible = false;
            // 
            // ThongBaoFormVNACCS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 520);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThongBaoFormVNACCS";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "";
            this.Load += new System.EventHandler(this.ThongBaoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.PictureBox iconStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus;
        private Janus.Windows.EditControls.UIButton btnTuDongLayPH;
        private Janus.Windows.GridEX.EditControls.ValueListUpDown txtTimer;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnDungLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnDungTuDongLayPH;
        private Company.KDT.SHARE.VNACCS.Controls.ucRespone ucRespone1;
    }
}