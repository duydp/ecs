using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class LoaiPhuKien : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _MaPhuKien = String.Empty;
        protected string _NoiDung = String.Empty;
        protected string _ThongTinMoi = String.Empty;
        protected long _Master_ID;
        protected string _ThongTinCu = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string MaPhuKien
        {
            set { this._MaPhuKien = value; }
            get { return this._MaPhuKien; }
        }
        public string NoiDung
        {
            set { this._NoiDung = value; }
            get { return this._NoiDung; }
        }
        public string ThongTinMoi
        {
            set { this._ThongTinMoi = value; }
            get { return this._ThongTinMoi; }
        }
        public long Master_ID
        {
            set { this._Master_ID = value; }
            get { return this._Master_ID; }
        }
        public string ThongTinCu
        {
            set { this._ThongTinCu = value; }
            get { return this._ThongTinCu; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) this._MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) this._NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) this._ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) this._ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public LoaiPhuKienCollection SelectCollectionBy_Master_ID()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            LoaiPhuKienCollection collection = new LoaiPhuKienCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                LoaiPhuKien entity = new LoaiPhuKien();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) entity.MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) entity.ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_Master_ID()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public LoaiPhuKienCollection SelectCollectionAll()
        {
            LoaiPhuKienCollection collection = new LoaiPhuKienCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                LoaiPhuKien entity = new LoaiPhuKien();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) entity.MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) entity.ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public LoaiPhuKienCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            LoaiPhuKienCollection collection = new LoaiPhuKienCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                LoaiPhuKien entity = new LoaiPhuKien();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) entity.MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) entity.ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(LoaiPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (LoaiPhuKien item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, LoaiPhuKienCollection collection)
        {
            foreach (LoaiPhuKien item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(LoaiPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (LoaiPhuKien item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }


        public int UpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_LoaiPhuKien_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, this._MaPhuKien);
            this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, this._NoiDung);
            this.db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, this._ThongTinMoi);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, this._ThongTinCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(LoaiPhuKienCollection collection, SqlTransaction transaction)
        {
            foreach (LoaiPhuKien item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.


        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(LoaiPhuKienCollection collection, SqlTransaction transaction)
        {
            foreach (LoaiPhuKien item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(LoaiPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (LoaiPhuKien item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_Master_ID()
        {
            string spName = "p_KDT_GC_LoaiPhuKien_DeleteBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_Master_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_LoaiPhuKien_DeleteBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}