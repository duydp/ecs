﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Infragistics.Excel;

namespace Company.Interface.SXXK
{
    public partial class SanPhamChuaCoDMForm : BaseForm
    {
        public DataSet ds = new DataSet();
        public bool isLenhSX = false;
        public SanPhamChuaCoDMForm()
        {
            InitializeComponent();
        }

        private void SanPhamChuaCoDMForm_Load(object sender, EventArgs e)
        {
            if (isLenhSX)
                this.Text = "DANH SÁCH SẢN PHẨM ĐÃ CÓ ĐỊNH MỨC NHƯNG CHƯA CẬP NHẬT LỆNH SẢN XUẤT";
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if(e.Row.RowType == RowType.Record)    
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("SanPhamChuaCoDM");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 5000;
            ws.Columns[2].Width = 5000;
            ws.Columns[3].Width = 10000;
            ws.Columns[4].Width = 5000;
            ws.Columns[5].Width = 5000;
            ws.Columns[6].Width = 5000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Số Tờ Khai";
            wsr0.Cells[1].Value = "Ngày Đăng Ký";
            wsr0.Cells[2].Value = "Mã SP";
            wsr0.Cells[3].Value = "Tên SP";
            wsr0.Cells[4].Value = "Mã HS";
            wsr0.Cells[5].Value = "Số Lượng";
            wsr0.Cells[6].Value = "ĐVT";
            DataTable table=this.ds.Tables[0];
   
            for (int i = 0; i < table.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = table.Rows[i]["SoToKhai"].ToString();
                wsr.Cells[1].Value = table.Rows[i]["NgayDangKy"].ToString();
                wsr.Cells[2].Value = table.Rows[i]["Ma"].ToString();
                wsr.Cells[3].Value = table.Rows[i]["Ten"].ToString();
                wsr.Cells[4].Value = table.Rows[i]["MaHS"].ToString();
                wsr.Cells[5].Value = table.Rows[i]["SoLuong"].ToString();
                wsr.Cells[6].Value =  this.DonViTinh_GetName(table.Rows[i]["DVT_ID"]); 
            }
            DialogResult rs= saveFileDialog1.ShowDialog(this);
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
            }
        }
    }
}