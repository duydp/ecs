﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    public class NodePK_SuaDoiThietBi
    {
        /// <summary>
        /// Mã thiết bị muốn sửa đổi 
        /// </summary>
        public const string preIdentification = "preIdentification";    
        /// <summary>
        /// Thiết bị bổ sung 
        /// </summary>
        public const string Equipment = "Equipment";    
        /// <summary>
        /// Thiết bị
        /// </summary>
        public const string Commodity = "Commodity";    
        /// <summary>
        /// Tên/Mô tả Thiết bị bổ sung
        /// </summary>
        public const string description = "description";    
        /// <summary>
        /// Mã Thiết bị bổ sung
        /// </summary>
        public const string identification = "identification";    
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";    
        /// <summary>
        /// Số lượng
        /// </summary>
        public const string quantity = "quantity";    
        /// <summary>
        /// Nước xuất xứ
        /// </summary>
        public const string Origin = "Origin";    
        /// <summary>
        /// Nước xuất xứ
        /// </summary>
        public const string originCountry = "originCountry";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";    
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string currencyType = "currencyType";   
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string CustomsValue = "CustomsValue"; 
        /// <summary>
        /// Trạng thái (mới = 0; cũ =1)
        /// </summary>
        public const string status = "status";    
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string unitPrice = "unitPrice";            
    }
}
