﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.GC
{
	public partial class ToKhaiMauDichRegistedForm : BaseForm
	{
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tmpCollection = new ToKhaiMauDichCollection();
        private string xmlCurrent = "";
        public string nhomLoaiHinh = "";
        public ToKhaiMauDichRegistedForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            //dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;            
           

        }

		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            try
			{
                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                BindHopDong();
                this.search();
                setCommandStatus();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
           
        }
        private void BindHopDong()
        {
            Company.GC.BLL.KDT.GC.HopDong hopdong = new Company.GC.BLL.KDT.GC.HopDong();
            DataTable dt;

            string where = string.Format("MaDoanhNghiep='{0}' and TrangThaiXuLy=1",  GlobalSettings.MA_DON_VI);
            dt = Company.GC.BLL.KDT.GC.HopDong.SelectDynamic(where, "").Tables[0];

            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            try
            {
                cbHopDong.SelectedIndex = 0;
            }
            catch { }
        }
        private ToKhaiMauDich getTKMDByID(long id)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("XV"))
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["SoToKhai"].Value.ToString().Trim()));
                        f.ShowDialog();
                    }
                    else if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("NV"))
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = this.getTKMDVNACCS(System.Convert.ToInt32(e.Row.Cells["SoToKhai"].Value.ToString().Trim()));
                        f.ShowDialog();
                    }
                    else
                    {
                        long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                        ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                        f.OpenType = OpenFormType.View;
                        f.TKMD = this.getTKMDByID(id);
                        f.TKMD.Load();
                        f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                        f.ShowDialog();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        private KDT_VNACC_ToKhaiMauDich getTKMDVNACCS(int Sotokhai)
        {
            KDT_VNACC_ToKhaiMauDich TKVNACC = new KDT_VNACC_ToKhaiMauDich();
            decimal sotokhaiVnacc = CapSoToKhai.GetSoTKVNACCS(Sotokhai);
            TKVNACC = KDT_VNACC_ToKhaiMauDich.Load(KDT_VNACC_ToKhaiMauDich.GetID(sotokhaiVnacc));
            TKVNACC.LoadFull();
            return TKVNACC;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.            
            string where = "1 = 1";
            where += string.Format(" AND MaDoanhNGhiep='{0}'",GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                if(txtSoTiepNhan.TextLength==12)
                    where += " AND LoaiVanDon = " + txtSoTiepNhan.Value;
                else
                    where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND YEAR(NgayDangKy) = " + txtNamTiepNhan.Value;
            }

            where += " AND TrangThaiXuLy = 1";
            if (cbHopDong.Text.Trim().Length > 0)
                where += " AND IDHopDong = " + cbHopDong.Value;
            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkmdCollection;

            this.setCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());

                //if (e.Row.Cells["ActionStatus"].Text == "5000")
                //{
                //    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["LoaiVanDon"].Text;
                //}
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSingleDownload":
                    this.downloadItemsSelect();
                    break;
                case "cmdCancel":
                    this.cancelItemsSelect();
                    break;
                case "cmdSend":
                    this.sendItemsSelect();
                    break;
                case "SaoChep":
                    this.SaoChepToKhaiMD();
                    break;
                case "SaoChepALL":
                    this.SaoChepALLHang();
                    break;
                case "SaoChepToKhaiHang":
                    this.SaoChepToKhaiMD();
                    break;
                case "XacNhan":
                    this.XacNhanThongTin();
                    break;
               
            }
        }
     
        private string checkDataImport(ToKhaiMauDichCollection collection)
        {
            string st = "";
            foreach (ToKhaiMauDich tkmd in collection)
            {
                ToKhaiMauDich tkmdInDatabase = new ToKhaiMauDich();
                tkmdInDatabase.ID = (tkmd.ID);
                tkmdInDatabase.Load();
                if (tkmdInDatabase != null)
                {
                    if (tkmdInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + tkmd.ID + " đã được duyệt.\n";
                    }
                    else
                    {                      
                        tmpCollection.Add(tkmd);
                    }
                }
                else
                {
                    if (tkmd.ID > 0)
                        tkmd.ID = 0;
                    tmpCollection.Add(tkmd);
                }
            }
            return st;
        }
      
        private void SaoChepALLHang()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.TrangThaiPhanBo = 0;
            tkmd.LoadHMDCollection();
            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }
            tkmd.ID = 0;
            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = OpenFormType.Edit;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;            
            f.HDGC.ID = tkmd.IDHopDong;
            f.Show();
        }
        private void SaoChepToKhaiMD()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;            
            ToKhaiMauDich tkmd =new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;          
            tkmd.Load();
            tkmd.SoToKhai = 0;
            tkmd.ID = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiPhanBo = 0;
            tkmd.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;          
            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.OpenType = OpenFormType.Edit;
            f.HDGC.ID = tkmd.IDHopDong;
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.Show();
        }
        private void LaySoTiepNhanDT()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();       
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                if (dgList.GetRow() != null)
                {
                    tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;                    
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    if (!sendXML.Load())
                    {
                        //ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                        showMsg("MSG_STN01");
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = tkmd.LayPhanHoiGC(password, sendXML.msg);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = showMsg("MSG_STN02");
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkmd.SoTiepNhan);
                        //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //ShowMessage("Đã hủy tờ khai này", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            showMsg("MSG_SEN11", tkmd.SoToKhai);
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai :" + tkmd.SoToKhai.ToString(), false);
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            showMsg("MSG_SEN04");
                            //ShowMessage("Hải quan chưa xử lý tờ khai này!", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            MsgSend sendXML = new MsgSend();
            try
            {

                if (dgList.GetRow() != null)
                {
                    tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;                    
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = tkmd.LayPhanHoiGC(pass, sendXML.msg);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = showMsg("MSG_STN02", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkmd.SoTiepNhan);
                        //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //ShowMessage("Đã hủy tờ khai này", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            showMsg("MSG_SEN11", tkmd.SoToKhai);
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai :"+tkmd.SoToKhai.ToString(), false);
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            showMsg("MSG_SEN04");
                            //ShowMessage("Hải quan chưa xử lý tờ khai này!", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void download()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            string password = "";
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    showMsg("MSG_WRN05");
                    return;
                }
            }
            else
            {
                showMsg("MSG_240212");
                //ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                return;
            }            
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = tkmd.WSRequestXMLGC(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;     
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }               
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

	    /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            MsgSend sendXML = new MsgSend();            
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_WRN05");
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    return;
                }
            }
            else
            {
                showMsg("MSG_240212");
                //ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;                  
                tkmd.LoadHMDCollection();
                tkmd.LoadChungTuTKCollection();
                if (tkmd.HMDCollection.Count == 0)
                {
                    showMsg("MSG_2702030");
                    //ShowMessage("Danh sách hàng hóa rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    this.Cursor = Cursors.Default;
                    return;
                }                
                if (tkmd.MaLoaiHinh.StartsWith("NGC"))
                {
                    xmlCurrent = tkmd.WSSendXMLNHAP(password);
                }
                else if (tkmd.MaLoaiHinh.StartsWith("XGC"))
                {
                    xmlCurrent = tkmd.WSSendXMLXuat(password, GlobalSettings.MaMID);
                }               
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;     

                    // Thực hiện kiểm tra.                   
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;               
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }
               
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        private void cancel()
        {            
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {              
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_WRN05");
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    return;
                }
            }
            else
            {
                showMsg("MSG_2702031");
                //ShowMessage("Chưa chọn thông tin để gửi.", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            this.Cursor = Cursors.WaitCursor;
            try
            {                                                 
                xmlCurrent = tkmd.WSCancelXMLGC(password);                
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.Insert();
                LayPhanHoi(password); ;
                
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;                
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }               
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
               
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void uiCommandBar1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void khaibaoCTMenu_Click(object sender, EventArgs e)
        {
            send();
        }

        private void NhanDuLieuCTMenu_Click(object sender, EventArgs e)
        {
            download();
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void saoChepTK_Click(object sender, EventArgs e)
        {
            SaoChepToKhaiMD();
        }

        private void saoChepHH_Click(object sender, EventArgs e)
        {
            SaoChepALLHang();
        }

        private void downloadItemsSelect()
        {            
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int item = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = GlobalS1ettings.MA_HAI_QUAN;
            //            try
            //            {
            //                k++;
            //                item++;                          
            //                {
            //                    xmlCurrent= tkmd.WSRequestXML(wsForm.txtMatKhau.Text.Trim());
            //                }
            //                if (xmlCurrent == "")
            //                {
            //                    ;
            //                }
            //                else
            //                {
            //                    MsgSend sendXML = new MsgSend();
            //                    sendXML.LoaiHS = "TK";
            //                    sendXML.master_id = tkmd.ID;
            //                    sendXML.msg = xmlCurrent;
            //                    sendXML.func = 2;
            //                    xmlCurrent = "";
            //                    sendXML.Insert();
            //                }      

            //            }
            //            catch (Exception ex)
            //            {
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                item--;
            //                string message = "";
            //                if (k == items.Count)
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không nhận dữ liệu về được?\nBạn có muốn đưa vào hàng đợi không";
            //                else
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không nhận dữ liệu về được?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục nhận dữ liệu của các bản ghi tiếp không";
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = TrangThaiXuLy.CHO_DUYET;
            //                    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;                               
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi nhận dữ liệu danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();


            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (item > 0)
            //            ShowMessage("Nhận dữ liệu thành công " + item.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không nhận dữ liệu tờ khai nào cả", false);
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi nhận dữ liệu khai báo danh sách tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            FormSendToKhai f = new FormSendToKhai();
            f.btnNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void sendItemsSelect()
        {            
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int itemOK = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = GlobalSet1tings.MA_HAI_QUAN;
            //            tkmd.LoadHMDCollection();
            //            tkmd.LoadChungTuTKCollection();
            //            try
            //            {
            //                k++;
            //                itemOK++;                          
            //                {

            //                    if (tkmd.MaLoaiHinh.StartsWith("NSX"))
            //                    {
            //                        if (tkmd.SoTiepNhan == 0)
            //                           xmlCurrent= tkmd.WSSendXMLNHAP(wsForm.txtMatKhau.Text.Trim());
            //                        else
            //                           xmlCurrent = tkmd.WSUpdateXMLNHAP(wsForm.txtMatKhau.Text.Trim());
            //                    }
            //                    else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
            //                    {

            //                        if (tkmd.SoTiepNhan == 0)
            //                            xmlCurrent = tkmd.WSSendXMLXuat(wsForm.txtMatKhau.Text.Trim());
            //                        else
            //                            xmlCurrent = tkmd.WSUpdateXMLXUAT(wsForm.txtMatKhau.Text.Trim());
            //                    }
            //                    if (xmlCurrent == "")
            //                    {
            //                        ;
            //                    }
            //                    else
            //                    {
            //                        MsgSend sendXML = new MsgSend();
            //                        sendXML.LoaiHS = "TK";
            //                        sendXML.master_id = tkmd.ID;
            //                        sendXML.msg = xmlCurrent;
            //                        sendXML.func = 1;
            //                        xmlCurrent = "";
            //                        sendXML.Insert();
            //                    }      
            //                }                            
            //            }
            //            catch (Exception ex)
            //            {
            //                itemOK--;
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                string message = "";
            //                if (k == items.Count)
            //                {
            //                    message = "Tờ khai thứ " + k.ToString() + " không khai báo được?\nBạn có muốn đưa vào hàng đợi không";                               
            //                }
            //                else
            //                {
            //                    message = "Tờ khai thứ " + k.ToString() + " không khai báo được?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục gửi dữ liệu của các tờ khai tiếp không";                                
            //                }
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = TrangThaiXuLy.CHO_DUYET;
            //                    hd.ChucNang = ChucNang.KHAI_BAO;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();

            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không khai báo được dữ liệu của tờ khai nào cả ! ", false);
            //    }
            //    else
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            FormSendToKhai f = new FormSendToKhai();
            f.btnSend.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void cancelItemsSelect()
        {            
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int itemOK = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = Globa1lSettings.MA_HAI_QUAN;                        
            //            try
            //            {
            //                k++;
            //                itemOK++;                          
            //                {
            //                    if (tkmd.MaLoaiHinh.StartsWith("NSX"))
            //                      xmlCurrent=  tkmd.WSCancelXMLNhap(wsForm.txtMatKhau.Text.Trim());
            //                    else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
            //                      xmlCurrent = tkmd.WSCancelXMLXuat(wsForm.txtMatKhau.Text.Trim());
            //                  if (xmlCurrent == "")
            //                  {
            //                      ;
            //                  }
            //                  else
            //                  {                                  
            //                      MsgSend sendXML = new MsgSend();
            //                      sendXML.LoaiHS = "TK";
            //                      sendXML.master_id = tkmd.ID;
            //                      sendXML.msg = xmlCurrent;
            //                      sendXML.func = 3;
            //                      xmlCurrent = "";
            //                      sendXML.Insert();
            //                  }      
            //                }                             
            //            }
            //            catch (Exception ex)
            //            {
            //                itemOK--;
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                string message = "";
            //                if (k == items.Count)
            //                {
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không hủy khai báo được?\nBạn có muốn đưa vào hàng đợi không";                               
            //                }
            //                else
            //                {
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " hủy khai báo được ?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục hủy các tờ khai tiếp không";                                
            //                }
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.GC.BLL.KDT.HangDoi hd = new Company.GC.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = tkmd.TrangThaiXuLy;
            //                    hd.ChucNang = ChucNang.HUY_KHAI_BAO;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi Hủy khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();


            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Hủy khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không hủy được khai báo của tờ khai nào cả ! ", false);
            //    }
            //    else
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Hủy khai báo thành công " + itemOK.ToString() + " tờ khai", false);                                            
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi hủy dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            FormSendToKhai f = new FormSendToKhai();
            f.btnHuy.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void uiContextMenu1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {                                        
              //{
              //    if (showMsg("MSG_DEL01", true) == "Yes")
              //    //if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
              //    {
              //        GridEXSelectedItemCollection items = dgList.SelectedItems;
              //        foreach (GridEXSelectedItem i in items)
              //        {
              //            if (i.RowType == RowType.Record)
              //            {
              //                ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
              //                if (TKMD.MaLoaiHinh.StartsWith("NGC"))
              //                {
              //                    if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year))
              //                    {
              //                        showMsg("MSG_ALL01");
              //                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
              //                        continue;
              //                    }
              //                }
              //                else
              //                {
              //                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
              //                    {
              //                        showMsg("MSG_ALL01");
              //                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
              //                        continue;
              //                    }
              //                }
              //                 MsgSend sendXML = new MsgSend();
              //                  sendXML.LoaiHS = "TK";
              //                  sendXML.master_id = tkmd.ID;
              //                  if (sendXML.Load())
              //                  {
              //                      showMsg("MSG_2702012", i.Position);
              //                      //ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
              //                  }
              //                  else
              //                  {
              //                      tkmd.Delete();
              //                  }
              //            }
              //        }
              //    }
              //    else
              //        e.Cancel = true;
              //}
              //this.search();
        }

        private void print_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            switch (tkmd.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = tkmd;
                    f.ShowDialog();
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = tkmd;
                    f1.ShowDialog();
                    break;
            }
            
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {

        }

        private void xacnhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaySoTiepNhanDT();
        }
        private void XacNhanThongTin()
        {
            FormSendToKhai f = new FormSendToKhai();
            f.btnXacNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            // GridEXSelectedItemCollection items = dgList.SelectedItems;
            // if (items.Count < 0) return;
            // if (showMsg("MSG_DEL01", true) == "Yes")
            ////if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
            //{
               
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            MsgSend sendXML = new MsgSend();
            //            sendXML.LoaiHS = "TK";
            //            sendXML.master_id = tkmd.ID;
            //            if (sendXML.Load())
            //            {
            //                showMsg("MSG_2702012", i.Position);
            //                //ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
            //            }
            //            else
            //            {
            //                if (tkmd.MaLoaiHinh.Contains("XGC"))
            //                {
            //                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
            //                    {
            //                        //showMsg("MSG_ALL01");
            //                        ShowMessage("Tờ khai có ID=" + tkmd.ID + " này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
            //                        continue;
            //                    }
            //                }
            //                else
            //                {
            //                    if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year))
            //                    {
            //                        ShowMessage("Tờ khai này có id= " + tkmd.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
            //                        continue;
            //                    }
            //                }
            //                tkmd.Delete();
            //            }
            //        }
            //    }
            //}
            //this.search();
        }
	}
}