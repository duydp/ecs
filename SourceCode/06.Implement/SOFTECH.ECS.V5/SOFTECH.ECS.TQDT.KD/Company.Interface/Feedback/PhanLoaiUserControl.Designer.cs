﻿namespace Company.Interface.Feedback
{
    partial class PhanLoaiUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.radLoi = new Janus.Windows.EditControls.UIRadioButton();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.radGopY = new Janus.Windows.EditControls.UIRadioButton();
            this.radThacMac = new Janus.Windows.EditControls.UIRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.radLoi);
            this.uiGroupBox1.Controls.Add(this.radGopY);
            this.uiGroupBox1.Controls.Add(this.radThacMac);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(284, 95);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Phân loại";
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // radLoi
            // 
            this.radLoi.CheckedValue = "Loi";
            this.radLoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLoi.Location = new System.Drawing.Point(12, 68);
            this.radLoi.Name = "radLoi";
            this.radLoi.Size = new System.Drawing.Size(258, 17);
            this.radLoi.TabIndex = 2;
            this.radLoi.Text = "Tôi muốn thông báo lỗi của chương trình/ hệ thống";
            this.radLoi.VisualStyleManager = this.visualStyleManager1;
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "VS2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // radGopY
            // 
            this.radGopY.CheckedValue = "GopY";
            this.radGopY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGopY.Location = new System.Drawing.Point(12, 45);
            this.radGopY.Name = "radGopY";
            this.radGopY.Size = new System.Drawing.Size(249, 17);
            this.radGopY.TabIndex = 1;
            this.radGopY.Text = "Tôi muốn đóng góp ý kiến xây dựng chương trình";
            this.radGopY.VisualStyleManager = this.visualStyleManager1;
            // 
            // radThacMac
            // 
            this.radThacMac.CheckedValue = "ThacMac";
            this.radThacMac.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radThacMac.Location = new System.Drawing.Point(12, 22);
            this.radThacMac.Name = "radThacMac";
            this.radThacMac.Size = new System.Drawing.Size(221, 17);
            this.radThacMac.TabIndex = 0;
            this.radThacMac.TabStop = true;
            this.radThacMac.Text = "Tôi có ý kiến thắc mắc chưa được giải đáp";
            this.radThacMac.VisualStyleManager = this.visualStyleManager1;
            // 
            // PhanLoaiUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "PhanLoaiUserControl";
            this.Size = new System.Drawing.Size(284, 95);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIRadioButton radThacMac;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.EditControls.UIRadioButton radLoi;
        private Janus.Windows.EditControls.UIRadioButton radGopY;
    }
}
