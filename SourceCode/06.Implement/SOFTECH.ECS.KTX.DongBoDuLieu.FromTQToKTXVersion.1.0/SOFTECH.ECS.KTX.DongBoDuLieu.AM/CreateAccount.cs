﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Infragistics.Excel;
using System.Security.Cryptography;

namespace ToolCreateAccount
{
    public partial class CreateAccount : Form
    {
        public CreateAccount()
        {
            InitializeComponent();
        }
        private string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            string path=Properties.Settings.Default.PathFileAccount;
            if (path == "")
            {
               DialogResult rel= MessageBox.Show("Đường dẫn tới file lưu trữ account chưa có.Bạn có muốn chọn đường dẫn không?","Tạo account",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
               if (rel == DialogResult.Yes)
               {
                   DialogResult ok = openFileDialog1.ShowDialog();
                   if (ok == DialogResult.OK)
                   {
                       path = openFileDialog1.FileName;
                       DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                       if (luuTHongTin == DialogResult.Yes)
                       {
                           Properties.Settings.Default.PathFileAccount = path;
                           Properties.Settings.Default.Save();
                       }
                   }
               }
               else                
                    return;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }catch
            {
                MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
                return;
            }
            XmlNodeList nodeList = doc.SelectNodes("//DoanhNghiep[@MaDoanhNghiep=\""+txtMa.Text.Trim()+"\"]");
            if (nodeList.Count > 0)
            {
                MessageBox.Show("Doanh nghiệp này đã có trong hệ thống.");
                return;
            }
            XmlElement newDN = doc.CreateElement("DoanhNghiep");
            XmlAttribute attDN=doc.CreateAttribute("MaDoanhNghiep");
            attDN.Value = txtMa.Text.Trim();
            
            XmlElement tenDN = doc.CreateElement("TenDoanhNghiep");
            tenDN.InnerText = txtTen.Text;
            XmlElement passDN = doc.CreateElement("MatKhau");
            passDN.InnerText = GetMD5Value(txtMa.Text.Trim() + "+" + txtMa.Text.Trim());

            newDN.AppendChild(tenDN);
            newDN.AppendChild(passDN);
            newDN.Attributes.Append(attDN);

            doc.SelectSingleNode("Root").AppendChild(newDN);
            doc.Save(path);
            BindData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            string path = Properties.Settings.Default.PathFileAccount;
            if (path == "")
            {
                DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ account chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (rel == DialogResult.Yes)
                {
                    DialogResult ok = openFileDialog1.ShowDialog();
                    if (ok == DialogResult.OK)
                    {
                        path = openFileDialog1.FileName;
                        DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (luuTHongTin == DialogResult.Yes)
                        {
                            Properties.Settings.Default.PathFileAccount = path;
                            Properties.Settings.Default.Save();
                        }
                    }
                }
                else
                    return;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
                return;
            }
            XmlNodeList nodeList = doc.SelectNodes("//DoanhNghiep[@MaDoanhNghiep=\"" + txtMa.Text.Trim() + "\"]");
            if (nodeList.Count == 0)
            {
                MessageBox.Show("Doanh nghiệp này không có trong hệ thống.");
                return;
            }
            XmlNode node = nodeList[0];
            node.ChildNodes[1].InnerText = GetMD5Value(txtMa.Text.Trim() + "+" + txtMa.Text.Trim());
            doc.Save(path);
            BindData();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            //string path = Properties.Settings.Default.PathFileAccount;
            //if (path == "")
            //{
            //    DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ account chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //    if (rel == DialogResult.Yes)
            //    {
            //        DialogResult ok = openFileDialog1.ShowDialog();
            //        if (ok == DialogResult.OK)
            //        {
            //            path = openFileDialog1.FileName;
            //            DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //            if (luuTHongTin == DialogResult.Yes)
            //            {
            //                Properties.Settings.Default.PathFileAccount = path;
            //                Properties.Settings.Default.Save();
            //            }
            //        }
            //    }
            //    else
            //        return;
            //}

            //XmlDocument doc = new XmlDocument();
            //try
            //{
            //    doc.Load(path);
            //}
            //catch
            //{
            //    MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
            //    return;
            //}


            ////doc du lieu tu file excel
            //Workbook wb = new Workbook();
            //Worksheet ws = null;
            //try
            //{
            //    wb = Workbook.Load(openFileDialog1.FileName);
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Lỗi khi mở file, bạn hãy kiểm tra lại đường dẫn và phải đóng file lại.", false);
            //    return;
            //}
            //try
            //{
            //    ws = wb.Worksheets[txtSheet.Text];
            //}
            //catch
            //{
            //    ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
            //    return;
            //}
            //WorksheetRowCollection wsrc = ws.Rows;
            //char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            //int maHangCol = ConvertCharToInt(maHangColumn);
            //char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            //int tenHangCol = ConvertCharToInt(tenHangColumn);
            //char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            //int maHSCol = ConvertCharToInt(maHSColumn);
            //char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            //int dvtCol = ConvertCharToInt(dvtColumn);
            //foreach (WorksheetRow wsr in wsrc)
            //{
            //    if (wsr.Index >= beginRow)
            //    {
            //        try
            //        {
            //            NguyenPhuLieu npl = new NguyenPhuLieu();
            //            npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
            //            npl.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
            //            npl.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
            //            npl.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value));
            //            if (checkNPLExit(npl.Ma) >= 0)
            //            {
            //                if (ShowMessage("Nguyên phụ liệu có mã \"" + npl.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế nguyên phụ liệu trên lưới bằng sản phẩm này?", true) == "Yes")
            //                    this.NPLCollection[checkNPLExit(npl.Ma)] = npl;

            //            }
            //            else this.NPLCollection.Add(npl);
            //        }
            //        catch
            //        {
            //            if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
            //            {
            //                this.NPLCollection.Clear();
            //                return;
            //            }
            //        }
            //    }
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateAccount_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            string path = Properties.Settings.Default.PathFileAccount;
            if (path == "")
            {
                DialogResult rel = MessageBox.Show("Đường dẫn tới file lưu trữ account chưa có.Bạn có muốn chọn đường dẫn không?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (rel == DialogResult.Yes)
                {
                    DialogResult ok = openFileDialog1.ShowDialog();
                    if (ok == DialogResult.OK)
                    {
                        path = openFileDialog1.FileName;
                        DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (luuTHongTin == DialogResult.Yes)
                        {
                            Properties.Settings.Default.PathFileAccount = path;
                            Properties.Settings.Default.Save();
                        }
                    }
                }
                else
                    return;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi đọc file.Hãy kiểm tra lại file");
                return;
            }
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            grdData.DataSource = ds.Tables[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult ok = openFileDialog1.ShowDialog();
            if (ok == DialogResult.OK)
            {
                string path = openFileDialog1.FileName;
                DialogResult luuTHongTin = MessageBox.Show("Bạn có muốn lưu đường dẫn file lại không ?", "Tạo account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (luuTHongTin == DialogResult.Yes)
                {
                    Properties.Settings.Default.PathFileAccount = path;
                    Properties.Settings.Default.Save();
                }
            }
        }
    }
}