﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
    [XmlRoot("Declaration")]
   public class Contract_VNACCS
    {
        /// <summary>
        /// Số tờ khai 
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Ngày khai báo YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }

        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        // <summary>
        /// Loại chứng từ = 309
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Chức năng của chứng từ = 8
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Tình trạng của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// <summary>
        /// Ngày đăng ký chứng từ
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Thông tin tham chiếu đến tờ khai
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public DeclarationDocument DeclarationDocument { get; set; }

        /// <summary>
        /// Thông tin về hợp đồng
        /// </summary>
        [XmlElement("ContractDocuments")]
        public ContractDocuments ContractDocuments { get; set; }

        /// <summary>
        /// AdditionalDocument
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public Company.KDT.SHARE.Components.AdditionalDocument AdditionalInformation { get; set; }

    }
}
