﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Messages.SmartCA
{
    public class ResponseCode
    {
        public const int Success = 1;
        public const int TwoFactorAuthenticationRequiredForThisAccount = 53;
        public const int NoAccountFoundForInputEmail = 10000;
        public const int AccountIsNotActivated = 10001;
        public const int AccountIsLockedOrDeleted = 10002;
        public const int SecondPasswordNotSet = 10003;
        public const int OtpIsInvalid = 10011;
        public const int NoValidServicePackForCurrentAccount = 11001;
        public const int ServicegroupidInvalid = 11002;
        public const int ServicepackExpiredOrOrSignaturesCountLimitExceeded = 11003;
        public const int UnsupportedInputFileType = 12001;
        public const int CertificateNotFound = 13001;
        public const int CertificateAndAccountNotMatch = 13002;
        public const int CertificateExpired = 13003;
        public const int FailedToCreateSignature = 14001;
        public const int PdfHeaderSignatureNotFound = 14002;
        public const int NoSignatureWorkerForThisAccount = 14003;
        public const int CannotArchiveSignedFile = 15001;
        public const int TransactionInformationNotFound = 16001;
        public const int TransactionFileNotFound = 16002;
        public const int RequestParameterIsRequired = 30000;
        public const int ServiceidIsRequired = 30001;
        public const int UnsuportedServiceid = 30002;
        public const int FunctionnameIsRequired = 30003;
        public const int UnsuportedFunctionname = 30004;
        public const int AccountAuthenticateRequired = 30005;
        public const int AccountEmailRequired = 30006;
        public const int InvalidInput = 30010;
        public const int ServicegroupidParameterRequired = 31001;
        public const int CertidParameterRequired = 31002;
        public const int TokenParameterExpired = 32001;
        public const int ServiceInternalError = 50000;
        public const int DatabaseProtectionError = 50001;
        public const int SendChangeSecondPaswordFailed = 50005;
        public const int OtpSendFailed = 50006;
        public const int EmailTemplateFileNotFound = 51000;

    }
    public class TranStatus
    {
        public const int SUCCESS = 1;
        public const int WAITING_FOR_SIGNER_CONFIRM = 4000;
        public const int EXPIRED = 4001;
        public const int SIGNER_REJECTED = 4002;
        public const int AUTHORIZE_KEY_FAILED = 4003;
        public const int SIGN_FAILED = 4004;
    }
    public class TranStatusString
    {
        public const string SUCCESS = "Thành công";
        public const string WAITING_FOR_SIGNER_CONFIRM = "Chờ người dùng xác nhận ký số";
        public const string EXPIRED = "Hết thời gian chờ ký";
        public const string SIGNER_REJECTED = "Người dùng từ chối ký số";
        public const string AUTHORIZE_KEY_FAILED = "Xác thực khoá thất bại";
        public const string SIGN_FAILED = "Ký số thất bại";
    }
}
