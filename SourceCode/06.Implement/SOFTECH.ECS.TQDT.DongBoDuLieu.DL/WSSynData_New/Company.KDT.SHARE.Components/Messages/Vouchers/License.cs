﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class License
    {

        /// <summary>
        /// Người cấp giấy phép
        /// </summary>
       [XmlElement("issuer")]
       public string Issuer { get; set; }

        /// <summary>
        /// Số giấy phép
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày cấp giấy phép
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Nơi cấp giấy phép
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Loại giấy phép
        /// </summary>
        [XmlElement("category")]
        public string Category { get; set; }

        /// <summary>
        /// Ngày hết hạn giấy phép
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
