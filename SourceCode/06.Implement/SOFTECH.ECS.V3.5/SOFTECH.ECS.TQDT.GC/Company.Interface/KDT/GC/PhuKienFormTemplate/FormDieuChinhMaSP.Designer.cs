﻿namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    partial class FormDieuChinhMaSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDieuChinhMaSP));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.label12 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.txtMaSPMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.label8 = new System.Windows.Forms.Label();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbNhomSP = new Janus.Windows.EditControls.UIComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaHSNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvMaNPL = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(628, 305);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.txtMaSPMoi);
            this.uiGroupBox3.Controls.Add(this.btnUpdate);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(372, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(253, 151);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Thông tin điều chỉnh";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(89, 118);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(74, 23);
            this.btnXoa.TabIndex = 37;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(231, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "*";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(172, 118);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // txtMaSPMoi
            // 
            this.txtMaSPMoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSPMoi.Location = new System.Drawing.Point(9, 53);
            this.txtMaSPMoi.MaxLength = 29;
            this.txtMaSPMoi.Name = "txtMaSPMoi";
            this.txtMaSPMoi.Size = new System.Drawing.Size(218, 21);
            this.txtMaSPMoi.TabIndex = 2;
            this.txtMaSPMoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSPMoi.VisualStyleManager = this.vsmMain;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Icon = ((System.Drawing.Icon)(resources.GetObject("btnUpdate.Icon")));
            this.btnUpdate.Location = new System.Drawing.Point(8, 118);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Ghi";
            this.btnUpdate.VisualStyleManager = this.vsmMain;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mã sản phẩm mới";
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.BackColor = System.Drawing.Color.White;
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(105, 64);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.ReadOnly = true;
            this.cbDonViTinh.Size = new System.Drawing.Size(73, 21);
            this.cbDonViTinh.TabIndex = 5;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Đơn vị tính";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtSoLuong);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.cbNhomSP);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.txtMaHSNPL);
            this.uiGroupBox4.Controls.Add(this.txtMaNPL);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.txtTenNPL);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(359, 151);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin sản phẩm";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(335, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "*";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(105, 113);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.ReadOnly = true;
            this.txtSoLuong.Size = new System.Drawing.Size(224, 21);
            this.txtSoLuong.TabIndex = 11;
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Số lượng";
            // 
            // cbNhomSP
            // 
            this.cbNhomSP.BackColor = System.Drawing.Color.White;
            this.cbNhomSP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbNhomSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNhomSP.Location = new System.Drawing.Point(105, 88);
            this.cbNhomSP.Name = "cbNhomSP";
            this.cbNhomSP.ReadOnly = true;
            this.cbNhomSP.Size = new System.Drawing.Size(224, 21);
            this.cbNhomSP.TabIndex = 9;
            this.cbNhomSP.ValueMember = "ID";
            this.cbNhomSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbNhomSP.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nhóm sản phẩm";
            // 
            // txtMaHSNPL
            // 
            this.txtMaHSNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSNPL.Location = new System.Drawing.Point(241, 64);
            this.txtMaHSNPL.Name = "txtMaHSNPL";
            this.txtMaHSNPL.ReadOnly = true;
            this.txtMaHSNPL.Size = new System.Drawing.Size(88, 21);
            this.txtMaHSNPL.TabIndex = 7;
            this.txtMaHSNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(105, 16);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(224, 21);
            this.txtMaNPL.TabIndex = 1;
            this.txtMaNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            this.txtMaNPL.ButtonClick += new System.EventHandler(this.txtMaNPL_ButtonClick);
            this.txtMaNPL.Leave += new System.EventHandler(this.txtMaNPL_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã sản phẩm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tên sản phẩm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(198, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Mã HS";
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(105, 40);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(224, 21);
            this.txtTenNPL.TabIndex = 3;
            this.txtTenNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvMaNPL
            // 
            this.rfvMaNPL.ControlToValidate = this.txtMaNPL;
            this.rfvMaNPL.ErrorMessage = "\"Mã sản phẩm\"không được để trống";
            this.rfvMaNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaNPL.Icon")));
            this.rfvMaNPL.InitialValue = "rfvMaNPL";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtMaSPMoi;
            this.rfvDVT.ErrorMessage = "\"Mã sản phẩm mới\" bắt buộc phải nhập.";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ColumnAutoSizeMode = Janus.Windows.GridEX.ColumnAutoSizeMode.DisplayedCellsAndHeader;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 160);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(622, 134);
            this.dgList.TabIndex = 9;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // FormDieuChinhMaSP
            // 
            this.AcceptButton = this.btnUpdate;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(628, 305);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDieuChinhMaSP";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin sản phẩm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDieuChinhMaSP_FormClosing);
            this.Load += new System.EventHandler(this.DieuChinhSoLuongNPLForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSPMoi;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaNPL;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIComboBox cbDonViTinh;
        private System.Windows.Forms.Label label4;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Janus.Windows.EditControls.UIComboBox cbNhomSP;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuong;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton btnXoa;
    }
}