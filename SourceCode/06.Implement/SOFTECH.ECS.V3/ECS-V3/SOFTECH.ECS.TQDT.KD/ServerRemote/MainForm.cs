﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using SignRemote;
using Company.KDT.SHARE.Components;
using Company.Interface;
using IWshRuntimeLibrary;
using System.IO;
//using iTextSharp.text.log;
using System.Security.Cryptography.X509Certificates;
//using Org.BouncyCastle.Security;
//using iTextSharp.text.pdf.security;
using System.Security.Cryptography;
using System.Security;
//using iTextSharp.text.pdf;
using ServerRemote.BLL;
using Company.QuanTri;
using Company.Interface.VNACCS.Vouchers;

namespace ServerRemote
{
    public partial class MainForm : Company.Interface.BaseForm
    {
        public MainForm()
        {
            InitializeComponent();
        }
        public bool IsSignOnLan = false;
        public bool isSignVNACCS = false;
        public ISignMessage _service = WebService.SignMessage();
        public float signWidth = 300;
        public float signHeight = 50;
        public float trang = 0;
        public float ViTriKy = 0;
        public float LeNgang = 0;
        public float LeDoc = 1;
        public string FileName;
        public string FileData;
        public System.IO.FileStream fs;
        public byte[] bytes;
        public string filebase64 = "";
        public long filesize = 0;
        public long size;
        public byte[] data;
        private bool isData = true;
        List<SignLogger> dataSigns = new List<SignLogger>();
        public static ECSPrincipal EcsQuanTri;
        public static bool isLoginSuccess = false;
        public string msgFeedBack;
        private ProcessAPI proc = new ProcessAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
        private ProcessFileAPI prof = new ProcessFileAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
        private void explorerSign_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //
                case "cmdMessageSend":
                    this.MessageSend();
                    break;
                case "cmdMessageSigned":
                    this.MessageSigned();
                    break;
                //
                case "cmdFileSend":
                    this.FileSend();
                    break;
                case "cmdFileSigned":
                    this.FileSigned();
                    break;
                case "cmdSign":
                    this.Sign();
                    break;
            }
        }

        private void Sign()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SignDigitalDocumentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            SignDigitalDocumentForm f = new SignDigitalDocumentForm();
            f.MdiParent = this;
            f.Show();
        }

        private void FileSigned()
        {
            try
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FileSignedForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                FileSignedForm f = new FileSignedForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FileSend()
        {
            try
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FileSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                FileSendForm f = new FileSendForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void MessageSigned()
        {
            try
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("MessageSignedForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                MessageSignedForm f = new MessageSignedForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void MessageSend()
        {
            try
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("MessageSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                MessageSendForm f = new MessageSendForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void explorerBarConfig_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //
                case "cmdConfig":
                    this.Config();
                    break;
                case "cmdUSB":
                    this.USB();
                    break;
                //
                case "cmdChangePassword":
                    this.ChangePassword();
                    break;
                case "cmdList":
                    this.List();
                    break;
                case "cmdExit":
                    Application.Exit();
                    break;
                case "cmdSwitchUser":
                    SwitchUser();
                    break;                    
            }
        }

        private void SwitchUser()
        {            
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("frmLogin"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            frmLogin f = new frmLogin();
            f.MdiParent = this;
            f.Show();
        }

        private void List()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("frmDanhSachAccount"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmDanhSachAccount f = new frmDanhSachAccount();
            f.MdiParent = this;
            f.Show();
        }

        private void ChangePassword()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("frmChangePassDL"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmChangePassDL f = new frmChangePassDL();
            f.MdiParent = this;
            f.Show();
        }

        private void USB()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("frmLoginUSB"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmLoginUSB f = new frmLoginUSB();
            f.MdiParent = this;
            f.Show();
        }

        private void Config()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThietLapThongSoKBForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.MdiParent = this;
            f.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Login login = new Login();
                //login.ShowDialog();
                //if (isLoginSuccess)
                //{
                    this.Show();
                    CreateShorcut();
                    CreateShorcutAutoUpdate();
                    //MessageSend();
                    //MessageSigned();
                    //FileSend();
                    //FileSigned();
                    IsSignOnLan = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignOnLan").ToString());
                    string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                    this.cmdVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));
                    Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChuKySo");
                    System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
                    if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                    {
                        x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                    }
                    else
                    {
                        msgFeedBack = "Chưa cấu hình chữ ký số để thực hiện ký số . Doanh nghiệp hãy cấu hình chữ ký số để thực hiện ký số";
                        MessageBoxControl.ShowMessage(msgFeedBack);
                        proc.IsAbort = true;
                        prof.IsAbort = true;
                        SetTextStatus();
                        return;
                    }
                    if (x509Certificate2 == null)
                    {
                        msgFeedBack = "Không tìm thấy chữ ký số . Doanh nghiệp hãy kiểm tra lại Thiết bị USB chữ ký số ";
                        MessageBoxControl.ShowMessage(msgFeedBack);
                        proc.IsAbort = true;
                        prof.IsAbort = true;
                        SetTextStatus();
                        return;
                    }
                    Excute();
                    ExcuteFile();
                    SetTextStatus();
                //}
                //else
                //{
                //    Application.Exit();
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetTextStatus()
        {
            if (proc.IsAbort == true && prof.IsAbort == true)
            {
                btnSign.Text = "BẮT ĐẦU KÝ TỰ ĐỘNG";
                lblStatus.Text = "TẠM DỪNG";
            }
            else
            {
                btnSign.Text = "TẠM DỪNG KÝ TỰ ĐỘNG";
                lblStatus.Text = "ĐANG KÝ";
            }
        }
        public void Excute()
        {
            proc = new ProcessAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
            proc.Sign += new EventHandler<SignEventArgs>(proc_Sign);
            proc.DoProcess();
            SetTextStatus();
        }
        public void ExcuteFile()
        {
            prof = new ProcessFileAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
            prof.FileSign += new EventHandler<SignFileEventArgs>(proc_SignFile);
            prof.DoProcessFile();
            SetTextStatus();
        }
        private void SendHandler(object sender, SignEventArgs e)
        {

            if (e.Error == null && e.Content != null)
            {
                SignLogger sign = new SignLogger()
                {
                    MSG_FROM = e.Content.From,
                    MESSAGE = e.Content.Message.Content,
                    MSG_TO = e.Content.To,
                    TIME_PROCESS = new DateTime(e.TotalTime.Ticks),
                    DATECREATE = DateTime.Now
                };
                try
                {
                    if (this.isData)
                    {
                        sign.Insert();
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                    }
                    else
                    {
                        sign.InsertAccess(Application.StartupPath);
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                    }
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", e.Content.From));
                }
                catch
                {
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                }
            }
            else
            {
                if (e.Content.From == null) e.Content.From = "Anonymous";
            }
        }
        private void SenFiledHandler(object sender, SignFileEventArgs e)
        {

            if (e.Error == null && e.Content != null)
            {
                SignLogger sign = new SignLogger()
                {
                    MSG_FROM = e.Content.From,
                    MESSAGE = e.Content.Message.FileName,
                    MSG_TO = e.Content.To,
                    TIME_PROCESS = new DateTime(e.TotalTime.Ticks),
                    DATECREATE = DateTime.Now
                };
                try
                {
                    if (this.isData)
                    {
                        sign.Insert();
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                    }
                    else
                    {
                        sign.InsertAccess(Application.StartupPath);
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                    }
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ :  '{0}' - Tên File đã ký :  '{1}'", e.Content.From, e.Content.Message.FileName));
                }
                catch
                {
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                }
            }
            else
            {
                if (e.Content.From == null) e.Content.From = "Anonymous";
            }
        }
        void proc_Sign(object sender, SignEventArgs e)
        {
            try
            {
                this.Invoke(
                     new EventHandler<SignEventArgs>(SendHandler),
                         sender, e);
            }
            catch { }
        }
        void proc_SignFile(object sender, SignFileEventArgs e)
        {
            try
            {
                this.Invoke(
                     new EventHandler<SignFileEventArgs>(SenFiledHandler),
                         sender, e);
            }
            catch { }
        }
        private void DoSignContent()
        {
            try
            {
                MessageSignature signInfo;
                string content;
                if (IsSignOnLan)
                {
                    List<INBOX> InboxCollection = INBOX.SelectCollectionAll();
                    foreach (INBOX item in InboxCollection)
                    {
                        if (item.STATUS != SendStatusInfo.SendVNACCS)
                        {

                            content = item.MSG_ORIGIN;
                            signInfo = Helpers.GetSignature(content);
                        }
                        else
                        {
                            content = Helpers.ConvertFromBase64VNACCS(item.MSG_ORIGIN);
                            Helpers.GetSignature("Test");
                            System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2New(Globals.GetX509CertificatedName);
                            signInfo = new MessageSignature()
                            {
                                Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                            };
                        }
                        MSG_OUTBOX MSG_OUTBOX = new MSG_OUTBOX();
                        MSG_OUTBOX.GUIDSTR = item.GUIDSTR;
                        MSG_OUTBOX.MSG_SIGN_DATA = signInfo.Data;
                        MSG_OUTBOX.MSG_SIGN_CERT = signInfo.FileCert;
                        MSG_OUTBOX.CREATE_TIME = DateTime.Now;
                        MSG_OUTBOX.MSG_FROM = item.MSG_FROM;
                        MSG_OUTBOX.MSG_TO = System.Environment.MachineName.ToString();
                        MSG_OUTBOX.InsertUpdate();
                        MSG_INBOX.DeleteDynamic("GUIDSTR = '" + MSG_OUTBOX.GUIDSTR + "'");
                        PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", MSG_OUTBOX.MSG_FROM));
                    }
                }
                else
                {
                    DataTable dt = _service.GetNewMessageClientSend(Globals.UserNameLogin);
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToInt32(dr["STATUS"].ToString()) == SendStatusInfo.NoSign || Convert.ToInt32(dr["STATUS"].ToString()) == SendStatusInfo.NoSignVNACCS)
                        {
                            if (Convert.ToInt32(dr["STATUS"].ToString()) != SendStatusInfo.NoSignVNACCS)
                            {

                                content = dr["MSG_ORIGIN"].ToString();
                                signInfo = Helpers.GetSignature(content);
                            }
                            else
                            {
                                content = Helpers.ConvertFromBase64VNACCS(dr["MSG_ORIGIN"].ToString());
                                Helpers.GetSignature("Test");
                                System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2New(Globals.GetX509CertificatedName);
                                signInfo = new MessageSignature()
                                {
                                    Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                                };
                            }
                            MessageSend msgReturn = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                To = dr["MSG_FROM"].ToString(),
                                SendStatus = SendStatusInfo.Send,
                                Message = new SignRemote.Message()
                                {
                                    SignData = signInfo.Data,
                                    SignCert = signInfo.FileCert,
                                    GuidStr = dr["GUIDSTR"].ToString()
                                }
                            };
                            _service.ServerSend(Helpers.Serializer(msgReturn));
                            PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", msgReturn.From));

                            MessageSend msg = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                SendStatus = SendStatusInfo.Request
                            };
                            _service = WebService.SignMessage();
                            _service.ServerSend(Helpers.Serializer(msg));   
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void DoSignContentFile()
        {
                try
                {
                    #region SignData
                    string FilePath = "";
                    string FilePathSiged = "";
                    if (IsSignOnLan)
                    {
                        List<SignRemote.MSG_FILE_INBOX> InboxCollection = SignRemote.MSG_FILE_INBOX.SelectCollectionAll();
                        foreach (SignRemote.MSG_FILE_INBOX item in InboxCollection)
                        {
                            FileName = item.FILE_NAME;
                            FileData = item.FILE_DATA;

                            string path = Application.StartupPath + "\\Temp";

                            //Delete File Temp 
                            System.IO.DirectoryInfo di = new DirectoryInfo(path);
                            try
                            {
                                foreach (FileInfo file in di.GetFiles())
                                {
                                    file.Delete();
                                }
                                foreach (DirectoryInfo dir in di.GetDirectories())
                                {
                                    dir.Delete(true);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }

                            if (System.IO.Directory.Exists(path) == false)
                            {
                                System.IO.Directory.CreateDirectory(path);
                            }
                            FilePath = path + "\\" + FileName;
                            FilePathSiged = path + "\\" + FileName.Replace(".pdf", "_Signed.pdf");
                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.File.Delete(FilePath);
                            }
                            if (System.IO.File.Exists(FilePathSiged))
                            {
                                System.IO.File.Delete(FilePathSiged);
                            }
                            fs = new System.IO.FileStream(FilePath, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                            bytes = System.Convert.FromBase64String(FileData);
                            fs.Write(bytes, 0, bytes.Length);
                            fs.Close();
                            //if (SignFile(FilePath, FilePathSiged))
                            //{
                            //    fs = new System.IO.FileStream(FilePathSiged, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                            //    size = 0;
                            //    size = fs.Length;
                            //    data = new byte[fs.Length];
                            //    fs.Read(data, 0, data.Length);
                            //    filebase64 = System.Convert.ToBase64String(data);
                            //    fs.Flush();
                            //    fs.Close();
                            //    FileName = FileName.Replace(".pdf", "_Signed.pdf");
                            //    FileData = filebase64;
                            //}
                            SignRemote.MSG_FILE_OUTBOX MSG_FILE_OUTBOX = new SignRemote.MSG_FILE_OUTBOX();
                            MSG_FILE_OUTBOX.GUIDSTR = item.GUIDSTR;
                            MSG_FILE_OUTBOX.FILE_NAME = FileName;
                            MSG_FILE_OUTBOX.FILE_DATA = FileData;
                            MSG_FILE_OUTBOX.CREATE_TIME = DateTime.Now;
                            MSG_FILE_OUTBOX.MSG_FROM = item.MSG_FROM;
                            MSG_FILE_OUTBOX.MSG_TO = System.Environment.MachineName.ToString(); ;
                            MSG_FILE_OUTBOX.InsertUpdate();
                            PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ :  '{0}' - Tên File đã ký :  '{1}'", MSG_FILE_OUTBOX.MSG_FROM, MSG_FILE_OUTBOX.FILE_NAME));

                            SignRemote.MSG_FILE_INBOX.DeleteDynamic("GUIDSTR='" + item.GUIDSTR + "'");
                        }
                    }
                    else
                    {
                        DataTable dt = _service.GetNewFileClientSend(Globals.UserNameLogin);

                        string path = Application.StartupPath + "\\Temp";

                        //Delete File Temp 
                        System.IO.DirectoryInfo di = new DirectoryInfo(path);
                        try
                        {
                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        if (System.IO.Directory.Exists(path) == false)
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            FileName = dr["FILE_NAME"].ToString();
                            FileData = dr["FILE_DATA"].ToString();
                            FilePath = path + "\\" + FileName;
                            FilePathSiged = path + "\\" + FileName.Replace(".pdf", "_Signed.pdf");
                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.File.Delete(FilePath);
                            }
                            if (System.IO.File.Exists(FilePathSiged))
                            {
                                System.IO.File.Delete(FilePathSiged);
                            }
                            fs = new System.IO.FileStream(FilePath, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                            bytes = System.Convert.FromBase64String(FileData);
                            fs.Write(bytes, 0, bytes.Length);
                            fs.Close();
                            //if (SignFile(FilePath, FilePathSiged))
                            //{
                            //    fs = new System.IO.FileStream(FilePathSiged, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                            //    size = 0;
                            //    size = fs.Length;
                            //    data = new byte[fs.Length];
                            //    fs.Read(data, 0, data.Length);
                            //    filebase64 = System.Convert.ToBase64String(data);
                            //    fs.Flush();
                            //    fs.Close();
                            //    FileName = FileName.Replace(".pdf", "_Signed.pdf");
                            //    FileData = filebase64;
                            //}
                            MessageSendFile msgReturn = new MessageSendFile()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                To = dr["MSG_FROM"].ToString(),
                                SendStatus = SendStatusInfo.Send,
                                Message = new SignRemote.MessageFile()
                                {
                                    FileName = FileName,
                                    FileData = FileData,
                                    GuidStr = dr["GUIDSTR"].ToString()
                                }
                            };
                            _service.ServerSendFile(Helpers.Serializer(msgReturn));
                            PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ :  '{0}' - Tên File đã ký :  '{1}'", msgReturn.From, msgReturn.Message.FileName));

                            MessageSendFile msg = new MessageSendFile()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                SendStatus = SendStatusInfo.Request
                            };
                            _service = WebService.SignMessage();
                            _service.ServerSendFile(Helpers.Serializer(msg));   
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            return string.Format("{0}", name);
        }

        //public void SignPDF(String src, String dest,
        //                 ICollection<Org.BouncyCastle.X509.X509Certificate> chain, X509Certificate2 pk,
        //                 String digestAlgorithm, CryptoStandard subfilter,
        //                 String reason, String location,
        //                 ICollection<ICrlClient> crlList,
        //                 IOcspClient ocspClient,
        //                 ITSAClient tsaClient,
        //                 int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        Dictionary<PdfName, int> exclusionSizes = new Dictionary<PdfName, int>();
        //        reader = new PdfReader(src);
        //        os = new FileStream(dest, FileMode.Create);
        //        stamper = PdfStamper.CreateSignature(reader, os, '\0');
        //        // Creating the appearance
        //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //        //appearance.Reason = reason;
        //        //appearance.Location = location;
        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, "Signed");
        //        //appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
        //        //appearance.Layer2Font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN);
        //        //iTextSharp.text.Font font = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 14, BaseColor.RED);
        //        //appearance.Layer2Font = font;
        //        //

        //        iTextSharp.text.Font font = new iTextSharp.text.Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false), 12);
        //        font.SetColor(0xff, 0, 0);
        //        appearance.Layer2Font = font;
        //        appearance.Layer2Text = "Ký bởi: " + GetName(pk.SubjectName.Name) + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

        //        float width = reader.GetPageSize(1).Width;
        //        float height = reader.GetPageSize(1).Height;
        //        int page = 1;
        //        if (trang == 1)
        //        {
        //            width = reader.GetPageSize(reader.NumberOfPages).Width;
        //            height = reader.GetPageSize(reader.NumberOfPages).Height;
        //            page = reader.NumberOfPages;
        //        }
        //        float llx = 0f;
        //        float signHeigh = signHeight;
        //        if (LeNgang == 1)
        //        {
        //            llx = width / 2f;
        //        }
        //        else if (LeNgang == 2)
        //        {
        //            llx = (width - 300f) / 2f;
        //        }
        //        if (LeDoc == 0)
        //        {
        //            signHeigh = height;
        //        }

        //        float yPos = reader.GetPageSize(1).Height - 50;
        //        float xPos = 70.0f;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, "Signed");

        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(llx, signHeigh, llx + signWidth, signHeigh - signHeight), page, "Signed");
        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(0, signWidth, 0, signHeigh), page, "Signed");

        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, null);
        //        PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
        //        {
        //            Date = new PdfDate(appearance.SignDate),
        //            Name = ""
        //        };
        //        if (appearance.Reason != null)
        //        {
        //            signature.Reason = appearance.Reason;
        //        }
        //        if (appearance.Location != null)
        //        {
        //            signature.Location = appearance.Location;
        //        }
        //        appearance.CryptoDictionary = signature;

        //        // insert the next two lines
        //        appearance.Acro6Layers = false;
        //        //appearance.Layer4Text = PdfSignatureAppearance.questionMark;

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        string password = Company.KDT.SHARE.Components.Globals.PasswordSign.ToString();
        //        if (password.Contains("=="))
        //        {
        //            password = Helpers.DecryptString(Company.KDT.SHARE.Components.Globals.PasswordSign.ToString(), "KEYWORD");
        //        }
        //        var secure = new SecureString();
        //        foreach (char c in password)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //private bool SignFile(String src, String dest)
        //{
        //    try
        //    {
        //        #region
        //        LoggerFactory.GetInstance().SetLogger(new SysoLogger());


        //        X509Store x509Store = new X509Store("My");
        //        x509Store.Open(OpenFlags.ReadOnly);
        //        X509Certificate2Collection certificates = x509Store.Certificates;
        //        IList<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();
        //        X509Certificate2 pk = null;
        //        string items = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChuKySo", "");//cbSigns.SelectedValue.ToString();
        //        //ItemSign items = (ItemSign)cbSigns.SelectedValue;
        //        if (certificates.Count > 0)
        //        {
        //            foreach (X509Certificate2 cer in certificates)
        //            {

        //                if (cer.SubjectName.Name == items)
        //                {
        //                    X509Certificate2Collection certificateCollection = new X509Certificate2Collection();
        //                    certificateCollection.Add(cer);
        //                    X509Certificate2Enumerator certificatesEn = certificateCollection.GetEnumerator();
        //                    certificatesEn.MoveNext();
        //                    pk = certificatesEn.Current;

        //                    X509Chain x509chain = new X509Chain();
        //                    x509chain.Build(pk);

        //                    foreach (X509ChainElement x509ChainElement in x509chain.ChainElements)
        //                    {
        //                        chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
        //                    }
        //                }
        //            }
        //        }
        //        x509Store.Close();


        //        IOcspClient ocspClient = new OcspClientBouncyCastle();
        //        ITSAClient tsaClient = null;
        //        for (int i = 0; i < chain.Count; i++)
        //        {
        //            Org.BouncyCastle.X509.X509Certificate cert = chain[i];
        //            String tsaUrl = CertificateUtil.GetTSAURL(cert);
        //            if (tsaUrl != null)
        //            {
        //                tsaClient = new TSAClientBouncyCastle(tsaUrl);
        //                break;
        //            }
        //        }
        //        IList<ICrlClient> crlList = new List<ICrlClient>();
        //        crlList.Add(new CrlClientOnline(chain));
        //        string item = Company.KDT.SHARE.Components.Globals.ChuKySo; ;//cbSigns.SelectedValue.ToString();
        //        //ItemSign item = (ItemSign)cbSigns.SelectedValue;
        //        SignPDF(src, dest, chain, pk, DigestAlgorithms.SHA1, CryptoStandard.CMS, "Test",
        // "Ghent",
        // crlList, ocspClient, tsaClient, 0);
        //        return true;
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //
                case "cmdConfigConnection":
                    this.Config();
                    break;
                case "cmdUSB":
                    this.USB();
                    break;
                //
                case "cmdChangePassword":
                    this.ChangePassword();
                    break;
                case "cmdList":
                    this.List();
                    break;
                case "cmdExit":
                    Application.Exit();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        public void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }
        public void setNotifyIcon(string text)
        {
            notifyIcon1.Visible = true;
            string sfmtCurr = notifyIcon1.BalloonTipText;
            notifyIcon1.BalloonTipText = text;
            notifyIcon1.ShowBalloonTip(500);
            notifyIcon1.BalloonTipText = sfmtCurr;
        }
        public void PopUpThongBao(string contentThongBao)
        {
            popupNotifier1.TitleText = "THÔNG BÁO TỪ ECS-SIGNREMOTE";
            popupNotifier1.ContentText = contentThongBao;
            popupNotifier1.ShowCloseButton = true;
            popupNotifier1.ShowOptionsButton = false;
            popupNotifier1.ShowGrip = false;
            popupNotifier1.Delay = 3000;
            popupNotifier1.AnimationInterval = 10;
            popupNotifier1.AnimationDuration = 1000;
            popupNotifier1.Popup();
        }
        public void GetNewFileClientSend()
        {
            try
            {
                DataTable dt = _service.GetNewFileClientSend(Globals.UserNameLogin);
                if (dt != null && dt.Rows.Count >= 1)
                {
                    DoSignContentFile();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewMessageClientSend()
        {
            try
            {
                DataTable dt = _service.GetNewMessageClientSend(Globals.UserNameLogin);
                if (dt != null && dt.Rows.Count >= 1)
                {
                    DoSignContent();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewMessageSend()
        {
            try
            {
                List<INBOX> InboxCollection = INBOX.SelectCollectionAll();
                if (InboxCollection.Count >= 1)
                {
                    DoSignContent();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewFileSend()
        {
            try
            {
                List<MSG_FILE_INBOX> InboxCollection = MSG_FILE_INBOX.SelectCollectionAll();
                if (InboxCollection.Count >= 1)
                {
                    DoSignContentFile();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (!IsSignOnLan)
            //{
            //    GetNewMessageClientSend();
            //    GetNewFileClientSend();
            //}
            //else
            //{
            //    GetNewMessageSend();
            //    GetNewFileSend();
            //}
            //Excute();
            //ExcuteFile();
        }

        private void cmdMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdUpdateDatabase":
                    UpdateDatabase();
                    break;
                case "cmdAutoUpdate":
                    AutoUpdate("MSG");
                    break;
                case "cmdConfig":
                    ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
                    f.Show(this);
                    break;
                case "cmdConfigUSB":
                    frmLoginUSB frm = new frmLoginUSB();
                    frm.ShowDialog(this);
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
                case "cmdExit":
                    Application.Exit();
                    break;
                case "cmdVersion":                    
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
            }
        }
        private void CreateShorcutAutoUpdate()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + "AUTO UPDATE " + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = AppDomain.CurrentDomain.BaseDirectory + "\\AppAutoUpdate.exe";
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void AutoUpdate(string args)
        {
            try
            {
                CreateShorcutAutoUpdate();
                //Kiem tra phien ban du lieu truoc khi cap nhat phien ban chuong trinh moi
                if (Helper.Controls.UpdateDatabaseSQLForm.Instance(true).IsHasNewDataVersion)
                {
                    UpdateNewDataVersion();
                }

                Company.KDT.SHARE.Components.DownloadUpdate dl = new DownloadUpdate(args);
                dl.DoDownload();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void UpdateNewDataVersion()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Helper.Controls.UpdateDatabaseSQLForm.Instance(true).ShowDialog();

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void UpdateDatabase()
        {
            UpdateNewDataVersion();
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            if (proc.IsAbort == true && prof.IsAbort == true)
            {
                Excute();
                ExcuteFile();
            }
            else
            {
                proc.IsAbort = true ;
                prof.IsAbort = true;
            }
            SetTextStatus();
        }

        private void uiPanelManager1_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this);
            }
        }
        private void doCloseMe()
        {
            Form form = uiPanelManager1.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != uiPanelManager1.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }
    }
}
