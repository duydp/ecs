﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class TQDTPhuLucToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewTKNTQDTForm report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public static bool inMaHang = false;
        public TQDTPhuLucToKhaiNhap()
        {
            InitializeComponent();
           

        }
        public void BindReport(string pls)
        {
            try
            {
                xrLabel3.Text = this.TKMD.MaLoaiHinh;//.Substring(1, 2);
                this.PrintingSystem.ShowMarginsWarning = false;
                decimal tongTriGiaNT = 0;
                decimal tongThueXNK = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;



                string chicuc = Company.BLL.KDT.SXXK.NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHaiQuanCuaKhau.Text = chicuc;

                //lblChiCucHaiQuanCuaKhau.Text = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

                if (TKMD.NgayDangKy > new DateTime(1900, 1, 1))
                    lblNgayDangKy.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    lblNgayDangKy.Text = "";

                if (TKMD.SoToKhai != 0)
                    lblSoToKhai.Text = TKMD.SoToKhai + "";
                else
                    lblSoToKhai.Text = "";

                //lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
                //lblThongBaoMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

                lblThongBaoMienThue.Visible = MienThue1;
                lblThongBaoMienThueGTGT.Visible = MienThue2;
                lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
                lblThongBaoMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

                for (int i = 0; i < 9; i++)
                {
                    XRControl control = new XRControl();

                    //Ten hang
                    this.xrTable1.Rows[i + 3].Controls["xrTableCell" + (i + 1)].Text = string.Format("{0}", i + 1 + (int.Parse(pls) - 1 > 0 ? (int.Parse(pls) - 1) * 9 : 0));

                    //Tien thue
                    this.xrTable2.Rows[i + 2].Controls["xrTableCell" + (34 + i + 1)].Text = string.Format("{0}", i + 1 + (int.Parse(pls) - 1 > 0 ? (int.Parse(pls) - 1) * 9 : 0));
                }

                xrLabel1.Text = pls;
                for (int i = 0; i < this.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.HMDCollection[i];
                    //Comment by Hungtq 19/03/2012.
                    //this.xrTable1.Rows[i + 3].Controls["xrTableCell" + (i + 1)].Text = string.Format("{0}", i + 1 + (int.Parse(pls) - 1) * 10);

                    control = this.xrTable1.Rows[i + 3].Controls["TenHang" + (i + 1)];//2
                    //if(control !=null )
                    if (!inMaHang)
                        control.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            control.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            control.Text = hmd.TenHang;
                    }
                    try
                    {
                        control.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                        return;
                    }

                    control = this.xrTable1.Rows[i + 3].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;

                    control = this.xrTable1.Rows[i + 3].Controls["XuatXu" + (i + 1)];
                    control.Text = hmd.NuocXX_ID;

                    control = this.xrTable1.Rows[i + 3].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString("G20");

                    control = this.xrTable1.Rows[i + 3].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID);

                  
                    control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("N" + GlobalSettings.DonGiaNT);

                    control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hmd.TriGiaTT.ToString("N0");

                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    control.Text = hmd.ThueSuatXNK.ToString("N0");
                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                    control.Text = hmd.TyLeThuKhac.ToString("N0");
                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                    //control.Text = hmd.PhuThu.ToString("N0");
                    control.Text = hmd.TriGiaThuKhac.ToString("N0");
                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hmd.ThueXNK.ToString("N0");
                    if (MienThue1) control.Text = "";

                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    control.Text = hmd.ThueSuatVATGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TyLeThuKhac) > 0)
                            control.Text = hmd.TyLeThuKhac.ToString("N0");
                        else
                            control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TriGiaThuKhac) > 0)
                            control.Text = hmd.TriGiaThuKhac.ToString("N0");
                        else
                            control.Text = "";
                    }
                    else if (hmd.ThueGTGT == 0 && hmd.ThueTTDB > 0)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        // if (hmd.ThueSuatTTDBGiam.Length == 0)
                        control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    control.Text = hmd.ThueSuatTTDBGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        control.Text = hmd.TyLeThuKhac.ToString("N0");

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    else if (hmd.ThueGTGT > 0 && hmd.ThueTTDB > 0)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        // if (hmd.ThueSuatTTDBGiam.Length == 0)
                        control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        // else
                        //     control.Text = hmd.ThueSuatTTDBGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    control.Text = hmd.ThueSuatVATGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                    }
                    // }

                    //if (!hmd.FOC)
                    //{
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                    tongThueXNK += hmd.ThueXNK;
                    //tongTriGiaThuKhac += hmd.PhuThu;
                    tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {

                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {

                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                    //}
                }
                lblTongbangso.Text = (tongThueXNK + tongTriGiaThuKhac + tongThueGTGT).ToString("N0"); //chi tinh rieng phan hang tren phan phu luc, khong tinh tat va cac hang. //this.TinhTongThueHMD().ToString();
                string s = Company.BLL.Utils.VNCurrency.ToString((tongThueXNK + tongTriGiaThuKhac + tongThueGTGT)); //Company.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                lblTongThueXNKChu.Text = s.Replace("  ", " ");

                lblTongTriGiaNT.Text = tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TongTriGiaNT));
                lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
                lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                if (tongTriGiaThuKhac > 0)
                    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
                else
                    lblTongTriGiaThuKhac.Text = "";
                if (MienThue1) lblTongTienThueXNK.Text = "";
                if (MienThue2) lblTongTienThueGTGT.Text = "";

                if (MienThue1 && MienThue2)
                {
                    lblTongbangso.Text = lblTongThueXNKChu.Text = lblTongTriGiaThuKhac.Text=string.Empty;
                }

                if (System.IO.File.Exists(System.Windows.Forms.Application.StartupPath + "\\Image\\lblMaVach.Image.gif"))
                    lblMaVach.Image = Image.FromFile(System.Windows.Forms.Application.StartupPath + "\\Image\\lblMaVach.Image.gif");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                //if (!hmd.FOC)
                tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            }
            return tong;
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            // lblThongBaoMienThue.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }
        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        //private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        //{
        //    XRControl cell = (XRControl)sender;
        //    report.Cell = cell;
        //    report.txtTenNhomHang.Text = cell.Text;
        //    report.label3.Text = cell.Tag.ToString();
        //}
        public void SetChiCucHQCuaKhau(string ChiCucHQCuaKhau)
        {
            lblChiCucHaiQuanCuaKhau.Text = ChiCucHQCuaKhau;
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang4_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang5_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang7_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang8_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang9_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
