﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.GC.BLL.KDT
{    
    public partial class KetQuaXuLy
    {
        public const string LoaiThongDiep_TuChoiTiepNhan = "Từ chối tiếp nhận";
        public const string LoaiThongDiep_KhaiBaoThanhCong = "Khai báo thành công";
        public const string LoaiThongDiep_ToKhaiDuocCapSo = "Tờ khai được cấp số";
        public const string LoaiThongDiep_ToKhaiDuocPhanLuong = "Tờ khai được phân luồng";
        public const string LoaiThongDiep_HopDongDuocPhanLuong = "Hợp đồng được phân luồng";
        public const string LoaiThongDiep_PhuKienDuocPhanLuong = "Phụ kiện được phân luồng";
        public const string LoaiThongDiep_ToKhaiSuaDuocDuyet = "Tờ khai sửa được duyệt";
        public const string LoaiThongDiep_KhaiBaoHuyTK_ThanhCong = "Khai báo hủy tờ khai thành công";
        public const string LoaiThongDiep_KhaiBaoHuyTK_DuocDuyet = "Tờ khai hủy được duyệt";
        public const string LoaiThongDiep_ToKhaiChuyenTT = "Tờ khai chuyển trạng thái";

        public const string LoaiChungTu_ToKhai = "TK";
        public const string LoaiChungTu_HopDong = "HD";
        public const string LoaiChungTu_NguyenPhuLieu = "NPL";
        public const string LoaiChungTu_SanPham = "SP";
        public const string LoaiChungTu_ThietBi = "TB";
        public const string LoaiChungTu_PhuKien = "PK";
        public const string LoaiChungTu_ToKhaiCT = "TKCT";

        public static List<KetQuaXuLy> SelectCollectionBy_ItemID(long itemID)
        {
            string whereCondition = string.Format("ItemID = {0}", itemID);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static List<KetQuaXuLy> SelectCollectionBy_ReferenceID(Guid referenceID)
        {
            string whereCondition = string.Format("ReferenceID = '{0}'", referenceID);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static string LayKetQuaXuLy(Guid referenceID, string loaiThongDiep)
        {
            IList<KetQuaXuLy> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(referenceID, loaiThongDiep);
            string msg = string.Empty;

            foreach (var kqxl in list)
            {
                msg += kqxl.NoiDung + "\r\n";
            }
            return msg;
        }

        public static string LayKetQuaXuLy(long itemID, string loaiThongDiep)
        {
            IList<KetQuaXuLy> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(itemID, loaiThongDiep);
            string msg = string.Empty;

            foreach (var kqxl in list)
            {                
                msg += kqxl.NoiDung + "\r\n";
            }
            return msg;
        }

        private static List<KetQuaXuLy> SelectCollectionBy_ReferenceID_LoaiThongDiep(Guid referenceID, string loaiThongDiep)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND LoaiThongDiep = N'{1}'", referenceID, loaiThongDiep);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<KetQuaXuLy> SelectCollectionBy_ReferenceID_LoaiThongDiep(long itemID, string loaiThongDiep)
        {
            string whereCondition = string.Format("ItemID = {0} AND LoaiThongDiep = N'{1}' AND ID = (SELECT MAX(ID) FROM dbo.t_KDT_KetQuaXuLy)", itemID, loaiThongDiep);
            string orderByExpression = "Ngay ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }
    }
}
