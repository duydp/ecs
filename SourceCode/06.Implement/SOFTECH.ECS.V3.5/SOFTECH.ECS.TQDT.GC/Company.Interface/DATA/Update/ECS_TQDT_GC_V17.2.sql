
INSERT INTO t_VNACC_Category_CustomsSubSection (TableID,CustomsCode,CustomsSubSectionCode,ImportExportClassification,Name) VALUES ('A014A','33PD','00','I',N'Chi cục HQ Thủy An')
INSERT INTO t_VNACC_Category_CustomsSubSection (TableID,CustomsCode,CustomsSubSectionCode,ImportExportClassification,Name) VALUES ('A014A','33PD','00','E',N'Chi cục HQ Thủy An')

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '17.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('17.2',GETDATE(), N'Cap nhat nhom xu ly ho so')
END	