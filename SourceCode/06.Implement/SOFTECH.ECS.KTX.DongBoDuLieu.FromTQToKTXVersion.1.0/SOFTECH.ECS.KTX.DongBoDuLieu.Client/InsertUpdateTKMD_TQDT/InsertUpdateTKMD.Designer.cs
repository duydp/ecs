﻿namespace DongBo
{
    partial class InsertUpdateTKMD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prcbar = new Janus.Windows.EditControls.UIProgressBar();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSoToKhai = new System.Windows.Forms.Label();
            this.lblNoiDung = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblChungTu = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // prcbar
            // 
            this.prcbar.Location = new System.Drawing.Point(12, 52);
            this.prcbar.Name = "prcbar";
            this.prcbar.Size = new System.Drawing.Size(511, 27);
            this.prcbar.TabIndex = 0;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Location = new System.Drawing.Point(488, 36);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(35, 13);
            this.lblTrangThai.TabIndex = 1;
            this.lblTrangThai.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số tờ khai đã được cập nhật:";
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.AutoSize = true;
            this.lblSoToKhai.Location = new System.Drawing.Point(186, 112);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Size = new System.Drawing.Size(13, 13);
            this.lblSoToKhai.TabIndex = 1;
            this.lblSoToKhai.Text = "0";
            // 
            // lblNoiDung
            // 
            this.lblNoiDung.AutoSize = true;
            this.lblNoiDung.Location = new System.Drawing.Point(12, 36);
            this.lblNoiDung.Name = "lblNoiDung";
            this.lblNoiDung.Size = new System.Drawing.Size(97, 13);
            this.lblNoiDung.TabIndex = 1;
            this.lblNoiDung.Text = "Nội dung thực hiện";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(12, 141);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(10, 13);
            this.lblError.TabIndex = 1;
            this.lblError.Text = ".";
            // 
            // lblChungTu
            // 
            this.lblChungTu.AutoSize = true;
            this.lblChungTu.Location = new System.Drawing.Point(12, 87);
            this.lblChungTu.Name = "lblChungTu";
            this.lblChungTu.Size = new System.Drawing.Size(10, 13);
            this.lblChungTu.TabIndex = 1;
            this.lblChungTu.Text = ".";
            // 
            // InsertUpdateTKMD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 196);
            this.Controls.Add(this.lblSoToKhai);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblChungTu);
            this.Controls.Add(this.lblNoiDung);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTrangThai);
            this.Controls.Add(this.prcbar);
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertUpdateTKMD";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tiến trình thực hiện";
            this.Load += new System.EventHandler(this.frmProcess_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.EditControls.UIProgressBar prcbar;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSoToKhai;
        private System.Windows.Forms.Label lblNoiDung;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblChungTu;
    }
}