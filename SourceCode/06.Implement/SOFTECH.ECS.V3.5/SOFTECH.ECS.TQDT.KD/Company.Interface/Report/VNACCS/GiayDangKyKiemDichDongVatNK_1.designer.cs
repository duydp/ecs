namespace Company.Interface.Report.VNACCS
{
    partial class GiayDangKyKiemDichDongVatNK_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayDangKyKiemDichDongVatNK_1));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDonViCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblToChucCaNhanXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhaTenDuongXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongXaXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuanHuyenXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTinhThanhXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDienthoaiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoFaxXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNuocXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaToChucCaNhanXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenToChucCaNhanXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDangKyKD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDienThoaiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoFaxNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNuocQuaCanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhuongTienVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhuongTienVC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCuaKhauNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCuaKhauNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemKiemDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBenhMienDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayTiemPhong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKhuTrung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNongDo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemNuoiTrong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKiemDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiGianKiemDich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemGiamSat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayGiamSat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiGianGiamSat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoBanCanCap = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVatDungKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoSoLienQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKetQuaKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoiChuyenDen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblNgayKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDonXinCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChucNangChungTu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEmailNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMaBuuChinhNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMaNuocNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDienThoaiNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoFaxNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable8,
            this.xrTable9,
            this.xrTable7});
            this.Detail.HeightF = 817.8332F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40});
            this.xrTable4.SizeF = new System.Drawing.SizeF(801.0001F, 617F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell26,
            this.xrTableCell30,
            this.lblMaDonViCapPhep,
            this.xrTableCell29,
            this.lblTenDonViCapPhep});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.84375;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.25114158630371092;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "Kính gửi:";
            this.xrTableCell26.Weight = 0.14869241205620012;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 0.03201424141210274;
            // 
            // lblMaDonViCapPhep
            // 
            this.lblMaDonViCapPhep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDonViCapPhep.Name = "lblMaDonViCapPhep";
            this.lblMaDonViCapPhep.StylePriority.UseFont = false;
            this.lblMaDonViCapPhep.Text = "XXXXXE";
            this.lblMaDonViCapPhep.Weight = 0.17908120660865962;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.027319738112660419;
            // 
            // lblTenDonViCapPhep
            // 
            this.lblTenDonViCapPhep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDonViCapPhep.Name = "lblTenDonViCapPhep";
            this.lblTenDonViCapPhep.StylePriority.UseFont = false;
            this.lblTenDonViCapPhep.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenDonViCapPhep.Weight = 1.5500749699652376;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell31});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.4375;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.030051806594849184;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "Đề nghị quý Cơ quan kiểm dịch lô hàng:....................................   (xuấ" +
                "t khẩu/nhập khẩu) ";
            this.xrTableCell31.Weight = 2.158272347863722;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell41,
            this.lblSoHopDong});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.4375;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 0.057371623192555996;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "Số hợp đồng hoặc số chứng từ thanh toán (L/C, TTr...):";
            this.xrTableCell39.Weight = 0.8328176242538512;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.027319988427434411;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.StylePriority.UseFont = false;
            this.lblSoHopDong.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoHopDong.Weight = 1.2708149185847297;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.lblToChucCaNhanXK});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.4375;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.057371560662267368;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "Tổ chức/cá nhân xuất khẩu:";
            this.xrTableCell46.Weight = 0.44775787855636739;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 0.029964436020493673;
            // 
            // lblToChucCaNhanXK
            // 
            this.lblToChucCaNhanXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToChucCaNhanXK.Name = "lblToChucCaNhanXK";
            this.lblToChucCaNhanXK.StylePriority.UseFont = false;
            this.lblToChucCaNhanXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblToChucCaNhanXK.Weight = 1.6532302792194429;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.lblMaBuuChinhXK,
            this.xrTableCell55,
            this.xrTableCell34,
            this.xrTableCell33,
            this.lblSoNhaTenDuongXK,
            this.xrTableCell35,
            this.lblPhuongXaXK});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.4375;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.081959731179419085;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "Mã bưu chính:";
            this.xrTableCell52.Weight = 0.21749243555895373;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.013659949857146402;
            // 
            // lblMaBuuChinhXK
            // 
            this.lblMaBuuChinhXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhXK.Name = "lblMaBuuChinhXK";
            this.lblMaBuuChinhXK.StylePriority.UseFont = false;
            this.lblMaBuuChinhXK.Text = "XXXXXXXXE";
            this.lblMaBuuChinhXK.Weight = 0.22198168480280933;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.013659949663526926;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "Địa chỉ:";
            this.xrTableCell34.Weight = 0.10927960350083028;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.01365994838998582;
            // 
            // lblSoNhaTenDuongXK
            // 
            this.lblSoNhaTenDuongXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhaTenDuongXK.Name = "lblSoNhaTenDuongXK";
            this.lblSoNhaTenDuongXK.StylePriority.UseFont = false;
            this.lblSoNhaTenDuongXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoNhaTenDuongXK.Weight = 0.79219788552490722;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 0.013659951793277683;
            // 
            // lblPhuongXaXK
            // 
            this.lblPhuongXaXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongXaXK.Name = "lblPhuongXaXK";
            this.lblPhuongXaXK.StylePriority.UseFont = false;
            this.lblPhuongXaXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XX";
            this.lblPhuongXaXK.Weight = 0.7107730141877151;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell44,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.lblQuanHuyenXK,
            this.xrTableCell61,
            this.lblTinhThanhXK});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.4375;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.081959731179419085;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.21749243555895373;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.013659949857146402;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 0.22198168480280933;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 0.013659949663526926;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 0.10927960350083028;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.01365994838998582;
            // 
            // lblQuanHuyenXK
            // 
            this.lblQuanHuyenXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanHuyenXK.Name = "lblQuanHuyenXK";
            this.lblQuanHuyenXK.StylePriority.UseFont = false;
            this.lblQuanHuyenXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblQuanHuyenXK.Weight = 0.79219788552490722;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.013659951793277683;
            // 
            // lblTinhThanhXK
            // 
            this.lblTinhThanhXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinhThanhXK.Name = "lblTinhThanhXK";
            this.lblTinhThanhXK.StylePriority.UseFont = false;
            this.lblTinhThanhXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XX";
            this.lblTinhThanhXK.Weight = 0.7107730141877151;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell65,
            this.xrTableCell66,
            this.lblMaQuocGiaXK,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.lblSoDienthoaiXK,
            this.xrTableCell75,
            this.xrTableCell71,
            this.xrTableCell72,
            this.lblSoFaxXK});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.4375;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.081959731179419085;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "Mã quốc gia:";
            this.xrTableCell65.Weight = 0.21749243555895373;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 0.013659949857146402;
            // 
            // lblMaQuocGiaXK
            // 
            this.lblMaQuocGiaXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaXK.Name = "lblMaQuocGiaXK";
            this.lblMaQuocGiaXK.StylePriority.UseFont = false;
            this.lblMaQuocGiaXK.Text = "XE";
            this.lblMaQuocGiaXK.Weight = 0.11873613270941294;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.028685019223769787;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "Điện thoại:";
            this.xrTableCell69.Weight = 0.19750008603398378;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.013659909308555429;
            // 
            // lblSoDienthoaiXK
            // 
            this.lblSoDienthoaiXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDienthoaiXK.Name = "lblSoDienthoaiXK";
            this.lblSoDienthoaiXK.StylePriority.UseFont = false;
            this.lblSoDienthoaiXK.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienthoaiXK.Weight = 0.55272280615043168;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.041425647074679134;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Text = "Fax:";
            this.xrTableCell71.Weight = 0.083452003942343;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.013659951793277697;
            // 
            // lblSoFaxXK
            // 
            this.lblSoFaxXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoFaxXK.Name = "lblSoFaxXK";
            this.lblSoFaxXK.StylePriority.UseFont = false;
            this.lblSoFaxXK.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoFaxXK.Weight = 0.82537048162659887;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.lblEmailXK,
            this.xrTableCell82,
            this.xrTableCell79,
            this.xrTableCell83,
            this.lblNuocXuatKhau});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.4375;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.081959731179419085;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Text = "Email:";
            this.xrTableCell77.Weight = 0.21749243555895373;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.013659949857146402;
            // 
            // lblEmailXK
            // 
            this.lblEmailXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailXK.Name = "lblEmailXK";
            this.lblEmailXK.StylePriority.UseFont = false;
            this.lblEmailXK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5";
            this.lblEmailXK.Weight = 1.488521137664738;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 0.013659949985712456;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "Nước xuất khẩu:";
            this.xrTableCell79.Weight = 0.24515874661177439;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.027320068728532147;
            // 
            // lblNuocXuatKhau
            // 
            this.lblNuocXuatKhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNuocXuatKhau.Name = "lblNuocXuatKhau";
            this.lblNuocXuatKhau.StylePriority.UseFont = false;
            this.lblNuocXuatKhau.Text = "XE";
            this.lblNuocXuatKhau.Weight = 0.10055213487229534;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.lblMaToChucCaNhanXK,
            this.xrTableCell88,
            this.lblTenToChucCaNhanXK});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.84375;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.057371560662267368;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Text = "Tổ chức/cá nhân nhập khẩu:";
            this.xrTableCell85.Weight = 0.40316179779055805;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.013659949857146404;
            // 
            // lblMaToChucCaNhanXK
            // 
            this.lblMaToChucCaNhanXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaToChucCaNhanXK.Name = "lblMaToChucCaNhanXK";
            this.lblMaToChucCaNhanXK.StylePriority.UseFont = false;
            this.lblMaToChucCaNhanXK.Text = "XXXXXXXXX1-XXE";
            this.lblMaToChucCaNhanXK.Weight = 0.3220836347557382;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.027319899971424907;
            // 
            // lblTenToChucCaNhanXK
            // 
            this.lblTenToChucCaNhanXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenToChucCaNhanXK.Name = "lblTenToChucCaNhanXK";
            this.lblTenToChucCaNhanXK.StylePriority.UseFont = false;
            this.lblTenToChucCaNhanXK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9";
            this.lblTenToChucCaNhanXK.Weight = 1.3647273114214367;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.lblSoDangKyKD});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.4375;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.057371560662267368;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "Số đăng ký kinh doanh:";
            this.xrTableCell91.Weight = 0.40316179779055805;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.013659949857146404;
            // 
            // lblSoDangKyKD
            // 
            this.lblSoDangKyKD.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDangKyKD.Name = "lblSoDangKyKD";
            this.lblSoDangKyKD.StylePriority.UseFont = false;
            this.lblSoDangKyKD.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoDangKyKD.Weight = 1.7141308461485996;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.lblMaBuuChinhNK,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell98,
            this.lblDiaChiNK});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.84375;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 0.084691439790262724;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Text = "Mã bưu chính:";
            this.xrTableCell95.Weight = 0.21476070610468054;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.013659949857146414;
            // 
            // lblMaBuuChinhNK
            // 
            this.lblMaBuuChinhNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhNK.Name = "lblMaBuuChinhNK";
            this.lblMaBuuChinhNK.StylePriority.UseFont = false;
            this.lblMaBuuChinhNK.Text = "XXXXXXE";
            this.lblMaBuuChinhNK.Weight = 0.22198174141044366;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.013659875433115809;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "Địa chỉ:";
            this.xrTableCell100.Weight = 0.10927960870682119;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.013660000493693092;
            // 
            // lblDiaChiNK
            // 
            this.lblDiaChiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNK.Name = "lblDiaChiNK";
            this.lblDiaChiNK.StylePriority.UseFont = false;
            this.lblDiaChiNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNK.Weight = 1.5166308326624081;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell104,
            this.lblMaQuocGiaNK,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.lblSoDienThoaiNK,
            this.xrTableCell112,
            this.xrTableCell110,
            this.xrTableCell113,
            this.lblSoFaxNK});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.4375;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.084691439790262724;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "Mã quốc gia:";
            this.xrTableCell103.Weight = 0.21476070610468054;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 0.013659949857146414;
            // 
            // lblMaQuocGiaNK
            // 
            this.lblMaQuocGiaNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaNK.Name = "lblMaQuocGiaNK";
            this.lblMaQuocGiaNK.StylePriority.UseFont = false;
            this.lblMaQuocGiaNK.Text = "XE";
            this.lblMaQuocGiaNK.Weight = 0.11873618931704727;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.028685017945362074;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "Điện thoại:";
            this.xrTableCell107.Weight = 0.19750001828797131;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.013660000493693092;
            // 
            // lblSoDienThoaiNK
            // 
            this.lblSoDienThoaiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDienThoaiNK.Name = "lblSoDienThoaiNK";
            this.lblSoDienThoaiNK.StylePriority.UseFont = false;
            this.lblSoDienThoaiNK.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiNK.Weight = 0.55272282188405653;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 0.041425737012495412;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Fax:";
            this.xrTableCell110.Weight = 0.0834520105064411;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.013659792059397657;
            // 
            // lblSoFaxNK
            // 
            this.lblSoFaxNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoFaxNK.Name = "lblSoFaxNK";
            this.lblSoFaxNK.StylePriority.UseFont = false;
            this.lblSoFaxNK.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoFaxNK.Weight = 0.82537047120001739;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.lblEmailNK});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.4375;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.084691439790262724;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Text = "Email:";
            this.xrTableCell115.Weight = 0.21476070610468054;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.013659949857146414;
            // 
            // lblEmailNK
            // 
            this.lblEmailNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailNK.Name = "lblEmailNK";
            this.lblEmailNK.StylePriority.UseFont = false;
            this.lblEmailNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWW";
            this.lblEmailNK.Weight = 1.8752120587064818;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.lblNuocQuaCanh,
            this.xrTableCell122,
            this.xrTableCell125,
            this.xrTableCell121,
            this.lblMaPhuongTienVC,
            this.xrTableCell126,
            this.lblTenPhuongTienVC});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.4375;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.057371560662267361;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "Nước quá cảnh:";
            this.xrTableCell118.Weight = 0.24208058523267589;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 0.013659949857146414;
            // 
            // lblNuocQuaCanh
            // 
            this.lblNuocQuaCanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNuocQuaCanh.Name = "lblNuocQuaCanh";
            this.lblNuocQuaCanh.StylePriority.UseFont = false;
            this.lblNuocQuaCanh.Text = "XE";
            this.lblNuocQuaCanh.Weight = 0.11873623100390639;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Weight = 0.028684944993358663;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "Phương tiện vận chuyển:";
            this.xrTableCell125.Weight = 0.36306337066977096;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.027319908792546477;
            // 
            // lblMaPhuongTienVC
            // 
            this.lblMaPhuongTienVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhuongTienVC.Name = "lblMaPhuongTienVC";
            this.lblMaPhuongTienVC.StylePriority.UseFont = false;
            this.lblMaPhuongTienVC.Text = "XE";
            this.lblMaPhuongTienVC.Weight = 0.066592765243778773;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 0.027319908792546421;
            // 
            // lblTenPhuongTienVC
            // 
            this.lblTenPhuongTienVC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenPhuongTienVC.Name = "lblTenPhuongTienVC";
            this.lblTenPhuongTienVC.StylePriority.UseFont = false;
            this.lblTenPhuongTienVC.Text = "WWWWWWWWW1WWWWWWWWW2WWWWE";
            this.lblTenPhuongTienVC.Weight = 1.2434949292105739;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.lblMaCuaKhauNhap,
            this.xrTableCell136,
            this.lblCuaKhauNhap});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.4375;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.057371560662267361;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Cửa khẩu nhập:";
            this.xrTableCell129.Weight = 0.24208058523267589;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.013659949857146414;
            // 
            // lblMaCuaKhauNhap
            // 
            this.lblMaCuaKhauNhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaCuaKhauNhap.Name = "lblMaCuaKhauNhap";
            this.lblMaCuaKhauNhap.StylePriority.UseFont = false;
            this.lblMaCuaKhauNhap.Text = "XXXXXE";
            this.lblMaCuaKhauNhap.Weight = 0.16108123582006029;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.027319908792546421;
            // 
            // lblCuaKhauNhap
            // 
            this.lblCuaKhauNhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuaKhauNhap.Name = "lblCuaKhauNhap";
            this.lblCuaKhauNhap.StylePriority.UseFont = false;
            this.lblCuaKhauNhap.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblCuaKhauNhap.Weight = 1.6868109140938747;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell132,
            this.xrTableCell133,
            this.lblSoGiayChungNhan});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.4375;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 0.057371560662267361;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "Giấy cho phép kiểm dịch nhập khẩu (nếu có) :";
            this.xrTableCell132.Weight = 0.66554653842439759;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Weight = 0.027319899842858861;
            // 
            // lblSoGiayChungNhan
            // 
            this.lblSoGiayChungNhan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayChungNhan.Name = "lblSoGiayChungNhan";
            this.lblSoGiayChungNhan.StylePriority.UseFont = false;
            this.lblSoGiayChungNhan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoGiayChungNhan.Weight = 1.4380861555290472;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTableCell138,
            this.xrTableCell140,
            this.lblDiaDiemKiemDich});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.84375;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.057371560662267361;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "Địa điểm kiểm dịch:";
            this.xrTableCell138.Weight = 0.30697276792572759;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.035489690486779579;
            // 
            // lblDiaDiemKiemDich
            // 
            this.lblDiaDiemKiemDich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemKiemDich.Name = "lblDiaDiemKiemDich";
            this.lblDiaDiemKiemDich.StylePriority.UseFont = false;
            this.lblDiaDiemKiemDich.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblDiaDiemKiemDich.Weight = 1.7884901353837965;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.lblBenhMienDich,
            this.xrTableCell146,
            this.xrTableCell149,
            this.xrTableCell148,
            this.lblNgayTiemPhong});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.84375;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.057371560662267361;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "Bệnh miễn dịch:";
            this.xrTableCell143.Weight = 0.24208060607610546;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.013659949857146456;
            // 
            // lblBenhMienDich
            // 
            this.lblBenhMienDich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBenhMienDich.Name = "lblBenhMienDich";
            this.lblBenhMienDich.StylePriority.UseFont = false;
            this.lblBenhMienDich.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblBenhMienDich.Weight = 1.1507791807037475;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 0.044513227537615455;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Text = "Ngày tiêm phòng:";
            this.xrTableCell149.Weight = 0.26590913629677759;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.027319731623395287;
            // 
            // lblNgayTiemPhong
            // 
            this.lblNgayTiemPhong.Name = "lblNgayTiemPhong";
            this.lblNgayTiemPhong.Text = "dd/MM/yyyy";
            this.lblNgayTiemPhong.Weight = 0.38669076170151584;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.lblKhuTrung,
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell156,
            this.lblNongDo});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1.25;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.057371560662267361;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Text = "Khử trùng:";
            this.xrTableCell151.Weight = 0.24208060607610546;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.013659949857146456;
            // 
            // lblKhuTrung
            // 
            this.lblKhuTrung.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKhuTrung.Name = "lblKhuTrung";
            this.lblKhuTrung.StylePriority.UseFont = false;
            this.lblKhuTrung.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblKhuTrung.Weight = 0.79024929602926708;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 0.0671614479585678;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Nồng độ:";
            this.xrTableCell155.Weight = 0.14803145497689488;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Weight = 0.030739304674322968;
            // 
            // lblNongDo
            // 
            this.lblNongDo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNongDo.Name = "lblNongDo";
            this.lblNongDo.StylePriority.UseFont = false;
            this.lblNongDo.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXXE";
            this.lblNongDo.Weight = 0.839030534223999;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.lblDiaDiemNuoiTrong});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.4375;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Weight = 0.057371560662267361;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "Địa điểm nuôi trồng (nếu có):";
            this.xrTableCell159.Weight = 0.44414168943455445;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.033580599088019525;
            // 
            // lblDiaDiemNuoiTrong
            // 
            this.lblDiaDiemNuoiTrong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemNuoiTrong.Name = "lblDiaDiemNuoiTrong";
            this.lblDiaDiemNuoiTrong.StylePriority.UseFont = false;
            this.lblDiaDiemNuoiTrong.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblDiaDiemNuoiTrong.Weight = 1.6532303052737296;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.lblNgayKiemDich,
            this.xrTableCell166,
            this.lblThoiGianKiemDich});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.4375;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.057371560662267361;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "Thời gian kiểm dịch:";
            this.xrTableCell163.Weight = 0.30697276792572759;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Weight = 0.035489732173638655;
            // 
            // lblNgayKiemDich
            // 
            this.lblNgayKiemDich.Name = "lblNgayKiemDich";
            this.lblNgayKiemDich.Text = "dd/MM/yyyy";
            this.lblNgayKiemDich.Weight = 0.23841534059750558;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Weight = 0.027319901981095684;
            // 
            // lblThoiGianKiemDich
            // 
            this.lblThoiGianKiemDich.Name = "lblThoiGianKiemDich";
            this.lblThoiGianKiemDich.Text = "hh:mm";
            this.lblThoiGianKiemDich.Weight = 1.5227548511183362;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.lblDiaDiemGiamSat});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.84375;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.057371560662267361;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "Địa điểm giám sát (nếu có) :";
            this.xrTableCell169.Weight = 0.40316169357341031;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.013659991544005531;
            // 
            // lblDiaDiemGiamSat
            // 
            this.lblDiaDiemGiamSat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemGiamSat.Name = "lblDiaDiemGiamSat";
            this.lblDiaDiemGiamSat.StylePriority.UseFont = false;
            this.lblDiaDiemGiamSat.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblDiaDiemGiamSat.Weight = 1.7141309086788878;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell173,
            this.xrTableCell174,
            this.lblNgayGiamSat,
            this.xrTableCell176,
            this.lblThoiGianGiamSat});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.4375;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 0.057371560662267361;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "Thời gian giám sát:";
            this.xrTableCell173.Weight = 0.30697274708229805;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.035489732173638655;
            // 
            // lblNgayGiamSat
            // 
            this.lblNgayGiamSat.Name = "lblNgayGiamSat";
            this.lblNgayGiamSat.Text = "dd/MM/yyyy";
            this.lblNgayGiamSat.Weight = 0.23841782617647872;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.027317398164121889;
            // 
            // lblThoiGianGiamSat
            // 
            this.lblThoiGianGiamSat.Name = "lblThoiGianGiamSat";
            this.lblThoiGianGiamSat.Text = "hh:mm";
            this.lblThoiGianGiamSat.Weight = 1.5227548901997665;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell180,
            this.lblSoBanCanCap,
            this.xrTableCell182,
            this.xrTableCell183});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.4375;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Weight = 0.057371560662267361;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Text = "Số bản giấy chứng nhận cần cấp:";
            this.xrTableCell179.Weight = 0.47772225738599572;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Weight = 0.027319899842858889;
            // 
            // lblSoBanCanCap
            // 
            this.lblSoBanCanCap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoBanCanCap.Name = "lblSoBanCanCap";
            this.lblSoBanCanCap.StylePriority.UseFont = false;
            this.lblSoBanCanCap.Text = "NE";
            this.lblSoBanCanCap.Weight = 0.0758381482035608;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Weight = 0.027317398164121889;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 1.5227548901997665;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.lblVatDungKhac});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.84375;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Weight = 0.057371560662267361;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "Vật dụng khác:";
            this.xrTableCell185.Weight = 0.24208059565439069;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Weight = 0.013659991544005545;
            // 
            // lblVatDungKhac
            // 
            this.lblVatDungKhac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVatDungKhac.Name = "lblVatDungKhac";
            this.lblVatDungKhac.StylePriority.UseFont = false;
            this.lblVatDungKhac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblVatDungKhac.Weight = 1.8752120065979077;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell189,
            this.xrTableCell190,
            this.lblHoSoLienQuan});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1.5;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.057371560662267361;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "Hồ sơ liên quan:";
            this.xrTableCell189.Weight = 0.24208059565439069;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Weight = 0.013659991544005545;
            // 
            // lblHoSoLienQuan
            // 
            this.lblHoSoLienQuan.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblHoSoLienQuan.Multiline = true;
            this.lblHoSoLienQuan.Name = "lblHoSoLienQuan";
            this.lblHoSoLienQuan.StylePriority.UseFont = false;
            this.lblHoSoLienQuan.Text = resources.GetString("lblHoSoLienQuan.Text");
            this.lblHoSoLienQuan.Weight = 1.8752120065979077;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell192,
            this.xrTableCell194});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.90625;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.030051665901699842;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Multiline = true;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = resources.GetString("xrTableCell194.Text");
            this.xrTableCell194.Weight = 2.1582724885568716;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell193,
            this.xrTableCell196,
            this.xrTableCell195});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.4375;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 0.030051665901699842;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Weight = 0.85269965363914535;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseFont = false;
            this.xrTableCell195.StylePriority.UseTextAlignment = false;
            this.xrTableCell195.Text = "TỔ CHỨC/CÁ NHÂN ĐĂNG KÝ";
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell195.Weight = 1.3055728349177262;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.4375;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 0.030051665901699842;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 0.85269965363914535;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseTextAlignment = false;
            this.xrTableCell199.Text = "(Ký, đóng dấu, ghi rõ họ tên)";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell199.Weight = 1.3055728349177262;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.625;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.030051665901699842;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseFont = false;
            this.xrTableCell201.Weight = 0.8526994868917217;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 1.30557300166515;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.78125;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.030051665901699842;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseFont = false;
            this.xrTableCell204.Text = "XÁC NHẬN CỦA CƠ QUAN KIỂM DỊCH ĐỘNG VẬT:";
            this.xrTableCell204.Weight = 0.85269965363914524;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseFont = false;
            this.xrTableCell205.StylePriority.UseTextAlignment = false;
            this.xrTableCell205.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell205.Weight = 1.3055728349177262;
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 802.8332F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(418.0341F, 15F);
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.lblNguoiKiemTra});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.6;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.0953586578369141;
            // 
            // lblNguoiKiemTra
            // 
            this.lblNguoiKiemTra.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiKiemTra.Name = "lblNguoiKiemTra";
            this.lblNguoiKiemTra.StylePriority.UseFont = false;
            this.lblNguoiKiemTra.StylePriority.UseTextAlignment = false;
            this.lblNguoiKiemTra.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblNguoiKiemTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNguoiKiemTra.Weight = 4.0849819183349618;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(335.8392F, 616.9999F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow50,
            this.xrTableRow49,
            this.xrTableRow51,
            this.xrTableRow55,
            this.xrTableRow52});
            this.xrTable9.SizeF = new System.Drawing.SizeF(465.1609F, 158F);
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell219});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 0.55999999999999983;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 0.26909015402063707;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 2.8811958225800685;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228,
            this.xrTableCell229});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.55999999999999994;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Weight = 0.067724444946722112;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseFont = false;
            this.xrTableCell229.StylePriority.UseTextAlignment = false;
            this.xrTableCell229.Text = "XÁC NHẬN CỦA CƠ QUAN HẢI QUAN:";
            this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell229.Weight = 3.0825615316539836;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.55999999999999983;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Weight = 0.067724651625947374;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseTextAlignment = false;
            this.xrTableCell231.Text = "(Trong trường hợp lô hàng không được nhập khẩu)";
            this.xrTableCell231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell231.Weight = 3.0825613249747583;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.55999999999999983;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Weight = 0.067724444946722112;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "Kết quả kiểm tra lô hàng:";
            this.xrTableCell223.Weight = 3.0825615316539836;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell220,
            this.lblKetQuaKiemTra});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 2.4;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 0.067724651625947457;
            // 
            // lblKetQuaKiemTra
            // 
            this.lblKetQuaKiemTra.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblKetQuaKiemTra.Multiline = true;
            this.lblKetQuaKiemTra.Name = "lblKetQuaKiemTra";
            this.lblKetQuaKiemTra.StylePriority.UseFont = false;
            this.lblKetQuaKiemTra.Text = resources.GetString("lblKetQuaKiemTra.Text");
            this.lblKetQuaKiemTra.Weight = 3.0825613249747588;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell224,
            this.xrTableCell225});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.56000000000000016;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Weight = 0.06772443291344854;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseFont = false;
            this.xrTableCell225.StylePriority.UseTextAlignment = false;
            this.xrTableCell225.Text = "Ngày";
            this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell225.Weight = 3.082561543687258;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell232,
            this.xrTableCell233});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.55999999999999972;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 0.06772443291344854;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseFont = false;
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "CHI CỤC HẢI QUAN CỬA KHẨU";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell233.Weight = 3.082561543687258;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell226,
            this.xrTableCell227});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.55999999999999983;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 0.067724226234223278;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseTextAlignment = false;
            this.xrTableCell227.Text = "(Ký, đóng dấu, ghi rõ họ tên)";
            this.xrTableCell227.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell227.Weight = 3.0825617503664828;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(7.152557E-05F, 616.9999F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46});
            this.xrTable7.SizeF = new System.Drawing.SizeF(335.8392F, 165F);
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209,
            this.xrTableCell206});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.55999999999999983;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 0.0902291863151663;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "Đồng ý đưa hàng hóa về địa điểm:";
            this.xrTableCell206.Weight = 2.6645619842448203;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.lblNoiChuyenDen});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 2.88;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Weight = 0.0902291863151663;
            // 
            // lblNoiChuyenDen
            // 
            this.lblNoiChuyenDen.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblNoiChuyenDen.Multiline = true;
            this.lblNoiChuyenDen.Name = "lblNoiChuyenDen";
            this.lblNoiChuyenDen.StylePriority.UseFont = false;
            this.lblNoiChuyenDen.Text = resources.GetString("lblNoiChuyenDen.Text");
            this.lblNoiChuyenDen.Weight = 2.6645619842448207;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell211,
            this.xrTableCell207});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1.1599999999999997;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 0.0902291863151663;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Multiline = true;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "để làm thủ tục kiểm dịch vào hồi:                 giờ, ngày:\r\n                   " +
                "                 Vào sổ số:                 ngày:";
            this.xrTableCell207.Weight = 2.6645619842448207;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell213,
            this.xrTableCell208});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1.4400000000000002;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 0.090229561805440062;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseFont = false;
            this.xrTableCell208.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.xrTableCell208.Weight = 2.6645616087545472;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.55999999999999994;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Weight = 0.090229561805440062;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseTextAlignment = false;
            this.xrTableCell215.Text = "(Ký, đóng dấu, ghi rõ họ tên)";
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell215.Weight = 2.6645616087545472;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 14.58397F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(9.54F, 100F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(787F, 18F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "GIẤY ĐĂNG KÝ KIỂM DỊCH ĐỘNG VẬT HOẶC CÁC SẢN PHẨM TỪ ĐỘNG VẬT CHO NHẬP KHẨU";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(764.1946F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(13.4303F, 15F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(777.6249F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(10F, 15F);
            this.xrLabel2.Text = "/";
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(788.0416F, 0F);
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrang.SizeF = new System.Drawing.SizeF(15.51361F, 15F);
            this.lblTongSoTrang.Text = "X";
            // 
            // lblTenNguoiKhai
            // 
            this.lblTenNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenNguoiKhai.Name = "lblTenNguoiKhai";
            this.lblTenNguoiKhai.StylePriority.UseFont = false;
            this.lblTenNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiKhai.Weight = 3.2281233527967035;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenNguoiKhai});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.0000002856162493;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.StylePriority.UseFont = false;
            this.lblMaNguoiKhai.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiKhai.Weight = 2.518878123271564;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.08202711161157708;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "CÔNG TY:";
            this.xrTableCell1.Weight = 0.6272181179135623;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.lblMaNguoiKhai});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.60000019445239083;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(498.8869F, 40F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "Độc lập - Tự do - Hạnh phúc";
            this.xrTableCell18.Weight = 3;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.60000000000000009;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            this.xrTableCell17.Weight = 3;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
            this.xrTableRow7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseFont = false;
            this.xrTableRow7.Weight = 0.6;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(498.8868F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow8});
            this.xrTable2.SizeF = new System.Drawing.SizeF(265.1965F, 30F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(552.1268F, 30F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(152.08F, 2F);
            // 
            // lblNgayKhaiBao
            // 
            this.lblNgayKhaiBao.Name = "lblNgayKhaiBao";
            this.lblNgayKhaiBao.Text = "dd/MM/yyyy";
            this.lblNgayKhaiBao.Weight = 1.0716326904296873;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.11458312988281261;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Ngày khai báo:";
            this.xrTableCell19.Weight = 0.8229168701171875;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.lblNgayKhaiBao});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(550.972F, 32.00002F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable3.SizeF = new System.Drawing.SizeF(202.068F, 18F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "BK-KD";
            this.xrTableCell21.Weight = 1.5575095637325642;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "/";
            this.xrTableCell4.Weight = 0.082027104048566488;
            // 
            // lblSoDonXinCapPhep
            // 
            this.lblSoDonXinCapPhep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDonXinCapPhep.Name = "lblSoDonXinCapPhep";
            this.lblSoDonXinCapPhep.StylePriority.UseFont = false;
            this.lblSoDonXinCapPhep.Text = "NNNNNNNNN1NE";
            this.lblSoDonXinCapPhep.Weight = 0.97306696518496494;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 0.082026983855567781;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Số:";
            this.xrTableCell13.Weight = 0.20536938317833653;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell16,
            this.lblSoDonXinCapPhep,
            this.xrTableCell4,
            this.xrTableCell21});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.59999999999999987;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(195.8622F, 118F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42,
            this.xrTableRow6});
            this.xrTable5.SizeF = new System.Drawing.SizeF(353.5417F, 30F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell15,
            this.lblChucNangChungTu,
            this.xrTableCell24,
            this.xrTableCell40,
            this.xrTableCell36,
            this.lblLoaiGiayPhep});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.59999999999999987;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Chức năng của chứng từ:";
            this.xrTableCell7.Weight = 1.043829964986273;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.082027109018992322;
            // 
            // lblChucNangChungTu
            // 
            this.lblChucNangChungTu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChucNangChungTu.Name = "lblChucNangChungTu";
            this.lblChucNangChungTu.StylePriority.UseFont = false;
            this.lblChucNangChungTu.Text = "N";
            this.lblChucNangChungTu.Weight = 0.29866021176364438;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.25935513942993593;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "Loại giấy phép:";
            this.xrTableCell40.Weight = 0.64610958788184658;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.092294258184985642;
            // 
            // lblLoaiGiayPhep
            // 
            this.lblLoaiGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiGiayPhep.Name = "lblLoaiGiayPhep";
            this.lblLoaiGiayPhep.StylePriority.UseFont = false;
            this.lblLoaiGiayPhep.Text = "XXXE";
            this.lblLoaiGiayPhep.Weight = 0.47772372873432206;
            // 
            // lblEmailNguoiKhai
            // 
            this.lblEmailNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblEmailNguoiKhai.Name = "lblEmailNguoiKhai";
            this.lblEmailNguoiKhai.StylePriority.UseFont = false;
            this.lblEmailNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblEmailNguoiKhai.Weight = 2.0091326904296873;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblEmailNguoiKhai});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.83333333333333337;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 79.99998F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable6.SizeF = new System.Drawing.SizeF(664.4583F, 14.99999F);
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrTable3,
            this.xrLine1,
            this.xrTable2,
            this.xrTable1,
            this.lblTongSoTrang,
            this.xrLabel2,
            this.xrLabel1,
            this.lblTenThongTinXuat,
            this.xrTable6,
            this.xrTable10});
            this.PageHeader.HeightF = 148F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 39.99999F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow4});
            this.xrTable10.SizeF = new System.Drawing.SizeF(549.4039F, 40F);
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMaBuuChinhNguoiKhai,
            this.xrTableCell6,
            this.lblDiaChiNguoiKhai});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1.0000003144695508;
            // 
            // lblMaBuuChinhNguoiKhai
            // 
            this.lblMaBuuChinhNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhNguoiKhai.Name = "lblMaBuuChinhNguoiKhai";
            this.lblMaBuuChinhNguoiKhai.StylePriority.UseFont = false;
            this.lblMaBuuChinhNguoiKhai.Text = "XXXXXXE";
            this.lblMaBuuChinhNguoiKhai.Weight = 0.39130891242314458;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.064706521053723376;
            // 
            // lblDiaChiNguoiKhai
            // 
            this.lblDiaChiNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblDiaChiNguoiKhai.Name = "lblDiaChiNguoiKhai";
            this.lblDiaChiNguoiKhai.StylePriority.UseFont = false;
            this.lblDiaChiNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiKhai.Weight = 3.0989862324093433;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblMaNuocNguoiKhai,
            this.lblSoDienThoaiNguoiKhai,
            this.lblSoFaxNguoiKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.60000016559908931;
            // 
            // lblMaNuocNguoiKhai
            // 
            this.lblMaNuocNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNuocNguoiKhai.Name = "lblMaNuocNguoiKhai";
            this.lblMaNuocNguoiKhai.StylePriority.UseFont = false;
            this.lblMaNuocNguoiKhai.Text = "XE";
            this.lblMaNuocNguoiKhai.Weight = 0.20058971963335925;
            // 
            // lblSoDienThoaiNguoiKhai
            // 
            this.lblSoDienThoaiNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDienThoaiNguoiKhai.Name = "lblSoDienThoaiNguoiKhai";
            this.lblSoDienThoaiNguoiKhai.StylePriority.UseFont = false;
            this.lblSoDienThoaiNguoiKhai.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiNguoiKhai.Weight = 1.2463819014690099;
            // 
            // lblSoFaxNguoiKhai
            // 
            this.lblSoFaxNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoFaxNguoiKhai.Name = "lblSoFaxNguoiKhai";
            this.lblSoFaxNguoiKhai.StylePriority.UseFont = false;
            this.lblSoFaxNguoiKhai.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoFaxNguoiKhai.Weight = 2.1080300447838423;
            // 
            // GiayDangKyKiemDichDongVatNK
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(26, 19, 9, 15);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDonViCapPhep;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDonXinCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblChucNangChungTu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiGiayPhep;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHopDong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell lblToChucCaNhanXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongXaXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhaTenDuongXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell lblQuanHuyenXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblTinhThanhXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDienthoaiXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblSoFaxXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblNuocXuatKhau;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell lblMaToChucCaNhanXK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblTenToChucCaNhanXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDangKyKD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNK;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblSoFaxNK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDienThoaiNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblEmailNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell lblNuocQuaCanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhuongTienVC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhuongTienVC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCuaKhauNhap;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell lblCuaKhauNhap;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemKiemDich;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell lblBenhMienDich;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayTiemPhong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell lblKhuTrung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell lblNongDo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemNuoiTrong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKiemDich;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiGianKiemDich;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemGiamSat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayGiamSat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiGianGiamSat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell lblSoBanCanCap;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell lblVatDungKhac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell lblHoSoLienQuan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell lblNoiChuyenDen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell lblKetQuaKiemTra;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiKiemTra;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDienThoaiNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoFaxNguoiKhai;
    }
}
