﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_MaterialSupply_Product
    {
        /// <summary>
        //Mã hải quan đăng ký danh mục
        /// </summary>
        [XmlElement("registerCustoms")]
        public string RegisterCustoms { get; set; }

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Tên Sản phẩm
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }
        /// <summary>
        /// ĐVT
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }


        /// <summary>
        /// ĐVT
        /// </summary>
        [XmlElement("Material")]
        public List<SXXK_MaterialSupply_Material> Material { get; set; }
    }
}
