﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC07FormRieKer : BaseForm
    {
        public DataSet ds = new DataSet();
        public HopDong HD = new HopDong();

        public ThanhKhoan07_RieKer BK07;
        public XRLabel Label;
        public ReportViewBC07FormRieKer()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            BK07 = new ThanhKhoan07_RieKer();
            Label = new XRLabel();
            BK07.report = this;
            ds = this.CreatSchemaDataSetRP07();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BK07.HD = this.HD;
            this.BK07.First = (cbPage.SelectedIndex == 0);
            if (cbPage.SelectedIndex + 1 == cbPage.Items.Count)
            {
                this.BK07.Last = true;
            }
            else
            {
                this.BK07.Last = false;
            }
            this.BK07.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BK07.PrintingSystem;
            this.BK07.CreateDocument();
        }

        public DataSet CreatSchemaDataSetRP07()
        {
            DataSet dsSTK = new DataSet();


            //DataTable dt1 = HD.Report7_qr1();
            //DataTable dt2 = HD.Report7_qr2();
            //DataTable dt3 = HD.Report7_qr3();
            //DataTable dt4 = HD.Report7_qr4();

            DataTable dt_TN = this.UnionDataTN(HD.Report7_qr1(), HD.Report7_qr2());
            //DataTable dt_TN = this.UnionDataTN(dt1, dt2);
            DataTable dt_TX = this.UnionDataTN(HD.Report7_qr3(), HD.Report7_qr4());

            DataTable dt_Bang07 = this.UnionDataTN(dt_TN, dt_TX);

            DataTable dtSTK_TN = this.GetSoToKhai(dt_TN);
            DataTable dtSTK_TX = this.GetSoToKhai(dt_TX);

            int soTable;
            if ((dtSTK_TN.Rows.Count - 1) / 2 > (dtSTK_TX.Rows.Count - 1) / 3)
            {
                soTable = (dtSTK_TN.Rows.Count - 1) / 2 + 1;
            }
            else
            {
                soTable = (dtSTK_TX.Rows.Count - 1) / 3 + 1;
            }

            for (int i = 0; i < soTable; i++)
            {
                DataTable dt07 = new DataTable();
                dt07.Columns.Add("TenMayMoc", Type.GetType("System.String"));
                dt07.Columns.Add("DVT", Type.GetType("System.String"));
                DataColumn col;
                string clumnCap = "";
                for (int n = i * 2; n <= (i * 2) + 1; n++)
                {
                    clumnCap = "";
                    if (n < dtSTK_TN.Rows.Count)
                    {
                        col = new DataColumn();
                        col.ColumnName = dtSTK_TN.Rows[n][0].ToString();
                        clumnCap += "Tờ khai số: \n" + dtSTK_TN.Rows[n][0].ToString() + "-" + dtSTK_TN.Rows[n][3].ToString();
                        clumnCap += "-" + DateTime.Parse(dtSTK_TN.Rows[n][1].ToString()).Year.ToString();
                        clumnCap += "\nNgày: " + DateTime.Parse(dtSTK_TN.Rows[n][1].ToString()).ToShortDateString();
                        col.Caption = clumnCap;
                        col.DataType = Type.GetType("System.Decimal");
                        dt07.Columns.Add(col);
                    }
                    else
                    {
                        col = new DataColumn();
                        col.ColumnName = "colN_" + n.ToString();
                        col.Caption = "Tờ khai số: \n\nNgày:";
                        dt07.Columns.Add(col);
                    }
                }
                for (int m = i * 3; m <= (i * 3) + 2; m++)
                {
                    clumnCap = "";
                    if (m < dtSTK_TX.Rows.Count)
                    {
                        col = new DataColumn();
                        col.ColumnName = dtSTK_TX.Rows[m][0].ToString();
                        clumnCap += "Tờ khai số: \n" + dtSTK_TX.Rows[m][0].ToString() + "-" + dtSTK_TX.Rows[m][3].ToString();
                        clumnCap += "-" + DateTime.Parse(dtSTK_TX.Rows[m][1].ToString()).Year.ToString();
                        clumnCap += "\nNgày: " + DateTime.Parse(dtSTK_TX.Rows[m][1].ToString()).ToShortDateString();
                        col.Caption = clumnCap;
                        col.DataType = Type.GetType("System.Decimal");
                        dt07.Columns.Add(col);
                    }
                    else
                    {
                        col = new DataColumn();
                        col.ColumnName = "colX_" + m.ToString();
                        col.Caption = "Tờ khai số: \n\nNgày:";
                        dt07.Columns.Add(col);
                    }
                }

                dt07.Columns.Add("ConLai", Type.GetType("System.Decimal"));
                dt07.Columns.Add("BienPhap", Type.GetType("System.String"));
                dsSTK.Tables.Add(dt07);
            }

            DataTable dtThietBi = this.GetMaTB(dt_Bang07);

            for (int t = 0; t < dtThietBi.Rows.Count; t++ )
            {
                DataRow dr;
                //for (int k = 0; k < soTable; k++)
                //{
                string MaTB = dtThietBi.Rows[t][0].ToString();
                int BangSauCung = -1;
                for (int k = 0; k < soTable; k++)
                {
                    dr = dsSTK.Tables[k].NewRow();
                    dr[0] = dtThietBi.Rows[t][1].ToString();
                    dr[1] = dtThietBi.Rows[t][2].ToString();
                    for (int k1 = 0; k1 < dt_Bang07.Rows.Count; k1++)
                    {
                        if (dt_Bang07.Rows[k1]["MaPhu"].ToString() == MaTB)
                        {
                            for (int l = 2; l <= 6; l++)
                            {
                                if (dsSTK.Tables[k].Columns[l].ColumnName == dt_Bang07.Rows[k1]["SoToKhai"].ToString())
                                {
                                    dr[l] = dt_Bang07.Rows[k1]["SoLuong"];
                                }
                            }
                        }
                    }
                    bool val1 = false;
                    for (int l = 2; l <= 6; l++)
                    {
                        if (dr[l].ToString() != "")
                        {
                            val1 = true;
                        }
                    }

                    if (val1)
                    {
                        dsSTK.Tables[k].Rows.Add(dr);
                        BangSauCung = k;
                    }
                }

                if (BangSauCung > -1)
                {
                    decimal tong = 0;
                    for (int b = 0; b <= BangSauCung; b++)
                    {
                        for (int b1 = 0; b1 < dsSTK.Tables[b].Rows.Count; b1++)
                        {
                            if (dsSTK.Tables[b].Rows[b1][0] == dtThietBi.Rows[t][1])
                            {
                                for (int b2 = 2; b2 <= 3; b2++)
                                {
                                    if (dsSTK.Tables[b].Rows[b1][b2].ToString() != "")
                                    {
                                        tong += Decimal.Parse(dsSTK.Tables[b].Rows[b1][b2].ToString());
                                    }
                                }
                                for (int b3 = 4; b3 <= 6; b3++)
                                {
                                    if (dsSTK.Tables[b].Rows[b1][b3].ToString() != "")
                                    {
                                        tong -= Decimal.Parse(dsSTK.Tables[b].Rows[b1][b3].ToString());
                                    }
                                }
                                if (b == BangSauCung)
                                {
                                    dsSTK.Tables[b].Rows[b1]["ConLai"] = tong;
                                }
                            }
                        }
                    }
                }
            }

            return dsSTK;
        }

        private DataTable GetSoToKhai(DataTable dt)
        {
            DataTable data = new DataTable();
            data.Columns.Add("SoToKhai", Type.GetType("System.Int64"));
            data.Columns.Add("NgayDangKy", Type.GetType("System.DateTime"));
            data.Columns.Add("TK_ID", Type.GetType("System.Int64"));
            data.Columns.Add("MaLoaiHinh", Type.GetType("System.String"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int soTK = int.Parse(dt.Rows[i]["SoToKhai"].ToString());
                if (!this.checkExitSoTK(soTK, data))
                {
                    DataRow dr = data.NewRow();
                    dr["SoToKhai"] = dt.Rows[i]["SoToKhai"];
                    dr["NgayDangKy"] = dt.Rows[i]["NgayDangKy"];
                    dr["TK_ID"] = dt.Rows[i]["TKMD_ID"];
                    dr["MaLoaiHinh"] = dt.Rows[i]["MaLoaiHinh"].ToString().Trim();
                    data.Rows.Add(dr);
                }
            }
            return data;
        }

        private DataTable GetMaTB(DataTable dt)
        {
            DataTable data = new DataTable();
            data.Columns.Add("MaTB", Type.GetType("System.String"));
            data.Columns.Add("TenTB", Type.GetType("System.String"));
            data.Columns.Add("DVT", Type.GetType("System.String"));
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string MaTB = dt.Rows[i]["MaPhu"].ToString();
                if (!this.checkExitMaTB(MaTB, data))
                {
                    DataRow dr = data.NewRow();
                    dr["MaTB"] = dt.Rows[i]["MaPhu"];
                    dr["TenTB"] = dt.Rows[i]["TenHang"];
                    dr["DVT"] = DonViTinh_GetName(int.Parse(dt.Rows[i]["DVT_ID"].ToString()));
                    data.Rows.Add(dr);
                }
            }
            return data;
        }

        private bool checkExitSoTK(int k, DataTable dt)
        { 
            bool val = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) == k) val = true;
            }
            return val;
        }

        private bool checkExitMaTB(string MaTB, DataTable dt)
        {
            bool val = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == MaTB) val = true;
            }
            return val;
        }

        private DataTable UnionDataTN(DataTable dt, DataTable dt2)
        {
            DataTable data = new DataTable();
            DataRow dr;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                DataColumn col = new DataColumn();
                col.ColumnName = dt.Columns[i].ColumnName;
                col.Caption = dt.Columns[i].Caption;
                col.DataType = dt.Columns[i].DataType;
                data.Columns.Add(col);                
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dr = data.NewRow();

                for (int j = 0; j < data.Columns.Count; j++)
                {
                    dr[j] = dt.Rows[i][j];
                }
                data.Rows.Add(dr);
            }

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                dr = data.NewRow();

                for (int j = 0; j < data.Columns.Count; j++)
                {
                    dr[j] = dt2.Rows[i][j];
                }
                data.Rows.Add(dr);
            }
            return data;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BK07.setText(this.Label, txtName.Text);
            this.BK07.CreateDocument();
        }

         
    }
}