using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.KDT.GC
{
    public partial class HopDongRegistedCopyForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        public bool IsBrowseForm = false;
        public bool IsBrowseFormCT = false;
        
        public HopDongRegistedCopyForm()
        {
            InitializeComponent();
        }
        public void BindData()
        {
            string where = string.Format("MaDoanhNghiep = '{0}'", GlobalSettings.MA_DON_VI);
            where += "AND TrangThaiXuLy = 1";
           
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                where += string.Format(" AND SoHopDong LIKE '%{0}%' ", txtSoHopDong.Text);
            }
            if ((int)txtNamDangKy.Value > 0)
            {
                where += string.Format(" AND Year(NgayKy) = '{0}' ", txtNamDangKy.Value);
            }
            dgList.DataSource = HopDong.SelectCollectionDynamic(where, "NgayKy DESC");
        }
        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];

        }

        private void HopDongRegistedCopyForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            txtNamDangKy.Value = DateTime.Today.Year;
            BindData();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.HopDongSelected = (HopDong)e.Row.DataRow;
                this.Close();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}