﻿
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace Company.BLL
{
    public partial class DoanhNghiep
    {
        //public bool WSLogin()
        //{
        //    KDTService kdtService = new KDTService();
        //    return true;
        //}
        public DataSet QuerySQL(string SQL, string connecString)
        {
            Database db1 = DatabaseFactory.CreateDatabase("SXXK");
            DbCommand c = db1.GetSqlStringCommand(SQL);
            return db1.ExecuteDataSet(c);

        }
        //Gia Cong :
        public DataSet GetSLXNK(string SQL, string connecString)
        {
            Database db2 = DatabaseFactory.CreateDatabase("SLXNK");
            DbCommand c = db2.GetSqlStringCommand(SQL);
            return db2.ExecuteDataSet(c);
        }
        //Khai điện tử :
        public DataSet GetKhaiDT(string SQL)
        {
            Database db3 = DatabaseFactory.CreateDatabase("KhaiDT");
            DbCommand c = db3.GetSqlStringCommand(SQL);
            return db3.ExecuteDataSet(c);
        }
    }
}