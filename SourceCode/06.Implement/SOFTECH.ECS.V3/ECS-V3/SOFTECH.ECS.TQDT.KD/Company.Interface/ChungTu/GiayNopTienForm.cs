﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class GiayNopTienForm : BaseForm
    {
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public GiayNopTien giayNopTien = new GiayNopTien() { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO, LoaiKB = 1 };

        public GiayNopTienForm()
        {
            InitializeComponent();
        }
        private void SetCommandStatus()
        {
            bool isEnable = giayNopTien.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                            giayNopTien.TrangThai == TrangThaiXuLy.DA_HUY ||
                            giayNopTien.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
            cmdChiTiet.Enabled = cmdChiTiet1.Enabled =
                cmdChungTu.Enabled = cmdChungTu1.Enabled =
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled =
                cmdLuu.Enabled = cmdLuu1.Enabled = cmdXoa.Enabled = cmdXoa1.Enabled
                = isEnable ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            isEnable = giayNopTien.TrangThai == TrangThaiXuLy.CHO_DUYET ||
                giayNopTien.TrangThai == TrangThaiXuLy.CHO_HUY ||
                giayNopTien.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;

            cmdNhanDuLieu.Enabled = cmdNhanDuLieu1.Enabled = isEnable ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

            #region Translate text
            if (giayNopTien.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lbTrangThai.Text = "Chưa khai báo";
            }
            else if (giayNopTien.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                lbTrangThai.Text = "Đã khai báo hải quan";
            else if (giayNopTien.TrangThai == TrangThaiXuLy.CHO_DUYET)
            {
                lbTrangThai.Text = "Chờ duyệt";
            }
            else if (giayNopTien.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                lbTrangThai.Text = "Không phê duyệt";
            else if (giayNopTien.TrangThai == TrangThaiXuLy.DA_DUYET)
                lbTrangThai.Text = "Đã duyệt";
            #endregion

            txtSoTiepNhan.Text = giayNopTien.SoTiepNhan > 0 ? giayNopTien.SoTiepNhan.ToString() : string.Empty;
            ccNgayTiepNhan.Text = giayNopTien.NgayTiepNhan.Year > 1900 ? giayNopTien.NgayTiepNhan.ToLongDateString() : string.Empty;
        }
        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(giayNopTien.GuidStr))
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = giayNopTien.ID;
                form.DeclarationIssuer = AdditionalDocumentType.GIAY_NOP_TIEN;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
        private void Set()
        {
            txtSoLenh.Text = giayNopTien.SoLenh;
            ccNgayPhatLenh.Text = giayNopTien.NgayPhatLenh.ToLongDateString();
            txtMaNguoiNop.Text = giayNopTien.SoCMNDNguoiNop;
            txtTenDVNop.Text = giayNopTien.TenNguoiNop;
            txtDiaChiNguoiNop.Text = giayNopTien.DiaChi;

            txtMaDVNop.Text = giayNopTien.MaDonViNop;
            txtTenDVNop.Text = giayNopTien.TenDonViNop;
            txtSoTKNop.Text = giayNopTien.SoTKNop;

            txtMaDVNhan.Text = giayNopTien.MaDonViNhan;
            txtTenDVNhan.Text = giayNopTien.TenDonViNhan;
            txtSoTKNhan.Text = giayNopTien.SoTKNhan;
            txtGhiChu.Text = giayNopTien.GhiChu;
            txtMaNganHangNhan.Text = giayNopTien.MaNganHangNhan;
            txtTenNganHangNhan.Text = giayNopTien.TenNganHangNhan;
            txtMaNganHangNop.Text = giayNopTien.MaNganHangNop;
            txtTenNganHangNop.Text = giayNopTien.TenNganHangNop;
            try
            {
                dgListDetail.DataSource = giayNopTien.ChiTietCollection;
                dgListChungTu.DataSource = giayNopTien.ChungTuCollection;
                dgListDetail.Refetch();
                dgListChungTu.Refetch();
            }
            catch
            {
                dgListDetail.Refresh();
                dgListChungTu.Refresh();
            }
            if (giayNopTien.ID == 0)
            {
                txtMaDVNop.Text = TKMD.MaDoanhNghiep;
                txtTenDVNop.Text = TKMD.TenDoanhNghiep;
            }

        }

        private void Get()
        {

            giayNopTien.SoLenh = txtSoLenh.Text;
            giayNopTien.NgayPhatLenh = ccNgayPhatLenh.Value;
            giayNopTien.SoCMNDNguoiNop = txtMaNguoiNop.Text;
            giayNopTien.TenNguoiNop = txtTenDVNop.Text;
            giayNopTien.DiaChi = txtDiaChiNguoiNop.Text;

            giayNopTien.MaDonViNop = txtMaDVNop.Text;
            giayNopTien.TenDonViNop = txtTenDVNop.Text;
            giayNopTien.SoTKNop = txtSoTKNop.Text;

            giayNopTien.MaNganHangNop = txtMaNganHangNop.Text;
            giayNopTien.TenNganHangNop = txtTenNganHangNop.Text;

            giayNopTien.MaDonViNhan = txtMaDVNhan.Text;
            giayNopTien.TenDonViNhan = txtTenDVNhan.Text;
            giayNopTien.MaNganHangNhan = txtMaNganHangNhan.Text;
            giayNopTien.TenNganHangNhan = txtTenNganHangNhan.Text;
            giayNopTien.SoTKNhan = txtSoTKNhan.Text;
            if (string.IsNullOrEmpty(giayNopTien.GuidStr))
                giayNopTien.GuidStr = Guid.NewGuid().ToString();
            giayNopTien.GhiChu = txtGhiChu.Text;
            giayNopTien.TKMD_ID = TKMD.ID;
            if (giayNopTien.NgayTiepNhan.Year < 1900)
                giayNopTien.NgayTiepNhan = new DateTime(1900, 1, 1);
        }
        private void Save()
        {
            try
            {
                bool isValidate = true;
                isValidate = Globals.ValidateNull(txtSoLenh, error, "Số lệnh")
                    && Globals.ValidateDate(ccNgayPhatLenh, error, "Ngày phát lệnh")
                    && Globals.ValidateNull(txtMaNguoiNop, error, "Mã người nộp")
                && Globals.ValidateNull(txtMaDVNop, error, "Mã đơn vị nộp")
                && Globals.ValidateNull(txtSoTKNop, error, "Tài khoản nộp")
                && Globals.ValidateNull(txtMaNganHangNop, error, "Mã ngân hàng nộp")
                && Globals.ValidateNull(txtMaDVNhan, error, "Mã đơn vị nhận")
                && Globals.ValidateNull(txtSoTKNhan, error, "Tài khoản nhận");
                if (!isValidate)
                    return;
                Get();
                bool isAdd = giayNopTien.ID == 0;

                giayNopTien.SaveFull();
                if (isAdd)
                {
                    TKMD.GiayNopTiens.Add(giayNopTien);
                }
                ShowMessage("Lưu thông tin thành công", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void GiayNopTienForm_Load(object sender, EventArgs e)
        {
            SetCommandStatus();
            Set();
        }
        private void Delete()
        {
            if (ShowMessage("Bạn có muốn xóa bản giấy nộp tiền này không?", true) != "Yes") return;
            try
            {
                giayNopTien.DeleteFull();
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void ChitietGiayNopTien(GiayNopTienChiTiet giayNopTienChiTiet)
        {
            try
            {
                bool isEdit = cmdLuu.Enabled == Janus.Windows.UI.InheritableBoolean.True;
                GiayNopTienChitietForm chitiet = new GiayNopTienChitietForm(isEdit);
                chitiet.ChiTiet = giayNopTienChiTiet;
                DialogResult result = chitiet.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    giayNopTien.ChiTietCollection.Add(chitiet.ChiTiet);
                }
                else if (result == DialogResult.No)
                {
                    giayNopTien.ChiTietCollection.Remove(chitiet.ChiTiet);
                }
                try
                {
                    dgListDetail.DataSource = giayNopTien.ChiTietCollection;
                    dgListDetail.Refetch();
                }
                catch
                {
                    dgListDetail.Refresh();
                }

            }
            catch (Exception ex)
            {

                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void ChungTuGiayNopTien(GiayNopTienChungTu giayNopTienChungTu)
        {
            try
            {
                bool isEdit = cmdLuu.Enabled == Janus.Windows.UI.InheritableBoolean.True;
                GiayNopTienChungTuForm chungtu = new GiayNopTienChungTuForm(isEdit);
                chungtu.ChungTu = giayNopTienChungTu;
                DialogResult result = chungtu.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    giayNopTien.ChungTuCollection.Add(chungtu.ChungTu);
                }
                else if (result == DialogResult.No)
                {
                    giayNopTien.ChungTuCollection.Remove(chungtu.ChungTu);
                }
                try
                {

                    dgListChungTu.DataSource = giayNopTien.ChungTuCollection;
                    dgListChungTu.Refetch();
                }
                catch
                {
                    dgListChungTu.Refresh();
                }

            }
            catch (Exception ex)
            {

                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void SendV3()
        {
            if (giayNopTien.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if (giayNopTien.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO || giayNopTien.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                    giayNopTien.GuidStr = Guid.NewGuid().ToString();
                else if (giayNopTien.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ sung giayNopTien đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3
                ObjectSend msgSend = SingleMessage.BoSungGiayNopTien(TKMD, giayNopTien);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;

                bool isSend = sendMessageForm.DoSend(msgSend);
                if ((giayNopTien.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                    giayNopTien.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET) && isSend)
                {
                    sendMessageForm.Message.XmlSaveMessage(giayNopTien.ID, MessageTitle.KhaiBaoBoSungGiayNopTien);
                    giayNopTien.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    giayNopTien.Update();

                }
                SetCommandStatus();
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void FeedBack()
        {
            ObjectSend msgSend = SingleMessage.FeedBack(TKMD, giayNopTien.GuidStr);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);
            //SetCommandStatus();
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(giayNopTien.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                giayNopTien.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", giayNopTien.GuidStr), string.Empty);                            
                            this.ShowMessageTQDT(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                giayNopTien.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);
                                if (feedbackContent.Acceptance.Length == 10)
                                    giayNopTien.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    giayNopTien.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    giayNopTien.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(giayNopTien.ID, MessageTitle.KhaiBoSungGiayNopTienCapSo, noidung);
                                giayNopTien.TrangThai = TrangThaiXuLy.CHO_DUYET;

                                this.ShowMessageTQDT("Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + giayNopTien.SoTiepNhan + "\r\nNgày tiếp nhận:" + giayNopTien.NgayTiepNhan.ToLongTimeString(), false);

                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayNopTien.ID, MessageTitle.KhaiBoSungGiayNopTienChapNhan, noidung);
                                giayNopTien.TrangThai = TrangThaiXuLy.DA_DUYET;
                                ShowMessageTQDT("Giấy nộp tiền đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung, false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayNopTien.ID, MessageTitle.Error, noidung);
                                ShowMessageTQDT("Xác nhận thông tin hải quan\r\nChưa rõ nội dung trả về:" + noidung, false);
                                break;
                            }
                    }
                    if (isUpdate)
                        giayNopTien.Update();
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        SetCommandStatus();
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdChiTiet":
                    ChitietGiayNopTien(new GiayNopTienChiTiet());
                    break;
                case "cmdChungTu":
                    ChungTuGiayNopTien(new GiayNopTienChungTu());
                    break;
                case "cmdLuu":
                    Save();
                    break;
                case "cmdXoa":
                    break;
                case "cmdKhaiBao":
                    SendV3();
                    break;
                case "cmdNhanDuLieu":
                    FeedBack();
                    break;
                default:
                    break;
            }
        }

        private void dgListDetail_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            GiayNopTienChiTiet giayNopTienChiTiet = (GiayNopTienChiTiet)e.Row.DataRow;
            if (giayNopTienChiTiet == null) return;
            ChitietGiayNopTien(giayNopTienChiTiet);
        }

        private void dgListDetail_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
                if (dgListDetail.GetRows().Length < 1) return;
                if (cmdLuu.Enabled == Janus.Windows.UI.InheritableBoolean.False) return;
                if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListDetail.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            GiayNopTienChiTiet chitiet = (GiayNopTienChiTiet)i.GetRow().DataRow;

                            if (chitiet.ID > 0)
                            {
                                chitiet.Delete();
                            }
                            giayNopTien.ChiTietCollection.Remove(chitiet);
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
                Set();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                new Company.Controls.KDTMessageBoxControl().ShowMessage("Xóa thông tin hàng không thành công.", false);
            }
        }

        private void dgListChungTu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayNopTienChungTu chungtu = (GiayNopTienChungTu)e.Row.DataRow;
            if (chungtu == null) return;
            ChungTuGiayNopTien(chungtu);
        }

        private void dgListChungTu_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
                if (dgListDetail.GetRows().Length < 1) return;
                if (cmdLuu.Enabled == Janus.Windows.UI.InheritableBoolean.False) return;
                if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListDetail.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            GiayNopTienChungTu chungtu = (GiayNopTienChungTu)i.GetRow().DataRow;

                            if (chungtu.ID > 0)
                            {
                                chungtu.Delete();
                            }
                            giayNopTien.ChungTuCollection.Remove(chungtu);
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
                Set();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                new Company.Controls.KDTMessageBoxControl().ShowMessage("Xóa thông tin hàng không thành công.", false);
            }
        }


    }
}
