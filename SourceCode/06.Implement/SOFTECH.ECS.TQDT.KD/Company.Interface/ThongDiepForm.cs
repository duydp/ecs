﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ThongDiepForm : Company.Interface.BaseForm
    {
        public long ItemID { set; get; }
        public ThongDiepForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = Company.KDT.SHARE.Components.Message.SelectCollectionBy_ItemID(this.ItemID);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                Company.KDT.SHARE.Components.Message message = Company.KDT.SHARE.Components.Message.Load(id);
                this.ShowMessageTQDT( "ID: [" +e.Row.Cells[1].Value +"]\r\n" +message.TieuDeThongBao + "\r\n" + message.NoiDungThongBao + "\r\n" + message.CreatedTime, false);
            }
        }
    }
}
