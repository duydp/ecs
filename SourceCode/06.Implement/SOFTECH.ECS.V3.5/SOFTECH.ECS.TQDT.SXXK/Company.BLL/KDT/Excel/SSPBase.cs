using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class SSP 
	{
		#region Private members.
		
		protected string _MA_HQ = String.Empty;
		protected string _MA_DV = String.Empty;
		protected string _MA_SP = String.Empty;
		protected string _TEN_SP = String.Empty;
		protected string _MA_HS = String.Empty;
		protected string _MA_DVT = String.Empty;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string MA_HQ
		{
			set {this._MA_HQ = value;}
			get {return this._MA_HQ;}
		}
		public string MA_DV
		{
			set {this._MA_DV = value;}
			get {return this._MA_DV;}
		}
		public string MA_SP
		{
			set {this._MA_SP = value;}
			get {return this._MA_SP;}
		}
		public string TEN_SP
		{
			set {this._TEN_SP = value;}
			get {return this._TEN_SP;}
		}
		public string MA_HS
		{
			set {this._MA_HS = value;}
			get {return this._MA_HS;}
		}
		public string MA_DVT
		{
			set {this._MA_DVT = value;}
			get {return this._MA_DVT;}
		}
		
		//---------------------------------------------------------------------------------------------

		
		#endregion
		
		//---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string maHaiQuan, string maDoanhNghiep)
        {
            //string sql = "SELECT a.*,b.Ten_DVT FROM SSP a INNER JOIN SDVT b ON a.Ma_DVT = b.Ma_DVT  WHERE Ma_HQ = @MaHQ AND MA_DV =@MaDV";
            string sql = "SELECT a.*,b.Ten FROM t_SXXK_SanPham a INNER JOIN t_HaiQuan_DonViTinh b ON a.DVT_ID = b.ID  WHERE MaHaiQuan = @MaHQ AND MaDoanhNghiep =@MaDV";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, maDoanhNghiep);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		
	}	
}