using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_GiayPhep_SMA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaNguoiKhai { set; get; }
		public string TenNguoiKhai { set; get; }
		public string MaBuuChinhNguoiKhai { set; get; }
		public string DiaChiNguoiKhai { set; get; }
		public string MaNuocNguoiKhai { set; get; }
		public string SoDienThoaiNguoiKhai { set; get; }
		public string SoFaxNguoiKhai { set; get; }
		public string EmailNguoiKhai { set; get; }
		public decimal SoDonXinCapPhep { set; get; }
		public int ChucNangChungTu { set; get; }
		public string LoaiGiayPhep { set; get; }
		public string MaDonViCapPhep { set; get; }
		public string TenDonViCapPhep { set; get; }
		public string MaThuongNhanNhapKhau { set; get; }
		public string TenThuongNhanNhapKhau { set; get; }
		public string MaBuuChinhNhapKhau { set; get; }
		public string DiaChiThuongNhanNhapKhau { set; get; }
		public string MaQuocGiaNhapKhau { set; get; }
		public string SoDienThoaiNhapKhau { set; get; }
		public string SoFaxNhapKhau { set; get; }
		public string EmailNhapKhau { set; get; }
		public decimal KetQuaXuLy { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string KetQuaXuLySoGiayPhep { set; get; }
		public DateTime KetQuaXuLyNgayCap { set; get; }
		public DateTime KetQuaXuLyHieuLucTuNgay { set; get; }
		public DateTime KetQuaXuLyHieuLucDenNgay { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string HoSoKemTheo_1 { set; get; }
		public string HoSoKemTheo_2 { set; get; }
		public string HoSoKemTheo_3 { set; get; }
		public string HoSoKemTheo_4 { set; get; }
		public string HoSoKemTheo_5 { set; get; }
		public string HoSoKemTheo_6 { set; get; }
		public string HoSoKemTheo_7 { set; get; }
		public string HoSoKemTheo_8 { set; get; }
		public string HoSoKemTheo_9 { set; get; }
		public string HoSoKemTheo_10 { set; get; }
		public string SoCuaGiayPhepLuuHanh { set; get; }
		public string SoGiayPhepThucHanh { set; get; }
		public string MaCuaKhauNhapDuKien { set; get; }
		public string TenCuaKhauNhapDuKien { set; get; }
		public DateTime TonKhoDenNgay { set; get; }
		public string TenGiamDocDoanhNghiep { set; get; }
		public string DaiDienDonViCapPhep { set; get; }
		public string GhiChuDanhChoCoQuanCapPhep { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string PhanLuong { set; get; }
		public string HuongDan { set; get; }
		public string HoSoLienQuan { set; get; }
		public string GhiChu { set; get; }
		public int TrangThaiXuLy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_GiayPhep_SMA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_GiayPhep_SMA> collection = new List<KDT_VNACC_GiayPhep_SMA>();
			while (reader.Read())
			{
				KDT_VNACC_GiayPhep_SMA entity = new KDT_VNACC_GiayPhep_SMA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNguoiKhai"))) entity.MaBuuChinhNguoiKhai = reader.GetString(reader.GetOrdinal("MaBuuChinhNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiKhai"))) entity.DiaChiNguoiKhai = reader.GetString(reader.GetOrdinal("DiaChiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNguoiKhai"))) entity.MaNuocNguoiKhai = reader.GetString(reader.GetOrdinal("MaNuocNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNguoiKhai"))) entity.SoDienThoaiNguoiKhai = reader.GetString(reader.GetOrdinal("SoDienThoaiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxNguoiKhai"))) entity.SoFaxNguoiKhai = reader.GetString(reader.GetOrdinal("SoFaxNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailNguoiKhai"))) entity.EmailNguoiKhai = reader.GetString(reader.GetOrdinal("EmailNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonXinCapPhep"))) entity.SoDonXinCapPhep = reader.GetDecimal(reader.GetOrdinal("SoDonXinCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChucNangChungTu"))) entity.ChucNangChungTu = reader.GetInt32(reader.GetOrdinal("ChucNangChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGiayPhep"))) entity.LoaiGiayPhep = reader.GetString(reader.GetOrdinal("LoaiGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViCapPhep"))) entity.MaDonViCapPhep = reader.GetString(reader.GetOrdinal("MaDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViCapPhep"))) entity.TenDonViCapPhep = reader.GetString(reader.GetOrdinal("TenDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanNhapKhau"))) entity.MaThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanNhapKhau"))) entity.TenThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNhapKhau"))) entity.MaBuuChinhNhapKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiThuongNhanNhapKhau"))) entity.DiaChiThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("DiaChiThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaNhapKhau"))) entity.MaQuocGiaNhapKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNhapKhau"))) entity.SoDienThoaiNhapKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxNhapKhau"))) entity.SoFaxNhapKhau = reader.GetString(reader.GetOrdinal("SoFaxNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailNhapKhau"))) entity.EmailNhapKhau = reader.GetString(reader.GetOrdinal("EmailNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLy"))) entity.KetQuaXuLy = reader.GetDecimal(reader.GetOrdinal("KetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLySoGiayPhep"))) entity.KetQuaXuLySoGiayPhep = reader.GetString(reader.GetOrdinal("KetQuaXuLySoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyNgayCap"))) entity.KetQuaXuLyNgayCap = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyNgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"))) entity.KetQuaXuLyHieuLucTuNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"))) entity.KetQuaXuLyHieuLucDenNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_1"))) entity.HoSoKemTheo_1 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_2"))) entity.HoSoKemTheo_2 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_3"))) entity.HoSoKemTheo_3 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_4"))) entity.HoSoKemTheo_4 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_5"))) entity.HoSoKemTheo_5 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_6"))) entity.HoSoKemTheo_6 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_7"))) entity.HoSoKemTheo_7 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_8"))) entity.HoSoKemTheo_8 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_9"))) entity.HoSoKemTheo_9 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_10"))) entity.HoSoKemTheo_10 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCuaGiayPhepLuuHanh"))) entity.SoCuaGiayPhepLuuHanh = reader.GetString(reader.GetOrdinal("SoCuaGiayPhepLuuHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhepThucHanh"))) entity.SoGiayPhepThucHanh = reader.GetString(reader.GetOrdinal("SoGiayPhepThucHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCuaKhauNhapDuKien"))) entity.MaCuaKhauNhapDuKien = reader.GetString(reader.GetOrdinal("MaCuaKhauNhapDuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCuaKhauNhapDuKien"))) entity.TenCuaKhauNhapDuKien = reader.GetString(reader.GetOrdinal("TenCuaKhauNhapDuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonKhoDenNgay"))) entity.TonKhoDenNgay = reader.GetDateTime(reader.GetOrdinal("TonKhoDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenGiamDocDoanhNghiep"))) entity.TenGiamDocDoanhNghiep = reader.GetString(reader.GetOrdinal("TenGiamDocDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDonViCapPhep"))) entity.DaiDienDonViCapPhep = reader.GetString(reader.GetOrdinal("DaiDienDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"))) entity.GhiChuDanhChoCoQuanCapPhep = reader.GetString(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoLienQuan"))) entity.HoSoLienQuan = reader.GetString(reader.GetOrdinal("HoSoLienQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_GiayPhep_SMA> collection, long id)
        {
            foreach (KDT_VNACC_GiayPhep_SMA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SMA VALUES(@MaNguoiKhai, @TenNguoiKhai, @MaBuuChinhNguoiKhai, @DiaChiNguoiKhai, @MaNuocNguoiKhai, @SoDienThoaiNguoiKhai, @SoFaxNguoiKhai, @EmailNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaDonViCapPhep, @TenDonViCapPhep, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @MaBuuChinhNhapKhau, @DiaChiThuongNhanNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @KetQuaXuLy, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @Notes, @InputMessageID, @MessageTag, @IndexTag, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @SoCuaGiayPhepLuuHanh, @SoGiayPhepThucHanh, @MaCuaKhauNhapDuKien, @TenCuaKhauNhapDuKien, @TonKhoDenNgay, @TenGiamDocDoanhNghiep, @DaiDienDonViCapPhep, @GhiChuDanhChoCoQuanCapPhep, @NgayKhaiBao, @SoTiepNhan, @NgayTiepNhan, @PhanLuong, @HuongDan, @HoSoLienQuan, @GhiChu, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SMA SET MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaBuuChinhNguoiKhai = @MaBuuChinhNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNuocNguoiKhai = @MaNuocNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, SoFaxNguoiKhai = @SoFaxNguoiKhai, EmailNguoiKhai = @EmailNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaDonViCapPhep = @MaDonViCapPhep, TenDonViCapPhep = @TenDonViCapPhep, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, DiaChiThuongNhanNhapKhau = @DiaChiThuongNhanNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, KetQuaXuLy = @KetQuaXuLy, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, SoCuaGiayPhepLuuHanh = @SoCuaGiayPhepLuuHanh, SoGiayPhepThucHanh = @SoGiayPhepThucHanh, MaCuaKhauNhapDuKien = @MaCuaKhauNhapDuKien, TenCuaKhauNhapDuKien = @TenCuaKhauNhapDuKien, TonKhoDenNgay = @TonKhoDenNgay, TenGiamDocDoanhNghiep = @TenGiamDocDoanhNghiep, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, NgayKhaiBao = @NgayKhaiBao, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLuong = @PhanLuong, HuongDan = @HuongDan, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SMA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLy", SqlDbType.Decimal, "KetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, "SoCuaGiayPhepLuuHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, "SoGiayPhepThucHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, "MaCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, "TenCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonKhoDenNgay", SqlDbType.DateTime, "TonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, "TenGiamDocDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLy", SqlDbType.Decimal, "KetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, "SoCuaGiayPhepLuuHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, "SoGiayPhepThucHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, "MaCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, "TenCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonKhoDenNgay", SqlDbType.DateTime, "TonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, "TenGiamDocDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SMA VALUES(@MaNguoiKhai, @TenNguoiKhai, @MaBuuChinhNguoiKhai, @DiaChiNguoiKhai, @MaNuocNguoiKhai, @SoDienThoaiNguoiKhai, @SoFaxNguoiKhai, @EmailNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaDonViCapPhep, @TenDonViCapPhep, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @MaBuuChinhNhapKhau, @DiaChiThuongNhanNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @KetQuaXuLy, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @Notes, @InputMessageID, @MessageTag, @IndexTag, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @SoCuaGiayPhepLuuHanh, @SoGiayPhepThucHanh, @MaCuaKhauNhapDuKien, @TenCuaKhauNhapDuKien, @TonKhoDenNgay, @TenGiamDocDoanhNghiep, @DaiDienDonViCapPhep, @GhiChuDanhChoCoQuanCapPhep, @NgayKhaiBao, @SoTiepNhan, @NgayTiepNhan, @PhanLuong, @HuongDan, @HoSoLienQuan, @GhiChu, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SMA SET MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaBuuChinhNguoiKhai = @MaBuuChinhNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNuocNguoiKhai = @MaNuocNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, SoFaxNguoiKhai = @SoFaxNguoiKhai, EmailNguoiKhai = @EmailNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaDonViCapPhep = @MaDonViCapPhep, TenDonViCapPhep = @TenDonViCapPhep, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, DiaChiThuongNhanNhapKhau = @DiaChiThuongNhanNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, KetQuaXuLy = @KetQuaXuLy, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, SoCuaGiayPhepLuuHanh = @SoCuaGiayPhepLuuHanh, SoGiayPhepThucHanh = @SoGiayPhepThucHanh, MaCuaKhauNhapDuKien = @MaCuaKhauNhapDuKien, TenCuaKhauNhapDuKien = @TenCuaKhauNhapDuKien, TonKhoDenNgay = @TonKhoDenNgay, TenGiamDocDoanhNghiep = @TenGiamDocDoanhNghiep, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, NgayKhaiBao = @NgayKhaiBao, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLuong = @PhanLuong, HuongDan = @HuongDan, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SMA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLy", SqlDbType.Decimal, "KetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, "SoCuaGiayPhepLuuHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, "SoGiayPhepThucHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, "MaCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, "TenCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonKhoDenNgay", SqlDbType.DateTime, "TonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, "TenGiamDocDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLy", SqlDbType.Decimal, "KetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, "SoCuaGiayPhepLuuHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, "SoGiayPhepThucHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, "MaCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, "TenCuaKhauNhapDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonKhoDenNgay", SqlDbType.DateTime, "TonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, "TenGiamDocDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_GiayPhep_SMA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_GiayPhep_SMA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_GiayPhep_SMA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_GiayPhep_SMA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_GiayPhep_SMA(string maNguoiKhai, string tenNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string tenDonViCapPhep, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, decimal ketQuaXuLy, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string soCuaGiayPhepLuuHanh, string soGiayPhepThucHanh, string maCuaKhauNhapDuKien, string tenCuaKhauNhapDuKien, DateTime tonKhoDenNgay, string tenGiamDocDoanhNghiep, string daiDienDonViCapPhep, string ghiChuDanhChoCoQuanCapPhep, DateTime ngayKhaiBao, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string hoSoLienQuan, string ghiChu, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SMA entity = new KDT_VNACC_GiayPhep_SMA();	
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.KetQuaXuLy = ketQuaXuLy;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.SoCuaGiayPhepLuuHanh = soCuaGiayPhepLuuHanh;
			entity.SoGiayPhepThucHanh = soGiayPhepThucHanh;
			entity.MaCuaKhauNhapDuKien = maCuaKhauNhapDuKien;
			entity.TenCuaKhauNhapDuKien = tenCuaKhauNhapDuKien;
			entity.TonKhoDenNgay = tonKhoDenNgay;
			entity.TenGiamDocDoanhNghiep = tenGiamDocDoanhNghiep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@KetQuaXuLy", SqlDbType.Decimal, KetQuaXuLy);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, SoCuaGiayPhepLuuHanh);
			db.AddInParameter(dbCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, SoGiayPhepThucHanh);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, MaCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, TenCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TonKhoDenNgay", SqlDbType.DateTime, TonKhoDenNgay.Year <= 1753 ? DBNull.Value : (object) TonKhoDenNgay);
			db.AddInParameter(dbCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, TenGiamDocDoanhNghiep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_GiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SMA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_GiayPhep_SMA(long id, string maNguoiKhai, string tenNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string tenDonViCapPhep, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, decimal ketQuaXuLy, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string soCuaGiayPhepLuuHanh, string soGiayPhepThucHanh, string maCuaKhauNhapDuKien, string tenCuaKhauNhapDuKien, DateTime tonKhoDenNgay, string tenGiamDocDoanhNghiep, string daiDienDonViCapPhep, string ghiChuDanhChoCoQuanCapPhep, DateTime ngayKhaiBao, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string hoSoLienQuan, string ghiChu, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SMA entity = new KDT_VNACC_GiayPhep_SMA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.KetQuaXuLy = ketQuaXuLy;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.SoCuaGiayPhepLuuHanh = soCuaGiayPhepLuuHanh;
			entity.SoGiayPhepThucHanh = soGiayPhepThucHanh;
			entity.MaCuaKhauNhapDuKien = maCuaKhauNhapDuKien;
			entity.TenCuaKhauNhapDuKien = tenCuaKhauNhapDuKien;
			entity.TonKhoDenNgay = tonKhoDenNgay;
			entity.TenGiamDocDoanhNghiep = tenGiamDocDoanhNghiep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_GiayPhep_SMA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@KetQuaXuLy", SqlDbType.Decimal, KetQuaXuLy);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, SoCuaGiayPhepLuuHanh);
			db.AddInParameter(dbCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, SoGiayPhepThucHanh);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, MaCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, TenCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TonKhoDenNgay", SqlDbType.DateTime, TonKhoDenNgay.Year <= 1753 ? DBNull.Value : (object) TonKhoDenNgay);
			db.AddInParameter(dbCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, TenGiamDocDoanhNghiep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_GiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SMA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_GiayPhep_SMA(long id, string maNguoiKhai, string tenNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string tenDonViCapPhep, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, decimal ketQuaXuLy, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string soCuaGiayPhepLuuHanh, string soGiayPhepThucHanh, string maCuaKhauNhapDuKien, string tenCuaKhauNhapDuKien, DateTime tonKhoDenNgay, string tenGiamDocDoanhNghiep, string daiDienDonViCapPhep, string ghiChuDanhChoCoQuanCapPhep, DateTime ngayKhaiBao, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string hoSoLienQuan, string ghiChu, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SMA entity = new KDT_VNACC_GiayPhep_SMA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.KetQuaXuLy = ketQuaXuLy;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.SoCuaGiayPhepLuuHanh = soCuaGiayPhepLuuHanh;
			entity.SoGiayPhepThucHanh = soGiayPhepThucHanh;
			entity.MaCuaKhauNhapDuKien = maCuaKhauNhapDuKien;
			entity.TenCuaKhauNhapDuKien = tenCuaKhauNhapDuKien;
			entity.TonKhoDenNgay = tonKhoDenNgay;
			entity.TenGiamDocDoanhNghiep = tenGiamDocDoanhNghiep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@KetQuaXuLy", SqlDbType.Decimal, KetQuaXuLy);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@SoCuaGiayPhepLuuHanh", SqlDbType.VarChar, SoCuaGiayPhepLuuHanh);
			db.AddInParameter(dbCommand, "@SoGiayPhepThucHanh", SqlDbType.VarChar, SoGiayPhepThucHanh);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhapDuKien", SqlDbType.VarChar, MaCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhapDuKien", SqlDbType.NVarChar, TenCuaKhauNhapDuKien);
			db.AddInParameter(dbCommand, "@TonKhoDenNgay", SqlDbType.DateTime, TonKhoDenNgay.Year <= 1753 ? DBNull.Value : (object) TonKhoDenNgay);
			db.AddInParameter(dbCommand, "@TenGiamDocDoanhNghiep", SqlDbType.NVarChar, TenGiamDocDoanhNghiep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_GiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SMA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_GiayPhep_SMA(long id)
		{
			KDT_VNACC_GiayPhep_SMA entity = new KDT_VNACC_GiayPhep_SMA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_GiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SMA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}