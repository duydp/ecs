﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using Company.GC.BLL.KDT.GC;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report.GC.TT39
{
    public partial class Report_16_ĐMTT_GSQL : DevExpress.XtraReports.UI.XtraReport
    {
        public Report_16_ĐMTT_GSQL()
        {
            InitializeComponent();
        }
        public void BindReport(List<DinhMuc> DinhMucCollection , DateTime  DateFrom , DateTime DateTo)
        {
            int k = 1;
            foreach (DinhMuc item in DinhMucCollection)
            {
                item.STT = k;
                item.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_ID);
                item.DVT = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT);
                item.DinhMucSuDung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                k++;
            }
            DetailReport.DataSource = DinhMucCollection;
            lblTuNgay.Text = DateFrom.ToString("dd/MM/yyyy");
            lblDenNgay.Text = DateTo.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenSP");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSanPham");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNguyenPhuLieu");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            txtDVTNPL.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            txtLuongNLVTTT.DataBindings.Add("Text", DetailReport.DataSource, "DinhMucSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");    
        }
        public void BindReport(KDT_SXXK_DinhMucThucTeDangKy dinhMucThucTeDangKy)
        {
            int k = 1;
            List<KDT_SXXK_DinhMucThucTe_DinhMuc> DinhMucCollection = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();
            foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
            {
                foreach (KDT_SXXK_DinhMucThucTe_DinhMuc ite in item.DMCollection)
                {
                    DinhMucCollection.Add(ite);
                }
            }
            foreach (KDT_SXXK_DinhMucThucTe_DinhMuc item in DinhMucCollection)
            {
                item.STT = k;
                item.DVT_SP = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_SP);
                item.DVT_NPL = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_NPL);
                item.DinhMucSuDung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                k++;
            }
            DetailReport.DataSource = DinhMucCollection;
            lblTuNgay.Text = dinhMucThucTeDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            lblDenNgay.Text = dinhMucThucTeDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenSanPham");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSanPham");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT_SP");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            txtDVTNPL.DataBindings.Add("Text", DetailReport.DataSource, "DVT_NPL");
            txtLuongNLVTTT.DataBindings.Add("Text", DetailReport.DataSource, "DinhMucSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");
        }

        public void BindReport(List<KDT_SXXK_DinhMucThucTe_DinhMuc> DinhMucCollection, DateTime DateFrom, DateTime DateTo)
        {
            int k = 1;
            foreach (KDT_SXXK_DinhMucThucTe_DinhMuc item in DinhMucCollection)
            {
                item.STT = k;
                item.DVT_SP = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_SP);
                item.DVT_NPL = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_NPL);
                item.DinhMucSuDung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                k++;
            }
            DetailReport.DataSource = DinhMucCollection;
            lblTuNgay.Text = DateFrom.ToString("dd/MM/yyyy");
            lblDenNgay.Text = DateTo.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenSanPham");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSanPham");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT_SP");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            txtDVTNPL.DataBindings.Add("Text", DetailReport.DataSource, "DVT_NPL");
            txtLuongNLVTTT.DataBindings.Add("Text", DetailReport.DataSource, "DinhMucSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");
        }
    }
}
