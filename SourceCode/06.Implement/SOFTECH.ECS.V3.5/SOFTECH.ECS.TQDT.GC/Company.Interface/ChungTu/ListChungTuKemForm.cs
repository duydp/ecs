﻿using System;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListChungTuKemForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListChungTuKemForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKMD.ID > 0)
            {
              // dgList.DataSource = ChungTuKem.SelectCollectionBy_TKMDID(TKMD.ID);
                TKMD.ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(TKMD.ID);
                dgList.DataSource = TKMD.ChungTuKemCollection;
            }           
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            //HUNGTQ, Uopdate 07/06/2010            
            //SetButtonStateCHUNGTUKEM(TKMD, isKhaiBoSung);
        }
        
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKem> listChungTuKem = new List<ChungTuKem>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKem ctk = (ChungTuKem)i.GetRow().DataRow;
                        listChungTuKem.Add(ctk);
                    }
                }
            }

            foreach (ChungTuKem ctk in listChungTuKem)
            {
                if (ctk.ID > 0)
                {
                    ctk.Delete();
                }
                //TKMD.GiayPhepCollection.Remove(gp);
                TKMD.ChungTuKemCollection.Remove(ctk);
            }

            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
              
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChungTuKemForm   ctForm = new ChungTuKemForm();   
            ctForm.TKMD = TKMD;
            ctForm.isKhaiBoSung = this.isKhaiBoSung;
            ctForm.ShowDialog();
            dgList.DataSource = TKMD.ChungTuKemCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuKemForm f = new ChungTuKemForm();           
            f.TKMD = TKMD;
            f.CTK = (ChungTuKem)e.Row.DataRow;
            f.isKhaiBoSung = this.isKhaiBoSung;
           
            
            if (f.CTK.ID > 0)
            f.CTK.LoadListCTChiTiet();

            f.ShowDialog();
            dgList.DataSource = TKMD.ChungTuKemCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUNGTUKEM(Company.GC.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung)
        {
            bool status = false;
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkmd.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkmd.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        //btnXoa.Enabled = status;
                        btnTaoMoi.Enabled = status;
                    }
                }
            }
            return true;
        }

        #endregion

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            else
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKem gp = (ChungTuKem)i.GetRow().DataRow;
                        if (gp.SOTN > 0)
                            btnXoa.Enabled = false;
                        else
                            btnXoa.Enabled = true;
                    }
                }
            }
        }
     
    }
}
