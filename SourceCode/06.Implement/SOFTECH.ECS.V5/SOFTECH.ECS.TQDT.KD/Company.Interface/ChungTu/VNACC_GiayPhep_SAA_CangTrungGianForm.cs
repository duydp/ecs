﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_GiayPhep_SAA_CangTrungGianForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_HangGiayPhep_CangTrungGian CangTrungGian = null;
        public KDT_VNACC_GiayPhep_SAA GiayPhep_SAA;
        private EGiayPhep GiayPhepType = EGiayPhep.SAA;
        private bool isAddNew = true;

        private DataTable dtHS = new DataTable();

        public VNACC_GiayPhep_SAA_CangTrungGianForm()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SAA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (isAddNew)
                {
                    CangTrungGian = new KDT_VNACC_HangGiayPhep_CangTrungGian();

                    GetHangGiayPhep(CangTrungGian);

                    GiayPhep_SAA.CangTrungGianCollection.Add(CangTrungGian);
                }
                else
                {
                    GetHangGiayPhep(CangTrungGian);
                }

                grdHang.DataSource = GiayPhep_SAA.CangTrungGianCollection;
                grdHang.Refetch();

                CangTrungGian = new KDT_VNACC_HangGiayPhep_CangTrungGian();
                SetHangGiayPhep(CangTrungGian);
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangGiayPhep(KDT_VNACC_HangGiayPhep_CangTrungGian CangTrungGian)
        {
            CangTrungGian.TenHangCangTrungGian = txtTenHangCangTrungGian.Text;
            CangTrungGian.TenCangTrungGian = txtTenCangTrungGian.Text;
            CangTrungGian.SoLuongHangCangTrungGian = Convert.ToDecimal(txtSoLuongHangCangTrungGian.Value);
            CangTrungGian.DonViTinhSoLuongHangCangTrungGian = ucDonViTinhSoLuongHangCangTrungGian.Code;
            CangTrungGian.KhoiLuongHangCangTrungGian = Convert.ToDecimal(txtKhoiLuongHangCangTrungGian.Value);
            CangTrungGian.DonViTinhKhoiLuongHangCangTrungGian = ucDonViTinhKhoiLuongHangCangTrungGian.Code;
        }

        private void SetHangGiayPhep(KDT_VNACC_HangGiayPhep_CangTrungGian CangTrungGian)
        {
            txtTenHangCangTrungGian.Text = CangTrungGian.TenHangCangTrungGian;
            txtTenCangTrungGian.Text = CangTrungGian.TenCangTrungGian;
            txtSoLuongHangCangTrungGian.Value = CangTrungGian.SoLuongHangCangTrungGian;
            ucDonViTinhSoLuongHangCangTrungGian.Code = CangTrungGian.DonViTinhSoLuongHangCangTrungGian;
            txtKhoiLuongHangCangTrungGian.Value = CangTrungGian.KhoiLuongHangCangTrungGian;
            ucDonViTinhKhoiLuongHangCangTrungGian.Code = CangTrungGian.DonViTinhKhoiLuongHangCangTrungGian;
        }

        private void VNACC_GiayPhep_SAA_CangTrungGianForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                SetMaxLengthControl();

                grdHang.DataSource = GiayPhep_SAA.CangTrungGianCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_HangGiayPhep_CangTrungGian> hangColl = new List<KDT_VNACC_HangGiayPhep_CangTrungGian>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangGiayPhep_CangTrungGian)i.GetRow().DataRow);
                        }

                    }

                    foreach (KDT_VNACC_HangGiayPhep_CangTrungGian hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        GiayPhep_SAA.CangTrungGianCollection.Remove(hmd);
                    }

                    grdHang.DataSource = GiayPhep_SAA.CangTrungGianCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                // List<KDT_VNACC_HangGiayPhep_CangTrungGian> hangColl = new List<KDT_VNACC_HangGiayPhep_CangTrungGian>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                CangTrungGian = (KDT_VNACC_HangGiayPhep_CangTrungGian)items[0].GetRow().DataRow;
                SetHangGiayPhep(CangTrungGian);
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHangCangTrungGian.Tag = "D__"; //Tên hàng cảng trung gian
                txtSoLuongHangCangTrungGian.Tag = "Q__"; //Số lượng hàng cảng trung gian
                ucDonViTinhSoLuongHangCangTrungGian.TagName = "U__"; //Đơn vị tính số lượng
                txtKhoiLuongHangCangTrungGian.Tag = "W__"; //Khối lượng hàng cảng trung gian
                ucDonViTinhKhoiLuongHangCangTrungGian.TagName = "X__"; //Đơn vị tính khối lượng
                txtTenCangTrungGian.Tag = "P__"; //Tên cảng trung gian
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //isValid &= Globals.ValidateNull(txtTenHang, errorProvider, "Mô tả hàng hóa (Loại động vật/sản phẩm động vật)");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHangCangTrungGian.MaxLength = 100;
                txtSoLuongHangCangTrungGian.MaxLength = 8;
                //txtDonViTinhSoLuongHangCangTrungGian.MaxLength = 3;
                txtKhoiLuongHangCangTrungGian.MaxLength = 10;
                txtKhoiLuongHangCangTrungGian.DecimalDigits = 3;
                //txtDonViTinhKhoiLuongHangCangTrungGian.MaxLength = 3;
                txtTenCangTrungGian.MaxLength = 100;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

    }
}
