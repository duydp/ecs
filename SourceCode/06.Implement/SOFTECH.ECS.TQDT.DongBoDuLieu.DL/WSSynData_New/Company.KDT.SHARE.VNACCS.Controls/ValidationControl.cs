﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Text.RegularExpressions;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public class ValidationControl
    {
        public static bool CheckEmail(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }

        /*DEVEXPRESS*/

        /// <summary>
        /// Kiểm tra thông tin trùng lặp cho Control DevExpress
        /// </summary>
        /// <param name="textboxControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateDuplicateDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, bool check)
        {
            return ValidateDuplicateDevExpress(textBoxControl, err, fieldName, check, false);
        }
        public static bool ValidateDuplicateDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, bool check, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
                textBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' đã tồn tại.";
            err.SetError(textBoxControl, "");
            err.SetIconAlignment(textBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (check)
            {
                isValid = false;
                err.SetError(textBoxControl, msgErr);
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }
            textBoxControl.Focus();
            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNullLookUpEditDevExpress(LookUpEdit lookupEditControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullLookUpEditDevExpress(lookupEditControl, err, fieldName, false);
        }
        public static bool ValidateNullLookUpEditDevExpress(LookUpEdit lookupEditControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                lookupEditControl.Focus();
                return true;
            }

            bool isValid = true;
            try
            {
                string msgErr = "Thông tin " + (fieldName != "" ? "'" + fieldName + "'" : "") + " không được để trống";

                err.SetError(lookupEditControl, "");
                err.SetIconAlignment(lookupEditControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
                if (lookupEditControl.Text == "")
                {
                    isValid = false;
                    err.SetError(lookupEditControl, msgErr);
                    lookupEditControl.Focus();
                    lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                isValid = false;
            }
            return isValid;
        }

        public static bool ValidateNullLookUpEditDevExpress(GridLookUpEdit lookupEditControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullLookUpEditDevExpress(lookupEditControl, err, fieldName, false);
        }
        public static bool ValidateNullLookUpEditDevExpress(GridLookUpEdit lookupEditControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                lookupEditControl.Focus();
                return true;
            }

            bool isValid = true;
            try
            {
                string msgErr = "Thông tin " + (fieldName != "" ? "'" + fieldName + "'" : "") + " không được để trống";

                err.SetError(lookupEditControl, "");
                err.SetIconAlignment(lookupEditControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
                if (lookupEditControl.Text == "")
                {
                    isValid = false;
                    err.SetError(lookupEditControl, msgErr);
                    lookupEditControl.Focus();
                    lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                isValid = false;
            }
            return isValid;
        }

        public static bool ValidateChooseLookUpEditDevExpress(LookUpEdit lookupEditControl, DXErrorProvider err, string fieldName)
        {
            return ValidateChooseLookUpEditDevExpress(lookupEditControl, err, fieldName, false);
        }
        public static bool ValidateChooseLookUpEditDevExpress(LookUpEdit lookupEditControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                lookupEditControl.Focus();
                return true;
            }

            bool isValid = true;
            try
            {
                string msgErr = "Bạn chưa chọn thông tin " + (fieldName != "" ? "'" + fieldName + "'" : "");

                err.SetError(lookupEditControl, "");
                err.SetIconAlignment(lookupEditControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
                if (lookupEditControl.EditValue == null)
                {
                    isValid = false;
                    err.SetError(lookupEditControl, msgErr);
                    lookupEditControl.Focus();
                    lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                isValid = false;
            }
            return isValid;
        }

        public static bool ValidateChooseLookUpEditDevExpress(GridLookUpEdit lookupEditControl, DXErrorProvider err, string fieldName)
        {
            return ValidateChooseLookUpEditDevExpress(lookupEditControl, err, fieldName, false);
        }
        public static bool ValidateChooseLookUpEditDevExpress(GridLookUpEdit lookupEditControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                lookupEditControl.Focus();
                return true;
            }

            bool isValid = true;
            try
            {
                string msgErr = "Bạn chưa chọn thông tin " + (fieldName != "" ? "'" + fieldName + "'" : "");

                err.SetError(lookupEditControl, "");
                err.SetIconAlignment(lookupEditControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
                if (lookupEditControl.EditValue == null)
                {
                    isValid = false;
                    err.SetError(lookupEditControl, msgErr);
                    lookupEditControl.Focus();
                    lookupEditControl.BackColor = System.Drawing.SystemColors.Info;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                isValid = false;
            }
            return isValid;
        }
        
        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNullComboBoxDevExpress(ComboBoxEdit comboBoxControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullComboBoxDevExpress(comboBoxControl, err, fieldName, false);
        }
        public static bool ValidateNullComboBoxDevExpress(ComboBoxEdit comboBoxControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                comboBoxControl.BackColor = System.Drawing.SystemColors.Info;
                comboBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(comboBoxControl, "");
            err.SetIconAlignment(comboBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            //if (comboBoxControl.SelectedIndex == -1)
            if (comboBoxControl.Text.Length == 0)
            {
                isValid = false;
                err.SetError(comboBoxControl, msgErr);
                comboBoxControl.Focus();
                comboBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }

            return isValid;
        }

        public static bool ValidateNullComboBoxDevExpress(GridLookUpEdit comboBoxControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullComboBoxDevExpress(comboBoxControl, err, fieldName, false);
        }
        public static bool ValidateNullComboBoxDevExpress(GridLookUpEdit comboBoxControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                comboBoxControl.BackColor = System.Drawing.SystemColors.Info;
                comboBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(comboBoxControl, "");
            err.SetIconAlignment(comboBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            //if (comboBoxControl.SelectedIndex == -1)
            if (comboBoxControl.Text.Length == 0)
            {
                isValid = false;
                err.SetError(comboBoxControl, msgErr);
                comboBoxControl.Focus();
                comboBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng cho Control DevExpress
        /// </summary>
        /// <param name="textboxControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNullDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullDevExpress(textBoxControl, err, fieldName, false);
        }
        public static bool ValidateNullDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
                textBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";
            err.SetError(textBoxControl, "");
            err.SetIconAlignment(textBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (textBoxControl.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(textBoxControl, msgErr);
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }

            if (textBoxControl.Text.Trim() == "1900")
            {
                isValid = false;
                err.SetError(textBoxControl, "Nhập năm không đúng");
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }
            textBoxControl.Focus();
            return isValid;
        }

        public static bool ValidateNullDevExpress(PopupContainerEdit popupContainerEdit, DXErrorProvider err, string fieldName)
        {
            return ValidateNullDevExpress(popupContainerEdit, err, fieldName, false);
        }
        public static bool ValidateNullDevExpress(PopupContainerEdit popupContainerEdit, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                popupContainerEdit.BackColor = System.Drawing.SystemColors.Info;
                popupContainerEdit.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";
            err.SetError(popupContainerEdit, "");
            err.SetIconAlignment(popupContainerEdit, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (popupContainerEdit.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(popupContainerEdit, msgErr);
                popupContainerEdit.BackColor = System.Drawing.SystemColors.Info;
            }

            popupContainerEdit.Focus();
            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng cho Control DevExpress
        /// </summary>
        /// <param name="textboxControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateEmailDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName)
        {
            return ValidateEmailDevExpress(textBoxControl, err, fieldName, false);
        }
        public static bool ValidateEmailDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
                textBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không hợp lệ.";
            err.SetError(textBoxControl, "");
            err.SetIconAlignment(textBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (!CheckEmail(textBoxControl.Text.Trim()))
            {
                isValid = false;
                err.SetError(textBoxControl, msgErr);
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }

            textBoxControl.Focus();
            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng cho Control DevExpress
        /// </summary>
        /// <param name="textboxControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNullDevExpress(ButtonEdit buttonEditControl, DXErrorProvider err, string fieldName)
        {
            return ValidateNullDevExpress(buttonEditControl, err, fieldName, false);
        }
        public static bool ValidateNullDevExpress(ButtonEdit buttonEditControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                buttonEditControl.BackColor = System.Drawing.SystemColors.Info;
                buttonEditControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";
            err.SetError(buttonEditControl, "");
            err.SetIconAlignment(buttonEditControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (buttonEditControl.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(buttonEditControl, msgErr);
                buttonEditControl.BackColor = System.Drawing.SystemColors.Info;
            }

            buttonEditControl.Focus();
            return isValid;
        }

        /// <summary>
        /// Hàm kiểm tra sai ngày
        /// </summary>
        /// <param name="textBoxControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateWrongDateFormatDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName)
        {
            return ValidateWrongDateFormatDevExpress(textBoxControl, err, fieldName, false);
        }
        public static bool ValidateWrongDateFormatDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
                textBoxControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Định dạng '" + fieldName + "' không đúng.";
            err.SetError(textBoxControl, "");
            err.SetIconAlignment(textBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (textBoxControl.Text == "1900")
            {
                isValid = false;
                err.SetError(textBoxControl, msgErr);
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }
            textBoxControl.Focus();
            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin > 0 cho Control DevExpress.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateZeroDevExpress(BaseControl numericUpDownControl, DXErrorProvider err, string fieldName)
        {
            return ValidateZeroDevExpress(numericUpDownControl, err, fieldName, false);
        }
        public static bool ValidateZeroDevExpress(BaseControl numericUpDownControl, DXErrorProvider err, string fieldName, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                numericUpDownControl.BackColor = System.Drawing.SystemColors.Info;
                numericUpDownControl.Focus();
                return true;
            }

            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' phải > 0.";

            err.SetError(numericUpDownControl, "");

            if (numericUpDownControl.Text == "0")
            {
                isValid = false;
                err.SetError(numericUpDownControl, msgErr);
                err.SetIconAlignment(numericUpDownControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
                numericUpDownControl.Focus();
                numericUpDownControl.BackColor = System.Drawing.SystemColors.Info;
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="valueString"></param>
        /// <param name="lengthLimit"></param>
        /// <returns></returns>
        public static bool ValidateLengthDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, int lengthLimit)
        {
            return ValidateLengthDevExpress(textBoxControl, err, fieldName, lengthLimit, false);
        }
        public static bool ValidateLengthDevExpress(BaseControl textBoxControl, DXErrorProvider err, string fieldName, int lengthLimit, bool isOnlyWarning)
        {
            if (isOnlyWarning)
            {
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
                textBoxControl.Focus();
                return true;
            }

            bool isValid = true;

            string msgErr = "Thông tin '" + fieldName + "' đã vượt quá độ dài cho phép. Độ dài quy định: " + lengthLimit + " ký tự.";

            err.SetError(textBoxControl, "");
            err.SetIconAlignment(textBoxControl, System.Windows.Forms.ErrorIconAlignment.MiddleRight);
            if (textBoxControl.Text.Trim().Length >= lengthLimit)
            {
                isValid = false;
                err.SetError(textBoxControl, msgErr);
                textBoxControl.BackColor = System.Drawing.SystemColors.Info;
            }
            textBoxControl.Focus();
            return isValid;
        }

        /// <summary>
        /// So sánh mat khau.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="err"></param>
        /// <param name="fieldNameStart"></param>
        /// <param name="fieldNameEnd"></param>
        /// <returns></returns>
        public static bool ValidateComparePasswordDevExpress(BaseControl start, BaseControl end, DXErrorProvider err, string fieldNameEnd)
        {
            bool isValid = true;
            string msgErr = "Mật khẩu không chính xác.";

            err.SetError(end, "");

            if (start.Text != end.Text)
            {
                isValid = false;
                err.SetError(end, msgErr);
                end.Focus();
                end.BackColor = System.Drawing.SystemColors.Info;
            }

            return isValid;
        }

    }
}
