using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace KeyServer
{
	public partial class Product
	{
		#region Properties.
		
		public string Product_ID { set; get; }
		public long Group_ID { set; get; }
		public string Name { set; get; }
		public string Description { set; get; }
		public string Prefix { set; get; }
		public string Creator { set; get; }
		public DateTime CreatedDate { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Product Load(string product_ID)
		{
			const string spName = "[dbo].[p_LM_Product_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, product_ID);
			Product entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new Product();
				if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
				if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<Product> SelectCollectionAll()
		{
			List<Product> collection = new List<Product>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				Product entity = new Product();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
				if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<Product> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<Product> collection = new List<Product>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				Product entity = new Product();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
				if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<Product> SelectCollectionBy_Group_ID(long group_ID)
		{
			List<Product> collection = new List<Product>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Group_ID(group_ID);
			while (reader.Read())
			{
				Product entity = new Product();
				if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
				if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Group_ID(long group_ID)
		{
			const string spName = "[dbo].[p_LM_Product_SelectBy_Group_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Group_ID(long group_ID)
		{
			const string spName = "p_LM_Product_SelectBy_Group_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertProduct(long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			Product entity = new Product();	
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Product_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Product item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateProduct(string product_ID, long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			Product entity = new Product();			
			entity.Product_ID = product_ID;
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Product_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year == 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Product item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateProduct(string product_ID, long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			Product entity = new Product();			
			entity.Product_ID = product_ID;
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Product_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year == 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Product item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteProduct(string product_ID)
		{
			Product entity = new Product();
			entity.Product_ID = product_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Product_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Group_ID(long group_ID)
		{
			const string spName = "[dbo].[p_LM_Product_DeleteBy_Group_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Product_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Product item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}