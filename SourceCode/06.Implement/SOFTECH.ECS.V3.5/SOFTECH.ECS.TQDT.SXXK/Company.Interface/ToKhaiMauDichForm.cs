﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.BLL;
using Company.BLL.KDT;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using System.Net.Mail;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;
using Infragistics.Excel;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;

namespace Company.Interface
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public string NhomLoaiHinh = string.Empty;
        private string xmlCurrent = "";
        public long pTKMD_ID = 0;
        public bool _bNew = true;
        public static string tenPTVT = "";
        private static int i;
        public bool luu = false;
        public static int soDongHang;
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        //public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();

        #region Biến phục vụ cho việc In tờ khai sửa đổi bổ sung
        //BEGIN: DATLMQ bổ sung các biến dùng để lưu giá trị TK cũ 16/02/2011
        private string nguoiNhapKhau = string.Empty;
        private string nguoiXuatKhau = string.Empty;
        private string nguoiUyThac = string.Empty;
        private string PTVT = string.Empty;
        private string soHieuPTVT = string.Empty;
        private DateTime ngayDenPTVT = new DateTime(1900, 1, 1);
        private string nuocXuatKhau = string.Empty;
        private string nuocNhapKhau = string.Empty;
        private string dieuKienGiaoHang = string.Empty;
        private string phuongThucThanhToan = string.Empty;
        private string soGiayPhep = string.Empty;
        private DateTime ngayGiayPhep = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanGiayPhep = new DateTime(1900, 1, 1);
        private string hoaDonTM = string.Empty;
        private DateTime ngayHoaDonTM = new DateTime(1900, 1, 1);
        private string diaDiemDoHang = string.Empty;
        private string nguyenTe = string.Empty;
        private decimal tyGiaTinhThue = 0;
        private decimal tyGiaUSD = 0;
        private string soHopDong = string.Empty;
        private DateTime ngayHopDong = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanHopDong = new DateTime(1900, 1, 1);
        private string soVanDon = string.Empty;
        private DateTime ngayVanDon = new DateTime(1900, 1, 1);
        private string diaDiemXepHang = string.Empty;
        private decimal soContainer20;
        private decimal soContainer40;
        private decimal soKienHang;
        private decimal trongLuong;
        private double trongLuongNet;
        private decimal lePhiHaiQuan;
        private decimal phiBaoHiem;
        private decimal phiVanChuyen;
        private decimal phiKhac;
        //END

        //DATLMQ bổ sung biến phục vụ việc tự động lưu nội dung sửa đổi bổ sung 04/03/2011
        //public static List<Company.KD.BLL.KDT.HangMauDich> ListHangMauDichEdit = new List<Company.KD.BLL.KDT.HangMauDich>();
        //private static Company.KD.BLL.KDT.HangMauDich hangMDTK = new Company.KD.BLL.KDT.HangMauDich();
        public static string maHS_Edit = string.Empty;
        public static string tenHang_Edit = string.Empty;
        public static string maHang_Edit = string.Empty;
        public static string xuatXu_Edit = string.Empty;
        public static decimal soLuong_Edit;
        public static string dvt_Edit = string.Empty;
        public static decimal dongiaNT_Edit;
        public static decimal trigiaNT_Edit;
        //END
        #endregion

        public bool isTuChoi = false;
        public ToKhaiMauDichForm()
        {
            InitializeComponent();

            SetEvent_TextBox_VanDon();
            SetEvent_TextBox_GiayPhep();
            SetEvent_TextBox_HopDong();
            SetEvent_TextBox_HoaDong();
        }

        private void loadTKMDData()
        {
            try
            {
                txtSoLuongPLTK.Text = this.TKMD.SoLuongPLTK.ToString();

                // Doanh nghiệp.
                txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
                txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;

                // Đơn vị đối tác.
                txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;

                // Đại lý TTHQ.
                txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
                txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

                //Don vi uy thac
                txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
                txtTenDonViUyThac.Text = this.TKMD.TenDonViUT;

                // Loại hình mậu dịch.
                ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;

                // Phương tiện vận tải.
                cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
                txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
                if (TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";
                // Nước.
                if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                    ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
                else
                    ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;

                // ĐKGH.
                cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

                // PTTT.
                cbPTTT.SelectedValue = this.TKMD.PTTT_ID;

                // Giấy phép.
                if (this.TKMD.SoGiayPhep.Trim().Length > 0) ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = false;
                txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
                if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToShortDateString();
                else ccNgayGiayPhep.Text = "";
                if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToShortDateString();
                else ccNgayHHGiayPhep.Text = "";
                //if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

                // Hóa đơn thương mại.
                if (this.TKMD.SoHoaDonThuongMai.Trim().Length > 0) ccNgayHDTM.ReadOnly = false;
                txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
                if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToShortDateString();
                else ccNgayHDTM.Text = "";
                //if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

                // Nguyên tệ.
                ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
                txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
                txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;

                // Hợp đồng.
                if (this.TKMD.SoHopDong.Trim().Length > 0) ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = false;
                txtSoHopDong.Text = this.TKMD.SoHopDong;
                if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Text = this.TKMD.NgayHopDong.ToShortDateString();
                else ccNgayHopDong.Text = "";
                if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToShortDateString();
                else ccNgayHHHopDong.Text = "";
                //if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
                if (this.TKMD.SoVanDon.Trim().Length > 0) ccNgayVanTaiDon.ReadOnly = false;
                if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
                else ccNgayVanTaiDon.Text = "";
                // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
                //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                // Tên chủ hàng.
                txtTenChuHang.Text = this.TKMD.TenChuHang;
                txtChucVu.Text = this.TKMD.ChucVu;

                // Container 20.
                //txtTongSoContainer.Value = this.TKMD.SoContainer20;

                // Container 40.
                //txtSoContainer40.Value = this.TKMD.SoContainer40;

                // Số kiện hàng.
                txtSoKien.Value = this.TKMD.SoKien;

                // Trọng lượng.
                txtTrongLuong.Value = this.TKMD.TrongLuong;

                // Lệ phí HQ.
                txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;

                // Phí BH.
                txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;

                // Phí VC.
                txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;

                //Phí ngân hàng
                txtPhiNganHang.Value = this.TKMD.PhiKhac;

                txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;

                if (this.TKMD.ID > 0)
                {
                    if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                        this.TKMD.LoadHMDCollection();
                    if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                        this.TKMD.LoadChungTuTKCollection();
                    this.TKMD.LoadChungTuHaiQuan();
                    if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                        this.TKMD.LoadCO();
                    #region Ân hạn thuế

                    if (TKMD.AnHanThue.IsAnHan)
                    {
                        chkAnHangThue.Checked = TKMD.AnHanThue.IsAnHan;
                        txtAnHanThue_LyDo.Text = TKMD.AnHanThue.LyDoAnHan;
                        txtAnHanThue_ThoiGian.Value = TKMD.AnHanThue.SoNgay;
                    }
                    #endregion

                    #region Đảm bảo nghĩa vụ nộp thuế
                    if (TKMD.DamBaoNghiaVuNopThue.IsDamBao)
                    {
                        chkDamBaoNopThue_isValue.Checked = TKMD.DamBaoNghiaVuNopThue.IsDamBao;
                        txtDamBaoNopThue_HinhThuc.Text = TKMD.DamBaoNghiaVuNopThue.HinhThuc;
                        txtDamBaoNopThue_TriGia.Value = TKMD.DamBaoNghiaVuNopThue.TriGiaDB;
                        ccDamBaoNopThue_NgayBatDau.Value = TKMD.DamBaoNghiaVuNopThue.NgayBatDau;
                        ccDamBaoNopThue_NgayKetThuc.Value = TKMD.DamBaoNghiaVuNopThue.NgayKetThuc;
                        ccDamBaoNopThue_NgayKetThuc.Text = ccDamBaoNopThue_NgayKetThuc.Value.ToString();
                        ccDamBaoNopThue_NgayBatDau.Text = ccDamBaoNopThue_NgayBatDau.Value.ToString();
                    }

                    #endregion

                    #region Load dữ liệu header Ngonnt 23/02
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    ccNgayTN.Value = this.TKMD.NgayTiepNhan;
                    txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                    ccNgayDK.Value = this.TKMD.NgayDangKy;
                    txtHDPL.Text = this.TKMD.HUONGDAN;
                    txtSoTiepNhan.ReadOnly = ccNgayTN.ReadOnly = txtSoToKhai.ReadOnly = ccNgayDK.ReadOnly = txtHDPL.ReadOnly = true;
                    #endregion
                }

                dgList.DataSource = this.TKMD.HMDCollection;
                dgList.Refetch();

                gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
                this.radTB.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
                this.radNPL.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
                this.radSP.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
                if (this.TKMD.LoaiHangHoa == "S")
                {
                    radSP.Checked = true;
                    radNPL.Checked = false;
                    radTB.Checked = false;
                }
                else if (this.TKMD.LoaiHangHoa == "N")
                {
                    radSP.Checked = false;
                    radNPL.Checked = true;
                    radTB.Checked = false;
                }
                else if (this.TKMD.LoaiHangHoa == "T")
                {
                    radSP.Checked = false;
                    radNPL.Checked = false;
                    radTB.Checked = true;
                }
                this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
                this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
                this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);

                //chung tu kem
                txtChungTu.Text = this.TKMD.GiayTo;
                //so to khai 
                if (this.TKMD.SoToKhai > 0)
                    txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();

                if (this.TKMD.SoTiepNhan > 0)
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();

                txtDeXuatKhac.Text = this.TKMD.DeXuatKhac;
                txtLyDoSua.Text = this.TKMD.LyDoSua;

                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait for approval";
                    }
                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa khai báo";
                    }
                    else
                    {
                        lblTrangThai.Text = " Not yet declarated";
                    }

                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không được phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = " Not yet Approved";
                    }
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Sửa tờ khai";
                    }
                    else
                    {
                        lblTrangThai.Text = " Edit ";
                    }
                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {

                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = " Approved";
                    }
                }

                if (TKMD.PhanLuong != "")
                {
                    if (TKMD.PhanLuong == "1")
                        lblTrangThai.Text = "Luồng xanh";
                    else if (TKMD.PhanLuong == "2")
                        lblTrangThai.Text = "Luồng vàng";
                    else if (TKMD.PhanLuong == "3")
                        lblTrangThai.Text = "Luồng đỏ";
                }
                else
                    lblTrangThai.Text = "Tờ khai chưa được phân luồng";

                txtTongSoContainer.Value = TKMD.SoLuongContainer != null ? TKMD.SoLuongContainer.TongSoCont() : 0;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void khoitao_DuLieuChuan()
        {
            if (GlobalSettings.IsDaiLy)
            {
                txtTenDaiLy.Text = GlobalSettings.TEN_DAI_LY;
                txtMaDaiLy.Text = GlobalSettings.MA_DAI_LY;
            }
            //Người đại diện,yy
            txtTenChuHang.Text = GlobalSettings.NguoiLienHe;
            txtChucVu.Text = GlobalSettings.ChucVu;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            // Phiph update 20/08/2013
            if (TKMD.ID == 0)
            {
                if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    if (!string.IsNullOrEmpty(GlobalSettings.LoaihinhMD_Nhap))
                        ctrLoaiHinhMauDich.Ma = GlobalSettings.LoaihinhMD_Nhap;
                    else
                        ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";
                }
                else
                {
                    if (!string.IsNullOrEmpty(GlobalSettings.LoaihinhMD_Xuat))
                        ctrLoaiHinhMauDich.Ma = GlobalSettings.LoaihinhMD_Xuat;
                    else
                        ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";
                }
            }

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            if (this.NhomLoaiHinh.StartsWith("N"))
            {
                ctrCuaKhau.Ma = GlobalSettings.DIA_DIEM_DO_HANG;
                ctrNuocXuatKhau.Ma = GlobalSettings.NUOC;
            }
            else
            {
                ctrCuaKhau.Ma = GlobalSettings.CUA_KHAU_XUAT_HANG;
                ctrNuocXuatKhau.Ma = GlobalSettings.NUOCNK;

            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            System.Data.DataTable dt = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDanhSachDoiTac(GlobalSettings.MA_DON_VI);
            foreach (System.Data.DataRow dr in dt.Rows)
                col.Add(dr["TenDonViDoiTac"].ToString());
            txtTenDonViDoiTac.AutoCompleteCustomSource = col;
            if (Company.KDT.SHARE.Components.Globals.IsDaiLy)
            {
                txtMaDaiLy.Text = GlobalSettings.MA_DAI_LY;
                //txtTenDaiLy.Text = GlobalSettings.;
            }
            //txtMaDonViUyThac.Text = GlobalSettings.MA_DON_VI;
            //txtTenDonViUyThac.Text = GlobalSettings.TEN_DON_VI;
        }

        private void khoitao_GiaoDienToKhai()
        {
            #region giao diện V4

            txtLePhiHQ.Enabled = GlobalSettings.SendV4;
            //txtTongSoContainer.Enabled = GlobalSettings.SendV4;
            uiDamBaoNghiaVuNT.Visible = GlobalSettings.SendV4;
            uiAnHanThue.Visible = GlobalSettings.SendV4;
            GiayKiemTra.Enabled = GiayKiemTra1.Enabled = GlobalSettings.SendV4 ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            // ChungTuKemTheo1.Enabled = !GlobalSettings.SendV4 ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

            #endregion
            if (this.NhomLoaiHinh.Length >= 3)
                this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);
            //if (NhomLoaiHinh != "NSX" && NhomLoaiHinh != "XSX" && NhomLoaiHinh != "NGC" && NhomLoaiHinh != "XGC")
            //    ctrDonViHaiQuan.ReadOnly = false;
            //grbHopDong.Enabled = chkHopDong.Enabled = true;
            //chkGiayPhep.Enabled = gbGiayPhep.Enabled = true;
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai nhập khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Import declaration (" + this.NhomLoaiHinh + ")";
                }
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                // Tiêu đề.

                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Export Declaration (" + this.NhomLoaiHinh + ")";
                }
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                // grbNguoiNK.Text = "Người xuất khẩu";
                // grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNguoiNK.Text = "Người xuất khẩu";
                    grbNguoiXK.Text = "Người nhập khẩu";
                }
                else
                {
                    grbNguoiNK.Text = "Importer";
                    grbNguoiXK.Text = "Exporter";
                }


                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = false;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                grbHoaDonThuongMai.Enabled = true;
                //chkVanTaiDon.Visible = 

                // Vận tải đơn.
                //Hungtq update 07/02/2012. Hien thi Van don, Hoa don.
                //grbVanTaiDon.Enabled = true;
                grbVanTaiDon.Enabled = false;
                // Nước XK.
                //grbNuocXK.Text = "Nước nhập khẩu";
                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                //grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                //chkDiaDiemXepHang.Enabled = 
                grbDiaDiemXepHang.Enabled = false;
                // chkDiaDiemXepHang.Visible = false;
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNuocXK.Text = "Nước nhập khẩu";
                    grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                }
                else
                {
                    grbNuocXK.Text = "Import country";
                    grbDiaDiemDoHang.Text = "Export store";

                }
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;

            }
            grpLoaiHangHoa.Visible = false;
            // Loại hình gia công.
            if (this.NhomLoaiHinh == "NSX" || this.NhomLoaiHinh == "XSX")
            {
                if (this.NhomLoaiHinh == "NSX")
                {
                    grpLoaiHangHoa.Visible = false;
                    if (TKMD.ID == 0)
                    {
                        radNPL.Checked = true;
                        radSP.Checked = false;
                        radTB.Checked = false;
                    }

                }
                else
                {
                    radTB.Visible = false;
                    grpLoaiHangHoa.Visible = true;
                }
            }
            else
                grpLoaiHangHoa.Visible = false;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                if (this.NhomLoaiHinh.Length > 1) this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 1);
                grpLoaiHangHoa.Visible = true;
                radTB.Visible = true;
                radNPL.Visible = true;
                radSP.Visible = true;
                //radNPL.Checked = true;
                //radSP.Checked = true;
                //radTB.Checked = true;

            }
        }

        //-----------------------------------------------------------------------------------------

        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            try
            {


                lblTongTGNT.Text = "0 (USD)";
                lblTongThue.Text = "0 (VND)";
                lblTongSoHang.Text = "0";
                if (TKMD.ID == 0 || TKMD.SoToKhai == 0) TKMD.PhanLuong = "";
                //InitialProgess();

                if (this.DesignMode)
                    return;

                // Người nhập khẩu / Người xuất khẩu.
                txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
                txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
                ccNgayDen.Value = DateTime.Today;

                this.khoitao_DuLieuChuan();
                switch (this.OpenType)
                {
                    case OpenFormType.View:
                        this.loadTKMDData();
                        //this.ViewTKMD();
                        this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                        break;
                    case OpenFormType.Edit:
                        this.loadTKMDData();
                        this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                        break;
                    case OpenFormType.Insert:
                        txtTyGiaUSD.Value = Company.KDT.SHARE.Components.Globals.GetNguyenTe("USD");//HungTQ Update 13/12/2010.
                        break;
                }

                this.khoitao_GiaoDienToKhai();
                SetCommandStatus();
                if (TKMD.ID > 0)
                {
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    sendXML.master_id = TKMD.ID;
                    if (sendXML.Load())
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đang chờ xác nhận hải quan";
                        }
                        else
                        {
                            lblTrangThai.Text = "Wait for approval";
                        }
                        NhanDuLieu.Enabled = NhanDuLieu2.Enabled = XacNhan2.Enabled = XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdSend.Enabled = cmdSend2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }

                    tinhTongTriGiaKhaiBao();
                }
                if (GlobalSettings.TuDongTinhThue == "0")
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)) && !MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        TopRebar1.Visible = false;
                    }
                }
                #region Khai thu cong
                if (Company.KDT.SHARE.Components.Globals.IsKTX)
                {
                    lblPhanLuong.Visible = false;
                    cbbPhanLuong.Visible = true;
                    cbbPhanLuong.SelectedValue = TKMD.PhanLuong == string.Empty ? "0" : TKMD.PhanLuong;
                    txtSoTKGiay.Text = TKMD.ChiTietDonViDoiTac;
                    clcNgayDuaVaoTK.Text = TKMD.NgayGiayPhep.ToShortDateString();
                    cmdSendTK.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                else
                {
                    lblPhanLuong.Visible = true;
                    cbbPhanLuong.Visible = false;
                    cmdSendTK.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    //cbbPhanLuong.SelectedValue = TKMD.PhanLuong;
                }
                #endregion
                //AVN 0707
                //cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdChungTuDangAnh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                //cmdChungTuDangAnhBS.Visible = Janus.Windows.UI.InheritableBoolean.False;

                //TODO: Hungtq updated 6/2/2013. Form_Load()
                if (TKMD.ID == 0) //La to khai moi
                {
                    if (Company.KDT.SHARE.Components.Globals.IsKTX)
                        tkmdVersionControlV1.Version = 1;
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                        tkmdVersionControlV1.Version = 2;
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && !GlobalSettings.SendV4)
                        tkmdVersionControlV1.Version = 3;
                    else if (GlobalSettings.SendV4) //Mac dinh V4
                        tkmdVersionControlV1.Version = 4;
                    else
                        tkmdVersionControlV1.Version = 0;

                    tkmdVersionControlV1.IsCKS = (Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature);
                    tkmdVersionControlV1.LoadInfo(TKMD.ID);
                  
                }
                else
                    tkmdVersionControlV1.LoadInfo(TKMD.ID);
             
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //ctrDonViHaiQuan.ReadOnly = true;

            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;

            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;
            txtChucVu.ReadOnly = true;

            // Container 20.
            txtTongSoContainer.ReadOnly = true;

            // Container 40.
            //txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            //grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            //gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            //grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            // grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            // grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void ReadExcel()
        {
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            //ReadExcelForm reform = new ReadExcelForm();
            ReadExcelForm2 reform = new ReadExcelForm2();
            reform.TiGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            reform.hmdCollection = this.TKMD.HMDCollection;
            reform.TKMD = this.TKMD;
            if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
            reform.ShowDialog(this);
            this.TKMD.HMDCollection = reform.hmdCollection;
            if (GlobalSettings.TuDongTinhThue == "1")
            {
                tinhLaiThue();
            }
            else
            {
                tinhTongTriGiaKhaiBao();
            }
            try
            {
                dgList.DataSource = this.TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            SetCommandStatus();
        }

        //-----------------------------------------------------------------------------------------
        private void themHang()
        {

            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            HangMauDichForm f = new HangMauDichForm();
            f.OpenType = OpenFormType.Edit;
            f.TKMD = this.TKMD;
            f.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

            f.NhomLoaiHinh = this.NhomLoaiHinh;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                f.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 1);
            if (radNPL.Checked) f.LoaiHangHoa = "N";
            if (radSP.Checked) f.LoaiHangHoa = "S";
            if (radTB.Checked) f.LoaiHangHoa = "T";
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //f.HMDCollection = this.TKMD.HMDCollection;
            f.MaNguyenTe = ctrNguyenTe.Ma;
            f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            //this.TKMD.HMDCollection = f.HMDCollection;
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            if (GlobalSettings.TuDongTinhThue == "1")
            {
                tinhLaiThue();
            }
            else
            {
                tinhTongTriGiaKhaiBao();
            }
            SetCommandStatus();
            this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);
        }

        //-----------------------------------------------------------------------------------------

        private Company.BLL.KDT.HangMauDich FindHangMauDichBy(List<Company.BLL.KDT.HangMauDich> list, long idTKMD, string maPhu)
        {
            foreach (Company.BLL.KDT.HangMauDich item in list)
            {
                if (item.TKMD_ID == idTKMD && item.MaPhu == maPhu)
                {
                    return item;
                }
            }

            return null;
        }

        public static List<Company.BLL.KDT.HangMauDich> deleteHMDCollection = new List<Company.BLL.KDT.HangMauDich>();
        public static List<Company.BLL.KDT.HangMauDich> insertHMDCollection = new List<Company.BLL.KDT.HangMauDich>();
        private bool AutoInsertHMDToTKEdit(string _maHS_Old, string _tenHang_Old, string _maHang_Old, string _xuatXu_Old, decimal _soLuong_Old, string _dvt_Old, decimal _donGiaNT_Old, decimal _triGiaNT_Old, ToKhaiMauDich toKhaiMauDich)
        {
            try
            {
                //Company.KD.BLL.KDT.HangMauDich HMD_Edit = new Company.KD.BLL.KDT.HangMauDich();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail noiDungEditDetail = new Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail();
                HangMauDichEditForm hmdEditForm = new HangMauDichEditForm();
                if (toKhaiMauDich.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    #region 21. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "21. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHS_Edit = _maHS_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHS_Edit = _maHS_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "20. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            tenHang_Edit = _tenHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                tenHang_Edit = _tenHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "20. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHang_Edit = _maHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHang_Edit = _maHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 22. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "22. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                            xuatXu_Edit = _xuatXu_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                                xuatXu_Edit = _xuatXu_Old;
                            }
                        }
                    }
                    #endregion

                    #region 23. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "23. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            soLuong_Edit = _soLuong_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                soLuong_Edit = _soLuong_Old;
                            }
                        }
                    }
                    #endregion

                    #region 24. Đơn vị tính
                    if (dvt_Edit.Trim() != _dvt_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "24. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                            dvt_Edit = _dvt_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                                dvt_Edit = _dvt_Old;
                            }
                        }
                    }
                    #endregion

                    #region 25. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "25. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            dongiaNT_Edit = _donGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                dongiaNT_Edit = _donGiaNT_Old;
                            }
                        }
                    }
                    #endregion

                    #region 26. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "26. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            trigiaNT_Edit = _triGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                trigiaNT_Edit = _triGiaNT_Old;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 18. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "18. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "17. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "17. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 19. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "19. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "20. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 21. Đơn vị tính
                    if (dvt_Edit.Trim() != _dvt_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "21. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "22. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "23. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception e)
            {
                ShowMessage("Có lỗi trong quá trình ghi dữ liệu vào tờ khai sửa đổi, bổ sung.\r\nChi tiết lỗi: " + e.Message, false);
                return false;
            }
        }

        //DATLMQ bổ sung AutoInsertAddHMD ngày 12/08/2011
        private bool checkHMDEsistsInTKMD(long _TKMD_ID, string _maHang)
        {
            //Company.BLL.KDT.HangMauDich hmd = new Company.BLL.KDT.HangMauDich();
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _maHang);
            List<Company.BLL.KDT.HangMauDich> hmdColl = (List<Company.BLL.KDT.HangMauDich>)Company.BLL.KDT.HangMauDich.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }

        private void AutoInsertAddHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            foreach (Company.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
            {
                if (!checkHMDEsistsInTKMD(TKMD.ID, hangMD.MaPhu))
                {
                    insertHMDCollection.Add(hangMD);
                    listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                    if (listNoiDungTK.Count == 0)
                    {
                        noiDungTK.Ma = "MaHang";
                        noiDungTK.Ten = "20. Mã hàng";
                        noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                        noiDungTK.InsertUpdate();

                        foreach (Company.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                    else
                    {
                        foreach (Company.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                }
            }
        }

        //DATLMQ bổ sung AutoInsertDeleteHMD ngày 12/08/2011
        private void AutoInsertDeleteHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            if (HangMauDichForm.isDeleted)
            {
                listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                if (listNoiDungTK.Count == 0)
                {
                    noiDungTK.Ma = "MaHang";
                    noiDungTK.Ten = "20. Mã hàng";
                    noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungTK.InsertUpdate();

                    foreach (Company.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin " + noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
                else
                {
                    foreach (Company.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = "Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
            }
        }

        /// <summary>
        /// Lưu thông tin tờ khai.
        /// </summary>
        public void Save()
        {
            bool valid = false;

            bool isKhaibaoSua = this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                //Neu to khai da luu thi kiem tra chung tu Van tai don
                if (TKMD != null)
                {
                    if (!TKMD.MaLoaiHinh.Contains("V"))
                    {
                        valid = ValidateControl.ValidateNull(txtSoVanTaiDon, epError, "Số vận đơn");
                        if (!valid)
                        {
                            txtSoVanTaiDon.Focus();
                            ShowMessage("Bạn chưa nhập thông tin 'Vận tải đơn' trên tờ khai chính", false);
                            return;
                        }

                        valid = ValidateControl.ValidateNull(txtSoHieuPTVT, epError, "Số hiệu phương tiện vận tải");
                        if (!valid)
                        {
                            txtSoVanTaiDon.Focus();
                            ShowMessage("Bạn chưa nhập thông tin 'Số hiệu phương tiện vận tải' trên tờ khai chính", false);
                            return;
                        }

                        valid = ValidateControl.ValidateNull(ccNgayDen, epError, "Ngày đến phương tiện vận tải");
                        if (!valid)
                        {
                            txtSoVanTaiDon.Focus();
                            ShowMessage("Bạn chưa nhập thông tin 'Ngày đến phương tiện vận tải' trên tờ khai chính", false);
                            return;
                        }
                    }
                }
            }
            else
            {
                valid = ValidateControl.ValidateNull(txtSoHopDong, epError, "Số hợp đồng");
                valid |= ValidateControl.ValidateNull(txtSoGiayPhep, epError, "Số giấy phép");
                if (valid) epError.Clear();
            }

            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                bool valid1 = true;//temp
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;

            }
            valid &= ValidateControl.ValidateNull(txtTenChuHang, epError, "Đại diện doanh nghiệp");

            #region Kiem tra Ân hạng thuế / Đảm bảo nghĩa vụ nộp thuế

            if (chkDamBaoNopThue_isValue.Checked)
            {
                valid &= ValidateControl.ValidateNull(ccDamBaoNopThue_NgayBatDau, epError, "Ngày bắt đầu được đảm bảo phải nhập");
                valid &= ValidateControl.ValidateNull(ccDamBaoNopThue_NgayKetThuc, epError, "Ngày kết thúc được đảm bảo phải nhập");
            }
            #endregion

            if (valid)
            {
                #region Cap nhat to khai

                #region Ân hạng thuế / Đảm bảo nghĩa vụ nộp thuế
                TKMD.AnHanThue.IsAnHan = chkAnHangThue.Checked;
                if (chkAnHangThue.Checked)
                {
                    TKMD.AnHanThue.LyDoAnHan = txtAnHanThue_LyDo.Text.Trim();
                    TKMD.AnHanThue.SoNgay = Convert.ToInt32(txtAnHanThue_ThoiGian.Value);
                }
                TKMD.DamBaoNghiaVuNopThue.IsDamBao = chkDamBaoNopThue_isValue.Checked;
                if (chkDamBaoNopThue_isValue.Checked)
                {
                    TKMD.DamBaoNghiaVuNopThue.HinhThuc = txtDamBaoNopThue_HinhThuc.Text.Trim();
                    TKMD.DamBaoNghiaVuNopThue.NgayBatDau = ccDamBaoNopThue_NgayBatDau.Value;
                    TKMD.DamBaoNghiaVuNopThue.NgayKetThuc = ccDamBaoNopThue_NgayKetThuc.Value;
                    TKMD.DamBaoNghiaVuNopThue.TriGiaDB = Convert.ToDecimal(txtDamBaoNopThue_TriGia.Value);
                }
                #endregion

                if (this.TKMD.HMDCollection.Count == 0)
                {
                    // Message("MSG_SAV01", "", false);
                    //  ShowMessage("Chưa nhập thông tin hàng của tờ khai", false);
                    MLMessages("Chưa nhập thông tin hàng của tờ khai", "MSG_SAV01", "", false);
                    return;
                }

                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, "Tỷ giá tính thuế không hợp lệ.");
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                //this.TKMD.SoTiepNhan = this.TKMD.SoTiepNhan;
                //this.TKMD.SoToKhai = this.TKMD.SoToKhai;
                //this.TKMD.ActionStatus = 0;

                //Set chổ này lưu lung tung dữ liệu
                //LanNT Comment
                #region Khong cho phep cap nhat trang thai
                //if (!isKhaibaoSua)
                //{
                //    if (this.TKMD.SoTiepNhan > 0)
                //    {
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                //        this.TKMD.ActionStatus = 0;
                //    }
                //    else
                //    {
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                //        this.TKMD.ActionStatus = -1;
                //        this.TKMD.NgayTiepNhan = new DateTime(1900, 1, 1);
                //    }

                //    if (this.TKMD.SoToKhai > 0)
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                //    else
                //    {
                //        this.TKMD.PhanLuong = "";
                //        this.TKMD.HUONGDAN = "";
                //        this.TKMD.NgayDangKy = new DateTime(1900, 1, 1);
                //    }
                //}
                #endregion
                // Đề xuất khác
                this.TKMD.DeXuatKhac = txtDeXuatKhac.Text;
                //Lý do sửa
                if (isKhaibaoSua)
                    this.TKMD.LyDoSua = txtLyDoSua.Text;

                this.TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);

                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;

                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Loại hình mậu dịch.
                this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
                // Thong tin to khai dua vao thanh khaon
                TKMD.ChiTietDonViDoiTac = txtSoTKGiay.Text;// luu so to khai giấy
                // TKMD.Ngay_THN_THX = clcNgayDuaVaoTK.Value;// Luu ngay dua vao thanh khoan
                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    if (!ccNgayGiayPhep.IsNullDate)
                        this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    else
                        this.TKMD.NgayGiayPhep = new DateTime(1900, 1, 1);
                    if (!ccNgayHHGiayPhep.IsNullDate)
                        this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                    else
                        this.TKMD.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = new DateTime(1900, 1, 1);
                    this.TKMD.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = new DateTime(1900, 1, 1);
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = new DateTime(1900, 1, 1);
                    this.TKMD.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                }
                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                if (ccNgayDen.IsNullDate)
                    this.TKMD.NgayDenPTVT = new DateTime(1900, 1, 1);
                else
                    this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = new DateTime(1900, 1, 1);
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = new DateTime(1900, 1, 1);
                }
                // Nước.
                if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.ChucVu = txtChucVu.Text;
                //                 this.TKMD.SoContainer20 = Convert.ToDecimal(txtTongSoContainer.Text);
                //                 this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                this.TKMD.TongTriGiaKhaiBao = 0;
                //
                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);

                this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                //this.tkmd.NgayGui = new DateTime(1900, 1, 1);
                if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
                if (GlobalSettings.TuDongTinhThue == "1")
                    this.tinhLaiThue();
                else
                    this.tinhTongTriGiaKhaiBao();

                this.TKMD.GiayTo = txtChungTu.Text;

                try
                {
                    if (this.TKMD.ID == 0)
                        this.TKMD.GUIDSTR = Guid.NewGuid().ToString();
                    #region Kiem tra khai bao sua to khai

                    if (isKhaibaoSua)
                    {
                        if (txtLyDoSua.Text.Trim() == "")
                        {
                            MLMessages("Chưa nhập lý do sửa tờ khai", "MSG_SAV06", "", false);
                            return;
                        }
                        try
                        {
                            if (!TKMD.MaLoaiHinh.Contains("V"))
                            {
                                List<Company.KDT.SHARE.Components.Message> tokhaiGocTruocKhiSuas = Company.KDT.SHARE.Components.Message.LayToKhaiGocTruocKhiSua_ItemID(TKMD.ID);

                                if (tokhaiGocTruocKhiSuas.Count == 0)
                                {
                                    MLMessages("*** LƯU Ý ***:\r\nKhông tự động tạo được nội dung thông tin thay đổi mới cho tờ khai sửa này, do chưa lưu thông tin tờ khai gốc trước khi sửa tờ khai.", "MSG_SAV06", "", false);
                                }
                                else
                                {
                                    NoiDungToKhai noiDungTK = new NoiDungToKhai();
                                    List<NoiDungToKhai> listNoiDungTK = new List<NoiDungToKhai>();
                                    NoiDungDieuChinhTKDetail noiDungEditDetail = new NoiDungDieuChinhTKDetail();
                                    NoiDungDieuChinhTK noiDungEdit = new NoiDungDieuChinhTK();
                                    noiDungEdit.TKMD_ID = TKMD.ID;
                                    noiDungEdit.SoTK = TKMD.SoToKhai;
                                    noiDungEdit.NgayDK = TKMD.NgayDangKy;
                                    noiDungEdit.MaLoaiHinh = TKMD.MaLoaiHinh;
                                    noiDungEdit.NgaySua = DateTime.Now;
                                    i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
                                    if (i == 0)
                                    {
                                        i++;
                                        noiDungEdit.SoDieuChinh = i;
                                        noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                        noiDungEdit.InsertUpdate();
                                        luu = true;
                                    }
                                    else
                                    {
                                        if (luu == false)
                                        {
                                            i++;
                                            noiDungEdit.SoDieuChinh = i;
                                            noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                            noiDungEdit.InsertUpdate();
                                            luu = true;
                                        }
                                        else
                                        {
                                            //Xoa thong tin cu cua lan dieu chinh
                                            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
                                            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                            //foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
                                            //{
                                            //    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && ndDieuChinhTK.SoDieuChinh == i)
                                            //    {
                                            //        Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(ndDieuChinhTK.ID);
                                            //    }
                                            //}
                                        }
                                    }


                                    //Lay thong tin to khai goc da luu truoc khi sua
                                    ToKhaiMauDich TKMD_Goc = Helpers.Deserialize<ToKhaiMauDich>(tokhaiGocTruocKhiSuas[0].MessageContent);

                                    //So sanh noi dung cua to khai goc va to khai sua
                                    bool ok = SoSanhNoiDungSuaDoiToKhai(TKMD_Goc, TKMD);

                                    if (!ok)
                                    {
                                        ShowMessage("Lỗi trong quá trình lưu tờ khai sửa.", false);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi trong quá trình lưu tờ khai sửa: " + ex.Message, false);
                        }
                    }

                    #endregion

                    #region Kiêm tra thông tin trùng tờ khai
                    try
                    {
                        decimal SoLuongHang = 0;
                        foreach (Company.BLL.KDT.HangMauDich item in TKMD.HMDCollection)
                        {
                            SoLuongHang = SoLuongHang + item.SoLuong;
                        }
                        long IDTrung = TrungToKhai.CheckToKhaiTrung(TKMD.ID, TKMD.MaLoaiHinh, TKMD.SoGiayPhep, TKMD.SoVanDon, TKMD.SoHoaDonThuongMai, TKMD.SoHopDong, TKMD.SoKien, TKMD.TrongLuong, SoLuongHang, TKMD.HMDCollection.Count);
                        if (IDTrung > 0)
                        {
                            TKMD.QuanLyMay = true;
                            if (ShowMessage("Tờ khai này trùng thông tin với tờ khai có ID = " + IDTrung + ". Bạn có muốn lưu lại ?", true) != "Yes")
                                return;

                        }
                        else
                            TKMD.QuanLyMay = false;
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        if (ShowMessage("Lỗi kiểm tra trùng tờ khai : " + ex + ". Bạn có muốn tiếp tục lưu tờ khai ?", true) != "Yes")
                            return;
                    }


                    #endregion
                    #region Cap nhat trang thai sua tơ khai VNACCS
                    if (TKMD.MaLoaiHinh.Contains("V"))
                    {
                        if (TKMD.LoaiVanDon.Length != 12)
                        {
                            TKMD.TrangThaiXuLy = -1;
                            TKMD.ActionStatus = 5000;
                        }
                        else
                        {
                            if (TKMD.CapNhatThongTinHangToKhaiSua())
                            {
                                string trangThaiCu = "Sửa tờ khai";//Company.KDT.SHARE.Components.Globals.GetTrangThaiXuLyTK(obj.TrangThaiXuLy);

                                TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                TKMD.ActionStatus = 5000;
                                Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                                if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                                    msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;

                                Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, TKMD.ID, TKMD.GUIDSTR,
                                    msgType,
                                    Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                                    Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay + string.Format(" từ trạng thái {0} -> {1} để cập nhật lại dữ liệu tờ khai sửa", trangThaiCu, Company.KDT.SHARE.Components.Globals.GetTrangThaiXuLyTK(TKMD.TrangThaiXuLy)),
                                    string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", TKMD.SoToKhai, TKMD.NgayDangKy.ToShortDateString(), TKMD.MaLoaiHinh.Trim(), TKMD.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKMD.PhanLuong)));

                                // ShowMessage("Thực hiện cập nhật dữ liệu tờ khai sửa, chuyển trạng thái thành công.", false, false, "");
                            }
                            else
                            {
                                ShowMessage("Lỗi thực hiện cập nhật dữ liệu tờ khai sửa.", false, false, "");
                                return;
                            }
                        }
                    }
                    #endregion
                    #region lưu phanluong cho khai tơ khai thu công
                    if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    {
                        if (cbbPhanLuong.SelectedValue.ToString() == "1" || cbbPhanLuong.SelectedValue.ToString() == "2" || cbbPhanLuong.SelectedValue.ToString() == "3")
                            TKMD.PhanLuong = cbbPhanLuong.SelectedValue.ToString();
                        else
                            TKMD.PhanLuong = "";
                        TKMD.ChiTietDonViDoiTac = txtSoTKGiay.Text;// luu so to khai giấy
                        TKMD.NgayGiayPhep = clcNgayDuaVaoTK.Value;// Luu ngay dua vao thanh khoan
                    }

                    #endregion

                    this.TKMD.InsertUpdateFull();

                    //Neu la chinh sua, cap nhat lai ID luu tam la ID cua TKMD dang mo: tranh bi copy trung du lieu cua TK cu.
                    if (_bNew == false)
                        pTKMD_ID = TKMD.ID;

                    #region COPY TO KHAI
                    //Lypt update date 22/01/2010
                    if (_bNew)
                    {
                        //Cap nhat lai trang thai sau khi them moi
                        _bNew = false;

                        //Copy Van don                        
                        List<VanDon> VanDoncollection = new List<VanDon>();
                        VanDoncollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (VanDon vd in VanDoncollection)
                        {
                            long vdResult = VanDon.InsertVanDon(vd.SoVanDon, vd.NgayVanDon, vd.HangRoi, vd.SoHieuPTVT, vd.NgayDenPTVT, vd.MaHangVT, vd.TenHangVT, vd.TenPTVT, vd.QuocTichPTVT, vd.NuocXuat_ID, vd.MaNguoiNhanHang,
                                vd.TenNguoiNhanHang, vd.MaNguoiGiaoHang, vd.TenNguoiNhanHang, vd.CuaKhauNhap_ID, vd.CuaKhauXuat, vd.MaNguoiNhanHangTrungGian, vd.TenNguoiNhanHangTrungGian, vd.MaCangXepHang, vd.TenCangXepHang,
                                vd.MaCangDoHang, vd.TenCangDoHang, this.TKMD.ID, vd.DKGH_ID, vd.DiaDiemGiaoHang, vd.NoiDi, vd.SoHieuChuyenDi, vd.NgayKhoiHanh, vd.TongSoKien, vd.LoaiKien, vd.SoHieuKien, vd.DiaDiemChuyenTai, vd.LoaiVanDon, vd.MoTaKhac, vd.ID_NuocPhatHanh);
                            List<Container> ContColl = (List<Container>)Company.KDT.SHARE.QuanLyChungTu.Container.SelectCollectionBy_VanDon_ID(vd.ID);
                            for (int v = 0; v < ContColl.Count; v++)
                            {
                                ContColl[v].VanDon_ID = vdResult;
                            }
                            Company.KDT.SHARE.QuanLyChungTu.Container.InsertCollection(ContColl);
                        }
                        //copy Hop dong
                        List<HopDongThuongMai> HopDongCollection = new List<HopDongThuongMai>();
                        HopDongCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (HopDongThuongMai hd in HopDongCollection)
                        {
                            //Copy hop dong
                            long hdResult = HopDongThuongMai.InsertHopDongThuongMai(hd.SoHopDongTM, hd.NgayHopDongTM, hd.ThoiHanThanhToan, hd.NguyenTe_ID, hd.PTTT_ID, hd.DKGH_ID, hd.DiaDiemGiaoHang, hd.MaDonViMua, hd.TenDonViMua,
                                hd.MaDonViBan, hd.TenDonViBan, hd.TongTriGia, hd.ThongTinKhac, this.TKMD.ID, hd.GuidStr, hd.LoaiKB, hd.SoTiepNhan, hd.NgayTiepNhan, hd.TrangThai, hd.NamTiepNhan, hd.MaDoanhNghiep);

                            //---Hang hop dong   
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hop dong
                            List<HopDongThuongMaiDetail> HopDongDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hd.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HopDongThuongMaiDetail> HopDongDTMColl = new List<HopDongThuongMaiDetail>();

                            for (int l = 0; l < HopDongDetailUpdateCollection.Count; l++)
                            {
                                HopDongThuongMaiDetail hopDongCopy = HopDongDetailUpdateCollection[l];

                                if (hopDongCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hopDongCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HopDongThuongMaiDetail hdtmDetail = new HopDongThuongMaiDetail();
                                    hdtmDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdtmDetail.HopDongTM_ID = hdResult;

                                    hdtmDetail.SoThuTuHang = hopDongCopy.SoThuTuHang;
                                    hdtmDetail.MaHS = hopDongCopy.MaHS;
                                    hdtmDetail.MaPhu = hopDongCopy.MaPhu;
                                    hdtmDetail.TenHang = hopDongCopy.TenHang;
                                    hdtmDetail.NuocXX_ID = hopDongCopy.NuocXX_ID;
                                    hdtmDetail.DVT_ID = hopDongCopy.DVT_ID;
                                    hdtmDetail.SoLuong = hopDongCopy.SoLuong;
                                    hdtmDetail.DonGiaKB = hopDongCopy.DonGiaKB;
                                    hdtmDetail.TriGiaKB = hopDongCopy.TriGiaKB;

                                    HopDongDTMColl.Add(hdtmDetail);
                                }
                            }

                            HopDongThuongMaiDetail.InsertCollection(HopDongDTMColl);
                        }

                        //copy Giay phep
                        List<GiayPhep> GPColl = new List<GiayPhep>();
                        GPColl = (List<GiayPhep>)GiayPhep.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (GiayPhep gp in GPColl)
                        {
                            long gpResult = GiayPhep.InsertGiayPhep(gp.SoGiayPhep, gp.NgayGiayPhep, gp.NgayHetHan, gp.NguoiCap, gp.NoiCap, gp.MaDonViDuocCap, gp.TenDonViDuocCap, gp.MaCoQuanCap, gp.TenQuanCap, gp.ThongTinKhac, gp.MaDoanhNghiep,
                                this.TKMD.ID, gp.GuidStr, gp.LoaiKB, gp.SoTiepNhan, gp.NgayTiepNhan, gp.TrangThai, gp.NamTiepNhan, gp.LoaiGiayPhep, gp.HinhThucTruLui);

                            //--Hang Giay phep
                            //Lay hang mau dich co cap nhat thong tin rieng cua Giay phep
                            List<HangGiayPhepDetail> GiayPhepDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HangGiayPhepDetail> HangGP = new List<HangGiayPhepDetail>();

                            for (int k = 0; k < GiayPhepDetailUpdateCollection.Count; k++)
                            {
                                HangGiayPhepDetail giayPhepCopy = GiayPhepDetailUpdateCollection[k];

                                if (giayPhepCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, giayPhepCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HangGiayPhepDetail gpDetail = new HangGiayPhepDetail();
                                    gpDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    gpDetail.GiayPhep_ID = gpResult;

                                    gpDetail.SoThuTuHang = giayPhepCopy.SoThuTuHang;
                                    gpDetail.MaHS = giayPhepCopy.MaHS;
                                    gpDetail.MaPhu = giayPhepCopy.MaPhu;
                                    gpDetail.TenHang = giayPhepCopy.TenHang;
                                    gpDetail.NuocXX_ID = giayPhepCopy.NuocXX_ID;
                                    gpDetail.DVT_ID = giayPhepCopy.DVT_ID;
                                    gpDetail.SoLuong = giayPhepCopy.SoLuong;
                                    gpDetail.DonGiaKB = giayPhepCopy.DonGiaKB;
                                    gpDetail.TriGiaKB = giayPhepCopy.TriGiaKB;

                                    gpDetail.MaChuyenNganh = giayPhepCopy.MaChuyenNganh;
                                    gpDetail.MaNguyenTe = giayPhepCopy.MaNguyenTe;

                                    HangGP.Add(gpDetail);
                                }
                            }
                            HangGiayPhepDetail.InsertCollection(HangGP);
                        }

                        // copy Hoa don thuong mai
                        List<HoaDonThuongMai> HDTMColl = new List<HoaDonThuongMai>();
                        HDTMColl = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (HoaDonThuongMai hdtm in HDTMColl)
                        {
                            long hdonResult = HoaDonThuongMai.InsertHoaDonThuongMai(hdtm.SoHoaDon, hdtm.NgayHoaDon, hdtm.NguyenTe_ID, hdtm.PTTT_ID, hdtm.DKGH_ID, hdtm.MaDonViMua, hdtm.TenDonViMua, hdtm.MaDonViBan, hdtm.TenDonViBan,
                                hdtm.ThongTinKhac, this.TKMD.ID, hdtm.GuidStr, hdtm.LoaiKB, hdtm.SoTiepNhan, hdtm.NgayTiepNhan, hdtm.TrangThai, hdtm.NamTiepNhan, hdtm.MaDoanhNghiep);

                            //--Hang hoa don thuong mai
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hoa don
                            List<HoaDonThuongMaiDetail> HoaDonDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hdtm.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HoaDonThuongMaiDetail> HoaDonTMColl = new List<HoaDonThuongMaiDetail>();

                            for (int l = 0; l < HoaDonDetailUpdateCollection.Count; l++)
                            {
                                HoaDonThuongMaiDetail hoaDonCopy = HoaDonDetailUpdateCollection[l];

                                if (hoaDonCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hoaDonCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HoaDonThuongMaiDetail hdonDetail = new HoaDonThuongMaiDetail();
                                    hdonDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdonDetail.HoaDonTM_ID = hdonResult;

                                    hdonDetail.SoThuTuHang = hoaDonCopy.SoThuTuHang;
                                    hdonDetail.MaHS = hoaDonCopy.MaHS;
                                    hdonDetail.MaPhu = hoaDonCopy.MaPhu;
                                    hdonDetail.TenHang = hoaDonCopy.TenHang;
                                    hdonDetail.NuocXX_ID = hoaDonCopy.NuocXX_ID;
                                    hdonDetail.DVT_ID = hoaDonCopy.DVT_ID;
                                    hdonDetail.SoLuong = hoaDonCopy.SoLuong;
                                    hdonDetail.DonGiaKB = hoaDonCopy.DonGiaKB;
                                    hdonDetail.TriGiaKB = hoaDonCopy.TriGiaKB;

                                    HoaDonTMColl.Add(hdonDetail);
                                }
                            }
                            HoaDonThuongMaiDetail.InsertCollection(HoaDonTMColl);
                        }                        // copy CO
                        List<CO> COColl = new List<CO>();
                        COColl = (List<CO>)Company.KDT.SHARE.QuanLyChungTu.CO.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (CO co in COColl)
                        {
                            Company.KDT.SHARE.QuanLyChungTu.CO.InsertCO(co.SoCO, co.NgayCO, co.ToChucCap, co.NuocCapCO, co.MaNuocXKTrenCO, co.MaNuocNKTrenCO, co.TenDiaChiNguoiXK, co.TenDiaChiNguoiNK, co.LoaiCO,
                                co.ThongTinMoTaChiTiet, co.MaDoanhNghiep, co.NguoiKy, co.NoCo, co.ThoiHanNop, this.TKMD.ID, co.GuidStr, co.LoaiKB, co.SoTiepNhan, co.NgayTiepNhan, co.TrangThai, co.NamTiepNhan, co.NgayHetHan, co.NgayKhoiHanh, co.CangXepHang, co.CangDoHang, co.MaNguoiXK, co.MaNguoiNK, co.HamLuongXuatXu, co.MoTaHangHoa);

                            //Them CO bo sung
                            Company.KDT.SHARE.Components.Common.COBoSung coBS = Company.KDT.SHARE.QuanLyChungTu.CO.LoadCoBoSung(co.ID);
                            if (coBS == null)
                            {
                                Company.KDT.SHARE.Components.Common.COBoSung.InsertCOBoSung(co.ID, co.TenCangXepHang, co.TenCangDoHang);
                            }
                            else
                            {
                                Company.KDT.SHARE.Components.Common.COBoSung.UpdateCOBoSung(coBS.ID, co.ID, co.TenCangXepHang, co.TenCangDoHang);
                            }
                        }
                        //Copy De nghi chuyen cua khau
                        List<DeNghiChuyenCuaKhau> chuyenCK = new List<DeNghiChuyenCuaKhau>();
                        chuyenCK = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (DeNghiChuyenCuaKhau ck in chuyenCK)
                        {
                            DeNghiChuyenCuaKhau.InsertDeNghiChuyenCuaKhau(ck.ThongTinKhac, ck.MaDoanhNghiep, this.TKMD.ID, ck.GuidStr, ck.LoaiKB, ck.SoTiepNhan, ck.NgayTiepNhan, ck.TrangThai,
                                ck.NamTiepNhan, ck.SoVanDon, ck.NgayVanDon, ck.ThoiGianDen, ck.DiaDiemKiemTra, ck.TuyenDuong, ck.PTVT_ID, ck.DiaDiemKiemTraCucHQThanhPho);
                        }

                        //Copy Chung tu kem
                        List<ChungTuKem> chungTuKem = new List<ChungTuKem>();
                        chungTuKem = ChungTuKem.SelectCollectionBy_TKMDID(pTKMD_ID);
                        long ctkID = 0;
                        foreach (ChungTuKem ctk in chungTuKem)
                        {
                            ctkID = ChungTuKem.InsertChungTuKem(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.DIENGIAI, ctk.LoaiKB, ctk.TrangThaiXuLy, ctk.Tempt, ctk.MessageID, ctk.GUIDSTR, ctk.KDT_WAITING, ctk.KDT_LASTINFO, ctk.SOTN, ctk.NGAYTN, ctk.TotalSize, ctk.Phanluong, ctk.HuongDan, this.TKMD.ID);

                            ctk.LoadListCTChiTiet();
                            foreach (ChungTuKemChiTiet chiTiet in ctk.listCTChiTiet)
                            {
                                ChungTuKemChiTiet.InsertChungTuKemChiTiet(ctkID, chiTiet.FileName, chiTiet.FileSize, chiTiet.NoiDung);
                            }
                        }

                        if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                            this.TKMD.LoadHMDCollection();
                        if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                            this.TKMD.LoadChungTuTKCollection();
                        this.TKMD.LoadChungTuHaiQuan();
                        if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                            this.TKMD.LoadCO();


                    }
                    //Het Lypt Update
                    #endregion

                    this.TKMD.LoadChungTuTKCollection();
                    this.TKMD.LoadHMDCollection();
                    dgList.DataSource = this.TKMD.HMDCollection;
                    dgList.Refetch();
                    gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                            log.ID_DK = TKMD.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                        try
                        {
                            string where = "1 = 1";
                            where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                            List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                            if (listLog.Count > 0)
                            {
                                long idLog = listLog[0].IDLog;
                                string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                long idDK = listLog[0].ID_DK;
                                string guidstr = listLog[0].GUIDSTR_DK;
                                string userKhaiBao = listLog[0].UserNameKhaiBao;
                                DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                string userSuaDoi = GlobalSettings.UserLog;
                                DateTime ngaySuaDoi = DateTime.Now;
                                string ghiChu = listLog[0].GhiChu;
                                bool isDelete = listLog[0].IsDelete;
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                            userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                            }
                            else
                            {
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                                log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                                log.ID_DK = TKMD.ID;
                                log.GUIDSTR_DK = "";
                                log.UserNameKhaiBao = GlobalSettings.UserLog;
                                log.NgayKhaiBao = DateTime.Now;
                                log.UserNameSuaDoi = GlobalSettings.UserLog;
                                log.NgaySuaDoi = DateTime.Now;
                                log.GhiChu = "";
                                log.IsDelete = false;
                                log.Insert();
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                            return;
                        }
                    }
                    #endregion

                    // ShowMessage("Lưu tờ khai thành công.", false);
                    MLMessages("Lưu tờ khai thành công!", "MSG_SAV02", "", false);
                    SetCommandStatus();

                    //TODO: updated Hungtq 6/2/2013. Save()
                    SaveVersion();
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }

                #endregion
            }
        }

        private bool SoSanhNoiDungSuaDoiToKhai(ToKhaiMauDich TKMD_Goc, ToKhaiMauDich TKMD)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string loaiHinh = TKMD.MaLoaiHinh.Substring(0, 1);

                if (loaiHinh.Equals("N"))
                {
                    #region To khai NHAP

                    ////Người nhập khẩu & Người xuất khẩu
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //{
                    //    nguoiNhapKhau = TKMD.TenDoanhNghiep;
                    //    nguoiXuatKhau = TKMD.TenDonViDoiTac;
                    //}
                    //else
                    //{
                    //    nguoiNhapKhau = TKMD.TenDonViDoiTac;
                    //    nguoiXuatKhau = TKMD.TenDoanhNghiep;
                    //}
                    //1. Người xuất khẩu
                    //Tên Đơn vị đối tác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiXuatKhau", "1. Người xuất khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac);

                    //3. Đơn vị ủy thác
                    //Tên đơn vị Ủy thác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViUT, TKMD.TenDonViUT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiUyThac", "3. Người ủy thác:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViUT, TKMD.TenDonViUT);

                    //6. Hóa đơn Thương mại
                    //Số Hóa đơn Thương mại
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HoaDonThuongMai", "6. Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai);
                    //Ngày Hóa đơn Thương mại
                    if (TKMD_Goc.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHoaDonThuongMai", "6. Ngày Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //7. Giấy phép
                    //Số Giấy phép
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GiayPhep", "7. Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep);
                    //Ngày Giấy phép
                    if (TKMD_Goc.NgayGiayPhep != TKMD.NgayGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayGiayPhep", "7. Ngày Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày Hết hạn Giấy phép
                    if (TKMD_Goc.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanGiayPhep", "7. Ngày hết hạn Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //8. Hợp đồng
                    //Số Hợp đồng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHopDong, TKMD.SoHopDong))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HopDong", "8. Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.SoHopDong, TKMD.SoHopDong);
                    //Ngày Hợp đồng
                    if (TKMD_Goc.NgayHopDong != TKMD.NgayHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHopDong", "8. Ngày Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày hết hạn Hợp đồng
                    if (TKMD_Goc.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanHopDong", "8. Ngày hết hạn Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //9. Vận tải đơn
                    //Số vận tải đơn
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoVanDon, TKMD.SoVanDon))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "VanDon", "9. Vận tải đơn:", TKMD.MaLoaiHinh, TKMD_Goc.SoVanDon, TKMD.SoVanDon);
                    //Ngày vận tải đơn
                    if (TKMD_Goc.NgayVanDon != TKMD.NgayVanDon)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayVanDon", "9. Ngày vận tải đơn:", TKMD.MaLoaiHinh, TKMD_Goc.NgayVanDon.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayVanDon.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    ////Địa điểm dở hàng & Địa điểm xếp hàng
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //{
                    //    diaDiemDoHang = TKMD.CuaKhau_ID;
                    //    diaDiemXepHang = TKMD.DiaDiemXepHang;
                    //}
                    //else
                    //{
                    //    diaDiemXepHang = TKMD.CuaKhau_ID;
                    //    diaDiemDoHang = TKMD.DiaDiemXepHang;
                    //}
                    //10. Cảng xếp hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangXepHang", "10. Cảng xếp hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang);

                    //11. Cảng dở hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID.Trim()))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangDohang", "11. Cảng dở hàng:", TKMD.MaLoaiHinh, TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID);

                    //Phương tiện vận tải
                    //Loại Phương tiện vận tải
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTVT_ID, TKMD.PTVT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTVT", "12. Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.PTVT_ID, TKMD.PTVT_ID);
                    //Số hiệu Phương tiện vận tải
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHieuPTVT, TKMD.SoHieuPTVT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuPTVT", "12. Số hiệu Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.SoHieuPTVT, TKMD.SoHieuPTVT);
                    //Ngày đến Phương tiện vận tải
                    if (TKMD_Goc.NgayDenPTVT != TKMD.NgayDenPTVT)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayDenPTVT", "12. Ngày đến Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.NgayDenPTVT.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayDenPTVT.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    ////Nước xuất khẩu & Nước nhập khẩu
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //    nuocXuatKhau = TKMD.NuocXK_ID;
                    //else
                    //    nuocNhapKhau = TKMD.NuocNK_ID;
                    //13. Nước xuất khẩu
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXK", "13. Nước xuất khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID);

                    //14. Điều kiện giao hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DKGH_ID, TKMD.DKGH_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DKGH", "14. Điều kiện giao hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DKGH_ID, TKMD.DKGH_ID);

                    //15. Phương thức thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTTT_ID, TKMD.PTTT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTTT", "15. Phương thức thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.PTTT_ID, TKMD.PTTT_ID);

                    //16. Đồng tiền thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DongTienThanhToan", "16. Đồng tiền thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //17. Tỷ giá tính thuế
                    if (TKMD_Goc.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyGiaTinhThue", "17. Tỷ giá tính thuế:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //31.b Số kiện hàng
                    if (TKMD_Goc.SoKien != TKMD.SoKien)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKien", "31.b Tổng số kiện:", TKMD.MaLoaiHinh, TKMD_Goc.SoKien.ToString(), TKMD.SoKien.ToString());

                    //31.c Trọng lượng
                    if (TKMD_Goc.TrongLuong != TKMD.TrongLuong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuong", "31.c Tổng trọng lượng:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuong.ToString(), TKMD.TrongLuong.ToString());

                    //31.c Trọng lượng tịnh
                    if (TKMD_Goc.TrongLuongNet != TKMD.TrongLuongNet)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongTinh", "31.c Trọng lượng tịnh:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuongNet.ToString(), TKMD.TrongLuongNet.ToString());

                    //35. Ghi chep khac
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GhiChepKhac", "35. Ghi chép khác:", TKMD.MaLoaiHinh, TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac);

                    #endregion
                }
                else
                {
                    #region To khai XUAT

                    //2. Người nhập khẩu
                    //Tên Đơn vị đối tác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiNhapKhau", "2. Người nhập khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac);

                    //3. Đơn vị ủy thác
                    //Tên đơn vị Ủy thác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViUT, TKMD.TenDonViUT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiUyThac", "3. Người ủy thác:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViUT, TKMD.TenDonViUT);

                    //6. Giấy phép
                    //Số Giấy phép
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GiayPhep", "6. Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep);
                    //Ngày Giấy phép
                    if (TKMD_Goc.NgayGiayPhep != TKMD.NgayGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayGiayPhep", "6. Ngày Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày Hết hạn Giấy phép
                    if (TKMD_Goc.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanGiayPhep", "6. Ngày hết hạn Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //7. Hợp đồng
                    //Số Hợp đồng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHopDong, TKMD.SoHopDong))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HopDong", "7. Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.SoHopDong, TKMD.SoHopDong);
                    //Ngày Hợp đồng
                    if (TKMD_Goc.NgayHopDong != TKMD.NgayHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHopDong", "7. Ngày Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày hết hạn Hợp đồng
                    if (TKMD_Goc.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanHopDong", "7. Ngày hết hạn Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //8. Hóa đơn Thương mại
                    //Số Hóa đơn Thương mại
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HoaDonThuongMai", "8. Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai);
                    //Ngày Hóa đơn Thương mại
                    if (TKMD_Goc.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHoaDonThuongMai", "8. Ngày Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //9. Cảng xếp hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangXepHang", "9. Cảng xếp hàng:", TKMD.MaLoaiHinh, TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID);

                    //10. Nước xuất khẩu
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXK", "10. Nước nhập khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.NuocNK_ID, TKMD.NuocNK_ID);

                    //11. Điều kiện giao hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DKGH_ID, TKMD.DKGH_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DKGH", "11. Điều kiện giao hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DKGH_ID, TKMD.DKGH_ID);

                    //12. Phương thức thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTTT_ID, TKMD.PTTT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTTT", "12. Phương thức thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.PTTT_ID, TKMD.PTTT_ID);

                    //13. Đồng tiền thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DongTienThanhToan", "13. Đồng tiền thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //14. Tỷ giá tính thuế
                    if (TKMD_Goc.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyGiaTinhThue", "14. Tỷ giá tính thuế:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //25.b Số kiện hàng
                    if (TKMD_Goc.SoKien != TKMD.SoKien)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKien", "25.b Tổng số kiện:", TKMD.MaLoaiHinh, TKMD_Goc.SoKien.ToString(), TKMD.SoKien.ToString());

                    //25.c Trọng lượng
                    if (TKMD_Goc.TrongLuong != TKMD.TrongLuong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuong", "25.c Tổng trọng lượng:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuong.ToString(), TKMD.TrongLuong.ToString());

                    //25.c Trọng lượng tịnh
                    if (TKMD_Goc.TrongLuongNet != TKMD.TrongLuongNet)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongTinh", "25.c Trọng lượng tịnh:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuongNet.ToString(), TKMD.TrongLuongNet.ToString());

                    //29. Ghi chep khac
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GhiChepKhac", "29. Ghi chép khác:", TKMD.MaLoaiHinh, TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac);

                    #endregion
                }

                //Lệ phí hải quan
                if (TKMD_Goc.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtLePhiHQ.Tag.ToString(), "Phí hải quan:", TKMD.MaLoaiHinh, TKMD_Goc.LePhiHaiQuan.ToString(), TKMD.LePhiHaiQuan.ToString());

                //Phí bảo hiểm
                if (TKMD_Goc.PhiBaoHiem != TKMD.PhiBaoHiem)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiBaoHiem.Tag.ToString(), "Phí bảo hiểm:", TKMD.MaLoaiHinh, TKMD_Goc.PhiBaoHiem.ToString(), TKMD.PhiBaoHiem.ToString());

                //Phí vận chuyển
                if (TKMD_Goc.PhiVanChuyen != TKMD.PhiVanChuyen)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiVanChuyen.Tag.ToString(), "Phí vận chuyển:", TKMD.MaLoaiHinh, TKMD_Goc.PhiVanChuyen.ToString(), TKMD.PhiVanChuyen.ToString());

                //Phí khác
                if (TKMD_Goc.PhiKhac != TKMD.PhiKhac)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiNganHang.Tag.ToString(), "Phí khác:", TKMD.MaLoaiHinh, TKMD_Goc.PhiKhac.ToString(), TKMD.PhiKhac.ToString());

                #region Thong tin CONTAINER
                //Kiem tra thong tin bi Xoa/ Them moi/ Sua doi
                if (TKMD.VanTaiDon != null && TKMD_Goc.VanTaiDon != null)
                {
                    //1. Container: Tim thong tin cu so voi thong tin moi -> Kiem tra thong tin bi XOA/ SUA DOI
                    Company.KDT.SHARE.QuanLyChungTu.Container filter = null;
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD_Goc.VanTaiDon.ContainerCollection) //To khai cu
                    {
                        filter = TKMD.VanTaiDon.ContainerCollection.Find(delegate(Company.KDT.SHARE.QuanLyChungTu.Container o) { return o.SoHieu == item.SoHieu; });

                        //Khong co thong tin -> Da xoa
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuContainer", (loaiHinh == "N" ? "31" : "25") + ".a Số hiệu container:", TKMD.MaLoaiHinh, item.SoHieu, string.Format("Xóa thông tin container '{0}'", item.SoHieu));
                        }
                        //Co thong tin -> Sua doi
                        else
                        {
                            if (item.SoKien != filter.SoKien)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKienContainer", (loaiHinh == "N" ? "31" : "25") + ".b Số lượng kiện trong container:", TKMD.MaLoaiHinh, item.SoKien.ToString(), filter.SoKien.ToString());

                            if (item.TrongLuong != filter.TrongLuong)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongContainer", (loaiHinh == "N" ? "31" : "25") + ".c Trọng lượng hàng trong container:", TKMD.MaLoaiHinh, item.TrongLuong.ToString(), filter.TrongLuong.ToString());

                            if (SoSanhStringNoiDungSua(item.DiaDiemDongHang, filter.DiaDiemDongHang))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DiaDiemDongHangContainer", (loaiHinh == "N" ? "31" : "25") + ".d Địa điểm đóng hàng:", TKMD.MaLoaiHinh, item.DiaDiemDongHang, filter.DiaDiemDongHang);
                        }
                    }

                    //2. Container: Tim thong tin moi so voi thong tin cu -> Kiem tra thong tin THEM MOI
                    filter = null;
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD.VanTaiDon.ContainerCollection) //To khai moi
                    {
                        filter = TKMD_Goc.VanTaiDon.ContainerCollection.Find(delegate(Company.KDT.SHARE.QuanLyChungTu.Container o) { return o.SoHieu == item.SoHieu; });

                        //Khong co thong tin -> Them moi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuContainer", (loaiHinh == "N" ? "31" : "25") + ".a Số hiệu container:", TKMD.MaLoaiHinh, item.SoHieu, string.Format("Thêm mới thông tin container '{0}'", item.SoHieu));
                        }
                    }
                }
                #endregion

                #region Thong tin HANG
                //Kiem tra thong tin bi Xoa/ Them moi/ Sua doi
                if (TKMD.HMDCollection != null && TKMD_Goc.HMDCollection != null)
                {
                    //1. Hang: Tim thong tin cu so voi thong tin moi -> Kiem tra thong tin bi XOA/ SUA DOI
                    Company.BLL.KDT.HangMauDich filter = null;
                    foreach (Company.BLL.KDT.HangMauDich item in TKMD_Goc.HMDCollection) //To khai cu
                    {
                        //Doi voi Kinh doanh: kiem tra ten hang, vi khong co ma.
                        //Doi voi SXXK, GC: kiem tra ma hang.
                        if (item.MaPhu != null && item.MaPhu != "")
                        {
                            filter = TKMD.HMDCollection.Find(delegate(Company.BLL.KDT.HangMauDich o) { return o.MaPhu == item.MaPhu; });
                        }
                        else
                        {
                            filter = TKMD.HMDCollection.Find(delegate(Company.BLL.KDT.HangMauDich o) { return o.TenHang == item.TenHang; });
                        }

                        //Khong co thong tin -> Da xoa/Sua doi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", (loaiHinh == "N" ? "18" : "15") + ". Mô tả hàng hóa:", TKMD.MaLoaiHinh, item.TenHang, string.Format("Xóa thông tin hàng '{0}/ {1}' tại dòng số {2}", item.TenHang, item.MaPhu, item.SoThuTuHang));
                        }
                        //Co thong tin -> Sua doi
                        else
                        {
                            if (item.SoThuTuHang != filter.SoThuTuHang)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoThuTuHMD", string.Format("Số thứ tự hàng của dòng hàng số {0}:", item.SoThuTuHang), TKMD.MaLoaiHinh, item.SoThuTuHang.ToString(), string.Format("Thay đổi số thứ tự dòng hàng số {0} -> dòng hàng số {1}", item.SoThuTuHang, filter.SoThuTuHang));

                            if (SoSanhStringNoiDungSua(item.TenHang, filter.TenHang))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", string.Format("{0} Mô tả hàng hóa (Tên hàng) của dòng hàng số {1}:", (loaiHinh == "N" ? "18." : "15."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TenHang, filter.TenHang);

                            if (SoSanhStringNoiDungSua(item.MaPhu, filter.MaPhu))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "MaHMD", string.Format("{0} Mô tả hàng hóa (Mã hàng) của dòng hàng số {1}:", (loaiHinh == "N" ? "18." : "15."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.MaPhu, filter.MaPhu);

                            if (SoSanhStringNoiDungSua(item.MaHS, filter.MaHS))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "MaHSHMD", string.Format("{0} Mã số hàng hóa của dòng hàng số {1}:", (loaiHinh == "N" ? "19." : "16."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.MaHS, filter.MaHS);

                            if (SoSanhStringNoiDungSua(item.NuocXX_ID, filter.NuocXX_ID))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXuatXuHMD", string.Format("{0} Xuất xứ của dòng hàng số {1}:", (loaiHinh == "N" ? "20." : "17."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.NuocXX_ID.ToString(), filter.NuocXX_ID.ToString());

                            if (SoSanhStringNoiDungSua(item.CheDoUuDai, filter.CheDoUuDai) & loaiHinh == "N")
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CheDoUuDaiHMD", string.Format("{0} Chế độ ưu đãi của dòng hàng số {1}:", (loaiHinh == "N" ? "21." : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.CheDoUuDai, filter.CheDoUuDai);

                            if (item.SoLuong != filter.SoLuong)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoLuongHMD", string.Format("{0} Lượng hàng của dòng hàng số {1}:", (loaiHinh == "N" ? "22." : "18."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.SoLuong.ToString(), filter.SoLuong.ToString());

                            if (SoSanhStringNoiDungSua(item.DVT_ID, filter.DVT_ID))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DonViTinhHMD", string.Format("{0} Đơn vị tính của dòng hàng số {1}:", (loaiHinh == "N" ? "23." : "19."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.DVT_ID.ToString(), filter.DVT_ID.ToString());

                            if (item.DonGiaKB != filter.DonGiaKB)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DonGiaHMD", string.Format("{0} Đơn giá của dòng hàng số {1}:", (loaiHinh == "N" ? "24." : "20."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.DonGiaKB.ToString(), filter.DonGiaKB.ToString());

                            if (item.TriGiaKB != filter.TriGiaKB)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TriGiaNguyenTeHMD", string.Format("{0} Trị giá nguyên tệ của dòng hàng số {1}:", (loaiHinh == "N" ? "25." : "21."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TriGiaKB.ToString(), filter.TriGiaKB.ToString());

                            //Thue XNK 
                            if (item.ThueSuatXNK != filter.ThueSuatXNK)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatXNKHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "26. Thuế nhập khẩu:" : "22.b Thuế xuât khẩu:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatXNK.ToString(), filter.ThueSuatXNK.ToString());

                            if (item.ThueXNK != filter.ThueXNK)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueXNKHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "26. Thuế nhập khẩu:" : "22.c Thuế xuât khẩu:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueXNK.ToString(), filter.ThueXNK.ToString());

                            //Thue TTDB
                            if (item.ThueSuatTTDB != filter.ThueSuatTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatTTDBHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế TTĐB:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatTTDB.ToString(), filter.ThueSuatTTDB.ToString());

                            if (item.ThueTTDB != filter.ThueTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueTTDBHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế TTĐB:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueTTDB.ToString(), filter.ThueTTDB.ToString());

                            //Thue Tu ve chong ban pha gia
                            if (item.ThueSuatTTDB != filter.ThueSuatTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatTVCBPGHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế Tự vệ chống bán phá giá:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatChongPhaGia.ToString(), filter.ThueSuatChongPhaGia.ToString());

                            if (item.ThueTTDB != filter.ThueTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueTVCBPGHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế Tự vệ chống bán phá giá:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueChongPhaGia.ToString(), filter.ThueChongPhaGia.ToString());

                            //Thue BVMT
                            if (item.ThueSuatBVMT != filter.ThueSuatBVMT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatBVMTHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "28. Thuế BVMT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatBVMT.ToString(), filter.ThueSuatBVMT.ToString());

                            if (item.ThueBVMT != filter.ThueBVMT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueBVMTHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế BVMT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueBVMT.ToString(), filter.ThueBVMT.ToString());

                            //Thue GTGT
                            if (item.ThueSuatGTGT != filter.ThueSuatGTGT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatGTGTHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "29. Thuế GTGT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatGTGT.ToString(), filter.ThueSuatGTGT.ToString());

                            if (item.ThueGTGT != filter.ThueGTGT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueGTGTHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "28. Thuế GTGT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueGTGT.ToString(), filter.ThueGTGT.ToString());

                            //Thue thu khac
                            if (item.TyLeThuKhac != filter.TyLeThuKhac && loaiHinh == "X") //Chi co to khai xuat
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyLeThuKhacHMD", string.Format("{0} Tỷ lệ(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "" : "23.b Thu khác:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TyLeThuKhac.ToString(), filter.TyLeThuKhac.ToString());

                            if (item.PhuThu != filter.PhuThu && loaiHinh == "X") //Chi co to khai xuat
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PhuThuHMD", string.Format("{0} Số tiền của dòng hàng số {1}:", (loaiHinh == "N" ? "" : "23.c Thu khác:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.PhuThu.ToString(), filter.PhuThu.ToString());
                        }
                    }

                    //2. Hang: Tim thong tin moi so voi thong tin cu -> Kiem tra thong tin THEM MOI
                    filter = null;
                    foreach (Company.BLL.KDT.HangMauDich item in TKMD.HMDCollection) //To khai moi
                    {
                        //Doi voi Kinh doanh: kiem tra ten hang, vi khong co ma.
                        //Doi voi SXXK, GC: kiem tra ma hang.
                        if (item.MaPhu != null && item.MaPhu != "")
                        {
                            filter = TKMD_Goc.HMDCollection.Find(delegate(Company.BLL.KDT.HangMauDich o) { return o.MaPhu == item.MaPhu; });
                        }
                        else
                        {
                            filter = TKMD_Goc.HMDCollection.Find(delegate(Company.BLL.KDT.HangMauDich o) { return o.TenHang == item.TenHang; });
                        }

                        //Khong co thong tin -> Them moi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", (loaiHinh == "N" ? "18" : "15") + ". Mô tả hàng hóa:", TKMD.MaLoaiHinh, item.TenHang, string.Format("Thêm mới thông tin hàng '{0}/ {1}' tại dòng số {2}", item.TenHang, item.MaPhu, item.SoThuTuHang));
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return false;
        }

        private void CapNhatNoiDungSuaToKhai(ToKhaiMauDich TKMD_Goc, ToKhaiMauDich TKMD, string maThongTin, string tenThongTin, string maLoaiHinh, string noiDungChinh, string noiDungSua)
        {
            NoiDungToKhai noiDungSuaTK = new NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungSuaChiTiet = new NoiDungDieuChinhTKDetail();

            List<NoiDungToKhai> listNoiDungSuaTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + maThongTin + "' " + " AND MaLoaiHinh = '" + maLoaiHinh + "'", null);

            if (listNoiDungSuaTK.Count == 0)
            {
                noiDungSuaTK.Ma = maThongTin;
                noiDungSuaTK.Ten = tenThongTin;
                noiDungSuaTK.MaLoaiHinh = maLoaiHinh;
                noiDungSuaTK.InsertUpdate();

                List<NoiDungDieuChinhTKDetail> noiDungDieuChinhTKDetailCollections = NoiDungDieuChinhTKDetail.SelectCollectionDynamic(string.Format("NoiDungTKChinh = N'{0}'", noiDungSuaTK.Ten + " " + noiDungChinh), "");

                if ((noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count > 0 && noiDungDieuChinhTKDetailCollections[0].NguoiTao == ENoiDungDieuChinhTK.Auto.ToString())
                    || (noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count == 0))
                {
                    noiDungSuaChiTiet.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                    noiDungSuaChiTiet.NoiDungTKChinh = noiDungSuaTK.Ten + " " + noiDungChinh;
                    noiDungSuaChiTiet.NoiDungTKSua = noiDungSuaTK.Ten + " " + noiDungSua;
                    noiDungSuaChiTiet.NguoiTao = ENoiDungDieuChinhTK.Auto.ToString();
                    noiDungSuaChiTiet.InsertUpdateByThongTin(null);
                }
            }
            else
            {
                foreach (NoiDungToKhai ndtk in listNoiDungSuaTK)
                {
                    if (ndtk.Ten != tenThongTin)
                    {
                        ndtk.Ten = tenThongTin;
                        ndtk.Update();
                    }

                    List<NoiDungDieuChinhTKDetail> noiDungDieuChinhTKDetailCollections = NoiDungDieuChinhTKDetail.SelectCollectionDynamic(string.Format("NoiDungTKChinh = N'{0}'", ndtk.Ten + " " + noiDungChinh), "");

                    if ((noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count > 0 && noiDungDieuChinhTKDetailCollections[0].NguoiTao == ENoiDungDieuChinhTK.Auto.ToString())
                        || (noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count == 0))
                    {
                        noiDungSuaChiTiet.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungSuaChiTiet.NoiDungTKChinh = ndtk.Ten + " " + noiDungChinh;
                        noiDungSuaChiTiet.NoiDungTKSua = ndtk.Ten + " " + noiDungSua;
                        noiDungSuaChiTiet.NguoiTao = ENoiDungDieuChinhTK.Auto.ToString();
                        noiDungSuaChiTiet.InsertUpdateByThongTin(null);
                    }
                }
            }
        }
        private bool SoSanhStringNoiDungSua(string goc, string sua)
        {
            return (string.IsNullOrEmpty(goc) ? "" : goc).Trim() != (string.IsNullOrEmpty(sua) ? "" : sua).Trim();
        }

        //Hungtq updated 6/2/2013
        public void SaveVersion()
        {
            try
            {
                bool cks = Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature;

                if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 1, cks, "", null);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 2, cks, "", null);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && GlobalSettings.SendV4 == false)
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 3, cks, "", null);
                else if (GlobalSettings.SendV4) //Mac dinh V4
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 4, cks, "", null);
                else
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 0, cks, "", null);

                //Reload
                tkmdVersionControlV1.LoadInfo(TKMD.ID);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void tinhLaiThue()
        {
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Value);

            decimal tongSoLuongHang = 0;

            if (this.TKMD.HMDCollection.Count == 0)
            {
                lblTongTGNT.Text = string.Format("{0} ({1})", "0", ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", "0");
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));

                return;
            }

            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
            decimal TriGiaTTMotDong = 0;
            if (TongTriGiaHang != 0)
                TriGiaTTMotDong = (Phi == 0 || GlobalSettings.TuDongTinhThue == "0") ? TKMD.TyGiaTinhThue : (1 + Phi / TongTriGiaHang) * TKMD.TyGiaTinhThue;
            //if (Phi == 0)
            //{
            //    TriGiaTTMotDong = Convert.ToDecimal(TKMD.HeSoNhan) * TKMD.TyGiaTinhThue;
            //}
            decimal tongThue = 0;
            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                //                 tongSoLuongHang += hmd.SoLuong;
                //                 hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                //                 hmd.TriGiaTT = hmd.DonGiaTT * hmd.SoLuong;
                //                 hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                //                 tongThue += hmd.ThueXNK + hmd.TriGiaThuKhac;
                if (hmd.FOC)
                {
                    hmd.TriGiaTT = hmd.ThueXNK = hmd.ThueTTDB = hmd.ThueGTGT = hmd.TriGiaThuKhac = hmd.ThueSuatXNK = hmd.ThueSuatTTDB = hmd.ThueSuatGTGT = hmd.TyLeThuKhac = 0;
                    continue;
                }

                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * Convert.ToDecimal(hmd.SoLuong), MidpointRounding.AwayFromZero);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                else
                {
                    hmd.ThueXNK = Math.Round((Decimal)hmd.DonGiaTuyetDoi * TKMD.TyGiaTinhThue * hmd.SoLuong);
                }
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueBVMT = hmd.ThueSuatBVMT > 0 ? Math.Round((hmd.TriGiaTT) * (decimal)hmd.ThueSuatBVMT / 100) : hmd.ThueBVMT;
                hmd.ThueChongPhaGia = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * (decimal)hmd.ThueSuatChongPhaGia);
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueChongPhaGia + hmd.ThueTTDB + hmd.TriGiaThuKhac + hmd.ThueBVMT) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGNT.Text = string.Format("{0} ({1})", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));
            }
            else
            {
                //lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma, tongThue.ToString("N"));
            }
        }
        private decimal tinhTongTriGiaKhaiBao()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            decimal tongThue = 0;
            decimal tongSoLuongHang = 0;

            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tongSoLuongHang += hmd.SoLuong;

                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
                tongThue += hmd.ThueXNK + hmd.TriGiaThuKhac;
            }
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGNT.Text = string.Format("{0} ({1})", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
            }

            return tongThue;
        }
        //-----------------------------------------------------------------------------------------

        private void inToKhai()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        private void inToKhaiTT15()
        {
            try
            {
                if (this.TKMD.HMDCollection.Count == 0) return;
                if (this.TKMD.ID == 0)
                {
                    //ShowMessage("Bạn hãy lưu trước khi in.",false);
                    MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                    return;
                }
                switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
                {
                    case "N":
                        ReportViewTKNTQDTFormTT196New f = new ReportViewTKNTQDTFormTT196New(false);
                        f.TKMD = this.TKMD;
                        f.Show();
                        break;

                    case "X":
                        ReportViewTKXTQDTFormTT196New f1 = new ReportViewTKXTQDTFormTT196New(false);
                        f1.TKMD = this.TKMD;
                        f1.Show();
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void inToKhaiA4()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "X":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        /// <summary>
        /// In tờ khai Thông Quan Điện tử
        /// </summary>
        private void inToKhaiTQDT()
        {
            TKMD.Load();
            TKMD.LoadHMDCollection();
            TKMD.LoadChungTuTKCollection();

            if (this.TKMD.HMDCollection.Count == 0)
                return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "X":
                    ReportViewTKXTQDTForm f1 = new ReportViewTKXTQDTForm();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        private void SetCommandStatus()
        {
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = "Chưa khai báo";
                    lblPhanLuong.Text = "Chưa phân luồng";
                }
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = txtSoToKhai.Text = "";

                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                lblTrangThai.Text = "Không phê duyệt";



                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu2.Enabled = XacNhan.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend2.Enabled = cmdSave.Enabled = ThemHang2.Enabled = ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = cmdChungTuBoSung.Enabled = cmdSuaToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;


                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                //Hungtq, Update 20/07/2010
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (TKMD.PhanLuong != "")
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }


                lblTrangThai.Text = "Đã duyệt";

                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();

                //Nếu tờ khai chờ duyệt có số tờ khai thì không cho phép hủy khai báo
                if (TKMD.SoToKhai > 0)
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (TKMD.PhanLuong == "1")
            {
                lblPhanLuong.ForeColor = Color.Green;
                lblPhanLuong.Text = "Luồng Xanh";
            }
            else if (TKMD.PhanLuong == "2")
            {
                lblPhanLuong.Text = "Luồng Vàng";
                lblPhanLuong.ForeColor = Color.Yellow;

            }
            else if (TKMD.PhanLuong == "3")
            {
                lblPhanLuong.Text = "Luồng Đỏ";
                lblPhanLuong.ForeColor = Color.Red;

            }
            else
            {
                lblPhanLuong.Text = "Chưa phân luồng";
                lblPhanLuong.ForeColor = Color.Black;
            }
            SetCommandStatusSuaHuyToKhai();

            SetStatusChungTuDinhKem(this.TKMD);

            #region set trùng tờ khai
            timer1.Stop();
            timer1.Interval = 500;
            if (TKMD.QuanLyMay)
            {

                pictureBox1.Visible = true;

                timer1.Start();
            }
            else
            {
                pictureBox1.Visible = false;
                timer1.Stop();
            }
            #endregion

        }

        private void KetQuaXuLyTK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TKMD.ID;
            bool isToKhaiNhap = TKMD.MaLoaiHinh.StartsWith("N");
            form.DeclarationIssuer = "";// isToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT;
            form.ShowDialog(this);
        }

        #region SUA - HUY TO KHAI

        private void SetCommandStatusSuaHuyToKhai()
        {
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUYTKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang2.Enabled = ThemHang1.Enabled = cmdSave.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Đã hủy";
            }

            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //Nếu tờ khai chờ duyệt có số tờ khai thì không cho phép hủy khai báo
                if (TKMD.SoToKhai > 0)
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }

            //Hungtq updated 24/02/2012.
            #region Cho phép xem thông tin Chứng từ kèm khi Tờ khai ở trạng thái: Chờ duyệt, Đã duyệt, Sửa tờ khai, Hủy tờ khai, Không phê duyệt, Chờ hủy tờ khai, Hủy tờ khai.

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                )
            {
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            #endregion
        }

        private void HuyToKhaiDaDuyet()
        {
            //TKMD.TrangThaiXuLy = 1;
            //TKMD.ActionStatus = 1;
            //TKMD.Update();
            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + this.TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + this.TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = this.TKMD;
                    f.ShowDialog(this);

                    //datlmq update 24/07/2010; Bổ sung History Kết quả xử lý
                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.TKMD.ID;
                    kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Hủy tờ khai";//KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                    kqxl.NoiDung = "Khai báo hủy";//string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", TKMD.SoTiepNhan, TKMD.NgayTiepNhan.ToShortDateString()); ;
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = this.TKMD;
                f.ShowDialog(this);
            }
        }

        #region Bổ sung Khai báo sửa tờ khai
        private void NhanPhanHoiSuaTK()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i < 3; i++)
                {
                    LayPhanHoiSuaTK(password);
                }
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiSuaTK(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(Company.KDT.SHARE.Components.MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    //LayPhanHoiSuaTK(pass);
                    return;
                }

                //}
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void SuaToKhaiDaDuyet()
        {
            //TODO: Hungtq noted 30/03/2011.
            /*
             * Kiểm tra tờ khai có tham gia hồ sơ thanh khoản nào không?. Hồ sơ thanh khoản đã đóng hay chưa đóng hồ sơ?.
             */
            try
            {
                if (this.TKMD.IsToKhaiDaThanhKhoanVaDongHoSo())
                {
                    ShowMessage(string.Format("Tờ khai: '{0}'\nMã loại hình: {1}\nNgày đăng ký: {2} \nđã tham gia thanh khoản và hồ sơ đã đóng, không thể sửa tờ khai này được.", this.TKMD.SoToKhai.ToString(), TKMD.MaLoaiHinh, TKMD.NgayDangKy), false);
                    return;
                }
                else if (this.TKMD.IsToKhaiDangThanhKhoan())
                {
                    ShowMessage(string.Format("Tờ khai: '{0}'\nMã loại hình: {1}\nNgày đăng ký: {2} \nđã tham gia thanh khoản và đang chạy hồ sơ, không thể sửa tờ khai này được.\nĐể có thể sửa tờ khai này được, hãy xóa tờ khai này trong bảng kê tờ khai của hồ sơ thanh khoản.", this.TKMD.SoToKhai.ToString(), TKMD.MaLoaiHinh, TKMD.NgayDangKy), false);
                    return;
                }

                //DATLMQ bổ sung Request hết thông tin trả về từ Hải quan trước khi chuyển sang trạng thái Sửa TK 24/05/2011
                if ((!TKMD.GUIDSTR.Equals("")) && TKMD.SoTiepNhan > 0)
                    FeedBackV3();

                string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
                msg += "\n\nSố tờ khai: " + this.TKMD.SoToKhai.ToString();
                msg += "\n----------------------";
                msg += "\nCó " + this.TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    long id = this.TKMD.ID;
                    this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                    //Cap nhat trang thai cho to khai
                    if (!this.TKMD.MaLoaiHinh.Contains("V"))
                        this.TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                    else
                        this.TKMD.GUIDSTR = Guid.NewGuid().ToString();

                    this.TKMD.Update();

                    //Cap nhat trang thai sua to khai
                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.TKMD.ID;
                    kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Chuyển trạng thái sửa tờ khai"; // KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                    kqxl.NoiDung = "";
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    //TODO: Luu thong tin to khai goc truoc khi sua. Hungtq, 05/11/2013.
                    Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                    if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                        msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                    Company.KDT.SHARE.Components.Globals.SaveMessage(Helpers.Serializer(TKMD), TKMD.ID, TKMD.GUIDSTR, msgType, Company.KDT.SHARE.Components.MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");

                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    try
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = TKMD.GUIDSTR;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    catch (Exception ex)
                    {
                        SingleMessage.SendMail("Không thể cập nhật user sửa đổi", TKMD.MaHaiQuan, new SendEventArgs(ex));
                        return;
                    }

                    SetCommandStatusSuaHuyToKhai();
                }
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail("Lỗi trong quá trình thực hiện chuyển trạng thái Sửa tờ khai", TKMD.MaHaiQuan, new SendEventArgs(ex));
                return;
            }
        }

        #endregion

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //in bang ke hop dong/don hang theo to khai
                case "cmdInBangKeHD_ToKhai":
                    InBangKeHD_ToKhai();
                    break;
                //Hungtq, Update 06/11/2011
                case "cmdInAnDinhThue":
                    InAnDinhThue(this.TKMD.ID, this.TKMD.GUIDSTR);
                    break;
                case "InToKhai":
                    this.inToKhai();
                    break;
                case "cmdInToKhaiTT15":
                    this.inToKhaiTT15();
                    break;
                case "cmdInTKTCTT196":
                    this.InToKhaiTaiChoTT196();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdPrint":
                    this.inToKhai();
                    break;
                case "cmdPrintA4":
                    this.inToKhaiA4();
                    break;
                case "cmdInToKhaiTQDT":
                    this.inToKhaiTQDT();
                    break;
                case "cmdInTKDTSuaDoiBoSung":
                    InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "cmdContainer196":
                    InContainer196();
                    break;
                case "cmdInPhieuKiemTra_ChungTu":
                    InPhieuKiemTra_ChungTu();
                    break;
                case "cmcInPhieuKiemTra_HangHoa":
                    InPhieuKiemTra_HangHoa();
                    break;
                //Hungtq, Update 14072010
                case "cmdHuyToKhaiDaDuyet":
                    HuyToKhaiDaDuyet();
                    break;
                case "cmdSuaToKhaiDaDuyet":
                    SuaToKhaiDaDuyet();
                    break;
                case "cmdInBangKeKemTheo":
                    InContainer();
                    break;
                case "cmdKetQuaXuLy":
                    this.KetQuaXuLyTK();
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    //ShowMessage("Tổng trị giá khai báo là : " + this.TKMD.TongTriGiaKhaiBao, false);
                    MLMessages("Tổng trị giá khai báo là : " + this.TKMD.TongTriGiaKhaiBao, "MSG_ADD06", "" + this.TKMD.TongTriGiaKhaiBao, false);
                    break;
                case "cmdReadExcel":
                    this.ReadExcel();
                    break;
                case "cmdThemHang":
                    this.themHang();
                    break;
                case "ThemHang":
                    this.themHang();
                    break;
                case "cmdSendTK":
                    string h = "Bạn đang khai báo thông tin tờ khai đưa vào thanh khoản đến hải quan?\r\n";
                    Company.KDT.SHARE.Components.Globals.IsKhaiTK = true;
                    if (this.ShowMessage(h, true) == "Yes")
                    {
                        SendV3();
                        checkTrangThaiXuLyTK();
                    }
                    Company.KDT.SHARE.Components.Globals.IsKhaiTK = false;
                    break;
                case "cmdSend":
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    int versionHD = Convert.ToInt32(node.InnerText);
                    string diaChiKhaibao = "";
                    string haiQuanKhaiBao = "";
                    if (versionHD == 1)
                    {
                        string MaDN = GlobalSettings.MA_DON_VI;
                        haiQuanKhaiBao = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_HAI_QUAN").Value_Config.ToString() +
                                         " - " + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_CUC_HAI_QUAN").Value_Config.ToString();
                        diaChiKhaibao = "http://" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI_HQ").Value_Config.ToString() +
                                        "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_DICH_VU").Value_Config.ToString();
                    }
                    else
                    {
                        diaChiKhaibao = GlobalSettings.DiaChiWS;
                        haiQuanKhaiBao = GlobalSettings.MA_HAI_QUAN + " - " + GlobalSettings.TEN_HAI_QUAN;
                    }
                    string hoi = "Bạn đang khai báo thông tin tờ khai đến:\r\n";
                    hoi += string.Format("Chi cục Hải quan:  {0}\r\n", haiQuanKhaiBao);
                    hoi += string.Format("Service khai báo: {0}\r\n\n", diaChiKhaibao);
                    hoi += "Bạn có muốn khai báo thông tin tờ khai này không?";

                    if (this.ShowMessage(hoi, true) == "Yes")
                    {
                        //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        //{
                        SendV3();
                        //}
                        //else
                        //{
                        //    this.Send();
                        //}
                        checkTrangThaiXuLyTK();
                    }
                    break;
                case "ChungTuKemTheo":
                    this.AddChungTu();
                    break;
                case "NhanDuLieu":// đang dùng hàm này để nhận duyệt/từ chối
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        FeedBackV3();
                    }
                    else
                    {
                        this.NhanPhanLuong();
                    }

                    checkTrangThaiXuLyTK();
                    break;
                case "XacNhan":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        FeedBackV3();
                    }
                    else
                    {
                        this.NhanPhanLuong();
                    }
                    checkTrangThaiXuLyTK();
                    break;
                case "FileDinhKem":
                    this.GetFileDinhKem();
                    break;

                case "cmdVanDon":
                    this.txtSoVanTaiDon_ButtonClick(null, null);
                    break;
                case "cmdHoaDonThuongMai":
                    this.txtSoHoaDonThuongMai_ButtonClick(null, null);
                    break;
                case "cmdCO":
                    this.QuanLyCO("");
                    break;
                case "cmdHopDong":
                    this.txtSoHopDong_ButtonClick(null, null);
                    break;
                case "cmdGiayPhep":
                    this.txtSoGiayPhep_ButtonClick(null, null);
                    break;
                case "cmdChuyenCuaKhau":
                    this.ShowChuyenCuaKhau("");
                    break;
                case "cmdVanDonBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoVanTaiDon_ButtonClick("1", null);
                    break;
                case "cmdHoaDonThuongMaiBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                    break;
                case "CO":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QuanLyCO("1");
                    break;
                case "GiayKiemTra":
                    this.QLGiayKiemTra(false);
                    break;
                case "BoSungGiayKiemTra":
                    this.QLGiayKiemTra(true);
                    break;
                case "ChungThuGiamDinh":
                    this.QLChungThuGiamDinh(false);
                    break;
                case "BoSungChungThuGiamDinh":
                    this.QLChungThuGiamDinh(true);
                    break;
                case "cmdHopDongBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHopDong_ButtonClick("1", null);
                    break;
                case "cmdGiayPhepBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoGiayPhep_ButtonClick("1", null);
                    break;
                case "cmdChuyenCuaKhauBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.ShowChuyenCuaKhau("1");
                    break;

                case "cmdChungTuDangAnh":
                    AddChungTuAnh();
                    break;
                case "cmdChungTuDangAnhBS":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.AddChungTuAnh_BoSung();
                    break;

                case "cmdHuyKhaiBao":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        CancelV3();
                    }
                    else
                        HuyKhaiBao();
                    break;
                case "cmdChungTuNo":
                    AddChungTuNo();
                    break;
                case "cmdGiayNopTien":
                    ShowGiayNopTien();
                    break;
                case "cmdBaoLanhThue":
                    ChungTuBaoLanhThue();
                    break;
                case "cmdPhanHoiSoTiepNhan":
                    FeedBackV3("29");
                    break;
                case "cmdPhanHoiSoToKhai":
                    FeedBackV3("30");
                    break;
                case "cmdPhanHoiPhanLuong":
                    FeedBackV3("31");
                    break;
                case "cmdPhuKienTK":
                    ShowPhuKien();
                    break;


            }
        }

        #region Chuc nang in
        /// <summary>
        /// In to khai tai cho
        /// </summary>
        private void InToKhaiTaiChoTT196()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (this.TKMD.HMDCollection.Count == 0) return;
                if (this.TKMD.ID == 0)
                {
                    //ShowMessage("Bạn hãy lưu trước khi in.",false);
                    MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                    return;
                }
                switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
                {
                    //Nhap
                    case "N":
                        ReportViewTKNTQDTFormTT196New f = new ReportViewTKNTQDTFormTT196New(true);
                        f.TKMD = this.TKMD;
                        f.Show();
                        break;
                    //Xuat
                    case "X":
                        ReportViewTKXTQDTFormTT196New f1 = new ReportViewTKXTQDTFormTT196New(true);
                        f1.TKMD = this.TKMD;
                        f1.Show();
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        /// <summary>
        /// In bảng kê Container 196
        /// </summary>
        private void InContainer196()
        {
            if (TKMD.ID == 0)
                return;
            //In mau cu
            //Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        /// <summary>
        /// In bang ke hang hoa tho to khai
        /// </summary>
        private void InBangKeHD_ToKhai()
        {
            if (TKMD.HopDongThuongMaiCollection.Count > 0)
            {
                Company.Interface.Report.SXXK.BangKe_HD_TheoTK f = new Company.Interface.Report.SXXK.BangKe_HD_TheoTK();
                f.tkmd = TKMD;
                f.BindReport_HopDong();
                f.ShowPreview();
            }
            else
                ShowMessage("Không tồn tại hợp đồng trong tờ khai này", false);

        }
        /// <summary>
        /// Bang ke container
        /// </summary>
        private void InContainer()
        {
            if (TKMD.ID == 0)
                return;

            Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }

        /// <summary>
        /// In to khai sua doi bo sung
        /// </summary>
        private void InToKhaiDienTuSuaDoiBoSung()
        {
            InToKhaiSuaDoiBoSungForm frmInToKhaiSuaDoiBoSung = new InToKhaiSuaDoiBoSungForm();
            InToKhaiSuaDoiBoSungForm.TKMD = TKMD;
            frmInToKhaiSuaDoiBoSung.ShowDialog(this);

            //bool ok = true;
            //NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            //ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            //foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            //{
            //    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            //    {
            //        ShowMessage("Thông tin sửa đổi, bổ sung chưa được duyệt. Không thể in tờ khai sửa đổi bổ sung", false);
            //        ok = false;
            //    }
            //}
            //if (ok)
            //{
            //    //Lấy đường dẫn file gốc
            //    string destFile = "";
            //    string fileName = Company.KDT.SHARE.Components.Globals.FILE_EXCEL_TOKHAI_SUADOI_BOSUNG;
            //    string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //    //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            //    try
            //    {
            //        Workbook workBook = Workbook.Load(sourcePath);
            //        Worksheet workSheet = workBook.Worksheets[0];

            //        UpdateWorkSheetHeaderExcel(workSheet);

            //        //Gán giá trị tương ứng vào các ô trong bảng nội dung sửa đổi
            //        fillNoiDungTKChinh(workBook, workSheet, 13, 1);
            //        fillNoiDungTKSua(workBook, workSheet, 13, 1);

            //        //Chọn đường dẫn file cần lưu
            //        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //        saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
            //        string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
            //        saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKMD.SoToKhai + "_NamDK" + TKMD.NamDK + "_" + dateNow;
            //        if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            //        {
            //            destFile = saveFileDialog1.FileName;
            //            //Ghi nội dung file gốc vào file cần lưu
            //            try
            //            {
            //                byte[] sourceFile = Globals.ReadFile(sourcePath);
            //                Globals.WriteFile(destFile, sourceFile);
            //                workBook.Save(destFile);
            //                ShowMessage("Lưu tệp tin thành công", false);
            //                System.Diagnostics.Process.Start(destFile);
            //            }
            //            catch (Exception ex)
            //            {
            //                ShowMessage(ex.Message, false);
            //                return;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        ShowMessage(ex.Message, false);
            //        return;
            //    }
            //}
        }

        private void UpdateWorkSheetHeaderExcel(Worksheet workSheet)
        {
            workSheet.Rows[5].Cells[6].Value = GlobalSettings.TEN_HAI_QUAN;
            workSheet.Rows[5].Cells[6].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[5].Cells[6].CellFormat.Font.Height = 10 * 20;
            //KhanhHN - Không in tên đơn vị đối tác lên tờ khai sửa đổi bổ sung
            //if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
            //{
            workSheet.Rows[7].Cells[6].Value = GlobalSettings.TEN_DON_VI;
            workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
            // }
            //else
            //{
            //    workSheet.Rows[7].Cells[6].Value = TKMD.TenDonViDoiTac;
            //    workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
            //    workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
            //}

            workSheet.Rows[9].Cells[3].Value = TKMD.SoToKhai;
            workSheet.Rows[9].Cells[3].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[9].Cells[3].CellFormat.Font.Height = 10 * 20;

            workSheet.Rows[9].Cells[9].Value = TKMD.MaLoaiHinh;
            workSheet.Rows[9].Cells[9].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[9].Cells[9].CellFormat.Font.Height = 10 * 20;

            workSheet.Rows[10].Cells[4].Value = TKMD.NgayTiepNhan;
            workSheet.Rows[10].Cells[4].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[10].Cells[4].CellFormat.Font.Height = 10 * 20;

            workSheet.Rows[9].Cells[15].Value = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
            workSheet.Rows[9].Cells[15].CellFormat.Font.Name = "Times New Roman";
            workSheet.Rows[9].Cells[15].CellFormat.Font.Height = 10 * 20;
        }

        private void fillNoiDungTKChinh(Workbook workBook, Worksheet worksheet, int row, int column)
        {
            int j = 1;
            int cnt = 0;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    cnt += 1;
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                        int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                    else if (row > 31)
                    {
                        //Tao them WorkSheet moi
                        Worksheet workSheet = workBook.Worksheets.Add("Mau so 6. TK sua doi, bo sung (2)");
                        UpdateWorkSheetHeaderExcel(worksheet);
                        fillNoiDungTKChinh(workBook, workSheet, row = 13, 1);
                    }
                }
            }
        }

        private void fillNoiDungTKSua(Workbook workBook, Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column + 11].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                        int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                            worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                    else if (row > 31)
                    {
                        //Tao them WorkSheet moi
                        Worksheet workSheet = workBook.Worksheets.Add("Mau so 6. TK sua doi, bo sung (2)");
                        fillNoiDungTKSua(workBook, workSheet, row = 13, 1);
                    }
                }
                //    }
            }
        }

        /// <summary>
        /// In phieu ghi ket qua kiem tra chung tu giay
        /// </summary>
        private void InPhieuKiemTra_ChungTu()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.KetQuaKiemTraChungTuGiay_TT196 f = new Company.Interface.Report.SXXK.KetQuaKiemTraChungTuGiay_TT196();
            f.TKMD = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        /// <summary>
        /// In phieu ghi ket qua kiem tra hang hoa
        /// </summary>
        private void InPhieuKiemTra_HangHoa()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.KetQuaKiemTraHangHoa_TT196 f = new Company.Interface.Report.SXXK.KetQuaKiemTraHangHoa_TT196();
            f.TKMD = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        #endregion

        private void ShowPhuKien()
        {
            PhuKienManagerFrm f = new PhuKienManagerFrm();
            f.TKMD = TKMD;
            f.ShowDialog();
        }
        private void ChungTuBaoLanhThue()
        {
            BaoLanhThueForm f = new BaoLanhThueForm();
            f.BaoLanhThue = TKMD.BaoLanhThue;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }
        private void ShowGiayNopTien()
        {
            ListGiayNopTienForm giayNopTienList = new ListGiayNopTienForm();
            giayNopTienList.TKMD = this.TKMD;
            giayNopTienList.ShowDialog(this);

        }

        //datlmq bo sung ham KiemTraTrangThaiToKhai 20/09/2010
        private void checkTrangThaiXuLyTK()
        {
            try
            {
                NoiDungChinhSuaTKForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKForm();
                noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                //Nếu là tờ khai sửa đã được duyệt
                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
                //Nếu là tờ khai sửa đang chờ duyệt
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
                //Nếu là tờ khai sửa mới
                else
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region Bổ sung hủy khai báo : DATLMQ 26/05/2011
        private void LayPhanHoiHuyKhaiBao(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(Company.KDT.SHARE.Components.MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                isTuChoi = true;
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {

                                string msg = KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void HuyKhaiBao()
        {
            ToKhaiMauDich tkmd = this.TKMD;
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();

            if (tkmd != null)
            {
                tkmd = this.TKMD;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

            this.Cursor = Cursors.WaitCursor;
            //DATLMQ bổ sung kiểm tra TK đã được HQ duyệt chưa 25/05/2011
            for (int i = 0; i < 2; i++)
            {
                LayPhanHoiHuyKhaiBao(password);
            }
            if (TKMD.SoToKhai > 0)
            {
                ShowMessage("Tờ khai đã được duyệt, không thể hủy khai báo", false);
                isTuChoi = false;
                return;
            }
            else if (isTuChoi)
            {
                ShowMessage("Tờ khai đã bị từ chối. Không thể hủy khai báo", false);
                return;
            }
            try
            {
                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    xmlCurrent = tkmd.WSCancelXMLNhap(password);
                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                    xmlCurrent = tkmd.WSCancelXMLXuat(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                sendXML.InsertUpdate();

                //xmlCurrent = tkmd.LayPhanHoiTQDTKhaiBao(password, xmlCurrent); // LINH CHUYỂN QUA DÙNG HÀM WSRequestPhanLuong()
                xmlCurrent = tkmd.WSRequestPhanLuong(password);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                string mess = "";
                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }

                    mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = tkmd.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = tkmd.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khi hủy khai báo Tờ khai : " + msg[0], false, true, ex.ToString());
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định khi hủy khai báo Tờ khai.", false, true, ex.ToString());
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void AddChungTuNo()
        {
            ListChungTuNoForm ctForm = new ListChungTuNoForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog(this);

        }
        private void AddChungTuAnh()
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }

            ListChungTuKemForm ctForm = new ListChungTuKemForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog(this);

        }

        private void AddChungTuAnh_BoSung()
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }

            ListChungTuKemForm f = new ListChungTuKemForm();
            f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void ShowChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListDeNghiChuyenCuaKhauTKMDForm f = new ListDeNghiChuyenCuaKhauTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void QuanLyCO(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListCOTKMDForm f = new ListCOTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog(this);

        }
        private void QLChungThuGiamDinh(bool isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ChungThuGiamDinhForm f = new ChungThuGiamDinhForm();
            f.isKhaiBoSung = isKhaiBoSung;
            f.TKMD = TKMD;
            f.ChungTuGiamDinh = TKMD.ChungThuGD;
            f.ShowDialog(this);
        }
        private void QLGiayKiemTra(bool isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayKiemTraForm f = new ListGiayKiemTraForm();
            f.isKhaiBoSung = isKhaiBoSung;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }
        private void inPhieuTN()
        {
            if (this.TKMD.SoTiepNhan == 0) return;
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "TỜ KHAI";
            phieuTN.soTN = this.TKMD.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = TKMD.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void GetFileDinhKem()
        {
            SendMailChungTuForm f = new SendMailChungTuForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
        }

        /*private void NhanDuLieuToKhai()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            string password = "";
            if (sendXML.Load())
            {
                // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.WSRequestXML(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK"; ;
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //  if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                                if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }*/

        //---------------------------
        private void NhanPhanLuong()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                //TODO: DoWork(30);
                LayPhanHoiPhanLuong(password);
                //TODO: DoExit();
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //---------------------------

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = OpenFormType.Insert;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();

        }

        private void Send()
        {
            if (TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin trước khi gửi!", "MSG_SEN14", "", false);
                return;
            }
            string password = "";
            this.Cursor = Cursors.Default;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.\nHãy chọn chức năng \"Nhận dữ liệu\" để lấy thông tin phản hồi.", "MSG_SEN03", "", false);

                //Hien thi nut Nhan du lieu
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                return;
            }
            int intHoaDon = TKMD.HoaDonThuongMaiCollection.Count;
            int intHopDong = TKMD.HopDongThuongMaiCollection.Count;
            int intGiayPhep = TKMD.GiayPhepCollection.Count;
            int intCO = TKMD.COCollection.Count;
            int intCTK = TKMD.ChungTuKemCollection.Count;
            int intVanDon = TKMD.VanTaiDon != null ? 1 : 0;
            int intDeNghiCCK = TKMD.listChuyenCuaKhau.Count;
            string thongBao = "Các chứng từ kèm theo tờ khai gồm: \n\t";
            thongBao += intCO + " CO \n\t";
            thongBao += intHoaDon + " Hóa đơn, ";
            thongBao += intHopDong + " Hợp đồng \n\t";
            thongBao += intGiayPhep + " Giấy phép, ";
            thongBao += intVanDon + " Vận đơn \n\t";
            thongBao += intDeNghiCCK + " Đề nghị chuyển cửa khẩu \n\t";
            thongBao += intCTK + " Chứng từ kèm dạng ảnh. \n";
            thongBao += "Bạn có muốn gởi thông tin tới Hải quan không?";
            string kq = "";// MLMessages(thongBao, "", "", true);
            kq = ShowMessage(thongBao, true); //MLMessages(thongBao, "", "", true);
            if (kq == "No")
            {
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                //TODO: DoWork(30);

                //if (MainForm.versionHD != 2)
                //{
                //    try
                //    {
                //        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                //        if (st != "")
                //        {
                //            ShowMessage(st, false);
                //            this.Cursor = Cursors.Default;
                //            return;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        //Logger.LocalLogger.Instance().WriteMessage(ex);
                //    }
                //}


                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    xmlCurrent = TKMD.WSKhaiBaoToKhaiSua(password, GlobalSettings.MaMID);
                }
                else
                {
                    if (TKMD.MaLoaiHinh.StartsWith("NSX"))
                    {
                        xmlCurrent = TKMD.WSSendXMLNHAP(password);
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("XSX"))
                    {
                        xmlCurrent = TKMD.WSSendXMLXuat(password, GlobalSettings.MaMID);
                    }
                }

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                dgList.AllowDelete = InheritableBoolean.False;

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công. Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    //hangdoi hd = new hangdoi();
                            //    //hd.id = tkmd.id;
                            //    //hd.loaitokhai = loaitokhai.to_khai_mau_dich;
                            //    //hd.trangthai = tkmd.trangthaixuly;
                            //    //hd.chucnang = chucnang.khai_bao;
                            //    //hd.password = password;
                            //    //mainform.addtoqueueform(hd);
                            //    //mainform.showqueueform();
                            //}
                            //ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                            //DATLMQ comment ngày 29/03/2011
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                        }
                        else
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Có lỗi khi khai báo Tờ khai : " + msg[0], false, true, ex.ToString());
                            ShowMessage("Có lỗi khi khai báo Tờ khai : " + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString() + " " + ex.StackTrace);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();


            }
            finally
            {
                //TODO: DoExit();

                this.Cursor = Cursors.Default;
            }

        }
        private void LaySoTiepNhanDT()
        {
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_TK06", "", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    //xmlCurrent = TKMD.LayPhanHoi(password, sendXML.msg);
                    xmlCurrent = TKMD.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa nhận được phản hồi từ hệ thống hải quan", "MSG_STN02", "", true);
                    //if (kq == "Yes")
                    //{
                    //    this.Refresh();
                    //    LayPhanHoi(password);
                    //}
                    return;
                }
                if (sendXML.func == 1)
                {
                    sendMailChungTu();
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan, "MSG_SEN05", "" + TKMD.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait for approval";
                    }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    // ShowMessage("Hủy khai báo thành công", false);
                    MLMessages("Hủy khai báo thành công", "MSG_CNL02", "", false);
                    txtSoTiepNhan.Text = "";
                    //lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa khai báo";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not yet declarated";
                    }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        // this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }

                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                        //try
                        //{
                        //    TKMD.TinhToanCanDoiNhapXuat();
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Có lỗi khi cập nhật dữ liệu tồn vào hệ thống : " + ex.Message, false);
                        //}
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        // this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt. ", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                sendXML.Delete();
                                SetCommandStatus();
                            }
                        }
                    }
                    else
                    {
                        // ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);                      
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendMailChungTu()
        {
            this.Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    //ShowMessage("Chưa cấu hình mail của doanh nghiệp", false);
                    MLMessages("Chưa cấu hình mail của doanh nghiệp", "MSG_MAL01", "", false);
                if (GlobalSettings.MailHaiQuan == "")
                    MLMessages("Chưa cấu hình mail của hải quan tiếp nhận", "MSG_MAL02", "", false);
                // ShowMessage("Chưa cấu hình mail của hải quan tiếp nhận", false);
                return;
            }
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8);
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            if (GlobalSettings.NGON_NGU == "0")
            {
                mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString();
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = "Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString(); ;
                mail.Body += "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = false;
                mail.Priority = MailPriority.High;
            }
            else
            {
                mail.Subject = "Electronic Declarate - " + GlobalSettings.MA_DON_VI + "- electronic receipt number:  : " + this.TKMD.SoTiepNhan.ToString();
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = "Declaration form have receipt number: : " + this.TKMD.SoTiepNhan.ToString(); ;
                mail.Body += "Eclectronic Customs - Approved declaration - Voucher ";
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = false;
                mail.Priority = MailPriority.High;
            }

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        Attachment att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (ct.SoBanChinh > 0)
                                mail.Body += "Số bản chính : " + ct.SoBanChinh;
                            if (ct.SoBanSao > 0)
                                mail.Body += "Số bản sao : " + ct.SoBanSao;
                        }
                        else
                        {
                            if (ct.SoBanChinh > 0)
                                mail.Body += "Quantity of main form : " + ct.SoBanChinh;
                            if (ct.SoBanSao > 0)
                                mail.Body += "Quantity of copy form : " + ct.SoBanSao;
                        }
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Credentials = new System.Net.NetworkCredential("haiquandientusoftech@gmail.com", "haiquandientu");
                    smtp.Send(mail);
                    //ShowMessage("Gửi mail thành công.", false);
                    MLMessages("Gửi mail thành công.", "MSG_MAL05", "", false);
                    this.Cursor = Cursors.Default;
                    //this.Close();
                }
                //this.Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                // ShowMessage("Không gửi chứng từ tới hải quan được.", false);
                MLMessages("Không gửi chứng từ tới hải quan được.", "MSG_MAL06", "", false);
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    return;

                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();

                //xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);// LINH CHUYỂN QUA DÙNG HÀM WSRequestPhanLuong(pass)
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string msg = string.Empty;
                    try
                    {
                        XmlDocument docRet = new XmlDocument();
                        docRet.LoadXml(xmlCurrent);
                        msg = "Thông báo từ hải quan: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docRet.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "\n";
                    }
                    catch { }
                    if (string.IsNullOrEmpty(msg))
                        msg = "Chưa có phản hồi từ hải quan.";
                    msg += "\nBạn có muốn tiếp tục lấy xác nhận thông tin không?";
                    string kq = MLMessages(msg, "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";

                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                            TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                            TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                            TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                            TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG, 0);
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                        }
                        else
                        {
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false, true, ex.ToString());
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi : " + ex.Message.ToString(), "MSG_SEN20", "", false);
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString() + " " + ex.StackTrace);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiPhanLuong(string pass)
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(Company.KDT.SHARE.Components.MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string msg = string.Empty;
                    try
                    {
                        XmlDocument docRet = new XmlDocument();
                        docRet.LoadXml(xmlCurrent);
                        msg = "THÔNG BÁO TỪ HỆ THỐNG hải quan: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docRet.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "\n";

                    }
                    catch
                    {
                    }
                    if (string.IsNullOrEmpty(msg))
                    {
                        msg = "Chưa có phản hồi từ hải quan.";
                    }
                    msg += "\nBạn có muốn tiếp tục lấy xác nhận thông tin không?";
                    string kq = MLMessages(msg, "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoiPhanLuong(pass);
                    }
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                sendXML.Delete();

                                string msg = "Tờ khai bị từ chối: " +
                                    Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                //KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);

                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        InAnDinhThue();
                    }
                }



                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void InAnDinhThue()
        {
            try
            {
                Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                    TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                    TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                    TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                    TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG, 0);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void InAnDinhThue(long tkmdID, string guidstr)
        {
            try
            {
                AnDinhThue anDinhThue = new AnDinhThue();
                List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (dsADT.Count > 0)
                {
                    anDinhThue = dsADT[0];
                    anDinhThue.LoadChiTiet();

                    Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                    anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                    anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                    anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                    anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                }
                else
                {
                    //Hungtq updated 21/02/2012.
                    //Không có thông tin Ấn định thuế hoặc Có thông tin ấn định thuế từ kết quả Hải quan trả về nhưng chưa lưu được xuống database.
                    Company.Interface.Globals.AnDinhThue_InsertFromXML(this.TKMD);

                    //InAnDinhThue
                    dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThue = dsADT[0];
                        anDinhThue.LoadChiTiet();

                        Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                        anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                        anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                        anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                        anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDForm f = new ListHopDongTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }

            TKMD.SoHopDong = txtSoHopDong.Text;
            TKMD.NgayHopDong = ccNgayHopDong.Value;
            TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;

            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;

            f.TKMD = TKMD;
            f.ShowDialog(this);

            if (!string.IsNullOrEmpty(f.TKMD.SoHopDong))
            {
                txtSoHopDong.Text = f.TKMD.SoHopDong;
                ccNgayHopDong.Text = f.TKMD.NgayHopDong.ToShortDateString();
                ccNgayHHHopDong.Text = f.TKMD.NgayHetHanHopDong.ToShortDateString();
            }

            //switch (this.NhomLoaiHinh)
            //{
            //    case "NGC":
            //        HopDongRegistedForm f = new HopDongRegistedForm();
            //        f.HopDongSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f.IsBrowseForm = true;
            //        f.ShowDialog(this);
            //        if (f.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f.HopDongSelected;
            //            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f.HopDongSelected.NgayHetHan;
            //            txtTenDonViDoiTac.Text = f.HopDongSelected.DonViDoiTac;
            //        }
            //        break;
            //    case "XGC":
            //        HopDongRegistedForm f1 = new HopDongRegistedForm();
            //        f1.HopDongSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f1.IsBrowseForm = true;
            //        f1.ShowDialog(this);     
            //        if (f1.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f1.HopDongSelected;
            //            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f1.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f1.HopDongSelected.NgayHetHan;
            //        }
            //        break;
            //}
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {

            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            int pos = items[0].Position;
            Company.BLL.KDT.HangMauDich hangmaudich = this.TKMD.HMDCollection[pos];
            #region LanNT thay doan tren bang doan duoi

            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        HangMauDichEditForm f = new HangMauDichEditForm();
            //        f.HMD = this.TKMD.HMDCollection[i.Position];
            //        soDongHang = i.Position + 1;
            //        f.collection = this.TKMD.HMDCollection;
            //        if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
            //        if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
            //        if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
            //        f.LoaiHangHoa = this.TKMD.LoaiHangHoa;
            //        f.NhomLoaiHinh = this.NhomLoaiHinh;
            //        f.MaHaiQuan = this.TKMD.MaHaiQuan;
            //        f.MaNguyenTe = ctrNguyenTe.Ma;
            //        f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            //        this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            //        this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            //        this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            //        f.TKMD = this.TKMD;
            //        if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            //            f.OpenType = OpenFormType.Edit;
            //        else
            //            f.OpenType = OpenFormType.View;
            //        f.ShowDialog(this);
            //        if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            //        {
            //            if (f.IsEdited)
            //            {
            //                if (f.HMD.TKMD_ID > 0)
            //                    f.HMD.Update();
            //                else
            //                    this.TKMD.HMDCollection[i.Position] = f.HMD;
            //            }
            //            else if (f.IsDeleted)
            //            {
            //                if (f.HMD.TKMD_ID > 0)
            //                    f.HMD.Delete();
            //                this.TKMD.HMDCollection.RemoveAt(i.Position);
            //            }
            //        }
            //        dgList.DataSource = this.TKMD.HMDCollection;
            //        if (GlobalSettings.TuDongTinhThue == "1")
            //        {
            //            tinhLaiThue();
            //        }
            //        else
            //        {
            //            tinhTongTriGiaKhaiBao();
            //        }
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch { dgList.DataSource = this.TKMD.HMDCollection; }
            //        setCommandStatus();
            //    }
            //    break;
            //}
            #endregion LanNT thay doan tren bang doan duoi
            HangMauDichForm frmHangMD = new HangMauDichForm(hangmaudich);
            frmHangMD.TKMD = this.TKMD;
            frmHangMD.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            frmHangMD.NhomLoaiHinh = this.NhomLoaiHinh;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                frmHangMD.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 1);
            frmHangMD.MaHaiQuan = this.TKMD.MaHaiQuan;
            frmHangMD.MaNguyenTe = ctrNguyenTe.Ma;
            frmHangMD.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            frmHangMD.ShowDialog(this);
            txtSoLuongPLTK.Text = TKMD.SoLuongPLTK.ToString();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Row.Cells["NuocXX_ID"].Value.ToString().Trim()))
                e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            if (!string.IsNullOrEmpty(e.Row.Cells["DVT_ID"].Value.ToString().Trim()))
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    // ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                    e.Cancel = true;
                    return;
                }
                // if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa thông tin này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    Company.BLL.KDT.HangMauDich hmd = (Company.BLL.KDT.HangMauDich)e.Row.DataRow;

                    //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                    if (checkExistHMD(TKMD.ID, hmd.MaPhu))
                    {
                        ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                        HangMauDichForm.isDeleted = true;
                    }

                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        this.TKMD.Load();
                        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.TKMD.Update();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        public bool checkExistHMD(long _TKMD_ID, string _mahang)
        {
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _mahang);
            List<Company.BLL.KDT.HangMauDich> hmdColl = (List<Company.BLL.KDT.HangMauDich>)Company.BLL.KDT.HangMauDich.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }
        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuForm f = new ChungTuForm();
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            ctDetail.TenChungTu = e.Row.Cells["TenChungTu"].Text;
            ctDetail.SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text);
            ctDetail.SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text);
            //        ctDetail.STTHang = Convert.ToInt16(e.Row.Cells["STTHang"].Text);
            ctDetail.Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text);
            ctDetail.FileUpLoad = e.Row.Cells["FileUpLoad"].Text;
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    this.TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            //   f.MaLoaiHinh = this.TKMD.MaLoaiHinh;
            f.MaLoaiHinh = this.NhomLoaiHinh;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();
        }

        private void gridEX1_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                //ShowMessage("Tờ khai đã được duyệt không được sửa",false);
                MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                e.Cancel = true;
                return;
            }
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            // if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa thông tin này không?", "MSG_DEL01", "", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (true)
            {
                if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    // ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                    e.Cancel = true;
                    return;
                }
                //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.BLL.KDT.HangMauDich hmd = (Company.BLL.KDT.HangMauDich)i.GetRow().DataRow;
                            if (hmd.ID > 0)
                            {

                                /*Xoa cac NPL trong cac chung tu kem theo: Hopdong, Hoa don TM, Giay phep*/
                                /*
                                //Giay phep
                                List<HangGiayPhepDetail> hangGP = HangGiayPhepDetail.SelectCollectionDynamic("HMD_ID = " + hmd.ID, "");

                                if (hangGP.Count > 0)
                                {
                                    hangGP[0].Delete();
                                }

                                //HoaDon thuong mai
                                List<HoaDonThuongMaiDetail> hoadonTM = HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID);
                                if (hoadonTM.Count > 0)
                                {
                                    hoadonTM[0].Delete();
                                }

                                //Hop dong
                                List<HopDongThuongMaiDetail> hopdongTM = HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID);
                                if (hopdongTM.Count > 0)
                                {
                                    hopdongTM[0].Delete();
                                }
                                */

                                if (TKMD.GiayPhepCollection.Count > 0 || TKMD.HopDongThuongMaiCollection.Count > 0 || TKMD.HoaDonThuongMaiCollection.Count > 0)
                                {
                                    this.ShowMessageTQDT("Thông báo", "Thông tin nguyên phụ liệu '" + i.GetRow().Cells["MaPhu"].Text + "'" +
                                    " này đang sử dụng trong các chứng từ: \n Hợp đồng \n Hóa đơn thương mại \n Giấy phép.\n\nKhông thể xóa!.", false);
                                    return;
                                }

                                //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                                if (checkExistHMD(TKMD.ID, hmd.MaPhu))
                                {
                                    ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                                    HangMauDichForm.isDeleted = true;
                                }

                                hmd.Delete();

                                //dgList.Refetch();
                            }
                            else
                                ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                        }
                    }


                    foreach (Company.BLL.KDT.HangMauDich hmdDelete in ToKhaiMauDichForm.deleteHMDCollection)
                    {
                        this.TKMD.HMDCollection.Remove(hmdDelete);
                    }

                    dgList.DataSource = this.TKMD.HMDCollection;

                    try
                    {
                        dgList.Refetch();
                    }
                    catch
                    {
                        dgList.Refresh();
                    }


                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        tinhLaiThue();
                    }
                    else
                    {
                        tinhTongTriGiaKhaiBao();
                    }
                    if (TKMD.ID > 0)
                        this.TKMD.Update();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            SetCommandStatus();
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            this.TKMD.HMDCollection.Clear();
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = this.TKMD.ChungTuTKCollection;

            //Ngonnt 25/02
            f.countHoaDon = TKMD.HoaDonThuongMaiCollection.Count;
            f.countHopDong = TKMD.HopDongThuongMaiCollection.Count;
            f.countVanTai = (TKMD.VanTaiDon != null) ? 1 : 0;
            f.countGiayPhep = TKMD.GiayPhepCollection.Count;
            f.countCO = TKMD.COCollection.Count;
            f.countChuyenCK = TKMD.listChuyenCuaKhau.Count;
            f.countGKT = TKMD.GiayKiemTraCollection.Count;
            //f.countCTNo = TKMD.Ch.Count;
            f.countCTGD = (TKMD.ChungThuGD != null && TKMD.ChungThuGD.ID > 0) ? 1 : 0;
            //Ngonnt 25/02

            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog(this);
            this.TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtSoHopDong_Leave(object sender, EventArgs e)
        {
            if (txtSoHopDong.Text.Trim().Length > 0)
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = false;
            else
            {
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = true;
                ccNgayHopDong.Text = ccNgayHHHopDong.Text = "";
            }
        }

        private void txtSoHoaDonThuongMai_Leave(object sender, EventArgs e)
        {
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                ccNgayHDTM.ReadOnly = false;
            else
            {
                ccNgayHDTM.ReadOnly = true;
                ccNgayHDTM.Text = "";
            }
        }

        private void txtSoVanTaiDon_Leave(object sender, EventArgs e)
        {
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
                ccNgayVanTaiDon.ReadOnly = false;
            else
            {
                ccNgayVanTaiDon.ReadOnly = true;
                ccNgayVanTaiDon.Text = "";

            }
        }

        private void txtSoGiayPhep_Leave(object sender, System.EventArgs e)
        {
            if (txtSoGiayPhep.Text.Trim().Length > 0)
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = false;
            else
            {
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = true;
                ccNgayGiayPhep.Text = ccNgayHHGiayPhep.Text = "";
            }
        }

        private void txtTenDonViDoiTac_ButtonClick(object sender, EventArgs e)
        {
            DonViDoiTacForm f = new DonViDoiTacForm();
            f.isBrower = true;
            f.ShowDialog(this);
            if (f.doiTac != null && f.doiTac.TenCongTy != "")
            {
                txtTenDonViDoiTac.Text = f.doiTac.TenCongTy + ".\r\n " + f.doiTac.DiaChi + "\r\n" + f.doiTac.GhiChu;
            }
        }

        private void ctrNguyenTe_ValueChanged(object sender, EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia + "";
            if (ctrNguyenTe.Ma == "USD")
            {
                txtTyGiaUSD.Text = ctrNguyenTe.TyGia + "";
            }
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }

            TKMD.SoGiayPhep = txtSoGiayPhep.Text;
            TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
            TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;

            f.TKMD = TKMD;
            f.ShowDialog(this);

            if (!string.IsNullOrEmpty(f.TKMD.SoGiayPhep))
            {
                txtSoGiayPhep.Text = f.TKMD.SoGiayPhep;
                ccNgayGiayPhep.Text = f.TKMD.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = f.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            }
        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();

            if (sender != null)
                f.isKhaiBoSung = true;

            TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
            TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;

            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;

            f.TKMD = TKMD;
            f.ShowDialog(this);

            if (!string.IsNullOrEmpty(f.TKMD.SoHoaDonThuongMai))
            {
                txtSoHoaDonThuongMai.Text = f.TKMD.SoHoaDonThuongMai;
                ccNgayHDTM.Text = f.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            }
        }

        private void txtSoVanTaiDon_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.MaLoaiHinh.Substring(0, 1) == "N" && TKMD.ID == 0)
            {
                ShowMessage("Bạn vui lòng nhập thông tin sau trên tờ khai chính, và lưu tờ khai:\r\n- Số vận đơn\n- Ngày vận đơn.\r\n\nLưu ý: Thông tin 'Vận tải đơn chi tiết' chỉ cho phép nhập khi đã Lưu thông tin tờ khai thành công.", false);
                return;
            }

            VanTaiDonForm f = new VanTaiDonForm();
            f.TKMD = TKMD;
            f.IDPhuongThucVanTai = cbPTVT.SelectedValue.ToString();
            f.NhomLoaiHinh = this.NhomLoaiHinh;

            //f.isKhaiBoSung = this.isKhaiBoSung;
            TKMD.SoVanDon = txtSoVanTaiDon.Text;
            TKMD.NgayVanDon = ccNgayVanTaiDon.Value;

            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            TKMD.NgayDenPTVT = ccNgayDen.Value;

            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            TKMD.CuaKhau_ID = ctrCuaKhau.Ma;
            TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;


            f.ShowDialog(this);
            if (TKMD.VanTaiDon != null)
            {
                if (TKMD.VanTaiDon.LoaiVanDon == LoaiVanDon.DUONG_KHONG)
                {
                    txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
                }

                if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";

                // ĐKGH.
                cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
                if (this.TKMD.VanTaiDon.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
                else ccNgayVanTaiDon.Text = "";
                // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
                //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                // Container 20.
                // txtTongSoContainer.Value = this.TKMD.SoContainer20;

                // Container 40.
                // txtSoContainer40.Value = this.TKMD.SoContainer40;
                // linh thêm nước xuất lấy từ vận đơn 14/01/2011
                //  ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
                TKMD.SoLuongContainer.LoadSoContFromVanDon(TKMD.VanTaiDon);
            }
            else
            {
                ccNgayVanTaiDon.Text = string.Empty;
                txtSoVanTaiDon.Text = string.Empty;
            }
            txtTongSoContainer.Value = TKMD.SoLuongContainer != null ? TKMD.SoLuongContainer.TongSoCont() : 0;
        }

        #region Validate Khai bo sung

        /// <summary>
        /// Kiem tra thong tin truoc khi khai bo sung chung tu.
        /// </summary>
        /// <param name="soToKhai"></param>
        /// <returns></returns>
        /// HUNGTQ, Update 07/06/2010.
        private bool ValidateKhaiBoSung(int soToKhai)
        {
            if (soToKhai == 0)
            {
                string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                //Globals.ShowMessageTQDT(msg, false);
                ShowMessage(msg, false);

                return false;
            }

            return true;
        }

        #endregion

        #region Begin ButtonClick TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Phuong tien van tai.
        /// </summary>
        /// Hungtq, Update 13072010
        private void SetEvent_TextBox_VanDon()
        {
            //txtSoHieuPTVT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            //txtSoHieuPTVT.ButtonClick += new EventHandler(txtSoHieuPTVT_ButtonClick2);

            txtSoVanTaiDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_GiayPhep()
        {
            txtSoGiayPhep.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HopDong()
        {
            txtSoHopDong.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HoaDong()
        {
            txtSoHoaDonThuongMai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void txtSoHieuPTVT_ButtonClick2(object sender, EventArgs e)
        {
            Company.Interface.VanTaiDonForm frmVanDon = new VanTaiDonForm();

            if (TKMD != null)
            {
                if (TKMD.MaLoaiHinh.Substring(0, 1) == "N" && TKMD.ID == 0)
                {
                    ShowMessage("Bạn vui lòng nhập thông tin sau trên tờ khai chính, và lưu tờ khai:\r\n- Số hiệu phương tiện vận tải\r\n- Ngày đến\r\n\nLưu ý: Thông tin 'Vận tải đơn chi tiết' chỉ cho phép nhập khi đã Lưu thông tin tờ khai thành công.", false);
                    return;
                }

                TKMD.SoVanDon = txtSoVanTaiDon.Text;
                TKMD.NgayVanDon = ccNgayVanTaiDon.Value;

                TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                TKMD.NgayDenPTVT = ccNgayDen.Value;

                frmVanDon.TKMD = TKMD;

                frmVanDon.ShowDialog(this);

                if (TKMD.VanTaiDon != null)
                {
                    txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                    ccNgayDen.Text = TKMD.VanTaiDon.NgayDenPTVT.ToShortDateString();

                    if (txtSoVanTaiDon.Text != "")
                    {
                        if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số vận đơn' không?.", true) == "Yes")
                        {
                            txtSoVanTaiDon.Text = TKMD.VanTaiDon.SoVanDon;
                            ccNgayVanTaiDon.Text = TKMD.NgayVanDon.ToShortDateString();
                        }
                    }
                }

            }
        }

        #endregion

        private void btnNoiDungDieuChinhTKForm_Click(object sender, EventArgs e)
        {
            try
            {
                NoiDungChinhSuaTKForm frmNoiDungSua = new NoiDungChinhSuaTKForm();
                frmNoiDungSua.TKMD = this.TKMD;
                frmNoiDungSua.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
                DialogResult ret = frmNoiDungSua.ShowDialog(this);
            }
            catch { }
        }

        #region Send V3 Create by LANNT
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackMessageV3(TKMD);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.DUYET_LUONG_CHUNG_TU && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetCommandStatus();
                }
            }
        }
        private void FeedBackV3(string DieuKienPhanHoi)
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend;
                if (string.IsNullOrEmpty(DieuKienPhanHoi))
                    msgSend = SingleMessage.FeedBackMessageV3(TKMD);
                else
                    msgSend = SingleMessage.FeedBack(TKMD, TKMD.GUIDSTR, DieuKienPhanHoi);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        if (string.IsNullOrEmpty(DieuKienPhanHoi))
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        else
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(DieuKienPhanHoi))
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        else
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetCommandStatus();
                }
                isFeedBack = false;
            }
        }
        private void CancelV3()
        {

            ObjectSend msgSend = SingleMessage.CancelMessageV3(TKMD);


            SendMessageForm dlgSendForm = new SendMessageForm();

            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend);
            dlgSendForm.Message.XmlSaveMessage(TKMD.ID, MessageTitle.HuyKhaiBaoToKhai);
            if (isSend) FeedBackV3();

        }
        private void SendV3()
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TKMD.Load();
                    TKMD.LoadChungTuHaiQuan();
                }

                //if (GlobalSettings.IsDaiLy && !GlobalSettings.ISKHAIBAO && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                //    this.ShowMessage("Đại lý chưa đồng bộ tờ khai cho doanh nghiệp. Số tờ khai tối đa đại lý được khai là: " + GlobalSettings.SOTOKHAI_DONGBO.ToString(), false);
                //    return;
                //}

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = TKMD.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }

                if (TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET && !TKMD.MaLoaiHinh.Contains("V"))
                {
                    if (!TKMD.MaLoaiHinh.Contains("V"))
                    {
                        string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                        ShowMessage(msg, false);
                        return;
                    }
                }
                else
                {
                    #region  Kiêm tra trùng tờ khai
                    try
                    {
                        decimal _soLuongHang = 0;
                        foreach (Company.BLL.KDT.HangMauDich item in TKMD.HMDCollection)
                        {
                            _soLuongHang = _soLuongHang + item.SoLuong;
                        }
                        long soToKhai = TrungToKhai.CheckSoToKhaiTrung(TKMD.SoToKhai, TKMD.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoGiayPhep, TKMD.SoVanDon, TKMD.SoHoaDonThuongMai, TKMD.SoHopDong, TKMD.SoKien, TKMD.TrongLuong, _soLuongHang, TKMD.HMDCollection.Count);
                        if (soToKhai > 0)
                        {
                            string notice = string.Format("Tờ khai này đang trùng với tờ khai {0}/{1}/{2}", soToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan);
                            string warning = string.Format("\r\nChú ý: Người dùng cố tình khai báo lặp lại tờ khai sẽ phải chịu xử phạt hành chính theo quy định hiện hành của HQ");
                            if (ShowMessageTQDT(notice + warning + "\r\nBạn có muốn tiếp tục khai báo ?", true) != "Yes")
                                return;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    #endregion

                    ObjectSend msgSend = SingleMessage.SendMessageV3(TKMD);
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    bool isSend = dlgSendForm.DoSend(msgSend);

                    //Lưu ý phải để dòng code lưu message sau kết quả trả về.
                    dlgSendForm.Message.XmlSaveMessage(TKMD.ID, TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);

                    //TODO: Hungtq updated 6/2/2013. SendV3()()
                    //Luu version, co su dung CKS tren to khai khi da gui duoc to khai den HQ.
                    if (isSend)
                        SaveVersion();

                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                        sendXML.master_id = TKMD.ID;
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        TKMD.Update();
                        FeedBackV3();
                        SetCommandStatus();
                        NhanDuLieu2.Enabled = NhanDuLieu.Enabled = XacNhan2.Enabled = XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);

            this.Invoke(new MethodInvoker(
                delegate
                {
                    if (this.TKMD.SoTiepNhan > 0)
                        txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    if (this.TKMD.SoToKhai > 0)
                        txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                    SetCommandStatus();
                }));
        }
        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandler(TKMD, ref msgInfor, sender, e);

        }

        #endregion

        /// <summary>
        /// Hien thi thong tin so luong chung tu dinh kem tren menu 'Chung tu dinh kem'. 
        /// Updaetd by Hungtq, 24/09/2012.
        /// </summary>
        /// <param name="tkmd"></param>
        private void SetStatusChungTuDinhKem(Company.BLL.KDT.ToKhaiMauDich tkmd)
        {
            try
            {
                this.cmdVanDon1.ImageIndex = (tkmd.VanTaiDon != null ? 11 : 0);

                //Kiem tra thong tin co phai la Bo sung khong?
                string query;
                query = string.Format("TKMD_ID = {0} AND LOAIKB = '0'", TKMD.ID);

                this.cmdHopDong1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai.KiemTraChungTuCoBoSung(tkmd.HopDongThuongMaiCollection) == false) == true ? 11 : 0;

                this.cmdGiayPhep1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayPhep.KiemTraChungTuCoBoSung(tkmd.GiayPhepCollection) == false) == true ? 11 : 0;

                this.cmdHoaDonThuongMai1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai.KiemTraChungTuCoBoSung(tkmd.HoaDonThuongMaiCollection) == false) == true ? 11 : 0;

                this.cmdCO1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.CO.KiemTraChungTuCoBoSung(tkmd.COCollection) == false) == true ? 11 : 0;

                this.cmdChuyenCuaKhau1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.KiemTraChungTuCoBoSung(tkmd.listChuyenCuaKhau) == false) == true ? 11 : 0;

                this.cmdChungTuDangAnh1.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.KiemTraChungTuCoBoSung(tkmd.ChungTuKemCollection) == false) == true ? 11 : 0;
                this.cmdChungTuDangAnh1.Visible = Janus.Windows.UI.InheritableBoolean.True;

                this.cmdChungTuNo1.ImageIndex = (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuNo.KiemTraChungTuCoBoSung(tkmd.ChungTuNoCollection) == false) == true ? 11 : 0;

                this.GiayKiemTra1.ImageIndex = (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.KiemTraChungTuCoBoSung(tkmd.GiayKiemTraCollection) == false) == true ? 11 : 0;

                this.ChungThuGiamDinh1.ImageIndex = (tkmd.ChungThuGD != null && tkmd.ChungThuGD.ListHang.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh.KiemTraChungTuCoBoSung(tkmd.ChungThuGD) == false) == true ? 11 : 0;

                //Chung tu dinh kem Bo sung
                this.cmdVanDonBoSung1.Visible = Janus.Windows.UI.InheritableBoolean.False;

                this.CO1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.CO.KiemTraChungTuCoBoSung(tkmd.COCollection) == true) == true ? 11 : 0;

                this.cmdGiayPhepBoSung1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayPhep.KiemTraChungTuCoBoSung(tkmd.GiayPhepCollection) == true) == true ? 11 : 0;

                this.cmdHopDongBoSung1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai.KiemTraChungTuCoBoSung(tkmd.HopDongThuongMaiCollection) == true) == true ? 11 : 0;

                this.cmdHoaDonThuongMaiBoSung1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai.KiemTraChungTuCoBoSung(tkmd.HoaDonThuongMaiCollection) == true) == true ? 11 : 0;

                this.cmdChuyenCuaKhauBoSung1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.KiemTraChungTuCoBoSung(tkmd.listChuyenCuaKhau) == true) == true ? 11 : 0;

                this.cmdChungTuDangAnhBS.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.KiemTraChungTuCoBoSung(tkmd.ChungTuKemCollection) == true) == true ? 11 : 0;
                this.cmdChungTuDangAnhBS.Visible = Janus.Windows.UI.InheritableBoolean.True;

                this.cmdGiayNopTien1.ImageIndex = (tkmd.GiayNopTiens != null && tkmd.GiayNopTiens.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayNopTien.KiemTraChungTuCoBoSung(tkmd.GiayNopTiens) == true) == true ? 11 : 0;

                this.BoSungGiayKiemTra1.ImageIndex = (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.KiemTraChungTuCoBoSung(tkmd.GiayKiemTraCollection) == true) == true ? 11 : 0;

                this.BoSungChungThuGiamDinh1.ImageIndex = (tkmd.ChungThuGD != null && tkmd.ChungThuGD.ID > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh.KiemTraChungTuCoBoSung(tkmd.ChungThuGD) == true) == true ? 11 : 0;

                //To khai tri gia PP1, PP2, PP3

                //this.cmdToKhaiTriGiaPP1.ImageIndex = (tkmd.TKTGCollection != null && tkmd.TKTGCollection.Count > 0) == true ? 11 : 0;

                //this.cmdToKhaiTriGiaPP2.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 11 : 0;

                //this.cmdToKhaiTriGiaPP3.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 11 : 0;

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private bool KiemTraChungTuCoBoSung(List<ChungTuKem> list)
        {
            return false;
        }

        private void chkAnHangThue_CheckedChanged(object sender, EventArgs e)
        {
            txtAnHanThue_ThoiGian.Enabled = txtAnHanThue_LyDo.Enabled = chkAnHangThue.Checked;
        }

        private void chkDamBaoNopThue_isValue_CheckedChanged(object sender, EventArgs e)
        {
            txtDamBaoNopThue_HinhThuc.Enabled = txtDamBaoNopThue_TriGia.Enabled = ccDamBaoNopThue_NgayBatDau.Enabled = ccDamBaoNopThue_NgayKetThuc.Enabled = chkDamBaoNopThue_isValue.Checked;
        }

        private void chkGiamThue_isValue_CheckedChanged(object sender, EventArgs e)
        {
            // txtGiamThue_SoVanBan.Enabled = txtGiamThue_ThueSuatGoc.Enabled = txtGiamThue_TyLeGiam.Enabled = chkGiamThue_isValue.Checked;
        }

        private void txtLePhiHQ_Click(object sender, EventArgs e)
        {
            LePhiHQ f = new LePhiHQ();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            this.TKMD.LePhiHQCollection = f.TKMD.LePhiHQCollection;
            decimal tongLePhi = 0;
            if (TKMD.LePhiHQCollection != null && TKMD.LePhiHQCollection.Count > 0)
            {
                foreach (Company.KDT.SHARE.QuanLyChungTu.LePhiHQ lephi in TKMD.LePhiHQCollection)
                {
                    tongLePhi += lephi.SoTienLePhi;
                }
            }
            txtLePhiHQ.Value = tongLePhi;
        }
        private void txtSoContainer20_Click(object sender, EventArgs e)
        {
            ListContainerTKMDForm f = new ListContainerTKMDForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            txtTongSoContainer.Value = TKMD.SoLuongContainer != null ? TKMD.SoLuongContainer.TongSoCont() : 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Visible = !pictureBox1.Visible;
        }

        private void txtSoTKGiay_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

