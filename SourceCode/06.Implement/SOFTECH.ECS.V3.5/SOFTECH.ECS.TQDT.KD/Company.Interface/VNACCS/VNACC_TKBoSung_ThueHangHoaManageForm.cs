﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TKBoSung_ThueHangHoaManageForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKSuaBoSung = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
        public List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> listTKBoSung = new List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung>();
        public string where = "";
        public VNACC_TKBoSung_ThueHangHoaManageForm()
        {
            InitializeComponent();
        }

        private void VNACC_GiayPhep_TheoDoi_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.Text = "Theo dõi Tờ khai sửa đổi bổ sung thuế hàng hóa - AMA";
                cbbPhanLoaiXuatNhap.SelectedIndex = 0;
                ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
                cbbTrangThai.SelectedIndex = 0;
                //Nhom xu ly ho so
                this.grdList.RootTable.Columns["NhomXuLyHoSoID"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection vlNhomXuLyHoSo = this.grdList.RootTable.Columns["NhomXuLyHoSoID"].ValueList;
                System.Data.DataView view = VNACC_Category_CustomsSubSection.SelectAll().Tables[0].DefaultView;
                view.Sort = "CustomsSubSectionCode ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    vlNhomXuLyHoSo.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"].ToString(), row["Name"].ToString()));
                }

                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void CopyTKBS( KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkbs, bool isCopyHangHoa)
        {
            try
            {
                KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkCopy = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
                HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich_KhaiBoSung>(tkCopy, tkbs, false);
                tkCopy.SoToKhaiBoSung = 0;
                tkCopy.TrangThaiXuLy = 0;
                tkCopy.InputMessageID = string.Empty;
                tkCopy.MessageTag = string.Empty;
                tkCopy.IndexTag = string.Empty;

                if (!isCopyHangHoa)
                {
                    tkCopy.HangCollection = new List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue>();
                }
                VNACC_TKBoSung_ThueHangHoaForm frm = new VNACC_TKBoSung_ThueHangHoaForm();
                frm.TKBoSung = tkCopy;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);

                VNACC_TKBoSung_ThueHangHoaForm f = new VNACC_TKBoSung_ThueHangHoaForm();
                f.TKBoSung = getGiayPhepID(id);
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private KDT_VNACC_ToKhaiMauDich_KhaiBoSung getGiayPhepID(long id)
        {
            try
            {
                listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionAll();

                foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung gp in listTKBoSung)
                {
                    if (gp.ID == id)
                    {
                        gp.LoadHangCollection();

                        foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hang in gp.HangCollection)
                        {
                            hang.LoadHangThuKhacCollection();
                        }

                        return gp;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return null;
            }
        }

        private void grdList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                TKSuaBoSung = getGiayPhepID(id);
                if (ShowMessage("Bạn có muốn xóa tờ khai sửa đổi bổ sung này không?", true) == "Yes")
                {
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hang in TKSuaBoSung.HangCollection)
                    {
                        hang.Delete();
                    }
                    TKSuaBoSung.Delete();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTKBS = txtSoToKhaiBS.Text.Trim();

                string phanloaiXNK = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "0" : cbbTrangThai.SelectedValue.ToString().Trim();
                string haiQuan = ctrMaHaiQuan.Code;
              /*  string loaihinh = ctrLoaiHinhMauDich.Code;*/
                where = String.Empty;
                if (trangthai != "")
                    where = " trangthaixuly =" + trangthai ;
                
                if (soTKBS != "")
                    if(soTKBS.Substring(0,1).Equals("7"))
                        where = where + " and SoToKhaiBoSung = " + soTKBS;
                    else
                        where = where + " and SoToKhai = " + soTKBS;

                if (phanloaiXNK != "")
                    where = where + " and MaLoaiHinh = '" + phanloaiXNK +"'" ;
                //if(haiQuan!="")
                //    where = where + " and CoQuanHaiQuan = '" + haiQuan+"'";
//                 if(loaihinh!="")
//                     where = where + " and MaLoaiHinh = '" + loaihinh;

                listTKBoSung.Clear();
                if (where == "")
                {
                    listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionAll();
                }
                else
                {
                    listTKBoSung = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionDynamic(where, "ID");
                }
                grdList.DataSource = listTKBoSung;
                grdList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }
        }

        private void cbbTrangThai_SelectedItemChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void cbbPhanLoaiXuatNhap_SelectedItemChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

       

        private void mnItem_Tokhai_Click(object sender, EventArgs e)
        {
            if (grdList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grdList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkbs = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
                    tkbs = getGiayPhepID(id);
                    CopyTKBS(tkbs, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void mnItem_ToKhai_Hang_Click(object sender, EventArgs e)
        {
            if (grdList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(grdList.SelectedItems[0].GetRow().Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkbs = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
                    tkbs = getGiayPhepID(id);
                    CopyTKBS(tkbs, true);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách tờ khai TIA_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách hàng hóa không ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grdList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoThuTuDongHangTrenToKhaiGoc", ColumnType.Text, EditType.NoEdit) { Caption = "STT dòng hàng" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MoTaHangHoaTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Mô tả hàng hóa trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MoTaHangHoaSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Mô tả hàng hóa sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaSoHangHoaTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS trước khi KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaSoHangHoaSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS sau khi KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaNuocXuatXuTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Nước xuất xứ trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaNuocXuatXuSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Nước xuât xứ sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá tính thuế sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoLuongTinhThueTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoLuongTinhThueSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaDonViTinhSoLuongTinhThueTruocKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền thuế trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền thuê sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiMienThueTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị MT trước KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiMienThueSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị MT sau KBS" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiSoTienTangGiamThueXuatNhapKhau", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị số tiền tăng giảm thuế" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienTangGiamThue", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền tăng giảm" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "TGTT trước khi KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("TriGiaTinhThueSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "TGTT sau khi KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoLuongTinhThueSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã áp dụng TS trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("MaApDungThueSuatSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Mã áp dụng TS sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("ThueSuatSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Thuế suất sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueTruocKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienThueSauKhiKhaiBoSungThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị MT trước KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiMienThueVaThuKhacSauKhiKhaiBoSung", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị MT sau KBS và Thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("HienThiSoTienTangGiamThueVaThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Hiển thị số tiền tăng giảm thuế và thu khác" });
                        grdList.Tables[0].Columns.Add(new GridEXColumn("SoTienTangGiamThuKhac", ColumnType.Text, EditType.NoEdit) { Caption = "Số tiền tăng giảm thuế và thu khác" });

                        grdList.DataSource = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.ThongKeToKhaiAMA(where, "").Tables[0];
                        gridEXExporter1.GridEX = grdList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        grdList.Tables[0].Columns.Remove("SoThuTuDongHangTrenToKhaiGoc");
                        grdList.Tables[0].Columns.Remove("MoTaHangHoaTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MoTaHangHoaSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaSoHangHoaTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaSoHangHoaSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaNuocXuatXuTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaNuocXuatXuSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("TriGiaTinhThueTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("TriGiaTinhThueSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("SoLuongTinhThueTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("SoLuongTinhThueSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaDonViTinhSoLuongTinhThueTruocKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("ThueSuatTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("ThueSuatSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("SoTienThueTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("SoTienThueSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("HienThiMienThueTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("HienThiMienThueSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("HienThiSoTienTangGiamThueXuatNhapKhau");
                        grdList.Tables[0].Columns.Remove("SoTienTangGiamThue");
                        grdList.Tables[0].Columns.Remove("TriGiaTinhThueTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("TriGiaTinhThueSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("SoLuongTinhThueTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("SoLuongTinhThueSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("MaApDungThueSuatTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("MaApDungThueSuatSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("ThueSuatTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("ThueSuatSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("SoTienThueTruocKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("SoTienThueSauKhiKhaiBoSungThuKhac");
                        grdList.Tables[0].Columns.Remove("HienThiMienThueVaThuKhacTruocKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("HienThiMienThueVaThuKhacSauKhiKhaiBoSung");
                        grdList.Tables[0].Columns.Remove("HienThiSoTienTangGiamThueVaThuKhac");
                        grdList.Tables[0].Columns.Remove("SoTienTangGiamThuKhac");

                        grdList.Refresh();
                        btnTimKiem_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

    }
}
