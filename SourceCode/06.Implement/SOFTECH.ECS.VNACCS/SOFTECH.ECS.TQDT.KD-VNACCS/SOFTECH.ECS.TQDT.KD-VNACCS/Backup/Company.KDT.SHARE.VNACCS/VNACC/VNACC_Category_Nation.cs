using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Nation 
	{
        protected static List<VNACC_Category_Nation> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_Nation> collection = new List<VNACC_Category_Nation>();
            while (reader.Read())
            {
                VNACC_Category_Nation entity = new VNACC_Category_Nation();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NationCode"))) entity.NationCode = reader.GetString(reader.GetOrdinal("NationCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CountryShortName"))) entity.CountryShortName = reader.GetString(reader.GetOrdinal("CountryShortName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_Nation> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [NationCode], CountryShortName FROM [dbo].[t_VNACC_Category_Nation]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }
        public static string GetID(string id)
        {
            try
            {
                string spName = string.Format("SELECT [NationCode] FROM [dbo].[t_VNACC_Category_Nation] WHERE NationCode='{0}'",id);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
                IDataReader reader = db.ExecuteReader(dbCommand);
                if (reader.Read())
                {
                    return reader["NationCode"].ToString();
                }
                return string.Empty;
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }


        public static List<VNACC_Category_Nation> SelectCollectionBy(List<VNACC_Category_Nation> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_Nation o)
            {
                return o.TableID.Contains(tableID);
            });
        }
	}	
}