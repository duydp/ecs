﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using System.Linq;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;

namespace Company.BLL.DataTransferObjectMapper
{
    /// <summary>
    /// Ánh xạ từ BOs(Bussines object)  sang DTOs (Data Transfer Objects)    
    /// </summary>
    public class Mapper
    {
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtYear = "yyyy";
        /// <summary>
        /// Chuyển dữ liệu từ SanPhamDangKy BO sang SXXK_SP DTO/ Huy Du Lieu Da Duoc Duyet
        /// </summary>
        /// <param name="sanphamdangky">SanPhamDangKy</param>
        /// <returns>SXXK_SP</returns>
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_SXXK_SP(SanPhamDangKy sanphamdangky, bool Huy)
        {
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.SXXK_SP,

                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = sanphamdangky.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangky.SoTiepNhan.ToString(),
                Acceptance = sanphamdangky.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = sanphamdangky.MaDoanhNghiep },
                Product = new List<Product>()
            };
            if (Huy)
                sanpham.Function = DeclarationFunction.HUY;
            else
            {
                sanpham.Function = DeclarationFunction.KHAI_BAO;
                sanpham.Agents.Add(new Agent
                {
                    Name = "??",
                    Identity = sanphamdangky.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                });
                if (sanphamdangky.SPCollection != null)
                    foreach (SanPham item in sanphamdangky.SPCollection)
                    {
                        sanpham.Product.Add(new Product
                        {
                            Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                            GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) : item.DVT_ID }
                        });
                    }
            }
            return sanpham;
        }
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDataTransferObject_DNCX_SP(SanPhamDangKy sanphamdangky, string Ghichu, int LoaiChungTu, string TenDonVi)
        {
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.DNCX_HANG_VAO,//LoaiChungTu.ToString(),

                Reference = sanphamdangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",

                DeclarationOffice = sanphamdangky.MaHaiQuan,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangky.SoTiepNhan.ToString(),
                Acceptance = sanphamdangky.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                //IncomingGoodsItem = new List<Product>(),
                Product = new List<Product>(),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            if (LoaiChungTu == 502)
                sanpham.Exporter = new NameBase
                {
                    Name = TenDonVi,
                    Identity = sanphamdangky.MaDoanhNghiep
                };
            else
                sanpham.Importer = new NameBase 
                { 
                    Name = TenDonVi, 
                    Identity = sanphamdangky.MaDoanhNghiep 
                };

            sanpham.Function = DeclarationFunction.KHAI_BAO;
            sanpham.Agents.Add(new Agent
            {
                Name = TenDonVi,
                Identity = sanphamdangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (sanphamdangky.SPCollection != null)
                switch (LoaiChungTu)
                {
                    case 501:
                        sanpham.IncomingGoodsItem = new List<Product>();
                        foreach (SanPham item in sanphamdangky.SPCollection)
                        {
                            sanpham.IncomingGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity 
                                { 
                                    Identification = item.Ma, 
                                    Description = item.Ten,
                                    TariffClassification = item.MaHS,
                                    Type = item.LoaiSP, Usage = item.MucDich 
                                },
                                GoodsMeasure = new GoodsMeasure 
                                {
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID), 
                                }
                            });
                        }
                        break;
                    case 502:
                        sanpham.OutgoingGoodsItem = new List<Product>();
                        foreach (SanPham item in sanphamdangky.SPCollection)
                        {
                            sanpham.OutgoingGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity 
                                { 
                                    Identification = item.Ma,
                                    Description = item.Ten,
                                    TariffClassification = item.MaHS,
                                    Type = item.LoaiSP,
                                    Usage = item.MucDich 
                                },
                                GoodsMeasure = new GoodsMeasure 
                                {
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) 
                                }
                            });
                        }
                        break;
                    case 510:
                        sanpham.CustomsGoodsItem = new List<Product>();
                        foreach (SanPham item in sanphamdangky.SPCollection)
                        {
                            sanpham.CustomsGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, Type = item.LoaiSP },
                                GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, Tariff = item.SoLuong.ToString() }
                            });
                        }
                        break;
                    case 512:
                        sanpham.CustomsGoodsItem = new List<Product>();
                        sanpham.time = DateTime.Now.ToString(sfmtDateTime);
                        foreach (SanPham item in sanphamdangky.SPCollection)
                        {
                            sanpham.CustomsGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity 
                                {
                                    Identification = item.Ma, 
                                    Description = item.Ten, 
                                    Type = item.LoaiSP 
                                },
                                GoodsMeasure = new GoodsMeasure 
                                { 
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID),
                                    /* Tariff = item.SoLuong.ToString(),*/ 
                                    Quantity = Helpers.FormatNumeric(item.SoLuong, 6) }
                            });
                        }
                        break;
                    case 513:
                        sanpham.CustomsGoodsItem = new List<Product>();
                        sanpham.time = DateTime.Now.ToString(sfmtDateTime);
                        foreach (SanPham item in sanphamdangky.SPCollection)
                        {
                            sanpham.CustomsGoodsItem.Add(new Product
                            {
                                Commodity = new Commodity { Identification = item.Ma, Description = item.Ten },
                                GoodsMeasure = new GoodsMeasure { MeasureUnit = item.DVT_ID, /* Tariff = item.SoLuong.ToString(),*/ Quantity = Helpers.FormatNumeric(item.SoLuong, 6) }
                            });
                        }
                        break;


                }
            sanpham.AdditionalInformations.Add(new AdditionalInformation
            {
                Content = new Content { Text = Ghichu }
            });

            return sanpham;
        }
        public static Company.KDT.SHARE.Components.SXXK_SanPham ToDaTaTransferObject_SXXK_SP_SUA(SanPhamDangKySUA sanphamdangkySUA, List<SanPhamSUA> listSP)
        {
            Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham
            {
                Issuer = DeclarationIssuer.SXXK_SP,
                Function = DeclarationFunction.SUA,
                Reference = sanphamdangkySUA.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = sanphamdangkySUA.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = sanphamdangkySUA.SoTiepNhan.ToString(),
                Acceptance = sanphamdangkySUA.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "??", Identity = sanphamdangkySUA.MaDoanhNghiep },
                Product = new List<Product>()
            };
            sanpham.Agents.Add(new Agent
            {
                Name = "??",
                Identity = sanphamdangkySUA.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listSP != null)
                foreach (SanPhamSUA item in listSP)
                {
                    sanpham.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) : item.DVT_ID }
                    });
                }
            return sanpham;
        }
        /// <summary>
        /// Chuyển dữ liệu từ NguyenPhuLieuDangKy BO sang SXXK_NPL DTO
        /// </summary>
        /// <param name="nguyenphulieudangky">NguyenPhuLieuDangKy</param>
        /// <returns>SXXK_NPL</returns>
        public static Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu ToDataTransferObject_SXXK_NPL(NguyenPhuLieuDangKy npldangky, bool isCancel, string tenDN)
        {
            bool isEdit = npldangky.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu nguyenphulieu = new Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu
            {
                
                Issuer = DeclarationIssuer.SXXK_NPL,
                Reference = npldangky.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = isEdit || isCancel ? Helpers.FormatNumeric(npldangky.SoTiepNhan) : String.Empty,
                Acceptance = isEdit || isCancel ? npldangky.NgayTiepNhan.ToString(sfmtDateTime) : String.Empty,
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(npldangky.MaHaiQuan.Trim()) : npldangky.MaHaiQuan,

                Agents = new List<Agent>(),
                Importer = new NameBase 
                { 
                    Name = tenDN,
                    Identity = npldangky.MaDoanhNghiep 
                },
                Product = new List<Product>(),
            };
            if (isCancel)
            {
                nguyenphulieu.Function = DeclarationFunction.HUY;
            }
            else if (isEdit)
            {
                nguyenphulieu.Function = DeclarationFunction.SUA;
            }
            else
            {
                nguyenphulieu.Function = DeclarationFunction.KHAI_BAO;
            }
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = npldangky.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (npldangky.NPLCollection != null)
                foreach (NguyenPhuLieu item in npldangky.NPLCollection)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity 
                        {
                            Description = item.Ten, 
                            Identification = item.Ma, 
                            TariffClassification = item.MaHS 
                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) : item.DVT_ID 
                        }
                    });
                }
            return nguyenphulieu;
        }
        public static Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu ToDaTaTransferObject_SXXK_NPL_SUA(NguyenPhuLieuDangKySUA npldangkysua, List<NguyenPhuLieuSUA> listNPLSua, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu nguyenphulieu = new Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Issuer = DeclarationIssuer.SXXK_NPL,
                Function = DeclarationFunction.SUA,
                Reference = npldangkysua.GUIDSTR,
                IssueLocation = "",
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(npldangkysua.MaHaiQuan).Trim() : npldangkysua.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(npldangkysua.SoTiepNhan),
                Acceptance = npldangkysua.NgayTiepNhan.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = TenDoanhNghiep, Identity = npldangkysua.MaDoanhNghiep },
                Product = new List<Product>(),
            };
            nguyenphulieu.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = npldangkysua.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
            });
            if (listNPLSua != null)
                foreach (NguyenPhuLieuSUA item in listNPLSua)
                {
                    nguyenphulieu.Product.Add(new Product
                    {
                        Commodity = new Commodity { Identification = item.Ma, Description = item.Ten, TariffClassification = item.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(item.DVT_ID.Trim()) : item.DVT_ID }
                    });
                }
            return nguyenphulieu;
        }

        /// <summary>
        /// Chuyển dữ liệu từ DinhMucDangKy BO sang SXXK_DinhMucSP DTO
        /// </summary>
        /// <param name="dinhmucdangky">DinhMucDangKy</param>
        /// <returns>SXXK_DinhMucSP</returns>
        /// 

        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc(KDT_SXXK_DinhMucThucTeDangKy dmdk, bool isCancel, bool isEdit, string tenDN)
        {
            //dmdk.MaHaiQuan = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan) : dmdk.MaHaiQuan;
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {
                Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                Reference = dmdk.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan, 0),
                Acceptance = dmdk.NgayTiepNhan.ToString(Globals.LaDNCX ? sfmtDate : sfmtDateTime),
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan).Trim() : dmdk.MaHaiQuan,
                Agents = new List<Agent>(),
                Importer = new NameBase
                {
                    Name = tenDN,
                    Identity = dmdk.MaDoanhNghiep,
                },
                ProductionNormType = "1",
                ProductionNorm = new List<ProductionNorm>(),
            };
            if (Globals.LaDNCX)
                dm.Issuer = DeclarationIssuer.DNCX_DINHMUC_SANPHAM;
            else
                dm.Issuer = DeclarationIssuer.SXXK_DINH_MUC;

            if (isCancel) dm.Function = DeclarationFunction.HUY;
            else if (isEdit) dm.Function = DeclarationFunction.SUA;
            else dm.Function = DeclarationFunction.KHAI_BAO;

            dm.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            try
            {
                List<ProductionNorm> products = dm.ProductionNorm;
                foreach (KDT_SXXK_DinhMucThucTe_SP dmSP in dmdk.SPCollection)
                {
                    ProductionNorm proNorm = new ProductionNorm
                    {
                        Product = new Product
                        {
                            Commodity = new Commodity
                            {
                                Identification = dmSP.MaSanPham,
                                Description = dmSP.TenSanPham,
                                TariffClassification = dmSP.MaHS,
                                ProductCtrlNo = dmdk.LenhSanXuat_ID.ToString(),

                            },
                            GoodsMeasure = new GoodsMeasure
                            {
                                MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(dmSP.DVT_ID) : dmSP.DVT_ID
                            }
                        },
                        MaterialsNorms = new List<MaterialsNorm>(),
                    };
                    List<MaterialsNorm> MaterialsNorms = new List<MaterialsNorm>();
                    foreach (KDT_SXXK_DinhMucThucTe_DinhMuc items in dmSP.DMCollection)
                    {
                        MaterialsNorm entity = new MaterialsNorm() { Material = new Product() { Commodity = new Commodity(), GoodsMeasure = new GoodsMeasure() } };
                        entity.Material.Commodity.TariffClassification = items.MaHS;
                        entity.Material.GoodsMeasure.MeasureUnit = items.DVT_NPL;
                        entity.Material.Commodity.Identification = items.MaNPL;
                        entity.Material.Commodity.Description = items.TenNPL;
                        entity.Description = items.GhiChu;
                        entity.Norm = Helpers.FormatNumeric(items.DinhMucSuDung, 8);
                        entity.Loss = Helpers.FormatNumeric(items.TyLeHaoHut, 4);
                        entity.RateExchange = "1";
                        MaterialsNorms.Add(entity);
                    }
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        foreach (MaterialsNorm m in MaterialsNorms)
                        {
                            m.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(m.Material.GoodsMeasure.MeasureUnit.Trim());
                        }
                    }
                    products.Add(proNorm);
                }

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return dm;
        }
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc(DinhMucDangKy dmdk, bool isCancel, bool isEdit, string tenDN)
        {
            //dmdk.MaHaiQuan = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan) : dmdk.MaHaiQuan;
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {
                Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(dmdk.SoTiepNhan, 0),
                Acceptance = dmdk.NgayTiepNhan.ToString(Globals.LaDNCX ? sfmtDate : sfmtDateTime),
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan).Trim() : dmdk.MaHaiQuan,
                Agents = new List<Agent>(),
                Importer = new NameBase 
                { 
                    Name = tenDN, 
                    Identity = dmdk.MaDoanhNghiep  ,
                    //ProductionNormType = dmdk.LoaiDinhMuc.ToString(),
                },
                ProductionNormType = dmdk.LoaiDinhMuc.ToString(),
                ProductionNorm = new List<ProductionNorm>(),
            };
            if (Globals.LaDNCX)
                dm.Issuer = DeclarationIssuer.DNCX_DINHMUC_SANPHAM;
            else
                dm.Issuer = DeclarationIssuer.SXXK_DINH_MUC;

            if (isCancel) dm.Function = DeclarationFunction.HUY;
            else if (isEdit) dm.Function = DeclarationFunction.SUA;
            else dm.Function = DeclarationFunction.KHAI_BAO;

            dm.Agents.Add(new Agent
            {
                Name = tenDN,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            IEnumerable<DinhMuc> DMCollection = dmdk.DMCollection.ToArray().Distinct(new
            DistinctMaSP());
            try
            {

            foreach (DinhMuc dmSP in DMCollection)
            {
                Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK = Company.BLL.SXXK.SanPham.getSanPham(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmSP.MaSanPham);
                ProductionNorm proNorm = new ProductionNorm
                {
                    Product = new Product
                    {
                        Commodity = new Commodity 
                        { 
                            Identification = dmSP.MaSanPham,
                            Description = spSXXK.Ten,
                            TariffClassification = spSXXK.MaHS,
                            ProductCtrlNo = dmSP.MaDinhDanhLenhSX,

                        },
                        GoodsMeasure = new GoodsMeasure 
                        { 
                            MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(spSXXK.DVT_ID) : spSXXK.DVT_ID 
                        }
                    },
                    MaterialsNorms = new List<MaterialsNorm>(),
                };
                proNorm.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMuc(dmdk.ID, dmSP.MaSanPham, dmdk.MaDoanhNghiep, dmdk.MaHaiQuan);
                if (Globals.IsKhaiVNACCS)
                {
                    foreach (MaterialsNorm item in proNorm.MaterialsNorms)
                    {
                        item.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.Material.GoodsMeasure.MeasureUnit.Trim());

                    }
                }
                //IEnumerable<DinhMuc> dmItems = from d in dmdk.DMCollection.ToArray()
                //                               where d.MaSanPham == dmSP.MaSanPham
                //                               select d;
                //if (dmItems != null)
                //{
                //    foreach (DinhMuc dmNPL in dmItems)
                //    {
                //        Company.BLL.SXXK.NguyenPhuLieu nplSXXK = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmNPL.MaNguyenPhuLieu);
                //        proNorm.MaterialsNorms.Add(new MaterialsNorm
                //        {
                //            Material = new Product
                //                {
                //                    Commodity = new Commodity { Identification = dmNPL.MaNguyenPhuLieu, Description = dmNPL.TenNPL, TariffClassification = nplSXXK.MaHS },
                //                    GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(dmNPL.DVT_ID) : dmNPL.DVT_ID },
                //                },
                //            Norm = Helpers.FormatNumeric(dmNPL.DinhMucSuDung, GlobalsShare.TriGiaNT),
                //            Loss = Helpers.FormatNumeric(dmNPL.TyLeHaoHut, GlobalsShare.TriGiaNT)

                //        });
                //    }
                //}
                dm.ProductionNorm.Add(proNorm);
                //}
            }

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return dm;
        }
        public static SXXK_DinhMucSP ToDataTransferObject_SXXK_DinhMuc_SUA(DinhMucDangKySUA dmdk, IList<DinhMucSUA> listDMSua, string TenDonVi)
        {
            long soTiepNhanDMDK;
            DateTime ngayTiepNhanDMDK;
            DinhMucDangKy objDm = DinhMucDangKy.Load(dmdk.IDDMDK);
            soTiepNhanDMDK = objDm == null ? 0 : objDm.SoTiepNhan;
            ngayTiepNhanDMDK = objDm == null ? new DateTime(1900, 01, 01) : objDm.NgayTiepNhan;
            SXXK_DinhMucSP dm = new SXXK_DinhMucSP
            {
                Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                Function = DeclarationFunction.SUA,
                Reference = dmdk.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdk.MaHaiQuan.Trim()) : dmdk.MaHaiQuan.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhanDMDK, 0),
                Acceptance = ngayTiepNhanDMDK.ToString(sfmtDateTime),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = TenDonVi, Identity = dmdk.MaDoanhNghiep },
                ProductionNorm = new List<ProductionNorm>(),
            };
            dm.Agents.Add(new Agent
            {
                Name = TenDonVi,
                Identity = dmdk.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,

            });
            IEnumerable<DinhMucSUA> DMCollection = listDMSua.ToArray().Distinct(new
             DistinctMaSPSua());
            foreach (DinhMucSUA dmSP in DMCollection)
            {
                Company.BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                spSXXK = Company.BLL.SXXK.SanPham.getSanPham(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmSP.MaSanPham);
                ProductionNorm proNorm = new ProductionNorm
                {
                    Product = new Product
                    {
                        Commodity = new Commodity { Identification = dmSP.MaSanPham, Description = spSXXK.Ten, TariffClassification = spSXXK.MaHS },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(spSXXK.DVT_ID) : spSXXK.DVT_ID }
                    },
                    MaterialsNorms = new List<MaterialsNorm>(),
                };
                proNorm.MaterialsNorms = NguyenPhuLieu.GetNPLFromDinhMucSUA(dmdk.ID, dmSP.MaSanPham, dmdk.MaDoanhNghiep, dmdk.MaHaiQuan);
                if (Globals.IsKhaiVNACCS)
                {
                    foreach (MaterialsNorm item in proNorm.MaterialsNorms)
                    {
                        item.Material.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.Material.GoodsMeasure.MeasureUnit.Trim());

                    }
                }
                //IEnumerable<DinhMucSUA> dmItems = from d in listDMSua.ToArray()
                //                                  where d.MaSanPham == dmSP.MaSanPham
                //                                  select d;
                //if (dmItems != null)
                //{
                //foreach (DinhMucSUA dmNPL in dmItems)
                //{
                //    Company.BLL.SXXK.NguyenPhuLieu nplSXXK = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(dmdk.MaHaiQuan, dmdk.MaDoanhNghiep, dmNPL.MaNguyenPhuLieu);
                //    proNorm.MaterialsNorms.Add(new MaterialsNorm
                //    {
                //        Material = new Product
                //        {
                //            Commodity = new Commodity { Identification = dmNPL.MaNguyenPhuLieu, Description = nplSXXK.Ten, TariffClassification = nplSXXK.MaHS },
                //            GoodsMeasure = new GoodsMeasure { MeasureUnit = dmNPL.DVT_ID },
                //        },
                //        Norm = Helpers.FormatNumeric(dmNPL.DinhMucSuDung, 6),
                //        Loss = Helpers.FormatNumeric(dmNPL.TyLeHaoHut, 4)

                //    });
                //}
                dm.ProductionNorm.Add(proNorm);
                //}
            };
            return dm;
        }
        /// <summary>
        /// Huy Khai Bao To Khai Chua Duoc Duyet
        /// </summary>
        /// <param name="LoaiToKhai"></param>
        /// <returns></returns>
        public static DeclarationBase HuyKhaiBao(string loaiToKhai, string reference, long soTiepNhan, string maHaiQuan, DateTime ngayTiepNhan)
        {
            DeclarationBase dec = new DeclarationBase()
            {
                Issuer = loaiToKhai,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(soTiepNhan, 0),
                Acceptance = ngayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = maHaiQuan,
                AdditionalInformations = new List<AdditionalInformation>()
            };
            dec.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Content = new Content() { Text = "." }
                });
            return dec;
        }

        #region TransferOb tờ khai SXXK
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = Company.KDT.SHARE.QuanLyChungTu.KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NamDK, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan, tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);

            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.SXXK_TOKHAI_NHAP : DeclarationIssuer.SXXK_TOKHAI_XUAT,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkmd.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()).Trim() : tkmd.MaHaiQuan.Trim(),
                //tkmd.MaHaiQuan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },
                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn
                /* AdditionalDocuments = new List<AdditionalDocument>(),*/

                // Hóa Ðon thuong mại
                Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },

                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                //Doanh nghiệp nhập khẩu
                Importer = !isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                new NameBase { Name = tkmd.TenDonViDoiTac, Identity = tkmd.MaDoanhNghiep.Trim() },

                // Người đại diện doanh nghiệp | Tên chủ hàng
                RepresentativePerson = new RepresentativePerson { Name = tkmd.TenChuHang, ContactFunction = tkmd.ChucVu },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),

                /* License = new List<License>(),
                 ContractDocument = new List<ContractDocument>(),
                 CommercialInvoices = new List<CommercialInvoice>(),
                 CertificateOfOrigins = new List<CertificateOfOrigin>(),
                 CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                 AttachDocumentItem = new List<AttachDocumentItem>(),
                 AdditionalDocumentEx = new List<AdditionalDocument>(),*/
                #endregion Nrr

            };
            tokhai.AdditionalInformations.Add(
                new AdditionalInformation
                {
                    Statement = "001",
                    Content = new Content() { Text = tkmd.DeXuatKhac }
                }
                );
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });


            #endregion Header

            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion


            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                {

                    CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                    Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                    StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT),
                    UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                    StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                    Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                    Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                    GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, 3), MeasureUnit = Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACC(hmd.DVT_ID) : hmd.DVT_ID }//hmd.DVT_ID }

                };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal dPhiVanChuyen = tkmd.PhiVanChuyen;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(dTongPhi / soluonghang, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen / soluonghang, 4),
                    Method = string.Empty,
                    OtherChargeDeduction = "0"
                };

                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS,
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //RegisterCustoms = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()).Trim() : tkmd.MaHaiQuan.Trim(),
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                if (tkmd.LoaiHangHoa == "N")
                    commodity.Type = "1";
                else if (tkmd.LoaiHangHoa == "S")
                    commodity.Type = "2";
                else if (tkmd.LoaiHangHoa == "T")
                    commodity.Type = "3";
                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = string.Empty,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 2),
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 2),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 2),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].TriGiaThuKhac, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 2),
                    Type = DutyTaxFeeType.THUE_KHAC,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(0, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(0, 2),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                    {
                        Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                        Issuer = giayphep.NguoiCap/*Người cấp*/,
                        IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                        Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                        Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                        Name = "GP"/* Tên giấy phép*/,
                        Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                    };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion

                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }

            #endregion CustomGoodsItem Danh sách hàng khai báo
            #region Danh sách chứng từ  XNK đi kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            #region License Giấy phép
            {
                tokhai.License = new List<License>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    License lic = LicenseFrom(giayPhep);
                    tokhai.License.Add(lic);
                }
            }
            #endregion Giấy phép

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            #region ContractDocument  Hợp đồng thương mại
            {
                tokhai.ContractDocument = new List<ContractDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
            }
            #endregion Hợp đồng thương mại

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            #region CommercialInvoice Hóa đơn thương mại
            {
                tokhai.CommercialInvoices = new List<CommercialInvoice>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
            }
            #endregion Hóa đơn thương mại

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            #region CertificateOfOrigin Thêm CO
            {
                tokhai.CertificateOfOrigins = new List<CertificateOfOrigin>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                {
                    tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                }
            }
            #endregion CO


            if (tkmd.VanTaiDon != null)
            #region BillOfLadings Vận đơn
            {
                tokhai.BillOfLadings = new List<BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                BillOfLading billOfLading = new BillOfLading()
                {
                    Reference = vandon.SoVanDon,
                    Issue = vandon.NgayVanDon.Year > 1900 ? vandon.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    IssueLocation = vandon.QuocTichPTVT
                };
                billOfLading.BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = vandon.SoHieuPTVT,
                    Identification = vandon.TenPTVT,
                    Journey = vandon.SoHieuChuyenDi,
                    ModeAndType = tkmd.PTVT_ID,
                    Departure = vandon.NgayKhoiHanh.Year > 1900 ? vandon.NgayKhoiHanh.ToString(sfmtDate) : string.Empty,
                    RegistrationNationality = string.IsNullOrEmpty(vandon.QuocTichPTVT) ? string.Empty : vandon.QuocTichPTVT.Substring(0, 2)
                };
                billOfLading.Carrier = new NameBase()
                {
                    Name = vandon.TenHangVT,
                    Identity = vandon.MaHangVT
                };

                billOfLading.Consignment = new Consignment()
                {
                    Consignor = new NameBase()
                    {
                        Name = vandon.TenNguoiGiaoHang,
                        Identity = vandon.MaNguoiGiaoHang
                    },
                    Consignee = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHang,
                        Identity = vandon.MaNguoiNhanHang
                    },
                    NotifyParty = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHangTrungGian,
                        Identity = vandon.MaNguoiNhanHangTrungGian
                    },
                    LoadingLocation = new LoadingLocation()
                    {
                        Name = vandon.TenCangXepHang,
                        Code = vandon.MaCangXepHang,
                        Loading = vandon.NgayKhoiHanh.Year > 1900 ? vandon.NgayKhoiHanh.ToString(sfmtDate) : string.Empty
                    },
                    UnloadingLocation = new UnloadingLocation()
                    {
                        Name = vandon.TenCangDoHang,
                        Code = vandon.MaCangDoHang,
                        Arrival = vandon.NgayDenPTVT.Year > 1900 ? vandon.NgayDenPTVT.ToString(sfmtDate) : string.Empty
                    },
                    DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },


                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                        Type = vandon.LoaiKien,
                        MarkNumber = string.Empty
                    }
                };

                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng và Container trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = string.Empty,//Mã hàng
                                    TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, GlobalsShare.TriGiaNT),
                                    MeasureUnit = hangVanDon.DVT_ID.Substring(0, 3)
                                },
                                EquipmentIdentification = new EquipmentIdentification()
                                {
                                    identification = hangVanDon.SoHieuContainer
                                }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);
            }
            //    #region Tự động thêm vận đơn rỗng cho tờ khai xuất
            //else if (tkmd.VanTaiDon == null && tkmd.MaLoaiHinh.Substring(0, 1).Trim().ToUpper() == "X")
            //{
            //    tokhai.BillOfLadings = new List<BillOfLading>();
            //    BillOfLading billOfLading = new BillOfLading()
            //    {
            //        Reference = " ",
            //        Issue = " ",
            //        IssueLocation = " "
            //    };
            //    billOfLading.BorderTransportMeans = new BorderTransportMeans()
            //    {
            //        Identity = " ",
            //        Identification = " ",
            //        Journey = " ",
            //        ModeAndType = tkmd.PTVT_ID,
            //        Departure = " ",
            //        RegistrationNationality = " "
            //    };
            //    billOfLading.Carrier = new NameBase()
            //    {
            //        Name = " ",
            //        Identity = " "
            //    };

            //    billOfLading.Consignment = new Consignment()
            //    {
            //        Consignor = new NameBase()
            //        {
            //            Name = " ",
            //            Identity = " "
            //        },
            //        Consignee = new NameBase()
            //        {
            //            Name = " ",
            //            Identity = " "
            //        },
            //        NotifyParty = new NameBase()
            //        {
            //            Name = " ",
            //            Identity = " "
            //        },
            //        LoadingLocation = new LoadingLocation()
            //        {
            //            Name = " ",
            //            Code = "",
            //            Loading = ""
            //        },
            //        UnloadingLocation = new UnloadingLocation()
            //        {
            //            Name = " ",
            //            Code = " ",
            //            Arrival = " "
            //        },
            //        DeliveryDestination = new DeliveryDestination() { Line = " " },


            //        ConsignmentItemPackaging = new Packaging()
            //        {
            //            Quantity = " ",
            //            Type = " ",
            //            MarkNumber = string.Empty
            //        }
            //    };
            //    tokhai.BillOfLadings.Add(billOfLading);
            //}
            //#endregion
            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
            }
            #endregion Đề nghị chuyển cửa khẩu

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            #region AttachDocumentItem Chứng từ đính kèm
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    if (fileinChungtuDinhKem.LoaiKB == "0")
                    {
                        AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                        tokhai.AttachDocumentItem.Add(attachDocumentItem);
                    }
                };
            }
            #endregion Chứng từ đính kèm

            #endregion Danh sách giấy phép XNK đi kèm
            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion

            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                if (tkmd.MaLoaiHinh.Contains("NCX"))
                    tokhai.Issuer = DeclarationIssuer.CX_TOKHAI_NHAP;
                else if (tkmd.MaLoaiHinh.Contains("XCX"))
                    tokhai.Issuer = DeclarationIssuer.CX_TOKHAI_XUAT;
            }
            return tokhai;
        }
        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        private static CertificateOfOrigin CertificateOfOriginFrom(Company.KDT.SHARE.QuanLyChungTu.CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
            {


                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } : new NameBase { Name = co.TenDiaChiNguoiXK, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = tkmd.MaDoanhNghiep },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                LoadingLocation = new LoadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },

                UnloadingLocation = new UnloadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            {
                GoodsItem goodsItem = new GoodsItem();
                goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
                goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
                goodsItem.CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = hangCo.MaNguyenTe
                };
                goodsItem.ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
                    Type = hangCo.LoaiKien,
                    MarkNumber = hangCo.SoHieuKien
                };
                goodsItem.Commodity = new Commodity()
                {
                    Description = hangCo.TenHang,
                    Identification = string.Empty,
                    TariffClassification = hangCo.MaHS
                };
                goodsItem.GoodsMeasure = new GoodsMeasure()
                {
                    GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, GlobalsShare.TriGiaNT),
                    MeasureUnit = hangCo.DVT_ID
                };
                goodsItem.Origin = new Origin()
                {
                    OriginCountry = hangCo.NuocXX_ID.Trim()
                };
                goodsItem.Invoice = new Invoice()
                {
                    Reference = hangCo.SoHoaDon,
                    Issue = hangCo.NgayHoaDon.ToString(sfmtDate)
                };
                certificateOfOrigin.GoodsItems.Add(goodsItem);
            }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem)
        {
            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = "200",
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        private static License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            License lic = new License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.DonGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = string.Empty,
                            TariffClassification = hangInGiayPhep.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static ContractDocument ContractFrom(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai)
        {
            ContractDocument contractDocument = new ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, GlobalsShare.TriGiaNT),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS,
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                }
            };
            return customsOfficeChangedRequest;
        }
        private static CommercialInvoice CommercialInvoiceFrom(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai)
        {
            CommercialInvoice commercialInvoice = new CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID.Trim() },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Trim() },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, GlobalsShare.DonGiaNT), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<CertificateOfOrigin>();
                CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<License>();
                License license = LicenseFrom(giayphep);
                boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                boSungChungTuDto.ContractDocuments = new List<ContractDocument>();
                ContractDocument contract = ContractFrom(hopdong);
                boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<CommercialInvoice>();
                CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                boSungChungTuDto.DeclarationDocument = null;
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm

            return boSungChungTuDto;
        }


        #endregion

        public static SXXK_HosoThanhKhoan ToDataTransferSXXK_HoSoThanhKhoan(HoSoThanhLyDangKy hosothanhly, string tenDoanhNghiep)
        {
            #region Header
            SXXK_HosoThanhKhoan thanhkhoan = new SXXK_HosoThanhKhoan()
            {
                Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                Function = DeclarationFunction.KHAI_BAO,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                DeclarationOffice = hosothanhly.MaHaiQuanTiepNhan.Trim(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Agents = new List<Agent>(),
                LoadingList = Helpers.FormatNumeric(hosothanhly.ChungTuTTs.Count),
                Importer = new NameBase()
                {
                    Name = tenDoanhNghiep,
                    Identity = hosothanhly.MaDoanhNghiep.Trim()
                },
                Reference = hosothanhly.GuidStr,
                ImportDeclarationList = new ImportDeclarationDocument()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    DeclarationDocuments = new List<DeclarationDocument>()
                },
                ExportDeclarationList = new ImportDeclarationDocument()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    DeclarationDocuments = new List<DeclarationDocument>()
                },
                PaymentDocumentList = new PaymentDocumentList()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    ContractReferences = new List<SXXK_ContractDocument>()
                },

                MaterialLeft = new DocumentList()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate)
                },
                MaterialExport = new DocumentList()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    ContractDocuments = new List<SXXK_ContractDocument>()


                },
                MaterialNotExport = new DocumentList()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    ContractDocuments = new List<SXXK_ContractDocument>()

                },
                MaterialList = new DocumentList()
                {
                    Reference = hosothanhly.GuidStr,
                    CustomsReference = hosothanhly.SoTiepNhan > 0 ? Helpers.FormatNumeric(hosothanhly.SoTiepNhan) : string.Empty,
                    Issuer = DeclarationIssuer.HO_SO_THANH_KHOAN_FULL,
                    Function = DeclarationFunction.KHAI_BAO,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    Acceptance = DateTime.Now.ToString(sfmtDate),
                    ImportDeclarationDocuments = new List<SXXK_ContractDocument>()
                }

            };
            thanhkhoan.Agents.Add(new Agent()
            {
                Name = tenDoanhNghiep,
                Identity = hosothanhly.MaDoanhNghiep.Trim(),
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN

            });
            #endregion Header

            int sobanke = hosothanhly.getBKToKhaiNhap();
            if (sobanke > -1)
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                #region ImportDeclarationList Bảng kê tờ khai xuất
                foreach (BKToKhaiNhap tkn in hosothanhly.BKCollection[sobanke].bkTKNCollection)
                {

                    thanhkhoan.ImportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                    {
                        CustomsReference = Helpers.FormatNumeric(tkn.SoToKhai),
                        NatureOfTransaction = tkn.MaLoaiHinh.Trim(),
                        Acceptance = tkn.NgayDangKy.ToString(sfmtDate),
                        DeclarationOffice = tkn.MaHaiQuan.Trim(),
                        Clearance = tkn.NgayHoanThanh.ToString(sfmtDate)
                    });
                }
                #endregion
            }
            sobanke = hosothanhly.getBKToKhaiXuat();
            if (sobanke > -1)
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                #region ExportDeclarationList Bảng kê tờ khai nhập

                foreach (BKToKhaiXuat tkx in hosothanhly.BKCollection[sobanke].bkTKXColletion)
                {
                    thanhkhoan.ExportDeclarationList.DeclarationDocuments.Add(new DeclarationDocument()
                    {
                        CustomsReference = Helpers.FormatNumeric(tkx.SoToKhai),
                        NatureOfTransaction = tkx.MaLoaiHinh.Trim(),
                        Acceptance = tkx.NgayDangKy.ToString(sfmtDate),
                        DeclarationOffice = tkx.MaHaiQuan.Trim(),
                        Clearance = tkx.NgayHoanThanh.ToString(sfmtDate)
                    });
                }

                #endregion
            }
            #region PaymentDocumentList Bảng chứng từ thanh toán
            foreach (ChungTuTT cttt in hosothanhly.ChungTuTTs)
            {
                SXXK_ContractDocument contract = new SXXK_ContractDocument()
                {
                    Reference = cttt.SoHopDong.Trim(),
                    Issue = cttt.NgayHopDong.ToString(sfmtDate),
                    CustomsValue = Helpers.FormatNumeric(cttt.TriGiaTheoHopDong, 2),
                    PaymentDocument = new PaymentDocument()
                    {
                        Reference = cttt.SoChungTu,
                        Issue = cttt.NgayChungTu.ToString(sfmtDate),
                        IssueLocation = cttt.NoiPhatHanh,
                        PaymentMethod = cttt.PTTT_ID.Trim(),
                        CustomsValue = Helpers.FormatNumeric(cttt.TriGiaTT, 2),
                        AdditionalInformations = new List<AdditionalInformation>()
                    }
                };
                contract.PaymentDocument.AdditionalInformations.Add(new AdditionalInformation()
                {
                    //CustomsValue = cttt.GhiChu,
                    Content = new Content() { Text = cttt.GhiChu }
                });
                if (cttt.ChungTuTTChitiets.Count > 0)
                    contract.Commoditys = new List<Commodity>();
                foreach (ChungTuTTChitiet item in cttt.ChungTuTTChitiets)
                {
                    contract.Commoditys.Add(new Commodity()
                    {
                        Identification = item.MaHang,
                        StatisticalValue = Helpers.FormatNumeric(item.TriGia, 3)
                    });
                }
                thanhkhoan.PaymentDocumentList.ContractReferences.Add(contract);

            }
            #endregion

            sobanke = hosothanhly.getBKNPLChuaThanhLY();
            if (sobanke > -1)
            #region MaterialLeft Bảng kê nguyên phụ liệu chưa thanh lý
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                thanhkhoan.MaterialLeft.ContractDocuments = new List<SXXK_ContractDocument>();

                BKNPLChuaThanhLyCollection bkSoToKhais = new BKNPLChuaThanhLyCollection();

                foreach (BKNPLChuaThanhLy item in hosothanhly.BKCollection[sobanke].bkNPLCTLCollection)
                {
                    bool isEqual = false;
                    foreach (BKNPLChuaThanhLy item1 in bkSoToKhais)
                    {
                        if (item.Equals(item1))
                        {
                            isEqual = true;
                            break;
                        }
                    }

                    if (!isEqual)
                        bkSoToKhais.Add(item);
                }

                foreach (BKNPLChuaThanhLy bkCTL in bkSoToKhais)
                {
                    SXXK_ContractDocument bagKeChuaTL = new SXXK_ContractDocument()
                    {
                        CustomsReference = Helpers.FormatNumeric(bkCTL.SoToKhai),
                        NatureOfTransaction = bkCTL.MaLoaiHinh.Trim(),
                        Acceptance = bkCTL.NgayDangKy.ToString(sfmtDate),
                        DeclarationOffice = bkCTL.MaHaiQuan.Trim(),
                        Materials = new List<SXXK_Product>()
                    };
                    #region Thêm hàng
                    SXXK_Product material = new SXXK_Product()
                        {
                            Commoditys = new List<Commodity>(),
                            GoodsMeasures = new List<SXXK_GoodsMeasure>()
                        };
                    foreach (BKNPLChuaThanhLy item in hosothanhly.BKCollection[sobanke].bkNPLCTLCollection)
                    {
                        if (item.Equals(bkCTL))
                        {
                            Commodity Commodity = new Commodity()
                            {
                                Identification = item.MaNPL,
                                Description = item.TenNPL
                            };
                            SXXK_GoodsMeasure GoodsMeasure = new SXXK_GoodsMeasure()
                            {

                                Quantity = Helpers.FormatNumeric(item.Luong, 5),
                                MeasureUnits = new List<string>(),
                                ConversionRate = "1"
                            };
                            GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                            GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                            material.Commoditys.Add(Commodity);
                            material.GoodsMeasures.Add(GoodsMeasure);
                            #region Kiểu thêm hàng bt
                            //material = new SXXK_Product()
                            //{

                            //    Commoditys = new Commodity()
                            //    {
                            //        Identification = item.MaNPL,
                            //        Description = item.TenNPL
                            //    },
                            //    GoodsMeasure = new SXXK_GoodsMeasure()
                            //    {
                            //        Tariff = Helpers.FormatNumeric(item.Luong, 5),
                            //        MeasureUnits = new List<string>(),
                            //        ConversionRate = "1"
                            //    }
                            //};
                            //material.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                            //material.GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());

                            //Cách theo hàng LanNT
                            //bagKeChuaTL.Materials.Add(material);
                            #endregion

                        }

                    }
                    bagKeChuaTL.Materials.Add(material);
                    #endregion

                    thanhkhoan.MaterialLeft.ContractDocuments.Add(bagKeChuaTL);
                }
            }
            #endregion
            sobanke = hosothanhly.getBKNPLXinHuy();
            if (sobanke > -1)
                #region Bảng kê nguyên nguyên phụ liệu hủy
                foreach (BKNPLXinHuy bkCTL in hosothanhly.BKCollection[sobanke].bkNPLXHCollection)
                {

                }
                #endregion
            sobanke = hosothanhly.getBKNPLXuatGiaCong();
            if (sobanke > -1)
            #region MaterialList Bảng kê nguyên phụ liệu xuất khẩu qua sản phẩm theo hợp đồng gia công
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                BKNPLXuatGiaCongCollection bkNPL2GC = hosothanhly.BKCollection[sobanke].bkNPLXGCCollection;
                BKNPLXuatGiaCongCollection bkToKhais = new BKNPLXuatGiaCongCollection();
                foreach (BKNPLXuatGiaCong item in bkNPL2GC)
                {
                    bool isExist = false;
                    foreach (BKNPLXuatGiaCong itemDt in bkToKhais)
                    {
                        isExist = itemDt.Equals(item);
                        if (isExist) break;
                    }
                    if (!isExist)
                        bkToKhais.Add(item);
                }

                foreach (BKNPLXuatGiaCong bkCTL in bkToKhais)
                {
                    SXXK_ContractDocument bkNPXToGC = new SXXK_ContractDocument()
                    {
                        CustomsReference = Helpers.FormatNumeric(bkCTL.SoToKhai),
                        NatureOfTransaction = bkCTL.MaLoaiHinh.Trim(),
                        Acceptance = bkCTL.NgayDangKy.ToString(sfmtDate),
                        DeclarationOffice = bkCTL.MaHaiQuan.Trim(),
                        Materials = new List<SXXK_Product>()
                    };

                    SXXK_Product Material = new SXXK_Product()
                    {
                        Commoditys = new List<Commodity>(),
                        DetailMaterialExports = new List<SXXK_GoodsMeasure>()
                    };

                    foreach (BKNPLXuatGiaCong item in hosothanhly.BKCollection[sobanke].bkNPLXGCCollection)
                    {
                        if (item.Equals(bkCTL))
                        {
                            Commodity Commodity = new Commodity()
                            {
                                Description = item.TenNPL,
                                Identification = item.MaNPL
                            };
                            Material.Commoditys.Add(Commodity);
                            SXXK_GoodsMeasure DetailMaterialExport = new SXXK_GoodsMeasure()
                            {
                                CustomsReference = Helpers.FormatNumeric(item.SoToKhaiXuat),
                                NatureOfTransaction = item.MaLoaiHinhXuat.Trim(),
                                Acceptance = item.NgayDangKyXuat.ToString(sfmtDate),
                                DeclarationOffice = item.MaHaiQuanXuat,
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    Quantity = Helpers.FormatNumeric(item.LuongXuat, 5),
                                    MeasureUnit = item.DVT_ID.Trim()
                                }

                            };
                            Material.DetailMaterialExports.Add(DetailMaterialExport);
                        }
                    }
                    bkNPXToGC.Materials.Add(Material);
                    thanhkhoan.MaterialExport.ContractDocuments.Add(bkNPXToGC);
                }
            }
            #endregion

            sobanke = hosothanhly.getBKNPLNopThue();
            if (sobanke > -1)
            #region MaterialNotExport Danh sách nguyên phụ liệu nộp thuế
            {
                hosothanhly.BKCollection[sobanke].LoadChiTietBangKe();
                BKNPLNopThueTieuThuNoiDiaCollection bkToKhais = new BKNPLNopThueTieuThuNoiDiaCollection();

                foreach (BKNPLNopThueTieuThuNoiDia bkCTL in hosothanhly.BKCollection[sobanke].bkNPLNTCollection)
                {
                    bool isEqual = false;
                    foreach (BKNPLNopThueTieuThuNoiDia item in bkToKhais)
                    {
                        if (bkCTL.Equals(item))
                        {
                            isEqual = true;
                            break;
                        }
                    }
                    if (!isEqual)
                        bkToKhais.Add(bkCTL);
                }
                foreach (BKNPLNopThueTieuThuNoiDia bkItem in bkToKhais)
                {
                    SXXK_ContractDocument document = new SXXK_ContractDocument()
                    {
                        CustomsReference = Helpers.FormatNumeric(bkItem.SoToKhai),
                        NatureOfTransaction = bkItem.MaLoaiHinh.Trim(),
                        Acceptance = bkItem.NgayDangKy.ToString(sfmtDate),
                        DeclarationOffice = bkItem.MaHaiQuan.Trim(),
                        MethodOfProcess = "1",
                        Materials = new List<SXXK_Product>()
                    };
                    SXXK_Product material = new SXXK_Product()
                    {
                        Commoditys = new List<Commodity>(),
                        GoodsMeasures = new List<SXXK_GoodsMeasure>()
                    };

                    foreach (BKNPLNopThueTieuThuNoiDia item in hosothanhly.BKCollection[sobanke].bkNPLNTCollection)
                    {
                        if (bkItem.Equals(item))
                        {
                            Commodity Commodity = new Commodity()
                            {
                                Description = item.TenNPL,
                                Identification = item.MaNPL
                            };
                            material.Commoditys.Add(Commodity);
                            SXXK_GoodsMeasure GoodsMeasure = new SXXK_GoodsMeasure()
                            {
                                Quantity = Helpers.FormatNumeric(item.LuongNopThue, 5),
                                MeasureUnits = new List<string>(),
                                ConversionRate = "1"
                            };
                            GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                            GoodsMeasure.MeasureUnits.Add(item.DVT_ID.Trim());
                            material.GoodsMeasures.Add(GoodsMeasure);

                        }
                    }
                    document.Materials.Add(material);
                    thanhkhoan.MaterialNotExport.ContractDocuments.Add(document);
                }

            }
            #endregion

            #region Chưa dùng đến
            sobanke = hosothanhly.getBKNPLTaiXuat();
            if (sobanke > -1)
                #region Danh sách nguyên phụ liệu tái xuất
                foreach (BKNPLTaiXuat bkCTL in hosothanhly.BKCollection[sobanke].bkNPLTXCollection)
                {
                }
                #endregion
            sobanke = hosothanhly.getBKNPLTamNopThue();
            if (sobanke > -1)
                #region Danh sách nguyên phụ liệu tạm nộp thuế
                foreach (BKNPLTamNopThue bkCTL in hosothanhly.BKCollection[sobanke].bkNPLTNTCollection)
                {
                }
                #endregion
            sobanke = hosothanhly.getBKNPLNhapKinhDoanh();
            if (sobanke > -1)
                #region Danh sách nguyên phụ liệu từ tờ khai nhập kinh doanh
                foreach (BKNPLXuatSuDungNKD bkCTL in hosothanhly.BKCollection[sobanke].bkNPLNKDCollection)
                {
                }
                #endregion
            #endregion Chưa dùng đến

            return thanhkhoan;
        }

    }
    class DistinctMaSP : IEqualityComparer<DinhMuc>
    {
        public bool Equals(DinhMuc x, DinhMuc y)
        {
            return x.MaSanPham == y.MaSanPham;
        }

        public int GetHashCode(DinhMuc obj)
        {
            return obj.MaSanPham.GetHashCode();
        }
    }
    class DistinctMaSPSua : IEqualityComparer<DinhMucSUA>
    {
        public bool Equals(DinhMucSUA x, DinhMucSUA y)
        {
            return x.MaSanPham == y.MaSanPham;
        }

        public int GetHashCode(DinhMucSUA obj)
        {
            return obj.MaSanPham.GetHashCode();
        }
    }
        #endregion
}