﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Company.Interface
{
    //public delegate string ClickHandler(object sender, IData data);

    public partial class BaseFormHaveGuidPanel : Company.Interface.BaseForm
    {
        public BaseFormHaveGuidPanel()
        {
            InitializeComponent();
        }

        public XmlDocument docGuide;

        /// <summary>
        /// Hien thi Guid theo control nhap lieu duoc chon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowGuide(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Control ctrl = sender as Control;

                if (ctrl.GetType().BaseType == typeof(TextBoxBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.EditBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.ComboBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.UpDownBase)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.BaseEdit)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.PopupBaseAutoSearchEdit)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.LookUpEditBase)
                    || ctrl.GetType() == typeof(Janus.Windows.CalendarCombo.CalendarCombo))
                {
                    txtGuide.Text = Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuide(docGuide, ctrl.Tag != null ? ctrl.Tag.ToString() : "");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        /// <summary>
        /// Thiet lap su kien Enter tren cac control de hien thi thong tin Guide theo control ID
        /// </summary>
        /// <param name="ctrl"></param>
        protected void SetHandler(Control ctrl)
        {
            foreach (Control cItem in ctrl.Controls)
            {
                cItem.Enter += new EventHandler(ShowGuide);

                if (cItem.Controls.Count > 0)
                    SetHandler(cItem);
            }
        }

        protected void SetTextChanged_Handler(object sender, EventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.GetType().BaseType == typeof(TextBoxBase)
                    || ctrl.GetType().BaseType == typeof(Janus.Windows.GridEX.EditControls.EditBase)
                    || ctrl.GetType().BaseType == typeof(DevExpress.XtraEditors.BaseEdit))
            {
                ctrl.Text = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2ASCII_New(ctrl.Text);
            }
        }
    }
}
