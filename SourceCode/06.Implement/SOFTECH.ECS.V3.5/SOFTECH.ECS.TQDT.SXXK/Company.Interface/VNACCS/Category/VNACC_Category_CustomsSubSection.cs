﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_CustomsSubSection : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection customsSubSection;
        public string where = "";
        public VNACC_Category_CustomsSubSection()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            txtName.Text = String.Empty;
            txtName_EN.Text = String.Empty;
            txtNotes.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã hải quan  ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName, errorProvider, "Mã CCHQ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName_EN, errorProvider, "Tên tiếng việt ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                customsSubSection.CustomsCode = txtCode.Text.Trim().ToUpper();
                customsSubSection.CustomsSubSectionCode = txtName.Text.Trim().ToUpper();
                customsSubSection.Name = txtName_EN.Text.Trim().ToUpper();
                customsSubSection.ImportExportClassification = txtNotes.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = customsSubSection.CustomsCode;
                txtName.Text = customsSubSection.CustomsSubSectionCode;
                txtName_EN.Text = customsSubSection.Name;
                txtNotes.Text = customsSubSection.ImportExportClassification;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND CustomsCode = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtName.Text))
                    where += " AND CustomsSubSectionCode LIKE N'%" + txtName.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtName_EN.Text))
                    where += " AND Name LIKE N'%" + txtName_EN.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND ImportExportClassification LIKE N'%" + txtNotes.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                customsSubSection.TableID = "A014A";
                customsSubSection.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                customsSubSection = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    customsSubSection.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                customsSubSection = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    customsSubSection = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection();
                    customsSubSection = (Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsSubSection)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_Category_CustomsSubSection_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
    }
}
