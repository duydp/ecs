﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.SXXK.DNCX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuManageForm : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private NguyenPhuLieuDangKyCollection nplDangKyCollection = new NguyenPhuLieuDangKyCollection();
        private NguyenPhuLieuDangKyCollection tmpCollection = new NguyenPhuLieuDangKyCollection();
        /// <summary>
        /// Thông tin nguyên phụ liệu đang dược chọn.
        /// </summary>
        private readonly NguyenPhuLieuDangKy currentNPLDangKy = new NguyenPhuLieuDangKy();
        NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public NguyenPhuLieuManageForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                saveFileDialog1.InitialDirectory = Application.StartupPath;
                cbStatus.SelectedIndex = 0;
                txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
                //An nut Xac nhan
                LaySoTiepNhan.Visible = InheritableBoolean.False;
                khoitao_DuLieuChuan();
                if (ckbTimKiem.Checked)
                    grbtimkiem.Enabled = true;
                else
                    grbtimkiem.Enabled = false;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatus();
        }

        //Phiph Update-----------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                txtNamTiepNhan.Value = DateTime.Today.Year;
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (txtMaNPL.Text == "")
                    this.search();
                else
                    this.search_NPL(txtMaNPL.Text);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.nplDangKyCollection = NguyenPhuLieuDangKy.SelectCollectionDynamic(where, "ID desc");
                dgList.DataSource = this.nplDangKyCollection;
                try
                {
                    this.tmpCollection = NguyenPhuLieuDangKy.SelectCollectionDynamicAll(where, "ID desc");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                this.setCommandStatus();

                this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //Phiph---------tìm kiem kem ma nguyen phu lieu--------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search_NPL(string ma_NPL)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = " WHERE NPL.Ma like '%" + ma_NPL + "%'";
                where += " AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.nplDangKyCollection = NguyenPhuLieuDangKy.SelectCollectionMaNPL(where, "ID desc");
                dgList.DataSource = this.nplDangKyCollection;
                try
                {
                    this.tmpCollection = NguyenPhuLieuDangKy.SelectCollectionDynamicAll(where, "ID desc");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                this.setCommandStatus();

                this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }


        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBo.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_HUY)
            {
                InheritableBoolean cho_duyet = Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET ? InheritableBoolean.True : InheritableBoolean.False;
                cmdSend.Enabled = cmdSend3.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel2.Enabled = cho_duyet;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                //cmdSend.Enabled  = InheritableBoolean.False;
                //cmdSingleDownload.Enabled  = InheritableBoolean.True;
                //cmdCancel.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //LaySoTiepNhan.Enabled = InheritableBoolean.True;
                //cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = cmdSend3.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.False;
                //cmdCancel.Enabled = cmdCancel2.Enabled = cmdCancel3.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = InheritableBoolean.True;
                //cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.True;
                //btnDelete.Enabled = true;
                //cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //btnSuaNPL.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_HUY)
            {
                //cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = cmdSend3.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.False;
                //cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel2.Enabled = cmdCancel3.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                //cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                //btnDelete.Enabled = true;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //btnSuaNPL.Enabled = false;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                {
                    cmdSend.Visible = InheritableBoolean.False;
                    cmdSingleDownload.Visible = InheritableBoolean.False;
                    cmdCancel.Visible = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    LaySoTiepNhan.Visible = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    DongBo.Visible = InheritableBoolean.False;
                    cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
                }
            }
            //LaySoTiepNhan.Enabled =LaySoTiepNhan1.Enabled=LaySoTiepNhan2.Enabled= InheritableBoolean.True;
            //cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.True;
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                        break;
                    case 2:
                        // e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                    //Update by Huỳnh Ngọc Khánh - 28/02/2012 
                    //Contents: Thêm trạng thái CHỜ HỦY và ĐÃ HỦY 
                    case 11:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for canceled";

                        }
                        break;
                    case 10:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Canceled";

                        }
                        break;

                    case 5:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Edited";

                        }
                        break;
                    case -2:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt sửa";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Waiting Edit";

                        }
                        break;
                    case -3:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                        }
                        break;
                    case -4:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                        }
                        break;
                    case 4:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                        }
                        break;

                }

                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã Nguyên phụ liệu vào Grid Danh sách nguyên phụ liệu


                Company.BLL.KDT.SXXK.NguyenPhuLieuCollection NguyenphulieuCollection = new NguyenPhuLieuCollection();
                int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaNPL = "";

                NguyenphulieuCollection = Company.BLL.KDT.SXXK.NguyenPhuLieu.SelectCollectionBy_Master_ID(ValueIdCell);
                Company.BLL.KDT.SXXK.NguyenPhuLieu entityNguyenPhuLieu = new NguyenPhuLieu();

                if (NguyenphulieuCollection.Count > 0)
                {


                    //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                    entityNguyenPhuLieu = NguyenphulieuCollection[0];
                    e.Row.Cells["MaNguyenPhuLieu"].Text = entityNguyenPhuLieu.Ma;

                    //lấy mã Nguyên phụ liệu bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (Company.BLL.KDT.SXXK.NguyenPhuLieu EntityNPL in NguyenphulieuCollection)
                    {

                        TatCaMaNPL = TatCaMaNPL + EntityNPL.Ma + "\n";

                    }
                    e.Row.Cells["MaNguyenPhuLieu"].ToolTipText = TatCaMaNPL;


                }

            }
        }


        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdInNPL":
                    InNguyenPhuLieu();
                    break;
                case "cmdMessage":
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null,null);
                    break;
                case "cmdUpdateStatus":
                    mnuUpdateStatus_Click(null, null);
                    break;
            }
        }

        private void InNguyenPhuLieu()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                NguyenPhuLieuDangKy npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;

                if (npldk != null)
                {
                    if (npldk.NPLCollection.Count == 0)
                        npldk.LoadNPLCollection();

                    BLL.SXXK.NguyenPhuLieuCollection NPLCollectionSelect = new BLL.SXXK.NguyenPhuLieuCollection();

                    foreach (NguyenPhuLieu item in npldk.NPLCollection)
                    {
                        BLL.SXXK.NguyenPhuLieu NPLSelect = new BLL.SXXK.NguyenPhuLieu();
                        NPLSelect.Ma = item.Ma;
                        NPLSelect.Ten = item.Ten;
                        NPLSelect.DVT_ID = item.DVT_ID;
                        NPLSelect.MaHS = item.MaHS;
                        NPLCollectionSelect.Add(NPLSelect);
                    }

                    Company.Interface.Report.ReportViewNPLForm f2 = new Company.Interface.Report.ReportViewNPLForm();
                    if (NPLCollectionSelect.Count > 0)
                    {
                        f2.NPLCollection = NPLCollectionSelect;
                        f2.Show();
                    }
                }
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SuaNPLDaDuyet()
        {
            try
            {
                NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
                npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                npldk.LoadNPLCollection();
                NguyenPhuLieuSendForm f = new NguyenPhuLieuSendForm();
                f.nplDangKy = npldk;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void inPhieuTN()
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "NGUYÊN PHỤ LIỆU";
                Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = nplDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = nplDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                    }
                }
                phieuTNAll.BindReport();
                phieuTNAll.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    NguyenPhuLieuDangKyCollection nplDKcoll = new NguyenPhuLieuDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        nplDKcoll.Add((NguyenPhuLieuDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO NGUYÊN PHỤ LIỆU NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < nplDKcoll.Count; i++)
                    {
                        if (nplDKcoll[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            nplDKcoll[i].LoadNPLCollection();
                            msgAccept += "[" + (i + 1) + "]-[" + nplDKcoll[i].ID + "]-[" + nplDKcoll[i].SoTiepNhan + "]-[" + nplDKcoll[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(nplDKcoll[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (NguyenPhuLieuDangKy item in nplDKcoll)
                        {
                            item.TrangThaiXuLy=TrangThaiXuLy.DA_DUYET;                            
                            item.TransgferDataToSXXK();
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ",false);
                    btnSearch_Click(null,null);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN",false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void XoaNguyenPhuLieuDangKy(GridEXSelectedItemCollection items)
        {

            try
            {
                if (items.Count == 0) return;
                List<NguyenPhuLieuDangKy> itemsDelete = new List<NguyenPhuLieuDangKy>();
                int k = 1;
                string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN XÓA DANH SÁCH KHAI BÁO NGUYÊN PHỤ LIỆU NÀY KHÔNG ?\n";
                string msgDelete = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieuDangKy npldk = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                        npldk.LoadNPLCollection();
                        msgDelete += "[" + (k) + "]-[" + npldk.ID + "]-[" + npldk.SoTiepNhan + "]-[" + npldk.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(npldk.TrangThaiXuLy).ToUpper() + "]\n";
                        k++;
                        itemsDelete.Add(npldk);
                    }
                }
                if (itemsDelete.Count >= 1)
                {
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgDelete + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (NguyenPhuLieuDangKy item in itemsDelete)
                        {
                            foreach (NguyenPhuLieu itemDelete in item.NPLCollection)
                            {
                                try
                                {
                                    Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                    NPL.Ma = itemDelete.Ma;
                                    NPL.MaHaiQuan = item.MaHaiQuan;
                                    NPL.MaDoanhNghiep = item.MaDoanhNghiep;
                                    NPL.Delete();
                                    itemDelete.Delete();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            item.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {

            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaNguyenPhuLieuDangKy(items);
            btnSearch_Click(null,null);
            #region Code cũ
            //try
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    if (items.Count <= 0) return;
            //    // if (ShowMessage("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
            //                MsgSend sendXML = new MsgSend();
            //                sendXML.LoaiHS = "NPL";
            //                sendXML.master_id = nplDangKySelected.ID;
            //                if (sendXML.Load())
            //                {
            //                    if (nplDangKySelected.SoTiepNhan != 0)
            //                        //  ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
            //                        MLMessages("Danh sách thứ " + i.Position + 1 + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + 1 + "", false);
            //                }
            //                else
            //                {
            //                    if (nplDangKySelected.ID > 0)
            //                    {
            //                        nplDangKySelected.CloneToDB(null);
            //                        nplDangKySelected.Delete();
            //                    }
            //                }

            //                if (nplDangKySelected.SoTiepNhan == 0 && nplDangKySelected.ID > 0)
            //                    nplDangKySelected.Delete();

            //                //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            //                #region Log
            //                try
            //                {
            //                    string where = "1 = 1";
            //                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", nplDangKySelected.ID, LoaiKhaiBao.NguyenPhuLieu);
            //                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
            //                    if (listLog.Count > 0)
            //                    {
            //                        long idLog = listLog[0].IDLog;
            //                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
            //                        long idDK = listLog[0].ID_DK;
            //                        string guidstr = listLog[0].GUIDSTR_DK;
            //                        string userKhaiBao = listLog[0].UserNameKhaiBao;
            //                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
            //                        string userSuaDoi = GlobalSettings.UserLog;
            //                        DateTime ngaySuaDoi = DateTime.Now;
            //                        string ghiChu = listLog[0].GhiChu;
            //                        bool isDelete = true;
            //                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
            //                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
            //                    }
            //                }
            //                catch (Exception ex)
            //                {
            //                    ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
            //                    return;
            //                }
            //                #endregion
            //            }
            //        }
            //        search();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion Code cũ

        }
        private string checkDataHangImport(NguyenPhuLieuDangKy nplDangky)
        {
            string st = "";
            foreach (NguyenPhuLieu npl in nplDangky.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplDaDuyet = new Company.BLL.SXXK.NguyenPhuLieu();
                nplDaDuyet.Ma = npl.Ma;
                nplDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                nplDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (nplDaDuyet.Load())
                {
                    if (npl.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + nplDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + nplDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Nguyên phụ liệu có mã '" + npl.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Material has code '" + npl.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(NguyenPhuLieuDangKyCollection collection)
        {
            string st = "";
            foreach (NguyenPhuLieuDangKy nplDangky in collection)
            {
                NguyenPhuLieuDangKy nplInDatabase = NguyenPhuLieuDangKy.Load(nplDangky.ID);
                if (nplInDatabase != null)
                {
                    if (nplInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //  st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + nplDangky.ID + " already approved.\n";

                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(nplDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(nplDangky);
                    }
                }
                else
                {
                    if (nplInDatabase.ID > 0)
                        nplInDatabase.ID = 0;
                    tmpCollection.Add(nplDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    NguyenPhuLieuDangKyCollection nplDKCollection = (NguyenPhuLieuDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(nplDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //  ShowMessage("Import thành công", false);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        // ShowMessage("Import thành công", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //ShowMessage("" + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách nguyên phụ liệu",false);
                //Message("MSG_REC02", "", false);
                MLMessages("Chưa chọn danh sách nguyên phụ liệu", "MSG_REC02", "", false);
                return;
            }
            try
            {
                NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                            nplDangKySelected.LoadNPLCollection();
                            col.Add(nplDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage(" " + ex.Message, false);
            }

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    NguyenPhuLieuSendForm f = new NguyenPhuLieuSendForm();
                    npldk = (NguyenPhuLieuDangKy)e.Row.DataRow;
                    npldk.NPLCollection = NguyenPhuLieu.SelectCollectionBy_Master_ID(npldk.ID);
                    f.nplDangKy = npldk;
                    f.ShowDialog(this);
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void NguyenPhuLieuManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }


        private void dgList_DeletingRecord(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
            #region code cũ
            //try
            //{
            //    // if (ShowMessage("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa danh sách nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
            //                MsgSend sendXML = new MsgSend();
            //                sendXML.LoaiHS = "NPL";
            //                sendXML.master_id = nplDangKySelected.ID;
            //                if (sendXML.Load())
            //                {
            //                    // ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);                                
            //                    MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + "", false);
            //                }
            //                else
            //                {
            //                    if (nplDangKySelected.ID > 0)
            //                    {
            //                        nplDangKySelected.Delete();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            #endregion code cũ

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void btnSuaNPL_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                this.SuaNPLDaDuyet();
            }
            else
            {
                ShowMessage("Chưa chọn thông tin để sửa.", false);
                return;
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void btnFindError_Click(object sender, EventArgs e)
        {
            // Thực hiện tìm kiếm.            
            try
            {
                this.nplDangKyCollection = NguyenPhuLieuDangKy.SelectCollectionMaNPL(" and NPL.DVT_ID =''", "ID desc");
                dgList.DataSource = this.nplDangKyCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "DANH SÁCH NGUYÊN PHỤ LIỆU_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XUẤT KÈM THEO THÔNG TIN SP KHÔNG ? ", true) == "No")
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {

                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    try
                    {
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            else
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {
                    try
                    {
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ma", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ten", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ HS" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐƠN VỊ TÍNH" });
                        dgList.DataSource = tmpCollection;
                        dgList.Refetch();

                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        dgList.Tables[0].Columns.Remove("Ma");
                        dgList.Tables[0].Columns.Remove("Ten");
                        dgList.Tables[0].Columns.Remove("MaHS");
                        dgList.Tables[0].Columns.Remove("DVT");
                        btnSearch_Click(null, null);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void mnuUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        NguyenPhuLieuDangKyCollection NpldkColl = new NguyenPhuLieuDangKyCollection();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            NpldkColl.Add((NguyenPhuLieuDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < NpldkColl.Count; i++)
                        {
                            NpldkColl[i].LoadNPLCollection();
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.nplDangKy = NpldkColl[i];
                            f.formType = "NPL";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                } 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAutoSend_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "NPL";
            f.ShowDialog(this);
        }


    }
}