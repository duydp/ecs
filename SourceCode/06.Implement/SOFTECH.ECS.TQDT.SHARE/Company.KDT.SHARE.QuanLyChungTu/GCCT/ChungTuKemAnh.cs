﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components.Nodes.GC;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class ChungTuKemAnh
    {
        public List<ChungTuKemAnhChiTiet> ListChungTuKemAnhChiTiet = new List<ChungTuKemAnhChiTiet>();

        public void LoadListChungTuKemAnhChiTiet()
        {
            ListChungTuKemAnhChiTiet = (List<ChungTuKemAnhChiTiet>)ChungTuKemAnhChiTiet.SelectCollectionBy_ChungTuKemAnhID(this.ID);
        }

        public void InsertUpdateFull(List<ChungTuKemAnhChiTiet> listCTDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);


                        foreach (ChungTuKemAnhChiTiet ctct in listCTDK)
                        {
                            ctct.ChungTuKemAnhID = this.ID;
                            if (id == 0)
                                ctct.ID = 0;

                            if (ctct.ID == 0)
                                ctct.Insert(transaction);
                            else
                                ctct.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (ChungTuKemAnhChiTiet item in ListChungTuKemAnhChiTiet)
                        {
                            item.ChungTuKemAnhID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static XmlNode ConvertCollectionCTDinhKemToXML(XmlDocument doc, List<ChungTuKemAnh> COCollection, long TKMD_ID, List<ChungTuKemAnhChiTiet> ListCTCT)
        {
            try
            {
                //List<ChungTuKemAnhChiTiet> ListCTCT = new List<ChungTuKemAnhChiTiet>();
                //ListCTCT =ChungTuKemAnhChiTiet.SelectCollectionBy_ChungTuKemID(tk)
                if (COCollection == null || COCollection.Count == 0)
                {
                    COCollection = (List<ChungTuKemAnh>)ChungTuKemAnh.SelectCollectionBy_ID_TK(TKMD_ID);
                }
                XmlElement CHUNG_TU_CO = doc.CreateElement("CHUNG_TU_KEM");
                foreach (ChungTuKemAnh co in COCollection)
                {
                    XmlElement CoItem = doc.CreateElement("CHUNG_TU_KEM.ITEM");
                    CHUNG_TU_CO.AppendChild(CoItem);

                    XmlElement SO_CT = doc.CreateElement("SO_CT");
                    SO_CT.InnerText = FontConverter.Unicode2TCVN(co.SO_CT);
                    CoItem.AppendChild(SO_CT);

                    XmlElement NGAY_CAP_CO = doc.CreateElement("NGAY_CT");
                    NGAY_CAP_CO.InnerText = co.NGAY_CT.ToString("yyyy-MM-dd");
                    CoItem.AppendChild(NGAY_CAP_CO);

                    //CHUA CO PHAI BO SUNG VA KO DC TRONG
                    XmlElement NGUOI_KY = doc.CreateElement("MA_LOAI_CT");
                    NGUOI_KY.InnerText = co.MA_LOAI_CT;
                    CoItem.AppendChild(NGUOI_KY);

                    //NOI PHAT HANH LA TO CHUC CAP
                    XmlElement NOI_PHAT_HANH = doc.CreateElement("DIENGIAI");
                    NOI_PHAT_HANH.InnerText = FontConverter.Unicode2TCVN(co.DIENGIAI);
                    CoItem.AppendChild(NOI_PHAT_HANH);

                    XmlElement DINH_KEM = doc.CreateElement("DINH_KEM");
                    //doc.DocumentElement.PrependChild(DINH_KEM);
                    CoItem.AppendChild(DINH_KEM);
                    //XmlElement DINHKEM_ITEM;
                    //XmlElement DINHKEM_ITEM_CT;//
                    //XmlText DINHKEM_ITEMtext;
                    foreach (ChungTuKemAnhChiTiet ctct in ListCTCT)
                    {

                        XmlElement DINHKEM_ITEM = doc.CreateElement("DINH_KEM.ITEM");
                        DINH_KEM.AppendChild(DINHKEM_ITEM);

                        XmlElement TenFile = doc.CreateElement("TENFILE");
                        TenFile.InnerText = ctct.FileName.Trim();
                        DINHKEM_ITEM.AppendChild(TenFile);

                        XmlElement NoiDung = doc.CreateElement("NOIDUNG");
                        //DINHKEM_ITEM.Value = "xmlns:dt=" ;//urn:schemas-microsoft-com:datatypes" dt:dt="bin.base64"";
                        // string 
                        // NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, 0, ctct.NoiDung.Length-1, Base64FormattingOptions.InsertLineBreaks);
                        NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.None);
                        DINHKEM_ITEM.AppendChild(NoiDung);
                        //string tempp = System.Convert.ToBase64String(ctct.NoiDung);

                        XmlAttribute AttNoidung1 = doc.CreateAttribute("xmlns:dt");
                        AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                        NoiDung.Attributes.Append(AttNoidung1);
                        // NoiDung.SetAttributeNode(AttNoidung1.LocalName, "");

                        XmlAttribute AttNoidung2 = doc.CreateAttribute(@"dt:dt");
                        AttNoidung2.Value = "bin.base64";
                        //AttNoidung2.InnerXml.Replace("dt", "dt:dt");
                        NoiDung.Attributes.Append(AttNoidung2);
                    }

                    // Lưu ý: Nếu không thay thế sẽ bị lỗi.
                    DINH_KEM.InnerXml = DINH_KEM.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");
                }
                if (CHUNG_TU_CO.ChildNodes.Count == 0)
                    return null;
                return CHUNG_TU_CO;
            }
            catch (Exception ex) { throw ex; }

            return null;
        }

        public static XmlElement ConvertCollectionCTDinhKemToXMLMoi(XmlDocument doc, List<ChungTuKemAnh> COCollection, long TKMD_ID, List<ChungTuKemAnhChiTiet> ListCTCT, XmlElement CHUNG_TU_CO)
        {
            //List<ChungTuKemAnhChiTiet> ListCTCT = new List<ChungTuKemAnhChiTiet>();
            //ListCTCT =ChungTuKemAnhChiTiet.SelectCollectionBy_ChungTuKemID(tk)
            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = (List<ChungTuKemAnh>)ChungTuKemAnh.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            //XmlNode  CHUNG_TU_CO = doc.GetElementsByTagName("CHUNG_TU_KEM")[0];
            foreach (ChungTuKemAnh co in COCollection)
            {
                XmlElement CoItem = doc.CreateElement("CHUNG_TU_KEM.ITEM");
                CHUNG_TU_CO.AppendChild(CoItem);

                XmlElement SO_CT = doc.CreateElement("SO_CT");
                SO_CT.InnerText = FontConverter.Unicode2TCVN(co.SO_CT);
                CoItem.AppendChild(SO_CT);

                // XmlAttribute SO_CO = doc.CreateAttribute("SO_CT");
                //SO_CT.Value = co.SO_CT;
                //CoItem.Attributes.Append(SO_CT);              

                XmlElement NGAY_CAP_CO = doc.CreateElement("NGAY_CT");
                NGAY_CAP_CO.InnerText = co.NGAY_CT.ToString("yyyy-MM-dd");
                CoItem.AppendChild(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlElement NGUOI_KY = doc.CreateElement("MA_LOAI_CT");
                NGUOI_KY.InnerText = co.MA_LOAI_CT;
                CoItem.AppendChild(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlElement NOI_PHAT_HANH = doc.CreateElement("DIENGIAI");
                NOI_PHAT_HANH.InnerText = FontConverter.Unicode2TCVN(co.DIENGIAI);
                CoItem.AppendChild(NOI_PHAT_HANH);

                XmlElement DINH_KEM = doc.CreateElement("DINH_KEM");
                //doc.DocumentElement.PrependChild(DINH_KEM);
                CoItem.AppendChild(DINH_KEM);
                //XmlElement DINHKEM_ITEM;
                //XmlElement DINHKEM_ITEM_CT;//
                //XmlText DINHKEM_ITEMtext;
                foreach (ChungTuKemAnhChiTiet ctct in ListCTCT)
                {

                    XmlElement DINHKEM_ITEM = doc.CreateElement("DINH_KEM.ITEM");
                    DINH_KEM.AppendChild(DINHKEM_ITEM);

                    XmlElement TenFile = doc.CreateElement("TENFILE");
                    TenFile.InnerText = ctct.FileName.Trim();
                    DINHKEM_ITEM.AppendChild(TenFile);

                    XmlElement NoiDung = doc.CreateElement("NOIDUNG");
                    //DINHKEM_ITEM.Value = "xmlns:dt=" ;//urn:schemas-microsoft-com:datatypes" dt:dt="bin.base64"";
                    // string 
                    // NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, 0, ctct.NoiDung.Length-1, Base64FormattingOptions.InsertLineBreaks);
                    NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.InsertLineBreaks);
                    DINHKEM_ITEM.AppendChild(NoiDung);
                    //string tempp = System.Convert.ToBase64String(ctct.NoiDung);

                    XmlAttribute AttNoidung1 = doc.CreateAttribute("xmlns:dt");
                    AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                    NoiDung.Attributes.Append(AttNoidung1);
                    // NoiDung.SetAttributeNode(AttNoidung1.LocalName, "");

                    XmlAttribute AttNoidung2 = doc.CreateAttribute(@"dt:dt");
                    AttNoidung2.Value = "bin.base64";
                    //AttNoidung2.InnerXml.Replace("dt", "dt:dt");
                    NoiDung.Attributes.Append(AttNoidung2);

                }

            }
            if (CHUNG_TU_CO.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_CO;

        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            //doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
            doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GUIDSTR = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        public string WSSend(string password, string maHQ, string tenHQ, long soTK, string maLH, int namDK,
            long TKMD_ID, string maDN, string tenDN, List<ChungTuKemAnhChiTiet> list, int messgaseTypeKB, int messgaseFunctionKB)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            //set thong tin doanh nghiep
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            XmlNode nodeFromName = nodeFrom.ChildNodes[0];
            nodeFromName.InnerText = tenDN;
            XmlNode nodeFromIdentity = nodeFrom.ChildNodes[1];
            nodeFromIdentity.InnerText = maDN;

            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = tenHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            //set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = messgaseTypeKB + "";
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = messgaseFunctionKB + "";


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GUIDSTR;

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            if (GUIDSTR == "")
            {
                GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GUIDSTR;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDN;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = tenDN;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = tenHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];

            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<ChungTuKemAnh> listChungTuKem = new List<ChungTuKemAnh>();
            listChungTuKem.Add(this);
            nodeDulieu.AppendChild(ChungTuKemAnh.ConvertCollectionCTDinhKemToXML(doc, listChungTuKem, TKMD_ID, list));


            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoChungTuDinhKem(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<ChungTuKemAnhChiTiet> list, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep.Trim();
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep.Trim();

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan.Trim();
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan.Trim();

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            GUIDSTR = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();
            if (GUIDSTR == "")
            {
                GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GUIDSTR;
                this.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep.Trim();
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep.Trim();

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan.Trim();

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<ChungTuKemAnh> listChungTuKem = new List<ChungTuKemAnh>();
            listChungTuKem.Add(this);
            xmlNodeDulieu.AppendChild(ChungTuKemAnh.ConvertCollectionCTDinhKemToXML(xmlDocument, listChungTuKem, TKMD_ID, list));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);

            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GUIDSTR);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.LayPhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKem;
                    message.NoiDungThongBao = noiDung;// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            //xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
            xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocument.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocument.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SOTN, this.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocument.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocument.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SOTN, this.NGAYTN.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SOTN = 0;
                this.NGAYTN = new DateTime(1900, 1, 1);
                this.Update();
                //this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET.ToString();
                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocument.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocument.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SOTN, this.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GUIDSTR);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocument.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocument.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SOTN, this.NGAYTN.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }


            return false;
        }

        public static XmlElement ConvertCollectionCTDinhKemToXMLV3(XmlDocument doc, List<ChungTuKemAnh> COCollection, long TKMD_ID, List<ChungTuKemAnhChiTiet> ListCTCT)
        {
            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = (List<ChungTuKemAnh>)ChungTuKemAnh.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement AttachDocuments = doc.CreateElement(NodeChungTuKem.AttachDocuments);
            
            foreach (ChungTuKemAnh ctk in COCollection)
            {
                XmlElement AttachDocumentItem = doc.CreateElement(NodeChungTuKem.AttachDocumentItem);
                AttachDocuments.AppendChild(AttachDocumentItem);
                XmlElement sequence = doc.CreateElement(NodeChungTuKem.sequence);
                AttachDocumentItem.AppendChild(sequence);
                XmlElement issuer = doc.CreateElement(NodeChungTuKem.issuer);
                AttachDocumentItem.AppendChild(issuer);
                issuer.InnerText = "202";
                XmlElement issue = doc.CreateElement(NodeChungTuKem.issue);
                AttachDocumentItem.AppendChild(issue);
                issue.InnerText = ctk.NGAY_CT.ToString(NgayThang.yyyyMMdd);
                XmlElement reference = doc.CreateElement(NodeChungTuKem.reference);
                AttachDocumentItem.AppendChild(reference);
                reference.InnerText = ctk.SO_CT;
                XmlElement description = doc.CreateElement(NodeChungTuKem.description);
                AttachDocumentItem.AppendChild(description);
                description.InnerText = ctk.DIENGIAI;
                //
                XmlElement AttachedFiles = doc.CreateElement(NodeChungTuKem.AttachedFiles);
                AttachDocumentItem.AppendChild(AttachedFiles);
                ctk.LoadListChungTuKemAnhChiTiet();
                foreach (ChungTuKemAnhChiTiet ctct in ctk.ListChungTuKemAnhChiTiet)
                {
                    XmlElement AttachedFile = doc.CreateElement(NodeChungTuKem.AttachedFile);
                    AttachedFiles.AppendChild(AttachedFile);
                    XmlElement fileName = doc.CreateElement(NodeChungTuKem.fileName);
                    AttachedFile.AppendChild(fileName);
                    fileName.InnerText = ctct.FileName.Trim();
                    XmlElement content = doc.CreateElement(NodeChungTuKem.content);
                    AttachedFile.AppendChild(content);
                    content.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.InsertLineBreaks);

                    XmlAttribute AttNoidung1 = doc.CreateAttribute("xmlns:dt");
                    AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                    content.Attributes.Append(AttNoidung1);

                    XmlAttribute AttNoidung2 = doc.CreateAttribute(@"dt:dt");
                    AttNoidung2.Value = "bin.base64";
                    content.Attributes.Append(AttNoidung2);
                }
                

            }
            if (AttachDocuments.ChildNodes.Count == 0)
                return null;
            return AttachDocuments;

        }

    }
}
