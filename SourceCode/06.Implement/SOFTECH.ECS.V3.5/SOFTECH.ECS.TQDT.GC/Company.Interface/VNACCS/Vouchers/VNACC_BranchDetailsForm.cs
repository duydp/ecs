﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_BranchDetailsForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_BillOfLadingNew billOfLadingNew ;
        public KDT_VNACCS_BillOfLadings_Detail billOfLadingsDetail = null;
        public KDT_VNACCS_BranchDetail branchDetail = null;
        public KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment = null;
        public String Caption;
        public bool IsChange;
        public VNACC_BranchDetailsForm()
        {
            InitializeComponent();

            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTTrongLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            base.SetHandler(this);

            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.SBL.ToString());
        }

        private void VNACC_BranchDetailsForm_Load(object sender, EventArgs e)
        {
            Caption = this.Text;
            SetIDControl();
            BinDataBranchDetail();
            BinDataContainer();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || billOfLadingNew.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private bool ValidateFormBranchDetail(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSTT, errorProvider, "SỐ THỨ TỰ VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoVanDonNhanh, errorProvider, "SỐ VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNguoiGuiHang, errorProvider, "TÊN NGƯỜI GỬI HÀNG CỦA VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiNguoiGuiHang, errorProvider, "ĐỊA CHỈ NGƯỜI GỬI HÀNG CỦA VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNguoiNhanHang, errorProvider, "TÊN NGƯỜI NHẬN HÀNG CỦA VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiNguoiNhanHang, errorProvider, "ĐỊA CHỈ NGƯỜI NHẬN HÀNG CỦA VẬN ĐƠN NHÁNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongHang, errorProvider, "SỐ LƯỢNG HÀNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTongTrongLuongHang, errorProvider, "TỔNG TRỌNG LƯỢNG HÀNG", isOnlyWarning);
                if (billOfLadingsDetail.LoaiHang.ToString() == "1")
                    isValid &= ValidateControl.ValidateNull(txtSoLuongContainer, errorProvider, "TỔNG SỐ LƯỢNG CONTAINER CỦA VẬN ĐƠN NHÁNH", isOnlyWarning);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void BinDataBranchDetail()
        {
            try
            {
                dgListVDN.Refresh();
                dgListVDN.DataSource = billOfLadingsDetail.Collection;
                dgListVDN.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BinDataContainer()
        {
            try
            {
                dgListContainer.Refresh();
                dgListContainer.DataSource = branchDetail.Collection;
                dgListContainer.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAddVDN_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormBranchDetail(false))
                    return;
                if (billOfLadingsDetail == null)
                {
                    billOfLadingsDetail = new KDT_VNACCS_BillOfLadings_Detail();
                    isAddNew = true;
                }
                if (branchDetail == null)
                {
                    branchDetail = new KDT_VNACCS_BranchDetail();
                    isAddNew = true;
                }
                if (billOfLadingsDetail.SoVanDonGoc == null)
                    isAddNew = true;
                if (Convert.ToInt32(txtSoLuongContainer.Text) != branchDetail.Collection.Count)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","TỔNG SỐ LƯỢNG CONTAINER CỦA VẬN ĐƠN NHÁNH VÀ SỐ LƯỢNG CONTAINER Ở THÔNG TIN CONTAINER CỦA VẬN ĐƠN NHÁNH LÀ KHÔNG BẰNG NHAU .", false);
                    uiTab1.SelectedTab.Name = "uiTabPageContainer";
                    uiTabPageContainer.Selected = true;
                    return;
                }
                branchDetail.STT = Convert.ToDecimal(txtSTT.Text.ToString());
                branchDetail.SoVanDon = txtSoVanDonNhanh.Text;
                branchDetail.TenNguoiGuiHang = txtTenNguoiGuiHang.Text;
                branchDetail.DiaChiNguoiGuiHang = txtDiaChiNguoiGuiHang.Text;
                branchDetail.TenNguoiNhanHang = txtTenNguoiNhanHang.Text;
                branchDetail.DiaChiNguoiNhanHang = txtDiaChiNguoiNhanHang.Text;
                branchDetail.TongSoLuongContainer = Convert.ToDecimal(txtSoLuongContainer.Text);
                branchDetail.SoLuongHang = Convert.ToDecimal(txtSoLuongHang.Text);
                branchDetail.DVTSoLuong = ctrDVTLuong1.Code;
                branchDetail.TongTrongLuongHang = Convert.ToDecimal(txtTongTrongLuongHang.Text.ToString());
                branchDetail.DVTTrongLuong = ctrMaDVTTrongLuong.Code;
                if (isAddNew)
                    billOfLadingsDetail.Collection.Add(branchDetail);
                foreach (KDT_VNACCS_BranchDetail item in billOfLadingsDetail.Collection)
                {
                    item.TenNguoiGuiHang = txtTenNguoiGuiHang.Text;
                    item.DiaChiNguoiGuiHang = txtDiaChiNguoiGuiHang.Text;
                    item.TenNguoiNhanHang = txtTenNguoiNhanHang.Text;
                    item.DiaChiNguoiNhanHang = txtDiaChiNguoiNhanHang.Text;
                }
                branchDetail = new KDT_VNACCS_BranchDetail();

                txtSTT.TextChanged -= new EventHandler(txt_TextChanged);
                txtSTT.Text = String.Empty;
                txtSTT.TextChanged += new EventHandler(txt_TextChanged);

                txtSoVanDonNhanh.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonNhanh.Text = String.Empty;
                txtSoVanDonNhanh.TextChanged += new EventHandler(txt_TextChanged);

                txtTenNguoiGuiHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenNguoiGuiHang.Text = String.Empty;
                txtTenNguoiGuiHang.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiNguoiGuiHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiNguoiGuiHang.Text = String.Empty;
                txtDiaChiNguoiGuiHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenNguoiNhanHang.Text = String.Empty;
                txtTenNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiNguoiNhanHang.Text = branchDetail.DiaChiNguoiNhanHang;
                txtDiaChiNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongContainer.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongContainer.Text = String.Empty;
                txtSoLuongContainer.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongHang.Text = String.Empty;
                txtSoLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTongTrongLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongTrongLuongHang.Text = String.Empty;
                txtTongTrongLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                BinDataBranchDetail();
                BinDataContainer();
                branchDetail = null;
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteVDN_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessageTQDT(" Thông báo từ hệ thống ","DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListVDN.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            branchDetail = (KDT_VNACCS_BranchDetail)i.GetRow().DataRow;
                            billOfLadingsDetail.Collection.Remove(branchDetail);
                            int index = 1;
                            foreach (KDT_VNACCS_BranchDetail item in billOfLadingsDetail.Collection)
                            {
                                item.STT = index;
                                item.SoVanDon = billOfLadingsDetail.SoVanDonGoc.ToString() + "-0" + index;
                                index++;
                            }
                            if (branchDetail.ID > 0)
                            {
                                branchDetail.DeleteFull();
                            }
                        }
                    }

                    txtSTT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSTT.Text = String.Empty;
                    txtSTT.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoVanDonNhanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoVanDonNhanh.Text = String.Empty;
                    txtSoVanDonNhanh.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenNguoiGuiHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenNguoiGuiHang.Text = String.Empty;
                    txtTenNguoiGuiHang.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiNguoiGuiHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiNguoiGuiHang.Text = String.Empty;
                    txtDiaChiNguoiGuiHang.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenNguoiNhanHang.Text = String.Empty;
                    txtTenNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiNguoiNhanHang.Text = branchDetail.DiaChiNguoiNhanHang;
                    txtDiaChiNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoLuongContainer.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoLuongContainer.Text = String.Empty;
                    txtSoLuongContainer.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoLuongHang.Text = String.Empty;
                    txtSoLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongTrongLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongTrongLuongHang.Text = String.Empty;
                    txtTongTrongLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                    ShowMessageTQDT(" Thông báo từ hệ thống ","XÓA THÀNH CÔNG", false);
                    BinDataBranchDetail();
                    BinDataContainer();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ReadExcelContainerForm f = new ReadExcelContainerForm();
                f.BranchDetai = branchDetail;
                f.Type = "BillOfLadding";
                f.ShowDialog(this);
                this.SetChange(f.ImportSuccess);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateFormContainer(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoContainer, errorProvider, "SỐ CONTAINER", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoSeal, errorProvider, "SỐ SEAL", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAddContainer_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (txtSoContainer.Text.ToString() == "0")
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","TỔNG SỐ LƯỢNG CONTAINER ĐANG NHẬP VÀO 0 NÊN KHÔNG THỂ NHẬP DANH SÁCH CONTAINER .", false);
                    return;
                }
                if (billOfLadingsDetail.LoaiHang.ToString() == "0")
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","LOẠI HÀNG HÓA ĐÃ CHỌN LÀ : HÀNG RỜI NÊN KHÔNG THỂ NHẬP DANH SÁCH CONTAINER .", false);
                    return;
                }
                if (!ValidateFormContainer(false))
                    return;
                if (branchDetail == null)
                {
                    branchDetail = new KDT_VNACCS_BranchDetail();
                    isAddNew = true;
                }
                if (transportEquipment == null)
                {
                    transportEquipment = new KDT_VNACCS_BranchDetail_TransportEquipment();
                    isAddNew = true;
                }
                transportEquipment.SoContainer = txtSoContainer.Text;
                transportEquipment.SoSeal = txtSoSeal.Text;
                if (isAddNew)
                    branchDetail.Collection.Add(transportEquipment);
                transportEquipment = new KDT_VNACCS_BranchDetail_TransportEquipment();

                txtSoContainer.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoContainer.Text = String.Empty;
                txtSoContainer.TextChanged += new EventHandler(txt_TextChanged);

                txtSoSeal.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoSeal.Text = String.Empty;
                txtSoSeal.TextChanged += new EventHandler(txt_TextChanged);  

                BinDataContainer();
                transportEquipment = null;
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteContainer_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessageTQDT(" Thông báo từ hệ thống ","DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListContainer.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            transportEquipment = (KDT_VNACCS_BranchDetail_TransportEquipment)i.GetRow().DataRow;
                            branchDetail.Collection.Remove(transportEquipment);
                            if (transportEquipment.ID > 0)
                            {
                                transportEquipment.Delete();
                            }
                        }
                    }
                    txtSoContainer.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoContainer.Text = String.Empty;
                    txtSoContainer.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoSeal.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoSeal.Text = String.Empty;
                    txtSoSeal.TextChanged += new EventHandler(txt_TextChanged);                                     

                    ShowMessageTQDT(" Thông báo từ hệ thống ","XÓA THÀNH CÔNG", false);
                    BinDataContainer();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListVDN_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                branchDetail = (KDT_VNACCS_BranchDetail)dgListVDN.GetRow().DataRow;

                txtSTT.TextChanged -= new EventHandler(txt_TextChanged);
                txtSTT.Text = branchDetail.STT.ToString();
                txtSTT.TextChanged += new EventHandler(txt_TextChanged);

                txtSoVanDonNhanh.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonNhanh.Text = branchDetail.SoVanDon;
                txtSoVanDonNhanh.TextChanged += new EventHandler(txt_TextChanged);

                txtSTT.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenNguoiGuiHang.Text = branchDetail.TenNguoiGuiHang;
                txtSTT.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiNguoiGuiHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiNguoiGuiHang.Text = branchDetail.DiaChiNguoiGuiHang;
                txtDiaChiNguoiGuiHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenNguoiNhanHang.Text = branchDetail.TenNguoiNhanHang;
                txtTenNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiNguoiNhanHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiNguoiNhanHang.Text = branchDetail.DiaChiNguoiNhanHang;
                txtDiaChiNguoiNhanHang.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongContainer.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongContainer.Text = branchDetail.TongSoLuongContainer.ToString();
                txtSoLuongContainer.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongHang.Text = branchDetail.SoLuongHang.ToString();
                txtSoLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrDVTLuong1.Code = branchDetail.DVTSoLuong;
                ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtTongTrongLuongHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongTrongLuongHang.Text = branchDetail.TongTrongLuongHang.ToString();
                txtTongTrongLuongHang.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaDVTTrongLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDVTTrongLuong.Code = branchDetail.DVTTrongLuong;
                ctrMaDVTTrongLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                BinDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListContainer_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                transportEquipment = (KDT_VNACCS_BranchDetail_TransportEquipment)dgListContainer.GetRow().DataRow;

                txtSoContainer.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoContainer.Text = transportEquipment.SoContainer;
                txtSoContainer.TextChanged += new EventHandler(txt_TextChanged);

                txtSoSeal.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoSeal.Text = transportEquipment.SoSeal;
                txtSoSeal.TextChanged += new EventHandler(txt_TextChanged);

                BinDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkCoppy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtSTT.Tag = "IMC"; //Số thứ tự vận đơn nhánh
                txtTenNguoiGuiHang.Tag = "IMN"; //Tên người gửi hàng của vận đơn nhánh
                txtDiaChiNguoiGuiHang.Tag = "IMY"; //Địa chỉ người gửi hàng của vận đơn nhánh
                txtTenNguoiNhanHang.Tag = "IMA"; //Tên người nhận hàng của vận đơn nhánh
                txtDiaChiNguoiNhanHang.Tag = "IMT"; //Địa chỉ người nhận hàng của vận đơn nhánh
                txtSoLuongHang.Tag = "REC"; //Tổng số lượng kiện hàng hóa của vận đơn nhánh
                ctrDVTLuong1.TagName = "TTC"; //Mã đơn vị tính của kiện hàng
                txtTongTrongLuongHang.Tag = "BRC"; //Tổng trọng lượng hàng hóa của vận đơn nhánh
                ctrMaDVTTrongLuong.TagName = "BYA"; //Mã đơn vị tính của trọng lượng hàng hóa của vận đơn nhánh.
                txtSoVanDonNhanh.Tag = "BCM"; //Số vận đơn nhánh
                txtSoLuongContainer.Tag = "SCM"; //Tổng số lượng container của vận đơn nhánh
                txtSoContainer.Tag = "RTA"; //Thông tin số container
                txtSoSeal.Tag = "REF"; //Thông tin số seal
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
    }
}
