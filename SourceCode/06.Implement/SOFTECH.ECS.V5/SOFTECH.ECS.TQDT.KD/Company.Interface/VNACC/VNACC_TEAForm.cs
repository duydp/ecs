﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_TEAForm : BaseForm
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();

        public VNACC_TEAForm()
        {
            InitializeComponent();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showHang();
                    break;
                case "cmdNguoiXNK":
                    showNguoiXNK();
                    break;
                case "cmdDieuChinh":
                    showDieuChinh();
                    break;
                case "cmdLuu":
                    Save();
                    break;
            }
        }

        private void showHang()
        {
            VNACC_TEA_HangForm f = new VNACC_TEA_HangForm();
            f.TEA = this.TEA;
            f.ShowDialog();
        }

        private void showNguoiXNK()
        {
            VNACC_TEA_NguoiXNK f = new VNACC_TEA_NguoiXNK();
            f.TEA = this.TEA;
            f.ShowDialog();
        }

        private void showDieuChinh()
        {
            VNACC_TEA_DieuChinhForm f = new VNACC_TEA_DieuChinhForm();
            f.TEA = this.TEA;
            f.ShowDialog();
        }

        private void GetTEA()
        {
            TEA.SoDanhMucMienThue = Convert.ToDecimal(txtSoDanhMucMienThue.Text);
            TEA.PhanLoaiXuatNhapKhau = cbbPhanLoaiXuatNhapKhau.SelectedValue.ToString();
            TEA.CoQuanHaiQuan = txtCoQuanHaiQuan.Text;
            TEA.DiaChiCuaNguoiKhai = txtDiaChiCuaNguoiKhai.Text;
            TEA.SDTCuaNguoiKhai = txtSDTCuaNguoiKhai.Text;
            TEA.ThoiHanMienThue = clcThoiHanMienThue.Value;
            TEA.TenDuAnDauTu = txtTenDuAnDauTu.Text;
            TEA.DiaDiemXayDungDuAn = txtDiaDiemXayDungDuAn.Text;
            TEA.MucTieuDuAn = txtMucTieuDuAn.Text;
            TEA.MaMienGiam = txtMaMienGiam.Text;
            TEA.PhamViDangKyDMMT = txtPhamViDangKyDMMT.Text;
            TEA.NgayDuKienXNK = clcNgayDuKienXNK.Value;
            TEA.GP_GCNDauTuSo = txtGP_GCNDauTuSo.Text;
            TEA.NgayChungNhan = clcNgayChungNhan.Value;
            TEA.CapBoi = txtCapBoi.Text;
            TEA.GhiChu = txtGhiChu.Text;
            TEA.CamKetSuDung = txtCamKetSuDung.Text;
        }

        private void SetTEA()
        {
            txtSoDanhMucMienThue.Text = TEA.SoDanhMucMienThue.ToString();
            cbbPhanLoaiXuatNhapKhau.SelectedValue = TEA.PhanLoaiXuatNhapKhau;
            txtCoQuanHaiQuan.Text = TEA.CoQuanHaiQuan;
            txtDiaChiCuaNguoiKhai.Text = TEA.DiaChiCuaNguoiKhai;
            txtSDTCuaNguoiKhai.Text = TEA.SDTCuaNguoiKhai;
            clcThoiHanMienThue.Value = TEA.ThoiHanMienThue;
            txtTenDuAnDauTu.Text = TEA.TenDuAnDauTu;
            txtDiaDiemXayDungDuAn.Text = TEA.DiaDiemXayDungDuAn;
            txtMucTieuDuAn.Text = TEA.MucTieuDuAn;
            txtMaMienGiam.Text = TEA.MaMienGiam;
            txtPhamViDangKyDMMT.Text = TEA.PhamViDangKyDMMT;
            clcNgayDuKienXNK.Value = TEA.NgayDuKienXNK;
            txtGP_GCNDauTuSo.Text = TEA.GP_GCNDauTuSo;
            clcNgayChungNhan.Value = TEA.NgayChungNhan;
            txtCapBoi.Text = TEA.CapBoi;
            txtGhiChu.Text = TEA.GhiChu;
            txtCamKetSuDung.Text = TEA.CamKetSuDung;
        }

        private void Save()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GetTEA();
                TEA.InsertUpdateFul();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void VNACC_TEAForm_Load(object sender, EventArgs e)
        {
            if (TEA.ID != 0)
            {
                SetTEA();
                LoadGrid();
            }
        }
        private void LoadGrid()
        {
            grDieuChinh.DataSource = TEA.DieuChinhCollection;
            grHang.DataSource = TEA.HangCollection;
            grNguoiXNK.DataSource = TEA.NguoiXNKCollection;
        }




    }
}
