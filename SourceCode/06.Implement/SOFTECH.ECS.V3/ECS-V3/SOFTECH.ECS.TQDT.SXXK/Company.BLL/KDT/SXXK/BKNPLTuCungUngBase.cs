using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class BKNPLTuCungUng 
	{
		#region Private members.
		
		protected long _ID;
		protected long _BangKeHoSoThanhLy_ID;
		protected string _MaNPL = String.Empty;
		protected string _TenNPL = String.Empty;
		protected string _SoHoaDong = String.Empty;
		protected DateTime _NgayHoaDong = new DateTime(1900, 01, 01);
		protected decimal _LuongTuCungUng;
		protected string _DVT_ID = String.Empty;
		protected string _TenDVT = String.Empty;
		protected int _STTHang;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public long BangKeHoSoThanhLy_ID
		{
			set {this._BangKeHoSoThanhLy_ID = value;}
			get {return this._BangKeHoSoThanhLy_ID;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public string TenNPL
		{
			set {this._TenNPL = value;}
			get {return this._TenNPL;}
		}
		public string SoHoaDong
		{
			set {this._SoHoaDong = value;}
			get {return this._SoHoaDong;}
		}
		public DateTime NgayHoaDong
		{
			set {this._NgayHoaDong = value;}
			get {return this._NgayHoaDong;}
		}
		public decimal LuongTuCungUng
		{
			set {this._LuongTuCungUng = value;}
			get {return this._LuongTuCungUng;}
		}
		public string DVT_ID
		{
			set {this._DVT_ID = value;}
			get {return this._DVT_ID;}
		}
		public string TenDVT
		{
			set {this._TenDVT = value;}
			get {return this._TenDVT;}
		}
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_Load";
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) this._BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDong"))) this._SoHoaDong = reader.GetString(reader.GetOrdinal("SoHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDong"))) this._NgayHoaDong = reader.GetDateTime(reader.GetOrdinal("NgayHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTuCungUng"))) this._LuongTuCungUng = reader.GetDecimal(reader.GetOrdinal("LuongTuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) this._TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------

        public static BKNPLTuCungUngCollection SelectCollectionBy_BangKeHoSoThanhLy_ID(long bangKeHoSoThanhLy_ID)
		{
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, bangKeHoSoThanhLy_ID);
			
			BKNPLTuCungUngCollection collection = new BKNPLTuCungUngCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				BKNPLTuCungUng entity = new BKNPLTuCungUng();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDong"))) entity.SoHoaDong = reader.GetString(reader.GetOrdinal("SoHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDong"))) entity.NgayHoaDong = reader.GetDateTime(reader.GetOrdinal("NgayHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTuCungUng"))) entity.LuongTuCungUng = reader.GetDecimal(reader.GetOrdinal("LuongTuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
            reader.Close();
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_BangKeHoSoThanhLy_ID()
		{
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLTuCungUng_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLTuCungUngCollection SelectCollectionAll()
		{
			BKNPLTuCungUngCollection collection = new BKNPLTuCungUngCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				BKNPLTuCungUng entity = new BKNPLTuCungUng();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDong"))) entity.SoHoaDong = reader.GetString(reader.GetOrdinal("SoHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDong"))) entity.NgayHoaDong = reader.GetDateTime(reader.GetOrdinal("NgayHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTuCungUng"))) entity.LuongTuCungUng = reader.GetDecimal(reader.GetOrdinal("LuongTuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLTuCungUngCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			BKNPLTuCungUngCollection collection = new BKNPLTuCungUngCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				BKNPLTuCungUng entity = new BKNPLTuCungUng();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDong"))) entity.SoHoaDong = reader.GetString(reader.GetOrdinal("SoHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDong"))) entity.NgayHoaDong = reader.GetDateTime(reader.GetOrdinal("NgayHoaDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTuCungUng"))) entity.LuongTuCungUng = reader.GetDecimal(reader.GetOrdinal("LuongTuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_Insert";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoHoaDong", SqlDbType.VarChar, this._SoHoaDong);
			db.AddInParameter(dbCommand, "@NgayHoaDong", SqlDbType.DateTime, this._NgayHoaDong);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, this._LuongTuCungUng);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(BKNPLTuCungUngCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTuCungUng item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, BKNPLTuCungUngCollection collection)
        {
            foreach (BKNPLTuCungUng item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_InsertUpdate";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoHoaDong", SqlDbType.VarChar, this._SoHoaDong);
			db.AddInParameter(dbCommand, "@NgayHoaDong", SqlDbType.DateTime, this._NgayHoaDong);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, this._LuongTuCungUng);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(BKNPLTuCungUngCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTuCungUng item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoHoaDong", SqlDbType.VarChar, this._SoHoaDong);
			db.AddInParameter(dbCommand, "@NgayHoaDong", SqlDbType.DateTime, this._NgayHoaDong);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, this._LuongTuCungUng);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(BKNPLTuCungUngCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLTuCungUng item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
        public int DeleteTransactionBy_BangKeHoSoThanhLy_ID(SqlTransaction transaction, long id)
        {
            string spName = "p_KDT_SXXK_BKNPLTuCungUng_DeleteBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, id);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLTuCungUng_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(BKNPLTuCungUngCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLTuCungUng item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(BKNPLTuCungUngCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTuCungUng item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}