using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapGiayPhepXNKVatLieuNoCN_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapGiayPhepXNKVatLieuNoCN_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAHP030 vahp)
        {
            int HangHoa = vahp.HangHoa.Count;
            if (HangHoa <= 10)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 10, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblMaNguoiKhai.Text = vahp.A01.GetValue().ToString().ToUpper();
            lblTenNguoikhai.Text = vahp.A02.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vahp.A08.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vahp.A06.GetValue().ToString().ToUpper();
            lblLoaiGiayPhep.Text = vahp.A07.GetValue().ToString().ToUpper();
            lblPhanLoaiXNK.Text = vahp.A15.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A03.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vahp.A03.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblMaDonViCapPhep.Text = vahp.A04.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vahp.A05.GetValue().ToString().ToUpper();
            lblKetQuaXuLy.Text = vahp.A09.GetValue().ToString().ToUpper();
            lblSoGiayPhep.Text = vahp.A10.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A12.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiayPhep.Text = Convert.ToDateTime(vahp.A12.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiayPhep.Text = "";
            }
            if (Convert.ToDateTime(vahp.A13.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHieuLucGP.Text = Convert.ToDateTime(vahp.A13.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHieuLucGP.Text = "";
            }
            if (Convert.ToDateTime(vahp.A14.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHieuLucGP.Text = Convert.ToDateTime(vahp.A14.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHetHieuLucGP.Text = "";
            }
            lblMaDoanhNghiepXNK.Text = vahp.A16.GetValue().ToString().ToUpper();
            lblTenDoanhNghiepXNK.Text = vahp.A17.GetValue().ToString().ToUpper();
            lblSoQDThanhLap.Text = vahp.A19.GetValue().ToString().ToUpper();
            lblSoGiayCNDKKD.Text = vahp.A20.GetValue().ToString().ToUpper();
            lblDonViCapCNDKKD.Text = vahp.A21.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A22.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayCapCNDKKD.Text = Convert.ToDateTime(vahp.A22.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayCapCNDKKD.Text = "";
            }
            lblMaBuuChinhDN.Text = vahp.A24.GetValue().ToString().ToUpper();
            lblDiachiDN.Text = vahp.A18.GetValue().ToString().ToUpper();
            lblMaQuocGiaDN.Text = vahp.A23.GetValue().ToString().ToUpper();
            lblSoDienThoaiDN.Text = vahp.A25.GetValue().ToString().ToUpper();
            lblSoFaxDN.Text = vahp.A26.GetValue().ToString().ToUpper();
            lblEmailDN.Text = vahp.A27.GetValue().ToString().ToUpper();
            lblMaPhuongTienVC.Text = vahp.A28.GetValue().ToString().ToUpper();
            lblTenPhuongTienVC.Text = vahp.MTN.GetValue().ToString().ToUpper();
            lblMaCuaKhauXNHang.Text = vahp.B05.GetValue().ToString().ToUpper();
            lblTenCuaKhauXNHang.Text = vahp.B06.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vahp.A29.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayBatDauXNK.Text = Convert.ToDateTime(vahp.A29.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayBatDauXNK.Text = "";
            }
            if (Convert.ToDateTime(vahp.A30.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKetThucXNK.Text = Convert.ToDateTime(vahp.A30.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKetThucXNK.Text = "";
            }
            lblGhiChu.Text = vahp.A31.GetValue().ToString().ToUpper();
            lblDaiDienDVCapPhep.Text = vahp.A11.GetValue().ToString().ToUpper();
        }



    }
}
