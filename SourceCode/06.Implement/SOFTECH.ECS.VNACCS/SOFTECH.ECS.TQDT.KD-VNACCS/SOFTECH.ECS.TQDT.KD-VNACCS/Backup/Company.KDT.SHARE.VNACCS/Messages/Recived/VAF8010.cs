using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF8010 : BasicVNACC
    {
       // public List<VAF5110_HANG> HangMD { get; set; }
        public void LoadVAF8010(string strResult)
        {
            try
            {

                this.GetObject<VAF8010>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF8010, false, VAF8010.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8010.TongSoByte);
               
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
