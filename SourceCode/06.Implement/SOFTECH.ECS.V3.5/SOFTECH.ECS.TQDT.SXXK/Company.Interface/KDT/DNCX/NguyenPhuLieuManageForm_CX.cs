﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.Interface.KDT.SXXK.DNCX;
using System.Data;
using Company.Interface.KDT.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuManageForm_CX : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private List<HangDuaVaoDangKy> nplDangKyCollection = new List<HangDuaVaoDangKy>();
        private List<HangDuaVaoDangKy> tmpCollection = new List<HangDuaVaoDangKy>();
        /// <summary>
        /// Thông tin nguyên phụ liệu đang dược chọn.
        /// </summary>
        private readonly HangDuaVaoDangKy currentNPLDangKy = new HangDuaVaoDangKy();
        HangDuaVaoDangKy npldk = new HangDuaVaoDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private DataTable dt;
        public NguyenPhuLieuManageForm_CX()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                saveFileDialog1.InitialDirectory = Application.StartupPath;
                cbStatus.SelectedIndex = 0;

                //An nut Xac nhan
                LaySoTiepNhan.Visible = InheritableBoolean.False;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtMaNPL.Text))
            {
                this.search();
            }
            else
            {
                search_NPL(txtMaNPL.Text.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.nplDangKyCollection = (List<HangDuaVaoDangKy>)HangDuaVaoDangKy.SelectCollectionDynamic(where, "ID desc");
                try
                {
                    dt = HangDuaVaoDangKy.SelectDynamicFull(where, "ID desc").Tables[0];
                }
                catch (Exception ex) 
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
    
                }
                dgList.DataSource = this.nplDangKyCollection;

                this.setCommandStatus();

                this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void search_NPL(string ma_NPL)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = " NPL.Ma like '%" + ma_NPL + "%'";
                where += " AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", donViHaiQuanNewControl1.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.nplDangKyCollection = (List<HangDuaVaoDangKy>)HangDuaVaoDangKy.SelectCollectionDynamicNPL(where, "ID desc");
                dgList.DataSource = this.nplDangKyCollection;
                this.setCommandStatus();

                this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBo.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_HUY)
            {
                InheritableBoolean cho_duyet = Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET ? InheritableBoolean.True : InheritableBoolean.False;
                cmdSend.Enabled =  cmdSend3.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled =  InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled =  cho_duyet;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
                //btnSuaNPL.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                cmdSend.Enabled =  InheritableBoolean.False;
                cmdSingleDownload.Enabled =  InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LaySoTiepNhan.Enabled = InheritableBoolean.True;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.False;
                //btnSuaNPL.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdSend.Enabled =  InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled =  InheritableBoolean.False;
                cmdCancel.Enabled =  InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = InheritableBoolean.True;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = true;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //btnSuaNPL.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_HUY)
            {
                cmdSend.Enabled =  cmdSend3.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled =  InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled =  InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatNPLChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = true;
                cmdXoa.Enabled = cmdXoa2.Enabled = InheritableBoolean.True;
                //btnSuaNPL.Enabled = false;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                {
                    cmdSend.Visible = InheritableBoolean.False;
                    cmdSingleDownload.Visible = InheritableBoolean.False;
                    cmdCancel.Visible = InheritableBoolean.False;
                    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    LaySoTiepNhan.Visible = InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    DongBo.Visible = InheritableBoolean.False;
                    btnDelete.Visible = false;
                }
            }
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                        break;
                    case 2:
                        // e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                    //Update by Huỳnh Ngọc Khánh - 28/02/2012 
                    //Contents: Thêm trạng thái CHỜ HỦY và ĐÃ HỦY 
                    case 11:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for canceled";

                        }
                        break;
                    case 10:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Canceled";

                        }
                        break;





                }

                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã Nguyên phụ liệu vào Grid Danh sách nguyên phụ liệu


                //Company.BLL.KDT.SXXK.NguyenPhuLieuCollection NguyenphulieuCollection = new NguyenPhuLieuCollection();
                IList<HangDuaVao> NguyenphulieuCollection = new List<HangDuaVao>();
                Int64 ValueIdCell = Int64.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaNPL = "";

                NguyenphulieuCollection = HangDuaVao.SelectCollectionBy_Master_ID(ValueIdCell);
                HangDuaVao entityNguyenPhuLieu = new HangDuaVao();

                if (NguyenphulieuCollection.Count > 0)
                {


                    //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                    entityNguyenPhuLieu = NguyenphulieuCollection[0];
                    e.Row.Cells["Ma"].Text = entityNguyenPhuLieu.Ma;

                    //lấy mã Nguyên phụ liệu bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (HangDuaVao EntityNPL in NguyenphulieuCollection)
                    {

                        TatCaMaNPL = TatCaMaNPL + EntityNPL.Ma + "\n";

                    }
                    e.Row.Cells["Ma"].ToolTipText = TatCaMaNPL;


                }

            }
        }

        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdXuatNPLChoPhongKhai":
                    XuatNPLChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdMessage":               
                    break;
                case "cmdExportExcel":
                    ExportExcel();
                    break;
                case "cmdUpdateStatus":
                    mnuUpdateStatus_Click(null, null);
                    break;
            }
        }
        private void mnuUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<HangDuaVaoDangKy> NpldkColl = new List<HangDuaVaoDangKy>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            NpldkColl.Add((HangDuaVaoDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < NpldkColl.Count; i++)
                        {
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.hangDuavao = NpldkColl[i];
                            f.formType = "HDV";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ExportExcel()
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "Danh sách nguyên phụ liệu_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (ShowMessage("Bạn có muốn xuất kèm theo thông tin NPL không ? ", true) == "No")
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {

                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    try
                    {
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi mở file " + ex.Message, false);

                    }
                }
            }
            else
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {
                    try
                    {
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ma", ColumnType.Text, EditType.NoEdit) { Caption = "Mã NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ten", ColumnType.Text, EditType.NoEdit) { Caption = "Tên NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn vị tính" });
                        dgList.DataSource = dt;
                        dgList.Refetch();

                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi mở file " + ex.Message, false);

                        }

                        dgList.Tables[0].Columns.Remove("Ma");
                        dgList.Tables[0].Columns.Remove("Ten");
                        dgList.Tables[0].Columns.Remove("MaHS");
                        dgList.Tables[0].Columns.Remove("DVT"); ;
                        btnSearch_Click(null,null);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
        
                    }

                }
            } 
        }
        private void SuaNPLDaDuyet()
        {
            try
            {
                NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
                npldk = (NguyenPhuLieuDangKy)dgList.GetRow().DataRow;
                npldk.LoadNPLCollection();
                if (NguyenPhuLieuDangKySUA.SelectCollectionBy_IDNPLDK(npldk.ID).Count == 0)
                {
                    string msg = "DOANH NGHIỆP CÓ MUỐN CHUYỂN SANG TRẠNG THÁI SỬA NGUYÊN PHỤ LIỆU KHÔNG?";
                    msg += "\n\nSỐ TIẾP NHẬN: " + npldk.SoTiepNhan.ToString();
                    msg += "\n----------------------";
                    msg += "\nCÓ " + npldk.NPLCollection.Count.ToString() + " NGUYÊN PHỤ LIỆU ĐĂNG KÝ ĐÃ ĐƯỢC DUYỆT";
                    if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                    {
                        npldk.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;                      
                        npldk.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void inPhieuTN()
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "NGUYÊN PHỤ LIỆU";
                Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = nplDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = nplDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void XuatNPLChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách nguyên phụ liệu cần xuất.", false);
                MLMessages("Chưa chọn danh sách nguyên phụ liệu cần xuất.", "MSG_REC02", "", false);
                return;
            }
            try
            {
                NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sonpl = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NguyenPhuLieuDangKy nplSelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                            nplSelected.LoadNPLCollection();
                            col.Add(nplSelected);
                            sonpl++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    // ShowMessage("Xuất thành công " + sonpl + " nguyên phụ liệu.", false);
                    MLMessages("Xuất thành công " + sonpl + " nguyên phụ liệu.", "MSG_NPL02", "" + sonpl, false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    List<HangDuaVaoDangKy> nplDangKy = new List<HangDuaVaoDangKy>();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        nplDangKy.Add((HangDuaVaoDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO NGUYÊN PHỤ LIỆU NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < nplDangKy.Count; i++)
                    {
                        if (nplDangKy[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            nplDangKy[i].loadHang();
                            msgAccept += "[" + (i + 1) + "]-[" + nplDangKy[i].ID + "]-[" + nplDangKy[i].SoTiepNhan + "]-[" + nplDangKy[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(nplDangKy[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (HangDuaVaoDangKy item in nplDangKy)
                        {
                            item.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.TransgferHDVToSXXK(item);
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ", false);
                    btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }         
        }
        public void XoaNguyenPhuLieuDangKy(GridEXSelectedItemCollection items)
        {

            try
            {
                if (items.Count == 0) return;
                List<HangDuaVaoDangKy> itemsDelete = new List<HangDuaVaoDangKy>();
                int k = 1;
                string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN XÓA DANH SÁCH KHAI BÁO NGUYÊN PHỤ LIỆU NÀY KHÔNG ?\n";
                string msgDelete = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangDuaVaoDangKy npldk = (HangDuaVaoDangKy)i.GetRow().DataRow;
                        npldk.loadHang();
                        msgDelete += "[" + (k) + "]-[" + npldk.ID + "]-[" + npldk.SoTiepNhan + "]-[" + npldk.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(npldk.TrangThaiXuLy).ToUpper() + "]\n";
                        k++;
                        itemsDelete.Add(npldk);
                    }
                }
                if (itemsDelete.Count >= 1)
                {
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgDelete + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ",msgWarning, true) == "Yes")
                    {
                        foreach (HangDuaVaoDangKy item in itemsDelete)
                        {
                            foreach (HangDuaVao itemDelete in item.DanhSachHangDuaVao)
                            {
                                try
                                {
                                    Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                    NPL.Ma = itemDelete.Ma;
                                    NPL.MaHaiQuan = item.MaHaiQuan;
                                    NPL.MaDoanhNghiep = item.MaDoanhNghiep;
                                    NPL.Delete();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.DeleteFull(item, item.MaDoanhNghiep, item.MaHaiQuan);
                            item.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {

            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaNguyenPhuLieuDangKy(items);
            btnSearch_Click(null,null);
        }
        private string checkDataHangImport(HangDuaVaoDangKy nplDangky)
        {
            string st = "";
            foreach (HangDuaVao npl in nplDangky.DanhSachHangDuaVao)
            {
                BLL.SXXK.NguyenPhuLieu nplDaDuyet = new Company.BLL.SXXK.NguyenPhuLieu();
                nplDaDuyet.Ma = npl.Ma;
                nplDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                nplDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (nplDaDuyet.Load())
                {
                    if (npl.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + nplDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + nplDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Nguyên phụ liệu có mã '" + npl.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Material has code '" + npl.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(List<HangDuaVaoDangKy> collection)
        {
            string st = "";
            foreach (HangDuaVaoDangKy nplDangky in collection)
            {
                HangDuaVaoDangKy nplInDatabase = HangDuaVaoDangKy.Load(nplDangky.ID);
                if (nplInDatabase != null)
                {
                    if (nplInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //  st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + nplDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + nplDangky.ID + " already approved.\n";

                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(nplDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(nplDangky);
                    }
                }
                else
                {
                    if (nplInDatabase.ID > 0)
                        nplInDatabase.ID = 0;
                    tmpCollection.Add(nplDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(List<HangDuaVao>));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    List<HangDuaVaoDangKy> nplDKCollection = (List<HangDuaVaoDangKy>)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(nplDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                          //  NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //  ShowMessage("Import thành công", false);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                      //  NguyenPhuLieuDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        // ShowMessage("Import thành công", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("" + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách nguyên phụ liệu",false);
                //Message("MSG_REC02", "", false);
                MLMessages("Chưa chọn danh sách nguyên phụ liệu", "MSG_REC02", "", false);
                return;
            }
            try
            {
                NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            NguyenPhuLieuDangKy nplDangKySelected = (NguyenPhuLieuDangKy)i.GetRow().DataRow;
                            nplDangKySelected.LoadNPLCollection();
                            col.Add(nplDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            { 
                NguyenPhuLieuSendForm_CX f = new NguyenPhuLieuSendForm_CX();
                f.nplDangKy = (HangDuaVaoDangKy)e.Row.DataRow;
                f.ShowDialog(this);
                btnSearch_Click(null, null);
            }
        }

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO || (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET))
                {
                    txtNamTiepNhan.Text = string.Empty;
                    txtNamTiepNhan.Value = 0;
                    txtNamTiepNhan.Enabled = false;
                    txtSoTiepNhan.Value = 0;
                    txtSoTiepNhan.Text = string.Empty;
                    txtSoTiepNhan.Enabled = false;
                }
                else
                {
                    txtNamTiepNhan.Value = DateTime.Today.Year;
                    txtNamTiepNhan.Enabled = true;
                    txtSoTiepNhan.Enabled = true;
                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void NguyenPhuLieuManageForm_Shown(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }


        private void dgList_DeletingRecord(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void btnSuaNPL_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                this.SuaNPLDaDuyet();
            }
            else
            {
                ShowMessage("CHƯA CHỌN THÔNG TIN ĐỂ SỬA.", false);
                return;
            }
        }
        private void uiMessage_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count <= 0) return;

                HangDuaVaoDangKy nplDangKySelected = (HangDuaVaoDangKy)dgList.CurrentRow.DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = nplDangKySelected.ID;
                form.DeclarationIssuer = DeclarationIssuer.DNCX_HANG_VAO;
                form.ShowDialog(this);
            }
            catch (System.Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow() != null)
                    npldk = (HangDuaVaoDangKy)dgList.GetRow().DataRow;

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                sendXML.master_id = npldk.ID;
                bool isSend = sendXML.Load() || npldk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || npldk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET
                    || npldk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY;

                cmdSend.Enabled =  isSend ? InheritableBoolean.False : InheritableBoolean.True; ;
                cmdSingleDownload.Enabled =  isSend ? InheritableBoolean.True : InheritableBoolean.False;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = npldk.ID;
                form.DeclarationIssuer = DeclarationIssuer.DNCX_HANG_VAO;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }
    }
}