﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_TransportEquipmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TransportEquipmentForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem21 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem22 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.cbbLoaiContainer = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSealHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoKMVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThoiGianVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDaiDienDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTuyenDuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaViTriDoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaViTriXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiemDoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrMaCangCKGaDoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrMaCangGaCKXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrDiaDiemXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayHetHanHDVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.dateNgayHopDongVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSoHopDongVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.timeKTVC = new System.Windows.Forms.DateTimePicker();
            this.timeBatDauVC = new System.Windows.Forms.DateTimePicker();
            this.dateNgayKetThucVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.dateNgayBatDauVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.dateNgayCP = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.dateNgayTK = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrCoQuanHQGS = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrCoQuanHQToKhai = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbMaMDVC = new Janus.Windows.EditControls.UIComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.cbbMaPTVC = new Janus.Windows.EditControls.UIComboBox();
            this.cbbCoBao = new Janus.Windows.EditControls.UIComboBox();
            this.cbbMaLHVC = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNhaVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiNhaVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNhaVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label9 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 843), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 843);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 819);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 819);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(1000, 843);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdUpdateGuidString});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(572, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "KHAI BÁO";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "LƯU LẠI";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "NHẬN PHẢN HỒI";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "KẾT QUẢ XỬ LÝ";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "CẬP NHẬT CHUỖI PHẢN HỒI";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1206, 28);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1000, 843);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.dgListTotal);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 901);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(977, 443);
            this.uiGroupBox7.TabIndex = 0;
            this.uiGroupBox7.Text = "Thông tin hàng hóa";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(3, 100);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.Size = new System.Drawing.Size(971, 340);
            this.dgListTotal.TabIndex = 41;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTotal.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListTotal_RowDoubleClick);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.btnDelete);
            this.uiGroupBox9.Controls.Add(this.btnAdd);
            this.uiGroupBox9.Controls.Add(this.cbbLoaiContainer);
            this.uiGroupBox9.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox9.Controls.Add(this.txtSoSealHQ);
            this.uiGroupBox9.Controls.Add(this.txtSoSeal);
            this.uiGroupBox9.Controls.Add(this.txtSoContainer);
            this.uiGroupBox9.Controls.Add(this.label55);
            this.uiGroupBox9.Controls.Add(this.label51);
            this.uiGroupBox9.Controls.Add(this.label52);
            this.uiGroupBox9.Controls.Add(this.label59);
            this.uiGroupBox9.Controls.Add(this.label60);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(971, 83);
            this.uiGroupBox9.TabIndex = 0;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(740, 50);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 45;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(658, 50);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(76, 23);
            this.btnAdd.TabIndex = 44;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cbbLoaiContainer
            // 
            this.cbbLoaiContainer.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiContainer.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "20";
            uiComboBoxItem1.Value = "20";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "40";
            uiComboBoxItem2.Value = "40";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "LK";
            uiComboBoxItem3.Value = "LK";
            this.cbbLoaiContainer.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cbbLoaiContainer.Location = new System.Drawing.Point(776, 17);
            this.cbbLoaiContainer.Name = "cbbLoaiContainer";
            this.cbbLoaiContainer.Size = new System.Drawing.Size(150, 21);
            this.cbbLoaiContainer.TabIndex = 41;
            this.cbbLoaiContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Location = new System.Drawing.Point(106, 50);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(290, 21);
            this.txtSoVanDon.TabIndex = 42;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSealHQ
            // 
            this.txtSoSealHQ.Location = new System.Drawing.Point(478, 50);
            this.txtSoSealHQ.Name = "txtSoSealHQ";
            this.txtSoSealHQ.Size = new System.Drawing.Size(200, 21);
            this.txtSoSealHQ.TabIndex = 43;
            this.txtSoSealHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal
            // 
            this.txtSoSeal.Location = new System.Drawing.Point(480, 17);
            this.txtSoSeal.Name = "txtSoSeal";
            this.txtSoSeal.Size = new System.Drawing.Size(200, 21);
            this.txtSoSeal.TabIndex = 40;
            this.txtSoSeal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.Location = new System.Drawing.Point(106, 17);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(290, 21);
            this.txtSoContainer.TabIndex = 39;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label55.Location = new System.Drawing.Point(22, 54);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(68, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Số vận đơn :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label51.Location = new System.Drawing.Point(406, 54);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(63, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Số seal HQ:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label52.Location = new System.Drawing.Point(689, 21);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(81, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Loại container :";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label59.Location = new System.Drawing.Point(406, 21);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(48, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "Số seal :";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label60.Location = new System.Drawing.Point(22, 21);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(74, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "Số container :";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtSoKMVC);
            this.uiGroupBox6.Controls.Add(this.txtGhiChu);
            this.uiGroupBox6.Controls.Add(this.txtSoFaxHQ);
            this.uiGroupBox6.Controls.Add(this.txtSoDienThoaiHQ);
            this.uiGroupBox6.Controls.Add(this.txtThoiGianVC);
            this.uiGroupBox6.Controls.Add(this.txtTenDaiDienDN);
            this.uiGroupBox6.Controls.Add(this.txtTuyenDuong);
            this.uiGroupBox6.Controls.Add(this.label47);
            this.uiGroupBox6.Controls.Add(this.label46);
            this.uiGroupBox6.Controls.Add(this.label45);
            this.uiGroupBox6.Controls.Add(this.label44);
            this.uiGroupBox6.Controls.Add(this.label25);
            this.uiGroupBox6.Controls.Add(this.label26);
            this.uiGroupBox6.Controls.Add(this.label31);
            this.uiGroupBox6.Controls.Add(this.label27);
            this.uiGroupBox6.Controls.Add(this.label28);
            this.uiGroupBox6.Controls.Add(this.label29);
            this.uiGroupBox6.Controls.Add(this.label30);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 744);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(977, 157);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Thông tin tuyến đường";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtSoKMVC
            // 
            this.txtSoKMVC.Location = new System.Drawing.Point(556, 85);
            this.txtSoKMVC.Name = "txtSoKMVC";
            this.txtSoKMVC.Size = new System.Drawing.Size(127, 21);
            this.txtSoKMVC.TabIndex = 37;
            this.txtSoKMVC.Text = "0";
            this.txtSoKMVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(109, 119);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(819, 21);
            this.txtGhiChu.TabIndex = 38;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoFaxHQ
            // 
            this.txtSoFaxHQ.Location = new System.Drawing.Point(176, 85);
            this.txtSoFaxHQ.Name = "txtSoFaxHQ";
            this.txtSoFaxHQ.Size = new System.Drawing.Size(223, 21);
            this.txtSoFaxHQ.TabIndex = 36;
            this.txtSoFaxHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDienThoaiHQ
            // 
            this.txtSoDienThoaiHQ.Location = new System.Drawing.Point(176, 53);
            this.txtSoDienThoaiHQ.Name = "txtSoDienThoaiHQ";
            this.txtSoDienThoaiHQ.Size = new System.Drawing.Size(223, 21);
            this.txtSoDienThoaiHQ.TabIndex = 34;
            this.txtSoDienThoaiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThoiGianVC
            // 
            this.txtThoiGianVC.Location = new System.Drawing.Point(556, 53);
            this.txtThoiGianVC.Name = "txtThoiGianVC";
            this.txtThoiGianVC.Size = new System.Drawing.Size(373, 21);
            this.txtThoiGianVC.TabIndex = 35;
            this.txtThoiGianVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDaiDienDN
            // 
            this.txtTenDaiDienDN.Location = new System.Drawing.Point(555, 21);
            this.txtTenDaiDienDN.Name = "txtTenDaiDienDN";
            this.txtTenDaiDienDN.Size = new System.Drawing.Size(373, 21);
            this.txtTenDaiDienDN.TabIndex = 33;
            this.txtTenDaiDienDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTuyenDuong
            // 
            this.txtTuyenDuong.Location = new System.Drawing.Point(109, 21);
            this.txtTuyenDuong.Name = "txtTuyenDuong";
            this.txtTuyenDuong.Size = new System.Drawing.Size(290, 21);
            this.txtTuyenDuong.TabIndex = 32;
            this.txtTuyenDuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(689, 89);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(14, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "*";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(934, 57);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(14, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "*";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(934, 25);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(14, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "*";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(398, 25);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(14, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label25.Location = new System.Drawing.Point(22, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(148, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Số điện thoại hải quan nơi đi :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label26.Location = new System.Drawing.Point(409, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tên đại diện doanh nghiệp :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label31.Location = new System.Drawing.Point(409, 89);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(107, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Kilomet vận chuyển :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label27.Location = new System.Drawing.Point(409, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(116, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Thời gian vận chuyển :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label28.Location = new System.Drawing.Point(22, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(119, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Số Fax hải quan nơi đi :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label29.Location = new System.Drawing.Point(22, 25);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Tuyến đường :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label30.Location = new System.Drawing.Point(22, 123);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Phần ghi chú :";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.ctrMaViTriDoHang);
            this.uiGroupBox5.Controls.Add(this.ctrMaViTriXepHang);
            this.uiGroupBox5.Controls.Add(this.ctrMaDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.ctrMaCangCKGaDoHang);
            this.uiGroupBox5.Controls.Add(this.ctrMaCangGaCKXepHang);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.txtDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.txtTenDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.label56);
            this.uiGroupBox5.Controls.Add(this.label42);
            this.uiGroupBox5.Controls.Add(this.label22);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Controls.Add(this.label54);
            this.uiGroupBox5.Controls.Add(this.label53);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Controls.Add(this.label21);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 531);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(977, 213);
            this.uiGroupBox5.TabIndex = 37;
            this.uiGroupBox5.Text = "Địa điểm xếp hàng / dỡ hàng";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrMaViTriDoHang
            // 
            this.ctrMaViTriDoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaViTriDoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaViTriDoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A305;
            this.ctrMaViTriDoHang.Code = "";
            this.ctrMaViTriDoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaViTriDoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaViTriDoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaViTriDoHang.IsOnlyWarning = false;
            this.ctrMaViTriDoHang.IsValidate = true;
            this.ctrMaViTriDoHang.Location = new System.Drawing.Point(328, 181);
            this.ctrMaViTriDoHang.Name = "ctrMaViTriDoHang";
            this.ctrMaViTriDoHang.Name_VN = "";
            this.ctrMaViTriDoHang.SetOnlyWarning = false;
            this.ctrMaViTriDoHang.SetValidate = false;
            this.ctrMaViTriDoHang.ShowColumnCode = true;
            this.ctrMaViTriDoHang.ShowColumnName = false;
            this.ctrMaViTriDoHang.Size = new System.Drawing.Size(120, 21);
            this.ctrMaViTriDoHang.TabIndex = 30;
            this.ctrMaViTriDoHang.TagName = "";
            this.ctrMaViTriDoHang.Where = null;
            this.ctrMaViTriDoHang.WhereCondition = "";
            // 
            // ctrMaViTriXepHang
            // 
            this.ctrMaViTriXepHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaViTriXepHang.Appearance.Options.UseBackColor = true;
            this.ctrMaViTriXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A305;
            this.ctrMaViTriXepHang.Code = "";
            this.ctrMaViTriXepHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaViTriXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaViTriXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaViTriXepHang.IsOnlyWarning = false;
            this.ctrMaViTriXepHang.IsValidate = true;
            this.ctrMaViTriXepHang.Location = new System.Drawing.Point(329, 82);
            this.ctrMaViTriXepHang.Name = "ctrMaViTriXepHang";
            this.ctrMaViTriXepHang.Name_VN = "";
            this.ctrMaViTriXepHang.SetOnlyWarning = false;
            this.ctrMaViTriXepHang.SetValidate = false;
            this.ctrMaViTriXepHang.ShowColumnCode = true;
            this.ctrMaViTriXepHang.ShowColumnName = false;
            this.ctrMaViTriXepHang.Size = new System.Drawing.Size(120, 21);
            this.ctrMaViTriXepHang.TabIndex = 26;
            this.ctrMaViTriXepHang.TagName = "";
            this.ctrMaViTriXepHang.Where = null;
            this.ctrMaViTriXepHang.WhereCondition = "";
            // 
            // ctrMaDiaDiemDoHang
            // 
            this.ctrMaDiaDiemDoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiemDoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiemDoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiemDoHang.Code = "";
            this.ctrMaDiaDiemDoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiemDoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiemDoHang.IsOnlyWarning = false;
            this.ctrMaDiaDiemDoHang.IsValidate = true;
            this.ctrMaDiaDiemDoHang.Location = new System.Drawing.Point(328, 113);
            this.ctrMaDiaDiemDoHang.Name = "ctrMaDiaDiemDoHang";
            this.ctrMaDiaDiemDoHang.Name_VN = "";
            this.ctrMaDiaDiemDoHang.SetOnlyWarning = false;
            this.ctrMaDiaDiemDoHang.SetValidate = false;
            this.ctrMaDiaDiemDoHang.ShowColumnCode = true;
            this.ctrMaDiaDiemDoHang.ShowColumnName = true;
            this.ctrMaDiaDiemDoHang.Size = new System.Drawing.Size(316, 21);
            this.ctrMaDiaDiemDoHang.TabIndex = 28;
            this.ctrMaDiaDiemDoHang.TagCode = "";
            this.ctrMaDiaDiemDoHang.TagName = "";
            this.ctrMaDiaDiemDoHang.Where = null;
            this.ctrMaDiaDiemDoHang.WhereCondition = "";
            // 
            // ctrMaCangCKGaDoHang
            // 
            this.ctrMaCangCKGaDoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaCangCKGaDoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaCangCKGaDoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrMaCangCKGaDoHang.Code = "";
            this.ctrMaCangCKGaDoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaCangCKGaDoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaCangCKGaDoHang.IsOnlyWarning = false;
            this.ctrMaCangCKGaDoHang.IsValidate = true;
            this.ctrMaCangCKGaDoHang.Location = new System.Drawing.Point(728, 181);
            this.ctrMaCangCKGaDoHang.Name = "ctrMaCangCKGaDoHang";
            this.ctrMaCangCKGaDoHang.Name_VN = "";
            this.ctrMaCangCKGaDoHang.SetOnlyWarning = false;
            this.ctrMaCangCKGaDoHang.SetValidate = false;
            this.ctrMaCangCKGaDoHang.ShowColumnCode = true;
            this.ctrMaCangCKGaDoHang.ShowColumnName = true;
            this.ctrMaCangCKGaDoHang.Size = new System.Drawing.Size(220, 21);
            this.ctrMaCangCKGaDoHang.TabIndex = 31;
            this.ctrMaCangCKGaDoHang.TagCode = "";
            this.ctrMaCangCKGaDoHang.TagName = "";
            this.ctrMaCangCKGaDoHang.Where = null;
            this.ctrMaCangCKGaDoHang.WhereCondition = "";
            // 
            // ctrMaCangGaCKXepHang
            // 
            this.ctrMaCangGaCKXepHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaCangGaCKXepHang.Appearance.Options.UseBackColor = true;
            this.ctrMaCangGaCKXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrMaCangGaCKXepHang.Code = "";
            this.ctrMaCangGaCKXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaCangGaCKXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaCangGaCKXepHang.IsOnlyWarning = false;
            this.ctrMaCangGaCKXepHang.IsValidate = true;
            this.ctrMaCangGaCKXepHang.Location = new System.Drawing.Point(728, 82);
            this.ctrMaCangGaCKXepHang.Name = "ctrMaCangGaCKXepHang";
            this.ctrMaCangGaCKXepHang.Name_VN = "";
            this.ctrMaCangGaCKXepHang.SetOnlyWarning = false;
            this.ctrMaCangGaCKXepHang.SetValidate = false;
            this.ctrMaCangGaCKXepHang.ShowColumnCode = true;
            this.ctrMaCangGaCKXepHang.ShowColumnName = true;
            this.ctrMaCangGaCKXepHang.Size = new System.Drawing.Size(220, 21);
            this.ctrMaCangGaCKXepHang.TabIndex = 27;
            this.ctrMaCangGaCKXepHang.TagCode = "";
            this.ctrMaCangGaCKXepHang.TagName = "";
            this.ctrMaCangGaCKXepHang.Where = null;
            this.ctrMaCangGaCKXepHang.WhereCondition = "";
            // 
            // ctrDiaDiemXepHang
            // 
            this.ctrDiaDiemXepHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemXepHang.Appearance.Options.UseBackColor = true;
            this.ctrDiaDiemXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrDiaDiemXepHang.Code = "";
            this.ctrDiaDiemXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemXepHang.IsOnlyWarning = false;
            this.ctrDiaDiemXepHang.IsValidate = true;
            this.ctrDiaDiemXepHang.Location = new System.Drawing.Point(329, 17);
            this.ctrDiaDiemXepHang.Name = "ctrDiaDiemXepHang";
            this.ctrDiaDiemXepHang.Name_VN = "";
            this.ctrDiaDiemXepHang.SetOnlyWarning = false;
            this.ctrDiaDiemXepHang.SetValidate = false;
            this.ctrDiaDiemXepHang.ShowColumnCode = true;
            this.ctrDiaDiemXepHang.ShowColumnName = true;
            this.ctrDiaDiemXepHang.Size = new System.Drawing.Size(315, 21);
            this.ctrDiaDiemXepHang.TabIndex = 24;
            this.ctrDiaDiemXepHang.TagCode = "";
            this.ctrDiaDiemXepHang.TagName = "";
            this.ctrDiaDiemXepHang.Where = null;
            this.ctrDiaDiemXepHang.WhereCondition = "";
            // 
            // txtDiaDiemDoHang
            // 
            this.txtDiaDiemDoHang.Location = new System.Drawing.Point(328, 148);
            this.txtDiaDiemDoHang.Name = "txtDiaDiemDoHang";
            this.txtDiaDiemDoHang.Size = new System.Drawing.Size(316, 21);
            this.txtDiaDiemDoHang.TabIndex = 29;
            this.txtDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(329, 51);
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(315, 21);
            this.txtTenDiaDiemXepHang.TabIndex = 25;
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(22, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(287, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Mã địa điểm dỡ hàng (Khu vực chịu sự giám sát Hải quan) :";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Red;
            this.label56.Location = new System.Drawing.Point(651, 118);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(14, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "*";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(651, 21);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(14, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(556, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(166, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Mã cảng/cửa khẩu/ga xếp hàng :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(469, 185);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(253, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Mã cảng/cửa khẩu/ga dỡ hàng (Mã cảng dỡ hàng) :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(25, 185);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(165, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã vị trí dỡ hàng (Nơi dỡ hàng) : ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label54.Location = new System.Drawing.Point(22, 152);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(291, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Tên địa điểm dỡ hàng (Khu vực chịu sự giám sát Hải quan): ";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label53.Location = new System.Drawing.Point(21, 55);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(300, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Tên địa điểm xếp hàng (Khu vực chịu sự giám sát Hải quan)  :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(21, 86);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(180, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Mã vị trí xếp hàng (Nơi chất hàng)  :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(21, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(293, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Mã địa điểm xếp hàng (Khu vực chịu sự giám sát Hải quan) :";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.dateNgayHetHanHDVC);
            this.uiGroupBox4.Controls.Add(this.dateNgayHopDongVC);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDongVC);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 407);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(977, 124);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Hợp đồng vận chuyển/Giấy tờ tương đương ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dateNgayHetHanHDVC
            // 
            this.dateNgayHetHanHDVC.Location = new System.Drawing.Point(401, 87);
            this.dateNgayHetHanHDVC.Name = "dateNgayHetHanHDVC";
            this.dateNgayHetHanHDVC.ReadOnly = false;
            this.dateNgayHetHanHDVC.Size = new System.Drawing.Size(127, 21);
            this.dateNgayHetHanHDVC.TabIndex = 23;
            this.dateNgayHetHanHDVC.TagName = "";
            this.dateNgayHetHanHDVC.Value = new System.DateTime(2017, 2, 13, 0, 0, 0, 0);
            this.dateNgayHetHanHDVC.WhereCondition = "";
            // 
            // dateNgayHopDongVC
            // 
            this.dateNgayHopDongVC.Location = new System.Drawing.Point(401, 23);
            this.dateNgayHopDongVC.Name = "dateNgayHopDongVC";
            this.dateNgayHopDongVC.ReadOnly = false;
            this.dateNgayHopDongVC.Size = new System.Drawing.Size(127, 21);
            this.dateNgayHopDongVC.TabIndex = 21;
            this.dateNgayHopDongVC.TagName = "";
            this.dateNgayHopDongVC.Value = new System.DateTime(2017, 2, 13, 0, 0, 0, 0);
            this.dateNgayHopDongVC.WhereCondition = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(22, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(377, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ngày hết hạn hợp đồng vận chuyển/Ngày hết hạn của giấy tờ tương đương :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(22, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(271, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số hợp đồng vận chuyển/Số của giấy tờ tương đương :";
            // 
            // txtSoHopDongVC
            // 
            this.txtSoHopDongVC.Location = new System.Drawing.Point(401, 56);
            this.txtSoHopDongVC.Name = "txtSoHopDongVC";
            this.txtSoHopDongVC.Size = new System.Drawing.Size(527, 21);
            this.txtSoHopDongVC.TabIndex = 22;
            this.txtSoHopDongVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(22, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(297, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Ngày hợp đồng vận chuyển/Ngày của giấy tờ tương đương :";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.timeKTVC);
            this.uiGroupBox3.Controls.Add(this.timeBatDauVC);
            this.uiGroupBox3.Controls.Add(this.dateNgayKetThucVC);
            this.uiGroupBox3.Controls.Add(this.dateNgayBatDauVC);
            this.uiGroupBox3.Controls.Add(this.dateNgayCP);
            this.uiGroupBox3.Controls.Add(this.dateNgayTK);
            this.uiGroupBox3.Controls.Add(this.ctrCoQuanHQGS);
            this.uiGroupBox3.Controls.Add(this.ctrCoQuanHQToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoTK);
            this.uiGroupBox3.Controls.Add(this.cbbMaMDVC);
            this.uiGroupBox3.Controls.Add(this.label41);
            this.uiGroupBox3.Controls.Add(this.label40);
            this.uiGroupBox3.Controls.Add(this.label39);
            this.uiGroupBox3.Controls.Add(this.label38);
            this.uiGroupBox3.Controls.Add(this.label37);
            this.uiGroupBox3.Controls.Add(this.label36);
            this.uiGroupBox3.Controls.Add(this.label35);
            this.uiGroupBox3.Controls.Add(this.cbbMaPTVC);
            this.uiGroupBox3.Controls.Add(this.cbbCoBao);
            this.uiGroupBox3.Controls.Add(this.cbbMaLHVC);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label49);
            this.uiGroupBox3.Controls.Add(this.label48);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label32);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 216);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(977, 191);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin tờ khai :";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // timeKTVC
            // 
            this.timeKTVC.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeKTVC.Location = new System.Drawing.Point(737, 151);
            this.timeKTVC.Name = "timeKTVC";
            this.timeKTVC.Size = new System.Drawing.Size(88, 21);
            this.timeKTVC.TabIndex = 20;
            // 
            // timeBatDauVC
            // 
            this.timeBatDauVC.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeBatDauVC.Location = new System.Drawing.Point(738, 114);
            this.timeBatDauVC.Name = "timeBatDauVC";
            this.timeBatDauVC.Size = new System.Drawing.Size(88, 21);
            this.timeBatDauVC.TabIndex = 17;
            // 
            // dateNgayKetThucVC
            // 
            this.dateNgayKetThucVC.Location = new System.Drawing.Point(604, 151);
            this.dateNgayKetThucVC.Name = "dateNgayKetThucVC";
            this.dateNgayKetThucVC.ReadOnly = false;
            this.dateNgayKetThucVC.Size = new System.Drawing.Size(127, 21);
            this.dateNgayKetThucVC.TabIndex = 19;
            this.dateNgayKetThucVC.TagName = "";
            this.dateNgayKetThucVC.Value = new System.DateTime(2017, 2, 13, 0, 0, 0, 0);
            this.dateNgayKetThucVC.WhereCondition = "";
            // 
            // dateNgayBatDauVC
            // 
            this.dateNgayBatDauVC.Location = new System.Drawing.Point(601, 114);
            this.dateNgayBatDauVC.Name = "dateNgayBatDauVC";
            this.dateNgayBatDauVC.ReadOnly = false;
            this.dateNgayBatDauVC.Size = new System.Drawing.Size(127, 21);
            this.dateNgayBatDauVC.TabIndex = 16;
            this.dateNgayBatDauVC.TagName = "";
            this.dateNgayBatDauVC.Value = new System.DateTime(2017, 2, 13, 0, 0, 0, 0);
            this.dateNgayBatDauVC.WhereCondition = "";
            // 
            // dateNgayCP
            // 
            this.dateNgayCP.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dateNgayCP.Appearance.Options.UseBackColor = true;
            this.dateNgayCP.Location = new System.Drawing.Point(799, 20);
            this.dateNgayCP.Name = "dateNgayCP";
            this.dateNgayCP.ReadOnly = false;
            this.dateNgayCP.Size = new System.Drawing.Size(127, 21);
            this.dateNgayCP.TabIndex = 10;
            this.dateNgayCP.TagName = "";
            this.dateNgayCP.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayCP.WhereCondition = "";
            // 
            // dateNgayTK
            // 
            this.dateNgayTK.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dateNgayTK.Appearance.Options.UseBackColor = true;
            this.dateNgayTK.Location = new System.Drawing.Point(562, 20);
            this.dateNgayTK.Name = "dateNgayTK";
            this.dateNgayTK.ReadOnly = false;
            this.dateNgayTK.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTK.TabIndex = 9;
            this.dateNgayTK.TagName = "";
            this.dateNgayTK.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayTK.WhereCondition = "";
            // 
            // ctrCoQuanHQGS
            // 
            this.ctrCoQuanHQGS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHQGS.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHQGS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHQGS.Code = "";
            this.ctrCoQuanHQGS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHQGS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHQGS.IsOnlyWarning = false;
            this.ctrCoQuanHQGS.IsValidate = true;
            this.ctrCoQuanHQGS.Location = new System.Drawing.Point(601, 52);
            this.ctrCoQuanHQGS.Name = "ctrCoQuanHQGS";
            this.ctrCoQuanHQGS.Name_VN = "";
            this.ctrCoQuanHQGS.SetOnlyWarning = false;
            this.ctrCoQuanHQGS.SetValidate = false;
            this.ctrCoQuanHQGS.ShowColumnCode = true;
            this.ctrCoQuanHQGS.ShowColumnName = true;
            this.ctrCoQuanHQGS.Size = new System.Drawing.Size(316, 21);
            this.ctrCoQuanHQGS.TabIndex = 12;
            this.ctrCoQuanHQGS.TagCode = "";
            this.ctrCoQuanHQGS.TagName = "";
            this.ctrCoQuanHQGS.Where = null;
            this.ctrCoQuanHQGS.WhereCondition = "";
            // 
            // ctrCoQuanHQToKhai
            // 
            this.ctrCoQuanHQToKhai.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHQToKhai.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHQToKhai.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHQToKhai.Code = "";
            this.ctrCoQuanHQToKhai.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHQToKhai.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHQToKhai.IsOnlyWarning = false;
            this.ctrCoQuanHQToKhai.IsValidate = true;
            this.ctrCoQuanHQToKhai.Location = new System.Drawing.Point(174, 52);
            this.ctrCoQuanHQToKhai.Name = "ctrCoQuanHQToKhai";
            this.ctrCoQuanHQToKhai.Name_VN = "";
            this.ctrCoQuanHQToKhai.SetOnlyWarning = false;
            this.ctrCoQuanHQToKhai.SetValidate = false;
            this.ctrCoQuanHQToKhai.ShowColumnCode = true;
            this.ctrCoQuanHQToKhai.ShowColumnName = true;
            this.ctrCoQuanHQToKhai.Size = new System.Drawing.Size(316, 21);
            this.ctrCoQuanHQToKhai.TabIndex = 11;
            this.ctrCoQuanHQToKhai.TagCode = "";
            this.ctrCoQuanHQToKhai.TagName = "";
            this.ctrCoQuanHQToKhai.Where = null;
            this.ctrCoQuanHQToKhai.WhereCondition = "";
            // 
            // txtSoTK
            // 
            this.txtSoTK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTK.Location = new System.Drawing.Point(94, 20);
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(199, 21);
            this.txtSoTK.TabIndex = 7;
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTK.ButtonClick += new System.EventHandler(this.btnSelect_Click);
            // 
            // cbbMaMDVC
            // 
            this.cbbMaMDVC.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbMaMDVC.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "ICD: Hàng hóa nhập khẩu vận chuyển từ cửa khẩu nhập đến cảng đích ghi trên vận tả" +
                "i đơn";
            uiComboBoxItem4.Value = "ICD";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "IFS: Hàng hóa nhập khẩu được vận chuyển từ cửa khẩu nhập về địa điểm thu gom hàng" +
                " lẻ";
            uiComboBoxItem5.Value = "IFS";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "ILS: Hàng hóa nhập khẩu vận chuyển từ cửa khẩu nhập về kho hàng không kéo dài";
            uiComboBoxItem6.Value = "ILS";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "ITH: Hàng hóa nhập khẩu khác";
            uiComboBoxItem7.Value = "ITH";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "ECD: Hàng hóa xuất khẩu vận chuyển từ cảng xuất khẩu thành lập trong nội địa đến " +
                "cửa khẩu xuất.";
            uiComboBoxItem8.Value = "ECD";
            this.cbbMaMDVC.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbMaMDVC.Location = new System.Drawing.Point(174, 151);
            this.cbbMaMDVC.Name = "cbbMaMDVC";
            this.cbbMaMDVC.Size = new System.Drawing.Size(200, 21);
            this.cbbMaMDVC.TabIndex = 18;
            this.cbbMaMDVC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(831, 159);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(14, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "*";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(831, 120);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(14, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "*";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(734, 86);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(14, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "*";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(693, 24);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(14, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "*";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(932, 24);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(14, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(379, 86);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(14, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "*";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(307, 24);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(14, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "*";
            // 
            // cbbMaPTVC
            // 
            this.cbbMaPTVC.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbMaPTVC.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Máy bay";
            uiComboBoxItem9.Value = "6";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Tàu thủy";
            uiComboBoxItem10.Value = "11";
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Xà lan";
            uiComboBoxItem11.Value = "16";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Tàu hỏa";
            uiComboBoxItem12.Value = "25";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = " Ô tô";
            uiComboBoxItem13.Value = "31";
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Khác";
            uiComboBoxItem14.Value = "17";
            this.cbbMaPTVC.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbbMaPTVC.Location = new System.Drawing.Point(175, 114);
            this.cbbMaPTVC.Name = "cbbMaPTVC";
            this.cbbMaPTVC.Size = new System.Drawing.Size(200, 21);
            this.cbbMaPTVC.TabIndex = 15;
            this.cbbMaPTVC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbCoBao
            // 
            this.cbbCoBao.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbCoBao.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Nhập khẩu";
            uiComboBoxItem15.Value = "I";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Xuất khẩu";
            uiComboBoxItem16.Value = "E";
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "Loại khác";
            uiComboBoxItem17.Value = "C";
            this.cbbCoBao.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem15,
            uiComboBoxItem16,
            uiComboBoxItem17});
            this.cbbCoBao.Location = new System.Drawing.Point(601, 82);
            this.cbbCoBao.Name = "cbbCoBao";
            this.cbbCoBao.Size = new System.Drawing.Size(127, 21);
            this.cbbCoBao.TabIndex = 14;
            this.cbbCoBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbMaLHVC
            // 
            this.cbbMaLHVC.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbMaLHVC.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "NR: Vận chuyển nói chung";
            uiComboBoxItem18.Value = "NR";
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = " EA: Vận chuyển nói chung (trường hợp vận chuyển có nhiều điểm xếp/dỡ hàng)";
            uiComboBoxItem19.Value = "EA";
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "QU: Vận chuyển hàng phải qua kiểm dịch, hàng XNK có điều kiện";
            uiComboBoxItem20.Value = "QU";
            uiComboBoxItem21.FormatStyle.Alpha = 0;
            uiComboBoxItem21.IsSeparator = false;
            uiComboBoxItem21.Text = "QU: Vận chuyển hàng phải qua kiểm dịch, hàng XNK có điều kiện";
            uiComboBoxItem21.Value = "QU";
            uiComboBoxItem22.FormatStyle.Alpha = 0;
            uiComboBoxItem22.IsSeparator = false;
            uiComboBoxItem22.Text = "CT: Vận chuyển có chuyển đổi phương tiện vận tải";
            uiComboBoxItem22.Value = "CT";
            this.cbbMaLHVC.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem18,
            uiComboBoxItem19,
            uiComboBoxItem20,
            uiComboBoxItem21,
            uiComboBoxItem22});
            this.cbbMaLHVC.Location = new System.Drawing.Point(174, 82);
            this.cbbMaLHVC.Name = "cbbMaLHVC";
            this.cbbMaLHVC.Size = new System.Drawing.Size(200, 21);
            this.cbbMaLHVC.TabIndex = 13;
            this.cbbMaLHVC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(398, 155);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(199, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Ngày, giờ dự kiến kết thúc vận chuyển :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(399, 118);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(197, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Ngày, giờ dự kiến bắt đầu vận chuyển :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(22, 155);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã mục đích vận chuyển :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(23, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Mã phương tiện vận chuyển :";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label49.Location = new System.Drawing.Point(496, 56);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(99, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Hải quan giám sát :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label48.Location = new System.Drawing.Point(23, 56);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(124, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Mã hải quan mở tờ khai :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(22, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Mã LH vận chuyển :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(711, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ngày cấp phép :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(323, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(155, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Click vào nút ... để chọn TK";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(502, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ngày TK :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(399, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cờ báo nhập khẩu/xuất khẩu :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(22, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số TK :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtMaNhaVC);
            this.uiGroupBox2.Controls.Add(this.txtDiaChiNhaVC);
            this.uiGroupBox2.Controls.Add(this.txtTenNhaVC);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label34);
            this.uiGroupBox2.Controls.Add(this.label33);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 89);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(977, 127);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin nhà vận chuyển";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtMaNhaVC
            // 
            this.txtMaNhaVC.Location = new System.Drawing.Point(174, 54);
            this.txtMaNhaVC.Name = "txtMaNhaVC";
            this.txtMaNhaVC.Size = new System.Drawing.Size(138, 21);
            this.txtMaNhaVC.TabIndex = 5;
            this.txtMaNhaVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiNhaVC
            // 
            this.txtDiaChiNhaVC.Location = new System.Drawing.Point(174, 89);
            this.txtDiaChiNhaVC.Name = "txtDiaChiNhaVC";
            this.txtDiaChiNhaVC.Size = new System.Drawing.Size(754, 21);
            this.txtDiaChiNhaVC.TabIndex = 6;
            this.txtDiaChiNhaVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNhaVC
            // 
            this.txtTenNhaVC.Location = new System.Drawing.Point(174, 20);
            this.txtTenNhaVC.Name = "txtTenNhaVC";
            this.txtTenNhaVC.Size = new System.Drawing.Size(754, 21);
            this.txtTenNhaVC.TabIndex = 4;
            this.txtTenNhaVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(22, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(146, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Địa chỉ của nhà vận chuyển :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(22, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã nhà vận chuyển :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(22, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tên nhà vận chuyển :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(934, 25);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(14, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "*";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(318, 58);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(14, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "*";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.Controls.Add(this.dateNgayTN);
            this.uiGroupBox8.Controls.Add(this.label9);
            this.uiGroupBox8.Controls.Add(this.label50);
            this.uiGroupBox8.Controls.Add(this.label6);
            this.uiGroupBox8.Controls.Add(this.label7);
            this.uiGroupBox8.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox8.Controls.Add(this.lblTrangThai);
            this.uiGroupBox8.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(977, 81);
            this.uiGroupBox8.TabIndex = 0;
            this.uiGroupBox8.Text = "Thông tin chung";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Location = new System.Drawing.Point(329, 15);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTN.TabIndex = 2;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(21, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tiếp nhận";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label50.Location = new System.Drawing.Point(17, 54);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(151, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Hải quan tiếp nhận chứng từ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(229, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngày tiếp nhận";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(465, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trạng thái: ";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(174, 46);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(305, 21);
            this.ctrCoQuanHaiQuan.TabIndex = 3;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(535, 20);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(94, 15);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(129, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_TransportEquipmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1206, 877);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_TransportEquipmentForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tờ khai vận chuyển độc lập đủ điều kiện qua KVGS";
            this.Load += new System.EventHandler(this.VNACC_TransportEquipmentForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIComboBox cbbMaMDVC;
        private Janus.Windows.EditControls.UIComboBox cbbMaPTVC;
        private Janus.Windows.EditControls.UIComboBox cbbCoBao;
        private Janus.Windows.EditControls.UIComboBox cbbMaLHVC;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNhaVC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNhaVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNhaVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtTuyenDuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayHetHanHDVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayHopDongVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDongVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayKetThucVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayBatDauVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayCP;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoKMVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDaiDienDN;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDiaDiemDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaCangCKGaDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaCangGaCKXepHang;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHQGS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHQToKhai;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Janus.Windows.GridEX.EditControls.EditBox txtThoiGianVC;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSealHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiContainer;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaViTriDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaViTriXepHang;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemDoHang;
        private System.Windows.Forms.DateTimePicker timeKTVC;
        private System.Windows.Forms.DateTimePicker timeBatDauVC;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnAdd;
    }
}