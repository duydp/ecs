﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS;
using System.Data.OleDb;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.Controls;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMuc_New : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();
        //private string maSP = "";
        //private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        //private FeedBackContent feedbackContent = null;
        private DinhMucMaHang objDinhMucMaHang = new DinhMucMaHang();
        private DinhMucDangKy objDinhMucDangKy = new DinhMucDangKy();
        public long _masterID = 0;
        //public bool isSua = false;
        //-----------------------------------------------------------------------------------------
        public DinhMuc_New()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            if (this._masterID != 0)
            {
                DataTable dtbMasterID = objDinhMucDangKy.SelectDynamic("ID = " + this._masterID, "").Tables[0];
                if (dtbMasterID.Rows.Count > 0)
                {
                    txtSoTiepNhan.Text = dtbMasterID.Rows[0]["SoTiepNhan"].ToString();
                    dtpNgayTao.Value = Convert.ToDateTime(dtbMasterID.Rows[0]["NgayTiepNhan"].ToString());
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private void DinhMucSendForm_Load(object sender, EventArgs e)
        {
            try
            {
                dtpNgayTao.Value = DateTime.Now;
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                grdMain.Tables[0].Columns["DinhMuc"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                grdMain.Tables[0].Columns["DinhMucThucTe"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                grdMain.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
                this.khoitao_DuLieuChuan();

                if (this._masterID != 0)
                {
                    DataTable dtbDinhMuc = objDinhMucMaHang.SelectByMasterID(this._masterID);
                    grdMain.DataSource = dtbDinhMuc;
                    grdMain.Refresh();
                }
                // this.SetCommand();
            }
            catch (Exception ex)
            {
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            dmDangKy = new DinhMucDangKy();

            DinhMucEditForm f = new DinhMucEditForm();
            f.dmDangKy = dmDangKy;
            f.WindowState = FormWindowState.Normal;
            f.ShowDialog(this);
            try
            {
                grdMain.DataSource = this.dmDangKy.DMCollection;

                grdMain.Refetch();
            }
            catch { grdMain.Refresh(); }

        }

        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------


        #region Set maSP Erorr
        private string _maSP = "";
        private decimal _soToKhai = 0;
        private string _maNPL = "";
        private decimal _luongTonKho = 0;
        private decimal _luongSuDung = 0;

        private decimal _SPTieuThu = 0;
        private decimal _DMThucTe = 0;

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //try
            //{
            //    this._maSP = string.IsNullOrEmpty(e.Row.Cells["MaSP"].Value.ToString()) ? "" : e.Row.Cells["MaSP"].Value.ToString();
            //    this._soToKhai = string.IsNullOrEmpty(e.Row.Cells["SoTKN_VNACCS"].Value.ToString()) ? 0 : Convert.ToDecimal(e.Row.Cells["SoTKN_VNACCS"].Value.ToString());
            //    this._maNPL = string.IsNullOrEmpty(e.Row.Cells["MaNPL"].Value.ToString()) ? "" : e.Row.Cells["MaNPL"].Value.ToString();
            //    this._luongSuDung = string.IsNullOrEmpty(e.Row.Cells["SoLuongSuDung"].Value.ToString()) ? 0 : Convert.ToDecimal(e.Row.Cells["SoLuongSuDung"].Value.ToString());

            //    //Check dữ liệu trong database
            //    if (!objDinhMucMaHang.checkData_DinhMucMaHang(_maSP, _soToKhai, _maNPL, _luongSuDung, _masterID))
            //    {
            //        Janus.Windows.GridEX.GridEXFormatStyle rowcol = new GridEXFormatStyle();
            //        rowcol.ForeColor = System.Drawing.Color.Red;
            //        e.Row.RowStyle = rowcol;
            //    }
            //    //Check dữ liệu quy định
            //    else
            //    {
            //        this._SPTieuThu = string.IsNullOrEmpty(e.Row.Cells["SPTieuThu"].Value.ToString()) ? 0 : Convert.ToDecimal(e.Row.Cells["SPTieuThu"].Value.ToString());
            //        this._DMThucTe = string.IsNullOrEmpty(e.Row.Cells["DinhMucThucTe"].Value.ToString()) ? 0 : Convert.ToDecimal(e.Row.Cells["DinhMucThucTe"].Value.ToString());

            //        if (_SPTieuThu * _DMThucTe > _luongSuDung)
            //        {
            //            Janus.Windows.GridEX.GridEXFormatStyle rowcol = new GridEXFormatStyle();
            //            rowcol.ForeColor = System.Drawing.Color.Orange;
            //            e.Row.RowStyle = rowcol;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{ }
        }
        #endregion

        private int getPostion(DinhMuc dm)
        {
            for (int i = 0; i < this.dmDangKy.DMCollection.Count; i++)
            {
                if (this.dmDangKy.DMCollection[i].MaSanPham == dm.MaSanPham && this.dmDangKy.DMCollection[i].MaNguyenPhuLieu == dm.MaNguyenPhuLieu) return i;
            }
            return -1;
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DinhMucEditForm f = new DinhMucEditForm();
                f.DMDetail = (DinhMuc)e.Row.DataRow;
                f.dmDangKy = dmDangKy;
                //---------------
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    f.OpenType = OpenFormType.View;
                else
                    f.OpenType = OpenFormType.Edit;
                f.ShowDialog();
                try
                {
                    grdMain.Refetch();
                }
                catch
                {
                    grdMain.Refresh();
                }

                //---------------

            }

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;
                grdMain.UpdateData();
                if (CheckNPLExist()==false) 
                {
                    return;
                }
                if (CheckSPDaTK() == true)
                {
                    DataTable dtMain = (DataTable)grdMain.DataSource;

                    if (this._masterID == 0 && this.CheckSP() == false)
                    {
                        //Insert

                        //Insert into DinhMucDangKy
                        objDinhMucDangKy.SoTiepNhan = Convert.ToInt32(txtSoTiepNhan.Text);
                        objDinhMucDangKy.NgayTiepNhan = Convert.ToDateTime(dtpNgayTao.Value);
                        objDinhMucDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        objDinhMucDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        objDinhMucDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                        objDinhMucDangKy.TrangThaiXuLy = 1;
                        objDinhMucDangKy.SoTiepNhanChungTu = 0;
                        objDinhMucDangKy.SoDinhMuc = 0;
                        objDinhMucDangKy.NgayDangKy = Convert.ToDateTime(dtpNgayTao.Value);
                        objDinhMucDangKy.NgayApDung = Convert.ToDateTime(dtpNgayTao.Value);
                        objDinhMucDangKy.GUIDSTR = "thanhkhoan2015";
                        objDinhMucDangKy.DeXuatKhac = "";
                        objDinhMucDangKy.LyDoSua = "";
                        objDinhMucDangKy.ActionStatus = 0;
                        objDinhMucDangKy.GuidReference = "";
                        objDinhMucDangKy.NamDK = 0;
                        objDinhMucDangKy.HUONGDAN = "";
                        objDinhMucDangKy.PhanLuong = "";
                        objDinhMucDangKy.Huongdan_PL = "";

                        objDinhMucMaHang.Master_ID = objDinhMucDangKy.Insert();
                    }
                    else 
                    {
                        //Update
                        objDinhMucMaHang.DeleteByMasterID(this._masterID);
                        objDinhMucMaHang.Master_ID = this._masterID;
                    }

                    //Insert vào DinhMucMaHang
                    foreach (DataRow dr in dtMain.Select())
                    {
                        try
                        {
                            //Insert into DinhMucMaHang
                            objDinhMucMaHang.TenKH = dr["TenKH"].ToString();
                            objDinhMucMaHang.PO = dr["PO"].ToString();
                            objDinhMucMaHang.Style = dr["Style"].ToString();
                            objDinhMucMaHang.MaSP = dr["MaSP"].ToString();
                            objDinhMucMaHang.MatHang = dr["MatHang"].ToString();
                            objDinhMucMaHang.SoLuongPO = string.IsNullOrEmpty(dr["SoLuongPO"].ToString()) ? 0 : Convert.ToInt64(dr["SoLuongPO"].ToString());
                            objDinhMucMaHang.SLXuat = string.IsNullOrEmpty(dr["SLXuat"].ToString()) ? 0 : Convert.ToDecimal(dr["SLXuat"].ToString());
                            objDinhMucMaHang.ChenhLech = string.IsNullOrEmpty(dr["ChenhLech"].ToString()) ? 0 : Convert.ToDecimal(dr["ChenhLech"].ToString());
                            objDinhMucMaHang.MaNPL = dr["MaNPL"].ToString();
                            objDinhMucMaHang.TenNPL = dr["TenNPL"].ToString();
                            objDinhMucMaHang.Art = dr["ART"].ToString();
                            objDinhMucMaHang.DVT_NPL = dr["DVT_NPL"].ToString();
                            objDinhMucMaHang.NCC = dr["NCC"].ToString();
                            objDinhMucMaHang.SPTieuThu = string.IsNullOrEmpty(dr["SPTieuThu"].ToString()) ? 0 : Convert.ToDecimal(dr["SPTieuThu"].ToString());
                            objDinhMucMaHang.DinhMuc = string.IsNullOrEmpty(dr["DinhMuc"].ToString()) ? 0 : Convert.ToDecimal(dr["DinhMuc"].ToString());
                            objDinhMucMaHang.TyLeHaoHut = string.IsNullOrEmpty(dr["TyLeHaoHut"].ToString()) ? 0 : Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                            objDinhMucMaHang.DinhMuc_HaoHut = string.IsNullOrEmpty(dr["DinhMuc_HaoHut"].ToString()) ? 0 : Convert.ToDecimal(dr["DinhMuc_HaoHut"].ToString());
                            objDinhMucMaHang.NhuCauXuat = string.IsNullOrEmpty(dr["NhuCauXuat"].ToString()) ? 0 : Convert.ToDecimal(dr["NhuCauXuat"].ToString());
                            objDinhMucMaHang.XuatKho = string.IsNullOrEmpty(dr["XuatKho"].ToString()) ? 0 : Convert.ToDecimal(dr["XuatKho"].ToString());
                            objDinhMucMaHang.ThuHoi = string.IsNullOrEmpty(dr["ThuHoi"].ToString()) ? 0 : Convert.ToDecimal(dr["ThuHoi"].ToString());
                            objDinhMucMaHang.ThucXuatKho = string.IsNullOrEmpty(dr["ThucXuatKho"].ToString()) ? 0 : Convert.ToDecimal(dr["ThucXuatKho"].ToString());
                            objDinhMucMaHang.DinhMucThucTe = string.IsNullOrEmpty(dr["DinhMucThucTe"].ToString()) ? 0 : Convert.ToDecimal(dr["DinhMucThucTe"].ToString());
                            objDinhMucMaHang.SoTKN_VNACCS = string.IsNullOrEmpty(dr["SoTKN_VNACCS"].ToString()) ? 0 : Convert.ToDecimal(dr["SoTKN_VNACCS"].ToString());
                            objDinhMucMaHang.Invoid_HD = dr["Invoid_HD"].ToString();
                            objDinhMucMaHang.NgayChungTu = Convert.ToDateTime(dr["NgayChungTu"].ToString());
                            objDinhMucMaHang.SoLuongNhapKho = string.IsNullOrEmpty(dr["SoLuongNhapKho"].ToString()) ? 0 : Convert.ToDecimal(dr["SoLuongNhapKho"].ToString());
                            objDinhMucMaHang.SoLuongSuDung = string.IsNullOrEmpty(dr["SoLuongSuDung"].ToString()) ? 0 : Convert.ToDecimal(dr["SoLuongSuDung"].ToString());

                            objDinhMucMaHang.Insert();
                        }
                        catch { }


                    }

                    MessageBox.Show("Lưu thành công !");
                    this._masterID = 0;
                }
                else
                {
                    MessageBox.Show("Định mức sản phẩn đã được thanh khoản.(Bạn không thể sửa định mức sản phẩm này)");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "SaoChep":
                    this.SaoChepDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Dinhmuc":
                    this.InDinhMuc();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                default:
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.dmDangKy.SoTiepNhan == 0)
            {
                return;
            }
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "ĐỊNH MỨC";
            phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void InDinhMuc()
        {
            if (dmDangKy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước ghi in.", false);
                return;
            }
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = this.dmDangKy;
            f.ShowDialog(this);
        }

        private void SaoChepDinhMuc()
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();

        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grdMain.SelectedItems;
                string msgWarning = string.Empty;
                if (items.Count == 0) return;

                if (MLMessages("Bạn có muốn xóa định mức này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    if (this._masterID != 0)
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                objDinhMucMaHang.ID = Convert.ToInt64(grdMain.GetRow(i.Position).Cells["ID"].Value.ToString());
                                objDinhMucMaHang.Delete();
                            }
                        }
                    }

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            grdMain.Delete();
                        }
                    }
                    grdMain.Refresh();
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //----------------------------------------------------------------------------------------------

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
               // this._masterID = 0;
                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + txtFilePath.Text + ";Extended Properties=Excel 8.0;";
                string query = string.Format("SELECT TenKH, PO, Style,MaSP, MatHang, SoLuongPO, SLXuat, ChenhLech, MaNPL, TenNPL, ART, DVT_NPL, NCC, SPTieuThu, DinhMuc, TyLeHaoHut, DinhMuc_HaoHut, NhuCauXuat, XuatKho, ThuHoi, ThucXuatKho, DinhMucThucTe, SoTKN_VNACCS, Invoid_HD, NgayChungTu, SoLuongNhapKho, SoLuongSuDung  FROM [{0}$]", txtSheetName.Text);

                OleDbDataAdapter da = new OleDbDataAdapter(query, connectionString);

                DataTable dtLayDL = new DataTable();
                da.Fill(dtLayDL);

                grdMain.DataSource = dtLayDL;
               // grdMain.Refresh();
                MessageBox.Show("Import thành công, vui lòng kiểm tra và nhấn lưu để cập nhật vào CSDL !");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Import không thành công. Lý do có thể là : \n\t+ Cấu trúc file không đúng , xem lại template mẫu.\n\t+ Định dạng file sai: Vui lòng chọn WordSheet 97 - 2003.\n\t+ Sai tên Sheet.");
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        #region CheckDataImport

        private bool _check = true;
        private bool checkDataImport()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this._check = true;

                foreach (GridEXRow items in grdMain.GetRows())
                {
                    this._maSP = string.IsNullOrEmpty(items.Cells["MaSP"].Value.ToString()) ? "" : items.Cells["MaSP"].Value.ToString();
                    this._soToKhai = string.IsNullOrEmpty(items.Cells["SoTKN_VNACCS"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoTKN_VNACCS"].Value.ToString());
                    this._maNPL = string.IsNullOrEmpty(items.Cells["MaNPL"].Value.ToString()) ? "" : items.Cells["MaNPL"].Value.ToString();
                    this._luongSuDung = string.IsNullOrEmpty(items.Cells["SoLuongSuDung"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoLuongSuDung"].Value.ToString());

                    //Check dữ liệu trong database
                    if (!objDinhMucMaHang.checkData_DinhMucMaHang(_maSP, _soToKhai, _maNPL, _luongSuDung, _masterID))
                    {
                        //Nếu Lỗi
                        this._check = false;
                    }
                    //Check dữ liệu quy định
                    else
                    {
                        this._SPTieuThu = string.IsNullOrEmpty(items.Cells["SPTieuThu"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SPTieuThu"].Value.ToString());
                        this._DMThucTe = string.IsNullOrEmpty(items.Cells["DinhMucThucTe"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["DinhMucThucTe"].Value.ToString());

                        if (_SPTieuThu * _DMThucTe > _luongSuDung)
                        {
                            //Nếu Lôi
                            this._check = false;
                        }
                    }
                }

                grdMain.Refresh();
                return this._check;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        private void txtFilePath_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                txtFilePath.Text = openFileDialog1.FileName;
            }
        }

        private void grdMain_FormattingRow(object sender, RowLoadEventArgs e)
        {
        }

        private void label4_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void label4_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            try
            {
                string _pathExcel = Application.StartupPath + "\\ExcelTemplate\\" + "Template-DinhMuc-ThanhKhoan-Moi.xls";
                System.Diagnostics.Process.Start(_pathExcel);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        /// <summary>
        /// kiểm tra lượng tồn tờ khai theo mã NPL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheckLuongTonTK_Click(object sender, EventArgs e)
        {
            string msg = "[Dòng][Mã SP/Mã NPL/Số tờ khai/SUM(Lượng tồn kho)/Lượng sử dụng]. ";
            int index = 0;
            foreach (GridEXRow items in grdMain.GetRows())
            {
                index += 1;
                this._maSP = string.IsNullOrEmpty(items.Cells["MaSP"].Value.ToString()) ? "" : items.Cells["MaSP"].Value.ToString();
                this._soToKhai = string.IsNullOrEmpty(items.Cells["SoTKN_VNACCS"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoTKN_VNACCS"].Value.ToString());
                this._maNPL = string.IsNullOrEmpty(items.Cells["MaNPL"].Value.ToString()) ? "" : items.Cells["MaNPL"].Value.ToString();
                this._luongTonKho = string.IsNullOrEmpty(items.Cells["SoLuongNhapKho"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoLuongNhapKho"].Value.ToString());
                this._luongSuDung = string.IsNullOrEmpty(items.Cells["SoLuongSuDung"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoLuongSuDung"].Value.ToString());
                decimal test = 0;
                if (this._soToKhai != 0)
                {
                    test = checkLuongTon(_soToKhai, _maNPL);
                    if (test == -1)
                    {
                        msg += "["+index+"][" + this._maSP + "/" + this._maNPL + "/" + this._soToKhai + " Không tồn tại]. ";
                    }
                    else 
                    {
                        if (this._luongTonKho < test)
                            msg += "[" + index + "][" + this._maSP + "/" + this._maNPL + "/" + this._soToKhai + "/Tồn DM:" + this._luongTonKho + " (Thực tế:" + test + ")]. ";
                        else if (this._luongSuDung > checkLuongTon(_soToKhai, _maNPL))
                            msg += "[" + index + "][" + this._maSP + "/" + this._maNPL + "/" + this._soToKhai + "/DM sử dụng:" + this._luongSuDung + " (Thực tế:" + test + ")].";
                    }
                    
                    
                }
            }
            KDTMessageBoxControl kdtMsg = new KDTMessageBoxControl();
            if (msg == "[Dòng][Mã SP/Mã NPL/Số tờ khai/SUM(Lượng tồn kho)/Lượng sử dụng]. ")
                MessageBox.Show("Định mức không có lỗi.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                //MessageBox.Show(msg,"Lỗi sai trên định mức",MessageBoxButtons.OK,MessageBoxIcon.Error);
                kdtMsg.ShowMessageTQDT("Lỗi sai trên định mức",msg,false);
        }

        private int checkMaHang(string maSP) 
        {
            string sql = "SELECT COUNT(MaSP) FROM dbo.t_KDT_SXXK_DinhMucMaHang WHERE MaSP = '{0}'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maSP));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                //return dt.Rows[0][0].ToString() + "/" + dt.Rows[0][1].ToString() + "/" + dt.Rows[0][2].ToString() + "/" + dt.Rows[0][3].ToString()+".";
                return Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        private decimal checkLuongTon(decimal sotokhai, string maNPL) 
        {
            string sql = "SELECT SoToKhaiVNACCS,MaNPL,SUM(Luong) as Luong,SUM(Ton) as Ton FROM dbo.v_HangTon WHERE MaNPL = '{0}' AND SoToKhaiVNACCS = {1} GROUP BY SoToKhaiVNACCS,MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql,maNPL,sotokhai));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                //return dt.Rows[0][0].ToString() + "/" + dt.Rows[0][1].ToString() + "/" + dt.Rows[0][2].ToString() + "/" + dt.Rows[0][3].ToString()+".";
                return Convert.ToDecimal(dt.Rows[0][3].ToString());
            }
            catch (Exception)
            {
                return -1;
            }
            
        }

        private bool CheckSP()
        {
            string msg = "";
            foreach (GridEXRow items in grdMain.GetRows())
            {
                
                this._maSP = string.IsNullOrEmpty(items.Cells["MaSP"].Value.ToString()) ? "" : items.Cells["MaSP"].Value.ToString();
                if (string.IsNullOrEmpty(_maSP))
                {
                    if (checkMaHang(_maSP) != 0)
                        msg += "Mã SP:[" + _maSP + "] đã tồn tại.";
                        
                }
            }
            KDTMessageBoxControl kdtMsg = new KDTMessageBoxControl();
            if (msg == "")
            {
                //MessageBox.Show("Mã sản phẩm OK.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else 
            {
                kdtMsg.ShowMessageTQDT("Lỗi sai trên định mức", msg, false);
                return true;
            }
                
        }
        private int checkMaHang_DaTK(string maSP)
        {
            string sql = "SELECT COUNT(MaSP) FROM dbo.t_KDT_SXXK_DinhMucThanhKhoan WHERE MaSP = '{0}'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maSP));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                //return dt.Rows[0][0].ToString() + "/" + dt.Rows[0][1].ToString() + "/" + dt.Rows[0][2].ToString() + "/" + dt.Rows[0][3].ToString()+".";
                return Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool CheckSPDaTK()
        {
            string msg = "";
            foreach (GridEXRow items in grdMain.GetRows())
            {

                this._maSP = string.IsNullOrEmpty(items.Cells["MaSP"].Value.ToString()) ? "" : items.Cells["MaSP"].Value.ToString();
                if (string.IsNullOrEmpty(_maSP))
                {
                    if (checkMaHang(_maSP) != 0)
                    {
                        msg += "Mã SP:[" + _maSP + "] đã được thanh khoản(Bạn không thể sửa định mức với mã hàng này).";
                        break;
                    }
                }
            }
            KDTMessageBoxControl kdtMsg = new KDTMessageBoxControl();
            if (msg == "")
            {
                //MessageBox.Show("Mã sản phẩm OK.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                kdtMsg.ShowMessageTQDT("Lỗi sai trên định mức", msg, false);
                return true;
            }

        }
        private void btnCapNhatNgayTK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                DataTable dtbSetChanges = (DataTable)grdMain.DataSource;
                foreach (DataRow dr in dtbSetChanges.Select())
                {

                    this._maSP = string.IsNullOrEmpty(dr["MaSP"].ToString()) ? "" : dr["MaSP"].ToString();
                    this._soToKhai = string.IsNullOrEmpty(dr["SoTKN_VNACCS"].ToString()) ? 0 : Convert.ToDecimal(dr["SoTKN_VNACCS"].ToString());
                    if (this._soToKhai != 0)
                    {
                        //DateTime dt = checkNgayTK(this._soToKhai);
                        //items.Cells["NgayChungTu"].Text = checkNgayTK(this._soToKhai).ToString();
                        dr["NgayChungTu"] = checkNgayTK(this._soToKhai);

                    }
                }
                grdMain.DataSource = dtbSetChanges;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            //foreach (GridEXRow items in grdMain.GetRows())
            //{

            //    try
            //    {
            //        this._maSP = string.IsNullOrEmpty(items.Cells["MaSP"].Value.ToString()) ? "" : items.Cells["MaSP"].Value.ToString();
            //        this._soToKhai = string.IsNullOrEmpty(items.Cells["SoTKN_VNACCS"].Value.ToString()) ? 0 : Convert.ToDecimal(items.Cells["SoTKN_VNACCS"].Value.ToString());
            //        if (this._soToKhai != 0)
            //        {
            //            //DateTime dt = checkNgayTK(this._soToKhai);
            //            //items.Cells["NgayChungTu"].Text = checkNgayTK(this._soToKhai).ToString();
            //            items.Cells["NgayChungTu"].Value = checkNgayTK(this._soToKhai);
                        
            //        }
            //    }
            //    catch (Exception ex)
            //    {

            //        //throw;
            //    }
                
            //}
            
        }
        private DateTime checkNgayTK(decimal sotokhai)
        {
            string sql = "SELECT SoToKhai,NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai = {0}";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql,sotokhai));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                return Convert.ToDateTime(dt.Rows[0][1].ToString());
            }
            catch (Exception)
            {
                return new DateTime(1900,1,1);
            }

        }
        private string getTenNPL_TenDVT(string maNPL)
        {
            string sql = "SELECT t_SXXK_NguyenPhuLieu.Ten AS TenNPL,t_HaiQuan_DonViTinh.Ten AS TenDVT_NPL FROM dbo.t_SXXK_NguyenPhuLieu JOIN dbo.t_HaiQuan_DonViTinh ON t_HaiQuan_DonViTinh.ID = t_SXXK_NguyenPhuLieu.DVT_ID WHERE Ma = '{0}'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maNPL));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                return dt.Rows[0][0].ToString() + "||" + dt.Rows[0][1].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return "||";
            }

        }
        private void btnCapNhatTenNPL_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                DataTable dtbSetChanges = (DataTable)grdMain.DataSource;
                foreach (DataRow dr in dtbSetChanges.Select())
                {
                    this._maNPL = string.IsNullOrEmpty(dr["MaNPL"].ToString()) ? "" : dr["MaNPL"].ToString();
                    if (!string.IsNullOrEmpty(this._maNPL))
                    {
                        string[] temp = getTenNPL_TenDVT(this._maNPL).Split(new string[] {"||"},StringSplitOptions.None);
                        if (temp.Length > 0) 
                        {
                            dr["TenNPL"] = temp[0];
                            dr["DVT_NPL"] = temp[1];
                        }
                    }
                }
                grdMain.DataSource = dtbSetChanges;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private string CheckNPL(string maNPL)
        {
            string sql = "SELECT count(Ma) from t_SXXK_NguyenPhuLieu WHERE Ma = '{0}'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maNPL));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                return dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }

        }
        /// <summary>
        /// Kiểm tra NPL chưa khai báo
        /// </summary>
        /// <returns></returns>
        private bool CheckNPLExist()
        {

            //string test = "So HD:INV-1502836, Ngay:05/06/2015, So TK:100440814350, So TK:100440814351, So TK:100440814352";
            //string t = "";
            //if (test.Contains("TK:")) 
            //{
            //    string[] tmp = test.Split(new string[] { "TK:" }, StringSplitOptions.None);
            //    if (tmp.Length > 0) 
            //    {

            //        for (int i = 0; i < tmp.Length; i++)
            //        {
            //            if(i!=0)
            //                t += "/" + tmp[i].Substring(0, 12);
            //        }

            //    }

            //}
            //MessageBox.Show(t);

            NguyenPhuLieuSendForm f = new NguyenPhuLieuSendForm();
            try
            {
                DataTable dt = (DataTable)grdMain.DataSource;
                if (dt.Rows.Count == 0)
                    return false;
                DataTable temp = new DataTable();


                foreach (DataRow dr in dt.Rows)
                {
                    string manpl = dr["MaNguyenPhuLieu"].ToString();
                    if (manpl == "MNLDUC0164")
                    {

                    }
                    string str = CheckNPL(manpl);
                    if (str == "0")
                    {
                        temp.ImportRow(dr);
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.Ma = dr["MaNguyenPhuLieu"].ToString();
                        npl.Ten = dr["ChungLoai"].ToString() + " " + dr["Kho"].ToString();
                        npl.MaHS = "";
                        npl.DVT_ID = DonViTinh.GetID(dr["DVT"].ToString().ToUpper());

                        f.nplDangKy.NPLCollection.Add(npl);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }

            if (f.nplDangKy.NPLCollection.Count > 0)
            {
                f.ShowDialog();
                return false;
            }
            else
            {
                MessageBox.Show("Không có NPL nào chưa được khai báo", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;

            }


        }
    }
}
