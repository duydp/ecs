﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        /// <summary>
        /// Mien thue NK
        /// </summary>
        public bool MienThue1 = false;
        /// <summary>
        /// Mien thue GTGT
        /// </summary>
        public bool MienThue2 = false;
        public ToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            double tongTienThueXNK = 0;
            double tongTienThueTatCa = 0;
            double tongTriGiaTT = 0;
            double tongTriGiaTTGTGT = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;
            lblMienThue2.Text = GlobalSettings.DonViBaoCao;
            lblMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            lblMienThue1.Visible = MienThue1;
            lblMienThue2.Visible = MienThue2;
            if (this.TKMD.MaLoaiHinh.Contains("KD") || this.TKMD.MaLoaiHinh.Contains("DT"))
            {
                //xrLabel3.Text = this.TKMD.MaLoaiHinh.Substring(1, 2);
                if (this.TKMD.MaLoaiHinh.Substring(1, 2) == "KD")
                {
                    xrKD.Visible = true;
                }
                else 
                {
                    xrDT.Visible = true;
                }
            }
            if (this.TKMD.MaLoaiHinh.Contains("NTA01"))
            {
                xrNTX.Visible = true;
            }
            DateTime minDate = new DateTime(1900,1,1);
            if (this.TKMD.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: "  + this.TKMD.SoTiepNhan;

            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();            
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if (this.TKMD.SoLuongPLTK > 0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);
            lblTenDoanhNghiep.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper();
            
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            lblSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            lblSoHoaDon.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai > minDate)
                lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            lblSoPTVT.Text = this.TKMD.SoHieuPTVT;
            if (this.TKMD.NgayDenPTVT > minDate)
                lblNgayDenPTVT.Text = this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
            lblSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon > minDate)
                lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocXK_ID);
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);
            lblDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID);
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID);
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            string st = "";
            if(this.TKMD.PhiBaoHiem > 0)
                st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
            if (this.TKMD.PhiVanChuyen > 0)
                st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
            if (this.TKMD.PhiKhac > 0)
                st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
            lblPhiBaoHiem.Text = st;
           
            lblTrongLuong.Text = "TC " + this.TKMD.SoKien.ToString("n0") + " kiện  " + this.TKMD.TrongLuong + " kg";

            if (this.TKMD.HMDCollection.Count <= 3)
            {
                if (this.TKMD.HMDCollection.Count >= 1)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[0];
                    TenHang1.Text = hmd.TenHang;
                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.NuocXX_ID;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT1.Text = "F.O.C";
                        TriGiaNT1.Text = "F.O.C";
                        TriGiaTT1.Text = "";
                        ThueSuatXNK1.Text = "";
                        TienThueXNK1.Text = "";
                        TriGiaTTGTGT1.Text = "";
                        ThueSuatGTGT1.Text = "";
                        TienThueGTGT1.Text = "";
                        TyLeThuKhac1.Text = "";
                        TriGiaThuKhac1.Text = "";


                    }
                    else
                    {
                        DonGiaNT1.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT1.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK1.Text = "";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK ;
                            TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                        }
                        else if(hmd.ThueTTDB >0 && hmd.ThueGTGT >0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            if(hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac1.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac1.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                            
                    }
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                if (this.TKMD.HMDCollection.Count >= 2)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[1];
                    TenHang2.Text = hmd.TenHang;
                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.NuocXX_ID;
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT2.Text = "F.O.C";
                        TriGiaNT2.Text = "F.O.C";
                        TriGiaTT2.Text = "";
                        ThueSuatXNK2.Text = "";
                        TienThueXNK2.Text = "";
                        TriGiaTTGTGT2.Text = "";
                        ThueSuatGTGT2.Text = "";
                        TienThueGTGT2.Text = "";
                        TyLeThuKhac2.Text = "";
                        TriGiaThuKhac2.Text = "";


                    }
                    else
                    {
                        DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK2.Text = "";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK2.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac2.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                    }
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;


                }
                if (this.TKMD.HMDCollection.Count == 3)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[2];
                    TenHang3.Text = hmd.TenHang;
                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.NuocXX_ID;
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);
                    if (hmd.FOC)
                    {
                        DonGiaNT3.Text = "F.O.C";
                        TriGiaNT3.Text = "F.O.C";
                        TriGiaTT3.Text = "";
                        ThueSuatXNK3.Text = "";
                        TienThueXNK3.Text = "";
                        TriGiaTTGTGT3.Text = "";
                        ThueSuatGTGT3.Text = "";
                        TienThueGTGT3.Text = "";
                        TyLeThuKhac3.Text = "";
                        TriGiaThuKhac3.Text = "";


                    }
                    else
                    {
                        DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                        TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                        if (hmd.ThueTuyetDoi)
                        {
                            ThueSuatXNK3.Text = "";
                        }
                        else
                        {
                            if (hmd.ThueSuatXNKGiam == 0)
                                ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                            else
                                ThueSuatXNK3.Text = hmd.ThueSuatXNKGiam.ToString();
                        }
                        TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            if (hmd.ThueSuatTTDBGiam == 0)
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            else
                                ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam.ToString();
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            if (hmd.ThueSuatVATGiam == 0)
                                TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            else
                                TyLeThuKhac3.Text = hmd.ThueSuatVATGiam.ToString();
                            TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString("N0");
                            tongTienThueXNK += hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                    }
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);

                    tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;



                }
            }
            else
            {
                TenHang1.Text = "HÀNG HÓA NHẬP";
                TenHang2.Text = "(CÓ PHỤ LỤC ĐÍNH KÈM)";
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    if (!hmd.FOC)
                    {
                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                        tongTienThueXNK += hmd.ThueXNK;
                        tongTriGiaTT += hmd.TriGiaTT;
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }

                    }
                }
                //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");
                   
                TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");

            }
            // - Linhhtn mark 20100622 - Tổng trị giá NT không được cộng các loại phí
            //tongTriGiaNT += Convert.ToDouble(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");

            lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");

            string s = Company.KD.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            if (MienThue1)
            {
                TriGiaTT1.Text = "";
                ThueSuatXNK1.Text = "";
                TienThueXNK1.Text = "";
                TriGiaTT2.Text = "";
                ThueSuatXNK2.Text = "";
                TienThueXNK2.Text = "";
                TriGiaTT3.Text = "";
                ThueSuatXNK3.Text = "";
                TienThueXNK3.Text = "";
                lblTongThueXNK.Text = "";
            }
            if (MienThue2)
            {
                TriGiaTTGTGT1.Text = "";
                ThueSuatGTGT1.Text = "";
                TienThueGTGT1.Text = "";
                TriGiaTTGTGT2.Text = "";
                ThueSuatGTGT2.Text = "";
                TienThueGTGT2.Text = "";
                TriGiaTTGTGT3.Text = "";
                ThueSuatGTGT3.Text = "";
                TienThueGTGT3.Text = "";
                lblTongTienThueGTGT.Text = "";
            }
            if (MienThue1 && MienThue2)
            {
                lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
            }

            XRControl control = new XRControl();
            int i = 5;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.LoaiCT < 5)
                {
                    control = this.Detail.Controls["lblSoBanChinh" + ct.LoaiCT];
                    control.Text = ct.SoBanChinh + "";
                    control = this.Detail.Controls["lblSoBanSao" + ct.LoaiCT];
                    control.Text = ct.SoBanSao + "";
                }
                else
                {
                    if (i == 7) return;
                    control = this.Detail.Controls["lblTenChungTu" + i];
                    control.Text = ct.TenChungTu;
                    control = this.Detail.Controls["lblSoBanChinh" + i];
                    control.Text = ct.SoBanChinh + "";
                    control = this.Detail.Controls["lblSoBanSao" + i];
                    control.Text = ct.SoBanSao + "";
                    i++;
                }
            }
        }
        public double TinhTongThueHMD()
        {
            double tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if(!hmd.FOC)
                    tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            }
            return tong;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "   ";
            temp += s[s.Length-1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            ptbImage.Visible = t;
        }
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang.ToUpper();
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            lblMienThue2.Visible = t;
            xrTable2.Visible = !t;
            lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKSo.Visible = lblTongThueXNKChu.Visible = !t;

        }
    }
}
