using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiXuatSXXK: DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiXuatSXXK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
             BindReport1();
        }
        public void BindReport1()
        {
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            DateTime minDate = new DateTime(1900,1,1);
            lblMaHaiQuan.Text = this.TKMD.MaHaiQuan;
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if(this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if(this.TKMD.SoLuongPLTK>0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep,"   ");
            lblTenDoanhNghiep.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\n\r" + GlobalSettings.DIA_CHI.ToUpper();
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            lblSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocNK_ID, "      ");
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID).ToUpper();
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID,"      ");
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID, "      ");
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            if(TKMD.SoKien>0)
                lblSoKienTrongLuong.Text = "Tổng cộng :" + TKMD.SoKien.ToString("n0") + " kiện " + this.TKMD.TrongLuong + " kg";
            if (this.TKMD.HMDCollection.Count <= 9)
            {
                for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.TKMD.HMDCollection[i];
                    control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                    control.Text = hmd.TenHang;
                    control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;
                    control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString("G15");
                    control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G10");
                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");
                    tongTriGiaNT += hmd.TriGiaKB;
                }
            }
            else
            {
                ArrayList arr = this.GetNhomHang();
                if (arr.Count == 1 && arr[0].ToString() == "")
                {
                    string nhomHang = arr[0].ToString();
                    XRControl control = new XRControl();
                    HangMauDichCollection col = GetHangCoCungNhom(nhomHang);
                    control = this.xrTable1.Rows[0].Controls["TenHang1"];
                    control.Text = "PHỤ LỤC ĐÍNH KÈM";
                    control = this.xrTable1.Rows[0].Controls["Luong1"];
                    control.Text = GetTongSoLuong(col).ToString("G15");
                    control = this.xrTable1.Rows[0].Controls["TriGiaNT1"];
                    control.Text = GetTongTriGiaNT(col).ToString("N2");

                }
                else
                {
                    for (int i = 0; i < arr.Count; i++)
                    {
                        string nhomHang = arr[i].ToString();
                        XRControl control = new XRControl();
                        HangMauDichCollection col = GetHangCoCungNhom(nhomHang);
                        control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                        control.Text = nhomHang;
                        control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                        control.Text = GetTongSoLuong(col).ToString("G15");
                        control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                        control.Text = GetTongTriGiaNT(col).ToString("N2");
                    }
                }
                tongTriGiaNT = this.GetTongTriGiaNT(this.TKMD.HMDCollection);
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            
        }

        private string ToStringForReport(string s, string patern)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + patern;
            temp += s[s.Length-1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            ptbImage.Visible = t;
        }
        private decimal GetTongTriGiaNT(HangMauDichCollection HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.TriGiaKB;
            return d;
        }
        private decimal GetTongSoLuong(HangMauDichCollection HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.SoLuong;
            return d;
        }
        private ArrayList GetNhomHang()
        {
            ArrayList arr = new ArrayList();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (!CheckExitNhomHang(hmd.NhomHang, arr)) arr.Add(hmd.NhomHang);
            }
            return arr;
        }
        private bool CheckExitNhomHang(string nhomHang, ArrayList arr)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].ToString() == nhomHang) return true;
            }
            return false;
        }
        private HangMauDichCollection GetHangCoCungNhom(string nhomHang)
        {
            HangMauDichCollection col = new HangMauDichCollection();
            foreach(HangMauDich hmd in this.TKMD.HMDCollection)
                if(hmd.NhomHang == nhomHang) col.Add(hmd);
            return col;
        }
    }
}
