﻿//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using System.Threading;

namespace Company.Interface.Report.GC
{
    public partial class ToKhaiGCCTA4Nhap : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";

        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewGCCTTQForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        DateTime minDate = new DateTime(1900, 1, 1);
        public ToKhaiGCCTA4Nhap()
        {
            InitializeComponent();
        }

        //public string GetChiCucHQCK()
        //{
        //    string chiCucHQCK = "";
        //    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
        //    {
        //        chiCucHQCK = this.TKCT. + "-" + CuaKhau.GetName(this.TKCT.CuaKhau_ID);
        //    }
        //    else
        //    {
        //        chiCucHQCK = this.TKCT.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKCT.CuaKhau_ID);
        //    }
        //    return chiCucHQCK;
        //}

        //dinh dang tri gia nguyen te
        string FormatNumber(object obj)
        {
            return Helpers.FormatNumeric(obj, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);

        }
        public void BindReport()
        {

            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;

            string MaLH = TKCT.MaLoaiHinh.Substring(0, 1);


            if (MaLH == "N")
            {
                lblLoaiHinhKhai.Text = "Nhập khẩu";
                //1. Nguoi nhap khau
                try
                {
                    // lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau")));
                    lblNguoiXK.Text = TKCT.TenDonViDoiTac;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }

                //2. Nguoi xuat khau
                try
                {
                    lblNguoiNK.Text = GlobalSettings.TEN_DON_VI + "\r\n" + GlobalSettings.DIA_CHI;
                    lblMaDoanhNghiep.Text = this.TKCT.MaDoanhNghiep;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }

                //19. Chung tu hai quan truoc
                lblChungTu.Text = this.TKCT.SoHDKH;
                lblNgayChungtu.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
                lblNgayHetHanCT.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");
                //8. Hop dong
                if (this.TKCT.SoHopDongDV.Length > 36)
                    lblHopDong.Font = new Font("Times New Roman", 6.5f);

                lblHopDong.Text = "" + this.TKCT.SoHopDongDV;

                if (this.TKCT.NgayHDDV > minDate)
                    lblNgayHopDong.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHopDong.Text = " ";

                if (this.TKCT.NgayHetHanHDDV > minDate)
                    lblNgayHetHanHopDong.Text = "" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHopDong.Text = " ";

            }
            else if (MaLH == "X")
            {

                try
                {
                    // lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau")));
                    lblNguoiNK.Text = TKCT.TenDonViDoiTac;
                    //lblMaDoanhNghiep.Text = this.TKCT.MaKhachHang;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }

                //2. Nguoi xuat khau
                try
                {
                    // lblNguoiXK.Text = (this.TKCT.MaDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                    lblNguoiXK.Text = GlobalSettings.TEN_DON_VI + "\r\n" + GlobalSettings.DIA_CHI;
                    lblMaNguoiXuat.Text = this.TKCT.MaDoanhNghiep;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }
                lblLoaiHinhKhai.Text = "Xuất khẩu";
                lblHD.Text = "Hợp đồng nhận";
                lblNgayHD.Text = "Ngày hợp đồng nhận";
                lblNgayHHHD.Text = "Ngày hết hạn hợp đồng nhận";
                //Khanhhn - 20/06/2012 - Edit bản in tờ khai gia công chuyển tiếp xuất
                //19. Chung tu hai quan truoc
                lblChungTu.Text = this.TKCT.SoHDKH;//SoHDKH
                lblNgayChungtu.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");//NgayHDKH
                lblNgayHetHanCT.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");//NgayHetHanHDKH
                //8. Hop dong
                if (this.TKCT.SoHDKH.Length > 36)
                    lblHopDong.Font = new Font("Times New Roman", 6.5f);

                lblHopDong.Text = "" + this.TKCT.SoHopDongDV;

                if (this.TKCT.NgayHDDV > minDate)
                    lblNgayHopDong.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHopDong.Text = " ";

                if (this.TKCT.NgayHetHanHDDV > minDate)
                    lblNgayHetHanHopDong.Text = "" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHopDong.Text = " ";

            }
            else
                lblLoaiHinhKhai.Text = "";

            lblMienThueNK.Visible = MienThue1;
            lblMienThueGTGT.Visible = MienThue2;

            //3.nguoi uy thac
            lblNguoiUyThac.Text = TKCT.TenDonViUT;
            //lblMienThueNK.Text = GlobalSettings.TieuDeInDinhMuc;
            //lblMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

            
            //if (this.TKMD.SoTiepNhan != 0)
            //    this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;


            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            //  lblChiCucHQCK.Text = GetChiCucHQCK();

            //So tham chieu
            if (this.TKCT.SoTiepNhan > 0)
                lblThamChieu.Text = this.TKCT.SoTiepNhan + "";
            else
                lblThamChieu.Text = "0";

            //Ngay gui
            if (this.TKCT.NgayTiepNhan > minDate)
                lblNgayGui.Text = this.TKCT.NgayTiepNhan.ToString("dd/MM/yyyy");
            else
                lblNgayGui.Text = "";

            //So to khai
            if (this.TKCT.SoToKhai > 0)
            {
                lblToKhai.Text = this.TKCT.SoToKhai + "";

            }
            else
            {
                lblToKhai.Text = "";
                labelTQDT.Image = null;
            }

            //Ngay dang ký
            if (this.TKCT.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKCT.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";


            //5 Loai hinh
            string stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKCT.MaLoaiHinh);
            lblLoaiHinh.Text = TKCT.MaLoaiHinh + "-" + stlh;
            //lblLoaiHinhKhai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));


            ////8. Hop dong
            //if (this.TKCT.SoHopDongDV.Length > 36)
            //    lblHopDong.Font = new Font("Times New Roman", 6.5f);

            //lblHopDong.Text = "" + this.TKCT.SoHopDongDV;

            //if (this.TKCT.NgayHDDV > minDate)
            //    lblNgayHopDong.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
            //else
            //    lblNgayHopDong.Text = " ";

            //if (this.TKCT.NgayHetHanHDDV > minDate)
            //    lblNgayHetHanHopDong.Text = "" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
            //else
            //    lblNgayHetHanHopDong.Text = " ";



            //14. Dieu kien giao hang
            lblDieuKienGiaoHang.Text = this.TKCT.DKGH_ID;

            //15. Phuong thuc thanh toan
            lblPhuongThucThanhToan.Text = this.TKCT.PTTT_ID;

            //16. Dong tin thanh toan
            lblDongTienThanhToan.Text = this.TKCT.NguyenTe_ID;

            //17. Ty gia tinh thue
            lblTyGiaTinhThue.Text = this.TKCT.TyGiaVND.ToString("G10");

            //18. Phan luong & huong dan cua hai quan
            lblHuongDan.Text = TKCT.HUONGDAN;
            //if (TKCT.PhanLuong == "1")
            //    lblLyDoTK.Text = "Tờ khai được thông quan";
            //else if (TKCT.PhanLuong == "2")
            //    lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
            //else if (TKCT.PhanLuong == "3")
            //    lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
            //else
            //    lblLyDoTK.Text = "";



            ////19. Chung tu hai quan truoc
            //lblChungTu.Text = this.TKCT.SoHDKH;
            //lblNgayChungtu.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
            //lblNgayHetHanCT.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");



            //31. Tong trong luong
            if (TKCT.TrongLuong > 0)
                lbltongtrongluong.Text = TKCT.TrongLuong + " kg ";
            else
                lbltongtrongluong.Text = "";
            // Tông số kiện

            lblTongKien.Text = " Tổng số kiện: " + TKCT.SoKien.ToString("N0");

            #region CONTAINER
            //Tong so CONTAINER
            //string tsContainer = "";
            //string cont20 = "", cont40 = "", soKien = "";
            //if (TKCT.SoContainer20 > 0)
            //{
            //    cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

            //    tsContainer += cont20 + "; ";
            //}
            //else
            //    cont20 = "";

            //if (TKMD.SoContainer40 > 0)
            //{
            //    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

            //    tsContainer += cont40 + "; ";
            //}
            //else
            //    cont40 = "";

            //if (TKMD.SoKien > 0)
            //{
            //    soKien = "Tổng số kiện: " + TKMD.SoKien;

            //    tsContainer += soKien;
            //}
            //else
            //    soKien = "";

            //TongSoContainer.Text = tsContainer.Length > 0 ? "Tổng số container: " + tsContainer : "Tổng số container:";
            #endregion

            if (this.TKCT.HCTCollection.Count <= 3)
            {
                #region Dong hang 1
                if (this.TKCT.HCTCollection.Count >= 1)
                {
                    lblSTTHang1.Text = "1";
                    HangChuyenTiep hct = this.TKCT.HCTCollection[0];
                    if (!inMaHang)
                        TenHang1.Text = hct.TenHang;
                    else
                    {
                        if (hct.MaHang.Trim().Length > 0)
                            TenHang1.Text = hct.TenHang + "/" + hct.MaHang;
                        else
                            TenHang1.Text = hct.TenHang;
                    }
                    TenHang1.WordWrap = true;
                    try
                    {
                        TenHang1.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }
                    MaHS1.Text = hct.MaHS;
                    XuatXu1.Text = hct.ID_NuocXX;
                    Luong1.Text = hct.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hct.ID_DVT);
                    DonGiaNT1.Text = hct.DonGia.ToString("G10");
                    TriGiaNT1.Text = FormatNumber(hct.TriGia);
                    tongTienThueXNK += hct.ThueXNK;

                    #region hien thi thue hang 1
                    lblSTTThue1.Text = "1";
                    //thue XNK  
                    if (hct.ThueSuatXNK.Equals(0))
                        TienThueXNK1.Text = "";
                    else
                    {
                        TriGiaTT1.Text = hct.TriGiaTT.ToString("N0");
                        TienThueXNK1.Text = hct.ThueXNK.ToString("N0");
                        ThueSuatXNK1.Text = hct.ThueSuatXNK.ToString("N0");
                    }
                    //Thue thu khac
                    if (hct.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac1.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac1.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hct.PhuThu.ToString("N0");
                    }
                    //Thue GTGT or Thue TTDB
                    if (hct.ThueGTGT > 0 && hct.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hct.TriGiaTT + hct.ThueXNK;

                        TyLeThuKhac1.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hct.PhuThu.ToString("N0");
                        tongThueGTGT += hct.ThueGTGT;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT1.Text = hct.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT1.Text = hct.ThueGTGT.ToString("N0");

                    }
                    else if ((hct.ThueTTDB > 0 && hct.ThueGTGT == 0) || (hct.ThueTTDB > 0 && hct.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hct.TriGiaTT + hct.ThueXNK;

                        tongThueGTGT += hct.ThueTTDB;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT1.Text = hct.ThueTTDB.ToString("N0");
                        ThueSuatGTGT1.Text = hct.ThueSuatTTDB.ToString("N0");
                    }
                    //if (MienThue1)
                    //{
                    //    TriGiaTT1.Text = "";
                    //    ThueSuatXNK1.Text = "";
                    //    TienThueXNK1.Text = "";
                    //}
                    //if (MienThue2)
                    //{

                    //}
                    #endregion
                    tongTriGiaNT += Math.Round(hct.TriGiaKB_VND, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hct.ThueXNK + hct.PhuThu + hct.ThueTTDB + hct.ThueGTGT;
                }

                #endregion
                #region Dong hang 2
                if (this.TKCT.HCTCollection.Count >= 2)
                {
                    lblSTTHang2.Text = "2";
                    HangChuyenTiep hct = this.TKCT.HCTCollection[1];
                    if (!inMaHang)
                        TenHang2.Text = hct.TenHang;
                    else
                    {
                        if (hct.MaHang.Trim().Length > 0)
                            TenHang2.Text = hct.TenHang + "/" + hct.MaHang;
                        else
                            TenHang2.Text = hct.TenHang;
                    }
                    TenHang2.WordWrap = true;
                    try
                    {
                        TenHang2.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }
                    MaHS2.Text = hct.MaHS;
                    XuatXu2.Text = hct.ID_NuocXX;
                    Luong2.Text = hct.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hct.ID_DVT);
                    DonGiaNT2.Text = hct.DonGia.ToString("G10");
                    TriGiaNT2.Text = FormatNumber(hct.TriGia);

                    tongTienThueXNK += hct.ThueXNK;
                    #region hien thi thue hang 2
                    lblSTTThue2.Text = "2";
                    //thue XNK
                    if (hct.ThueSuatXNK.Equals(0))
                        TienThueXNK2.Text = "";
                    else
                    {
                        TriGiaTT2.Text = hct.TriGiaTT.ToString("N0");
                        TienThueXNK2.Text = hct.ThueXNK.ToString("N0");
                        ThueSuatXNK2.Text = hct.ThueSuatXNK.ToString("N0");
                    }
                    //Thue Thu khac
                    if (hct.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac2.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac2.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hct.PhuThu.ToString("N0");
                    }
                    //Thue GTGT or Thue TTDB
                    if (hct.ThueGTGT > 0 && hct.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hct.TriGiaTT + hct.ThueXNK;

                        TyLeThuKhac2.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hct.PhuThu.ToString("N0");
                        tongThueGTGT += hct.ThueGTGT;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT2.Text = hct.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT2.Text = hct.ThueGTGT.ToString("N0");

                    }
                    else if ((hct.ThueTTDB > 0 && hct.ThueGTGT == 0) || (hct.ThueTTDB > 0 && hct.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hct.TriGiaTT + hct.ThueXNK;

                        tongThueGTGT += hct.ThueTTDB;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT2.Text = hct.ThueTTDB.ToString("N0");
                        ThueSuatGTGT2.Text = hct.ThueSuatTTDB.ToString("N0");
                    }
                    #endregion
                    tongTriGiaNT += Math.Round(hct.TriGiaKB_VND, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hct.ThueXNK + hct.PhuThu + hct.ThueTTDB + hct.ThueGTGT;
                }
                #endregion
                #region Dong hang 3
                if (this.TKCT.HCTCollection.Count == 3)
                {

                    lblSTTHang3.Text = "3";
                    HangChuyenTiep hct = this.TKCT.HCTCollection[2];
                    if (!inMaHang)
                        TenHang3.Text = hct.TenHang;
                    else
                    {
                        if (hct.MaHang.Trim().Length > 0)
                            TenHang3.Text = hct.TenHang + "/" + hct.MaHang;
                        else
                            TenHang3.Text = hct.TenHang;
                    }


                    TenHang3.WordWrap = true;
                    try
                    {
                        TenHang3.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }

                    MaHS3.Text = hct.MaHS;
                    XuatXu3.Text = hct.ID_NuocXX;
                    Luong3.Text = hct.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hct.ID_DVT);
                    DonGiaNT3.Text = hct.DonGia.ToString("G10");
                    TriGiaNT3.Text = FormatNumber(hct.TriGia);

                    tongTienThueXNK += hct.ThueXNK;
                    #region hien thi thue hang 3
                    //Thue XNK
                    lblSTTThue3.Text = "3";
                    if (hct.ThueSuatXNK.Equals(0))
                        TienThueXNK3.Text = "";
                    else
                    {
                        TriGiaTT3.Text = hct.TriGiaTT.ToString("N0");
                        TienThueXNK3.Text = hct.ThueXNK.ToString("N0");
                        ThueSuatXNK3.Text = hct.ThueSuatXNK.ToString("N0");
                    }
                    //Thue thu khac
                    if (hct.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac3.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac3.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hct.PhuThu.ToString("N0");
                    }
                    //thue GTGT or Thue TTDB
                    if (hct.ThueGTGT > 0 && hct.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hct.TriGiaTT + hct.ThueXNK;

                        TyLeThuKhac3.Text = hct.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hct.PhuThu.ToString("N0");
                        tongThueGTGT += hct.ThueGTGT;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT3.Text = hct.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT3.Text = hct.ThueGTGT.ToString("N0");

                    }
                    else if ((hct.ThueTTDB > 0 && hct.ThueGTGT == 0) || (hct.ThueTTDB > 0 && hct.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hct.TriGiaTT + hct.ThueXNK;
                        tongThueGTGT += hct.ThueTTDB;
                        tongTriGiaThuKhac += hct.PhuThu;
                        TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT3.Text = hct.ThueTTDB.ToString("N0");
                        ThueSuatGTGT3.Text = hct.ThueSuatTTDB.ToString("N0");
                    }
                    #endregion

                    tongTriGiaNT += Math.Round(hct.TriGiaKB_VND, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa += hct.ThueXNK + hct.PhuThu + hct.ThueTTDB + hct.ThueGTGT;
                }
                #endregion
            }
            else
            {
                #region PHU LUC

                TenHang1.Text = "HÀNG HÓA NHẬP";
                TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB_VND, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGiaTT;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.PhuThu;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.PhuThu;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                }


                #endregion
            }



            //30. Tong so tien thue & Thu khac
            lblTongThueXNKSo.Text = tongTienThueTatCa.ToString("N0");

            string s = Company.GC.BLL.Utils.VNCurrency.ToString((decimal)tongTienThueTatCa).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);

            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            //if (MienThue1)
            //{
            //    TriGiaTT1.Text = "";
            //    ThueSuatXNK1.Text = "";
            //    TienThueXNK1.Text = "";
            //    TriGiaTT2.Text = "";
            //    ThueSuatXNK2.Text = "";
            //    TienThueXNK2.Text = "";
            //    TriGiaTT3.Text = "";
            //    ThueSuatXNK3.Text = "";
            //    TienThueXNK3.Text = "";
            //    lblTongThueXNK.Text = "";
            //}
            //if (MienThue2)
            //{
            //    TriGiaTTGTGT1.Text = "";
            //    ThueSuatGTGT1.Text = "";
            //    TienThueGTGT1.Text = "";
            //    TriGiaTTGTGT2.Text = "";
            //    ThueSuatGTGT2.Text = "";
            //    TienThueGTGT2.Text = "";
            //    TriGiaTTGTGT3.Text = "";
            //    ThueSuatGTGT3.Text = "";
            //    TienThueGTGT3.Text = "";
            //    lblTongTienThueGTGT.Text = "";
            //}

            //if (MienThue1 && MienThue2)
            //{
            //    lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
            //}

            //Neu co phu luc dinh kem -> khong hien thi gia tri thue
            if (this.TKCT.HCTCollection.Count > 3)
            {
                lblTongThueXNK.Text = lblTongTienThueGTGT.Text = lblTongTriGiaThuKhac.Text = "";
            }

            //Ngay thang nam in to khai
            if (TKCT.NgayDangKy == new DateTime(1900, 1, 1))
                lblNgayIn.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
            else
                lblNgayIn.Text = "Ngày " + TKCT.NgayTiepNhan.Day + " tháng " + TKCT.NgayTiepNhan.Month + " năm " + TKCT.NgayTiepNhan.Year;

            //32. Ghi chep khac
            lblGhChepKhac.Text = "32. Ghi chép khác: " + TKCT.DeXuatKhac;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        //public decimal TinhTongThueHMD()
        //{
        //    decimal tong = 0;
        //    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
        //    {
        //        tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
        //    }
        //    return tong;
        //}
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }
        //private bool IsHaveTax()
        //{
        //    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
        //        if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
        //    return false;
        //}
        public void ShowMienThue(bool t)
        {
            //lblMienThueGTGT.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        public void setThongTin(XRControl cell, string thongTin)
        {
            if (cell.Name.Equals("lblGhChepKhac"))
                cell.Text = "32. Ghi chép khác: " + thongTin;
            else
                cell.Text = thongTin;
        }

        public void setDeXuatKhac(string deXuatKhac)
        {
            lblGhChepKhac.Text = "32. Ghi chép khác: " + deXuatKhac;
        }

        private void TriGiaTT1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblGhChepKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            // report.txtDeXuatKhac.Text = cell.Text;
            //report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lbltongtrongluong_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblNgayIn_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
