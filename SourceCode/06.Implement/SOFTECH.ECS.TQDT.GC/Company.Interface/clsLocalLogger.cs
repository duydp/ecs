using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Reflection;
using System.Resources;

namespace Company.Interface
{
    /// <summary>
    /// Summary description for clsLocalLogger.
    /// </summary>
    public class clsLocalLogger
    {
        StreamWriter sw = null;
        private static clsLocalLogger instance;
        private string fileName;

        // Create a TraceSwitch.
        private static TraceSwitch generalSwitch = new TraceSwitch("General", "Entire Application");

        public clsLocalLogger()
        {
            //
            // TODO: Add constructor logic here
            //
            string baseDir = AppDomain.CurrentDomain.BaseDirectory + "\\";
            fileName = baseDir + "Debug.log";
        }
        public static clsLocalLogger getLogger()
        {
            if (instance == null)
                instance = new clsLocalLogger();
            return instance;
        }
        /*
        public void put(string msg)
        {
            System.Text.StringBuilder sNewMessage = new System.Text.StringBuilder();

            sNewMessage.Append("Exception Summary \n")
                    .Append("-------- ")
              .Append(DateTime.Now.ToShortDateString())
                    .Append(":")
              .Append(DateTime.Now.ToShortTimeString())
                    .AppendLine(" -------- ")
                    .AppendLine(msg)
                    .AppendLine("\n");

            sw = new StreamWriter(fileName, true, System.Text.Encoding.Unicode);
            lock (sw)
            {
                sw.WriteLine(sNewMessage.ToString());
                sw.Close();
            }
        }
        */

        public void stop()
        {
            //sw.Close();
        }
        ~clsLocalLogger()
        {
            if (sw != null) sw.Close();
            sw = null;
        }

        public void message(string title, System.Exception exception)
        {
            message(title, exception.GetHashCode(), exception.Message + "\n\n" + exception.StackTrace, exception.InnerException, true);
        }

        public void message(System.Exception exception)
        {
            message("", exception.GetHashCode(), exception.Message + "\n\n" + exception.StackTrace, exception.InnerException, true);
        }

        private void message(string title, int nCode, string sMessage, System.Exception oInnerException, bool bLog)
        {
            //if (oSource != null)
            //    base.Source = oSource.ToString();

            // need to add logic to check what log destination we
            // should logging to e.g. file, eventlog, database, remote
            // debugger
            if (bLog)
            {
                // log it
                sw = new StreamWriter(fileName, true, System.Text.Encoding.Unicode);
                lock (sw)
                {
                    sw.WriteLine(Format(title, nCode, sMessage,
                                        oInnerException));
                    sw.Close();
                }
            }
        }

        public string Format(string title, int nCode, string
                          sMessage, System.Exception oInnerException)
        {
            StringBuilder sNewMessage = new StringBuilder();
            string sErrorStack = null;

            // get the error stack, if InnerException is null,
            // sErrorStack will be "exception was not chained" and
            // should never be null
            sErrorStack = BuildErrorStack(oInnerException);

            // we want immediate gradification
            Trace.AutoFlush = true;

            sNewMessage.AppendLine("===============================\n")
                       .Append("Title: ")
                       .Append(title)
                       .AppendLine()
                       .Append("No.: \n")
                       .Append(nCode)
                       .Append(" - ")
                 .Append(DateTime.Now.ToShortDateString())
                       .Append(":")
                 .AppendLine(DateTime.Now.ToShortTimeString())
                       .AppendLine("===============================\n")
                       .AppendLine(sMessage)
                       .Append("\n")
                       .Append(sErrorStack)
                       .AppendLine("\n");

            return sNewMessage.ToString();
        }

        /// <summary>
        /// Takes a first nested exception object and builds a error
        /// stack from its chained contents
        /// </summary>
        /// <param name="oChainedException"></param>
        /// <returns></returns>
        private string BuildErrorStack(System.Exception
     oChainedException)
        {
            string sErrorStack = null;
            StringBuilder sbErrorStack = new StringBuilder();
            int nErrStackNum = 1;
            System.Exception oInnerException = null;

            if (oChainedException != null)
            {
                sbErrorStack.AppendLine("\n")
                            .AppendLine("Error Stack: \n")
                            .AppendLine("-----\n");

                oInnerException = oChainedException;
                while (oInnerException != null)
                {
                    sbErrorStack.Append(nErrStackNum)
                                .Append(") ")
                     .Append(oInnerException.Message)
                                .AppendLine("\n");

                    oInnerException =
                          oInnerException.InnerException;

                    nErrStackNum++;
                }

                sbErrorStack.Append("Call Stack:\n")
                  .Append(oChainedException.StackTrace);

                sErrorStack = sbErrorStack.ToString();
            }
            else
            {
                sErrorStack = "exception was not chained";
            }

            return sErrorStack;
        }

    }




}
