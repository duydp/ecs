﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;

using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.Interface.GC;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.ToKhai
{
    public partial class SelectNhieuHangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------       
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public ToKhaiMauDich TKMD;
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public decimal TyGiaTT;
        //-----------------------------------------------------------------------------------------

        public SelectNhieuHangMauDichForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            if (this.TKMD.MaLoaiHinh.StartsWith("NGC"))
            {
                dgList.RootTable.Columns["Ma_HTS"].Visible = false;
                dgList.RootTable.Columns["DVT_HTS"].Visible = false;
                dgList.RootTable.Columns["SoLuong_HTS"].Visible = false;
            }
            else
            {
                if (this.TKMD.LoaiHangHoa != "S" || TKMD.MaMid == "")
                {
                    dgList.RootTable.Columns["Ma_HTS"].Visible = false;
                    dgList.RootTable.Columns["DVT_HTS"].Visible = false;
                    dgList.RootTable.Columns["SoLuong_HTS"].Visible = false;
                }
            }
            foreach (DataRow rowNuoc in this._Nuoc.Rows)
                dgList.RootTable.Columns["NuocXX_ID"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());

        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            List<HangMauDich> hangMauDichs = new List<HangMauDich>();
            if (TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKMD.IDHopDong;

                foreach (NguyenPhuLieu item in npl.SelectCollectionBy_HopDong_ID())
                {
                    HangMauDich hmd = new HangMauDich()
                    {
                        MaPhu = item.Ma,
                        TenHang = item.Ten,
                        DVT_ID = item.DVT_ID,
                        MaHS = item.MaHS

                    };
                    hangMauDichs.Add(hmd);
                }
            }
            else if (TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.IDHopDong;
                foreach (SanPham item in sp.SelectCollectionBy_HopDong_ID())
                {
                    HangMauDich hmd = new HangMauDich()
                    {
                        MaPhu = item.Ma,
                        TenHang = item.Ten,
                        MaHS = item.MaHS,
                        DVT_ID = item.DVT_ID
                    };
                    hangMauDichs.Add(hmd);

                }
            }
            else if (TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKMD.IDHopDong;
                foreach (ThietBi item in tb.SelectCollectionBy_HopDong_ID())
                {
                    HangMauDich hmd = new HangMauDich()
                    {
                        MaPhu = item.Ma,
                        TenHang = item.Ten,
                        MaHS = item.MaHS,
                        DVT_ID = item.DVT_ID
                    };
                    hangMauDichs.Add(hmd);

                }
            }
            else if (TKMD.LoaiHangHoa == "H")
            {
                foreach (HangMau item in HangMau.SelectCollectionDynamic("HopDong_ID=" + TKMD.IDHopDong, ""))
                {
                    HangMauDich hmd = new HangMauDich()
                    {
                        MaPhu = item.Ma,
                        TenHang = item.Ten,
                        MaHS = item.MaHS,
                        DVT_ID = item.DVT_ID

                    };
                    hangMauDichs.Add(hmd);
                }
            }
            dgList.DataSource = hangMauDichs;
        }


        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper()) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            List<HangMauDich> hangs = new List<HangMauDich>();
            foreach (GridEXRow item in items)
            {
                HangMauDich hmd = (HangMauDich)item.DataRow;
                string sfmt = string.Format("Mã hàng '{0}'\r\nTên hàng '{1}\r\n", hmd.MaPhu, hmd.TenHang);
                if (string.IsNullOrEmpty(hmd.NuocXX_ID))
                {
                    showMsg(sfmt + "Nước xuất xứ chưa chọn");
                    return;
                }
                if (hmd.SoLuong <= 0)
                {
                    showMsg(sfmt + "Số lượng không hợp lệ");
                    return;
                }
                if (hmd.DonGiaKB <= 0)
                {
                    showMsg(sfmt + "Đơn giá nguyên tệ không hợp lệ");
                    return;
                }
                hangs.Add(hmd);
            }
            if (this.TKMD.HMDCollection.Count > 0)
            {
                if (ShowMessage("Bạn có muốn xóa hàng đã có trước đây không?", true) == "Yes")
                    this.TKMD.HMDCollection.Clear();
            }
            foreach (HangMauDich HMD in hangs)
            {
                this.TKMD.HMDCollection.Add(HMD.Copy());
            }
            TKMD.SoLuongPLTK = Convert.ToInt16(Helpers.GetLoading(TKMD.HMDCollection.Count));
            this.Close();
        }
        private void reset()
        {
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

        }
        private void SetGridSettings()
        {


            this.dgList.RowFormatStyle.BackColor = this.dgList.HeaderFormatStyle.BackColor;
            foreach (GridEXColumn col in this.dgList.RootTable.Columns)
            {
                col.CellStyle.BackColor = SystemColors.Window;
            }

        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            if (this.TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                btnAddNew.Visible = false;
        }

        //-----------------------------------------------------------------------------------------------       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //GridEXSelectedItemCollection items = dgList.SelectedItems;
            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        HangMauDichEditForm f = new HangMauDichEditForm();
            //        f.HMD = (HangMauDich)e.Row.DataRow;
            //        f.HD.ID = this.TKMD.IDHopDong;                                                                                                    
            //        f.TKMD = this.TKMD;
            //        f.TyGiaTT = this.TyGiaTT;
            //        if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            //            f.OpenType = OpenFormType.Edit;
            //        else
            //            f.OpenType = OpenFormType.View;
            //        f.ShowDialog();                                        
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch { dgList.Refresh(); }
            //        decimal tongTGKB = 0;
            //        foreach (HangMauDich hmd in this.HMDCollection)
            //        {
            //            // Tính lại thuế.
            //            //hmd.TinhThue(this.TyGiaTT);
            //            // Tổng trị giá khai báo.
            //            tongTGKB += hmd.TriGiaKB;
            //        }
            //        lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", tongTGKB.ToString("N"), this.TKMD.NguyenTe_ID);
            //    }
            //    break;
            //}
        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (showMsg("MSG_DEL01", true) == "Yes")
            ////if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
            //            if (hmd.ID > 0)
            //            {
            //                hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
            //            }
            //        }
            //    }               
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            if (TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieuRegistedGCForm f = new NguyenPhuLieuRegistedGCForm();
                f.TKMD = TKMD;
                f.HMDCollection = HMDCollection;
                f.ShowDialog();
            }
            else if (TKMD.LoaiHangHoa == "S")
            {
                SanPhamRegistedGCForm f = new SanPhamRegistedGCForm();
                f.TKMD = TKMD;
                f.HMDCollection = HMDCollection;
                f.ShowDialog();
            }
            else
            {
                ThietBiRegistedForm f = new ThietBiRegistedForm();
                f.TKMD = TKMD;
                f.HMDCollection = HMDCollection;
                f.ShowDialog();
            }
            dgList.DataSource = HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private bool KiemTraLuongHang(string MaHang, decimal SoLuong)
        {
            decimal LuongCon = 0;
            if (this.TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = MaHang;
                if (npl.Load())
                {
                    if (this.TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        //LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                        //if (LuongCon < SoLuong)
                        //{
                        //    if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                        //        //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?","MSG_WRN32",LuongCon.ToString(), true) != "Yes")
                        //        return false;
                        //}
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap + npl.SoLuongCungUng - npl.SoLuongDaDung;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }

                }
                else
                {
                }
            }
            else if (this.TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = MaHang;
                if (sp.Load())
                {
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    if (LuongCon < SoLuong)
                    {
                        if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                            return false;
                    }
                }
                else
                {
                    ;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = MaHang;
                if (tb.Load())
                {
                    if (this.TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            return true;
        }
        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DonGiaKB")
            {
                decimal dongia = Convert.ToDecimal(e.Value);
                if (dongia <= 0)
                {
                    showMsg("MSG_WRN33");
                    e.Cancel = true;
                }
                GridEXRow row = dgList.GetRow();
                HangMauDich HMD = (HangMauDich)row.DataRow;
                HMD.DonGiaKB = dongia;
                HMD.TriGiaKB = HMD.DonGiaKB * HMD.SoLuong;
                HMD.TriGiaKB_VND = HMD.TriGiaKB * TKMD.TyGiaTinhThue;
                row.Cells["SoLuong"].Text = HMD.SoLuong.ToString();
                row.Cells["TriGiaKB"].Text = HMD.TriGiaKB.ToString();

            }
            else if (e.Column.Key == "SoLuong")
            {
                decimal SoLuong = Convert.ToDecimal(e.Value);
                if (SoLuong <= 0)
                {
                    showMsg("MSG_WRN33");
                    //MLMessages("Dữ liệu không hợp lệ","MSG_WRN33","", false);
                    e.Cancel = true;
                }
                GridEXRow row = dgList.GetRow();

                HangMauDich HMD = (HangMauDich)row.DataRow;
                if (!KiemTraLuongHang(HMD.MaPhu.Trim(), SoLuong))
                    e.Cancel = true;
                HMD.SoLuong = SoLuong;
                HMD.TriGiaKB = HMD.DonGiaKB * HMD.SoLuong;
                HMD.TriGiaKB_VND = HMD.TriGiaKB * TKMD.TyGiaTinhThue;
                row.Cells["TriGiaKB"].Text = HMD.TriGiaKB.ToString();
                row.Cells["SoLuong"].Text = HMD.SoLuong.ToString();

            }
            else if (e.Column.Key == "TriGiaKB")
            {
                decimal trigia = Convert.ToDecimal(e.Value);
                if (trigia <= 0)
                {
                    showMsg("MSG_WRN33");
                    e.Cancel = true;
                }
                GridEXRow row = dgList.GetRow();

                HangMauDich HMD = (HangMauDich)row.DataRow;
                if (HMD.DonGiaKB == 0)

                    HMD.TriGiaKB = trigia;
                HMD.TriGiaKB = HMD.DonGiaKB * HMD.SoLuong;
                HMD.TriGiaKB_VND = HMD.TriGiaKB * TKMD.TyGiaTinhThue;
                row.Cells["TriGiaKB"].Text = HMD.TriGiaKB.ToString();
                row.Cells["SoLuong"].Text = HMD.SoLuong.ToString();
            }
            else if (e.Column.Key == "NuocXX_ID")
            {

                GridEXRow[] items = dgList.GetCheckedRows();

                foreach (GridEXRow item in items)
                {
                    HangMauDich hmd = (HangMauDich)item.DataRow;
                    hmd.NuocXX_ID = e.Value.ToString();
                    item.Cells["NuocXX_ID"].Text = Nuoc_GetName(e.Value);
                }

            }
        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            HangMauDich HMD = (HangMauDich)dgList.GetRow().DataRow;
            HMD.TriGiaKB = HMD.DonGiaKB * HMD.SoLuong;
            HMD.TriGiaKB_VND = HMD.TriGiaKB * TKMD.TyGiaTinhThue;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {

        }




    }
}
