﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS.Vouchers
{
	public partial class KDT_VNACCS_TotalInventoryReport_ContractReference : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TotalInventory_ID { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayHopDong { set; get; }
		public string MaHQ { set; get; }
		public DateTime NgayHetHan { set; get; }

        public List<KDT_VNACCS_TotalInventoryReport_Detail> DetailCollection = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TotalInventoryReport_ContractReference> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection = new List<KDT_VNACCS_TotalInventoryReport_ContractReference>();
			while (reader.Read())
			{
				KDT_VNACCS_TotalInventoryReport_ContractReference entity = new KDT_VNACCS_TotalInventoryReport_ContractReference();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TotalInventory_ID"))) entity.TotalInventory_ID = reader.GetInt64(reader.GetOrdinal("TotalInventory_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection, long id)
        {
            foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TotalInventoryReport_ContractReference VALUES(@TotalInventory_ID, @SoHopDong, @NgayHopDong, @MaHQ, @NgayHetHan)";
            string update = "UPDATE t_KDT_VNACCS_TotalInventoryReport_ContractReference SET TotalInventory_ID = @TotalInventory_ID, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQ = @MaHQ, NgayHetHan = @NgayHetHan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TotalInventoryReport_ContractReference WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TotalInventoryReport_ContractReference VALUES(@TotalInventory_ID, @SoHopDong, @NgayHopDong, @MaHQ, @NgayHetHan)";
            string update = "UPDATE t_KDT_VNACCS_TotalInventoryReport_ContractReference SET TotalInventory_ID = @TotalInventory_ID, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQ = @MaHQ, NgayHetHan = @NgayHetHan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TotalInventoryReport_ContractReference WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TotalInventoryReport_ContractReference Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TotalInventoryReport_ContractReference> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TotalInventoryReport_ContractReference> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_TotalInventoryReport_ContractReference> SelectCollectionBy_TotalInventory_ID(long totalInventory_ID)
		{
            IDataReader reader = SelectReaderBy_TotalInventory_ID(totalInventory_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "p_KDT_VNACCS_TotalInventoryReport_ContractReference_SelectBy_TotalInventory_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TotalInventoryReport_ContractReference(long totalInventory_ID, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan)
		{
			KDT_VNACCS_TotalInventoryReport_ContractReference entity = new KDT_VNACCS_TotalInventoryReport_ContractReference();	
			entity.TotalInventory_ID = totalInventory_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TotalInventoryReport_ContractReference(long id, long totalInventory_ID, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan)
		{
			KDT_VNACCS_TotalInventoryReport_ContractReference entity = new KDT_VNACCS_TotalInventoryReport_ContractReference();			
			entity.ID = id;
			entity.TotalInventory_ID = totalInventory_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TotalInventoryReport_ContractReference_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TotalInventoryReport_ContractReference(long id, long totalInventory_ID, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan)
		{
			KDT_VNACCS_TotalInventoryReport_ContractReference entity = new KDT_VNACCS_TotalInventoryReport_ContractReference();			
			entity.ID = id;
			entity.TotalInventory_ID = totalInventory_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TotalInventoryReport_ContractReference(long id)
		{
			KDT_VNACCS_TotalInventoryReport_ContractReference entity = new KDT_VNACCS_TotalInventoryReport_ContractReference();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteBy_TotalInventory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_ContractReference_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TotalInventoryReport_ContractReference> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            try
            {
                if (this.ID == 0)
                {
                    this.ID = this.Insert();
                }
                else
                {
                    this.Update();
                }
                foreach (KDT_VNACCS_TotalInventoryReport_Detail item in DetailCollection)
                {
                    if (item.ContractReference_ID == 0)
                    {
                        item.ContractReference_ID = this.ID;
                        item.Insert();
                    }
                    else
                    {
                        item.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_TotalInventoryReport_Detail item in DetailCollection)
                {
                    item.Delete();
                }
                this.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}