using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class ITE : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute IED { get; set; }
public PropertiesAttribute UCD { get; set; }
public PropertiesAttribute IDF { get; set; }
public PropertiesAttribute IDT { get; set; }
public PropertiesAttribute CCN { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute APC { get; set; }
public PropertiesAttribute TEN { get; set; }

public ITE()
        {
CH = new PropertiesAttribute(6, typeof(string));
IED = new PropertiesAttribute(1, typeof(string));
UCD = new PropertiesAttribute(13, typeof(string));
IDF = new PropertiesAttribute(8, typeof(DateTime));
IDT = new PropertiesAttribute(8, typeof(DateTime));
CCN = new PropertiesAttribute(14, typeof(string));
ICN = new PropertiesAttribute(20, typeof(string));
APC = new PropertiesAttribute(1, typeof(string));
TEN = new PropertiesAttribute(12, typeof(int));
TongSoByte = 101;
}

}public partial class EnumGroupID
    {
}

}
