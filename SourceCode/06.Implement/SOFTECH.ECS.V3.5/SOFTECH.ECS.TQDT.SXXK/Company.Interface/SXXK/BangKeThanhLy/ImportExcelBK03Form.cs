﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.BLL.KDT;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class ImportExcelBK03Form : BaseForm
    {
        public BKNPLChuaThanhLyCollection bkCollection = new BKNPLChuaThanhLyCollection();
        public ImportExcelBK03Form()
        {
            InitializeComponent();
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnReadFile_Click(object sender, EventArgs e)
        {
            //cvError.Validate();
            //if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRowIndex.Value);
            if (beginRow < 0)
            {
                //error.SetError(txtRowIndex, "Dòng bắt đầu phải lớn hơn 0");
                //error.SetIconPadding(txtRowIndex, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"MSG_EXC01", "", txtSheet.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char STTColumn = Convert.ToChar(txtSTT.Text.Trim());
            int STTCol = ConvertCharToInt(STTColumn);

            char SoTKColumn = Convert.ToChar(txtSoTK.Text.Trim());
            int  SoTKCol = ConvertCharToInt(SoTKColumn);

            //char MaLHColumn = Convert.ToChar(txtMaLoaiHinh.Text.Trim());
            //int MaLHCol = ConvertCharToInt(MaLHColumn);

            //char NamDKColumn = Convert.ToChar(txtNamDK.Text.Trim());
            //int NamDKCol = ConvertCharToInt(NamDKColumn);

            //char MaHQColumn = Convert.ToChar(txtMaHQ.Text.Trim());
            //int MaHQCol = ConvertCharToInt(MaHQColumn);

            //char NgayDKColumn = Convert.ToChar(txtNgayDK.Text.Trim());
            //int  NgayDKCol = ConvertCharToInt(NgayDKColumn);

            char MaNPLColumn = Convert.ToChar(txtMaNPL.Text.Trim());
            int  MaNPLCol = ConvertCharToInt(MaNPLColumn);

            char TenNPLColumn = Convert.ToChar(txtTenNPL.Text.Trim());
            int  TenNPLCol = ConvertCharToInt(TenNPLColumn);

            char LuongColumn = Convert.ToChar(txtLuong.Text.Trim());
            int  LuongCol = ConvertCharToInt(LuongColumn);

            char DVTColumn = Convert.ToChar(txtDVT.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        BKNPLChuaThanhLy bk = new BKNPLChuaThanhLy();
                        bk.STTHang = Convert.ToInt32(wsr.Cells[STTCol].Value);
                        if (bk.STTHang.ToString().Length == 0)
                        {
                            MLMessages("STT ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        //bk.SoToKhai = Convert.ToInt32(wsr.Cells[SoTKCol].Value);
                        //bk.SoToKhai = CapSoToKhai.GetSoTKByVNACC(Convert.ToInt32(wsr.Cells[SoTKCol].Value));
                        try
                        {
                            CapSoToKhai cs = CapSoToKhai.GetFromTKMDVNACCS(Convert.ToDecimal(wsr.Cells[SoTKCol].Value));
                            bk.SoToKhai = cs.SoTK;
                            bk.SoToKhaiVNACCS = cs.SoTKVNACCS;
                            bk.MaLoaiHinh = "NV" + cs.MaLoaiHinh;
                            bk.NamDangKy = Convert.ToInt16(cs.NamDangKy);
                            bk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            ToKhaiMauDich tkmd = ToKhaiMauDich.LoadBySoToKhai(cs.SoTK, bk.MaLoaiHinh, cs.NamDangKy);
                            bk.NgayDangKy = tkmd.NgayDangKy;
                            bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                            bk.MaHaiQuan = tkmd.MaHaiQuan;
                        }
                        catch (Exception)
                        {
                            MLMessages("Số tờ khai ở dòng " + (wsr.Index + 1) + " không đúng.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        //if (bk.SoToKhai.ToString().Trim().Length == 0)
                        //{
                        //    MLMessages("Số tờ khai ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                        //    return;
                        //}
                        //bk.MaLoaiHinh = wsr.Cells[MaLHCol].Value.ToString();
                        //if (bk.MaLoaiHinh.ToString().Trim().Length == 0)
                        //{
                        //    MLMessages("Mã loại hình ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                        //    return;
                        //}
                        //bk.NamDangKy = Convert.ToInt16(wsr.Cells[NamDKCol].Value);
                        //if (bk.NamDangKy.ToString().Length == 0)
                        //{
                        //    MLMessages("Năm đăng ký ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                        //    return;
                        //}
                        //bk.MaHaiQuan = Convert.ToString(wsr.Cells[MaHQCol].Value).Trim();
                        //if (bk.MaHaiQuan.Trim().Length == 0)
                        //{
                        //    MLMessages("Mã hải quan ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                        //    return;
                        //}
                        //bk.NgayDangKy = Convert.ToDateTime(wsr.Cells[NgayDKCol].Value);
                        //if (bk.NgayDangKy.ToString().Length == 0)
                        //{
                        //    MLMessages("Ngày đăng ký ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                        //    return;
                        //}
                        bk.MaNPL = wsr.Cells[MaNPLCol].Value.ToString();
                        if (bk.MaNPL.ToString().Trim().Length == 0)
                        {
                            MLMessages("Mã NPL ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        bk.TenNPL = Convert.ToString(wsr.Cells[TenNPLCol].Value);
                        if (bk.TenNPL.ToString().Trim().Length == 0)
                        {
                            MLMessages("Tên NPL ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        bk.Luong = Convert.ToDecimal(wsr.Cells[LuongCol].Value);
                        if (bk.Luong.ToString().Trim().Length == 0)
                        {
                           MLMessages("Lượng chưa thanh lý ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        bk.TenDVT = Convert.ToString(wsr.Cells[DVTCol].Value);
                        bk.DVT_ID = DonViTinh_GetID(bk.TenDVT);
                        if (bk.TenDVT.ToString().Trim().Length == 0)
                        {
                            MLMessages("Đơn vị tính ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        bkCollection.Add(bk);
                    }
                    catch (Exception ex)
                    {
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                        }
                    }
                }
            }
            if (bkCollection.Count > 0)
            {
                MLMessages("Nhập hàng từ Excel thành công ", "MSG_EXC06", "", false);
            }
            else
            {
                MLMessages("Nhập hàng từ Excel không thành công ", "MSG_EXC06", "", false);
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog sf = new OpenFileDialog();
            sf.ShowDialog(this);
            txtFilePath.Text = sf.FileName;
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_NPLCHUATL();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
