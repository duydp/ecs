﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 || KD_V4 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
using System.Data;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class SelectHangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public ToKhaiMauDich TKMD;
        public List<HangMauDich> HMDTMPCollection = new List<HangMauDich>();
        //-----------------------------------------------------------------------------------------

        public SelectHangMauDichForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkHangExit(long HMD_ID)
        {
            foreach (HangMauDich hmd in HMDTMPCollection)
            {
                if (hmd.ID == HMD_ID) return true;
            }
            return false;
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            TKMD.LoadHMDCollection();
            dgList.DataSource = TKMD.HMDCollection;
            if (TKMD.TrangThaiXuLy != -1)
            {
                //this.btnChonAll.Enabled = false;
                //this.btnChonNhieuHang.Enabled = false;
            }
        }

        private void btnChonAll_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length < 0) return;
            foreach (GridEXRow i in items)
            {
                if (i.IsChecked)
                {
                    HangMauDich HMD = (HangMauDich)i.DataRow;
                    if (!checkHangExit(HMD.ID))
                        HMDTMPCollection.Add(HMD);
                }
                //if (i.RowType == RowType.Record)
                //{
                //    HangMauDich HMD = (HangMauDich)i.DataRow;
                //    if(!checkHangExit(HMD.ID))
                //        HMDTMPCollection.Add(HMD);
                //}
            }
            this.Close();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetRows();
            if (items.Length < 0) return;
            foreach (GridEXRow i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDich HMD = (HangMauDich)i.DataRow;
                    if (!checkHangExit(HMD.ID))
                        HMDTMPCollection.Add(HMD);
                }
            }
            this.Close();
        }




    }
}
