using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class ToKhaiChuyenTiep
	{
        protected int _SoThuTuHang;
        protected string _MaHS = String.Empty;
        protected string _MaHang = String.Empty;
        protected string _TenHang = String.Empty;
        protected string _NuocXuatXu = String.Empty;
        protected decimal _SoLuong;
        protected decimal _DonGia;
        protected decimal _TriGia;
        protected decimal _DonGiaTT;
        protected decimal _TriGiaTT;
        protected string _DonViTinh = String.Empty;
		#region Properties.

        public int SoThuTuHang
        {
            set { this._SoThuTuHang = value; }
            get { return this._SoThuTuHang; }
        }
        public string NuocXuatXu
        {
            get
            {
                return _NuocXuatXu;
            }
            set
            {
                if (_NuocXuatXu == value)
                    return;
                _NuocXuatXu = value;
            }
        }
        public decimal SoLuong
        {
            get
            {
                return _SoLuong;
            }
            set
            {
                if (_SoLuong == value)
                    return;
                _SoLuong = value;
            }
        }
        public decimal DonGia
        {
            get
            {
                return _DonGia;
            }
            set
            {
                if (_DonGia == value)
                    return;
                _DonGia = value;
            }
        }
        public decimal TriGia
        {
            get
            {
                return _TriGia;
            }
            set
            {
                if (_TriGia == value)
                    return;
                _TriGia = value;
            }
        }
        public decimal DonGiaTT
        {
            get
            {
                return _DonGiaTT;
            }
            set
            {
                if (_DonGiaTT == value)
                    return;
                _DonGiaTT = value;
            }
        }
        public decimal TriGiaTT
        {
            get
            {
                return _TriGiaTT;
            }
            set
            {
                if (_TriGiaTT == value)
                    return;
                _TriGiaTT = value;
            }
        }
        public string DonViTinh
        {
            get
            {
                return _DonViTinh;
            }
            set
            {
                if (_DonViTinh == value)
                    return;
                _DonViTinh = value;
            }
        }
        public string TenHang
        {
            get
            {
                return _TenHang;
            }
            set
            {
                if (_TenHang == value)
                    return;
                _TenHang = value;
            }
        }
        public string MaHS
        {
            get
            {
                return _MaHS;
            }
            set
            {
                if (_MaHS == value)
                    return;
                _MaHS = value;
            }
        }
        public string MaHang
        {
            get
            {
                return _MaHang;
            }
            set
            {
                if (_MaHang == value)
                    return;
                _MaHang = value;
            }
        }
		public long ID { set; get; }
		public long IDHopDong { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string MaHaiQuanTiepNhan { set; get; }
		public long SoToKhai { set; get; }
		public string CanBoDangKy { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string SoHopDongDV { set; get; }
		public DateTime NgayHDDV { set; get; }
		public DateTime NgayHetHanHDDV { set; get; }
		public string NguoiChiDinhDV { set; get; }
		public string MaKhachHang { set; get; }
		public string TenKH { set; get; }
		public string SoHDKH { set; get; }
		public DateTime NgayHDKH { set; get; }
		public DateTime NgayHetHanHDKH { set; get; }
		public string NguoiChiDinhKH { set; get; }
		public string MaDaiLy { set; get; }
		public string MaLoaiHinh { set; get; }
		public string DiaDiemXepHang { set; get; }
		public decimal TyGiaVND { set; get; }
		public decimal TyGiaUSD { set; get; }
		public decimal LePhiHQ { set; get; }
		public string SoBienLai { set; get; }
		public DateTime NgayBienLai { set; get; }
		public string ChungTu { set; get; }
		public string NguyenTe_ID { set; get; }
		public string MaHaiQuanKH { set; get; }
		public long ID_Relation { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public int ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public short NamDK { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string Huongdan_PL { set; get; }
		public string PTTT_ID { set; get; }
		public string DKGH_ID { set; get; }
		public DateTime ThoiGianGiaoHang { set; get; }
		public string LoaiHangHoa { set; get; }
		public decimal SoKien { set; get; }
		public decimal TrongLuong { set; get; }
		public double TrongLuongNet { set; get; }
		public string DaiDienDoanhNghiep { set; get; }
		public string MaDonViUT { set; get; }
		public string TenDonViUT { set; get; }
		public string MaDaiLyTTHQ { set; get; }
		public string TenDaiLyTTHQ { set; get; }
		public string TenDonViDoiTac { set; get; }
		public string NuocXK_ID { set; get; }
		public string NuocNK_ID { set; get; }
		public string CuaKhau_ID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ToKhaiChuyenTiep Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			ToKhaiChuyenTiep entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ToKhaiChuyenTiep();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ToKhaiChuyenTiep> SelectCollectionAll()
		{
			List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ToKhaiChuyenTiep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
        public static List<ToKhaiChuyenTiep> SelectCollectionDynamicAndHCT(string whereCondition, string orderByExpression)
        {
            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();

            SqlDataReader reader = (SqlDataReader)SelectReaderDynamicByHCT(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));

                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocXX"))) entity.NuocXuatXu = reader.GetString(reader.GetOrdinal("ID_NuocXX"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DonViTinh = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong(long iDHopDong)
		{
			List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_IDHopDong(iDHopDong);
			while (reader.Read())
			{
				ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<ToKhaiChuyenTiep> SelectCollectionTKNhapBy_PhanBo(long iDHopDong ,int NamHangVeKho)
        {
            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            SqlDataReader reader = (SqlDataReader)SelectReaderTKNBy_PhanBo(iDHopDong,NamHangVeKho);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        // Select by foreign key return collection		
        public static IList<ToKhaiChuyenTiep> SelectCollectionTKXuatBy_PhanBo(long iDHopDong)
        {
            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            SqlDataReader reader = (SqlDataReader)SelectReaderTKXBy_PhanBo(iDHopDong);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }		
		public static DataSet SelectBy_IDHopDong(long iDHopDong)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            //whereCondition.Replace("LoaiVanDon", "HuongDan_PL");
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
        public static IDataReader SelectReaderDynamicByHCT(string whereCondition, string orderByExpression)
        {
            //whereCondition.Replace("LoaiVanDon", "HuongDan_PL");
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicAndHCT]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_IDHopDong(long iDHopDong)
		{
            const string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);
            return db.ExecuteReader(dbCommand);
		}
        public static IDataReader SelectReaderTKNBy_PhanBo(long iDHopDong,int NamHangVeKho)
        {
            const string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectTKNhapBy_PhanBo";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);
            db.AddInParameter(dbCommand, "@NamHangVeKho", SqlDbType.Int, NamHangVeKho);

            return db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderTKXBy_PhanBo(long iDHopDong)
        {
            const string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectTKXuatBy_PhanBo";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);
            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertToKhaiChuyenTiep(long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pTTT_ID, string dKGH_ID, DateTime thoiGianGiaoHang, string loaiHangHoa, decimal soKien, decimal trongLuong, double trongLuongNet, string daiDienDoanhNghiep, string maDonViUT, string tenDonViUT, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string nuocXK_ID, string nuocNK_ID, string cuaKhau_ID)
		{
			ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();	
			entity.IDHopDong = iDHopDong;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.SoToKhai = soToKhai;
			entity.CanBoDangKy = canBoDangKy;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoHopDongDV = soHopDongDV;
			entity.NgayHDDV = ngayHDDV;
			entity.NgayHetHanHDDV = ngayHetHanHDDV;
			entity.NguoiChiDinhDV = nguoiChiDinhDV;
			entity.MaKhachHang = maKhachHang;
			entity.TenKH = tenKH;
			entity.SoHDKH = soHDKH;
			entity.NgayHDKH = ngayHDKH;
			entity.NgayHetHanHDKH = ngayHetHanHDKH;
			entity.NguoiChiDinhKH = nguoiChiDinhKH;
			entity.MaDaiLy = maDaiLy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.TyGiaVND = tyGiaVND;
			entity.TyGiaUSD = tyGiaUSD;
			entity.LePhiHQ = lePhiHQ;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.ChungTu = chungTu;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.MaHaiQuanKH = maHaiQuanKH;
			entity.ID_Relation = iD_Relation;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.ThoiGianGiaoHang = thoiGianGiaoHang;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.SoKien = soKien;
			entity.TrongLuong = trongLuong;
			entity.TrongLuongNet = trongLuongNet;
			entity.DaiDienDoanhNghiep = daiDienDoanhNghiep;
			entity.MaDonViUT = maDonViUT;
			entity.TenDonViUT = tenDonViUT;
			entity.MaDaiLyTTHQ = maDaiLyTTHQ;
			entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.NuocXK_ID = nuocXK_ID;
			entity.NuocNK_ID = nuocNK_ID;
			entity.CuaKhau_ID = cuaKhau_ID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
			db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year <= 1753 ? DBNull.Value : (object) NgayHDDV);
			db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHDDV);
			db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
			db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
			db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year <= 1753 ? DBNull.Value : (object) NgayHDKH);
			db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHDKH);
			db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
			db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
			db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
			db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object) ThoiGianGiaoHang);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
			db.AddInParameter(dbCommand, "@DaiDienDoanhNghiep", SqlDbType.NVarChar, DaiDienDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
			db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
			db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
			db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
			db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiChuyenTiep item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateToKhaiChuyenTiep(long id, long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pTTT_ID, string dKGH_ID, DateTime thoiGianGiaoHang, string loaiHangHoa, decimal soKien, decimal trongLuong, double trongLuongNet, string daiDienDoanhNghiep, string maDonViUT, string tenDonViUT, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string nuocXK_ID, string nuocNK_ID, string cuaKhau_ID)
		{
			ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();			
			entity.ID = id;
			entity.IDHopDong = iDHopDong;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.SoToKhai = soToKhai;
			entity.CanBoDangKy = canBoDangKy;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoHopDongDV = soHopDongDV;
			entity.NgayHDDV = ngayHDDV;
			entity.NgayHetHanHDDV = ngayHetHanHDDV;
			entity.NguoiChiDinhDV = nguoiChiDinhDV;
			entity.MaKhachHang = maKhachHang;
			entity.TenKH = tenKH;
			entity.SoHDKH = soHDKH;
			entity.NgayHDKH = ngayHDKH;
			entity.NgayHetHanHDKH = ngayHetHanHDKH;
			entity.NguoiChiDinhKH = nguoiChiDinhKH;
			entity.MaDaiLy = maDaiLy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.TyGiaVND = tyGiaVND;
			entity.TyGiaUSD = tyGiaUSD;
			entity.LePhiHQ = lePhiHQ;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.ChungTu = chungTu;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.MaHaiQuanKH = maHaiQuanKH;
			entity.ID_Relation = iD_Relation;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.ThoiGianGiaoHang = thoiGianGiaoHang;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.SoKien = soKien;
			entity.TrongLuong = trongLuong;
			entity.TrongLuongNet = trongLuongNet;
			entity.DaiDienDoanhNghiep = daiDienDoanhNghiep;
			entity.MaDonViUT = maDonViUT;
			entity.TenDonViUT = tenDonViUT;
			entity.MaDaiLyTTHQ = maDaiLyTTHQ;
			entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.NuocXK_ID = nuocXK_ID;
			entity.NuocNK_ID = nuocNK_ID;
			entity.CuaKhau_ID = cuaKhau_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year < 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year < 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
			db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year < 1753 ? DBNull.Value : (object) NgayHDDV);
			db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year < 1753 ? DBNull.Value : (object) NgayHetHanHDDV);
			db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
			db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
			db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year < 1753 ? DBNull.Value : (object) NgayHDKH);
			db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year < 1753 ? DBNull.Value : (object) NgayHetHanHDKH);
			db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
			db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
			db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year < 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
			db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year < 1753 ? DBNull.Value : (object) ThoiGianGiaoHang);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
			db.AddInParameter(dbCommand, "@DaiDienDoanhNghiep", SqlDbType.NVarChar, DaiDienDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
			db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
			db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
			db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
			db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiChuyenTiep item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateToKhaiChuyenTiep(long id, long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pTTT_ID, string dKGH_ID, DateTime thoiGianGiaoHang, string loaiHangHoa, decimal soKien, decimal trongLuong, double trongLuongNet, string daiDienDoanhNghiep, string maDonViUT, string tenDonViUT, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string nuocXK_ID, string nuocNK_ID, string cuaKhau_ID)
		{
			ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();			
			entity.ID = id;
			entity.IDHopDong = iDHopDong;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.SoToKhai = soToKhai;
			entity.CanBoDangKy = canBoDangKy;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.SoHopDongDV = soHopDongDV;
			entity.NgayHDDV = ngayHDDV;
			entity.NgayHetHanHDDV = ngayHetHanHDDV;
			entity.NguoiChiDinhDV = nguoiChiDinhDV;
			entity.MaKhachHang = maKhachHang;
			entity.TenKH = tenKH;
			entity.SoHDKH = soHDKH;
			entity.NgayHDKH = ngayHDKH;
			entity.NgayHetHanHDKH = ngayHetHanHDKH;
			entity.NguoiChiDinhKH = nguoiChiDinhKH;
			entity.MaDaiLy = maDaiLy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.DiaDiemXepHang = diaDiemXepHang;
			entity.TyGiaVND = tyGiaVND;
			entity.TyGiaUSD = tyGiaUSD;
			entity.LePhiHQ = lePhiHQ;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.ChungTu = chungTu;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.MaHaiQuanKH = maHaiQuanKH;
			entity.ID_Relation = iD_Relation;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.ThoiGianGiaoHang = thoiGianGiaoHang;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.SoKien = soKien;
			entity.TrongLuong = trongLuong;
			entity.TrongLuongNet = trongLuongNet;
			entity.DaiDienDoanhNghiep = daiDienDoanhNghiep;
			entity.MaDonViUT = maDonViUT;
			entity.TenDonViUT = tenDonViUT;
			entity.MaDaiLyTTHQ = maDaiLyTTHQ;
			entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.NuocXK_ID = nuocXK_ID;
			entity.NuocNK_ID = nuocNK_ID;
			entity.CuaKhau_ID = cuaKhau_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year < 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year < 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
			db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year < 1753 ? DBNull.Value : (object) NgayHDDV);
			db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year < 1753 ? DBNull.Value : (object) NgayHetHanHDDV);
			db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
			db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
			db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
			db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
			db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year < 1753 ? DBNull.Value : (object) NgayHDKH);
			db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year < 1753 ? DBNull.Value : (object) NgayHetHanHDKH);
			db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
			db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
			db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year < 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
			db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
			db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year < 1753 ? DBNull.Value : (object) ThoiGianGiaoHang);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
			db.AddInParameter(dbCommand, "@DaiDienDoanhNghiep", SqlDbType.NVarChar, DaiDienDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
			db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
			db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
			db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
			db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiChuyenTiep item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteToKhaiChuyenTiep(long id)
		{
			ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_IDHopDong(long iDHopDong)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_DeleteBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ToKhaiChuyenTiep item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}