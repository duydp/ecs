namespace Company.Interface.GC
{
    partial class PhieuXuatNhapKhoManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuXuatNhapKhoManager));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbTimKiem = new Janus.Windows.EditControls.UICheckBox();
            this.grbtimkiem = new Janus.Windows.EditControls.UIGroupBox();
            this.clcTuNgay = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcDenNgay = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rdPhieuXuatKho = new System.Windows.Forms.RadioButton();
            this.rdPhieuNhapKho = new System.Windows.Forms.RadioButton();
            this.btnXNT_Details = new Janus.Windows.EditControls.UIButton();
            this.btnXNT = new Janus.Windows.EditControls.UIButton();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTaoMoi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnTaoMoi_V4 = new Janus.Windows.EditControls.UIButton();
            this.btnTaoMoi_V4_GCCT = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).BeginInit();
            this.grbtimkiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnAddNew);
            this.grbMain.Controls.Add(this.btnTaoMoi_V4_GCCT);
            this.grbMain.Controls.Add(this.btnTaoMoi_V4);
            this.grbMain.Controls.Add(this.btnTaoMoi);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1011, 586);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ckbTimKiem);
            this.uiGroupBox1.Controls.Add(this.grbtimkiem);
            this.uiGroupBox1.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.rdPhieuXuatKho);
            this.uiGroupBox1.Controls.Add(this.rdPhieuNhapKho);
            this.uiGroupBox1.Controls.Add(this.btnXNT_Details);
            this.uiGroupBox1.Controls.Add(this.btnXNT);
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(987, 90);
            this.uiGroupBox1.TabIndex = 24;
            this.uiGroupBox1.Text = "Thông tin tờ khai";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ckbTimKiem
            // 
            this.ckbTimKiem.Location = new System.Drawing.Point(152, 10);
            this.ckbTimKiem.Margin = new System.Windows.Forms.Padding(0);
            this.ckbTimKiem.Name = "ckbTimKiem";
            this.ckbTimKiem.Size = new System.Drawing.Size(15, 15);
            this.ckbTimKiem.TabIndex = 282;
            this.ckbTimKiem.VisualStyleManager = this.vsmMain;
            this.ckbTimKiem.CheckedChanged += new System.EventHandler(this.ckbTimKiem_CheckedChanged);
            // 
            // grbtimkiem
            // 
            this.grbtimkiem.Controls.Add(this.clcTuNgay);
            this.grbtimkiem.Controls.Add(this.clcDenNgay);
            this.grbtimkiem.Controls.Add(this.label1);
            this.grbtimkiem.Controls.Add(this.label2);
            this.grbtimkiem.Enabled = false;
            this.grbtimkiem.Location = new System.Drawing.Point(161, 11);
            this.grbtimkiem.Name = "grbtimkiem";
            this.grbtimkiem.Size = new System.Drawing.Size(217, 70);
            this.grbtimkiem.TabIndex = 281;
            this.grbtimkiem.VisualStyleManager = this.vsmMain;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.Location = new System.Drawing.Point(87, 14);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.ReadOnly = false;
            this.clcTuNgay.Size = new System.Drawing.Size(107, 21);
            this.clcTuNgay.TabIndex = 19;
            this.clcTuNgay.TagName = "";
            this.clcTuNgay.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcTuNgay.WhereCondition = "";
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.Location = new System.Drawing.Point(87, 41);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.ReadOnly = false;
            this.clcDenNgay.Size = new System.Drawing.Size(107, 21);
            this.clcDenNgay.TabIndex = 19;
            this.clcDenNgay.TagName = "";
            this.clcDenNgay.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcDenNgay.WhereCondition = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Từ ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Đến ngày";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(476, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(127, 21);
            this.txtSoHopDong.TabIndex = 276;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(393, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 275;
            this.label7.Text = "Số hợp đồng";
            // 
            // rdPhieuXuatKho
            // 
            this.rdPhieuXuatKho.AutoSize = true;
            this.rdPhieuXuatKho.Location = new System.Drawing.Point(16, 56);
            this.rdPhieuXuatKho.Name = "rdPhieuXuatKho";
            this.rdPhieuXuatKho.Size = new System.Drawing.Size(109, 17);
            this.rdPhieuXuatKho.TabIndex = 274;
            this.rdPhieuXuatKho.TabStop = true;
            this.rdPhieuXuatKho.Text = "Phiếu xuất kho";
            this.rdPhieuXuatKho.UseVisualStyleBackColor = true;
            this.rdPhieuXuatKho.CheckedChanged += new System.EventHandler(this.rdPhieuXuatKho_CheckedChanged);
            // 
            // rdPhieuNhapKho
            // 
            this.rdPhieuNhapKho.AutoSize = true;
            this.rdPhieuNhapKho.Location = new System.Drawing.Point(18, 24);
            this.rdPhieuNhapKho.Name = "rdPhieuNhapKho";
            this.rdPhieuNhapKho.Size = new System.Drawing.Size(111, 17);
            this.rdPhieuNhapKho.TabIndex = 274;
            this.rdPhieuNhapKho.TabStop = true;
            this.rdPhieuNhapKho.Text = "Phiếu nhập kho";
            this.rdPhieuNhapKho.UseVisualStyleBackColor = true;
            this.rdPhieuNhapKho.CheckedChanged += new System.EventHandler(this.rdPhieuNhapKho_CheckedChanged);
            // 
            // btnXNT_Details
            // 
            this.btnXNT_Details.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXNT_Details.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXNT_Details.Icon")));
            this.btnXNT_Details.ImageIndex = 4;
            this.btnXNT_Details.Location = new System.Drawing.Point(617, 16);
            this.btnXNT_Details.Name = "btnXNT_Details";
            this.btnXNT_Details.Size = new System.Drawing.Size(205, 29);
            this.btnXNT_Details.TabIndex = 269;
            this.btnXNT_Details.Text = "Xuất - Nhập - Tồn (Chi tiết)";
            this.btnXNT_Details.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXNT_Details.Click += new System.EventHandler(this.btnXNT_Details_Click);
            // 
            // btnXNT
            // 
            this.btnXNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXNT.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXNT.Icon")));
            this.btnXNT.ImageIndex = 4;
            this.btnXNT.Location = new System.Drawing.Point(618, 53);
            this.btnXNT.Name = "btnXNT";
            this.btnXNT.Size = new System.Drawing.Size(204, 29);
            this.btnXNT.TabIndex = 269;
            this.btnXNT.Text = "Xuất - Nhập - Tồn (Tổng lượng)";
            this.btnXNT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTimKiem.Icon")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.Location = new System.Drawing.Point(476, 53);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(126, 29);
            this.btnTimKiem.TabIndex = 269;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnTimKiem.VisualStyleManager = this.vsmMain;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(860, 541);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 29);
            this.btnClose.TabIndex = 271;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 8);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(981, 411);
            this.dgList.TabIndex = 277;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgList);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 104);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(987, 422);
            this.uiGroupBox2.TabIndex = 24;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnTaoMoi
            // 
            this.btnTaoMoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTaoMoi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTaoMoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoMoi.Image = global::Company.Interface.Properties.Resources._83;
            this.btnTaoMoi.Location = new System.Drawing.Point(570, 541);
            this.btnTaoMoi.Name = "btnTaoMoi";
            this.btnTaoMoi.Size = new System.Drawing.Size(200, 29);
            this.btnTaoMoi.TabIndex = 272;
            this.btnTaoMoi.Text = "Tạo mới(Chọn TK_VNACCS)";
            this.btnTaoMoi.VisualStyleManager = this.vsmMain;
            this.btnTaoMoi.Click += new System.EventHandler(this.btnTaoMoi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = global::Company.Interface.Properties.Resources.clear;
            this.btnXoa.Location = new System.Drawing.Point(778, 541);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(76, 29);
            this.btnXoa.TabIndex = 273;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Image = global::Company.Interface.Properties.Resources._83;
            this.btnAddNew.Location = new System.Drawing.Point(73, 541);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(76, 29);
            this.btnAddNew.TabIndex = 272;
            this.btnAddNew.Text = "Tạo mới";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnTaoMoi_V4
            // 
            this.btnTaoMoi_V4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTaoMoi_V4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTaoMoi_V4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoMoi_V4.Image = global::Company.Interface.Properties.Resources._83;
            this.btnTaoMoi_V4.Location = new System.Drawing.Point(363, 541);
            this.btnTaoMoi_V4.Name = "btnTaoMoi_V4";
            this.btnTaoMoi_V4.Size = new System.Drawing.Size(200, 29);
            this.btnTaoMoi_V4.TabIndex = 272;
            this.btnTaoMoi_V4.Text = "Tạo mới(Chọn TK_V4)";
            this.btnTaoMoi_V4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnTaoMoi_V4.Click += new System.EventHandler(this.btnTaoMoi_V4_Click);
            // 
            // btnTaoMoi_V4_GCCT
            // 
            this.btnTaoMoi_V4_GCCT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTaoMoi_V4_GCCT.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTaoMoi_V4_GCCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoMoi_V4_GCCT.Image = global::Company.Interface.Properties.Resources._83;
            this.btnTaoMoi_V4_GCCT.Location = new System.Drawing.Point(157, 541);
            this.btnTaoMoi_V4_GCCT.Name = "btnTaoMoi_V4_GCCT";
            this.btnTaoMoi_V4_GCCT.Size = new System.Drawing.Size(200, 29);
            this.btnTaoMoi_V4_GCCT.TabIndex = 272;
            this.btnTaoMoi_V4_GCCT.Text = "Tạo mới(Chọn TK_V4_GCCT)";
            this.btnTaoMoi_V4_GCCT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnTaoMoi_V4_GCCT.Click += new System.EventHandler(this.btnTaoMoi_V4_GCCT_Click);
            // 
            // PhieuXuatNhapKhoManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1011, 586);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PhieuXuatNhapKhoManager";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách tờ khai";
            this.Load += new System.EventHandler(this.PhieuXuatNhapKhoManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).EndInit();
            this.grbtimkiem.ResumeLayout(false);
            this.grbtimkiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnTaoMoi;
        private System.Windows.Forms.RadioButton rdPhieuXuatKho;
        private System.Windows.Forms.RadioButton rdPhieuNhapKho;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIGroupBox grbtimkiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcTuNgay;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcDenNgay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UICheckBox ckbTimKiem;
        private Janus.Windows.EditControls.UIButton btnXNT;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Janus.Windows.EditControls.UIButton btnXNT_Details;
        private Janus.Windows.EditControls.UIButton btnTaoMoi_V4;
        private Janus.Windows.EditControls.UIButton btnTaoMoi_V4_GCCT;
    }
}