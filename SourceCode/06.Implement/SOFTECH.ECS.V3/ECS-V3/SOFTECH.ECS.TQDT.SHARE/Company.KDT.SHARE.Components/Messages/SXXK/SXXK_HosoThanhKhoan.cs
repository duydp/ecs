﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_HosoThanhKhoan : DeclarationBase
    {
        /// <summary>
        /// Danh sách các tờ khai Nhập khẩu thanh khoản
        /// </summary>
        [XmlElement("ImportDeclarationList")]
        public ImportDeclarationDocument ImportDeclarationList { get; set; }
        /// <summary>
        /// Danh sách các tờ khai Xuất khẩu thanh khoản
        /// </summary>
        [XmlElement("ExportDeclarationList")]
        public ImportDeclarationDocument ExportDeclarationList { get; set; }
        /// <summary>
        /// Bảng kê các chứng từ thanh toán hàng NSXXK
        /// </summary>
        [XmlElement("PaymentDocumentList")]
        public PaymentDocumentList PaymentDocumentList { get; set; }
        /// <summary>
        /// Bảng kê sản phẩm 
        /// </summary>
        [XmlElement("MaterialList")]
        public DocumentList MaterialList { get; set; }
        /// <summary>
        /// Bảng kê nguyên liệu chưa đưa vào thanh khoản
        /// </summary>
        [XmlElement("MaterialLeft")]
        public DocumentList MaterialLeft { get; set; }
        /// <summary>
        /// Bảng kê nguyên phụ liệu xuất khẩu qua sản phẩm theo hợp đồng gia công
        /// </summary>
        [XmlElement("MaterialExport")]
        public DocumentList MaterialExport { get; set; }
        /// <summary>
        /// Bảng kê nguyên phụ liệu không xuất khẩu xin nộp thuế vào ngân sách
        /// </summary>
        [XmlElement("MaterialNotExport")]
        public DocumentList MaterialNotExport { get; set; }
    }
}
