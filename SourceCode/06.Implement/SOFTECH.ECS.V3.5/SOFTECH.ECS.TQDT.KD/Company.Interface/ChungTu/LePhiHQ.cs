﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
#endif


namespace Company.Interface
{
    public partial class LePhiHQ : Form
    {
        public LePhiHQ()
        {
            InitializeComponent();
        }
        private DataTable dtLePhi = LoaiLePhiHQ.SelectAll().Tables[0];
        public ToKhaiMauDich TKMD;
        DataColumn dcSoTien = new DataColumn("SoTien", typeof(decimal));

        //public List<LePhiHQ> LePhiHQ = new List<LePhiHQ>();
        private void LePhiHQ_Load(object sender, EventArgs e)
        {
            dtLePhi.Columns.Add(dcSoTien);
            if (TKMD == null)
                TKMD = new ToKhaiMauDich();
            if (TKMD != null && TKMD.ID > 0)
            {
                if (TKMD.LePhiHQCollection == null || TKMD.LePhiHQCollection.Count == 0)
                    TKMD.LoadListLePhiHQ();
                if (TKMD.LePhiHQCollection != null && TKMD.LePhiHQCollection.Count > 0)
                {
                    foreach (Company.KDT.SHARE.QuanLyChungTu.LePhiHQ item in TKMD.LePhiHQCollection)
                    {
                        foreach (DataRow dr in dtLePhi.Rows)
                        {
                            if (item.MaLePhi == dr["MaLePhi"].ToString().Trim())
                                dr["SoTien"] = item.SoTienLePhi;
                        }
                    }
                }
            }
            dgLePhi.DataSource = dtLePhi;
        }

        private void LePhiHQ_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //Huypvt chinh ngay 17/01/2013 : Do ngu qua.

                Company.KDT.SHARE.QuanLyChungTu.LePhiHQ.DeleteDynamic("TKMD_ID=" + TKMD.ID);
                
                TKMD.LePhiHQCollection = new List<Company.KDT.SHARE.QuanLyChungTu.LePhiHQ>();
                foreach (DataRow dr in dtLePhi.Rows)
                {
                    if (dr["SoTien"] != DBNull.Value && Convert.ToDecimal(dr["SoTien"]) > 0)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LePhiHQ lephi = new Company.KDT.SHARE.QuanLyChungTu.LePhiHQ();
                        lephi.MaLePhi = dr["MaLePhi"].ToString();
                        lephi.SoTienLePhi = Convert.ToDecimal(dr["SoTien"]);
                        TKMD.LePhiHQCollection.Add(lephi);
                    }

                }
                // Khanh oi, em xem lai doan code ben duoi sao code dai the. :). Code dai ma con sai nua chu. he' he'


                //DataTable dtchanges = dtLePhi.GetChanges();
                //if (dtchanges != null && dtchanges.Rows.Count > 0)
                //{
                //    if (TKMD.LePhiHQCollection == null || TKMD.LePhiHQCollection.Count == 0)
                //    {
                //        TKMD.LePhiHQCollection = new List<Company.KDT.SHARE.QuanLyChungTu.LePhiHQ>();
                //        foreach (DataRow dr in dtchanges.Rows)
                //        {
                //            Company.KDT.SHARE.QuanLyChungTu.LePhiHQ lephi = new Company.KDT.SHARE.QuanLyChungTu.LePhiHQ();
                //            lephi.MaLePhi = dr["MaLePhi"].ToString();
                //            lephi.SoTienLePhi = Convert.ToDecimal(dr["SoTien"]);
                //            TKMD.LePhiHQCollection.Add(lephi);
                //        }
                //    }
                //    else
                //    {
                //        foreach (DataRow dr in dtchanges.Rows)
                //        {
                //            bool isEdit = false; <-- dung co len nhieu the em :)
                //            foreach (Company.KDT.SHARE.QuanLyChungTu.LePhiHQ item in TKMD.LePhiHQCollection)
                //            {
                //                if (item.MaLePhi == dr["MaLePhi"].ToString().Trim())
                //                {
                //                    item.SoTienLePhi = Convert.ToDecimal(dr["SoTien"] != System.DBNull.Value ? dr["SoTien"] : 0);
                //                    isEdit = true;
                //                    break;
                //                }
                //            }
                //            if (!isEdit)
                //            {
                //                Company.KDT.SHARE.QuanLyChungTu.LePhiHQ lephi = new Company.KDT.SHARE.QuanLyChungTu.LePhiHQ();
                //                lephi.MaLePhi = dr["MaLePhi"].ToString();
                //                lephi.SoTienLePhi = Convert.ToDecimal(dr["SoTien"] != System.DBNull.Value ? dr["SoTien"] : 0);
                //                TKMD.LePhiHQCollection.Add(lephi);
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
