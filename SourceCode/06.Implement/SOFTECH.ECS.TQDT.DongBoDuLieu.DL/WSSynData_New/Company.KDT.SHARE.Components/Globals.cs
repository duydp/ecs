﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.ComponentModel;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.VNACCS;


namespace Company.KDT.SHARE.Components
{
    public static class Globals
    {
        public static string FILE_EXCEL_TOKHAI_SUADOI_BOSUNG = "Mau_so_06._To_khai_dieu_chinh_bo_sung.xls";
        public static DateTime TuNgay_HSTK;
        public static DateTime DenNgay_HSTK;
        public static bool IsKhaiVNACCS;
        public static bool IsMaPhanLoaiHangHoa;
        public static string FontHuongdan;
        public static DataSet GlobalDanhMucChuanHQ = new DataSet("DanhMucChuanHQ");

        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;

        public static List<string> ListTableNameSource = new List<string>();
        /// <summary>
        /// Khoi tao cac danh muc chuan Hai quan luu tai local.
        /// </summary>
        public static void KhoiTao_DanhMucChuanHQ()
        {
            try
            {
                if (GlobalDanhMucChuanHQ == null)
                    GlobalDanhMucChuanHQ = new DataSet("DanhMucChuanHQ");

                GlobalDanhMucChuanHQ.Tables.Clear();

                DataTable dt = DuLieuChuan.CuaKhau.SelectAll().Tables[0];
                dt.TableName = "CuaKhau";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.DieuKienGiaoHang.SelectAll().Tables[0];
                dt.TableName = "DieuKienGiaoHang";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.DonViHaiQuan.SelectAll().Tables[0];
                dt.TableName = "DonViHaiQuan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.DonViTinh.SelectAll().Tables[0];
                dt.TableName = "DonViTinh";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiChungTu.SelectAll().Tables[0];
                dt.TableName = "LoaiChungTu";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.LoaiCO.SelectAll().Tables[0];
                dt.TableName = "LoaiCO";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiHinhMauDich.SelectAll();
                dt.TableName = "LoaiHinhMauDich";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiPhiChungTuThanhToan.SelectAll().Tables[0];
                dt.TableName = "LoaiPhiChungTuThanhToan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
#if GC_V3 || GC_V4
                dt = DuLieuChuan.LoaiPhieuChuyenTiep.SelectAll();
                dt.TableName = "LoaiPhieuChuyenTiep";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());


                dt = DuLieuChuan.LoaiPhuKien.SelectAll();
                dt.TableName = "LoaiPhuKien";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.NhomSanPham.SelectAll().Tables[0];
                dt.TableName = "NhomSanPham";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
#endif

                dt = DuLieuChuan.NguyenTe.SelectAll().Tables[0];
                dt.TableName = "NguyenTe";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.NhomCuaKhau.SelectAll().Tables[0];
                dt.TableName = "NhomCuaKhau";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.Nuoc.SelectAll().Tables[0];
                dt.TableName = "Nuoc";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
                dt.TableName = "PhuongThucThanhToan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.PhuongThucVanTai.SelectAll().Tables[0];
                dt.TableName = "PhuongThucVanTai";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                //dt = DuLieuChuan.MaHS.SelectAll();
                //dt.TableName = "MaHS";
                //GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                // dt = DuLieuChuan.MucDichSuDung.SelectAll().Tables[0];
                // dt.TableName = "MucDichSuDung";
                // GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
                dt = DuLieuChuan.VNACCS_Mapper.SelectDynamic(" LoaiMapper='DVT'", null).Tables[0];
                dt.TableName = "DVTMapper";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());


                dt = DuLieuChuan.VNACCS_Mapper.SelectDynamic(" LoaiMapper='MaHQ'", null).Tables[0];
                dt.TableName = "MaHQMapper";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
                
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Khoi tao cac danh muc chuan Hai quan luu tai local. Ham KhoiTao_DanhMucChuanHQ_MaHS() nay phai load sau ham KhoiTao_DanhMucChuanHQ()
        /// </summary>
        public static void KhoiTao_DanhMucChuanHQ_MaHS()
        {
            try
            {
                DataTable dt = DuLieuChuan.MaHS.SelectAllIntoDanhMuc();
                dt.TableName = "MaHS";
                if (GlobalDanhMucChuanHQ.Tables.Contains("MaHS")) GlobalDanhMucChuanHQ.Tables.Remove("MaHS");
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static decimal tinhDinhMucChung(decimal dm, decimal tylehaohut)
        {
            decimal dmc = dm + (dm * tylehaohut / 100);
            return dmc;
        }
        public static string Translate(this string trangthaixuly)
        {
            return Translate(int.Parse(trangthaixuly));
        }
        public static string Translate(int trangthaixuly)
        {
            switch (trangthaixuly)
            {
                case -1: return "Chưa khai báo";
                case 0: return "Chờ duyệt";
                case 1: return "Đã duyệt";
                case 2: return "Không phê duyệt";
                case -2: return "Chờ duyệt đã sửa chữa";
                case 3: return "Đã phân luồng";
                case -3: return "Đã khai báo, chưa có phản hồi";
                case -4: return "Hủy khai báo";
                case 5: return "Sửa tờ khai";
                case -5: return "Hủy tờ khai";
                case 10: return "Đã hủy";
                case 11: return "Chờ hủy";
                default:
                    return string.Empty;
            }
        }

        #region Trim Mã hải quan
        public static string trimMaHaiQuan(string maHQ)
        {
            return (maHQ.Trim());
        }
        #endregion

        #region Tỷ Giá - Exchange Rate

        public static decimal GetNguyenTe(string maNguyenTe)
        {
            try
            {
                return Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.Load(maNguyenTe).TyGiaTinhThue;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return 0;
        }

        /// <summary>
        /// Lay ty gia ngoai te Online
        /// </summary>
        public static bool GetTyGia()
        {
            try
            {
                //Update by Hungtq
                //List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateFromNHNNVN();
                List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateFromTCHQ();
                //Lay ty gia tai website Tong cuc neu khong the lay thong tin tu website Ngan hang nha nuoc VN
                if (listNgoaiTe.Count == 0)
                {
                    listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateFromTCHQ();
                }

                if (listNgoaiTe.Count == 0)
                {
                    return false;
                }

                //Luu file XML
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                doc.AppendChild(root);
                foreach (Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe item in listNgoaiTe)
                {
                    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                    itemMaNT.InnerText = item.Ten;

                    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                    itemTenNT.InnerText = item.Ten;

                    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                    itemTyNT.InnerText = item.TyGiaTinhThue.ToString();

                    itemNT.AppendChild(itemMaNT);
                    itemNT.AppendChild(itemTenNT);
                    itemNT.AppendChild(itemTyNT);
                    root.AppendChild(itemNT);
                }
                doc.Save(System.Windows.Forms.Application.StartupPath + "\\TyGia.xml");

                //Update Data
                Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.InsertUpdateCollection(listNgoaiTe);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Lay ty gia tu website Ngan hang Nha nuoc Viet nam
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <returns></returns>
        public static List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> GetUSDExchangeRateFromNHNNVN()
        {
            List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> collection = new List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe>();

            try
            {
                //Lấy dữ liệu từ website: http://www.sbv.gov.vn/wps/portal/vn
                //string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/pVLLboMwEPwkFtsYc3RsMGCCgyGP5hIhtYoSNQ9VUdTm6-skVaXSBg71yqfZmZ0drbf0XO3b82bdnjaHffvqLbwlXYEkUZgEBBivMWRG6AiURiRG3txbAFnVW3Ycf5wWxUWcm-2lgrJp30s5d79C47KYPM_sdMT5di3xm9N8oqsEcYqSFIHJkYSMcMsRNbgm4Y-JJhKBm8iEAJtgFQV3tlA8JWEBoPJKQKbympGRDwDBgN8b-wFuZvgfbGKCwayW144-7zeFB4_DF_4gGz_o6JtURpCxXBeNP8EJwne8L_k-f4x2-KxhseOPwkqzsQ-TXzgBlwCJo8qoWW5D2u8vQ539VIJCt5-Nq5pLhKZBRx-AXP1TkFwnSFkysL_fi-cK-ufHA_551_8f19V3Ie5-yvSwe_GOu7PWuqA2_i72CXkZa0g!/dl3/d3/L0lDU0lKSmdwcGlRb0tVUWtnQSEhL29Pb2dBRUlRaGpFQ1VJZ0FBQUl5RkFNaHdVaFM0TFVFQVVvIS80QzFiOVdfTnIwZ0NVZ3hFbVJDVXdpSkg0QSEhLzdfRjJBNjJGSDIwME5SMjBJNDNOM1BTNzAwVTcvNFd0ME40NTcwMDIzMC8xNTM0OTU1NzkyMjUvYmZfYWN0aW9uL21haW4!";

                // string path = "http://sbv.gov.vn/portal/faces/vi/vim/vipages_trangchu/qlnh/tygia/tygiacheo";
                string path = "http://www.customs.gov.vn/Lists/ExchangeRate/Default.aspx";
                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlWeb().Load(path);

                //WebBrowser wb = new WebBrowser();
                ////wb.ScriptErrorsSuppressed = true;
                //wb.Url = new Uri(path);
                //wb.Navigate(path);

                System.Globalization.CultureInfo culVN = new System.Globalization.CultureInfo("vi-VN");

                ////Create a Web-Request to an URL
                //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(path);

                ////Defined poperties for the Web-Request
                //httpWebRequest.Method = "POST";
                //httpWebRequest.MediaType = "HTTP/1.1";
                //httpWebRequest.ContentType = "text/xml";
                //httpWebRequest.UserAgent = "Example Client";

                ////Defined data for the Web-Request
                //byte[] byteArrayData = Encoding.ASCII.GetBytes("A string you would like to send");
                //httpWebRequest.ContentLength = byteArrayData.Length;

                ////Attach data to the Web-Request
                //Stream dataStream = httpWebRequest.GetRequestStream();
                //dataStream.Write(byteArrayData, 0, byteArrayData.Length);
                //dataStream.Close();

                ////Send Web-Request and receive a Web-Response
                //HttpWebResponse httpWebesponse = (HttpWebResponse)httpWebRequest.GetResponse();

                ////Translate data from the Web-Response to a string
                //dataStream = httpWebesponse.GetResponseStream();
                //StreamReader streamreader = new StreamReader(dataStream, Encoding.UTF8);
                //string response = streamreader.ReadToEnd();
                //streamreader.Close();

                #region Lay ty gia nguyen te USD
                HtmlNodeCollection nc = htmlDocument.DocumentNode.SelectNodes("//div[@float='left']|//Table[@class='list']|//tbody|//tr[@class='noBR']");


                //DateTime ngayApDung = Convert.ToDateTime(nc[i].SelectSingleNode("//span[@name='ngayapdung']").InnerText);
                string giatriso = nc[0].SelectSingleNode("//td[@class='cellRight']").InnerText;
                giatriso = giatriso.Replace(" đ", "");
                decimal tyGiaUSD = Convert.ToDecimal(giatriso != "" ? giatriso : "0", culVN);

                if (tyGiaUSD != 0)
                {
                    Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe usd = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                    usd.ID = "USD";
                    usd.Ten = "Đô la Mỹ";
                    usd.TyGiaTinhThue = tyGiaUSD;
                    usd.DateCreated = usd.DateModified = DateTime.Now;

                    collection.Add(usd);

                }

                #endregion

                #region Lay ty gia xuat khau nguyen te khac
                //Get cung tai vi tri 2 trong danh sach
                HtmlNodeCollection ncKhac = htmlDocument.DocumentNode.SelectNodes("//Table[@class='list']");

                for (int j = 0; j < ncKhac.Count; j++)
                {
                    //DateTime apDungTuNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='startDateD']").InnerText);
                    //DateTime apDungDenNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='endDateD']").InnerText);
                    //string maNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='NGOAITE']").InnerText;
                    //string tenNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='TENGOI']").InnerText;
                    //decimal tyGiaNgoaiTe = Convert.ToDecimal(ncKhac[j].SelectSingleNode("//span[@name='GIATRI']").InnerText);
                    string[] array = ncKhac[j].InnerText.Trim().Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

                    if (array.Length != 0 && array.Length == 4)
                    {
                        Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe nt = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                        nt.ID = array[1];
                        nt.Ten = array[2];
                        nt.TyGiaTinhThue = Convert.ToDecimal(array[3], culVN);
                        nt.DateCreated = nt.DateModified = DateTime.Now;

                        collection.Add(nt);
                    }
                }
                #endregion
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

        /// <summary>
        /// Lay ty gia tu website Tong cuc Hai quan Viet nam
        /// </summary>
        /// <param name="htmlDocument"></param>
        /// <returns></returns>
        public static List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> GetUSDExchangeRateFromTCHQ()
        {
            List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> collection = new List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe>();

            try
            {
                //Lấy dữ liệu từ website: http://www.customs.gov.vn
                //string path = "http://www.customs.gov.vn/Lists/TyGia/Default.aspx";

                string path = "http://www.customs.gov.vn/Lists/ExchangeRate/Default.aspx";
                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlWeb().Load(path);

                System.Globalization.CultureInfo culEn = new System.Globalization.CultureInfo("en-US");

                #region Lay ty gia nguyen te USD
                HtmlNodeCollection ncUSD = htmlDocument.DocumentNode.SelectNodes("//table[@class='list']//tr");

                for (int i = 1; i < 2; i++)
                {
                    HtmlNodeCollection nodeTD = ncUSD[i].SelectNodes("td");

                    
                    string giatriso = nodeTD[1].InnerText.Replace(" đ", "");
                    giatriso = giatriso.Replace(".", "");
                    giatriso = giatriso.Replace(",", ".");

                    decimal tyGiaUSD = Convert.ToDecimal(giatriso != "" ? giatriso : "0", culEn);
                    if (tyGiaUSD != 0)
                    {
                        Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe usd = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                        usd.ID = "USD";
                        usd.Ten = "Đô la Mỹ";
                        usd.TyGiaTinhThue = tyGiaUSD;
                        usd.DateCreated = usd.DateModified = DateTime.Now;

                        collection.Add(usd);
                    }
                }
                #endregion
                #region Lay ty gia nguyen te khac
                HtmlNodeCollection nc = htmlDocument.DocumentNode.SelectNodes("//div[@style='width:60%;float:right;']//table//tr");

                for (int i = 1; i < nc.Count; i++)
                {
                    HtmlNodeCollection nodeTD = nc[i].SelectNodes("td");


                    string maNT = nodeTD[0].InnerText;
                    string tenNT = nodeTD[1].InnerText;
                    string giatriso = nodeTD[3].InnerText.Replace(" đ", "");
                    giatriso = giatriso.Replace(".", "");
                    giatriso = giatriso.Replace(",", ".");
                    decimal tyGiaNT = Convert.ToDecimal(giatriso != "" ? giatriso : "0", culEn);
                    if (tyGiaNT != 0)
                    {
                        Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe ntObj = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                        ntObj.ID = maNT;
                        ntObj.Ten = tenNT;
                        ntObj.TyGiaTinhThue = tyGiaNT;
                        ntObj.DateCreated = ntObj.DateModified = DateTime.Now;

                        collection.Add(ntObj);
                    }
                }
                #endregion



            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

        #endregion

        #region SAVE NODE XML
        public static void SaveNodeXmlAppSettings(string key, object value, object setting)
        {
            SaveNodeXmlAppSettings(key, value);
            try
            {
                setting = value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        public static string ReadNodeXmlAppSettings(string key)
        {
            return ReadNodeXmlAppSettings(key, "");
        }
        public static string ReadNodeXmlAppSettings(string key, string defaultValue)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key, defaultValue);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }

        public static void UpdateNodeXml(System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                XmlNode Node = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']");
                if (Node != null)
                {
                    result = Node.SelectSingleNode("@value").Value;
                }
                else
                {
                    AddNodeConfig(key, "");
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            return result;
        }
        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, string default_Value)
        {
            string result = "";

            try
            {
                XmlNode Node = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']");
                if (Node != null)
                {

                    result = Node.SelectSingleNode("@value").Value;
                    if (string.IsNullOrEmpty(result))
                    {
                        result = default_Value;
                    }
                }
                else
                {
                    AddNodeConfig(key, default_Value);
                    result = default_Value;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
            if (string.IsNullOrEmpty(result))
                result = default_Value;
            return result;
        }
        public static void AddNodeConfig(string key, string ValueDefault)
        {
            //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Add(key, ValueDefault);
            config.Save();
        }

        public static void SaveNodeXmlSetting(string name, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string s = "//setting[@name='" + name + "']";
                doc.SelectSingleNode(s).SelectSingleNode("value").ChildNodes[0].Value = value.ToString();
                doc.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Error at key: " + name, ex); }
        }

        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[0].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void ReadNodeXmlConnectionStrings()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                string[] arr = connectionString.Split(new char[] { ';' });

                sConnectionString.Server = arr[0].Split(new char[] { '=' })[1];
                sConnectionString.Database = arr[1].Split(new char[] { '=' })[1];
                sConnectionString.User = arr[2].Split(new char[] { '=' })[1];
                sConnectionString.Pass = arr[3].Split(new char[] { '=' })[1];
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlConnectionStrings2()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                return connectionString;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }
        #endregion

        public static XmlNode GetNodeConfigDN(XmlDocument doc, string key)
        {
            try
            {
                foreach (XmlNode node in doc.GetElementsByTagName("Config"))
                {
                    if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                    {
                        return node["Value"];
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            XmlDocument docx = new XmlDocument();
            return docx.CreateNode(XmlNodeType.Element, key, "");
        }
        public static string GetConfig(XmlDocument doc, string key, string defaultValue)
        {
            XmlNode node = GetNodeConfigDN(doc, key);
            if (node == null) return defaultValue;
            else if (string.IsNullOrEmpty(node.InnerText)) return defaultValue;
            else
                return node.InnerText;
        }
        static string GetConfig(string key)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                return config.AppSettings.Settings[key].Value.ToString();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            return "";
        }
        public static bool IsSignOnLan { get; set; }
        public static string DataSignLan { get; set; }

        public static string MA_DON_VI { get; set; }

        public static bool IsTest { get; set; }
        public static bool IsTinhThue { get; set; }

        public static bool InCV3742 { get; set; }

        public static int TriGiaNT = 4;
        public static int DonGiaNT = 6;

        public static bool isKhaiDL;

        // True: Khai báo thanh khoản các bảng kê tờ khai nhập và bản kê tờ khai xuất theo danh sách các tờ khai tham gia thanh khoản
        // False: Khai báo tất cả các tờ khai trong bảng kê của hồ sơ thanh khoản
        public static bool KhaiBaoThanhKoanLoai2 { get; set; }

        #region Cấu hình chữ ký số V3 LANNT
        static bool _isRemmberLogin = false;
        public static bool IsRememberLogin
        {
            get
            {
                return _isRemmberLogin;
            }
            set
            {
                _isRemmberLogin = value;
            }
        }
        static string _userNameLogin = string.Empty;
        public static string UserNameLogin
        {
            get
            {
                return _userNameLogin;
            }
            set
            {
                _userNameLogin = value;
            }
        }
        static string _passwordLogin = string.Empty;
        public static string PasswordLogin
        {
            get
            {
                return _passwordLogin;
            }
            set
            {
                _passwordLogin = value;
            }
        }

        public static bool IsRememberSignOnline;
        static string _appSend = string.Empty;
        public static string AppSend
        {
            get
            {
                return _appSend;
            }
        }
        static int _countSend = 1;
        public static int CountSend
        {
            get { return _countSend; }
            set { _countSend = value; }
        }
        static int _timeoutBackup = 30;
        public static int TimeoutBackup
        {
            get { return _timeoutBackup; }
            set { _timeoutBackup = value; }
        }

        static string _GetX509CertificatedName = string.Empty;
        public static string GetX509CertificatedName
        {
            get
            {
                return _GetX509CertificatedName;
            }
            set
            {
                _GetX509CertificatedName = value;
            }
        }
        static bool _isSignature = false;
        public static bool IsSignature
        {
            get
            {
                return _isSignature;
            }
            set
            {
                _isSignature = value;
            }

        }
        static string _passwordSign = string.Empty;
        public static string PasswordSign
        {
            get
            {
                return _passwordSign;
            }
            set
            {
                _passwordSign = value;
            }
        }
        public static string ChuKySo { get; set; }
        public static bool IsDaiLy { get; set; }

        static bool _isRememberSign = false;
        public static bool IsRememberSign { get { return _isRememberSign; } set { _isRememberSign = value; } }
        static string _versionSend = string.Empty;
        static bool _suDungHttps = false;
        public static bool SuDungHttps
        {
            get
            {
                return _suDungHttps;
            }
            set
            {
                _suDungHttps = value;
            }

        }

        static bool _isSignRemote = false;
        public static bool IsSignRemote
        {
            get
            {
                return _isSignRemote;
            }
            set
            {
                _isSignRemote = value;
            }
        }

        static string _userNameSignRemote;
        public static string UserNameSignRemote
        {
            get
            {
                return _userNameSignRemote;
            }
            set
            {
                _userNameSignRemote = value;
            }
        }
        static string _passwordSignRemote;
        public static string PasswordSignRemote
        {
            get
            {
                return _passwordSignRemote;
            }
            set
            {
                _passwordSignRemote = value;
            }
        }
        static string _fontName = "Unicode";
        public static string FontName
        {
            get
            {
                return _fontName;
            }
            set
            {
                _fontName = value;
            }
        }
        /// <summary>
        /// Mặc định khai báo V3 là 3.00 tưng ứng với các version còn lại.
        /// </summary>
        public static string VersionSend
        {
            get
            {
                return _versionSend;
            }
            set
            {
                _versionSend = value;
            }
        }

        static int _timeConnect = 2;
        public static int TimeConnect
        {
            get
            {
                return _timeConnect;
            }
            set
            {
                _timeConnect = value;
            }
        }
        static int _timeDelay = 5;
        public static int Delay
        {
            get
            {
                return _timeDelay;
            }
            set
            {
                _timeDelay = value;
            }
        }

        static bool _laDNCX = false;
        public static bool LaDNCX
        {
            get
            {
                return _laDNCX;
            }
            set
            {
                _laDNCX = value;
            }

        }
        // lựa chọn cấu hình khai từ xa
        static bool _isKTX = false;
        public static bool IsKTX
        {
            get
            {
                return _isKTX;
            }
            set
            {
                _isKTX = value;
            }
        }
        static bool _isKhaiTK = false;
        public static bool IsKhaiTK
        {
            get
            {
                return _isKhaiTK;
            }
            set
            {
                _isKhaiTK = value;
            }
        }

        //TODO: Hungtq, 20/11/2013. Cau hinh chay da luong/ 1 luong AutoUpdate
        static bool _isAutoUpdateMultiThread = false;
        /// <summary>
        /// Cau hinh cho phep chay da luong tien trinh Cap nhat tu dong. True = Nhieu luong, False = 1 luong.
        /// </summary>
        public static bool IsAutoUpdateMultiThread
        {
            get
            {
                return _isAutoUpdateMultiThread;
            }
            set
            {
                _isAutoUpdateMultiThread = value;
            }
        }

        /// <summary>
        /// Khởi tạo giá trị từ file cấu hình LanNT
        /// </summary>
        static Globals()
        {
            try
            {
                string value = GetConfig("SuDungChuKySo");
                _isSignature = bool.Parse(value);
                _appSend = GetConfig("AssemblyName");
                _GetX509CertificatedName = GetConfig("ChuKySo");
                _passwordSign = GetConfig("PasswordSign");
                if (!string.IsNullOrEmpty(_passwordSign))
                    _passwordSign = Helpers.DecryptString(_passwordSign, "KEYWORD").Trim('\0');
                _versionSend = GetConfig("VersionSend");
                _timeDelay = int.Parse(GetConfig("TimeDelay"));
                _isRememberSign = bool.Parse(GetConfig("IsRememberSign"));
                _timeConnect = int.Parse(GetConfig("TimeConnect"));

                _suDungHttps = bool.Parse(GetConfig("SuDungHttps"));
                _isSignRemote = bool.Parse(GetConfig("IsSignRemote"));
                _userNameSignRemote = GetConfig("UserNameSignRemote");
                _passwordSignRemote = GetConfig("PasswordSignRemote");
                if (!string.IsNullOrEmpty(_passwordSignRemote))
                    _passwordSignRemote = Helpers.DecryptString(_passwordSignRemote, "KEYWORD").Trim('\0');

                #region Login ký từ xa| Chỉ dùng cho bản ký từ xa
                try
                {
                    _isRemmberLogin = bool.Parse(GetConfig("IsRememberLogin"));
                    _userNameLogin = GetConfig("UserName");
                    _passwordLogin = GetConfig("Password");
                    if (!string.IsNullOrEmpty(_passwordLogin))
                        _passwordLogin = Helpers.DecryptString(_passwordLogin, "KEYWORD").Trim('\0');
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                #endregion
                value = GetConfig("TriGiaNT");
                if (!string.IsNullOrEmpty(value))
                    TriGiaNT = int.Parse(value);
                value = GetConfig("DonGiaNT");
                if (!string.IsNullOrEmpty(value))
                    DonGiaNT = int.Parse(value);
                value = GetConfig("CountSend");
                if (!string.IsNullOrEmpty(value))
                    _countSend = int.Parse(value);

                value = GetConfig("TimeoutBackup");
                if (!string.IsNullOrEmpty(value))
                    _timeoutBackup = int.Parse(value);

                _fontName = GetConfig("FontName");
                if (string.IsNullOrEmpty(_fontName))
                    _fontName = "Unicode";
                string cfgName = "Config.xml";
                if (File.Exists(cfgName))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(cfgName);
                    if (doc != null)
                    {
                        XmlNode node = doc.SelectSingleNode("Root/Type");
                        if (node != null && !string.IsNullOrEmpty(node.InnerText))
                            IsDaiLy = node.InnerText.Trim() == "1" ? true : false;
                    }
                }

                value = GetConfig("IsSignOnLan");
                if (!string.IsNullOrEmpty(value))
                    IsSignOnLan = Convert.ToBoolean(value);

                DataSignLan = GetConfig("DataSignLan");
                MA_DON_VI = GetConfig("MA_DON_VI");

                FileInfo f = new FileInfo("Error.log");
                if (f.Exists && Math.Round(((decimal)f.Length / 1024) / 1024, 2) > 1)
                    f.Delete();
                f = new FileInfo("Error.txt");
                if (f.Exists && Math.Round(((decimal)f.Length / 1024) / 1024, 2) > 1)
                    f.Delete();

                try
                {
                    if (!bool.TryParse(GetConfig("IsKTX"), out _isKTX))
                        _isKTX = false;
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage("sai config KTX", ex);
                    Globals.SaveNodeXmlAppSettings("IsKTX", "false");
                }
                string _isTest = ReadNodeXmlAppSettings("IsTest", "False");
                IsTest = string.IsNullOrEmpty(_isTest) ? false : System.Convert.ToBoolean(_isTest);

                string _isTinhThue = ReadNodeXmlAppSettings("IsTinhThue", "True");
                IsTinhThue = string.IsNullOrEmpty(_isTinhThue) ? true : System.Convert.ToBoolean(_isTinhThue);

                string _InCV3742 = ReadNodeXmlAppSettings("InCV3742", "true");
                InCV3742 = string.IsNullOrEmpty(_InCV3742) ? true : System.Convert.ToBoolean(_InCV3742);

                KhaiBaoThanhKoanLoai2 = Convert.ToBoolean(ReadNodeXmlAppSettings("KhaiBaoThanhKoanLoai2", "False"));
                _laDNCX = Convert.ToBoolean(ReadNodeXmlAppSettings("LaDNCX", "False"));

                //TODO: Hungtq, 20/11/2013. Cau hinh chay da luong/ 1 luong AutoUpdate
                string _isAutoUpdateMultiThread = ReadNodeXmlAppSettings("IsAutoUpdateMultiThread", "False");
                IsAutoUpdateMultiThread = string.IsNullOrEmpty(_isAutoUpdateMultiThread) ? false : System.Convert.ToBoolean(_isAutoUpdateMultiThread);
                IsKhaiVNACCS = System.Convert.ToBoolean(ReadNodeXmlAppSettings("IsKhaiVNACCS", "False"));
                IsMaPhanLoaiHangHoa = System.Convert.ToBoolean(ReadNodeXmlAppSettings("IsMaPhanLoaiHangHoa", "False"));
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                _versionSend = "3.00";
                _appSend = "DEFAULT_SOFTECH";
            }
        }

        #endregion
        #region  V5 E-Customs
         public static bool KiemTraKBVNACC(string MaHaiQuan)
        {
            string MaCuc = string.IsNullOrEmpty(MaHaiQuan) ? string.Empty : MaHaiQuan.Substring(1,2);
            if (string.IsNullOrEmpty(MaCuc)) return false;
            if (DanhSachHQApDungVNACC.Contains(MaCuc))
                return true;
            else
                return false;
        }

         public static List<string> DanhSachHQApDungVNACC = new List<string>()
         {
             "43","01","03","34","47"
         };

        #endregion
       
        public struct sConnectionString
        {
            public static string Server;
            public static string Database;
            public static string User;
            public static string Pass;
        }

        public static System.Drawing.Printing.Margins GetMargin(string margins)
        {
            try
            {
                string[] arr = margins.Split(new char[] { ',' });

                return new System.Drawing.Printing.Margins(
                    int.Parse(arr[0].Trim()),
                    int.Parse(arr[1].Trim()),
                    int.Parse(arr[2].Trim()),
                    int.Parse(arr[3].Trim()));
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return new System.Drawing.Printing.Margins(0, 0, 0, 0);
            }

        }

        /// <summary>
        /// Lay ten 1 file xml trong danh sach trong 1 folder.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string GetFileName(string folderPath)
        {
            string[] fileNames = null;
            try
            {

                if (System.IO.Directory.Exists(folderPath))
                {
                    fileNames = System.IO.Directory.GetFiles(folderPath, "*.xml");
                }

                if (fileNames.Length > 0)
                {
                    return fileNames[0]; //Lay file xml dau tien trong danh sach (Neu co nhieu hon 1).
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }

        #region Send email
        public static bool SendMailReg(string subject, string tenDN, string maDN, string maHQ, string soDT, string nguoiLH, string diachi, string email, string loaiHinh, string appVersion, string dataversion)
        {
            return sendEmail(subject, string.Empty, string.Empty, tenDN, maDN, maHQ, soDT, string.Empty, nguoiLH, diachi, email, loaiHinh, appVersion, dataversion, string.Empty, new List<string>());
        }
        public static void sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string soDT, string soFax, string nguoiLH, string loaiHinh, string appVersion, string dataversion, string exception)
        {
            sendEmail(subject, body, imagePath, maDN, maDN, "", soDT, soFax, nguoiLH, loaiHinh, appVersion, dataversion, exception);
        }
        public static bool sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string maHQ, string soDT, string soFax, string nguoiLH, string loaiHinh, string appVersion, string dataversion, string exception)
        {
            return sendEmail(subject, body, imagePath, tenDN, maDN, maHQ, soDT, soFax, nguoiLH, string.Empty, string.Empty, loaiHinh, appVersion, dataversion, exception, new List<string>());
        }
        public static bool sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string maHQ, string soDT, string soFax, string nguoiLH, string diachi, string email, string loaiHinh, string appVersion, string dataversion, string exception, List<string> Files)
        {
            try
            {
                //string ccTo = Globals.ReadNodeXmlAppSettings("MailDoanhNghiep");
                string toEmail = Globals.ReadNodeXmlAppSettings("ToMail");
                if (!CheckEmail(toEmail))
                    toEmail = "ecs@softech.vn";
                string fromEmail = "hotrotqdt@gmail.com";
                string content = "<font color=#1F497D><b>Hệ thống tự động báo lỗi</b><br><b>Tên doanh nghiệp:</b> " + tenDN + "<br>";
                content += string.IsNullOrEmpty(maDN) ? string.Empty : "<b>Mã doanh nghiệp: </b>" + maDN + "<br>";
                content += string.IsNullOrEmpty(diachi) ? string.Empty : "<b>Địa chỉ doanh nghiệp: </b>" + diachi + "<br>";
                content += string.IsNullOrEmpty(email) ? string.Empty : "<b>Email: </b>" + email + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Mã Hải quan: </b>" + maHQ + "<br>";
                content += string.IsNullOrEmpty(soDT) ? string.Empty : "<b>Số điện thoại: </b>" + soDT + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Số Fax: </b>" + soFax + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Người liên hệ: </b>" + nguoiLH + "<br>";
                //Hungtq update 16/07/2012.
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Loại hình khai báo: </b>" + loaiHinh + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Phiên bản phần mềm: </b>" + appVersion + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Phiên bản dữ liệu: </b>" + dataversion + "<br>";

                content += "<b>Thông báo lỗi như sau:</b> <br><br>";
                content += body + "</font><hr size=1 width=100% noshade style='color:#92D050' align=center><br><br><\t>";
                content += "<i>" + exception + "</i>";

                MailMessage messageObj = new MailMessage();
                messageObj.Subject = subject;
                messageObj.Body = content;
                messageObj.IsBodyHtml = true;
                messageObj.Priority = MailPriority.Normal;
                if (imagePath != "")
                {
                    Attachment attachment = new Attachment(imagePath);
                    messageObj.Attachments.Add(attachment);
                }
                foreach (string item in Files)
                {
                    if (File.Exists(item))
                        messageObj.Attachments.Add(new Attachment(item));
                }
                #region Send Mail

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(fromEmail, "hotrotqdt");
                client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                messageObj.From = new MailAddress(fromEmail);
                messageObj.To.Add(toEmail);
                //if (CheckEmail(ccTo))
                //    messageObj.CC.Add(ccTo);

                #endregion Send Mail
                client.Timeout = 15000;
                client.SendAsync(messageObj, "Sending..");
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        static void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            String token = (string)e.UserState;
            if (e.Error != null)
            {
                //comment by minhnd
                //throw e.Error;

            }
            else
            {
                //  LoggerWrapper.Logger.Write("Message Delivered.", LogCategory.Information);
            }
        }
        private static bool CheckEmail(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }
        public static bool SaveAsImage(string filename)
        {
            try
            {
                Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
                Size s = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

                Graphics g = Graphics.FromImage(b);
                g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, s, CopyPixelOperation.SourceCopy);
                b.Save(filename, ImageFormat.Jpeg);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            //b.Save(@"c:\ERROR.png", ImageFormat.Png);  
        }

        /// <summary>
        /// Kiểm tra chuỗi nhập vào có phải là kiểu số.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isNumber(string value)
        {
            bool isValid = true;

            char[] arr = value.ToCharArray();

            for (int i = 0; i < arr.Length; i++)
            {
                isValid &= Char.IsNumber(arr[i]);
            }

            return isValid;
        }

        public static bool isEmail(string inputEmail)
        {
            return CheckEmail(inputEmail);
        }
        #endregion

        #region Save Message
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao, string noidungthongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, noidungthongbao);
        }
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao, string noidungthongbao)
        {
            if (string.IsNullOrEmpty(xml)) return false;
            try
            {
                if (string.IsNullOrEmpty(xml)) return false;
                XmlDocument doc = new XmlDocument();
                Message message = new Message();
                message.ItemID = itemId;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = xml;
                bool canLoad = false;
                try
                {
                    doc.LoadXml(xml);
                    canLoad = true;

                }
                catch { }
                if (canLoad)
                {
                    if (doc.SelectSingleNode("Envelope/Header/Subject/type")!=null)
                    {
                        message.MessageType = (MessageTypes)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/type").InnerText);
                        message.MessageFunction = (MessageFunctions)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/function").InnerText);
                        message.ReferenceID = new Guid(doc.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
                        message.MessageFrom = doc.SelectSingleNode("Envelope/Header/From/identity").InnerText;
                        message.MessageTo = doc.SelectSingleNode("Envelope/Header/To/identity").InnerText;
                        message.CreatedTime = DateTime.Now;   
                    }
                }

                message.Insert();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao)
        {
            return SaveMessage(xml, itemId, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, string messagefrom, string messageto, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageFrom = messagefrom;
                message.MessageTo = messageto;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        public static string Message2File(string messageError)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(messageError);
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                string reference = string.Empty;
                if (node != null) reference = node.InnerText;
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Errors"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Errors");
                string filename = AppDomain.CurrentDomain.BaseDirectory + "Errors\\" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss_") + reference + ".xml";
                StreamWriter sw = File.CreateText(filename);
                sw.Write(messageError);
                sw.Close();
                return filename;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(messageError, ex);
                return string.Empty;
            }
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao)
        {
            return SaveMessage(content, itemId, guidstr, messagetype, function, tieudethongbao, string.Empty);
        }
        #endregion

        public static string GetPhanLuong(string phanLuong)
        {
            if (phanLuong.Trim() == "1")
                return "Luồng xanh";
            else if (phanLuong.Trim() == "2")
                return "Luồng vàng";
            if (phanLuong.Trim() == "3")
                return "Luồng đỏ";
            else
                return "";
        }
        /// <summary>
        /// Lây tên trạng thái xử lý
        /// </summary>
        /// <param name="trangThai"></param>
        /// <returns></returns>
        public static string GetTrangThaiXuLyTK(int trangThai)
        {
            if (trangThai == TrangThaiXuLy.DA_DUYET)
                return "Đã duyệt";
            else if (trangThai == TrangThaiXuLy.CHO_DUYET)
                return "Chờ duyệt";
            if (trangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                return "Chưa khai báo";
            if (trangThai == TrangThaiXuLy.SUATKDADUYET)
                return "Sửa tờ khai";
            if (trangThai == TrangThaiXuLy.HUYTKDADUYET)
                return "Hủy tờ khai";
            if (trangThai == TrangThaiXuLy.CHO_HUY)
                return "Chờ hủy";
            if (trangThai == TrangThaiXuLy.DA_HUY)
                return "Đã hủy";
            if (trangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                return "Không phê duyệt";
            else
                return "";
        }
        /// <summary>
        /// Lây tên trạng thái thanh khoản
        /// </summary>
        /// <param name="trangThai"></param>
        /// <returns></returns>
        public static string GetTrangThaiThanhKhoan(int trangThai)
        {
            if (trangThai == 0)
                return "Đang nhập liệu";
            else if (trangThai > 0 && trangThai < (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
                return "Đang chạy thanh khoản";
            if (trangThai == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
                return "Đã chạy thanh khoản";
            if (trangThai == (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo)
                return "Đã đóng hồ sơ";
            else
                return "";
        }

        public static string FormatNumber(int number)
        {
            return FormatNumber(number, false);

        }
        public static string FormatNumber(int number, bool batBuocSoThapPhan)
        {
            return "{0:###,###,##0" + Globals.GetDot(number, batBuocSoThapPhan) + "}";
        }
        /// <summary>
        /// Định dạng số thập phân
        /// chuẩn microsoft "###,###,###.########"
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static String GetPrecision(int num)
        {
            return GetPrecision(num, false);
        }
        public static String GetPrecision(int num, bool batBuocSoThapPhan)
        {
            string val = "###,###,##0" + GetDot(num, batBuocSoThapPhan);
            return val;
        }
        public static string GetDot(int decimalFormat, bool batBuocSoThapPhan)
        {
            string ret = string.Empty;
            if (decimalFormat < 0) return ret;
            else if (decimalFormat > 0)
                ret += ".";
            for (int i = 0; i < decimalFormat; i++)
            {
                if (!batBuocSoThapPhan)
                    ret += "#";
                else
                    ret += "0";
            }
            return ret;
        }
        /// <summary>
        /// Thoi gian cho gui & nhan (second). Mac dinh: 5s -> 5000 milisecond.
        /// </summary>
        /// <returns>MiliSeconds</returns>
        public static int TimeDelay()
        {
            int TimeDelay = 5;

            try
            {
                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");

                TimeDelay = delay != "" ? int.Parse(delay) : 5;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("TimeDelay", ex); }

            return TimeDelay * 1000; //1s= 1000ms.
        }

        public static string Zip(string sourceFile, string fromDirectory, string toDirectory)
        {
            string destinateFile = "";

            if (System.IO.File.Exists(sourceFile))
            {
                System.IO.FileInfo fi = new FileInfo(sourceFile);

                destinateFile = toDirectory + "\\" + fi.Name.Replace(fi.Extension, "") + ".zip";
            }
            else
                return "";

            if (System.IO.File.Exists(sourceFile))
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("zip.exe", "-j -r -9 -D \"" +
                                                         destinateFile + "\" \"" +
                                                         sourceFile + "\"");
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process ps = System.Diagnostics.Process.Start(psi);
                ps.WaitForExit();
            }

            return destinateFile;
        }

        /// <summary>
        /// Mở webrowser
        /// </summary>
        /// <param name="url"></param>
        public static void OpenWebrowser(string url)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = url;

            process.Start();
        }

        #region Menu Command - Main form

        /// <summary>
        /// Command menu Bieu thue (HS)
        /// </summary>
        /// <param name="key"></param>
        public static void OpenCommandBieuThue(string key)
        {
            string url = "";

            switch (key)
            {
                case "cmdTraCuuXNKOnline": //Tra cứu biểu thuế xuất nhập khẩu trực tuyến
                    url = "http://customs.gov.vn/Lists/BieuThue2012/TraCuu.aspx";
                    break;
                case "cmdTraCuuNoThueOnline": //Tra cứu nợ thuế trực tuyến
                    url = "http://customs.gov.vn/Lists/TraCuuNoThue/Default.aspx";
                    break;
                case "cmdTraCuuVanBanOnline": //Tra cứu thư viện văn bản trực tuyến
                    url = "http://vanbanphapquy.vn/Default.aspx";
                    break;
                case "cmdTuVanHQOnline": //Tư vấn Hải quan trực tuyến
                    url = "http://customs.gov.vn/Lists/HoTroTrucTuyen/Default.aspx";
                    break;
                case "cmdGetCategoryOnline": //Tư vấn Hải quan trực tuyến
                    url = "http://customs.gov.vn/Lists/HoTroTrucTuyen/Default.aspx";
                    break;
            }

            if (url.Length > 0)
                Company.KDT.SHARE.Components.Globals.OpenWebrowser(url);
        }

        /// <summary>
        /// Cập nhật dữ liệu danh mục Online từ Server về Doanh nghiệp.
        /// </summary>
        /// <returns></returns>
        public static string UpdateCategoryOnline()
        {
            ISyncData syndata = WebService.SyncService();

            DataSet ds = new DataSet();

            DateTime dateLastUpdated = new DateTime(1900, 1, 1);

            string msg = "";

            try
            {
                #region Get data from Internet

                // EDanhMucHaiQuan.CuaKhau:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.CuaKhau.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.CuaKhau);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.CuaKhau.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.CuaKhau.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Cửa khẩu:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.Cuc:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.Cuc.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.Cuc);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Cục Hải quan:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DieuKienGiaoHang:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DieuKienGiaoHang.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DieuKienGiaoHang);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DieuKienGiaoHang.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DieuKienGiaoHang.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Điều kiện giao hàng:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DonViHaiQuan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DonViHaiQuan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DonViHaiQuan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Đơn vị Hải quan:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DonViTinh:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DonViTinh.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DonViTinh);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViTinh.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViTinh.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Đơn vị tính:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.LoaiPhiChungTuThanhToan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.LoaiPhiChungTuThanhToan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.LoaiPhiChungTuThanhToan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiPhiChungTuThanhToan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiPhiChungTuThanhToan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Loại phí chứng từ thanh toán:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.MaHS:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.MaHS.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.MaHS);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.MaHS.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.MaHS.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Mã biểu thuế (HS):\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.NguyenTe:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.NguyenTe.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.NguyenTe);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Nguyên tệ:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.Nuoc:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.Nuoc.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.Nuoc);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.Nuoc.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.Nuoc.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Nước:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.PhuongThucThanhToan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.PhuongThucThanhToan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.PhuongThucThanhToan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucThanhToan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucThanhToan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Phương thức thanh toán:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.PhuongThucVanTai:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.PhuongThucVanTai.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.PhuongThucVanTai);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucVanTai.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucVanTai.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Phương thức vận tải:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.LoaiHinhMauDich:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.LoaiHinhMauDich.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.LoaiHinhMauDich);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiHinhMauDich.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiHinhMauDich.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Loại hình mậu dịch:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                #endregion

                return msg.Length != 0 ? "Đã cập nhật thông tin các Danh mục:" + msg : "";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msg = "E|" + ex.Message;
            }

            return msg;
        }

        /// <summary>
        /// Cập nhật dữ liệu danh mục Online từ Server về Doanh nghiệp.
        /// </summary>
        /// <returns></returns>
        public static string UpdateCategoryVNACCSOnline()
        { 
            //ISyncData syndata = WebService.SyncService();
            DataSet dsCategory = new DataSet();
            WSSyncData.Service syndata = WebService.SyncDataService();
            DateTime dateLastUpdated = new DateTime(1900, 1, 1);

            string msg = "";

            //try
            //{
            //    ///Md5 mkkb1998 =220c888ca1a69018c4a78bbff19e6273
            //    #region Get Category from Internet
            //    //Nhóm xử lý hồ sơ
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A014);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_CustomsSubSection.InsertUpdateCollection(VNACC_Category_CustomsSubSection.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Nhóm xử lý hồ sơ :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    // Mã nước(Country, coded), Mã nước xuất xứ
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A015);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_Nation.InsertUpdateCollection(VNACC_Category_Nation.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã nước(Country, coded), Mã nước xuất xứ :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A016);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_CityUNLOCODE.InsertUpdateCollection(VNACC_Category_CityUNLOCODE.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A016T);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_CityUNLOCODE.InsertUpdateCollection(VNACC_Category_CityUNLOCODE.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Cơ quan Hải quan
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A038);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_CustomsOffice.InsertUpdateCollection(VNACC_Category_CustomsOffice.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Cơ quan Hải quan :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A202);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_Cargo.InsertUpdateCollection(VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    // Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A204);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_Cargo.InsertUpdateCollection(VNACC_Category_Cargo.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm kho hàng CFS khai báo đến Chi cục HQ Bình Dương và Đồng Nai :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Container size
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A301);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_ContainerSize.InsertUpdateCollection(VNACC_Category_ContainerSize.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Container size :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã đơn vị tính (Packages unit code)
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A316);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_PackagesUnit.InsertUpdateCollection(VNACC_Category_PackagesUnit.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã đơn vị tính (Packages unit code) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã biểu thuế nhập khẩu
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A404);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_TaxClassificationCode.InsertUpdateCollection(VNACC_Category_TaxClassificationCode.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã biểu thuế nhập khẩu :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A501);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_QuantityUnit.InsertUpdateCollection(VNACC_Category_QuantityUnit.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã số hàng hóa (HS)
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A506);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_HSCode.InsertUpdateCollection(VNACC_Category_HSCode.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã số hàng hóa (HS) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã đồng tiền
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A527);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_CurrencyExchange.InsertUpdateCollection(VNACC_Category_CurrencyExchange.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã đồng tiền :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Loại chứng từ đính kèm
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A546);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_ApplicationProcedureType.InsertUpdateCollection(VNACC_Category_ApplicationProcedureType.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Loại chứng từ đính kèm :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã địa điểm dỡ hàng (Stations)
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A601);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_Station.InsertUpdateCollection(VNACC_Category_Station.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm dỡ hàng (Stations) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Mã địa điểm dỡ hàng (Border gate)
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A620);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_BorderGate.InsertUpdateCollection(VNACC_Category_BorderGate.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Mã địa điểm dỡ hàng (Border gate) :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Transport means (Vehicle) 
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A621);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_TransportMean.InsertUpdateCollection(VNACC_Category_TransportMean.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Transport means (Vehicle)  :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //OGA User
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.A700);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_OGAUser.InsertUpdateCollection(VNACC_Category_OGAUser.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-OGA User :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    //Tổng hợp danh mục
            //    dsCategory = syndata.UpdateCategoryOnline("220c888ca1a69018c4a78bbff19e6273", dateLastUpdated, Company.KDT.SHARE.Components.WSSyncData.ECategory.E001);
            //    if (dsCategory != null && dsCategory.Tables.Count > 0)
            //    {
            //        if (VNACC_Category_Common.InsertUpdateCollection(VNACC_Category_Common.ConvertDataSetToCollection(dsCategory)) == true && dsCategory.Tables[0].Rows.Count > 0)
            //            msg += "\r\n-Tổng hợp danh mục :\t" + dsCategory.Tables[0].Rows.Count + " bản ghi";
            //        dsCategory = null;
            //    }
            //    #endregion

            //    return msg.Length != 0 ? "Đã cập nhật thông tin các Danh mục:" + msg : "";
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    msg = "E|" + ex.Message;
            //}

            return msg;
        }

        /// <summary>
        /// Lay ngay cap nhat moi gan nhat cua Danh muc.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DateTime GetDanhMucMaxDateModified(string tableName)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT max(DateModified) as MaxDate FROM {0}", tableName);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                return reader["MaxDate"] != System.DBNull.Value ? Convert.ToDateTime(reader["MaxDate"]) : new DateTime(1900, 01, 01);
            }
            return new DateTime(1900, 1, 1);
        }

        /// <summary>
        /// Lay ngay cap nhat moi gan nhat cua Danh muc.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DateTime GetMaxDateModified(string tableName, string fieldName)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT max(" + fieldName + ") as MaxDate FROM {0}", tableName);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                return reader["MaxDate"] != System.DBNull.Value ? Convert.ToDateTime(reader["MaxDate"]) : new DateTime(1900, 01, 01);
            }
            return new DateTime(1900, 1, 1);
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số tự động. (Cắt 2 số '00' bên phải mã HS, chỉ lấy 8 số).
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Auto(string maHQ, string maDN, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_Convert_MaHS8SoAuto]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số theo giá trị mã HS mới
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="ma"></param>
        /// <param name="dvtId"></param>
        /// <param name="maHSCu"></param>
        /// <param name="maHSMoi"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Manual_ByCode(string maHQ, string maDN, string ma, string dvtId, string maHSCu, string maHSMoi, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_Convert_MaHS8So]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
                db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, ma);
                db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.NVarChar, dvtId);
                db.AddInParameter(dbCommand, "@MaHSCu", SqlDbType.NVarChar, maHSCu);
                db.AddInParameter(dbCommand, "@MaHSMoi", SqlDbType.NVarChar, maHSMoi);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số theo giá trị mã HS mới
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="ten"></param>
        /// <param name="dvtId"></param>
        /// <param name="maHSCu"></param>
        /// <param name="maHSMoi"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Manual_ByName(string maHQ, string maDN, string ten, string dvtId, string maHSCu, string maHSMoi, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_Convert_MaHS8So_Ten]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
                db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, ten);
                db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.NVarChar, dvtId);
                db.AddInParameter(dbCommand, "@MaHSCu", SqlDbType.NVarChar, maHSCu);
                db.AddInParameter(dbCommand, "@MaHSMoi", SqlDbType.NVarChar, maHSMoi);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Tải chương trình Image Resize từ trang codeplex.com.
        /// Tool open source, miễn phí.
        /// </summary>
        /// <returns></returns>
        public static string DownloadToolImageResize()
        {
            try
            {
                string url = "http://imageresizer.codeplex.com/downloads/get/347439";

                OpenWebrowser(url);

                //string folderPath = AppDomain.CurrentDomain.BaseDirectory + "Support";

                //if (!System.IO.Directory.Exists(folderPath))
                //    System.IO.Directory.CreateDirectory(folderPath);

                //string filePath = folderPath + "\\" + "ImageResizerSetup.exe";

                //if (!System.IO.File.Exists(filePath))
                //{
                //    WebClient clientDownload = new WebClient();

                //    clientDownload.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                //    clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                //    clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //    clientDownload.DownloadFileAsync(new Uri(url), filePath, "ImageResizerSetup.exe");

                //    clientDownload.Dispose();
                //    clientDownload = null;
                //}
                //else
                //    System.Diagnostics.Process.Start(filePath);

                return "";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }
        }

        public static string HDSDImageResize()
        {
            string folderPath = AppDomain.CurrentDomain.BaseDirectory + "Support";

            string filePath = folderPath + "\\" + "HuongDanSuDung_ImageResize.doc";

            if (!System.IO.File.Exists(filePath))
            {
                return "Thiếu tệp tin 'Hướng dẫn sử dụng giảm dung lượng ảnh' trên chương trình.";
            }
            else
            {
                System.Diagnostics.Process.Start(filePath);
                return "";
            }
        }

        public static string HDSD_CKS()
        {
            string folderPath = AppDomain.CurrentDomain.BaseDirectory + "Support";

            string filePath = folderPath + "\\" + "HuongDanDangKyCKS.pdf";

            if (!System.IO.File.Exists(filePath))
            {
                return "Thiếu tệp tin 'Hướng dẫn đăng ký và sử dụng Chữ ký số (CA)' trên chương trình.";
            }
            else
            {
                System.Diagnostics.Process.Start(filePath);
                return "";
            }
        }

        #endregion

        //public static string ConvertToString(object source)
        //{
        //    string ConvertToString;
        //    DateTime dateTime;

        //    if (source == null)
        //        return String.Empty;
        //    if (String.IsNullOrEmpty(Conversions.ToString(source)))
        //        return String.Empty;
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy\uFFFD");
        //    }
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy HH:mm:ss\uFFFD");
        //    }
        //    return source.ToString();
        //}

        //public static string FormatNumeric(object obj, int percent)
        //{
        //    string FormatNumeric;
        //    decimal dec;

        //    string ret = Conversions.ToString(0);
        //    string sFormat = "#,###,##0.0";

        //    for (int i = 1; i <= percent; i++)
        //    {
        //        sFormat += "#";
        //    }
        //    if (obj is int)
        //    {
        //        int i2 = Int32.Parse(Conversions.ToString(obj));
        //        ret = i2.ToString();
        //    }
        //    else if (obj is double)
        //    {
        //        double d = Double.Parse(Conversions.ToString(obj));
        //        ret = d.ToString(sFormat);
        //    }
        //    else if (obj is decimal)
        //    {
        //        dec = Decimal.Parse(Conversions.ToString(obj));
        //        ret = dec.ToString(sFormat);
        //    }
        //    else if (obj is string)
        //    {
        //        if (String.IsNullOrEmpty(Conversions.ToString(obj)))
        //        {
        //            ret = Conversions.ToString(0);
        //        }
        //        else
        //        {
        //            dec = Decimal.Parse(Conversions.ToString(obj));
        //            ret = dec.ToString(sFormat);
        //        }
        //    }
        //    return ret;
        //}

        #region EXCEL TEMPLATE

        public static void CreateExcelTemplate_ChuyenMaHS8So()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "ChuyenMaHS8So.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Tên hàng";
                workSheet.GetCell("C1").Value = "Đơn vị tính";
                workSheet.GetCell("D1").Value = "Mã HS cũ";
                workSheet.GetCell("E1").Value = "Mã HS mới";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion

        //mau excel them hang
        #region Excel Template Hang Hoa
        public static void CreateExcelTemplate_HangHoa()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "HangHoa_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Tên hàng";
                workSheet.GetCell("C1").Value = "Mã HS";
                workSheet.GetCell("D1").Value = "Xuất xứ ";
                workSheet.GetCell("E1").Value = "Đơn vị tính";
                workSheet.GetCell("F1").Value = "Số lượng";
                workSheet.GetCell("G1").Value = "Đơn giá";
                workSheet.GetCell("H1").Value = "TGTT";
                workSheet.GetCell("I1").Value = "TS XNK";
                workSheet.GetCell("J1").Value = "TS TTDB";
                workSheet.GetCell("K1").Value = "TS VAT";
                workSheet.GetCell("L1").Value = "TLCL giá";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #endregion
        #region EXCEL TEMPLATE BÁO CÁO QUYẾT TOÁN 
        public static void CreateExcelTemplate_GoodItem()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "BÁO CÁO QUYẾT TOÁN TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "STT";
                workSheet.GetCell("B1").Value = "LOẠI HÀNG HÓA";
                workSheet.GetCell("C1").Value = "TÀI KHOẢN";
                workSheet.GetCell("D1").Value = "TÊN HÀNG HÓA";
                workSheet.GetCell("E1").Value = "MÃ HÀNG HÓA ";
                workSheet.GetCell("F1").Value = "ĐVT";
                workSheet.GetCell("G1").Value = "TỒN ĐẦU KỲ";
                workSheet.GetCell("H1").Value = "NHẬP TRONG KỲ";
                workSheet.GetCell("I1").Value = "XUẤT TRONG KỲ";
                workSheet.GetCell("J1").Value = "TỒN CUỐI KỲ";
                workSheet.GetCell("K1").Value = "GHI CHÚ";

                workSheet.GetCell("A2").Value = "Số thứ tự phải tăng dần và liên tục";
                workSheet.GetCell("B2").Value = "Nhập giá trị là : 1 (Đối với Nguyên phụ liệu) ; Nhập giá trị là : 2 (Đối với Thành phẩm từ nguồn sản xuất)";
                workSheet.GetCell("C2").Value = "Nhập giá trị là : 152 (Đối với Nguyên phụ liệu) ; Nhập giá trị là : 155 (Đối với Thành phẩm từ nguồn sản xuất)";
                workSheet.GetCell("D2").Value = "Tên hàng hóa bắt buộc phải nhập";
                workSheet.GetCell("E2").Value = "Mã hàng hóa không bắt buộc phải nhập ";
                workSheet.GetCell("F2").Value = "Đơn vị tính không bắt buộc phải nhập (Nếu nhập phải nhập ĐVT theo chuẩn (ĐVT khai báo trên tờ khai VNACC) (Ví dụ : Cái/Chiếc thì nhập là : PCE )";
                workSheet.GetCell("G2").Value = "Chỉ cho phép nhập tối đa đến 4 số thập phân (Nhập tay không dùng Format để tránh sai dữ liệu khi đọc Excel vào)";
                workSheet.GetCell("H2").Value = "Chỉ cho phép nhập tối đa đến 4 số thập phân (Nhập tay không dùng Format để tránh sai dữ liệu khi đọc Excel vào)";
                workSheet.GetCell("I2").Value = "Chỉ cho phép nhập tối đa đến 4 số thập phân (Nhập tay không dùng Format để tránh sai dữ liệu khi đọc Excel vào)";
                workSheet.GetCell("J2").Value = "Chỉ cho phép nhập tối đa đến 4 số thập phân (Giá trị tồn cuối kỳ PHẢI ĐÚNG BẰNG  = Tồn đầu kỳ + Nhập trong kỳ - Xuất trong kỳ)";
                workSheet.GetCell("K2").Value = "Phần ghi chú ghi thông tin liên quan đến NPL (Như Số HĐ gia công hay Số lượng hủy ,Số lượng cung ứng ,vv....)";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public static bool IsFileLocked(string fileName)
        {
            FileStream stream = null;

            try
            {
                stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
        public static void CreateExcelTemplate_GoodItem_New()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "MẪU BÁO CÁO QUYẾT TOÁN TT39 TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("MẪU 15-BCQT-NVL-GSQL");
                Infragistics.Excel.Worksheet workSheet1 = workBook.Worksheets.Add("MẪU 15A-BCQT-SP-GSQL");

                workSheet.GetCell("A1").Value = "STT";
                workSheet.GetCell("B1").Value = "TÊN HÀNG HÓA";
                workSheet.GetCell("C1").Value = "MÃ HÀNG HÓA ";
                workSheet.GetCell("D1").Value = "ĐVT";
                workSheet.GetCell("E1").Value = "TỒN ĐẦU KỲ";
                workSheet.GetCell("F1").Value = "NHẬP TRONG KỲ";
                workSheet.GetCell("G1").Value = "TÁI XUẤT";
                workSheet.GetCell("H1").Value = "CHUYỂN MĐSD";
                workSheet.GetCell("I1").Value = "XUẤT KHÁC";
                workSheet.GetCell("J1").Value = "XUẤT TRONG KỲ";
                workSheet.GetCell("K1").Value = "TỒN CUỐI KỲ";
                workSheet.GetCell("L1").Value = "GHI CHÚ";
                workSheet.GetCell("M1").Value = "SỐ HỢP ĐỒNG GIA CÔNG";

                workSheet1.GetCell("A1").Value = "STT";
                workSheet1.GetCell("B1").Value = "TÊN HÀNG HÓA";
                workSheet1.GetCell("C1").Value = "MÃ HÀNG HÓA ";
                workSheet1.GetCell("D1").Value = "ĐVT";
                workSheet1.GetCell("E1").Value = "TỒN ĐẦU KỲ";
                workSheet1.GetCell("F1").Value = "NHẬP TRONG KỲ";
                workSheet1.GetCell("G1").Value = "TÁI XUẤT";
                workSheet1.GetCell("H1").Value = "CHUYỂN MĐSD";
                workSheet1.GetCell("I1").Value = "XUẤT KHÁC";
                workSheet1.GetCell("J1").Value = "XUẤT TRONG KỲ";
                workSheet1.GetCell("K1").Value = "TỒN CUỐI KỲ";
                workSheet1.GetCell("L1").Value = "GHI CHÚ";
                workSheet1.GetCell("M1").Value = "SỐ HỢP ĐỒNG GIA CÔNG";

                workSheet.MergedCellsRegions.Add(9, 0, 9, 11);
                workSheet.MergedCellsRegions.Add(10, 0, 10, 11);
                workSheet.MergedCellsRegions.Add(11, 0, 11, 11);
                workSheet.MergedCellsRegions.Add(12, 0, 12, 11);
                workSheet.MergedCellsRegions.Add(13, 0, 13, 11);
                workSheet.MergedCellsRegions.Add(14, 0, 14, 11);
                workSheet.MergedCellsRegions.Add(15, 0, 15, 11);
                workSheet.MergedCellsRegions.Add(16, 0, 16, 11);
                workSheet.MergedCellsRegions.Add(17, 0, 17, 11);
                workSheet.MergedCellsRegions.Add(18, 0, 18, 11);
                workSheet.MergedCellsRegions.Add(19, 0, 19, 11);
                workSheet.MergedCellsRegions.Add(20, 0, 20, 11);
                workSheet.MergedCellsRegions.Add(21, 0, 21, 11);
                workSheet.GetCell("A10").Value = "SỐ THỨ TỰ PHẢI TĂNG DẦN VÀ LIÊN TỤC . ĐỐI VỚI GC THÌ SỐ THỨ TỰ TĂNG DẦN CỦA TỪNG HĐGC";
                workSheet.GetCell("A11").Value = "TÊN HÀNG HÓA  BẮT BUỘC PHẢI NHẬP ";
                workSheet.GetCell("A12").Value = "MÃ HÀNG HÓA  BẮT BUỘC PHẢI NHẬP VÀ ĐÃ ĐƯỢC ĐĂNG KÝ";
                workSheet.GetCell("A13").Value = "ĐVT : NHẬP ĐVT THEO CHUẨN CỦA HQ  (ĐVT KHAI BÁO TRÊN TỜ KHAI VNACC) (VÍ DỤ : CÁI/CHIẾC THÌ NHẬP LÀ : PCE )";
                workSheet.GetCell("A14").Value = "TỒN ĐẦU KỲ : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY ). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A15").Value = "NHẬP TRONG KỲ : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A16").Value = "TÁI XUẤT : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A17").Value = "CHUYỂN MĐSD : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A18").Value = "XUẤT KHÁC : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A19").Value = "XUẤT TRONG KỲ : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A20").Value = "TỒN CUỐI KỲ : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN (KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A21").Value = "GHI CHÚ : PHẦN GHI CHÚ GHI THÔNG TIN LIÊN QUAN ĐẾN NPL (NHƯ SỐ HĐ GIA CÔNG HAY SỐ LƯỢNG HỦY ,SỐ LƯỢNG CUNG ỨNG ,VV....)";
                workSheet.GetCell("A22").Value = "SỐ HĐGC : CHỈ NHẬP ĐỐI VỚI BCQT LOẠI HÌNH GC KHI MUỐN ĐỌC EXCEL TỪ NHIỀU HỢP ĐỒNG GC VÀO";
                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public static void CreateExcelTemplate_TotalInventoryReport()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "BÁO CÁO CHỐT TỒN TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("BCCT");

                workSheet.GetCell("A1").Value = "MÃ HÀNG HÓA";
                workSheet.GetCell("B1").Value = "TÊN HÀNG HÓA";
                workSheet.GetCell("C1").Value = "ĐVT";
                workSheet.GetCell("D1").Value = "SỐ LƯỢNG TỒN KHO SỔ SÁCH";
                workSheet.GetCell("E1").Value = "SỐ LƯỢNG TỒN KHO THỰC TẾ";
                workSheet.GetCell("F1").Value = "SỐ HỢP ĐỒNG GIA CÔNG";

                workSheet.MergedCellsRegions.Add(9, 0, 9, 11);
                workSheet.MergedCellsRegions.Add(10, 0, 10, 11);
                workSheet.MergedCellsRegions.Add(11, 0, 11, 11);
                workSheet.MergedCellsRegions.Add(12, 0, 12, 11);
                workSheet.MergedCellsRegions.Add(13, 0, 13, 11);
                workSheet.MergedCellsRegions.Add(14, 0, 14, 11);

                workSheet.GetCell("A10").Value = "MÃ HÀNG HÓA  BẮT BUỘC PHẢI NHẬP VÀ ĐÃ ĐƯỢC ĐĂNG KÝ";
                workSheet.GetCell("A11").Value = "TÊN HÀNG HÓA  BẮT BUỘC PHẢI NHẬP";
                workSheet.GetCell("A12").Value = "ĐVT : NHẬP ĐVT THEO CHUẨN CỦA HQ  (ĐVT KHAI BÁO TRÊN TỜ KHAI VNACC) (VÍ DỤ : CÁI/CHIẾC THÌ NHẬP LÀ : PCE )";
                workSheet.GetCell("A13").Value = "TỒN KHO SỔ SÁCH : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY ). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A14").Value = "TỒN KHO THỰC TẾ : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 4 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY ). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                workSheet.GetCell("A15").Value = "SỐ HĐGC : CHỈ NHẬP ĐỐI VỚI BCQT LOẠI HÌNH GC KHI MUỐN ĐỌC EXCEL TỪ NHIỀU HỢP ĐỒNG GC VÀO";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        public static void CreateExcelTemplate_ScrapInformation()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "HÀNG HÓA TIÊU HỦY EXCEL TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("TTTH");

                workSheet.GetCell("A1").Value = "LOẠI HÀNG HÓA";
                workSheet.GetCell("B1").Value = "MÃ HÀNG HÓA";
                workSheet.GetCell("C1").Value = "TÊN HÀNG HÓA";
                workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                workSheet.GetCell("E1").Value = "SỐ LƯỢNG TIÊU HỦY";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        public static void CreateExcelTemplate_WareHouse()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "HÀNG HÓA PHIẾU XUẤT NHẬP KHO EXCEL TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("HANGHOA");

                workSheet.GetCell("A1").Value = "STT";
                workSheet.GetCell("B1").Value = "LOẠI HÀNG HÓA";
                workSheet.GetCell("C1").Value = "MÃ HÀNG HÓA";
                workSheet.GetCell("D1").Value = "TÊN HÀNG HÓA";
                workSheet.GetCell("E1").Value = "ĐƠN VỊ TÍNH";
                workSheet.GetCell("F1").Value = "NGUỒN NHẬP HOẶC MỤC ĐÍCH SỬ DỤNG";
                workSheet.GetCell("G").Value = "SỐ LƯỢNG DỰ KIẾN";
                workSheet.GetCell("H1").Value = "SỐ LƯỢNG THỰC";
                workSheet.GetCell("I1").Value = "MÃ ĐỊNH DANH";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public static void CreateExcelTemplate_NPLCHUATL()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "NGUYÊN PHỤ LIỆU CHƯA THANH LÝ TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "STT";
                workSheet.GetCell("B1").Value = "SỐ TỜ KHAI";
                workSheet.GetCell("C1").Value = "MÃ NPL";
                workSheet.GetCell("D1").Value = "TÊN NPL ";
                workSheet.GetCell("E1").Value = "LƯỢNG";
                workSheet.GetCell("F1").Value = "ĐVT";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #endregion
        #region Excel Template Hang Hoa VNACCS
        public static void CreateExcelTemplate_HangHoa_VANCCS()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "HangHoa_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Tên hàng";
                workSheet.GetCell("C1").Value = "Mã HS";
                workSheet.GetCell("D1").Value = "Số lượng";
                workSheet.GetCell("E1").Value = "Xuất xứ ";
                workSheet.GetCell("F1").Value = "Đơn vị tính";
                workSheet.GetCell("G1").Value = "Đơn giá";
                workSheet.GetCell("H1").Value = "Trị giá";
                workSheet.GetCell("I1").Value = "Biểu thuế";
                workSheet.GetCell("J1").Value = "Thuế suất";
                workSheet.GetCell("K1").Value = "Mã áp dụng mức thuế";
                workSheet.GetCell("L1").Value = "Mã miễn giảm";
                workSheet.GetCell("M1").Value = "Sô tiền miễn giảm";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public static void CreateExcelTemplate_HangHoaUpdate()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "UpdateHangHoa_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Mã HS";
                workSheet.GetCell("C1").Value = "Số hợp đồng";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #endregion
        #region Excel Template Danh sách Container đính kèm
        public static void CreateExcelTemplate_DSContainerDinhKem()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "DanhSachContainer_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Số vận đơn";
                workSheet.GetCell("B1").Value = "Số Container";
                workSheet.GetCell("C1").Value = "Số Seal";
                workSheet.GetCell("D1").Value = "Ghi chú";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #endregion
        // mau excel SXXK
        #region Excel Template SXXK

        public static void CreateExcelTemplate_SXXK(string type)
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = type + " EXCEL TEMPALTE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add(type);
                if (type == "NPL")
                {
                    workSheet.GetCell("A1").Value = "MÃ NPL";
                    workSheet.GetCell("B1").Value = "TÊN NPL";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                }
                else if (type == "SP")
                {
                    workSheet.GetCell("A1").Value = "MÃ SẢN PHẨM";
                    workSheet.GetCell("B1").Value = "TÊN SẢN PHẨM";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                }
                else if (type == "HangDuaRa")
                {
                    workSheet.GetCell("A1").Value = "MÃ SẢN PHẨM";
                    workSheet.GetCell("B1").Value = "TÊN SẢN PHẨM";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "MỤC ĐÍCH SỬ DỤNG (NẾU LÀ DOANH NGHIỆP CHẾ XUẤT)";
                }
                else if (type == "DM")
                {
                    workSheet.GetCell("A1").Value = "MÃ SẢN PHẨM";
                    workSheet.GetCell("B1").Value = "MÃ NPL";
                    workSheet.GetCell("C1").Value = "ĐỊNH MỨC SỬ DỤNG";
                    workSheet.GetCell("D1").Value = "TỶ LỆ HAO HỤT";
                    workSheet.GetCell("E1").Value = "MÃ ĐỊNH DANH CỦA LỆNH SX";

                    workSheet.MergedCellsRegions.Add(9, 0, 9, 11);
                    workSheet.MergedCellsRegions.Add(10, 0, 10, 11);
                    workSheet.MergedCellsRegions.Add(11, 0, 11, 11);
                    workSheet.MergedCellsRegions.Add(12, 0, 12, 11);
                    workSheet.MergedCellsRegions.Add(13, 0, 36, 11);

                    workSheet.GetCell("A10").Value = "MÃ SẢN PHẨM : BẮT BUỘC PHẢI NHẬP VÀ ĐÃ ĐƯỢC ĐĂNG KÝ";
                    workSheet.GetCell("A11").Value = "MÃ NPL : BẮT BUỘC PHẢI NHẬP";
                    workSheet.GetCell("A12").Value = "ĐỊNH MỨC SỬ DỤNG : BẮT BUỘC PHẢI NHẬP CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 8 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY )";
                    workSheet.GetCell("A13").Value = "TỶ LỆ HAO HỤT : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 1 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY ). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                    workSheet.GetCell("A14").Value = "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT : \n- ĐƯỢC NGẦM HIỂU ĐẠI DIỆN CHO ĐƠN HÀNG (PO) XUẤT KHẨU SẢN PHẨM CHO ĐỐI TÁC (ÁP DỤNG TRONG LOẠI HÌNH SXXK, GIA CÔNG, CHẾ XUẤT). MÃ ĐỊNH DANH LỆNH SẢN XUẤT DO DOANH NGHIỆP CẤP VÀ PHẢI LÀ DUY NHẤT TRÊN TOÀN HỆ THỐNG."+

"\n- MỘT LỆNH SẢN XUẤT CÓ 1 HOẶC NHIỀU MÃ SẢN PHẨM. TƯƠNG ỨNG MÃ SẢN PHẨM NÀY TỒN TẠI TRONG MỘT HAY NHIỀU LỆNH SẢN XUẤT KHÁC NHAU, VÀ CÓ THỂ CÓ ĐỊNH MỨC KHÁC NHAU TẠI MỖI THỜI ĐIỂM SẢN XUẤT."+

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT LÀ CHỈ TIÊU KHAI BÁO BẮT BUỘC CỦA ĐỊNH MỨC SẢN PHẨM."+

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT CŨNG LÀ CHỈ TIÊU KHAI BÁO BẮT BUỘC CỦA THÔNG TIN PHIẾU NHẬP KHO XUẤT KHO SẢN PHẨM TRONG KỲ BÁO CÁO QUYẾT TOÁN."+

"\n- THEO TT39, TẠI MỖI THỜI ĐIỂM KHÁC NHAU, MỘT MÃ SẢN PHẨM CÓ THỂ CÓ NHIỀU ĐỊNH MỨC KHÁC NHAU. VẬY LỆNH SẢN XUẤT SẼ ĐÓNG VAI TRÒ ĐỂ PHÂN BIỆT ĐỊNH MỨC GIỮA 2 HOẶC NHIỀU ĐỊNH MỨC CỦA MỘT MÃ SẢN PHẨM."+

"\nVÍ DỤ:"+
"\n+ KỲ BÁO CÁO QUYẾT TOÁN 01/01/2017 ĐẾN 31/12/2017:"+
"\nLỆNH SẢN XUẤT [LSX0001] CÓ 100 MẶT HÀNG XUẤT TƯƠNG ỨNG 100 MÃ SẢN PHẨM. TRONG ĐÓ MÃ SẢN PHẨM SP001 CÓ ĐỊNH MỨC TIÊU HAO NPLA LÀ 1."+
"\n==> ĐẾN GẦN NGÀY BÁO CÁO QUYẾT TOÁN NĂM 2017: KHAI BÁO ĐỊNH MỨC THỰC TẾ CỦA 100 MẶT HÀNG TƯƠNG ỨNG VỚI LSX0001"+

"\n+ KỲ BÁO CÁO QUYẾT TOÁN 01/01/2018 ĐẾN 31/12/2018:"+
"\nMỘT SỐ MÁY MÓC THIẾT BỊ HƯ HỎNG HOẶC LỖI KỸ THUẬT DẪN ĐẾN TIÊU HAO ĐỊNH MỨC NPL NHIỀU HƠN(MÃ SẢN PHẨM SP001 TIÊU HAO ĐỊNH MỨC NPLA LÀ 1.5). NHƯ VẬY [LSX0002] ĐƯỢC TẠO RA TƯƠNG ỨNG VỚI ĐỊNH MỨC MỚI CỦA MÃ SẢN PHẨM SP001"+
"\n==> ĐẾN GẦN NGÀY BÁO CÁO QUYẾT TOÁN NĂM 2018: KHAI BÁO ĐỊNH MỨC THỰC TẾ CỦA MÃ SP001 TƯƠNG ỨNG VỚI LSX002"+

"\n***CÁC ĐỊNH MỨC CỦA SẢN PHẨM CŨ ĐÃ KHAI TỪ NĂM 2017, SANG NĂM 2018 KHÔNG THAY ĐỔI VÀ KHÔNG KHAI BÁO LẠI THÌ MẶC ĐỊNH ĐƯỢC HIỂU ĐỊNH MỨC CÁC MÃ SẢN PHẨM 2018 SẼ GIỐNG NHƯ 2017"+

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT LÀ CHỈ TIÊU KHAI BÁO CỦA ĐỊNH MỨC SẢN PHẨM, CŨNG LÀ CHỈ TIÊU KHAI BÁO PHIẾU NHẬP KHO SẢN PHẨM VÀ PHIẾU XUẤT KHO SẢN PHẨM ==> DỰA VÀO MÃ ĐỊNH DANH LỆNH SẢN XUẤT VÀ MÃ SẢN PHẨM, HẢI QUAN SẼ XÁC ĐỊNH ĐƯỢC ĐỊNH MỨC SẢN PHẨM ĐƯỢC SỬ DỤNG TƯƠNG ỨNG VỚI PHIẾU NHẬP XUẤT KHO TẠI MỖI THỜI ĐIỂM KHÁC NHAU TRONG KỲ BÁO CÁO QUYẾT TOÁN.";

                    workSheet.GetCell("A14").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Justify;
                }
                else if (type == "HangHoa")
                {
                    workSheet.GetCell("A1").Value = "MÃ HÀNG";
                    workSheet.GetCell("B1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("C1").Value = "XUẤT XỨ";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "THUẾ SUẤT XNK";
                }

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion
        // Mau Excel GC
        #region Excel Template GC

        public static void CreateExcelTemplate_GC(string type)
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = type + "EXCEL TEMPLATE.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    if (IsFileLocked(filePath))
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    else
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");
                if (type == "NPL_HD")
                {
                    workSheet.GetCell("A1").Value = "MÃ NPL";
                    workSheet.GetCell("B1").Value = "TÊN NPL";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("F1").Value = "ĐƠN GIÁ";
                }
                else if (type == "SP_HD")
                {
                    workSheet.GetCell("A1").Value = "MÃ SẢN PHẨM";
                    workSheet.GetCell("B1").Value = "TÊN SẢN PHẨM";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("F1").Value = "ĐƠN GIÁ";
                    workSheet.GetCell("G1").Value = "LOẠI SPGC";
                }
                else if (type == "TB_HD")
                {
                    workSheet.GetCell("A1").Value = "MÃ THIẾT BỊ";
                    workSheet.GetCell("B1").Value = "TÊN THIẾT BỊ";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "XUẤT XỨ";
                    workSheet.GetCell("F1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("G1").Value = "ĐƠN GIÁ";
                    workSheet.GetCell("H1").Value = "NGUYÊN TỆ";
                    workSheet.GetCell("I1").Value = "TÌNH TRẠNG";

                }
                else if (type == "HangHoa")
                {
                    workSheet.GetCell("A1").Value = "MÃ HÀNG";
                    workSheet.GetCell("B1").Value = "TÊN HÀNG";
                    workSheet.GetCell("C1").Value = "MÃ HS";
                    workSheet.GetCell("D1").Value = "ĐƠN VỊ TÍNH";
                    workSheet.GetCell("E1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("F1").Value = "ĐƠN GIÁ";
                    workSheet.GetCell("G1").Value = "XUẤT XỨ";
                    workSheet.GetCell("H1").Value = "TGTT";
                    workSheet.GetCell("I1").Value = "TS XNK";
                }
                else if (type == "DM")
                {
                    workSheet.GetCell("A1").Value = "MÃ SẢN PHẨM";
                    workSheet.GetCell("B1").Value = "MÃ NPL";
                    workSheet.GetCell("C1").Value = "ĐỊNH MỨC SỬ DỤNG";
                    workSheet.GetCell("D1").Value = "TỶ LỆ HAO HỤT";
                    workSheet.GetCell("E1").Value = "NPL TỰ CUNG ỨNG";
                    workSheet.GetCell("F1").Value = "MÃ ĐỊNH DANH CỦA LỆNH SX";

                    workSheet.MergedCellsRegions.Add(9, 0, 9, 11);
                    workSheet.MergedCellsRegions.Add(10, 0, 10, 11);
                    workSheet.MergedCellsRegions.Add(11, 0, 11, 11);
                    workSheet.MergedCellsRegions.Add(12, 0, 12, 11);
                    workSheet.MergedCellsRegions.Add(13, 0, 36, 11);

                    workSheet.GetCell("A10").Value = "MÃ SẢN PHẨM  BẮT BUỘC PHẢI NHẬP VÀ ĐÃ ĐƯỢC ĐĂNG KÝ";
                    workSheet.GetCell("A11").Value = "MÃ NPL BẮT BUỘC PHẢI NHẬP";
                    workSheet.GetCell("A12").Value = "ĐỊNH MỨC SỬ DỤNG : BẮT BUỘC PHẢI NHẬP CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 8 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY )";
                    workSheet.GetCell("A13").Value = "TỶ LỆ HAO HỤT : CHỈ CHO PHÉP NHẬP TỐI ĐA ĐẾN 1 SỐ THẬP PHÂN . KHÔNG THỰC HIỆN PHÉP TÍNH TRÊN CỘT NÀY ). NẾU KHÔNG CÓ GIÁ TRỊ THÌ NHẬP VÀO LÀ : 0";
                    workSheet.GetCell("A14").Value = "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT : \n- ĐƯỢC NGẦM HIỂU ĐẠI DIỆN CHO ĐƠN HÀNG (PO) XUẤT KHẨU SẢN PHẨM CHO ĐỐI TÁC (ÁP DỤNG TRONG LOẠI HÌNH SXXK, GIA CÔNG, CHẾ XUẤT). MÃ ĐỊNH DANH LỆNH SẢN XUẤT DO DOANH NGHIỆP CẤP VÀ PHẢI LÀ DUY NHẤT TRÊN TOÀN HỆ THỐNG." +

"\n- MỘT LỆNH SẢN XUẤT CÓ 1 HOẶC NHIỀU MÃ SẢN PHẨM. TƯƠNG ỨNG MÃ SẢN PHẨM NÀY TỒN TẠI TRONG MỘT HAY NHIỀU LỆNH SẢN XUẤT KHÁC NHAU, VÀ CÓ THỂ CÓ ĐỊNH MỨC KHÁC NHAU TẠI MỖI THỜI ĐIỂM SẢN XUẤT." +

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT LÀ CHỈ TIÊU KHAI BÁO BẮT BUỘC CỦA ĐỊNH MỨC SẢN PHẨM." +

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT CŨNG LÀ CHỈ TIÊU KHAI BÁO BẮT BUỘC CỦA THÔNG TIN PHIẾU NHẬP KHO XUẤT KHO SẢN PHẨM TRONG KỲ BÁO CÁO QUYẾT TOÁN." +

"\n- THEO TT39, TẠI MỖI THỜI ĐIỂM KHÁC NHAU, MỘT MÃ SẢN PHẨM CÓ THỂ CÓ NHIỀU ĐỊNH MỨC KHÁC NHAU. VẬY LỆNH SẢN XUẤT SẼ ĐÓNG VAI TRÒ ĐỂ PHÂN BIỆT ĐỊNH MỨC GIỮA 2 HOẶC NHIỀU ĐỊNH MỨC CỦA MỘT MÃ SẢN PHẨM." +

"\nVÍ DỤ:" +
"\n+ KỲ BÁO CÁO QUYẾT TOÁN 01/01/2017 ĐẾN 31/12/2017:" +
"\nLỆNH SẢN XUẤT [LSX0001] CÓ 100 MẶT HÀNG XUẤT TƯƠNG ỨNG 100 MÃ SẢN PHẨM. TRONG ĐÓ MÃ SẢN PHẨM SP001 CÓ ĐỊNH MỨC TIÊU HAO NPLA LÀ 1." +
"\n==> ĐẾN GẦN NGÀY BÁO CÁO QUYẾT TOÁN NĂM 2017: KHAI BÁO ĐỊNH MỨC THỰC TẾ CỦA 100 MẶT HÀNG TƯƠNG ỨNG VỚI LSX0001" +

"\n+ KỲ BÁO CÁO QUYẾT TOÁN 01/01/2018 ĐẾN 31/12/2018:" +
"\nMỘT SỐ MÁY MÓC THIẾT BỊ HƯ HỎNG HOẶC LỖI KỸ THUẬT DẪN ĐẾN TIÊU HAO ĐỊNH MỨC NPL NHIỀU HƠN(MÃ SẢN PHẨM SP001 TIÊU HAO ĐỊNH MỨC NPLA LÀ 1.5). NHƯ VẬY [LSX0002] ĐƯỢC TẠO RA TƯƠNG ỨNG VỚI ĐỊNH MỨC MỚI CỦA MÃ SẢN PHẨM SP001" +
"\n==> ĐẾN GẦN NGÀY BÁO CÁO QUYẾT TOÁN NĂM 2018: KHAI BÁO ĐỊNH MỨC THỰC TẾ CỦA MÃ SP001 TƯƠNG ỨNG VỚI LSX002" +

"\n***CÁC ĐỊNH MỨC CỦA SẢN PHẨM CŨ ĐÃ KHAI TỪ NĂM 2017, SANG NĂM 2018 KHÔNG THAY ĐỔI VÀ KHÔNG KHAI BÁO LẠI THÌ MẶC ĐỊNH ĐƯỢC HIỂU ĐỊNH MỨC CÁC MÃ SẢN PHẨM 2018 SẼ GIỐNG NHƯ 2017" +

"\n- MÃ ĐỊNH DANH LỆNH SẢN XUẤT LÀ CHỈ TIÊU KHAI BÁO CỦA ĐỊNH MỨC SẢN PHẨM, CŨNG LÀ CHỈ TIÊU KHAI BÁO PHIẾU NHẬP KHO SẢN PHẨM VÀ PHIẾU XUẤT KHO SẢN PHẨM ==> DỰA VÀO MÃ ĐỊNH DANH LỆNH SẢN XUẤT VÀ MÃ SẢN PHẨM, HẢI QUAN SẼ XÁC ĐỊNH ĐƯỢC ĐỊNH MỨC SẢN PHẨM ĐƯỢC SỬ DỤNG TƯƠNG ỨNG VỚI PHIẾU NHẬP XUẤT KHO TẠI MỖI THỜI ĐIỂM KHÁC NHAU TRONG KỲ BÁO CÁO QUYẾT TOÁN.";

                    workSheet.GetCell("A14").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Justify;
                }
                else if (type == "HangHoa_CT")
                {
                    workSheet.GetCell("A1").Value = "MÃ HÀNG";
                    workSheet.GetCell("B1").Value = "SỐ LƯỢNG";
                    workSheet.GetCell("C1").Value = "ĐƠN GIÁ";
                    workSheet.GetCell("D1").Value = "XUẤT XỨ";
                }

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion

        public static void SyncConfig(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                newCfg.Load(newPath);

                System.Xml.XmlDocument oldCfg = new System.Xml.XmlDocument();
                oldCfg.Load(oldPath);

                XmlNode oldNode = oldCfg.SelectSingleNode("configuration/connectionStrings/add");
                XmlNode newNode = newCfg.SelectSingleNode("configuration/connectionStrings/add");
                if (oldNode != null && newNode != null)
                {
                    newNode.Attributes["connectionString"].Value = oldNode.Attributes["connectionString"].Value;
                }
                XmlNodeList newNodes = newCfg.SelectNodes("configuration/applicationSettings");
                XmlNodeList oldNodes = oldCfg.SelectNodes("configuration/applicationSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    newNode = newNodes[0].ChildNodes[i];

                    for (int j = 0; j < newNode.ChildNodes.Count; j++)
                    {
                        XmlNode newNodeChild = newNode.ChildNodes[j];
                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNode.Name + "/" + newNodeChild.Name + "[@name='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            if (newNodeChild.SelectSingleNode("//value") != null && oldNodeChild.SelectSingleNode("//value") != null)
                                newNodeChild.SelectSingleNode("//value").InnerText = oldNodeChild.SelectSingleNode("//value").InnerText;
                            else
                                newNodeChild.InnerText = oldNodeChild.InnerText;
                        }
                    }
                }
                newNodes = newCfg.SelectNodes("configuration/appSettings");
                oldNodes = oldCfg.SelectNodes("configuration/appSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    XmlNode newNodeChild = newNodes[0].ChildNodes[i];
                    if (newNodeChild.Name == "add")
                    {
                        if (newNodeChild.Attributes[0].Value == "WS_SyncData")
                        {
                            continue;
                        }

                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNodeChild.Name + "[@key='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            for (int k = 0; k < newNodeChild.Attributes.Count; k++)
                            {
                                try
                                {
                                    string name = newNodeChild.Attributes[k].Name.Trim();
                                    newNodeChild.Attributes[name].Value = oldNodeChild.Attributes[name].Value;
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                            }
                        }
                    }
                }
                newCfg.Save(newPath);
            }
        }
        public static void AfterUnzip(string folderConfig)
        {
            try
            {
                if (File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml.old") && File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml"))
                {
                    XmlDocument configOld = new XmlDocument();
                    configOld.Load(folderConfig + "\\ConfigDoanhNghiep.xml.old");
                    XmlDocument configNew = new XmlDocument();

                    configNew.Load(folderConfig + "\\ConfigDoanhNghiep.xml");
                    foreach (XmlNode node in configNew.GetElementsByTagName("Config"))
                    {
                        XmlNode nodeOld = configOld.SelectSingleNode("/Configs/Config[Key='" + node["Key"].InnerText + "']");
                        if (nodeOld != null)
                            node["Value"].InnerText = nodeOld["Value"].InnerText;
                    }
                    configNew.Save(folderConfig + "\\ConfigDoanhNghiep.xml");
                }
                //SyncConfig(folderConfig + "SOFTECH.ECS.TQDT.KD.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.KD.exe.Config");
                //SyncConfig(folderConfig + "SOFTECH.ECS.TQDT.SXXK.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.SXXK.exe.Config");
                //SyncConfig(folderConfig + "SOFTECH.ECS.TQDT.GC.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.GC.exe.Config");

                //Updated by Hungtq, 14/09/2012.
                if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old")
                    && File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                    SyncConfig(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            if (File.Exists(folderConfig + "Config.xml.old") && File.Exists(folderConfig + "Config.xml"))

                try
                {
                    XmlDocument docOld = new XmlDocument();
                    XmlDocument docNew = new XmlDocument();
                    docOld.Load(folderConfig + "Config.xml.old");
                    docNew.Load(folderConfig + "Config.xml");
                    XmlNode node = docOld.SelectSingleNode("Root/Type");
                    XmlNode nodeNew = docNew.SelectSingleNode("Root/Type");
                    nodeNew.InnerText = node.InnerText;
                    docNew.Save(folderConfig + "Config.xml");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            if (!File.Exists(folderConfig + "Config.xml")) RollbackFile(folderConfig + "Config.xml.old", folderConfig + "Config.xml");
            if (!File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml")) RollbackFile(folderConfig + "\\ConfigDoanhNghiep.xml.old", folderConfig + "\\ConfigDoanhNghiep.xml");
            //if (!File.Exists(folderConfig + "SOFTECH.ECS.TQDT.SXXK.exe.Config")) RollbackFile(folderConfig + "SOFTECH.ECS.TQDT.SXXK.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.SXXK.exe.Config");
            //if (!File.Exists(folderConfig + "SOFTECH.ECS.TQDT.GC.exe.Config")) RollbackFile(folderConfig + "SOFTECH.ECS.TQDT.GC.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.GC.exe.Config");
            //if (!File.Exists(folderConfig + "SOFTECH.ECS.TQDT.KD.exe.Config")) RollbackFile(folderConfig + "SOFTECH.ECS.TQDT.KD.exe.Config.old", folderConfig + "SOFTECH.ECS.TQDT.KD.exe.Config");

            if (!File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile)) RollbackFile(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            //Update date release
            if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                SaveNodeXmlAppSettings(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, "LastUpdated", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));

        }
        public static void RollbackFile(string oldFile, string newFile)
        {
            if (File.Exists(oldFile))
                System.IO.File.Move(oldFile, newFile);
        }

        /// <summary>
        /// Đồng bộ lại thông tin từ file cấu hình XMl sang file cấu hình EXE của chương trình.
        /// </summary>
        /// <param name="phanHe">KD, GC, SXXK</param>
        /// <returns></returns>
        public static bool AupdateConfigXMLToExe(string phanHe)
        {
            try
            {
                string pathConfigXML = AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml";
                string pathConfigEXE = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.Replace(".vshost", "");

                if (File.Exists(pathConfigXML) && File.Exists(pathConfigEXE))
                {
                    XmlDocument configXML = new XmlDocument();
                    configXML.Load(pathConfigXML);

                    //Service khai bao
                    SaveNodeXmlAppSettings("WS_URL", GetConfig(configXML, "WS", ""));
                    SaveNodeXmlAppSettings("DiaChiWS", GetConfig(configXML, "WS", ""));
                    //Database
                    string S = GetConfig(configXML, "Server", ".\\ECSEXPRESS");
                    string D = GetConfig(configXML, "Database", string.Format("ECS_TQDT_{0}_V4", phanHe));
                    string U = GetConfig(configXML, "UserName", "sa");
                    string P = GetConfig(configXML, "Password", "123456");
                    SaveNodeXmlAppSettings("ServerName", S);
                    SaveNodeXmlAppSettings("DATABASE_NAME", D);
                    SaveNodeXmlAppSettings("user", U);
                    SaveNodeXmlAppSettings("pass", P);
                    //Database connectionstring
                    string concatString = "";
                    concatString += "Server=" + S + ";";
                    concatString += "Database=" + D + ";";
                    concatString += "uid=" + U + ";";
                    concatString += "pwd=" + P + ";";
                    SaveNodeXmlConnectionStrings(concatString);
                    //Thong tin Hai quan
                    SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", GetConfig(configXML, "MaCucHQ", ""));
                    SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", GetConfig(configXML, "TenCucHQ", ""));
                    SaveNodeXmlAppSettings("MA_HAI_QUAN", GetConfig(configXML, "MaChiCucHQ", ""));
                    SaveNodeXmlAppSettings("TEN_HAI_QUAN", GetConfig(configXML, "TenChiCucHQ", ""));
                    SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", GetConfig(configXML, "TenNganChiCucHQ", ""));
                    //Thong tin Doanh nghiep
                    SaveNodeXmlAppSettings("MA_DON_VI", GetConfig(configXML, "MaDoanhNghiep", ""));
                    SaveNodeXmlAppSettings("TEN_DON_VI", GetConfig(configXML, "TenDoanhNghiep", ""));
                    SaveNodeXmlAppSettings("DIA_CHI", GetConfig(configXML, "DiaChiDoanhNghiep", ""));
                    SaveNodeXmlAppSettings("MailDoanhNghiep", GetConfig(configXML, "MailDoanhNghiep", ""));
                }

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }

        public static void SaveMessage()
        {
            throw new NotImplementedException();
        }

        public static bool ServiceExists(string url, bool throwExceptions, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;

                // try accessing the web service directly via it's URL
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 15000;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception("Error locating web service");
                }

                // try getting the WSDL?
                // asmx lets you put "?wsdl" to make sure the URL is a web service
                // could parse and validate WSDL here

            }
            catch (WebException ex)
            {
                // decompose 400- codes here if you like
                errorMessage = string.Format("Error testing connection to web service at \"{0}\":\r\n{1}", url, ex);
                Logger.LocalLogger.Instance().WriteMessage(errorMessage, new Exception(""));
                if (throwExceptions)
                    throw new Exception(errorMessage, ex);
                return false;
            }
            catch (Exception ex)
            {
                errorMessage = string.Format("Error testing connection to web service at \"{0}\":\r\n{1}", url, ex);
                Logger.LocalLogger.Instance().WriteMessage(errorMessage, new Exception(""));
                if (throwExceptions)
                    throw new Exception(errorMessage, ex);
                return false;
            }

            return true;
        }

        public static string GetLocalIPAddress()
        {
            string localIP = "127.0.0.1";
            try
            {
                System.Net.IPHostEntry host;
                host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                foreach (System.Net.IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }

            return localIP;
        }

        ///
        /// Tạo shortcut
        ///
        /// Tập tin bạn muốn tạo shortcut
        /// Đường dẫn đến tập tin shortcut bao gồm cả phần đuôi (.lnk)
        /// Chuỗi mô tả shortcut
        /// Tham số dòng lệnh
        /// Chuỗi phím tắt cho shortcut, ví dụ "Ctrl+Alt+A"
        /// Tham số phần "Start in" của shortcut
        /// Đường dẫn đến icon cần gán cho shortcut
        public static void CreateShortcut(string targetPath, string shortcutFile, string description, string arguments, string hotKey, string workingDirectory, string iconLocation)
        {
            // Kiểm tra 2 tham số cần thiết của hàm
            if (String.IsNullOrEmpty(targetPath))
                throw new ArgumentNullException("targetPath");
            if (String.IsNullOrEmpty(shortcutFile))
                throw new ArgumentNullException("shortcutFile");

            // Tạo một thể hiện của lớp WshShellClass
            IWshRuntimeLibrary.WshShellClass wshShell = new IWshRuntimeLibrary.WshShellClass();

            // Tạo đối tượng shortcut
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)wshShell.CreateShortcut(shortcutFile);

            // Gán các thuộc tính cho shortcut
            shortcut.TargetPath = targetPath;
            shortcut.Description = description;

            if (!String.IsNullOrEmpty(arguments))
                shortcut.Arguments = arguments;
            if (!String.IsNullOrEmpty(hotKey))
                shortcut.Hotkey = hotKey;
            if (!String.IsNullOrEmpty(workingDirectory))
                shortcut.WorkingDirectory = workingDirectory;
            if (!String.IsNullOrEmpty(iconLocation))
                shortcut.IconLocation = iconLocation;

            // Lưu shortcut
            shortcut.Save();
        }
    }

    public class GenericListToDataTable
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        private GenericListToDataTable()
        { }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name=”T”>Custome Class </typeparam>
        /// <param name=”lst”>List Of The Custome Class</param>
        /// <returns> Return the class datatbl </returns>
        public static DataTable ConvertTo<T>(IList<T> lst)
        {
            //create DataTable Structure
            DataTable tbl = CreateTable<T>();
            Type entType = typeof(T);

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            //get the list item and add into the list
            foreach (T item in lst)
            {
                DataRow row = tbl.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                tbl.Rows.Add(row);
            }

            return tbl;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name=”T”>Custome Class</typeparam>
        /// <returns></returns>
        public static DataTable CreateTable<T>()
        {
            //T –> ClassName
            Type entType = typeof(T);

            //set the datatable name as class name
            DataTable tbl = new DataTable(entType.Name);

            //get the property list
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (PropertyDescriptor prop in properties)
            {
                //add property as column
                tbl.Columns.Add(prop.Name, prop.PropertyType);
            }

            return tbl;
        }
    }

    public class ThongBao
    {
        public const string APPLICATION_ERROR_01Param = "Có lỗi trong quá trình xử lý thông tin:\r\n{0}";
        public const string APPLICATION_ERROR_03Param = "Có lỗi trong quá trình xử lý thông tin. Xin vui lòng liên hệ nhà cung cấp để được hướng dẫn.\r\n\nThông tin liên hệ:\r\n-Người liên hệ: {0}\r\n-Điện thoại: {1}\r\n-Email: {2}";
        public const string APPLICATION_ERROR_LOADING_01Param = "Có lỗi trong quá trình tải dữ liệu:\r\n{0}";
        public const string APPLICATION_NOT_CONNECT_INTERNET_0Param = "Không kết nối được dữ liệu.\r\n\nXin vui lòng liên hệ nhà cung cấp để được hướng dẫn.";
        public const string APPLICATION_SEND_EMAIL_SUCCESS_0Param = "Thông tin đã được gửi đến nhà cung cấp thành công.\r\n\nXin vui lòng chờ email phản hồi để được hướng dẫn.";
        public const string APPLICATION_SEND_EMAIL_FAIL_03Param = "Gửi thông tin không thành công.\r\n\nXin vui lòng liên hệ nhà cung cấp để được hướng dẫn.\r\n\nThông tin liên hệ:\r\n-Người liên hệ: {0}\r\n-Điện thoại: {1}\r\n-Email: {2}";
        public const string APPLICATION_SAVE_DATA_SUCCESS_0Param = "Lưu thông tin thành công.";
        public const string APPLICATION_SAVE_DATA_FAIL_0Param = "Lưu thông tin thất bại.";
    }
}
