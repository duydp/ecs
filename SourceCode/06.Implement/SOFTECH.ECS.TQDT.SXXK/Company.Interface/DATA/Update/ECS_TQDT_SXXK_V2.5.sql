
/****** Object:  StoredProcedure [dbo].[p_ToKhaiSua_KiemTra_ThongTinHang]    Script Date: 07/06/2012 14:03:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_ToKhaiSua_KiemTra_ThongTinHang]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_ToKhaiSua_KiemTra_ThongTinHang]
GO


/****** Object:  StoredProcedure [dbo].[p_ToKhaiSua_KiemTra_ThongTinHang]    Script Date: 07/06/2012 14:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[p_ToKhaiSua_KiemTra_ThongTinHang]
(
	@NamDK int,
	@SoTK bigint,
	@MaLH varchar(10)
)
as

begin

--Create by Hungtq, 06/07/2012. So sanh lay thong tin hang to khai bi thay doi cua
--to khai da dang ky va to khai sua doi bo sung.

select b.MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NamDangKy, b.MaPhu from 
(
select t.SoToKhai, t.MaLoaiHinh, year(t.NgayDangKy) as NamDangKy, h.MaPhu, t.MaHaiQuan, t.MaDoanhNghiep
from t_KDT_HangMauDich h 
	inner join t_KDT_ToKhaiMauDich t on h.TKMD_ID = t.ID
) as a 
full join
(
select h.SoToKhai, h.MaLoaiHinh, h.NamDangKy, h.MaPhu, h.MaHaiQuan
from t_sxxk_HangMauDich h 
) as b
on a.SoToKhai = b.SoToKhai and a.MaLoaiHinh = b.MaLoaiHinh and a.NamDangKy = b.NamDangKy
	and a.MaHaiQuan = b.MaHaiQuan and a.MaPhu = b.MaPhu
where b.NamDangKy = @NamDK and a.MaPhu is null and b.SoToKhai = @SoTK and b.MaLoaiHinh = @MaLH

end


GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.5', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.5', getdate(), null)
	end 


