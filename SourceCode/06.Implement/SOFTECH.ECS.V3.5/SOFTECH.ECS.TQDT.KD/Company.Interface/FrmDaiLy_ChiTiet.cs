﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;

namespace Company.Interface
{
    public partial class FrmDaiLy_ChiTiet :BaseForm
    {
        public string MaDN;
        public int trangthai=0;
        public DataSet dataset = new DataSet();
        public static SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public FrmDaiLy_ChiTiet()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool status = true;
            if (trangthai == 0)
            {
                try
                {
                    //select ma dai ly và mã doanh nghiep

                  
                    dataset = Company.KDT.SHARE.Components.HeThongPhongKhai.SelectDynamic("Role=1 and key_config ='DIA_CHI'", "madoanhnghiep");
                    
                    //Check service

                    DataTable dt = dataset.Tables[0];
                    string madaily = dt.Rows[0]["MaDoanhNghiep"].ToString();
                    status = true;
                    // tam thoi khong dung do service chua online
                    //ISyncData wsSyncData = WebService.SyncService();
                    //MaDN = txtMaDN.Text;
                    //string msgSend = wsSyncData.CheckDaiLy(madaily, MaDN);
                    //if (msgSend == string.Empty)
                    //{
                    //    status = true;
                    //}
                    //else
                    //{
                    //    ShowMessage(msgSend, false);
                    //    status = false;
                    //}
                }
                catch(Exception ex)
                {
                    ShowMessage("Lỗi kiểm tra"+ex,false);
                    status = false;
                }
            }
            //----------
            if (status == true)
            {
                bool isValidate = false;
                isValidate = ValidateControl.ValidateNull(txtMaDN, errorProvider, "Mã Doanh Nghiệp");
                isValidate &= ValidateControl.ValidateNull(txtTenDN, errorProvider, "Tên Doanh nghiệp");
                isValidate &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, "Địa chỉ doanh nghiệp");
                isValidate &= ValidateControl.ValidateNull(txtDienThoaiDN, errorProvider, "Điện thoại doanh nghiệp");
                isValidate &= ValidateControl.ValidateNull(txtNguoiLienHeDN, errorProvider, "Người liên hệ");
                isValidate &= ValidateControl.ValidateNull(txtMailDoanhNghiep, errorProvider, "Mail doanh nghiệp");
                isValidate &= ValidateControl.ValidateNull(txtMaCuc, errorProvider, "Mã cục Hải Quan");
                isValidate &= ValidateControl.ValidateNull(txtTenCucHQ, errorProvider, "Tên cục Hải Quan");
                isValidate &= ValidateControl.ValidateNull(txtTenNganCucHQ, errorProvider, "Tên ngắn cục Hải Quan");
                if (!isValidate)
                    return;

                string maDN = txtMaDN.Text;
                string tenDN = txtTenDN.Text;
                string diaChi = txtDiaChi.Text;
                string nguoiLH = txtNguoiLienHeDN.Text;
                string eMail = txtMailDoanhNghiep.Text;
                string maHQ = donViHaiQuanControl.Ma.ToString();
                string mailHQ = txtMailHaiQuan.Text;
                string midDN = txtMaMid.Text;
                string maCuc = txtMaCuc.Text;
                string tenCuc = donViHaiQuanControl.cbTen.Text;
                string tenHQ = txtTenCucHQ.Text;
                string tenNganHQ = txtTenNganCucHQ.Text;
                string dienthoai = txtDienThoaiDN.Text;
                string sofax = txtSoFaxDN.Text;



                //inser update du lieu
                try
                {
                    Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                    htpk.MaDoanhNghiep = maDN;
                    htpk.TenDoanhNghiep = tenDN;

                    htpk.Key_Config = "CauHinh";
                    htpk.Value_Config = "1";
                    htpk.InsertUpdate();
                    htpk.Key_Config = "DIA_CHI";
                    htpk.Value_Config = diaChi;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "ĐIEN_THOAI";
                    htpk.Value_Config = dienthoai;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "SO_FAX";
                    htpk.Value_Config = sofax;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "NGUOI_LIEN_HE";
                    htpk.Value_Config = nguoiLH;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "MA_CUC_HAI_QUAN";
                    htpk.Value_Config = maCuc;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "MA_HAI_QUAN";
                    htpk.Value_Config = maHQ;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "MailDoanhNghiep";
                    htpk.Value_Config = eMail;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "MailHaiQuan";
                    htpk.Value_Config = mailHQ;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "MaMID";
                    htpk.Value_Config = midDN;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "TEN_CUC_HAI_QUAN";
                    htpk.Value_Config = tenCuc;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "TEN_HAI_QUAN_NGAN";
                    htpk.Value_Config = tenNganHQ;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "TEN_HAI_QUAN";
                    htpk.Value_Config = tenHQ;
                    htpk.InsertUpdate();

                    ShowMessage("Lưu thành công! ", false);
                    this.Close();

                }

                catch (Exception ex)
                {
                    ShowMessageTQDT("Lưu không thành công. /n/n" + ex, false);
                }
            }

        }
        private void donViHaiQuanControl_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(donViHaiQuanControl.Ma)) return;
            string maCuc = donViHaiQuanControl.Ma.Substring(1, 2);
            txtMaCuc.Text = maCuc;

            Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load("Z" + maCuc + "Z");

            txtTenCucHQ.Text = objDonViHQ != null ? objDonViHQ.Ten : "";
            txtTenNganCucHQ.Text = donViHaiQuanControl.Ten;
            txtMailHaiQuan.Text = "";

           
        }

        private void FrmDaiLy_ChiTiet_Load(object sender, EventArgs e)
        {

            if (trangthai == 1)
            {
                Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                txtMaDN.Text = MaDN;
                txtTenDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CauHinh").TenDoanhNghiep.ToString();
                txtDiaChi.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI").Value_Config.ToString();
                txtDienThoaiDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "ĐIEN_THOAI").Value_Config.ToString();
                txtSoFaxDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SO_FAX").Value_Config.ToString();
                txtNguoiLienHeDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "NGUOI_LIEN_HE").Value_Config.ToString();
                txtMailDoanhNghiep.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailDoanhNghiep").Value_Config.ToString();

                txtMaCuc.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_CUC_HAI_QUAN").Value_Config.ToString();
                donViHaiQuanControl.Ma = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_HAI_QUAN").Value_Config.ToString();
                txtMailHaiQuan.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailHaiQuan").Value_Config.ToString();
                txtMaMid.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MaMID").Value_Config.ToString();
                txtTenCucHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN").Value_Config.ToString();
                txtTenNganCucHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN_NGAN").Value_Config.ToString();
                txtMaDN.ReadOnly = true;
           


            }
            donViHaiQuanControl.Leave += new EventHandler(donViHaiQuanControl_Leave);
        }

        private void FrmDaiLy_ChiTiet_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnSave_Click(null, null);
            }
        }
      
       

    }
}
