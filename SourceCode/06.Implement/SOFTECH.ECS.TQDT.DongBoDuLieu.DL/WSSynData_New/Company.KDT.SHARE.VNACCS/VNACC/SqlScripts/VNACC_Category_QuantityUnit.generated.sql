-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
(
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@TableID,
	@Code,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_QuantityUnit]
SET
	[TableID] = @TableID,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[Code] = @Code

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [Code] FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE [Code] = @Code)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_QuantityUnit] 
		SET
			[TableID] = @TableID,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[Code] = @Code
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
	(
			[TableID],
			[Code],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@TableID,
			@Code,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]
	@Code varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]
	@Code varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_QuantityUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]	

GO

