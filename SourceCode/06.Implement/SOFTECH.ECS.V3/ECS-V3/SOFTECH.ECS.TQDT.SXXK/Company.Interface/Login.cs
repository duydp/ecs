﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;

namespace Company.Interface
{
    public partial class Login : Form
    {
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        ShowMessage("Đăng nhập không thành công.", false);

                    }
                    else
                    {
                        ShowMessage("Login unsuccessfully", false);
                    }
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    MainForm.isLoginSuccess = true;
                    GlobalSettings.UserLog = txtUser.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserLog", txtUser.Text.Trim());
                    GlobalSettings.PassLog = txtMatKhau.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassLog", txtMatKhau.Text.Trim());
                    GlobalSettings.RefreshKey();
                    this.Close();
                }

                /*
                #region Load thong so cau hinh

                try
                {
                    //Hungtq, updated 07/01/2013. Cap nhat IP, service khai bao
                    string maChiCucHQ = GlobalSettings.MA_HAI_QUAN;

                    //Lay thong tin IP, service dua vao ma chi cuc HQ
                    Company.KDT.SHARE.Components.Globals.GetIPWebService(maChiCucHQ);

                    //Cap nhat lai ip, service khai bao trong file Cau hinh daonh nghiep
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    string path = Company.BLL.EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
                    //Hungtq update 28/01/2011.
                    string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);
                    doc.Load(fileName);

                    //Set thông tin MaCucHQ
                    GlobalSettings.MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.MaCuc;
                    //Set thông tin TenCucHQ
                    GlobalSettings.TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenCuc;
                    //Set thông tin MaChiCucHQ
                    GlobalSettings.MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.MaChiCuc;
                    //Set thông tin TenChiCucHQ
                    GlobalSettings.TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenChiCuc;
                    //Set thông tin TenChiCucHQ                
                    GlobalSettings.DiaChiWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.IP;
                    GlobalSettings.DiaChiWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenDichVu;
                    GlobalSettings.DiaChiWS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS").InnerText = Company.KDT.SHARE.Components.Globals.IPServiceHQ.WebService;

                    //Cap nhat lai ip, service khai bao trong file config exe
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", Company.KDT.SHARE.Components.Globals.IPServiceHQ.MaCuc);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenCuc);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", Company.KDT.SHARE.Components.Globals.IPServiceHQ.MaChiCuc);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenChiCuc);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", Company.KDT.SHARE.Components.Globals.IPServiceHQ.WebService);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", Company.KDT.SHARE.Components.Globals.IPServiceHQ.WebService);

                    doc.Save(fileName);
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                #endregion
                */
            }
            catch
            {
                MessageBox.Show("Không kết nối tới cơ sở dữ liệu");
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = string.Empty;
            txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            if (GlobalSettings.NGON_NGU == "0")
            {

                linkLabel1.Text = "Cấu hình thông số kết nối ";
                uiButton3.Text = "Đăng nhập";
                uiButton2.Text = "Thoát";


                if (System.IO.File.Exists("loginv.png"))
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("loginv.png");

                }

                //  this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
            else
            {
                linkLabel1.Text = "Connected configuration";
                uiButton3.Text = "Login";
                uiButton2.Text = "Close";

                if (System.IO.File.Exists("logine.png"))
                    this.BackgroundImage = System.Drawing.Image.FromFile("logine.png");
                //this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
        }




    }
}