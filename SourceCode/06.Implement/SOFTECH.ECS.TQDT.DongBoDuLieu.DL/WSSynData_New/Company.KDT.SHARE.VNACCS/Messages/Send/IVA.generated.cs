using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class IVA : BasicVNACC
    {
        public PropertiesAttribute NIV { get; set; }
        public PropertiesAttribute YNK { get; set; }
        public PropertiesAttribute TII { get; set; }
        public PropertiesAttribute IVN { get; set; }
        public PropertiesAttribute IVD { get; set; }
        public PropertiesAttribute IVB { get; set; }
        public GroupAttribute NOM { get; set; }
        public PropertiesAttribute HOS { get; set; }
        public GroupAttribute AT_ { get; set; }
        public PropertiesAttribute IMC { get; set; }
        public PropertiesAttribute IMN { get; set; }
        public PropertiesAttribute IMY { get; set; }
        public PropertiesAttribute IMA { get; set; }
        public PropertiesAttribute IMT { get; set; }
        public PropertiesAttribute SAA { get; set; }
        public PropertiesAttribute EPC { get; set; }
        public PropertiesAttribute EPN { get; set; }
        public PropertiesAttribute EP1 { get; set; }
        public PropertiesAttribute EPA { get; set; }
        public PropertiesAttribute EP2 { get; set; }
        public PropertiesAttribute EP3 { get; set; }
        public PropertiesAttribute EP4 { get; set; }
        public PropertiesAttribute EP6 { get; set; }
        public PropertiesAttribute EEE { get; set; }
        public PropertiesAttribute KNO { get; set; }
        public PropertiesAttribute TTP { get; set; }
        public PropertiesAttribute VSS { get; set; }
        public PropertiesAttribute VNO { get; set; }
        public PropertiesAttribute PSC { get; set; }
        public PropertiesAttribute PSN { get; set; }
        public PropertiesAttribute PSD { get; set; }
        public PropertiesAttribute DST { get; set; }
        public PropertiesAttribute DCN { get; set; }
        public PropertiesAttribute KYC { get; set; }
        public PropertiesAttribute KYN { get; set; }
        public PropertiesAttribute GWJ { get; set; }
        public PropertiesAttribute GWT { get; set; }
        public PropertiesAttribute JWJ { get; set; }
        public PropertiesAttribute JWT { get; set; }
        public PropertiesAttribute STJ { get; set; }
        public PropertiesAttribute STT { get; set; }
        public PropertiesAttribute NOJ { get; set; }
        public PropertiesAttribute NOT { get; set; }
        public PropertiesAttribute NT3 { get; set; }
        public PropertiesAttribute PLN { get; set; }
        public PropertiesAttribute LCB { get; set; }
        public PropertiesAttribute FON { get; set; }
        public PropertiesAttribute FOT { get; set; }
        public PropertiesAttribute FKK { get; set; }
        public PropertiesAttribute FKT { get; set; }
        public PropertiesAttribute FR3 { get; set; }
        public PropertiesAttribute FR2 { get; set; }
        public PropertiesAttribute FR4 { get; set; }
        public PropertiesAttribute FS1 { get; set; }
        public PropertiesAttribute FT1 { get; set; }
        public PropertiesAttribute FH1 { get; set; }
        public PropertiesAttribute FS2 { get; set; }
        public PropertiesAttribute FT2 { get; set; }
        public PropertiesAttribute FH2 { get; set; }
        public PropertiesAttribute NUH { get; set; }
        public PropertiesAttribute NTK { get; set; }
        public PropertiesAttribute IN3 { get; set; }
        public PropertiesAttribute IN2 { get; set; }
        public PropertiesAttribute IK1 { get; set; }
        public PropertiesAttribute IK2 { get; set; }
        public PropertiesAttribute NBG { get; set; }
        public PropertiesAttribute NBC { get; set; }
        public PropertiesAttribute NBS { get; set; }
        public PropertiesAttribute SKG { get; set; }
        public PropertiesAttribute SKC { get; set; }
        public PropertiesAttribute SKS { get; set; }
        public PropertiesAttribute IP4 { get; set; }
        public PropertiesAttribute IP3 { get; set; }
        public PropertiesAttribute IP2 { get; set; }
        public PropertiesAttribute HWT { get; set; }
        public PropertiesAttribute NT1 { get; set; }
        public PropertiesAttribute SNM { get; set; }

        public IVA()
        {
            NIV = new PropertiesAttribute(12, typeof(int));
            YNK = new PropertiesAttribute(1, typeof(string));
            TII = new PropertiesAttribute(5, typeof(string));
            IVN = new PropertiesAttribute(35, typeof(string));
            IVD = new PropertiesAttribute(8, typeof(DateTime));
            IVB = new PropertiesAttribute(35, typeof(string));
            HOS = new PropertiesAttribute(30, typeof(string));
            IMC = new PropertiesAttribute(13, typeof(string));
            IMN = new PropertiesAttribute(300, typeof(string));
            IMY = new PropertiesAttribute(7, typeof(string));
            IMA = new PropertiesAttribute(300, typeof(string));
            IMT = new PropertiesAttribute(20, typeof(string));
            SAA = new PropertiesAttribute(60, typeof(string));
            EPC = new PropertiesAttribute(13, typeof(string));
            EPN = new PropertiesAttribute(70, typeof(string));
            EP1 = new PropertiesAttribute(9, typeof(string));
            EPA = new PropertiesAttribute(35, typeof(string));
            EP2 = new PropertiesAttribute(35, typeof(string));
            EP3 = new PropertiesAttribute(35, typeof(string));
            EP4 = new PropertiesAttribute(35, typeof(string));
            EP6 = new PropertiesAttribute(12, typeof(string));
            EEE = new PropertiesAttribute(2, typeof(string));
            KNO = new PropertiesAttribute(140, typeof(string));
            TTP = new PropertiesAttribute(4, typeof(string));
            VSS = new PropertiesAttribute(35, typeof(string));
            VNO = new PropertiesAttribute(10, typeof(string));
            PSC = new PropertiesAttribute(5, typeof(string));
            PSN = new PropertiesAttribute(20, typeof(string));
            PSD = new PropertiesAttribute(8, typeof(DateTime));
            DST = new PropertiesAttribute(5, typeof(string));
            DCN = new PropertiesAttribute(20, typeof(string));
            KYC = new PropertiesAttribute(5, typeof(string));
            KYN = new PropertiesAttribute(30, typeof(string));
            GWJ = new PropertiesAttribute(10, typeof(int), 2);
            GWT = new PropertiesAttribute(3, typeof(string));
            JWJ = new PropertiesAttribute(10, typeof(int), 2);
            JWT = new PropertiesAttribute(3, typeof(string));
            STJ = new PropertiesAttribute(10, typeof(int), 2);
            STT = new PropertiesAttribute(3, typeof(string));
            NOJ = new PropertiesAttribute(8, typeof(int));
            NOT = new PropertiesAttribute(3, typeof(string));
            NT3 = new PropertiesAttribute(210, typeof(string));
            PLN = new PropertiesAttribute(10, typeof(string));
            LCB = new PropertiesAttribute(80, typeof(string));
            FON = new PropertiesAttribute(20, typeof(int), 2);
            FOT = new PropertiesAttribute(3, typeof(string));
            FKK = new PropertiesAttribute(13, typeof(int), 2);
            FKT = new PropertiesAttribute(3, typeof(string));
            FR3 = new PropertiesAttribute(18, typeof(int), 2);
            FR2 = new PropertiesAttribute(3, typeof(string));
            FR4 = new PropertiesAttribute(20, typeof(string));
            FS1 = new PropertiesAttribute(9, typeof(int), 2);
            FT1 = new PropertiesAttribute(3, typeof(string));
            FH1 = new PropertiesAttribute(20, typeof(string));
            FS2 = new PropertiesAttribute(9, typeof(int), 2);
            FT2 = new PropertiesAttribute(3, typeof(string));
            FH2 = new PropertiesAttribute(20, typeof(string));
            NUH = new PropertiesAttribute(9, typeof(int), 2);
            NTK = new PropertiesAttribute(3, typeof(string));
            IN3 = new PropertiesAttribute(16, typeof(int), 2);
            IN2 = new PropertiesAttribute(3, typeof(string));
            IK1 = new PropertiesAttribute(9, typeof(int), 2);
            IK2 = new PropertiesAttribute(3, typeof(string));
            NBG = new PropertiesAttribute(9, typeof(int), 2);
            NBC = new PropertiesAttribute(3, typeof(string));
            NBS = new PropertiesAttribute(20, typeof(string));
            SKG = new PropertiesAttribute(9, typeof(int), 2);
            SKC = new PropertiesAttribute(3, typeof(string));
            SKS = new PropertiesAttribute(20, typeof(string));
            IP4 = new PropertiesAttribute(20, typeof(int), 2);
            IP3 = new PropertiesAttribute(3, typeof(string));
            IP2 = new PropertiesAttribute(20, typeof(string));
            HWT = new PropertiesAttribute(30, typeof(string));
            NT1 = new PropertiesAttribute(300, typeof(string));
            SNM = new PropertiesAttribute(4, typeof(int));
            #region NOM
            List<PropertiesAttribute> listNOM = new List<PropertiesAttribute>();
            listNOM.Add(new PropertiesAttribute(3, 11, EnumGroupID.IVA_N__, typeof(string)));
            listNOM.Add(new PropertiesAttribute(35, 11, EnumGroupID.IVA_O__, typeof(string)));
            listNOM.Add(new PropertiesAttribute(8, 11, EnumGroupID.IVA_M__, typeof(DateTime)));
            NOM = new GroupAttribute("NOM", 11, listNOM);
            #endregion NOM
            #region AT_
            List<PropertiesAttribute> listAT_ = new List<PropertiesAttribute>();
            listAT_.Add(new PropertiesAttribute(6, 2, EnumGroupID.IVA_AT_, typeof(string)));
            AT_ = new GroupAttribute("AT_", 2, listAT_);
            #endregion AT_
        }

    }
    public partial class EnumGroupID
    {
        public static readonly string IVA_N__ = "IVA_N__";
        public static readonly string IVA_O__ = "IVA_O__";
        public static readonly string IVA_M__ = "IVA_M__";
        public static readonly string IVA_AT_ = "IVA_AT_";
    }

}
