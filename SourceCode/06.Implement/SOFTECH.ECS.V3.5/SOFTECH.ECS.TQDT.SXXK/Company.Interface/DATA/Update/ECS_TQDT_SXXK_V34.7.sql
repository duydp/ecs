GO
IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_SXXK_ThanhLy_NPLNhapTon' AND COLUMN_NAME = 'SoThuTuHang')
	ALTER TABLE t_SXXK_ThanhLy_NPLNhapTon 
	ADD SoThuTuHang INT
GO
UPDATE dbo.t_KDT_ToKhaiMauDich SET NamDK = YEAR(NgayDangKy) WHERE NamDK IS NULL 
GO
UPDATE t_KDT_ToKhaiMauDich SET NamDK = YEAR(NgayDangKy) WHERE NamDK != YEAR(NgayDangKy)
GO
UPDATE t_SXXK_ThanhLy_NPLNhapTon  SET SoThuTuHang =( SELECT TOP 1 SoThuTuHang FROM dbo.t_KDT_HangMauDich WHERE TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_ToKhaiMauDich WHERE SoToKhai =t_SXXK_ThanhLy_NPLNhapTon.SoToKhai AND MaLoaiHinh = t_SXXK_ThanhLy_NPLNhapTon.MaLoaiHinh AND NamDK = t_SXXK_ThanhLy_NPLNhapTon.NamDangKy AND MaPhu = t_SXXK_ThanhLy_NPLNhapTon.MaNPL) ) WHERE t_SXXK_ThanhLy_NPLNhapTon.SoThuTuHang IS NULL
GO
UPDATE t_SXXK_ThanhLy_NPLNhapTon  SET SoThuTuHang =( SELECT TOP 1 SoThuTuHang FROM dbo.t_KDT_HangMauDich WHERE TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_ToKhaiMauDich WHERE SoToKhai =t_SXXK_ThanhLy_NPLNhapTon.SoToKhai AND MaLoaiHinh = t_SXXK_ThanhLy_NPLNhapTon.MaLoaiHinh AND NamDK = t_SXXK_ThanhLy_NPLNhapTon.NamDangKy AND MaPhu = t_SXXK_ThanhLy_NPLNhapTon.MaNPL AND SoThuTuHang = SoThuTuHang) ) WHERE t_SXXK_ThanhLy_NPLNhapTon.SoThuTuHang IS NOT NULL
go
DELETE FROM dbo.t_SXXK_ThanhLy_NPLNhapTon WHERE SoThuTuHang IS NULL 
GO
ALTER TABLE t_SXXK_ThanhLy_NPLNhapTon 
ALTER COLUMN SoThuTuHang INT NOT NULL
GO
ALTER TABLE t_SXXK_ThanhLy_NPLNhapTon 
ALTER COLUMN ThueXNK FLOAT NOT NULL
GO
ALTER TABLE [dbo].[t_SXXK_ThanhLy_NPLNhapTon] DROP CONSTRAINT [PK_t_SXXK_ThanhLy_NPLNhapTon] 
GO
ALTER TABLE [dbo].[t_SXXK_ThanhLy_NPLNhapTon] ADD CONSTRAINT [PK_t_SXXK_ThanhLy_NPLNhapTon] PRIMARY KEY CLUSTERED ([SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL], [Luong],SoThuTuHang, [ThueXNK]) ON [PRIMARY] 
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate]
GO  
CREATE PROCEDURE [dbo].[p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate]  
 @SoToKhai int,  
 @MaLoaiHinh char(5),  
 @NamDangKy smallint,  
 @MaHaiQuan char(6),  
 @MaNPL varchar(30),
 @SoThuTuHang INT,  
 @MaDoanhNghiep varchar(14),  
 @Luong numeric(18, 8),  
 @Ton numeric(18, 8),  
 @ThueXNK float,  
 @ThueTTDB float,  
 @ThueVAT float,  
 @PhuThu float,  
 @ThueCLGia float,  
 @ThueXNKTon float  
AS  
IF EXISTS(SELECT [SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL],SoThuTuHang FROM [dbo].[t_SXXK_ThanhLy_NPLNhapTon] WHERE [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [NamDangKy] = @NamDangKy AND [MaHaiQuan] = @MaHaiQuan AND [MaNPL] = @MaNPL AND SoThuTuHang=@SoThuTuHang
AND Luong = @Luong AND Round(ThueXNK,0) = Round(@ThueXNK,0))  
 BEGIN  
  UPDATE  
   [dbo].[t_SXXK_ThanhLy_NPLNhapTon]   
  SET  
   [MaDoanhNghiep] = @MaDoanhNghiep,  
   [Luong] = @Luong,  
   [Ton] = @Ton,  
   [ThueXNK] = @ThueXNK,  
   [ThueTTDB] = @ThueTTDB,  
   [ThueVAT] = @ThueVAT,  
   [PhuThu] = @PhuThu,  
   [ThueCLGia] = @ThueCLGia,  
   [ThueXNKTon] = @ThueXNKTon  
  WHERE  
   [SoToKhai] = @SoToKhai  
   AND [MaLoaiHinh] = @MaLoaiHinh  
   AND [NamDangKy] = @NamDangKy  
   AND [MaHaiQuan] = @MaHaiQuan  
   AND [MaNPL] = @MaNPL  
   AND SoThuTuHang =@SoThuTuHang
   AND Luong = @Luong AND Round(ThueXNK,0) = Round(@ThueXNK,0)  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_SXXK_ThanhLy_NPLNhapTon]  
 (  
   [SoToKhai],  
   [MaLoaiHinh],  
   [NamDangKy],  
   [MaHaiQuan],  
   [MaNPL],
   SoThuTuHang,  
   [MaDoanhNghiep],  
   [Luong],  
   [Ton],  
   [ThueXNK],  
   [ThueTTDB],  
   [ThueVAT],  
   [PhuThu],  
   [ThueCLGia],  
   [ThueXNKTon]  
 )  
 VALUES  
 (  
   @SoToKhai,  
   @MaLoaiHinh,  
   @NamDangKy,  
   @MaHaiQuan,  
   @MaNPL,
   @SoThuTuHang,  
   @MaDoanhNghiep,  
   @Luong,  
   @Ton,  
   @ThueXNK,  
   @ThueTTDB,  
   @ThueVAT,  
   @PhuThu,  
   @ThueCLGia,  
   @ThueXNKTon  
 )   
 END  

 GO
 IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_SXXK_NPLNhapTonThucTe' AND COLUMN_NAME = 'SoThuTuHang')
	ALTER TABLE dbo.t_SXXK_NPLNhapTonThucTe 
	ADD SoThuTuHang INT
 GO
 UPDATE t_SXXK_NPLNhapTonThucTe  SET SoThuTuHang =( SELECT TOP 1 SoThuTuHang FROM dbo.t_KDT_HangMauDich WHERE TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_ToKhaiMauDich WHERE SoToKhai =t_SXXK_NPLNhapTonThucTe.SoToKhai AND MaLoaiHinh = t_SXXK_NPLNhapTonThucTe.MaLoaiHinh AND NamDK = t_SXXK_NPLNhapTonThucTe.NamDangKy  AND MaPhu = t_SXXK_NPLNhapTonThucTe.MaNPL )) WHERE t_SXXK_NPLNhapTonThucTe.SoThuTuHang IS NULL
GO
DELETE FROM t_SXXK_NPLNhapTonThucTe WHERE SoThuTuHang IS NULL
GO
 ALTER TABLE dbo.t_SXXK_NPLNhapTonThucTe 
 ALTER COLUMN SoThuTuHang INT NOT NULL
 GO
 ALTER TABLE dbo.t_SXXK_NPLNhapTonThucTe 
 ALTER COLUMN ThueXNK FLOAT NOT NULL
 GO
 ALTER TABLE [dbo].[t_SXXK_NPLNhapTonThucTe] DROP CONSTRAINT [PK_t_SXXK_NPLNhapTonThucTe]
 GO
 ALTER TABLE [dbo].[t_SXXK_NPLNhapTonThucTe] ADD CONSTRAINT [PK_t_SXXK_NPLNhapTonThucTe] PRIMARY KEY CLUSTERED ([SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL], [Luong],SoThuTuHang, [ThueXNK]) ON [PRIMARY]
 GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_NPLNhapTonThucTe_InsertUpdate]  
-- Database: Vinatex  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, December 12, 2008  
------------------------------------------------------------------------------------------------------------------------  
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_SXXK_NPLNhapTonThucTe_InsertUpdate]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_SXXK_NPLNhapTonThucTe_InsertUpdate]
GO   
CREATE PROCEDURE [dbo].[p_SXXK_NPLNhapTonThucTe_InsertUpdate]  
 @SoToKhai int,  
 @MaLoaiHinh char(5),  
 @NamDangKy smallint,  
 @MaHaiQuan char(6),  
 @MaNPL varchar(30), 
 @SoThuTuHang INT, 
 @MaDoanhNghiep varchar(14),  
 @Luong numeric(18, 8),  
 @Ton numeric(18, 8),  
 @ThueXNK float,  
 @ThueTTDB float,  
 @ThueVAT float,  
 @PhuThu float,  
 @ThueCLGia float,  
 @ThueXNKTon float,  
 @NgayDangKy datetime  
AS  
IF EXISTS(SELECT [SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL],SoThuTuHang FROM [dbo].[t_SXXK_NPLNhapTonThucTe] WHERE [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [NamDangKy] = @NamDangKy AND [MaHaiQuan] = @MaHaiQuan AND [MaNPL] = @MaNPL AND SoThuTuHang = @SoThuTuHang AND ThueXNK = @ThueXNK AND Luong = @Luong)  
 BEGIN  
  UPDATE  
   [dbo].[t_SXXK_NPLNhapTonThucTe]   
  SET  
   [MaDoanhNghiep] = @MaDoanhNghiep,  
   [Luong] = @Luong,  
   [Ton] = @Ton,  
   [ThueXNK] = @ThueXNK,  
   [ThueTTDB] = @ThueTTDB,  
   [ThueVAT] = @ThueVAT,  
   [PhuThu] = @PhuThu,  
   [ThueCLGia] = @ThueCLGia,  
   [ThueXNKTon] = @ThueXNKTon,  
   [NgayDangKy] = @NgayDangKy  
  WHERE  
   [SoToKhai] = @SoToKhai  
   AND [MaLoaiHinh] = @MaLoaiHinh  
   AND [NamDangKy] = @NamDangKy  
   AND [MaHaiQuan] = @MaHaiQuan  
   AND [MaNPL] = @MaNPL 
   AND SoThuTuHang =@SoThuTuHang 
AND ThueXNK = @ThueXNK AND Luong = @Luong  
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_SXXK_NPLNhapTonThucTe]  
 (  
   [SoToKhai],  
   [MaLoaiHinh],  
   [NamDangKy],  
   [MaHaiQuan],  
   [MaNPL],  
   SoThuTuHang,
   [MaDoanhNghiep],  
   [Luong],  
   [Ton],  
   [ThueXNK],  
   [ThueTTDB],  
   [ThueVAT],  
   [PhuThu],  
   [ThueCLGia],  
   [ThueXNKTon],  
   [NgayDangKy]  
 )  
 VALUES  
 (  
   @SoToKhai,  
   @MaLoaiHinh,  
   @NamDangKy,  
   @MaHaiQuan,  
   @MaNPL,  
   @SoThuTuHang,
   @MaDoanhNghiep,  
   @Luong,  
   @Ton,  
   @ThueXNK,  
   @ThueTTDB,  
   @ThueVAT,  
   @PhuThu,  
   @ThueCLGia,  
   @ThueXNKTon,  
   @NgayDangKy  
 )   
 END  

 GO
  IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_KDT_SXXK_NPLNhapTon' AND COLUMN_NAME = 'SoThuTuHang')
	ALTER TABLE t_KDT_SXXK_NPLNhapTon
    ADD SoThuTuHang INT
 GO
 UPDATE t_KDT_SXXK_NPLNhapTon  SET SoThuTuHang =( SELECT TOP 1 SoThuTuHang FROM dbo.t_KDT_HangMauDich WHERE TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_ToKhaiMauDich WHERE SoToKhai =t_KDT_SXXK_NPLNhapTon.SoToKhai AND MaLoaiHinh = t_KDT_SXXK_NPLNhapTon.MaLoaiHinh AND NamDK = t_KDT_SXXK_NPLNhapTon.NamDangKy  AND MaPhu = t_KDT_SXXK_NPLNhapTon.MaNPL )) WHERE t_KDT_SXXK_NPLNhapTon.SoThuTuHang IS NULL
GO
DELETE FROM t_KDT_SXXK_NPLNhapTon WHERE SoThuTuHang IS NULL
GO
 ALTER TABLE t_KDT_SXXK_NPLNhapTon
 ALTER COLUMN SoThuTuHang INT NOT NULL
 GO
 ALTER TABLE t_KDT_SXXK_NPLNhapTon
 ALTER COLUMN ThueXNK FLOAT NOT NULL
 GO
ALTER TABLE [dbo].[t_KDT_SXXK_NPLNhapTon] DROP CONSTRAINT [PK_t_KDT_SXXK_NPLNhapTon_1] 
GO
ALTER TABLE [dbo].[t_KDT_SXXK_NPLNhapTon] ADD CONSTRAINT [PK_t_KDT_SXXK_NPLNhapTon_1] PRIMARY KEY CLUSTERED ([SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL], [LanThanhLy], [Luong], [ThueXNK],SoThuTuHang) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLNhapTon_InsertUpdate]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_NPLNhapTon_InsertUpdate]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_InsertUpdate]
GO  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_InsertUpdate]  
 @SoToKhai int,  
 @MaLoaiHinh char(5),  
 @NamDangKy smallint,
 @MaHaiQuan char(6),   
 @MaNPL varchar(30),
 @SoThuTuHang INT,  
 @LanThanhLy int,  
 @Luong numeric(18, 8),  
 @TonDau numeric(18, 8),  
 @TonCuoi numeric(18, 8),  
 @ThueXNK float,  
 @ThueTTDB float,  
 @ThueVAT float,  
 @PhuThu float,  
 @ThueCLGia float,  
 @TonDauThueXNK float,  
 @TonCuoiThueXNK float,  
 @MaDoanhNghiep varchar(14)  
AS  
IF EXISTS(SELECT [SoToKhai], [MaLoaiHinh], [NamDangKy], [MaHaiQuan], [MaNPL],SoThuTuHang, [LanThanhLy] FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [NamDangKy] = @NamDangKy AND [MaHaiQuan] = @MaHaiQuan AND [MaNPL]= @MaNPL AND SoThuTuHang=@SoThuTuHang AND [LanThanhLy] = @LanThanhLy AND Luong = @Luong  
AND ThueXNK = @ThueXNK )  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_SXXK_NPLNhapTon]   
  SET  
   [Luong] = @Luong,  
   [TonDau] = @TonDau,  
   [TonCuoi] = @TonCuoi,  
   [ThueXNK] = @ThueXNK,  
   [ThueTTDB] = @ThueTTDB,  
   [ThueVAT] = @ThueVAT,  
   [PhuThu] = @PhuThu,  
   [ThueCLGia] = @ThueCLGia,  
   [TonDauThueXNK] = @TonDauThueXNK,  
   [TonCuoiThueXNK] = @TonCuoiThueXNK  
  WHERE  
   [SoToKhai] = @SoToKhai  
   AND [MaLoaiHinh] = @MaLoaiHinh  
   AND [NamDangKy] = @NamDangKy  
   AND [MaHaiQuan] = @MaHaiQuan  
   AND [MaNPL] = @MaNPL  
   AND SoThuTuHang =@SoThuTuHang 
   AND [LanThanhLy] = @LanThanhLy  
   AND [MaDoanhNghiep] = @MaDoanhNghiep  
AND Luong = @Luong  
AND ThueXNK = @ThueXNK   
 END  
ELSE  
 BEGIN  
 INSERT INTO [dbo].[t_KDT_SXXK_NPLNhapTon]  
 (  
   [SoToKhai],  
   [MaLoaiHinh],  
   [NamDangKy],  
   [MaHaiQuan],  
   [MaNPL],
   SoThuTuHang , 
   [LanThanhLy],  
   [Luong],  
   [TonDau],  
   [TonCuoi],  
   [ThueXNK],  
   [ThueTTDB],  
   [ThueVAT],  
   [PhuThu],  
   [ThueCLGia],  
   [TonDauThueXNK],  
   [TonCuoiThueXNK],  
   [MaDoanhNghiep]  
 )  
 VALUES  
 (  
   @SoToKhai,  
   @MaLoaiHinh,  
   @NamDangKy,  
   @MaHaiQuan,  
   @MaNPL,  
   @SoThuTuHang,
   @LanThanhLy,  
   @Luong,  
   @TonDau,  
   @TonCuoi,  
   @ThueXNK,  
   @ThueTTDB,  
   @ThueVAT,  
   @PhuThu,  
   @ThueCLGia,  
   @TonDauThueXNK,  
   @TonCuoiThueXNK,  
   @MaDoanhNghiep  
 )   
 END  
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.7',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO BÁO CÁO QUYẾT TOÁN')
END