using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ToKhaiMauDich_KhaiBoSung
    {
        List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> _ListHangBoSung = new List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue>();

        public List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> HangCollection
        {
            set { this._ListHangBoSung = value; }
            get { return this._ListHangBoSung; }
        }

        public void LoadHangCollection()
        {
            HangCollection = KDT_VNACC_HangMauDich_KhaiBoSung_Thue.SelectCollectionDynamic("TKMDBoSung_ID = " + this.ID, "ID");
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();

                    // luu hang to khai
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.TKMDBoSung_ID = this.ID;
                            itemHang.ID = itemHang.Insert();
                        }
                        else
                        {
                            itemHang.Update();
                        }

                        foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac itemHangThuKhac in itemHang.HangThuKhacCollection)
                        {
                            if (itemHangThuKhac.ID == 0)
                            {
                                itemHangThuKhac.HMDBoSung_ID = itemHang.ID;
                                itemHangThuKhac.ID = itemHangThuKhac.Insert();
                            }
                            else
                            {
                                itemHangThuKhac.Update();
                            }
                        }
                    }

                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

    }
}