using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
using Company.BLL.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
namespace Company.Interface.KDT.SXXK
{
    public partial class HangDuaVaoReadExcelForm : BaseForm
    {
        public List<HangDuaVao> NPLCollection = new List<HangDuaVao>();
        public HangDuaVaoDangKy nplDangky;
        public HangDuaVaoReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
            cbbSheetName.DataSource = GetAllSheetName();
            cbbSheetName.SelectedIndex = 0;
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //showMsg("MSG_0203008");
                    ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private int checkNPLExit(string maNPL)
        {
            if (this.NPLCollection == null)
                this.NPLCollection = new List<HangDuaVao>();
            for (int i = 0; i < this.NPLCollection.Count; i++)
            {
                if (this.NPLCollection[i].Ma.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(cbMucDich, errorProvider, "Mục đích sử dụng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cmbLoaiHang, errorProvider, "Loại hàng đưa vào", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Started row must be greater than 0 ");
                }

                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
            }
            catch
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                // ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorMaHangHoaValid = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";

            string errorNPL = "";

            List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
            List<HangDuaVao> Collection = new List<HangDuaVao>();
            Company.BLL.SXXK.NguyenPhuLieuCollection NPLCollection = Company.BLL.SXXK.NguyenPhuLieu.SelectCollectionDynamicBy(" MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        HangDuaVao npl = new HangDuaVao();

                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                            if (npl.Ma.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                // KIỂM TRA MÃ NPL ĐÃ ĐĂNG KÝ
                                foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLCollection)
                                {
                                    if (npl.Ma==item.Ma)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                                if (!isExits)
                                {
                                    // KIỂM TRA MÃ NPL ĐÃ ĐƯỢC NHẬP TRƯỚC ĐÓ BỎ QUA ĐÃ HỦY 
                                    IList<HangDuaVaoDangKy> NPLDKCollection = HangDuaVaoDangKy.SelectCollectionDynamicAll("MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND Ma ='" + npl.Ma + "' AND t_KDT_CX_HangDuaVaoDangKy.ID NOT IN (" + nplDangky.ID + ") AND TrangThaiXuLy NOT IN (10)", "");
                                    if (NPLDKCollection.Count >= 1)
                                    {
                                        errorNPL += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]-[" + NPLDKCollection[0].SoTiepNhan + "]-[" + NPLDKCollection[0].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(NPLDKCollection[0].TrangThaiXuLy) + "]\n";
                                        isExits = true;
                                        isAdd = false;
                                    }
                                }
                                if (!isExits)
                                {
                                    // KIỂM TRA MÃ NPL ĐÃ NHẬP TRÊN LƯỚI
                                    foreach (HangDuaVao item in nplDangky.DanhSachHangDuaVao)
                                    {
                                        if (npl.Ma == item.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        errorMaHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            if (isExits)
                            {
                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            npl.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                            if (npl.Ten.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ten + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + npl.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            npl.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (npl.MaHS.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_HSCode item in HSCollection)
                                {
                                    if (item.HSCode == npl.MaHS)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + npl.MaHS + "]\n";
                            isAdd = false;
                        }
                        string DonViTinh = String.Empty;
                        try
                        {
                            isExits = false;
                            DonViTinh = Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper());
                            if (DonViTinh.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (HaiQuan_DonViTinh item in DVTCollection)
                                {
                                    if (item.Ten == DonViTinh)
                                    {
                                        npl.DVT_ID = DonViTinh_GetID(DonViTinh);
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                            isAdd = false;
                        }
                        npl.MucDich = cbMucDich.SelectedValue.ToString();
                        npl.LoaiNPL = cmbLoaiHang.SelectedValue.ToString();
                        if (isAdd)
                            Collection.Add(npl);
                    }
                    catch (Exception ex)
                    {
                        //this.SaveDefault();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " không được để trống";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " đã được đăng ký ";
            if (!String.IsNullOrEmpty(errorMaHangHoaValid))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaValid + " đã được nhập liệu trên lưới ";
            if (!String.IsNullOrEmpty(errorNPL))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorNPL + " đã được Nhập liệu trước đó .";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " không được để trống";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " không được để trống";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " không hợp lệ ";
            if (!String.IsNullOrEmpty(errorMaHS))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " không được để trống";
            if (!String.IsNullOrEmpty(errorMaHSExits))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " không hợp lệ";
            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (HangDuaVao item in Collection)
                {
                    this.NPLCollection.Add(item);
                }
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "Nhập hàng từ Excel thành công ", false);
            }
            else
            {
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "Nhập từ Excel không thành công ." + errorTotal, false);
                return;
            }
            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {

        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_SXXK("NPL");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        
    }
}