﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class CommercialInvoices
    {
        /// <summary>
        /// Thông tin về hóa đơn
        /// </summary>
       [XmlElement("CommercialInvoice")]
       public List<CommercialInvoice> CommercialInvoice { get; set; }
    }
}
