﻿namespace Microsoft.Xml.Serialization.GeneratedAssembly
{
    using Naccs.Core.BatchDoc;
    using Naccs.Core.Classes;
    using Naccs.Core.DataView;
    using Naccs.Core.DataView.Arrange;
    using Naccs.Core.Job;
    using Naccs.Core.Main;
    using Naccs.Core.Option;
    using Naccs.Core.Print;
    using Naccs.Core.Settings;
    using System;
    using System.Collections;
    using System.Xml.Serialization;

    public class XmlSerializerContract : XmlSerializerImplementation
    {
        private Hashtable readMethods;
        private Hashtable typedSerializers;
        private Hashtable writeMethods;

        public override bool CanSerialize(Type type)
        {
            return ((type == typeof(AbstractSettings)) || ((type == typeof(AbstractConfig)) || ((type == typeof(ReceiveNotice)) || ((type == typeof(ListItem)) || ((type == typeof(ReceiveInformClass)) || ((type == typeof(SoundClass)) || ((type == typeof(WarningInformClass)) || ((type == typeof(InformClass)) || ((type == typeof(PrintSetups)) || ((type == typeof(DefaultInfoClass)) || ((type == typeof(PrinterInfoClass)) || ((type == typeof(BinClass)) || ((type == typeof(FormsClass)) || ((type == typeof(OutCodeClass)) || ((type == typeof(PathInfo)) || ((type == typeof(PathItem)) || ((type == typeof(PathType)) || ((type == typeof(ProtocolType)) || ((type == typeof(SettingDivType)) || ((type == typeof(UserKindType)) || ((type == typeof(KeyListClass)) || ((type == typeof(KeyListClass.UserKeyType)) || ((type == typeof(JobKeyListClass)) || ((type == typeof(UserEnvironment)) || ((type == typeof(TerminalInfoClass)) || ((type == typeof(DebugFunctionClass)) || ((type == typeof(InteractiveInfoClass)) || ((type == typeof(MailInfoClass)) || ((type == typeof(HttpOptionClass)) || ((type == typeof(LogFileLengthClass)) || ((type == typeof(OutCodeListClass)) || ((type == typeof(BinListClass)) || ((type == typeof(WriterLog)) || ((type == typeof(NaccsHeader)) || ((type == typeof(DataControl)) || ((type == typeof(TermMessageSetListClass)) || ((type == typeof(SystemEnvironment)) || ((type == typeof(TerminalInfoSysClass)) || ((type == typeof(InteractiveServerClass)) || ((type == typeof(InteractiveInfoSysClass)) || ((type == typeof(MailServerClass)) || ((type == typeof(MailInfoSysClass)) || ((type == typeof(BatchdocInfoSysClass)) || ((type == typeof(LogFileLengthSysClass)) || ((type == typeof(UJobMenu.ListRec)) || ((type == typeof(UDataView.NodeSorter)) || ((type == typeof(DataCompress.IndexRec)) || ((type == typeof(TestServer)) || ((type == typeof(NetTestMode)) || ((type == typeof(User)) || ((type == typeof(ApplicationClass)) || ((type == typeof(ToolBarClass)) || ((type == typeof(WindowClass)) || ((type == typeof(LastLogonClass)) || ((type == typeof(HistoryJobsClass)) || ((type == typeof(ColumnWidthClass)) || ((type == typeof(DataViewFormClass)) || ((type == typeof(ResultCodeColorClass)) || ((type == typeof(JobOptionClass)) || ((type == typeof(BatchSendClass)) || ((type == typeof(HistoryUserClass)) || ((type == typeof(HistoryAddressClass)) || ((type == typeof(HistorySearchClass)) || ((type == typeof(DetailsListClass)) || ((type == typeof(GatewaySettingClass)) || ((type == typeof(DatasetRevert)) || ((type == typeof(PrintStatus)) || ((type == typeof(RevertEventArgs)) || ((type == typeof(DatasetStorage)) || ((type == typeof(ArrangeDataTable)) || ((type == typeof(CustomizeMenu)) || ((type == typeof(UserPathNames)) || ((type == typeof(CommonPathClass)) || ((type == typeof(RemoveWorkerArgs)) || ((type == typeof(HelpSettings)) || ((type == typeof(HelpPathClass)) || ((type == typeof(ConfigFiles)) || ((type == typeof(RemoveWorkerResult)) || ((type == typeof(DatasetRemove)) || ((type == typeof(OptionCertification)) || ((type == typeof(AllCertificationClass)) || ((type == typeof(ComboManager)) || ((type == typeof(DataFactory)) || ((type == typeof(UserKey)) || ((type == typeof(JobKeyClass)) || ((type == typeof(KeyClass)) || ((type == typeof(PrinterInfoListClass)) || ((type == typeof(StringList)) || ((type == typeof(JobFormFactory)) || ((type == typeof(ULogClass)) || ((type == typeof(ModeFont)) || ((type == typeof(JobPanelStatus)) || ((type == typeof(VersionuptoolUpdater)) || ((type == typeof(ArrangeProgress.ProgresType)) || ((type == typeof(VersionSettings)) || ((type == typeof(PrintManager)) || ((type == typeof(USendReportDlg.ReportStatus)) || ((type == typeof(DataStatus)) || ((type == typeof(Signflg)) || ((type == typeof(ContainedType)) || ((type == typeof(OutcodeTbl)) || ((type == typeof(OutcodeTbl.OutcodeRec)) || ((type == typeof(MessageClassify)) || ((type == typeof(TermClass)) || ((type == typeof(DataViewClass)) || ((type == typeof(TermListClass)) || ((type == typeof(UserKeySet)) || ((type == typeof(TypeListClass)) || ((type == typeof(GStampSettings)) || ((type == typeof(GStampInfoClass)) || ((type == typeof(FileSave)) || ((type == typeof(AllFileSaveClass)) || ((type == typeof(TypeClass)) || ((type == typeof(FileNameClass)) || ((type == typeof(SendFileClass)) || ((type == typeof(DetailsClass)) || ((type == typeof(ListType)) || ((type == typeof(TermMessage)) || ((type == typeof(TermMessageSetClass)) || ((type == typeof(ReceiveInformListClass)) || ((type == typeof(ResourceManager)) || ((type == typeof(NaccsData)) || ((type == typeof(ButtonPatern)) || (type == typeof(MessageKind)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
        }

        public override XmlSerializer GetSerializer(Type type)
        {
            if (type == typeof(AbstractSettings))
            {
                return new AbstractSettingsSerializer();
            }
            if (type == typeof(AbstractConfig))
            {
                return new AbstractConfigSerializer();
            }
            if (type == typeof(ReceiveNotice))
            {
                return new ReceiveNoticeSerializer();
            }
            if (type == typeof(ListItem))
            {
                return new ListItemSerializer();
            }
            if (type == typeof(ReceiveInformClass))
            {
                return new ReceiveInformClassSerializer();
            }
            if (type == typeof(SoundClass))
            {
                return new SoundClassSerializer();
            }
            if (type == typeof(WarningInformClass))
            {
                return new WarningInformClassSerializer();
            }
            if (type == typeof(InformClass))
            {
                return new InformClassSerializer();
            }
            if (type == typeof(PrintSetups))
            {
                return new PrintSetupsSerializer();
            }
            if (type == typeof(DefaultInfoClass))
            {
                return new DefaultInfoClassSerializer();
            }
            if (type == typeof(PrinterInfoClass))
            {
                return new PrinterInfoClassSerializer();
            }
            if (type == typeof(BinClass))
            {
                return new BinClassSerializer();
            }
            if (type == typeof(FormsClass))
            {
                return new FormsClassSerializer();
            }
            if (type == typeof(OutCodeClass))
            {
                return new OutCodeClassSerializer();
            }
            if (type == typeof(PathInfo))
            {
                return new PathInfoSerializer();
            }
            if (type == typeof(PathItem))
            {
                return new PathItemSerializer();
            }
            if (type == typeof(PathType))
            {
                return new PathTypeSerializer();
            }
            if (type == typeof(ProtocolType))
            {
                return new ProtocolTypeSerializer();
            }
            if (type == typeof(SettingDivType))
            {
                return new SettingDivTypeSerializer();
            }
            if (type == typeof(UserKindType))
            {
                return new UserKindTypeSerializer();
            }
            if (type == typeof(KeyListClass))
            {
                return new KeyListClassSerializer();
            }
            if (type == typeof(KeyListClass.UserKeyType))
            {
                return new UserKeyTypeSerializer();
            }
            if (type == typeof(JobKeyListClass))
            {
                return new JobKeyListClassSerializer();
            }
            if (type == typeof(UserEnvironment))
            {
                return new UserEnvironmentSerializer();
            }
            if (type == typeof(TerminalInfoClass))
            {
                return new TerminalInfoClassSerializer();
            }
            if (type == typeof(DebugFunctionClass))
            {
                return new DebugFunctionClassSerializer();
            }
            if (type == typeof(InteractiveInfoClass))
            {
                return new InteractiveInfoClassSerializer();
            }
            if (type == typeof(MailInfoClass))
            {
                return new MailInfoClassSerializer();
            }
            if (type == typeof(HttpOptionClass))
            {
                return new HttpOptionClassSerializer();
            }
            if (type == typeof(LogFileLengthClass))
            {
                return new LogFileLengthClassSerializer();
            }
            if (type == typeof(OutCodeListClass))
            {
                return new OutCodeListClassSerializer();
            }
            if (type == typeof(BinListClass))
            {
                return new BinListClassSerializer();
            }
            if (type == typeof(WriterLog))
            {
                return new WriterLogSerializer();
            }
            if (type == typeof(NaccsHeader))
            {
                return new NaccsHeaderSerializer();
            }
            if (type == typeof(DataControl))
            {
                return new DataControlSerializer();
            }
            if (type == typeof(TermMessageSetListClass))
            {
                return new TermMessageSetListClassSerializer();
            }
            if (type == typeof(SystemEnvironment))
            {
                return new SystemEnvironmentSerializer();
            }
            if (type == typeof(TerminalInfoSysClass))
            {
                return new TerminalInfoSysClassSerializer();
            }
            if (type == typeof(InteractiveServerClass))
            {
                return new InteractiveServerClassSerializer();
            }
            if (type == typeof(InteractiveInfoSysClass))
            {
                return new InteractiveInfoSysClassSerializer();
            }
            if (type == typeof(MailServerClass))
            {
                return new MailServerClassSerializer();
            }
            if (type == typeof(MailInfoSysClass))
            {
                return new MailInfoSysClassSerializer();
            }
            if (type == typeof(BatchdocInfoSysClass))
            {
                return new BatchdocInfoSysClassSerializer();
            }
            if (type == typeof(LogFileLengthSysClass))
            {
                return new LogFileLengthSysClassSerializer();
            }
            if (type == typeof(UJobMenu.ListRec))
            {
                return new ListRecSerializer();
            }
            if (type == typeof(UDataView.NodeSorter))
            {
                return new NodeSorterSerializer();
            }
            if (type == typeof(DataCompress.IndexRec))
            {
                return new IndexRecSerializer();
            }
            if (type == typeof(TestServer))
            {
                return new TestServerSerializer();
            }
            if (type == typeof(NetTestMode))
            {
                return new NetTestModeSerializer();
            }
            if (type == typeof(User))
            {
                return new UserSerializer();
            }
            if (type == typeof(ApplicationClass))
            {
                return new ApplicationClassSerializer();
            }
            if (type == typeof(ToolBarClass))
            {
                return new ToolBarClassSerializer();
            }
            if (type == typeof(WindowClass))
            {
                return new WindowClassSerializer();
            }
            if (type == typeof(LastLogonClass))
            {
                return new LastLogonClassSerializer();
            }
            if (type == typeof(HistoryJobsClass))
            {
                return new HistoryJobsClassSerializer();
            }
            if (type == typeof(ColumnWidthClass))
            {
                return new ColumnWidthClassSerializer();
            }
            if (type == typeof(DataViewFormClass))
            {
                return new DataViewFormClassSerializer();
            }
            if (type == typeof(ResultCodeColorClass))
            {
                return new ResultCodeColorClassSerializer();
            }
            if (type == typeof(JobOptionClass))
            {
                return new JobOptionClassSerializer();
            }
            if (type == typeof(BatchSendClass))
            {
                return new BatchSendClassSerializer();
            }
            if (type == typeof(HistoryUserClass))
            {
                return new HistoryUserClassSerializer();
            }
            if (type == typeof(HistoryAddressClass))
            {
                return new HistoryAddressClassSerializer();
            }
            if (type == typeof(HistorySearchClass))
            {
                return new HistorySearchClassSerializer();
            }
            if (type == typeof(DetailsListClass))
            {
                return new DetailsListClassSerializer();
            }
            if (type == typeof(GatewaySettingClass))
            {
                return new GatewaySettingClassSerializer();
            }
            if (type == typeof(DatasetRevert))
            {
                return new DatasetRevertSerializer();
            }
            if (type == typeof(PrintStatus))
            {
                return new PrintStatusSerializer();
            }
            if (type == typeof(RevertEventArgs))
            {
                return new RevertEventArgsSerializer();
            }
            if (type == typeof(DatasetStorage))
            {
                return new DatasetStorageSerializer();
            }
            if (type == typeof(ArrangeDataTable))
            {
                return new ArrangeDataTableSerializer();
            }
            if (type == typeof(CustomizeMenu))
            {
                return new CustomizeMenuSerializer();
            }
            if (type == typeof(UserPathNames))
            {
                return new UserPathNamesSerializer();
            }
            if (type == typeof(CommonPathClass))
            {
                return new CommonPathClassSerializer();
            }
            if (type == typeof(RemoveWorkerArgs))
            {
                return new RemoveWorkerArgsSerializer();
            }
            if (type == typeof(HelpSettings))
            {
                return new HelpSettingsSerializer();
            }
            if (type == typeof(HelpPathClass))
            {
                return new HelpPathClassSerializer();
            }
            if (type == typeof(ConfigFiles))
            {
                return new ConfigFilesSerializer();
            }
            if (type == typeof(RemoveWorkerResult))
            {
                return new RemoveWorkerResultSerializer();
            }
            if (type == typeof(DatasetRemove))
            {
                return new DatasetRemoveSerializer();
            }
            if (type == typeof(OptionCertification))
            {
                return new OptionCertificationSerializer();
            }
            if (type == typeof(AllCertificationClass))
            {
                return new AllCertificationClassSerializer();
            }
            if (type == typeof(ComboManager))
            {
                return new ComboManagerSerializer();
            }
            if (type == typeof(DataFactory))
            {
                return new DataFactorySerializer();
            }
            if (type == typeof(UserKey))
            {
                return new UserKeySerializer();
            }
            if (type == typeof(JobKeyClass))
            {
                return new JobKeyClassSerializer();
            }
            if (type == typeof(KeyClass))
            {
                return new KeyClassSerializer();
            }
            if (type == typeof(PrinterInfoListClass))
            {
                return new PrinterInfoListClassSerializer();
            }
            if (type == typeof(StringList))
            {
                return new StringListSerializer();
            }
            if (type == typeof(JobFormFactory))
            {
                return new JobFormFactorySerializer();
            }
            if (type == typeof(ULogClass))
            {
                return new ULogClassSerializer();
            }
            if (type == typeof(ModeFont))
            {
                return new ModeFontSerializer();
            }
            if (type == typeof(JobPanelStatus))
            {
                return new JobPanelStatusSerializer();
            }
            if (type == typeof(VersionuptoolUpdater))
            {
                return new VersionuptoolUpdaterSerializer();
            }
            if (type == typeof(ArrangeProgress.ProgresType))
            {
                return new ProgresTypeSerializer();
            }
            if (type == typeof(VersionSettings))
            {
                return new VersionSettingsSerializer();
            }
            if (type == typeof(PrintManager))
            {
                return new PrintManagerSerializer();
            }
            if (type == typeof(USendReportDlg.ReportStatus))
            {
                return new ReportStatusSerializer();
            }
            if (type == typeof(DataStatus))
            {
                return new DataStatusSerializer();
            }
            if (type == typeof(Signflg))
            {
                return new SignflgSerializer();
            }
            if (type == typeof(ContainedType))
            {
                return new ContainedTypeSerializer();
            }
            if (type == typeof(OutcodeTbl))
            {
                return new OutcodeTblSerializer();
            }
            if (type == typeof(OutcodeTbl.OutcodeRec))
            {
                return new OutcodeRecSerializer();
            }
            if (type == typeof(MessageClassify))
            {
                return new MessageClassifySerializer();
            }
            if (type == typeof(TermClass))
            {
                return new TermClassSerializer();
            }
            if (type == typeof(DataViewClass))
            {
                return new DataViewClassSerializer();
            }
            if (type == typeof(TermListClass))
            {
                return new TermListClassSerializer();
            }
            if (type == typeof(UserKeySet))
            {
                return new UserKeySetSerializer();
            }
            if (type == typeof(TypeListClass))
            {
                return new TypeListClassSerializer();
            }
            if (type == typeof(GStampSettings))
            {
                return new GStampSettingsSerializer();
            }
            if (type == typeof(GStampInfoClass))
            {
                return new GStampInfoClassSerializer();
            }
            if (type == typeof(FileSave))
            {
                return new FileSaveSerializer();
            }
            if (type == typeof(AllFileSaveClass))
            {
                return new AllFileSaveClassSerializer();
            }
            if (type == typeof(TypeClass))
            {
                return new TypeClassSerializer();
            }
            if (type == typeof(FileNameClass))
            {
                return new FileNameClassSerializer();
            }
            if (type == typeof(SendFileClass))
            {
                return new SendFileClassSerializer();
            }
            if (type == typeof(DetailsClass))
            {
                return new DetailsClassSerializer();
            }
            if (type == typeof(ListType))
            {
                return new ListTypeSerializer();
            }
            if (type == typeof(TermMessage))
            {
                return new TermMessageSerializer();
            }
            if (type == typeof(TermMessageSetClass))
            {
                return new TermMessageSetClassSerializer();
            }
            if (type == typeof(ReceiveInformListClass))
            {
                return new ReceiveInformListClassSerializer();
            }
            if (type == typeof(ResourceManager))
            {
                return new ResourceManagerSerializer();
            }
            if (type == typeof(NaccsData))
            {
                return new NaccsDataSerializer();
            }
            if (type == typeof(ButtonPatern))
            {
                return new ButtonPaternSerializer();
            }
            if (type == typeof(MessageKind))
            {
                return new MessageKindSerializer();
            }
            return null;
        }

        public override XmlSerializationReader Reader
        {
            get
            {
                return new XmlSerializationReader1();
            }
        }

        public override Hashtable ReadMethods
        {
            get
            {
                if (this.readMethods == null)
                {
                    Hashtable hashtable = new Hashtable();
                    hashtable["Naccs.Core.Settings.AbstractSettings::"] = "Read118_AbstractSettings";
                    hashtable["Naccs.Core.Settings.AbstractConfig::"] = "Read119_AbstractConfig";
                    hashtable["Naccs.Core.Settings.ReceiveNotice::ReceiveNotice:True:"] = "Read120_ReceiveNotice";
                    hashtable["Naccs.Core.Settings.ListItem::"] = "Read121_ListItem";
                    hashtable["Naccs.Core.Settings.ReceiveInformClass::ReceiveInform:True:"] = "Read122_ReceiveInform";
                    hashtable["Naccs.Core.Settings.SoundClass::Sound:True:"] = "Read123_Sound";
                    hashtable["Naccs.Core.Settings.WarningInformClass::WarningInform:True:"] = "Read124_WarningInform";
                    hashtable["Naccs.Core.Settings.InformClass::Inform:True:"] = "Read125_Inform";
                    hashtable["Naccs.Core.Settings.PrintSetups::Printer:True:"] = "Read126_Printer";
                    hashtable["Naccs.Core.Settings.DefaultInfoClass::DefaultInfo:True:"] = "Read127_DefaultInfo";
                    hashtable["Naccs.Core.Settings.PrinterInfoClass::PrinterInfo:True:"] = "Read128_PrinterInfo";
                    hashtable["Naccs.Core.Settings.BinClass::Bin:True:"] = "Read129_Bin";
                    hashtable["Naccs.Core.Settings.FormsClass::Forms:True:"] = "Read130_Forms";
                    hashtable["Naccs.Core.Settings.OutCodeClass::OutCode:True:"] = "Read131_OutCode";
                    hashtable["Naccs.Core.Settings.PathInfo::path:True:"] = "Read132_path";
                    hashtable["Naccs.Core.Settings.PathItem::"] = "Read133_PathItem";
                    hashtable["Naccs.Core.Settings.PathType::"] = "Read134_PathType";
                    hashtable["Naccs.Core.Settings.ProtocolType::"] = "Read135_ProtocolType";
                    hashtable["Naccs.Core.Settings.SettingDivType::"] = "Read136_SettingDivType";
                    hashtable["Naccs.Core.Settings.UserKindType::"] = "Read137_UserKindType";
                    hashtable["Naccs.Core.Settings.KeyListClass::"] = "Read138_KeyListClass";
                    hashtable["Naccs.Core.Settings.KeyListClass+UserKeyType::"] = "Read139_UserKeyType";
                    hashtable["Naccs.Core.Settings.JobKeyListClass::"] = "Read140_ArrayOfJobKeyClass";
                    hashtable["Naccs.Core.Settings.UserEnvironment::UserEnvironment:True:"] = "Read141_UserEnvironment";
                    hashtable["Naccs.Core.Settings.TerminalInfoClass::TerminalInfo:True:"] = "Read142_TerminalInfo";
                    hashtable["Naccs.Core.Settings.DebugFunctionClass::DebugFunction:True:"] = "Read143_DebugFunction";
                    hashtable["Naccs.Core.Settings.InteractiveInfoClass::InteractiveInfo:True:"] = "Read144_InteractiveInfo";
                    hashtable["Naccs.Core.Settings.MailInfoClass::MailInfo:True:"] = "Read145_MailInfo";
                    hashtable["Naccs.Core.Settings.HttpOptionClass::HttpOption:True:"] = "Read146_HttpOption";
                    hashtable["Naccs.Core.Settings.LogFileLengthClass::LogFileLength:True:"] = "Read147_LogFileLength";
                    hashtable["Naccs.Core.Settings.OutCodeListClass::"] = "Read148_ArrayOfOutCodeClass1";
                    hashtable["Naccs.Core.Settings.BinListClass::"] = "Read149_ArrayOfBinClass1";
                    hashtable["Naccs.Core.DataView.Arrange.WriterLog::"] = "Read150_WriterLog";
                    hashtable["Naccs.Core.Classes.NaccsHeader::"] = "Read151_NaccsHeader";
                    hashtable["Naccs.Core.Classes.DataControl::"] = "Read152_DataControl";
                    hashtable["Naccs.Core.Settings.TermMessageSetListClass::"] = "Read153_ArrayOfTermMessageSetClass";
                    hashtable["Naccs.Core.Settings.SystemEnvironment::SystemEnvironment:True:"] = "Read154_SystemEnvironment";
                    hashtable["Naccs.Core.Settings.TerminalInfoSysClass::"] = "Read155_TerminalInfoSysClass";
                    hashtable["Naccs.Core.Settings.InteractiveServerClass::"] = "Read156_InteractiveServerClass";
                    hashtable["Naccs.Core.Settings.InteractiveInfoSysClass::"] = "Read157_InteractiveInfoSysClass";
                    hashtable["Naccs.Core.Settings.MailServerClass::"] = "Read158_MailServerClass";
                    hashtable["Naccs.Core.Settings.MailInfoSysClass::"] = "Read159_MailInfoSysClass";
                    hashtable["Naccs.Core.Settings.BatchdocInfoSysClass::BatchdocInfo:True:"] = "Read160_BatchdocInfo";
                    hashtable["Naccs.Core.Settings.LogFileLengthSysClass::"] = "Read161_LogFileLengthSysClass";
                    hashtable["Naccs.Core.Main.UJobMenu+ListRec::"] = "Read162_ListRec";
                    hashtable["Naccs.Core.DataView.UDataView+NodeSorter::"] = "Read163_NodeSorter";
                    hashtable["Naccs.Core.DataView.DataCompress+IndexRec::"] = "Read164_IndexRec";
                    hashtable["Naccs.Core.Classes.TestServer::"] = "Read165_TestServer";
                    hashtable["Naccs.Core.Classes.NetTestMode::"] = "Read166_NetTestMode";
                    hashtable["Naccs.Core.Settings.User::User:True:"] = "Read167_User";
                    hashtable["Naccs.Core.Settings.ApplicationClass::Application:True:"] = "Read168_Application";
                    hashtable["Naccs.Core.Settings.ToolBarClass::ToolBar:True:"] = "Read169_ToolBar";
                    hashtable["Naccs.Core.Settings.WindowClass::Window:True:"] = "Read170_Window";
                    hashtable["Naccs.Core.Settings.LastLogonClass::LastLogon:True:"] = "Read171_LastLogon";
                    hashtable["Naccs.Core.Settings.HistoryJobsClass::HistoryJobs:True:"] = "Read172_HistoryJobs";
                    hashtable["Naccs.Core.Settings.ColumnWidthClass::ColumnWidth:True:"] = "Read173_ColumnWidth";
                    hashtable["Naccs.Core.Settings.DataViewFormClass::DataViewForm:True:"] = "Read174_DataViewForm";
                    hashtable["Naccs.Core.Settings.ResultCodeColorClass::ResultCodeColor:True:"] = "Read175_ResultCodeColor";
                    hashtable["Naccs.Core.Settings.JobOptionClass::JobOption:True:"] = "Read176_JobOption";
                    hashtable["Naccs.Core.Settings.BatchSendClass::BatchSend:True:"] = "Read177_BatchSend";
                    hashtable["Naccs.Core.Settings.HistoryUserClass::HistoryUser:True:"] = "Read178_HistoryUser";
                    hashtable["Naccs.Core.Settings.HistoryAddressClass::HistoryAddress:True:"] = "Read179_HistoryAddress";
                    hashtable["Naccs.Core.Settings.HistorySearchClass::HistorySearch:True:"] = "Read180_HistorySearch";
                    hashtable["Naccs.Core.Settings.DetailsListClass::"] = "Read181_ArrayOfDetailsClass";
                    hashtable["Naccs.Core.Option.GatewaySettingClass::"] = "Read182_GatewaySettingClass";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetRevert::"] = "Read183_DatasetRevert";
                    hashtable["Naccs.Core.Print.PrintStatus::"] = "Read184_PrintStatus";
                    hashtable["Naccs.Core.DataView.Arrange.RevertEventArgs::"] = "Read185_RevertEventArgs";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetStorage::"] = "Read186_DatasetStorage";
                    hashtable["Naccs.Core.DataView.Arrange.ArrangeDataTable::"] = "Read187_ArrangeDataTable";
                    hashtable["Naccs.Core.Classes.CustomizeMenu::"] = "Read188_CustomizeMenu";
                    hashtable["Naccs.Core.Settings.UserPathNames::UserPathNames:True:"] = "Read189_UserPathNames";
                    hashtable["Naccs.Core.Settings.CommonPathClass::CommonPath:True:"] = "Read190_CommonPath";
                    hashtable["Naccs.Core.DataView.Arrange.RemoveWorkerArgs::"] = "Read191_RemoveWorkerArgs";
                    hashtable["Naccs.Core.Settings.HelpSettings::Help:True:"] = "Read192_Help";
                    hashtable["Naccs.Core.Settings.HelpPathClass::HelpPath:True:"] = "Read193_HelpPath";
                    hashtable["Naccs.Core.Settings.ConfigFiles::"] = "Read194_ConfigFiles";
                    hashtable["Naccs.Core.DataView.Arrange.RemoveWorkerResult::"] = "Read195_RemoveWorkerResult";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetRemove::"] = "Read196_DatasetRemove";
                    hashtable["Naccs.Core.Settings.OptionCertification::OptionCertification:True:"] = "Read197_OptionCertification";
                    hashtable["Naccs.Core.Settings.AllCertificationClass::AllCertification:True:"] = "Read198_AllCertification";
                    hashtable["Naccs.Core.Main.ComboManager::"] = "Read199_ComboManager";
                    hashtable["Naccs.Core.Classes.DataFactory::"] = "Read200_DataFactory";
                    hashtable["Naccs.Core.Settings.UserKey::UserKey:True:"] = "Read201_UserKey";
                    hashtable["Naccs.Core.Settings.JobKeyClass::JobKey:True:"] = "Read202_JobKey";
                    hashtable["Naccs.Core.Settings.KeyClass::Key:True:"] = "Read203_Key";
                    hashtable["Naccs.Core.Settings.PrinterInfoListClass::"] = "Read204_ArrayOfPrinterInfoClass1";
                    hashtable["Naccs.Core.Settings.StringList::"] = "Read205_ArrayOfString4";
                    hashtable["Naccs.Core.Job.JobFormFactory::"] = "Read206_JobFormFactory";
                    hashtable["Naccs.Core.Classes.ULogClass::"] = "Read207_ULogClass";
                    hashtable["Naccs.Core.Job.ModeFont::"] = "Read208_ModeFont";
                    hashtable["Naccs.Core.Job.JobPanelStatus::"] = "Read209_JobPanelStatus";
                    hashtable["Naccs.Core.Main.VersionuptoolUpdater::"] = "Read210_VersionuptoolUpdater";
                    hashtable["Naccs.Core.DataView.Arrange.ArrangeProgress+ProgresType::"] = "Read211_ProgresType";
                    hashtable["Naccs.Core.Settings.VersionSettings::"] = "Read212_VersionSettings";
                    hashtable["Naccs.Core.Print.PrintManager::"] = "Read213_PrintManager";
                    hashtable["Naccs.Core.DataView.USendReportDlg+ReportStatus::"] = "Read214_ReportStatus";
                    hashtable["Naccs.Core.Classes.DataStatus::"] = "Read215_DataStatus";
                    hashtable["Naccs.Core.Classes.Signflg::"] = "Read216_Signflg";
                    hashtable["Naccs.Core.Classes.ContainedType::"] = "Read217_ContainedType";
                    hashtable["Naccs.Core.BatchDoc.OutcodeTbl::"] = "Read218_OutcodeTbl";
                    hashtable["Naccs.Core.BatchDoc.OutcodeTbl+OutcodeRec::"] = "Read219_OutcodeRec";
                    hashtable["Naccs.Core.Settings.MessageClassify::MessageClassify:True:"] = "Read220_MessageClassify";
                    hashtable["Naccs.Core.Settings.TermClass::Term:True:"] = "Read221_Term";
                    hashtable["Naccs.Core.Settings.DataViewClass::DataView:True:"] = "Read222_DataView";
                    hashtable["Naccs.Core.Settings.TermListClass::"] = "Read223_ArrayOfTermClass1";
                    hashtable["Naccs.Core.Classes.UserKeySet::"] = "Read224_UserKeySet";
                    hashtable["Naccs.Core.Settings.TypeListClass::"] = "Read225_ArrayOfTypeClass";
                    hashtable["Naccs.Core.Settings.GStampSettings::GStompSettings:True:"] = "Read226_GStompSettings";
                    hashtable["Naccs.Core.Settings.GStampInfoClass::GStompInfo:True:"] = "Read227_GStompInfo";
                    hashtable["Naccs.Core.Settings.FileSave::FileSave:True:"] = "Read228_FileSave";
                    hashtable["Naccs.Core.Settings.AllFileSaveClass::AllFileSave:True:"] = "Read229_AllFileSave";
                    hashtable["Naccs.Core.Settings.TypeClass::Type:True:"] = "Read230_Type";
                    hashtable["Naccs.Core.Settings.FileNameClass::FileName:True:"] = "Read231_FileName";
                    hashtable["Naccs.Core.Settings.SendFileClass::SendFile:True:"] = "Read232_SendFile";
                    hashtable["Naccs.Core.Settings.DetailsClass::Details:True:"] = "Read233_Details";
                    hashtable["Naccs.Core.Main.ListType::"] = "Read234_ListType";
                    hashtable["Naccs.Core.Settings.TermMessage::TermMessage:True:"] = "Read235_TermMessage";
                    hashtable["Naccs.Core.Settings.TermMessageSetClass::TermMessageSet:True:"] = "Read236_TermMessageSet";
                    hashtable["Naccs.Core.Settings.ReceiveInformListClass::"] = "Read237_ArrayOfReceiveInformClass1";
                    hashtable["Naccs.Core.Classes.ResourceManager::"] = "Read238_ResourceManager";
                    hashtable["Naccs.Core.Classes.NaccsData::ExData:True:"] = "Read239_ExData";
                    hashtable["Naccs.Core.Classes.ButtonPatern::"] = "Read240_ButtonPatern";
                    hashtable["Naccs.Core.Classes.MessageKind::"] = "Read241_MessageKind";
                    if (this.readMethods == null)
                    {
                        this.readMethods = hashtable;
                    }
                }
                return this.readMethods;
            }
        }

        public override Hashtable TypedSerializers
        {
            get
            {
                if (this.typedSerializers == null)
                {
                    Hashtable hashtable = new Hashtable();
                    hashtable.Add("Naccs.Core.Settings.HistoryUserClass::HistoryUser:True:", new HistoryUserClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ColumnWidthClass::ColumnWidth:True:", new ColumnWidthClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.FileSave::FileSave:True:", new FileSaveSerializer());
                    hashtable.Add("Naccs.Core.Settings.TermMessageSetClass::TermMessageSet:True:", new TermMessageSetClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.OptionCertification::OptionCertification:True:", new OptionCertificationSerializer());
                    hashtable.Add("Naccs.Core.Settings.OutCodeClass::OutCode:True:", new OutCodeClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.InteractiveServerClass::", new InteractiveServerClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ReceiveNotice::ReceiveNotice:True:", new ReceiveNoticeSerializer());
                    hashtable.Add("Naccs.Core.Settings.MailInfoSysClass::", new MailInfoSysClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.DatasetStorage::", new DatasetStorageSerializer());
                    hashtable.Add("Naccs.Core.Settings.AbstractSettings::", new AbstractSettingsSerializer());
                    hashtable.Add("Naccs.Core.Settings.TermListClass::", new TermListClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.PrinterInfoListClass::", new PrinterInfoListClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.ResourceManager::", new ResourceManagerSerializer());
                    hashtable.Add("Naccs.Core.Settings.StringList::", new StringListSerializer());
                    hashtable.Add("Naccs.Core.Classes.TestServer::", new TestServerSerializer());
                    hashtable.Add("Naccs.Core.Settings.DetailsListClass::", new DetailsListClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.UserPathNames::UserPathNames:True:", new UserPathNamesSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.DatasetRemove::", new DatasetRemoveSerializer());
                    hashtable.Add("Naccs.Core.Settings.SystemEnvironment::SystemEnvironment:True:", new SystemEnvironmentSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.RemoveWorkerArgs::", new RemoveWorkerArgsSerializer());
                    hashtable.Add("Naccs.Core.Settings.TypeClass::Type:True:", new TypeClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.JobKeyListClass::", new JobKeyListClassSerializer());
                    hashtable.Add("Naccs.Core.Job.JobFormFactory::", new JobFormFactorySerializer());
                    hashtable.Add("Naccs.Core.DataView.UDataView+NodeSorter::", new NodeSorterSerializer());
                    hashtable.Add("Naccs.Core.Settings.SendFileClass::SendFile:True:", new SendFileClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.PathInfo::path:True:", new PathInfoSerializer());
                    hashtable.Add("Naccs.Core.Settings.HelpPathClass::HelpPath:True:", new HelpPathClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.DefaultInfoClass::DefaultInfo:True:", new DefaultInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.CommonPathClass::CommonPath:True:", new CommonPathClassSerializer());
                    hashtable.Add("Naccs.Core.BatchDoc.OutcodeTbl::", new OutcodeTblSerializer());
                    hashtable.Add("Naccs.Core.Settings.LogFileLengthClass::LogFileLength:True:", new LogFileLengthClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.CustomizeMenu::", new CustomizeMenuSerializer());
                    hashtable.Add("Naccs.Core.Settings.SettingDivType::", new SettingDivTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.VersionSettings::", new VersionSettingsSerializer());
                    hashtable.Add("Naccs.Core.Settings.InteractiveInfoSysClass::", new InteractiveInfoSysClassSerializer());
                    hashtable.Add("Naccs.Core.Job.JobPanelStatus::", new JobPanelStatusSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.RevertEventArgs::", new RevertEventArgsSerializer());
                    hashtable.Add("Naccs.Core.Settings.HistorySearchClass::HistorySearch:True:", new HistorySearchClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.KeyListClass::", new KeyListClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.ULogClass::", new ULogClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.InformClass::Inform:True:", new InformClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.FormsClass::Forms:True:", new FormsClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.PrintSetups::Printer:True:", new PrintSetupsSerializer());
                    hashtable.Add("Naccs.Core.Settings.TermClass::Term:True:", new TermClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.DatasetRevert::", new DatasetRevertSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.WriterLog::", new WriterLogSerializer());
                    hashtable.Add("Naccs.Core.Settings.PrinterInfoClass::PrinterInfo:True:", new PrinterInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.BinClass::Bin:True:", new BinClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.Signflg::", new SignflgSerializer());
                    hashtable.Add("Naccs.Core.Settings.GStampSettings::GStompSettings:True:", new GStampSettingsSerializer());
                    hashtable.Add("Naccs.Core.Settings.UserEnvironment::UserEnvironment:True:", new UserEnvironmentSerializer());
                    hashtable.Add("Naccs.Core.Settings.HelpSettings::Help:True:", new HelpSettingsSerializer());
                    hashtable.Add("Naccs.Core.Settings.ReceiveInformListClass::", new ReceiveInformListClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.DataCompress+IndexRec::", new IndexRecSerializer());
                    hashtable.Add("Naccs.Core.Settings.UserKindType::", new UserKindTypeSerializer());
                    hashtable.Add("Naccs.Core.Main.VersionuptoolUpdater::", new VersionuptoolUpdaterSerializer());
                    hashtable.Add("Naccs.Core.Settings.User::User:True:", new UserSerializer());
                    hashtable.Add("Naccs.Core.Settings.BatchSendClass::BatchSend:True:", new BatchSendClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.BatchdocInfoSysClass::BatchdocInfo:True:", new BatchdocInfoSysClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.FileNameClass::FileName:True:", new FileNameClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ConfigFiles::", new ConfigFilesSerializer());
                    hashtable.Add("Naccs.Core.BatchDoc.OutcodeTbl+OutcodeRec::", new OutcodeRecSerializer());
                    hashtable.Add("Naccs.Core.Settings.MessageClassify::MessageClassify:True:", new MessageClassifySerializer());
                    hashtable.Add("Naccs.Core.Settings.PathItem::", new PathItemSerializer());
                    hashtable.Add("Naccs.Core.Settings.WindowClass::Window:True:", new WindowClassSerializer());
                    hashtable.Add("Naccs.Core.Print.PrintManager::", new PrintManagerSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.ArrangeDataTable::", new ArrangeDataTableSerializer());
                    hashtable.Add("Naccs.Core.Settings.ResultCodeColorClass::ResultCodeColor:True:", new ResultCodeColorClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.HistoryJobsClass::HistoryJobs:True:", new HistoryJobsClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.DataViewClass::DataView:True:", new DataViewClassSerializer());
                    hashtable.Add("Naccs.Core.Job.ModeFont::", new ModeFontSerializer());
                    hashtable.Add("Naccs.Core.Main.ComboManager::", new ComboManagerSerializer());
                    hashtable.Add("Naccs.Core.Classes.ButtonPatern::", new ButtonPaternSerializer());
                    hashtable.Add("Naccs.Core.Settings.DetailsClass::Details:True:", new DetailsClassSerializer());
                    hashtable.Add("Naccs.Core.Main.UJobMenu+ListRec::", new ListRecSerializer());
                    hashtable.Add("Naccs.Core.Settings.InteractiveInfoClass::InteractiveInfo:True:", new InteractiveInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.SoundClass::Sound:True:", new SoundClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.MailServerClass::", new MailServerClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.ContainedType::", new ContainedTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.WarningInformClass::WarningInform:True:", new WarningInformClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.UserKey::UserKey:True:", new UserKeySerializer());
                    hashtable.Add("Naccs.Core.Settings.AbstractConfig::", new AbstractConfigSerializer());
                    hashtable.Add("Naccs.Core.Settings.HttpOptionClass::HttpOption:True:", new HttpOptionClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.DataViewFormClass::DataViewForm:True:", new DataViewFormClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.ArrangeProgress+ProgresType::", new ProgresTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.JobOptionClass::JobOption:True:", new JobOptionClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.JobKeyClass::JobKey:True:", new JobKeyClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.Arrange.RemoveWorkerResult::", new RemoveWorkerResultSerializer());
                    hashtable.Add("Naccs.Core.Classes.NetTestMode::", new NetTestModeSerializer());
                    hashtable.Add("Naccs.Core.Settings.DebugFunctionClass::DebugFunction:True:", new DebugFunctionClassSerializer());
                    hashtable.Add("Naccs.Core.DataView.USendReportDlg+ReportStatus::", new ReportStatusSerializer());
                    hashtable.Add("Naccs.Core.Settings.BinListClass::", new BinListClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.KeyClass::Key:True:", new KeyClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ProtocolType::", new ProtocolTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.TerminalInfoClass::TerminalInfo:True:", new TerminalInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.TermMessageSetListClass::", new TermMessageSetListClassSerializer());
                    hashtable.Add("Naccs.Core.Option.GatewaySettingClass::", new GatewaySettingClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.LastLogonClass::LastLogon:True:", new LastLogonClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.MessageKind::", new MessageKindSerializer());
                    hashtable.Add("Naccs.Core.Classes.UserKeySet::", new UserKeySetSerializer());
                    hashtable.Add("Naccs.Core.Settings.GStampInfoClass::GStompInfo:True:", new GStampInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.DataControl::", new DataControlSerializer());
                    hashtable.Add("Naccs.Core.Settings.ListItem::", new ListItemSerializer());
                    hashtable.Add("Naccs.Core.Main.ListType::", new ListTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.HistoryAddressClass::HistoryAddress:True:", new HistoryAddressClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.PathType::", new PathTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.MailInfoClass::MailInfo:True:", new MailInfoClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.OutCodeListClass::", new OutCodeListClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.LogFileLengthSysClass::", new LogFileLengthSysClassSerializer());
                    hashtable.Add("Naccs.Core.Print.PrintStatus::", new PrintStatusSerializer());
                    hashtable.Add("Naccs.Core.Classes.DataFactory::", new DataFactorySerializer());
                    hashtable.Add("Naccs.Core.Settings.AllFileSaveClass::AllFileSave:True:", new AllFileSaveClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ApplicationClass::Application:True:", new ApplicationClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.TypeListClass::", new TypeListClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ReceiveInformClass::ReceiveInform:True:", new ReceiveInformClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.NaccsData::ExData:True:", new NaccsDataSerializer());
                    hashtable.Add("Naccs.Core.Settings.AllCertificationClass::AllCertification:True:", new AllCertificationClassSerializer());
                    hashtable.Add("Naccs.Core.Settings.ToolBarClass::ToolBar:True:", new ToolBarClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.DataStatus::", new DataStatusSerializer());
                    hashtable.Add("Naccs.Core.Settings.TermMessage::TermMessage:True:", new TermMessageSerializer());
                    hashtable.Add("Naccs.Core.Settings.KeyListClass+UserKeyType::", new UserKeyTypeSerializer());
                    hashtable.Add("Naccs.Core.Settings.TerminalInfoSysClass::", new TerminalInfoSysClassSerializer());
                    hashtable.Add("Naccs.Core.Classes.NaccsHeader::", new NaccsHeaderSerializer());
                    if (this.typedSerializers == null)
                    {
                        this.typedSerializers = hashtable;
                    }
                }
                return this.typedSerializers;
            }
        }

        public override Hashtable WriteMethods
        {
            get
            {
                if (this.writeMethods == null)
                {
                    Hashtable hashtable = new Hashtable();
                    hashtable["Naccs.Core.Settings.AbstractSettings::"] = "Write118_AbstractSettings";
                    hashtable["Naccs.Core.Settings.AbstractConfig::"] = "Write119_AbstractConfig";
                    hashtable["Naccs.Core.Settings.ReceiveNotice::ReceiveNotice:True:"] = "Write120_ReceiveNotice";
                    hashtable["Naccs.Core.Settings.ListItem::"] = "Write121_ListItem";
                    hashtable["Naccs.Core.Settings.ReceiveInformClass::ReceiveInform:True:"] = "Write122_ReceiveInform";
                    hashtable["Naccs.Core.Settings.SoundClass::Sound:True:"] = "Write123_Sound";
                    hashtable["Naccs.Core.Settings.WarningInformClass::WarningInform:True:"] = "Write124_WarningInform";
                    hashtable["Naccs.Core.Settings.InformClass::Inform:True:"] = "Write125_Inform";
                    hashtable["Naccs.Core.Settings.PrintSetups::Printer:True:"] = "Write126_Printer";
                    hashtable["Naccs.Core.Settings.DefaultInfoClass::DefaultInfo:True:"] = "Write127_DefaultInfo";
                    hashtable["Naccs.Core.Settings.PrinterInfoClass::PrinterInfo:True:"] = "Write128_PrinterInfo";
                    hashtable["Naccs.Core.Settings.BinClass::Bin:True:"] = "Write129_Bin";
                    hashtable["Naccs.Core.Settings.FormsClass::Forms:True:"] = "Write130_Forms";
                    hashtable["Naccs.Core.Settings.OutCodeClass::OutCode:True:"] = "Write131_OutCode";
                    hashtable["Naccs.Core.Settings.PathInfo::path:True:"] = "Write132_path";
                    hashtable["Naccs.Core.Settings.PathItem::"] = "Write133_PathItem";
                    hashtable["Naccs.Core.Settings.PathType::"] = "Write134_PathType";
                    hashtable["Naccs.Core.Settings.ProtocolType::"] = "Write135_ProtocolType";
                    hashtable["Naccs.Core.Settings.SettingDivType::"] = "Write136_SettingDivType";
                    hashtable["Naccs.Core.Settings.UserKindType::"] = "Write137_UserKindType";
                    hashtable["Naccs.Core.Settings.KeyListClass::"] = "Write138_KeyListClass";
                    hashtable["Naccs.Core.Settings.KeyListClass+UserKeyType::"] = "Write139_UserKeyType";
                    hashtable["Naccs.Core.Settings.JobKeyListClass::"] = "Write140_ArrayOfJobKeyClass";
                    hashtable["Naccs.Core.Settings.UserEnvironment::UserEnvironment:True:"] = "Write141_UserEnvironment";
                    hashtable["Naccs.Core.Settings.TerminalInfoClass::TerminalInfo:True:"] = "Write142_TerminalInfo";
                    hashtable["Naccs.Core.Settings.DebugFunctionClass::DebugFunction:True:"] = "Write143_DebugFunction";
                    hashtable["Naccs.Core.Settings.InteractiveInfoClass::InteractiveInfo:True:"] = "Write144_InteractiveInfo";
                    hashtable["Naccs.Core.Settings.MailInfoClass::MailInfo:True:"] = "Write145_MailInfo";
                    hashtable["Naccs.Core.Settings.HttpOptionClass::HttpOption:True:"] = "Write146_HttpOption";
                    hashtable["Naccs.Core.Settings.LogFileLengthClass::LogFileLength:True:"] = "Write147_LogFileLength";
                    hashtable["Naccs.Core.Settings.OutCodeListClass::"] = "Write148_ArrayOfOutCodeClass1";
                    hashtable["Naccs.Core.Settings.BinListClass::"] = "Write149_ArrayOfBinClass1";
                    hashtable["Naccs.Core.DataView.Arrange.WriterLog::"] = "Write150_WriterLog";
                    hashtable["Naccs.Core.Classes.NaccsHeader::"] = "Write151_NaccsHeader";
                    hashtable["Naccs.Core.Classes.DataControl::"] = "Write152_DataControl";
                    hashtable["Naccs.Core.Settings.TermMessageSetListClass::"] = "Write153_ArrayOfTermMessageSetClass";
                    hashtable["Naccs.Core.Settings.SystemEnvironment::SystemEnvironment:True:"] = "Write154_SystemEnvironment";
                    hashtable["Naccs.Core.Settings.TerminalInfoSysClass::"] = "Write155_TerminalInfoSysClass";
                    hashtable["Naccs.Core.Settings.InteractiveServerClass::"] = "Write156_InteractiveServerClass";
                    hashtable["Naccs.Core.Settings.InteractiveInfoSysClass::"] = "Write157_InteractiveInfoSysClass";
                    hashtable["Naccs.Core.Settings.MailServerClass::"] = "Write158_MailServerClass";
                    hashtable["Naccs.Core.Settings.MailInfoSysClass::"] = "Write159_MailInfoSysClass";
                    hashtable["Naccs.Core.Settings.BatchdocInfoSysClass::BatchdocInfo:True:"] = "Write160_BatchdocInfo";
                    hashtable["Naccs.Core.Settings.LogFileLengthSysClass::"] = "Write161_LogFileLengthSysClass";
                    hashtable["Naccs.Core.Main.UJobMenu+ListRec::"] = "Write162_ListRec";
                    hashtable["Naccs.Core.DataView.UDataView+NodeSorter::"] = "Write163_NodeSorter";
                    hashtable["Naccs.Core.DataView.DataCompress+IndexRec::"] = "Write164_IndexRec";
                    hashtable["Naccs.Core.Classes.TestServer::"] = "Write165_TestServer";
                    hashtable["Naccs.Core.Classes.NetTestMode::"] = "Write166_NetTestMode";
                    hashtable["Naccs.Core.Settings.User::User:True:"] = "Write167_User";
                    hashtable["Naccs.Core.Settings.ApplicationClass::Application:True:"] = "Write168_Application";
                    hashtable["Naccs.Core.Settings.ToolBarClass::ToolBar:True:"] = "Write169_ToolBar";
                    hashtable["Naccs.Core.Settings.WindowClass::Window:True:"] = "Write170_Window";
                    hashtable["Naccs.Core.Settings.LastLogonClass::LastLogon:True:"] = "Write171_LastLogon";
                    hashtable["Naccs.Core.Settings.HistoryJobsClass::HistoryJobs:True:"] = "Write172_HistoryJobs";
                    hashtable["Naccs.Core.Settings.ColumnWidthClass::ColumnWidth:True:"] = "Write173_ColumnWidth";
                    hashtable["Naccs.Core.Settings.DataViewFormClass::DataViewForm:True:"] = "Write174_DataViewForm";
                    hashtable["Naccs.Core.Settings.ResultCodeColorClass::ResultCodeColor:True:"] = "Write175_ResultCodeColor";
                    hashtable["Naccs.Core.Settings.JobOptionClass::JobOption:True:"] = "Write176_JobOption";
                    hashtable["Naccs.Core.Settings.BatchSendClass::BatchSend:True:"] = "Write177_BatchSend";
                    hashtable["Naccs.Core.Settings.HistoryUserClass::HistoryUser:True:"] = "Write178_HistoryUser";
                    hashtable["Naccs.Core.Settings.HistoryAddressClass::HistoryAddress:True:"] = "Write179_HistoryAddress";
                    hashtable["Naccs.Core.Settings.HistorySearchClass::HistorySearch:True:"] = "Write180_HistorySearch";
                    hashtable["Naccs.Core.Settings.DetailsListClass::"] = "Write181_ArrayOfDetailsClass";
                    hashtable["Naccs.Core.Option.GatewaySettingClass::"] = "Write182_GatewaySettingClass";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetRevert::"] = "Write183_DatasetRevert";
                    hashtable["Naccs.Core.Print.PrintStatus::"] = "Write184_PrintStatus";
                    hashtable["Naccs.Core.DataView.Arrange.RevertEventArgs::"] = "Write185_RevertEventArgs";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetStorage::"] = "Write186_DatasetStorage";
                    hashtable["Naccs.Core.DataView.Arrange.ArrangeDataTable::"] = "Write187_ArrangeDataTable";
                    hashtable["Naccs.Core.Classes.CustomizeMenu::"] = "Write188_CustomizeMenu";
                    hashtable["Naccs.Core.Settings.UserPathNames::UserPathNames:True:"] = "Write189_UserPathNames";
                    hashtable["Naccs.Core.Settings.CommonPathClass::CommonPath:True:"] = "Write190_CommonPath";
                    hashtable["Naccs.Core.DataView.Arrange.RemoveWorkerArgs::"] = "Write191_RemoveWorkerArgs";
                    hashtable["Naccs.Core.Settings.HelpSettings::Help:True:"] = "Write192_Help";
                    hashtable["Naccs.Core.Settings.HelpPathClass::HelpPath:True:"] = "Write193_HelpPath";
                    hashtable["Naccs.Core.Settings.ConfigFiles::"] = "Write194_ConfigFiles";
                    hashtable["Naccs.Core.DataView.Arrange.RemoveWorkerResult::"] = "Write195_RemoveWorkerResult";
                    hashtable["Naccs.Core.DataView.Arrange.DatasetRemove::"] = "Write196_DatasetRemove";
                    hashtable["Naccs.Core.Settings.OptionCertification::OptionCertification:True:"] = "Write197_OptionCertification";
                    hashtable["Naccs.Core.Settings.AllCertificationClass::AllCertification:True:"] = "Write198_AllCertification";
                    hashtable["Naccs.Core.Main.ComboManager::"] = "Write199_ComboManager";
                    hashtable["Naccs.Core.Classes.DataFactory::"] = "Write200_DataFactory";
                    hashtable["Naccs.Core.Settings.UserKey::UserKey:True:"] = "Write201_UserKey";
                    hashtable["Naccs.Core.Settings.JobKeyClass::JobKey:True:"] = "Write202_JobKey";
                    hashtable["Naccs.Core.Settings.KeyClass::Key:True:"] = "Write203_Key";
                    hashtable["Naccs.Core.Settings.PrinterInfoListClass::"] = "Write204_ArrayOfPrinterInfoClass1";
                    hashtable["Naccs.Core.Settings.StringList::"] = "Write205_ArrayOfString4";
                    hashtable["Naccs.Core.Job.JobFormFactory::"] = "Write206_JobFormFactory";
                    hashtable["Naccs.Core.Classes.ULogClass::"] = "Write207_ULogClass";
                    hashtable["Naccs.Core.Job.ModeFont::"] = "Write208_ModeFont";
                    hashtable["Naccs.Core.Job.JobPanelStatus::"] = "Write209_JobPanelStatus";
                    hashtable["Naccs.Core.Main.VersionuptoolUpdater::"] = "Write210_VersionuptoolUpdater";
                    hashtable["Naccs.Core.DataView.Arrange.ArrangeProgress+ProgresType::"] = "Write211_ProgresType";
                    hashtable["Naccs.Core.Settings.VersionSettings::"] = "Write212_VersionSettings";
                    hashtable["Naccs.Core.Print.PrintManager::"] = "Write213_PrintManager";
                    hashtable["Naccs.Core.DataView.USendReportDlg+ReportStatus::"] = "Write214_ReportStatus";
                    hashtable["Naccs.Core.Classes.DataStatus::"] = "Write215_DataStatus";
                    hashtable["Naccs.Core.Classes.Signflg::"] = "Write216_Signflg";
                    hashtable["Naccs.Core.Classes.ContainedType::"] = "Write217_ContainedType";
                    hashtable["Naccs.Core.BatchDoc.OutcodeTbl::"] = "Write218_OutcodeTbl";
                    hashtable["Naccs.Core.BatchDoc.OutcodeTbl+OutcodeRec::"] = "Write219_OutcodeRec";
                    hashtable["Naccs.Core.Settings.MessageClassify::MessageClassify:True:"] = "Write220_MessageClassify";
                    hashtable["Naccs.Core.Settings.TermClass::Term:True:"] = "Write221_Term";
                    hashtable["Naccs.Core.Settings.DataViewClass::DataView:True:"] = "Write222_DataView";
                    hashtable["Naccs.Core.Settings.TermListClass::"] = "Write223_ArrayOfTermClass1";
                    hashtable["Naccs.Core.Classes.UserKeySet::"] = "Write224_UserKeySet";
                    hashtable["Naccs.Core.Settings.TypeListClass::"] = "Write225_ArrayOfTypeClass";
                    hashtable["Naccs.Core.Settings.GStampSettings::GStompSettings:True:"] = "Write226_GStompSettings";
                    hashtable["Naccs.Core.Settings.GStampInfoClass::GStompInfo:True:"] = "Write227_GStompInfo";
                    hashtable["Naccs.Core.Settings.FileSave::FileSave:True:"] = "Write228_FileSave";
                    hashtable["Naccs.Core.Settings.AllFileSaveClass::AllFileSave:True:"] = "Write229_AllFileSave";
                    hashtable["Naccs.Core.Settings.TypeClass::Type:True:"] = "Write230_Type";
                    hashtable["Naccs.Core.Settings.FileNameClass::FileName:True:"] = "Write231_FileName";
                    hashtable["Naccs.Core.Settings.SendFileClass::SendFile:True:"] = "Write232_SendFile";
                    hashtable["Naccs.Core.Settings.DetailsClass::Details:True:"] = "Write233_Details";
                    hashtable["Naccs.Core.Main.ListType::"] = "Write234_ListType";
                    hashtable["Naccs.Core.Settings.TermMessage::TermMessage:True:"] = "Write235_TermMessage";
                    hashtable["Naccs.Core.Settings.TermMessageSetClass::TermMessageSet:True:"] = "Write236_TermMessageSet";
                    hashtable["Naccs.Core.Settings.ReceiveInformListClass::"] = "Write237_ArrayOfReceiveInformClass1";
                    hashtable["Naccs.Core.Classes.ResourceManager::"] = "Write238_ResourceManager";
                    hashtable["Naccs.Core.Classes.NaccsData::ExData:True:"] = "Write239_ExData";
                    hashtable["Naccs.Core.Classes.ButtonPatern::"] = "Write240_ButtonPatern";
                    hashtable["Naccs.Core.Classes.MessageKind::"] = "Write241_MessageKind";
                    if (this.writeMethods == null)
                    {
                        this.writeMethods = hashtable;
                    }
                }
                return this.writeMethods;
            }
        }

        public override XmlSerializationWriter Writer
        {
            get
            {
                return new XmlSerializationWriter1();
            }
        }
    }
}

