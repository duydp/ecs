﻿namespace Company.Interface
{
    partial class VNACC_ChuyenCuaKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChuyenCuaKhau));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.cboPhanLoaiXuatNhap = new Janus.Windows.EditControls.UIComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoToKhaiVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHangHoa");
            this.cmdContainer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdSoTKXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSoTKXuat");
            this.cmdTrungChuyen1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdInTamOLA1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamOLA");
            this.cmdKetQuaHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaHQ");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdHangHoa");
            this.cmdContainer = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdOLA1 = new Janus.Windows.UI.CommandBars.UICommand("cmdOLA");
            this.cmdOLC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdOLC");
            this.cmdKhaiBaoSua1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoSua");
            this.cmdBOT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBOT");
            this.cmdKhaiBaoHuy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoHuy");
            this.cmdKetQuaHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaHQ");
            this.cmdSoTKXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdSoTKXuat");
            this.cmdKhaiBaoSua = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoSua");
            this.cmdOLC = new Janus.Windows.UI.CommandBars.UICommand("cmdOLC");
            this.cmdOLA = new Janus.Windows.UI.CommandBars.UICommand("cmdOLA");
            this.cmdBOT = new Janus.Windows.UI.CommandBars.UICommand("cmdBOT");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdTrungChuyen = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdKhaiBaoHuy = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoHuy");
            this.cmdInTamOLA = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamOLA");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.label26 = new System.Windows.Forms.Label();
            this.chkDungChungSoQuanLy = new Janus.Windows.EditControls.UICheckBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenNha_VC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiNha_VC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNhaVanChuyen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGioKetThuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGioBatDau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTuyenDuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKyHieuCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cbbLoaiBaoLanh = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTienBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNamPhatHanhBL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 808), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 808);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 784);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 784);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox9);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(806, 808);
            // 
            // cboPhanLoaiXuatNhap
            // 
            this.cboPhanLoaiXuatNhap.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Xuất khẩu";
            uiComboBoxItem4.Value = "E";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Nhập khẩu";
            uiComboBoxItem5.Value = "I";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Loại khác";
            uiComboBoxItem6.Value = "C";
            this.cboPhanLoaiXuatNhap.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cboPhanLoaiXuatNhap.Location = new System.Drawing.Point(171, 87);
            this.cboPhanLoaiXuatNhap.Name = "cboPhanLoaiXuatNhap";
            this.cboPhanLoaiXuatNhap.SelectedIndex = 1;
            this.cboPhanLoaiXuatNhap.Size = new System.Drawing.Size(123, 21);
            this.cboPhanLoaiXuatNhap.TabIndex = 0;
            this.cboPhanLoaiXuatNhap.Text = "Nhập khẩu";
            this.cboPhanLoaiXuatNhap.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(15, 92);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(128, 13);
            this.label24.TabIndex = 44;
            this.label24.Text = "Phân loại xuất nhập khẩu";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(15, 59);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 47;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Số tờ khai";
            // 
            // txtSoToKhaiVC
            // 
            this.txtSoToKhaiVC.Enabled = false;
            this.txtSoToKhaiVC.Location = new System.Drawing.Point(111, 24);
            this.txtSoToKhaiVC.Name = "txtSoToKhaiVC";
            this.txtSoToKhaiVC.Size = new System.Drawing.Size(128, 21);
            this.txtSoToKhaiVC.TabIndex = 0;
            this.txtSoToKhaiVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHangHoa,
            this.cmdContainer,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdKetQuaHQ,
            this.cmdSoTKXuat,
            this.cmdKhaiBaoSua,
            this.cmdOLC,
            this.cmdOLA,
            this.cmdBOT,
            this.cmdKetQuaXuLy,
            this.cmdTrungChuyen,
            this.cmdKhaiBaoHuy,
            this.cmdInTamOLA});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("6766954b-d96b-4ffe-98f7-903db524103e");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 636);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(837, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHangHoa1,
            this.cmdContainer1,
            this.cmdSoTKXuat1,
            this.cmdTrungChuyen1,
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdInTamOLA1,
            this.cmdKetQuaHQ1,
            this.cmdKetQuaXuLy1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1012, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdHangHoa1
            // 
            this.cmdHangHoa1.Key = "cmdHangHoa";
            this.cmdHangHoa1.Name = "cmdHangHoa1";
            // 
            // cmdContainer1
            // 
            this.cmdContainer1.Key = "cmdContainer";
            this.cmdContainer1.Name = "cmdContainer1";
            // 
            // cmdSoTKXuat1
            // 
            this.cmdSoTKXuat1.Key = "cmdSoTKXuat";
            this.cmdSoTKXuat1.Name = "cmdSoTKXuat1";
            // 
            // cmdTrungChuyen1
            // 
            this.cmdTrungChuyen1.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen1.Name = "cmdTrungChuyen1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdInTamOLA1
            // 
            this.cmdInTamOLA1.Key = "cmdInTamOLA";
            this.cmdInTamOLA1.Name = "cmdInTamOLA1";
            // 
            // cmdKetQuaHQ1
            // 
            this.cmdKetQuaHQ1.Key = "cmdKetQuaHQ";
            this.cmdKetQuaHQ1.Name = "cmdKetQuaHQ1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdHangHoa
            // 
            this.cmdHangHoa.Image = ((System.Drawing.Image)(resources.GetObject("cmdHangHoa.Image")));
            this.cmdHangHoa.Key = "cmdHangHoa";
            this.cmdHangHoa.Name = "cmdHangHoa";
            this.cmdHangHoa.Text = "Hàng hóa";
            // 
            // cmdContainer
            // 
            this.cmdContainer.Image = ((System.Drawing.Image)(resources.GetObject("cmdContainer.Image")));
            this.cmdContainer.Key = "cmdContainer";
            this.cmdContainer.Name = "cmdContainer";
            this.cmdContainer.Text = "Container";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdOLA1,
            this.cmdOLC1,
            this.cmdKhaiBaoSua1,
            this.cmdBOT1,
            this.cmdKhaiBaoHuy1});
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdOLA1
            // 
            this.cmdOLA1.Image = ((System.Drawing.Image)(resources.GetObject("cmdOLA1.Image")));
            this.cmdOLA1.Key = "cmdOLA";
            this.cmdOLA1.Name = "cmdOLA1";
            // 
            // cmdOLC1
            // 
            this.cmdOLC1.Image = ((System.Drawing.Image)(resources.GetObject("cmdOLC1.Image")));
            this.cmdOLC1.Key = "cmdOLC";
            this.cmdOLC1.Name = "cmdOLC1";
            // 
            // cmdKhaiBaoSua1
            // 
            this.cmdKhaiBaoSua1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBaoSua1.Image")));
            this.cmdKhaiBaoSua1.Key = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua1.Name = "cmdKhaiBaoSua1";
            // 
            // cmdBOT1
            // 
            this.cmdBOT1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBOT1.Image")));
            this.cmdBOT1.Key = "cmdBOT";
            this.cmdBOT1.Name = "cmdBOT1";
            this.cmdBOT1.Text = "Khai báo sửa (COT)";
            // 
            // cmdKhaiBaoHuy1
            // 
            this.cmdKhaiBaoHuy1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBaoHuy1.Image")));
            this.cmdKhaiBaoHuy1.Key = "cmdKhaiBaoHuy";
            this.cmdKhaiBaoHuy1.Name = "cmdKhaiBaoHuy1";
            this.cmdKhaiBaoHuy1.Text = "Khai báo hủy (COT)";
            // 
            // cmdKetQuaHQ
            // 
            this.cmdKetQuaHQ.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaHQ.Image")));
            this.cmdKetQuaHQ.Key = "cmdKetQuaHQ";
            this.cmdKetQuaHQ.Name = "cmdKetQuaHQ";
            this.cmdKetQuaHQ.Text = "Thông tin từ HQ";
            // 
            // cmdSoTKXuat
            // 
            this.cmdSoTKXuat.Image = ((System.Drawing.Image)(resources.GetObject("cmdSoTKXuat.Image")));
            this.cmdSoTKXuat.Key = "cmdSoTKXuat";
            this.cmdSoTKXuat.Name = "cmdSoTKXuat";
            this.cmdSoTKXuat.Text = "Tờ khai xuất";
            // 
            // cmdKhaiBaoSua
            // 
            this.cmdKhaiBaoSua.Key = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Name = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Text = "Chuyển sang trạng thái sửa";
            // 
            // cmdOLC
            // 
            this.cmdOLC.Key = "cmdOLC";
            this.cmdOLC.Name = "cmdOLC";
            this.cmdOLC.Text = "Khai báo chính thức (OLC)";
            // 
            // cmdOLA
            // 
            this.cmdOLA.Key = "cmdOLA";
            this.cmdOLA.Name = "cmdOLA";
            this.cmdOLA.Text = "Khai báo tạm (OLA)";
            // 
            // cmdBOT
            // 
            this.cmdBOT.Key = "cmdBOT";
            this.cmdBOT.Name = "cmdBOT";
            this.cmdBOT.Text = "Khai báo sửa (BOT)";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy.Image")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdTrungChuyen
            // 
            this.cmdTrungChuyen.Image = ((System.Drawing.Image)(resources.GetObject("cmdTrungChuyen.Image")));
            this.cmdTrungChuyen.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen.Name = "cmdTrungChuyen";
            this.cmdTrungChuyen.Text = "Trung chuyển";
            // 
            // cmdKhaiBaoHuy
            // 
            this.cmdKhaiBaoHuy.Key = "cmdKhaiBaoHuy";
            this.cmdKhaiBaoHuy.Name = "cmdKhaiBaoHuy";
            this.cmdKhaiBaoHuy.Text = "Khai báo hủy";
            // 
            // cmdInTamOLA
            // 
            this.cmdInTamOLA.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTamOLA.Image")));
            this.cmdInTamOLA.Key = "cmdInTamOLA";
            this.cmdInTamOLA.Name = "cmdInTamOLA";
            this.cmdInTamOLA.Text = "In tờ khai tạm(OLA)";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 608);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(837, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 608);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1012, 28);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(310, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 43;
            this.label26.Text = "Ngày đăng ký";
            // 
            // chkDungChungSoQuanLy
            // 
            this.chkDungChungSoQuanLy.BackColor = System.Drawing.Color.Transparent;
            this.chkDungChungSoQuanLy.Location = new System.Drawing.Point(316, 87);
            this.chkDungChungSoQuanLy.Name = "chkDungChungSoQuanLy";
            this.chkDungChungSoQuanLy.Size = new System.Drawing.Size(270, 23);
            this.chkDungChungSoQuanLy.TabIndex = 49;
            this.chkDungChungSoQuanLy.Text = "Dùng chung số quản lý hàng hóa với tờ khai VC khác";
            this.chkDungChungSoQuanLy.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.chkDungChungSoQuanLy);
            this.uiGroupBox7.Controls.Add(this.txtSoToKhaiVC);
            this.uiGroupBox7.Controls.Add(this.label1);
            this.uiGroupBox7.Controls.Add(this.label26);
            this.uiGroupBox7.Controls.Add(this.label27);
            this.uiGroupBox7.Controls.Add(this.label24);
            this.uiGroupBox7.Controls.Add(this.cboPhanLoaiXuatNhap);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 118);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(789, 126);
            this.uiGroupBox7.TabIndex = 50;
            this.uiGroupBox7.Text = "Thông tin chung";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtTenNha_VC);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiNha_VC);
            this.uiGroupBox1.Controls.Add(this.txtMaNhaVanChuyen);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 244);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(789, 116);
            this.uiGroupBox1.TabIndex = 51;
            this.uiGroupBox1.Text = "Nhà vận chuyển";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(21, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Địa chỉ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(21, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Tên";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(21, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã";
            // 
            // txtTenNha_VC
            // 
            this.txtTenNha_VC.Location = new System.Drawing.Point(102, 45);
            this.txtTenNha_VC.Name = "txtTenNha_VC";
            this.txtTenNha_VC.Size = new System.Drawing.Size(510, 21);
            this.txtTenNha_VC.TabIndex = 1;
            this.txtTenNha_VC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiNha_VC
            // 
            this.txtDiaChiNha_VC.Location = new System.Drawing.Point(102, 77);
            this.txtDiaChiNha_VC.Name = "txtDiaChiNha_VC";
            this.txtDiaChiNha_VC.Size = new System.Drawing.Size(510, 21);
            this.txtDiaChiNha_VC.TabIndex = 2;
            this.txtDiaChiNha_VC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNhaVanChuyen
            // 
            this.txtMaNhaVanChuyen.Location = new System.Drawing.Point(101, 16);
            this.txtMaNhaVanChuyen.Name = "txtMaNhaVanChuyen";
            this.txtMaNhaVanChuyen.Size = new System.Drawing.Size(138, 21);
            this.txtMaNhaVanChuyen.TabIndex = 0;
            this.txtMaNhaVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(23, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số hợp đồng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(248, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ngày hợp đồng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(431, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày hết hạn ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(23, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mã phương tiện";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(248, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Mã mục đích";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(431, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Loại hình vận tải";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(24, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ngày dự kiến bắt đầu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(313, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày dự kiến kết thúc";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(533, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "giờ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(244, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "giờ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtGioKetThuc);
            this.uiGroupBox2.Controls.Add(this.txtGioBatDau);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(789, 118);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.Text = "Hợp đồng vận chuyển";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtGioKetThuc
            // 
            this.txtGioKetThuc.Location = new System.Drawing.Point(560, 82);
            this.txtGioKetThuc.Name = "txtGioKetThuc";
            this.txtGioKetThuc.Size = new System.Drawing.Size(59, 21);
            this.txtGioKetThuc.TabIndex = 9;
            this.txtGioKetThuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGioBatDau
            // 
            this.txtGioBatDau.Location = new System.Drawing.Point(264, 82);
            this.txtGioBatDau.Name = "txtGioBatDau";
            this.txtGioBatDau.Size = new System.Drawing.Size(43, 21);
            this.txtGioBatDau.TabIndex = 7;
            this.txtGioBatDau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(111, 19);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(128, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtTenDiaDiemXepHang);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 360);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(789, 118);
            this.uiGroupBox3.TabIndex = 52;
            this.uiGroupBox3.Text = "Địa điểm xếp hàng";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(101, 79);
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(511, 21);
            this.txtTenDiaDiemXepHang.TabIndex = 3;
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(22, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(196, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Mã (Khu vực chịu sự giám sát Hải quan)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(243, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Cảng/cửa khẩu/ga";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(22, 83);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Tên địa điểm";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(22, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Vị trí xếp hàng";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtTenDiaDiemDoHang);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 478);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(789, 113);
            this.uiGroupBox4.TabIndex = 53;
            this.uiGroupBox4.Text = "Địa điểm dỡ hàng";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemDoHang
            // 
            this.txtTenDiaDiemDoHang.Location = new System.Drawing.Point(103, 80);
            this.txtTenDiaDiemDoHang.Name = "txtTenDiaDiemDoHang";
            this.txtTenDiaDiemDoHang.Size = new System.Drawing.Size(509, 21);
            this.txtTenDiaDiemDoHang.TabIndex = 3;
            this.txtTenDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(23, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(196, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Mã (Khu vực chịu sự giám sát Hải quan)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(229, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Cảng/cửa khẩu/ga";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(23, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Tên địa điểm ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(23, 52);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Vị trí dở hàng";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtTuyenDuong);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 591);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(789, 47);
            this.uiGroupBox5.TabIndex = 54;
            this.uiGroupBox5.Text = "Tuyến đường";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTuyenDuong
            // 
            this.txtTuyenDuong.Location = new System.Drawing.Point(9, 16);
            this.txtTuyenDuong.Name = "txtTuyenDuong";
            this.txtTuyenDuong.Size = new System.Drawing.Size(350, 21);
            this.txtTuyenDuong.TabIndex = 0;
            this.txtTuyenDuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 638);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(789, 189);
            this.uiGroupBox9.TabIndex = 57;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtSoCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtKyHieuCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label34);
            this.uiGroupBox8.Controls.Add(this.cbbLoaiBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtSoTienBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtNamPhatHanhBL);
            this.uiGroupBox8.Controls.Add(this.label23);
            this.uiGroupBox8.Controls.Add(this.label35);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label37);
            this.uiGroupBox8.Controls.Add(this.label25);
            this.uiGroupBox8.Controls.Add(this.label38);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(370, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(416, 178);
            this.uiGroupBox8.TabIndex = 10;
            this.uiGroupBox8.Text = "Bảo lảnh thuế";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // txtSoCTBaoLanh
            // 
            this.txtSoCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTBaoLanh.Location = new System.Drawing.Point(96, 113);
            this.txtSoCTBaoLanh.MaxLength = 12;
            this.txtSoCTBaoLanh.Name = "txtSoCTBaoLanh";
            this.txtSoCTBaoLanh.Size = new System.Drawing.Size(139, 21);
            this.txtSoCTBaoLanh.TabIndex = 4;
            this.txtSoCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKyHieuCTBaoLanh
            // 
            this.txtKyHieuCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTBaoLanh.Location = new System.Drawing.Point(96, 88);
            this.txtKyHieuCTBaoLanh.MaxLength = 12;
            this.txtKyHieuCTBaoLanh.Name = "txtKyHieuCTBaoLanh";
            this.txtKyHieuCTBaoLanh.Size = new System.Drawing.Size(139, 21);
            this.txtKyHieuCTBaoLanh.TabIndex = 3;
            this.txtKyHieuCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Số chứng từ";
            // 
            // cbbLoaiBaoLanh
            // 
            this.cbbLoaiBaoLanh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Bảo lãnh riêng";
            uiComboBoxItem2.Value = "A";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Bảo lãnh chung";
            uiComboBoxItem3.Value = "B";
            this.cbbLoaiBaoLanh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cbbLoaiBaoLanh.Location = new System.Drawing.Point(96, 13);
            this.cbbLoaiBaoLanh.Name = "cbbLoaiBaoLanh";
            this.cbbLoaiBaoLanh.Size = new System.Drawing.Size(139, 21);
            this.cbbLoaiBaoLanh.TabIndex = 0;
            this.cbbLoaiBaoLanh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoTienBaoLanh
            // 
            this.txtSoTienBaoLanh.DecimalDigits = 12;
            this.txtSoTienBaoLanh.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienBaoLanh.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienBaoLanh.Location = new System.Drawing.Point(96, 138);
            this.txtSoTienBaoLanh.MaxLength = 15;
            this.txtSoTienBaoLanh.Name = "txtSoTienBaoLanh";
            this.txtSoTienBaoLanh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienBaoLanh.Size = new System.Drawing.Size(121, 21);
            this.txtSoTienBaoLanh.TabIndex = 5;
            this.txtSoTienBaoLanh.Text = "0";
            this.txtSoTienBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamPhatHanhBL
            // 
            this.txtNamPhatHanhBL.DecimalDigits = 12;
            this.txtNamPhatHanhBL.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamPhatHanhBL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhBL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamPhatHanhBL.Location = new System.Drawing.Point(96, 63);
            this.txtNamPhatHanhBL.MaxLength = 15;
            this.txtNamPhatHanhBL.Name = "txtNamPhatHanhBL";
            this.txtNamPhatHanhBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNamPhatHanhBL.Size = new System.Drawing.Size(53, 21);
            this.txtNamPhatHanhBL.TabIndex = 2;
            this.txtNamPhatHanhBL.Text = "0";
            this.txtNamPhatHanhBL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamPhatHanhBL.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamPhatHanhBL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 142);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 13);
            this.label23.TabIndex = 33;
            this.label23.Text = "Số tiền bảo lãnh";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(4, 92);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Ký hiệu chứng từ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(4, 67);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 33;
            this.label36.Text = "Năm phát hành";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(4, 43);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 33;
            this.label37.Text = "Mã ngân hàng";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(218, 142);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(27, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "VND";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(4, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Loại ";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtGhiChu);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(367, 178);
            this.uiGroupBox6.TabIndex = 9;
            this.uiGroupBox6.Text = "Ghi chú";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGhiChu.Location = new System.Drawing.Point(3, 17);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(361, 158);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_ChuyenCuaKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1012, 842);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseFormHaveGuidPanel.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChuyenCuaKhau";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai chuyển cửa khẩu";
            this.Load += new System.EventHandler(this.VNACC_ChuyenCuaKhau_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIComboBox cboPhanLoaiXuatNhap;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhaiVC;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHangHoa1;
        private Janus.Windows.UI.CommandBars.UICommand cmdContainer1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHangHoa;
        private Janus.Windows.UI.CommandBars.UICommand cmdContainer;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaHQ;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSoTKXuat1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSoTKXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoSua;
        private Janus.Windows.UI.CommandBars.UICommand cmdOLC;
        private Janus.Windows.UI.CommandBars.UICommand cmdOLA;
        private Janus.Windows.UI.CommandBars.UICommand cmdBOT;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen;
        private Janus.Windows.EditControls.UICheckBox chkDungChungSoQuanLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdOLA1;
        private Janus.Windows.UI.CommandBars.UICommand cmdOLC1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoSua1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBOT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoHuy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoHuy;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamOLA1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamOLA;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNha_VC;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNha_VC;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNhaVanChuyen;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTuyenDuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCangDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrViTriDoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemDoHang;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCangXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrViTriXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrLoaiHinhVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaMucDich;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongTien;
        private Janus.Windows.GridEX.EditControls.EditBox txtGioKetThuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtGioBatDau;
        private System.Windows.Forms.Label label12;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDuKienKetThuc;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDuKienBatDau;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHHHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNganHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTBaoLanh;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhBL;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
    }
}