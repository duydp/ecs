﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class CX_BaoCao_HangTonKho
    {
       [XmlElement("CustomsGoodsItem")]
       public List<CustomsGoodsItem> CustomsGoodsItemL { get; set; }
    }
}
