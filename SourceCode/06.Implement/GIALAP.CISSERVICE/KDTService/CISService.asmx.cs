﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;

namespace TNTT.Receive.Request
{
    public struct DeclarationFunction
    {
        public const string HUY = "1";
        public const string SUA = "5";
        /// <summary>
        /// 8
        /// </summary>
        public const string KHAI_BAO = "8";
        public const string CHUA_XU_LY = "12";
        public const string HOI_TRANG_THAI = "13";
        public const string DE_NGHI = "16";
        public const string KHONG_CHAP_NHAN = "27";
        public const string CAP_SO_TIEP_NHAN = "29";
        public const string CAP_SO_TO_KHAI = "30";
        public const string DUYET_LUONG_CHUNG_TU = "31";
        public const string THONG_QUAN = "32";
        public const string THUC_XUAT = "33";
        public const string XAC_NHAN_RA_KHOI_KV_GIAM_SAT = "34";

    }

    public struct DeclarationIssuer
    {
        public const string KD_TOKHAI_NHAP = "929";
        public const string KD_TOKHAI_XUAT = "930";
        public const string SXXK_TOKHAI_NHAP = "931";
        public const string SXXK_TOKHAI_XUAT = "932";
        //public const string SXXK_TOKHAI_KCX_DONGIAN = "939";
        //public const string SXXK_TOKHAI_KCX_THANG = "940";
        public const string GC_TOKHAI_NHAP = "935";
        public const string GC_TOKHAI_XUAT = "936";
        public const string GC_GIA_HAN_THANH_KHOAN = "6021";
        public const string GC_TOKHAI_CHUYENTIEP_XUAT = "986";
        public const string GC_TOKHAI_CHUYENTIEP_NHAP = "968";

        public const string TOKHAI_DIENTU_DON_GIAN = "939";
        public const string TOKHAI_DIENTU_THANG = "940";
        public const string DNCX_TOKHAI_VAO = "931";//"976";
        public const string DNCX_TOKHAI_RA = "932";//"975";

        public const string SXXK_NPL = "100";
        public const string SXXK_SP = "101";
        public const string SXXK_DINH_MUC = "102";
        public const string HO_SO_THANH_KHOAN = "103";
        public const string HO_SO_THANH_KHOAN_FULL = "104";
        public const string HOP_DONG_GIA_CONG = "601";
        public const string GC_PHU_KIEN_HOP_DONG = "602";
        public const string GC_DINH_MUC = "603";
        public const string GC_YEU_CAU_THANH_KHOAN_HD = "605";
        public const string GC_DENGHI_GIAMSAT_TIEU_HUY = "609";
        //Doanh nghiệp chế xuất dnh mục hàng hóa đưa vào
        public const string DNCX_HANG_VAO = "501";
        public const string DNCX_HANG_RA = "502";
        public const string DNCX_DINHMUC_SANPHAM = "508";
        public const string DNCX_BAOCAO_HANG_TKHO = "510";
        public const string QDINH_KTRA_THUC_TE_HANG_TON_KHO = "511";
        public const string DNCX_HUY = "512";
        public const string THANHLY_TSCD = "513";
        //511	Quyết định kiểm tra thực tế hàng tồn kho
        //512	Thông tin huỷ nguyên liệu, sản phẩm, phế liệu, phế phẩm
        //513	Thông tin thanh lý tài sản cố định
    }

    public class CompressionHelper
    {
        public static string EncodeBase64String(string s)
        {
            return Convert.ToBase64String(Encoding.Unicode.GetBytes(s));
        }

        public static string DecodeBase64String(string s)
        {
            return Encoding.Unicode.GetString(Convert.FromBase64String(s)).Replace("�", "");
        }
    }

    [ToolboxItem(false)]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [WebService(Description = "Tiếp nhận message của doanh nghiệp.", Namespace = "http://cis.customs.gov.vn/")]
    public class CISService : WebService
    {
        public static string ReplaceSpecialInXML(string s)
        {
            s = s.Replace(" & ", "");
            s = s.Replace(" &  ", "");
            s = s.Replace("  & ", "");
            s = s.Replace("  &  ", "");
            s = s.Replace("& ", "");
            return s;
        }

        public static string CreateCapSoTiepNhanMessage(string MessageXML)
        {
            // Lấy số tiếp nhận max
            int sotiepnhan;
            using (var db = new Entities())
            {
                var entity = db.CapSoTiepNhans.FirstOrDefault();
                if (entity == null)
                {
                    var newEntity = new CapSoTiepNhan();
                    newEntity.SoTiepNhan = 1;
                    db.CapSoTiepNhans.AddObject(newEntity);
                    db.SaveChanges();
                }
                else
                {
                    var newEntity = db.CapSoTiepNhans.FirstOrDefault();
                    newEntity.SoTiepNhan = newEntity.SoTiepNhan + 1;
                    db.SaveChanges();
                }
                var max = db.CapSoTiepNhans.Max(x => x.SoTiepNhan);
                sotiepnhan = max + 1;
            }

            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.CAP_SO_TIEP_NHAN; // Cap so tiep nhan
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/CapSoTiepNhanMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
            // So tiep nhan
            xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", sotiepnhan);
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = string.Format("{0}/{1}/{2}", sotiepnhan, DateTime.Today.Year, mahaiquan);

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

            using (var db = new Entities())
            {
                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";
                msg.CustomsReference = sotiepnhan;
                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();
            }
            return xmlDocument.OuterXml;
        }

        public static string CreateCapSoToKhaiMessage(string MessageXML)
        {
            // Lấy số tiếp nhận max * 100 + 3
            int sotokhai;
            using (var db = new Entities())
            {
                var entity = db.CapSoTiepNhans.FirstOrDefault();
                if (entity == null)
                {
                    var newEntity = new CapSoTiepNhan();
                    newEntity.SoTiepNhan = 1;
                    db.CapSoTiepNhans.AddObject(newEntity);
                    db.SaveChanges();
                }
                else
                {
                    var newEntity = db.CapSoTiepNhans.FirstOrDefault();
                    newEntity.SoTiepNhan = newEntity.SoTiepNhan + 1;
                    db.SaveChanges();
                }
                var max = db.CapSoTiepNhans.Max(x => x.SoTiepNhan);
                sotokhai = max * 3 + 1;
            }

            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            var sContent = xmlOriginalDocument.SelectSingleNode("Envelope/Body/Content").InnerText;

            var xmlContentOriginalDocument = new XmlDocument();
            xmlContentOriginalDocument.LoadXml(CompressionHelper.DecodeBase64String(sContent));
            var loaihinh = xmlContentOriginalDocument.SelectSingleNode("Declaration/natureOfTransaction").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.CAP_SO_TO_KHAI; // Cap so to khai
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/CapSoToKhaiMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
            // So to khai
            xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", sotokhai);
            // Loai hinh
            xmlContent.SelectSingleNode("Declaration/natureOfTransaction").InnerText = string.Format("{0}", loaihinh);
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = string.Format("{0}/{1}/{2}/{3}", sotokhai, loaihinh, DateTime.Today.Year, mahaiquan); //"10/NKD01/2013/C34C";

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

            using (var db = new Entities())
            {
                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";

                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();

                // Cập nhật số tiếp nhận
                using (var database = new Entities())
                {
                    var item = database.OutboxMessages.OrderBy(x => x.ID).FirstOrDefault(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    var sotiepnhan = item.CustomsReference;

                    var items = database.OutboxMessages.Where(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    foreach (var message in items)
                    {
                        message.CustomsReference = sotiepnhan;
                    }
                    database.SaveChanges();
                }
            }

            return xmlDocument.OuterXml;
        }

        public static string CreatePhanLuongToKhaiNhanMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.DUYET_LUONG_CHUNG_TU; // Duyet phan luong to khai
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/PhanLuongToKhaiMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
            // So tờ khai
            xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", 0);
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = "Luồng XANH/Chấp nhận thông quan"; ;

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

            using (var db = new Entities())
            {
                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";

                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();

                // Cập nhật số tiếp nhận
                using (var database = new Entities())
                {
                    var item = database.OutboxMessages.OrderBy(x => x.ID).FirstOrDefault(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    var sotiepnhan = item.CustomsReference;

                    var items = database.OutboxMessages.Where(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    foreach (var message in items)
                    {
                        message.CustomsReference = sotiepnhan;
                    }
                    database.SaveChanges();
                }

            }
            return xmlDocument.OuterXml;
        }

        public static string CreateChapNhanThongQuanToKhaiMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.THONG_QUAN; // Chap nhan thong quan
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/ChapNhanThongQuanToKhaiMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");

            var sotokhai = 0;

            using (var db = new Entities())
            {
                // Cập nhật số tiếp nhận
                using (var database = new Entities())
                {
                    var item = database.OutboxMessages.OrderBy(x => x.ID).FirstOrDefault(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    var sotiepnhan = item.CustomsReference;
                    sotokhai = (int)sotiepnhan * 3 + 1;

                    var items = database.OutboxMessages.Where(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    foreach (var message in items)
                    {
                        message.CustomsReference = sotiepnhan;
                    }
                    database.SaveChanges();
                }

                // So tờ khai
                xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", sotokhai);
                // Noi dung chi tiet
                xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = "CHẤP NHẬN THÔNG QUAN";

                xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";
                //msg.CustomsReference = sotokhai;

                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();
            }
            
            return xmlDocument.OuterXml;
        }

        public static string CreateChapNhanChungTuMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.THONG_QUAN; // Chap nhan thong quan
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/ChapNhanChungTuMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
            // So tờ khai
            xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", 0);
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = "CHỨNG TỪ ĐÃ ĐƯỢC HẢI QUAN CHẤP NHẬN";

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

            using (var db = new Entities())
            {
                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";

                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();

                // Cập nhật số tiếp nhận
                using (var database = new Entities())
                {
                    var item = database.OutboxMessages.OrderBy(x => x.ID).FirstOrDefault(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    var sotiepnhan = item.CustomsReference;

                    var items = database.OutboxMessages.Where(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid);
                    foreach (var message in items)
                    {
                        message.CustomsReference = sotiepnhan;
                    }
                    database.SaveChanges();
                }

            }
            return xmlDocument.OuterXml;
        }

        public static string CreateChapNhanHuyKhaiBaoMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            // Build message tra ve
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.THONG_QUAN; // Chap nhan
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/ChapNhanKhaiBaoMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();

            // Ngay khai bao
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            // Ngay tiep nhan
            xmlContent.SelectSingleNode("Declaration/acceptance").InnerText = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
            // So tờ khai
            xmlContent.SelectSingleNode("Declaration/customsReference").InnerText = string.Format("{0}", 0);
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = "CHỨNG TỪ ĐÃ ĐƯỢC CHẤP NHẬN HỦY";

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);

            using (var db = new Entities())
            {
                var msg = new OutboxMessage();
                msg.ReferenceID = referenceid;
                msg.MaDoanhNghiep = madoanhnghiep;
                msg.MaHaiQuan = mahaiquan;
                msg.MessageContent = xmlDocument.OuterXml;
                msg.CreatedDate = DateTime.Now;
                msg.Status = "";

                db.OutboxMessages.AddObject(msg);
                db.SaveChanges();

            }
            return xmlDocument.OuterXml;
        }

        public static string CreateChuaCoPhanHoiMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;

            var xmlDocument = new XmlDocument();
            xmlDocument.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/Envelope.xml"));
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/version").InnerText = "1.00";
            xmlDocument.SelectSingleNode("Envelope/Header/Reference/messageId").InnerText = Guid.NewGuid().ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText = mahaiquan;
            xmlDocument.SelectSingleNode("Envelope/Header/From/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText = madoanhnghiep;
            xmlDocument.SelectSingleNode("Envelope/Header/To/name").InnerText = "";
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText = referenceid.ToString();
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText = DeclarationFunction.CHUA_XU_LY; // Chua xu ly
            xmlDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText = messageType;

            // Noi dung ket qua tra ve
            var xmlContent = new XmlDocument();
            xmlContent.Load(HttpContext.Current.Request.MapPath("~/ResponseMessagesXml/ChuaCoPhanHoiMessage.xml"));
            xmlContent.SelectSingleNode("Declaration/issuer").InnerText = messageType;
            xmlContent.SelectSingleNode("Declaration/reference").InnerText = referenceid.ToString();
            xmlContent.SelectSingleNode("Declaration/issue").InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            xmlContent.SelectSingleNode("Declaration/declarationOffice").InnerText = mahaiquan;
            // Noi dung chi tiet
            xmlContent.SelectSingleNode("Declaration/AdditionalInformation/content").InnerText = string.Format("Message [{0}] chưa có thông tin phản hồi từ hải quan.", referenceid);

            xmlDocument.SelectSingleNode("Envelope/Body/Content").InnerText = CompressionHelper.EncodeBase64String(xmlContent.OuterXml);
            return xmlDocument.OuterXml;
        }

        public static string GetOutboxMessage(string MessageXML)
        {
            // Message doanh nghiep gui di
            var xmlOriginalDocument = new XmlDocument();
            xmlOriginalDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlOriginalDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlOriginalDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var messageType = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/type").InnerText;
            var function = xmlOriginalDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText;

            #region Hủy khai báo
            if (function == DeclarationFunction.HUY)
            {
                // Tìm số tiếp nhận cần hủy
                var sContent = xmlOriginalDocument.SelectSingleNode("Envelope/Body/Content").InnerText;
                var xmlContentOriginalDocument = new XmlDocument();
                xmlContentOriginalDocument.LoadXml(CompressionHelper.DecodeBase64String(sContent));
                var sotiepnhan = Convert.ToInt32(xmlContentOriginalDocument.SelectSingleNode("Declaration/customsReference").InnerText);

                // Xóa các reference có số tiếp nhận
                using (var db = new Entities())
                {
                    var outboxMessages = db.OutboxMessages.Where(x => x.MaDoanhNghiep == madoanhnghiep && x.Status == "" && x.CustomsReference == sotiepnhan);
                    foreach (var outboxMessage in outboxMessages)
                    {
                        db.OutboxMessages.DeleteObject(outboxMessage);
                    }
                    db.SaveChanges();
                }

                // Cấp số tiếp nhận
                CreateCapSoTiepNhanMessage(MessageXML);
                CreateChapNhanHuyKhaiBaoMessage(MessageXML);
                return CreateChuaCoPhanHoiMessage(MessageXML);
            }
            #endregion

            #region Sửa tờ khai
            if (function == DeclarationFunction.SUA)
            {
                // Cấp số tiếp nhận
                CreateCapSoTiepNhanMessage(MessageXML);
                // Chấp nhận chứng từ.
                CreateChapNhanChungTuMessage(MessageXML);
                // Phân luồng tờ khai
                CreatePhanLuongToKhaiNhanMessage(MessageXML);
                // Chấp nhận thông quan.
                CreateChapNhanThongQuanToKhaiMessage(MessageXML);

                return CreateChuaCoPhanHoiMessage(MessageXML);
            }
            #endregion

            // Kiểm tra nếu là khai báo thì cấp số tiếp nhận
            if (function == DeclarationFunction.KHAI_BAO)
            {
                // Cấp số tiếp nhận
                CreateCapSoTiepNhanMessage(MessageXML);

                // Nếu là tờ khai KD | SXXK | GC
                if (messageType == DeclarationIssuer.KD_TOKHAI_NHAP || messageType == DeclarationIssuer.KD_TOKHAI_XUAT || messageType == DeclarationIssuer.SXXK_TOKHAI_NHAP || messageType == DeclarationIssuer.SXXK_TOKHAI_XUAT || messageType == DeclarationIssuer.GC_TOKHAI_NHAP || messageType == DeclarationIssuer.GC_TOKHAI_XUAT)
                {
                    // Cấp số tờ khai
                    CreateCapSoToKhaiMessage(MessageXML);
                    // Phân luồng tờ khai
                    CreatePhanLuongToKhaiNhanMessage(MessageXML);
                    // Chấp nhận thông quan.
                    CreateChapNhanThongQuanToKhaiMessage(MessageXML);
                }
                // Nếu là Hop dong GC
                else if (messageType == DeclarationIssuer.HOP_DONG_GIA_CONG || messageType == DeclarationIssuer.GC_PHU_KIEN_HOP_DONG)
                {
                    // Cấp số tờ khai
                    CreateCapSoToKhaiMessage(MessageXML);
                    // Duyet luồng chugn tu
                    CreatePhanLuongToKhaiNhanMessage(MessageXML);
                }
                else
                {
                    // Chấp nhận chứng từ.
                    CreateChapNhanChungTuMessage(MessageXML);
                }
                return CreateChuaCoPhanHoiMessage(MessageXML);
            }

            using (var db = new Entities())
            {
                var outboxMessage = db.OutboxMessages.OrderBy(x => x.ID).FirstOrDefault(x => x.MaDoanhNghiep == madoanhnghiep && x.ReferenceID == referenceid && x.Status == "");
                if (outboxMessage != null)
                {
                    outboxMessage.Status = "SEND";
                    db.SaveChanges();
                    return outboxMessage.MessageContent;
                }
            }

            return CreateChuaCoPhanHoiMessage(MessageXML);
        }

        [WebMethod(Description = "Tiếp nhận message của doanh nghiệp theo chuẩn message V3 chỉ dùng user.")]
        public string Send(string MessageXML, string UserId, string Password)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(MessageXML);

            var madoanhnghiep = xmlDocument.SelectSingleNode("Envelope/Header/From/identity").InnerText;
            var mahaiquan = xmlDocument.SelectSingleNode("Envelope/Header/To/identity").InnerText;
            var referenceid = new Guid(xmlDocument.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
            var function = xmlDocument.SelectSingleNode("Envelope/Header/Subject/function").InnerText;

            if (function != DeclarationFunction.HOI_TRANG_THAI)
            {
                using (var db = new Entities())
                {
                    var msg = new InboxMessage();
                    msg.ReferenceID = referenceid;
                    msg.MaDoanhNghiep = madoanhnghiep;
                    msg.MaHaiQuan = mahaiquan;
                    msg.MessageContent = MessageXML;
                    msg.CreatedDate = DateTime.Now;
                    msg.Status = "";

                    db.InboxMessages.AddObject(msg);
                    db.SaveChanges();
                }
            }
            return GetOutboxMessage(MessageXML);
        }
    }
}
