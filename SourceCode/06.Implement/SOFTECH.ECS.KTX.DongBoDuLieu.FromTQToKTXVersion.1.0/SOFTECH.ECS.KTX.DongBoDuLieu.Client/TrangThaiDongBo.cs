﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DongBoDuLieu.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Globalization;


namespace DongBo
{
    public partial class TrangThaiDongBo : Form
    {
        private int step = 1;
        private int value = 1;

        public DataSet HQHopDong;
        public DataSet DNHopDong;
        public DataSet dsHopDongTTDB;

        //
        public DataSet dsLoaiSPHD;
        public DataSet dsNPLHD;
        public DataSet dsSPHD;
        public DataSet dsTBHD;
        public DataSet dsDM;
        //

        public string maDN = "";
        public string maHQ = "";
        public string matKhau = "";
        public DateTime ngayBatDau;
        public DateTime ngayKetThuc;
        public int namdk = 2008;

        //
        public DataSet HQLoaiSPColection;
        public DataSet HQNPLColection;
        public DataSet HQSPColection;
        public DataSet HQTBColection;
        public DataSet HQDMColection;
        public DataSet HQGTKCTColection;
        public DataSet HQHCTColection;
        public DataSet HQGTKMDColection;
        public DataSet HQGHMDColection;
        public DataSet HQGBKCUColection;
        public DataSet HQGPKDKColection;

        public DataSet HQDMDS;
        public DataSet HQGTKCTDS;
        public DataSet HQHCTDS;
        public DataSet HQGTKMDDS;
        public DataSet HQGHMDDS;
        public DataSet HQGBKCUDS;
        public DataSet HQGPKDKDS;

        public DataSet DNLoaiSPColection;
        public DataSet DNNPLColection;
        public DataSet DNSPColection;
        public DataSet DNTBColection;
        public DataSet DNDMColection;
        public DataSet DNGTKCTColection;
        public DataSet DNHCTColection;
        public DataSet DNGTKMDColection;
        public DataSet DNGHMDColection;
        public DataSet DNGBKCUColection;
        public DataSet DNGPKDKColection;
        //

        delegate void SetTextCallback(string text);

        private void setText(string str)
        {
            if (lblMsg.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { str });
            }
            else
            {
                lblMsg.Text = str;
            }
        }

        public TrangThaiDongBo()
        {
            InitializeComponent();
        }

        private void TrangThaiDongBo_Load(object sender, EventArgs e)
        {
            this.step = 1;
            timer1.Enabled = true;
            timer1.Start();
            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            LoaDataFromCustomsSys(1);
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // this.step = 5;
            // this.value = 100;
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Maximum;
            label1.Text = "Đã tải xong hệ thống Hải quan ";
            lblMsg.Text = "";
            this.Close();
        }

        #region DongBoDuLieu
        #region Get method
        // Loai san pham :
        private DataSet GetLoaiSanPham(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            //DataSet dsHopDong = GetHopDong();
            try
            {
                ds = LoaiSP.GetDataSetLoaiSanPham(HD, matKhau);
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }
        // Nguyen phu lieu :
        private DataSet GetNguyenPhuLieu(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            //DataSet dsHopDong = GetHopDong();
            try
            {
                ds = NPL.GetDataSetNguyenPhuLieu(HD, matKhau);
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }
        // San pham :
        private DataSet GetSanPham(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = SP.GetDataSetSanPham(HD, matKhau);
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }
        // Thiet Bi :
        private DataSet GetThietBi(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = TB.GetDataSetThietBi(HD, matKhau);
            }
            catch (Exception ex) { throw ex; }
            return ds;
        }
        private DataSet GetDinhMucDS(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet ds = null;
            try
            {
                ds = DM.GetDataSetDinhMuc(HD, matKhau);
                HQDMDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetToKhaiMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                //ds = TKMD.GetDataSetALLToKhaiByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = TKMD.GetDataSetALLToKhai(HD, matKhau);
                HQGTKMDDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetHangMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                //ds = HMD.GetDataSetHangByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = HMD.GetDataSetHang(HD, matKhau);
                HQGHMDDS = ds;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            return ds;
        }
        private DataSet GetHangMD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsToKhai)
        {
            DataSet ds = new DataSet();

            try
            {
                //Kiem tra neu dshang = null vi ly do request timeout, request lai theo tung to khai
                DataSet dsHangTemp = new DataSet();
                foreach (DataRow row in dsToKhai.Tables[0].Rows)
                {
                    int soTK = Convert.ToInt32(row["SoTK"].ToString());

                    dsHangTemp = HMD.GetDataSetHang(HD, matKhau, soTK);

                    if (dsHangTemp != null)
                        ds.Merge(dsHangTemp);
                }

                HQGHMDDS = ds;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
            return ds;
        }
        private DataSet GetToKhaiCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                //ds = TKCT.GetDataSetToKhaiChuyenTiepByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = TKCT.GetDataSetALLToKhaiChuyenTiep(HD, matKhau);
                HQGTKCTDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetHangCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                //ds = HCT.GetDataSetHangChuyenTiepByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = HCT.GetDataSetALLHangChuyenTiep(HD, matKhau);
                HQHCTDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;

            try
            {
                //ds = BKCU.GetDataSetBangKeCungUngByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = BKCU.GetDataSetBangKeCungUng(HD, matKhau);
                HQGBKCUDS = ds;

            }
            catch { }
            return ds;
        }
        private DataSet GetSanPhamBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                //ds = BKCU.GetDataSetSanPhamCungUngBy(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = BKCU.GetDataSetSanPhamCungUng(HD, matKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetNPLDSBangKeCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                //ds = BKCU.GetDataSetNPLCungUngByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = BKCU.GetDataSetNPLCungUng(HD, matKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetSoPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                ds = PK.GetDataSetPhuKien(HD, matKhau);
                HQGPKDKDS = ds;
            }
            catch { }
            return ds;
        }
        private DataSet GetLoaiPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                //ds = PK.GetDataSetLoaiPhuKienByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = PK.GetDataSetALLLoaiPhuKien(HD, matKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetHangPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet ds = null;
            try
            {
                //ds = PK.GetDataSetHangPhuKienByNgay(HD, ngayBatDau, ngayKetThuc, matKhau);
                ds = PK.GetDataSetHangPhuKien(HD, matKhau);
            }
            catch { }
            return ds;
        }
        private DataSet GetHopDong()
        {
            DataSet dsHopDong = null;
            try
            {
                dsHopDong = HD.GetDataSetHopDong(maHQ, maDN, namdk, matKhau);
                HQHopDong = dsHopDong;
            }
            catch (Exception ex) { throw ex; }
            return dsHopDong;
        }
        #endregion
        #region DongBo Method
        public void LoaiSPOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsLoaiSP)
        {
            HD.NhomSPCollection = new Company.GC.BLL.KDT.GC.NhomSanPhamCollection();
            DataRow[] rowSelect = dsLoaiSP.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham NhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
                if (row["Gia"].ToString() != "")
                    NhomSP.GiaGiaCong = Convert.ToDecimal(row["Gia"]);
                NhomSP.HopDong_ID = HD.ID;
                NhomSP.MaSanPham = row["Ma"].ToString().Trim();
                if (row["SoLuong"].ToString() != "")
                    NhomSP.SoLuong = Convert.ToDecimal(row["SoLuong"]);
                NhomSP.TenSanPham = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(row["Ma"].ToString().Trim());
                HD.NhomSPCollection.Add(NhomSP);
            }
            //  HQLoaiSPColection = HD.nhomSPCollection;
        }
        public void DongBoLoaiSPFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsLoaiSP)
        {
            HD.NhomSPCollection = new Company.GC.BLL.KDT.GC.NhomSanPhamCollection();
            foreach (DataRow row in dsLoaiSP.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham NhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
                NhomSP.GiaGiaCong = Convert.ToDecimal(row["GiaGiaCong"]);
                NhomSP.HopDong_ID = HD.ID;
                NhomSP.MaSanPham = row["MaSanPham"].ToString().Trim();
                NhomSP.SoLuong = Convert.ToDecimal(row["SoLuong"]);
                NhomSP.TenSanPham = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(row["MaSanPham"].ToString().Trim());
                HD.NhomSPCollection.Add(NhomSP);
            }
        }
        public void SanPhamOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsSP)
        {
            HD.SPCollection = new Company.GC.BLL.KDT.GC.SanPhamCollection();
            DataRow[] rowSelect = dsSP.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.SanPham SP = new Company.GC.BLL.KDT.GC.SanPham();
                SP.DVT_ID = row["DVT_ID"].ToString();
                SP.HopDong_ID = HD.ID;
                SP.Ma = row["Ma"].ToString().Substring(1).Trim();
                SP.MaHS = row["MaHS"].ToString().Trim();
                SP.NhomSanPham_ID = row["Nhom_SP"].ToString();
                if (row["SoLuongDangKy"].ToString() != "")
                    SP.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                SP.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                HD.SPCollection.Add(SP);
            }
            //HQSPColection = HD.SPCollection;
        }
        public void DongBoSPFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsSP)
        {
            HD.SPCollection = new Company.GC.BLL.KDT.GC.SanPhamCollection();
            foreach (DataRow row in dsSP.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.SanPham SP = new Company.GC.BLL.KDT.GC.SanPham();
                SP.DVT_ID = row["DVT_ID"].ToString();
                SP.HopDong_ID = HD.ID;
                SP.Ma = row["Ma"].ToString().Trim();
                SP.MaHS = row["MaHS"].ToString().Trim();
                SP.NhomSanPham_ID = row["NhomSanPham_ID"].ToString();
                if (row["SoLuongDangKy"].ToString() != "")
                    SP.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                SP.Ten = (row["Ten"].ToString().Trim());
                HD.SPCollection.Add(SP);
            }
        }
        public void NguyenPhuLieuOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsNPL)
        {
            HD.NPLCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCollection();
            DataRow[] rowSelect = dsNPL.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                npl.DVT_ID = row["DVT_ID"].ToString();
                npl.HopDong_ID = HD.ID;
                npl.Ma = row["Ma"].ToString().Substring(1).Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"].ToString());
                npl.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                HD.NPLCollection.Add(npl);
            }
            //HQNPLColection = HD.NPLCollection;
        }
        public void DongBoNPLFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsNPL)
        {
            HD.NPLCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCollection();
            foreach (DataRow row in dsNPL.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                npl.DVT_ID = row["DVT_ID"].ToString();
                npl.HopDong_ID = HD.ID;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"].ToString());
                npl.Ten = (row["Ten"].ToString().Trim());
                HD.NPLCollection.Add(npl);
            }
        }
        public void ThietBiOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsTB)
        {
            HD.TBCollection = new Company.GC.BLL.KDT.GC.ThietBiCollection();
            DataRow[] rowSelect = dsTB.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.DVT_ID = row["DVT_ID"].ToString();
                tb.HopDong_ID = HD.ID;
                tb.Ma = row["Ma"].ToString().Trim().Substring(1);
                tb.MaHS = row["MaHS"].ToString().Trim();
                tb.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                tb.NuocXX_ID = row["NuocXX_ID"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                tb.Ten = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten"].ToString().Trim());
                tb.TinhTrang = row["TinhTrang"].ToString().Trim();
                tb.TriGia = Convert.ToDouble(row["TriGia"]);
                HD.TBCollection.Add(tb);
            }
            //TBColection = HD.TBCollection;
        }
        public void DongBoTBFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsTB)
        {
            HD.TBCollection = new Company.GC.BLL.KDT.GC.ThietBiCollection();
            foreach (DataRow row in dsTB.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.DVT_ID = row["DVT_ID"].ToString();
                tb.HopDong_ID = HD.ID;
                tb.Ma = row["Ma"].ToString().Trim();
                tb.MaHS = row["MaHS"].ToString().Trim();
                tb.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                tb.NuocXX_ID = row["NuocXX_ID"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                tb.Ten = (row["Ten"].ToString().Trim());
                tb.TinhTrang = row["TinhTrang"].ToString().Trim();
                tb.TriGia = Convert.ToDouble(row["TriGia"]);
                HD.TBCollection.Add(tb);
            }
            //TBColection = HD.TBCollection;
        }
        public void DinhMucOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM, int dk)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load();
                DM.TenNPL = npl.Ten;
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.Load();
                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD);
            }

        }
        public void DongBoDinhMucMoiFromDBTamToDBOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (DataRow row in dsDM.Tables[0].Rows)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString().Trim();
                DM.MaSanPham = row["MaSanPham"].ToString().Trim();
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.Load())
                    continue;
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = DM.MaNguyenPhuLieu;
                npl.Load();
                DM.TenNPL = npl.Ten;
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = DM.MaSanPham;
                sp.Load();
                DM.TenSanPham = sp.Ten;
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.InsertUpdate(dmCollection, HD);
            }

        }
        public void LayThongTinTKMD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = GetToKhaiMD(HD);
            DataSet dsHang = GetHangMD(HD);
            //Kiem tra neu dshang = null vi ly do request timeout, request lai theo tung to khai
            if (dsHang == null)
                dsHang = GetHangMD(HD, dsToKhai);
            TKMD.InsertGhiDeALLToKhai(HD, dsToKhai, dsHang);
        }
        public void LayThongTinBKCU(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsBangKe = GetBangKeCU(HD);
            DataSet dsSanPhamBangKe = GetSanPhamBangKeCU(HD);
            DataSet dsNPLBangKe = GetNPLDSBangKeCU(HD);

            BKCU.InsertGhiDeALLBangKeCungUng(HD, dsBangKe, dsSanPhamBangKe, dsNPLBangKe);
        }
        public void LayThongTinPhuKien(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsPhuKienDK = GetSoPhuKien(HD);
            DataSet dsLoaiPK = GetLoaiPhuKien(HD);
            DataSet dsHangPK = GetHangPhuKien(HD);
            PK.InsertGhiDeALLPhuKien(HD, dsPhuKienDK, dsLoaiPK, dsHangPK);

        }
        public void DongBoTKMDFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            ToKhaiMauDich TKMDDBTamp = new ToKhaiMauDich();
            TKMDDBTamp.IDHopDong = id_HDTMP;
            TKMDDBTamp.SetDabaseMoi("MSSQL");
            ToKhaiMauDichCollection TKMDCollection = TKMDDBTamp.SelectCollectionBy_IDHopDong();
            foreach (ToKhaiMauDich TKitem in TKMDCollection)
            {
                TKitem.LoadHMDCollection("MSSQL");
                TKitem.ID = 0;
                TKitem.TrangThaiPhanBo = 0;
                TKitem.IDHopDong = HD.ID;
                foreach (HangMauDich HMD in TKitem.HMDCollection)
                    HMD.ID = 0;
            }
            if (TKMDCollection.Count > 0)
            {
                try
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.InsertUpdateDongBoDuLieu(TKMDCollection, HD);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi khi đồng bộ dữ liệu TKMD : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
                }
            }
        }
        public void DongBoTKChuyenTiepFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long id_HDTMP)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            ToKhaiChuyenTiep TKCTDBTam = new ToKhaiChuyenTiep();
            TKCTDBTam.IDHopDong = id_HDTMP;
            TKCTDBTam.SetDabaseMoi("MSSQL");
            TKCTCollection = TKCTDBTam.SelectCollectionBy_IDHopDong();
            foreach (ToKhaiChuyenTiep TKCTItem in TKCTCollection)
            {
                //TKCTItem.LoadHCTCollection("MSSQL");
                TKCTItem.ID = 0;
                TKCTItem.IDHopDong = HD.ID;
                foreach (HangChuyenTiep HCT in TKCTItem.HCTCollection)
                    HCT.ID = 0;
            }
            try
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.InsertUpdateDongBoDuLieu(TKCTCollection, HD);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi khi đồng bộ dữ liệu TKCT : " + ex.Message + " \r\ncủa hợp đồng " + HD.SoHopDong);
            }
        }
        public void DongBoPhuKienFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            PhuKienDangKy pkdk = new PhuKienDangKy();
            pkdk.HopDong_ID = ID_HDTMP;
            pkdk.SetDabaseMoi("MSSQL");
            PhuKienDangKyCollection pkdkCollection = pkdk.SelectCollectionBy_HopDong_ID();
            foreach (PhuKienDangKy pkdkItem in pkdkCollection)
            {
                pkdkItem.LoadCollection("MSSQL");
                pkdkItem.ID = 0;
                pkdkItem.HopDong_ID = HD.ID;
                foreach (LoaiPhuKien loaiPK in pkdkItem.PKCollection)
                {
                    loaiPK.LoadCollection("MSSQL");
                    loaiPK.ID = 0;
                    foreach (HangPhuKien hangPK in loaiPK.HPKCollection)
                    {
                        hangPK.ID = 0;

                    }
                }
            }


            try
            {
                PhuKienDangKy pk = new PhuKienDangKy();
                pk.InsertUpdateDongBoDuLieu(pkdkCollection, HD);
            }
            catch { }

        }
        private void DongBoBKCUFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
            }
            BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
            BKCUUpdate.InsertUpdateDongBoDuLieu(BKCUDangKyCollection, HD);

        }
        private void DongBoBKCUMoiFromDBTamToDB(Company.GC.BLL.KDT.GC.HopDong HD, long ID_HDTMP)
        {
            BKCungUngDangKy bkTMP = new BKCungUngDangKy();
            bkTMP.SetDabaseMoi("MSSQL");
            BKCungUngDangKyCollection BKCUDangKyCollection = bkTMP.SelectBangKeCUByHopDong(ID_HDTMP);
            BKCungUngDangKyCollection BKCUDangKyMoiCollection = new BKCungUngDangKyCollection();

            foreach (BKCungUngDangKy BKCU in BKCUDangKyCollection)
            {
                if (BKCU.LoadBySoBangKe())
                {
                    continue;
                }
                BKCU.LoadSanPhamCungUngCollection("MSSQL");
                BKCU.ID = 0;
                BKCU.TKCT_ID = 0;
                BKCU.TKMD_ID = 0;
                if (BKCU.isTKMD == 0)
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = BKCU.SoToKhai;
                    TKMD.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKMD.MaHaiQuan = Properties.Settings.Default.MaHaiQuan;
                    //TKMD.SetDabaseMoi("MSSQL");
                    if (TKMD.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKMD.ID;
                    else
                        continue;
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.SoToKhai = BKCU.SoToKhai;
                    TKCT.MaLoaiHinh = BKCU.MaLoaiHinh;
                    TKCT.MaHaiQuanTiepNhan = Properties.Settings.Default.MaHaiQuan;
                    //TKCT.SetDabaseMoi("MSSQL");
                    if (TKCT.LoadBySoToKhai(BKCU.NamDangKy))
                        BKCU.TKMD_ID = TKCT.ID;
                    else
                        continue;
                }
                foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                {
                    spCU.LoadNPLCungUngCollection("MSSQL");
                    spCU.ID = 0;
                    foreach (NguyenPhuLieuCungUng NPLCu in spCU.NPLCungUngCollection)
                    {
                        NPLCu.ID = 0;
                    }
                }
                BKCUDangKyMoiCollection.Add(BKCU);
            }
            if (BKCUDangKyMoiCollection.Count > 0)
            {
                BKCungUngDangKy BKCUUpdate = new BKCungUngDangKy();
                BKCUUpdate.InsertUpdateDongBoDuLieuMOi(BKCUDangKyMoiCollection, HD);
            }

        }
        #endregion DongBo Method
        #region TT
        public void DinhMucMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsDM)
        {
            Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
            DataRow[] rowSelect = dsDM.Tables[0].Select("SoHopDong='" + HD.SoHopDong + "' and MaHaiQuan='" + HD.MaHaiQuan + "' and MaDoanhNghiep='" + HD.MaDoanhNghiep + "'");
            foreach (DataRow row in rowSelect)
            {
                Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                DM.DinhMucSuDung = Convert.ToDecimal(row["DinhMuc"]);
                DM.HopDong_ID = HD.ID;
                DM.MaNguyenPhuLieu = row["MaNPL"].ToString().Substring(1);
                DM.MaSanPham = row["MaSP"].ToString().Substring(1);
                DM.NPL_TuCungUng = Convert.ToDecimal(row["NPL_TuCungUng"]);
                DM.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                if (DM.CheckExitsDinhMucSanPham())
                {
                    continue;
                }
                dmCollection.Add(DM);
            }
            if (dmCollection.Count > 0)
                try
                {
                    new Company.GC.BLL.GC.DinhMuc().InsertUpdate(dmCollection);
                }
                catch
                {
                    ;
                }

        }
        public void ToKhaiMDMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = HQGTKMDDS;// GetDataSetToKhai(HD);GetToKhaiMD(HD);//
            DataSet dsHang = HQGHMDDS; // GetHangMD(HD);
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.ToKhaiMauDich TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
                TKMD.MaDoanhNghiep = maDN;
                TKMD.MaHaiQuan = maHQ;
                TKMD.SoToKhai = Convert.ToInt32(row["SoTK"].ToString());
                TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());

                if (Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(maHQ, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NgayDangKy.Year) > 0)
                {
                    continue;
                }

                TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                TKMD.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CangNN"].ToString());
                TKMD.DKGH_ID = row["Ma_GH"].ToString();
                TKMD.GiayTo = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GiayTo"].ToString().Trim());
                TKMD.SoVanDon = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Van_Don"].ToString());
                if (row["NgayDen"].ToString() != "")
                    TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                if (row["Ngay_GP"].ToString() != "")
                    TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                if (row["Ngay_HHGP"].ToString() != "")
                    TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                if (row["Ngay_HHHD"].ToString() != "")
                    TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                if (row["Ngay_HDTM"].ToString() != "")
                    TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                if (row["Ngay_HD"].ToString() != "")
                    TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                if (row["Ngay_VanDon"].ToString() != "")
                    TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                {
                    TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                }
                else
                {
                    TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                    TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                }
                if (row["Phi_BH"].ToString() != "")
                    TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                if (row["Phi_VC"].ToString() != "")
                    TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                if (row["So_Container"].ToString() != "")
                    TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                if (row["So_container40"].ToString() != "")
                    TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                TKMD.SoGiayPhep = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_GP"].ToString());
                TKMD.SoHieuPTVT = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Ten_PTVT"].ToString());
                TKMD.SoHoaDonThuongMai = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["So_HDTM"].ToString());
                TKMD.SoHopDong = row["So_HD"].ToString();
                if (row["So_Kien"].ToString() != "")
                    TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                if (row["So_PLTK"].ToString() != "")
                    TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                TKMD.TenChuHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["TenCH"].ToString().Trim());
                TKMD.TrangThaiXuLy = 1;
                if (row["Tr_Luong"].ToString() != "")
                    TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                if (row["TyGia_VND"].ToString() != "")
                    TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                if (row["TyGia_USD"].ToString() != "")
                    TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                TKMD.TenDonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DV_DT"].ToString().Trim());

                DataRow[] rowHang = dsHang.Tables[0].Select("SoTK=" + TKMD.SoToKhai + " and Ma_LH='" + TKMD.MaLoaiHinh + "' and Ma_HQ='" + TKMD.MaHaiQuan + "' and NamDK=" + TKMD.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    TKMD.HMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.HangMauDich hang = new Company.GC.BLL.KDT.HangMauDich();
                        if (rowIndex["DGia_KB"].ToString() != "")
                            hang.DonGiaKB = Convert.ToDecimal(rowIndex["DGia_KB"].ToString());
                        if (rowIndex["DGia_TT"].ToString() != "")
                            hang.DonGiaTT = Convert.ToDecimal(rowIndex["DGia_TT"].ToString());
                        hang.DVT_ID = rowIndex["Ma_DVT"].ToString();
                        hang.MaHS = rowIndex["Ma_HangKB"].ToString().Trim();
                        hang.MaPhu = rowIndex["Ma_Phu"].ToString().Substring(1).Trim();
                        TKMD.LoaiHangHoa = rowIndex["Ma_Phu"].ToString().Substring(0, 1);
                        if (rowIndex["MienThue"].ToString() != "")
                            hang.MienThue = Convert.ToByte(rowIndex["MienThue"].ToString());
                        hang.NuocXX_ID = rowIndex["Nuoc_XX"].ToString();
                        if (rowIndex["Phu_Thu"].ToString() != "")
                            hang.PhuThu = Convert.ToDecimal(rowIndex["Phu_Thu"].ToString());
                        if (rowIndex["Luong"].ToString() != "")
                            hang.SoLuong = Convert.ToDecimal(rowIndex["Luong"].ToString());
                        hang.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowIndex["Ten_Hang"].ToString().Trim());
                        if (rowIndex["Thue_VAT"].ToString() != "")
                            hang.ThueGTGT = Convert.ToDecimal(rowIndex["Thue_VAT"].ToString());
                        if (rowIndex["TS_VAT"].ToString() != "")
                            hang.ThueSuatGTGT = Convert.ToDecimal(rowIndex["TS_VAT"].ToString());
                        if (rowIndex["TS_TTDB"].ToString() != "")
                            hang.ThueSuatTTDB = Convert.ToDecimal(rowIndex["TS_TTDB"].ToString());
                        if (rowIndex["TS_XNK"].ToString() != "")
                            hang.ThueSuatXNK = Convert.ToDecimal(rowIndex["TS_XNK"].ToString());
                        if (rowIndex["Thue_TTDB"].ToString() != "")
                            hang.ThueTTDB = Convert.ToDecimal(rowIndex["Thue_TTDB"].ToString());
                        if (rowIndex["Thue_XNK"].ToString() != "")
                            hang.ThueXNK = Convert.ToDecimal(rowIndex["Thue_XNK"].ToString());
                        if (rowIndex["TriGia_KB"].ToString() != "")
                            hang.TriGiaKB = Convert.ToDecimal(rowIndex["TriGia_KB"].ToString());
                        if (rowIndex["TGKB_VND"].ToString() != "")
                            hang.TriGiaKB_VND = Convert.ToDecimal(rowIndex["TGKB_VND"].ToString());
                        if (rowIndex["TriGia_ThuKhac"].ToString() != "")
                            hang.TriGiaThuKhac = Convert.ToDecimal(rowIndex["TriGia_ThuKhac"].ToString());
                        if (rowIndex["TriGia_TT"].ToString() != "")
                            hang.TriGiaTT = Convert.ToDecimal(rowIndex["TriGia_TT"].ToString());
                        if (rowIndex["TyLe_ThuKhac"].ToString() != "")
                            hang.TyLeThuKhac = Convert.ToDecimal(rowIndex["TyLe_ThuKhac"].ToString());
                        TKMD.HMDCollection.Add(hang);
                    }
                    if (TKMD.HMDCollection.Count > 0)
                    {
                        try
                        {
                            TKMD.IDHopDong = HD.ID;
                            TKMD.InsertUpdateDongBoDuLieu();
                        }
                        catch
                        {
                            ;
                        }
                    }
                }
            }

        }
        public void LayThongTinTKCT(Company.GC.BLL.KDT.GC.HopDong HD)
        {

            DataSet dsToKhai = GetToKhaiCT(HD);
            DataSet dsHang = GetHangCT(HD);

            TKCT.InsertGhiDeALLToKhai(HD, dsToKhai, dsHang);
        }
        public void ToKhaiChuyenTiepMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsToKhai = HQGTKCTDS;// GetDataSetToKhaiChuyenTiep(HD);
            DataSet dsHang = HQHCTDS; //GetHangCT(HD);
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                tkct.MaDoanhNghiep = maHQ;
                tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                tkct.MaHaiQuanTiepNhan = maHQ;
                tkct.SoToKhai = Convert.ToInt64(row["So_CT"].ToString());
                tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);
                if (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(maHQ, tkct.MaLoaiHinh, tkct.SoToKhai, tkct.NgayDangKy.Year) > 0)
                {
                    continue;
                }
                tkct.ChungTu = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                if (row["LePhiHQ"].ToString() != "")
                    tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                string sohopdong = "";
                sohopdong = row["So_HD"].ToString();
                tkct.SoHopDongDV = row["So_HD"].ToString();
                tkct.SoHDKH = row["So_HDCh"].ToString();
                if (tkct.MaLoaiHinh.EndsWith("N"))
                {
                    tkct.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                }
                else
                {
                    tkct.DiaDiemXepHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                }

                tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                tkct.MaKhachHang = row["Ma_DoiTac"].ToString();
                tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                tkct.NgayHetHanHDDV = HD.NgayHetHan;
                tkct.NguoiChiDinhDV = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                tkct.NguoiChiDinhKH = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                tkct.NgayTiepNhan = tkct.NgayDangKy;
                tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                tkct.SoTiepNhan = 0;
                tkct.TrangThaiXuLy = 1;
                tkct.IDHopDong = HD.ID;
                if (row["TyGiaVND"].ToString() != "")
                    tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());
                DataRow[] rowHang = dsHang.Tables[0].Select("So_CT=" + tkct.SoToKhai + " and Ma_CT='" + tkct.MaLoaiHinh + "' and Ma_HQ='" + tkct.MaHaiQuanTiepNhan + "' and Nam_CT=" + tkct.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    tkct.HCTCollection = new Company.GC.BLL.KDT.GC.HangChuyenTiepCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.GC.HangChuyenTiep hangCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                        hangCT.DonGia = Convert.ToDecimal(rowIndex["DonGia"]);
                        hangCT.ID_DVT = rowIndex["Ma_DVT"].ToString();
                        hangCT.ID_NuocXX = rowIndex["NuocXX"].ToString();
                        hangCT.MaHang = rowIndex["P_Code"].ToString().Substring(1).Trim();
                        hangCT.MaHS = rowIndex["HS_Code"].ToString().Trim();
                        hangCT.SoLuong = Convert.ToDecimal(rowIndex["SoLuong"]);
                        hangCT.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowIndex["TenHang"].ToString().Trim());
                        hangCT.TriGia = Convert.ToDecimal(rowIndex["TriGia"]);
                        tkct.HCTCollection.Add(hangCT);
                    }
                    try
                    {
                        tkct.InsertUpdateDongBoDuLieu();
                    }
                    catch
                    {
                        ;
                    }
                }
            }

        }
        public void PhuKienMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            // DataSet dsPhuKienDK = GetDataSetDistinctSoPhuKien(HD);
            DataSet dsPhuKienDK = HQGPKDKDS;// GetSoPhuKien(HD);// HQGPKDKColection;
            if (dsPhuKienDK.Tables[0].Rows.Count > 0)
            {
                DataSet dsLoaiPK = GetLoaiPhuKien(HD);
                DataSet dsHangPK = GetHangPhuKien(HD);
                NumberFormatInfo infoNumber = new CultureInfo("vi-VN").NumberFormat;
                foreach (DataRow row in dsPhuKienDK.Tables[0].Rows)
                {
                    string sohopdong = row["So_HD"].ToString().Trim();
                    Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                    pkdk.HopDong_ID = HD.ID;
                    pkdk.SoPhuKien = row["So_PK"].ToString().Trim();
                    if (Company.GC.BLL.KDT.GC.PhuKienDangKy.GetIDPhuKienByHopDongAndSoPhuKien(pkdk.SoPhuKien, pkdk.HopDong_ID) > 0)
                        continue;

                    // pkdk.GhiChu = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["GhiChu"].ToString().Trim());
                    pkdk.MaDoanhNghiep = maHQ;
                    pkdk.MaHaiQuan = maDN;
                    pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                    pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                    pkdk.NguoiDuyet = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                    pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                    pkdk.TrangThaiXuLy = 1;
                    pkdk.VanBanChoPhep = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["VBCP"].ToString().Trim());
                    pkdk.TrangThaiXuLy = 1;
                    string st = "DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_PK='" + pkdk.SoPhuKien + "'";
                    DataRow[] rowLoaiPK = dsLoaiPK.Tables[0].Select(st);
                    foreach (DataRow rowLoai in rowLoaiPK)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                        LoaiPK.MaPhuKien = rowLoai["Ma_PK"].ToString();
                        LoaiPK.NoiDung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowLoai["Noi_Dung"].ToString());
                        LoaiPK.ThongTinCu = rowLoai["Old_Info"].ToString();
                        if (LoaiPK.MaPhuKien != "H06")
                            LoaiPK.ThongTinMoi = rowLoai["New_Info"].ToString();
                        else
                        {
                            DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                            LoaiPK.ThongTinMoi = Convert.ToDateTime(rowLoai["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy");
                        }
                        LoaiPK.HPKCollection = new Company.GC.BLL.KDT.GC.HangPhuKienCollection();
                        DataRow[] rowHangLoaiPK = dsHangPK.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_PK='" + pkdk.SoPhuKien + "' and Ma_PK='" + LoaiPK.MaPhuKien + "'");
                        foreach (DataRow rowHang in rowHangLoaiPK)
                        {
                            Company.GC.BLL.KDT.GC.HangPhuKien hangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                            if (rowHang["DonGia"].ToString() != "")
                                hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                            hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                            if (LoaiPK.MaPhuKien != "S15")
                                hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                            else
                                hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                            hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                            hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                            hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                            hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                            hangPK.ThongTinCu = rowHang["Old_Info"].ToString();
                            try
                            {
                                if (rowHang["So_Luong"].ToString() != "")
                                {
                                    if (LoaiPK.MaPhuKien.Trim() == "N05")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else if (LoaiPK.MaPhuKien.Trim() == "N11")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else if (LoaiPK.MaPhuKien.Trim() == "T01")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                }
                            }
                            catch { }
                            hangPK.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString().Trim());
                            hangPK.TinhTrang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString().Trim());
                            if (rowHang["TriGia"].ToString() != "")
                                hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                            LoaiPK.HPKCollection.Add(hangPK);
                        }
                        pkdk.PKCollection.Add(LoaiPK);
                    }
                    try
                    {

                        pkdk.InsertUpDatePhuKienDongBoDuLieu();
                    }
                    catch { }
                }
            }

        }
        public void BangKeCungUngMoiOfHD(Company.GC.BLL.KDT.GC.HopDong HD)
        {
            DataSet dsBangKe = HQGBKCUDS;// GetDataSetBangKeCungUng(HD);
            DataSet dsSanPhamBangKe = GetSanPhamBangKeCU(HD);
            DataSet dsNPLBangKe = GetNPLDSBangKeCU(HD);
            foreach (DataRow rowBangKe in dsBangKe.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCungUng = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCungUng.MaDoanhNghiep = maHQ;
                BKCungUng.MaHaiQuan = maDN;
                BKCungUng.NgayTiepNhan = Convert.ToDateTime(rowBangKe["Ngay_TN"]);
                BKCungUng.SoBangKe = Convert.ToInt64(rowBangKe["So_TN"]);
                BKCungUng.SoTiepNhan = 0;
                BKCungUng.TrangThaiXuLy = 1;
                string MaLoaiHinh = rowBangKe["Ma_LH"].ToString();
                int NamDK = Convert.ToInt32(rowBangKe["NamDK"].ToString());
                long SoToKhai = Convert.ToInt64(rowBangKe["SoTK"].ToString());

                if (Company.GC.BLL.KDT.GC.BKCungUngDangKy.GetIDBangKeBySoBangKeAndNamDangKy(maHQ, BKCungUng.SoBangKe, NamDK) > 0)
                    continue;

                if (MaLoaiHinh.StartsWith("XGC"))
                {
                    BKCungUng.TKMD_ID = Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(maHQ, MaLoaiHinh, SoToKhai, NamDK);
                }
                else
                {
                    BKCungUng.TKCT_ID = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(maHQ, MaLoaiHinh, SoToKhai, NamDK);
                }
                if (BKCungUng.TKMD_ID > 0 || BKCungUng.TKCT_ID > 0)
                {
                    BKCungUng.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
                    DataRow[] rowSPCungUngCollection = dsSanPhamBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK);
                    foreach (DataRow SPCungUng in rowSPCungUngCollection)
                    {
                        Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUngEntity = new Company.GC.BLL.KDT.GC.SanPhanCungUng();
                        string maSP = SPCungUng["SPP_Code"].ToString();
                        spCungUngEntity.LuongCUSanPham = Convert.ToDecimal(SPCungUng["SoLuongSP"]);
                        spCungUngEntity.MaSanPham = maSP.Substring(1).Trim();
                        spCungUngEntity.NPLCungUngCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                        DataRow[] rowNPLOfSanPhamCollection = dsNPLBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and Ngay_Ky='" + HD.NgayKy.ToString("dd/MM/yyyy") + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK + " and SPP_Code='" + maSP + "'");
                        foreach (DataRow rowNPL in rowNPLOfSanPhamCollection)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCungUng = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng();
                            nplCungUng.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            nplCungUng.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            nplCungUng.HinhThuCungUng = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            nplCungUng.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            nplCungUng.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1).Trim();
                            nplCungUng.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            nplCungUng.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUngEntity.NPLCungUngCollection.Add(nplCungUng);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUngEntity);
                    }
                }
                try
                {
                    BKCungUng.InsertUpdateDongBoDuLieu();
                }
                catch { }
            }
        }
        #endregion

        private void LoaDataFromCustomsSys(int dk)
        {
            setText("");
            //xoa toan bo hop dong cu :
            HopDong hdong = new HopDong();
            hdong.SetDabaseMoi("MSSQL");
            //TODO: 
            //if (DialogResult.OK == MessageBox.Show("Bạn có muốn xóa tất cả các thông tin HỢP ĐỒNG cũ đã có trước đó không?.\n\nĐể tránh mất dữ liệu trong một số trường hợp đặc biệt, người dùng nên backup dữ liệu trước khi thực hiện chức năng xóa hợp đồng.\r\n\nBạn có muốn tiếp tục không ?", this.Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
            //{
            //hdong.DeleteAllHopDong(); //Luu y
            //}

            // Xoa toan bo bang ke cung ung :
            // Set lai default chuoi ket noi trong file config :
            // Dong bo vao database tam :
            try
            {
                setText("Hợp đồng");
                DataSet dsHopDong = GetHopDong();// dsHopDongTTDB;             
                foreach (DataRow row in dsHopDong.Tables[0].Rows)
                {
                    string soHD = "HD: " + row["SoHopDong"].ToString().Trim();

                    //if (soHD.Contains("01-YV10/2010-PLHD"))
                    //{ }
                    //else
                    //    continue;

                    Company.GC.BLL.KDT.GC.HopDong HDG = new Company.GC.BLL.KDT.GC.HopDong();

                    bool isExist = HDG.checkSoHopDongExit(row["SoHopDong"].ToString().Trim(), maHQ, maDN);

                    if (isExist)
                        continue;

                    HDG.DiaChiDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DiaChi_DT"].ToString()).Trim();
                    HDG.DonViDoiTac = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(row["DVDT"].ToString()).Trim();
                    HDG.MaDoanhNghiep = maDN;
                    HDG.MaHaiQuan = maHQ;
                    if (row["NgayGiaHan"].ToString() != "")
                        HDG.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                    HDG.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                    HDG.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                    HDG.NguyenTe_ID = row["NguyenTe_ID"].ToString();
                    HDG.NuocThue_ID = row["NuocThue_ID"].ToString();
                    HDG.SoHopDong = row["SoHopDong"].ToString().Trim();
                    HDG.SoTiepNhan = 0;
                    HDG.TrangThaiXuLy = 1;
                    setText("Loại sản phẩm gia công - " + soHD);
                    LoaiSPOfHD(HDG, GetLoaiSanPham(HDG));
                    //Nguyen phu lieu :
                    setText("Nguyên phụ liệu - " + soHD);
                    NguyenPhuLieuOfHD(HDG, GetNguyenPhuLieu(HDG));
                    //San pham :
                    setText("Sản phẩm - " + soHD);
                    SanPhamOfHD(HDG, GetSanPham(HDG));
                    //Thiet bi :
                    setText("Thiết bị - " + soHD);
                    ThietBiOfHD(HDG, GetThietBi(HDG));

                    try
                    {
                        string databaseName = "MSSQL";
                        if (dk == 1)
                        {
                            HDG.SetDabaseMoi(databaseName);
                        }

                        //Comment: chi cho them moi, ko cho cap nhat. tranh cap nhat sai du lieu
                        //Load hop dong (Neu co)
                        //Company.GC.BLL.KDT.GC.HopDong hd = HDG.LoadSoHopDongByKTX(databaseName, HDG.SoHopDong, HDG.NgayKy, GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep);
                        //if (hd != null)
                        //    HDG.ID = hd.ID;

                        HDG.InsertUpdateHopDongDongBoDuLieuKTX(databaseName);

                        setText("Định mức - " + soHD);
                        DM.InsertGhiDeDuLieu(HDG, GetDinhMucDS(HDG), "");
                        setText("Phụ kiện - " + soHD);
                        LayThongTinPhuKien(HDG);
                        setText("Tờ khai mậu dịch - " + soHD);
                        LayThongTinTKMD(HDG);
                        setText("Tờ khai chuyển tiếp - " + soHD);
                        LayThongTinTKCT(HDG);
                        setText("Bảng kê cung ứng - " + soHD);
                        LayThongTinBKCU(HDG);

                    }
                    catch (Exception ex1) { MessageBox.Show("Lỗi : " + soHD + "\n" + ex1.Message + "\r\nChi tiết: " + ex1.StackTrace); }

                }

                // Lay ra tu database temp :                
                //if (dk == 1)
                //{
                //    GetHopDongAtDN(1);
                //}
                //else
                //{
                //    GetHopDongAtDN(2);
                //}

            }
            catch (Exception ex)
            { //this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message);
            }

        }
        #endregion

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            //if (pBar.Value == 100) this.Close();
            //if (this.value < 100)
            //{
            //    this.value += this.step;
            //    if (value > 99)
            //        value = 100;

            //}
            //else
            //{
            //    if (pBar.Value < 99) this.value = pBar.Value + 1;
            //}
            //pBar.Value = this.value;
        }

        private void TrangThaiDongBo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pBar.Value < 100)
                e.Cancel = true;
        }

    }
}