﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BillOfLadingNew
    {
        /// <summary>
        /// Số vận đơn gốc
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày vận đơn gốc
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        // <summary>
        /// Mã người phát hành vận đơn gốc
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        // <summary>
        /// Số lượng vận đơn nhánh
        /// </summary>
        [XmlElement("number")]
        public string Number { get; set; }

        // <summary>
        /// Phân loại tách vận đơn
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        // <summary>
        /// Loại hàng hóa
        /// </summary>
        [XmlElement("isContainer")]
        public string IsContainer { get; set; }

        /// <summary>
        /// Thông tin về vận đơn
        /// </summary>
        [XmlElement("BranchDetails")]
        public BranchDetails BranchDetails { get; set; }
    }
}
