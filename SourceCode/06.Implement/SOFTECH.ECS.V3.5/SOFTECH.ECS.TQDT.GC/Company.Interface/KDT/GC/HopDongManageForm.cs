using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.Interface.Report;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class HopDongManageForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        private List<HopDong> collection = new List<HopDong>();
        public List<HopDong> list_HopDong = new List<HopDong>();
        public bool IsBrowseForm = false;
        public bool IsDaDuyet = false;
        public bool isKhaiBoSung = false;
        private string xmlCurrent = "";
        string msgInfor = string.Empty;
        public bool isQuyetToan = false;
        public bool isTaoHoSo = false;
        public bool isPhieuXuat = false;
        public string ListHopDongSelected = "";
        public string ListID_HopDongSelected = "";
        public string LIST_HOPDONG_ID = "";
        public int NAMQUYETTOAN;
        FeedBackContent feedbackContent = null;
        public HopDongManageForm()
        {
            InitializeComponent();
            //dgList.RootTable.Columns[7].Visible = false;
            cbStatus.SelectedIndex = 0;
        }

        public void BindData()
        {
            try
            {
                string where = "";
                if (isTaoHoSo)
                {
                    where = string.Format("MaDoanhNghiep = '{0}' and TrangThaiXuLy={2} AND TrangThaiThanhKhoan = 2", GlobalSettings.MA_DON_VI, cbStatus.SelectedValue.ToString());
                }
                else
                {
                    where = string.Format("MaDoanhNghiep = '{0}' and TrangThaiXuLy={2} TrangThaiThanhKhoan in (0,1)", GlobalSettings.MA_DON_VI, cbStatus.SelectedValue.ToString());
                }

                Company.GC.BLL.KDT.GC.HopDong hd = new HopDong();
                collection = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic(where, "");
                dgList.DataSource = collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            try
            {
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                cbStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }        

        }

        //-----------------------------------------------------------------------------------------

        private void HopDongManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.khoitao_DuLieuChuan();
                txtNamQT.Text = DateTime.Now.Year.ToString();
                if (isQuyetToan == false)
                {
                    btnChon.Visible = false;
                    label5.Visible = true;
                }
                else
                {
                    btnChon.Visible = true;
                    //btnChon.Text = "Chọn nhiều hợp đồng";
                    label5.Visible = false;
                }

                ccNgayKyHD_Den.Value = DateTime.Now;
                if (IsBrowseForm)
                {
                    label5.Visible = this.IsBrowseForm;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmMain.CommandBars.Clear();
                    if (IsDaDuyet)
                    {
                        cbStatus.SelectedValue = "1";
                        cbStatus.ReadOnly = true;
                    }
                }
                else
                {

                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (!IsBrowseForm)
                    {
                        long ID = (long)Convert.ToInt32(e.Row.Cells["ID"].Text);

                        HopDongEditForm hopdong = new HopDongEditForm();
                        hopdong.HD = HopDong.Load(ID);
                        if (hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            hopdong.OpenType = OpenFormType.Edit;
                        else
                            hopdong.OpenType = OpenFormType.Edit;
                        hopdong.ShowDialog();

                        btnSearch.PerformClick();
                    }
                    else
                    {
                        HopDongSelected = (HopDong)e.Row.DataRow;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells[6].Text == TrangThaiXuLy.CHO_DUYET.ToString())
                        e.Row.Cells[6].Text = "Chờ duyệt";
                    //if (GlobalSettings.NGON_NGU == "1") { e.Row.Cells[6].Text = "Wait to approve"; }
                    else if (e.Row.Cells[6].Text == TrangThaiXuLy.CHUA_KHAI_BAO.ToString())
                        e.Row.Cells[6].Text = "Chưa khai báo";
                    else if (e.Row.Cells[6].Text == TrangThaiXuLy.DA_DUYET.ToString())
                        e.Row.Cells[6].Text = "Đã duyệt";
                    else if (e.Row.Cells[6].Text == TrangThaiXuLy.KHONG_PHE_DUYET.ToString())
                        e.Row.Cells[6].Text = "Không phê duyệt";
                    else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY.ToString())
                        e.Row.Cells[6].Text = "Chờ hủy";
                    else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY.ToString())
                        e.Row.Cells[6].Text = "Đã hủy";
                    else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET.ToString())
                        e.Row.Cells[6].Text = "Sửa";
                    else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString())
                        e.Row.Cells[6].Text = "Đã khai báo";
                    if (dgList.RootTable.Columns[7].Visible == true)
                    {
                        if (e.Row.Cells[7].Value.ToString() == "1")
                        {
                            e.Row.Cells[7].Text = "Đã duyệt";
                        }
                        else
                            e.Row.Cells[7].Text = "Chưa duyệt";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void search()
        {
            try
            {
                string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' ";
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan like '%" + txtSoTiepNhan.Text.Trim() + "%'";
                }
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    where += " and sohopdong like '%" + txtSoHopDong.Text.Trim() + "%'";

                }
                if (ccNgayKyHD.Text.Length > 0)
                {
                    where += " and NgayKy>='" + ccNgayKyHD.Value.Month.ToString() + "/" + ccNgayKyHD.Value.Day.ToString() + "/" + ccNgayKyHD.Value.Year + "'  and NgayKy<='" + ccNgayKyHD_Den.Value.Month.ToString() + "/" + ccNgayKyHD_Den.Value.Day.ToString() + "/" + ccNgayKyHD_Den.Value.Year + "'";
                }
                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text.Trim();
                }
                if (cbStatus.Text.Length > 0)
                    where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                if (isTaoHoSo)
                {
                    where += " and TrangThaiThanhKhoan = 2";
                    where += " AND ID not in(Select HopDong_ID from t_KDT_GC_HoSoQuyetToan_DSHopDong)";
                }

                Company.GC.BLL.KDT.GC.HopDong hd = new HopDong();
                try
                {
                    collection = HopDong.SelectCollectionDynamic(where, "NgayKy asc");
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                dgList.Refresh();
                dgList.DataSource = collection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.search();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }



        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdCSDaDuyet": 
                    ChuyenTrangThai(); 
                    break;
                case "InPhieuTN": 
                    this.inPhieuTN(); 
                    break;
                case "cmdExportExcel": 
                    this.ExportExcel(); 
                    break;
                case "cmdDelete": 
                    this.btnXoa_Click(null,null);
                    break;
                case "cmdUpdateStatus": 
                    this.UpdateStatus(); 
                    break;
            }

        }
        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<HopDong> hdColl = new List<HopDong>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            hdColl.Add((HopDong)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < hdColl.Count; i++)
                        {
                            hdColl[i] = HopDong.Load(hdColl[i].ID);
                            hdColl[i].LoadCollection();
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.HD = hdColl[i];
                            f.formType = "HD";
                            f.ShowDialog(this);
                        }
                        this.search();
                    }
                    else
                    {
                        showMsg("MSG_2702052");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            
        }
        private void ExportExcel()
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách hợp đồng " + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessageTQDT(" Thông báo từ hệ thống "," Doanh nghiệp có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ","Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessageTQDT(" Thông báo từ hệ thống ","Xuất Excel không thành công", false);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "HỢP ĐỒNG";
                Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDong hdDangKySelected = (HopDong)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = hdDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = hdDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                        //break;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }


        }
        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    List<HopDong> hdColl = new List<HopDong>();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        hdColl.Add((HopDong)grItem.GetRow().DataRow);
                    }
                    for (int i = 0; i < hdColl.Count; i++)
                    {
                        if (hdColl[i].TrangThaiXuLy==TrangThaiXuLy.CHO_DUYET)
                        {
                            hdColl[i].LoadCollection();
                            string msg = "Bạn có muốn chuyển trạng thái của hợp đồng được chọn sang đã duyệt không?";
                            msg += "\n\nSố thứ tự của hợp đồng: " + hdColl[i].ID.ToString();
                            msg += "\nCó " + hdColl[i].NhomSPCollection.Count.ToString() + " sản phẩm gia công";
                            msg += "\nCó " + hdColl[i].NPLCollection.Count.ToString() + " nguyên phụ liệu đăng ký";
                            msg += "\nCó " + hdColl[i].SPCollection.Count.ToString() + " sản phẩm đăng ký";
                            msg += "\nCó " + hdColl[i].TBCollection.Count.ToString() + " thiết bị đăng ký đăng ký";

                            string[] args = new string[5];
                            args[0] = hdColl[i].ID.ToString();
                            args[1] = hdColl[i].NhomSPCollection.Count.ToString();
                            args[2] = hdColl[i].NPLCollection.Count.ToString();
                            args[3] = hdColl[i].SPCollection.Count.ToString();
                            args[4] = hdColl[i].TBCollection.Count.ToString();

                            if (showMsg("MSG_0203070", args, true) == "Yes")
                            {
                                //if (hdColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                                //{
                                //    hdColl[i].NgayTiepNhan = DateTime.Now;
                                //}
                                //if (hdColl[i].SoTiepNhan == 0)
                                //{
                                //  hdColl[i].SoTiepNhan = hdColl[i].SoTiepNhanMax() + 1;
                                //}
                                hdColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                hdColl[i].Update();
                                Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                                    hdColl[i].ID,
                                    hdColl[i].GUIDSTR, Company.KDT.SHARE.Components.MessageTypes.HopDong, Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                                    string.Format("Trước khi chuyển trạng thái ID={0},Trạng thái ={1},Số tiếp nhận{2},Hợp đồng={3}",
                                      hdColl[i].TrangThaiXuLy, hdColl[i].ID, hdColl[i].SoTiepNhan, hdColl[i].SoHopDong));

                            }
                        }
                        else
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ","Chỉ những HĐ đã khai báo đến HQ và cấp số TN khai báo và được HQ duyệt mới được phép dùng chức năng này .",false);
                            return;
                        }
                    }
                    this.search();
                }
                else
                {
                    showMsg("MSG_2702052");

                    //MLMessages("Chưa có dữ liệu được chọn!","MSG_WRN11","", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    int j = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.HopDong hd = (Company.GC.BLL.KDT.GC.HopDong)i.GetRow().DataRow;
                            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                            sendXML.LoaiHS = "HD";
                            sendXML.master_id = hd.ID;
                            if (sendXML.Load())
                            {
                                j = i.Position + 1;
                                showMsg("MSG_2702012", i.Position + 1);                               
                            }
                            else
                            {
                                if (hd.ID > 0)
                                {
                                    hd.Delete();
                                }
                            }
                        }
                    }
                    btnSearch_Click(null,null);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (collection.Count <= 0) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int j = 0;
                if (items.Count <= 0) return;
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa hợp đồng này không?", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.HopDong hd = (Company.GC.BLL.KDT.GC.HopDong)i.GetRow().DataRow;
                            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                            sendXML.LoaiHS = "HD";
                            sendXML.master_id = hd.ID;
                            if (sendXML.Load())
                            {
                                j = i.Position + 1;
                                showMsg("MSG_2702012", i.Position + 1);                               
                            }
                            else
                            {
                                if (hd.ID > 0)
                                {
                                    hd.Delete();
                                }
                            }
                        }
                    }
                    this.search();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow() == null)
                {
                    showMsg("MSG_0203075");
                    return;
                }
                HopDongSelected = (HopDong)dgList.GetRow().DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = HopDongSelected.ID;
                form.DeclarationIssuer = DeclarationIssuer.HOP_DONG_GIA_CONG;
                form.ShowDialog(this);
                //Globals.ShowKetQuaXuLyBoSung(HopDongSelected.GUIDSTR);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        //-----------------------------------------------------------------------------------------

        private void btnChon_Click(object sender, EventArgs e)
        {
            try
            {
                NAMQUYETTOAN = Convert.ToInt32(txtNamQT.Text);
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int i = 0;
                foreach (GridEXSelectedItem item in items)
                {
                    GridEXRow r = item.GetRow();
                    i++;
                    if (i != items.Count)
                    {
                        ListID_HopDongSelected += "" + r.Cells["ID"].Text + ",";
                        ListHopDongSelected += "'" + r.Cells[1].Text + "',";
                        LIST_HOPDONG_ID += r.Cells["ID"].Text + ",";
                    }
                    else
                    {
                        ListID_HopDongSelected += "" + r.Cells["ID"].Text + "";
                        ListHopDongSelected += "'" + r.Cells[1].Text + "'";
                        LIST_HOPDONG_ID += r.Cells["ID"].Text + ",";
                    }
                    try
                    {
                        list_HopDong.Add(HopDong.Load(Int32.Parse(r.Cells["ID"].Text)));
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "HD";
            f.ShowDialog(this);
        }
    }
}