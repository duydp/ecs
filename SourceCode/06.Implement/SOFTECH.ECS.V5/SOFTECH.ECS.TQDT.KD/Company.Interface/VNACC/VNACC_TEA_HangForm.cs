﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TEA_HangForm : BaseForm
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();
        private KDT_VNACCS_TEA_HangHoa Hang = null;
        public VNACC_TEA_HangForm()
        {
            InitializeComponent();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                bool isAddNew = false;
                if (Hang == null)
                {
                    Hang = new KDT_VNACCS_TEA_HangHoa();
                    isAddNew = true;
                }
                GetHang();
                if (isAddNew)
                    TEA.HangCollection.Add(Hang);
                grListHang.DataSource = TEA.HangCollection;
                grListHang.Refetch();
                Hang = new KDT_VNACCS_TEA_HangHoa();
                SetHang();
                Hang = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void SetHang()
        {
            txtMoTaHangHoa.Text = Hang.MoTaHangHoa;
            txtSoLuongDangKyMT.Text = Hang.SoLuongDangKyMT.ToString();
            ctrSoLuongDangKyMT.Code = Hang.DVTSoLuongDangKyMT;
            txtSoLuongDaSuDung.Text = Hang.SoLuongDaSuDung.ToString();
            ctrSoLuongDaSuDung.Code = Hang.DVTSoLuongDaSuDung;
            UIGroup.Text = Hang.TriGia.ToString();
            txtTriGiaDuKien.Text = Hang.TriGiaDuKien.ToString();

        }
        private void GetHang()
        {
            Hang.MoTaHangHoa = txtMoTaHangHoa.Text;
            Hang.SoLuongDangKyMT = Convert.ToDecimal(txtSoLuongDangKyMT.Value);
            Hang.DVTSoLuongDangKyMT = ctrSoLuongDangKyMT.Code;
            Hang.SoLuongDaSuDung = Convert.ToDecimal(txtSoLuongDaSuDung.Value);
            Hang.DVTSoLuongDaSuDung = ctrSoLuongDaSuDung.Code;
            Hang.TriGia = Convert.ToDecimal(txtTriGia.Value);
            Hang.TriGiaDuKien = Convert.ToDecimal(txtTriGiaDuKien.Value);
        }

        private void VNACC_TEA_HangForm_Load(object sender, EventArgs e)
        {
            grListHang.DataSource = TEA.HangCollection;
        }

        private void grListHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (grListHang.GetRows().Length < 0) return;
            Hang = (KDT_VNACCS_TEA_HangHoa)grListHang.CurrentRow.DataRow;
            SetHang();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grListHang.SelectedItems;
                List<KDT_VNACCS_TEA_HangHoa> ItemColl = new List<KDT_VNACCS_TEA_HangHoa>();
                if (grListHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TEA_HangHoa)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TEA_HangHoa item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TEA.HangCollection.Remove(item);
                    }

                    grListHang.DataSource = TEA.HangCollection;
                    grListHang.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }


    }
}
