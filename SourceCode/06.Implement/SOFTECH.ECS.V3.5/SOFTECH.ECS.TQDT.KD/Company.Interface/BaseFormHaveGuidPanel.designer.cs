﻿namespace Company.Interface
{
    partial class BaseFormHaveGuidPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseFormHaveGuidPanel));
            this.uiPanelManager = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanelGuide = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanelGuideContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.txtGuide = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(203, 3);
            this.grbMain.Size = new System.Drawing.Size(249, 189);
            // 
            // uiPanelManager
            // 
            this.uiPanelManager.ContainerControl = this;
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 189), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Image = ((System.Drawing.Image)(resources.GetObject("uiPanelGuide.Image")));
            this.uiPanelGuide.InnerContainer = this.uiPanelGuideContainer;
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 3);
            this.uiPanelGuide.Name = "uiPanelGuide";
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 189);
            this.uiPanelGuide.TabIndex = 4;
            this.uiPanelGuide.TabStop = false;
            this.uiPanelGuide.Text = "Hướng dẫn nhập liệu";
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Controls.Add(this.txtGuide);
            this.uiPanelGuideContainer.Location = new System.Drawing.Point(1, 23);
            this.uiPanelGuideContainer.Name = "uiPanelGuideContainer";
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 165);
            this.uiPanelGuideContainer.TabIndex = 0;
            // 
            // txtGuide
            // 
            this.txtGuide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGuide.Location = new System.Drawing.Point(0, 0);
            this.txtGuide.Multiline = true;
            this.txtGuide.Name = "txtGuide";
            this.txtGuide.ReadOnly = true;
            this.txtGuide.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGuide.Size = new System.Drawing.Size(194, 165);
            this.txtGuide.TabIndex = 0;
            this.txtGuide.TabStop = false;
            this.txtGuide.VisualStyleManager = this.vsmMain;
            // 
            // BaseFormHaveGuidPanel
            // 
            this.ClientSize = new System.Drawing.Size(455, 195);
            this.Controls.Add(this.uiPanelGuide);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaseFormHaveGuidPanel";
            this.helpProvider1.SetShowHelp(this, true);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.UI.Dock.UIPanelManager uiPanelManager;
        protected Janus.Windows.UI.Dock.UIPanel uiPanelGuide;
        protected Janus.Windows.GridEX.EditControls.EditBox txtGuide;
        protected Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanelGuideContainer;
    }
}
