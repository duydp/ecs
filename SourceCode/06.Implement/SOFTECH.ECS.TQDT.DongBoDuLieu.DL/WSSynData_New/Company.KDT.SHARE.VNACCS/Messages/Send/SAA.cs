﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class SAA : BasicVNACC
    {
        public List<SAA_HANGHOA> ListHangHoa { get; set; }
        public StringBuilder BuilEdiMessagesSAA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<SAA>(StrBuild, true, GlobalVNACC.PathConfig, "SAA");
            foreach (SAA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<SAA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "SAA_HANGHOA");
            }
            return str;
        }

    }
}
