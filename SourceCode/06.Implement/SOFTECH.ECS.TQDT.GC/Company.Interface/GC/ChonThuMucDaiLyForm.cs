﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.Xml.Serialization;
using System.IO;

namespace Company.Interface
{
    public partial class ChonThuMucDaiLyForm : BaseForm
    {
        public string ThuMucLuu;
        public ChonThuMucDaiLyForm()
        {
            InitializeComponent();
        }

        private void btnXuat_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtThuMuc.Text == "" || txtThuMuc.Text == null)
                {
                    ShowMessage("Bạn chưa chọn thư mục chứa các file nhập từ dại lý !", false);
                    return;
                }
                DirectoryInfo dir = new DirectoryInfo(ThuMucLuu);
                if (!dir.Exists)
                {
                    ShowMessage("Thư mục này không tồn tại !", false);
                    return;
                }
                //load thong tin hop dong
                HopDongCollection HDFileCollection = new HopDongCollection();
                XmlSerializer serializer = new XmlSerializer(typeof(HopDongCollection));
                FileInfo fileInfo = new FileInfo(dir.FullName + "\\HopDong.xml");
                FileStream fs = null;
                if (fileInfo.Exists)
                {
                    fs = new FileStream(dir.FullName + "\\HopDong.xml", FileMode.Open);
                    HDFileCollection = (HopDongCollection)serializer.Deserialize(fs);
                    fs.Close();

                }
                ToKhaiChuyenTiepCollection TKCTFileCollection = new ToKhaiChuyenTiepCollection();
                fileInfo = new FileInfo(dir.FullName + "\\ToKhaiGiaCongChuyenTiep.xml");
                if (fileInfo.Exists)
                {
                    serializer = new XmlSerializer(typeof(ToKhaiChuyenTiepCollection));
                    fs = new FileStream(dir.FullName + "\\ToKhaiGiaCongChuyenTiep.XML", FileMode.Open);
                    TKCTFileCollection = (ToKhaiChuyenTiepCollection)serializer.Deserialize(fs);
                    fs.Close();
                }

                ToKhaiMauDichCollection TKMDFileCollection = new ToKhaiMauDichCollection();
                fileInfo = new FileInfo(dir.FullName + "\\ToKhaiMauDich.xml");
                if (fileInfo.Exists)
                {
                    serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    fs = new FileStream(dir.FullName + "\\ToKhaiMauDich.XML", FileMode.Open);
                    TKMDFileCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();
                }
                Company.GC.BLL.GC.DinhMucCollection DMFileCollection = new Company.GC.BLL.GC.DinhMucCollection();
                fileInfo = new FileInfo(dir.FullName + "\\DinhMuc.xml");
                if (fileInfo.Exists)
                {
                    serializer = new XmlSerializer(typeof(Company.GC.BLL.GC.DinhMucCollection));
                    fs = new FileStream(dir.FullName + "\\DinhMuc.XML", FileMode.Open);
                    DMFileCollection = (Company.GC.BLL.GC.DinhMucCollection)serializer.Deserialize(fs);
                    fs.Close();
                }
                PhuKienDangKyCollection PKDKFileCollection = new PhuKienDangKyCollection();
                fileInfo = new FileInfo(dir.FullName + "\\PhuKien.xml");
                if (fileInfo.Exists)
                {
                    serializer = new XmlSerializer(typeof(PhuKienDangKyCollection));
                    fs = new FileStream(dir.FullName + "\\PhuKien.XML", FileMode.Open);
                    PKDKFileCollection = (PhuKienDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                }
                BKCungUngDangKyCollection BKCUFileCollection = new BKCungUngDangKyCollection();
                fileInfo = new FileInfo(dir.FullName + "\\BangKeCungUng.xml");
                if (fileInfo.Exists)
                {
                    serializer = new XmlSerializer(typeof(BKCungUngDangKyCollection));
                    fs = new FileStream(dir.FullName + "\\BangKeCungUng.XML", FileMode.Open);
                    BKCUFileCollection = (BKCungUngDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                }
                HopDongCollection HDCollection = new HopDong().SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
                foreach (HopDong HD in HDFileCollection)
                {
                    bool isExitHopDong = false;
                    foreach (HopDong HDinDatabase in HDCollection)
                    {
                        if (HD.SoHopDong.Trim().ToUpper() == HDinDatabase.SoHopDong.Trim().ToUpper() && HD.NgayKy == HDinDatabase.NgayKy && HD.MaHaiQuan == HDinDatabase.MaHaiQuan)
                        {
                            isExitHopDong = true;
                            foreach (ToKhaiChuyenTiep TKCT in TKCTFileCollection)
                            {
                                if (TKCT.IDHopDong == HD.ID)
                                {
                                    TKCT.IDHopDong = HDinDatabase.ID;
                                }
                            }
                            foreach (ToKhaiMauDich TKMD in TKMDFileCollection)
                            {
                                if (TKMD.IDHopDong == HD.ID)
                                {
                                    TKMD.IDHopDong = HDinDatabase.ID;
                                }
                            }
                            foreach (PhuKienDangKy PKDK in PKDKFileCollection)
                            {
                                if (PKDK.HopDong_ID == HD.ID)
                                {
                                    PKDK.HopDong_ID = HDinDatabase.ID;
                                }
                            }

                            foreach (Company.GC.BLL.GC.DinhMuc DM in DMFileCollection)
                            {
                                if (DM.HopDong_ID == HD.ID)
                                {
                                    DM.HopDong_ID = HDinDatabase.ID;
                                }
                            }
                        }
                    }
                    if (!isExitHopDong)
                    {
                        long idHopDongCu = HD.ID;
                        HD.ID = 0;
                        HD.InsertUpdateHopDong();
                        foreach (ToKhaiChuyenTiep TKCT in TKCTFileCollection)
                        {
                            if (TKCT.IDHopDong == idHopDongCu)
                            {
                                TKCT.IDHopDong = HD.ID;
                            }
                        }
                        foreach (ToKhaiMauDich TKMD in TKMDFileCollection)
                        {
                            if (TKMD.IDHopDong == idHopDongCu)
                            {
                                TKMD.IDHopDong = HD.ID;
                            }
                        }
                        foreach (PhuKienDangKy PKDK in PKDKFileCollection)
                        {
                            if (PKDK.HopDong_ID == idHopDongCu)
                            {
                                PKDK.HopDong_ID = HD.ID;
                            }
                        }

                        foreach (Company.GC.BLL.GC.DinhMuc DM in DMFileCollection)
                        {
                            if (DM.HopDong_ID == idHopDongCu)
                            {
                                DM.HopDong_ID = HD.ID;
                            }
                        }
                    }

                }
                #region GHi de du lieu

                if (cbStatus.SelectedValue.ToString().Trim() == "-1")
                {
                    if (TKCTFileCollection.Count > 0)
                        (new ToKhaiChuyenTiep()).InsertUpdateDongBoDuLieu(TKCTFileCollection, GlobalSettings.MA_DON_VI);

                    if (TKMDFileCollection.Count > 0)
                        (new ToKhaiMauDich()).InsertUpdateDongBoDuLieu(TKMDFileCollection, GlobalSettings.MA_DON_VI);

                    if (PKDKFileCollection.Count > 0)
                        (new PhuKienDangKy()).InsertUpdateDongBoDuLieu(PKDKFileCollection, GlobalSettings.MA_DON_VI);


                    if (DMFileCollection.Count > 0)
                        (new Company.GC.BLL.GC.DinhMuc()).InsertUpdate(DMFileCollection, GlobalSettings.MA_DON_VI);
                }
                else
                {
                    if (TKCTFileCollection.Count > 0)
                        (new ToKhaiChuyenTiep()).InsertUpdateDongBoDuLieuMoi(TKCTFileCollection);

                    if (TKMDFileCollection.Count > 0)
                        (new ToKhaiMauDich()).InsertUpdateDongBoDuLieuMoi(TKMDFileCollection);

                    if (PKDKFileCollection.Count > 0)
                        (new PhuKienDangKy()).InsertUpdateDongBoDuLieuMoi(PKDKFileCollection);


                    if (DMFileCollection.Count > 0)
                        (new Company.GC.BLL.GC.DinhMuc()).InsertUpdateMoi(DMFileCollection);
                }



                #endregion GHi de du lieu
                ShowMessage("Đồng bộ dữ liệu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Đồng bộ dữ liệu thành công."+ex.Message, false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            if (ThuMucDaiLy.ShowDialog() == DialogResult.OK)
                ThuMucLuu = ThuMucDaiLy.SelectedPath;
            this.txtThuMuc.Text = ThuMucLuu;


        }

        #region Kiểm tra điều kiện hợp đồng trước khi nhập

        public string KiemTraTonTaiHopDongNhap(HopDongCollection HDCollection)
        {
            string st = "";
            string sohopdong = "";
            Company.GC.BLL.KDT.GC.HopDongCollection HDCollectionExit = new HopDongCollection();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.checkSoHopDongExit(HopDongDK.SoHopDong, HopDongDK.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }

        public string KiemTraHopDongDoanhNghiep(HopDongCollection HDCollection)
        {
            string st = "";
            string sohopdong = "";
            Company.GC.BLL.KDT.GC.HopDongCollection HDCollectionExit = new HopDongCollection();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }

        #endregion

        #region Kiểm tra điều kiện định mức trước khi nhập

        private string KiemTraTonTaiDinhMucNhap(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionExit = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (DM.CheckExitsDinhMucSanPhamNguyenPhuLieu())
                {
                    DMCollectionExit.Add(DM);
                    if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                    {
                        masanpham = DM.MaSanPham;
                        st += masanpham + ", ";
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionExit)
            {
                DMCollection.Remove(DM);
            }
            return st;
        }

        private string KiemTraDinhMucSanPham(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionSP = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.SanPhamCollection SPCollection = new Company.GC.BLL.GC.SanPhamCollection();
            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            SPCollection = SP.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.SanPham SPExist in SPCollection)
                {
                    if (DM.MaSanPham == SPExist.Ma)
                    {
                        DMCollectionSP.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                {
                    masanpham = DM.MaSanPham;
                    st += masanpham + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucHopDong(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string mahopdong = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionHD = new Company.GC.BLL.GC.DinhMucCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (DM.HopDong_ID == HDExist.ID)
                    {
                        DMCollectionHD.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (mahopdong.Trim().ToUpper() != DM.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = DM.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucNguyenPhuLieu(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string manguyenphulieu = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionNPL = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollection = new Company.GC.BLL.GC.NguyenPhuLieuCollection();
            Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLCollection = NPL.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLExist in NPLCollection)
                {
                    if (DM.MaNguyenPhuLieu == NPLExist.Ma)
                    {
                        DMCollectionNPL.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (manguyenphulieu.Trim().ToUpper() != DM.MaNguyenPhuLieu.Trim().ToUpper())
                {
                    manguyenphulieu = DM.MaNguyenPhuLieu;
                    st += manguyenphulieu + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        #endregion

        #region Kiểm tra điều kiện phụ kiện trước khi nhập

        public string KiemTraTonTaiPhuKienNhap(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.checkExitsSoPK(PK.SoPhuKien, PK.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }

        public string KiemTraPhuKienHopDong(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string mahopdong = "";
            PhuKienDangKyCollection PKCollectionHD = new PhuKienDangKyCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (PK.HopDong_ID == HDExist.ID)
                    {
                        PKCollectionHD.Add(PK);
                        break;
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Remove(PK);
            }
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (mahopdong.Trim().ToUpper() != PK.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = PK.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            PKCollection.Clear();
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Add(PK);
            }
            return st;
        }

        public string KiemTraPhuKienDoanhNghiep(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }

        #endregion

        #region Kiểm tra điều kiện tờ khai mậu dịch trước khi nhập

        public string KiemTraTonTaiTKMDNhap(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExist = new ToKhaiMauDichCollection();
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.CheckExistToKhaiMauDich(TKMD.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NgayDangKy))
                {
                    sotokhai = TKMD.SoToKhai + "/Ngày " + TKMD.NgayDangKy;
                    st += sotokhai;
                    TKMDCollectionExist.Add(TKMD);
                }
            }
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollectionExist)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDHopDong(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string mahopdong = "";
            ToKhaiMauDichCollection TKMDCollectionHD = new ToKhaiMauDichCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKMD.IDHopDong == HDExist.ID)
                    {
                        TKMDCollectionHD.Add(TKMD);
                        break;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Remove(TKMD);
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKMD.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKMD.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKMDCollection.Clear();
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Add(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDDoanhNghiep(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExit = new ToKhaiMauDichCollection();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKMDCollectionExit.Add(TKMD);
                    if (sotokhai.Trim().ToUpper() != TKMD.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKMD.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionExit)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }

        #endregion

        #region Kiêm tra điều kiện tờ khai GCCT trước khi nhập

        public string KiemTraTonTaiTKCTNhap(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionExist = new ToKhaiChuyenTiepCollection();
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.CheckExistToKhaiChuyenTiep(TKCT.MaHaiQuanTiepNhan, TKCT.MaLoaiHinh, TKCT.SoToKhai, TKCT.NgayDangKy))
                {
                    sotokhai = TKCT.SoToKhai + "/Ngày " + TKCT.NgayDangKy;
                    st += sotokhai;
                    TKCTCollectionExist.Add(TKCT);
                }
            }
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollectionExist)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTHopDong(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string mahopdong = "";
            ToKhaiChuyenTiepCollection TKCTCollectionHD = new ToKhaiChuyenTiepCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKCT.IDHopDong == HDExist.ID)
                    {
                        TKCTCollectionHD.Add(TKCT);
                        break;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Remove(TKCT);
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKCT.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKCT.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKCTCollection.Clear();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Add(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTDoanhNghiep(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionExit = new ToKhaiChuyenTiepCollection();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKCTCollectionExit.Add(TKCT);
                    if (sotokhai.Trim().ToUpper() != TKCT.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKCT.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionExit)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }

        #endregion

        private void ChonThuMucDaiLyForm_Load(object sender, EventArgs e)
        {
            cbStatus.SelectedIndex = 0;
        }

        #region Kiểm tra điều kiện bảng kê cung ứng trước khi nhập


        #endregion
    }
}