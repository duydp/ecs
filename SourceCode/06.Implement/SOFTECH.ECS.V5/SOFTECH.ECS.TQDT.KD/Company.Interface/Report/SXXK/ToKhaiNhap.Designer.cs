﻿namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiNhap));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrNTX = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMienThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMienThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThueGTGT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenChungTu5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChungTu6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueXNKSo = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongThueXNKChu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPTTT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgoaiTe = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDKGH = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDenPTVT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPTVT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoiTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrKD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPLTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.ptbImage = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrNTX,
            this.lblMienThue1,
            this.lblMienThue2,
            this.xrDT,
            this.lblTongTriGiaThuKhac,
            this.lblTongTienThueGTGT,
            this.xrTable3,
            this.lblTenChungTu5,
            this.lblTenChungTu6,
            this.lblSoBanChinh6,
            this.lblSoBanSao6,
            this.lblSoBanSao5,
            this.lblSoBanChinh5,
            this.lblSoBanChinh4,
            this.lblSoBanChinh3,
            this.lblSoBanChinh2,
            this.lblSoBanChinh1,
            this.lblSoBanSao1,
            this.lblSoBanSao2,
            this.lblSoBanSao3,
            this.lblSoBanSao4,
            this.lblSoTiepNhan,
            this.xrTable2,
            this.xrTable1,
            this.lblTongThueXNKSo,
            this.lblMaDiaDiemDoHang,
            this.lblMaHaiQuan,
            this.xrLabel3,
            this.lblSoToKhai,
            this.lblMaDaiLyTTHQ,
            this.lblMaNguoiUyThac,
            this.lblTongThueXNKChu,
            this.lblTongThueXNK,
            this.lblPTTT,
            this.lblTyGiaTT,
            this.lblNgoaiTe,
            this.lblDKGH,
            this.lblDiaDiemDoHang,
            this.lblDiaDiemXepHang,
            this.lblMaNuoc,
            this.lblTenNuoc,
            this.lblSoHopDong,
            this.lblNgayHHHopDong,
            this.lblNgayHopDong,
            this.lblNgayVanTaiDon,
            this.lblSoGiayPhep,
            this.lblNgayGiayPhep,
            this.lblNgayHHGiayPhep,
            this.lblSoVanTaiDon,
            this.lblNgayDenPTVT,
            this.lblSoPTVT,
            this.lblNgayHoaDon,
            this.lblSoHoaDon,
            this.lblTenDaiLyTTHQ,
            this.lblTenDoanhNghiep,
            this.lblNguoiUyThac,
            this.lblTenDoiTac,
            this.xrKD,
            this.lblMaDoanhNghiep,
            this.lblSoPLTK,
            this.lblChiCucHaiQuan,
            this.lblCucHaiQuan,
            this.lblNgayDangKy,
            this.ptbImage});
            this.Detail.Height = 1127;
            this.Detail.Name = "Detail";
            // 
            // xrNTX
            // 
            this.xrNTX.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrNTX.Location = new System.Drawing.Point(467, 217);
            this.xrNTX.Name = "xrNTX";
            this.xrNTX.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrNTX.ParentStyleUsing.UseFont = false;
            this.xrNTX.Size = new System.Drawing.Size(17, 16);
            this.xrNTX.Text = "×";
            this.xrNTX.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrNTX.Visible = false;
            // 
            // lblMienThue1
            // 
            this.lblMienThue1.CanGrow = false;
            this.lblMienThue1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblMienThue1.Location = new System.Drawing.Point(58, 783);
            this.lblMienThue1.Multiline = true;
            this.lblMienThue1.Name = "lblMienThue1";
            this.lblMienThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue1.ParentStyleUsing.UseFont = false;
            this.lblMienThue1.Size = new System.Drawing.Size(242, 75);
            this.lblMienThue1.Text = "ad";
            this.lblMienThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMienThue1.Visible = false;
            // 
            // lblMienThue2
            // 
            this.lblMienThue2.CanGrow = false;
            this.lblMienThue2.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.lblMienThue2.Location = new System.Drawing.Point(317, 783);
            this.lblMienThue2.Multiline = true;
            this.lblMienThue2.Name = "lblMienThue2";
            this.lblMienThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue2.ParentStyleUsing.UseFont = false;
            this.lblMienThue2.Size = new System.Drawing.Size(300, 75);
            this.lblMienThue2.Text = "ad";
            this.lblMienThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMienThue2.Visible = false;
            // 
            // xrDT
            // 
            this.xrDT.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrDT.Location = new System.Drawing.Point(449, 198);
            this.xrDT.Name = "xrDT";
            this.xrDT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDT.ParentStyleUsing.UseFont = false;
            this.xrDT.Size = new System.Drawing.Size(17, 16);
            this.xrDT.Text = "×";
            this.xrDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrDT.Visible = false;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.Location = new System.Drawing.Point(667, 867);
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.Size = new System.Drawing.Size(116, 25);
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongTienThueGTGT
            // 
            this.lblTongTienThueGTGT.Location = new System.Drawing.Point(500, 867);
            this.lblTongTienThueGTGT.Name = "lblTongTienThueGTGT";
            this.lblTongTienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueGTGT.Size = new System.Drawing.Size(119, 25);
            this.lblTongTienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(50, 675);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.Size = new System.Drawing.Size(733, 25);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrongLuong,
            this.lblPhiBaoHiem,
            this.xrTableCell3,
            this.lblTongTriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(733, 25);
            // 
            // lblTrongLuong
            // 
            this.lblTrongLuong.CanGrow = false;
            this.lblTrongLuong.Location = new System.Drawing.Point(0, 0);
            this.lblTrongLuong.Name = "lblTrongLuong";
            this.lblTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrongLuong.Size = new System.Drawing.Size(267, 25);
            this.lblTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.CanGrow = false;
            this.lblPhiBaoHiem.Location = new System.Drawing.Point(267, 0);
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.Size = new System.Drawing.Size(308, 25);
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Location = new System.Drawing.Point(575, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Size = new System.Drawing.Size(58, 25);
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.CanGrow = false;
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(633, 0);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(100, 25);
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenChungTu5
            // 
            this.lblTenChungTu5.Location = new System.Drawing.Point(40, 1038);
            this.lblTenChungTu5.Name = "lblTenChungTu5";
            this.lblTenChungTu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu5.Size = new System.Drawing.Size(108, 17);
            // 
            // lblTenChungTu6
            // 
            this.lblTenChungTu6.Location = new System.Drawing.Point(40, 1058);
            this.lblTenChungTu6.Name = "lblTenChungTu6";
            this.lblTenChungTu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu6.Size = new System.Drawing.Size(108, 17);
            // 
            // lblSoBanChinh6
            // 
            this.lblSoBanChinh6.Location = new System.Drawing.Point(192, 1058);
            this.lblSoBanChinh6.Name = "lblSoBanChinh6";
            this.lblSoBanChinh6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh6.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao6
            // 
            this.lblSoBanSao6.Location = new System.Drawing.Point(300, 1058);
            this.lblSoBanSao6.Name = "lblSoBanSao6";
            this.lblSoBanSao6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao6.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao5
            // 
            this.lblSoBanSao5.Location = new System.Drawing.Point(300, 1038);
            this.lblSoBanSao5.Name = "lblSoBanSao5";
            this.lblSoBanSao5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao5.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh5
            // 
            this.lblSoBanChinh5.Location = new System.Drawing.Point(192, 1038);
            this.lblSoBanChinh5.Name = "lblSoBanChinh5";
            this.lblSoBanChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh5.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh4
            // 
            this.lblSoBanChinh4.Location = new System.Drawing.Point(192, 1018);
            this.lblSoBanChinh4.Name = "lblSoBanChinh4";
            this.lblSoBanChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh4.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh3
            // 
            this.lblSoBanChinh3.Location = new System.Drawing.Point(192, 998);
            this.lblSoBanChinh3.Name = "lblSoBanChinh3";
            this.lblSoBanChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh3.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh2
            // 
            this.lblSoBanChinh2.Location = new System.Drawing.Point(192, 978);
            this.lblSoBanChinh2.Name = "lblSoBanChinh2";
            this.lblSoBanChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh2.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh1
            // 
            this.lblSoBanChinh1.Location = new System.Drawing.Point(192, 958);
            this.lblSoBanChinh1.Name = "lblSoBanChinh1";
            this.lblSoBanChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh1.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao1
            // 
            this.lblSoBanSao1.Location = new System.Drawing.Point(300, 958);
            this.lblSoBanSao1.Name = "lblSoBanSao1";
            this.lblSoBanSao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao1.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao2
            // 
            this.lblSoBanSao2.Location = new System.Drawing.Point(300, 978);
            this.lblSoBanSao2.Name = "lblSoBanSao2";
            this.lblSoBanSao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao2.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao3
            // 
            this.lblSoBanSao3.Location = new System.Drawing.Point(300, 998);
            this.lblSoBanSao3.Name = "lblSoBanSao3";
            this.lblSoBanSao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao3.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao4
            // 
            this.lblSoBanSao4.Location = new System.Drawing.Point(300, 1018);
            this.lblSoBanSao4.Name = "lblSoBanSao4";
            this.lblSoBanSao4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao4.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(650, 17);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.ParentStyleUsing.UseFont = false;
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(50, 775);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable2.Size = new System.Drawing.Size(733, 90);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TriGiaTTGTGT1,
            this.ThueSuatGTGT1,
            this.TienThueGTGT1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(733, 30);
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.CanGrow = false;
            this.TriGiaTT1.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.Size = new System.Drawing.Size(100, 30);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.CanGrow = false;
            this.ThueSuatXNK1.Location = new System.Drawing.Point(100, 0);
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.CanGrow = false;
            this.TienThueXNK1.Location = new System.Drawing.Point(150, 0);
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.Size = new System.Drawing.Size(108, 30);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT1
            // 
            this.TriGiaTTGTGT1.CanGrow = false;
            this.TriGiaTTGTGT1.Location = new System.Drawing.Point(258, 0);
            this.TriGiaTTGTGT1.Name = "TriGiaTTGTGT1";
            this.TriGiaTTGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT1.Size = new System.Drawing.Size(134, 30);
            this.TriGiaTTGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT1
            // 
            this.ThueSuatGTGT1.CanGrow = false;
            this.ThueSuatGTGT1.Location = new System.Drawing.Point(392, 0);
            this.ThueSuatGTGT1.Name = "ThueSuatGTGT1";
            this.ThueSuatGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT1.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT1
            // 
            this.TienThueGTGT1.CanGrow = false;
            this.TienThueGTGT1.Location = new System.Drawing.Point(442, 0);
            this.TienThueGTGT1.Name = "TienThueGTGT1";
            this.TienThueGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT1.Size = new System.Drawing.Size(125, 30);
            this.TienThueGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.CanGrow = false;
            this.TyLeThuKhac1.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.Size = new System.Drawing.Size(50, 30);
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.CanGrow = false;
            this.TriGiaThuKhac1.Location = new System.Drawing.Point(617, 0);
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.Size = new System.Drawing.Size(116, 30);
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TriGiaTTGTGT2,
            this.ThueSuatGTGT2,
            this.TienThueGTGT2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(733, 30);
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.CanGrow = false;
            this.TriGiaTT2.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.Size = new System.Drawing.Size(100, 30);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.CanGrow = false;
            this.ThueSuatXNK2.Location = new System.Drawing.Point(100, 0);
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.CanGrow = false;
            this.TienThueXNK2.Location = new System.Drawing.Point(150, 0);
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.Size = new System.Drawing.Size(108, 30);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT2
            // 
            this.TriGiaTTGTGT2.CanGrow = false;
            this.TriGiaTTGTGT2.Location = new System.Drawing.Point(258, 0);
            this.TriGiaTTGTGT2.Name = "TriGiaTTGTGT2";
            this.TriGiaTTGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT2.Size = new System.Drawing.Size(134, 30);
            this.TriGiaTTGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT2
            // 
            this.ThueSuatGTGT2.CanGrow = false;
            this.ThueSuatGTGT2.Location = new System.Drawing.Point(392, 0);
            this.ThueSuatGTGT2.Name = "ThueSuatGTGT2";
            this.ThueSuatGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT2.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT2
            // 
            this.TienThueGTGT2.CanGrow = false;
            this.TienThueGTGT2.Location = new System.Drawing.Point(442, 0);
            this.TienThueGTGT2.Name = "TienThueGTGT2";
            this.TienThueGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT2.Size = new System.Drawing.Size(125, 30);
            this.TienThueGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.CanGrow = false;
            this.TyLeThuKhac2.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.Size = new System.Drawing.Size(50, 30);
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.CanGrow = false;
            this.TriGiaThuKhac2.Location = new System.Drawing.Point(617, 0);
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.Size = new System.Drawing.Size(116, 30);
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TriGiaTTGTGT3,
            this.ThueSuatGTGT3,
            this.TienThueGTGT3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(733, 30);
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.CanGrow = false;
            this.TriGiaTT3.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.Size = new System.Drawing.Size(100, 30);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.CanGrow = false;
            this.ThueSuatXNK3.Location = new System.Drawing.Point(100, 0);
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.CanGrow = false;
            this.TienThueXNK3.Location = new System.Drawing.Point(150, 0);
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.Size = new System.Drawing.Size(108, 30);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT3
            // 
            this.TriGiaTTGTGT3.CanGrow = false;
            this.TriGiaTTGTGT3.Location = new System.Drawing.Point(258, 0);
            this.TriGiaTTGTGT3.Name = "TriGiaTTGTGT3";
            this.TriGiaTTGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT3.Size = new System.Drawing.Size(134, 30);
            this.TriGiaTTGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT3
            // 
            this.ThueSuatGTGT3.CanGrow = false;
            this.ThueSuatGTGT3.Location = new System.Drawing.Point(392, 0);
            this.ThueSuatGTGT3.Name = "ThueSuatGTGT3";
            this.ThueSuatGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT3.Size = new System.Drawing.Size(50, 30);
            this.ThueSuatGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT3
            // 
            this.TienThueGTGT3.CanGrow = false;
            this.TienThueGTGT3.Location = new System.Drawing.Point(442, 0);
            this.TienThueGTGT3.Name = "TienThueGTGT3";
            this.TienThueGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT3.Size = new System.Drawing.Size(125, 30);
            this.TienThueGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.CanGrow = false;
            this.TyLeThuKhac3.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.Size = new System.Drawing.Size(50, 30);
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.CanGrow = false;
            this.TriGiaThuKhac3.Location = new System.Drawing.Point(617, 0);
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.Size = new System.Drawing.Size(116, 30);
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(52, 583);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.Size = new System.Drawing.Size(730, 93);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(730, 31);
            // 
            // TenHang1
            // 
            this.TenHang1.Location = new System.Drawing.Point(0, 0);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang1.Size = new System.Drawing.Size(263, 31);
            // 
            // MaHS1
            // 
            this.MaHS1.Location = new System.Drawing.Point(263, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.Size = new System.Drawing.Size(90, 31);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Location = new System.Drawing.Point(353, 0);
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.Size = new System.Drawing.Size(66, 31);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong1
            // 
            this.Luong1.Location = new System.Drawing.Point(419, 0);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.Size = new System.Drawing.Size(66, 31);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT1
            // 
            this.DVT1.Location = new System.Drawing.Point(485, 0);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.Size = new System.Drawing.Size(66, 31);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Location = new System.Drawing.Point(551, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.Size = new System.Drawing.Size(83, 31);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Location = new System.Drawing.Point(634, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Size = new System.Drawing.Size(96, 31);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(730, 31);
            // 
            // TenHang2
            // 
            this.TenHang2.Location = new System.Drawing.Point(0, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang2.Size = new System.Drawing.Size(263, 31);
            // 
            // MaHS2
            // 
            this.MaHS2.Location = new System.Drawing.Point(263, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.Size = new System.Drawing.Size(90, 31);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Location = new System.Drawing.Point(353, 0);
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.Size = new System.Drawing.Size(65, 31);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong2
            // 
            this.Luong2.Location = new System.Drawing.Point(418, 0);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.Size = new System.Drawing.Size(67, 31);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT2
            // 
            this.DVT2.Location = new System.Drawing.Point(485, 0);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.Size = new System.Drawing.Size(65, 31);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Location = new System.Drawing.Point(550, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.Size = new System.Drawing.Size(83, 31);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Location = new System.Drawing.Point(633, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Size = new System.Drawing.Size(97, 31);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(730, 31);
            // 
            // TenHang3
            // 
            this.TenHang3.Location = new System.Drawing.Point(0, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang3.Size = new System.Drawing.Size(263, 31);
            // 
            // MaHS3
            // 
            this.MaHS3.Location = new System.Drawing.Point(263, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.Size = new System.Drawing.Size(90, 31);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Location = new System.Drawing.Point(353, 0);
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.Size = new System.Drawing.Size(66, 31);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong3
            // 
            this.Luong3.Location = new System.Drawing.Point(419, 0);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.Size = new System.Drawing.Size(66, 31);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT3
            // 
            this.DVT3.Location = new System.Drawing.Point(485, 0);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.Size = new System.Drawing.Size(65, 31);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Location = new System.Drawing.Point(550, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.Size = new System.Drawing.Size(83, 31);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Location = new System.Drawing.Point(633, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Size = new System.Drawing.Size(97, 31);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongThueXNKSo
            // 
            this.lblTongThueXNKSo.Location = new System.Drawing.Point(325, 892);
            this.lblTongThueXNKSo.Name = "lblTongThueXNKSo";
            this.lblTongThueXNKSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKSo.Size = new System.Drawing.Size(216, 17);
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDiaDiemDoHang.Location = new System.Drawing.Point(692, 433);
            this.lblMaDiaDiemDoHang.Multiline = true;
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblMaDiaDiemDoHang.Size = new System.Drawing.Size(91, 23);
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblMaHaiQuan.Location = new System.Drawing.Point(475, 67);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.lblMaHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblMaHaiQuan.Size = new System.Drawing.Size(42, 16);
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel3.Location = new System.Drawing.Point(433, 67);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(42, 16);
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Location = new System.Drawing.Point(350, 67);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.ParentStyleUsing.UseFont = false;
            this.lblSoToKhai.Size = new System.Drawing.Size(58, 16);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(142, 458);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(266, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMaNguoiUyThac
            // 
            this.lblMaNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNguoiUyThac.Location = new System.Drawing.Point(142, 358);
            this.lblMaNguoiUyThac.Name = "lblMaNguoiUyThac";
            this.lblMaNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblMaNguoiUyThac.Size = new System.Drawing.Size(266, 24);
            this.lblMaNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTongThueXNKChu
            // 
            this.lblTongThueXNKChu.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTongThueXNKChu.Location = new System.Drawing.Point(92, 908);
            this.lblTongThueXNKChu.Name = "lblTongThueXNKChu";
            this.lblTongThueXNKChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKChu.ParentStyleUsing.UseFont = false;
            this.lblTongThueXNKChu.Size = new System.Drawing.Size(692, 15);
            // 
            // lblTongThueXNK
            // 
            this.lblTongThueXNK.Location = new System.Drawing.Point(200, 867);
            this.lblTongThueXNK.Name = "lblTongThueXNK";
            this.lblTongThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNK.Size = new System.Drawing.Size(108, 25);
            this.lblTongThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblPTTT
            // 
            this.lblPTTT.Location = new System.Drawing.Point(667, 500);
            this.lblPTTT.Name = "lblPTTT";
            this.lblPTTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPTTT.Size = new System.Drawing.Size(116, 31);
            this.lblPTTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.Location = new System.Drawing.Point(567, 508);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTT.Size = new System.Drawing.Size(91, 23);
            // 
            // lblNgoaiTe
            // 
            this.lblNgoaiTe.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblNgoaiTe.Location = new System.Drawing.Point(592, 458);
            this.lblNgoaiTe.Multiline = true;
            this.lblNgoaiTe.Name = "lblNgoaiTe";
            this.lblNgoaiTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgoaiTe.ParentStyleUsing.UseFont = false;
            this.lblNgoaiTe.Size = new System.Drawing.Size(75, 23);
            // 
            // lblDKGH
            // 
            this.lblDKGH.Location = new System.Drawing.Point(408, 500);
            this.lblDKGH.Name = "lblDKGH";
            this.lblDKGH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDKGH.Size = new System.Drawing.Size(92, 31);
            this.lblDKGH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiaDiemDoHang
            // 
            this.lblDiaDiemDoHang.Location = new System.Drawing.Point(658, 400);
            this.lblDiaDiemDoHang.Multiline = true;
            this.lblDiaDiemDoHang.Name = "lblDiaDiemDoHang";
            this.lblDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemDoHang.Size = new System.Drawing.Size(125, 33);
            this.lblDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiaDiemXepHang
            // 
            this.lblDiaDiemXepHang.Location = new System.Drawing.Point(533, 400);
            this.lblDiaDiemXepHang.Name = "lblDiaDiemXepHang";
            this.lblDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemXepHang.Size = new System.Drawing.Size(125, 33);
            this.lblDiaDiemXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNuoc.Location = new System.Drawing.Point(483, 433);
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.lblMaNuoc.ParentStyleUsing.UseFont = false;
            this.lblMaNuoc.Size = new System.Drawing.Size(50, 23);
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.Location = new System.Drawing.Point(408, 383);
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNuoc.Size = new System.Drawing.Size(117, 41);
            this.lblTenNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.CanGrow = false;
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.Location = new System.Drawing.Point(692, 192);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHopDong.ParentStyleUsing.UseFont = false;
            this.lblSoHopDong.Size = new System.Drawing.Size(100, 33);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.Location = new System.Drawing.Point(700, 250);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHopDong.Size = new System.Drawing.Size(66, 17);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Location = new System.Drawing.Point(700, 225);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(83, 16);
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.Location = new System.Drawing.Point(700, 333);
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayVanTaiDon.Size = new System.Drawing.Size(83, 17);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoGiayPhep.Location = new System.Drawing.Point(567, 192);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblSoGiayPhep.Size = new System.Drawing.Size(91, 33);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(567, 225);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(91, 17);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.Location = new System.Drawing.Point(567, 250);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoVanTaiDon
            // 
            this.lblSoVanTaiDon.CanGrow = false;
            this.lblSoVanTaiDon.CanShrink = true;
            this.lblSoVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoVanTaiDon.Location = new System.Drawing.Point(683, 283);
            this.lblSoVanTaiDon.Name = "lblSoVanTaiDon";
            this.lblSoVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblSoVanTaiDon.ParentStyleUsing.UseFont = false;
            this.lblSoVanTaiDon.Size = new System.Drawing.Size(109, 50);
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(592, 333);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(66, 17);
            // 
            // lblSoPTVT
            // 
            this.lblSoPTVT.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoPTVT.Location = new System.Drawing.Point(533, 300);
            this.lblSoPTVT.Name = "lblSoPTVT";
            this.lblSoPTVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPTVT.ParentStyleUsing.UseFont = false;
            this.lblSoPTVT.Size = new System.Drawing.Size(125, 33);
            this.lblSoPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHoaDon
            // 
            this.lblNgayHoaDon.Location = new System.Drawing.Point(450, 333);
            this.lblNgayHoaDon.Name = "lblNgayHoaDon";
            this.lblNgayHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoaDon.Size = new System.Drawing.Size(75, 17);
            // 
            // lblSoHoaDon
            // 
            this.lblSoHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHoaDon.Location = new System.Drawing.Point(433, 283);
            this.lblSoHoaDon.Multiline = true;
            this.lblSoHoaDon.Name = "lblSoHoaDon";
            this.lblSoHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHoaDon.ParentStyleUsing.UseFont = false;
            this.lblSoHoaDon.Size = new System.Drawing.Size(92, 50);
            this.lblSoHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(33, 492);
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(367, 41);
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(25, 200);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(375, 58);
            this.lblTenDoanhNghiep.Text = "CÔNG TY CỔ PHẦN DỆT MAY 19/3\r\n60 MẸ NHU THANH KHÊ ĐÀ NẴNG\r\nMID CODE: VNMARTEX478D" +
                "AN";
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(33, 392);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblNguoiUyThac.Size = new System.Drawing.Size(367, 58);
            this.lblNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoiTac
            // 
            this.lblTenDoiTac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoiTac.Location = new System.Drawing.Point(25, 292);
            this.lblTenDoiTac.Multiline = true;
            this.lblTenDoiTac.Name = "lblTenDoiTac";
            this.lblTenDoiTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoiTac.ParentStyleUsing.UseFont = false;
            this.lblTenDoiTac.Size = new System.Drawing.Size(375, 66);
            this.lblTenDoiTac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrKD
            // 
            this.xrKD.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrKD.Location = new System.Drawing.Point(412, 198);
            this.xrKD.Name = "xrKD";
            this.xrKD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrKD.ParentStyleUsing.UseFont = false;
            this.xrKD.Size = new System.Drawing.Size(17, 16);
            this.xrKD.Text = "×";
            this.xrKD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrKD.Visible = false;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(142, 175);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(283, 24);
            // 
            // lblSoPLTK
            // 
            this.lblSoPLTK.Location = new System.Drawing.Point(417, 117);
            this.lblSoPLTK.Name = "lblSoPLTK";
            this.lblSoPLTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK.Size = new System.Drawing.Size(33, 16);
            this.lblSoPLTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblChiCucHaiQuan
            // 
            this.lblChiCucHaiQuan.Location = new System.Drawing.Point(133, 108);
            this.lblChiCucHaiQuan.Multiline = true;
            this.lblChiCucHaiQuan.Name = "lblChiCucHaiQuan";
            this.lblChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuan.Size = new System.Drawing.Size(134, 16);
            this.lblChiCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.Location = new System.Drawing.Point(108, 83);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.Size = new System.Drawing.Size(150, 16);
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(367, 92);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(75, 16);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ptbImage
            // 
            this.ptbImage.Image = ((System.Drawing.Image)(resources.GetObject("ptbImage.Image")));
            this.ptbImage.Location = new System.Drawing.Point(0, 3);
            this.ptbImage.Name = "ptbImage";
            this.ptbImage.Size = new System.Drawing.Size(812, 1119);
            this.ptbImage.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // ToKhaiNhap
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(0, 15, 5, 19);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK;
        private DevExpress.XtraReports.UI.XRLabel xrKD;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoiTac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblSoPTVT;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDenPTVT;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblTenNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgoaiTe;
        private DevExpress.XtraReports.UI.XRLabel lblDKGH;
        private DevExpress.XtraReports.UI.XRLabel lblPTTT;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTT;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNK;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNKChu;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNKSo;
        public DevExpress.XtraReports.UI.XRPictureBox ptbImage;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu5;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao4;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueGTGT;
        private DevExpress.XtraReports.UI.XRLabel xrDT;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue2;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue1;
        private DevExpress.XtraReports.UI.XRLabel xrNTX;
    }
}
