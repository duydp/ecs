using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAJP010 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute SBN { get; set; }
public PropertiesAttribute SPC { get; set; }
public PropertiesAttribute ADS { get; set; }
public PropertiesAttribute SCC { get; set; }
public PropertiesAttribute SUN { get; set; }
public PropertiesAttribute SFN { get; set; }
public PropertiesAttribute SBE { get; set; }
public PropertiesAttribute DOS { get; set; }
public PropertiesAttribute ONO { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute PRT { get; set; }
public PropertiesAttribute OAC { get; set; }
public PropertiesAttribute OAN { get; set; }
public PropertiesAttribute CNO { get; set; }
public PropertiesAttribute EXT { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EBN { get; set; }
public PropertiesAttribute ESN { get; set; }
public PropertiesAttribute EDN { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute ERC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EFX { get; set; }
public PropertiesAttribute EEM { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute BRN { get; set; }
public PropertiesAttribute BPC { get; set; }
public PropertiesAttribute AOI { get; set; }
public PropertiesAttribute BCC { get; set; }
public PropertiesAttribute BPN { get; set; }
public PropertiesAttribute BFX { get; set; }
public PropertiesAttribute BEM { get; set; }
public PropertiesAttribute TCC { get; set; }
public PropertiesAttribute MOT { get; set; }
public PropertiesAttribute MTN { get; set; }
public PropertiesAttribute IBC { get; set; }
public PropertiesAttribute IBG { get; set; }
public PropertiesAttribute PRN { get; set; }
public PropertiesAttribute QUL { get; set; }
public PropertiesAttribute VAN { get; set; }
public PropertiesAttribute DOV { get; set; }
public PropertiesAttribute ANT { get; set; }
public PropertiesAttribute CON { get; set; }
public PropertiesAttribute PRC { get; set; }
public PropertiesAttribute QUT { get; set; }
public PropertiesAttribute QUH { get; set; }
public PropertiesAttribute CTL { get; set; }
public PropertiesAttribute CTT { get; set; }
public PropertiesAttribute CTH { get; set; }
public PropertiesAttribute NOC { get; set; }
public PropertiesAttribute ORE { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute AVL { get; set; }
public PropertiesAttribute TET { get; set; }
public PropertiesAttribute TRS { get; set; }
public PropertiesAttribute SPN { get; set; }
public PropertiesAttribute NTN { get; set; }
public PropertiesAttribute CAN { get; set; }
public PropertiesAttribute DON { get; set; }
public PropertiesAttribute CRN { get; set; }
public PropertiesAttribute PAN { get; set; }
public PropertiesAttribute LDP { get; set; }
public PropertiesAttribute NAP { get; set; }
public PropertiesAttribute DCP { get; set; }
public PropertiesAttribute DOD { get; set; }
public PropertiesAttribute GFP { get; set; }
public PropertiesAttribute QFP { get; set; }
public PropertiesAttribute QFU { get; set; }
public PropertiesAttribute WFP { get; set; }
public PropertiesAttribute WFU { get; set; }
public GroupAttribute G01 { get; set; }
public PropertiesAttribute DGD { get; set; }
public PropertiesAttribute DGQ { get; set; }
public PropertiesAttribute DQU { get; set; }
public PropertiesAttribute DGW { get; set; }
public PropertiesAttribute DWU { get; set; }

public VAJP010()
        {
SBC = new PropertiesAttribute(13, typeof(string));
SBN = new PropertiesAttribute(300, typeof(string));
SPC = new PropertiesAttribute(7, typeof(string));
ADS = new PropertiesAttribute(300, typeof(string));
SCC = new PropertiesAttribute(2, typeof(string));
SUN = new PropertiesAttribute(20, typeof(string));
SFN = new PropertiesAttribute(20, typeof(string));
SBE = new PropertiesAttribute(210, typeof(string));
DOS = new PropertiesAttribute(8, typeof(DateTime));
ONO = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
PRT = new PropertiesAttribute(4, typeof(string));
OAC = new PropertiesAttribute(6, typeof(string));
OAN = new PropertiesAttribute(300, typeof(string));
CNO = new PropertiesAttribute(35, typeof(string));
EXT = new PropertiesAttribute(70, typeof(string));
EPC = new PropertiesAttribute(9, typeof(string));
EBN = new PropertiesAttribute(35, typeof(string));
ESN = new PropertiesAttribute(35, typeof(string));
EDN = new PropertiesAttribute(35, typeof(string));
ECN = new PropertiesAttribute(35, typeof(string));
ERC = new PropertiesAttribute(2, typeof(string));
EPN = new PropertiesAttribute(20, typeof(string));
EFX = new PropertiesAttribute(20, typeof(string));
EEM = new PropertiesAttribute(210, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
BRN = new PropertiesAttribute(35, typeof(string));
BPC = new PropertiesAttribute(7, typeof(string));
AOI = new PropertiesAttribute(300, typeof(string));
BCC = new PropertiesAttribute(2, typeof(string));
BPN = new PropertiesAttribute(20, typeof(string));
BFX = new PropertiesAttribute(20, typeof(string));
BEM = new PropertiesAttribute(210, typeof(string));
TCC = new PropertiesAttribute(2, typeof(string));
MOT = new PropertiesAttribute(2, typeof(string));
MTN = new PropertiesAttribute(75, typeof(string));
IBC = new PropertiesAttribute(6, typeof(string));
IBG = new PropertiesAttribute(35, typeof(string));
PRN = new PropertiesAttribute(35, typeof(string));
QUL = new PropertiesAttribute(150, typeof(string));
VAN = new PropertiesAttribute(100, typeof(string));
DOV = new PropertiesAttribute(8, typeof(DateTime));
ANT = new PropertiesAttribute(100, typeof(string));
CON = new PropertiesAttribute(100, typeof(string));
PRC = new PropertiesAttribute(70, typeof(string));
QUT = new PropertiesAttribute(8, typeof(DateTime));
QUH = new PropertiesAttribute(4, typeof(string));
CTL = new PropertiesAttribute(150, typeof(string));
CTT = new PropertiesAttribute(8, typeof(DateTime));
CTH = new PropertiesAttribute(4, typeof(string));
NOC = new PropertiesAttribute(2, typeof(int));
ORE = new PropertiesAttribute(150, typeof(string));
AD = new PropertiesAttribute(750, typeof(string));
AVL = new PropertiesAttribute(250, typeof(string));
TET = new PropertiesAttribute(50, typeof(string));
TRS = new PropertiesAttribute(996, typeof(string));
SPN = new PropertiesAttribute(35, typeof(string));
NTN = new PropertiesAttribute(2, typeof(string));
CAN = new PropertiesAttribute(50, typeof(string));
DON = new PropertiesAttribute(50, typeof(string));
CRN = new PropertiesAttribute(4, typeof(int));
PAN = new PropertiesAttribute(4, typeof(int));
LDP = new PropertiesAttribute(100, typeof(string));
NAP = new PropertiesAttribute(100, typeof(string));
DCP = new PropertiesAttribute(100, typeof(string));
DOD = new PropertiesAttribute(8, typeof(DateTime));
GFP = new PropertiesAttribute(100, typeof(string));
QFP = new PropertiesAttribute(8, typeof(int));
QFU = new PropertiesAttribute(3, typeof(string));
WFP = new PropertiesAttribute(10, typeof(int));
WFU = new PropertiesAttribute(3, typeof(string));
DGD = new PropertiesAttribute(100, typeof(string));
DGQ = new PropertiesAttribute(8, typeof(int));
DQU = new PropertiesAttribute(3, typeof(string));
DGW = new PropertiesAttribute(10, typeof(int));
DWU = new PropertiesAttribute(3, typeof(string));
#region G01
List<PropertiesAttribute> listG01 = new List<PropertiesAttribute>();
listG01.Add(new PropertiesAttribute(100, 10, EnumGroupID.VAJP010_G01, typeof(string)));
listG01.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAJP010_Q01, typeof(int)));
listG01.Add(new PropertiesAttribute(3, 10, EnumGroupID.VAJP010_QU01, typeof(string)));
listG01.Add(new PropertiesAttribute(10, 10, EnumGroupID.VAJP010_W01, typeof(int)));
listG01.Add(new PropertiesAttribute(3, 10, EnumGroupID.VAJP010_WU01, typeof(string)));
listG01.Add(new PropertiesAttribute(100, 10, EnumGroupID.VAJP010_P01, typeof(string)));
G01 = new GroupAttribute("G01", 10, listG01);
#endregion G01
TongSoByte = 8890;
}

}public partial class EnumGroupID
    {
public static readonly string VAJP010_G01 = "VAJP010_G01";
public static readonly string VAJP010_Q01 = "VAJP010_Q01";
public static readonly string VAJP010_QU01 = "VAJP010_QU01";
public static readonly string VAJP010_W01 = "VAJP010_W01";
public static readonly string VAJP010_WU01 = "VAJP010_WU01";
public static readonly string VAJP010_P01 = "VAJP010_P01";
}

}
