﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace DongBoDuLieu
{
    public class FormType
    {
        public static string CLIENT = "CLIENT";
        public static string SERVER = "SERVER";
    }

    public class Helps
    {
        private static string passEncryptToString = @"Softech.ECS";

        #region Encrypt/Decrypt String

        public static string EncryptString(string plainText)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(passEncryptToString, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }

        public static string DecryptString(string plainText)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(passEncryptToString, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }

        #endregion

    }
}
