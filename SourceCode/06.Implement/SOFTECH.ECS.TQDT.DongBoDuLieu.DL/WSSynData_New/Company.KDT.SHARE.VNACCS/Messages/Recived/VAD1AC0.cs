﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD1AC0 : BasicVNACC
    {
        public List<VAD1AC0_HANG> HangMD {get;set;}
        public void LoadVAD1AC0(string strResult)
        {
            try
            {

                this.GetObject<VAD1AC0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD1AC0, false, VAD1AC0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD1AC0.TongSoByte);
                while (true)
                {
                    VAD1AC0_HANG vad1agHang = new VAD1AC0_HANG();
                    vad1agHang.GetObject<VAD1AC0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD1AC0_HANG", false, VAD1AC0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD1AC0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD1AC0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD1AC0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD1AC0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }
}
