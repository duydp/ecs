﻿namespace Company.Interface
{
    partial class FrmCauHinhChuKySo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCauHinhChuKySo));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportInfo = new Janus.Windows.EditControls.UIButton();
            this.btnRefersh = new Janus.Windows.EditControls.UIButton();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btnCheckHSD_CKS = new Janus.Windows.EditControls.UIButton();
            this.btnCheckConnect = new Janus.Windows.EditControls.UIButton();
            this.txtPassword = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chIsRemember = new Janus.Windows.EditControls.UICheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkIsUseSign = new Janus.Windows.EditControls.UICheckBox();
            this.lblNhaCungCap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSigns = new Janus.Windows.EditControls.UIComboBox();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chIsSignOnLan = new Janus.Windows.EditControls.UICheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDataSignName = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtUserNameSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSignRemote = new Janus.Windows.EditControls.UIButton();
            this.chIsUseSignRemote = new Janus.Windows.EditControls.UICheckBox();
            this.txtPasswordSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtUserNameSmartCA = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCheckSmartCA = new Janus.Windows.EditControls.UIButton();
            this.chkSmartCA = new Janus.Windows.EditControls.UICheckBox();
            this.txtPasswordSmartCA = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(549, 366);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "connect.png");
            this.imageList1.Images.SetKeyName(1, "disconnect.png");
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(176, 16);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(257, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnSave);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 318);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(549, 48);
            this.uiGroupBox3.TabIndex = 3;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(549, 318);
            this.uiTab1.TabIndex = 4;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage3,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox6);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(547, 296);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Ký trực tiếp";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnExportInfo);
            this.uiGroupBox6.Controls.Add(this.btnRefersh);
            this.uiGroupBox6.Controls.Add(this.linkLabel1);
            this.uiGroupBox6.Controls.Add(this.btnCheckHSD_CKS);
            this.uiGroupBox6.Controls.Add(this.btnCheckConnect);
            this.uiGroupBox6.Controls.Add(this.txtPassword);
            this.uiGroupBox6.Controls.Add(this.chIsRemember);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.chkIsUseSign);
            this.uiGroupBox6.Controls.Add(this.lblNhaCungCap);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSigns);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(547, 296);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Thông tin chữ ký số";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnExportInfo
            // 
            this.btnExportInfo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExportInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnExportInfo.Image")));
            this.btnExportInfo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportInfo.Location = new System.Drawing.Point(280, 196);
            this.btnExportInfo.Name = "btnExportInfo";
            this.btnExportInfo.Size = new System.Drawing.Size(159, 23);
            this.btnExportInfo.TabIndex = 2;
            this.btnExportInfo.Text = "Thông tin về CKS";
            this.btnExportInfo.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Far;
            this.btnExportInfo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportInfo.Click += new System.EventHandler(this.btnExportInfo_Click);
            // 
            // btnRefersh
            // 
            this.btnRefersh.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnRefersh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefersh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefersh.Image")));
            this.btnRefersh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnRefersh.Location = new System.Drawing.Point(445, 61);
            this.btnRefersh.Name = "btnRefersh";
            this.btnRefersh.Size = new System.Drawing.Size(87, 23);
            this.btnRefersh.TabIndex = 2;
            this.btnRefersh.Text = "Làm mới";
            this.btnRefersh.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Far;
            this.btnRefersh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnRefersh.Click += new System.EventHandler(this.btnRefersh_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(99, 233);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(175, 13);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Hướng dẫn cấu hình chữ ký số";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // btnCheckHSD_CKS
            // 
            this.btnCheckHSD_CKS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckHSD_CKS.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckHSD_CKS.Image")));
            this.btnCheckHSD_CKS.Location = new System.Drawing.Point(99, 196);
            this.btnCheckHSD_CKS.Name = "btnCheckHSD_CKS";
            this.btnCheckHSD_CKS.Size = new System.Drawing.Size(175, 23);
            this.btnCheckHSD_CKS.TabIndex = 6;
            this.btnCheckHSD_CKS.Text = "Kiểm tra hạn dùng CKS";
            this.btnCheckHSD_CKS.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCheckHSD_CKS.Click += new System.EventHandler(this.btnCheckHSD_CKS_Click);
            // 
            // btnCheckConnect
            // 
            this.btnCheckConnect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckConnect.Image")));
            this.btnCheckConnect.ImageList = this.imageList1;
            this.btnCheckConnect.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCheckConnect.Location = new System.Drawing.Point(445, 127);
            this.btnCheckConnect.Name = "btnCheckConnect";
            this.btnCheckConnect.Size = new System.Drawing.Size(87, 22);
            this.btnCheckConnect.TabIndex = 6;
            this.btnCheckConnect.Text = "Kiểm tra";
            this.btnCheckConnect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCheckConnect.Click += new System.EventHandler(this.btnCheckConnect_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(99, 128);
            this.txtPassword.MaxLength = 100;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(340, 21);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPassword.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chIsRemember
            // 
            this.chIsRemember.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsRemember.Location = new System.Drawing.Point(99, 163);
            this.chIsRemember.Name = "chIsRemember";
            this.chIsRemember.Size = new System.Drawing.Size(116, 18);
            this.chIsRemember.TabIndex = 7;
            this.chIsRemember.Text = "Ghi nhớ mật khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã PIN";
            // 
            // chkIsUseSign
            // 
            this.chkIsUseSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsUseSign.Location = new System.Drawing.Point(99, 29);
            this.chkIsUseSign.Name = "chkIsUseSign";
            this.chkIsUseSign.Size = new System.Drawing.Size(128, 18);
            this.chkIsUseSign.TabIndex = 1;
            this.chkIsUseSign.Text = "Sử dụng chữ ký số";
            this.chkIsUseSign.CheckedChanged += new System.EventHandler(this.chkIsUseSign_CheckedChanged);
            // 
            // lblNhaCungCap
            // 
            this.lblNhaCungCap.AutoSize = true;
            this.lblNhaCungCap.BackColor = System.Drawing.Color.Transparent;
            this.lblNhaCungCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhaCungCap.ForeColor = System.Drawing.Color.Red;
            this.lblNhaCungCap.Location = new System.Drawing.Point(99, 99);
            this.lblNhaCungCap.Name = "lblNhaCungCap";
            this.lblNhaCungCap.Size = new System.Drawing.Size(86, 13);
            this.lblNhaCungCap.TabIndex = 3;
            this.lblNhaCungCap.Text = "Chưa xác định";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nhà cung cấp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cho phép";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Chữ ký số";
            // 
            // cbSigns
            // 
            this.cbSigns.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbSigns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSigns.Location = new System.Drawing.Point(99, 61);
            this.cbSigns.Name = "cbSigns";
            this.cbSigns.Size = new System.Drawing.Size(340, 21);
            this.cbSigns.TabIndex = 2;
            this.cbSigns.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbSigns.SelectedValueChanged += new System.EventHandler(this.cbChuKySo_SelectedValueChanged);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox1);
            this.uiTabPage3.Controls.Add(this.uiGroupBox2);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(547, 296);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Ký Online / Nội bộ";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chIsSignOnLan);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.cbDataSignName);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 129);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(547, 167);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Ký thông tin trong nội bộ";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chIsSignOnLan
            // 
            this.chIsSignOnLan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsSignOnLan.Location = new System.Drawing.Point(99, 57);
            this.chIsSignOnLan.Name = "chIsSignOnLan";
            this.chIsSignOnLan.Size = new System.Drawing.Size(179, 18);
            this.chIsSignOnLan.TabIndex = 1;
            this.chIsSignOnLan.Text = "Sử dụng trong mạng lan";
            this.chIsSignOnLan.CheckedChanged += new System.EventHandler(this.chIsSignOnLan_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Cơ sỡ dữ liệu ký";
            // 
            // cbDataSignName
            // 
            this.cbDataSignName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDataSignName.Location = new System.Drawing.Point(99, 25);
            this.cbDataSignName.Name = "cbDataSignName";
            this.cbDataSignName.Size = new System.Drawing.Size(293, 21);
            this.cbDataSignName.TabIndex = 0;
            this.cbDataSignName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtUserNameSign);
            this.uiGroupBox2.Controls.Add(this.btnSignRemote);
            this.uiGroupBox2.Controls.Add(this.chIsUseSignRemote);
            this.uiGroupBox2.Controls.Add(this.txtPasswordSign);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(547, 129);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Sử dụng hệ thống ký số từ xa (Online)";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtUserNameSign
            // 
            this.txtUserNameSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNameSign.Location = new System.Drawing.Point(99, 24);
            this.txtUserNameSign.MaxLength = 100;
            this.txtUserNameSign.Name = "txtUserNameSign";
            this.txtUserNameSign.Size = new System.Drawing.Size(293, 21);
            this.txtUserNameSign.TabIndex = 0;
            this.txtUserNameSign.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtUserNameSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnSignRemote
            // 
            this.btnSignRemote.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignRemote.Image = ((System.Drawing.Image)(resources.GetObject("btnSignRemote.Image")));
            this.btnSignRemote.ImageList = this.imageList1;
            this.btnSignRemote.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSignRemote.Location = new System.Drawing.Point(398, 62);
            this.btnSignRemote.Name = "btnSignRemote";
            this.btnSignRemote.Size = new System.Drawing.Size(101, 22);
            this.btnSignRemote.TabIndex = 2;
            this.btnSignRemote.Text = "Kiểm tra";
            this.btnSignRemote.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSignRemote.Click += new System.EventHandler(this.btnSignRemote_Click);
            // 
            // chIsUseSignRemote
            // 
            this.chIsUseSignRemote.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsUseSignRemote.Location = new System.Drawing.Point(99, 96);
            this.chIsUseSignRemote.Name = "chIsUseSignRemote";
            this.chIsUseSignRemote.Size = new System.Drawing.Size(183, 18);
            this.chIsUseSignRemote.TabIndex = 3;
            this.chIsUseSignRemote.Text = "Sử dụng ký số từ xa";
            this.chIsUseSignRemote.CheckedChanged += new System.EventHandler(this.chIsUseSignRemote_CheckedChanged);
            // 
            // txtPasswordSign
            // 
            this.txtPasswordSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordSign.Location = new System.Drawing.Point(99, 63);
            this.txtPasswordSign.MaxLength = 100;
            this.txtPasswordSign.Name = "txtPasswordSign";
            this.txtPasswordSign.PasswordChar = '*';
            this.txtPasswordSign.Size = new System.Drawing.Size(293, 21);
            this.txtPasswordSign.TabIndex = 1;
            this.txtPasswordSign.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPasswordSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Tên đăng nhập";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Mật khẩu";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.txtUserNameSmartCA);
            this.uiTabPage2.Controls.Add(this.txtCheckSmartCA);
            this.uiTabPage2.Controls.Add(this.chkSmartCA);
            this.uiTabPage2.Controls.Add(this.txtPasswordSmartCA);
            this.uiTabPage2.Controls.Add(this.label8);
            this.uiTabPage2.Controls.Add(this.label9);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(547, 296);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Ký Smart CA";
            // 
            // txtUserNameSmartCA
            // 
            this.txtUserNameSmartCA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNameSmartCA.Location = new System.Drawing.Point(103, 30);
            this.txtUserNameSmartCA.MaxLength = 100;
            this.txtUserNameSmartCA.Name = "txtUserNameSmartCA";
            this.txtUserNameSmartCA.Size = new System.Drawing.Size(293, 21);
            this.txtUserNameSmartCA.TabIndex = 4;
            this.txtUserNameSmartCA.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtUserNameSmartCA.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtCheckSmartCA
            // 
            this.txtCheckSmartCA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckSmartCA.Image = ((System.Drawing.Image)(resources.GetObject("txtCheckSmartCA.Image")));
            this.txtCheckSmartCA.ImageList = this.imageList1;
            this.txtCheckSmartCA.ImageSize = new System.Drawing.Size(20, 20);
            this.txtCheckSmartCA.Location = new System.Drawing.Point(103, 103);
            this.txtCheckSmartCA.Name = "txtCheckSmartCA";
            this.txtCheckSmartCA.Size = new System.Drawing.Size(101, 22);
            this.txtCheckSmartCA.TabIndex = 6;
            this.txtCheckSmartCA.Text = "Kiểm tra";
            this.txtCheckSmartCA.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.txtCheckSmartCA.Click += new System.EventHandler(this.txtCheckSmartCA_Click);
            // 
            // chkSmartCA
            // 
            this.chkSmartCA.BackColor = System.Drawing.Color.Transparent;
            this.chkSmartCA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSmartCA.Location = new System.Drawing.Point(213, 105);
            this.chkSmartCA.Name = "chkSmartCA";
            this.chkSmartCA.Size = new System.Drawing.Size(183, 18);
            this.chkSmartCA.TabIndex = 9;
            this.chkSmartCA.Text = "Sử dụng ký số SmartCA";
            this.chkSmartCA.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkSmartCA.CheckedChanged += new System.EventHandler(this.chkSmartCA_CheckedChanged);
            // 
            // txtPasswordSmartCA
            // 
            this.txtPasswordSmartCA.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordSmartCA.Location = new System.Drawing.Point(103, 69);
            this.txtPasswordSmartCA.MaxLength = 100;
            this.txtPasswordSmartCA.Name = "txtPasswordSmartCA";
            this.txtPasswordSmartCA.PasswordChar = '*';
            this.txtPasswordSmartCA.Size = new System.Drawing.Size(293, 21);
            this.txtPasswordSmartCA.TabIndex = 5;
            this.txtPasswordSmartCA.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPasswordSmartCA.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Tên đăng nhập";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Mật khẩu";
            // 
            // FrmCauHinhChuKySo
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 366);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "FrmCauHinhChuKySo";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thiết lập thông tin chữ ký";
            this.Load += new System.EventHandler(this.FrmCauHinhChuKySo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnExportInfo;
        private Janus.Windows.EditControls.UIButton btnRefersh;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Janus.Windows.EditControls.UIButton btnCheckHSD_CKS;
        private Janus.Windows.EditControls.UIButton btnCheckConnect;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassword;
        private Janus.Windows.EditControls.UICheckBox chIsRemember;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UICheckBox chkIsUseSign;
        private System.Windows.Forms.Label lblNhaCungCap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbSigns;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UICheckBox chIsSignOnLan;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIComboBox cbDataSignName;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserNameSign;
        private Janus.Windows.EditControls.UIButton btnSignRemote;
        private Janus.Windows.EditControls.UICheckBox chIsUseSignRemote;
        private Janus.Windows.GridEX.EditControls.EditBox txtPasswordSign;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserNameSmartCA;
        private Janus.Windows.EditControls.UIButton txtCheckSmartCA;
        private Janus.Windows.EditControls.UICheckBox chkSmartCA;
        private Janus.Windows.GridEX.EditControls.EditBox txtPasswordSmartCA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}