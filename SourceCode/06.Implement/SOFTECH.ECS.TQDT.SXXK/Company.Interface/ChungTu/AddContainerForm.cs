﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.BLL.DuLieuChuan;
using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;
using Company.BLL.KDT;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Windows.Forms;

namespace Company.Interface
{
    public partial class AddContainerForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public Container container;
        public AddContainerForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (save())
            {
                txtSealNo.Clear();
                txtSoHieuContainer.Clear();
                container = null;
            }
        }

        private void AddContainerForm_Load(object sender, EventArgs e)
        {
            cbTrangThai.SelectedIndex = 0;
            cbLoaiContainer.SelectedIndex = 0;
            if (container != null)
            {
                txtSealNo.Text = container.Seal_No;
                txtSoHieuContainer.Text = container.SoHieu;
                cbLoaiContainer.SelectedValue = container.LoaiContainer;
                cbTrangThai.SelectedValue = container.Trang_thai;
            }

            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //btnSave.Enabled = false;
            //btnSaveNew.Enabled = false;
            //}

            btnSave.Enabled = btnSaveNew.Enabled = true;
            //}
        }
        private bool checkSoHieu(string soHieu)
        {
            foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                if (c.SoHieu.Trim().ToUpper() == soHieu.ToUpper().Trim())
                {
                    return true;
                }
            return false;
        }
        private bool save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return false;
            }

            if (!ValidateVanDon())
                return false;

            if (TKMD.VanTaiDon == null)
            {
                TKMD.VanTaiDon = new VanDon();
            }

            TKMD.VanTaiDon.ContainerCollection.Remove(container);

            if (checkSoHieu(txtSoHieuContainer.Text))
            {
                if (container != null)
                    TKMD.VanTaiDon.ContainerCollection.Add(container);
                TKMD.VanTaiDon.ContainerCollection.Remove(container);
                ShowMessage("Đã có số hiệu Container này.", false);
                return false;
            }
            if (container == null)
                container = new Container();
            container.SoHieu = txtSoHieuContainer.Text.Trim();
            container.LoaiContainer = cbLoaiContainer.SelectedValue.ToString();
            container.Seal_No = txtSealNo.Text.Trim();
            container.Trang_thai = Convert.ToInt32(cbTrangThai.SelectedValue.ToString());
            TKMD.VanTaiDon.ContainerCollection.Add(container);
            if (cbLoaiContainer.SelectedValue.ToString() == "2")
                TKMD.SoContainer20++;
            else
                TKMD.SoContainer40++;
            return true;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save())
                this.Close();
        }
        
        #region Begin VALIDATE VAN DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateVanDon()
        {
            bool isValid = true;

            //Container_No	varchar(17)
            isValid = Globals.ValidateLength(txtSoHieuContainer, 17, err, "Số hiệu Container");

            //Seal_No	varchar(35)
            isValid &= Globals.ValidateLength(txtSealNo, 35, err, "Số Serial Container");

            return isValid;
        }

        #endregion End VALIDATE VAN DON


    }
}