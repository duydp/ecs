﻿namespace Company.Interface.SXXK
{
    partial class ChungTuThanhToanForm_Modified
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuThanhToanForm_Modified));
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SoNgayChungTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HinhThuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TriGiaChungTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.CanhBaoChungTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.ToKhaiXuat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayDangKyXuat2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.SoToKhai = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.NgayDangKyXuat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.SoHopDong = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.MaHangXuat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.TriGiaHopDong = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TriGiaHangThucXuat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CanhBao = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiCommandManager = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCmdMain = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdQuanLyHopDongXuatKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyHopDongXuatKhau");
            this.cmdQuanLyChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyChungTu");
            this.cmdTaoCTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTaoCTTT");
            this.cmdXoaChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXoaChungTu");
            this.cmdQuanLyHopDongXuatKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyHopDongXuatKhau");
            this.cmdQuanLyChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyChungTu");
            this.cmdTaoCTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdTaoCTTT");
            this.cmdXoaChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdXoaChungTu");
            this.imgSmall = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.panel3);
            this.grbMain.Controls.Add(this.panel2);
            this.grbMain.Controls.Add(this.panel1);
            this.grbMain.Controls.Add(this.gridControl1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1095, 402);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SoNgayChungTu,
            this.HinhThuc,
            this.TriGiaChungTu,
            this.GhiChu,
            this.CanhBaoChungTu,
            this.ToKhaiXuat,
            this.NgayDangKyXuat2});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // SoNgayChungTu
            // 
            this.SoNgayChungTu.AppearanceCell.Options.UseTextOptions = true;
            this.SoNgayChungTu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoNgayChungTu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoNgayChungTu.AppearanceHeader.Options.UseTextOptions = true;
            this.SoNgayChungTu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoNgayChungTu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoNgayChungTu.Caption = "Số, ngày chứng từ";
            this.SoNgayChungTu.FieldName = "SoNgayChungTu";
            this.SoNgayChungTu.Name = "SoNgayChungTu";
            this.SoNgayChungTu.OptionsColumn.AllowEdit = false;
            this.SoNgayChungTu.Visible = true;
            this.SoNgayChungTu.VisibleIndex = 0;
            // 
            // HinhThuc
            // 
            this.HinhThuc.AppearanceCell.Options.UseTextOptions = true;
            this.HinhThuc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HinhThuc.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HinhThuc.AppearanceHeader.Options.UseTextOptions = true;
            this.HinhThuc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HinhThuc.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.HinhThuc.Caption = "Hình thức";
            this.HinhThuc.FieldName = "HinhThucThanhToan";
            this.HinhThuc.Name = "HinhThuc";
            this.HinhThuc.OptionsColumn.AllowEdit = false;
            this.HinhThuc.Visible = true;
            this.HinhThuc.VisibleIndex = 1;
            // 
            // TriGiaChungTu
            // 
            this.TriGiaChungTu.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaChungTu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaChungTu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaChungTu.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaChungTu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaChungTu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaChungTu.Caption = "Trị giá chứng từ";
            this.TriGiaChungTu.DisplayFormat.FormatString = "###.###.###.##0,00##";
            this.TriGiaChungTu.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TriGiaChungTu.FieldName = "TriGiaCT";
            this.TriGiaChungTu.Name = "TriGiaChungTu";
            this.TriGiaChungTu.OptionsColumn.AllowEdit = false;
            this.TriGiaChungTu.Visible = true;
            this.TriGiaChungTu.VisibleIndex = 2;
            // 
            // GhiChu
            // 
            this.GhiChu.AppearanceCell.Options.UseTextOptions = true;
            this.GhiChu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GhiChu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GhiChu.AppearanceHeader.Options.UseTextOptions = true;
            this.GhiChu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GhiChu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GhiChu.Caption = "Ghi chú";
            this.GhiChu.ColumnEdit = this.repositoryItemMemoEdit3;
            this.GhiChu.FieldName = "GhiChuCT";
            this.GhiChu.Name = "GhiChu";
            this.GhiChu.OptionsColumn.AllowEdit = false;
            this.GhiChu.OptionsColumn.ReadOnly = true;
            this.GhiChu.Visible = true;
            this.GhiChu.VisibleIndex = 3;
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // CanhBaoChungTu
            // 
            this.CanhBaoChungTu.AppearanceCell.Options.UseTextOptions = true;
            this.CanhBaoChungTu.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CanhBaoChungTu.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CanhBaoChungTu.AppearanceHeader.Options.UseTextOptions = true;
            this.CanhBaoChungTu.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CanhBaoChungTu.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CanhBaoChungTu.Caption = "Cảnh báo";
            this.CanhBaoChungTu.ColumnEdit = this.repositoryItemMemoEdit4;
            this.CanhBaoChungTu.FieldName = "CanhBao";
            this.CanhBaoChungTu.Name = "CanhBaoChungTu";
            this.CanhBaoChungTu.OptionsColumn.AllowEdit = false;
            this.CanhBaoChungTu.OptionsColumn.ReadOnly = true;
            this.CanhBaoChungTu.Visible = true;
            this.CanhBaoChungTu.VisibleIndex = 4;
            // 
            // repositoryItemMemoEdit4
            // 
            this.repositoryItemMemoEdit4.Name = "repositoryItemMemoEdit4";
            // 
            // ToKhaiXuat
            // 
            this.ToKhaiXuat.FieldName = "ToKhaiXuat";
            this.ToKhaiXuat.Name = "ToKhaiXuat";
            this.ToKhaiXuat.OptionsColumn.AllowEdit = false;
            // 
            // NgayDangKyXuat2
            // 
            this.NgayDangKyXuat2.FieldName = "NgayDangKyXuat";
            this.NgayDangKyXuat2.Name = "NgayDangKyXuat2";
            this.NgayDangKyXuat2.OptionsColumn.AllowEdit = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.LevelTemplate = this.gridView2;
            gridLevelNode1.RelationName = "ChungTuThanhToan";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.LookAndFeel.SkinName = "Office 2010 Blue";
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemMemoEdit2,
            this.repositoryItemMemoEdit3,
            this.repositoryItemMemoEdit4});
            this.gridControl1.Size = new System.Drawing.Size(1089, 367);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1,
            this.gridView2});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.SoToKhai,
            this.NgayDangKyXuat,
            this.SoHopDong,
            this.MaHangXuat,
            this.TriGiaHopDong,
            this.TriGiaHangThucXuat,
            this.CanhBao});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand1.Caption = "Số tờ khai";
            this.gridBand1.Columns.Add(this.SoToKhai);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.RowCount = 2;
            this.gridBand1.Width = 84;
            // 
            // SoToKhai
            // 
            this.SoToKhai.AppearanceCell.Options.UseTextOptions = true;
            this.SoToKhai.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoToKhai.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoToKhai.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SoToKhai.AppearanceHeader.Options.UseTextOptions = true;
            this.SoToKhai.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoToKhai.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoToKhai.FieldName = "ToKhaiXuat";
            this.SoToKhai.Name = "SoToKhai";
            this.SoToKhai.OptionsColumn.AllowEdit = false;
            this.SoToKhai.OptionsColumn.ShowCaption = false;
            this.SoToKhai.Visible = true;
            this.SoToKhai.Width = 84;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand2.Caption = "Ngày đăng ký TK";
            this.gridBand2.Columns.Add(this.NgayDangKyXuat);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 107;
            // 
            // NgayDangKyXuat
            // 
            this.NgayDangKyXuat.AppearanceCell.Options.UseTextOptions = true;
            this.NgayDangKyXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayDangKyXuat.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayDangKyXuat.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NgayDangKyXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.NgayDangKyXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NgayDangKyXuat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NgayDangKyXuat.FieldName = "NgayDangKyXuat";
            this.NgayDangKyXuat.Name = "NgayDangKyXuat";
            this.NgayDangKyXuat.OptionsColumn.AllowEdit = false;
            this.NgayDangKyXuat.OptionsColumn.ShowCaption = false;
            this.NgayDangKyXuat.Visible = true;
            this.NgayDangKyXuat.Width = 107;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand3.Caption = "Hợp đồng xuất khẩu";
            this.gridBand3.Columns.Add(this.SoHopDong);
            this.gridBand3.Columns.Add(this.MaHangXuat);
            this.gridBand3.Columns.Add(this.TriGiaHopDong);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 337;
            // 
            // SoHopDong
            // 
            this.SoHopDong.AppearanceCell.Options.UseTextOptions = true;
            this.SoHopDong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoHopDong.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoHopDong.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SoHopDong.AppearanceHeader.Options.UseTextOptions = true;
            this.SoHopDong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SoHopDong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SoHopDong.Caption = "Số, ngày";
            this.SoHopDong.FieldName = "SoNgayHopDong";
            this.SoHopDong.Name = "SoHopDong";
            this.SoHopDong.OptionsColumn.AllowEdit = false;
            this.SoHopDong.Visible = true;
            this.SoHopDong.Width = 112;
            // 
            // MaHangXuat
            // 
            this.MaHangXuat.AppearanceCell.Options.UseTextOptions = true;
            this.MaHangXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaHangXuat.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHangXuat.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MaHangXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.MaHangXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MaHangXuat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.MaHangXuat.Caption = "Mã hàng xuất";
            this.MaHangXuat.ColumnEdit = this.repositoryItemMemoEdit1;
            this.MaHangXuat.FieldName = "MaHangXuat";
            this.MaHangXuat.Name = "MaHangXuat";
            this.MaHangXuat.OptionsColumn.AllowEdit = false;
            this.MaHangXuat.OptionsColumn.ReadOnly = true;
            this.MaHangXuat.Visible = true;
            this.MaHangXuat.Width = 103;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // TriGiaHopDong
            // 
            this.TriGiaHopDong.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaHopDong.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHopDong.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHopDong.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TriGiaHopDong.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaHopDong.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHopDong.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHopDong.Caption = "Trị giá";
            this.TriGiaHopDong.DisplayFormat.FormatString = "###.###.###.##0,00##";
            this.TriGiaHopDong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TriGiaHopDong.FieldName = "TriGiaHopDong";
            this.TriGiaHopDong.Name = "TriGiaHopDong";
            this.TriGiaHopDong.OptionsColumn.AllowEdit = false;
            this.TriGiaHopDong.Visible = true;
            this.TriGiaHopDong.Width = 122;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand4.Caption = "Trị giá hàng thực Nhập/Xuất";
            this.gridBand4.Columns.Add(this.TriGiaHangThucXuat);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 153;
            // 
            // TriGiaHangThucXuat
            // 
            this.TriGiaHangThucXuat.AppearanceCell.Options.UseTextOptions = true;
            this.TriGiaHangThucXuat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHangThucXuat.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHangThucXuat.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TriGiaHangThucXuat.AppearanceHeader.Options.UseTextOptions = true;
            this.TriGiaHangThucXuat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TriGiaHangThucXuat.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TriGiaHangThucXuat.DisplayFormat.FormatString = "###.###.###.##0,00##";
            this.TriGiaHangThucXuat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TriGiaHangThucXuat.FieldName = "TriGiaHangThucXuat";
            this.TriGiaHangThucXuat.Name = "TriGiaHangThucXuat";
            this.TriGiaHangThucXuat.OptionsColumn.AllowEdit = false;
            this.TriGiaHangThucXuat.OptionsColumn.ReadOnly = true;
            this.TriGiaHangThucXuat.OptionsColumn.ShowCaption = false;
            this.TriGiaHangThucXuat.Visible = true;
            this.TriGiaHangThucXuat.Width = 153;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand5.Caption = "Cảnh Báo";
            this.gridBand5.Columns.Add(this.CanhBao);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 278;
            // 
            // CanhBao
            // 
            this.CanhBao.AppearanceCell.Options.UseTextOptions = true;
            this.CanhBao.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CanhBao.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.CanhBao.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CanhBao.ColumnEdit = this.repositoryItemMemoEdit2;
            this.CanhBao.FieldName = "CanhBao";
            this.CanhBao.Name = "CanhBao";
            this.CanhBao.OptionsColumn.AllowEdit = false;
            this.CanhBao.OptionsColumn.ReadOnly = true;
            this.CanhBao.OptionsColumn.ShowCaption = false;
            this.CanhBao.Visible = true;
            this.CanhBao.Width = 278;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(1017, 376);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 6;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(889, 376);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(122, 23);
            this.uiButton1.TabIndex = 4;
            this.uiButton1.Text = "Xuất báo cáo";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(26, 6);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(174, 80);
            this.dgList.TabIndex = 0;
            this.dgList.TabStop = false;
            this.dgList.Visible = false;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(808, 375);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.Visible = false;
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiCommandManager
            // 
            this.uiCommandManager.BottomRebar = this.BottomRebar1;
            this.uiCommandManager.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCmdMain});
            this.uiCommandManager.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdQuanLyHopDongXuatKhau,
            this.cmdQuanLyChungTu,
            this.cmdTaoCTTT,
            this.cmdXoaChungTu});
            this.uiCommandManager.ContainerControl = this;
            this.uiCommandManager.Id = new System.Guid("7dc5cb1e-ad95-457a-a83e-06ae21390bc8");
            this.uiCommandManager.ImageList = this.imgSmall;
            this.uiCommandManager.LeftRebar = this.LeftRebar1;
            this.uiCommandManager.RightRebar = this.RightRebar1;
            this.uiCommandManager.Tag = null;
            this.uiCommandManager.TopRebar = this.TopRebar1;
            this.uiCommandManager.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandManager_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 396);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(911, 0);
            // 
            // uiCmdMain
            // 
            this.uiCmdMain.CommandManager = this.uiCommandManager;
            this.uiCmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdQuanLyHopDongXuatKhau1,
            this.cmdQuanLyChungTu1,
            this.cmdTaoCTTT1,
            this.cmdXoaChungTu1});
            this.uiCmdMain.Key = "uiCmdMain";
            this.uiCmdMain.Location = new System.Drawing.Point(0, 0);
            this.uiCmdMain.Name = "uiCmdMain";
            this.uiCmdMain.RowIndex = 0;
            this.uiCmdMain.Size = new System.Drawing.Size(677, 28);
            this.uiCmdMain.Text = "CommandBar1";
            // 
            // cmdQuanLyHopDongXuatKhau1
            // 
            this.cmdQuanLyHopDongXuatKhau1.Key = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau1.Name = "cmdQuanLyHopDongXuatKhau1";
            // 
            // cmdQuanLyChungTu1
            // 
            this.cmdQuanLyChungTu1.Key = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu1.Name = "cmdQuanLyChungTu1";
            // 
            // cmdTaoCTTT1
            // 
            this.cmdTaoCTTT1.Key = "cmdTaoCTTT";
            this.cmdTaoCTTT1.Name = "cmdTaoCTTT1";
            // 
            // cmdXoaChungTu1
            // 
            this.cmdXoaChungTu1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXoaChungTu1.Icon")));
            this.cmdXoaChungTu1.Key = "cmdXoaChungTu";
            this.cmdXoaChungTu1.Name = "cmdXoaChungTu1";
            // 
            // cmdQuanLyHopDongXuatKhau
            // 
            this.cmdQuanLyHopDongXuatKhau.ImageIndex = 0;
            this.cmdQuanLyHopDongXuatKhau.Key = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau.Name = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau.Text = "Quản lý hợp đồng xuất khẩu";
            // 
            // cmdQuanLyChungTu
            // 
            this.cmdQuanLyChungTu.ImageIndex = 0;
            this.cmdQuanLyChungTu.Key = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu.Name = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu.Text = "Chi tiết chứng từ thanh toán";
            // 
            // cmdTaoCTTT
            // 
            this.cmdTaoCTTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTaoCTTT.Icon")));
            this.cmdTaoCTTT.Key = "cmdTaoCTTT";
            this.cmdTaoCTTT.Name = "cmdTaoCTTT";
            this.cmdTaoCTTT.Text = "Tạo mới thông tin chứng từ thanh toán";
            // 
            // cmdXoaChungTu
            // 
            this.cmdXoaChungTu.Key = "cmdXoaChungTu";
            this.cmdXoaChungTu.Name = "cmdXoaChungTu";
            this.cmdXoaChungTu.Text = "Xóa báo cáo";
            // 
            // imgSmall
            // 
            this.imgSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgSmall.ImageStream")));
            this.imgSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imgSmall.Images.SetKeyName(0, "");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 368);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(911, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 368);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCmdMain});
            this.TopRebar1.CommandManager = this.uiCommandManager;
            this.TopRebar1.Controls.Add(this.uiCmdMain);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1095, 28);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(3, 375);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(19, 18);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.Green;
            this.panel2.Location = new System.Drawing.Point(185, 375);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(19, 18);
            this.panel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(28, 379);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Tờ khai chưa phân bổ CTTT";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(210, 379);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tờ khai phân bổ dư trị giá";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.Color.Blue;
            this.panel3.Location = new System.Drawing.Point(381, 375);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(19, 18);
            this.panel3.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(406, 379);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tờ khai phân bổ thiếu trị giá";
            // 
            // ChungTuThanhToanForm_Modified
            // 
            this.ClientSize = new System.Drawing.Size(1095, 430);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChungTuThanhToanForm_Modified";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng tổng hợp chứng từ thanh toán";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ChungTuThanhToanForm_Modified_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCmdMain;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyHopDongXuatKhau1;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyChungTu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyHopDongXuatKhau;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyChungTu;
        private System.Windows.Forms.ImageList imgSmall;
        private Janus.Windows.UI.CommandBars.UICommand cmdTaoCTTT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTaoCTTT;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SoToKhai;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SoHopDong;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn NgayDangKyXuat;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn MaHangXuat;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TriGiaHopDong;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TriGiaHangThucXuat;
        private DevExpress.XtraGrid.Columns.GridColumn SoNgayChungTu;
        private DevExpress.XtraGrid.Columns.GridColumn HinhThuc;
        private DevExpress.XtraGrid.Columns.GridColumn TriGiaChungTu;
        private DevExpress.XtraGrid.Columns.GridColumn GhiChu;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CanhBao;
        private DevExpress.XtraGrid.Columns.GridColumn CanhBaoChungTu;
        private DevExpress.XtraGrid.Columns.GridColumn ToKhaiXuat;
        private DevExpress.XtraGrid.Columns.GridColumn NgayDangKyXuat2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoaChungTu;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoaChungTu1;
    }
}
