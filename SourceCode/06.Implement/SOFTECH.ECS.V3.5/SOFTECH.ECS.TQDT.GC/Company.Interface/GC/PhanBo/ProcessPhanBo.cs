﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Interface;
using System.Threading;

namespace SOFTECH.ECS.TQDT.GC.GC.PhanBo
{
    public partial class ProcessPhanBo : BaseForm
    {
        public ProcessPhanBo()
        {
            InitializeComponent();
        }
        public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        DataSet dsBangPhanBo = new DataSet();
        DataSet dsForExcel = new DataSet();
        public long HDID;
        public int soLuongSP;
        public int soTable = 0;
        public int tableCuoi = 0;
        public int TongBang5;
        public int TongBang3;
        public DataSet BangPhanBo;
        private void ProcessPhanBo_Load(object sender, EventArgs e)
        {
            //DoProcess(null);
            //if (BangPhanBo != null)
            //    this.DialogResult = DialogResult.OK;
            //else
            //    this.DialogResult = DialogResult.No;
        }
        public void DoProcess( object obj)
        {
            PhanBo PB = new PhanBo();
            PB.PBEventArgs += new EventHandler<PhanBoEventArgs>(PB_PBEventArgs);
            BangPhanBo = PB.TaoBangPhanBo(tableCuoi, soTable, HDID, soLuongSP, TongBang5, TongBang3, TKMD);
            if (BangPhanBo != null)
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.No;
        }

        void PB_PBEventArgs(object sender, PhanBoEventArgs e)
        {
            this.Invoke(
               new EventHandler<PhanBoEventArgs>(PBHandler),
               sender, e);
        }
        void PBHandler(object sender, PhanBoEventArgs e)
        {
            if (e.error == null)
            {
                lblTrangThai.Text = e.Message;
                uiProgressBar1.Value = e.Percent;
               
            }
            else lblTrangThai.Text = e.error.Message;
        }
    }
}
