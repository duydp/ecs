﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KD.BLL;
using Company.Interface.Report;
#if GC_V3 || GC_V4
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
#endif
namespace Company.Interface
{
    public partial class ToKhaiTriGiaForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public ToKhaiTriGia TKTG;

        public ToKhaiTriGiaForm()
        {
            InitializeComponent();
        }

        private void ToKhaiTriGiaForm_Load(object sender, EventArgs e)
        {
            if (TKTG == null)
            {
                TKTG = new ToKhaiTriGia();
                TKTG.HTGCollection = new HangTriGiaCollection();
            }
            else
            {
                BindData();
            }
            dgList.DataSource = TKTG.HTGCollection;
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                Luu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                addHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }

        private void BindData()
        {
            TKTG.Load();
            TKTG.LoadHTGCollection();
            txtToSo.Text = TKTG.ToSo.ToString();

            if (TKTG.NgayXuatKhau == null || TKTG.NgayXuatKhau.Year <= 1900)
                ccNgayXuatKhau.Text = TKMD.NgayVanDon.ToShortDateString();
            else
                ccNgayXuatKhau.Value = TKTG.NgayXuatKhau;

            rbQuyenSuDung.Checked = TKTG.QuyenSuDung;
            radioButton2.Checked = !rbQuyenSuDung.Checked;

            rbKhongXacDinh.Checked = TKTG.KhongXacDinh;
            radioButton5.Checked = !rbKhongXacDinh.Checked;

            rbTraThem.Checked = TKTG.TraThem;
            radioButton3.Checked = !rbTraThem.Checked;

            rbTienTra.Checked = TKTG.TienTra;
            radioButton11.Checked = !rbTienTra.Checked;

            rbQuanHeDB.Checked = TKTG.CoQuanHeDacBiet;
            radioButton7.Checked = !rbQuanHeDB.Checked;

            rbAnhHuongQH.Checked = TKTG.AnhHuongQuanHe;
            radioButton9.Checked = !rbAnhHuongQH.Checked;
            //if (TKTG.QuyenSuDung)
            //    rbQuyenSuDung.Checked = true;
            //else
            //    rbQuyenSuDung.Checked = false;

            //if (TKTG.KhongXacDinh)
            //    rbKhongXacDinh.Checked = true;
            //else
            //    rbKhongXacDinh.Checked = true;

            //if (TKTG.TraThem)
            //    rbTraThem.Checked = true;
            //else
            //    rbTraThem.Checked = false;

            //if (TKTG.TienTra)
            //    rbTienTra.Checked = true;
            //else
            //    rbTienTra.Checked = false;

            //if (TKTG.CoQuanHeDacBiet)
            //    rbQuanHeDB.Checked = true;
            //else
            //    rbQuanHeDB.Checked = false;

            //if (TKTG.AnhHuongQuanHe)
            //    rbAnhHuongQH.Checked = true;
            //else
            //    rbAnhHuongQH.Checked = false;

            txtGhiChep.Text = TKTG.GhiChep;

            if (TKTG.NgayKhaiBao == null || TKTG.NgayKhaiBao.Year <= 1900)
                ccNgayKhaiBao.Text = DateTime.Now.ToShortDateString();
            else
                ccNgayKhaiBao.Value = TKTG.NgayKhaiBao;
            if (TKTG.ID == 0)
                txtNguoiKhai.Text = TKMD.TenChuHang;
            else
                txtNguoiKhai.Text = TKTG.NguoiKhaiBao;

            txtChucDanh.Text = TKTG.ChucDanhNguoiKhaiBao;

            txtKieuQH.Text = (TKTG.KieuQuanHe);

        }

        private void TopRebar1_Click(object sender, EventArgs e)
        {

        }

        private void Save()
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                if (TKTG.HTGCollection.Count == 0)
                {
                    MLMessages("Chưa chọn hàng cho tờ khai trị giá.", "MSG_SAV06", "", false);
                    return;
                }
                TKTG.AnhHuongQuanHe = rbAnhHuongQH.Checked;
                TKTG.CapDoThuongMai = "B";
                TKTG.ChucDanhNguoiKhaiBao = txtChucDanh.Text.Trim();
                TKTG.CoQuanHeDacBiet = rbQuanHeDB.Checked;
                TKTG.GhiChep = txtGhiChep.Text.Trim();
                TKTG.KhongXacDinh = rbKhongXacDinh.Checked;
                TKTG.KieuQuanHe = txtKieuQH.Text.Trim();
                TKTG.NgayKhaiBao = ccNgayKhaiBao.Value;
                TKTG.NgayXuatKhau = ccNgayXuatKhau.Value;
                TKTG.NguoiKhaiBao = txtNguoiKhai.Text.Trim();
                TKTG.QuyenSuDung = rbQuyenSuDung.Checked;
                TKTG.TienTra = rbTienTra.Checked;
                TKTG.TKMD_ID = TKMD.ID;
                TKTG.ToSo = Convert.ToInt64(txtToSo.Text);
                TKTG.TraThem = rbTraThem.Checked;
                TKTG.InsertUpdateFull();
                if (TKTG.InsertUpdateFull()) MLMessages("Lưu thông tin thành công.", "MSG_SAV02", "", false);
                bool ok = true;
                foreach (ToKhaiTriGia tktg in TKMD.TKTGCollection)
                {
                    if (tktg.ID == TKTG.ID)
                    {
                        ok = false;
                        break;
                    }
                }
                if (ok) TKMD.TKTGCollection.Add(TKTG);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void PrintTKTG()
        {
            ReportViewTKTGA4Form f1 = new ReportViewTKTGA4Form();
            f1.TKTG = this.TKTG;
            f1.TKMD = this.TKMD;
            f1.ShowDialog(this);
        }

        private void add()
        {
            SelectHangTriGiaForm f = new SelectHangTriGiaForm();
            f.TKTG = this.TKTG;
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Luu":
                    this.Save();
                    break;
                case "addHang":
                    this.add();
                    break;
                case "cmdInTKTG":
                    this.PrintTKTG();
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangTKTGForm f = new HangTKTGForm();
                    f.HangTG = (HangTriGia)i.GetRow().DataRow;
                    f.TKTG = this.TKTG;
                    f.TyGiaTT = TKMD.TyGiaTinhThue;
                    if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                        this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET || this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                        f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                    else
                        f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
                    f.ShowDialog(this);
                }
                break;
            }
        }
        
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangTriGia hmd = (HangTriGia)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            hmd.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }



    }
}