using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_QuantityUnit 
	{
        protected static List<VNACC_Category_QuantityUnit> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_QuantityUnit> collection = new List<VNACC_Category_QuantityUnit>();
            while (reader.Read())
            {
                VNACC_Category_QuantityUnit entity = new VNACC_Category_QuantityUnit();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_QuantityUnit> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [Code], Name FROM [dbo].[t_VNACC_Category_QuantityUnit]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }
        public static string GetID(string id)
        {
            try
            {
                string spName = string.Format("SELECT Code FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE Code='{0}'", id);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
                IDataReader reader = db.ExecuteReader(dbCommand);
                if (reader.Read())
                {
                    return reader["Code"].ToString();
                }
                return string.Empty;
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }

        public static List<VNACC_Category_QuantityUnit> SelectCollectionBy(List<VNACC_Category_QuantityUnit> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_QuantityUnit o)
            {
                return o.TableID.Contains(tableID);
            });
        }
	}	
}