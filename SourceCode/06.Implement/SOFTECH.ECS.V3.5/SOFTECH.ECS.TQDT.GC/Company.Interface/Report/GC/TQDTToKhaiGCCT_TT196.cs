﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
using Company.GC.BLL.KDT.GC;
#elif SXXK_V3
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using Company.KDT.SHARE.Components;
using System.Linq;
using System.Collections.Generic;


namespace Company.Interface.Report
{
    public partial class TQDTToKhaiGCCT_TT196 : DevExpress.XtraReports.UI.XtraReport
    {

        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public Company.Interface.Report.ReportViewGCCTTQ_Nhan_TT196Form report;
        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;
        
        public TQDTToKhaiGCCT_TT196()
        {
            InitializeComponent();
        }
        
        public void BindReport()
        {
            try
            {
                ResetEmpty();

                this.PrintingSystem.ShowMarginsWarning = false;

                decimal tongTriGiaNT = 0;


                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh

                //Cuc HQ
                string maCuc = TKCT.MaHaiQuanTiepNhan.Substring(1, 2);
                lblCucHaiQuan.Text = DonViHaiQuan.GetName("Z" + maCuc + "Z");
                lblCucHaiQuan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = TKCT.MaHaiQuanTiepNhan + " - " + DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //So tham chieu
                if (this.TKCT.SoTiepNhan > 0)
                    this.lblSoThamChieu.Text = this.TKCT.SoTiepNhan.ToString();
                else
                    this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                if (this.TKCT.NgayTiepNhan > minDate)
                    lblNgaySoThamChieu.Text = this.TKCT.NgayTiepNhan.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                if (this.TKCT.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKCT.SoToKhai + "";

                //Ngày giờ đăng ký
                if (this.TKCT.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKCT.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                if (this.TKCT.HCTCollection.Count <= 4)
                    lblSoPhuLucToKhai.Text = "00";
                else
                    lblSoPhuLucToKhai.Text = this.TKCT.HCTCollection.Count > 0 ? ((this.TKCT.HCTCollection.Count / 4) + 1).ToString("00") : "0";

                //01.Ben giao Hang
                lblTenDVGiao.Font = lblMSTBenGiao.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblMSTBenGiao.Text = TKCT.MaKhachHang;
                lblTenDVGiao.Text = TKCT.TenDonViDoiTac.ToString();


                //02.Ben nhan hang
                lblTenDVNhan.Font = lblMSTBenNhan.Font = lblDiaChiBenNhan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenDVNhan.Text = GlobalSettings.TEN_DON_VI;
                lblMSTBenNhan.Text = TKCT.MaDoanhNghiep;
                lblDiaChiBenNhan.Text = GlobalSettings.DIA_CHI;


                //03.ben chi dinh nhan hang
                lblBenChiDinhNhan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblBenChiDinhNhan.Text = TKCT.NguoiChiDinhKH; // Tên người chỉ định giao hàng
                lblMaChiDinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblMaChiDinh.Text = TKCT.NguoiChiDinhDV; // Mã người chỉ định giao hàng


                //08.Dai ly Hai quan
                lblTenDaiLy.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenDaiLy.Text = this.TKCT.TenDaiLyTTHQ;


                //04. Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKCT.MaLoaiHinh);
                lblLoaiHinh.Text = TKCT.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));


                //05. Hop dong nhan (Hop dong nhan là do khach hang chuyen nen là HopDongKD)
                lblSoHD.Text = this.TKCT.SoHopDongDV.ToString();
                lblHopDong.Font = lblNgayHD.Font = lblNgayHetHanHD.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKCT.NgayHDDV > minDate)
                    lblNgayHD.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHD.Text = " ";
                if (this.TKCT.NgayHetHanHDDV > minDate)
                    lblNgayHetHanHD.Text = "" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHD.Text = " ";
#if GC_V4
                //06. Phu luc hop dong nhan----------Chua
                TKCT.LoadPhuLucHD();
                if (TKCT.PhuLucHDCollection != null && TKCT.PhuLucHDCollection.Count > 0)
                {
                    PhuLucHopDong plhd = TKCT.PhuLucHDCollection[0];
                    lblSoPL.Text = plhd.SoPhuLuc.ToString();
                    lblSoPL.Font = lblNgayPL.Font = lblNgayHetHanPL.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                    if (plhd.NgayPhuLuc > minDate)
                        lblNgayPL.Text = plhd.NgayPhuLuc.ToString("dd/MM/yyyy");
                    else
                        lblNgayPL.Text = " ";
                    if (plhd.NgayHetHan > minDate)
                        lblNgayHetHanPL.Text = "" + plhd.NgayHetHan.ToString("dd/MM/yyyy");
                    else
                        lblNgayHetHanPL.Text = " ";
                }
#endif



                //07. Dia diem nhan hang
                lblDiaDiemNhanHang.Text = this.TKCT.CuaKhau_ID; lblDiaDiemNhanHang.Text = this.TKCT.CuaKhau_ID + " - " + CuaKhau.GetName(this.TKCT.CuaKhau_ID);
                lblDiaDiemNhanHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblThoiDiemNhanHang.Text = this.TKCT.ThoiGianGiaoHang.ToString("dd/MM/yyyy");
                lblThoiDiemNhanHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //08. Dai ly thu tuc hai quan

                //09. Ket qua va huong dan phan luong
                lblHuongDan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblHuongDan.Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKCT.PhanLuong);
                if (TKCT.PhanLuong != "")
                {
                    lblHuongDan.Text += TKCT.HUONGDAN != "" ? " - " + TKCT.HUONGDAN : "";
                }

                #endregion



                #region Thong tin hang

                List<HangChuyenTiep> dataSourceHangCT = new List<HangChuyenTiep>();

                if (TKCT.HCTCollection.Count <= 4)
                {
                    foreach (HangChuyenTiep item in TKCT.HCTCollection)
                    {
                        dataSourceHangCT.Add(new HangChuyenTiep
                        {
                            SoThuTuHang = item.SoThuTuHang,
                            TenHang = InMaHang ? item.TenHang + "/ " + item.MaHang : item.TenHang,
                            MaHS = item.MaHS,
                            ID_DVT = item.ID_DVT,
                            SoLuong = item.SoLuong,
                            DonGia = item.DonGia,
                            TriGia = item.TriGia
                        });
                    }
                }
                else if (TKCT.HCTCollection.Count > 4)
                {
                    dataSourceHangCT.Add(new HangChuyenTiep { TenHang = "(Chi tiết theo phụ lục đính kèm)", SoThuTuHang = 1 });
                }

                while (dataSourceHangCT.Count < 4)
                {
                    dataSourceHangCT.Add(new HangChuyenTiep { SoThuTuHang = dataSourceHangCT.Count + 1 });
                }

                //                 if (this.TKCT.HCTCollection.Count <= 4)
                //                 {
                //                     #region Dong hang 
                //                     if (this.TKCT.HCTCollection.Count >= 1)
                //                     {
                DetailHangHoa.DataSource = dataSourceHangCT;
                lblSTTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                lblMoTaHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
                lblMaHSHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                lblDVTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "ID_DVT");
                lblLuongHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan.LuongSP));
                lblDonGiaHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGia", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.DonGiaNT));
                lblTGNTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.TriGiaNT));

                //                     }
                //                     #endregion

                //                 }
                //                 else
                //                 {
                // 
                //                     lblMoTaHang.Text = "(Chi tiết theo phụ lục đính kèm)";
                // 
                //                 }

                //16. Cong
                foreach (HangChuyenTiep hct in this.TKCT.HCTCollection)
                {
                    tongTriGiaNT += Math.Round(hct.TriGia, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                }
                lblTongHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTongHang.Text = TKCT.HCTCollection.Count < 5 ? tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)): string.Empty;
                #endregion
                //17. thu tuc hai quan truoc do               
                lblThuTucHQTruoc.Text = "Hợp đồng giao: " + TKCT.SoHDKH + ", Ngày HĐ:" + TKCT.NgayHDKH.ToString("dd/MM/yyy") + ", Ngày hết hạn HĐ:" + TKCT.NgayHetHanHDKH.ToString("dd/MM/yyy");

                //18. Chung tu di kem
                string ctk = "";
                ctk += ((TKCT.GiayPhepCollection != null && TKCT.GiayPhepCollection.Count != 0) ? string.Format("- Giấy phép ({0}) \r\n", TKCT.GiayPhepCollection.Count) : "");
                ctk += ((TKCT.HoaDonThuongMaiCollection != null && TKCT.HoaDonThuongMaiCollection.Count != 0) ? string.Format("- Hóa đơn ({0}) \r\n", TKCT.HoaDonThuongMaiCollection.Count) : "");
                ctk += ((TKCT.HopDongThuongMaiCollection != null && TKCT.HopDongThuongMaiCollection.Count != 0) ? string.Format("- Hợp đồng ({0}) \r\n", TKCT.HopDongThuongMaiCollection.Count) : "");
                ctk += ((TKCT.DeNghiChuyenCuaKhau != null && TKCT.DeNghiChuyenCuaKhau.Count != 0) ? string.Format("- Đề nghị chuyển cửa khẩu ({0}) \r\n", TKCT.DeNghiChuyenCuaKhau.Count) : "");
                ctk += ((TKCT.AnhCollection != null && TKCT.AnhCollection.Count != 0) ? string.Format("- Chứng từ kèm dạng ảnh ({0}) \r\n", TKCT.AnhCollection.Count) : "");
                ctk += ((TKCT.ChungTuNoCollection != null && TKCT.ChungTuNoCollection.Count != 0) ? string.Format("- Chứng từ nợ ({0}) \r\n", TKCT.ChungTuNoCollection.Count) : "");
//                 ctk = ctk.Substring(0, 1).Equals(";") ? ctk.Remove(0, 1) : ctk;
//                 ctk = ctk.Substring(ctk.Length - 1, 1).Equals(";") ? ctk.Remove(ctk.Length - 1, 1) : ctk;
                lblChungTuDiKem.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblChungTuDiKem.Text = ctk;

                //19. Ghi chep khac
                lblGhiChepKhac.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblGhiChepKhac.Text = TKCT.DeXuatKhac.ToString();

                //20. Ky cam doan
                //Ngay thang nam in to khai
                if (TKCT.NgayDangKy == new DateTime(1900, 1, 1))
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKCT.NgayDangKy.Day.ToString("00") + " tháng " + TKCT.NgayDangKy.Month.ToString("00") + " năm " + TKCT.NgayDangKy.Year;

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {
                //Cuc HQ
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lable.Text = "";

                //So tham chieu
                this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";

                //01.Bên giao
                lblTenDVGiao.Text =lblMSTBenGiao.Text="";

                //02.Ben nhân
                lblTenDVNhan.Text = lblMSTBenNhan.Text = "";

                //03.Ben chi đinh
                lblBenChiDinhNhan.Text =  "";

                //04. Ma Loai Hinh
                lblLoaiHinh.Text = "";
                //05. HDGC nhan
                lblHDGCNhan.Text = lblSoHD.Text = lblNgayHD.Text = lblNgayHetHanHD.Text = "";

                //06.Phu luc HDGC nhan
                lblPLHDGCNhan.Text = lblSoPL.Text = lblNgayPL.Text = lblNgayHetHanPL.Text = "";
                
                //07. Dai diem nhan hang
                lblDiaChiBenNhan.Text = lblThoiDiemNhanHang.Text = "";

                //08.Dai ly Hai quan
                lblDaiLyHaiQuan.Text = lblNoiDungUyQuyen.Text = "";

                //09. ket qua phan luong
                lblHuongDan.Text = "";
                //17. Thu tục hai quan trưo do
                lblThuTucHQTruoc.Text = "";
                //18. Chung tu kem
                lblChungTuDiKem.Text = "";

                //19. Ghi chep Khac
                lblGhiChepKhac.Text = "";

                //20.Ngay thang nam in to khai
                lblNgayThangNam.Text = "Ngày...tháng...năm.....";

                

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void lblDVTHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;
            if (!string.IsNullOrEmpty(dvt.Text))
            {
                dvt.Text = DonViTinh.GetName(dvt.Text);
            }

        }

        private void lblLuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == "0")
            {
                cell.Text = string.Empty;
            }
        }
    }
}
