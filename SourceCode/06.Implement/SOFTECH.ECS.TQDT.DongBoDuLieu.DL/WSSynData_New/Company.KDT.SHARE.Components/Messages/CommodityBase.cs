﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CommodityBase
    {

        /// <summary>
        /// Kiểu
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Số thứ tự
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Mô tả hàng hóa
        /// </summary>
        [XmlElement("description")]
        public string Description { set; get; }

        /// <summary>
        /// Mã hàng do doanh nghiệp khai
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Mã HS
        /// </summary>
        [XmlElement("tariffClassification")]
        public string TariffClassification { set; get; }

        /// <summary>
        /// Mã định danh của lệnh sản xuất
        /// </summary>
        [XmlElement("productCtrlNo")]
        public string ProductCtrlNo { set; get; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        [XmlElement("content")]
        public string Content { set; get; }

        /// <summary>
        /// Trị giá sản phẩm hàng thực xuất( Khai báo thanh khoản )
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }


    }
}
