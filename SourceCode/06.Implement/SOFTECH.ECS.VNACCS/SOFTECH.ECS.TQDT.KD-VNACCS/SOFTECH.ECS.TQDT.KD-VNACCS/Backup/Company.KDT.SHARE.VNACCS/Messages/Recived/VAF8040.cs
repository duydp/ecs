using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF8040 : BasicVNACC
    {
       
        public void LoadVAF8040(string strResult)
        {
            try
            {

                this.GetObject<VAF8040>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF8040, false, VAF8040.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8040.TongSoByte);
               
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
