﻿namespace Company.Interface
{
    partial class FrmCauHinhThoiGian
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCauHinhThoiGian));
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbarConnected = new Janus.Windows.EditControls.UITrackBar();
            this.tbarTimeoutBackup = new Janus.Windows.EditControls.UITrackBar();
            this.tbarCountSend = new Janus.Windows.EditControls.UITrackBar();
            this.tbarCounter = new Janus.Windows.EditControls.UITrackBar();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkIsMultiThread = new Janus.Windows.EditControls.UICheckBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Size = new System.Drawing.Size(347, 288);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.tbarConnected);
            this.uiGroupBox6.Controls.Add(this.tbarTimeoutBackup);
            this.uiGroupBox6.Controls.Add(this.tbarCountSend);
            this.uiGroupBox6.Controls.Add(this.tbarCounter);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(347, 176);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Thông tin";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Thời gian kết nối (giây)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Thời gian sao lưu \r\ndữ liệu (phút)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Số lần lấy phản hồi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Thời gian đếm (giây)";
            // 
            // tbarConnected
            // 
            this.tbarConnected.Location = new System.Drawing.Point(127, 57);
            this.tbarConnected.Maximum = 20;
            this.tbarConnected.Name = "tbarConnected";
            this.tbarConnected.Size = new System.Drawing.Size(206, 30);
            this.tbarConnected.SmallChange = 2;
            this.tbarConnected.TabIndex = 1;
            this.tbarConnected.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // tbarTimeoutBackup
            // 
            this.tbarTimeoutBackup.Location = new System.Drawing.Point(127, 129);
            this.tbarTimeoutBackup.Name = "tbarTimeoutBackup";
            this.tbarTimeoutBackup.Size = new System.Drawing.Size(206, 30);
            this.tbarTimeoutBackup.SmallChange = 2;
            this.tbarTimeoutBackup.TabIndex = 3;
            this.tbarTimeoutBackup.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // tbarCountSend
            // 
            this.tbarCountSend.Location = new System.Drawing.Point(127, 93);
            this.tbarCountSend.Name = "tbarCountSend";
            this.tbarCountSend.Size = new System.Drawing.Size(206, 30);
            this.tbarCountSend.SmallChange = 2;
            this.tbarCountSend.TabIndex = 2;
            this.tbarCountSend.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // tbarCounter
            // 
            this.tbarCounter.Location = new System.Drawing.Point(127, 21);
            this.tbarCounter.Maximum = 20;
            this.tbarCounter.Name = "tbarCounter";
            this.tbarCounter.Size = new System.Drawing.Size(206, 30);
            this.tbarCounter.SmallChange = 2;
            this.tbarCounter.TabIndex = 0;
            this.tbarCounter.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageList = this.imageList1;
            this.btnSave.Location = new System.Drawing.Point(102, 257);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "connect.png");
            this.imageList1.Images.SetKeyName(1, "disconnect.png");
            this.imageList1.Images.SetKeyName(2, "disk.png");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(183, 257);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkIsMultiThread);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 176);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(347, 61);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Cấu hình phân luồng cho tiến trình chạy Cập nhật tự động";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkIsMultiThread
            // 
            this.chkIsMultiThread.Location = new System.Drawing.Point(185, 29);
            this.chkIsMultiThread.Name = "chkIsMultiThread";
            this.chkIsMultiThread.Size = new System.Drawing.Size(17, 18);
            this.chkIsMultiThread.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cho phép chạy tiến trình đa luồng";
            // 
            // FrmCauHinhThoiGian
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 288);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(309, 163);
            this.Name = "FrmCauHinhThoiGian";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thiết lập thông tin";
            this.Load += new System.EventHandler(this.FrmCauHinhThoiGian_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.EditControls.UITrackBar tbarCounter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UITrackBar tbarConnected;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UITrackBar tbarCountSend;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UITrackBar tbarTimeoutBackup;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UICheckBox chkIsMultiThread;
        private System.Windows.Forms.Label label5;
    }
}