﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.VNACC;

namespace Company.Interface.GC
{
    public partial class XuLyHopDongFrm : BaseForm
    {
        public XuLyHopDongFrm()
        {
            InitializeComponent();
        }
        public Company.GC.BLL.KDT.GC.HopDong HD;
        public Exception ExceptionForm;
        public bool isDongBoThongQuan;
        public bool isDinhMucChuaDangKy;
        public DateTime dateFrom;
        public DateTime dateTo;
        public bool isQuyetoan = false;
        public bool isXNT = false;
        private void SetStatus(int valueProcess, string sts)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess; }));
            }
            else
            {
                lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess;
            }
        }
        private void CloseFrom(bool isOK)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { this.DialogResult = isOK ? DialogResult.OK : DialogResult.No; }));
            }
            else
            {
                this.DialogResult = isOK ? DialogResult.OK : DialogResult.No;
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                SetStatus(0, "Đồng bộ tờ khai");
                List<Company.KDT.SHARE.VNACCS.KDT_VNACC_ToKhaiMauDich> list = Company.KDT.SHARE.VNACCS.KDT_VNACC_ToKhaiMauDich.SelectCollectionChuaThanhKhoan(isDongBoThongQuan, false);
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        try
                        {
                            Company.KDT.SHARE.VNACCS.KDT_VNACC_ToKhaiMauDich item = list[i];
                            item.LoadFull();
                            ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(item, GlobalSettings.MA_DON_VI != "4000395355");
                            SetStatus(i * 100 / list.Count, "Đồng bộ tờ khai");
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                    }
                }

                Company.GC.BLL.KDT.ToKhaiMauDich.CapNhatNamDangKyToKhaiDaDuyet();

                HD.CapNhatLoaiHangHoaToKhai();
                HD.dateFrom = dateFrom;
                HD.dateTo = dateTo;
                if (isQuyetoan)
                {
                    HD.ProcessQuyetToanHopDong(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP, GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.TLHH,dateFrom,dateTo);
                }
                else if (isXNT)
                {
                    HD.ProcessBCXuatNhapTon(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP, GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.TLHH,dateFrom,dateTo);
                }
                else
                {
                    HD.TinhToanCanDoiHopDong(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP, GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.TLHH,isDinhMucChuaDangKy);
                }
                CloseFrom(true);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ExceptionForm = ex;
                CloseFrom(false);
            }
        }

        private void XuLyHopDongFrm_Load(object sender, EventArgs e)
        {
            HD.HopDongEventArg += new EventHandler<Company.GC.BLL.KDT.GC.HopDongEventArgs>(HD_HopDongEventArg);
            //this.Load +=new EventHandler(XuLyHopDongFrm_Load);
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }

        void HD_HopDongEventArg(object sender, Company.GC.BLL.KDT.GC.HopDongEventArgs e)
        {
            if (e.Error == null)
            {
                SetStatus(e.Percent, e.Status);
            }
            else
            {
                ExceptionForm = e.Error;
                CloseFrom(false);

            }
        }

    }
}
