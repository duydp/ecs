﻿namespace CA_QuanLyGCN
{
    partial class ThongTinDoanhNghiepfrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridThongTinPhat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongTinDoanhNghiepfrm));
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout gridChungNhan_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridThongTinPhat = new Janus.Windows.GridEX.GridEX();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridChungNhan = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnThemGiayCN = new Janus.Windows.EditControls.UIButton();
            this.btnThemQDPhat = new Janus.Windows.EditControls.UIButton();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCoQuanCapMST = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNgayCapMST = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDiaChiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMST = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtHoKhauTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHoTen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNgayCapCMND = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtCoQuanCapCMND = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuocTich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCNMD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtChucDanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThongTinPhat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridChungNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(980, 672);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox4.Controls.Add(this.gridThongTinPhat);
            this.uiGroupBox4.Location = new System.Drawing.Point(630, 12);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(344, 654);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin phạt";
            this.uiGroupBox4.VisualStyleManager = this.visualStyleManager1;
            // 
            // gridThongTinPhat
            // 
            this.gridThongTinPhat.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridThongTinPhat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            gridThongTinPhat_DesignTimeLayout.LayoutString = resources.GetString("gridThongTinPhat_DesignTimeLayout.LayoutString");
            this.gridThongTinPhat.DesignTimeLayout = gridThongTinPhat_DesignTimeLayout;
            this.gridThongTinPhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridThongTinPhat.GroupByBoxVisible = false;
            this.gridThongTinPhat.Location = new System.Drawing.Point(3, 16);
            this.gridThongTinPhat.Name = "gridThongTinPhat";
            this.gridThongTinPhat.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.gridThongTinPhat.Size = new System.Drawing.Size(338, 635);
            this.gridThongTinPhat.TabIndex = 1;
            this.gridThongTinPhat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridThongTinPhat.VisualStyleManager = this.visualStyleManager1;
            this.gridThongTinPhat.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridThongTinPhat_RowDoubleClick);
            this.gridThongTinPhat.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.gridThongTinPhat_DeletingRecords);
            // 
            // visualStyleManager1
            // 
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Office2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme2);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uiGroupBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox3.Controls.Add(this.gridChungNhan);
            this.uiGroupBox3.Location = new System.Drawing.Point(6, 431);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(618, 235);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Các cơ sở kinh doanh đã chứng nhận";
            this.uiGroupBox3.VisualStyleManager = this.visualStyleManager1;
            // 
            // gridChungNhan
            // 
            this.gridChungNhan.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridChungNhan.ColumnAutoSizeMode = Janus.Windows.GridEX.ColumnAutoSizeMode.DisplayedCellsAndHeader;
            gridChungNhan_DesignTimeLayout.LayoutString = resources.GetString("gridChungNhan_DesignTimeLayout.LayoutString");
            this.gridChungNhan.DesignTimeLayout = gridChungNhan_DesignTimeLayout;
            this.gridChungNhan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridChungNhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridChungNhan.FrozenColumns = 3;
            this.gridChungNhan.GroupByBoxVisible = false;
            this.gridChungNhan.Location = new System.Drawing.Point(3, 16);
            this.gridChungNhan.Name = "gridChungNhan";
            this.gridChungNhan.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.gridChungNhan.Size = new System.Drawing.Size(612, 216);
            this.gridChungNhan.TabIndex = 0;
            this.gridChungNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridChungNhan.VisualStyleManager = this.visualStyleManager1;
            this.gridChungNhan.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridChungNhan_RowDoubleClick);
            this.gridChungNhan.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.gridChungNhan_DeletingRecords);
            this.gridChungNhan.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridChungNhan_LoadingRow);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox2.Controls.Add(this.lblStatus);
            this.uiGroupBox2.Controls.Add(this.btnThemGiayCN);
            this.uiGroupBox2.Controls.Add(this.btnThemQDPhat);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.btnLuu);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox2.Location = new System.Drawing.Point(6, 11);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(618, 414);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin doanh nghiệp";
            this.uiGroupBox2.VisualStyleManager = this.visualStyleManager1;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblStatus.Location = new System.Drawing.Point(6, 390);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 5;
            // 
            // btnThemGiayCN
            // 
            this.btnThemGiayCN.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemGiayCN.Icon")));
            this.btnThemGiayCN.Location = new System.Drawing.Point(309, 385);
            this.btnThemGiayCN.Name = "btnThemGiayCN";
            this.btnThemGiayCN.Size = new System.Drawing.Size(75, 23);
            this.btnThemGiayCN.TabIndex = 4;
            this.btnThemGiayCN.Text = "Thêm CN";
            this.btnThemGiayCN.VisualStyleManager = this.visualStyleManager1;
            this.btnThemGiayCN.Click += new System.EventHandler(this.btnThemGiayCN_Click);
            // 
            // btnThemQDPhat
            // 
            this.btnThemQDPhat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemQDPhat.Icon")));
            this.btnThemQDPhat.Location = new System.Drawing.Point(390, 385);
            this.btnThemQDPhat.Name = "btnThemQDPhat";
            this.btnThemQDPhat.Size = new System.Drawing.Size(117, 23);
            this.btnThemQDPhat.TabIndex = 5;
            this.btnThemQDPhat.Text = "Thêm QĐ phạt";
            this.btnThemQDPhat.VisualStyleManager = this.visualStyleManager1;
            this.btnThemQDPhat.Click += new System.EventHandler(this.btnThemQDPhat_Click);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(110, 313);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(496, 66);
            this.txtGhiChu.TabIndex = 2;
            this.txtGhiChu.VisualStyleManager = this.visualStyleManager1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 313);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ghi chú về DN";
            // 
            // btnLuu
            // 
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(513, 385);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(93, 23);
            this.btnLuu.TabIndex = 3;
            this.btnLuu.Text = "Lưu thông tin";
            this.btnLuu.VisualStyleManager = this.visualStyleManager1;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtCoQuanCapMST);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.txtNgayCapMST);
            this.uiGroupBox6.Controls.Add(this.label5);
            this.uiGroupBox6.Controls.Add(this.txtDiaChiDN);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.txtTenDN);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.txtMST);
            this.uiGroupBox6.Location = new System.Drawing.Point(6, 19);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(606, 113);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtCoQuanCapMST
            // 
            this.txtCoQuanCapMST.Location = new System.Drawing.Point(433, 11);
            this.txtCoQuanCapMST.Name = "txtCoQuanCapMST";
            this.txtCoQuanCapMST.Size = new System.Drawing.Size(167, 20);
            this.txtCoQuanCapMST.TabIndex = 2;
            this.txtCoQuanCapMST.Text = "Sở kế hoạch và đầu tư TPĐN";
            this.txtCoQuanCapMST.VisualStyleManager = this.visualStyleManager1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "MST doanh nghiệp";
            // 
            // txtNgayCapMST
            // 
            // 
            // 
            // 
            this.txtNgayCapMST.DropDownCalendar.Name = "";
            this.txtNgayCapMST.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.txtNgayCapMST.Location = new System.Drawing.Point(262, 10);
            this.txtNgayCapMST.Name = "txtNgayCapMST";
            this.txtNgayCapMST.Size = new System.Drawing.Size(94, 20);
            this.txtNgayCapMST.TabIndex = 1;
            this.txtNgayCapMST.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.txtNgayCapMST.VisualStyleManager = this.visualStyleManager1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(362, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cơ quan cấp";
            // 
            // txtDiaChiDN
            // 
            this.txtDiaChiDN.Location = new System.Drawing.Point(104, 83);
            this.txtDiaChiDN.Name = "txtDiaChiDN";
            this.txtDiaChiDN.Size = new System.Drawing.Size(496, 20);
            this.txtDiaChiDN.TabIndex = 4;
            this.txtDiaChiDN.VisualStyleManager = this.visualStyleManager1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày cấp ";
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(104, 47);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(496, 20);
            this.txtTenDN.TabIndex = 3;
            this.txtTenDN.VisualStyleManager = this.visualStyleManager1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên doanh nghiệp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Địa chỉ trụ sở";
            // 
            // txtMST
            // 
            this.txtMST.Location = new System.Drawing.Point(104, 11);
            this.txtMST.Name = "txtMST";
            this.txtMST.Size = new System.Drawing.Size(95, 20);
            this.txtMST.TabIndex = 0;
            this.txtMST.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtHoKhauTT);
            this.uiGroupBox5.Controls.Add(this.label12);
            this.uiGroupBox5.Controls.Add(this.txtHoTen);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtNgayCapCMND);
            this.uiGroupBox5.Controls.Add(this.txtCoQuanCapCMND);
            this.uiGroupBox5.Controls.Add(this.txtQuocTich);
            this.uiGroupBox5.Controls.Add(this.txtSoCNMD);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Controls.Add(this.txtChucDanh);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.label9);
            this.uiGroupBox5.Controls.Add(this.label7);
            this.uiGroupBox5.Location = new System.Drawing.Point(6, 138);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(606, 169);
            this.uiGroupBox5.TabIndex = 1;
            this.uiGroupBox5.Text = "Người đại diện doanh nghiệp";
            this.uiGroupBox5.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtHoKhauTT
            // 
            this.txtHoKhauTT.Location = new System.Drawing.Point(104, 139);
            this.txtHoKhauTT.Name = "txtHoKhauTT";
            this.txtHoKhauTT.Size = new System.Drawing.Size(496, 20);
            this.txtHoKhauTT.TabIndex = 6;
            this.txtHoKhauTT.VisualStyleManager = this.visualStyleManager1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Nơi đăng ký HKTT";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(104, 29);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(496, 20);
            this.txtHoTen.TabIndex = 0;
            this.txtHoTen.VisualStyleManager = this.visualStyleManager1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Họ và tên";
            // 
            // txtNgayCapCMND
            // 
            // 
            // 
            // 
            this.txtNgayCapCMND.DropDownCalendar.Name = "";
            this.txtNgayCapCMND.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.txtNgayCapCMND.Location = new System.Drawing.Point(262, 101);
            this.txtNgayCapCMND.Name = "txtNgayCapCMND";
            this.txtNgayCapCMND.Size = new System.Drawing.Size(94, 20);
            this.txtNgayCapCMND.TabIndex = 4;
            this.txtNgayCapCMND.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtCoQuanCapCMND
            // 
            this.txtCoQuanCapCMND.Location = new System.Drawing.Point(433, 102);
            this.txtCoQuanCapCMND.Name = "txtCoQuanCapCMND";
            this.txtCoQuanCapCMND.Size = new System.Drawing.Size(167, 20);
            this.txtCoQuanCapCMND.TabIndex = 5;
            this.txtCoQuanCapCMND.Text = "CA TP ĐÀ NẴNG";
            this.txtCoQuanCapCMND.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtQuocTich
            // 
            this.txtQuocTich.Location = new System.Drawing.Point(262, 65);
            this.txtQuocTich.Name = "txtQuocTich";
            this.txtQuocTich.Size = new System.Drawing.Size(94, 20);
            this.txtQuocTich.TabIndex = 2;
            this.txtQuocTich.Text = "VIỆT NAM";
            this.txtQuocTich.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtSoCNMD
            // 
            this.txtSoCNMD.Location = new System.Drawing.Point(104, 102);
            this.txtSoCNMD.Name = "txtSoCNMD";
            this.txtSoCNMD.Size = new System.Drawing.Size(95, 20);
            this.txtSoCNMD.TabIndex = 3;
            this.txtSoCNMD.VisualStyleManager = this.visualStyleManager1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(362, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Cơ quan cấp";
            // 
            // txtChucDanh
            // 
            this.txtChucDanh.Location = new System.Drawing.Point(104, 65);
            this.txtChucDanh.Name = "txtChucDanh";
            this.txtChucDanh.Size = new System.Drawing.Size(95, 20);
            this.txtChucDanh.TabIndex = 1;
            this.txtChucDanh.Text = "GIÁM ĐỐC";
            this.txtChucDanh.VisualStyleManager = this.visualStyleManager1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(205, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ngày cấp";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Quốc tịch";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số CMND";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chức danh";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ThongTinDoanhNghiepfrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 672);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "ThongTinDoanhNghiepfrm";
            this.Text = "ThongTinDoanhNghiepfrm";
            this.Load += new System.EventHandler(this.ThongTinDoanhNghiepfrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridThongTinPhat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridChungNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX gridThongTinPhat;
        private Janus.Windows.GridEX.GridEX gridChungNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private Janus.Windows.GridEX.EditControls.EditBox txtMST;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo txtNgayCapMST;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDN;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoTen;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtChucDanh;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanCapMST;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.CalendarCombo.CalendarCombo txtNgayCapCMND;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanCapCMND;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuocTich;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCNMD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton btnThemGiayCN;
        private Janus.Windows.EditControls.UIButton btnThemQDPhat;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoKhauTT;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblStatus;
    }
}