using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_SXXK_PhuKienDetail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public DateTime NgayTKHeThong { set; get; }
		public DateTime NgayTKSuaDoi { set; get; }
		public string SoHopDongCu { set; get; }
		public string SoHopDongMoi { set; get; }
		public DateTime NgayHopDongCu { set; get; }
		public DateTime NgayHopDongMoi { set; get; }
		public int STTHang { set; get; }
		public string MaHangCu { set; get; }
		public string LoaiHangCu { set; get; }
		public decimal SoLuongCu { set; get; }
		public string DVTCu { set; get; }
		public string MaHQDangKyCu { set; get; }
		public string MaHangMoi { set; get; }
		public string LoaiHangMoi { set; get; }
		public decimal SoLuongMoi { set; get; }
		public string DVTMoi { set; get; }
		public string MaHQDangKyMoi { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_SXXK_PhuKienDetail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_SXXK_PhuKienDetail> collection = new List<KDT_SXXK_PhuKienDetail>();
			while (reader.Read())
			{
				KDT_SXXK_PhuKienDetail entity = new KDT_SXXK_PhuKienDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTKHeThong"))) entity.NgayTKHeThong = reader.GetDateTime(reader.GetOrdinal("NgayTKHeThong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTKSuaDoi"))) entity.NgayTKSuaDoi = reader.GetDateTime(reader.GetOrdinal("NgayTKSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongCu"))) entity.SoHopDongCu = reader.GetString(reader.GetOrdinal("SoHopDongCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongMoi"))) entity.SoHopDongMoi = reader.GetString(reader.GetOrdinal("SoHopDongMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongCu"))) entity.NgayHopDongCu = reader.GetDateTime(reader.GetOrdinal("NgayHopDongCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongMoi"))) entity.NgayHopDongMoi = reader.GetDateTime(reader.GetOrdinal("NgayHopDongMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangCu"))) entity.MaHangCu = reader.GetString(reader.GetOrdinal("MaHangCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangCu"))) entity.LoaiHangCu = reader.GetString(reader.GetOrdinal("LoaiHangCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCu"))) entity.SoLuongCu = reader.GetDecimal(reader.GetOrdinal("SoLuongCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTCu"))) entity.DVTCu = reader.GetString(reader.GetOrdinal("DVTCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQDangKyCu"))) entity.MaHQDangKyCu = reader.GetString(reader.GetOrdinal("MaHQDangKyCu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangMoi"))) entity.MaHangMoi = reader.GetString(reader.GetOrdinal("MaHangMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangMoi"))) entity.LoaiHangMoi = reader.GetString(reader.GetOrdinal("LoaiHangMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongMoi"))) entity.SoLuongMoi = reader.GetDecimal(reader.GetOrdinal("SoLuongMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTMoi"))) entity.DVTMoi = reader.GetString(reader.GetOrdinal("DVTMoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQDangKyMoi"))) entity.MaHQDangKyMoi = reader.GetString(reader.GetOrdinal("MaHQDangKyMoi"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_SXXK_PhuKienDetail> collection, long id)
        {
            foreach (KDT_SXXK_PhuKienDetail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_PhuKienDetail VALUES(@Master_ID, @NgayTKHeThong, @NgayTKSuaDoi, @SoHopDongCu, @SoHopDongMoi, @NgayHopDongCu, @NgayHopDongMoi, @STTHang, @MaHangCu, @LoaiHangCu, @SoLuongCu, @DVTCu, @MaHQDangKyCu, @MaHangMoi, @LoaiHangMoi, @SoLuongMoi, @DVTMoi, @MaHQDangKyMoi)";
            string update = "UPDATE t_KDT_SXXK_PhuKienDetail SET Master_ID = @Master_ID, NgayTKHeThong = @NgayTKHeThong, NgayTKSuaDoi = @NgayTKSuaDoi, SoHopDongCu = @SoHopDongCu, SoHopDongMoi = @SoHopDongMoi, NgayHopDongCu = @NgayHopDongCu, NgayHopDongMoi = @NgayHopDongMoi, STTHang = @STTHang, MaHangCu = @MaHangCu, LoaiHangCu = @LoaiHangCu, SoLuongCu = @SoLuongCu, DVTCu = @DVTCu, MaHQDangKyCu = @MaHQDangKyCu, MaHangMoi = @MaHangMoi, LoaiHangMoi = @LoaiHangMoi, SoLuongMoi = @SoLuongMoi, DVTMoi = @DVTMoi, MaHQDangKyMoi = @MaHQDangKyMoi WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_PhuKienDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKHeThong", SqlDbType.DateTime, "NgayTKHeThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, "NgayTKSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongCu", SqlDbType.VarChar, "SoHopDongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongMoi", SqlDbType.VarChar, "SoHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongCu", SqlDbType.DateTime, "NgayHopDongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongMoi", SqlDbType.DateTime, "NgayHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangCu", SqlDbType.VarChar, "MaHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangCu", SqlDbType.VarChar, "LoaiHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCu", SqlDbType.Decimal, "SoLuongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTCu", SqlDbType.VarChar, "DVTCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQDangKyCu", SqlDbType.VarChar, "MaHQDangKyCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangMoi", SqlDbType.VarChar, "MaHangMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangMoi", SqlDbType.VarChar, "LoaiHangMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongMoi", SqlDbType.Decimal, "SoLuongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMoi", SqlDbType.VarChar, "DVTMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, "MaHQDangKyMoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKHeThong", SqlDbType.DateTime, "NgayTKHeThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, "NgayTKSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongCu", SqlDbType.VarChar, "SoHopDongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongMoi", SqlDbType.VarChar, "SoHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongCu", SqlDbType.DateTime, "NgayHopDongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongMoi", SqlDbType.DateTime, "NgayHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangCu", SqlDbType.VarChar, "MaHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangCu", SqlDbType.VarChar, "LoaiHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCu", SqlDbType.Decimal, "SoLuongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTCu", SqlDbType.VarChar, "DVTCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQDangKyCu", SqlDbType.VarChar, "MaHQDangKyCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangMoi", SqlDbType.VarChar, "MaHangMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangMoi", SqlDbType.VarChar, "LoaiHangMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongMoi", SqlDbType.Decimal, "SoLuongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMoi", SqlDbType.VarChar, "DVTMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, "MaHQDangKyMoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_PhuKienDetail VALUES(@Master_ID, @NgayTKHeThong, @NgayTKSuaDoi, @SoHopDongCu, @SoHopDongMoi, @NgayHopDongCu, @NgayHopDongMoi, @STTHang, @MaHangCu, @LoaiHangCu, @SoLuongCu, @DVTCu, @MaHQDangKyCu, @MaHangMoi, @LoaiHangMoi, @SoLuongMoi, @DVTMoi, @MaHQDangKyMoi)";
            string update = "UPDATE t_KDT_SXXK_PhuKienDetail SET Master_ID = @Master_ID, NgayTKHeThong = @NgayTKHeThong, NgayTKSuaDoi = @NgayTKSuaDoi, SoHopDongCu = @SoHopDongCu, SoHopDongMoi = @SoHopDongMoi, NgayHopDongCu = @NgayHopDongCu, NgayHopDongMoi = @NgayHopDongMoi, STTHang = @STTHang, MaHangCu = @MaHangCu, LoaiHangCu = @LoaiHangCu, SoLuongCu = @SoLuongCu, DVTCu = @DVTCu, MaHQDangKyCu = @MaHQDangKyCu, MaHangMoi = @MaHangMoi, LoaiHangMoi = @LoaiHangMoi, SoLuongMoi = @SoLuongMoi, DVTMoi = @DVTMoi, MaHQDangKyMoi = @MaHQDangKyMoi WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_PhuKienDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKHeThong", SqlDbType.DateTime, "NgayTKHeThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, "NgayTKSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongCu", SqlDbType.VarChar, "SoHopDongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongMoi", SqlDbType.VarChar, "SoHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongCu", SqlDbType.DateTime, "NgayHopDongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongMoi", SqlDbType.DateTime, "NgayHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangCu", SqlDbType.VarChar, "MaHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangCu", SqlDbType.VarChar, "LoaiHangCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCu", SqlDbType.Decimal, "SoLuongCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTCu", SqlDbType.VarChar, "DVTCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQDangKyCu", SqlDbType.VarChar, "MaHQDangKyCu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangMoi", SqlDbType.VarChar, "MaHangMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangMoi", SqlDbType.VarChar, "LoaiHangMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongMoi", SqlDbType.Decimal, "SoLuongMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMoi", SqlDbType.VarChar, "DVTMoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, "MaHQDangKyMoi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKHeThong", SqlDbType.DateTime, "NgayTKHeThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, "NgayTKSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongCu", SqlDbType.VarChar, "SoHopDongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongMoi", SqlDbType.VarChar, "SoHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongCu", SqlDbType.DateTime, "NgayHopDongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongMoi", SqlDbType.DateTime, "NgayHopDongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangCu", SqlDbType.VarChar, "MaHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangCu", SqlDbType.VarChar, "LoaiHangCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCu", SqlDbType.Decimal, "SoLuongCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTCu", SqlDbType.VarChar, "DVTCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQDangKyCu", SqlDbType.VarChar, "MaHQDangKyCu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangMoi", SqlDbType.VarChar, "MaHangMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangMoi", SqlDbType.VarChar, "LoaiHangMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongMoi", SqlDbType.Decimal, "SoLuongMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMoi", SqlDbType.VarChar, "DVTMoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, "MaHQDangKyMoi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_SXXK_PhuKienDetail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_SXXK_PhuKienDetail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_SXXK_PhuKienDetail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_SXXK_PhuKienDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_SXXK_PhuKienDetail(long master_ID, DateTime ngayTKHeThong, DateTime ngayTKSuaDoi, string soHopDongCu, string soHopDongMoi, DateTime ngayHopDongCu, DateTime ngayHopDongMoi, int sTTHang, string maHangCu, string loaiHangCu, decimal soLuongCu, string dVTCu, string maHQDangKyCu, string maHangMoi, string loaiHangMoi, decimal soLuongMoi, string dVTMoi, string maHQDangKyMoi)
		{
			KDT_SXXK_PhuKienDetail entity = new KDT_SXXK_PhuKienDetail();	
			entity.Master_ID = master_ID;
			entity.NgayTKHeThong = ngayTKHeThong;
			entity.NgayTKSuaDoi = ngayTKSuaDoi;
			entity.SoHopDongCu = soHopDongCu;
			entity.SoHopDongMoi = soHopDongMoi;
			entity.NgayHopDongCu = ngayHopDongCu;
			entity.NgayHopDongMoi = ngayHopDongMoi;
			entity.STTHang = sTTHang;
			entity.MaHangCu = maHangCu;
			entity.LoaiHangCu = loaiHangCu;
			entity.SoLuongCu = soLuongCu;
			entity.DVTCu = dVTCu;
			entity.MaHQDangKyCu = maHQDangKyCu;
			entity.MaHangMoi = maHangMoi;
			entity.LoaiHangMoi = loaiHangMoi;
			entity.SoLuongMoi = soLuongMoi;
			entity.DVTMoi = dVTMoi;
			entity.MaHQDangKyMoi = maHQDangKyMoi;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NgayTKHeThong", SqlDbType.DateTime, NgayTKHeThong.Year <= 1753 ? DBNull.Value : (object) NgayTKHeThong);
			db.AddInParameter(dbCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, NgayTKSuaDoi.Year <= 1753 ? DBNull.Value : (object) NgayTKSuaDoi);
			db.AddInParameter(dbCommand, "@SoHopDongCu", SqlDbType.VarChar, SoHopDongCu);
			db.AddInParameter(dbCommand, "@SoHopDongMoi", SqlDbType.VarChar, SoHopDongMoi);
			db.AddInParameter(dbCommand, "@NgayHopDongCu", SqlDbType.DateTime, NgayHopDongCu.Year <= 1753 ? DBNull.Value : (object) NgayHopDongCu);
			db.AddInParameter(dbCommand, "@NgayHopDongMoi", SqlDbType.DateTime, NgayHopDongMoi.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMoi);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@MaHangCu", SqlDbType.VarChar, MaHangCu);
			db.AddInParameter(dbCommand, "@LoaiHangCu", SqlDbType.VarChar, LoaiHangCu);
			db.AddInParameter(dbCommand, "@SoLuongCu", SqlDbType.Decimal, SoLuongCu);
			db.AddInParameter(dbCommand, "@DVTCu", SqlDbType.VarChar, DVTCu);
			db.AddInParameter(dbCommand, "@MaHQDangKyCu", SqlDbType.VarChar, MaHQDangKyCu);
			db.AddInParameter(dbCommand, "@MaHangMoi", SqlDbType.VarChar, MaHangMoi);
			db.AddInParameter(dbCommand, "@LoaiHangMoi", SqlDbType.VarChar, LoaiHangMoi);
			db.AddInParameter(dbCommand, "@SoLuongMoi", SqlDbType.Decimal, SoLuongMoi);
			db.AddInParameter(dbCommand, "@DVTMoi", SqlDbType.VarChar, DVTMoi);
			db.AddInParameter(dbCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, MaHQDangKyMoi);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_SXXK_PhuKienDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_PhuKienDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_SXXK_PhuKienDetail(long id, long master_ID, DateTime ngayTKHeThong, DateTime ngayTKSuaDoi, string soHopDongCu, string soHopDongMoi, DateTime ngayHopDongCu, DateTime ngayHopDongMoi, int sTTHang, string maHangCu, string loaiHangCu, decimal soLuongCu, string dVTCu, string maHQDangKyCu, string maHangMoi, string loaiHangMoi, decimal soLuongMoi, string dVTMoi, string maHQDangKyMoi)
		{
			KDT_SXXK_PhuKienDetail entity = new KDT_SXXK_PhuKienDetail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.NgayTKHeThong = ngayTKHeThong;
			entity.NgayTKSuaDoi = ngayTKSuaDoi;
			entity.SoHopDongCu = soHopDongCu;
			entity.SoHopDongMoi = soHopDongMoi;
			entity.NgayHopDongCu = ngayHopDongCu;
			entity.NgayHopDongMoi = ngayHopDongMoi;
			entity.STTHang = sTTHang;
			entity.MaHangCu = maHangCu;
			entity.LoaiHangCu = loaiHangCu;
			entity.SoLuongCu = soLuongCu;
			entity.DVTCu = dVTCu;
			entity.MaHQDangKyCu = maHQDangKyCu;
			entity.MaHangMoi = maHangMoi;
			entity.LoaiHangMoi = loaiHangMoi;
			entity.SoLuongMoi = soLuongMoi;
			entity.DVTMoi = dVTMoi;
			entity.MaHQDangKyMoi = maHQDangKyMoi;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_PhuKienDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NgayTKHeThong", SqlDbType.DateTime, NgayTKHeThong.Year <= 1753 ? DBNull.Value : (object) NgayTKHeThong);
			db.AddInParameter(dbCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, NgayTKSuaDoi.Year <= 1753 ? DBNull.Value : (object) NgayTKSuaDoi);
			db.AddInParameter(dbCommand, "@SoHopDongCu", SqlDbType.VarChar, SoHopDongCu);
			db.AddInParameter(dbCommand, "@SoHopDongMoi", SqlDbType.VarChar, SoHopDongMoi);
			db.AddInParameter(dbCommand, "@NgayHopDongCu", SqlDbType.DateTime, NgayHopDongCu.Year <= 1753 ? DBNull.Value : (object) NgayHopDongCu);
			db.AddInParameter(dbCommand, "@NgayHopDongMoi", SqlDbType.DateTime, NgayHopDongMoi.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMoi);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@MaHangCu", SqlDbType.VarChar, MaHangCu);
			db.AddInParameter(dbCommand, "@LoaiHangCu", SqlDbType.VarChar, LoaiHangCu);
			db.AddInParameter(dbCommand, "@SoLuongCu", SqlDbType.Decimal, SoLuongCu);
			db.AddInParameter(dbCommand, "@DVTCu", SqlDbType.VarChar, DVTCu);
			db.AddInParameter(dbCommand, "@MaHQDangKyCu", SqlDbType.VarChar, MaHQDangKyCu);
			db.AddInParameter(dbCommand, "@MaHangMoi", SqlDbType.VarChar, MaHangMoi);
			db.AddInParameter(dbCommand, "@LoaiHangMoi", SqlDbType.VarChar, LoaiHangMoi);
			db.AddInParameter(dbCommand, "@SoLuongMoi", SqlDbType.Decimal, SoLuongMoi);
			db.AddInParameter(dbCommand, "@DVTMoi", SqlDbType.VarChar, DVTMoi);
			db.AddInParameter(dbCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, MaHQDangKyMoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_SXXK_PhuKienDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_PhuKienDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_SXXK_PhuKienDetail(long id, long master_ID, DateTime ngayTKHeThong, DateTime ngayTKSuaDoi, string soHopDongCu, string soHopDongMoi, DateTime ngayHopDongCu, DateTime ngayHopDongMoi, int sTTHang, string maHangCu, string loaiHangCu, decimal soLuongCu, string dVTCu, string maHQDangKyCu, string maHangMoi, string loaiHangMoi, decimal soLuongMoi, string dVTMoi, string maHQDangKyMoi)
		{
			KDT_SXXK_PhuKienDetail entity = new KDT_SXXK_PhuKienDetail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.NgayTKHeThong = ngayTKHeThong;
			entity.NgayTKSuaDoi = ngayTKSuaDoi;
			entity.SoHopDongCu = soHopDongCu;
			entity.SoHopDongMoi = soHopDongMoi;
			entity.NgayHopDongCu = ngayHopDongCu;
			entity.NgayHopDongMoi = ngayHopDongMoi;
			entity.STTHang = sTTHang;
			entity.MaHangCu = maHangCu;
			entity.LoaiHangCu = loaiHangCu;
			entity.SoLuongCu = soLuongCu;
			entity.DVTCu = dVTCu;
			entity.MaHQDangKyCu = maHQDangKyCu;
			entity.MaHangMoi = maHangMoi;
			entity.LoaiHangMoi = loaiHangMoi;
			entity.SoLuongMoi = soLuongMoi;
			entity.DVTMoi = dVTMoi;
			entity.MaHQDangKyMoi = maHQDangKyMoi;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@NgayTKHeThong", SqlDbType.DateTime, NgayTKHeThong.Year <= 1753 ? DBNull.Value : (object) NgayTKHeThong);
			db.AddInParameter(dbCommand, "@NgayTKSuaDoi", SqlDbType.DateTime, NgayTKSuaDoi.Year <= 1753 ? DBNull.Value : (object) NgayTKSuaDoi);
			db.AddInParameter(dbCommand, "@SoHopDongCu", SqlDbType.VarChar, SoHopDongCu);
			db.AddInParameter(dbCommand, "@SoHopDongMoi", SqlDbType.VarChar, SoHopDongMoi);
			db.AddInParameter(dbCommand, "@NgayHopDongCu", SqlDbType.DateTime, NgayHopDongCu.Year <= 1753 ? DBNull.Value : (object) NgayHopDongCu);
			db.AddInParameter(dbCommand, "@NgayHopDongMoi", SqlDbType.DateTime, NgayHopDongMoi.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMoi);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@MaHangCu", SqlDbType.VarChar, MaHangCu);
			db.AddInParameter(dbCommand, "@LoaiHangCu", SqlDbType.VarChar, LoaiHangCu);
			db.AddInParameter(dbCommand, "@SoLuongCu", SqlDbType.Decimal, SoLuongCu);
			db.AddInParameter(dbCommand, "@DVTCu", SqlDbType.VarChar, DVTCu);
			db.AddInParameter(dbCommand, "@MaHQDangKyCu", SqlDbType.VarChar, MaHQDangKyCu);
			db.AddInParameter(dbCommand, "@MaHangMoi", SqlDbType.VarChar, MaHangMoi);
			db.AddInParameter(dbCommand, "@LoaiHangMoi", SqlDbType.VarChar, LoaiHangMoi);
			db.AddInParameter(dbCommand, "@SoLuongMoi", SqlDbType.Decimal, SoLuongMoi);
			db.AddInParameter(dbCommand, "@DVTMoi", SqlDbType.VarChar, DVTMoi);
			db.AddInParameter(dbCommand, "@MaHQDangKyMoi", SqlDbType.VarChar, MaHQDangKyMoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_SXXK_PhuKienDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_PhuKienDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_SXXK_PhuKienDetail(long id)
		{
			KDT_SXXK_PhuKienDetail entity = new KDT_SXXK_PhuKienDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_SXXK_PhuKienDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_PhuKienDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}