﻿
using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
//Kiểm tra interface của project LanNT
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.Interface.GC;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages.Send;
namespace Company.Interface
{
    public partial class HangGCCTForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        //public List<HangChuyenTiep> HMDCollection = new List<HangChuyenTiep>();
        public string LoaiHangHoa;
        public ToKhaiChuyenTiep TKCT;
        public bool isByHand = false;
        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private decimal st_clg;
        private HangChuyenTiep hangChuyenTiep = null;
        public decimal TyGiaTT;
        private decimal luong;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;


        //-----------------------------------------------------------------------------------------
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;

        private decimal LuongCon = 0;

        //-------------------------------------------------------------------------------------------

        public HangGCCTForm()
        {
            InitializeComponent();
        }
        public HangGCCTForm(HangChuyenTiep hangchuyentiep)
            : this()
        {
            hangChuyenTiep = hangchuyentiep;
        }

        private void khoitao_DuLieuChuan()
        {
            cbLoaiCo.DataSource = LoaiCO.SelectAll().Tables[0];
            cbLoaiCo.DisplayMember = "Ten";
            cbLoaiCo.ValueMember = "Ma";
            cbLoaiCo.SelectedIndex = 0;
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtTGNT.DecimalDigits = GlobalSettings.TriGiaNT;

        }

        private bool checkMaHangExit(string maHang)
        {
            //foreach (HangChuyenTiep hmd in TKCT.HCTCollection)
            //{
            //    if (hmd.MaHang == maHang) return true;
            //}
            //return false;
            return true;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();

                if (!cvError.IsValid) return;
                epError.Clear();
                if (string.IsNullOrEmpty(ctrNuocXX.Ma.Trim()))
                {
                    epError.SetError(ctrNuocXX, "Không được để trống nước xuất xứ");
                    return;
                }
                if (string.IsNullOrEmpty(cbDonViTinh.Text))
                {
                    epError.SetError(ctrNuocXX, "Không được để trống đơn vị tính");
                    return;
                }
                txtMaHang.Focus();

                bool isAddNew = false;
                if (hangChuyenTiep == null || hangChuyenTiep.ID == 0)
                {
                    hangChuyenTiep = new HangChuyenTiep();
                    hangChuyenTiep.SoThuTuHang = 0;
                    isAddNew = true;
                }
                GetHang(hangChuyenTiep);
                if (isAddNew)
                    TKCT.HCTCollection.Add(hangChuyenTiep);

                this.refresh_STTHang();
                dgList.DataSource = TKCT.HCTCollection;
                dgList.Refetch();
                btnAddNew_Click(null, null);

            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value"),
                    TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }
        private void refresh_STTHang()
        {
            int i = 1;
            foreach (HangChuyenTiep hct in TKCT.HCTCollection)
            {
                hct.SoThuTuHang = i++;
            }
        }
        private void GetHang(HangChuyenTiep hct)
        {

            hct.MaHS = txtMaHS.Text;
            hct.MaHang = txtMaHang.Text.Trim();
            hct.TenHang = txtTenHang.Text.Trim();
            hct.ID_NuocXX = ctrNuocXX.Ma;
            hct.ID_DVT = cbDonViTinh.SelectedValue.ToString();
            hct.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hct.DonGia = Convert.ToDecimal(txtDGNT.Value);
            hct.DonGiaTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
            hct.TriGia = Convert.ToDecimal(txtTGNT.Value);
            hct.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            hct.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hct.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            hct.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            hct.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            hct.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            hct.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
            hct.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
            hct.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
            hct.DonGiaTT = hct.TriGiaTT / Convert.ToDecimal(hct.SoLuong);
            hct.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
            hct.NoCo = chNoCo.Checked;
            hct.ThueTuyetDoi = chkThueTuyetDoi.Checked;
            hct.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
            hct.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
            hct.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
            hct.TyLeThuKhac = Convert.ToDecimal(txtTyLeThuKhac.Value);
            hct.PhuThu = (txtPhuThu.Text != "" ? Convert.ToDecimal(txtPhuThu.Text) : 0);
            if (chkMienThue.Checked)
                hct.MienThue = 1;
            else
                hct.MienThue = 0;
            #region Thêm Mới V3
            hct.MaHSMoRong = txtMaHSMoRong.Text;
            hct.NhanHieu = txtNhanHieu.Text;
            hct.QuyCachPhamChat = txtQuyCach.Text;
            hct.ThanhPhan = txtThanhPhan.Text;
            hct.Model = txtModel.Text;
            hct.TenHangSX = txtTenHangSX.Text;
            hct.MaHangSX = txtMaHangSX.Text;
            hct.MaLoaiCo = cbLoaiCo.SelectedValue != null ? cbLoaiCo.SelectedValue.ToString() : string.Empty;
            #endregion
        }
        private void SetHang(HangChuyenTiep hct)
        {

            txtMaHang.Text = hct.MaHang;
            txtTenHang.Text = hct.TenHang;
            txtMaHS.Text = hct.MaHS;
            cbDonViTinh.SelectedValue = hct.ID_DVT;

            #region Thêm Mới V3
            txtMaHSMoRong.Text = hct.MaHSMoRong;
            txtMaHangSX.Text = hct.MaHangSX;
            txtTenHangSX.Text = hct.TenHangSX;
            txtNhanHieu.Text = hct.NhanHieu;
            txtQuyCach.Text = hct.QuyCachPhamChat;
            txtThanhPhan.Text = hct.ThanhPhan;
            txtModel.Text = hct.Model;
            #endregion

            chNoCo.Checked = hct.NoCo;
            chkThueTuyetDoi.Checked = hct.ThueTuyetDoi;
            txtLuong.Value = hct.SoLuong;
            txtDGNT.Value = hct.DonGia;
            txtDonGiaTuyetDoi.Value = hct.DonGiaTuyetDoi;
            txtTGNT.Value = hct.TriGia;
            txtTriGiaKB.Value = hct.TriGiaKB_VND;
            ctrNuocXX.Ma = hct.ID_NuocXX;

            txtTGTT_NK.Value = hct.TriGiaTT;
            txtTS_NK.Value = hct.ThueSuatXNK;
            txtTS_TTDB.Value = hct.ThueSuatTTDB;
            txtTS_GTGT.Value = hct.ThueSuatGTGT;
            txtTL_CLG.Value = hct.TyLeThuKhac;
            txtTienThue_NK.Value = hct.ThueXNK;
            txtTienThue_TTDB.Value = hct.ThueTTDB;
            txtTienThue_GTGT.Value = hct.ThueGTGT;
            txtTien_CLG.Value = hct.TriGiaThuKhac;
            txtTSXNKGiam.Text = hct.ThueSuatXNKGiam;
            txtTSTTDBGiam.Text = hct.ThueSuatTTDBGiam;
            txtTSVatGiam.Text = hct.ThueSuatVATGiam;
            txtTyLeThuKhac.Value = hct.TyLeThuKhac;
            txtPhuThu.Value = hct.PhuThu;
            chkMienThue.Checked = hct.MienThue == 1 ? true : false;
            cbLoaiCo.SelectedValue = hct.MaLoaiCo;

        }

        private void reSetError()
        {
            epError.SetError(txtMaHang, null);
            epError.SetError(txtTenHang, null);
            epError.SetError(txtMaHS, null);
        }
        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {

            reSetError();
            bool isNotFound = false;
            if (this.TKCT.LoaiHangHoa == "N")
            {

                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                //if (NhomLoaiHinh.StartsWith("N"))
                npl.HopDong_ID = TKCT.IDHopDong;
                //else
                //    npl.HopDong_ID = TKCT.ID_Relation;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKCT.LoaiHangHoa == "S")
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                //if (NhomLoaiHinh.StartsWith("N"))
                sp.HopDong_ID = TKCT.IDHopDong;
                //else
                //    sp.HopDong_ID = TKCT.ID_Relation;
                sp.Ma = txtMaHang.Text.Trim();

                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKCT.LoaiHangHoa == "T")
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                //if (NhomLoaiHinh.StartsWith("N"))
                    tb.HopDong_ID = TKCT.IDHopDong;
                //else
                //    tb.HopDong_ID = TKCT.ID_Relation;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    txtMaHang.Text = tb.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    cbDonViTinh.SelectedValue = tb.DVT_ID;
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKCT.LoaiHangHoa == "H")
            {
                Company.GC.BLL.GC.HangMau hangmau = null;
                //if (NhomLoaiHinh.StartsWith("N"))
                hangmau = Company.GC.BLL.GC.HangMau.Load(TKCT.IDHopDong, txtMaHang.Text.Trim());
                //else
                //    hangmau = Company.GC.BLL.GC.HangMau.Load(TKCT.ID_Relation, txtMaHang.Text.Trim());
                if (hangmau != null)
                {
                    txtMaHang.Text = hangmau.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = hangmau.MaHS;
                    txtTenHang.Text = hangmau.Ten;
                    cbDonViTinh.SelectedValue = hangmau.DVT_ID;
                }
                else isNotFound = true;

            }
            if (isNotFound)
            {
                epError.SetError(txtMaHang, setText("Không tồn tại mã này.", "This value is not exist"));
                txtTenHang.Text = txtMaHS.Text = string.Empty;
                cbDonViTinh.SelectedValue = "";
                return;
            }
        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            if (NhomLoaiHinh.Substring(0, 1) != "N")
            {
                chNoCo.Visible = label38.Visible = cbLoaiCo.Visible = false;
            }
            bool isActive = TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isByHand) isActive = true;
            btnXoa.Enabled = btnGhi.Enabled = isActive;

            if (hangChuyenTiep != null)
                SetHang(hangChuyenTiep);

            if (btnXoa.Enabled)
                this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            else this.dgList.AllowDelete = InheritableBoolean.False;


            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            if (this.NhomLoaiHinh[0].ToString().Equals("X"))
                label16.Text = setText("Trị giá tính thuế XK", "Export Tax assessment");


            dgList.DataSource = TKCT.HCTCollection;
            this.refresh_STTHang();

            lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");

            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                btnGhi.Visible = false;
            txtMaHang.Focus();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["ID_NuocXX"].Value != null)
                e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Value.ToString());
            if (e.Row.Cells["ID_DVT"].Value != null)
                e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Value);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            foreach (System.Windows.Forms.Control item in grbThue.Controls)
            {
                if (item.Name != "chkMienThue" && item.GetType() != typeof(System.Windows.Forms.Label))
                    item.Enabled = !chkMienThue.Checked;
            }
            chkMienThue.Enabled = true;

            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.NhomLoaiHinh)) return;
            switch (TKCT.LoaiHangHoa)
            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    //if (NhomLoaiHinh.StartsWith("N"))
                    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKCT.IDHopDong;
                    //else
                    //    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKCT.ID_Relation;
                    this.NPLRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "S":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    //if (NhomLoaiHinh.StartsWith("N"))
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKCT.IDHopDong;
                    //else this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKCT.ID_Relation;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "T":
                    ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                    //if (NhomLoaiHinh.StartsWith("N"))
                    ftb.ThietBiSelected.HopDong_ID = TKCT.IDHopDong;
                    //else
                    //    ftb.ThietBiSelected.HopDong_ID = TKCT.ID_Relation;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                    }
                    break;
                case "H":
                    HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                    hangmauForm.isBrower = true;
                    hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    //if (NhomLoaiHinh.StartsWith("N"))
                    hangmauForm.HangMauSelected.HopDong_ID = TKCT.IDHopDong;
                    //else hangmauForm.HangMauSelected.HopDong_ID = TKCT.ID_Relation;
                    hangmauForm.ShowDialog();
                    if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                    {
                        txtMaHang.Text = hangmauForm.HangMauSelected.Ma;
                        txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        txtMaHS.Text = hangmauForm.HangMauSelected.MaHS;
                        cbDonViTinh.SelectedValue = hangmauForm.HangMauSelected.DVT_ID.PadRight(3);
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            epError.Clear();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            hangChuyenTiep = (HangChuyenTiep)items[0].GetRow().DataRow;
            SetHang(hangChuyenTiep);
        }


        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    HangChuyenTiep hmd = (HangChuyenTiep)e.Row.DataRow;
                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangChuyenTiep hct = (HangChuyenTiep)i.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hct.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hct.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }

                            hct.Delete();
                        }
                    }
                }
                refresh_STTHang();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangChuyenTiep hct = (HangChuyenTiep)dgList.GetRow().DataRow;
            SetHang(hct);
            txtLuong.Focus();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            txtPhuThu.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);

        }
        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            txtPhuThu.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);
        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //     txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS,setText("Mã HS không hợp lệ.","Invalid input value"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
            //string MoTa = MaHS.CheckExist(txtMaHS.Text);
            //if (MoTa == "")
            //{
            //    txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.","This HS is not exist"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            List<HangChuyenTiep> hctColl = new List<HangChuyenTiep>();
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        hctColl.Add((HangChuyenTiep)i.GetRow().DataRow);
                    }

                }
                foreach (HangChuyenTiep hct in hctColl)
                {
                    try
                    {
                        if (hct.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hct.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hct.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                return;
                            }

                            hct.Delete();
                        }
                        try
                        {
                            TKCT.HCTCollection.Remove(hct);
                        }
                        catch { }
                    }
                    catch { }
                }
            }
            refresh_STTHang();
            dgList.DataSource = TKCT.HCTCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkThueTuyetDoi.Checked)
            {
                txtTS_NK.Enabled = false;
                txtTienThue_NK.ReadOnly = false;
                txtDonGiaTuyetDoi.Enabled = true;

                this.txtTienThue_NK.BackColor = Color.White;
            }
            else
            {
                txtTS_NK.Enabled = true;
                txtTienThue_NK.ReadOnly = true;
                txtDonGiaTuyetDoi.Enabled = false;
                this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            }
        }


        private void txtTGNT_Leave(object sender, EventArgs e)
        {

        }

        private void lblTyGiaTT_Click(object sender, EventArgs e)
        {

        }

        private void txtLuong_Click(object sender, EventArgs e)
        {

        }

        private void txtPhuThu_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;
            txtTyLeThuKhac.Value = Convert.ToDecimal(txtPhuThu.Value) / (Convert.ToDecimal(txtTGTT_NK.Value) / 100);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            hangChuyenTiep = new HangChuyenTiep();
            hangChuyenTiep.SoThuTuHang = 0;
            SetHang(hangChuyenTiep);
            hangChuyenTiep = null;
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            if (dgnt == 0) return;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }
        private decimal tinhthue()
        {
            luong = Convert.ToDecimal(txtLuong.Value);
            if (luong == 0) return 0;
            this.tgnt = Convert.ToDecimal(txtTGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
            txtDGNT.Text = Helpers.Format(tgnt / luong, GlobalSettings.TriGiaNT);

            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                decimal dgntTuyetDoi = Convert.ToDecimal(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDecimal(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;

            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue3()
        {
            luong = Convert.ToDecimal(txtLuong.Value);
            if (luong == 0) return 0;
            //this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgnt = Convert.ToDecimal(txtTGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
            txtDGNT.Text = Helpers.Format(tgnt / luong, GlobalSettings.TriGiaNT);
            dgnt = Convert.ToDecimal(txtDGNT.Value);
            decimal tgntkophi = tgnt;

            this.tgtt_nk = tgntkophi * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue2()
        {
            this.dgnt = (Convert.ToDecimal(txtDGNT.Text));
            this.luong = Convert.ToDecimal(txtLuong.Value);
            if (luong == 0) return 0;
            if (!chkThueTuyetDoi.Checked)
            {
                this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            }

            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = Convert.ToDecimal(this.dgnt) * (this.luong);

            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                decimal dgntTuyetDoi = Convert.ToDecimal(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDecimal(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);

            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTienThue_NK_Leave(object sender, EventArgs e)
        {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTL_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

    }
}
