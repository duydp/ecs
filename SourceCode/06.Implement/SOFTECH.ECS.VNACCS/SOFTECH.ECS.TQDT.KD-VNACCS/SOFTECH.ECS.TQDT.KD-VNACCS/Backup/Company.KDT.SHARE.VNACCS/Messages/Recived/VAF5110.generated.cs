using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF5110 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute BRC { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute UBP { get; set; }
public PropertiesAttribute A04 { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute BPS { get; set; }
public PropertiesAttribute BPN { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A10 { get; set; }
public PropertiesAttribute A11 { get; set; }
public PropertiesAttribute A12 { get; set; }
public PropertiesAttribute CCC { get; set; }
public PropertiesAttribute A14 { get; set; }

public VAF5110()
        {
BRC = new PropertiesAttribute(11, typeof(string));
A02 = new PropertiesAttribute(210, typeof(string));
UBP = new PropertiesAttribute(13, typeof(string));
A04 = new PropertiesAttribute(300, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
BPS = new PropertiesAttribute(10, typeof(string));
BPN = new PropertiesAttribute(10, typeof(string));
A08 = new PropertiesAttribute(1, typeof(string));
A09 = new PropertiesAttribute(8, typeof(DateTime));
A10 = new PropertiesAttribute(8, typeof(DateTime));
A11 = new PropertiesAttribute(13, typeof(int));
A12 = new PropertiesAttribute(13, typeof(int));
CCC = new PropertiesAttribute(3, typeof(string));
A14 = new PropertiesAttribute(10, typeof(int));
TongSoByte = 642;
}

}public partial class EnumGroupID
    {
}

}
