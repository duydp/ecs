
-------------------------------------------- STORE

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]    Script Date: 11/11/2013 17:33:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HoaDon_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_GiayPhep_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]    Script Date: 11/11/2013 17:33:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoContainer_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]    Script Date: 11/11/2013 17:33:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]    Script Date: 11/11/2013 17:33:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]
GO



/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten varchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
(
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@LoaiThongTin,
	@Master_ID,
	@PhanLoai,
	@Ngay,
	@Ten,
	@NoiDung,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten varchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
		SET
			[LoaiThongTin] = @LoaiThongTin,
			[Master_ID] = @Master_ID,
			[PhanLoai] = @PhanLoai,
			[Ngay] = @Ngay,
			[Ten] = @Ten,
			[NoiDung] = @NoiDung,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
		(
			[LoaiThongTin],
			[Master_ID],
			[PhanLoai],
			[Ngay],
			[Ten],
			[NoiDung],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@LoaiThongTin,
			@Master_ID,
			@PhanLoai,
			@Ngay,
			@Ten,
			@NoiDung,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten varchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
SET
	[LoaiThongTin] = @LoaiThongTin,
	[Master_ID] = @Master_ID,
	[PhanLoai] = @PhanLoai,
	[Ngay] = @Ngay,
	[Ten] = @Ten,
	[NoiDung] = @NoiDung,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Insert]
	@ChungTuKemID bigint,
	@FileName nvarchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
(
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
)
VALUES 
(
	@ChungTuKemID,
	@FileName,
	@FileSize,
	@NoiDung
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_InsertUpdate]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName nvarchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] 
		SET
			[ChungTuKemID] = @ChungTuKemID,
			[FileName] = @FileName,
			[FileSize] = @FileSize,
			[NoiDung] = @NoiDung
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
		(
			[ChungTuKemID],
			[FileName],
			[FileSize],
			[NoiDung]
		)
		VALUES 
		(
			@ChungTuKemID,
			@FileName,
			@FileSize,
			@NoiDung
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_ChiTiet_Update]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName nvarchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]
SET
	[ChungTuKemID] = @ChungTuKemID,
	[FileName] = @FileName,
	[FileSize] = @FileSize,
	[NoiDung] = @NoiDung
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
(
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
)
VALUES 
(
	@LoaiChungTu,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@TieuDe,
	@SoToKhai,
	@GhiChu,
	@PhanLoaiThuTucKhaiBao,
	@SoDienThoaiNguoiKhaiBao,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@MaKetQuaXuLy,
	@TenThuTucKhaiBao,
	@NgayKhaiBao,
	@TrangThaiKhaiBao,
	@NgaySuaCuoiCung,
	@TenNguoiKhaiBao,
	@DiaChiNguoiKhaiBao,
	@SoDeLayTepDinhKem,
	@NgayHoanThanhKiemTraHoSo,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TongDungLuong,
	@PhanLuong,
	@HuongDan,
	@TKMD_ID,
	@NgayBatDau,
	@NgayCapNhatCuoiCung,
	@CanCuPhapLenh,
	@CoBaoXoa,
	@GhiChuHaiQuan,
	@HinhThucTimKiem,
	@LyDoChinhSua,
	@MaDiaDiemDen,
	@MaNhaVanChuyen,
	@MaPhanLoaiDangKy,
	@MaPhanLoaiXuLy,
	@MaThongTinXuat,
	@NgayCapPhep,
	@NgayThongBao,
	@NguoiGui,
	@NoiDungChinhSua,
	@PhuongTienVanChuyen,
	@SoChuyenDiBien,
	@SoSeri,
	@SoTiepNhanToKhaiPhoThong,
	@TrangThaiXuLyHaiQuan,
	@NguoiCapNhatCuoiCung,
	@NhomXuLyHoSoID
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]    Script Date: 11/11/2013 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[TieuDe] = @TieuDe,
			[SoToKhai] = @SoToKhai,
			[GhiChu] = @GhiChu,
			[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
			[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
			[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
			[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
			[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
			[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
			[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TongDungLuong] = @TongDungLuong,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[TKMD_ID] = @TKMD_ID,
			[NgayBatDau] = @NgayBatDau,
			[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
			[CanCuPhapLenh] = @CanCuPhapLenh,
			[CoBaoXoa] = @CoBaoXoa,
			[GhiChuHaiQuan] = @GhiChuHaiQuan,
			[HinhThucTimKiem] = @HinhThucTimKiem,
			[LyDoChinhSua] = @LyDoChinhSua,
			[MaDiaDiemDen] = @MaDiaDiemDen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaThongTinXuat] = @MaThongTinXuat,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayThongBao] = @NgayThongBao,
			[NguoiGui] = @NguoiGui,
			[NoiDungChinhSua] = @NoiDungChinhSua,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[SoChuyenDiBien] = @SoChuyenDiBien,
			[SoSeri] = @SoSeri,
			[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
			[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
			[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
		(
			[LoaiChungTu],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[TieuDe],
			[SoToKhai],
			[GhiChu],
			[PhanLoaiThuTucKhaiBao],
			[SoDienThoaiNguoiKhaiBao],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[MaKetQuaXuLy],
			[TenThuTucKhaiBao],
			[NgayKhaiBao],
			[TrangThaiKhaiBao],
			[NgaySuaCuoiCung],
			[TenNguoiKhaiBao],
			[DiaChiNguoiKhaiBao],
			[SoDeLayTepDinhKem],
			[NgayHoanThanhKiemTraHoSo],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TongDungLuong],
			[PhanLuong],
			[HuongDan],
			[TKMD_ID],
			[NgayBatDau],
			[NgayCapNhatCuoiCung],
			[CanCuPhapLenh],
			[CoBaoXoa],
			[GhiChuHaiQuan],
			[HinhThucTimKiem],
			[LyDoChinhSua],
			[MaDiaDiemDen],
			[MaNhaVanChuyen],
			[MaPhanLoaiDangKy],
			[MaPhanLoaiXuLy],
			[MaThongTinXuat],
			[NgayCapPhep],
			[NgayThongBao],
			[NguoiGui],
			[NoiDungChinhSua],
			[PhuongTienVanChuyen],
			[SoChuyenDiBien],
			[SoSeri],
			[SoTiepNhanToKhaiPhoThong],
			[TrangThaiXuLyHaiQuan],
			[NguoiCapNhatCuoiCung],
			[NhomXuLyHoSoID]
		)
		VALUES 
		(
			@LoaiChungTu,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@TieuDe,
			@SoToKhai,
			@GhiChu,
			@PhanLoaiThuTucKhaiBao,
			@SoDienThoaiNguoiKhaiBao,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@MaKetQuaXuLy,
			@TenThuTucKhaiBao,
			@NgayKhaiBao,
			@TrangThaiKhaiBao,
			@NgaySuaCuoiCung,
			@TenNguoiKhaiBao,
			@DiaChiNguoiKhaiBao,
			@SoDeLayTepDinhKem,
			@NgayHoanThanhKiemTraHoSo,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TongDungLuong,
			@PhanLuong,
			@HuongDan,
			@TKMD_ID,
			@NgayBatDau,
			@NgayCapNhatCuoiCung,
			@CanCuPhapLenh,
			@CoBaoXoa,
			@GhiChuHaiQuan,
			@HinhThucTimKiem,
			@LyDoChinhSua,
			@MaDiaDiemDen,
			@MaNhaVanChuyen,
			@MaPhanLoaiDangKy,
			@MaPhanLoaiXuLy,
			@MaThongTinXuat,
			@NgayCapPhep,
			@NgayThongBao,
			@NguoiGui,
			@NoiDungChinhSua,
			@PhuongTienVanChuyen,
			@SoChuyenDiBien,
			@SoSeri,
			@SoTiepNhanToKhaiPhoThong,
			@TrangThaiXuLyHaiQuan,
			@NguoiCapNhatCuoiCung,
			@NhomXuLyHoSoID
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(11, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 0),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(11, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[TieuDe] = @TieuDe,
	[SoToKhai] = @SoToKhai,
	[GhiChu] = @GhiChu,
	[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
	[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
	[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
	[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
	[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
	[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
	[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TongDungLuong] = @TongDungLuong,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[TKMD_ID] = @TKMD_ID,
	[NgayBatDau] = @NgayBatDau,
	[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
	[CanCuPhapLenh] = @CanCuPhapLenh,
	[CoBaoXoa] = @CoBaoXoa,
	[GhiChuHaiQuan] = @GhiChuHaiQuan,
	[HinhThucTimKiem] = @HinhThucTimKiem,
	[LyDoChinhSua] = @LyDoChinhSua,
	[MaDiaDiemDen] = @MaDiaDiemDen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaThongTinXuat] = @MaThongTinXuat,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayThongBao] = @NgayThongBao,
	[NguoiGui] = @NguoiGui,
	[NoiDungChinhSua] = @NoiDungChinhSua,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[SoChuyenDiBien] = @SoChuyenDiBien,
	[SoSeri] = @SoSeri,
	[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
	[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
	[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteBy_ChungTuThanhToan_ID]
	@ChungTuThanhToan_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Insert]
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
(
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
)
VALUES 
(
	@ChungTuThanhToan_ID,
	@SoThuTuGiaoDich,
	@NgayTaoDuLieu,
	@ThoiGianTaoDuLieu,
	@SoTienTruLui,
	@SoTienTangLen,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaLoaiHinh,
	@CoQuanHaiQuan,
	@TenCucHaiQuan
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_InsertUpdate]
	@ID bigint,
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] 
		SET
			[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID,
			[SoThuTuGiaoDich] = @SoThuTuGiaoDich,
			[NgayTaoDuLieu] = @NgayTaoDuLieu,
			[ThoiGianTaoDuLieu] = @ThoiGianTaoDuLieu,
			[SoTienTruLui] = @SoTienTruLui,
			[SoTienTangLen] = @SoTienTangLen,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[TenCucHaiQuan] = @TenCucHaiQuan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
		(
			[ChungTuThanhToan_ID],
			[SoThuTuGiaoDich],
			[NgayTaoDuLieu],
			[ThoiGianTaoDuLieu],
			[SoTienTruLui],
			[SoTienTangLen],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaLoaiHinh],
			[CoQuanHaiQuan],
			[TenCucHaiQuan]
		)
		VALUES 
		(
			@ChungTuThanhToan_ID,
			@SoThuTuGiaoDich,
			@NgayTaoDuLieu,
			@ThoiGianTaoDuLieu,
			@SoTienTruLui,
			@SoTienTangLen,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaLoaiHinh,
			@CoQuanHaiQuan,
			@TenCucHaiQuan
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectBy_ChungTuThanhToan_ID]
	@ChungTuThanhToan_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
WHERE
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuThanhToan_ID],
	[SoThuTuGiaoDich],
	[NgayTaoDuLieu],
	[ThoiGianTaoDuLieu],
	[SoTienTruLui],
	[SoTienTangLen],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan]
FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_ChiTiet_Update]
	@ID bigint,
	@ChungTuThanhToan_ID bigint,
	@SoThuTuGiaoDich numeric(4, 0),
	@NgayTaoDuLieu datetime,
	@ThoiGianTaoDuLieu datetime,
	@SoTienTruLui numeric(13, 0),
	@SoTienTangLen numeric(13, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]
SET
	[ChungTuThanhToan_ID] = @ChungTuThanhToan_ID,
	[SoThuTuGiaoDich] = @SoThuTuGiaoDich,
	[NgayTaoDuLieu] = @NgayTaoDuLieu,
	[ThoiGianTaoDuLieu] = @ThoiGianTaoDuLieu,
	[SoTienTruLui] = @SoTienTruLui,
	[SoTienTangLen] = @SoTienTangLen,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[TenCucHaiQuan] = @TenCucHaiQuan
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan]
(
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy]
)
VALUES 
(
	@LoaiChungTu,
	@LoaiHinhBaoLanh,
	@MaNganHangCungCapBaoLanh,
	@NamPhatHanh,
	@KiHieuChungTuPhatHanhBaoLanh,
	@SoChungTuPhatHanhBaoLanh,
	@MaTienTe,
	@MaNganHangTraThay,
	@TenNganHangTraThay,
	@MaDonViSuDungHanMuc,
	@TenDonViSuDungHanMuc,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@MaKetQuaXuLy,
	@TenNganHangCungCapBaoLanh,
	@SoToKhai,
	@NgayDuDinhKhaiBao,
	@MaLoaiHinh,
	@CoQuanHaiQuan,
	@TenCucHaiQuan,
	@SoVanDon_1,
	@SoVanDon_2,
	@SoVanDon_3,
	@SoVanDon_4,
	@SoVanDon_5,
	@SoHoaDon,
	@MaDonViSuDungBaoLanh,
	@TenDonViSuDungBaoLanh,
	@MaDonViDaiDienSuDungBaoLanh,
	@TenDonViDaiDienSuDungBaoLanh,
	@NgayBatDauHieuLuc,
	@NgayHetHieuLuc,
	@SoTienDangKyBaoLanh,
	@CoBaoVoHieu,
	@SoDuBaoLanh,
	@SoTienHanMucDangKi,
	@SoDuHanMuc,
	@SoThuTuHanMuc,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]    Script Date: 11/11/2013 17:33:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuThanhToan] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[LoaiHinhBaoLanh] = @LoaiHinhBaoLanh,
			[MaNganHangCungCapBaoLanh] = @MaNganHangCungCapBaoLanh,
			[NamPhatHanh] = @NamPhatHanh,
			[KiHieuChungTuPhatHanhBaoLanh] = @KiHieuChungTuPhatHanhBaoLanh,
			[SoChungTuPhatHanhBaoLanh] = @SoChungTuPhatHanhBaoLanh,
			[MaTienTe] = @MaTienTe,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaDonViSuDungHanMuc] = @MaDonViSuDungHanMuc,
			[TenDonViSuDungHanMuc] = @TenDonViSuDungHanMuc,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenNganHangCungCapBaoLanh] = @TenNganHangCungCapBaoLanh,
			[SoToKhai] = @SoToKhai,
			[NgayDuDinhKhaiBao] = @NgayDuDinhKhaiBao,
			[MaLoaiHinh] = @MaLoaiHinh,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[TenCucHaiQuan] = @TenCucHaiQuan,
			[SoVanDon_1] = @SoVanDon_1,
			[SoVanDon_2] = @SoVanDon_2,
			[SoVanDon_3] = @SoVanDon_3,
			[SoVanDon_4] = @SoVanDon_4,
			[SoVanDon_5] = @SoVanDon_5,
			[SoHoaDon] = @SoHoaDon,
			[MaDonViSuDungBaoLanh] = @MaDonViSuDungBaoLanh,
			[TenDonViSuDungBaoLanh] = @TenDonViSuDungBaoLanh,
			[MaDonViDaiDienSuDungBaoLanh] = @MaDonViDaiDienSuDungBaoLanh,
			[TenDonViDaiDienSuDungBaoLanh] = @TenDonViDaiDienSuDungBaoLanh,
			[NgayBatDauHieuLuc] = @NgayBatDauHieuLuc,
			[NgayHetHieuLuc] = @NgayHetHieuLuc,
			[SoTienDangKyBaoLanh] = @SoTienDangKyBaoLanh,
			[CoBaoVoHieu] = @CoBaoVoHieu,
			[SoDuBaoLanh] = @SoDuBaoLanh,
			[SoTienHanMucDangKi] = @SoTienHanMucDangKi,
			[SoDuHanMuc] = @SoDuHanMuc,
			[SoThuTuHanMuc] = @SoThuTuHanMuc,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan]
		(
			[LoaiChungTu],
			[LoaiHinhBaoLanh],
			[MaNganHangCungCapBaoLanh],
			[NamPhatHanh],
			[KiHieuChungTuPhatHanhBaoLanh],
			[SoChungTuPhatHanhBaoLanh],
			[MaTienTe],
			[MaNganHangTraThay],
			[TenNganHangTraThay],
			[MaDonViSuDungHanMuc],
			[TenDonViSuDungHanMuc],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[MaKetQuaXuLy],
			[TenNganHangCungCapBaoLanh],
			[SoToKhai],
			[NgayDuDinhKhaiBao],
			[MaLoaiHinh],
			[CoQuanHaiQuan],
			[TenCucHaiQuan],
			[SoVanDon_1],
			[SoVanDon_2],
			[SoVanDon_3],
			[SoVanDon_4],
			[SoVanDon_5],
			[SoHoaDon],
			[MaDonViSuDungBaoLanh],
			[TenDonViSuDungBaoLanh],
			[MaDonViDaiDienSuDungBaoLanh],
			[TenDonViDaiDienSuDungBaoLanh],
			[NgayBatDauHieuLuc],
			[NgayHetHieuLuc],
			[SoTienDangKyBaoLanh],
			[CoBaoVoHieu],
			[SoDuBaoLanh],
			[SoTienHanMucDangKi],
			[SoDuHanMuc],
			[SoThuTuHanMuc],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@LoaiChungTu,
			@LoaiHinhBaoLanh,
			@MaNganHangCungCapBaoLanh,
			@NamPhatHanh,
			@KiHieuChungTuPhatHanhBaoLanh,
			@SoChungTuPhatHanhBaoLanh,
			@MaTienTe,
			@MaNganHangTraThay,
			@TenNganHangTraThay,
			@MaDonViSuDungHanMuc,
			@TenDonViSuDungHanMuc,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@MaKetQuaXuLy,
			@TenNganHangCungCapBaoLanh,
			@SoToKhai,
			@NgayDuDinhKhaiBao,
			@MaLoaiHinh,
			@CoQuanHaiQuan,
			@TenCucHaiQuan,
			@SoVanDon_1,
			@SoVanDon_2,
			@SoVanDon_3,
			@SoVanDon_4,
			@SoVanDon_5,
			@SoHoaDon,
			@MaDonViSuDungBaoLanh,
			@TenDonViSuDungBaoLanh,
			@MaDonViDaiDienSuDungBaoLanh,
			@TenDonViDaiDienSuDungBaoLanh,
			@NgayBatDauHieuLuc,
			@NgayHetHieuLuc,
			@SoTienDangKyBaoLanh,
			@CoBaoVoHieu,
			@SoDuBaoLanh,
			@SoTienHanMucDangKi,
			@SoDuHanMuc,
			@SoThuTuHanMuc,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[LoaiHinhBaoLanh] = @LoaiHinhBaoLanh,
	[MaNganHangCungCapBaoLanh] = @MaNganHangCungCapBaoLanh,
	[NamPhatHanh] = @NamPhatHanh,
	[KiHieuChungTuPhatHanhBaoLanh] = @KiHieuChungTuPhatHanhBaoLanh,
	[SoChungTuPhatHanhBaoLanh] = @SoChungTuPhatHanhBaoLanh,
	[MaTienTe] = @MaTienTe,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaDonViSuDungHanMuc] = @MaDonViSuDungHanMuc,
	[TenDonViSuDungHanMuc] = @TenDonViSuDungHanMuc,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenNganHangCungCapBaoLanh] = @TenNganHangCungCapBaoLanh,
	[SoToKhai] = @SoToKhai,
	[NgayDuDinhKhaiBao] = @NgayDuDinhKhaiBao,
	[MaLoaiHinh] = @MaLoaiHinh,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[TenCucHaiQuan] = @TenCucHaiQuan,
	[SoVanDon_1] = @SoVanDon_1,
	[SoVanDon_2] = @SoVanDon_2,
	[SoVanDon_3] = @SoVanDon_3,
	[SoVanDon_4] = @SoVanDon_4,
	[SoVanDon_5] = @SoVanDon_5,
	[SoHoaDon] = @SoHoaDon,
	[MaDonViSuDungBaoLanh] = @MaDonViSuDungBaoLanh,
	[TenDonViSuDungBaoLanh] = @TenDonViSuDungBaoLanh,
	[MaDonViDaiDienSuDungBaoLanh] = @MaDonViDaiDienSuDungBaoLanh,
	[TenDonViDaiDienSuDungBaoLanh] = @TenDonViDaiDienSuDungBaoLanh,
	[NgayBatDauHieuLuc] = @NgayBatDauHieuLuc,
	[NgayHetHieuLuc] = @NgayHetHieuLuc,
	[SoTienDangKyBaoLanh] = @SoTienDangKyBaoLanh,
	[CoBaoVoHieu] = @CoBaoVoHieu,
	[SoDuBaoLanh] = @SoDuBaoLanh,
	[SoTienHanMucDangKi] = @SoTienHanMucDangKi,
	[SoDuHanMuc] = @SoDuHanMuc,
	[SoThuTuHanMuc] = @SoThuTuHanMuc,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID


GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '10.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('10.9', GETDATE(), N'Cap nhat store (VNACCS)')
END	

