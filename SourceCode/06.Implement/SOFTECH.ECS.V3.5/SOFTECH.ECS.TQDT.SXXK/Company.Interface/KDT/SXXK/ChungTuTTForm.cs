﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;
using System.Windows.Forms;
using Company.Interface;
using Company.Interface.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class ChungTuTTForm : BaseForm
    {
        public ChungTuTT ChungTuTT = new ChungTuTT();
        public ChungTuTTForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void Set()
        {
            txtSoChungTu.Text = ChungTuTT.SoChungTu;
            if (ChungTuTT.NgayChungTu.Year > 1900)
                ccNgayChungTu.Text = ChungTuTT.NgayChungTu.ToShortDateString();
            txtNoiPhatHanh.Text = ChungTuTT.NoiPhatHanh;
            if (!string.IsNullOrEmpty(ChungTuTT.PTTT_ID))
                cbPTTT.SelectedValue = ChungTuTT.PTTT_ID;
            txtTrigiaTT.Value = ChungTuTT.TriGiaTT;
            txtSoHopDong.Text = ChungTuTT.SoHopDong;
            if (ChungTuTT.NgayHopDong.Year > 1900)
                ccNgayHopDong.Text = ChungTuTT.NgayHopDong.ToShortDateString();
            txtTriGiaTTHD.Value = ChungTuTT.TriGiaTheoHopDong;
            txtGhiChu.Text = ChungTuTT.GhiChu;
            dgList.DataSource = ChungTuTT.ChungTuTTChitiets;
        }
        private void Get()
        {
            ChungTuTT.SoChungTu = txtSoChungTu.Text;

            if (ccNgayChungTu.Value.Year < 1900)
                ccNgayChungTu.Value = new DateTime(1900, 1, 1);
            else ChungTuTT.NgayChungTu = ccNgayChungTu.Value;

            ChungTuTT.NoiPhatHanh = txtNoiPhatHanh.Text;

            if (!string.IsNullOrEmpty(cbPTTT.SelectedValue.ToString()))
                ChungTuTT.PTTT_ID = cbPTTT.SelectedValue.ToString();
            ChungTuTT.TriGiaTT = Convert.ToDouble(txtTrigiaTT.Value);
            ChungTuTT.SoHopDong = txtSoHopDong.Text;
            if (ccNgayHopDong.Value.Year < 1900)
                ChungTuTT.NgayHopDong = new DateTime(1900, 1, 1);
            else ChungTuTT.NgayHopDong = ccNgayHopDong.Value;
            ChungTuTT.TriGiaTheoHopDong = Convert.ToDouble(txtTriGiaTTHD.Value);
            ChungTuTT.GhiChu = txtGhiChu.Text;
        }
        private void ChungTuTTForm_Load(object sender, EventArgs e)
        {
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            Set();
            txtSoChungTu.Focus();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SanPhamRegistedForm SPRegistedForm = new SanPhamRegistedForm();
            SPRegistedForm.CalledForm = "ChungTuTTForm";
            SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            SPRegistedForm.ShowDialog(this);
            if (SPRegistedForm.SanPhamSelected != null && !string.IsNullOrEmpty(SPRegistedForm.SanPhamSelected.Ma))
            {
                ChungTuTTChitiet chungtuChitiet = new ChungTuTTChitiet()
                {
                    MaHang = SPRegistedForm.SanPhamSelected.Ma,
                    TenHang = SPRegistedForm.SanPhamSelected.Ten
                };
                ChungTuTT.ChungTuTTChitiets.Add(chungtuChitiet);
                dgList.DataSource = ChungTuTT.ChungTuTTChitiets;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            bool isValidate = true;
            isValidate &= ValidateControl.ValidateNull(txtSoChungTu, error, "Số chứng từ");
            isValidate &= ValidateControl.ValidateDate(ccNgayChungTu, error, "Ngày chứng từ");
            isValidate &= ValidateControl.ValidateNull(txtSoHopDong, error, "Số hợp đồng");
            isValidate &= ValidateControl.ValidateDate(ccNgayHopDong, error, "Ngày hợp đồng");
            if (!isValidate) return;
            Get();
            try
            {
                if (ChungTuTT.ID == 0)
                {
                    ChungTuTT.Insert();
                }
                else
                {
                    ChungTuTT.Update();
                }
                GridEXRow[] rows = dgList.GetDataRows();
                foreach (GridEXRow item in rows)
                {
                    ChungTuTTChitiet ctct = (ChungTuTTChitiet)item.DataRow;
                    ctct.Master_ID = ChungTuTT.ID;
                    ctct.TriGia = Convert.ToDouble(item.Cells["TriGia"].Value);

                }
                ChungTuTTChitiet.InsertUpdateCollection(ChungTuTT.ChungTuTTChitiets);
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) != "Yes") return;
            try
            {
                if (ChungTuTT.ID == 0)
                {
                    ChungTuTT = new ChungTuTT();
                }
                else
                {
                    ChungTuTTChitiet.DeleteCollection(ChungTuTT.ChungTuTTChitiets);
                    ChungTuTT.Delete();
                }
                dgList.DataSource = ChungTuTT.ChungTuTTChitiets;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));

            }
        }

        private void DeleteHang()
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count == 0) return;
                if (ShowMessage("Bạn có thật sự muốn xóa sản phẩm này không?", true) != "Yes") return;

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuTTChitiet ctct = (ChungTuTTChitiet)i.GetRow().DataRow;
                        ctct.Delete();
                        ChungTuTT.ChungTuTTChitiets.Remove(ctct);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try
            {
                dgList.Refetch();
            }
            catch (Exception)
            {

                dgList.Refresh();

            }
        }

        private void btnXoaHang_Click(object sender, EventArgs e)
        {
            DeleteHang();
        }
    }
}
