using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Logger;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.SXXK
{
    public partial class ChayThanhLyForm : BaseForm
    {
        public int ProccesValue;
        public bool temp = false;
        private bool temp2 = false;
        private int limit = 99;
        private bool onRunning = false;
        private bool isError = false;
        private string msgError = "";
        public bool ThanhKhoanMH = false;
        public String WhereCondition;
        public DateTime DateFrom;
        public DateTime DateTo;
        public int ChenhLechNgay;
        public int AmXuLyTiep;
        public long HopDong_ID;
        public ChayThanhLyForm()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (onRunning) e.Cancel = false;
            else
                base.OnClosing(e);
        }
        private void ChayThanhLyForm_Load_1(object sender, EventArgs e)
        {
            this.ProccesValue = 1;
            backgroundWorker1.RunWorkerAsync();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (temp)
                {
                    timer1.Enabled = false;
                    Thread.Sleep(2000);
                    this.Close();
                }

                DataRow dr = dtProcess.NewRow();

                if (GlobalSettings.NGON_NGU == "0")
                {
                    #region Tieng viet

                    if (this.ProccesValue == 1)
                    {
                        //DataRow dr = dtProcess.NewRow();
                        // txtMessage.Text += "Đang khởi tạo dữ liệu thanh khoản...";
                        dr["TenProcess"] = "Đang khởi tạo dữ liệu ...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                    }
                    if (this.ProccesValue == 19)
                    {
                        dtProcess.Select("TenProcess='" + "Đang khởi tạo dữ liệu ..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        // txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 21)
                    {
                        dr["TenProcess"] = "Đang kiểm tra số liệu...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang kiểm tra số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 39)
                    {
                        dtProcess.Select("TenProcess='" + "Đang kiểm tra số liệu..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 41)
                    {
                        dr["TenProcess"] = "Đang xử lý số liệu ....";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang xử lý số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 59)
                    {
                        dtProcess.Select("TenProcess='" + "Đang xử lý số liệu ...." + "'")[0]["TrangThai"] = "Hoàn thành";
                    }
                    if (this.ProccesValue == 61)
                    {
                        dr["TenProcess"] = "Đang chạy xử lý dữ liệu...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang chạy thanh khoản...";
                    }
                    if (this.ProccesValue == 62)
                    {
                        //try
                        //{
                        //    this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Lỗi: " + ex.Message, false);
                        //    this.Close();
                        //}
                    }
                    if (this.ProccesValue == 79)
                    {
                        dtProcess.Select("TenProcess='" + "Đang chạy xử lý dữ liệu..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 81)
                    {
                        dr["TenProcess"] = "Đang tạo các báo cáo dữ liệu...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        // txtMessage.Text += "Đang tạo các báo cáo thanh khoản...";
                    }
                    if (this.ProccesValue == 99 && !temp2)
                    {
                        dtProcess.Select("TenProcess='" + "Đang tạo các báo cáo dữ liệu..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                        temp2 = true;
                    }

                    #endregion
                }
                else
                {
                    #region Tieng anh
                    if (this.ProccesValue == 1)
                    {
                        //DataRow dr = dtProcess.NewRow();
                        // txtMessage.Text += "Đang khởi tạo dữ liệu thanh khoản...";
                        dr["TenProcess"] = "Creating the Liquidation\'s Data...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                    }
                    if (this.ProccesValue == 19)
                    {
                        dtProcess.Select("TenProcess='" + "Creating the data of liquidation..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        // txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 21)
                    {
                        dr["TenProcess"] = "Checking the Figures of Liquidation ...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang kiểm tra số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 39)
                    {
                        dtProcess.Select("TenProcess='" + "Checking the Figures of Liquidation ..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 41)
                    {
                        dr["TenProcess"] = "Handling the Figures of Liquidation....";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang xử lý số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 59)
                    {
                        dtProcess.Select("TenProcess='" + "Handling the Figures of Liquidation...." + "'")[0]["TrangThai"] = "Hoàn thành";
                    }
                    if (this.ProccesValue == 61)
                    {
                        dr["TenProcess"] = "Running Liquidation...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang chạy thanh khoản...";
                    }
                    if (this.ProccesValue == 62)
                    {
                        //try
                        //{
                        //    this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Lỗi: " + ex.Message, false);
                        //    this.Close();
                        //}
                    }
                    if (this.ProccesValue == 79)
                    {
                        dtProcess.Select("TenProcess='" + "Running Liquidation..." + "'")[0]["TrangThai"] = "Finish";

                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 81)
                    {
                        dr["TenProcess"] = "Creating the Reports of Liquidation...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        // txtMessage.Text += "Đang tạo các báo cáo thanh khoản...";
                    }
                    if (this.ProccesValue == 99 && !temp2)
                    {
                        dtProcess.Select("TenProcess='" + "Creating the Reports of Liquidation..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                        temp2 = true;
                    }
                    #endregion
                }

                if (this.ProccesValue == 100)
                {
                    if (pbProgress.Value == 99)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            dr["TenProcess"] = "Chạy xử lý dữ liệu thành công.";
                            dr["TrangThai"] = "Xin chờ...";
                        }
                        else
                        {
                            dr["TenProcess"] = " Run liquidation successfully.";
                            dr["TrangThai"] = "Wait...";
                        }
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Chạy thanh khoản thành công.";
                        temp = true;
                    }
                    else
                    {
                        this.ProccesValue = pbProgress.Value;
                        limit = 100;
                    }

                }
                pbProgress.Value = this.ProccesValue;
                if (this.ProccesValue < limit) this.ProccesValue++;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void ChayThanhLyForm_Shown(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
          
            onRunning = true;
            try
            {
                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);

                this.ChayThanhLyNgayHoanThanhXuat(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK, 0, AmXuLyTiep, ChenhLechNgay, 1);
            }
            catch (Exception ex)
            {
                timer1.Enabled = false;
                LocalLogger.Instance().WriteMessage("Lỗi chạy xử lý dữ liệu: ", ex);
                isError = true;
                msgError = ex.Message;
            }
            onRunning = false;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (isError)
            {
                DataRow dr = dtProcess.NewRow();
                dr["TenProcess"] = msgError;
                dr["TrangThai"] = "Lỗi";
                dtProcess.Rows.Add(dr);

                grdProcess.GetRow(grdProcess.RowCount - 1).Cells["TenProcess"].ToolTipText = msgError;

                this.ProccesValue = 100;
            }
            else
                this.ProccesValue = 100;
        }
        public void ChayThanhLyNgayHoanThanhXuat(int SoThapPhanNPL, int nplKoTK, int TKToKhaiNKD, int AmTKTiep, int chenhLechNgay, int ToKhaiKoTK)
        {
            int soThapPhanBCXuatNhapTon_Thue = 5; /*Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("BCTK") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("BCTK")) : 0;*/


            // Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Lay thong tin tu Config: MaNPL: {0}, SoTKN: {1}, MaSP: {2}, SoTKX: {3}", configMaNPL, configSoTKN, configMaSP, configSoTKX)));

            DataSet ds = new DataSet();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region Khởi tạo các dữ liệu ban đầu

                    //Lấy danh sách nguyên phụ liệu tồn của các tờ khai nhập có trong bộ hồ sơ thanh khoản
                    string sp = "p_KDT_GC_NPLNhapTon";
                    DbCommand cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
                    db.AddInParameter(cmd, "@OrderByExpression", SqlDbType.NVarChar, "");
                    db.LoadDataSet(cmd, ds, "t_NPLNhapTon");

                    //Lấy danh sách tất cả NPL quy đổi từ định mức sản phẩm của tất cả tờ khai xuất trong bộ hồ sợ thanh khoản
                    /*HUNGTQ updated 18/10/2010. Cap nhat them tieu chi sap xep theo 'Ngay hoan thanh xuat' theo cau hinh bao cao cho store procedure p_KDT_SXXK_DanhSachNPLXuatTonOver5.*/
                    sp = "p_KDT_GC_NPLXuatTon";
                    cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
                    db.AddInParameter(cmd, "@OrderByExpression", SqlDbType.NVarChar, "");
                    db.LoadDataSet(cmd, ds, "t_NPLXuatTon");

                    List<KDT_GC_BCXuatNhapTon> bcXNTCollection = new List<KDT_GC_BCXuatNhapTon>();

                    DataTable dtNPLXuatTon = ds.Tables["t_NPLXuatTon"];
                    DataTable dtNPLNhapTon = ds.Tables["t_NPLNhapTon"];
                    #region
                    //Gộp lại các mã NPL có cùng số lượng và thuế trong tờ khai 
                    DataTable dt = dtNPLNhapTon.Clone();
                    decimal SoToKhai;
                    string MaLoaiHinh;
                    int NamDangKy;
                    DateTime NgayDangKy;
                    string MaHaiQuan;
                    string TenNPL;
                    string MaNPL;
                    string TenDVT_NPL;
                    decimal Luong;
                    decimal TonDau;
                    decimal TonCuoi;
                    DateTime NgayThucNhap;
                    DateTime NgayHoanThanh;
                    decimal BangKeHoSoThanhLy_ID;
                    decimal ThueXNK;
                    decimal TonDauThueXNK;
                    decimal TonCuoiThueXNK;
                    //decimal DonGiaTT;
                    decimal ThueSuat;
                    decimal TyGiaTT;
                    #endregion
                    //Tạo table báo cáo nhập xuất tồn
                    DataTable dtBCXuatNhapTon = new DataTable();
                    DataColumn[] cols = new DataColumn[38];
                    cols[0] = new DataColumn("LanThanhLy", typeof(int));
                    cols[1] = new DataColumn("MaDoanhNghiep", typeof(string));
                    cols[2] = new DataColumn("STT", typeof(long));
                    cols[3] = new DataColumn("MaNPL", typeof(string));
                    cols[4] = new DataColumn("SoToKhaiNhap", typeof(Int64));
                    cols[5] = new DataColumn("NgayDangKyNhap", typeof(DateTime));
                    cols[6] = new DataColumn("NgayHoanThanhNhap", typeof(DateTime));
                    cols[7] = new DataColumn("LuongNhap", typeof(decimal));
                    cols[8] = new DataColumn("LuongTonDau", typeof(decimal));
                    cols[9] = new DataColumn("TenDVT_NPL", typeof(string));
                    cols[10] = new DataColumn("MaSP", typeof(string));
                    cols[11] = new DataColumn("SoToKhaiXuat", typeof(Int64));
                    cols[12] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
                    cols[13] = new DataColumn("NgayHoanThanhXuat", typeof(DateTime));
                    cols[14] = new DataColumn("LuongSPXuat", typeof(decimal));
                    cols[15] = new DataColumn("TenDVT_SP", typeof(string));
                    cols[16] = new DataColumn("DinhMuc", typeof(decimal));
                    cols[17] = new DataColumn("LuongNPLSuDung", typeof(decimal));
                    cols[18] = new DataColumn("ToKhaiTaiXuat", typeof(decimal));
                    cols[19] = new DataColumn("LuongNPLTaiXuat", typeof(decimal));
                    cols[20] = new DataColumn("NgayTaiXuat", typeof(DateTime));
                    cols[21] = new DataColumn("LuongTonCuoi", typeof(decimal));
                    cols[22] = new DataColumn("ThanhKhoanTiep", typeof(string));
                    cols[24] = new DataColumn("TenNPL", typeof(string));
                    cols[25] = new DataColumn("TenSP", typeof(string));
                    cols[26] = new DataColumn("MaLoaiHinhNhap", typeof(string));
                    cols[27] = new DataColumn("MaLoaiHinhXuat", typeof(string));
                    cols[28] = new DataColumn("DonGiaTT", typeof(double));
                    cols[29] = new DataColumn("TyGiaTT", typeof(decimal));
                    cols[30] = new DataColumn("ThueSuat", typeof(decimal));
                    cols[31] = new DataColumn("ThueXNK", typeof(double));
                    cols[32] = new DataColumn("ThueXNKTon", typeof(double));
                    cols[33] = new DataColumn("NgayThucXuat", typeof(DateTime));
                    cols[34] = new DataColumn("SoDong", typeof(int));
                    cols[35] = new DataColumn("TuNgay", typeof(DateTime));
                    cols[36] = new DataColumn("DenNgay", typeof(DateTime));
                    cols[37] = new DataColumn("HopDong_ID", typeof(Int64));
                    dtBCXuatNhapTon.Columns.AddRange(cols);
                    #endregion

                    Logger.LocalLogger.Instance().WriteMessage("THỰC HIỆN CHẠY XỬ LÝ DỮ LIỆU", new Exception());
                    #region Thực hiện việc chạy xử lý dữ liệu

                    //Tạo table lưu tạm báo cáo nhập xuất tồn
                    DataTable dtTinhAm = dtBCXuatNhapTon.Clone();

                    //Luu so ton cuoi cua to khai xuat tinh am ke tiep cuoi cung cho to khai nhap theo maNPL
                    DataTable dtTonCuoiIndex = new DataTable();
                    dtTonCuoiIndex.TableName = "dtTonCuoiIndex";
                    DataColumn[] dc = new DataColumn[6];
                    dc[0] = new DataColumn("TKN", typeof(long));
                    dc[1] = new DataColumn("MaLHN", typeof(string));
                    dc[2] = new DataColumn("NgayHTN", typeof(string));
                    dc[3] = new DataColumn("MaNPL", typeof(string));
                    dc[4] = new DataColumn("TonCuoi", typeof(decimal));
                    dc[5] = new DataColumn("RowIndex", typeof(long));
                    dtTonCuoiIndex.Columns.AddRange(dc);

                    //Duyệt từng dòng trong danh sách nguyên phụ liệu xuất
                    #region Duyệt từng dòng trong danh sách nguyên phụ liệu xuất
                    foreach (DataRow drx in dtNPLXuatTon.Rows)
                    {
                        //TEST
                        string nplx = drx["MaNPL"].ToString().ToLower();
                        string spx = drx["MaSP"].ToString().ToLower();
                        string sotkx = drx["SoToKhai"].ToString();
                        string malhx = drx["MaLoaiHinh"].ToString();

                        decimal luongAm = 0;//Biến chứa lượng nguyên phụ liệu âm sau khi trừ gán bằng 0 trước khi xử lý 1 dòng trong danh sách NPL xuất
                        int j = -1;//Biến đánh dấu vị trí npl nhập
                        //Duyệt từng dòng trong danh sách nguyên phụ liệu nhập
                        #region Duyệt từng dòng trong danh sách nguyên phụ liệu nhập
                        for (int i = 0; i < dtNPLNhapTon.Rows.Count; i++)
                        {
                            DataRow drn = dtNPLNhapTon.Rows[i];


                            string npln = drn["MaNPL"].ToString().ToLower();
                            string sotkn = drn["SoToKhai"].ToString();
                            string malhn = drn["MaLoaiHinh"].ToString();
                            string maHQ = drn["CoQuanHaiQuan"].ToString();

                            //Nếu ngày thực nhập tờ khai nhập > ngày đăng ký tờ khai xuất thì bỏ qua tờ khai nhập

                            DateTime ngaytokhainhap = System.Convert.ToDateTime(drn["NgayHoanThanh"]);

                            DateTime ngaytokhaixuat = Convert.ToDateTime(drx["NgayDangKy"]);
                            if (drx["NgayHoanThanhXuat"] != null && Convert.ToDateTime(drx["NgayHoanThanhXuat"]).Year > 1900)
                                ngaytokhaixuat = Convert.ToDateTime(drx["NgayHoanThanhXuat"]);

                            //Kiem tra them ngay hoan thanh cua tk nhap KE TIEP + ngay chenh lech <= ngay hoan thanh tkx.
                            //Neu thoa man dieu kien -> cho them moi. 
                            bool tinhAmToKhaiTiepTheo = false;
                            if (AmTKTiep == 1 && System.Convert.ToDecimal(drn["TonCuoi"]) < 0)
                                tinhAmToKhaiTiepTheo = GetTKNKeTiep(dtNPLNhapTon, i, drn["MaNPL"].ToString().ToLower(), System.Convert.ToInt64(sotkn), malhn, System.Convert.ToDateTime(drn["NgayDangKy"]).Year, chenhLechNgay, ngaytokhaixuat, maHQ);


                            //TODO: So sanh chenh lech ngay giua TKX & TKN
                            //TODO: Updated by Hungtq, 25/10/2011. So sanh ngay hoan thanh, khong dung ngay dang ky.
                            //Luu y: Bo gio trong ngay khi so sanh theo ngay.
                            //if (Convert.ToDateTime(drn["NgayThucNhap"]).AddDays((double)chenhLechNgay) <= ngaytokhaixuat)
                            #region So sanh chenh lech ngay giua TKX & TKN
                            if (System.Convert.ToDateTime(System.Convert.ToDateTime(drn["NgayHoanThanh"]).AddDays((double)chenhLechNgay).ToShortDateString()) <= System.Convert.ToDateTime(ngaytokhaixuat.ToShortDateString()))
                            {
                                if (drn["MaNPL"].ToString().ToLower().Trim() == drx["MaNPL"].ToString().ToLower().Trim()
                                    && Convert.ToDecimal(drn["TonCuoi"]) > 0 && Convert.ToDecimal(drx["TonNPL"]) > 0)
                                {
                                    #region
                                    //Nếu mã NPL giống nhau trong NPL xuất và nhập và lượng tồn NPL nhập và tồn của npl xuất > 0
                                    //Tạo thêm 1 dòng mới trong báo cáo nhập xuất tồn và gán giá trị cho các cột
                                    try
                                    {
                                        DataRow dr = dtBCXuatNhapTon.NewRow();
                                        dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                        dr["TuNgay"] = DateTo;
                                        dr["DenNgay"] = DateFrom;
                                        dr["HopDong_ID"] = HopDong_ID;
                                        dr["STT"] = 0;
                                        dr["MaNPL"] = drn["MaNPL"].ToString().Trim();
                                        dr["TenNPL"] = drn["TenNPL"].ToString();
                                        dr["SoDong"] = Convert.ToInt32(drn["SoDong"]);
                                        dr["SoToKhaiNhap"] = drn["SoToKhai"].ToString();
                                        dr["NgayDangKyNhap"] = Convert.ToDateTime(drn["NgayDangKy"]);
                                        dr["NgayHoanThanhNhap"] = Convert.ToDateTime(drn["NgayHoanThanh"]);
                                        dr["MaLoaiHinhNhap"] = Convert.ToString(drn["MaLoaiHinh"]);
                                        dr["LuongNhap"] = Convert.ToDecimal(drn["Luong"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                        dr["LuongTonDau"] = Convert.ToDecimal(drn["TonDau"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                        dr["TenDVT_NPL"] = Convert.ToString(drn["TenDVT_NPL"]);
                                        dr["DonGiaTT"] = Convert.ToDouble(drn["DonGiaTT"]);
                                        dr["TyGiaTT"] = Convert.ToDecimal(drn["TyGiaTT"]);
                                        dr["ThueSuat"] = Convert.ToDecimal(drn["ThueSuat"]);
                                        dr["ThueXNK"] = Convert.ToDouble(drn["ThueXNK"]);
                                        dr["ThueXNKTon"] = Convert.ToDouble(drn["TonDauThueXNK"]);

                                        dr["MaSP"] = drx["MaSP"].ToString();
                                        dr["TenSP"] = drx["TenSP"].ToString();
                                        dr["SoToKhaiXuat"] = drx["SoToKhai"].ToString();
                                        dr["NgayDangKyXuat"] = Convert.ToDateTime(drx["NgayDangKy"]);
                                        //
                                        dr["NgayHoanThanhXuat"] = Convert.ToDateTime(drx["NgayHoanThanhXuat"]);
                                        dr["NgayThucXuat"] = Convert.ToDateTime(drx["NgayThucXuat"]);

                                        dr["MaLoaiHinhXuat"] = Convert.ToString(drx["MaLoaiHinh"]);
                                        dr["LuongSPXuat"] = Convert.ToDecimal(drx["LuongSP"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                        //HungTQ, updaed so thap phan 11/02/2012
                                        dr["LuongNPLSuDung"] = Convert.ToDecimal(drx["LuongNPL"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                        dr["TenDVT_SP"] = Convert.ToString(drx["TenDVT_SP"]);
                                        dr["DinhMuc"] = Convert.ToDecimal(drx["DinhMuc"]);


                                        if (Convert.ToDecimal(drn["TonCuoi"]) >= Convert.ToDecimal(drx["TonNPL"]))
                                        {
                                            //Nếu tồn cuối của npl nhập mà >= tồn của npl xuất
                                            if (luongAm < 0)
                                            {
                                                //Nếu lượng âm < 0
                                                dr["LuongTonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) + luongAm;
                                                drn["TonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) - Convert.ToDecimal(drx["TonNPL"]);
                                            }
                                            else
                                            {
                                                //Ngược lại
                                                drn["TonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) - Convert.ToDecimal(drx["TonNPL"]);
                                                dr["LuongTonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]);
                                            }
                                            drx["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 ==> đã xử lý xong dòng npl xuất này 

                                            dtBCXuatNhapTon.Rows.Add(dr);//Add thêm 1 dòng vào báo cáo

                                            //Danh dau vi tri dong npl bi am dau tien
                                            if (luongAm < 0)
                                            {
                                                DataRow nr = dtTonCuoiIndex.NewRow();
                                                nr["TKN"] = System.Convert.ToInt64(sotkn);
                                                nr["MaLHN"] = malhn;
                                                nr["NgayHTN"] = ngaytokhainhap;
                                                nr["MaNPL"] = npln;
                                                nr["TonCuoi"] = System.Convert.ToDecimal(dr["LuongTonCuoi"]);
                                                nr["RowIndex"] = dtBCXuatNhapTon.Rows.Count;
                                                dtTonCuoiIndex.Rows.Add(nr);
                                            }

                                            if (AmTKTiep == 1)
                                            {
                                                //Am thanh khoan tiếp
                                                if (Convert.ToDecimal(drn["TonCuoi"]) == 0)
                                                {
                                                    //Nếu lượng tồn npl này hết
                                                    drn["LanDieuChinh"] = 100;//đánh dấu bằng con số 100 để biết npl của tờ khai này là đứng cuối cùng trong danh sách npl có tồn =0
                                                    for (int k = 0; k < i; k++)
                                                        if (dtNPLNhapTon.Rows[k]["MaNPL"].ToString().ToUpper().Trim() == drn["MaNPL"].ToString().ToUpper().Trim()) dtNPLNhapTon.Rows[k]["LanDieuChinh"] = 0;// Đánh dấu các npl của tờ khai trước đó bằng 0
                                                }
                                            }
                                            break;//Thoát khỏi vòng lập
                                        }
                                        else
                                        {
                                            //Nếu lượng tồn npl nhập < lượng tồn npl xuất
                                            if (luongAm < 0) // Lượng tồn < 0
                                            {
                                                //Nếu lương âm vẫn còn nhỏ hơn 0
                                                dr["LuongTonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) + luongAm;
                                                drx["TonNPL"] = Convert.ToDecimal(drx["TonNPL"]) - Convert.ToDecimal(drn["TonCuoi"]);
                                                drn["TonCuoi"] = 0;
                                                luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                            }
                                            else //Bo sung dong: else
                                            {
                                                //Nếu lớn hơn 0
                                                decimal temp = Convert.ToDecimal(drx["TonNPL"]);
                                                drx["TonNPL"] = System.Convert.ToDecimal(drx["TonNPL"]) - System.Convert.ToDecimal(drn["TonCuoi"]);
                                                dr["LuongTonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) - temp;
                                                drn["TonCuoi"] = 0;
                                                luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);

                                            }
                                            dtBCXuatNhapTon.Rows.Add(dr);

                                            //Danh dau vi tri dong npl bi am dau tien
                                            if (luongAm < 0)
                                            {
                                                DataRow nr = dtTonCuoiIndex.NewRow();
                                                nr["TKN"] = System.Convert.ToInt64(sotkn);
                                                nr["MaLHN"] = malhn;
                                                nr["NgayHTN"] = ngaytokhainhap;
                                                nr["MaNPL"] = npln;
                                                nr["TonCuoi"] = System.Convert.ToDecimal(dr["LuongTonCuoi"]);
                                                nr["RowIndex"] = dtBCXuatNhapTon.Rows.Count;
                                                dtTonCuoiIndex.Rows.Add(nr);
                                            }

                                            j = i;// đánh dấu vị trí bằng i (index của bảng Nhập Tồn dtNPLNhapTon)
                                        }
                                    #endregion

                                        #region Lưu log  debug theo config

                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                        #endregion
                                }
                                else
                                {
                                    #region Am thanh khoan tiep
                                    //Am thanh khoan tiep
                                    if (AmTKTiep == 1)
                                    {
                                        try
                                        {
                                            if (drn["MaNPL"].ToString().ToLower().Trim() == drx["MaNPL"].ToString().ToLower().Trim()
                                                //&& CheckNPLSau(dtNPLNhapTon, drn["MaNPL"].ToString().ToLower(), indexTK > 0 ? indexTK - 1 : i, System.Convert.ToDateTime(drx["NgayHoanThanhXuat"])) //Cap nhat NgayDangky = NgayHoanThanhXuat
                                                && CheckNPLSau(dtNPLNhapTon, drn["MaNPL"].ToString().ToLower(), i, System.Convert.ToDateTime(drx["NgayHoanThanhXuat"])) //Cap nhat NgayDangky = NgayHoanThanhXuat
                                                && (System.Convert.ToDecimal(drn["TonCuoi"]) < 0
                                                || System.Convert.ToInt32(drn["LanDieuChinh"]) == 100)
                                                && System.Convert.ToDecimal(drx["TonNPL"]) > 0)
                                            {

                                                //Kiem tra them ngay hoan thanh cua tk nhap KE TIEP <= ngay hoan thanh tkx.
                                                //Neu thoa man dieu kien -> cho them moi.
                                                if (tinhAmToKhaiTiepTheo == true)
                                                    continue;
                                                //if (drn["MaNPL"].ToString().ToLower().Trim() == "daykeo" && System.Convert.ToDecimal(drn["SoToKhai"]) == 2510)
                                                //{ }


                                                AddBCNhapXuatTon(dtBCXuatNhapTon, soThapPhanBCXuatNhapTon_Thue, drn, drx);

                                                //Cap nhat lai ton
                                                drn["TonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) - Convert.ToDecimal(drx["TonNPL"]);
                                                drx["TonNPL"] = 0;

                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                            //Lưu tạm các tờ khai nhập mà nếu không tính chênh lệch ngày
                            else if (System.Convert.ToDateTime(System.Convert.ToDateTime(drn["NgayHoanThanh"]).ToShortDateString()) <= System.Convert.ToDateTime(ngaytokhaixuat.ToShortDateString()))
                            {
                                try
                                {
                                    if (drn["MaNPL"].ToString().ToLower().Trim() == drx["MaNPL"].ToString().ToLower().Trim())
                                    {
                                        AddBCNhapXuatTon(dtTinhAm, soThapPhanBCXuatNhapTon_Thue, drn, drx);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                        }
                        #endregion
                        if (Convert.ToDecimal(drx["TonNPL"]) > 0)
                        {
                            //Nếu hết tất cả tờ khai nhập mà lượng tồn xuất ra vẫn còn dương
                            if (j >= 0)
                            {
                                //Nếu biến j >0 
                                dtNPLNhapTon.Rows[j]["TonCuoi"] = 0 - Convert.ToDecimal(drx["TonNPL"]);//Gán lại lượng tồn cuối của npl cuối cùng thanh khoản cho npl xuất này
                                dtNPLNhapTon.Rows[j]["LanDieuChinh"] = 100;
                                drx["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 để bỏ qua npl xuất này
                            }
                        }
                    }
                    #endregion

                    #region Bổ sung tờ khai tính âm kế tiếp. Hungtq 07/06/2012.

                    if (AmTKTiep == 1)
                    {
                        try
                        {
                            dtTinhAm.DefaultView.Sort = "SoToKhaiNhap, MaNPL";

                            DataView dvBCXNTTemp = dtBCXuatNhapTon.Copy().DefaultView;
                            dvBCXNTTemp.RowFilter = /*"SoToKhaiNhap = 2510"; // */"ThanhKhoanTiep = 'Mua tại VN'";

                            //Nhóm theo Tờ khai -> danh sách tờ khai nhập
                            List<string> lStringTemp = new List<string>();
                            string query = "";
                            int rowIndex = 0;
                            DataRow[] rowsTempBCXNT;
                            DataRow[] rowsTempTKX;
                            DataRow[] rowsTempTKX_SuaTonCuoi;
                            DataTable dtResult = dtBCXuatNhapTon.Clone();

                            foreach (DataRowView drv in dvBCXNTTemp)
                            {
                                query = string.Format("SoToKhaiNhap = {0} and MaLoaiHinhNhap = '{1}' and NgayHoanThanhNhap = '{2}' and MaNPL = '{3}' and ThanhKhoanTiep = 'Mua tại VN'", drv["SoToKhaiNhap"], drv["MaLoaiHinhNhap"], drv["NgayHoanThanhNhap"], drv["MaNPL"]);

                                if (!SearchStringInList(lStringTemp, query))
                                {
                                    lStringTemp.Add(query);

                                    rowsTempBCXNT = dtTinhAm.Select(query);

                                    decimal tonCuoiAmTKX = 0;
                                    query = string.Format("TKN = {0} and MaLHN = '{1}' and NgayHTN = '{2}' and MaNPL = '{3}' and TonCuoi < 0", drv["SoToKhaiNhap"], drv["MaLoaiHinhNhap"], drv["NgayHoanThanhNhap"], drv["MaNPL"]);
                                    rowsTempTKX = dtTonCuoiIndex.Select(query);
                                    rowIndex = rowsTempTKX.Length > 0 ? Convert.ToInt32(rowsTempTKX[0]["RowIndex"]) : 0;
                                    //Lay ton < 0 lan dau phat sinh
                                    tonCuoiAmTKX = rowsTempTKX.Length > 0 ? Convert.ToDecimal(rowsTempTKX[0]["TonCuoi"]) : 0;

                                    //Lay danh sach to khai xuat co ton cuoi < 0
                                    query = string.Format("SoToKhaiNhap = {0} and MaLoaiHinhNhap = '{1}' and NgayHoanThanhNhap = '{2}' and MaNPL = '{3}' and ThanhKhoanTiep = 'Mua tại VN' and LuongTonCuoi < 0", drv["SoToKhaiNhap"], drv["MaLoaiHinhNhap"], drv["NgayHoanThanhNhap"], drv["MaNPL"]);
                                    rowsTempTKX_SuaTonCuoi = dtBCXuatNhapTon.Select(query);

                                    //Cac to khai xuat am ke tiep bo sung
                                    for (int e = 0; e < rowsTempBCXNT.Length; e++)
                                    {
                                        rowsTempBCXNT[e]["LuongTonCuoi"] = tonCuoiAmTKX - System.Convert.ToDecimal(rowsTempBCXNT[e]["LuongNPLSuDung"]);
                                        tonCuoiAmTKX = System.Convert.ToDecimal(rowsTempBCXNT[e]["LuongTonCuoi"]);

                                        //Chen row tai vi tri cu the
                                        rowIndex += 1;
                                        DataRow desRow = GetDataRow(dtBCXuatNhapTon, rowsTempBCXNT[e]);
                                        dtBCXuatNhapTon.Rows.InsertAt(desRow, rowIndex);
                                    }
                                    //Cap nhat row trừ lùi do vị trí các row sắp xếp đảo ngược
                                    for (int l = rowsTempTKX_SuaTonCuoi.Length - 1; l >= 0; l--)
                                    {
                                        rowsTempTKX_SuaTonCuoi[l]["LuongTonCuoi"] = tonCuoiAmTKX - System.Convert.ToDecimal(rowsTempTKX_SuaTonCuoi[l]["LuongNPLSuDung"]);
                                        tonCuoiAmTKX = System.Convert.ToDecimal(rowsTempTKX_SuaTonCuoi[l]["LuongTonCuoi"]);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                    }

                    #endregion

                    #endregion
                    Logger.LocalLogger.Instance().WriteMessage("insert BAOCAONPLNHAPTON", new Exception());

                    #region insert BCXuatNhapTon
                    //Xử lý để insert DataTable dtBCXuatNhapTon vào bảng t_KDT_SXXK_BCXuatNhapTon
                    dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";

                    if (TKToKhaiNKD != 1)
                    {

                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap LIKE 'NKD%'";
                    }
                    int STT = 0;
                    string maNPL = "";
                    //duydp Tên NPL và Số thứ tự dòng hàng
                    string tenNPL = "";
                    int soDong = 0;
                    Int64 soToKhaiNhap = 0;
                    string maLoaiHinhNhap = "";
                    DateTime ngayDangKyNhap = new DateTime(1900, 1, 1);

                    for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                    {
                        try
                        {
                            DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                            //if (maNPL != Convert.ToString(rv["MaNPL"]) || soDong !=Convert.ToInt32(rv["SoDong"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"]))
                            if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt64(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"]))
                                //Them TenNPL minhnd 03/07/2015
                                //if (maNPL != Convert.ToString(rv["MaNPL"]) || tenNPL != Convert.ToString(rv["TenNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) 
                                STT++;

                            KDT_GC_BCXuatNhapTon bc = new KDT_GC_BCXuatNhapTon();

                            bc.STT = STT;
                            bc.TuNgay = DateTo;
                            bc.DenNgay = DateFrom;
                            bc.HopDong_ID = HopDong_ID;
                            bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                            bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                            if (rv["LuongNPLTaiXuat"].ToString() != "")
                                bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                            bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                            bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongTonDau"]);
                            bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                            maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                            // duydp Thêm Số thứ tự dòng hàng
                            soDong = bc.SoThuTuHang = Convert.ToInt32(rv["SoDong"]);
                            bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                            bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);
                            //bc.SoThuTuHang = Convert.ToInt32(rv["SoDong"]);
                            bc.MaSP = Convert.ToString(rv["MaSP"]);
                            bc.TenSP = Convert.ToString(rv["TenSP"]);
                            ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                            bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                            bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                            bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);

                            //TODO: NGayThucXuat
                            try
                            {
                                if (rv["NgayThucXuat"] == null || rv["NgayThucXuat"] == DBNull.Value)
                                {
                                    //throw new Exception("Ngày thực xuất không được trống!");
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                }
                                else
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayThucXuat"]);
                            }
                            catch (Exception ex)
                            {
                                bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                Logger.LocalLogger.Instance().WriteMessage("Lỗi xử lý dữ liệu do Ngày thực xuất", ex);
                            }

                            //
                            bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                            bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                            soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt64(rv["SoToKhaiNhap"]);
#if DEBUG
                            if (bc.SoToKhaiNhap == 583)
                            {
                                if (bc.SoThuTuHang == 2)
                                {

                                }
                            }
#endif
                            bc.SoToKhaiXuat = Convert.ToInt64(rv["SoToKhaiXuat"]);
                            if (rv["ToKhaiTaiXuat"].ToString() != "")
                            {
                                bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                                bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                            }
                            bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                            bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                            if (rv["ThanhKhoanTiep"].ToString() != "")
                                bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                            else
                            {
                                if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                    bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            }


                            //Xử lý ghi chú là mua VN, chuyển lần sau TK hay chuyển TK xyz
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                            {
                                if (TKToKhaiNKD == 1)
                                {
                                    if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                    {
                                        DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];

                                        //TODO: Test
                                        string sotkx = rv1["SoToKhaiXuat"].ToString();
                                        string npl = rv["MaNPL"].ToString();
                                        string npl1 = rv1["MaNPL"].ToString();

                                        if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim())
                                            && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString()
                                            || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString()
                                            && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                        {

                                            if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                                bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                                //}
                                            }
                                        }
                                        else
                                        {
                                            if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                        }
                                    }
                                }
                            }
                            bcXNTCollection.Add(bc);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }


                    }
                    if (TKToKhaiNKD != 1)
                    {
                        dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap NOT LIKE 'NKD%'";
                        STT = 0;
                        maNPL = "";
                        tenNPL = "";
                        soDong = 0;
                        soToKhaiNhap = 0;
                        maLoaiHinhNhap = "";
                        ngayDangKyNhap = new DateTime(1900, 1, 1);

                        for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                        {
                            DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                            //if (maNPL != Convert.ToString(rv["MaNPL"]) || soDong == Convert.ToInt32(rv["SoDong"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"]))
                            if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt64(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"]))
                                //Them TenNPL 
                                //if (maNPL != Convert.ToString(rv["MaNPL"]) || tenNPL != Convert.ToString(rv["TenNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) 
                                STT++;

                            KDT_GC_BCXuatNhapTon bc = new KDT_GC_BCXuatNhapTon();

                            bc.STT = STT;
                            bc.TuNgay = DateTo;
                            bc.DenNgay = DateFrom;
                            bc.HopDong_ID = HopDong_ID;
                            bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                            bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                            if (rv["LuongNPLTaiXuat"].ToString() != "")
                                bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                            bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                            bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongTonDau"]);
                            bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                            maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                            bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                            // duydp Thêm Số thứ tự dòng hàng
                            bc.SoThuTuHang = Convert.ToInt32(rv["SoDong"]);
                            bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                            bc.MaSP = Convert.ToString(rv["MaSP"]);
                            bc.TenSP = Convert.ToString(rv["TenSP"]);
                            ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                            bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                            bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                            bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);

                            //TODO: NGayThucXuat
                            try
                            {
                                if (rv["NgayThucXuat"] == null || rv["NgayThucXuat"] == DBNull.Value)
                                {
                                    //throw new Exception("Ngày thực xuất không được trống!");
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                }
                                else
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayThucXuat"]);
                            }
                            catch (Exception ex)
                            {
                                bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                Logger.LocalLogger.Instance().WriteMessage("Lỗi thanh khoản do Ngày thực xuất", ex);
                            }

                            bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                            bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                            soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt64(rv["SoToKhaiNhap"]);
                            bc.SoToKhaiXuat = Convert.ToInt64(rv["SoToKhaiXuat"]);
                            if (rv["ToKhaiTaiXuat"].ToString() != "")
                            {
                                bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                                bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                            }
                            bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                            bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                            if (rv["ThanhKhoanTiep"].ToString().Trim() != "")
                                bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                            else
                            {
                                if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                    bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            }
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    //minhnd Them tenNPL
                                    //if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv1["TenNPL"].ToString().ToLower().Trim() == rv["TenNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }

                            bcXNTCollection.Add(bc);
                        }
                    }

                    this.InsertBCXNT(bcXNTCollection, transaction);
                    tenNPL = "";
                    maNPL = "";
                    soDong = 0;
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    maLoaiHinhNhap = "";
                    bool temp3 = false;

                    //Xử lý collection bcXNTCollection trước khi đưa vào xử lý báo cáo này để tổng hợp nên báo cáo thuế XNK
                    List<KDT_GC_BCXuatNhapTon> bcTemp = new List<KDT_GC_BCXuatNhapTon>();
                    for (int i = 0; i < bcXNTCollection.Count; i++)
                    {
                        //duydp Thêm Tên NPL và Số thứ tự dòng hàng
                        //if (maNPL == bcXNTCollection[i].MaNPL && tenNPL == bcXNTCollection[i].TenNPL && soDong == bcXNTCollection[i].SoThuTuHang && soToKhaiNhap == bcXNTCollection[i].SoToKhaiNhap && ngayDangKyNhap == bcXNTCollection[i].NgayDangKyNhap && maLoaiHinhNhap == bcXNTCollection[i].MaLoaiHinhNhap)
                        if (maNPL == bcXNTCollection[i].MaNPL && tenNPL == bcXNTCollection[i].TenNPL && soToKhaiNhap == bcXNTCollection[i].SoToKhaiNhap && ngayDangKyNhap == bcXNTCollection[i].NgayDangKyNhap && maLoaiHinhNhap == bcXNTCollection[i].MaLoaiHinhNhap)
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0)
                            {
                                if (!temp3)
                                    temp3 = true;
                                else
                                    bcTemp.Add(bcXNTCollection[i]);

                            }
                            else
                            {
                                temp3 = false;
                            }
                        }
                        else
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0) temp3 = true;
                            else temp3 = false;
                            // Them ten NPL
                            tenNPL = bcXNTCollection[i].TenNPL;
                            maNPL = bcXNTCollection[i].MaNPL;
                            soDong = bcXNTCollection[i].SoThuTuHang;
                            soToKhaiNhap = bcXNTCollection[i].SoToKhaiNhap;
                            ngayDangKyNhap = bcXNTCollection[i].NgayDangKyNhap;
                            maLoaiHinhNhap = bcXNTCollection[i].MaLoaiHinhNhap;
                        }
                    }
                    for (int i = 0; i < bcTemp.Count; i++)
                    {
                        bcXNTCollection.Remove(bcTemp[i]);
                    }

                    #endregion
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        private void AddBCNhapXuatTon(DataTable dtSource, int soThapPhanBCXuatNhapTon_Thue, DataRow drn, DataRow drx)
        {
            DataRow dr = dtSource.NewRow();
            dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
            dr["STT"] = 0;
            dr["TuNgay"] = drn["TuNgay"].ToString().Trim();
            dr["DenNgay"] = drn["DenNgay"].ToString().Trim();
            dr["HopDong_ID"] = drn["HopDong_ID"].ToString().Trim();
            dr["MaNPL"] = drn["MaNPL"].ToString().Trim();
            dr["TenNPL"] = drn["TenNPL"].ToString();
            dr["SoToKhaiNhap"] = drn["SoToKhai"].ToString();
            dr["NgayDangKyNhap"] = Convert.ToDateTime(drn["NgayDangKy"]);
            dr["NgayHoanThanhNhap"] = Convert.ToDateTime(drn["NgayHoanThanh"]);
            dr["MaLoaiHinhNhap"] = Convert.ToString(drn["MaLoaiHinh"]);
            dr["LuongNhap"] = Convert.ToDecimal(drn["Luong"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
            dr["LuongTonDau"] = string.IsNullOrEmpty(Convert.ToDecimal(drn["TonDau"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue))) ? "0" : Convert.ToDecimal(drn["TonDau"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
            dr["TenDVT_NPL"] = Convert.ToString(drn["TenDVT_NPL"]);
            dr["DonGiaTT"] = Convert.ToDouble(drn["DonGiaTT"]);
            dr["TyGiaTT"] = Convert.ToDecimal(drn["TyGiaTT"]);
            dr["ThueSuat"] = Convert.ToDecimal(drn["ThueSuat"]);
            dr["ThueXNK"] = Convert.ToDouble(drn["ThueXNK"]);
            dr["ThueXNKTon"] = Convert.ToDouble(drn["TonDauThueXNK"]);

            dr["MaSP"] = drx["MaSP"].ToString();
            dr["TenSP"] = drx["TenSP"].ToString();
            dr["SoToKhaiXuat"] = drx["SoToKhai"].ToString();
            dr["NgayDangKyXuat"] = Convert.ToDateTime(drx["NgayDangKy"]);
            //
            dr["NgayHoanThanhXuat"] = Convert.ToDateTime(drx["NgayHoanThanhXuat"]);
            dr["NgayThucXuat"] = Convert.ToDateTime(drx["NgayThucXuat"]);

            dr["MaLoaiHinhXuat"] = Convert.ToString(drx["MaLoaiHinh"]);
            dr["LuongSPXuat"] = Convert.ToDecimal(drx["LuongSP"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
            dr["LuongNPLSuDung"] = Convert.ToDecimal(drx["LuongNPL"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
            dr["TenDVT_SP"] = Convert.ToString(drx["TenDVT_SP"]);
            dr["DinhMuc"] = Convert.ToDecimal(drx["DinhMuc"]);
            dr["LuongTonCuoi"] = Convert.ToDecimal(drn["TonCuoi"]) - Convert.ToDecimal(drx["TonNPL"]);

            dr["ThanhKhoanTiep"] = "Mua tại VN";
            dtSource.Rows.Add(dr);
        }
        private void InsertBCXNT(List<KDT_GC_BCXuatNhapTon> bcCollection, SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                KDT_GC_BCXuatNhapTon bc = new KDT_GC_BCXuatNhapTon();
                bc.DeleteDynamicTransaction(transaction, this.MaDoanhNghiep,HopDong_ID);
                bc.InsertTransaction(transaction, bcCollection);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        private DataRow GetDataRow(DataTable dtSource, DataRow drxnt)
        {
            DataRow dr = dtSource.NewRow();
            dr["MaDoanhNghiep"] = drxnt["MaDoanhNghiep"];
            dr["STT"] = 0;
            dr["TuNgay"] = drxnt["TuNgay"].ToString().Trim();
            dr["DenNgay"] = drxnt["DenNgay"].ToString().Trim();
            dr["HopDong_ID"] = drxnt["HopDong_ID"].ToString().Trim();
            dr["MaNPL"] = drxnt["MaNPL"];
            dr["TenNPL"] = drxnt["TenNPL"];
            dr["SoToKhaiNhap"] = drxnt["SoToKhaiNhap"];
            dr["NgayDangKyNhap"] = drxnt["NgayDangKyNhap"];
            dr["NgayHoanThanhNhap"] = drxnt["NgayHoanThanhNhap"];
            dr["MaLoaiHinhNhap"] = drxnt["MaLoaiHinhNhap"];
            dr["LuongNhap"] = drxnt["LuongNhap"];
            dr["LuongTonDau"] = drxnt["LuongTonDau"];
            dr["TenDVT_NPL"] = drxnt["TenDVT_NPL"];
            dr["DonGiaTT"] = drxnt["DonGiaTT"];
            dr["TyGiaTT"] = drxnt["TyGiaTT"];
            dr["ThueSuat"] = drxnt["ThueSuat"];
            dr["ThueXNK"] = drxnt["ThueXNK"];
            dr["ThueXNKTon"] = drxnt["ThueXNKTon"];

            dr["MaSP"] = drxnt["MaSP"];
            dr["TenSP"] = drxnt["TenSP"];
            dr["SoToKhaiXuat"] = drxnt["SoToKhaiXuat"];
            dr["NgayDangKyXuat"] = drxnt["NgayDangKyXuat"];
            dr["NgayHoanThanhXuat"] = drxnt["NgayHoanThanhXuat"];
            dr["NgayThucXuat"] = drxnt["NgayThucXuat"];
            dr["MaLoaiHinhXuat"] = drxnt["MaLoaiHinhXuat"];
            dr["LuongSPXuat"] = drxnt["LuongSPXuat"];
            dr["LuongNPLSuDung"] = drxnt["LuongNPLSuDung"];
            dr["TenDVT_SP"] = drxnt["TenDVT_SP"];
            dr["DinhMuc"] = drxnt["DinhMuc"];
            dr["LuongTonCuoi"] = drxnt["LuongTonCuoi"];
            dr["ThanhKhoanTiep"] = drxnt["ThanhKhoanTiep"];

            return dr;
        }

        private DataRow SearchDataTinhAmTK(DataTable dtTonCuoi, long sotkn, string malhn, DateTime ngayhtn, string npl)
        {
            for (int i = 0; i < dtTonCuoi.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtTonCuoi.Rows[i]["TKN"]) == sotkn
                    && dtTonCuoi.Rows[i]["MaLHN"].ToString() == malhn
                    && Convert.ToDateTime(dtTonCuoi.Rows[i]["NgayHTN"]).ToShortDateString() == ngayhtn.ToShortDateString()
                    && dtTonCuoi.Rows[i]["MaNPL"].ToString().ToLower() == npl.ToLower())
                {
                    return dtTonCuoi.Rows[i];
                }
            }

            return null;
        }

        private bool SearchStringInList(List<string> listString, string value)
        {
            foreach (string item in listString)
            {
                if (item.Equals(value))
                {
                    return true;
                }
            }

            return false;
        }

        private int CheckMuaTaiVN(DataView dv, int i, string maNPL, string soToKhaiXuat, string maSP)
        {
            for (int j = i; j < dv.Count; j++)
            {
                DataRowView rv2 = dv[j];
                if (rv2["MaNPL"].ToString().ToLower().Trim() == maNPL && soToKhaiXuat == rv2["SoToKhaiXuat"].ToString() && rv2["MaSP"].ToString() == maSP)
                {
                    return j;
                }
            }
            return 0;
        }
        private bool CheckNPLSau(DataTable dt, string MaNPL, int i, DateTime ngayXuat)
        {
            for (int k = i + 1; k < dt.Rows.Count; k++)
            {
                if (dt.Rows[k]["MaNPL"].ToString().ToLower().Trim() == MaNPL.ToLower().Trim()
                    && System.Convert.ToDateTime(System.Convert.ToDateTime(dt.Rows[k]["NgayHoanThanh"]).ToShortDateString()) <= System.Convert.ToDateTime(ngayXuat.ToShortDateString()) //Khong so sanh GIỜ & chenh lech ngay
                    && System.Convert.ToDecimal(dt.Rows[k]["TonCuoi"]) >= 0) //Cap nhat NgayThucNhap = NgayHoanThanh
                {
                    return false;
                }
            }
            return true;

        }
        private bool GetTKNKeTiep(DataTable dt, int rowIndex, string MaNPL, long soToKhaiNhap, string maLoaiHinh, int namDangKy, int chenhLechNgay, DateTime ngayHoanThanhTKXuat, string maHQ)
        {
            for (int k = rowIndex + 1; k < dt.Rows.Count; k++) // Tính bắt đầu từ tờ khai nhập soToKhaiNhap tính xuống
            {
                if (System.Convert.ToInt64(dt.Rows[k]["SoToKhai"]) == soToKhaiNhap
                    && dt.Rows[k]["MaLoaiHinh"].ToString().Trim() == maLoaiHinh.Trim()
                    && System.Convert.ToDateTime(dt.Rows[k]["NgayDangKy"]).Year == namDangKy
                    && dt.Rows[k]["CoQuanHaiQuan"].ToString().Trim().ToLower() == maHQ.Trim().ToLower())
                    continue;
                else
                {
                    // Không tính chênh lệch ngày trong kiểm tra tờ khai nhập âm kế tiếp
                    if (System.Convert.ToInt64(dt.Rows[k]["SoToKhai"]) == 90) continue;
                    if (dt.Rows[k]["MaNPL"].ToString().ToLower().Trim() == MaNPL.ToLower().Trim()
                        && System.Convert.ToDateTime(dt.Rows[k]["NgayHoanThanh"]).AddDays((double)chenhLechNgay) <= ngayHoanThanhTKXuat)
                    {

                        return true;
                    }
                }
            }
            return false;

        }
    }
}