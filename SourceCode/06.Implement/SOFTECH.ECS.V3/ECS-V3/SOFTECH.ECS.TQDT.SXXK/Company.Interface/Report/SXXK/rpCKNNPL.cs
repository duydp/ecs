﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class rpCKNNPL : DevExpress.XtraReports.UI.XtraReport
    {
        //private ToKhaiMauDich TKMD = null;

        public rpCKNNPL(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            lblDoanhNghiep.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblNgaythang.Text = GlobalSettings.TieudeNgay;
            lblChiCuc1.Text = "- " + DonViHaiQuan.GetName(tkmd.MaHaiQuan).ToUpper();            
            this.dispReport(tkmd);
        }

        private void dispReport(ToKhaiMauDich TKMD)
        {
            lblHoTro.Text = lblHoTro.Text.Replace("#", GlobalSettings.TEN_DON_VI);
            lblCongTy.Text = GlobalSettings.TEN_DON_VI;
            TKMD.LoadHMDCollection();
            if (TKMD.HMDCollection.Count > 0)
            {
                HangMauDich hmd = TKMD.HMDCollection[0];
                lblTenHang.Text = hmd.TenHang;
                lblSoLuong.Text = hmd.SoLuong.ToString() + " " + DonViTinh.GetName(hmd.DVT_ID);
            }
        }

        private void lbl_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            DoiNoiDung obj = new DoiNoiDung(lbl.Text);
            obj.ShowDialog();
            if (obj.NoiDungMoi != "")
            {
                lbl.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                this.CreateDocument();
            }
        }

        private void cellSoCV_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            DoiNoiDung obj = new DoiNoiDung(cell.Text);
            obj.ShowDialog();
            if (obj.NoiDungMoi != "")
            {
                cell.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                this.CreateDocument();
            }
        }

        public void dispBackColor(bool val)
        {
            if (val)
            {
                cellSoCV.BackColor = Color.Silver;
                lblNgaythang.BackColor = Color.Silver;              
                lblChiCuc1.BackColor = Color.Silver;
                xrLabel31.BackColor = Color.Silver;
                lblHoTro.BackColor = Color.Silver;
                lblCongTy.BackColor = Color.Silver;
                lblTenHang.BackColor = Color.Silver;
                lblSoInvoice.BackColor = Color.Silver;
                lblSoBill.BackColor = Color.Silver;
                lblSoLuong.BackColor = Color.Silver;
                lblChuKy.BackColor = Color.Silver;
            }
            else
            {
                cellSoCV.BackColor = Color.Transparent;
                lblNgaythang.BackColor = Color.Transparent;               
                lblChiCuc1.BackColor = Color.Transparent;
                xrLabel31.BackColor = Color.Transparent;
                lblHoTro.BackColor = Color.Transparent;
                lblCongTy.BackColor = Color.Transparent;
                lblTenHang.BackColor = Color.Transparent;
                lblSoInvoice.BackColor = Color.Transparent;
                lblSoBill.BackColor = Color.Transparent;
                lblSoLuong.BackColor = Color.Transparent;
                lblChuKy.BackColor = Color.Transparent;
            }
            this.CreateDocument();
        }
    }
}
