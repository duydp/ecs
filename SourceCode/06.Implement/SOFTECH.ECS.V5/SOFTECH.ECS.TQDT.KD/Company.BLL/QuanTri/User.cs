using System;
using System.Data;
using System.Data.SqlClient;
using Company.KD.BLL;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;

namespace Company.QuanTri
{
    public partial class User
    {
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public ArrayList RoleList = new ArrayList();
        private bool _Check = false;
        public bool Check
        {
            set { _Check = value; }
            get { return _Check; }
        }
        public bool Load(string user_name)
        {
            string spName = "select * from [User] where [USER_NAME]=@USER_NAME";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, user_name);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) this.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
                if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) this.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
                if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) this.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
                if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) this.HO_TEN = reader.GetString(reader.GetOrdinal("MO_TA"));
                if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) this.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool TestPassWord(long ID, string password)
        {
            string spName = "select * from [User] where ID=@ID and [PASSWORD]=@PASSWORD";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, password);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public ArrayList GetEffectivePermissionList()
        {
            string sql = "Select  ID from ROLE ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetUserRoles()
        {
            string sql = "select MA_NHOM from GROUPS";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetEffectivePermissionList(long UserID)
        {
            string sql = "Select distinct ID_ROLE from GROUP_ROLE where GROUP_ID in(select MA_NHOM from USER_GROUP where USER_ID=@USER_ID)";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, UserID);
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public ArrayList GetUserRoles(long UserID)
        {
            string sql = "select distinct MA_NHOM from USER_GROUP where USER_ID=@USER_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, UserID);
            ArrayList permissions = new ArrayList();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                permissions.Add(reader.GetInt64(0));
            }
            reader.Close();
            return permissions;
        }
        public void LoadRoleList()
        {
            RoleList = GetUserRoles(this.ID);
        }
        public long ValidateLogin(string UserName, string password)
        {
            string spName = "select ID from [User] where [USER_NAME]=@USER_NAME and [PASSWORD]=@PASSWORD";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, UserName);
            this.db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, password);

            object o = this.db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }

        public bool CheckUserName(string UserName)
        {
            string spName = "select ID from [User] where [USER_NAME]=@USER_NAME and ID<>@ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, UserName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this.ID);

            object o = this.db.ExecuteScalar(dbCommand);
            return (o != null);
        }
        public void InsertUpdateFull()
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID > 0)
                        this.Update(transaction);
                    else
                        this.Insert(transaction);
                    DeleteRoleTransaction(transaction);
                    for (int i = 0; i < RoleList.Count; ++i)
                    {
                        USER_GROUP ug = new USER_GROUP();
                        ug.USER_ID = this.ID;
                        ug.MA_NHOM = Convert.ToInt64(RoleList[i]);
                        ug.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int DeleteRoleTransaction(SqlTransaction transaction)
        {
            string spName = "delete from USER_GROUP where USER_ID=@USER_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, this.ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //TODO: VNACCS---------------------------------------------------------------------------------------------

        /*** VNACCS ***/

        public string VNACC_UserCode { set; get; }
        public string VNACC_UserID { set; get; }
        public string VNACC_UserPass { set; get; }
        public string VNACC_TerminalID { set; get; }
        public string VNACC_TerminalPass { set; get; }
        public string VNACC_CA { set; get; }
        public string VNACC_CAPublicKey { set; get; }
        public bool isOnline { set; get; }
        public string HostName { set; get; }
        public string IP { set; get; }

        /// <summary>
        /// Kiem tra va tu dong cap nhat trang thai thong tin Terminal theo USER dang nhap chuong trinh. Chi cho phep duy nhat 1 Terminal ID duoc phep thuc hien khai bao.
        /// </summary>
        public static void CheckAndUpdateTerminalStatus()
        {
            const string spName = "[dbo].[CheckAndUpdateTerminalStatus]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.ExecuteNonQuery(dbCommand);
        }

        /// <summary>
        /// Kiem tra co nhieu Terminal dang ket noi khai bao hay khong?. 
        /// </summary>
        /// <returns></returns>
        public static int CheckVNACCTerminalCountOnline(string terminalID)
        {
            string where = "isOnline = 1 AND VNACC_TerminalID = '" + terminalID + "'";
            return SelectCollectionDynamic_VNACC(where, "").Count;
        }

        public static int CheckVNACCTerminalCountOnline(string terminalID, string userName)
        {
            string where = "isOnline = 1 AND VNACC_TerminalID = '" + terminalID + "' and User_Name = '" + userName + "'";
            return SelectCollectionDynamic_VNACC(where, "").Count;
        }

        public static int CheckVNACCUserCodeCountOnline(string userCode)
        {
            string where = "isOnline = 1 AND VNACC_UserCode = '" + userCode + "'";
            return SelectCollectionDynamic_VNACC(where, "").Count;
        }

        public static int CheckVNACCUserCodeCountOnline(string userCode, string userName)
        {
            string where = "isOnline = 1 AND VNACC_UserCode = '" + userCode + "' and User_Name = '" + userName + "'";
            return SelectCollectionDynamic_VNACC(where, "").Count;
        }

        //---------------------------------------------------------------------------------------------
        protected static List<User> ConvertToCollection_VNACC(IDataReader reader)
        {
            List<User> collection = new List<User>();
            while (reader.Read())
            {
                User entity = new User();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) entity.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
                if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) entity.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
                if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) entity.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
                if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) entity.MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
                if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) entity.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_UserCode"))) entity.VNACC_UserCode = reader.GetString(reader.GetOrdinal("VNACC_UserCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_UserID"))) entity.VNACC_UserID = reader.GetString(reader.GetOrdinal("VNACC_UserID"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_UserPass"))) entity.VNACC_UserPass = reader.GetString(reader.GetOrdinal("VNACC_UserPass"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_TerminalID"))) entity.VNACC_TerminalID = reader.GetString(reader.GetOrdinal("VNACC_TerminalID"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_TerminalPass"))) entity.VNACC_TerminalPass = reader.GetString(reader.GetOrdinal("VNACC_TerminalPass"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_CA"))) entity.VNACC_CA = reader.GetString(reader.GetOrdinal("VNACC_CA"));
                if (!reader.IsDBNull(reader.GetOrdinal("VNACC_CAPublicKey"))) entity.VNACC_CAPublicKey = reader.GetString(reader.GetOrdinal("VNACC_CAPublicKey"));
                if (!reader.IsDBNull(reader.GetOrdinal("isOnline"))) entity.isOnline = reader.GetBoolean(reader.GetOrdinal("isOnline"));
                if (!reader.IsDBNull(reader.GetOrdinal("HostName"))) entity.HostName = reader.GetString(reader.GetOrdinal("HostName"));
                if (!reader.IsDBNull(reader.GetOrdinal("IP"))) entity.IP = reader.GetString(reader.GetOrdinal("IP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------
        public static User Load_VNACC(long id)
        {
            const string spName = "[dbo].[p_User_Load_VNACC]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<User> collection = ConvertToCollection_VNACC(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static List<User> SelectCollectionDynamic_VNACC(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic_VNACC(whereCondition, orderByExpression);
            return ConvertToCollection_VNACC(reader);
        }

        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderDynamic_VNACC(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_User_SelectDynamic_VNACC]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static int UpdateUser_VNACC(long id, string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin, string vNACC_UserCode, string vNACC_UserID, string vNACC_UserPass, string vNACC_TerminalID, string vNACC_TerminalPass, string vNACC_CA, string vNACC_CAPublicKey, bool isOnline, string hostName, string ip)
        {
            User entity = new User();
            entity.ID = id;
            entity.USER_NAME = uSER_NAME;
            entity.PASSWORD = pASSWORD;
            entity.HO_TEN = hO_TEN;
            entity.MO_TA = mO_TA;
            entity.isAdmin = isAdmin;
            entity.VNACC_UserCode = vNACC_UserCode;
            entity.VNACC_UserID = vNACC_UserID;
            entity.VNACC_UserPass = vNACC_UserPass;
            entity.VNACC_TerminalID = vNACC_TerminalID;
            entity.VNACC_TerminalPass = vNACC_TerminalPass;
            entity.VNACC_CA = vNACC_CA;
            entity.VNACC_CAPublicKey = vNACC_CAPublicKey;
            entity.isOnline = isOnline;
            entity.HostName = hostName;
            entity.IP = ip;
            return entity.Update_VNACC();
        }

        //---------------------------------------------------------------------------------------------

        public int Update_VNACC()
        {
            return this.Update_VNACC(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update_VNACC(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_User_Update_VNACC]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
            db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
            db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
            db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
            db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
            db.AddInParameter(dbCommand, "@VNACC_UserCode", SqlDbType.VarChar, VNACC_UserCode);
            db.AddInParameter(dbCommand, "@VNACC_UserID", SqlDbType.VarChar, VNACC_UserID);
            db.AddInParameter(dbCommand, "@VNACC_UserPass", SqlDbType.NVarChar, VNACC_UserPass);
            db.AddInParameter(dbCommand, "@VNACC_TerminalID", SqlDbType.VarChar, VNACC_TerminalID);
            db.AddInParameter(dbCommand, "@VNACC_TerminalPass", SqlDbType.NVarChar, VNACC_TerminalPass);
            db.AddInParameter(dbCommand, "@VNACC_CA", SqlDbType.NVarChar, VNACC_CA);
            db.AddInParameter(dbCommand, "@VNACC_CAPublicKey", SqlDbType.NVarChar, VNACC_CAPublicKey);
            db.AddInParameter(dbCommand, "@isOnline", SqlDbType.Bit, isOnline);
            db.AddInParameter(dbCommand, "@HostName", SqlDbType.VarChar, HostName);
            db.AddInParameter(dbCommand, "@IP", SqlDbType.VarChar, IP);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}