GO
IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]  
-- Database: ECS_TQDT_KD_VNACCS  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]  
 @WhereCondition NVARCHAR(MAX)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT TKMD.ID,
		SoToKhai,
		NgayDangKy,
        TKMD.MaLoaiHinh,
	    SoHoaDon,
		CASE WHEN TKMD.HopDong_ID > 0 THEN (SELECT TOP 1 SoHopDong FROM dbo.t_KDT_GC_HopDong WHERE TKMD.HopDong_ID=ID) ELSE NULL END AS HopDong_ID,
		MaPhanLoaiKiemTra,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CertificateOfOrigin WHERE TKMD_ID =TKMD.ID ) AS CertificateOfOrigin,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_BillOfLading WHERE TKMD_ID =TKMD.ID ) AS BillOfLading,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_ContractDocument WHERE TKMD_ID =TKMD.ID ) AS ContractDocument,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CommercialInvoice WHERE TKMD_ID =TKMD.ID ) AS CommercialInvoice,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_License WHERE TKMD_ID =TKMD.ID ) AS License,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_Container_Details WHERE TKMD_ID =TKMD.ID ) AS Container,
		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_AdditionalDocument WHERE TKMD_ID =TKMD.ID ) AS AdditionalDocument
		FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD 
LEFT JOIN t_KDT_VNACCS_CertificateOfOrigin CO ON CO.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_BillOfLading BL ON BL.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_ContractDocument CT ON CT.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_CommercialInvoice CM ON CM.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_License LC ON LC.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_Container_Details CTD ON CTD.TKMD_ID = TKMD.ID
LEFT JOIN t_KDT_VNACCS_AdditionalDocument AD ON AD.TKMD_ID = TKMD.ID     
WHERE  ' + @WhereCondition +  ' GROUP BY TKMD.ID,TKMD.SoToKhai,TKMD.NgayDangKy,TKMD.MaLoaiHinh,TKMD.SoHoaDon,TKMD.HopDong_ID,TKMD.MaPhanLoaiKiemTra  '
  
EXEC sp_executesql @SQL  
  
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.8',GETDATE(), N'CẬP NHẬT PROCDURE AUTO FEEDBACK CHỨNG TỪ ')
END