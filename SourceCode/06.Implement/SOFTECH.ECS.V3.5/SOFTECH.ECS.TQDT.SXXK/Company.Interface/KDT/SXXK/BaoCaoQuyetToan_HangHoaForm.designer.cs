﻿namespace Company.Interface.KDT
{
    partial class BaoCaoQuyetToan_HangHoaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoQuyetToan_HangHoaForm));
            Janus.Windows.GridEX.GridEXLayout cbbMaHangHoa_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnImportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTaiXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTonCuoiKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtXuatTrongKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtXuatKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNhapTrongKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChuyenMDSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTonDauKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbMaHangHoa = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaHangHoa)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1236, 702);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1236, 702);
            this.uiGroupBox4.TabIndex = 103;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.dgListTotal);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 313);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1236, 343);
            this.uiGroupBox6.TabIndex = 110;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(3, 8);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTotal.Size = new System.Drawing.Size(1230, 332);
            this.dgListTotal.TabIndex = 11;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTotal.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListTotal_RowDoubleClick);
            this.dgListTotal.SelectionChanged += new System.EventHandler(this.dgListTotal_SelectionChanged);
            this.dgListTotal.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTotal_FormattingRow);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnDelete);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 656);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1236, 46);
            this.uiGroupBox7.TabIndex = 109;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1144, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(1058, 15);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 23);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.Controls.Add(this.cbbMaHangHoa);
            this.uiGroupBox5.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox5.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox5.Controls.Add(this.btnImportExcel);
            this.uiGroupBox5.Controls.Add(this.btnAdd);
            this.uiGroupBox5.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox5.Controls.Add(this.txtGhiChu);
            this.uiGroupBox5.Controls.Add(this.label13);
            this.uiGroupBox5.Controls.Add(this.label14);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Controls.Add(this.label16);
            this.uiGroupBox5.Controls.Add(this.txtTaiXuat);
            this.uiGroupBox5.Controls.Add(this.txtTonCuoiKy);
            this.uiGroupBox5.Controls.Add(this.txtXuatTrongKy);
            this.uiGroupBox5.Controls.Add(this.txtXuatKhac);
            this.uiGroupBox5.Controls.Add(this.txtNhapTrongKy);
            this.uiGroupBox5.Controls.Add(this.txtChuyenMDSD);
            this.uiGroupBox5.Controls.Add(this.txtTonDauKy);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Controls.Add(this.label22);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1236, 313);
            this.uiGroupBox5.TabIndex = 107;
            this.uiGroupBox5.Text = "Thông tin hàng hóa";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(122, 55);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(95, 21);
            this.ctrDVTLuong1.TabIndex = 5;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnImportExcel.Image")));
            this.btnImportExcel.Location = new System.Drawing.Point(223, 241);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(145, 23);
            this.btnImportExcel.TabIndex = 14;
            this.btnImportExcel.Text = "Nhập hàng hóa từ Excel";
            this.btnImportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(122, 241);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 23);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.BackColor = System.Drawing.Color.White;
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(445, 20);
            this.txtTenHangHoa.Multiline = true;
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(536, 56);
            this.txtTenHangHoa.TabIndex = 4;
            this.txtTenHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(122, 212);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(866, 21);
            this.txtGhiChu.TabIndex = 13;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 216);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ghi chú :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(359, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tên hàng hóa :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã hàng hóa :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Đơn vị tính :";
            // 
            // txtTaiXuat
            // 
            this.txtTaiXuat.DecimalDigits = 4;
            this.txtTaiXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaiXuat.Location = new System.Drawing.Point(122, 132);
            this.txtTaiXuat.Name = "txtTaiXuat";
            this.txtTaiXuat.Size = new System.Drawing.Size(160, 21);
            this.txtTaiXuat.TabIndex = 8;
            this.txtTaiXuat.Text = "0.0000";
            this.txtTaiXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTaiXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTaiXuat.TextChanged += new System.EventHandler(this.txtTaiXuat_TextChanged);
            // 
            // txtTonCuoiKy
            // 
            this.txtTonCuoiKy.DecimalDigits = 4;
            this.txtTonCuoiKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTonCuoiKy.Location = new System.Drawing.Point(741, 172);
            this.txtTonCuoiKy.Name = "txtTonCuoiKy";
            this.txtTonCuoiKy.ReadOnly = true;
            this.txtTonCuoiKy.Size = new System.Drawing.Size(160, 21);
            this.txtTonCuoiKy.TabIndex = 12;
            this.txtTonCuoiKy.Text = "0.0000";
            this.txtTonCuoiKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTonCuoiKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtXuatTrongKy
            // 
            this.txtXuatTrongKy.DecimalDigits = 4;
            this.txtXuatTrongKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatTrongKy.Location = new System.Drawing.Point(122, 172);
            this.txtXuatTrongKy.Name = "txtXuatTrongKy";
            this.txtXuatTrongKy.Size = new System.Drawing.Size(160, 21);
            this.txtXuatTrongKy.TabIndex = 10;
            this.txtXuatTrongKy.Text = "0.0000";
            this.txtXuatTrongKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtXuatTrongKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtXuatTrongKy.TextChanged += new System.EventHandler(this.txtXuatTrongKy_TextChanged);
            // 
            // txtXuatKhac
            // 
            this.txtXuatKhac.DecimalDigits = 4;
            this.txtXuatKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatKhac.Location = new System.Drawing.Point(445, 172);
            this.txtXuatKhac.Name = "txtXuatKhac";
            this.txtXuatKhac.Size = new System.Drawing.Size(160, 21);
            this.txtXuatKhac.TabIndex = 11;
            this.txtXuatKhac.Text = "0.0000";
            this.txtXuatKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtXuatKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtXuatKhac.TextChanged += new System.EventHandler(this.txtXuatKhac_TextChanged);
            // 
            // txtNhapTrongKy
            // 
            this.txtNhapTrongKy.DecimalDigits = 4;
            this.txtNhapTrongKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNhapTrongKy.Location = new System.Drawing.Point(445, 92);
            this.txtNhapTrongKy.Name = "txtNhapTrongKy";
            this.txtNhapTrongKy.Size = new System.Drawing.Size(160, 21);
            this.txtNhapTrongKy.TabIndex = 7;
            this.txtNhapTrongKy.Text = "0.0000";
            this.txtNhapTrongKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtNhapTrongKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNhapTrongKy.TextChanged += new System.EventHandler(this.txtNhapTrongKy_TextChanged);
            // 
            // txtChuyenMDSD
            // 
            this.txtChuyenMDSD.DecimalDigits = 4;
            this.txtChuyenMDSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuyenMDSD.Location = new System.Drawing.Point(445, 131);
            this.txtChuyenMDSD.Name = "txtChuyenMDSD";
            this.txtChuyenMDSD.Size = new System.Drawing.Size(160, 21);
            this.txtChuyenMDSD.TabIndex = 9;
            this.txtChuyenMDSD.Text = "0.0000";
            this.txtChuyenMDSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtChuyenMDSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChuyenMDSD.TextChanged += new System.EventHandler(this.txtChuyenMDSD_TextChanged);
            // 
            // txtTonDauKy
            // 
            this.txtTonDauKy.DecimalDigits = 4;
            this.txtTonDauKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTonDauKy.Location = new System.Drawing.Point(122, 92);
            this.txtTonDauKy.Name = "txtTonDauKy";
            this.txtTonDauKy.Size = new System.Drawing.Size(160, 21);
            this.txtTonDauKy.TabIndex = 6;
            this.txtTonDauKy.Text = "0.0000";
            this.txtTonDauKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTonDauKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTonDauKy.TextChanged += new System.EventHandler(this.txtTonDauKy_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(638, 176);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tồn cuối kỳ :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(304, 176);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Xuất khác : ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(19, 176);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Xuất trong kỳ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(304, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chuyển mục đích sử dụng :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tái xuất :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(304, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhập trong kỳ :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tồn đầu kỳ :";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnTimKiem);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtMaHang);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 268);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1230, 42);
            this.uiGroupBox3.TabIndex = 287;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.Location = new System.Drawing.Point(494, 13);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(89, 23);
            this.btnTimKiem.TabIndex = 87;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 86;
            this.label5.Text = "Mã hàng hóa";
            // 
            // txtMaHang
            // 
            this.txtMaHang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHang.BackColor = System.Drawing.Color.White;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(119, 14);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(369, 21);
            this.txtMaHang.TabIndex = 85;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHang.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // cbbMaHangHoa
            // 
            cbbMaHangHoa_DesignTimeLayout.LayoutString = resources.GetString("cbbMaHangHoa_DesignTimeLayout.LayoutString");
            this.cbbMaHangHoa.DesignTimeLayout = cbbMaHangHoa_DesignTimeLayout;
            this.cbbMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaHangHoa.Location = new System.Drawing.Point(122, 19);
            this.cbbMaHangHoa.Name = "cbbMaHangHoa";
            this.cbbMaHangHoa.SelectedIndex = -1;
            this.cbbMaHangHoa.SelectedItem = null;
            this.cbbMaHangHoa.Size = new System.Drawing.Size(231, 21);
            this.cbbMaHangHoa.TabIndex = 288;
            this.cbbMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaHangHoa.ValueChanged += new System.EventHandler(this.cbbMaHangHoa_ValueChanged);
            // 
            // BaoCaoQuyetToan_HangHoaForm
            // 
            this.ClientSize = new System.Drawing.Size(1236, 702);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MinimumSize = new System.Drawing.Size(1244, 733);
            this.Name = "BaoCaoQuyetToan_HangHoaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách hàng hóa Báo cáo quyết toán";
            this.Load += new System.EventHandler(this.BaoCaoQuyetToan_HangHoaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaHangHoa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTaiXuat;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTonCuoiKy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtXuatTrongKy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtXuatKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTonDauKy;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNhapTrongKy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChuyenMDSD;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnImportExcel;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHang;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaHangHoa;
    }
}
