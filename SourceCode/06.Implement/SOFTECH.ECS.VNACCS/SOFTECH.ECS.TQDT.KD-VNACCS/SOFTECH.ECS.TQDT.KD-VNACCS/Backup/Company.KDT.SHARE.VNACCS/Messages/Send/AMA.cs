﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class AMA : BasicVNACC
    {
        public List<AMA_HANGHOA> ListHangHoa = new List<AMA_HANGHOA>();

        public StringBuilder BuilEdiMessagesSFA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<AMA>(StrBuild, true, GlobalVNACC.PathConfig, "AMA");
            foreach (AMA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<AMA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "AMA_HANGHOA");
            }
            return str;
        }

    }
}
