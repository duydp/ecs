﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class CungUngNPLForm : BaseForm
    {
        public KDT_GC_CungUngDangKy CungUngDK = new KDT_GC_CungUngDangKy();
        public KDT_GC_CungUng CungUngNPL = new KDT_GC_CungUng();
        public long HopDong_ID;
        bool isAuto = false;
        bool isEdit = false;
        public CungUngNPLForm()
        {
            InitializeComponent();
        }
        private void CungUngForm_Load(object sender, EventArgs e)
        {
            txtSoLuong.ReadOnly = false;
            btnThem.Enabled = true;
            LoadData();
        }
        private void LoadData()
        {
            txtMaNPL.Text = CungUngNPL.MaNPL;
            txtTenNPL.Text = CungUngNPL.TenNPL;
            txtSoLuong.Text = CungUngNPL.LuongCungUng.ToString();
            txtThanhTien.Text = CungUngNPL.ThanhTien.ToString();
            ucCategoryAllowEmpty1.Code = CungUngNPL.DVT_ID;
           // txtSoLuong.Text = CungUngNPL.LuongCungUng.ToString();
            dgList.DataSource = CungUngNPL.CungUngDetail_List;
            dgList.Refetch();
            //decimal tongluongNPL = 0;
           
            //foreach ( GridEXRow item in dgList.GetDataRows())
            //{
            //    if(item.RowType== RowType.Record)
            //    tongluongNPL = tongluongNPL + Convert.ToDecimal(item.Cells["SoLuongNPL"].Text);
            //}
            //txtSoLuong.Value = tongluongNPL;
        }
        private void SetCommand()
        {
            int TrangThai = CungUngDK.TrangThaiXuLy;
            //if (CungUngDK.TrangThaiXuLy == -1 || CungUngDK.TrangThaiXuLy == 2)
            //{
            //    cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    btnXoa.Enabled = true;
            //}
            //else
            //{
            //    cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    btnXoa.Enabled = false;
            //}

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_CungUng_Detail> ItemColl = new List<KDT_GC_CungUng_Detail>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa Sản phẩm này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_CungUng_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_CungUng_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            KDT_GC_CungUng_ChungTu.DeleteDynamic("Master_ID = " + item.ID);
                            item.Delete();
                        }
                        CungUngNPL.CungUngDetail_List.Remove(item);
                    }
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                KDT_GC_CungUng_Detail cungUngDetail = new KDT_GC_CungUng_Detail();
                cungUngDetail = (KDT_GC_CungUng_Detail)dgList.CurrentRow.DataRow;
                CungUngDetailForm f = new CungUngDetailForm();
                f.CU_Detail = cungUngDetail;
                f.MaNPL = CungUngNPL.MaNPL;
                f.HopDong_ID = CungUngDK.HopDong_ID;
                f.tragThai = CungUngDK.TrangThaiXuLy;
                f.ShowDialog();
                LoadData();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            CungUngSelectTKForm f = new CungUngSelectTKForm();
            f.CungUng = CungUngNPL;
            f.IDHopDong = CungUngDK.HopDong_ID;
            f.ShowDialog();
            LoadData();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (CungUngDK.ID == 0)
                {
                    CungUngDK.TrangThaiXuLy = -1;
                    CungUngDK.Insert();
                }
                else
                    CungUngDK.Update();
                CungUngNPL.Master_ID = CungUngDK.ID;
                CungUngNPL.LuongCungUng = Convert.ToDecimal(txtSoLuong.Value);
                CungUngNPL.ThanhTien = Convert.ToDecimal(txtThanhTien.Value);
                //bỏ thêm cũng ứng chi tiết
                //if (CungUngNPL.ID == 0)
                //    CungUngDK.NPLCungUng_List.Add(CungUngNPL);
                //CungUngNPL.InsertUpdatFull();
                CungUngNPL.InsertUpdate();
                
                ShowMessage("Lưu thành công.", false);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công :"+ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                KDT_GC_CungUng_Detail cu = new KDT_GC_CungUng_Detail();
                cu = (KDT_GC_CungUng_Detail)e.Row.DataRow;
                decimal luong = 0;
                foreach (KDT_GC_CungUng_ChungTu item in cu.CungUngCT_List)
                {
                    luong = luong + Convert.ToDecimal( item.SoLuong);
                    
                }
                if (luong==0)
                {
                    luong = cu.SoLuong;
                    e.Row.Cells["SoLuongNPL"].Text = Convert.ToString(luong);
                }
                else
                {
                    e.Row.Cells["SoLuongNPL"].Text = Convert.ToString(luong);
                }
              

            }
           

        }
     
        
    }
}
