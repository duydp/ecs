﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.GC
{
    public partial class BaoCaoQuyetToanMMTB_HangHoaForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public List<HopDong> HDCollection = new List<HopDong>();
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem = new KDT_VNACCS_BaoCaoQuyetToan_MMTB();
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail goodItemHDDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
        public bool isAdd = true;
        public bool isExits = false;
        public ThietBiRegistedForm TBRegistedForm;
        public BaoCaoQuyetToanMMTB_HangHoaForm()
        {
            InitializeComponent();
        }
        private void Bind()
        {
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                try
                {
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                    collection.Add(hd);
                    cbHopDong.DataSource = collection;
                    cbHopDong.DisplayMember = "SoHopDong";
                    cbHopDong.ValueMember = "ID";
                    cbHopDong.SelectedIndex = collection.Count - 1;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        private void BaoCaoQuyetToanMMTB_HangHoaForm_Load(object sender, EventArgs e)
        {
            Bind();
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnAddHD.Enabled = false;
                btnDelete.Enabled = false;
                btnDeleteHD.Enabled = false;
                btnImportExcel.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
                btnAddHD.Enabled = true;
                btnDelete.Enabled = true;
                btnDeleteHD.Enabled = true;
                btnImportExcel.Enabled = true;
            }
            cbHopDong.Enabled = false;
            BindHopDong();
        }
        private void BindHopDong()
        {
            try
            {
                dgListHopDong.Refresh();
                dgListHopDong.DataSource = goodItem.GoodItemCollection;
                dgListHopDong.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataHangHoa()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItemHDDetail.GoodItemDetailCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAddHD_Click(object sender, EventArgs e)
        {
            try
            {
                HopDongManageForm f = new HopDongManageForm();
                f.isQuyetToan = true;
                f.isTaoHoSo = true;
                f.IsBrowseForm = true;
                f.ShowDialog();
                HDCollection = f.list_HopDong;
                if (HDCollection.Count > 0)
                {
                    foreach (HopDong items in HDCollection)
                    {
                        foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                        {
                            if (items.ID == item.ID_HopDong)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            goodItemHDDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
                            goodItemHDDetail.ID_HopDong = items.ID;
                            goodItemHDDetail.SoHopDong = items.SoHopDong;
                            goodItemHDDetail.NgayHopDong = items.NgayKy;
                            goodItemHDDetail.NgayHetHan = items.NgayGiaHan.Year == 1900 ? items.NgayHetHan : items.NgayGiaHan;
                            goodItemHDDetail.MaHQ = items.MaHaiQuan;
                            goodItem.GoodItemCollection.Add(goodItemHDDetail);
                            int k = 1;
                            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                            {
                                item.STT = k;
                                k++;
                            }
                        }
                    }
                    BindHopDong();
                }
                else
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào báo cáo quyết toán .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteHD_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListHopDong.SelectedItems;
                List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> ItemColl = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail>();
                if (dgListHopDong.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa Hợp đồng này khỏi Báo cáo quyết toán không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                             item.DeleteFull();
                        goodItem.GoodItemCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindHopDong();
                    goodItemHDDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
                    BindDataHangHoa();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListHopDong_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListHopDong.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        goodItemHDDetail = (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail)i.GetRow().DataRow;
                        BindDataHangHoa();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            this.TBRegistedForm = new ThietBiRegistedForm();
            this.TBRegistedForm.isBrower = true;
            this.TBRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            this.TBRegistedForm.ThietBiSelected.HopDong_ID = goodItemDetail.ID_HopDong;
            this.TBRegistedForm.ShowDialog();
            if (!string.IsNullOrEmpty(this.TBRegistedForm.ThietBiSelected.Ma))
            {
                txtMaHangHoa.Text = this.TBRegistedForm.ThietBiSelected.Ma;
                txtTenHangHoa.Text = this.TBRegistedForm.ThietBiSelected.Ten;
                ctrDVTLuong1.Code = this.DVT_VNACC(this.TBRegistedForm.ThietBiSelected.DVT_ID.PadRight(3));
                ctrMaSoHang.Code = this.TBRegistedForm.ThietBiSelected.MaHS;
            }
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, " Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, " Tên hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrDVTLuong1, errorProvider, " Đơn vị tính ");

                //isValid &= ValidateControl.ValidateNull(txtTonDauKy, errorProvider, " Tồn đầu kỳ ");
                isValid &= ValidateControl.ValidateNull(txtNhapTrongKy, errorProvider, " Lượng tạm nhập ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtTaiXuat, errorProvider, " Tái xuất ");
                //isValid &= ValidateControl.ValidateNull(txtChuyenMDSD, errorProvider, " Chuyển mục đích sử dụng ");
                //isValid &= ValidateControl.ValidateNull(txtXuatKhac, errorProvider, " Xuất khác ");
                //isValid &= ValidateControl.ValidateNull(txtXuatTrongKy, errorProvider, " Xuất trong kỳ ");
                //isValid &= ValidateControl.ValidateNull(txtTonCuoiKy, errorProvider, " Tồn cuối kỳ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetHangHoa()
        {
            try
            {
                if (goodItemDetail == null)
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                goodItemDetail.ID_HopDong = goodItemHDDetail.ID_HopDong;
                goodItemDetail.MaHangHoa = txtMaHangHoa.Text.ToString();
                goodItemDetail.TenHangHoa = txtTenHangHoa.Text.ToString();
                goodItemDetail.MaHS = Convert.ToDecimal(ctrMaSoHang.Code.ToString());
                goodItemDetail.DVT = ctrDVTLuong1.Code.ToString();
                goodItemDetail.LuongTamNhap = Convert.ToDecimal(txtNhapTrongKy.Value.ToString());
                goodItemDetail.LuongTaiXuat = Convert.ToDecimal(txtTaiXuat.Value.ToString());
                goodItemDetail.LuongChuyenTiep = Convert.ToDecimal(txtChuyenMDSD.Value.ToString());
                if (goodItemDetail.LuongTaiXuat >= 1)
                {
                    goodItemDetail.SoHopDong = cbHopDong.Text.ToString();
                    goodItemDetail.NgayHopDong = clcNgayHD.Value;
                    goodItemDetail.NgayHetHan = clcNgayHD.Value;
                    goodItemDetail.MaHQ = GlobalSettings.MA_HAI_QUAN;
                }
                else
                {
                    goodItemDetail.SoHopDong = String.Empty;
                    goodItemDetail.NgayHopDong = DateTime.Now;
                    goodItemDetail.NgayHetHan = DateTime.Now;
                    goodItemDetail.MaHQ = GlobalSettings.MA_HAI_QUAN;
                }
                goodItemDetail.LuongConLai = Convert.ToDecimal(txtTonCuoiKy.Value.ToString());
                goodItemDetail.GhiChu = txtGhiChu.Text.ToString();
                if (isAdd)
                {
                    goodItemHDDetail.GoodItemDetailCollection.Add(goodItemDetail);
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in goodItemHDDetail.GoodItemDetailCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                }
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                isAdd = true;
                txtMaHangHoa.Text = String.Empty;
                txtTenHangHoa.Text = String.Empty;
                ctrDVTLuong1.Code = String.Empty;
                txtNhapTrongKy.Value = String.Empty;
                txtTaiXuat.Value = String.Empty;
                txtChuyenMDSD.Value = String.Empty;
                txtTonCuoiKy.Value = String.Empty;
                txtGhiChu.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetHangHoa()
        {
            try
            {
                txtMaHangHoa.Text = goodItemDetail.MaHangHoa;
                txtTenHangHoa.Text = goodItemDetail.TenHangHoa;
                ctrDVTLuong1.Code = goodItemDetail.DVT;
                ctrMaSoHang.Code = goodItemDetail.MaHS.ToString();
                txtNhapTrongKy.Value = goodItemDetail.LuongTamNhap;
                txtTaiXuat.Value = goodItemDetail.LuongTaiXuat;
                if (goodItemDetail.LuongTaiXuat>=1)
                {
                    cbHopDong.Text = goodItemDetail.SoHopDong;
                    clcNgayHD.Value = goodItemDetail.NgayHopDong;
                    clcNgayHH.Value = goodItemDetail.NgayHetHan;
                }
                else
                {
                    clcNgayHD.Value = DateTime.Now;
                    clcNgayHH.Value = DateTime.Now;
                }
                txtChuyenMDSD.Value = goodItemDetail.LuongChuyenTiep;
                txtTonCuoiKy.Value = goodItemDetail.LuongConLai;
                txtGhiChu.Text = goodItemDetail.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (goodItemHDDetail == null)
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào Báo cáo quyết toán", false);
                    return;
                }
                GetHangHoa();
                BindDataHangHoa();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            ReadExcelFormMMTB f = new ReadExcelFormMMTB();
            f.goodItem = goodItem;
            f.goodItemHD = goodItemHDDetail;
            f.ShowDialog(this);
            BindHopDong();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail> ItemColl = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail>();
                if (dgListTotal.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        goodItemHDDetail.GoodItemDetailCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in goodItemHDDetail.GoodItemDetailCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindDataHangHoa();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListTotal_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                goodItemDetail = (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail)e.Row.DataRow;
                SetHangHoa();
                isAdd = false;
            }
        }

        private void dgListTotal_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            goodItemDetail = (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail)i.GetRow().DataRow;
                            SetHangHoa();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNhapTrongKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtChuyenMDSD_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTaiXuat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value);
                if (Convert.ToDecimal(txtTaiXuat.Value) >= 1)
                {
                    cbHopDong.Enabled = true;
                }
                else
                {
                    cbHopDong.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbHopDong.Value!=null)
                {
                    HopDong HD = new HopDong();
                    HD = HD.LoadHD(Convert.ToInt64(cbHopDong.Value));
                    clcNgayHD.Value = HD.NgayKy;
                    clcNgayHH.Value = HD.NgayGiaHan.Year==1900 ? HD.NgayHetHan : HD.NgayGiaHan;
                }
            }
            catch (Exception ex)
            {                
              Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
