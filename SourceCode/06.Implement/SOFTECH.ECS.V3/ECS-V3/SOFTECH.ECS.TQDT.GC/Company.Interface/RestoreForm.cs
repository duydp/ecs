﻿using System;
using System.Windows.Forms;
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
using System.Threading;
using System.Data;
using SQLDMO;
using System.Configuration;
namespace Company.Interface
{
    public partial class RestoreForm : BaseForm
    {             
        SQLDMO.RestoreClass restoreClass = new SQLDMO.RestoreClass();
        public RestoreForm()
        {
            InitializeComponent();
        }

        private void BackUpAndReStoreForm_Load(object sender, EventArgs e)
        {                       
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (editBox1.Text.Length <= 0)
            {
                showMsg("MSG_MAL07");
                //ShowMessage("Đường dẫn tên file không được để rỗng!", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
                server.Connect(GlobalSettings.SERVER_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                restoreClass.Action = SQLDMO.SQLDMO_RESTORE_TYPE.SQLDMORestore_Database;
                restoreClass.Database = GlobalSettings.DATABASE_NAME;
                restoreClass.Files = editBox1.Text.Trim();
                restoreClass.SQLRestore(server);
                showMsg("MSG_0203040");
                //ShowMessage("Phục hồi dữ liệu thành công.", false);
                server.DisConnect();
                this.Cursor = Cursors.Default;
                this.Close();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //string    st = "Lỗi khi phục hồi dữ liệu.";
                //ShowMessage(st+" "+ex.Message, false);
                showMsg("MSG_0203042", ex.Message);
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
      
  
    }
}