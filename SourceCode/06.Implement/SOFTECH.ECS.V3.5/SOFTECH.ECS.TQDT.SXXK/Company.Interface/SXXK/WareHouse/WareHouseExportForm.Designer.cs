﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout cbbMaKho_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseExportForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayTK = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayHoaDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.txtTongTriGiaHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTongLuongHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbMaKho = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNguoiGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienGiai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAdd1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdAddExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddExcel");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdAdd = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdAddExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdAddExcel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbMaNT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTyGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayCT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiChungTu = new Janus.Windows.EditControls.UIComboBox();
            this.clcNgayCTGoc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label18 = new System.Windows.Forms.Label();
            this.cbbLoaiHang = new Janus.Windows.EditControls.UIComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoCTGoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChiPhiKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiVC = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbbPhanBo = new Janus.Windows.EditControls.UIComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 732), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 732);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 708);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 708);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(995, 732);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.clcNgayTK);
            this.uiGroupBox3.Controls.Add(this.clcNgayHoaDon);
            this.uiGroupBox3.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.txtSoTK);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(995, 86);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin hợp đồng";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // clcNgayTK
            // 
            this.clcNgayTK.CustomFormat = "dd/MM/yyyy";
            this.clcNgayTK.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayTK.DropDownCalendar.Name = "";
            this.clcNgayTK.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTK.Location = new System.Drawing.Point(130, 56);
            this.clcNgayTK.Name = "clcNgayTK";
            this.clcNgayTK.ReadOnly = true;
            this.clcNgayTK.Size = new System.Drawing.Size(185, 21);
            this.clcNgayTK.TabIndex = 2;
            this.clcNgayTK.Value = new System.DateTime(2018, 11, 11, 18, 39, 31, 0);
            this.clcNgayTK.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayHoaDon
            // 
            this.clcNgayHoaDon.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHoaDon.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHoaDon.DropDownCalendar.Name = "";
            this.clcNgayHoaDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHoaDon.Location = new System.Drawing.Point(415, 56);
            this.clcNgayHoaDon.Name = "clcNgayHoaDon";
            this.clcNgayHoaDon.Size = new System.Drawing.Size(177, 21);
            this.clcNgayHoaDon.TabIndex = 4;
            this.clcNgayHoaDon.Value = new System.DateTime(2018, 11, 13, 0, 0, 0, 0);
            this.clcNgayHoaDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHoaDon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHoaDon.BackColor = System.Drawing.Color.White;
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(415, 24);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(177, 21);
            this.txtSoHoaDon.TabIndex = 3;
            this.txtSoHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(24, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ngày tờ khai";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(321, 60);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Ngày hóa đơn :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(321, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số hóa đơn :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số tờ khai :";
            // 
            // txtSoTK
            // 
            this.txtSoTK.BackColor = System.Drawing.Color.White;
            this.txtSoTK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.Location = new System.Drawing.Point(130, 24);
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(185, 21);
            this.txtSoTK.TabIndex = 1;
            this.txtSoTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTK.ButtonClick += new System.EventHandler(this.txtSoTK_ButtonClick);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Controls.Add(this.txtTongTriGiaHang);
            this.uiGroupBox5.Controls.Add(this.label21);
            this.uiGroupBox5.Controls.Add(this.txtTongLuongHang);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 685);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(995, 47);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(903, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtTongTriGiaHang
            // 
            this.txtTongTriGiaHang.DecimalDigits = 4;
            this.txtTongTriGiaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHang.Location = new System.Drawing.Point(430, 17);
            this.txtTongTriGiaHang.Name = "txtTongTriGiaHang";
            this.txtTongTriGiaHang.ReadOnly = true;
            this.txtTongTriGiaHang.Size = new System.Drawing.Size(140, 21);
            this.txtTongTriGiaHang.TabIndex = 21;
            this.txtTongTriGiaHang.Text = "0.0000";
            this.txtTongTriGiaHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTongTriGiaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(304, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Tổng trị giá hàng :";
            // 
            // txtTongLuongHang
            // 
            this.txtTongLuongHang.DecimalDigits = 4;
            this.txtTongLuongHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongLuongHang.Location = new System.Drawing.Point(139, 17);
            this.txtTongLuongHang.Name = "txtTongLuongHang";
            this.txtTongLuongHang.ReadOnly = true;
            this.txtTongLuongHang.Size = new System.Drawing.Size(140, 21);
            this.txtTongLuongHang.TabIndex = 21;
            this.txtTongLuongHang.Text = "0.0000";
            this.txtTongLuongHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTongLuongHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(13, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tổng lượng hàng :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbbMaKho);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.txtNguoiGiao);
            this.uiGroupBox2.Controls.Add(this.txtTenKho);
            this.uiGroupBox2.Controls.Add(this.txtDienGiai);
            this.uiGroupBox2.Controls.Add(this.txtDiaChi);
            this.uiGroupBox2.Controls.Add(this.txtMa);
            this.uiGroupBox2.Controls.Add(this.txtTen);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(600, 258);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbMaKho
            // 
            cbbMaKho_DesignTimeLayout.LayoutString = resources.GetString("cbbMaKho_DesignTimeLayout.LayoutString");
            this.cbbMaKho.DesignTimeLayout = cbbMaKho_DesignTimeLayout;
            this.cbbMaKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaKho.Location = new System.Drawing.Point(157, 168);
            this.cbbMaKho.Name = "cbbMaKho";
            this.cbbMaKho.SelectedIndex = -1;
            this.cbbMaKho.SelectedItem = null;
            this.cbbMaKho.Size = new System.Drawing.Size(410, 21);
            this.cbbMaKho.TabIndex = 12;
            this.cbbMaKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaKho.ValueChanged += new System.EventHandler(this.cbbMaKho_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(21, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tên người giao / nhận :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người giao / nhận hàng  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Địa chỉ :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(21, 204);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tên kho :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(21, 172);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã kho :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(21, 140);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Diễn giải :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 111);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Người  nhận / giao :";
            // 
            // txtNguoiGiao
            // 
            this.txtNguoiGiao.BackColor = System.Drawing.Color.White;
            this.txtNguoiGiao.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtNguoiGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiGiao.Location = new System.Drawing.Point(157, 107);
            this.txtNguoiGiao.Name = "txtNguoiGiao";
            this.txtNguoiGiao.Size = new System.Drawing.Size(410, 21);
            this.txtNguoiGiao.TabIndex = 10;
            this.txtNguoiGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiGiao.ButtonClick += new System.EventHandler(this.txtNguoiGiao_ButtonClick);
            // 
            // txtTenKho
            // 
            this.txtTenKho.BackColor = System.Drawing.Color.White;
            this.txtTenKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKho.Location = new System.Drawing.Point(157, 200);
            this.txtTenKho.Name = "txtTenKho";
            this.txtTenKho.Size = new System.Drawing.Size(410, 21);
            this.txtTenKho.TabIndex = 13;
            this.txtTenKho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.BackColor = System.Drawing.Color.White;
            this.txtDienGiai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienGiai.Location = new System.Drawing.Point(157, 136);
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Size = new System.Drawing.Size(410, 21);
            this.txtDienGiai.TabIndex = 11;
            this.txtDienGiai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienGiai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.BackColor = System.Drawing.Color.White;
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(157, 77);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(410, 21);
            this.txtDiaChi.TabIndex = 9;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMa
            // 
            this.txtMa.BackColor = System.Drawing.Color.White;
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(157, 21);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(158, 21);
            this.txtMa.TabIndex = 7;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.White;
            this.txtTen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Location = new System.Drawing.Point(157, 50);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(410, 21);
            this.txtTen.TabIndex = 8;
            this.txtTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAdd,
            this.cmdAddExcel,
            this.cmdSave,
            this.cmdPrint});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAdd1,
            this.cmdAddExcel1,
            this.cmdSave1,
            this.cmdPrint1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(515, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdAdd1
            // 
            this.cmdAdd1.Key = "cmdAdd";
            this.cmdAdd1.Name = "cmdAdd1";
            // 
            // cmdAddExcel1
            // 
            this.cmdAddExcel1.Key = "cmdAddExcel";
            this.cmdAddExcel1.Name = "cmdAddExcel1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Key = "cmdAdd";
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Text = "Thêm hàng hóa";
            // 
            // cmdAddExcel
            // 
            this.cmdAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddExcel.Image")));
            this.cmdAddExcel.Key = "cmdAddExcel";
            this.cmdAddExcel.Name = "cmdAddExcel";
            this.cmdAddExcel.Text = "Thêm hàng hóa từ Excel";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In phiếu xuất nhập kho";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1201, 32);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 86);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(995, 269);
            this.uiGroupBox7.TabIndex = 0;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbbMaNT);
            this.uiGroupBox4.Controls.Add(this.txtTyGia);
            this.uiGroupBox4.Controls.Add(this.clcNgayCT);
            this.uiGroupBox4.Controls.Add(this.cbbLoaiChungTu);
            this.uiGroupBox4.Controls.Add(this.clcNgayCTGoc);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.cbbLoaiHang);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtSoCTGoc);
            this.uiGroupBox4.Controls.Add(this.txtSoCT);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(603, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(389, 258);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin chứng từ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbMaNT
            // 
            this.cbbMaNT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbMaNT.Appearance.Options.UseBackColor = true;
            this.cbbMaNT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.cbbMaNT.Code = "";
            this.cbbMaNT.ColorControl = System.Drawing.Color.Empty;
            this.cbbMaNT.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMaNT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbMaNT.IsOnlyWarning = false;
            this.cbbMaNT.IsValidate = true;
            this.cbbMaNT.Location = new System.Drawing.Point(125, 168);
            this.cbbMaNT.Name = "cbbMaNT";
            this.cbbMaNT.Name_VN = "";
            this.cbbMaNT.SetOnlyWarning = false;
            this.cbbMaNT.SetValidate = false;
            this.cbbMaNT.ShowColumnCode = true;
            this.cbbMaNT.ShowColumnName = false;
            this.cbbMaNT.Size = new System.Drawing.Size(139, 21);
            this.cbbMaNT.TabIndex = 18;
            this.cbbMaNT.TagName = "";
            this.cbbMaNT.Where = null;
            this.cbbMaNT.WhereCondition = "";
            // 
            // txtTyGia
            // 
            this.txtTyGia.DecimalDigits = 4;
            this.txtTyGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGia.Location = new System.Drawing.Point(125, 199);
            this.txtTyGia.Name = "txtTyGia";
            this.txtTyGia.Size = new System.Drawing.Size(140, 21);
            this.txtTyGia.TabIndex = 19;
            this.txtTyGia.Text = "1.0000";
            this.txtTyGia.Value = new decimal(new int[] {
            10000,
            0,
            0,
            262144});
            this.txtTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayCT
            // 
            this.clcNgayCT.CustomFormat = "dd/MM/yyyy";
            this.clcNgayCT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayCT.DropDownCalendar.Name = "";
            this.clcNgayCT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayCT.Location = new System.Drawing.Point(124, 46);
            this.clcNgayCT.Name = "clcNgayCT";
            this.clcNgayCT.Size = new System.Drawing.Size(140, 21);
            this.clcNgayCT.TabIndex = 15;
            this.clcNgayCT.Value = new System.DateTime(2018, 11, 13, 0, 0, 0, 0);
            this.clcNgayCT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbbLoaiChungTu
            // 
            this.cbbLoaiChungTu.BackColor = System.Drawing.Color.White;
            this.cbbLoaiChungTu.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Chứng từ Nhập kho";
            uiComboBoxItem3.Value = "N";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Chứng từ Xuất kho";
            uiComboBoxItem4.Value = "X";
            this.cbbLoaiChungTu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbLoaiChungTu.Location = new System.Drawing.Point(124, 73);
            this.cbbLoaiChungTu.Name = "cbbLoaiChungTu";
            this.cbbLoaiChungTu.Size = new System.Drawing.Size(252, 21);
            this.cbbLoaiChungTu.TabIndex = 23;
            this.cbbLoaiChungTu.ValueMember = "ID";
            this.cbbLoaiChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiChungTu.SelectedValueChanged += new System.EventHandler(this.cbbLoaiChungTu_SelectedValueChanged);
            // 
            // clcNgayCTGoc
            // 
            this.clcNgayCTGoc.CustomFormat = "dd/MM/yyyy";
            this.clcNgayCTGoc.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayCTGoc.DropDownCalendar.Name = "";
            this.clcNgayCTGoc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayCTGoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayCTGoc.Location = new System.Drawing.Point(124, 132);
            this.clcNgayCTGoc.Name = "clcNgayCTGoc";
            this.clcNgayCTGoc.Size = new System.Drawing.Size(140, 21);
            this.clcNgayCTGoc.TabIndex = 17;
            this.clcNgayCTGoc.Value = new System.DateTime(2018, 11, 13, 0, 0, 0, 0);
            this.clcNgayCTGoc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(13, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Loại chứng từ : ";
            // 
            // cbbLoaiHang
            // 
            this.cbbLoaiHang.BackColor = System.Drawing.Color.White;
            this.cbbLoaiHang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Nguyên phụ liệu";
            uiComboBoxItem5.Value = "N";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Sản phẩm";
            uiComboBoxItem6.Value = "S";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Thiết bị";
            uiComboBoxItem7.Value = "T";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Hàng mẫu";
            uiComboBoxItem8.Value = "H";
            this.cbbLoaiHang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbLoaiHang.Location = new System.Drawing.Point(125, 229);
            this.cbbLoaiHang.Name = "cbbLoaiHang";
            this.cbbLoaiHang.Size = new System.Drawing.Size(140, 21);
            this.cbbLoaiHang.TabIndex = 20;
            this.cbbLoaiHang.ValueMember = "ID";
            this.cbbLoaiHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHang.SelectedValueChanged += new System.EventHandler(this.cbbLoaiHang_SelectedValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(13, 233);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Loại hàng :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(12, 203);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tỷ giá :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(12, 172);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Mã nguyên tệ :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ngày chứng từ gốc :";
            // 
            // txtSoCTGoc
            // 
            this.txtSoCTGoc.BackColor = System.Drawing.Color.White;
            this.txtSoCTGoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTGoc.Location = new System.Drawing.Point(124, 103);
            this.txtSoCTGoc.Name = "txtSoCTGoc";
            this.txtSoCTGoc.Size = new System.Drawing.Size(250, 21);
            this.txtSoCTGoc.TabIndex = 16;
            this.txtSoCTGoc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTGoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoCT
            // 
            this.txtSoCT.BackColor = System.Drawing.Color.White;
            this.txtSoCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCT.Location = new System.Drawing.Point(124, 18);
            this.txtSoCT.Name = "txtSoCT";
            this.txtSoCT.ReadOnly = true;
            this.txtSoCT.Size = new System.Drawing.Size(250, 21);
            this.txtSoCT.TabIndex = 14;
            this.txtSoCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số chứng từ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngày chứng từ :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Số chứng từ gốc :";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtChiPhiKhac);
            this.uiGroupBox6.Controls.Add(this.txtPhiVC);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.cbbPhanBo);
            this.uiGroupBox6.Controls.Add(this.label14);
            this.uiGroupBox6.Controls.Add(this.label13);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.txtGhiChu);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 355);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(995, 85);
            this.uiGroupBox6.TabIndex = 104;
            this.uiGroupBox6.Text = "Chi phí phát sinh";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtChiPhiKhac
            // 
            this.txtChiPhiKhac.DecimalDigits = 4;
            this.txtChiPhiKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiPhiKhac.Location = new System.Drawing.Point(150, 53);
            this.txtChiPhiKhac.Name = "txtChiPhiKhac";
            this.txtChiPhiKhac.Size = new System.Drawing.Size(140, 21);
            this.txtChiPhiKhac.TabIndex = 22;
            this.txtChiPhiKhac.Text = "0.0000";
            this.txtChiPhiKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtChiPhiKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChiPhiKhac.TextChanged += new System.EventHandler(this.txtChiPhiKhac_TextChanged);
            // 
            // txtPhiVC
            // 
            this.txtPhiVC.DecimalDigits = 4;
            this.txtPhiVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVC.Location = new System.Drawing.Point(150, 20);
            this.txtPhiVC.Name = "txtPhiVC";
            this.txtPhiVC.Size = new System.Drawing.Size(140, 21);
            this.txtPhiVC.TabIndex = 21;
            this.txtPhiVC.Text = "0.0000";
            this.txtPhiVC.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtPhiVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiVC.TextChanged += new System.EventHandler(this.txtPhiVC_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(24, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Chi phí quản lý khác : ";
            // 
            // cbbPhanBo
            // 
            this.cbbPhanBo.BackColor = System.Drawing.Color.White;
            this.cbbPhanBo.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Phân bổ theo Đơn giá";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Phân bổ theo Số lượng";
            uiComboBoxItem2.Value = "1";
            this.cbbPhanBo.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbPhanBo.Location = new System.Drawing.Point(426, 20);
            this.cbbPhanBo.Name = "cbbPhanBo";
            this.cbbPhanBo.Size = new System.Drawing.Size(177, 21);
            this.cbbPhanBo.TabIndex = 23;
            this.cbbPhanBo.ValueMember = "ID";
            this.cbbPhanBo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbPhanBo.SelectedValueChanged += new System.EventHandler(this.cbbPhanBo_SelectedValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(334, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ghi chú :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(334, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Phân bổ theo : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Phí vận chuyển :";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(426, 53);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(552, 21);
            this.txtGhiChu.TabIndex = 24;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 440);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(995, 245);
            this.uiGroupBox1.TabIndex = 105;
            this.uiGroupBox1.Text = "Thông tin hàng hóa";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 4;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(989, 225);
            this.dgList.TabIndex = 90;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // WareHouseExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 770);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseExportForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phiếu xuất nhập kho hàng hóa";
            this.Load += new System.EventHandler(this.WareHouseExportForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHoaDon;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTK;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaKho;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiGiao;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenKho;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienGiai;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.EditBox txtTen;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory cbbMaNT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyGia;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayCT;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayCTGoc;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHang;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTGoc;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChiPhiKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVC;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIComboBox cbbPhanBo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiChungTu;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHang;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongLuongHang;
        private System.Windows.Forms.Label label20;
    }
}