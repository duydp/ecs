﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.Interface.GC;
#endif
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.Messages.CSSX;
#if SXXK_V4
using Company.BLL.DataTransferObjectMapper;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK;
#endif
namespace Company.Interface.VNACCS.Vouchers
{
    public partial class ManufactureFactories : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_StorageAreasProduction storageAreasProduction = new KDT_VNACCS_StorageAreasProduction();
        public KDT_VNACCS_ManufactureFactory manufactureFactory = null;
        public KDT_VNACCS_MemberCompany memberCompany = null;
        public KDT_VNACCS_HoldingCompany holdingCompany = null;
        public KDT_VNACCS_ContentInspection contentInspection = null;
        public KDT_VNACCS_AffiliatedMemberCompany affiliatedMemberCompany = null;
        public KDT_VNACCS_Career career = null;
        public KDT_VNACCS_Careery careery = null;

        public List<KDT_VNACCS_Careery> careeryCollection = new List<KDT_VNACCS_Careery>();
        public KDT_VNACCS_StorageOfGood storageOfGood = null;
        public KDT_VNACCS_Careeries_Product careeriesProduct = null;
        public KDT_VNACCS_ProductionCapacity_Product productionCapacityProduct = null;

        public List<KDT_VNACCS_Careeries_Product> CareeriesProductCollection = new List<KDT_VNACCS_Careeries_Product>();
        public List<KDT_VNACCS_ProductionCapacity_Product> ProductionCapacityProductCollection = new List<KDT_VNACCS_ProductionCapacity_Product>();

        public KDT_VNACCS_OutsourcingManufactureFactory outsourcingManufactureFactory = null;
        public KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument outsourcingManufactureFactoryContractDocument = null;
        public KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory outsourcingManufactureFactoryManufactureFactory = null;
        public KDT_VNACCS_OutsourcingManufactureFactory_Product outsourcingManufactureFactoryProduct = null;
        public List<KDT_VNACCS_OutsourcingManufactureFactory_Product> outsourcingManufactureFactoryProductCollection = new List<KDT_VNACCS_OutsourcingManufactureFactory_Product>();
        public List<KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product> outsourcingManufactureFactoryProductionCapacityProductCollection = new List<KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product>();
        public KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product outsourcingManufactureFactoryProductionCapacityProduct = null;
        public KDT_VNACCS_StorageAreasProduction_AttachedFile attachedFile = null;

#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif

        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public ManufactureFactories()
        {
            InitializeComponent();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdChuyenTT":
                    this.ChuyenTT();
                    break;
                default:
                    break;
            }
        }
        private void ChuyenTT()
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có chắc chắn muốn chuyển cơ sở sản xuất này sang Khai báo sửa không ? ", true) == "Yes")
                {
                    storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    storageAreasProduction.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_StorageAreasProduction", "", Convert.ToInt32(storageAreasProduction.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void ManufactureFactories_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            txtDiaChiTruSoChinh.Text = GlobalSettings.DIA_CHI;
            try
            {
                if (storageAreasProduction.ID > 0)
                {
                    SetStorageAreasProduction();
                    GetDataAll();
                    //BinDataCareery();
                    BinDataManufactureFactory();
                    BindDataAffiliatedMemberCompanies();
                    BindDataCareers();
                    BindDataContentInspection();
                    BindDataHoldingCompanies();
                    BindDataMemberCompanies();
                    BindDataStorageOfGoods();
                    BindDataAttachedFile();
                    BinDataOurSourceManufactureFactory();
                }
                else
                {
                    storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void GetDataAll()
        {
            try
            {
                storageAreasProduction.ManufactureFactoryCollection = KDT_VNACCS_ManufactureFactory.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                foreach (KDT_VNACCS_ManufactureFactory manufactureFactory in storageAreasProduction.ManufactureFactoryCollection)
                {
                    manufactureFactory.CareeryCollection = KDT_VNACCS_Careery.SelectCollectionBy_ManufactureFactory_ID(manufactureFactory.ID);
                    foreach (KDT_VNACCS_Careery careery in manufactureFactory.CareeryCollection)
                    {
                        careery.CareeriesProductCollection = KDT_VNACCS_Careeries_Product.SelectCollectionBy_Careeries_ID(careery.ID);
                        careery.ProductionCapacityProductCollection = KDT_VNACCS_ProductionCapacity_Product.SelectCollectionBy_Careeries_ID(careery.ID);
                    }
                }
                storageAreasProduction.MemberCompanyCollection = KDT_VNACCS_MemberCompany.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.HoldingCompanyCollection = KDT_VNACCS_HoldingCompany.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.ContentInspectionCollection = KDT_VNACCS_ContentInspection.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.AffiliatedMemberCompanyCollection = KDT_VNACCS_AffiliatedMemberCompany.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.CareerCollection = KDT_VNACCS_Career.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.AttachedFileGoodCollection = KDT_VNACCS_StorageAreasProduction_AttachedFile.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.StorageOfGoodCollection = KDT_VNACCS_StorageOfGood.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                storageAreasProduction.OutsourcingManufactureFactoryCollection = KDT_VNACCS_OutsourcingManufactureFactory.SelectCollectionBy_StorageAreasProduction_ID(storageAreasProduction.ID);
                foreach (KDT_VNACCS_OutsourcingManufactureFactory outsourcingManufactureFactory in storageAreasProduction.OutsourcingManufactureFactoryCollection)
                {
                    outsourcingManufactureFactory.ContractDocumentCollection = KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument.SelectCollectionBy_OutsourcingManufactureFactory_ID(outsourcingManufactureFactory.ID);
                    outsourcingManufactureFactory.ManufactureFactoryCollection = KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory.SelectCollectionBy_OutsourcingManufactureFactory_ID(outsourcingManufactureFactory.ID);
                    foreach (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory manufactureFactory in outsourcingManufactureFactory.ManufactureFactoryCollection)
                    {
                        manufactureFactory.ProductCollection = KDT_VNACCS_OutsourcingManufactureFactory_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(manufactureFactory.ID);
                        manufactureFactory.ProductionCapacityProductCollection = KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product.SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(manufactureFactory.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormCareery(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbLoaiNganhNghe, errorProvider, "Loại ngành nghề", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtChuKySX, errorProvider, "Chu kỳ sản xuất", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormCareer(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbLoaiNganhNgheSX, errorProvider, "Loại ngành nghề", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormManufactureFactory(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbLoaiCSSX, errorProvider, "Loại cơ sở sản xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbDCTruSoCSSX, errorProvider, "Địa chỉ trụ sở chính", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiCSSX, errorProvider, "Địa chỉ cơ sở sản xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDienTichNX, errorProvider, "Diện tích nhà xưởng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongCN, errorProvider, "Số lượng công nhân", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongSoHuu, errorProvider, "Số lượng sở hữu", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongDiThue, errorProvider, "Số lượng đi thuê", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongKhac, errorProvider, "Số lượng khác", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTongSoLuong, errorProvider, "Tổng Số lượng ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNangLucSX, errorProvider, "Năng lực sản xuất", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateOurFormManufactureFactory(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiepDT, errorProvider, "Mã doanh nghiệp đối tác", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiepDT, errorProvider, "Tên doanh nghiệp đối tác", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiDoanhNghiepDT, errorProvider, "Địa chỉ đối tác", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormHoldingCompany(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiepCTM, errorProvider, "Tên doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiepCTM, errorProvider, "Mã doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiCSSXCTM, errorProvider, "Địa chỉ chi CSSX", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormContentInspection(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoBienBanKT, errorProvider, "Số biên bản kiểm tra", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoKetLuanKT, errorProvider, "Số kết luận kiểm tra", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(dateNgayKT, errorProvider, "Ngày kiểm tra", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormAffiliatedMemberCompany(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghepTV, errorProvider, "Tên doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiCSSXTV, errorProvider, "Địa chỉ CSSX", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiepTV, errorProvider, "Mã doanh nghiệp", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateFormMemberCompany(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiepCN, errorProvider, "Tên doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiepCN, errorProvider, "Mã doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiCN, errorProvider, "Địa chỉ chi nhánh", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiep, errorProvider, "Tên doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiep, errorProvider, "Mã doanh nghiệp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTruSoChinh, errorProvider, "Địa chỉ trụ sở chính (Loại)", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(cbbNuocDauTu, errorProvider, "Nước đầu tư", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNganhNgheSX, errorProvider, "Ngành nghề sản xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(SoCMNDChuTich, errorProvider, "Số CMND/hộ chiếu", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(dateNgayCapGPChuTich, errorProvider, "Ngày cấp giấy phép", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNoiCapGPChuTich, errorProvider, "Nơi cấp giấy phép", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNoiDKHKChuTich, errorProvider, "Nơi đăng ký hộ khẩu thường trú", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoDTChuTich, errorProvider, "Số điện thoại", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoCMNDGD, errorProvider, "Số CMND/hộ chiếu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNoiCapGPGD, errorProvider, "Nơi cấp giấy phép", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNoiDKHKGD, errorProvider, "Nơi đăng ký hộ khẩu thường trú", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoDTGD, errorProvider, "Số điện thoại", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbDaKiemTra, errorProvider, "Đã/Chưa được cơ quan hải quan kiểm tra", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiSua, errorProvider, "Loại sửa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinhDN, errorProvider, "Loại hình doanh nghiệp", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(cbbLoaiNganhNgheSX, errorProvider, "Loại ngành nghề", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtBoPhanQuanLy, errorProvider, "Bộ phận quản lý", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongCongNhan, errorProvider, "Số lượng công nhân", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongSoHuuCSSX, errorProvider, "Số lượng sở hữu", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongThanhVienCTM, errorProvider, "Số lượng thành viên", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtTenCongTyMe, errorProvider, "Tên công ty mẹ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtMaCongTyMe, errorProvider, "Mã công ty mẹ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongThanhVien, errorProvider, "Số lượng thành viên", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongChiNhanh, errorProvider, "Số lượng chi nhánh", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbBuonLau, errorProvider, "Bị xử lý về hành vi buôn lậu, vận chuyển trái phép hàng hóa qua biên giới, trốn thuế", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTronThue, errorProvider, "Bị xử phạt về hành vi trốn thuế, gian lận thuế", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbKeToan, errorProvider, "Bị các cơ quan quản lý nhà nước xử phạt vi phạm trong lĩnh vực kế toán", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (storageAreasProduction.ManufactureFactoryCollection.Count < 1)
                {
                    ShowMessage("Bạn chưa Thêm Thông tin Cơ sở sản xuất (CSSX) ", false);
                    return;
                }
                if (storageAreasProduction.CareerCollection.Count < 1)
                {
                    ShowMessage("Bạn chưa Thêm Ngành nghề ", false);
                    uiTab1.SelectedTab.Selected = uiTabPage2.Selected;
                    return;
                }
                if (storageAreasProduction.AttachedFileGoodCollection.Count < 1)
                {
                    ShowMessage("Bạn chưa Thêm File đính kèm ", false);
                    uiTab1.SelectedTab.Selected = uiTabPage4.Selected;
                    return;
                }
                if (storageAreasProduction.AffiliatedMemberCompanyCollection.Count != Convert.ToInt32(txtSoLuongThanhVien.Text))
                {
                    ShowMessage("Số lượng Đơn vị thành viên phải bằng Số lượng thành viên Công ty thành viên trực thuộc Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị thành viên khác trực thuộc Công ty mẹ ", false);
                    uiTab1.SelectedTab.Selected = uiTabPage3.Selected;
                    return;
                }
                if (storageAreasProduction.MemberCompanyCollection.Count != Convert.ToInt32(txtSoLuongChiNhanh.Text))
                {
                    ShowMessage("Số lượng Đơn vị thành viên phải bằng Số lượng thành viên Công ty thành viên nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị trực thuộc Công ty thành viên có CSSX", false);
                    uiTab1.SelectedTab.Selected = uiTabPage3.Selected;
                    return;
                }
                if (storageAreasProduction.HoldingCompanyCollection.Count != Convert.ToInt32(txtSoLuongThanhVienCTM.Text))
                {
                    ShowMessage("Số lượng Đơn vị thành viên phải bằng Số lượng Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để gia công, sản xuất xuất khẩu cho các đơn vị thành viên trực thuộc", false);
                    uiTab1.SelectedTab.Selected = uiTabPage4.Selected;
                    return;
                }
                GetStorageAreasProduction();
                storageAreasProduction.InsertUpdateFull();
                ShowMessage("Lưu thành công", false);
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        public void GetStorageAreasProduction()
        {
            try
            {
                if (storageAreasProduction.ID == 0)
                    storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                storageAreasProduction.SoTN = Convert.ToInt64(txtSoTN.Text.ToString());
                storageAreasProduction.NgayTN = dateNgayTN.Value;
                storageAreasProduction.MaHQ = ctrCoQuanHaiQuan.Code;
                storageAreasProduction.TenDoanhNghiep = txtTenDoanhNghiep.Text;
                storageAreasProduction.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                storageAreasProduction.DiaChiTruSoChinh = txtDiaChiTruSoChinh.Text;
                storageAreasProduction.LoaiDiaChiTruSoChinh = Convert.ToInt32(cbbTruSoChinh.SelectedValue.ToString());
                storageAreasProduction.NuocDauTu = ctrNuocXuatXu.Code;
                storageAreasProduction.NganhNgheSanXuat = txtNganhNgheSX.Text;
                storageAreasProduction.TenDoanhNghiepTKTD = txtTenDoanhNghiepTruoc.Text;
                storageAreasProduction.MaDoanhNghiepTKTD = txtMaDoanhNghiepTruoc.Text;
                storageAreasProduction.LyDoChuyenDoi = txtLyDoChuyenDoi.Text;
                storageAreasProduction.SoCMNDCT = SoCMNDChuTich.Text;
                storageAreasProduction.NgayCapGiayPhepCT = dateNgayCapGPChuTich.Value;
                storageAreasProduction.NoiCapGiayPhepCT = txtNoiCapGPChuTich.Text;
                storageAreasProduction.NoiDangKyHKTTCT = txtNoiDKHKChuTich.Text;
                storageAreasProduction.SoDienThoaiCT = txtSoDTChuTich.Text;
                storageAreasProduction.SoCMNDGD = txtSoCMNDGD.Text;
                storageAreasProduction.NgayCapGiayPhepGD = dateNgayCapGPGD.Value;
                storageAreasProduction.NoiCapGiayPhepGD = txtNoiCapGPGD.Text;
                storageAreasProduction.NoiDangKyHKTTGD = txtNoiDKHKGD.Text;
                storageAreasProduction.SoDienThoaiGD = txtSoDTGD.Text;
                storageAreasProduction.DaDuocCQHQKT = Convert.ToInt32(cbbDaKiemTra.SelectedValue.ToString());
                storageAreasProduction.BiXuPhatVeBuonLau = Convert.ToInt32(cbbBuonLau.SelectedValue.ToString());
                storageAreasProduction.BiXuPhatVeTronThue = Convert.ToInt32(cbbTronThue.SelectedValue.ToString());
                storageAreasProduction.BiXuPhatVeKeToan = Convert.ToInt32(cbbKeToan.SelectedValue.ToString());
                //storageAreasProduction.ThoiGianSanXuat = Convert.ToInt32(cbbThoiGianSX.SelectedValue.ToString());
                //storageAreasProduction.SoLuongSanPham = Convert.ToDecimal(txtSoLuongSP.Text);
                storageAreasProduction.BoPhanQuanLy = Convert.ToDecimal(txtBoPhanQuanLy.Text);
                storageAreasProduction.SoLuongCongNhan = Convert.ToDecimal(txtSoLuongCongNhan.Text);

                storageAreasProduction.SoLuongSoHuu = Convert.ToDecimal(txtSoLuongSoHuuCSSX.Text);
                storageAreasProduction.SoLuongDiThue = Convert.ToDecimal(txtSoLuongDiThueCSSX.Text);
                storageAreasProduction.SoLuongKhac = Convert.ToDecimal(txtSoLuongKhacCSSX.Text);
                storageAreasProduction.TongSoLuong = Convert.ToDecimal(txtTongSoLuongCSSX.Text);

                storageAreasProduction.LoaiSua = Convert.ToInt32(cbbLoaiSua.SelectedValue.ToString());
                storageAreasProduction.NgayKetThucNamTC = clcNgayKetThucNTC.Value;
                storageAreasProduction.LoaiHinhDN = Convert.ToInt32(cbbLoaiHinhDN.SelectedValue.ToString());

                storageAreasProduction.TenCongTyMe = txtTenCongTyMe.Text;
                storageAreasProduction.MaCongTyMe = txtMaCongTyMe.Text;
                storageAreasProduction.SoLuongThanhVien = Convert.ToDecimal(txtSoLuongThanhVien.Text);
                storageAreasProduction.SoLuongChiNhanh = Convert.ToDecimal(txtSoLuongChiNhanh.Text);
                storageAreasProduction.SoLuongThanhVienCTM = Convert.ToInt32(txtSoLuongThanhVienCTM.Text);
                storageAreasProduction.GhiChuKhac = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void SetStorageAreasProduction()
        {
            try
            {
                if (storageAreasProduction.ID == 0)
                    storageAreasProduction.TrangThaiXuLy = -1;
                if (storageAreasProduction.TrangThaiXuLy == -1)
                    lblTrangThai.Text = "Chưa khai báo";
                else if (storageAreasProduction.TrangThaiXuLy == 0)
                    lblTrangThai.Text = "Chờ duyệt";
                else if (storageAreasProduction.TrangThaiXuLy == 1)
                    lblTrangThai.Text = "Đã duyệt";
                else if (storageAreasProduction.TrangThaiXuLy == 2)
                    lblTrangThai.Text = "Không phê duyệt";
                txtSoTN.Text = storageAreasProduction.SoTN.ToString();
                dateNgayTN.Value = storageAreasProduction.NgayTN;
                txtTenDoanhNghiep.Text = storageAreasProduction.TenDoanhNghiep;
                txtMaDoanhNghiep.Text = storageAreasProduction.MaDoanhNghiep;
                txtDiaChiTruSoChinh.Text = storageAreasProduction.DiaChiTruSoChinh;
                cbbTruSoChinh.SelectedValue = storageAreasProduction.LoaiDiaChiTruSoChinh;
                ctrNuocXuatXu.Code = storageAreasProduction.NuocDauTu;
                txtNganhNgheSX.Text = storageAreasProduction.NganhNgheSanXuat;
                txtTenDoanhNghiepTruoc.Text = storageAreasProduction.TenDoanhNghiepTKTD;
                txtMaDoanhNghiepTruoc.Text = storageAreasProduction.MaDoanhNghiepTKTD;
                txtLyDoChuyenDoi.Text = storageAreasProduction.LyDoChuyenDoi;
                SoCMNDChuTich.Text = storageAreasProduction.SoCMNDCT;
                dateNgayCapGPChuTich.Value = storageAreasProduction.NgayCapGiayPhepCT;
                txtNoiCapGPChuTich.Text = storageAreasProduction.NoiCapGiayPhepCT;
                txtNoiDKHKChuTich.Text = storageAreasProduction.NoiDangKyHKTTCT;
                txtSoDTChuTich.Text = storageAreasProduction.SoDienThoaiCT;
                txtSoCMNDGD.Text = storageAreasProduction.SoCMNDGD;
                dateNgayCapGPGD.Value = storageAreasProduction.NgayCapGiayPhepGD;
                txtNoiCapGPGD.Text = storageAreasProduction.NoiCapGiayPhepGD;
                txtNoiDKHKGD.Text = storageAreasProduction.NoiDangKyHKTTGD;
                txtSoDTGD.Text = storageAreasProduction.SoDienThoaiGD;
                cbbDaKiemTra.SelectedValue = storageAreasProduction.DaDuocCQHQKT;
                cbbBuonLau.SelectedValue = storageAreasProduction.BiXuPhatVeBuonLau;
                cbbTronThue.SelectedValue = storageAreasProduction.BiXuPhatVeTronThue;
                cbbKeToan.SelectedValue = storageAreasProduction.BiXuPhatVeKeToan;
                //cbbThoiGianSX.SelectedValue = storageAreasProduction.ThoiGianSanXuat;
                //txtSoLuongSP.Text = storageAreasProduction.SoLuongSanPham.ToString();
                txtBoPhanQuanLy.Text = storageAreasProduction.BoPhanQuanLy.ToString();
                txtSoLuongCongNhan.Text = storageAreasProduction.SoLuongCongNhan.ToString();

                txtSoLuongSoHuuCSSX.Text = storageAreasProduction.SoLuongSoHuu.ToString();
                txtSoLuongDiThueCSSX.Text = storageAreasProduction.SoLuongDiThue.ToString();
                txtSoLuongKhacCSSX.Text = storageAreasProduction.SoLuongKhac.ToString();
                txtTongSoLuongCSSX.Text = storageAreasProduction.TongSoLuong.ToString();

                cbbLoaiSua.SelectedValue = storageAreasProduction.LoaiSua.ToString();
                cbbLoaiHinhDN.SelectedValue = storageAreasProduction.LoaiHinhDN.ToString();
                clcNgayKetThucNTC.Value = storageAreasProduction.NgayKetThucNamTC;

                txtTenCongTyMe.Text = storageAreasProduction.TenCongTyMe;
                txtMaCongTyMe.Text = storageAreasProduction.MaCongTyMe;
                txtSoLuongThanhVien.Text = storageAreasProduction.SoLuongThanhVien.ToString();
                txtSoLuongChiNhanh.Text = storageAreasProduction.SoLuongChiNhanh.ToString();
                txtSoLuongThanhVienCTM.Text = storageAreasProduction.SoLuongThanhVienCTM.ToString();
                txtGhiChu.Text = storageAreasProduction.GhiChuKhac;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void SetCommandStatus()
        {
            if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTN.Text = storageAreasProduction.SoTN.ToString();
                dateNgayTN.Value = storageAreasProduction.NgayTN;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                dateNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTN.Text = storageAreasProduction.SoTN.ToString();
                dateNgayTN.Value = storageAreasProduction.NgayTN;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = storageAreasProduction.SoTN.ToString();
                dateNgayTN.Value = storageAreasProduction.NgayTN;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = storageAreasProduction.SoTN.ToString();
                dateNgayTN.Value = storageAreasProduction.NgayTN;
                lblTrangThai.Text = storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (storageAreasProduction.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    GetDataAll();
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.StorageAreasProduction;
                sendXML.master_id = storageAreasProduction.ID;

                if (sendXML.Load())
                {
                    if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    storageAreasProduction.GuidStr = Guid.NewGuid().ToString();
                    StorageAreasProduction_VNACCS storageAreasProduction_VNACCS = new StorageAreasProduction_VNACCS();
                    storageAreasProduction_VNACCS = Mapper_V4.ToDataTransferStorageAreasProduction(storageAreasProduction, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = storageAreasProduction.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(storageAreasProduction.MaHQ)),
                              Identity = storageAreasProduction.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = storageAreasProduction_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = storageAreasProduction.GuidStr,
                          },
                          storageAreasProduction_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        storageAreasProduction.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(storageAreasProduction.ID, MessageTitle.RegisterStorageAreasProduction);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.StorageAreasProduction;
                        sendXML.master_id = storageAreasProduction.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = Convert.ToInt32(storageAreasProduction_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            storageAreasProduction.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            SetCommandStatus();
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            storageAreasProduction.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatus();
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.StorageAreasProduction;
            sendXML.master_id = storageAreasProduction.ID;
            if (!sendXML.Load())
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            while (isFeedBack)
            {
                string reference = storageAreasProduction.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = "360",//DeclarationIssuer.StorageAreasProduction,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = "360",//DeclarationIssuer.GoodsItem,
                };
                subjectBase.Type = "360";//DeclarationIssuer.StorageAreasProduction;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = storageAreasProduction.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(storageAreasProduction.MaHQ.Trim())),
                                                  Identity = storageAreasProduction.MaHQ
                                              }, subjectBase, null);
                if (storageAreasProduction.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(storageAreasProduction.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        SetCommandStatus();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        storageAreasProduction.Update();
                        isFeedBack = true;
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        storageAreasProduction.Update();
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        storageAreasProduction.Update();
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        SetCommandStatus();
                    }
                }
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = storageAreasProduction.ID;
                form.DeclarationIssuer = DeclarationIssuer.StorageAreasProduction;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.StorageAreasProductionSendHandler(storageAreasProduction, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void BinDataManufactureFactory()
        {
            try
            {
                dgListCSSX.Refresh();
                dgListCSSX.DataSource = storageAreasProduction.ManufactureFactoryCollection;
                dgListCSSX.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddCSSX_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormManufactureFactory(false))
                    return;
                if (manufactureFactory == null)
                {
                    ShowMessage("Bạn chưa thêm danh sách Ngành nghề", false);
                    return;
                }
                if (manufactureFactory.CareeryCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm danh sách Ngành nghề", false);
                    return;
                }
                if (manufactureFactory.LoaiCSSX == 0)
                    isAddNew = true;
                manufactureFactory.LoaiCSSX = Convert.ToInt32(cbbLoaiCSSX.SelectedValue.ToString());
                manufactureFactory.DiaChiCSSX = txtDiaChiCSSX.Text;
                manufactureFactory.LoaiDiaChiCSSX = Convert.ToInt32(cbbDCTruSoCSSX.SelectedValue.ToString());
                manufactureFactory.DienTichNhaXuong = Convert.ToDecimal(txtDienTichNX.Text.ToString());
                manufactureFactory.SoLuongCongNhan = Convert.ToDecimal(txtSoLuongCN.Text.ToString());
                manufactureFactory.SoLuongSoHuu = Convert.ToDecimal(txtSoLuongSoHuu.Text);
                manufactureFactory.SoLuongDiThue = Convert.ToDecimal(txtSoLuongDiThue.Text);
                manufactureFactory.SoLuongKhac = Convert.ToDecimal(txtSoLuongKhac.Text);
                manufactureFactory.TongSoLuong = Convert.ToDecimal(txtTongSoLuong.Text);
                manufactureFactory.NangLucSanXuat = txtNangLucSX.Text;
                if (isAddNew)
                    storageAreasProduction.ManufactureFactoryCollection.Add(manufactureFactory);
                manufactureFactory = new KDT_VNACCS_ManufactureFactory();
                txtDiaChiCSSX.Text = String.Empty;
                txtDienTichNX.Text = String.Empty;
                txtSoLuongCN.Text = String.Empty;
                txtSoLuongSoHuu.Text = String.Empty;
                txtSoLuongDiThue.Text = String.Empty;
                txtSoLuongKhac.Text = String.Empty;
                txtTongSoLuong.Text = String.Empty;
                txtNangLucSX.Text = String.Empty;
                BinDataManufactureFactory();
                BinDataCareery();
                manufactureFactory = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteCSSX_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListCSSX.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            manufactureFactory = (KDT_VNACCS_ManufactureFactory)i.GetRow().DataRow;
                            storageAreasProduction.ManufactureFactoryCollection.Remove(manufactureFactory);
                            if (manufactureFactory.ID > 0)
                            {
                                manufactureFactory.DeleteFull();
                            }
                        }
                    }
                    txtDiaChiCSSX.Text = String.Empty;
                    txtDienTichNX.Text = String.Empty;
                    txtSoLuongCN.Text = String.Empty;
                    txtSoLuongSoHuu.Text = String.Empty;
                    txtSoLuongDiThue.Text = String.Empty;
                    txtSoLuongKhac.Text = String.Empty;
                    txtTongSoLuong.Text = String.Empty;
                    txtNangLucSX.Text = String.Empty;
                    ShowMessage("Xóa thành công", false);
                    BinDataManufactureFactory();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BinDataCareery()
        {
            try
            {
                dgListNganhNghe.Refresh();
                dgListNganhNghe.DataSource = manufactureFactory.CareeryCollection;
                dgListNganhNghe.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddNganhNghe_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormCareery(false))
                    return;
                if (careery == null)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Chu kỳ sản xuất và Năng lực sản xuất ", false);
                    return;
                }
                if (careery.CareeriesProductCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Chu kỳ sản xuất ", false);
                    return;
                }
                if (careery.ProductionCapacityProductCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Năng lực sản xuất ", false);
                    return;
                }
                if (manufactureFactory == null)
                {
                    manufactureFactory = new KDT_VNACCS_ManufactureFactory();
                    isAddNew = true;
                }
                if (careery.LoaiNganhNghe == 0)
                    isAddNew = true;
                CareeriesProductCollection = careery.CareeriesProductCollection;
                ProductionCapacityProductCollection = careery.ProductionCapacityProductCollection;
                careery.LoaiNganhNghe = Convert.ToInt32(cbbLoaiNganhNghe.SelectedValue.ToString());
                careery.CareeriesProductCollection = CareeriesProductCollection;
                careery.ProductionCapacityProductCollection = ProductionCapacityProductCollection;
                if (isAddNew)
                    manufactureFactory.CareeryCollection.Add(careery);
                careery = new KDT_VNACCS_Careery();
                //txtChuKySX.Text = String.Empty;
                cbbLoaiNganhNghe.Text = String.Empty;
                BinDataCareery();
                BindDataCareeriesProduct();
                BindDataProductionCapacityProduct();
                careery = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteNganhNghe_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListNganhNghe.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            careery = (KDT_VNACCS_Careery)i.GetRow().DataRow;
                            manufactureFactory.CareeryCollection.Remove(careery);
                            //storageAreasProduction.CareeryCollection.Remove(careery);
                            if (careery.ID > 0)
                            {
                                careery.DeleteFull();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    cbbLoaiNganhNghe.Text = String.Empty;
                    BinDataCareery();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BindDataContentInspection()
        {
            try
            {
                dgListKiemTra.Refresh();
                dgListKiemTra.DataSource = storageAreasProduction.ContentInspectionCollection;
                dgListKiemTra.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddKiemTra_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormContentInspection(false))
                    return;
                if (contentInspection == null)
                {
                    contentInspection = new KDT_VNACCS_ContentInspection();
                    isAddNew = true;
                }
                contentInspection.SoBienBanKiemTra = txtSoBienBanKT.Text;
                contentInspection.SoKetLuanKiemTra = txtSoKetLuanKT.Text;
                contentInspection.NgayKiemTra = dateNgayKT.Value;
                if (isAddNew)
                    storageAreasProduction.ContentInspectionCollection.Add(contentInspection);
                contentInspection = new KDT_VNACCS_ContentInspection();
                txtSoBienBanKT.Text = String.Empty;
                txtSoKetLuanKT.Text = String.Empty;
                BindDataContentInspection();
                contentInspection = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteKiemTra_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListKiemTra.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            contentInspection = (KDT_VNACCS_ContentInspection)i.GetRow().DataRow;
                            storageAreasProduction.ContentInspectionCollection.Remove(contentInspection);
                            if (contentInspection.ID > 0)
                            {
                                contentInspection.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtSoBienBanKT.Text = String.Empty;
                    txtSoKetLuanKT.Text = String.Empty;
                    BindDataContentInspection();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BindDataCareers()
        {
            try
            {
                dgListNganhHangSXChinh.Refresh();
                dgListNganhHangSXChinh.DataSource = storageAreasProduction.CareerCollection;
                dgListNganhHangSXChinh.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddNganhHangSXChinh_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormCareer(false))
                    return;
                if (career == null)
                {
                    career = new KDT_VNACCS_Career();
                    isAddNew = true;
                }
                career.LoaiNganhNghe = Convert.ToInt32(cbbLoaiNganhNgheSX.SelectedValue.ToString());
                storageAreasProduction.CareerCollection.Add(career);
                if (isAddNew)
                    storageAreasProduction.CareerCollection.Add(career);
                career = new KDT_VNACCS_Career();
                cbbLoaiNganhNgheSX.SelectedValue = String.Empty;
                BindDataCareers();
                career = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteNganhHangSXChinh_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListNganhHangSXChinh.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            career = (KDT_VNACCS_Career)i.GetRow().DataRow;
                            storageAreasProduction.CareerCollection.Remove(career);
                            if (career.ID > 0)
                            {
                                career.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    cbbLoaiNganhNgheSX.SelectedValue = String.Empty;
                    BindDataCareers();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BindDataAffiliatedMemberCompanies()
        {
            try
            {
                dgListCongTyMe.Refresh();
                dgListCongTyMe.DataSource = storageAreasProduction.AffiliatedMemberCompanyCollection;
                dgListCongTyMe.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddCongTyMe_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormAffiliatedMemberCompany(false))
                    return;
                if (affiliatedMemberCompany == null)
                {
                    affiliatedMemberCompany = new KDT_VNACCS_AffiliatedMemberCompany();
                    isAddNew = true;
                }
                affiliatedMemberCompany.TenDoanhNghiep = txtTenDoanhNghepTV.Text;
                affiliatedMemberCompany.MaDoanhNghiep = txtMaDoanhNghiepTV.Text;
                affiliatedMemberCompany.DiaChiCSSX = txtDiaChiCSSXTV.Text;
                if (isAddNew)
                    storageAreasProduction.AffiliatedMemberCompanyCollection.Add(affiliatedMemberCompany);
                affiliatedMemberCompany = new KDT_VNACCS_AffiliatedMemberCompany();
                txtTenDoanhNghepTV.Text = String.Empty;
                txtMaDoanhNghiepTV.Text = String.Empty;
                txtDiaChiCSSXTV.Text = String.Empty;
                BindDataAffiliatedMemberCompanies();
                affiliatedMemberCompany = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteCongTyMe_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListCongTyMe.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            affiliatedMemberCompany = (KDT_VNACCS_AffiliatedMemberCompany)i.GetRow().DataRow;
                            storageAreasProduction.AffiliatedMemberCompanyCollection.Remove(affiliatedMemberCompany);
                            if (affiliatedMemberCompany.ID > 0)
                            {
                                affiliatedMemberCompany.Delete();
                            }
                        }
                    }
                    txtTenDoanhNghepTV.Text = String.Empty;
                    txtMaDoanhNghiepTV.Text = String.Empty;
                    txtDiaChiCSSXTV.Text = String.Empty;
                    ShowMessage("Xóa thành công", false);
                    BindDataAffiliatedMemberCompanies();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BindDataHoldingCompanies()
        {
            try
            {
                dgListCTM.Refresh();
                dgListCTM.DataSource = storageAreasProduction.HoldingCompanyCollection;
                dgListCTM.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        public void BindDataMemberCompanies()
        {
            try
            {
                dgListThanhVien.Refresh();
                dgListThanhVien.DataSource = storageAreasProduction.MemberCompanyCollection;
                dgListThanhVien.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddChiNhanh_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormMemberCompany(false))
                    return;
                if (memberCompany == null)
                {
                    memberCompany = new KDT_VNACCS_MemberCompany();
                    isAddNew = true;
                }
                memberCompany.TenDoanhNghiep = txtTenDoanhNghiepCN.Text;
                memberCompany.MaDoanhNghiep = txtMaDoanhNghiepCN.Text;
                memberCompany.DiaChiChiNhanh = txtDiaChiCN.Text;
                if (isAddNew)
                    storageAreasProduction.MemberCompanyCollection.Add(memberCompany);
                memberCompany = new KDT_VNACCS_MemberCompany();
                txtTenDoanhNghiepCN.Text = String.Empty;
                txtMaDoanhNghiepCN.Text = String.Empty;
                txtDiaChiCN.Text = String.Empty;
                BindDataMemberCompanies();
                memberCompany = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteChiNhanh_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListThanhVien.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            memberCompany = (KDT_VNACCS_MemberCompany)i.GetRow().DataRow;
                            storageAreasProduction.MemberCompanyCollection.Remove(memberCompany);
                            if (memberCompany.ID > 0)
                            {
                                memberCompany.Delete();
                            }
                        }
                    }
                    txtTenDoanhNghiepCN.Text = String.Empty;
                    txtMaDoanhNghiepCN.Text = String.Empty;
                    txtDiaChiCN.Text = String.Empty;
                    ShowMessage("Xóa thành công", false);
                    BindDataMemberCompanies();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void dgListCSSX_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                manufactureFactory = (KDT_VNACCS_ManufactureFactory)dgListCSSX.GetRow().DataRow;
                cbbLoaiCSSX.SelectedValue = manufactureFactory.LoaiCSSX;
                txtDiaChiCSSX.Text = manufactureFactory.DiaChiCSSX;
                cbbDCTruSoCSSX.SelectedValue = manufactureFactory.LoaiDiaChiCSSX;
                txtDienTichNX.Text = manufactureFactory.DienTichNhaXuong.ToString();
                txtSoLuongCN.Text = manufactureFactory.SoLuongCongNhan.ToString();
                txtSoLuongSoHuu.Text = manufactureFactory.SoLuongSoHuu.ToString();
                txtSoLuongDiThue.Text = manufactureFactory.SoLuongDiThue.ToString();
                txtSoLuongKhac.Text = manufactureFactory.SoLuongKhac.ToString();
                txtTongSoLuong.Text = manufactureFactory.TongSoLuong.ToString();
                txtNangLucSX.Text = manufactureFactory.NangLucSanXuat;
                BinDataCareery();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListNganhNghe_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                careery = (KDT_VNACCS_Careery)dgListNganhNghe.GetRow().DataRow;
                cbbLoaiNganhNghe.SelectedValue = careery.LoaiNganhNghe;
                BindDataCareeriesProduct();
                BindDataProductionCapacityProduct();
                //txtChuKySX.Text = careery.ChuKySanXuat;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListKiemTra_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                contentInspection = (KDT_VNACCS_ContentInspection)dgListKiemTra.GetRow().DataRow;
                txtSoBienBanKT.Text = contentInspection.SoBienBanKiemTra;
                txtSoKetLuanKT.Text = contentInspection.SoKetLuanKiemTra;
                dateNgayKT.Value = contentInspection.NgayKiemTra;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListNganhHangSXChinh_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                career = (KDT_VNACCS_Career)dgListNganhHangSXChinh.GetRow().DataRow;
                cbbLoaiNganhNgheSX.SelectedValue = career.LoaiNganhNghe;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListCongTyMe_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                affiliatedMemberCompany = (KDT_VNACCS_AffiliatedMemberCompany)dgListCongTyMe.GetRow().DataRow;
                txtTenDoanhNghepTV.Text = affiliatedMemberCompany.TenDoanhNghiep;
                txtMaDoanhNghiepTV.Text = affiliatedMemberCompany.MaDoanhNghiep;
                txtDiaChiCSSXTV.Text = affiliatedMemberCompany.DiaChiCSSX;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListThanhVien_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                memberCompany = (KDT_VNACCS_MemberCompany)dgListThanhVien.GetRow().DataRow;
                txtTenDoanhNghiepCN.Text = memberCompany.TenDoanhNghiep;
                txtMaDoanhNghiepCN.Text = memberCompany.MaDoanhNghiep;
                txtDiaChiCN.Text = memberCompany.DiaChiChiNhanh;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnAddCTM_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormHoldingCompany(false))
                    return;
                if (holdingCompany == null)
                {
                    holdingCompany = new KDT_VNACCS_HoldingCompany();
                    isAddNew = true;
                }
                holdingCompany.TenDoanhNghiep = txtTenDoanhNghiepCTM.Text;
                holdingCompany.MaDoanhNghiep = txtMaDoanhNghiepCTM.Text;
                holdingCompany.DiaChiCSSX = txtDiaChiCSSXCTM.Text;
                if (isAddNew)
                    storageAreasProduction.HoldingCompanyCollection.Add(holdingCompany);
                holdingCompany = new KDT_VNACCS_HoldingCompany();
                txtTenDoanhNghiepCTM.Text = String.Empty;
                txtMaDoanhNghiepCTM.Text = String.Empty;
                txtDiaChiCSSXCTM.Text = String.Empty;
                BindDataHoldingCompanies();
                holdingCompany = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteCTM_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListCTM.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            holdingCompany = (KDT_VNACCS_HoldingCompany)i.GetRow().DataRow;
                            storageAreasProduction.HoldingCompanyCollection.Remove(holdingCompany);
                            if (holdingCompany.ID > 0)
                            {
                                holdingCompany.Delete();
                            }
                        }
                    }
                    txtTenDoanhNghiepCTM.Text = String.Empty;
                    txtMaDoanhNghiepCTM.Text = String.Empty;
                    txtDiaChiCSSXCTM.Text = String.Empty;
                    ShowMessage("Xóa thành công", false);
                    BindDataHoldingCompanies();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void dgListCSSX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int LoaiCSSX = Convert.ToInt32(e.Row.Cells["LoaiCSSX"].Value);
                    int LoaiDCCSSX = Convert.ToInt32(e.Row.Cells["LoaiDiaChiCSSX"].Value);
                    if (LoaiCSSX == 1)
                    {
                        e.Row.Cells["LoaiCSSX"].Text = "CSSX trong khu vực nhà máy";
                    }
                    else
                    {
                        e.Row.Cells["LoaiCSSX"].Text = "CSSX ngoài khu vực nhà máy";
                    }

                    if (LoaiDCCSSX == 1)
                    {
                        e.Row.Cells["LoaiDiaChiCSSX"].Text = " CSSX thuộc quyền sở hữu của DN ";
                    }
                    else
                    {
                        e.Row.Cells["LoaiDiaChiCSSX"].Text = "CSSX thuê";
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListNganhNghe_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string LoaiNganhNghe = e.Row.Cells["LoaiNganhNghe"].Value.ToString();
                    switch (LoaiNganhNghe)
                    {
                        case "1":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Da giầy";
                            break;
                        case "2":
                            e.Row.Cells["LoaiNganhNghe"].Text = "May mặc";
                            break;
                        case "3":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Điện tử, điện lạnh";
                            break;
                        case "4":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Chế biến thực phẩm";
                            break;
                        case "5":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Cơ khí";
                            break;
                        case "6":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Gỗ";
                            break;
                        case "7":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Nhựa";
                            break;
                        case "8":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Nông sản";
                            break;
                        case "9":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Loại khác";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListKiemTra_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListNganhHangSXChinh_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string LoaiNganhNghe = e.Row.Cells["LoaiNganhNghe"].Value.ToString();
                    switch (LoaiNganhNghe)
                    {
                        case "1":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Da giầy";
                            break;
                        case "2":
                            e.Row.Cells["LoaiNganhNghe"].Text = "May mặc";
                            break;
                        case "3":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Điện tử, điện lạnh";
                            break;
                        case "4":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Chế biến thực phẩm";
                            break;
                        case "5":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Cơ khí";
                            break;
                        case "6":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Gỗ";
                            break;
                        case "7":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Nhựa";
                            break;
                        case "8":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Nông sản";
                            break;
                        case "9":
                            e.Row.Cells["LoaiNganhNghe"].Text = "Loại khác";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListCongTyMe_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListThanhVien_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {

        }

        private void dgListCTM_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                holdingCompany = (KDT_VNACCS_HoldingCompany)dgListCTM.GetRow().DataRow;
                txtTenDoanhNghiepCTM.Text = holdingCompany.TenDoanhNghiep;
                txtMaDoanhNghiepCTM.Text = holdingCompany.MaDoanhNghiep;
                txtDiaChiCSSXCTM.Text = holdingCompany.DiaChiCSSX;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void uiGroupBox22_Click(object sender, EventArgs e)
        {

        }

        private void txtSoLuongSoHuu_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuu.Text) + Convert.ToDecimal(txtSoLuongKhac.Text) + Convert.ToDecimal(txtSoLuongDiThue.Text);
                txtTongSoLuong.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongKhac_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuu.Text) + Convert.ToDecimal(txtSoLuongKhac.Text) + Convert.ToDecimal(txtSoLuongDiThue.Text);
                txtTongSoLuong.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongDiThue_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuu.Text) + Convert.ToDecimal(txtSoLuongKhac.Text) + Convert.ToDecimal(txtSoLuongDiThue.Text);
                txtTongSoLuong.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormStorageOfGoods(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaDiaDiemLGHH, errorProvider, "Mã địa điểm lưu giữ hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDiaDiemLGHH, errorProvider, "Tên địa điểm lưu giữ hàng hóa", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void BindDataStorageOfGoods()
        {
            try
            {
                dgListDDLGHH.Refresh();
                dgListDDLGHH.DataSource = storageAreasProduction.StorageOfGoodCollection;
                dgListDDLGHH.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                // 
            }
        }
        private void btnAddDDLGHH_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormStorageOfGoods(false))
                    return;
                if (storageOfGood == null)
                {
                    storageOfGood = new KDT_VNACCS_StorageOfGood();
                    isAddNew = true;
                }
                storageOfGood.Ma = txtMaDiaDiemLGHH.Text;
                storageOfGood.Ten = txtTenDiaDiemLGHH.Text;
                if (isAddNew)
                    storageAreasProduction.StorageOfGoodCollection.Add(storageOfGood);
                storageOfGood = new KDT_VNACCS_StorageOfGood();
                txtMaDiaDiemLGHH.Text = String.Empty;
                txtTenDiaDiemLGHH.Text = String.Empty;
                BindDataStorageOfGoods();
                storageOfGood = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteDDLGHH_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListDDLGHH.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            storageOfGood = (KDT_VNACCS_StorageOfGood)i.GetRow().DataRow;
                            storageAreasProduction.StorageOfGoodCollection.Remove(storageOfGood);
                            if (storageOfGood.ID > 0)
                            {
                                storageOfGood.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaDiaDiemLGHH.Text = String.Empty;
                    txtTenDiaDiemLGHH.Text = String.Empty;
                    BindDataStorageOfGoods();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListDDLGHH_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                storageOfGood = (KDT_VNACCS_StorageOfGood)dgListDDLGHH.GetRow().DataRow;
                txtTenDiaDiemLGHH.Text = storageOfGood.Ten;
                txtMaDiaDiemLGHH.Text = storageOfGood.Ma;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormCareeriesProduct(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaSPCK, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaHSCK, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbCKSXDVT, errorProvider, "Chu kỳ sản xuất (ĐVT)", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtChuKySXTG, errorProvider, "Chu kỳ sản xuất (thời gian)", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void BindDataCareeriesProduct()
        {
            try
            {
                dgListSPCKSX.Refetch();
                dgListSPCKSX.DataSource = careery.CareeriesProductCollection;
                dgListSPCKSX.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddSPCKSX_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormCareeriesProduct(false))
                    return;
                if (careery == null)
                    careery = new KDT_VNACCS_Careery();
                //HungTQ, 20220318, Fix bug: ChuKySanXuat null                
                careery.ChuKySanXuat = cbbCKSXDVT.Text;                

                if (careeriesProduct == null)
                {
                    careeriesProduct = new KDT_VNACCS_Careeries_Product();
                    isAddNew = true;
                }
                careeriesProduct.MaSP = txtMaSPCK.Text;
                careeriesProduct.MaHS = ctrMaHSCK.Code;
                careeriesProduct.ChuKySXTG = Convert.ToInt32(txtChuKySXTG.Text);
                careeriesProduct.ChuKySXDVT = Convert.ToInt32(cbbCKSXDVT.SelectedValue.ToString());
                if (isAddNew)
                    careery.CareeriesProductCollection.Add(careeriesProduct);
                careeriesProduct = new KDT_VNACCS_Careeries_Product();
                careeriesProduct = null;
                txtMaSPCK.Text = String.Empty;
                ctrMaHSCK.Code = String.Empty;
                txtChuKySXTG.Text = String.Empty;
                cbbCKSXDVT.Text = String.Empty;
                BindDataCareeriesProduct();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteSPCKSX_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListSPCKSX.SelectedItems;
                    List<KDT_VNACCS_Careeries_Product> ItemColl = new List<KDT_VNACCS_Careeries_Product>();
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_Careeries_Product)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_VNACCS_Careeries_Product item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        careery.CareeriesProductCollection.Remove(item);
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaSPCK.Text = String.Empty;
                    ctrMaHSCK.Code = String.Empty;
                    txtChuKySXTG.Text = String.Empty;
                    cbbCKSXDVT.Text = String.Empty;
                    BindDataCareeriesProduct();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPCKSX_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                careeriesProduct = (KDT_VNACCS_Careeries_Product)dgListSPCKSX.GetRow().DataRow;
                txtMaSPCK.Text = careeriesProduct.MaSP;
                ctrMaHSCK.Code = careeriesProduct.MaHS;
                txtChuKySXTG.Text = careeriesProduct.ChuKySXTG.ToString();
                cbbCKSXDVT.SelectedValue = careeriesProduct.ChuKySXDVT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void dgListSPCKSX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string ChuKySX = e.Row.Cells["ChuKySXDVT"].Value.ToString();
                    switch (ChuKySX)
                    {
                        case "1":
                            e.Row.Cells["ChuKySXDVT"].Text = "Năm";
                            break;
                        case "2":
                            e.Row.Cells["ChuKySXDVT"].Text = "Qúy";
                            break;
                        case "3":
                            e.Row.Cells["ChuKySXDVT"].Text = "Tháng";
                            break;
                        case "4":
                            e.Row.Cells["ChuKySXDVT"].Text = "Tuần";
                            break;
                        case "5":
                            e.Row.Cells["ChuKySXDVT"].Text = "Ngày";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void txtMaSPCK_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaSPCK.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    ctrMaHSCK.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    //this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                }

            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaSPCK.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    ctrMaHSCK.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    //ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
            }
#elif GC_V4
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaSPCK.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        ctrMaHSCK.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        //ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
#endif
        }

        private void txtMaSPNLSX_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaSPNLSX.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    ctrMaHSNLSX.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    ctrDVT.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                }

            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaSPNLSX.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    ctrMaHSNLSX.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
            }
#elif GC_V4
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaSPNLSX.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        ctrMaHSNLSX.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
#endif
        }

        private bool ValidateFormProductionCapacityProduct(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaSPNLSX, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaHSNLSX, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrDVT, errorProvider, "ĐVT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbThoiGianSXDVT, errorProvider, "Thời gian sản xuất (ĐVT)", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtThoiGianSXTG, errorProvider, "Thời gian sản xuất (thời gian)", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void BindDataProductionCapacityProduct()
        {
            try
            {
                dgListSPNLSX.Refetch();
                dgListSPNLSX.DataSource = careery.ProductionCapacityProductCollection;
                dgListSPNLSX.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnAddSPNLSX_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormProductionCapacityProduct(false))
                    return;
                if (careery == null)
                    careery = new KDT_VNACCS_Careery();
                if (productionCapacityProduct == null)
                {
                    productionCapacityProduct = new KDT_VNACCS_ProductionCapacity_Product();
                    isAddNew = true;
                }
                productionCapacityProduct.MaSP = txtMaSPNLSX.Text;
                productionCapacityProduct.MaHS = ctrMaHSNLSX.Code;
                productionCapacityProduct.DVT = ctrDVT.Code;
                productionCapacityProduct.SoLuong = Convert.ToDecimal(txtSoLuongSP.Text.ToString());
                productionCapacityProduct.ThoiGianSXTG = Convert.ToInt32(txtThoiGianSXTG.Text);
                productionCapacityProduct.ThoiGianSXDVT = Convert.ToInt32(cbbThoiGianSXDVT.SelectedValue.ToString());
                if (isAddNew)
                    careery.ProductionCapacityProductCollection.Add(productionCapacityProduct);
                productionCapacityProduct = new KDT_VNACCS_ProductionCapacity_Product();
                productionCapacityProduct = null;
                txtMaSPNLSX.Text = String.Empty;
                ctrMaHSNLSX.Code = String.Empty;
                ctrDVT.Code = String.Empty;
                txtSoLuongSP.Text = String.Empty;
                txtThoiGianSXTG.Text = String.Empty;
                cbbThoiGianSXDVT.Text = String.Empty;
                BindDataProductionCapacityProduct();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteSPNLSX_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListSPNLSX.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            productionCapacityProduct = (KDT_VNACCS_ProductionCapacity_Product)i.GetRow().DataRow;
                            careery.ProductionCapacityProductCollection.Remove(productionCapacityProduct);
                            if (productionCapacityProduct.ID > 0)
                            {
                                productionCapacityProduct.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaSPNLSX.Text = String.Empty;
                    ctrMaHSNLSX.Code = String.Empty;
                    ctrDVT.Code = String.Empty;
                    txtSoLuongSP.Text = String.Empty;
                    txtThoiGianSXTG.Text = String.Empty;
                    cbbThoiGianSXDVT.Text = String.Empty;
                    BindDataProductionCapacityProduct();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPNLSX_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                productionCapacityProduct = (KDT_VNACCS_ProductionCapacity_Product)dgListSPNLSX.GetRow().DataRow;
                txtMaSPNLSX.Text = productionCapacityProduct.MaSP;
                ctrMaHSNLSX.Code = productionCapacityProduct.MaHS;
                ctrDVT.Code = productionCapacityProduct.DVT;
                txtSoLuongSP.Text = productionCapacityProduct.SoLuong.ToString();
                txtThoiGianSXTG.Text = productionCapacityProduct.ThoiGianSXTG.ToString();
                cbbThoiGianSXDVT.SelectedValue = productionCapacityProduct.ThoiGianSXDVT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPNLSX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string ThoiGianSX = e.Row.Cells["ThoiGianSXDVT"].Value.ToString();
                    switch (ThoiGianSX)
                    {
                        case "1":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Năm";
                            break;
                        case "2":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Qúy";
                            break;
                        case "3":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Tháng";
                            break;
                        case "4":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Tuần";
                            break;
                        case "5":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Ngày";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        /// <summary>
        /// Tinh tong dung luong
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        public void BindDataAttachedFile()
        {
            try
            {

                // Tính tổng dung lượng File
                long size = 0;
                for (int i = 0; i < storageAreasProduction.AttachedFileGoodCollection.Count; i++)
                {
                    size += Convert.ToInt64(storageAreasProduction.AttachedFileGoodCollection[i].FileSize);
                }
                lblTotalSize.Text = CalculateFileSize(size);

                dgListAttachFile.Refetch();
                dgListAttachFile.DataSource = storageAreasProduction.AttachedFileGoodCollection;
                dgListAttachFile.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog OpenFileDialog = new OpenFileDialog();

                OpenFileDialog.FileName = "";
                OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                        + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                        + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

                OpenFileDialog.Multiselect = false;
                if (OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                    if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                      && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                      && fin.Extension.ToUpper() != ".gif".ToUpper()
                      && fin.Extension.ToUpper() != ".tiff".ToUpper()
                      && fin.Extension.ToUpper() != ".txt".ToUpper()
                      && fin.Extension.ToUpper() != ".xml".ToUpper()
                      && fin.Extension.ToUpper() != ".xsl".ToUpper()
                      && fin.Extension.ToUpper() != ".csv".ToUpper()
                      && fin.Extension.ToUpper() != ".doc".ToUpper()
                      && fin.Extension.ToUpper() != ".mdb".ToUpper()
                      && fin.Extension.ToUpper() != ".pdf".ToUpper()
                      && fin.Extension.ToUpper() != ".ppt".ToUpper()
                      && fin.Extension.ToUpper() != ".xls".ToUpper())
                    {
                        ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                    }
                    else
                    {
                        System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        size = 0;
                        for (int i = 0; i < storageAreasProduction.AttachedFileGoodCollection.Count; i++)
                        {
                            size += Convert.ToInt64(storageAreasProduction.AttachedFileGoodCollection[i].FileSize);
                        }
                        size += fs.Length;
                        //if (size > 3145728)
                        //{
                        //    this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(3145728), CalculateFileSize(size)), false);
                        //    return;
                        //}
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                        }
                        byte[] data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        filesize = fs.Length;
                        bool isAddNew = false;
                        if (attachedFile == null)
                        {
                            attachedFile = new KDT_VNACCS_StorageAreasProduction_AttachedFile();
                            isAddNew = true;
                        }
                        attachedFile.FileName = fin.Name;
                        attachedFile.FileSize = filesize;
                        attachedFile.Content = filebase64;
                        if (isAddNew)
                            storageAreasProduction.AttachedFileGoodCollection.Add(attachedFile);
                        lblFileName.Text = fin.Name;
                        txtFileSize.Text = CalculateFileSize(filesize).ToString();
                        lblTotalSize.Text = CalculateFileSize(size);
                        attachedFile = new KDT_VNACCS_StorageAreasProduction_AttachedFile();
                        BindDataAttachedFile();
                        attachedFile = null;
                    }
                    //BindDataAttachedFile();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnViewFile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string path = Application.StartupPath + "\\Temp";
                System.IO.FileStream fs;
                byte[] bytes;
                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                if (dgListAttachFile.GetRow() == null)
                {
                    ShowMessage("Chưa chọn file để xem", false);
                    return;
                }
                KDT_VNACCS_StorageAreasProduction_AttachedFile fileData = (KDT_VNACCS_StorageAreasProduction_AttachedFile)dgListAttachFile.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(fileData.Content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnDeleteFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListAttachFile.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            attachedFile = (KDT_VNACCS_StorageAreasProduction_AttachedFile)i.GetRow().DataRow;
                            storageAreasProduction.AttachedFileGoodCollection.Remove(attachedFile);
                            if (attachedFile.ID > 0)
                            {
                                attachedFile.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    attachedFile = null;
                    BindDataAttachedFile();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListAttachFile_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                attachedFile = (KDT_VNACCS_StorageAreasProduction_AttachedFile)dgListAttachFile.GetRow().DataRow;
                txtFileSize.Text = CalculateFileSize(attachedFile.FileSize); ;
                lblFileName.Text = attachedFile.FileName;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        public void BindDataContractDocument()
        {
            try
            {
                grListHD.Refresh();
                grListHD.DataSource = outsourcingManufactureFactory.ContractDocumentCollection;
                grListHD.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormContractDocumnents(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "Số hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcNgayHD, errorProvider, "Ngày hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayHH, errorProvider, "Ngày hết hạn hợp đồng", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAddHD_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormContractDocumnents(false))
                    return;
                if (outsourcingManufactureFactory == null)
                {
                    outsourcingManufactureFactory = new KDT_VNACCS_OutsourcingManufactureFactory();
                    isAddNew = true;
                }
                if (outsourcingManufactureFactoryContractDocument == null)
                {
                    outsourcingManufactureFactoryContractDocument = new KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument();
                    isAddNew = true;
                }
                outsourcingManufactureFactoryContractDocument.SoHopDong = txtSoHopDong.Text;
                outsourcingManufactureFactoryContractDocument.NgayHopDong = clcNgayHD.Value;
                outsourcingManufactureFactoryContractDocument.NgayHetHan = clcNgayHH.Value;
                if (isAddNew)
                    outsourcingManufactureFactory.ContractDocumentCollection.Add(outsourcingManufactureFactoryContractDocument);
                outsourcingManufactureFactoryContractDocument = new KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument();
                txtSoHopDong.Text = String.Empty;
                BindDataContractDocument();
                outsourcingManufactureFactoryContractDocument = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteHD_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = grListHD.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            outsourcingManufactureFactoryContractDocument = (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument)i.GetRow().DataRow;
                            outsourcingManufactureFactory.ContractDocumentCollection.Remove(outsourcingManufactureFactoryContractDocument);
                            if (outsourcingManufactureFactoryContractDocument.ID > 0)
                            {
                                outsourcingManufactureFactoryContractDocument.Delete();
                            }
                        }
                    }
                    txtSoHopDong.Text = String.Empty;
                    ShowMessage("Xóa thành công", false);
                    BindDataContractDocument();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListHD_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                outsourcingManufactureFactoryContractDocument = (KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument)grListHD.GetRow().DataRow;
                txtSoHopDong.Text = outsourcingManufactureFactoryContractDocument.SoHopDong;
                clcNgayHD.Value = outsourcingManufactureFactoryContractDocument.NgayHopDong;
                clcNgayHH.Value = outsourcingManufactureFactoryContractDocument.NgayHetHan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private bool ValidateFormOutsourcingManufactureFactoryProduct(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaSPCKSXDT, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaHSCKSXDT, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbChuKySXDVTDT, errorProvider, "Chu kỳ sản xuất (ĐVT)", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtChuKySXTGDT, errorProvider, "Chu kỳ sản xuất (thời gian)", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void BindDataOutsourcingManufactureFactoryProduct()
        {
            try
            {
                dgListSPCKSXDT.Refresh();
                dgListSPCKSXDT.DataSource = outsourcingManufactureFactoryManufactureFactory.ProductCollection;
                dgListSPCKSXDT.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnAddSPCKSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormOutsourcingManufactureFactoryProduct(false))
                    return;
                if (outsourcingManufactureFactoryManufactureFactory == null)
                {
                    outsourcingManufactureFactoryManufactureFactory = new KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory();
                }
                if (outsourcingManufactureFactoryProduct == null)
                {
                    outsourcingManufactureFactoryProduct = new KDT_VNACCS_OutsourcingManufactureFactory_Product();
                    isAddNew = true;
                }
                outsourcingManufactureFactoryProduct.MaSP = txtMaSPCKSXDT.Text;
                outsourcingManufactureFactoryProduct.MaHS = ctrMaHSCKSXDT.Code;
                outsourcingManufactureFactoryProduct.ChuKySXTG = Convert.ToInt32(txtChuKySXTGDT.Text);
                outsourcingManufactureFactoryProduct.ChuKySXDVT = Convert.ToInt32(cbbChuKySXDVTDT.SelectedValue.ToString());
                if (isAddNew)
                    outsourcingManufactureFactoryManufactureFactory.ProductCollection.Add(outsourcingManufactureFactoryProduct);
                outsourcingManufactureFactoryProduct = new KDT_VNACCS_OutsourcingManufactureFactory_Product();
                txtMaSPCKSXDT.Text = String.Empty;
                ctrMaHSCKSXDT.Code = String.Empty;
                txtChuKySXTGDT.Text = String.Empty;
                cbbChuKySXDVTDT.Text = String.Empty;
                BindDataOutsourcingManufactureFactoryProduct();
                outsourcingManufactureFactoryProduct = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteSPCKSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListSPCKSXDT.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            outsourcingManufactureFactoryProduct = (KDT_VNACCS_OutsourcingManufactureFactory_Product)i.GetRow().DataRow;
                            outsourcingManufactureFactoryManufactureFactory.ProductCollection.Remove(outsourcingManufactureFactoryProduct);
                            if (outsourcingManufactureFactoryProduct.ID > 0)
                            {
                                outsourcingManufactureFactoryProduct.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaSPCKSXDT.Text = String.Empty;
                    ctrMaHSCKSXDT.Code = String.Empty;
                    txtChuKySXTGDT.Text = String.Empty;
                    cbbChuKySXDVTDT.Text = String.Empty;
                    BindDataOutsourcingManufactureFactoryProduct();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPCKSXDT_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                outsourcingManufactureFactoryProduct = (KDT_VNACCS_OutsourcingManufactureFactory_Product)dgListSPCKSXDT.GetRow().DataRow;
                txtMaSPCKSXDT.Text = outsourcingManufactureFactoryProduct.MaSP;
                ctrMaHSCKSXDT.Code = outsourcingManufactureFactoryProduct.MaHS;
                txtChuKySXTGDT.Text = outsourcingManufactureFactoryProduct.ChuKySXTG.ToString();
                cbbChuKySXDVTDT.SelectedValue = outsourcingManufactureFactoryProduct.ChuKySXDVT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPCKSXDT_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string ChuKySX = e.Row.Cells["ChuKySXDVT"].Value.ToString();
                    switch (ChuKySX)
                    {
                        case "1":
                            e.Row.Cells["ChuKySXDVT"].Text = "Năm";
                            break;
                        case "2":
                            e.Row.Cells["ChuKySXDVT"].Text = "Qúy";
                            break;
                        case "3":
                            e.Row.Cells["ChuKySXDVT"].Text = "Tháng";
                            break;
                        case "4":
                            e.Row.Cells["ChuKySXDVT"].Text = "Tuần";
                            break;
                        case "5":
                            e.Row.Cells["ChuKySXDVT"].Text = "Ngày";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtMaSPCKSXDT_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaSPCKSXDT.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    ctrMaHSCKSXDT.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    //this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                }

            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaSPCKSXDT.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    ctrMaHSCKSXDT.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    //ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
            }
#elif GC_V4
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaSPCKSXDT.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        ctrMaHSCKSXDT.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        //ctrDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
#endif
        }

        private void btnAddSPNLSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateOutSourceFormProductionCapacityProduct(false))
                    return;
                if (outsourcingManufactureFactoryProductionCapacityProduct == null)
                {
                    outsourcingManufactureFactoryProductionCapacityProduct = new KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product();
                    isAddNew = true;
                }

                outsourcingManufactureFactoryProductionCapacityProduct.MaSP = txtMaSPNLSXDT.Text;
                outsourcingManufactureFactoryProductionCapacityProduct.MaHS = ctrMaHSNLSXDT.Code;
                outsourcingManufactureFactoryProductionCapacityProduct.DVT = ctrDVTDT.Code;
                outsourcingManufactureFactoryProductionCapacityProduct.SoLuong = Convert.ToDecimal(txtSoLuongSPDT.Text);
                outsourcingManufactureFactoryProductionCapacityProduct.ThoiGianSXTG = Convert.ToInt32(txtThoiGianSXTGDT.Text);
                outsourcingManufactureFactoryProductionCapacityProduct.ThoiGianSXDVT = Convert.ToInt32(cbbThoiGianSXDVTDT.SelectedValue.ToString());
                if (isAddNew)
                    outsourcingManufactureFactoryManufactureFactory.ProductionCapacityProductCollection.Add(outsourcingManufactureFactoryProductionCapacityProduct);
                outsourcingManufactureFactoryProductionCapacityProduct = new KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product();
                BindDataOutSourceProductionCapacityProduct();
                txtMaSPNLSXDT.Text = String.Empty;
                ctrMaHSNLSXDT.Code = String.Empty;
                ctrDVTDT.Code = String.Empty;
                txtSoLuongSPDT.Text = String.Empty;
                txtThoiGianSXTGDT.Text = String.Empty;
                cbbThoiGianSXDVTDT.Text = String.Empty;
                outsourcingManufactureFactoryProductionCapacityProduct = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteSPNLSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListSPNLSXDT.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            outsourcingManufactureFactoryProductionCapacityProduct = (KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product)i.GetRow().DataRow;
                            outsourcingManufactureFactoryManufactureFactory.ProductionCapacityProductCollection.Remove(outsourcingManufactureFactoryProductionCapacityProduct);
                            if (outsourcingManufactureFactoryProductionCapacityProduct.ID > 0)
                            {
                                outsourcingManufactureFactoryProductionCapacityProduct.Delete();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaSPNLSXDT.Text = String.Empty;
                    ctrMaHSNLSXDT.Code = String.Empty;
                    ctrDVTDT.Code = String.Empty;
                    txtSoLuongSPDT.Text = String.Empty;
                    txtThoiGianSXTGDT.Text = String.Empty;
                    cbbThoiGianSXDVTDT.Text = String.Empty;
                    BindDataOutSourceProductionCapacityProduct();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListSPNLSXDT_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                outsourcingManufactureFactoryProductionCapacityProduct = (KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product)dgListSPNLSXDT.GetRow().DataRow;
                txtMaSPNLSXDT.Text = outsourcingManufactureFactoryProductionCapacityProduct.MaSP;
                ctrMaHSNLSXDT.Code = outsourcingManufactureFactoryProductionCapacityProduct.MaHS;
                ctrDVTDT.Code = outsourcingManufactureFactoryProductionCapacityProduct.DVT;
                txtSoLuongSPDT.Text = outsourcingManufactureFactoryProductionCapacityProduct.SoLuong.ToString();
                txtThoiGianSXTGDT.Text = outsourcingManufactureFactoryProductionCapacityProduct.ThoiGianSXTG.ToString();
                cbbThoiGianSXDVTDT.SelectedValue = outsourcingManufactureFactoryProductionCapacityProduct.ThoiGianSXDVT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private bool ValidateOutSourceFormProductionCapacityProduct(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaSPNLSXDT, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaHSNLSXDT, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrDVTDT, errorProvider, "ĐVT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbThoiGianSXDVTDT, errorProvider, "Thời gian sản xuất (ĐVT)", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtThoiGianSXTGDT, errorProvider, "Thời gian sản xuất (thời gian)", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongSPDT, errorProvider, "Số lượng sản phẩm", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void BindDataOutSourceProductionCapacityProduct()
        {
            try
            {
                dgListSPNLSXDT.Refresh();
                dgListSPNLSXDT.DataSource = outsourcingManufactureFactoryManufactureFactory.ProductionCapacityProductCollection;
                dgListSPNLSXDT.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtMaSPNLSXDT_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaSPNLSXDT.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    ctrMaHSNLSXDT.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    ctrDVTDT.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                }

            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaSPNLSXDT.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    ctrMaHSNLSXDT.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    ctrDVTDT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
            }
#elif GC_V4
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaSPNLSXDT.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        ctrMaHSNLSXDT.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrDVTDT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
#endif
        }

        private void dgListSPNLSXDT_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string ThoiGianSX = e.Row.Cells["ThoiGianSXDVT"].Value.ToString();
                    switch (ThoiGianSX)
                    {
                        case "1":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Năm";
                            break;
                        case "2":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Qúy";
                            break;
                        case "3":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Tháng";
                            break;
                        case "4":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Tuần";
                            break;
                        case "5":
                            e.Row.Cells["ThoiGianSXDVT"].Text = "Ngày";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void uiGroupBox48_Click(object sender, EventArgs e)
        {

        }

        private void grListHD_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }
        private void BinDataOurSourceManufactureFactory()
        {
            try
            {
                dgListDoiTac.Refresh();
                dgListDoiTac.DataSource = storageAreasProduction.OutsourcingManufactureFactoryCollection;
                dgListDoiTac.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void BinDataOurSourceManufactureFactoryManufactureFactory()
        {
            try
            {
                dgListTTCSSX.Refresh();
                dgListTTCSSX.DataSource = outsourcingManufactureFactory.ManufactureFactoryCollection;
                dgListTTCSSX.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnAddCSSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateOurFormManufactureFactory(false))
                    return;
                if (outsourcingManufactureFactory == null)
                {
                    ShowMessage("Bạn chưa thêm danh sách Cơ sở sản xuất", false);
                    return;
                }
                if (outsourcingManufactureFactory.ContractDocumentCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm danh sách Hợp đồng", false);
                    return;
                }
                if (outsourcingManufactureFactory.ManufactureFactoryCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm danh sách Cơ sở sản xuất", false);
                    return;
                }
                if (outsourcingManufactureFactory.MaDoiTac == null)
                    isAddNew = true;
                outsourcingManufactureFactory.MaDoiTac = txtMaDoanhNghiepDT.Text.ToString();
                outsourcingManufactureFactory.TenDoiTac = txtTenDoanhNghiepDT.Text.ToString();
                outsourcingManufactureFactory.DiaChiDoiTac = txtDiaChiDoanhNghiepDT.Text.ToString();

                if (isAddNew)
                    storageAreasProduction.OutsourcingManufactureFactoryCollection.Add(outsourcingManufactureFactory);
                outsourcingManufactureFactory = new KDT_VNACCS_OutsourcingManufactureFactory();
                txtMaDoanhNghiepDT.Text = String.Empty;
                txtTenDoanhNghiepDT.Text = String.Empty;
                txtDiaChiDoanhNghiepDT.Text = String.Empty;
                BinDataOurSourceManufactureFactory();
                BindDataContractDocument();
                //BindDataOutSourceProductionCapacityProduct();
                //BindDataOutsourcingManufactureFactoryProduct();
                outsourcingManufactureFactory = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteCSSXDT_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListDoiTac.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            outsourcingManufactureFactory = (KDT_VNACCS_OutsourcingManufactureFactory)i.GetRow().DataRow;
                            storageAreasProduction.OutsourcingManufactureFactoryCollection.Remove(outsourcingManufactureFactory);
                            if (outsourcingManufactureFactory.ID > 0)
                            {
                                outsourcingManufactureFactory.DeleteFull();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtMaDoanhNghiepDT.Text = String.Empty;
                    txtTenDoanhNghiepDT.Text = String.Empty;
                    txtDiaChiDoanhNghiepDT.Text = String.Empty;
                    BinDataOurSourceManufactureFactory();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListDoiTac_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                outsourcingManufactureFactory = (KDT_VNACCS_OutsourcingManufactureFactory)dgListDoiTac.GetRow().DataRow;
                txtMaDoanhNghiepDT.Text = outsourcingManufactureFactory.MaDoiTac;
                txtTenDoanhNghiepDT.Text = outsourcingManufactureFactory.TenDoiTac.ToString();
                txtDiaChiDoanhNghiepDT.Text = outsourcingManufactureFactory.DiaChiDoiTac.ToString();
                BindDataContractDocument();
                BinDataOurSourceManufactureFactoryManufactureFactory();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private bool ValidateFormManufactureFactoryManufactureFactory(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtDienTichNXDT, errorProvider, "Diện tích nhà xưởng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiCSSXDT, errorProvider, "Địa chỉ CSSX đối tác", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongCNDT, errorProvider, "Số lượng công nhân", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongSoHuuDT, errorProvider, "Số lượng sở hữu", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongDiThueDT, errorProvider, "Số lượng đi thuê", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongKhacDT, errorProvider, "Số lượng khác", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTongSoLuongDT, errorProvider, "Tổng Số lượng ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAddTTCSSX_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAddNew = false;
                if (!ValidateFormManufactureFactoryManufactureFactory(false))
                    return;
                if (outsourcingManufactureFactoryManufactureFactory == null)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Chu kỳ sản xuất và Năng lực sản xuất ", false);
                    return;
                }
                if (outsourcingManufactureFactoryManufactureFactory.ProductCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Chu kỳ sản xuất ", false);
                    return;
                }
                if (outsourcingManufactureFactoryManufactureFactory.ProductionCapacityProductCollection.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm Danh sách sản phẩm của Năng lực sản xuất ", false);
                    return;
                }
                if (outsourcingManufactureFactoryManufactureFactory.SoLuongCongNhan == 0)
                    isAddNew = true;
                outsourcingManufactureFactoryProductCollection = outsourcingManufactureFactoryManufactureFactory.ProductCollection;
                outsourcingManufactureFactoryProductionCapacityProductCollection = outsourcingManufactureFactoryManufactureFactory.ProductionCapacityProductCollection;

                outsourcingManufactureFactoryManufactureFactory.DiaChiCSSX = txtDiaChiCSSXDT.Text.ToString();
                outsourcingManufactureFactoryManufactureFactory.SoLuongCongNhan = Convert.ToDecimal(txtSoLuongCNDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.DienTichNX = Convert.ToDecimal(txtDienTichNXDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.SoLuongSoHuu = Convert.ToDecimal(txtSoLuongSoHuuDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.SoLuongDiThue = Convert.ToDecimal(txtSoLuongDiThueDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.SoLuongKhac = Convert.ToDecimal(txtSoLuongKhacDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.TongSoLuong = Convert.ToDecimal(txtTongSoLuongDT.Text.ToString());
                outsourcingManufactureFactoryManufactureFactory.NangLucSX = txtNangLucSXDT.Text.ToString();
                if (isAddNew)
                    outsourcingManufactureFactory.ManufactureFactoryCollection.Add(outsourcingManufactureFactoryManufactureFactory);
                outsourcingManufactureFactoryManufactureFactory = new KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory();

                txtDiaChiCSSXDT.Text = String.Empty;
                txtDienTichNXDT.Text = String.Empty;
                txtSoLuongCNDT.Text = String.Empty;
                txtSoLuongSoHuuDT.Text = String.Empty;
                txtSoLuongKhacDT.Text = String.Empty;
                txtSoLuongDiThueDT.Text = String.Empty;
                txtTongSoLuongDT.Text = String.Empty;
                txtNangLucSXDT.Text = String.Empty;
                BinDataOurSourceManufactureFactoryManufactureFactory();
                BindDataOutSourceProductionCapacityProduct();
                BindDataOutsourcingManufactureFactoryProduct();
                outsourcingManufactureFactoryManufactureFactory = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDeleteTTCSSX_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListDoiTac.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            outsourcingManufactureFactoryManufactureFactory = (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory)i.GetRow().DataRow;
                            outsourcingManufactureFactory.ManufactureFactoryCollection.Remove(outsourcingManufactureFactoryManufactureFactory);
                            if (outsourcingManufactureFactoryManufactureFactory.ID > 0)
                            {
                                outsourcingManufactureFactoryManufactureFactory.DeleteFull();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    txtDiaChiCSSXDT.Text = String.Empty;
                    txtDienTichNXDT.Text = String.Empty;
                    txtSoLuongCNDT.Text = String.Empty;
                    txtSoLuongSoHuuDT.Text = String.Empty;
                    txtSoLuongKhacDT.Text = String.Empty;
                    txtSoLuongDiThueDT.Text = String.Empty;
                    txtTongSoLuongDT.Text = String.Empty;
                    txtNangLucSXDT.Text = String.Empty;
                    BinDataOurSourceManufactureFactoryManufactureFactory();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListTTCSSX_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                outsourcingManufactureFactoryManufactureFactory = (KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory)dgListTTCSSX.GetRow().DataRow;
                txtDiaChiCSSXDT.Text = outsourcingManufactureFactoryManufactureFactory.DiaChiCSSX.ToString();
                txtDienTichNXDT.Text = outsourcingManufactureFactoryManufactureFactory.DienTichNX.ToString();
                txtSoLuongCNDT.Text = outsourcingManufactureFactoryManufactureFactory.SoLuongCongNhan.ToString();
                txtSoLuongSoHuuDT.Text = outsourcingManufactureFactoryManufactureFactory.SoLuongSoHuu.ToString();
                txtSoLuongDiThueDT.Text = outsourcingManufactureFactoryManufactureFactory.SoLuongDiThue.ToString();
                txtSoLuongKhacDT.Text = outsourcingManufactureFactoryManufactureFactory.SoLuongKhac.ToString();
                txtTongSoLuongDT.Text = outsourcingManufactureFactoryManufactureFactory.TongSoLuong.ToString();
                txtNangLucSXDT.Text = outsourcingManufactureFactoryManufactureFactory.NangLucSX.ToString();
                BindDataOutSourceProductionCapacityProduct();
                BindDataOutsourcingManufactureFactoryProduct();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongSoHuuCSSX_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuCSSX.Text) + Convert.ToDecimal(txtSoLuongKhacCSSX.Text) + Convert.ToDecimal(txtSoLuongDiThueCSSX.Text);
                txtTongSoLuongCSSX.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongDiThueCSSX_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuCSSX.Text) + Convert.ToDecimal(txtSoLuongKhacCSSX.Text) + Convert.ToDecimal(txtSoLuongDiThueCSSX.Text);
                txtTongSoLuongCSSX.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongKhacCSSX_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuCSSX.Text) + Convert.ToDecimal(txtSoLuongKhacCSSX.Text) + Convert.ToDecimal(txtSoLuongDiThueCSSX.Text);
                txtTongSoLuongCSSX.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTongSoLuongCSSX_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuCSSX.Text) + Convert.ToDecimal(txtSoLuongKhacCSSX.Text) + Convert.ToDecimal(txtSoLuongDiThueCSSX.Text);
                txtTongSoLuongCSSX.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongSoHuuDT_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuDT.Text) + Convert.ToDecimal(txtSoLuongKhacDT.Text) + Convert.ToDecimal(txtSoLuongDiThueDT.Text);
                txtTongSoLuongDT.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongDiThueDT_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuDT.Text) + Convert.ToDecimal(txtSoLuongKhacDT.Text) + Convert.ToDecimal(txtSoLuongDiThueDT.Text);
                txtTongSoLuongDT.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtSoLuongKhacDT_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuDT.Text) + Convert.ToDecimal(txtSoLuongKhacDT.Text) + Convert.ToDecimal(txtSoLuongDiThueDT.Text);
                txtTongSoLuongDT.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtTongSoLuongDT_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal TongSoLuong = Convert.ToDecimal(txtSoLuongSoHuuDT.Text) + Convert.ToDecimal(txtSoLuongKhacDT.Text) + Convert.ToDecimal(txtSoLuongDiThueDT.Text);
                txtTongSoLuongDT.Value = TongSoLuong;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListCSSX_LoadingRow_1(object sender, RowLoadEventArgs e)
        {

        }
    }
}
