﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
    public partial class VAD2AE0 :BasicVNACC
    {
        public List<VAD2AE0_HANG> HangMD { get; set; }
        public void LoadVAD2AE0(string strResult)
        {
            try
            {

                this.GetObject<VAD2AE0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD2AE0, false, VAD2AE0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AE0.TongSoByte);
                while (true)
                {
                    VAD2AE0_HANG vad1agHang = new VAD2AE0_HANG();
                    vad1agHang.GetObject<VAD2AE0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD2AE0_HANG", false, VAD2AE0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD2AE0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD2AE0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD2AE0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AE0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
