using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.GC.BLL.GC
{
    public partial class BKToKhai
    {


        public DataSet BaoCao09TT117(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            string spName = "p_GC_BC09HSTK_GC_TT117_NEW";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet BaoCao07TT117(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            string spName = "[p_GC_BC07HSTK_GC_TT117]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet BaoCao07TT117_NEW(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            string spName = "[p_GC_BC07HSTK_GC_TT117_NEW]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet getToKhaiNK(long IDHopDong)
        {
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string strSQL = " select * from t_GC_BKToKhai Where IDHopDong= @IDHopDong AND MaLoaiHinh like 'N%' ";
            DbCommand dbcommand = db.GetSqlStringCommand(strSQL);
            //db.AddInParameter(dbcommand, "@SoToKhai", DbType.Int64, SoTK);
            db.AddInParameter(dbcommand, "@IDHopDong", DbType.Int64, IDHopDong);
            return db.ExecuteDataSet(dbcommand);
        }

        public DataSet getToKhaiXK(long IDHopDong)
        {
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string strSQL = " select * from t_GC_BKToKhai Where  IDHopDong= @IDHopDong  AND MaLoaiHinh like 'X%'  ";
            DbCommand dbcommand = db.GetSqlStringCommand(strSQL);
            //db.AddInParameter(dbcommand, "@SoToKhai", DbType.Int64, SoTK);
            db.AddInParameter(dbcommand, "@IDHopDong", DbType.Int64, IDHopDong);
            return db.ExecuteDataSet(dbcommand);
        }

        public int DeleteTK(long IDHopDong)
        {

            string strSQL = " Delete from t_GC_BKToKhai where IDHopDong=@IDHopDong ";
            DbCommand dbCommand = this.db.GetSqlStringCommand(strSQL);
            db.AddInParameter(dbCommand, "@IDHopDong", DbType.Int64, IDHopDong);
            return this.db.ExecuteNonQuery(dbCommand);
        }
        public bool UpdateGrid(BKToKhaiCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKToKhai item in collection)
                    {
                        BKToKhai bk = new BKToKhai();
                        bk.CanBoDuyet = item.CanBoDuyet;
                        bk.CKXN = item.CKXN;
                        bk.DVHQ = item.DVHQ;
                        bk.DVT = item.DVT;
                        bk.GhiChu = item.GhiChu;
                        bk.ID = item.ID;
                        bk.IDHopDong = item.IDHopDong;
                        bk.Luong = item.Luong;
                        bk.MaLoaiHinh = item.MaLoaiHinh;
                        bk.Ngay_DK = item.Ngay_DK;
                        bk.NgayTK = item.NgayTK;
                        bk.SoTK = item.SoTK;
                        bk.STT = item.STT;
                        bk.VT = item.VT;
                        if (bk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool UpdateDataSetFromGrid(DataSet ds)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        BKToKhai bk = new BKToKhai();
                        bk.CanBoDuyet = dr["CanBoDuyet"].ToString();
                        bk.CKXN = dr["CKXN"].ToString();
                        bk.DVHQ = dr["DVHQ"].ToString();
                        bk.DVT = dr["DVT"].ToString();
                        bk.GhiChu = dr["GhiChu"].ToString();
                        bk.ID = Convert.ToInt64(dr["ID"]);
                        bk.IDHopDong = Convert.ToInt64(dr["IDHopDong"]);
                        bk.Luong = Convert.ToDecimal(dr["Luong"]);
                        bk.MaLoaiHinh = dr["MaLoaiHinh"].ToString();
                        bk.Ngay_DK = Convert.ToDateTime(dr["Ngay_DK"]);
                        bk.NgayTK = Convert.ToDateTime(dr["NgayTK"]);
                        bk.SoTK = Convert.ToInt64(dr["SoTK"]);
                        bk.STT = Convert.ToInt32(dr["STT"]);
                        bk.VT = dr["VT"].ToString();
                        if (bk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}