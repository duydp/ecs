using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Licenses_v2.Class
{
	public partial class Client
	{
		#region Properties.
		
		public string MaMay { set; get; }
		public string TenMay { set; get; }
		public string CustomersID { set; get; }
		public int ClientDetailID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Client Load(string maMay)
		{
			const string spName = "[dbo].[p_LICENSE_Client_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, maMay);
			Client entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new Client();
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ClientDetailID"))) entity.ClientDetailID = reader.GetInt32(reader.GetOrdinal("ClientDetailID"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<Client> SelectCollectionAll()
		{
			List<Client> collection = new List<Client>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				Client entity = new Client();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ClientDetailID"))) entity.ClientDetailID = reader.GetInt32(reader.GetOrdinal("ClientDetailID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<Client> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<Client> collection = new List<Client>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				Client entity = new Client();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ClientDetailID"))) entity.ClientDetailID = reader.GetInt32(reader.GetOrdinal("ClientDetailID"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<Client> SelectCollectionBy_ClientDetailID(int clientDetailID)
		{
			List<Client> collection = new List<Client>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_ClientDetailID(clientDetailID);
			while (reader.Read())
			{
				Client entity = new Client();
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ClientDetailID"))) entity.ClientDetailID = reader.GetInt32(reader.GetOrdinal("ClientDetailID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		public static IList<Client> SelectCollectionBy_CustomersID(string customersID)
		{
			List<Client> collection = new List<Client>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_CustomersID(customersID);
			while (reader.Read())
			{
				Client entity = new Client();
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ClientDetailID"))) entity.ClientDetailID = reader.GetInt32(reader.GetOrdinal("ClientDetailID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ClientDetailID(int clientDetailID)
		{
			const string spName = "[dbo].[p_LICENSE_Client_SelectBy_ClientDetailID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, clientDetailID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public static DataSet SelectBy_CustomersID(string customersID)
		{
			const string spName = "[dbo].[p_LICENSE_Client_SelectBy_CustomersID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, customersID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Client_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Client_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Client_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Client_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ClientDetailID(int clientDetailID)
		{
			const string spName = "p_LICENSE_Client_SelectBy_ClientDetailID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, clientDetailID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		public static IDataReader SelectReaderBy_CustomersID(string customersID)
		{
			const string spName = "p_LICENSE_Client_SelectBy_CustomersID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, customersID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertClient(string tenMay, string customersID, int clientDetailID)
		{
			Client entity = new Client();	
			entity.TenMay = tenMay;
			entity.CustomersID = customersID;
			entity.ClientDetailID = clientDetailID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LICENSE_Client_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@TenMay", SqlDbType.NVarChar, TenMay);
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, CustomersID);
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, ClientDetailID);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Client> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Client item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateClient(string maMay, string tenMay, string customersID, int clientDetailID)
		{
			Client entity = new Client();			
			entity.MaMay = maMay;
			entity.TenMay = tenMay;
			entity.CustomersID = customersID;
			entity.ClientDetailID = clientDetailID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LICENSE_Client_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@TenMay", SqlDbType.NVarChar, TenMay);
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, CustomersID);
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, ClientDetailID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Client> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Client item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateClient(string maMay, string tenMay, string customersID, int clientDetailID)
		{
			Client entity = new Client();			
			entity.MaMay = maMay;
			entity.TenMay = tenMay;
			entity.CustomersID = customersID;
			entity.ClientDetailID = clientDetailID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Client_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@TenMay", SqlDbType.NVarChar, TenMay);
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, CustomersID);
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, ClientDetailID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Client> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Client item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteClient(string maMay)
		{
			Client entity = new Client();
			entity.MaMay = maMay;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Client_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ClientDetailID(int clientDetailID)
		{
			const string spName = "[dbo].[p_LICENSE_Client_DeleteBy_ClientDetailID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ClientDetailID", SqlDbType.Int, clientDetailID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		public static int DeleteBy_CustomersID(string customersID)
		{
			const string spName = "[dbo].[p_LICENSE_Client_DeleteBy_CustomersID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CustomersID", SqlDbType.VarChar, customersID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LICENSE_Client_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Client> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Client item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}