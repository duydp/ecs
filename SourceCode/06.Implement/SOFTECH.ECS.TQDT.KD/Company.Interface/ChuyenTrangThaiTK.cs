using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.KD.BLL;

namespace Company.Interface
{
    public partial class ChuyenTrangThaiTK : BaseForm
    {
        ToKhaiMauDich tkmd;
        public ChuyenTrangThaiTK(ToKhaiMauDich tk)
        {
            InitializeComponent();
            this.tkmd = tk;
            this.Text +=  " " + this.tkmd.ID.ToString();
            this.lblCaption.Text +=  " " + this.tkmd.ID.ToString();
            this.lblMaToKhai.Text = this.tkmd.ID.ToString();
            this.lblMaLoaiHinh.Text = this.tkmd.MaLoaiHinh;
            this.txtHuongDan.Text = this.tkmd.HUONGDAN;
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            if (this.validData())
            {
                this.getData();
            //    this.tkmd.LoadHMDCollection();

                Company.KD.BLL.SXXK.ToKhai.ToKhaiMauDich tkDaDangKy = new Company.KD.BLL.SXXK.ToKhai.ToKhaiMauDich();
                tkDaDangKy.MaHaiQuan = tkmd.MaHaiQuan;
                tkDaDangKy.NamDangKy = (short)tkmd.NgayDangKy.Year;
                tkDaDangKy.MaLoaiHinh = tkmd.MaLoaiHinh;
                tkDaDangKy.SoToKhai = tkmd.SoToKhai;
                tkDaDangKy.PhanLuong = tkmd.PhanLuong;
                
                if (!tkDaDangKy.Load())
                {
                    this.tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.tkmd.Update();
                    this.tkmd.TransgferDataToSXXK();
                    if (MLMessages("Chuyển trạng thái thành công","MSG_WRN34","", false) == "Cancel")
                    {
                        this.Close();
                    }
                }
                else
                {
                    MLMessages("Thông tin tờ khai này trùng trong danh sách đăng ký! \nVui lòng kiểm tra lại số tờ khai và ngày đăng ký","MSG_CHA02","", false);
                }
            }
            else
            {
                MLMessages("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.","MSG_WRN33","", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtSoToKhai.Text == "" || int.Parse(txtSoToKhai.Text) < 0) value = false;

            return value;
        }

        private void getData()
        {
            this.tkmd.SoToKhai = int.Parse(txtSoToKhai.Text);
            this.tkmd.NgayDangKy =Convert.ToDateTime(ccNgayDayKy.Value.ToShortDateString());
            this.tkmd.PhanLuong = cbPL.SelectedValue.ToString();
            this.tkmd.HUONGDAN = this.txtHuongDan.Text.Trim();
            if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                tkmd.NgayTiepNhan = tkmd.NgayDangKy;
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}