using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using System.Collections.Generic;

namespace Company.Interface.GC
{
    public partial class ThietBiThanhKhoanForm : BaseForm
    {
        public ThietBi ThietBiSelected = new ThietBi();
        public List<ThietBi> TBThanhKhoan = new List<ThietBi>();
        public bool isBrower = true;
        public bool isChonNhieu = false;
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien cac npl da duyet nhhung chua dieu chinh va bo sung . 

        public ThietBiThanhKhoanForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(ThietBiSelected.HopDong_ID);
            dgList.DataSource = hd.GetTB();
            
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];



            lblHint.Visible = !this.btnChonNhieuHang.Visible;
        }

        //-----------------------------------------------------------------------------------------

        private void ThietBiRegistedForm_Load(object sender, EventArgs e)
        {
            if (isChonNhieu) btnChonNhieuHang.Visible = true;
            this.khoitao_DuLieuChuan();
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    this.ThietBiSelected = (ThietBi)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {

            }
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ThietBi thietBi = (ThietBi)i.DataRow;

                        TBThanhKhoan.Add(thietBi);
                    }
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}