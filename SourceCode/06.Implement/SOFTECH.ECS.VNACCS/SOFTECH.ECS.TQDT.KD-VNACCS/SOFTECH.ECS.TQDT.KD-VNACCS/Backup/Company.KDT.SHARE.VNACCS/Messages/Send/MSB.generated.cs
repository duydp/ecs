using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class MSB : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CHN { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute SUB { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute TUS { get; set; }

public MSB()
        {
CHN = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
SUB = new PropertiesAttribute(210, typeof(string));
ICN = new PropertiesAttribute(12, typeof(int));
TUS = new PropertiesAttribute(996, typeof(string));
TongSoByte = 1236;
}

}public partial class EnumGroupID
    {
}

}
