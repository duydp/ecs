using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class MaTienTeControl : UserControl
    {
        public MaTienTeControl()
        {
            this.InitializeComponent();
        }
        public decimal TyGia = 0;

        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }

        public string Ma
        {
            set
            {
                if (this.cbTen != null)
                    this.cbTen.Value = this.cbTen.Text.PadRight(3);
            }
            get { return this.cbTen.Text; }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            // if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.NGUYEN_TE_MAC_DINH;
            dtNguyenTe.Rows.Clear();
            DataTable dt = NguyenTe.SelectAll().Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                this.dtNguyenTe.ImportRow(row);
            }
             this.cbTen.Value = this.cbTen.Text.PadRight(3);
        }

        private void MaTienTeControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbTen_Leave(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.cbTen.Text = this.cbTen.Text.Trim();
                if (this.dtNguyenTe.Select("ID='" + cbTen.Text.Trim() + "'").Length == 0)
                {
                    this.cbTen.Text = GlobalSettings.NGUYEN_TE_MAC_DINH;
                }
                TyGia = NguyenTe.GetTyGia(cbTen.Text.Trim());
                this.cbTen.Value = this.cbTen.Text.PadRight(3);
            }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set { this.cbTen.VisualStyleManager = value; }
            get { return this.cbTen.VisualStyleManager; }
        }
        public bool ReadOnly
        {
            set { this.cbTen.ReadOnly = value; }
            get { return this.cbTen.ReadOnly; }
        }








    }
}