﻿namespace Company.Interface
{
    partial class ChungTuNoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuNoForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiCT = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chIsChungTuNo = new Janus.Windows.EditControls.UICheckBox();
            this.ccThoiHanNop = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.ccNgayChungTu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtToChucCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtNoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayHetHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(568, 320);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbLoaiCT);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.chIsChungTuNo);
            this.uiGroupBox2.Controls.Add(this.ccThoiHanNop);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.ccNgayChungTu);
            this.uiGroupBox2.Controls.Add(this.txtToChucCap);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Controls.Add(this.txtNoiCap);
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtSoChungTu);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayHetHan);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(568, 320);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiCT
            // 
            this.cbLoaiCT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Giấy đăng ký kiểm tra";
            uiComboBoxItem4.Value = "200";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Giấy kêt quả kiểm tra,miền kiểm tra";
            uiComboBoxItem5.Value = "201";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Giấy nộp tiền";
            uiComboBoxItem6.Value = "202";
            this.cbLoaiCT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbLoaiCT.Location = new System.Drawing.Point(122, 71);
            this.cbLoaiCT.Name = "cbLoaiCT";
            this.cbLoaiCT.Size = new System.Drawing.Size(428, 21);
            this.cbLoaiCT.TabIndex = 3;
            this.cbLoaiCT.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Loại chứng từ";
            // 
            // chIsChungTuNo
            // 
            this.chIsChungTuNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsChungTuNo.Location = new System.Drawing.Point(122, 257);
            this.chIsChungTuNo.Name = "chIsChungTuNo";
            this.chIsChungTuNo.Size = new System.Drawing.Size(109, 23);
            this.chIsChungTuNo.TabIndex = 7;
            this.chIsChungTuNo.Text = "Nợ chứng từ";
            // 
            // ccThoiHanNop
            // 
            // 
            // 
            // 
            this.ccThoiHanNop.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccThoiHanNop.DropDownCalendar.Name = "";
            this.ccThoiHanNop.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccThoiHanNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccThoiHanNop.IsNullDate = true;
            this.ccThoiHanNop.Location = new System.Drawing.Point(312, 257);
            this.ccThoiHanNop.Name = "ccThoiHanNop";
            this.ccThoiHanNop.Nullable = true;
            this.ccThoiHanNop.NullButtonText = "Xóa";
            this.ccThoiHanNop.ShowNullButton = true;
            this.ccThoiHanNop.Size = new System.Drawing.Size(141, 21);
            this.ccThoiHanNop.TabIndex = 8;
            this.ccThoiHanNop.TodayButtonText = "Hôm nay";
            this.ccThoiHanNop.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccThoiHanNop.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(238, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Thời hạn nộp";
            // 
            // ccNgayChungTu
            // 
            // 
            // 
            // 
            this.ccNgayChungTu.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayChungTu.DropDownCalendar.Name = "";
            this.ccNgayChungTu.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayChungTu.IsNullDate = true;
            this.ccNgayChungTu.Location = new System.Drawing.Point(122, 42);
            this.ccNgayChungTu.Name = "ccNgayChungTu";
            this.ccNgayChungTu.Nullable = true;
            this.ccNgayChungTu.NullButtonText = "Xóa";
            this.ccNgayChungTu.ShowNullButton = true;
            this.ccNgayChungTu.Size = new System.Drawing.Size(167, 21);
            this.ccNgayChungTu.TabIndex = 1;
            this.ccNgayChungTu.TodayButtonText = "Hôm nay";
            this.ccNgayChungTu.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayChungTu.VisualStyleManager = this.vsmMain;
            // 
            // txtToChucCap
            // 
            this.txtToChucCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToChucCap.Location = new System.Drawing.Point(122, 133);
            this.txtToChucCap.MaxLength = 255;
            this.txtToChucCap.Name = "txtToChucCap";
            this.txtToChucCap.Size = new System.Drawing.Size(427, 21);
            this.txtToChucCap.TabIndex = 5;
            this.txtToChucCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtToChucCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(296, 285);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiCap.Location = new System.Drawing.Point(122, 101);
            this.txtNoiCap.MaxLength = 255;
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Size = new System.Drawing.Size(427, 21);
            this.txtNoiCap.TabIndex = 4;
            this.txtNoiCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNoiCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNoiCap.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(390, 285);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(77, 23);
            this.btnGhi.TabIndex = 10;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Nơi cấp";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(473, 285);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(302, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ngày hết hạn";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(122, 163);
            this.txtGhiChu.MaxLength = 255;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(425, 88);
            this.txtGhiChu.TabIndex = 6;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 24);
            this.label2.TabIndex = 14;
            this.label2.Text = "Thông tin khác";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 19);
            this.label8.TabIndex = 14;
            this.label8.Text = "Tổ chức cấp";
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoChungTu.Location = new System.Drawing.Point(121, 15);
            this.txtSoChungTu.MaxLength = 255;
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(428, 21);
            this.txtSoChungTu.TabIndex = 0;
            this.txtSoChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày chứng từ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số chứng từ";
            // 
            // ccNgayHetHan
            // 
            // 
            // 
            // 
            this.ccNgayHetHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHetHan.DropDownCalendar.Name = "";
            this.ccNgayHetHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHetHan.IsNullDate = true;
            this.ccNgayHetHan.Location = new System.Drawing.Point(384, 42);
            this.ccNgayHetHan.Name = "ccNgayHetHan";
            this.ccNgayHetHan.Nullable = true;
            this.ccNgayHetHan.NullButtonText = "Xóa";
            this.ccNgayHetHan.ShowNullButton = true;
            this.ccNgayHetHan.Size = new System.Drawing.Size(166, 21);
            this.ccNgayHetHan.TabIndex = 2;
            this.ccNgayHetHan.TodayButtonText = "Hôm nay";
            this.ccNgayHetHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHetHan.VisualStyleManager = this.vsmMain;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // ChungTuNoForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 320);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "ChungTuNoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin chứng từ";
            this.Load += new System.EventHandler(this.ChungTuNoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayChungTu;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCap;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCap;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHetHan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccThoiHanNop;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UICheckBox chIsChungTuNo;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIComboBox cbLoaiCT;
        private System.Windows.Forms.Label label4;
    }
}