using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8060 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute TYS { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A10 { get; set; }
public PropertiesAttribute A11 { get; set; }
public PropertiesAttribute A12 { get; set; }
public PropertiesAttribute A13 { get; set; }
public PropertiesAttribute A14 { get; set; }
public PropertiesAttribute CCC { get; set; }

public VAF8060()
        {
TYS = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
A02 = new PropertiesAttribute(210, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
A06 = new PropertiesAttribute(1, typeof(string));
A07 = new PropertiesAttribute(13, typeof(string));
A08 = new PropertiesAttribute(300, typeof(string));
A09 = new PropertiesAttribute(13, typeof(string));
A10 = new PropertiesAttribute(300, typeof(string));
A11 = new PropertiesAttribute(8, typeof(DateTime));
A12 = new PropertiesAttribute(8, typeof(DateTime));
A13 = new PropertiesAttribute(13, typeof(int));
A14 = new PropertiesAttribute(13, typeof(int));
CCC = new PropertiesAttribute(3, typeof(string));
TongSoByte = 950;
}

}public partial class EnumGroupID
    {
}

}
