﻿namespace Company.Interface.Report
{
    partial class ReportViewGCCTA4Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkInBanLuuHaiQuan = new Janus.Windows.EditControls.UICheckBox();
            this.chkInMaHang = new Janus.Windows.EditControls.UICheckBox();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cboToKhai = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.txtTenNhomHang = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(895, 428);
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(1, 73);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(892, 353);
            this.printControl1.TabIndex = 2;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ShowMarginsWarning = false;
            this.printingSystem1.ShowPrintStatusDialog = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkInBanLuuHaiQuan);
            this.uiGroupBox1.Controls.Add(this.chkInMaHang);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cboToKhai);
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(465, 64);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkInBanLuuHaiQuan
            // 
            this.chkInBanLuuHaiQuan.Location = new System.Drawing.Point(142, 41);
            this.chkInBanLuuHaiQuan.Name = "chkInBanLuuHaiQuan";
            this.chkInBanLuuHaiQuan.Size = new System.Drawing.Size(147, 23);
            this.chkInBanLuuHaiQuan.TabIndex = 6;
            this.chkInBanLuuHaiQuan.Text = "In bản lưu Hải quan";
            this.chkInBanLuuHaiQuan.VisualStyleManager = this.vsmMain;
            this.chkInBanLuuHaiQuan.CheckedChanged += new System.EventHandler(this.chkInBanLuuHaiQuan_CheckedChanged);
            // 
            // chkInMaHang
            // 
            this.chkInMaHang.Location = new System.Drawing.Point(57, 41);
            this.chkInMaHang.Name = "chkInMaHang";
            this.chkInMaHang.Size = new System.Drawing.Size(79, 23);
            this.chkInMaHang.TabIndex = 4;
            this.chkInMaHang.Text = "In mã hàng";
            this.chkInMaHang.VisualStyleManager = this.vsmMain;
            this.chkInMaHang.CheckedChanged += new System.EventHandler(this.chkInMaHang_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(364, 18);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chọn tờ khai chính / phụ lục";
            // 
            // cboToKhai
            // 
            this.cboToKhai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Tờ khai chính";
            uiComboBoxItem2.Value = 0;
            this.cboToKhai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem2});
            this.cboToKhai.Location = new System.Drawing.Point(200, 19);
            this.cboToKhai.Name = "cboToKhai";
            this.cboToKhai.Size = new System.Drawing.Size(158, 21);
            this.cboToKhai.TabIndex = 1;
            this.cboToKhai.VisualStyleManager = this.vsmMain;
            this.cboToKhai.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.uiButton3);
            this.uiGroupBox2.Controls.Add(this.txtTenNhomHang);
            this.uiGroupBox2.Location = new System.Drawing.Point(471, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(421, 64);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên hàng 1";
            // 
            // uiButton3
            // 
            this.uiButton3.Location = new System.Drawing.Point(283, 17);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(75, 23);
            this.uiButton3.TabIndex = 2;
            this.uiButton3.Text = "Áp dụng";
            this.uiButton3.VisualStyleManager = this.vsmMain;
            this.uiButton3.Click += new System.EventHandler(this.uiButton3_Click);
            // 
            // txtTenNhomHang
            // 
            this.txtTenNhomHang.Location = new System.Drawing.Point(113, 19);
            this.txtTenNhomHang.Name = "txtTenNhomHang";
            this.txtTenNhomHang.Size = new System.Drawing.Size(164, 21);
            this.txtTenNhomHang.TabIndex = 1;
            this.txtTenNhomHang.VisualStyleManager = this.vsmMain;
            // 
            // ReportViewTKNA4Form
            // 
            this.AcceptButton = this.btnPrint;
            this.ClientSize = new System.Drawing.Size(895, 428);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ReportViewTKNA4Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "In tờ khai nhập giấy A4";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cboToKhai;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton uiButton3;
        public System.Windows.Forms.Label label3;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenNhomHang;
        private Janus.Windows.EditControls.UICheckBox chkInMaHang;
        private Janus.Windows.EditControls.UICheckBox chkInBanLuuHaiQuan;
    }
}