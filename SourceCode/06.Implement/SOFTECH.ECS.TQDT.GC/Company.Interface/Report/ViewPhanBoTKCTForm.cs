﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.GC.BLL.SXXK;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Threading;
using System.Diagnostics;

namespace Company.Interface.Report
{
    public partial class ViewPhanBoTKCTForm : BaseForm
    {
        PhanBoReportBySeven ReportSeven = new PhanBoReportBySeven();
        PhanBoReport Report = new PhanBoReport();
        public ToKhaiChuyenTiep TKCT;
        DataSet dsBangPhanBo = new DataSet();
        DataSet dsForExcel = new DataSet();
        public HopDong HD;
        int soLuongSP;
        int soTable = 0;
        int tableCuoi = 0;
        int TongBang7;
        int TongBang4;

        public ViewPhanBoTKCTForm()
        {
            InitializeComponent();
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            this.ViewPhanBo();
        }

        private void ViewPhanBo()
        {
            soLuongSP = this.TKCT.HCTCollection.Count;
            this.TKCT.LoadHCTCollection();
            soTable = (soLuongSP - 1) / 7 + 1;
            tableCuoi = soLuongSP % 7;

            if (tableCuoi == 0 || tableCuoi > 4)
            {
                soTable += 1;
                TongBang7 = soTable - 2;
                TongBang4 = 2;
            }
            else
            {
                TongBang7 = soTable - 1;
                TongBang4 = 1;
            }

            dsBangPhanBo = TaoBangPhanBo();

            for (int i = 0; i < soTable; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private string GetPhanBoToKhaiXuat(string maNPL, decimal tongNhuCau)
        {
            string temp = "";
            DataSet ds = PhanBoToKhaiXuat.SelectViewPhanBoToKhaiXuatOfTKCTAndMaNPL(this.TKCT.ID, maNPL);
            decimal tongPhanBo = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (dr["SoToKhaiNhap"].ToString() != "0")
                {
                    temp += "TK " + dr["SoToKhaiNhap"] + "/" + dr["MaLoaiHinhNhap"].ToString() + "/" + dr["NamDangKyNhap"].ToString() + ": " + Convert.ToDecimal(dr["LuongPhanBo"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL) + "; ";
                    tongPhanBo += Convert.ToDecimal(dr["LuongPhanBo"]);
                }
            }
            decimal muaVN = tongNhuCau - tongPhanBo;
            if (muaVN > 0)
                temp += "Mua VN: " + muaVN.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            return temp;

        }

        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPage.SelectedIndex < TongBang7)
            {
                this.ReportSeven.ToSo = cbPage.SelectedIndex + 1;
                this.ReportSeven.First = (cbPage.SelectedIndex == 0);
                this.ReportSeven.Last = (cbPage.SelectedIndex == (dsBangPhanBo.Tables.Count - 1));
                this.ReportSeven.dt = dsBangPhanBo.Tables[cbPage.SelectedIndex];
                this.ReportSeven.BindReport(this.TKCT);
                printControl1.PrintingSystem = this.ReportSeven.PrintingSystem;
                this.ReportSeven.CreateDocument();
            }
            else
            {
                this.Report.ToSo = cbPage.SelectedIndex + 1;
                this.Report.First = (cbPage.SelectedIndex == 0);
                this.Report.Last = (cbPage.SelectedIndex == (dsBangPhanBo.Tables.Count - 1));
                this.Report.dt = dsBangPhanBo.Tables[cbPage.SelectedIndex];
                this.Report.BindReport(this.TKCT);
                printControl1.PrintingSystem = this.Report.PrintingSystem;
                this.Report.CreateDocument();
            }
        }

        private string GetTenHang(string maHang)
        {
            foreach (HangChuyenTiep HCT in this.TKCT.HCTCollection)
            {
                if (HCT.MaHang.Trim() == maHang.Trim()) return HCT.TenHang;
            }
            return "";
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            dsForExcel = CreatBangPhanBo();
            this.TKCT.LoadHCTCollection();
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("BangPhanBoNPL");

            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            WorksheetRow wsr1 = ws.Rows[1];
            WorksheetRow wsr2 = ws.Rows[2];
            wsr0.Cells[1].Value = "BẢNG PHÂN BỔ NGUYÊN PHỤ LIỆU HÀNG XUẤT KHẨU TỜ KHAI XUẤT KHẨU SỐ " + this.TKCT.SoToKhai + " NGÀY " + this.TKCT.NgayDangKy.ToShortDateString();
            wsr0.Height = 1000;
            wsr0.Cells[1].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            ws.Columns[0].Width = 1200;
            ws.Columns[1].Width = 8000;
            ws.Columns[2].Width = 2500;
            ws.Columns[3].Width = 1500;
            wsr1.Cells[0].Value = "STT";
            wsr1.Cells[1].Value = "Tên NPL";
            wsr1.Cells[2].Value = "Mã NPL";
            wsr1.Cells[3].Value = "ĐVT";
            ws.MergedCellsRegions.Add(1, 0, 2, 0);
            ws.MergedCellsRegions.Add(1, 1, 2, 1);
            ws.MergedCellsRegions.Add(1, 2, 2, 2);
            ws.MergedCellsRegions.Add(1, 3, 2, 3);
            int temp = 4;
            int temp1 = 4;
            foreach (DataTable dt in this.dsForExcel.Tables)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (dt.Columns["DinhMuc" + i].Caption != " ")
                    {

                        wsr1.Cells[temp].Value = GetTenHang(dt.Columns["DinhMuc" + i].Caption) + " / " + dt.Columns["DinhMuc" + i].Caption + "\nSố lượng: " + dt.Columns["LuongSD" + i].Caption;
                        ws.MergedCellsRegions.Add(1, temp, 1, temp + 1);
                        temp = temp + 2;
                        wsr2.Cells[temp1].Value = "ĐM";
                        temp1++;
                        wsr2.Cells[temp1].Value = "NC";
                        temp1++;
                    }
                    else break;

                }
            }
            wsr1.Cells[temp].Value = "Tổng NPL";
            ws.MergedCellsRegions.Add(1, temp, 2, temp);
            temp++;
            wsr1.Cells[temp].Value = "Hao hụt";
            ws.MergedCellsRegions.Add(1, temp, 2, temp);
            temp++;
            wsr1.Cells[temp].Value = "Tổng NPL gồm TLHH";
            ws.MergedCellsRegions.Add(1, temp, 2, temp);
            temp++;
            wsr1.Cells[temp].Value = "Tờ khai nhập khẩu sử dụng";
            ws.MergedCellsRegions.Add(1, temp, 2, temp);
            wsr0.CellFormat.BottomBorderStyle = CellBorderLineStyle.Medium;
            wsr1.CellFormat.WrapText = ExcelDefaultableBoolean.True;
            wsr1.CellFormat.Alignment = HorizontalCellAlignment.Center;
            wsr1.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            wsr1.CellFormat.BottomBorderStyle = wsr1.CellFormat.LeftBorderStyle = wsr1.CellFormat.RightBorderStyle = wsr2.CellFormat.TopBorderStyle = CellBorderLineStyle.Medium;
            wsr1.Height = 900;
            wsr2.CellFormat.WrapText = ExcelDefaultableBoolean.True;
            wsr2.CellFormat.Alignment = HorizontalCellAlignment.Center;
            wsr2.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            wsr2.CellFormat.BottomBorderStyle = wsr2.CellFormat.LeftBorderStyle = wsr2.CellFormat.RightBorderStyle = wsr2.CellFormat.TopBorderStyle = CellBorderLineStyle.Medium;
            wsr2.Height = 400;
            ws.DisplayOptions.PanesAreFrozen = true;
            ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 4;

            DataTable dtLast = this.dsForExcel.Tables[this.dsForExcel.Tables.Count - 1];
            for (int i = 0; i < dtLast.Rows.Count; i++)
            {
                temp = 4;
                WorksheetRow wsr = ws.Rows[i + 3];
                wsr.Cells[0].Value = dtLast.Rows[i]["STT"];
                wsr.Cells[1].Value = dtLast.Rows[i]["TenNPL"].ToString();
                wsr.Cells[2].Value = dtLast.Rows[i]["MaNPL"].ToString();
                wsr.Cells[3].Value = dtLast.Rows[i]["DVT"].ToString();
                foreach (DataTable dt in this.dsForExcel.Tables)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        if (dt.Columns["DinhMuc" + j].Caption != " ")
                        {
                            wsr.Cells[temp].Value = Convert.ToDecimal(dt.Rows[i]["DinhMuc" + j]).ToString("N" + GlobalSettings.SoThapPhan.DinhMuc);
                            wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                            temp++;
                            wsr.Cells[temp].Value = Convert.ToDecimal(dt.Rows[i]["LuongSD" + j]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                            wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                            temp++;
                        }
                        else break;

                    }
                }
                wsr.Cells[temp].Value = Convert.ToDecimal(dtLast.Rows[i]["TongNPL"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                temp++;
                wsr.Cells[temp].Value = dtLast.Rows[i]["TyLeHaoHut"];
                temp++;
                wsr.Cells[temp].Value = Convert.ToDecimal(dtLast.Rows[i]["TongNPLTLHH"]).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                wsr.Cells[temp].CellFormat.Alignment = HorizontalCellAlignment.Right;
                temp++;
                wsr.Cells[temp].Value = dtLast.Rows[i]["PhanBo"].ToString();
            }

            string fileName = "";
            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
            }

            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
            {
                Process.Start(fileName);
            }
        }


        public DataSet CreatBangPhanBo()
        {

            this.TKCT.LoadHCTCollection();
            int soLuongSP = this.TKCT.HCTCollection.Count;

            int soTable = 0;
            soTable = (soLuongSP - 1) / 4 + 1;
            DataSet dsBangPhanBoForExcel = new DataSet();
            DataTable dttemp = new DataTable();
            Company.GC.BLL.GC.DinhMuc dm1 = new Company.GC.BLL.GC.DinhMuc();
            decimal TLHH = dm1.GetTLHHOfHopDong(this.HD.ID);
            for (int z = 0; z < soTable; z++)
            {
                dttemp = new DataTable("dtBangPhanBo" + z.ToString());
                DataColumn[] dcCol = new DataColumn[17];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 0;
                for (int k = z * 4; k < (z + 1) * 4; k++)
                {

                    if (k < soLuongSP)
                    {
                        HangChuyenTiep hct = this.TKCT.HCTCollection[k];
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hct.MaHang;
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = hct.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "DinhMuc" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;
                        dcCol[4 + t] = new DataColumn();
                        dcCol[4 + t].ColumnName = "LuongSD" + k % 4;
                        dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                        dcCol[4 + t].Caption = " ";
                        t++;

                    }
                }
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongNPL";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongNPL";
                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TyLeHaoHut";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TyLeHaoHut";
                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "TongNPLTLHH";
                dcCol[4 + t].DataType = Type.GetType("System.Decimal");
                dcCol[4 + t].Caption = "TongNPLTLHH";
                t++;
                dcCol[4 + t] = new DataColumn();
                dcCol[4 + t].ColumnName = "PhanBo";
                dcCol[4 + t].DataType = Type.GetType("System.String");
                dcCol[4 + t].Caption = "PhanBo";
                dttemp.Columns.AddRange(dcCol);

                int stt = 0;
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoTKCT(this.TKCT.ID, this.HD.ID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttemp.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 4; n++)
                        {
                            if (dttemp.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                //dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                //dm.MaHaiQuan = GlobalSet1tings.MA_HAI_QUAN;
                                dm.MaSanPham = dttemp.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                drData["TyLeHaoHut"] = TLHH;
                                drData["DinhMuc" + n] = dm.DinhMucSuDung;
                                drData["LuongSD" + n] = Convert.ToDecimal(dttemp.Columns["LuongSD" + n].Caption) * dm.DinhMucSuDung;
                            }
                            else
                            {
                                drData["TyLeHaoHut"] = TLHH;
                            }
                        }
                        dttemp.Rows.Add(drData);
                    }
                    catch
                    {

                    }


                }
                dsBangPhanBoForExcel.Tables.Add(dttemp);
            }

            decimal tongNPLTLHH = 0;
            if (dsBangPhanBoForExcel.Tables.Count > 0)
            {
                for (int t = 0; t < dsBangPhanBoForExcel.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;
                    for (int i = 0; i < dsBangPhanBoForExcel.Tables.Count; i++)
                    {
                        try
                        {
                            DataRow dr = dsBangPhanBoForExcel.Tables[i].Rows[t];
                            for (int n = 0; n < 4; n++)
                            {
                                if (dr["LuongSD" + n] != DBNull.Value)
                                    tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                            }
                        }
                        catch { }
                    }
                    try
                    {
                        dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TongNPL"] = tongTungNPL;
                        tongNPLTLHH = tongTungNPL * (100 + Convert.ToDecimal(dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TyLeHaoHut"])) / 100;
                        dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["TongNPLTLHH"] = tongNPLTLHH;
                        dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["PhanBo"] = GetPhanBoToKhaiXuat(dsBangPhanBoForExcel.Tables[dsBangPhanBoForExcel.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongNPLTLHH);
                    }
                    catch { }
                }
            }
            return dsBangPhanBoForExcel;
        }


        public DataSet TaoBangPhanBo()
        {
            DataSet dsTemp = new DataSet();
            DataTable dttempBySeven = new DataTable();
            DataTable dttempByFour = new DataTable();

            #region TaoBang7

            for (int BangSo = 0; BangSo < TongBang7; BangSo++)
            {
                dttempBySeven = new DataTable("dtBangPhanBoSeven" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[18];

                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = BangSo * 7; k < (BangSo + 1) * 7; k++)
                {
                    HangChuyenTiep hct = this.TKCT.HCTCollection[k];

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "DinhMuc" + k % 7;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hct.MaHang;
                    t++;
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "LuongSD" + k % 7;
                    dcCol[t].DataType = Type.GetType("System.Decimal");
                    dcCol[t].Caption = hct.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                    t++;
                }

                dttempBySeven.Columns.AddRange(dcCol);

                int stt = 0;
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoTKCT(this.TKCT.ID, this.HD.ID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttempBySeven.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 7; n++)
                        {
                            if (dttempBySeven.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempBySeven.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                drData["DinhMuc" + n] = dm.DinhMucSuDung;
                                drData["LuongSD" + n] = Convert.ToDecimal(dttempBySeven.Columns["LuongSD" + n].Caption) * dm.DinhMucSuDung;
                            }

                        }
                        dttempBySeven.Rows.Add(drData);
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempBySeven);
            }

            #endregion

            #region TaoBang4

            Company.GC.BLL.GC.DinhMuc DMtemp = new Company.GC.BLL.GC.DinhMuc();
            decimal TLHH = DMtemp.GetTLHHOfHopDong(this.HD.ID);

            for (int BangSo = 0; BangSo < TongBang4; BangSo++)
            {
                dttempByFour = new DataTable("dtBangPhanBoFour" + BangSo.ToString());
                DataColumn[] dcCol = new DataColumn[16];
                dcCol[0] = new DataColumn("STT", Type.GetType("System.String"));
                dcCol[0].Caption = "STT";
                dcCol[1] = new DataColumn("MaNPL", Type.GetType("System.String"));
                dcCol[1].Caption = "MaNPL";
                dcCol[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
                dcCol[2].Caption = "TenNPL";
                dcCol[3] = new DataColumn("DVT", Type.GetType("System.String"));
                dcCol[3].Caption = "DVT";

                int t = 4;
                for (int k = (BangSo * 4) + (7 * TongBang7); k < (BangSo + 1) * 4 + (7 * TongBang7); k++)
                {

                    int phanchia = k - (7 * TongBang7);
                    if (k < soLuongSP)
                    {
                        HangChuyenTiep hct = this.TKCT.HCTCollection[k];

                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 4;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hct.MaHang;
                        t++;
                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 4;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = hct.SoLuong.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
                        t++;
                    }
                    else
                    {
                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "DinhMuc" + phanchia % 4;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;
                        dcCol[t] = new DataColumn();
                        dcCol[t].ColumnName = "LuongSD" + phanchia % 4;
                        dcCol[t].DataType = Type.GetType("System.Decimal");
                        dcCol[t].Caption = " ";
                        t++;

                    }
                }

                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TongNPL";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TongNPL";
                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TyLeHaoHut";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TyLeHaoHut";
                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "TongNPLTLHH";
                dcCol[t].DataType = Type.GetType("System.Decimal");
                dcCol[t].Caption = "TongNPLTLHH";
                t++;
                dcCol[t] = new DataColumn();
                dcCol[t].ColumnName = "PhanBo";
                dcCol[t].DataType = Type.GetType("System.String");
                dcCol[t].Caption = "PhanBo";

                dttempByFour.Columns.AddRange(dcCol);

                int stt = 0;
                DataSet ds = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoTKCT(this.TKCT.ID, this.HD.ID);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drData = dttempByFour.NewRow();
                        drData[0] = ++stt;
                        drData[1] = dr["MaNPL"].ToString();
                        drData[2] = dr["TenNPL"].ToString();
                        drData[3] = DonViTinh.GetName(dr["DVT_ID"].ToString());
                        for (int n = 0; n < 4; n++)
                        {
                            if (dttempByFour.Columns["DinhMuc" + n].Caption != " ")
                            {
                                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                                dm.MaSanPham = dttempByFour.Columns["DinhMuc" + n].Caption;
                                dm.MaNguyenPhuLieu = dr["MaNPL"].ToString();
                                dm.HopDong_ID = this.HD.ID;
                                dm.Load();
                                drData["TyLeHaoHut"] = TLHH;
                                drData["DinhMuc" + n] = dm.DinhMucSuDung;
                                drData["LuongSD" + n] = Convert.ToDecimal(dttempByFour.Columns["LuongSD" + n].Caption) * dm.DinhMucSuDung;
                            }
                        }

                        dttempByFour.Rows.Add(drData);
                    }
                    catch { }
                }

                dsTemp.Tables.Add(dttempByFour);
            }

            decimal tongNPLTLHH = 0;

            if (soTable > 0)
            {
                for (int t = 0; t < dsTemp.Tables[0].Rows.Count; t++)
                {
                    decimal tongTungNPL = 0;

                    if (tableCuoi == 0 || tableCuoi > 4)
                    {
                        for (int i = 0; i < soTable - 2; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 7; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                        for (int i = soTable - 2; i < soTable; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 4; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                    }
                    else
                    {
                        for (int i = 0; i < soTable - 1; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 7; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                        for (int i = soTable - 1; i < soTable; i++)
                        {
                            try
                            {
                                DataRow dr = dsTemp.Tables[i].Rows[t];
                                for (int n = 0; n < 4; n++)
                                {
                                    if (dr["LuongSD" + n] != DBNull.Value)
                                        tongTungNPL += Convert.ToDecimal(dr["LuongSD" + n]);

                                }
                            }
                            catch { }
                        }

                    }

                    try
                    {
                        dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TongNPL"] = tongTungNPL;
                        tongNPLTLHH = tongTungNPL * (100 + Convert.ToDecimal(dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TyLeHaoHut"])) / 100;
                        dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["TongNPLTLHH"] = tongNPLTLHH;
                        dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["PhanBo"] = GetPhanBoToKhaiXuat(dsTemp.Tables[dsTemp.Tables.Count - 1].Rows[t]["MaNPL"].ToString(), tongNPLTLHH);
                    }
                    catch { }
                }
            }

            #endregion

            return dsTemp;
        }
    }
}