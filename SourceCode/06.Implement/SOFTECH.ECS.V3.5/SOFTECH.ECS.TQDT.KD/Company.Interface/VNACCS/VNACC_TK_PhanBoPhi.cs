﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;


namespace Company.Interface
{
    public partial class VNACC_TK_PhanBoPhi : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        private DataTable dtHang;
        private DataSet _dsMaTen = new DataSet();
        private DataSet _dsMaPhanLoai = new DataSet();
        public VNACC_TK_PhanBoPhi(DataSet dsMaTen, DataSet dsMaPhanLoai)
        {
            InitializeComponent();
           // base.SetHandler(this);
           // base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
            _dsMaTen = dsMaTen;
            _dsMaPhanLoai = dsMaPhanLoai;
            this.Text = "Phân bổ các khoản trị giá";
            //grList.FormattingRow += new RowLoadEventHandler(grList_LoadingRow);
        }

        private void VNACC_TK_TKTriGiaForm_Load(object sender, EventArgs e)
        {
           
            LoadData();
        }
        private void LoadData()
        {

            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {
                this.ShowMessage("Bạn chưa nhập thông tin hàng của tờ khai", false);
                this.Close();
            }
            else
            {
                loadHang();
                LoadSoKhoanMuc();
            }
        }
        private void SetKhaiTriGia()
        {
            
        
        }
        private void loadHang()
        {
            try
            {
                dtHang = new DataTable();
                dtHang.Columns.Add("SoTT", typeof(int));
                dtHang.Columns.Add("TenHang", typeof(string));
                dtHang.Columns.Add("MaHS", typeof(string));
                dtHang.Columns.Add("SoLuong", typeof(string));
                dtHang.Columns.Add("DVT", typeof(string));
                dtHang.Columns.Add("TriGiaKB", typeof(decimal));
                dtHang.Columns.Add("SoMucKhai", typeof(string));

                for (int i = 0; i<TKMD.HangCollection.Count ; i++)
                {
                    KDT_VNACC_HangMauDich hang = TKMD.HangCollection[i];
                    DataRow dr = dtHang.NewRow();
                    dr["SoTT"] = i + 1;
                    dr["TenHang"] = hang.TenHang;
                    dr["MaHS"] = hang.MaSoHang;
                    dr["SoLuong"] = hang.SoLuong1;
                    dr["DVT"] = hang.DVTLuong1;
                    dr["TriGiaKB"] = hang.TriGiaHoaDon;
                    dr["SoMucKhai"] = hang.SoMucKhaiKhoanDC;
                    dtHang.Rows.Add(dr);
                }
                grList.DataSource = dtHang;
                grList.Refetch();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi load dữ liệu: " + ex.Message, false);
            }
        }
        private void CheckMucKhoanDC(int soKhoangMuc)
        {
            /*grList.Refetch();*/
            loadHang();
            foreach (GridEXRow item in grList.GetRows())
            {
                DataRowView dr = (DataRowView)item.DataRow;
                if(dr["SoMucKhai"].ToString().Contains(soKhoangMuc.ToString()))
                {
                    item.CheckState = RowCheckState.Checked;
                    item.IsChecked = true;
                }

            }

           // grList.Refetch();
        }

        private void LoadSoKhoanMuc()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SoTT", typeof(int));
            dt.Columns.Add("TenDieuChinh", typeof(string));
            dt.Columns.Add("PhanLoaiDieuChinh", typeof(string));

            foreach ( KDT_VNACC_TK_KhoanDieuChinh item  in TKMD.KhoanDCCollection)
            {
                DataRow dr = dt.NewRow();
                dr["SoTT"] = item.SoTT;
                dr["TenDieuChinh"] = getTenMaPhanLoai(item.MaTenDieuChinh,_dsMaTen);
                dr["PhanLoaiDieuChinh"] = getTenMaPhanLoai(item.MaPhanLoaiDieuChinh, _dsMaPhanLoai);
                dt.Rows.Add(dr);
            }
            multiColumnCombo1.DataSource = dt;
            multiColumnCombo1.DropDownList.ColumnAutoResize = true;
            multiColumnCombo1.DropDownList.DisplayMember = "TenDieuChinh";
            
            multiColumnCombo1.DisplayMember = "TenDieuChinh";
            multiColumnCombo1.ValueMember = "SoTT";
            multiColumnCombo1.DropDownList.Columns["SoTT"].Width = 19;
            multiColumnCombo1.DropDownList.Columns["TenDieuChinh"].Width = 180;
            multiColumnCombo1.Refresh();
            multiColumnCombo1.ReadOnly = false;
        }


        private void GetKhaiTriGia()
        {
           
        }

        private string getTenMaPhanLoai(string ma, DataSet dsDanhMuc)
        {
            DataRow[] ListTen = dsDanhMuc.Tables[0].Select("Code = '" + ma + "'");
            if (ListTen.Length > 0)
                return ListTen[0]["Name_VN"].ToString();
            else
                return ma;
        }
        

        private void Save(int SoKhoanMucDieuChinh)
        {
            List<int> listIndexSave = new List<int>();
           foreach (GridEXRow row in grList.GetCheckedRows())
           {
               DataRowView dr = (DataRowView)row.DataRow;
               KDT_VNACC_HangMauDich hmd = TKMD.HangCollection[System.Convert.ToInt16(dr["SoTT"]) - 1];
               listIndexSave.Add(System.Convert.ToInt16(dr["SoTT"]) - 1);
               if(!hmd.SoMucKhaiKhoanDC.Contains(SoKhoanMucDieuChinh.ToString()))
               {
                   hmd.SoMucKhaiKhoanDC += SoKhoanMucDieuChinh.ToString();
               }
               hmd.SoMucKhaiKhoanDC = SapXep(hmd.SoMucKhaiKhoanDC);
           }
           for (int i = 0; i < TKMD.HangCollection.Count ;i++ )
           {
               if (!listIndexSave.Contains(i))
               {
                   TKMD.HangCollection[i].SoMucKhaiKhoanDC = TKMD.HangCollection[i].SoMucKhaiKhoanDC.Replace(SoKhoanMucDieuChinh.ToString(), "0");
                   TKMD.HangCollection[i].SoMucKhaiKhoanDC = SapXep(TKMD.HangCollection[i].SoMucKhaiKhoanDC);
               }
           }
        }
        private string SapXep(string SoMucKhaiKhoanDC)
        {
            
            string result = string.Empty;

            for (int i = 0; i < SoMucKhaiKhoanDC.Length; i++ )
            {
                int minSoMuc = System.Convert.ToUInt16(SoMucKhaiKhoanDC[i].ToString());
                for (int j = i + 1; j < SoMucKhaiKhoanDC.Length; j++)
                {
                    int SoMucTiep = System.Convert.ToUInt16(SoMucKhaiKhoanDC[j].ToString());
                    if (minSoMuc > SoMucTiep && SoMucTiep != 0)
                    {
                        minSoMuc = SoMucTiep;
                    }
                }
                result = result + minSoMuc.ToString();
                SoMucKhaiKhoanDC = SoMucKhaiKhoanDC.Replace(minSoMuc.ToString(), "0");
            }
            result = result.Replace("0","");
            return result;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                DataRowView dr = (DataRowView)multiColumnCombo1.SelectedItem;
                Save(System.Convert.ToInt16(dr["SoTT"]));
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //this.Cursor = Cursors.Default;
            }

        }

        private void multiColumnCombo1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView dr = (DataRowView)multiColumnCombo1.SelectedItem;
                if (dr != null)
                    CheckMucKhoanDC((int)multiColumnCombo1.Value);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.ShowMessage(ex.Message, false);
            }
           
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (multiColumnCombo1.Value != null)
            {
                DataRowView dr = (DataRowView)e.Row.DataRow;
                if (dr["SoMucKhai"].ToString().Contains(multiColumnCombo1.Value.ToString()))
                {
                    e.Row.CheckState = RowCheckState.Checked;
                }
            }
        }



      

    }
}
