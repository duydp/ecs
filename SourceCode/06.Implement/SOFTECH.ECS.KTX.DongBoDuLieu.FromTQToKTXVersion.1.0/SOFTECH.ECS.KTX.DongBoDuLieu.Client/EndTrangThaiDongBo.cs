﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DongBo
{
    public partial class EndTrangThaiDongBo : Form
    {
        private int step = 1;
        private int value = 1;

        public EndTrangThaiDongBo()
        {
            InitializeComponent();
        }

        private void TrangThaiDongBo_Load(object sender, EventArgs e)
        {
            this.step = 1;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
           
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.step = 5;
            this.value = 100;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            if (pBar.Value == 100) this.Close();
            if (this.value < 100)
            {
                this.value += this.step;
                if (value > 99)
                    value = 100;

            }
            else
            {
                if (pBar.Value < 9) this.value = pBar.Value + 1;
            }
            pBar.Value = this.value;
        }

    }
}