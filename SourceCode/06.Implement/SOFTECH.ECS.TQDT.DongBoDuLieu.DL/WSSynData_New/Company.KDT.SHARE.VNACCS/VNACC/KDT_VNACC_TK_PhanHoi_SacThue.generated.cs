using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TK_PhanHoi_SacThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string MaSacThue { set; get; }
		public string TenSacThue { set; get; }
		public decimal TongTienThue { set; get; }
		public decimal SoDongTongTienThue { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TK_PhanHoi_SacThue> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TK_PhanHoi_SacThue> collection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
			while (reader.Read())
			{
				KDT_VNACC_TK_PhanHoi_SacThue entity = new KDT_VNACC_TK_PhanHoi_SacThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSacThue"))) entity.MaSacThue = reader.GetString(reader.GetOrdinal("MaSacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacThue"))) entity.TenSacThue = reader.GetString(reader.GetOrdinal("TenSacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTienThue"))) entity.TongTienThue = reader.GetDecimal(reader.GetOrdinal("TongTienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDongTongTienThue"))) entity.SoDongTongTienThue = reader.GetDecimal(reader.GetOrdinal("SoDongTongTienThue"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TK_PhanHoi_SacThue> collection, long id)
        {
            foreach (KDT_VNACC_TK_PhanHoi_SacThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_PhanHoi_SacThue VALUES(@Master_ID, @MaSacThue, @TenSacThue, @TongTienThue, @SoDongTongTienThue)";
            string update = "UPDATE t_KDT_VNACC_TK_PhanHoi_SacThue SET Master_ID = @Master_ID, MaSacThue = @MaSacThue, TenSacThue = @TenSacThue, TongTienThue = @TongTienThue, SoDongTongTienThue = @SoDongTongTienThue WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_PhanHoi_SacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThue", SqlDbType.VarChar, "MaSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThue", SqlDbType.Decimal, "TongTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongTongTienThue", SqlDbType.Decimal, "SoDongTongTienThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThue", SqlDbType.VarChar, "MaSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThue", SqlDbType.Decimal, "TongTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongTongTienThue", SqlDbType.Decimal, "SoDongTongTienThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_PhanHoi_SacThue VALUES(@Master_ID, @MaSacThue, @TenSacThue, @TongTienThue, @SoDongTongTienThue)";
            string update = "UPDATE t_KDT_VNACC_TK_PhanHoi_SacThue SET Master_ID = @Master_ID, MaSacThue = @MaSacThue, TenSacThue = @TenSacThue, TongTienThue = @TongTienThue, SoDongTongTienThue = @SoDongTongTienThue WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_PhanHoi_SacThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThue", SqlDbType.VarChar, "MaSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThue", SqlDbType.Decimal, "TongTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongTongTienThue", SqlDbType.Decimal, "SoDongTongTienThue", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThue", SqlDbType.VarChar, "MaSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThue", SqlDbType.NVarChar, "TenSacThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThue", SqlDbType.Decimal, "TongTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongTongTienThue", SqlDbType.Decimal, "SoDongTongTienThue", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TK_PhanHoi_SacThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TK_PhanHoi_SacThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TK_PhanHoi_SacThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TK_PhanHoi_SacThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TK_PhanHoi_SacThue(long master_ID, string maSacThue, string tenSacThue, decimal tongTienThue, decimal soDongTongTienThue)
		{
			KDT_VNACC_TK_PhanHoi_SacThue entity = new KDT_VNACC_TK_PhanHoi_SacThue();	
			entity.Master_ID = master_ID;
			entity.MaSacThue = maSacThue;
			entity.TenSacThue = tenSacThue;
			entity.TongTienThue = tongTienThue;
			entity.SoDongTongTienThue = soDongTongTienThue;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSacThue", SqlDbType.VarChar, MaSacThue);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TongTienThue", SqlDbType.Decimal, TongTienThue);
			db.AddInParameter(dbCommand, "@SoDongTongTienThue", SqlDbType.Decimal, SoDongTongTienThue);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TK_PhanHoi_SacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_PhanHoi_SacThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TK_PhanHoi_SacThue(long id, long master_ID, string maSacThue, string tenSacThue, decimal tongTienThue, decimal soDongTongTienThue)
		{
			KDT_VNACC_TK_PhanHoi_SacThue entity = new KDT_VNACC_TK_PhanHoi_SacThue();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaSacThue = maSacThue;
			entity.TenSacThue = tenSacThue;
			entity.TongTienThue = tongTienThue;
			entity.SoDongTongTienThue = soDongTongTienThue;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSacThue", SqlDbType.VarChar, MaSacThue);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TongTienThue", SqlDbType.Decimal, TongTienThue);
			db.AddInParameter(dbCommand, "@SoDongTongTienThue", SqlDbType.Decimal, SoDongTongTienThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TK_PhanHoi_SacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_PhanHoi_SacThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TK_PhanHoi_SacThue(long id, long master_ID, string maSacThue, string tenSacThue, decimal tongTienThue, decimal soDongTongTienThue)
		{
			KDT_VNACC_TK_PhanHoi_SacThue entity = new KDT_VNACC_TK_PhanHoi_SacThue();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaSacThue = maSacThue;
			entity.TenSacThue = tenSacThue;
			entity.TongTienThue = tongTienThue;
			entity.SoDongTongTienThue = soDongTongTienThue;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSacThue", SqlDbType.VarChar, MaSacThue);
			db.AddInParameter(dbCommand, "@TenSacThue", SqlDbType.NVarChar, TenSacThue);
			db.AddInParameter(dbCommand, "@TongTienThue", SqlDbType.Decimal, TongTienThue);
			db.AddInParameter(dbCommand, "@SoDongTongTienThue", SqlDbType.Decimal, SoDongTongTienThue);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TK_PhanHoi_SacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_PhanHoi_SacThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TK_PhanHoi_SacThue(long id)
		{
			KDT_VNACC_TK_PhanHoi_SacThue entity = new KDT_VNACC_TK_PhanHoi_SacThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TK_PhanHoi_SacThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_PhanHoi_SacThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}