﻿namespace Company.Interface.Report
{
    partial class ReportViewBC03_DMDKForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportViewBC03_DMDKForm));
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMoTaSP = new System.Windows.Forms.TextBox();
            this.txtTo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFrom = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnPrintAll = new Janus.Windows.EditControls.UIButton();
            this.btnApDungMoTaSP = new Janus.Windows.EditControls.UIButton();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.btnPrevious = new Janus.Windows.EditControls.UIButton();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPage = new Janus.Windows.EditControls.UIComboBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSP)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(883, 363);
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BackColor = System.Drawing.Color.Empty;
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.ForeColor = System.Drawing.Color.Empty;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(1, 113);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(880, 248);
            this.printControl1.TabIndex = 0;
            this.printControl1.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            // 
            // printingSystem1
            // 
            this.printingSystem1.ShowMarginsWarning = false;
            this.printingSystem1.ShowPrintStatusDialog = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtMoTaSP);
            this.uiGroupBox1.Controls.Add(this.txtTo);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtFrom);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.btnPrintAll);
            this.uiGroupBox1.Controls.Add(this.btnApDungMoTaSP);
            this.uiGroupBox1.Controls.Add(this.btnNext);
            this.uiGroupBox1.Controls.Add(this.btnPrevious);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cbPage);
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(880, 104);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtMoTaSP
            // 
            this.txtMoTaSP.Location = new System.Drawing.Point(95, 42);
            this.txtMoTaSP.Multiline = true;
            this.txtMoTaSP.Name = "txtMoTaSP";
            this.txtMoTaSP.Size = new System.Drawing.Size(690, 56);
            this.txtMoTaSP.TabIndex = 276;
            // 
            // txtTo
            // 
            this.txtTo.DecimalDigits = 0;
            this.txtTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTo.FormatString = "#####";
            this.txtTo.Location = new System.Drawing.Point(497, 15);
            this.txtTo.MaxLength = 5;
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(23, 21);
            this.txtTo.TabIndex = 275;
            this.txtTo.Text = "1";
            this.txtTo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTo.Value = 1;
            this.txtTo.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTo.VisualStyleManager = this.vsmMain;
            this.txtTo.TextChanged += new System.EventHandler(this.txtTo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(479, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 17);
            this.label3.TabIndex = 274;
            this.label3.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(390, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 273;
            this.label2.Text = "Sản phẩm";
            // 
            // txtFrom
            // 
            this.txtFrom.DecimalDigits = 0;
            this.txtFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrom.FormatString = "#####";
            this.txtFrom.Location = new System.Drawing.Point(449, 15);
            this.txtFrom.MaxLength = 5;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(23, 21);
            this.txtFrom.TabIndex = 272;
            this.txtFrom.Text = "1";
            this.txtFrom.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtFrom.Value = 1;
            this.txtFrom.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtFrom.VisualStyleManager = this.vsmMain;
            this.txtFrom.TextChanged += new System.EventHandler(this.txtFrom_TextChanged);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Location = new System.Drawing.Point(629, 14);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(75, 23);
            this.btnExportExcel.TabIndex = 9;
            this.btnExportExcel.Text = "Xuất &Excel";
            this.btnExportExcel.VisualStyleManager = this.vsmMain;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnPrintAll
            // 
            this.btnPrintAll.Location = new System.Drawing.Point(548, 14);
            this.btnPrintAll.Name = "btnPrintAll";
            this.btnPrintAll.Size = new System.Drawing.Size(75, 23);
            this.btnPrintAll.TabIndex = 8;
            this.btnPrintAll.Text = "In &tất cả";
            this.btnPrintAll.VisualStyleManager = this.vsmMain;
            this.btnPrintAll.Click += new System.EventHandler(this.btnPrintAll_Click);
            // 
            // btnApDungMoTaSP
            // 
            this.btnApDungMoTaSP.Location = new System.Drawing.Point(791, 75);
            this.btnApDungMoTaSP.Name = "btnApDungMoTaSP";
            this.btnApDungMoTaSP.Size = new System.Drawing.Size(72, 23);
            this.btnApDungMoTaSP.TabIndex = 3;
            this.btnApDungMoTaSP.Text = "&Áp dụng";
            this.btnApDungMoTaSP.VisualStyleManager = this.vsmMain;
            this.btnApDungMoTaSP.Click += new System.EventHandler(this.btnApDungMoTaSP_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(791, 14);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(72, 23);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "&Sau";
            this.btnNext.VisualStyleManager = this.vsmMain;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(710, 14);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 3;
            this.btnPrevious.Text = "&Trước";
            this.btnPrevious.VisualStyleManager = this.vsmMain;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(309, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "&In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(9, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 53);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mô tả đặc tính kỹ thuật sản phẩm ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Chọn mặt hàng";
            // 
            // cbPage
            // 
            this.cbPage.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPage.Location = new System.Drawing.Point(95, 15);
            this.cbPage.Name = "cbPage";
            this.cbPage.Size = new System.Drawing.Size(197, 21);
            this.cbPage.TabIndex = 0;
            this.cbPage.VisualStyleManager = this.vsmMain;
            this.cbPage.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvSP
            // 
            this.rfvSP.ControlToValidate = this.cbPage;
            this.rfvSP.ErrorMessage = "\"Chưa chọn sản phẩm\"";
            this.rfvSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSP.Icon")));
            // 
            // ReportViewBC03_DMDKForm
            // 
            this.AcceptButton = this.btnPrint;
            this.ClientSize = new System.Drawing.Size(883, 363);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ReportViewBC03_DMDKForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Xem báo cáo số 03 đăng ký định mức gia công";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbPage;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSP;
        private Janus.Windows.EditControls.UIButton btnPrintAll;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIButton btnPrevious;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtFrom;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox txtMoTaSP;
        private Janus.Windows.EditControls.UIButton btnApDungMoTaSP;
        private System.Windows.Forms.Label label4;
    }
}