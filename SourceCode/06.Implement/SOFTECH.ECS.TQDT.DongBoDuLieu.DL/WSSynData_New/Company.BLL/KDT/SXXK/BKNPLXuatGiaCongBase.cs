using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
	public partial class BKNPLXuatGiaCong 
	{
		#region Private members.
		
		protected long _ID;
		protected long _BangKeHoSoThanhLy_ID;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
		protected string _MaNPL = String.Empty;
		protected string _TenNPL = String.Empty;
		protected int _SoToKhaiXuat;
		protected string _MaLoaiHinhXuat = String.Empty;
		protected short _NamDangKyXuat;
		protected string _MaHaiQuanXuat = String.Empty;
		protected DateTime _NgayDangKyXuat = new DateTime(1900, 01, 01);
		protected decimal _LuongXuat;
		protected string _DVT_ID = String.Empty;
		protected string _TenDVT = String.Empty;
		protected int _STTHang;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public long BangKeHoSoThanhLy_ID
		{
			set {this._BangKeHoSoThanhLy_ID = value;}
			get {return this._BangKeHoSoThanhLy_ID;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public DateTime NgayDangKy
		{
			set {this._NgayDangKy = value;}
			get {return this._NgayDangKy;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public string TenNPL
		{
			set {this._TenNPL = value;}
			get {return this._TenNPL;}
		}
		public int SoToKhaiXuat
		{
			set {this._SoToKhaiXuat = value;}
			get {return this._SoToKhaiXuat;}
		}
		public string MaLoaiHinhXuat
		{
			set {this._MaLoaiHinhXuat = value;}
			get {return this._MaLoaiHinhXuat;}
		}
		public short NamDangKyXuat
		{
			set {this._NamDangKyXuat = value;}
			get {return this._NamDangKyXuat;}
		}
		public string MaHaiQuanXuat
		{
			set {this._MaHaiQuanXuat = value;}
			get {return this._MaHaiQuanXuat;}
		}
		public DateTime NgayDangKyXuat
		{
			set {this._NgayDangKyXuat = value;}
			get {return this._NgayDangKyXuat;}
		}
		public decimal LuongXuat
		{
			set {this._LuongXuat = value;}
			get {return this._LuongXuat;}
		}
		public string DVT_ID
		{
			set {this._DVT_ID = value;}
			get {return this._DVT_ID;}
		}
		public string TenDVT
		{
			set {this._TenDVT = value;}
			get {return this._TenDVT;}
		}
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_Load";
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) this._BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) this._NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) this._MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) this._NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuat"))) this._LuongXuat = reader.GetDecimal(reader.GetOrdinal("LuongXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) this._TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static BKNPLXuatGiaCongCollection SelectCollectionBy_BangKeHoSoThanhLy_ID(long bangKeHoSoThanhLy_ID)
		{
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, bangKeHoSoThanhLy_ID);
			
			BKNPLXuatGiaCongCollection collection = new BKNPLXuatGiaCongCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				BKNPLXuatGiaCong entity = new BKNPLXuatGiaCong();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuat"))) entity.LuongXuat = reader.GetDecimal(reader.GetOrdinal("LuongXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
            reader.Close();
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_BangKeHoSoThanhLy_ID()
		{
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLXuatGiaCongCollection SelectCollectionAll()
		{
			BKNPLXuatGiaCongCollection collection = new BKNPLXuatGiaCongCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				BKNPLXuatGiaCong entity = new BKNPLXuatGiaCong();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuat"))) entity.LuongXuat = reader.GetDecimal(reader.GetOrdinal("LuongXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLXuatGiaCongCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			BKNPLXuatGiaCongCollection collection = new BKNPLXuatGiaCongCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				BKNPLXuatGiaCong entity = new BKNPLXuatGiaCong();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuat"))) entity.LuongXuat = reader.GetDecimal(reader.GetOrdinal("LuongXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_Insert";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
			db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, this._LuongXuat);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(BKNPLXuatGiaCongCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLXuatGiaCong item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, BKNPLXuatGiaCongCollection collection)
        {
            foreach (BKNPLXuatGiaCong item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_InsertUpdate";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
			db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, this._LuongXuat);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(BKNPLXuatGiaCongCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLXuatGiaCong item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
			db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
			db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
			db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, this._LuongXuat);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, this._TenDVT);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(BKNPLXuatGiaCongCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLXuatGiaCong item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
        public int DeleteTransactionBy_BangKeHoSoThanhLy_ID(SqlTransaction transaction, long id)
        {
            string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_DeleteBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, id);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLXuatGiaCong_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(BKNPLXuatGiaCongCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLXuatGiaCong item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(BKNPLXuatGiaCongCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLXuatGiaCong item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
        public int DeleteTransactionByBKTKN(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, SqlTransaction transaction)
        {
            string query = "DELETE FROM t_KDT_SXXK_BKNPLXuatGiaCong WHERE ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string where = "SoToKhai=" + soToKhai + " AND MaLoaiHinh='" + maLoaiHinh + "' AND NamDangKy=" + namDangKy + " AND MaHaiQuan ='" + maHaiQuan + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query + where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteTransactionByBKTKX(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, SqlTransaction transaction)
        {
            string query = "DELETE FROM t_KDT_SXXK_BKNPLXuatGiaCong WHERE ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string where = "SoToKhaiXuat=" + soToKhai + " AND MaLoaiHinhXuat='" + maLoaiHinh + "' AND NamDangKyXuat=" + namDangKy + " AND MaHaiQuanXuat ='" + maHaiQuan + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query + where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		#endregion
	}	
}