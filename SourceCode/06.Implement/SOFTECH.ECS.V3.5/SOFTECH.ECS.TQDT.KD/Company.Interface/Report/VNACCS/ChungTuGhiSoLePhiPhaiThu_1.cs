using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class ChungTuGhiSoLePhiPhaiThu_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public ChungTuGhiSoLePhiPhaiThu_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAF8040 vaf804)
        {
            lblTencqHQ.Text = vaf804.CSM.GetValue().ToString();
            lblTenchicucHQ.Text = vaf804.CBN.GetValue().ToString();
            lblSochungtu.Text = vaf804.SPN.GetValue().ToString();
            lblTendvXNK.Text = vaf804.IEN.GetValue().ToString();
            lblMadvXNK.Text = vaf804.IEC.GetValue().ToString();
            lblMabuuchinh.Text = vaf804.PCD.GetValue().ToString();
            lblDiachinguoiXNK.Text = vaf804.ADB.GetValue().ToString();
            lblSodtXNK.Text = vaf804.TEL.GetValue().ToString();
            lblSotokhai.Text = vaf804.ICN.GetValue().ToString();
            if (Convert.ToDateTime(vaf804.DDC.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgaydangkytokhai.Text = Convert.ToDateTime(vaf804.DDC.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgaydangkytokhai.Text = "";
            lblMaphanloai.Text = vaf804.DKC.GetValue().ToString();
            lblManganhang.Text = vaf804.BCD.GetValue().ToString();
            lblTennganhang.Text = vaf804.BNM.GetValue().ToString();
            lblKihieuchungtu.Text = vaf804.BPS.GetValue().ToString();
            lblSohieuphathanh.Text = vaf804.BPN.GetValue().ToString();
            BindReportSacPhi(vaf804);
            lblTongsophi.Text = vaf804.DTM.GetValue().ToString();
            lblSotaikhoan.Text = vaf804.TAN.GetValue().ToString();
            lblTenkhobac.Text = vaf804.TNM.GetValue().ToString();
            if (Convert.ToDateTime(vaf804.DOP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayphathanh.Text = Convert.ToDateTime(vaf804.DOP.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayphathanh.Text = "";


        }
        public void BindReportSacPhi(VAF8040 vaf804)
        {
            try
            {
               
                DataTable dt = new DataTable();
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("TenSacPhi", typeof(string));
                dt.Columns.Add("SoTienPhi", typeof(string));

                for (int i = 0; i < vaf804.CC1.listAttribute[0].ListValue.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["STT"] = (i + 1).ToString();
                    dr["TenSacPhi"] = vaf804.CC1.listAttribute[0].GetValueCollection(i).ToString().Trim();
                    dr["SoTienPhi"] = vaf804.CC1.listAttribute[1].GetValueCollection(i).ToString().Trim();
                    dt.Rows.Add(dr);
                }

                //while (dt.Rows.Count < 4)
                //{


                //    DataRow dr = dt.NewRow();
                //    dr["SacThue"] = "";
                //    dr["Chuong"] = chuong[dt.Rows.Count];
                //    dr["TieuMuc"] = "";
                //    dr["SoTienThue"] = "";
                //    dr["SoTienMien"] = "";
                //    dr["SoTienGiam"] = "";
                //    dr["SoThuePhaiNop"] = "";
                //    dt.Rows.Add(dr);
                //}
                DetailReport.DataSource = dt;
                lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblTensacphi.DataBindings.Add("Text", DetailReport.DataSource, "TenSacPhi");
                lblSophiphainop.DataBindings.Add("Text", DetailReport.DataSource, "SoTienPhi");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }

    }
}
