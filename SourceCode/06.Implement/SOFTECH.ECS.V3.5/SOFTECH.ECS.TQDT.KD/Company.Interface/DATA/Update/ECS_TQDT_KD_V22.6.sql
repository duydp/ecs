
DELETE FROM [t_VNACC_Category_Common] WHERE ReferenceDB='A528'
go
ALTER TABLE dbo.t_VNACC_Category_Common
ALTER COLUMN Notes NVARCHAR(max) NULL
go

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WA02',N'Giấy phép XNK, giấy phép quá cảnh',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WY02',N'Giấy phép xuất khẩu (nhập khẩu) hoá chất Bảng 1  của Thủ tướng Chính phủ',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WY03',N'Giấy phép xuất khẩu (nhập khẩu) hóa chất Bảng 2, hóa chất Bảng 3 của Bộ Công thương',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WF02',N'Giấy chứng nhận đủ điều kiện xk gạo; Hợp đồng được Hiệp hội lương thực Việt Nam xác nhận',N'    ')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WL02',N'Giấy xác nhận của Kiểm Lâm hoặc của UBND nơi có cây trồng; Bảng kê lâm sảnHàng nhập khẩu có xác nhận của Cites Kiểm dịch thực vật',N'Xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WM02',N'Giấy chứng nhận nhà nhập khẩu, ủy quyền, được chỉ định nhập khẩu Giấy đăng ký kiểm tra VSATTP Thông báo kêt quả chất lượng',N'Xuất trình cho cơ quan hải quan trong quá trình làm thủ tục')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WQ02',N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế',N'Xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WQ03',N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế',N'Xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WQ04',N'Giấy phép của Bộ Công thương',N'Xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WR02',N'Giấy xác nhận của Bộ KHCN, Bộ Kế hoạch đầu tư',N'Xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','WS02',N'Giấy chứng nhận hợp quy Giấy đăng ký đúng ngành nghềThông báo kết quả kiểm tra đạt chất lượng',N'Xuất trình cho cơ quan hải quan trong quá trình làm thủ tục')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','YY02',N'- Văn bản chấp thuận cho nhập khẩu (đối với thức ăn chăn nuôi, thủy sản nhập khẩu chưa được phép lưu hành tại Việt Nam',N'Tại thời điểm đăng ký tờ khai')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','YY01',N'- Kiểm tra chất lượng',N'Trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ZA02',N'Phiếu tiếp nhận hồ sơ công bố tiêu chuẩn áp dụng đối với loại A',N'Trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ZA03',N'Giấy chứng nhận đăng ký lưu hành trang thiết bị y tế áp dụng đới với loại B,C,D',N'')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','YZ02',N'Thông báo kết quả kiểm tra nhà nước chất lượng phân bón nhập khẩu',N'Trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','YZ03',N'Giấy phép nhập khẩu (trong 1 số trường hợp)',N'')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ZB02',N'Giấy phép xuất khẩu nhập khẩu tiền chất',N'Trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ZB03',N'Khai báo hóa chất',N'')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AA02',N'Trường hợp xuất khẩu, nhập khẩu hoá chất thuộc Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu trong những trường hợp đặc biệt cho mục đích nghiên cứu, y tế, dược phẩm hoặc bảo vệ thực hiện theo quy định tại Điều 5 Nghị định số 100/2005/NĐ-CP, phải có các giấy tờ:- Giấy chứng nhận đăng ký kinh doanh hành nghề hoá chất do cơ quan nhà nước có thẩm quyền cấp và chứng nhận đăng ký mã số xuất, nhập khẩu ghi trên Giấy chứng nhận đăng ký thuế do cơ quan thuế cấp;-Giấy chứng nhận đủ điều kiện kinh doanh hàng hoá là hoá chất độc hại và sản phẩm có hoá chất độc hại do Sở Khoa học và Công nghệ tỉnh, thành phố trực thuộc Trung ương cấp theo quy định của Bộ Khoa học và Công nghệ;-  Giấy phép của Bộ Công nghiệp cấp cho doanh nghiệp đối với từng lần xuất khẩu hoặc nhập khẩu.',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AC02',N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương ',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AD02',N'Giấy phép nhập khẩu của Bộ Công nghiệp (Bộ Công Thương)',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AD03',N'Bản kê khai tiêu chuẩn chất lượng và tiêu chuẩn kỹ thuật tương ứng để cơ quan HQ kiểm tra đối chiếu. Việc NK hoá chất có tiêu chuẩn thấp hơn phải có ý kiến đồng ý bằng văn bản của Bộ CT',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AF02',N'Giấy phép nhập khẩu tự động',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AG01',N'Giấy phép xuất khẩu, giấy phép nhập khẩu',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AJ02',N'Giấy phép nhập khẩu tự động của Bộ Công Thương',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AG02',N'Giấy phép xuất khẩu, giấy phép nhập khẩu',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AQ02',N'Giấy phép nhập khẩu tự động',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AR02',N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương ',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AS02',N'Giấy phép nhập khẩu của Bộ Công thương',N'a) Áp dụng giấy phép nhập khẩu để được hưởng thuế suất thuế nhập khẩu trong hạn ngạch thuế quan đối với các mặt hàng thuộc danh mục hàng hóa nhập khẩu theo hạn ngạch thuế quan nêu trên.b) Các mặt hàng thuộc danh mục áp dụng hạn ngạch thuế quan nhập khẩu không có giấy phép của Bộ Công Thương được áp dụng mức thuế ngoài hạn ngạch thuế quan. Riêng thuốc lá nguyên liệu nhập khẩu ngoài hạn ngạch thuế quan để sản xuất thuốc lá điếu thực hiện theo hướng dẫn của Bộ Công Thương.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AS03',N'Trường hợp nhập khẩu phục vụ nghiên cứu khoa học, viện trợ nhân đạo: phải có văn bản của Bộ Công Thương',N'Cấm nhập khẩu')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AS04',N'Giấy phép của Bộ Công Thương Đối với trường hợp nhập khẩu hàng hóa để phục vụ cho mục đích an ninh, quốc phòng, việc nhập khẩu thực hiện theo quy định của Bộ Công an, Bộ Quốc phòng.',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','AV02',N'-Hàng hóa nhập khẩu phải có Giấy chứng nhận xuất xứ hàng hóa mẫu S (C/O form S) do Bộ Thương mại Vương quốc Campuchia hoặc cơ quan được ủy quyền cấp theo quy định và được thông quan qua các cặp cửa khẩu nêu tại Phụ lục số 02 kèm theo Thông tư này-Thương nhân Việt Nam được nhập khẩu mặt hàng thóc, gạo các loại theo hạn ngạch thuế quan.- Đối với lá thuốc lá khô, chỉ những thương nhân có giấy phép nhập khẩu thuốc lá nguyên liệu theo hạn ngạch thuế quan do Bộ Công Thương cấp theo quy định tại Thông tư số 04/2006/TT-BTM ngày 06 tháng 4 năm 2006 của Bộ Thương mại (nay là Bộ Công Thương) .',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BV02',N'- Phân bón thuộc Danh mục quy định tại Phụ lục ban hành kèm theo Thông tư chỉ được nhập khẩu qua cửa khẩu quốc tế và cửa khẩu chính. - Trường hợp nhập khẩu qua cửa khẩu phụ, lối mở thì phải được sự cho phép của Ủy ban nhân dân tỉnh.',N'Trước khi thông quan')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BC02',N'-Thông báo thực phẩm đạt yêu cầu nhập khẩu hoặc Thông báo thực phẩm chỉ kiểm tra hồ sơ',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BD02',N'Giấy phép xuất khẩu/Giấy phép nhập khẩu của Bộ Công Thương',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BF02',N'Giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón thuộc Danh mục quy định tại Phụ lục I ',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BQ02',N'Giấy phép nhập khẩu tự động',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BT02',N'Kết quả kiểm tra chất lượng (Quy định tại Luật Chất lượng và Nghi định 132/NĐ-CP)',N'Yêu cầu nộp trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BU02',N'Kết quả Phiếu thử nghiệm hiệu suất năng lượng tối thiểu (Theo công văn hướng dẫn của Tổng cục Hải quan)',N'Yêu cầu nộp trước khi thông quan ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','BW02',N'Mã số kinh donh tạm nhập tái xuất đối với hàng ',N'Khi mở tờ khai hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','DS02',N'Nhập khẩu các mặt hàng nuôi trồng thủy sản ngoài Danh mục thức ăn chăn nuôi thủy sản được phép lưu hành tại Việt Nam và Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam do Bộ Nông nghiệp và Phát triển nông thôn ban hành phải có giấy phép của Tổng cục Thủy sản',N'Yêu cầu nộp/ xuất trình Giấy phép của Bộ NNPTNT trong quá trình làm thủ tục hải quan đối với các mặt hàng nuôi trồng thủy sản ngoài Danh mục thức ăn chăn nuôi thủy sản được phép lưu hành tại Việt Nam và Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','DS03',N'Các loài thủy sản có tên trong Danh mục các loài thuỷ sản xuất khẩu có điều kiện chỉ được xuất khẩu khi đáp ứng đủ các điều kiện nêu tại Phụ lục 02 Thông tư 88/2011/TT-BNNPTNT ngày 28/12/2011, không phải xin phép.',N'Yêu cầu nộp/ xuất trình Giấy phép của Bộ NNPTNT trong quá trình làm thủ tục hải quan đối với các loài thủy sản có tên trong Danh mục các loài thuỷ sản xuất khẩu có điều kiện chỉ được xuất khẩu khi đáp ứng đủ các điều kiện nêu tại Phụ lục 02 Thông tư 88/2011/TT-BNNPTNT ngày 28/12/2011')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','DX02',N'Phụ lục 3. Danh mục hoá chất, kháng sinh hạn chế sử dụng trong sản xuất, kinh doanh thuỷ sản Nhập khẩu thức ăn chăn nuôi ngoài Danh mục thức ăn chăn nuôi thủy sản phải có giấy phép của Tổng cục Thủy sản (đối với thức ăn thủy sản); Nhập khẩu sản phẩm xử lý, cải tạo môi trường nuôi trồng thủy sản ngoài Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam phải xin phép Tổng cục Thủy sản',N'Yêu cầu nộp/ xuất trình Giấy phép của Cục Thủy sản trong quá trình làm thủ tục hải quan đối với  hàng hóa thuộc Danh mục hoá chất, kháng sinh hạn chế sử dụng trong sản xuất, kinh doanh thuỷ sản')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','DX03',N'Phụ lục 4. Danh mục thuốc, hoá chất, kháng sinh hạn chế sử dụng trong thú y . Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép.Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép',N'Yêu cầu nộp/ xuất trình Giấy phép của Cục Thú y trong quá trình làm thủ tục hải quan đối với hàng hóa thuộc Danh mục thuốc, hoá chất, kháng sinh hạn chế sử dụng trong thú y.')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FZ02',N'Thông báo kiểm tra chất lượng đáp ứng yêu cầu nhập khẩu đối với thuốc bảo vệ thực vật',N'Trước thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FY02',N'Thông báo kiểm tra nhà nước về chất lượng đáp ứng yêu cầu nhập khẩu đối với thuốc bảo vệ thực vật thuộc Danh mục thuốc bảo vệ thực vật được phép sử dụng tại Việt Nam(theo quy định tại TT 21/2015/TT-BNNPTNT (Điều 43)- Luật Bảo vệ và Kiểm dịch thực vật (khoản 3 Điều 67)',N'Trước thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FY03',N'- Cấp- Giấy phép nhập khẩu trong trường hợp nhập khẩu thuốc trong Danh mục thuốc bảo vệ thực vật cấm sử dụng tại Việt Nam nhưng nhập khẩu để làm chất chuẩn (theo quy định tại Luật bảo vệ và kiểm dịch thực vật (khoản 2 Điều 67)',N'Tại thời điểm đăng ký tờ khai hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EC02',N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm trao đổi quốc tế trong trường hợp đặc biệt theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn',N'Yêu cầu nộp/ xuất trình văn bản đồng ý của Bộ NNPTNT trong quá trình làm thủ tục hải quan đối với giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm trao đổi quốc tế trong trường hợp đặc biệt.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EC03',N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm hạn chế trao đổi quốc tế theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn',N'Yêu cầu nộp/ xuất trình văn bản đồng ý của Bộ NNPTNT trong quá trình làm thủ tục hải quan đối với giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm trao đổi quốc tế trong trường hợp đặc biệt.')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FS02',N' Danh mục hàng hóa xuất khẩu theo giấy phép là củi, than làm từ gỗ hoặc củi có nguồn gỗ từ gỗ rừng tự nhiên trong nước',N'Yêu cầu nộp/ xuất trình giấy phép ')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EH02',N'Thương nhân trao đổi với nước ngoài những giống vật nuôi quý hiếm có trong Danh mục giống vật nuôi quý hiếm cấm xuất khẩu và Danh mục nguồn gen vật nuôi quý hiếm cần bảo tồn để phục vụ nghiên cứu khoa học hoặc các mục đích đặc biệt khác do Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn quyết định',N'Yêu cầu nộp/ xuất trình văn bản đồng ý của Bộ NNPTNT trong quá trình làm thủ tục hải quan đối với giống vật nuôi quý hiếm có trong Danh mục giống vật nuôi quý hiếm cấm xuất khẩu và Danh mục nguồn gen vật nuôi quý hiếm cần bảo tồn để phục vụ nghiên cứu khoa học hoặc các mục đích đặc biệt khác.')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EL02',N'Nộp/xuất trình giấy phép của Cục Thú y theo TT 88/2011/TT-BNNPTNT',N'Nộp/xuất trình giấy phép của Cục Thú y theo TT 88/2011/TT-BNNPTNT')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EN02',N'Giấy thông báo kết quả kiểm dịch hoặc giấy miễn kiểm dịch theo Thông tư 01/2012/TT-BTC ngày 03/01/2012; Giấy thông báo kết quả kiểm tra VSATTP đối với hàng hóa có nguồn gốc động vật nhập khẩu theo Thông tư 25/2010/TT-BNNPNT ngày 08/4/2010;',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EP02',N'Giấy thông báo kết quả kiểm tra hoặc giấy miễn kiểm tra theo Thông tư 01/2012/TT-BTC ngày 03/01/2012',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EQ02',N'Giấy thông báo kết quả kiểm tra nhà nước về chất lượng hoặc giấy miễn kiểm tra nhà nước về chất lượng theo Thông tư 50/2009/TT-BNNTPTN ngày 18/8/2009',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ER02',N'Hàng phải kiểm tra an toàn thực phẩm',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan.')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EX02',N'Hàng thuộc diện phải kiểm dịch theo TT 32/2012/TT-BNNPTNT',N'Yêu cầu nộp/ xuất trình trong quá trình làm thủ tục hải quan.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FA02',N'Giấy xác nhận chất lượng',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FE02',N'Thông báo kết quả kiểm tra chất lượng muối nhập khẩu',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FF02',N'Thông báo kết quả kiểm dịch đạt yêu cầu',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH02',N'Giấy phép CITES khi XK gỗ và sản phẩm làm từ gỗ thuộc các Phụ lục của CITES',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH03',N'Giấy phép xuất khẩu với hàng hóa xuất khẩu quy định tại Điều 10 Thông tư này',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH04',N'Giấy phép nhập khẩu với hàng hóa nhập khẩu quy định tại Điều 11 Thông tư này',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH05',N'Giấy phép xuất khẩu, nhập khẩu giống cây trồng quy định tại Mục 3 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH06',N'Giấy phép xuất khẩu, nhập khẩu giống vật nuôi quy định tại Mục 4 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH07',N'Giấy phép nhập khẩu thuốc thú y quy định tại Mục 5 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH08',N'Giấy phép nhập khẩu thuốc bảo vệ thực vật và vật thể phải có giấy phép kiểm dịch thực vật nhập khẩu  quy định tại Mục 6 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH09',N'Giấy phép nhập khẩu thức ăn gia súc, gia cầm quy định tại Mục 7 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH10',N'Giấy phép nhập khẩu phân bón quy định tại Mục 8 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH11',N'Giấy phép xuất khẩu, nhập khẩu nguồn gen cây trồng phục vụ nghiên cứu, trao đổi khoa học kỹ thuật quy định tại Mục 9 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FH12',N'Giấy phép xuất khẩu, nhập khẩu hàng hóa chuyên ngành thủy sản theo quy định tại Mục 10 Thông tư',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FN02',N'Giấy phép nhập khẩu thuốc bảo vệ thực vật nhằm mục đích xuất khẩu',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FN03',N'Giấy chứng nhận đủ điều kiện kiểm tra chất lượng',N'Xuất trình trước khi thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FQ02',N'Danh mục động vật, sản phẩm động vật thủy sản thuộc diện phải kiểm dịch',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','FT02',N'Danh mục động vật, sản phẩm động vật trên cạn thuộc diện phải kiểm dịch',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','EQ02',N'Giấy thông báo kết quả kiểm tra nhà nước về chất lượng hoặc giấy miễn kiểm tra nha nước về chất lượng',N'Chỉ thông quan khi có kết quả kiểm tra hoặc giấy thông báo miễn kiểm tra')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','GA01',N'Cấp Giấy phép xuất khẩu đối với các trường hợp sau:a) Mẫu vật các loài động vật, thực vật thuộc Phụ lục I CITES có nguồn gốc từ tự nhiên không vì mục đích thương mại: phục vụ ngoại giao, nghiên cứu khoa học, trao đổi giữa các vườn động vật, vườn thực vật, triển lãm không vì mục đích thương mại, biểu diễn xiếc không vì mục đích thương mại; trao đổi, trao trả mẫu vật giữa các Cơ quan quản lý CITES các nước.b) Xuất khẩu vì mục đích thương mại:- Mẫu vật động vật, thực vật hoang dã, nguy cấp, quý, hiếm từ tự nhiên quy định tại Phụ lục II, III của Công ước CITES, mẫu vật tiền Công ước CITES;- Mẫu vật nuôi sinh sản, nuôi sinh trưởng, trồng cấy nhân tạo quy định tại các Phụ lục của Công ước CITES;- Mẫu vật thực vật rừng từ tự nhiên thuộc nhóm IIA theo quy định của Chính phủ;- Mẫu vật động vật, thực vật hoang dã, nguy cấp, quý, hiếm thuộc nhóm I, nhóm II theo quy định của Chính phủ và mẫu vật quy định tại Phụ lục của Công ước CITES có nguồn gốc nuôi, trồng theo quy định tại Nghị định số 82/2006/NĐ-CP ngày 10/8/2006 của Chính phủ về quản lý hoạt động xuất khẩu, nhập khẩu, tái xuất khẩu, nhập nội từ biển, quá cảnh, nuôi sinh sản, nuôi sinh trưởng và trồng cấy nhân tạo các loài động vật, thực vật hoang dã nguy cấp, quý, hiếm và Điều 5 Nghị định số 98/2011/NĐ-CP ngày 26/10/2011 của Chính phủ sửa đổi, bổ sung một số điều của các Nghị định về nông nghiệp.(theo Thông tư số 04/2015/TT-BNNPTNT Khoản 2 Điều 10, 12)',N'Tại thời điểm đăng ký tờ khai')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','GA02',N'Cấp chứng chỉ CITES xuất khẩu mẫu vật lưu niệm(Theo Nghị định 82/2006/NĐ-CP (khoản 2 Điều 15, khoản 1 Điều 4))',N'Tại thời điểm đăng ký tờ khai')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','GA03',N'Chứng chỉ mẫu vật tiền công ước đối với mẫu vật nuôi sinh sản, nuôi sinh trưởng, trồng cấy nhân tạo quy định tại các Phụ lục của Công ước CITES(theo Nghị định 82/2006/NĐ-CP (khoản 3 Điều 15, khoản 1 Điều 4))',N'Tại thời điểm đăng ký tờ khai')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','GA11',N'Giấy phép nhập khẩu cho các trường hợp sau:a) Mẫu vật các loài động vật, thực vật thuộc Phụ lục I CITES có nguồn gốc từ tự nhiên không vì mục đích thương mại: phục vụ ngoại giao, nghiên cứu khoa học, trao đổi giữa các vườn động vật, vườn thực vật, triển lãm không vì mục đích thương mại, biểu diễn xiếc không vì mục đích thương mại; trao đổi, trao trả mẫu vật giữa các Cơ quan quản lý CITES các nước.b) Nhập khẩu Mẫu vật động vật, thực vật hoang dã, nguy cấp, quý, hiếm từ tự nhiên quy định tại Phụ lục II, III của Công ước CITES, mẫu vật tiền Công ước CITES;c) Mẫu vật nuôi sinh sản, nuôi sinh trưởng, trồng cấy nhân tạo quy định tại các Phụ lục của Công ước CITES;b) Mẫu vật động vật, thực vật thuộc Phụ lục I có nguồn gốc từ trại nuôi sinh sản, cơ sở trồng cấy nhân tạo, mẫu vật quy định tại Điểm a Khoản 2 Điều này và mẫu vật động vật, thực vật thuộc Phụ II và III của Công ước CITES phải được Cơ quan Quản lý CITES Việt Nam cấp giấy phép.(theo Thông tư 04/2015/TT-BNNPTNT (khoản 2 Điều 11))',N'Tại thời điểm đăng ký tờ khai')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','GE02',N'Giấy chứng nhận hoặc thông báo đạt yêu cầu kiểm tra ATTP (Theo thông tư số 13/TTLT-2014/BYT-BNNPTNT-BCT)',N'Trước khi thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HW02',N'Công bố hợp quy  (Theo 38/2012/NĐ-CP ngày 25/04/2012)',N'Trước khi thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HW03',N'Giấy xác nhận đăng ký kiểm tra vệ sinh an toàn thực phẩm (Theo 38/2012/NĐ-CP ngày 25/04/2012))',N'')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','TB02',N'Danh mục hàng hóa phải có giấy phép NK ',N'Nộp trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','TC02',N'Giấy phép xuất khẩu, nhập khẩu vàng nguyên liệu',N'Nộp trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','VA02',N'Giấy đăng ký kiểm tra chất lượng  (được đưa hàng về bảo quản trước khi có kết quả kiểm tra) hoặc giấy chứng nhận kiểm tra chất lượng ',N'Hàng phải đạt chất lượng mới được phép NK')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LA02',N'Danh mục tem bưu chính cần GPNK',N'Giấy phép nhập khẩu ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LA03',N'DM thiết bị thu phát sóng vô tuyến điện cần GPNK',N'Giấy phép nhập khẩu ')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LJ02',N'Văn bản cho phép nhập khẩu sản phẩm thuộc Danh mục cấm nhập khẩu để nghiên cứu khoa học của Bộ Thông tin và truyền thông',N'Yêu cầu nộp /xuất trình trong quá trình làm thủ tục hải quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LJ03',N'Văn bản chấp thuận cho phép thực hiện hoạt động gia công của Bộ Thông tin và truyền thông khi thực hiện hoạt động gia công tái chế, sửa chữa các sản phẩm công nghệ thông tin đã qua sử dụng thuộc danh mục cấm nhập khẩu cho thương nhân nước ngoài',N'Yêu cầu nộp /xuất trình trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LE02',N'Danh mục thiết bị phát, thu-phát sóng vô tuyến điện theo Phụ lục I',N'Giấy phép nhập khẩu ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LF02',N'Danh mịc tem bưu chính cần giấy phép nhập khẩu theo quy định tại Phụ lục I',N'Giấy phép nhập khẩu ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LG02',N'Không ',N'Giấy phép nhập khẩu xuất bản phẩm')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LH02',N'Danh mục chủng loại thiết bị in phải có giấy phép nhập khẩu theo quy định tại Điều 9 Thông tư',N'Giấy phép nhập khẩu ')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LM02',N'Có danh mục',N'- Giấy phép nhập khẩu- Văn bản xác nhận đăng ký nhập khẩu')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','LN02',N'Có Danh mục, không có HS',N'Chứng nhận, công bố hợp quy')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','MB02',N'DM phế liệu được phép NK từ nước ngoài để làm nguyên liệu sản xuất',N'- Thuộc DM- Đáp ứng yêu cầu kỹ thuật- DN có giấy chứng nhận đủ điều kiện NK')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','MC02',N'Có Danh mục',N'Giấy xác nhận đủ điều kiện về bảo vệ môi trường trong nhập khẩu phế liệu làm nguyên liệu sản xuất')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','NB02',N'Danh mục sản phẩm, hàng hoá có khả năng gây mất an toàn',N'Nộp/xuất trình giấy đăng ký kiểm tra chất lượng theo Điểm c Khoản 2 Điều 4 Chương II Thông tư 11/2009/TT-BXD')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','NC02',N'không',N'Cho phép tạm thông quan trước với điều kiện phải có kết quả thử nghiệm, giám định mẫu về chỉ tiêu hoạt tính cường độ ở tuổi 3 ngày ± 45 phút và các chỉ tiêu còn lại khác phù hợp với quy định nêu tại Điều 4. Sau đó phải có kết quả thử nghiệm, giám định chỉ tiêu hoạt tính cường độ ở tuổi 28 ngày ± 45 giờ phù hợp với quy định liên quan nêu tại Điều 4, nhà nhập khẩu mới được phép sử dụng  hoặc lưu thông trên thị trường.  ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','ND02',N'Danh mục hàng hoá vật liệu xây dựng gạch ốp lát',N'Thuộc đối tượng kiểm tra nhà nước về chất lượng;  Nộp bản sao giấy chứng nhận hệ thống quản lý chất lượng của nhà sản xuất ra sản phẩm phù hợp tiêu chuẩn TCVN ISO 9001/ISO 9001 còn thời hạn hiệu lực;')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','NG02',N'Giấy phép khai thác khoáng sản theo quyết định để sản xuất vôi, dô lô mit nung cho cơ quan hải quan ',N'Tại thời điểm làm thủ tục xuất khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','PC02',N'Danh mục hàng hóa được phép xuất khẩu',N'Giấy phép xuất khẩu')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','PC03',N'Danh mục hàng hóa được phép nhập khẩu',N'Giấy phép nhập khẩu')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','PC04',N'',N'Giấy xác nhận đủ điều kiện nhập khẩu và Danh mục nhập khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','QB02',N'',N'Giấy phép xuất khẩu, nhập khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HA02',N'24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại VN',N'- Giấy đăng ký lưu hành- Đăng ký kiểm tra chất lượng, được đưa hàng về bảo quản, phải có kết quả trước khi thông quan')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HB02',N'Danh mục vắc xin, sinh phẩm y tế dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu ',N'- Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HB03',N'Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu',N'- Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC02',N'1.Danh mục mã số hàng hóa nguyên liệu làm thuốc dùng cho người nhập khẩu vào VN',N'Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC03',N'2.Danh mục mã số hàng hóa thuốc bán thành phẩm nhập khẩu vào VN',N'Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC04',N'3.Danh mục mã số hàng hóa thuốc thành phẩm dạng đơn chất nhập khẩu vào VN',N'Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC05',N'4.Danh mục Mã số hàng hóa của thuốc thành phẩm đa thành phần nhập khẩu vào VN',N'Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC06',N'5.Danh mục mã số hàng hóa Dược liệu nhập khẩu vào VN',N'Giấy đăng ký lưu hành')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HC07',N'6.Danh mục hàng hóa mỹ phẩm nhập khẩu vào VN',N'- Giấy đăng ký lưu hành - Phiếu công bố sản phẩm mỹ phẩm')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HQ01',N'Danh mục nguyên liệu và thuốc thành phẩm cấm nhập khẩu để làm thuốc dùng cho người',N' Nếu thuộc DM thì không được phép NK.')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HU02',N'Không có danh mục (Danh mục dược liệu quy định tại QĐ 41/2007/QĐ-BYT)',N'- Giấy phép nhập khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HK01',N'Danh mục hàng hoá nhập khẩu phải kiểm tra về vệ sinh an toàn thực phẩm theo mã số HS',N'- Đăng ký Kiểm tra chất lượng (được đưa hàng về bảo quản trước khi có kết quả Kiểm tra) hoặc thông quan khi Xuất trình kết quả đạt chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HK01',N'Danh mục hàng hoá nhập khẩu phải kiểm tra về vệ sinh an toàn thực phẩm theo mã số HS',N'- Đăng ký Kiểm tra chất lượng (được đưa hàng về bảo quản trước khi có kết quả Kiểm tra) hoặc thông quan khi Xuất trình kết quả đạt chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HL02',N'không',N'Phiếu công bố sản phẩm mỹ phẩm')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HM02',N'Không',N'- Giấy đăng ký lưu hành/Giấy xác nhận nguyên liệu được phép NK đối với nguyên lệu NK để sản xuất hóa chất, chế phẩm- Kiểm tra chất lượng- Giấy phép NK (khi chưa có giấy đăng ký lưu hành nhưng đã có văn bản cho phép NK của Bộ Y tế)')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HN02',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc phạm vi được phân công quản lý của Bộ Y tế',N'- Đăng ký Kiểm tra chất lượng (được đưa hàng về bảo quản trước khi có kết quả Kiểm tra) hoặc thông quan khi Xuất trình kết quả đạt chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HP02',N'Không',N'Đơn thuốc, sổ khám bệnh, đơn đề nghị có xác nhận của sở y tế.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HQ02',N'Không có danh mục, chỉ sửa đổi phần nguyên tắc quản lý, không sửa đổi danh mục',N'Không thay đổi so với TT 47/2010/TT-BYT')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HR02',N'',N'Là doanh nghiệp thuộc danh sách các doanh nghiệp được phép xuất khẩu, nhập khẩu…')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HS02',N'Không có',N'Giấy phép xuất khẩu, nhập khẩu mẫu bệnh phẩm')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','HT02',N'Danh mục trang thiết bị y tế phải cấp giấy phép nhập khẩu',N'Giấy phép nhập khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KD02',N'Không có',N'Giấy đăng ký kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KE02',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông vận tải',N'Giấy đăng ký kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KH02',N'Giấy phép nhập khẩu pháp hiệu cho an toàn hàng hải',N'Xuất trình giấy phép trong quá trình làm thủ tục hải quan')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KK02',N'Danh mục hàng hóa có khả năng gây mất an toàn',N'Kết quả kiểm tra chất lượng theo Luật Chất lượng và Nghi định 132/NĐ-CP')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KL02',N'- Không có danh mục- Dẫn chiếu đến TCVN 7772: 2007 và Phụ lục II TT 13/2015/TT-BGTVT',N'Chứng chỉ chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KM02',N'',N'- Được nhập khẩu theo giấy phép và thuộc diện quản lý chuyên ngành của Bộ GTVt')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','RA02',N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.',N'Giấy thông báo kết quả kiểm tra chất lượngGiấy đăng ký kiểm tra chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','RD02',N' Danh mục hàng hóa nhập khẩu phải kiểm tra chất lượng ',N'Giấy thông báo kết quả kiểm tra chất lượng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','RE02',N' Danh mục hàng hóa nhập khẩu phải kiểm tra chất lượng ',N'Chứng thư giám định của một tổ chức giám định..')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','RF02',N'Không có',N'Đăng ký kiểm tra chất lượng hàng hóa')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','RF03',N'',N'Thông báo kết quả kiểm tra chất lượng')


INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','UB02',N'Không có',N'Giấy phép nhập khẩu')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','XA02',N'Không có danh mục',N'Giây chứng nhận KP')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KD02',N'Không có',N' - Giấy chứng nhận đăng ký xe ô tô hoặc Giấy chứng nhận đăng ký lưu hành không quá 5 năm.- Giấy thông báo kết quả kiểm tra chất lượng- Quy định cửa khẩu nhập')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','KD02',N'Không có',N' - Giấy đăng ký kiểm tra nhà nước về chất lượng;  - Giấy chứng nhận kết quả kiểm tra chất lượng đạt yêu cầu; - Quy định cửa khẩu nhập')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','XF02',N'Có danh mục',N'Giấy đăng ký của Bộ TN- MT Giấy phép của Bộ Công thưog')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','XG02',N'',N' - Giấy chứng nhận hợp quy; -  Kết quả kiểm tra chất lượng.')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A528','XJ02',N'PL1: Danh mục các sản phẩm thép không thuộc phạm vi điều chỉnh của Thông tư liên tịchPL2: Danh mục các sản phẩm thép phải kiểm tra chất lượng theo tiêu chuẩn cơ sở, tiêu chuẩn quốc gia (TCVN), tiêu chuẩn khu vực, tiêu chuẩn của các nước và tiêu chuẩn quốc tếPL3:Danh mục các sản phẩm thép phải kiểm tra chất lượng theo tiêu chuẩn quốc gia (TCVN) của Việt nam, Tiêu chuẩn quốc gia của nước xuất khẩu',N'Thông báo kết quả kiểm tra chất lượng hàng hóa nhập khẩu.Đối với các loại thép quy định tại Mục 2 Phụ lục III cần nộp thêm Bản kê khai thép nhập khẩu và bản sao Giấy xác nhận nhu cầu nhập khẩu')
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.6',GETDATE(), N'CẬP NHẬT MÃ PHÂN LOẠI GIẤY PHÉP')
END