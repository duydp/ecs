﻿namespace Company.Interface.GC
{
    partial class XuLyQuyetToanHopDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XuLyQuyetToanHopDong));
            Janus.Windows.GridEX.GridEXLayout dgListTotalNPLNhap_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTotalNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDataNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDataSPXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDataNPLQT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDataSPQT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.panel1 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnProcessDataSP = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnProcessDataNPL = new System.Windows.Forms.Button();
            this.btnPrintReportValue = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnPrintReportNumber = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.mnuExportExcel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.tpTotalNPLNhap = new System.Windows.Forms.TabPage();
            this.dgListTotalNPLNhap = new Janus.Windows.GridEX.GridEX();
            this.tpTotalNPLXuat = new System.Windows.Forms.TabPage();
            this.dgListTotalNPLXuat = new Janus.Windows.GridEX.GridEX();
            this.tpTKN = new System.Windows.Forms.TabPage();
            this.dgListTKN = new Janus.Windows.GridEX.GridEX();
            this.tpTKX = new System.Windows.Forms.TabPage();
            this.dgListTKX = new Janus.Windows.GridEX.GridEX();
            this.tpNPLNhap = new System.Windows.Forms.TabPage();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.tpNPLXuat = new System.Windows.Forms.TabPage();
            this.dgListDataNPL = new Janus.Windows.GridEX.GridEX();
            this.tpSPXuat = new System.Windows.Forms.TabPage();
            this.dgListDataSPXuat = new Janus.Windows.GridEX.GridEX();
            this.tpNPLQT = new System.Windows.Forms.TabPage();
            this.dgListDataNPLQT = new Janus.Windows.GridEX.GridEX();
            this.tpSPQT = new System.Windows.Forms.TabPage();
            this.dgListDataSPQT = new Janus.Windows.GridEX.GridEX();
            this.mnuImportData = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuImportDataNumber = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.mnuExportExcel.SuspendLayout();
            this.tpTotalNPLNhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalNPLNhap)).BeginInit();
            this.tpTotalNPLXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalNPLXuat)).BeginInit();
            this.tpTKN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKN)).BeginInit();
            this.tpTKX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKX)).BeginInit();
            this.tpNPLNhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.tpNPLXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataNPL)).BeginInit();
            this.tpSPXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPXuat)).BeginInit();
            this.tpNPLQT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataNPLQT)).BeginInit();
            this.tpSPQT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPQT)).BeginInit();
            this.mnuImportData.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.tabControl1);
            this.grbMain.Controls.Add(this.panel1);
            this.grbMain.Size = new System.Drawing.Size(1092, 501);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.btnProcessDataSP);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnProcessDataNPL);
            this.panel1.Controls.Add(this.btnPrintReportValue);
            this.panel1.Controls.Add(this.btnProcess);
            this.panel1.Controls.Add(this.btnPrintReportNumber);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 441);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1092, 60);
            this.panel1.TabIndex = 0;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1092, 15);
            this.progressBar1.TabIndex = 1;
            this.progressBar1.Value = 10;
            // 
            // btnProcessDataSP
            // 
            this.btnProcessDataSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnProcessDataSP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessDataSP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnProcessDataSP.ForeColor = System.Drawing.Color.White;
            this.btnProcessDataSP.Location = new System.Drawing.Point(200, 25);
            this.btnProcessDataSP.Name = "btnProcessDataSP";
            this.btnProcessDataSP.Size = new System.Drawing.Size(190, 25);
            this.btnProcessDataSP.TabIndex = 0;
            this.btnProcessDataSP.Text = "BƯỚC 2 : XỬ LÝ SỐ LIỆU SP";
            this.btnProcessDataSP.UseVisualStyleBackColor = false;
            this.btnProcessDataSP.Click += new System.EventHandler(this.btnProcessDataSP_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(1029, 25);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(59, 25);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "ĐÓNG";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnProcessDataNPL
            // 
            this.btnProcessDataNPL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnProcessDataNPL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessDataNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcessDataNPL.ForeColor = System.Drawing.Color.White;
            this.btnProcessDataNPL.Location = new System.Drawing.Point(4, 25);
            this.btnProcessDataNPL.Name = "btnProcessDataNPL";
            this.btnProcessDataNPL.Size = new System.Drawing.Size(190, 25);
            this.btnProcessDataNPL.TabIndex = 0;
            this.btnProcessDataNPL.Text = "BƯỚC 1 : XỬ LÝ SỐ LIỆU NPL";
            this.btnProcessDataNPL.UseVisualStyleBackColor = false;
            this.btnProcessDataNPL.Click += new System.EventHandler(this.btnProcessDataNPL_Click);
            // 
            // btnPrintReportValue
            // 
            this.btnPrintReportValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnPrintReportValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintReportValue.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnPrintReportValue.ForeColor = System.Drawing.Color.White;
            this.btnPrintReportValue.Location = new System.Drawing.Point(904, 25);
            this.btnPrintReportValue.Name = "btnPrintReportValue";
            this.btnPrintReportValue.Size = new System.Drawing.Size(118, 25);
            this.btnPrintReportValue.TabIndex = 0;
            this.btnPrintReportValue.Text = "MẪU 15 (TRỊ GIÁ)";
            this.btnPrintReportValue.UseVisualStyleBackColor = false;
            this.btnPrintReportValue.Click += new System.EventHandler(this.btnPrintReportValue_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnProcess.ForeColor = System.Drawing.Color.White;
            this.btnProcess.Location = new System.Drawing.Point(606, 25);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(172, 25);
            this.btnProcess.TabIndex = 0;
            this.btnProcess.Text = "BƯỚC 4 : XỬ LÝ BÁO CÁO";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnPrintReportNumber
            // 
            this.btnPrintReportNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnPrintReportNumber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintReportNumber.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnPrintReportNumber.ForeColor = System.Drawing.Color.White;
            this.btnPrintReportNumber.Location = new System.Drawing.Point(784, 25);
            this.btnPrintReportNumber.Name = "btnPrintReportNumber";
            this.btnPrintReportNumber.Size = new System.Drawing.Size(118, 25);
            this.btnPrintReportNumber.TabIndex = 0;
            this.btnPrintReportNumber.Text = "MẪU15 (LƯỢNG)";
            this.btnPrintReportNumber.UseVisualStyleBackColor = false;
            this.btnPrintReportNumber.Click += new System.EventHandler(this.btnPrintReportNumber_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(396, 25);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(204, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "BƯỚC 3 : CẬP NHẬT LƯỢNG TỒN";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.ContextMenuStrip = this.mnuExportExcel;
            this.tabControl1.Controls.Add(this.tpTotalNPLNhap);
            this.tabControl1.Controls.Add(this.tpTotalNPLXuat);
            this.tabControl1.Controls.Add(this.tpTKN);
            this.tabControl1.Controls.Add(this.tpTKX);
            this.tabControl1.Controls.Add(this.tpNPLNhap);
            this.tabControl1.Controls.Add(this.tpNPLXuat);
            this.tabControl1.Controls.Add(this.tpSPXuat);
            this.tabControl1.Controls.Add(this.tpNPLQT);
            this.tabControl1.Controls.Add(this.tpSPQT);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1092, 441);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnExportExcel});
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(140, 26);
            // 
            // mnExportExcel
            // 
            this.mnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnExportExcel.Image")));
            this.mnExportExcel.Name = "mnExportExcel";
            this.mnExportExcel.Size = new System.Drawing.Size(139, 22);
            this.mnExportExcel.Text = "XUẤT EXCEL";
            this.mnExportExcel.Click += new System.EventHandler(this.mnExportExcel_Click);
            // 
            // tpTotalNPLNhap
            // 
            this.tpTotalNPLNhap.Controls.Add(this.dgListTotalNPLNhap);
            this.tpTotalNPLNhap.Location = new System.Drawing.Point(4, 22);
            this.tpTotalNPLNhap.Name = "tpTotalNPLNhap";
            this.tpTotalNPLNhap.Size = new System.Drawing.Size(1084, 415);
            this.tpTotalNPLNhap.TabIndex = 7;
            this.tpTotalNPLNhap.Text = "TỔNG NGUYÊN PHỤ LIỆU NHẬP";
            this.tpTotalNPLNhap.UseVisualStyleBackColor = true;
            // 
            // dgListTotalNPLNhap
            // 
            this.dgListTotalNPLNhap.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotalNPLNhap.AlternatingColors = true;
            this.dgListTotalNPLNhap.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotalNPLNhap.ColumnAutoResize = true;
            this.dgListTotalNPLNhap.ContextMenuStrip = this.mnuExportExcel;
            dgListTotalNPLNhap_DesignTimeLayout.LayoutString = resources.GetString("dgListTotalNPLNhap_DesignTimeLayout.LayoutString");
            this.dgListTotalNPLNhap.DesignTimeLayout = dgListTotalNPLNhap_DesignTimeLayout;
            this.dgListTotalNPLNhap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotalNPLNhap.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotalNPLNhap.FrozenColumns = 3;
            this.dgListTotalNPLNhap.GroupByBoxVisible = false;
            this.dgListTotalNPLNhap.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalNPLNhap.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalNPLNhap.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotalNPLNhap.Location = new System.Drawing.Point(0, 0);
            this.dgListTotalNPLNhap.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotalNPLNhap.Name = "dgListTotalNPLNhap";
            this.dgListTotalNPLNhap.RecordNavigator = true;
            this.dgListTotalNPLNhap.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotalNPLNhap.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotalNPLNhap.Size = new System.Drawing.Size(1084, 415);
            this.dgListTotalNPLNhap.TabIndex = 5;
            this.dgListTotalNPLNhap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpTotalNPLXuat
            // 
            this.tpTotalNPLXuat.Controls.Add(this.dgListTotalNPLXuat);
            this.tpTotalNPLXuat.Location = new System.Drawing.Point(4, 22);
            this.tpTotalNPLXuat.Name = "tpTotalNPLXuat";
            this.tpTotalNPLXuat.Size = new System.Drawing.Size(447, 109);
            this.tpTotalNPLXuat.TabIndex = 8;
            this.tpTotalNPLXuat.Text = "TỔNG NGUYÊN PHỤ LIỆU XUẤT";
            this.tpTotalNPLXuat.UseVisualStyleBackColor = true;
            // 
            // dgListTotalNPLXuat
            // 
            this.dgListTotalNPLXuat.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotalNPLXuat.AlternatingColors = true;
            this.dgListTotalNPLXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotalNPLXuat.ColumnAutoResize = true;
            this.dgListTotalNPLXuat.ContextMenuStrip = this.mnuExportExcel;
            dgListTotalNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgListTotalNPLXuat_DesignTimeLayout.LayoutString");
            this.dgListTotalNPLXuat.DesignTimeLayout = dgListTotalNPLXuat_DesignTimeLayout;
            this.dgListTotalNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotalNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotalNPLXuat.FrozenColumns = 3;
            this.dgListTotalNPLXuat.GroupByBoxVisible = false;
            this.dgListTotalNPLXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalNPLXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotalNPLXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotalNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgListTotalNPLXuat.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotalNPLXuat.Name = "dgListTotalNPLXuat";
            this.dgListTotalNPLXuat.RecordNavigator = true;
            this.dgListTotalNPLXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotalNPLXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotalNPLXuat.Size = new System.Drawing.Size(447, 109);
            this.dgListTotalNPLXuat.TabIndex = 6;
            this.dgListTotalNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpTKN
            // 
            this.tpTKN.Controls.Add(this.dgListTKN);
            this.tpTKN.Location = new System.Drawing.Point(4, 22);
            this.tpTKN.Margin = new System.Windows.Forms.Padding(0);
            this.tpTKN.Name = "tpTKN";
            this.tpTKN.Size = new System.Drawing.Size(447, 109);
            this.tpTKN.TabIndex = 0;
            this.tpTKN.Text = "DANH SÁCH TỜ KHAI NHẬP";
            this.tpTKN.UseVisualStyleBackColor = true;
            // 
            // dgListTKN
            // 
            this.dgListTKN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKN.AlternatingColors = true;
            this.dgListTKN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTKN.ColumnAutoResize = true;
            dgListTKN_DesignTimeLayout.LayoutString = resources.GetString("dgListTKN_DesignTimeLayout.LayoutString");
            this.dgListTKN.DesignTimeLayout = dgListTKN_DesignTimeLayout;
            this.dgListTKN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKN.FrozenColumns = 3;
            this.dgListTKN.GroupByBoxVisible = false;
            this.dgListTKN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKN.Location = new System.Drawing.Point(0, 0);
            this.dgListTKN.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTKN.Name = "dgListTKN";
            this.dgListTKN.RecordNavigator = true;
            this.dgListTKN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKN.Size = new System.Drawing.Size(447, 109);
            this.dgListTKN.TabIndex = 2;
            this.dgListTKN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpTKX
            // 
            this.tpTKX.Controls.Add(this.dgListTKX);
            this.tpTKX.Location = new System.Drawing.Point(4, 22);
            this.tpTKX.Margin = new System.Windows.Forms.Padding(0);
            this.tpTKX.Name = "tpTKX";
            this.tpTKX.Size = new System.Drawing.Size(447, 109);
            this.tpTKX.TabIndex = 1;
            this.tpTKX.Text = "DANH SÁCH TỜ KHAI XUẤT";
            this.tpTKX.UseVisualStyleBackColor = true;
            // 
            // dgListTKX
            // 
            this.dgListTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKX.AlternatingColors = true;
            this.dgListTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTKX.ColumnAutoResize = true;
            this.dgListTKX.ContextMenuStrip = this.mnuExportExcel;
            dgListTKX_DesignTimeLayout.LayoutString = resources.GetString("dgListTKX_DesignTimeLayout.LayoutString");
            this.dgListTKX.DesignTimeLayout = dgListTKX_DesignTimeLayout;
            this.dgListTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKX.FrozenColumns = 3;
            this.dgListTKX.GroupByBoxVisible = false;
            this.dgListTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKX.Location = new System.Drawing.Point(0, 0);
            this.dgListTKX.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTKX.Name = "dgListTKX";
            this.dgListTKX.RecordNavigator = true;
            this.dgListTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKX.Size = new System.Drawing.Size(447, 109);
            this.dgListTKX.TabIndex = 3;
            this.dgListTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpNPLNhap
            // 
            this.tpNPLNhap.Controls.Add(this.dgListNPL);
            this.tpNPLNhap.Location = new System.Drawing.Point(4, 22);
            this.tpNPLNhap.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLNhap.Name = "tpNPLNhap";
            this.tpNPLNhap.Size = new System.Drawing.Size(447, 109);
            this.tpNPLNhap.TabIndex = 2;
            this.tpNPLNhap.Text = "SỐ LIỆU NGUYÊN PHỤ LIỆU NHẬP TRONG KỲ";
            this.tpNPLNhap.UseVisualStyleBackColor = true;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            this.dgListNPL.ContextMenuStrip = this.mnuExportExcel;
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.FrozenColumns = 3;
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListNPL.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.Size = new System.Drawing.Size(447, 109);
            this.dgListNPL.TabIndex = 4;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPL_LoadingRow);
            // 
            // tpNPLXuat
            // 
            this.tpNPLXuat.Controls.Add(this.dgListDataNPL);
            this.tpNPLXuat.Location = new System.Drawing.Point(4, 22);
            this.tpNPLXuat.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLXuat.Name = "tpNPLXuat";
            this.tpNPLXuat.Size = new System.Drawing.Size(447, 109);
            this.tpNPLXuat.TabIndex = 3;
            this.tpNPLXuat.Text = "SỐ LIỆU NGUYÊN PHỤ LIỆU XUẤT TRONG KỲ";
            this.tpNPLXuat.UseVisualStyleBackColor = true;
            // 
            // dgListDataNPL
            // 
            this.dgListDataNPL.AlternatingColors = true;
            this.dgListDataNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDataNPL.ColumnAutoResize = true;
            this.dgListDataNPL.ContextMenuStrip = this.mnuExportExcel;
            dgListDataNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListDataNPL_DesignTimeLayout.LayoutString");
            this.dgListDataNPL.DesignTimeLayout = dgListDataNPL_DesignTimeLayout;
            this.dgListDataNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDataNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDataNPL.FrozenColumns = 3;
            this.dgListDataNPL.GroupByBoxVisible = false;
            this.dgListDataNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDataNPL.Location = new System.Drawing.Point(0, 0);
            this.dgListDataNPL.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDataNPL.Name = "dgListDataNPL";
            this.dgListDataNPL.RecordNavigator = true;
            this.dgListDataNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDataNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDataNPL.Size = new System.Drawing.Size(447, 109);
            this.dgListDataNPL.TabIndex = 5;
            this.dgListDataNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpSPXuat
            // 
            this.tpSPXuat.Controls.Add(this.dgListDataSPXuat);
            this.tpSPXuat.Location = new System.Drawing.Point(4, 22);
            this.tpSPXuat.Name = "tpSPXuat";
            this.tpSPXuat.Size = new System.Drawing.Size(447, 109);
            this.tpSPXuat.TabIndex = 6;
            this.tpSPXuat.Text = "SỐ LIỆU SẢN PHẨM XUẤT TRONG KỲ";
            this.tpSPXuat.UseVisualStyleBackColor = true;
            // 
            // dgListDataSPXuat
            // 
            this.dgListDataSPXuat.AlternatingColors = true;
            this.dgListDataSPXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDataSPXuat.ColumnAutoResize = true;
            this.dgListDataSPXuat.ContextMenuStrip = this.mnuExportExcel;
            dgListDataSPXuat_DesignTimeLayout.LayoutString = resources.GetString("dgListDataSPXuat_DesignTimeLayout.LayoutString");
            this.dgListDataSPXuat.DesignTimeLayout = dgListDataSPXuat_DesignTimeLayout;
            this.dgListDataSPXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDataSPXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDataSPXuat.FrozenColumns = 3;
            this.dgListDataSPXuat.GroupByBoxVisible = false;
            this.dgListDataSPXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDataSPXuat.Location = new System.Drawing.Point(0, 0);
            this.dgListDataSPXuat.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDataSPXuat.Name = "dgListDataSPXuat";
            this.dgListDataSPXuat.RecordNavigator = true;
            this.dgListDataSPXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDataSPXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDataSPXuat.Size = new System.Drawing.Size(447, 109);
            this.dgListDataSPXuat.TabIndex = 8;
            this.dgListDataSPXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpNPLQT
            // 
            this.tpNPLQT.Controls.Add(this.dgListDataNPLQT);
            this.tpNPLQT.Location = new System.Drawing.Point(4, 22);
            this.tpNPLQT.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLQT.Name = "tpNPLQT";
            this.tpNPLQT.Size = new System.Drawing.Size(447, 109);
            this.tpNPLQT.TabIndex = 4;
            this.tpNPLQT.Text = "TỔNG HỢP SỐ LIỆU NGUYÊN PHỤ LIÊU QUYẾT TOÁN";
            this.tpNPLQT.UseVisualStyleBackColor = true;
            // 
            // dgListDataNPLQT
            // 
            this.dgListDataNPLQT.AlternatingColors = true;
            this.dgListDataNPLQT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDataNPLQT.ColumnAutoResize = true;
            this.dgListDataNPLQT.ContextMenuStrip = this.mnuExportExcel;
            dgListDataNPLQT_DesignTimeLayout.LayoutString = resources.GetString("dgListDataNPLQT_DesignTimeLayout.LayoutString");
            this.dgListDataNPLQT.DesignTimeLayout = dgListDataNPLQT_DesignTimeLayout;
            this.dgListDataNPLQT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDataNPLQT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDataNPLQT.FrozenColumns = 3;
            this.dgListDataNPLQT.GroupByBoxVisible = false;
            this.dgListDataNPLQT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataNPLQT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataNPLQT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDataNPLQT.Location = new System.Drawing.Point(0, 0);
            this.dgListDataNPLQT.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDataNPLQT.Name = "dgListDataNPLQT";
            this.dgListDataNPLQT.RecordNavigator = true;
            this.dgListDataNPLQT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDataNPLQT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDataNPLQT.Size = new System.Drawing.Size(447, 109);
            this.dgListDataNPLQT.TabIndex = 6;
            this.dgListDataNPLQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpSPQT
            // 
            this.tpSPQT.Controls.Add(this.dgListDataSPQT);
            this.tpSPQT.Location = new System.Drawing.Point(4, 22);
            this.tpSPQT.Name = "tpSPQT";
            this.tpSPQT.Size = new System.Drawing.Size(447, 109);
            this.tpSPQT.TabIndex = 5;
            this.tpSPQT.Text = "TỔNG HỢP SỐ LIỆU SẢN PHẨM QUYẾT TOÁN";
            this.tpSPQT.UseVisualStyleBackColor = true;
            // 
            // dgListDataSPQT
            // 
            this.dgListDataSPQT.AlternatingColors = true;
            this.dgListDataSPQT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDataSPQT.ColumnAutoResize = true;
            this.dgListDataSPQT.ContextMenuStrip = this.mnuExportExcel;
            dgListDataSPQT_DesignTimeLayout.LayoutString = resources.GetString("dgListDataSPQT_DesignTimeLayout.LayoutString");
            this.dgListDataSPQT.DesignTimeLayout = dgListDataSPQT_DesignTimeLayout;
            this.dgListDataSPQT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDataSPQT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDataSPQT.FrozenColumns = 3;
            this.dgListDataSPQT.GroupByBoxVisible = false;
            this.dgListDataSPQT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPQT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDataSPQT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDataSPQT.Location = new System.Drawing.Point(0, 0);
            this.dgListDataSPQT.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDataSPQT.Name = "dgListDataSPQT";
            this.dgListDataSPQT.RecordNavigator = true;
            this.dgListDataSPQT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDataSPQT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDataSPQT.Size = new System.Drawing.Size(447, 109);
            this.dgListDataSPQT.TabIndex = 7;
            this.dgListDataSPQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // mnuImportData
            // 
            this.mnuImportData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.mnuImportData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuImportDataNumber});
            this.mnuImportData.Name = "mnuImportData";
            this.mnuImportData.Size = new System.Drawing.Size(222, 26);
            // 
            // mnuImportDataNumber
            // 
            this.mnuImportDataNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.mnuImportDataNumber.ForeColor = System.Drawing.Color.White;
            this.mnuImportDataNumber.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.mnuImportDataNumber.Name = "mnuImportDataNumber";
            this.mnuImportDataNumber.Size = new System.Drawing.Size(221, 22);
            this.mnuImportDataNumber.Text = "NHẬP LƯỢNG TỒN ĐẦU KỲ";
            this.mnuImportDataNumber.Click += new System.EventHandler(this.mnuImportDataNumber_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // XuLyQuyetToanHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 501);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "XuLyQuyetToanHopDong";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quyết toán hợp đồng gia công";
            this.Load += new System.EventHandler(this.XuLyQuyetToanHopDong_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.mnuExportExcel.ResumeLayout(false);
            this.tpTotalNPLNhap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalNPLNhap)).EndInit();
            this.tpTotalNPLXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotalNPLXuat)).EndInit();
            this.tpTKN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKN)).EndInit();
            this.tpTKX.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKX)).EndInit();
            this.tpNPLNhap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.tpNPLXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataNPL)).EndInit();
            this.tpSPXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPXuat)).EndInit();
            this.tpNPLQT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataNPLQT)).EndInit();
            this.tpSPQT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDataSPQT)).EndInit();
            this.mnuImportData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpTKN;
        private System.Windows.Forms.TabPage tpTKX;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdate;
        private Janus.Windows.GridEX.GridEX dgListTKN;
        private System.Windows.Forms.TabPage tpNPLNhap;
        private Janus.Windows.GridEX.GridEX dgListTKX;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnProcessDataSP;
        private System.Windows.Forms.Button btnProcessDataNPL;
        private System.Windows.Forms.TabPage tpNPLXuat;
        private Janus.Windows.GridEX.GridEX dgListDataNPL;
        private System.Windows.Forms.TabPage tpNPLQT;
        private Janus.Windows.GridEX.GridEX dgListDataNPLQT;
        private System.Windows.Forms.Button btnPrintReportNumber;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPrintReportValue;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ContextMenuStrip mnuImportData;
        private System.Windows.Forms.ToolStripMenuItem mnuImportDataNumber;
        private System.Windows.Forms.TabPage tpSPQT;
        private Janus.Windows.GridEX.GridEX dgListDataSPQT;
        private System.Windows.Forms.TabPage tpSPXuat;
        private Janus.Windows.GridEX.GridEX dgListDataSPXuat;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ContextMenuStrip mnuExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnExportExcel;
        private System.Windows.Forms.TabPage tpTotalNPLNhap;
        private System.Windows.Forms.TabPage tpTotalNPLXuat;
        private Janus.Windows.GridEX.GridEX dgListTotalNPLNhap;
        private Janus.Windows.GridEX.GridEX dgListTotalNPLXuat;
    }
}