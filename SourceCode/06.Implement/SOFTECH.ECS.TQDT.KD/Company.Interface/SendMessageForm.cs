using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using System;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.ServiceModel;
using System.Threading;
using System.Collections.Generic;
namespace Company.Interface
{

    public enum CommandStatus
    {
        Send = 0,
        FeedBack,
        Cancel
    }
    interface ICommand
    {
        void Send();
    }
    class Command : ICommand
    {
        private SendMessageForm _sendForm = null;
        private string _message = string.Empty;
        public Command(SendMessageForm sendForm, string message)
        {
            _sendForm = sendForm;
            _message = message;
        }
        public void Send()
        {
            _sendForm.DoSend(_message);
        }
    }
    public class SendCommand
    {
        public SendMessageForm SendMessageForm { get; set; }
        private Dictionary<CommandStatus, ICommand> _commands = new Dictionary<CommandStatus, ICommand>();
        public void Add(CommandStatus status, string message)
        {
            Remove(status);
            ICommand cmd = new Command(SendMessageForm, message);
            _commands.Add(status, cmd);
        }
        public void Clear()
        {
            _commands.Clear();
        }
        public void Remove(CommandStatus status)
        {
            _commands.Remove(status);
        }
        public void Send(CommandStatus status)
        {
            ICommand cmd = _commands[status];
            cmd.Send();
        }
    }

    public class SendMessageForm : XtraForm
    {

        private Font _boldFont;
        private string _caption;
        private Font _font;
        private PictureBox _pic;
        private string _title;
        public string Caption
        {

            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(Refresh));
                }
            }
        }



        public override bool AllowFormSkin
        {
            get
            {
                return false;
            }
        }

        public SendMessageForm()
            : this(string.Empty)
        {

        }

        public SendMessageForm(string caption)
            : this(caption, string.Empty)
        {
        }

        public SendMessageForm(string caption, string title)
            : this(caption, title, new Size(260, 50), null)
        {
        }

        public SendMessageForm(string caption, Size size)
            : this(caption, string.Empty, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size)
            : this(caption, title, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size, Form parent)
        {
            this._caption = string.Empty;
            this._title = string.Empty;
            _boldFont = new Font("Arial", 9.0F, FontStyle.Bold);
            _font = new Font("Arial", 9.0F);
            this._caption = caption;
            this._title = title == string.Empty ? "Đang thực hiện. Vui lòng chờ giây lát." : title;
            _pic = new PictureBox();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            ControlBox = false;
            ClientSize = size;
            if (parent == null)
            {
                StartPosition = FormStartPosition.CenterScreen;
            }
            else
            {
                StartPosition = FormStartPosition.Manual;
                Left = parent.Left + ((parent.Width - Width) / 2);
                Top = parent.Top + ((parent.Height - Height) / 2);
            }
            ShowInTaskbar = false;
            TopMost = false;
            Paint += new PaintEventHandler(WaitDialogPaint);
            _pic.Size = new Size(16, 16);
            Size size1 = ClientSize;
            _pic.Location = new Point(8, (size1.Height / 2) - 16);
            if (System.IO.File.Exists(@"Image/Wait.gif"))
                _pic.Image = Image.FromFile(@"Image/Wait.gif");
            Controls.Add(_pic);
            Refresh();
        }
        public string GetCaption()
        {
            return Caption;
        }

        public void SetCaption(string newCaption)
        {
            Caption = newCaption;
        }

        private void WaitDialogPaint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = e.ClipRectangle;
            rectangle.Inflate(-1, -1);
            GraphicsCache graphicsCache = new GraphicsCache(e);
            using (StringFormat stringFormat = new StringFormat())
            {
                Brush brush = graphicsCache.GetSolidBrush(LookAndFeelHelper.GetSystemColor(LookAndFeel, SystemColors.WindowText));
                StringAlignment stringAlignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment = stringAlignment;
                stringFormat.Trimming = StringTrimming.EllipsisCharacter;
                if (LookAndFeel.ActiveLookAndFeel.ActiveStyle == ActiveLookAndFeelStyle.Skin)
                    ObjectPainter.DrawObject(graphicsCache, new SkinTextBorderPainter(LookAndFeel), new BorderObjectInfoArgs(null, rectangle, null));
                else
                    ControlPaint.DrawBorder3D(e.Graphics, rectangle, Border3DStyle.RaisedInner);
                rectangle.X += 30;
                rectangle.Width -= 30;
                rectangle.Height /= 3;
                rectangle.Y += rectangle.Height / 2;
                e.Graphics.DrawString(_title, _boldFont, brush, rectangle, stringFormat);
                rectangle.Y += rectangle.Height;
                e.Graphics.DrawString(_caption, _font, brush, rectangle, stringFormat);
                graphicsCache.Dispose();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _pic.Image = null;
            _boldFont = null;
            _font = null;
            base.OnClosing(e);
        }
        #region Process Send Message
        private string _messageSend = string.Empty;
        private string _password = GlobalSettings.PassWordDT;
        public void DoSend(string message)
        {
            this._messageSend = message;
            if (!GlobalSettings.IsRemember)
            {

                if (string.IsNullOrEmpty(GlobalSettings.PassWordDT))
                {
                    WSForm wsForm = new WSForm();
                    wsForm.ShowDialog(this.Parent);
                    if (!wsForm.IsReady) return;
                    _password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                }
            }
            ThreadPool.QueueUserWorkItem(DoWork);
            ShowDialog();
            //else
            //    DoWork(this);
        }
        public Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient Service { get; set; }
        public event EventHandler<SendEventArgs> Send;
        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private void DoWork(object obj)
        {
            DialogResult result = DialogResult.Cancel;
            try
            {
                if (string.IsNullOrEmpty(_messageSend))
                    throw new ArgumentNullException("Thông tin gửi không thể rỗng");

                string sfmtFeedback = string.Empty;
                Service = WebService.GetCisService();

                Helpers.DoAction<Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient>(Service, client =>
                {
                    sfmtFeedback = client.TestWebservie();
                });
                SetCaption("Đã đóng gói dữ liệu");
                Thread.Sleep(500);
                int count = Company.KDT.SHARE.Components.Globals.Delay;
                while (count > 0)
                {
                    SetCaption(string.Format("Còn lại {0} giây", count));
                    Thread.Sleep(1000);
                    count--;
                }
                SetCaption(sfmtFeedback);
                Thread.Sleep(500);
                SetCaption("Đang truyền dữ liệu lên hải quan");
                this._lastStartTime = DateTime.Now;
                Helpers.DoAction<Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient>(Service, client =>
                {
                    sfmtFeedback = client.Send(_messageSend, GlobalSettings.UserId, _password);
                });
                SetCaption("Truyền dữ liệu thành công");
                this.OnSend(new SendEventArgs(sfmtFeedback, this.TotalUsedTime, null));
                result = DialogResult.OK;
            }
            catch (Exception ex)
            {
                SetCaption("Lỗi truyền dữ liệu");
                OnSend(new SendEventArgs(
                    string.Empty, this.TotalUsedTime, ex));
                Company.KDT.SHARE.Components.Globals.Message2File(_messageSend);

            }
            finally
            {
                this.DialogResult = result;
            }
        }

        private void OnSend(SendEventArgs e)
        {
            if (Send != null)
            {
                Send(this, e);
            }
        }
        #endregion Process Send Message

    }

}

