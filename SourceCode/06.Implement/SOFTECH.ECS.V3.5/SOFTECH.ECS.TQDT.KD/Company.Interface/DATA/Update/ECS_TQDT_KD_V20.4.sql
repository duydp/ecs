IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'58/2003/NĐ-CP' ,Notes=N'Quy định về kiểm soát NK, XK, vận chuyển quá cảnh lãnh thổ VN chất ma túy, tiền chất, thuôc gây nghiện, hướng thần' WHERE ReferenceDB='A519' AND Code='WA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WA',N'58/2003/NĐ-CP',N'Quy định về kiểm soát NK, XK, vận chuyển quá cảnh lãnh thổ VN chất ma túy, tiền chất, thuôc gây nghiện, hướng thần')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WY')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'38/2014/NĐ-CP' ,Notes=N'Về quản lý hóa chất thuộc diện kiểm soát của công ước cấm phát triển, sản xuất, tàng trữ, sử dụng và phá hủy vũ khí hóa học' WHERE ReferenceDB='A519' AND Code='WY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WY',N'38/2014/NĐ-CP',N'Về quản lý hóa chất thuộc diện kiểm soát của công ước cấm phát triển, sản xuất, tàng trữ, sử dụng và phá hủy vũ khí hóa học')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'32/2006/NĐ-CP' ,Notes=N'Quản lý động vật, thực vật rừng nguy cấp, quý hiếm ' WHERE ReferenceDB='A519' AND Code='WC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WC',N'32/2006/NĐ-CP',N'Quản lý động vật, thực vật rừng nguy cấp, quý hiếm ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'108/2008/NĐ-CP' ,Notes=N'Quy định chi tiết và hướng dẫn thi hành một số điều của Luật Hóa chất' WHERE ReferenceDB='A519' AND Code='WD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WD',N'108/2008/NĐ-CP',N'Quy định chi tiết và hướng dẫn thi hành một số điều của Luật Hóa chất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'39/2009/NĐ-CP ' ,Notes=N'Về vật liệu nổ công nghiệp' WHERE ReferenceDB='A519' AND Code='WE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WE',N'39/2009/NĐ-CP ',N'Về vật liệu nổ công nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'109/2010/NĐ-CP' ,Notes=N'Về kinh doanh XK gạo' WHERE ReferenceDB='A519' AND Code='WF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WF',N'109/2010/NĐ-CP',N'Về kinh doanh XK gạo')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'2239/TTg-KTN' ,Notes=N'Vv khai thác, vận chuyển, tàng trữ, tiêu thụ, xuất, nhập khẩu cây cảnh, cây bóng mát, cây cổ thụ từ rừng tự nhiên' WHERE ReferenceDB='A519' AND Code='WG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WG',N'2239/TTg-KTN',N'Vv khai thác, vận chuyển, tàng trữ, tiêu thụ, xuất, nhập khẩu cây cảnh, cây bóng mát, cây cổ thụ từ rừng tự nhiên')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'26/2011/NĐ-CP' ,Notes=N'Sửa đổi, bổ sung một số điều của NĐ108 quy định chi tiết và hướng dẫn thi hành một số điều của Luật Hóa chất' WHERE ReferenceDB='A519' AND Code='WH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WH',N'26/2011/NĐ-CP',N'Sửa đổi, bổ sung một số điều của NĐ108 quy định chi tiết và hướng dẫn thi hành một số điều của Luật Hóa chất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'24/2012/NĐ-CP' ,Notes=N'Về quản lý kinh doanh vàng' WHERE ReferenceDB='A519' AND Code='WJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WJ',N'24/2012/NĐ-CP',N'Về quản lý kinh doanh vàng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'38/2012/NĐ-CP' ,Notes=N'Quy định chi tiết Luật ATTP quy định chức năng của 3 Bộ quản lý ATTP theo nhóm hàng (các Bộ sẽ ban hành Danh mục hh thay thế các văn bản nêu trên)' WHERE ReferenceDB='A519' AND Code='WK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WK',N'38/2012/NĐ-CP',N'Quy định chi tiết Luật ATTP quy định chức năng của 3 Bộ quản lý ATTP theo nhóm hàng (các Bộ sẽ ban hành Danh mục hh thay thế các văn bản nêu trên)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'39/2012/QĐ-TTg' ,Notes=N'Quyết định ban hành quy chế quản lý cây cảnh, cây bóng mát, cây cổ thụ' WHERE ReferenceDB='A519' AND Code='WL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WL',N'39/2012/QĐ-TTg',N'Quyết định ban hành quy chế quản lý cây cảnh, cây bóng mát, cây cổ thụ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'94/2012/NĐ-CP' ,Notes=N'Quy định về sản xuất kinh doanh rượu' WHERE ReferenceDB='A519' AND Code='WM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WM',N'94/2012/NĐ-CP',N'Quy định về sản xuất kinh doanh rượu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'QĐ 11/2013/QĐ-TTg' ,Notes=N'Cấm XNK, mua bán mẫu vật một số loài động vật hoang dã thuộc các Phụ lục công ước quốc tế về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp' WHERE ReferenceDB='A519' AND Code='WN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WN',N'QĐ 11/2013/QĐ-TTg',N'Cấm XNK, mua bán mẫu vật một số loài động vật hoang dã thuộc các Phụ lục công ước quốc tế về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'67/2013/NĐ-CP' ,Notes=N'Quy định chi tiết một số điều về Luật thi hành phòng, chống tác hại của thuốc lá và kinh doanh thuốc lá' WHERE ReferenceDB='A519' AND Code='WP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WP',N'67/2013/NĐ-CP',N'Quy định chi tiết một số điều về Luật thi hành phòng, chống tác hại của thuốc lá và kinh doanh thuốc lá')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WQ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'82/2013/NĐ-CP' ,Notes=N'Danh mục chất ma túy và tiền chất' WHERE ReferenceDB='A519' AND Code='WQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WQ',N'82/2013/NĐ-CP',N'Danh mục chất ma túy và tiền chất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WR')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'17/CT-TTg' ,Notes=N'Về việc tăng cường quản lý, kiểm soát việc nhập khẩu công nghệ, máy móc thiết bị của doanh nghiệp' WHERE ReferenceDB='A519' AND Code='WR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WR',N'17/CT-TTg',N'Về việc tăng cường quản lý, kiểm soát việc nhập khẩu công nghệ, máy móc thiết bị của doanh nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WS')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'202/2013/NĐ-CP' ,Notes=N'Về quản lý phân bón' WHERE ReferenceDB='A519' AND Code='WS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WS',N'202/2013/NĐ-CP',N'Về quản lý phân bón')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='WT')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'78/2013/QĐ-TTg' ,Notes=N'Về việc ban hành danh mục và lộ trình phương tiện, thiết bị sử dụng năng lượng phải loại bỏ và các tổ máy phát điện hiệu suất thấp không được xây dựng mới.' WHERE ReferenceDB='A519' AND Code='WT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WT',N'78/2013/QĐ-TTg',N'Về việc ban hành danh mục và lộ trình phương tiện, thiết bị sử dụng năng lượng phải loại bỏ và các tổ máy phát điện hiệu suất thấp không được xây dựng mới.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='YA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'187/2013/NĐ-CP' ,Notes=N'Quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài' WHERE ReferenceDB='A519' AND Code='YA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YA',N'187/2013/NĐ-CP',N'Quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='YB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'60/2014/NĐ-CP' ,Notes=N'Quy định về hoạt động in' WHERE ReferenceDB='A519' AND Code='YB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YB',N'60/2014/NĐ-CP',N'Quy định về hoạt động in')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='YC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'73/2014/QĐ-TTg' ,Notes=N'Quy định Danh mục phế liệu được phép nhập khẩu từ nước ngoài làm nguyên liệu sản xuất' WHERE ReferenceDB='A519' AND Code='YC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YC',N'73/2014/QĐ-TTg',N'Quy định Danh mục phế liệu được phép nhập khẩu từ nước ngoài làm nguyên liệu sản xuất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='YD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'111/2014/NĐ-CP' ,Notes=N'Quy định niên hạn sử dụng của phương tiện thủy nội địa và niên hạn của phương tiện thủy được phép nhập khẩu' WHERE ReferenceDB='A519' AND Code='YD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YD',N'111/2014/NĐ-CP',N'Quy định niên hạn sử dụng của phương tiện thủy nội địa và niên hạn của phương tiện thủy được phép nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='YE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'114/2014/NĐ-CP' ,Notes=N'Quy định đối tượng, điều kiện được phép nhập khẩu phá dỡ tàu biển đã qua sử dụng' WHERE ReferenceDB='A519' AND Code='YE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YE',N'114/2014/NĐ-CP',N'Quy định đối tượng, điều kiện được phép nhập khẩu phá dỡ tàu biển đã qua sử dụng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'05/2006/QĐ - BCN ' ,Notes=N'Công bố danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu theo quy định tại Nghị Định số 12/2006/NĐ-CP ngày 23 tháng 01 năm 2006 của Chính Phủ' WHERE ReferenceDB='A519' AND Code='AA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AA',N'05/2006/QĐ - BCN ',N'Công bố danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu theo quy định tại Nghị Định số 12/2006/NĐ-CP ngày 23 tháng 01 năm 2006 của Chính Phủ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'06/2006/QĐ-BCN' ,Notes=N'V/v Công bố DM hàng cấm nhập khẩu theo quy định tại NĐ số 12/2006/NĐ-CP ngày 23/01/2006 của CP' WHERE ReferenceDB='A519' AND Code='AB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AB',N'06/2006/QĐ-BCN',N'V/v Công bố DM hàng cấm nhập khẩu theo quy định tại NĐ số 12/2006/NĐ-CP ngày 23/01/2006 của CP')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'24/2006/QĐ-BTM' ,Notes=N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại' WHERE ReferenceDB='A519' AND Code='AC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AC',N'24/2006/QĐ-BTM',N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2006/TT-BCN  ' ,Notes=N'Về việc hướng dẫn quản lý xuất khẩu, nhập khẩu hóa chất độc và sản phẩm có hóa chất độc hại, tiền chất ma túy, hóa chất theo tiêu chuẩn kỹ thuật thuộc dạng quản lý chuyên ngành của Bộ Công nghiệp' WHERE ReferenceDB='A519' AND Code='AD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AD',N'01/2006/TT-BCN  ',N'Về việc hướng dẫn quản lý xuất khẩu, nhập khẩu hóa chất độc và sản phẩm có hóa chất độc hại, tiền chất ma túy, hóa chất theo tiêu chuẩn kỹ thuật thuộc dạng quản lý chuyên ngành của Bộ Công nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'40/2006/QĐ-BCN ' ,Notes=N'Bổ sung Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu (Ban hành kèm theo QĐ số 05/2006/QĐ-BCN ngày 7/4/2006 của Bộ CN về việc công bố DM hoá chất cấm xuất khẩu cấm nhập khẩu)' WHERE ReferenceDB='A519' AND Code='AE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AE',N'40/2006/QĐ-BCN ',N'Bổ sung Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu (Ban hành kèm theo QĐ số 05/2006/QĐ-BCN ngày 7/4/2006 của Bộ CN về việc công bố DM hoá chất cấm xuất khẩu cấm nhập khẩu)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'06/2007/TT- BTM ' ,Notes=N'Hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên' WHERE ReferenceDB='A519' AND Code='AF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AF',N'06/2007/TT- BTM ',N'Hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'23/2009/TT-BCT' ,Notes=N'Quy định chi tiết một số điều của Nghị định số 39/2009/NĐ-CP ngày 23 tháng 4 năm 2009 của Chính phủ về vật liệu nổ công nghiệp' WHERE ReferenceDB='A519' AND Code='AG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AG',N'23/2009/TT-BCT',N'Quy định chi tiết một số điều của Nghị định số 39/2009/NĐ-CP ngày 23 tháng 4 năm 2009 của Chính phủ về vật liệu nổ công nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'20/2011/TT-BCT' ,Notes=N'Quy định bổ sung thủ tục nhập khẩu xe ô tô chở người loại từ 09 chỗ ngồi trở xuống' WHERE ReferenceDB='A519' AND Code='AH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AH',N'20/2011/TT-BCT',N'Quy định bổ sung thủ tục nhập khẩu xe ô tô chở người loại từ 09 chỗ ngồi trở xuống')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'23/2012/TT-BCT' ,Notes=N'Quy định việc áp dụng chế độ cấp giấy phép nhập  khẩu tự động đối với một số sản phẩm thép ' WHERE ReferenceDB='A519' AND Code='AJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AJ',N'23/2012/TT-BCT',N'Quy định việc áp dụng chế độ cấp giấy phép nhập  khẩu tự động đối với một số sản phẩm thép ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'26/2012/TT-BCT ' ,Notes=N'v/v sửa đổi Thông tư 23/2009/TT-BCT hướng dẫn Nghị định 39/2009/NĐ-CP về vật liệu nổ công nghiệp' WHERE ReferenceDB='A519' AND Code='AK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AK',N'26/2012/TT-BCT ',N'v/v sửa đổi Thông tư 23/2009/TT-BCT hướng dẫn Nghị định 39/2009/NĐ-CP về vật liệu nổ công nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'41/2012/TT-BCT' ,Notes=N'Quy định về xuất khẩu khoáng sản' WHERE ReferenceDB='A519' AND Code='AL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AL',N'41/2012/TT-BCT',N'Quy định về xuất khẩu khoáng sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'15/2013/TT-BCT' ,Notes=N'Hướng dẫn xuất khẩu than' WHERE ReferenceDB='A519' AND Code='AM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AM',N'15/2013/TT-BCT',N'Hướng dẫn xuất khẩu than')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'49/2014/TT-BCT' ,Notes=N'Quy định việc nhập khẩu thuốc lá nguyên liệu nhập khẩu theo hạn ngạch thuế quan năm 2014' WHERE ReferenceDB='A519' AND Code='BJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BJ',N'49/2014/TT-BCT',N'Quy định việc nhập khẩu thuốc lá nguyên liệu nhập khẩu theo hạn ngạch thuế quan năm 2014')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'37/2013/TT-BCT' ,Notes=N'Quy định nhập khẩu thuốc là điếu, xì gà' WHERE ReferenceDB='A519' AND Code='AP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AP',N'37/2013/TT-BCT',N'Quy định nhập khẩu thuốc là điếu, xì gà')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AQ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'04/VBHN-BCT' ,Notes=N'Văn bản hợp nhất số 04/VBHN-BCT ngày 23/01/2014 của Bộ Công thương về việc hợp nhất Thông tư hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên' WHERE ReferenceDB='A519' AND Code='AQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AQ',N'04/VBHN-BCT',N'Văn bản hợp nhất số 04/VBHN-BCT ngày 23/01/2014 của Bộ Công thương về việc hợp nhất Thông tư hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AR')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'05/VBHN-BCT' ,Notes=N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại' WHERE ReferenceDB='A519' AND Code='AR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AR',N'05/VBHN-BCT',N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AS')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'04/2014/TT-BCT' ,Notes=N'Hướng dẫn một số nội dung tại NĐ 187/2013/NĐ-CP của CP quy định chi tiết thi hành Luật TM về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài' WHERE ReferenceDB='A519' AND Code='AS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AS',N'04/2014/TT-BCT',N'Hướng dẫn một số nội dung tại NĐ 187/2013/NĐ-CP của CP quy định chi tiết thi hành Luật TM về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AT')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'05/2014/TT-BCT' ,Notes=N'Quy định về hoạt động TNTX, chuyển khẩu hàng hóa' WHERE ReferenceDB='A519' AND Code='AT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AT',N'05/2014/TT-BCT',N'Quy định về hoạt động TNTX, chuyển khẩu hàng hóa')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AV')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'09/2014/TT-BCT' ,Notes=N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2014 và năm 2015 với thuế suất nhập khẩu 0% đối với hàng hóa có xuất xứ từ Campuchia' WHERE ReferenceDB='A519' AND Code='AV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AV',N'09/2014/TT-BCT',N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2014 và năm 2015 với thuế suất nhập khẩu 0% đối với hàng hóa có xuất xứ từ Campuchia')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AY')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'29/2010/TT-BCT' ,Notes=N'Về việc nhập khẩu ô tô chưa qua sử dụng bị đục sửa số khung, máy' WHERE ReferenceDB='A519' AND Code='AY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AY',N'29/2010/TT-BCT',N'Về việc nhập khẩu ô tô chưa qua sử dụng bị đục sửa số khung, máy')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='AZ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'08/2012/TT-BCT ' ,Notes=N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công thương do Bộ trưởng Bộ Công thương ban hành' WHERE ReferenceDB='A519' AND Code='AZ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AZ',N'08/2012/TT-BCT ',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công thương do Bộ trưởng Bộ Công thương ban hành')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'14/2013/TT-BCT' ,Notes=N'Quy định về điều kiện kinh doanh than' WHERE ReferenceDB='A519' AND Code='BA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BA',N'14/2013/TT-BCT',N'Quy định về điều kiện kinh doanh than')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'18/2013/TT-BCT' ,Notes=N'Ban hành quy chuẩn kỹ thuật quốc gia về an toàn chai chứa khí dầu mỏ hoá lỏng bằng thép' WHERE ReferenceDB='A519' AND Code='BB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BB',N'18/2013/TT-BCT',N'Ban hành quy chuẩn kỹ thuật quốc gia về an toàn chai chứa khí dầu mỏ hoá lỏng bằng thép')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'28/2013/TT-BCT' ,Notes=N'Quy định kiểm tra nhà nước về an toàn thực phẩm đối với thực phẩm nhập khẩu thuộc trách nhiệm quản lý của Bộ Công Thương' WHERE ReferenceDB='A519' AND Code='BC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BC',N'28/2013/TT-BCT',N'Quy định kiểm tra nhà nước về an toàn thực phẩm đối với thực phẩm nhập khẩu thuộc trách nhiệm quản lý của Bộ Công Thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'42/2013/TT-BCT' ,Notes=N'Quy định quản lý, kiểm soát tiền chất trong lĩnh vực công nghiệp' WHERE ReferenceDB='A519' AND Code='BD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BD',N'42/2013/TT-BCT',N'Quy định quản lý, kiểm soát tiền chất trong lĩnh vực công nghiệp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'35/2014/TT-BCT' ,Notes=N'Quy định việc áp dụng chế độ cấp giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón' WHERE ReferenceDB='A519' AND Code='BF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BF',N'35/2014/TT-BCT',N'Quy định việc áp dụng chế độ cấp giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'37/2014/TT-BCT' ,Notes=N'Quy định việc tạm ngừng kinh doanh TNTX gỗ trò, gỗ xẻ từ rừng tự nhiên từ Lào và Campuchia' WHERE ReferenceDB='A519' AND Code='BG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BG',N'37/2014/TT-BCT',N'Quy định việc tạm ngừng kinh doanh TNTX gỗ trò, gỗ xẻ từ rừng tự nhiên từ Lào và Campuchia')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'29/2014/TT-BCT' ,Notes=N'Quy định cụ thể và hướng dẫn thực hiện một số điều về phân bón vô cơ, hướng dẫn việc cấp phép sản xuất phân bón vô cơ đồng thời sản xuất phân bón hữu cơ và phân bón khác tại Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón' WHERE ReferenceDB='A519' AND Code='BH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BH',N'29/2014/TT-BCT',N'Quy định cụ thể và hướng dẫn thực hiện một số điều về phân bón vô cơ, hướng dẫn việc cấp phép sản xuất phân bón vô cơ đồng thời sản xuất phân bón hữu cơ và phân bón khác tại Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'02/2015/TT-BCT' ,Notes=N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2015 với thuế suất 0% đối với hàng hóa có xuất xứ từ CHDCND Lào' WHERE ReferenceDB='A519' AND Code='BK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BK',N'02/2015/TT-BCT',N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2015 với thuế suất 0% đối với hàng hóa có xuất xứ từ CHDCND Lào')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2015/TT-BCT' ,Notes=N'Quy định về nguyên tắc điều hành hạn ngạch thuế quan nhập khẩu đối với mặt hàng muối, trứng gia cầm năm 2015' WHERE ReferenceDB='A519' AND Code='BL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BL',N'03/2015/TT-BCT',N'Quy định về nguyên tắc điều hành hạn ngạch thuế quan nhập khẩu đối với mặt hàng muối, trứng gia cầm năm 2015')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'08/2015/TT-BCT' ,Notes=N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2015 mặt hàng đường có xuất xứ từ CHDCND Lào' WHERE ReferenceDB='A519' AND Code='BM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BM',N'08/2015/TT-BCT',N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2015 mặt hàng đường có xuất xứ từ CHDCND Lào')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='BN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'27/2015/TT-BTC' ,Notes=N'Quy định về mức giới hạn và việc kiểm tra hàm lượng formaldehyt và amin thơm chuyển hóa từ thuốc nhuộm azo trong sản phẩm dệt may' WHERE ReferenceDB='A519' AND Code='BN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BN',N'27/2015/TT-BTC',N'Quy định về mức giới hạn và việc kiểm tra hàm lượng formaldehyt và amin thơm chuyển hóa từ thuốc nhuộm azo trong sản phẩm dệt may')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'17/2009/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất, kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DA',N'17/2009/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất, kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'43/2009/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất, kinh doanh và sử dụng ở Việt Nam' WHERE ReferenceDB='A519' AND Code='DB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DB',N'43/2009/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất, kinh doanh và sử dụng ở Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'62/2009/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DC',N'62/2009/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'85/2009/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DD',N'85/2009/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'40/2010/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DE',N'40/2010/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'49/2010/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất, kinh doanh và sử dụng ở Việt Nam' WHERE ReferenceDB='A519' AND Code='DF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DF',N'49/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất, kinh doanh và sử dụng ở Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'65/2010/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh, sử dụng và danh mục thuốc thú y, vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DG',N'65/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh, sử dụng và danh mục thuốc thú y, vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'70/2010/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DH',N'70/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'29/2011/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam ' WHERE ReferenceDB='A519' AND Code='DJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DJ',N'29/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'42/2011/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DK',N'42/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'59/2011/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DL',N'59/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'86/2011/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DM',N'86/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'13/2012/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DN',N'13/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'31/2012/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DP',N'31/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DQ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'45/2012/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DQ',N'45/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DR')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'64/2012/TT-BNNPTNT' ,Notes=N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DR',N'64/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DS')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'38/2008/QĐ-BNNPTNT' ,Notes=N'Danh mục áp mã số HS hàng hoá xuất khẩu, nhập khẩu chuyên ngành thủy sản' WHERE ReferenceDB='A519' AND Code='DS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DS',N'38/2008/QĐ-BNNPTNT',N'Danh mục áp mã số HS hàng hoá xuất khẩu, nhập khẩu chuyên ngành thủy sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DU')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'39/2013/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục bổ sung, sửa đổi thức ăn thủy sản; sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam' WHERE ReferenceDB='A519' AND Code='DU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DU',N'39/2013/TT-BNNPTNT',N'Ban hành Danh mục bổ sung, sửa đổi thức ăn thủy sản; sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DW')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'26/2012/TT-BNNPTNT' ,Notes=N'Danh mục tạm thời thức ăn chăn nuôi gia súc gia cầm được phép lưu hành tại Việt nam' WHERE ReferenceDB='A519' AND Code='DW'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DW',N'26/2012/TT-BNNPTNT',N'Danh mục tạm thời thức ăn chăn nuôi gia súc gia cầm được phép lưu hành tại Việt nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'50/2014/TT-BNNPTNT' ,Notes=N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10/10/2011 Quy định chi tiết một số điều Nghị định số 08/2011/NĐ-CP về quản lý thức ăn chăn nuôi' WHERE ReferenceDB='A519' AND Code='FG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FG',N'50/2014/TT-BNNPTNT',N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10/10/2011 Quy định chi tiết một số điều Nghị định số 08/2011/NĐ-CP về quản lý thức ăn chăn nuôi')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DX')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'15/2009/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng ' WHERE ReferenceDB='A519' AND Code='DX'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DX',N'15/2009/TT-BNNPTNT',N'Ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='DY')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'29/2009/TT-BNNPTNT' ,Notes=N'Bổ sung, sửa đổi Thông tư số 15/2009/TT-BNN ngày 17/3/2009 của Bộ trưởng' WHERE ReferenceDB='A519' AND Code='DY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DY',N'29/2009/TT-BNNPTNT',N'Bổ sung, sửa đổi Thông tư số 15/2009/TT-BNN ngày 17/3/2009 của Bộ trưởng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'42/2015/TT-BNNPTNN' ,Notes=N'Bộ Nông nghiệp và PTNT ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng' WHERE ReferenceDB='A519' AND Code='FK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FK',N'42/2015/TT-BNNPTNN',N'Bộ Nông nghiệp và PTNT ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2015/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam ' WHERE ReferenceDB='A519' AND Code='FL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FL',N'03/2015/TT-BNNPTNT',N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'34/2015/TT-BNNPTNT' ,Notes=N'Ban hành danh mục thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam và công bố mã HS đối với thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam' WHERE ReferenceDB='A519' AND Code='FM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FM',N'34/2015/TT-BNNPTNT',N'Ban hành danh mục thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam và công bố mã HS đối với thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'69/2004/QĐ-BNNPTNT' ,Notes=N'Sửa đổi, bổ sung một số nội dung của Thông tư số 03/2015/TT-BNNPTNT ngày 29/1/2015 của Bộ trưởng Bộ Nông nghiệp và phát triển nông thôn về ban hành Danh mục thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam' WHERE ReferenceDB='A519' AND Code='EB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EB',N'69/2004/QĐ-BNNPTNT',N'Sửa đổi, bổ sung một số nội dung của Thông tư số 03/2015/TT-BNNPTNT ngày 29/1/2015 của Bộ trưởng Bộ Nông nghiệp và phát triển nông thôn về ban hành Danh mục thuốc bảo vệ thực vật được phép sử dụng, cấm sử dụng ở Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'79 /2005/QĐ-BNNPTNT' ,Notes=N'Ban hành Danh mục giống cây trồng quý hiếm cấm xuất khẩu' WHERE ReferenceDB='A519' AND Code='EC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EC',N'79 /2005/QĐ-BNNPTNT',N'Ban hành Danh mục giống cây trồng quý hiếm cấm xuất khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2015/TT-BNNPTNT' ,Notes=N'Quy định về trao đổi quốc tế nguồn gen cây trồng quý hiếm' WHERE ReferenceDB='A519' AND Code='FP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FP',N'01/2015/TT-BNNPTNT',N'Quy định về trao đổi quốc tế nguồn gen cây trồng quý hiếm')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='ED')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'78/2004/QĐ-BNNPTNT' ,Notes=N'Ban hành danh mục bổ sung giống cây trồng được phép sản xuất, kinh doanh ở Việt Nam' WHERE ReferenceDB='A519' AND Code='ED'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ED',N'78/2004/QĐ-BNNPTNT',N'Ban hành danh mục bổ sung giống cây trồng được phép sản xuất, kinh doanh ở Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2010/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục giống vật nuôi quý hiếm cấm xuất khẩu  ' WHERE ReferenceDB='A519' AND Code='EE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EE',N'01/2010/TT-BNNPTNT',N'Ban hành Danh mục giống vật nuôi quý hiếm cấm xuất khẩu  ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'06/2012/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục bổ sung giống vật nuôi được phép sản xuất kinh doanh ' WHERE ReferenceDB='A519' AND Code='EH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EH',N'06/2012/TT-BNNPTNT',N'Ban hành Danh mục bổ sung giống vật nuôi được phép sản xuất kinh doanh ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'40/2013/TT-BNNPTNT ' ,Notes=N'Ban hành Danh mục bổ sung nguồn gen vật nuôi quý hiếm cần được bảo tồn  ' WHERE ReferenceDB='A519' AND Code='EK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EK',N'40/2013/TT-BNNPTNT ',N'Ban hành Danh mục bổ sung nguồn gen vật nuôi quý hiếm cần được bảo tồn  ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'25/2012/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục các loài động vật, thực vật hoang dã quy định trong các phụ lục của công ước về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp. ' WHERE ReferenceDB='A519' AND Code='EL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EL',N'25/2012/TT-BNNPTNT',N'Ban hành Danh mục các loài động vật, thực vật hoang dã quy định trong các phụ lục của công ước về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp. ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'28/2013/TT-BNNPTNT' ,Notes=N'Thuốc thú y, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y thủy sản được phép lưu hành tại Việt Nam' WHERE ReferenceDB='A519' AND Code='EM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EM',N'28/2013/TT-BNNPTNT',N'Thuốc thú y, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y thủy sản được phép lưu hành tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'45/2005/QĐ-BNNPTNT' ,Notes=N'Ban hành Danh mục thuốc thú y được phép lưu hành tại Việt Nam; Danh mục vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam' WHERE ReferenceDB='A519' AND Code='EN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EN',N'45/2005/QĐ-BNNPTNT',N'Ban hành Danh mục thuốc thú y được phép lưu hành tại Việt Nam; Danh mục vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'46/2005/QĐ-BNNPTNT' ,Notes=N'Danh mục đối tượng kiểm dịch động vật, sản phẩm động vật; danh mục động vật, sản phẩm động vật thuộc diện phải kiểm dịch' WHERE ReferenceDB='A519' AND Code='EP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EP',N'46/2005/QĐ-BNNPTNT',N'Danh mục đối tượng kiểm dịch động vật, sản phẩm động vật; danh mục động vật, sản phẩm động vật thuộc diện phải kiểm dịch')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EQ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'50/2009/TT-BNNPTNT' ,Notes=N'Danh mục đối tượng kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y bắt buộc áp dụng tiêu chuẩn vệ sinh thú ý' WHERE ReferenceDB='A519' AND Code='EQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EQ',N'50/2009/TT-BNNPTNT',N'Danh mục đối tượng kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y bắt buộc áp dụng tiêu chuẩn vệ sinh thú ý')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='ER')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'13/2011/TT-BNNPTNT' ,Notes=N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ NN & PTNT' WHERE ReferenceDB='A519' AND Code='ER'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ER',N'13/2011/TT-BNNPTNT',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ NN & PTNT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EW')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2012/TT/BNNPTNT' ,Notes=N'Hướng dẫn việc kiểm tra an toàn thực phẩm hàng hóa có nguồn gốc thực vật nhập khẩu' WHERE ReferenceDB='A519' AND Code='EW'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EW',N'01/2012/TT/BNNPTNT',N'Hướng dẫn việc kiểm tra an toàn thực phẩm hàng hóa có nguồn gốc thực vật nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='EX')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'32/2012/TT-BNNPTNT' ,Notes=N'Quy định hồ sơ lâm sản hợp pháp và kiểm tra nguồn gốc  lâm sản' WHERE ReferenceDB='A519' AND Code='EX'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EX',N'32/2012/TT-BNNPTNT',N'Quy định hồ sơ lâm sản hợp pháp và kiểm tra nguồn gốc  lâm sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'66/2011/TT-BNNPTNT' ,Notes=N'Danh mục đối tượng kiểm dịch thủy sản, sản phẩm thủy sản; Danh mục thủy sản, sản phẩm thủy sản thuộc diện phải kiểm dịch' WHERE ReferenceDB='A519' AND Code='FA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FA',N'66/2011/TT-BNNPTNT',N'Danh mục đối tượng kiểm dịch thủy sản, sản phẩm thủy sản; Danh mục thủy sản, sản phẩm thủy sản thuộc diện phải kiểm dịch')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'28/2014/TT-BNNPTNT' ,Notes=N'Quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05/02/2010 của Chính phủ về quản lý thức ăn chăn nuôi' WHERE ReferenceDB='A519' AND Code='FD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FD',N'28/2014/TT-BNNPTNT',N'Quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05/02/2010 của Chính phủ về quản lý thức ăn chăn nuôi')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'34/2014/TT-BNNPTNT' ,Notes=N'Danh mục hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam' WHERE ReferenceDB='A519' AND Code='FE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FE',N'34/2014/TT-BNNPTNT',N'Danh mục hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'30/2014/TT-BNNPTNT' ,Notes=N'Thông tư hướng dẫn kiểm tra chât lượng muối nhập khẩu' WHERE ReferenceDB='A519' AND Code='FF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FF',N'30/2014/TT-BNNPTNT',N'Thông tư hướng dẫn kiểm tra chât lượng muối nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'50/2014/TT-BNNPTNT' ,Notes=N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10 tháng 10 năm 2011 quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05 tháng 02 năm 2010 của Chính phủ về quản lý thức ăn chăn nuôi' WHERE ReferenceDB='A519' AND Code='FG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FG',N'50/2014/TT-BNNPTNT',N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10 tháng 10 năm 2011 quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05 tháng 02 năm 2010 của Chính phủ về quản lý thức ăn chăn nuôi')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'04/2015/TT-BNNPTNT' ,Notes=N'Thông tư hướng dẫn thực hiện một số nội dung của Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý, mua, bán, gia công và quá cảnh hàng hóa với nước ngoài trong lĩnh vực nông nghiệp, lâm nghiệp và thủy sản' WHERE ReferenceDB='A519' AND Code='FH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FH',N'04/2015/TT-BNNPTNT',N'Thông tư hướng dẫn thực hiện một số nội dung của Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý, mua, bán, gia công và quá cảnh hàng hóa với nước ngoài trong lĩnh vực nông nghiệp, lâm nghiệp và thủy sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'41/2014/TT-BNNPTNT' ,Notes=N'Thông tư hướng dẫm một số điều của Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón thuộc trách nhiệm quản lý nhà nước của Bộ Nông nghiệp và phát triển nông thôn' WHERE ReferenceDB='A519' AND Code='FJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FJ',N'41/2014/TT-BNNPTNT',N'Thông tư hướng dẫm một số điều của Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón thuộc trách nhiệm quản lý nhà nước của Bộ Nông nghiệp và phát triển nông thôn')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'42/2015/TT-BNNPTNT' ,Notes=N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam' WHERE ReferenceDB='A519' AND Code='FK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FK',N'42/2015/TT-BNNPTNT',N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='FN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'21/2015/TT-BNNPTNT' ,Notes=N'Quản lý thuốc bảo vệ thực vật' WHERE ReferenceDB='A519' AND Code='FN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FN',N'21/2015/TT-BNNPTNT',N'Quản lý thuốc bảo vệ thực vật')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='TB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'18/2014/TT-NHNN' ,Notes=N'Hướng dẫn hoạt động nhập khẩu hàng hóa thuộc diện quản lý chuyên ngành của Ngân hàng Nhà nước Việt Nam' WHERE ReferenceDB='A519' AND Code='TB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','TB',N'18/2014/TT-NHNN',N'Hướng dẫn hoạt động nhập khẩu hàng hóa thuộc diện quản lý chuyên ngành của Ngân hàng Nhà nước Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='TC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'16/2012/TT-NHNN' ,Notes=N'Hướng dẫn Nghị định 24/2012/NĐ-CP về quản lý hoạt động kinh doanh vàng do Ngân hàng Nhà nước Việt Nam ban hành' WHERE ReferenceDB='A519' AND Code='TC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','TC',N'16/2012/TT-NHNN',N'Hướng dẫn Nghị định 24/2012/NĐ-CP về quản lý hoạt động kinh doanh vàng do Ngân hàng Nhà nước Việt Nam ban hành')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='VA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2010/TT-BLDTBXH' ,Notes=N'Ban hành DM sản phẩm hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ LĐTBXH' WHERE ReferenceDB='A519' AND Code='VA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','VA',N'03/2010/TT-BLDTBXH',N'Ban hành DM sản phẩm hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ LĐTBXH')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'14/2011/TT-BTTTT  ' ,Notes=N'Quy định chi tiết thi hành nghị định số 12/2002/NĐ-CP' WHERE ReferenceDB='A519' AND Code='LA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LA',N'14/2011/TT-BTTTT  ',N'Quy định chi tiết thi hành nghị định số 12/2002/NĐ-CP')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'31/2015/TT-BTTTT' ,Notes=N'Hướng dẫn một số điều của Nghị định 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với hoạt động xuất, nhập khẩu sản phẩm công nghệ thông tin đã qua sử dụng' WHERE ReferenceDB='A519' AND Code='LJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LJ',N'31/2015/TT-BTTTT',N'Hướng dẫn một số điều của Nghị định 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với hoạt động xuất, nhập khẩu sản phẩm công nghệ thông tin đã qua sử dụng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'18/2014/TT-BTTTT' ,Notes=N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc cấp giấy phép nhập khẩu thiết bị phát, thu phát sóng vô tuyến điện' WHERE ReferenceDB='A519' AND Code='LE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LE',N'18/2014/TT-BTTTT',N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc cấp giấy phép nhập khẩu thiết bị phát, thu phát sóng vô tuyến điện')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'26/2014/TT-BTTTT' ,Notes=N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc nhập khẩu tem bưu chính' WHERE ReferenceDB='A519' AND Code='LF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LF',N'26/2014/TT-BTTTT',N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc nhập khẩu tem bưu chính')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'23/2014/TT-BTTTT ' ,Notes=N'Quy định chi tiết và hướng dẫn thi hành một số điều của Luật Xuất bản và Nghị định số 195/2013/NĐ-CP ngày 21/11/2013 của Chính phủ quy định chi tiết một số điều và biện pháp thi hành Luật xuất bản' WHERE ReferenceDB='A519' AND Code='LG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LG',N'23/2014/TT-BTTTT ',N'Quy định chi tiết và hướng dẫn thi hành một số điều của Luật Xuất bản và Nghị định số 195/2013/NĐ-CP ngày 21/11/2013 của Chính phủ quy định chi tiết một số điều và biện pháp thi hành Luật xuất bản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='LH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2015/TT-BTTTT' ,Notes=N'Quy định chi tiết và hướng dẫn thi hành một số điều, khoản của Nghị định số 60/2014/NĐ-CP ngày 19/6/2014 của Chính phủ quy định về hoạt động in' WHERE ReferenceDB='A519' AND Code='LH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LH',N'03/2015/TT-BTTTT',N'Quy định chi tiết và hướng dẫn thi hành một số điều, khoản của Nghị định số 60/2014/NĐ-CP ngày 19/6/2014 của Chính phủ quy định về hoạt động in')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='MA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'QĐ 15/2006/QĐ-BTNMT' ,Notes=N'V/v Ban hành DM thiết bị làm lạnh sử dụng môi chất lạnh CFC cấm nhập khẩu' WHERE ReferenceDB='A519' AND Code='MA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','MA',N'QĐ 15/2006/QĐ-BTNMT',N'V/v Ban hành DM thiết bị làm lạnh sử dụng môi chất lạnh CFC cấm nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='MB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2013/TT-BTNMT' ,Notes=N'Quy định về phế liệu được phép nhập khẩu làm nguyên liệu sản xuất' WHERE ReferenceDB='A519' AND Code='MB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','MB',N'01/2013/TT-BTNMT',N'Quy định về phế liệu được phép nhập khẩu làm nguyên liệu sản xuất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='NA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2012/TT-BXD' ,Notes=N'Vv công bố danh mục và mã số HS vật liệu amiăng thuộc nhóm amfibole cấm nhập khẩu ' WHERE ReferenceDB='A519' AND Code='NA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NA',N'03/2012/TT-BXD',N'Vv công bố danh mục và mã số HS vật liệu amiăng thuộc nhóm amfibole cấm nhập khẩu ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='NB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'11/2009/TT-BXD' ,Notes=N'Quản lý chất lượng kính xây dựng thuộc trách nhiệm quản lý của Bộ Xây dựng' WHERE ReferenceDB='A519' AND Code='NB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NB',N'11/2009/TT-BXD',N'Quản lý chất lượng kính xây dựng thuộc trách nhiệm quản lý của Bộ Xây dựng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='NC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2010/TT-BXD' ,Notes=N'Quản lý chất lượng clanhke xi măng pooc lăng thuộc trách nhiệm của Bộ Xây dựng' WHERE ReferenceDB='A519' AND Code='NC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NC',N'01/2010/TT-BXD',N'Quản lý chất lượng clanhke xi măng pooc lăng thuộc trách nhiệm của Bộ Xây dựng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='ND')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'14/2010/TT-BXD' ,Notes=N'Quản lý chất lượng gạch ốp lát thuộc trách nhiệm quản lý của Bộ Xây dựng.' WHERE ReferenceDB='A519' AND Code='ND'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ND',N'14/2010/TT-BXD',N'Quản lý chất lượng gạch ốp lát thuộc trách nhiệm quản lý của Bộ Xây dựng.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='NE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'04/2012/TT-BXD' ,Notes=N'Hướng dẫn xuất khẩu khoáng sản làm vật liệu xây dựng' WHERE ReferenceDB='A519' AND Code='NE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NE',N'04/2012/TT-BXD',N'Hướng dẫn xuất khẩu khoáng sản làm vật liệu xây dựng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='PC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'28/2014/TT-BVHTTDL' ,Notes=N'Quy định về quản lý hoạt động mua bán hàng hóa quốc tế thuộc diện quản lý chuyên ngành văn hóa của Bộ Văn hóa, Thể thao và Du lịch' WHERE ReferenceDB='A519' AND Code='PC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','PC',N'28/2014/TT-BVHTTDL',N'Quy định về quản lý hoạt động mua bán hàng hóa quốc tế thuộc diện quản lý chuyên ngành văn hóa của Bộ Văn hóa, Thể thao và Du lịch')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='QA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'80/2006/QĐ-BQP ' ,Notes=N'Công bố Danh mục cấm xuất khẩu, cấm nhập khẩu theo QĐ tại NĐ 12/2006/NĐ-CP' WHERE ReferenceDB='A519' AND Code='QA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','QA',N'80/2006/QĐ-BQP ',N'Công bố Danh mục cấm xuất khẩu, cấm nhập khẩu theo QĐ tại NĐ 12/2006/NĐ-CP')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='QB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'22/2013/TT-BQP' ,Notes=N'Quy định về quản lý hoạt động vật liệu nổ công nghiệp trong quân đội' WHERE ReferenceDB='A519' AND Code='QB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','QB',N'22/2013/TT-BQP',N'Quy định về quản lý hoạt động vật liệu nổ công nghiệp trong quân đội')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'1064/2001/QĐ-BYT' ,Notes=N'Công bố 24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại Việt Nam' WHERE ReferenceDB='A519' AND Code='HA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HA',N'1064/2001/QĐ-BYT',N'Công bố 24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'674/QĐ-BYT' ,Notes=N'Về việc ban hành Danh mục vắc xin, sinh phẩm y tế và danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu ' WHERE ReferenceDB='A519' AND Code='HB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HB',N'674/QĐ-BYT',N'Về việc ban hành Danh mục vắc xin, sinh phẩm y tế và danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'41/2007/QĐ-BYT' ,Notes=N'Quyết định ban hành Danh mục thuộc dành cho người và mỹ phẩm nhập khẩu vào VN đã được xác định mã só hàng hóa theo Danh mục hàng hóa xuất khẩu, nhập khẩu và Biểu thuế nhập khẩu ưu đãi' WHERE ReferenceDB='A519' AND Code='HC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HC',N'41/2007/QĐ-BYT',N'Quyết định ban hành Danh mục thuộc dành cho người và mỹ phẩm nhập khẩu vào VN đã được xác định mã só hàng hóa theo Danh mục hàng hóa xuất khẩu, nhập khẩu và Biểu thuế nhập khẩu ưu đãi')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'09/2010/TT-BYT' ,Notes=N'Hướng dẫn việc quản lý chất lượng thuốc' WHERE ReferenceDB='A519' AND Code='HD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HD',N'09/2010/TT-BYT',N'Hướng dẫn việc quản lý chất lượng thuốc')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'47/2010/TT-BYT' ,Notes=N'Hướng dẫn hoạt động xuất khẩu, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc' WHERE ReferenceDB='A519' AND Code='HG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HG',N'47/2010/TT-BYT',N'Hướng dẫn hoạt động xuất khẩu, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'25/2011/TT-BYT' ,Notes=N'Ban hành Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn được phép đăng ký để sử dụng, được phép đăng ký nhưng hạn chế sử dụng và cấm sử dụng trong lĩnh vực gia dụng và y tế tại Việt Nam' WHERE ReferenceDB='A519' AND Code='HJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HJ',N'25/2011/TT-BYT',N'Ban hành Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn được phép đăng ký để sử dụng, được phép đăng ký nhưng hạn chế sử dụng và cấm sử dụng trong lĩnh vực gia dụng và y tế tại Việt Nam')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HK')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'818/2007/QĐ-BYT' ,Notes=N'Danh mục hàng hóa NK phải kiểm tra về VSATTP theo mã số HS' WHERE ReferenceDB='A519' AND Code='HK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HK',N'818/2007/QĐ-BYT',N'Danh mục hàng hóa NK phải kiểm tra về VSATTP theo mã số HS')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HL')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'06/2011/TT-BYT' ,Notes=N'Quy định về quản lý mỹ phẩm' WHERE ReferenceDB='A519' AND Code='HL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HL',N'06/2011/TT-BYT',N'Quy định về quản lý mỹ phẩm')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HM')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'29/2011/TT-BYT' ,Notes=N'Quy định về quản lý hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế' WHERE ReferenceDB='A519' AND Code='HM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HM',N'29/2011/TT-BYT',N'Quy định về quản lý hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HN')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'44/2011/TT-BYT' ,Notes=N'Ban hành Danh mục hàng hóa nhóm 2 thuộc trách nhiệm của Bộ Y tế' WHERE ReferenceDB='A519' AND Code='HN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HN',N'44/2011/TT-BYT',N'Ban hành Danh mục hàng hóa nhóm 2 thuộc trách nhiệm của Bộ Y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HP')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'39/2013/TT-BYT' ,Notes=N'Quy định về quản lý thuốc chữa bệnh cho người theo đường xuất khẩu, nhập khẩu phi mậu dịch' WHERE ReferenceDB='A519' AND Code='HP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HP',N'39/2013/TT-BYT',N'Quy định về quản lý thuốc chữa bệnh cho người theo đường xuất khẩu, nhập khẩu phi mậu dịch')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HQ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'38/2013/TT-BYT' ,Notes=N'Sửa đổi Thông tư 47/2010/TT-BYT hướng dẫn hoạt động xuất, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc ' WHERE ReferenceDB='A519' AND Code='HQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HQ',N'38/2013/TT-BYT',N'Sửa đổi Thông tư 47/2010/TT-BYT hướng dẫn hoạt động xuất, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HR')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'19/2014/TT-BYT' ,Notes=N'Quy định quản lý thuốc gây nghiện, thuốc hướng tâm thần và tiền chất dùng làm thuốc ' WHERE ReferenceDB='A519' AND Code='HR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HR',N'19/2014/TT-BYT',N'Quy định quản lý thuốc gây nghiện, thuốc hướng tâm thần và tiền chất dùng làm thuốc ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HS')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'43/2011/TT-BYT' ,Notes=N'Quy định chế độ quản lý mẫu bệnh phẩm truyền nhiễm' WHERE ReferenceDB='A519' AND Code='HS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HS',N'43/2011/TT-BYT',N'Quy định chế độ quản lý mẫu bệnh phẩm truyền nhiễm')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='HT')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'30/2015/TT-BYT' ,Notes=N'Quy định việc nhập khẩu trang thiết bị y tế' WHERE ReferenceDB='A519' AND Code='HT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HT',N'30/2015/TT-BYT',N'Quy định việc nhập khẩu trang thiết bị y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'19/2006/QĐ-BGTVT ' ,Notes=N'Danh mục hàng hoá nhập khẩu thuộc diện quản lý chuyên ngành theo quy định tại Nghị định số 12/2006/NĐ-CP ' WHERE ReferenceDB='A519' AND Code='KA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KA',N'19/2006/QĐ-BGTVT ',N'Danh mục hàng hoá nhập khẩu thuộc diện quản lý chuyên ngành theo quy định tại Nghị định số 12/2006/NĐ-CP ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'23/2009/TT-BGTVT' ,Notes=N'Thông tư quy định về kiểm tra chất lượng, an toàn kĩ thuật và bảo vệ môi trường xe máy chuyên dùng' WHERE ReferenceDB='A519' AND Code='KB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KB',N'23/2009/TT-BGTVT',N'Thông tư quy định về kiểm tra chất lượng, an toàn kĩ thuật và bảo vệ môi trường xe máy chuyên dùng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KC')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'41/2011/TT-BGTVT' ,Notes=N'Sửa đổi bổ sung một số điều của Thông tư số 23/2009/TT-BGTVT' WHERE ReferenceDB='A519' AND Code='KC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KC',N'41/2011/TT-BGTVT',N'Sửa đổi bổ sung một số điều của Thông tư số 23/2009/TT-BGTVT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'31/2011/TT-BGTVT' ,Notes=N'Quy định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường xe cơ giới nhập khẩu' WHERE ReferenceDB='A519' AND Code='KD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KD',N'31/2011/TT-BGTVT',N'Quy định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường xe cơ giới nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'63/2011/TT-BGTVT' ,Notes=N'Ban hành danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông Vận tải' WHERE ReferenceDB='A519' AND Code='KE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KE',N'63/2011/TT-BGTVT',N'Ban hành danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông Vận tải')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'44/2012/TT BGTVT' ,Notes=N'Quy định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường  xe mô tô, xe gắn máy nhập khẩu và động cơ nhập khẩu sử dụng  để sản xuất, lắp ráp xe mô tô, xe gắn máy' WHERE ReferenceDB='A519' AND Code='KF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KF',N'44/2012/TT BGTVT',N'Quy định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường  xe mô tô, xe gắn máy nhập khẩu và động cơ nhập khẩu sử dụng  để sản xuất, lắp ráp xe mô tô, xe gắn máy')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'41/2013/TT-BGTVT' ,Notes=N'Quy định về kiểm tra chất lượng an toàn kỹ thuật xe đạp điện ' WHERE ReferenceDB='A519' AND Code='KG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KG',N'41/2013/TT-BGTVT',N'Quy định về kiểm tra chất lượng an toàn kỹ thuật xe đạp điện ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'04/2014/TT-BGTVT' ,Notes=N'Quy định điều kiện và thủ tục cấp giấy phép nhập khẩu pháo hiệu cho an toàn hàng hải' WHERE ReferenceDB='A519' AND Code='KH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KH',N'04/2014/TT-BGTVT',N'Quy định điều kiện và thủ tục cấp giấy phép nhập khẩu pháo hiệu cho an toàn hàng hải')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='KJ')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'19/2014/TT-BGTVT' ,Notes=N'Thông tư sửa dổi Thông tư 23/2014/TT-BGTVT' WHERE ReferenceDB='A519' AND Code='KJ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KJ',N'19/2014/TT-BGTVT',N'Thông tư sửa dổi Thông tư 23/2014/TT-BGTVT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='RA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2009/TT-BKHCN' ,Notes=N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.' WHERE ReferenceDB='A519' AND Code='RA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','RA',N'01/2009/TT-BKHCN',N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='UA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'14/2012/TT-BCA' ,Notes=N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công an' WHERE ReferenceDB='A519' AND Code='UA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','UA',N'14/2012/TT-BCA',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công an')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='UB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'57/2012/TT-BCA' ,Notes=N'Quy định chi tiết thi hành việc nhập khẩu, quản lý, sử dụng, tiêu hủy mẫu các chất ma túy vì mục đích quốc phòng, an ninh' WHERE ReferenceDB='A519' AND Code='UB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','UB',N'57/2012/TT-BCA',N'Quy định chi tiết thi hành việc nhập khẩu, quản lý, sử dụng, tiêu hủy mẫu các chất ma túy vì mục đích quốc phòng, an ninh')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='SA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'111/2012/TT-BTC ' ,Notes=N'Danh mục hàng hóa và thuế suất thuế nhập khẩu để áp dụng hạn ngạch thuế quan' WHERE ReferenceDB='A519' AND Code='SA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','SA',N'111/2012/TT-BTC ',N'Danh mục hàng hóa và thuế suất thuế nhập khẩu để áp dụng hạn ngạch thuế quan')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XA')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'14/2009/TTLT-BCT-BTC' ,Notes=N'Hướng dẫn việc cấp chứng nhận và thủ tục nhập khẩu, xuất khẩu kim cương thô nhằm thực thi các quy định của quy chế chứng nhận quy trình KIMBERLEY' WHERE ReferenceDB='A519' AND Code='XA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XA',N'14/2009/TTLT-BCT-BTC',N'Hướng dẫn việc cấp chứng nhận và thủ tục nhập khẩu, xuất khẩu kim cương thô nhằm thực thi các quy định của quy chế chứng nhận quy trình KIMBERLEY')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XB')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'01/2012/TTLT-BCT-BTC' ,Notes=N'Sửa đổi bổ sung TT 14/2009/TTLT-BCT-BTC ngày 23/6/2009' WHERE ReferenceDB='A519' AND Code='XB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XB',N'01/2012/TTLT-BCT-BTC',N'Sửa đổi bổ sung TT 14/2009/TTLT-BCT-BTC ngày 23/6/2009')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XD')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'03/2006/TTLT-BTM-BGTVT-BTC-BCA' ,Notes=N'Hướng dẫn việc nhập khẩu ô tô dưới 16 chỗ ngồi đã qua sử dụng theo Nghị định 12/2006/NĐ-CP ngày 23 tháng 01 năm 2006 của Chính phủ' WHERE ReferenceDB='A519' AND Code='XD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XD',N'03/2006/TTLT-BTM-BGTVT-BTC-BCA',N'Hướng dẫn việc nhập khẩu ô tô dưới 16 chỗ ngồi đã qua sử dụng theo Nghị định 12/2006/NĐ-CP ngày 23 tháng 01 năm 2006 của Chính phủ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XE')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'25 /2010 /TTLT-BCT-BGTVT-BTC' ,Notes=N'Quy định việc nhập khẩu ô tô chở người dưới 16 chỗ ngồi, loại mới (chưa qua sử dụng)' WHERE ReferenceDB='A519' AND Code='XE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XE',N'25 /2010 /TTLT-BCT-BGTVT-BTC',N'Quy định việc nhập khẩu ô tô chở người dưới 16 chỗ ngồi, loại mới (chưa qua sử dụng)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XF')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'47/2011/TTLT-BCT-BTNM' ,Notes=N'Quy định việc quản lý nhập khẩu, xuất khẩu, tạm nhập tái xuất các chất làm suy giảm tầng ô zôn theo quy định của nghị định thư montreal vê các chất làm suy giảm o- zon' WHERE ReferenceDB='A519' AND Code='XF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XF',N'47/2011/TTLT-BCT-BTNM',N'Quy định việc quản lý nhập khẩu, xuất khẩu, tạm nhập tái xuất các chất làm suy giảm tầng ô zôn theo quy định của nghị định thư montreal vê các chất làm suy giảm o- zon')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XG')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'06/2013/TTLT-BKHCN-BCA-BGTVT' ,Notes=N'Hướng dẫn việc nhập khẩu mũ bảo hiểm cho người đi xe mô tô, xe gắn máy, xe đạp máy' WHERE ReferenceDB='A519' AND Code='XG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XG',N'06/2013/TTLT-BKHCN-BCA-BGTVT',N'Hướng dẫn việc nhập khẩu mũ bảo hiểm cho người đi xe mô tô, xe gắn máy, xe đạp máy')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A519' AND Code='XH')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'44/2013/TTLT-BCT-BKHCN' ,Notes=N'Quy định về kiểm tra chất lượng thép nhập khẩu' WHERE ReferenceDB='A519' AND Code='XH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XH',N'44/2013/TTLT-BCT-BKHCN',N'Quy định về kiểm tra chất lượng thép nhập khẩu')
END

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.4',GETDATE(), N'Cập nhật mã văn bản pháp quy ')
END	