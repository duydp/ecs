﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.LogMessages;
using System.Collections.Generic;
namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiBoSungHangHoaNhapKhauCQHQ_Page_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public bool IsXacNhanSua;
        public ToKhaiBoSungHangHoaNhapKhauCQHQ_Page_1()
        {
            InitializeComponent();
        }
        List<MsgPhanBo> _ListMsg = new List<MsgPhanBo>();
        public List<MsgPhanBo> MsgCollection
        {
            set { this._ListMsg = value; }
            get { return this._ListMsg; }
        }
        public void BindingReport(VAD2AY0 vad2ay)
        {
            //if(IsKhaiBaoSua== true)
            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
            lblSoTrang.Text = "1/" + vad2ay.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad2ay.ICN.GetValue().ToString().ToUpper();
            lblSoToKhaiDauTien.Text = vad2ay.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad2ay.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad2ay.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad2ay.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad2ay.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad2ay.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad2ay.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad2ay.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad2ay.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad2ay.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad2ay.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad2ay.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad2ay.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";

            }
            if (vad2ay.AD1.GetValue().ToString().ToUpper() != "" && vad2ay.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad2ay.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ay.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ay.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ay.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad2ay.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad2ay.AD3.GetValue().ToString().ToUpper() != "" && vad2ay.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad2ay.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ay.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ay.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ay.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad2ay.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad2ay.AAA.GetValue().ToString().ToUpper();
            lblMaNguoiNhapKhau.Text = vad2ay.IMC.GetValue().ToString().ToUpper();
            lblTenNguoiNhapKhau.Text = vad2ay.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNhapKhau.Text = vad2ay.IMY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiNhapKhau.Text = vad2ay.IMA.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiNhapKhau.Text = vad2ay.IMT.GetValue().ToString().ToUpper();
            lblMaNguoiUyThacNhapKhau.Text = vad2ay.NMC.GetValue().ToString().ToUpper();
            lblTenNguoiUyThacNhapKhau.Text = vad2ay.NMN.GetValue().ToString().ToUpper();
            lblMaNguoiXuatKhau.Text = vad2ay.EPC.GetValue().ToString().ToUpper();
            lblTenNguoiXuatKhau.Text = vad2ay.EPN.GetValue().ToString().ToUpper();
            lblMaBuuChinhXuatKhau.Text = vad2ay.EPY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau1.Text = vad2ay.EPA.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau2.Text = vad2ay.EP2.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau3.Text = vad2ay.EP3.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau4.Text = vad2ay.EP4.GetValue().ToString().ToUpper();
            lblMaNuoc.Text = vad2ay.EPO.GetValue().ToString().ToUpper();
            lblNguoiUyThacXuatKhau.Text = vad2ay.ENM.GetValue().ToString().ToUpper();
            lblMaDaiLyHaiQuan.Text = vad2ay.A37.GetValue().ToString().ToUpper();
            lblTenDaiLyHaiQuan.Text = vad2ay.A38.GetValue().ToString().ToUpper();
            lblMaNhanVienHaiQuan.Text = vad2ay.A39.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.BL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoVanDon1.Text = vad2ay.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoVanDon2.Text = vad2ay.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoVanDon3.Text = vad2ay.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoVanDon4.Text = vad2ay.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoVanDon5.Text = vad2ay.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblSoLuong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad2ay.NO.GetValue().ToString().ToUpper().ToString().Replace(".", ",")));
            lblMaDonViTinh.Text = vad2ay.NOT.GetValue().ToString().ToUpper();
            //string a = vad2ay.GW.GetValue().ToString().Replace(".", ",");
            lblTongTrongLuongHang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad2ay.GW.GetValue().ToString().Replace(".", ",")));
            lblMaDonViTinhTrongLuong.Text = vad2ay.GWT.GetValue().ToString().ToUpper();
            lblSoLuongContainer.Text = vad2ay.COC.GetValue().ToString().ToUpper();
            lblMaDiaDiemLuuKhoHang.Text = vad2ay.ST.GetValue().ToString().ToUpper();
            lblTenDiaDiemLuuKhoHang.Text = vad2ay.A51.GetValue().ToString().ToUpper();
            lblMaDiaDiemDoHang.Text = vad2ay.DST.GetValue().ToString().ToUpper();
            lblTenDiaDiemDoHang.Text = vad2ay.DSN.GetValue().ToString().ToUpper();
            lblMaDiaDiemXepHang.Text = vad2ay.PSC.GetValue().ToString().ToUpper();
            lblTenDiaDiemXepHang.Text = vad2ay.PSN.GetValue().ToString().ToUpper();
            lblMaPhuongTienVanChuyen.Text = vad2ay.VSC.GetValue().ToString().ToUpper();
            lblTenPhuongTienVanChuyen.Text = vad2ay.VSN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.ARR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHangDen.Text = Convert.ToDateTime(vad2ay.ARR.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHangDen.Text = "";
            }
            lblKyHieuSoHieu.Text = vad2ay.MRK.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.ISD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuocPhepNhapKho.Text = Convert.ToDateTime(vad2ay.ISD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuocPhepNhapKho.Text = "";
            }
            for (int i = 0; i < vad2ay.OL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaVanBanPhapQuyKhac1.Text = vad2ay.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaVanBanPhapQuyKhac2.Text = vad2ay.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaVanBanPhapQuyKhac3.Text = vad2ay.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaVanBanPhapQuyKhac4.Text = vad2ay.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaVanBanPhapQuyKhac5.Text = vad2ay.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanLoaiHinhThucHoaDon.Text = vad2ay.IV1.GetValue().ToString().ToUpper();
            lblSoHoaDon.Text = vad2ay.IV3.GetValue().ToString().ToUpper();
            lblSoTiepNhanHoaDonDienTu.Text = vad2ay.IV2.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ay.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayPhatHanh.Text = Convert.ToDateTime(vad2ay.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayPhatHanh.Text = "";
            }
            lblPhuongThucThanhToan.Text = vad2ay.IVP.GetValue().ToString().ToUpper();
            lblMaPhanLoaiGiaHoaDon.Text = vad2ay.IP1.GetValue().ToString().ToUpper();
            lblMaDieuKienGiaHoaDon.Text = vad2ay.IP2.GetValue().ToString().ToUpper();
            lblMaDongTienHoaDon.Text = vad2ay.IP3.GetValue().ToString().ToUpper();
            lblTongTriGiaHoaDon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.IP4.GetValue().ToString().Replace(".", ",")));
            lblTongTriGiaTinhThue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.A86.GetValue().ToString().Replace(".", ",")));
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTongHeSoPhanBoTriGia.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            lblMaPhanLoaiNhapLieu.Text = vad2ay.A97.GetValue().ToString().ToUpper();
            lblMaKetQuaKiemTraNoiDung.Text = vad2ay.N4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiGiayPhepNhapKhau1.Text = vad2ay.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep1.Text = vad2ay.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiGiayPhepNhapKhau2.Text = vad2ay.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep2.Text = vad2ay.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiGiayPhepNhapKhau3.Text = vad2ay.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep3.Text = vad2ay.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblPhanLoaiGiayPhepNhapKhau4.Text = vad2ay.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep4.Text = vad2ay.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblPhanLoaiGiayPhepNhapKhau5.Text = vad2ay.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep5.Text = vad2ay.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblMaPhanLoaiKhaiTriGia.Text = vad2ay.VD1.GetValue().ToString().ToUpper();
            lblSoTiepNhanKhaiTriGiaTongHop.Text = vad2ay.VD2.GetValue().ToString().ToUpper() == "0" ? "" : vad2ay.VD2.GetValue().ToString().ToUpper();
            lblPhanLoaiCongThucChuan.Text = vad2ay.A93.GetValue().ToString().ToUpper();
            lblMaPhanLoaiDieuChinhTriGia.Text = vad2ay.A94.GetValue().ToString().ToUpper();
            lblPhuongPhapDieuChinhTriGia.Text = vad2ay.A95.GetValue().ToString().ToUpper();
            lblMaTienTe.Text = vad2ay.VCC.GetValue().ToString().ToUpper();
            lblGiaCoSo.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VPC.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiPhiVanChuyen.Text = vad2ay.FR1.GetValue().ToString().ToUpper();
            lblMaTienTePhiVanChuyen.Text = vad2ay.FR2.GetValue().ToString().ToUpper();
            lblPhiVanChuyen.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.FR3.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiBaoHiem.Text = vad2ay.IN1.GetValue().ToString().ToUpper();
            lblMaTienTeCuaTienBaoHiem.Text = vad2ay.IN2.GetValue().ToString().ToUpper();
            lblPhiBaoHiem.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.IN3.GetValue().ToString().Replace(".", ",")));
            lblSoDangKyBaoHiemTongHop.Text = vad2ay.IN4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.VR_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaTenKhoanDieuChinh1.Text = vad2ay.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh1.Text = vad2ay.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia1.Text = vad2ay.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaTenKhoanDieuChinh2.Text = vad2ay.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh2.Text = vad2ay.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia2.Text = vad2ay.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaTenKhoanDieuChinh3.Text = vad2ay.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh3.Text = vad2ay.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia3.Text = vad2ay.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 3:
                        lblMaTenKhoanDieuChinh4.Text = vad2ay.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh4.Text = vad2ay.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia4.Text = vad2ay.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 4:
                        lblMaTenKhoanDieuChinh5.Text = vad2ay.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh5.Text = vad2ay.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia5.Text = vad2ay.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblChiTietKhaiTriGia.Text = vad2ay.VLD.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ay.KF1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThue1.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue1.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong1.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaSacThue2.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue2.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong2.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaSacThue3.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue3.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong3.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaSacThue4.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue4.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong4.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaSacThue5.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue5.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong5.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblMaSacThue6.Text = vad2ay.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue6.Text = vad2ay.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue6.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong6.Text = vad2ay.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblTongTienThuePhaiNop.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.B02.GetValue().ToString().Replace(".", ",")));
            lblSoTienBaoLanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.B03.GetValue().ToString().Replace(".", ",")));
            for (int i = 0; i < vad2ay.KJ1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaDongTienTyGiaTinhThue1.Text = vad2ay.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaDongTienTyGiaTinhThue2.Text = vad2ay.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaDongTienTyGiaTinhThue3.Text = vad2ay.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ay.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblMaXacDinhThoiHanNopThue.Text = vad2ay.ENC.GetValue().ToString().ToUpper();
            lblNguoiNopThue.Text = vad2ay.TPM.GetValue().ToString().ToUpper();
            lblMaLyDoDeNghi.Text = vad2ay.BP.GetValue().ToString().ToUpper();
            lblPhanLoaiNopThue.Text = vad2ay.B08.GetValue().ToString().ToUpper();
            lblTongSoTrangToKhai.Text = vad2ay.B12.GetValue().ToString().ToUpper();
            lblTongSoDongHang.Text = vad2ay.B13.GetValue().ToString().ToUpper();

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }
    }
}
