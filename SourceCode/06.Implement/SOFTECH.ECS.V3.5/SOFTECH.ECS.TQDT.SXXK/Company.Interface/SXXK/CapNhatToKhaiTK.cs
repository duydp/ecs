﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ToKhai;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.BLL.SXXK.ThanhKhoan;
using System.Globalization;
namespace Company.Interface.SXXK
{
    public partial class CapNhatToKhaiTK : BaseForm
    {
        public CapNhatToKhaiTK()
        {
            InitializeComponent();
            gridEX1.LoadingRow += new RowLoadEventHandler(gridEX1_LoadingRow);
        }


        string sotokhai = "";
        DataTable dt = new DataTable();
        DataTable dtSendHQ = new DataTable();
        public bool ConvertDonGiaTT;
        public int LanThanhLy;
        void xuLyChuoi()
        {
            try
            {
                string str = txtChuoiXL.Text;
                string[] catchuoi = str.Split(',');
                foreach (string st in catchuoi)
                {
                    DataRow dr = dt.NewRow();
                    string st1 = st.Replace(" ", "");
                    string st2 = st1.Replace("[", "");
                    string st3 = st2.Replace("]", "");
                    string[] chuoi = st3.Split('-');
                    dr["Sotokhai"] = chuoi[1].Split('/')[0].ToString();
                    dr["NgayKhaiBao"] = Convert.ToDateTime(chuoi[2].ToString());
                    dr["NgayThucTe"] = Convert.ToDateTime(chuoi[3].ToString());
                    dt.Rows.Add(dr);
                }
                if (dt != null)
                {
                    gridEX1.DataSource = dt;
                    gridEX1.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }
        void xuLyChuoiDonGia()
        {
            try
            {
                dt.Clear();
                string str = txtChuoiXL.Text;
                string[] NguyenPhuLieu = str.Split(',');
                foreach (string st in NguyenPhuLieu)
                {
                    string[] chuoiNPL = st.Split('-');
                    // Xử lý chuỗi trong trường hợp MÃ NPL có dấu -
                    if (chuoiNPL.Length > 4)
                    {
                        string tempStr = string.Empty;
                        foreach (string item in chuoiNPL)
                        {
                            if (!item.Contains('[') || !item.Contains(']'))
                                tempStr = string.IsNullOrEmpty(tempStr) ? item : tempStr + "-" + item;
                        }
                        chuoiNPL[0] = tempStr;
                        string[] chuoiNPLtemp = new string[] { tempStr, chuoiNPL[chuoiNPL.Length - 3], chuoiNPL[chuoiNPL.Length - 2], chuoiNPL[chuoiNPL.Length - 1] };
                        chuoiNPL = chuoiNPLtemp;

                    }
                    if (chuoiNPL.Length == 4)
                    {
                        DataRow dr = dt.NewRow();
                        dr["MaNPL"] = chuoiNPL[0].Replace("[", "").Replace("]", "").Trim();
                        dr["DonGiaTTThanhKhoan"] = chuoiNPL[1].Replace("[", "").Replace("]", "").Trim();
                        string[] ChuoiToKhai = chuoiNPL[3].Split(']');
                        if (ChuoiToKhai.Length > 1)
                        {
                            // Tìm kiếm đơn giá thuộc tờ khai nào. 
                            // Lấy đơn giá đang khai báo bỏ đi số thập phân cuối để tìm kiếm cho chính xác
                            string dongiaKB = dr["DonGiaTTThanhKhoan"].ToString().Contains('.') ? dr["DonGiaTTThanhKhoan"].ToString().Substring(0, dr["DonGiaTTThanhKhoan"].ToString().Length - 1) : dr["DonGiaTTThanhKhoan"].ToString();
                            if (dongiaKB != "0")
                            {
                                foreach (string strItem in ChuoiToKhai)
                                {
                                    if (strItem.Contains(dongiaKB))
                                    {
                                        string[] tokhai = strItem.Split('/');
                                        dr["DonGiaTTThucTe"] = tokhai[0].Replace("[", "").Replace("]", "").Trim();
                                        dr["ThueSuatThucTe"] = tokhai[1].Replace("[", "").Replace("]", "").Trim();
                                        dr["SoToKhai"] = tokhai[2].Replace("[", "").Replace("]", "").Trim();
                                        //dr["SotokhaiGoc"] = tokhai[1].Replace("[", "").Replace("]", "").Trim();
                                        dr["MaLoaiHinh"] = tokhai[3].Replace("[", "").Replace("]", "").Trim();
                                        dr["NamDK"] = tokhai[4].Replace("[", "").Replace("]", "").Trim();
                                        break;
                                    }
                                }
                            }
                        }
                        dt.Rows.Add(dr);

                    }
                }
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["SoToKhai"] == null || string.IsNullOrEmpty(dr["SoToKhai"].ToString()))
                    {
                        string fillter = string.Format("MaNPL = '{0}' AND Convert(DonGiaTT,'System.String') like '{1}%'", dr["MaNPL"].ToString().Trim(), dr["DonGiaTTThanhKhoan"].ToString().Substring(0, dr["DonGiaTTThanhKhoan"].ToString().Length - 1));
                        DataRow[] temprow = dtSendHQ.Select(fillter);
                        if (temprow.Length > 0)
                        {
                            dr["MaLoaiHinh"] = temprow[0]["MaLoaiHinh"];
                            dr["NamDK"] = temprow[0]["NamDK"];//(object)temprow[0]["NamDK"].ToString();
                            if (dr["MaLoaiHinh"].ToString().Contains("V"))
                                dr["SoToKhai"] = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(System.Convert.ToInt32(temprow[0]["SoToKhaiNhap"]));
                            else
                                dr["SoToKhai"] = temprow[0]["SoToKhaiNhap"];

                        }
                    }
                }
                if (dt != null)
                {
                    gridEX1.DataSource = dt;
                    gridEX1.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }
        void UpdateDonGiaTT()
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            List<DataRow> listDelete = new List<DataRow>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (!string.IsNullOrEmpty(dr["DonGiaTTThucTe"].ToString().Trim()))
                {
                    int SoToKhai = 0;
                    string MaLoaiHinh = dr["MaLoaiHinh"].ToString().Trim();
                    MaLoaiHinh = MaLoaiHinh.Length == 3 ? "NV" + MaLoaiHinh : MaLoaiHinh;
                    if (MaLoaiHinh.Contains("V"))
                    {

                        List<int> temp = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKByVNACCS(System.Convert.ToDecimal(dr["SoToKhai"].ToString().Trim()));
                        if (temp != null && temp.Count > 0) SoToKhai = temp[0];
                    }
                    else
                    {
                        SoToKhai = Convert.ToInt32(dr["SoToKhai"].ToString().Trim());

                    }

                    string MaNPL = dr["MaNPL"].ToString().Trim();
                    int NamDK = Convert.ToInt32(dr["NamDK"].ToString().Trim());
                    try
                    {
                        double DonGiaMoi = System.Convert.ToDouble(dr["DonGiaTTThucTe"].ToString().Trim(), provider);
                        double DonGiaCu = System.Convert.ToDouble(dr["DonGiaTTThanhKhoan"].ToString().Trim(), provider); // bo ki tu cuoi cung de lam tron
                        double ThueSuat = System.Convert.ToDouble(dr["ThueSuatThucTe"].ToString().Trim(), provider);
                        if (new HangMauDich().UpdateByDonGiaTT(SoToKhai, MaLoaiHinh, NamDK, MaNPL, DonGiaCu, DonGiaMoi, ThueSuat) > 0)
                        {
                            //dt.Rows.Remove(dr);
                            listDelete.Add(dr);
                            BCXuatNhapTon.UpdateByDonGiaTT(MaNPL, DonGiaCu, DonGiaMoi, LanThanhLy, ThueSuat);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Exception ex2 = new Exception("Số tờ khai : " + SoToKhai + " - Mã LH: " + MaLoaiHinh + " - Mã NPL: " + MaNPL + "\r\n" + ex.Message, ex);
                        Logger.LocalLogger.Instance().WriteMessage(ex2);
                    }
                }
            }
            foreach (DataRow item in listDelete)
            {
                dt.Rows.Remove(item);
            }
            gridEX1.DataSource = dt;
            gridEX1.Refetch();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //DataSet ds = new DataSet();
            //Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
            //ToKhaiMauDichCollection listUpdate = new ToKhaiMauDichCollection();
            try
            {
                if (ConvertDonGiaTT)
                    UpdateDonGiaTT();
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();

                        sotokhai = dr["SoToKhai"].ToString();
                        tkmd.LoadVNACCSBy(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, sotokhai);
                        if (tkmd != null)
                        {
                            tkmd.NGAY_THN_THX = Convert.ToDateTime(dr["NgayThucTe"].ToString());
                            tkmd.NgayHoanThanh = Convert.ToDateTime(dr["NgayThucTe"].ToString());
                            tkmd.Update();
                        }
                    }
                }
                ShowMessage("Lưu thành công", false);
                if (!ConvertDonGiaTT) this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }



        }

        private void btnConvert_Click(object sender, EventArgs e)
        {

            if (txtChuoiXL.Text == "")
            {
                this.ShowMessage("Vui lòng nhập chuỗi xử lý", false);
                return;
            }
            else
            {
                if (!ConvertDonGiaTT)
                {
                    xuLyChuoi();
                }
                else
                {
                    dtSendHQ = new Company.BLL.KDT.SXXK.BCXuatNhapTon().GetBCXNTByLanThanhLy(GlobalSettings.MA_DON_VI, LanThanhLy);
                    xuLyChuoiDonGia();
                }
            }
        }

        private void CapNhatToKhaiTK_Load(object sender, EventArgs e)
        {
            if (!ConvertDonGiaTT)
            {
                dt.Columns.Add("Sotokhai");
                dt.Columns.Add("NgayKhaiBao");
                dt.Columns.Add("NgayThucTe");
            }
            else
            {
                gridEX1.RootTable.Columns.Clear();

                gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaNPL", ColumnType.Text, EditType.NoEdit) { Caption = "Mã NPL" });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("SoToKhai", ColumnType.Text, EditType.NoEdit) { Caption = "Số tờ khai" });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaLoaiHinh", ColumnType.Text, EditType.NoEdit) { Caption = "Mã LH" });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("NamDK", ColumnType.Text, EditType.NoEdit) { Caption = "Năm ĐK", TextAlignment = TextAlignment.Center });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGiaTTThanhKhoan", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá KB", FormatString = "N6" });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGiaTTThucTe", ColumnType.Text, EditType.TextBox) { Caption = "Đơn giá thực tế", FormatString = "N6" });
                gridEX1.Tables[0].Columns.Add(new GridEXColumn("ThueSuatThucTe", ColumnType.Text, EditType.TextBox) { Caption = "Thuế suất thực tế", FormatString = "N6" });

                dt.Columns.Add("MaNPL");
                dt.Columns.Add("Sotokhai");
                // dt.Columns.Add("SotokhaiGoc"); //Số tờ khai INT
                dt.Columns.Add("MaLoaiHinh");
                dt.Columns.Add("NamDK");
                dt.Columns.Add("DonGiaTTThanhKhoan");
                dt.Columns.Add("DonGiaTTThucTe");
                dt.Columns.Add("ThueSuatThucTe");
            }

            gridEX1.DataSource = dt;
            gridEX1.Refetch();
        }
        void gridEX1_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {


                //throw new NotImplementedException();
                if (ConvertDonGiaTT)
                {
                    //object a = e.Row.Cells["NamDK"].Text;
                    if (e.Row.RowType == RowType.Record)
                    {

                        //DataRowView dr = (DataRowView)e.Row.DataRow;
                        //if (dr["SoToKhai"] == null || string.IsNullOrEmpty(dr["SoToKhai"].ToString()))
                        //{
                        //    string fillter = string.Format("MaNPL = '{0}' AND Convert(DonGiaTT,'System.String') like '{1}%'", dr["MaNPL"].ToString().Trim(), dr["DonGiaTTThanhKhoan"].ToString().Substring(0, dr["DonGiaTTThanhKhoan"].ToString().Length - 1));
                        //    DataRow[] temprow = dtSendHQ.Select(fillter);
                        //    if (temprow.Length > 0)
                        //    {
                        //        dr["MaLoaiHinh"] = temprow[0]["MaLoaiHinh"];
                        //        //dr["NamDK"] = temprow[0]["MaLoaiHinh"];//(object)temprow[0]["NamDK"].ToString();
                        //        e.Row.Cells["NamDK"].Text = temprow[0]["NamDK"].ToString();

                        //        if (dr["MaLoaiHinh"].ToString().Contains("V"))
                        //            dr["SoToKhai"] = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(System.Convert.ToInt32(temprow[0]["SoToKhaiNhap"]));
                        //        else
                        //            dr["SoToKhai"] = temprow[0]["SoToKhaiNhap"];

                        //    }
                        //}
                        if (string.IsNullOrEmpty(e.Row.Cells["DonGiaTTThucTe"].Value.ToString().Trim()))
                        {
                            if (e.Row.RowStyle == null) e.Row.RowStyle = new GridEXFormatStyle();
                            e.Row.RowStyle.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtChuoiXL_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbChuoi = new StringBuilder();
                string str = txtChuoiXL.Text;
                string[] catchuoi = str.Split(',');

                foreach (string st in catchuoi)
                {
                    sbChuoi.Append(st.Replace("\n", ""));
                    sbChuoi.AppendLine(", ");
                    sbChuoi.AppendLine();
                }

                txtChuoiXL.Text = sbChuoi.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }

        private void CapNhatToKhaiTK_Resize(object sender, EventArgs e)
        {
            try
            {
                uiGroupBox1.Height = this.Height / 2;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
