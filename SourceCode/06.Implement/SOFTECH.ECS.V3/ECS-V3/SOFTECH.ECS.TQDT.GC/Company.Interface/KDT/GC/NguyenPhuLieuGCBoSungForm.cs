﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Linq;
namespace Company.Interface.KDT.GC
{
    public partial class NguyenPhuLieuGCBoSungForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKMuaVN = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public string MaLoaiPK = string.Empty;
        public bool boolFlag;

        public NguyenPhuLieuGCBoSungForm()
        {
            InitializeComponent();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void NguyenPhuLieuGCEditForm_Load(object sender, EventArgs e)
        {
            bool isSuaSP = false;
            isSuaSP = MaLoaiPK == "503";
            dgList.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSuaSP;
            btnChon.Visible = MaLoaiPK != "803";
            txtMa.Focus();

            //collumn Soluong
            dgList.Tables[0].Columns[7].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            //collumn Dongia
            dgList.Tables[0].Columns[8].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;



            if (_DonViTinh == null || _DonViTinh.Rows.Count == 0)
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;


            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {

                btnAdd.Enabled = false;

                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

            }
            //Registered 
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;

            }

            LoaiPK.MaPhuKien = MaLoaiPK;
            dgList.DataSource = LoaiPK.HPKCollection;
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MaLoaiPK))
            {
                ShowMessage("Mã loại phụ kiện chưa được xác nhận", false);
                DialogResult = DialogResult.Cancel;
            };
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                checkExitsNPLAndSTTHang();
            }
        }

        private void checkExitsNPLAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.Ma = txtMa.Text.Trim();
                npl.HopDong_ID = this.HD.ID;

                if (npl.Load())
                {
                    if (npl.Ma.Trim().ToUpper() != HangPK.MaHang.Trim().ToUpper())
                    {
                        showMsg("MSG_2702056");
                        //MLMessages("Nguyên phụ liệu này đã khai báo.","MSG_STN03","raw materials", false);
                        txtMa.Focus();
                        return;
                    }
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                foreach (HangPhuKien HPK in LoaiPK.HPKCollection)
                {
                    if (HPK.MaHang == txtMa.Text.Trim())
                    {
                        showMsg("MSG_2702056");
                        //MLMessages("Nguyên phụ liệu này đã khai báo.", "MSG_STN03", "raw materials", false);
                        txtMa.Focus();
                        if (HangPK.MaHang.Trim().Length > 0)
                            LoaiPK.HPKCollection.Add(HangPK);
                        return;
                    }
                }
                HangPK.Delete(LoaiPK.MaPhuKien, HD.ID);
                HangPK.ID = 0;
                HangPK.TenHang = txtTen.Text.Trim();
                HangPK.MaHang = txtMa.Text.Trim();
                HangPK.MaHS = txtMaHS.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                LoaiPK.InsertUpdateBoSungNPL(pkdk.HopDong_ID);
                reset();
                this.setErro();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (HangPhuKien)e.Row.DataRow;
            txtMa.Text = HangPK.MaHang;
            txtMaHS.Text = HangPK.MaHS;
            txtTen.Text = HangPK.TenHang;
            txtMaCu.Text = HangPK.ThongTinCu;
            cbDonViTinh.SelectedValue = HangPK.DVT_ID;

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();

        }
        public bool CheckExitNPLInMuaVN(HangPhuKien HPK)
        {
            foreach (HangPhuKien HangPK in LoaiPKMuaVN.HPKCollection)
            {
                if (HangPK.MaHang.Trim().ToUpper() == HPK.MaHang.Trim().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnXoa_Click(null, null);
            /* {
                 if (showMsg("MSG_DEL01", true) == "Yes")
                 //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
                 {

                     GridEXSelectedItemCollection items = dgList.SelectedItems;
                     foreach (GridEXSelectedItem row in items)
                     {
                         if (row.RowType == RowType.Record)
                         {
                             Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                             if (CheckExitNPLInMuaVN(hpkDelete))
                             {
                                 showMsg("MSG_DEL04");
                                 //MLMessages("Nguyên phụ liệu : "+hpkDelete.MaHang+" này có liên quan tới bên loại phụ kiện mua Việt Nam nên không xóa được.","MSG_DEL04",hpkDelete.MaHang, false);
                                 e.Cancel = true;
                                 return;
                             }

                         }
                     }
                     foreach (GridEXSelectedItem row in items)
                     {
                         if (row.RowType == RowType.Record)
                         {
                             Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                             if (hpkDelete.ID > 0)
                             {
                                 hpkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                             }
                         }
                     }
                 }
                 else
                 {
                     e.Cancel = true;
                 }
             }*/
        }

        private void NguyenPhuLieuGCBoSungForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = MaLoaiPK;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(MaLoaiPK);
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID); ;
            }
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);
            error.SetError(txtTen, null);


        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {

                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (CheckExitNPLInMuaVN(hpkDelete))
                        {
                            showMsg("MSG_DEL04");
                            //MLMessages("Nguyên phụ liệu : " + hpkDelete.MaHang + " này có liên quan tới bên loại phụ kiện mua Việt Nam nên không xóa được.", "MSG_DEL04", hpkDelete.MaHang, false);
                            return;
                        }

                    }
                }
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try
                        {
                            if (hpkDelete.ID > 0)
                            {
                                hpkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                            }

                            LoaiPK.HPKCollection.Remove(hpkDelete);
                        }
                        catch { }
                    }
                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }

        }

        private void reset()
        {
            txtMa.Text = "";
            txtMaHS.Text = "";
            txtTen.Text = "";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();

        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonNPL frm = new FormChonNPL();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    foreach (HangPhuKien hangPK in frm.HPKCollection)
                    {
                        hangPK.Master_ID = LoaiPK.ID;
                        LoaiPK.HPKCollection.Add(hangPK);
                    }
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    dgList.DataSource = LoaiPK.HPKCollection;
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }
                reset();
                this.setErro();
            }
        }
    }
}