﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
 

namespace Company.Interface.Report.GC
{
    public partial class DinhMucKDTReport : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public DinhMucCollection DMCollection = new DinhMucCollection();
        private int STT = 0;
        protected DataTable _DonViTinh;
        public DinhMucKDTReport()
        {
            InitializeComponent();
            _DonViTinh = DonViTinh.SelectAll();
        }
        public void BindReport(string maSP)
        {

            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            lblSoLuong.Text = "";
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac.ToUpper();
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            SP.Ma = maSP;
            SP.HopDong_ID = this.HD.ID;
            SP.Load();
            lblMathang.Text = SP.Ten + "  (Mã hàng: " + maSP +")";
            lblSoLuong.Text = SP.SoLuongDangKy.ToString("N"+GlobalSettings.SoThapPhan.LuongSP) + " " + DonViTinh.GetName(SP.DVT_ID);
            this.DataSource = this.DMCollection;
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblDMSD.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:N"+ GlobalSettings.SoThapPhan.DinhMuc +"}");
            lblTLHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{00:F00}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;
            

        }

        private void DinhMucReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }
        protected string DonViTinh_GetName(object id)
        {
            if (this._DonViTinh == null) this._DonViTinh = DonViTinh.SelectAll();
            return this._DonViTinh.Select(string.Format("ID = '{0}'", id.ToString().PadRight(3)))[0][1].ToString();

        }
        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
            if (lblDVT.Text.Trim() == "")
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.Ma = GetCurrentColumnValue("MaNguyenPhuLieu").ToString();
                npl.HopDong_ID = HD.ID;
                npl.Load();
                lblDVT.Text = DonViTinh_GetName(npl.DVT_ID);
                lblTenNPL.Text = npl.Ten;
            }
        }
    }
}
