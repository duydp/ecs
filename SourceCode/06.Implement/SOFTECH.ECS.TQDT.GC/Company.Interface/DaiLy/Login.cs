﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Company.GC.BLL.KDT;
using Company.GC.BLL.Utils.KDT;

namespace Company.Interface.DaiLy
{
    public partial class Login : Form
    {
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            DataTable dtCauHinh = new HeThongPhongKhai().SelectCauHinhByMaDoanhNghiep(cbMaDoanhNghiep.Value.ToString()).Tables[0];
            GlobalSettings.MA_DON_VI = cbMaDoanhNghiep.Value.ToString().Trim();
            GlobalSettings.TEN_DON_VI = cbMaDoanhNghiep.Text;
            GlobalSettings.DIA_CHI = GetValue(dtCauHinh, "DIA_CHI");
            GlobalSettings.MaMID = GetValue(dtCauHinh, "MaMID");
            if (GetValue(dtCauHinh, "MA_HAI_QUAN") != "")
                GlobalSettings.MA_HAI_QUAN = GetValue(dtCauHinh, "MA_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_HAI_QUAN") != "")
                GlobalSettings.TEN_HAI_QUAN = GetValue(dtCauHinh, "TEN_HAI_QUAN");
            if (GetValue(dtCauHinh, "MA_CUC_HAI_QUAN") != "")
                GlobalSettings.MA_CUC_HAI_QUAN = GetValue(dtCauHinh, "MA_CUC_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN") != "")
                GlobalSettings.TEN_CUC_HAI_QUAN = GetValue(dtCauHinh, "TEN_CUC_HAI_QUAN");
            if (GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN") != "")
                GlobalSettings.TEN_HAI_QUAN_NGAN = GetValue(dtCauHinh, "TEN_HAI_QUAN_NGAN");
            if (GetValue(dtCauHinh, "MailHaiQuan") != "")
                GlobalSettings.MailHaiQuan = GetValue(dtCauHinh, "MailHaiQuan");
            if (GetValue(dtCauHinh, "TEN_DOI_TAC") != "")
                GlobalSettings.TEN_DOI_TAC = GetValue(dtCauHinh, "TEN_DOI_TAC");
            if (GetValue(dtCauHinh, "MaHTS") != "")
                GlobalSettings.MaHTS = Convert.ToInt32(GetValue(dtCauHinh, "MaHTS"));
            if (GetValue(dtCauHinh, "TuDongTinhThue") != "")
                GlobalSettings.TuDongTinhThue = GetValue(dtCauHinh, "TuDongTinhThue");
            if(GetValue(dtCauHinh, "CUA_KHAU") != "")
                GlobalSettings.CUA_KHAU = GetValue(dtCauHinh, "CUA_KHAU");
            if(GetValue(dtCauHinh, "DIA_DIEM_DO_HANG") != "")
                GlobalSettings.DIA_DIEM_DO_HANG = GetValue(dtCauHinh, "DIA_DIEM_DO_HANG");
            if (GetValue(dtCauHinh, "DKGH_MAC_DINH") != "")
                GlobalSettings.DKGH_MAC_DINH = GetValue(dtCauHinh, "DKGH_MAC_DINH");
            if (GetValue(dtCauHinh, "DVT_MAC_DINH") != "")
                GlobalSettings.DVT_MAC_DINH = GetValue(dtCauHinh, "DVT_MAC_DINH");
            GlobalSettings.LOAI_HINH = "NSX01";
            if (GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH") != "")
                GlobalSettings.NGUYEN_TE_MAC_DINH = GetValue(dtCauHinh, "NGUYEN_TE_MAC_DINH");
            if (GetValue(dtCauHinh, "NUOC") != "")
                GlobalSettings.NUOC = GetValue(dtCauHinh, "NUOC");
            if (GetValue(dtCauHinh, "PTTT_MAC_DINH") != "")
                GlobalSettings.PTTT_MAC_DINH = GetValue(dtCauHinh, "PTTT_MAC_DINH");
            if (GetValue(dtCauHinh, "PTVT_MAC_DINH") != "")
                GlobalSettings.PTVT_MAC_DINH = GetValue(dtCauHinh, "PTVT_MAC_DINH");
            if (GetValue(dtCauHinh, "TieuDeDM") != "")
                GlobalSettings.TieuDeInDinhMuc = GetValue(dtCauHinh, "TieuDeDM");

            if (GetValue(dtCauHinh, "DinhMuc") != "")
                GlobalSettings.SoThapPhan.DinhMuc = Convert.ToInt32(GetValue(dtCauHinh, "DinhMuc"));
            else
                GlobalSettings.SoThapPhan.DinhMuc = 5;
            if (GetValue(dtCauHinh, "LuongNPL") != "")
                GlobalSettings.SoThapPhan.LuongNPL = Convert.ToInt32(GetValue(dtCauHinh, "LuongNPL"));
            else
                GlobalSettings.SoThapPhan.LuongNPL = 5;
            if (GetValue(dtCauHinh, "LuongSP") != "")
                GlobalSettings.SoThapPhan.LuongSP = Convert.ToInt32(GetValue(dtCauHinh, "LuongSP"));
            else
                GlobalSettings.SoThapPhan.LuongSP = 0;
            GlobalSettings.Luu_SoThapPhan(GlobalSettings.SoThapPhan.DinhMuc, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.LuongSP,GlobalSettings.SoThapPhan.TLHH);

            MainForm.EcsQuanTri = ECSPrincipal.LayDanhSachQuyen();
            MainForm.isLoginSuccess = true;
            this.Close();

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }
        private string GetValue(DataTable dtCauHinh, string key)
        {
            foreach (DataRow dr in dtCauHinh.Rows)
            {
                if (dr["Key_Config"].ToString().Trim().ToLower() == key.Trim().ToLower()) return dr["Value_Config"].ToString();
            }
            return "";
        }
        private bool checkSoLuongDoanhNghiep()
        {
            KdtObj obj = Company.GC.BLL.Utils.KDT.KDT.getSoLuongDoanhNghiep();
            int soLuongDN = HeThongPhongKhai.SelectedCountDN();
            if (soLuongDN > obj.SoLuong)
                return true;
            return false;
        }
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                KdtObj obj = null;
                try { obj = Company.GC.BLL.Utils.KDT.KDT.getSoLuongDoanhNghiep(); }
                catch { }
                if (obj != null)
                {
                    if (checkSoLuongDoanhNghiep())
                    {
                        ShowMessage("Bạn đã nhập số quá số lượng quy định doanh nghiệp được khai báo.", false);
                    }
                }
                else
                {
                    obj = new KdtObj();
                    obj.SoLuong = 1;
                }

                DataTable dtMaDoanhNghiep = new HeThongPhongKhai().SelectDanhSachDoanhNghiep((int)obj.SoLuong).Tables[0];

                if (dtMaDoanhNghiep.Rows.Count > 0)
                {
                    cbMaDoanhNghiep.DataSource = dtMaDoanhNghiep;
                    cbMaDoanhNghiep.DisplayMember = "TenDoanhNghiep";
                    cbMaDoanhNghiep.ValueMember = "MaDoanhNghiep";
                    cbMaDoanhNghiep.SelectedIndex = 0;
                }
            }
            catch
            {
                MessageBox.Show("Không kết nối được với cơ sở dữ liệu");
                linkLabel1_LinkClicked(null, null);
            }
        }

   

      
    }
}