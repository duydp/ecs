﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
  public  class ContentInspections
    {
       /// <summary>
        /// Thông tin các lần kiểm tra
        /// </summary>
        [XmlElement("ContentInspection")]
      public List<ContentInspection> ContentInspection { get; set; }
    }
}
