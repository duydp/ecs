namespace Company.Interface.Feedback
{
    partial class LinhVucUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TrlLinhvuc = new DevExpress.XtraTreeList.TreeList();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.TrlLinhvuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TrlLinhvuc
            // 
            this.TrlLinhvuc.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.TrlLinhvuc.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.TrlLinhvuc.Appearance.Empty.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(227)))), ((int)(((byte)(245)))));
            this.TrlLinhvuc.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(227)))), ((int)(((byte)(245)))));
            this.TrlLinhvuc.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.EvenRow.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.EvenRow.Options.UseBorderColor = true;
            this.TrlLinhvuc.Appearance.EvenRow.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.TrlLinhvuc.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.FocusedCell.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.FocusedCell.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(171)))), ((int)(((byte)(177)))));
            this.TrlLinhvuc.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.TrlLinhvuc.Appearance.FocusedRow.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.FocusedRow.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(208)))));
            this.TrlLinhvuc.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(221)))), ((int)(((byte)(208)))));
            this.TrlLinhvuc.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.FooterPanel.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.TrlLinhvuc.Appearance.FooterPanel.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(209)))), ((int)(((byte)(188)))));
            this.TrlLinhvuc.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(209)))), ((int)(((byte)(188)))));
            this.TrlLinhvuc.Appearance.GroupButton.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.GroupButton.Options.UseBorderColor = true;
            this.TrlLinhvuc.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(234)))), ((int)(((byte)(221)))));
            this.TrlLinhvuc.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(234)))), ((int)(((byte)(221)))));
            this.TrlLinhvuc.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.GroupFooter.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.TrlLinhvuc.Appearance.GroupFooter.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(211)))), ((int)(((byte)(215)))));
            this.TrlLinhvuc.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(130)))), ((int)(((byte)(134)))));
            this.TrlLinhvuc.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(197)))), ((int)(((byte)(180)))));
            this.TrlLinhvuc.Appearance.HorzLine.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.TrlLinhvuc.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.TrlLinhvuc.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.OddRow.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.OddRow.Options.UseBorderColor = true;
            this.TrlLinhvuc.Appearance.OddRow.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
            this.TrlLinhvuc.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.TrlLinhvuc.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(130)))), ((int)(((byte)(134)))));
            this.TrlLinhvuc.Appearance.Preview.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.Preview.Options.UseFont = true;
            this.TrlLinhvuc.Appearance.Preview.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.TrlLinhvuc.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.Row.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.Row.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(201)))), ((int)(((byte)(207)))));
            this.TrlLinhvuc.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.TrlLinhvuc.Appearance.SelectedRow.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.SelectedRow.Options.UseForeColor = true;
            this.TrlLinhvuc.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(136)))), ((int)(((byte)(122)))));
            this.TrlLinhvuc.Appearance.TreeLine.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(197)))), ((int)(((byte)(180)))));
            this.TrlLinhvuc.Appearance.VertLine.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(136)))), ((int)(((byte)(122)))));
            this.TrlLinhvuc.Appearance.VertLine.Options.UseBackColor = true;
            this.TrlLinhvuc.Appearance.VertLine.Options.UseBorderColor = true;
            this.TrlLinhvuc.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.TrlLinhvuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrlLinhvuc.KeyFieldName = "";
            this.TrlLinhvuc.Location = new System.Drawing.Point(2, 22);
            this.TrlLinhvuc.LookAndFeel.SkinName = "Office 2007 Blue";
            this.TrlLinhvuc.Name = "TrlLinhvuc";
            this.TrlLinhvuc.OptionsView.ShowColumns = false;
            this.TrlLinhvuc.OptionsView.ShowIndentAsRowStyle = true;
            this.TrlLinhvuc.OptionsView.ShowIndicator = false;
            this.TrlLinhvuc.OptionsView.ShowVertLines = false;
            this.TrlLinhvuc.Size = new System.Drawing.Size(363, 182);
            this.TrlLinhvuc.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.TrlLinhvuc);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Office 2007 Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(367, 206);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "groupControl1";
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "T�n";
            this.treeListColumn1.FieldName = "Ten";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // LinhVucUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "LinhVucUC";
            this.Size = new System.Drawing.Size(367, 206);
            this.Load += new System.EventHandler(this.LinhVucUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TrlLinhvuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList TrlLinhvuc;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;

    }
}
