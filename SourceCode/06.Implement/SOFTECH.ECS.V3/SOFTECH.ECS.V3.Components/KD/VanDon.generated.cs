﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class VanDon
    {
        #region Properties.

        public long ID { set; get; }
        public string SoVanDon { set; get; }
        public DateTime NgayVanDon { set; get; }
        public bool HangRoi { set; get; }
        public string SoHieuPTVT { set; get; }
        public DateTime NgayDenPTVT { set; get; }
        public string MaHangVT { set; get; }
        public string TenHangVT { set; get; }
        public string TenPTVT { set; get; }
        public string QuocTichPTVT { set; get; }
        public string NuocXuat_ID { set; get; }
        public string MaNguoiNhanHang { set; get; }
        public string TenNguoiNhanHang { set; get; }
        public string MaNguoiGiaoHang { set; get; }
        public string TenNguoiGiaoHang { set; get; }
        public string CuaKhauNhap_ID { set; get; }
        public string CuaKhauXuat { set; get; }
        public string MaNguoiNhanHangTrungGian { set; get; }
        public string TenNguoiNhanHangTrungGian { set; get; }
        public string MaCangXepHang { set; get; }
        public string TenCangXepHang { set; get; }
        public string MaCangDoHang { set; get; }
        public string TenCangDoHang { set; get; }
        public long TKMD_ID { set; get; }
        public string DKGH_ID { set; get; }
        public string DiaDiemGiaoHang { set; get; }
        public string NoiDi { set; get; }
        public string SoHieuChuyenDi { set; get; }
        public DateTime NgayKhoiHanh { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        protected static IList<VanDon> ConvertToCollection(IDataReader reader)
        {
            IList<VanDon> collection = new List<VanDon>();
            while (reader.Read())
            {
                VanDon entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #region Select methods.

        public static VanDon LoadBy_SoVanDon(string SoVanDon)
        {
            const string spName = "[dbo].[p_KDT_VanDon_LoadSoVanDon]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.Char, SoVanDon);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<VanDon> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        public static VanDon Load(long id)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            VanDon entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------


        // Select by foreign key return collection		
        public static IList<VanDon> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            //IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            IDataReader reader = SelectReaderDynamic("TKMD_ID=" + tKMD_ID.ToString(), "TKMD_ID ASC");
            return ConvertToCollection(reader);
        }

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {

            const string spName = "p_KDT_VanDon_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertVanDon(string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateVanDon(long id, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.ID = id;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_VanDon_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateVanDon(long id, string soVanDon, DateTime ngayVanDon, bool hangRoi, string soHieuPTVT, DateTime ngayDenPTVT, string maHangVT, string tenHangVT, string tenPTVT, string quocTichPTVT, string nuocXuat_ID, string maNguoiNhanHang, string tenNguoiNhanHang, string maNguoiGiaoHang, string tenNguoiGiaoHang, string cuaKhauNhap_ID, string cuaKhauXuat, string maNguoiNhanHangTrungGian, string tenNguoiNhanHangTrungGian, string maCangXepHang, string tenCangXepHang, string maCangDoHang, string tenCangDoHang, long tKMD_ID, string dKGH_ID, string diaDiemGiaoHang, string noiDi, string soHieuChuyenDi, DateTime ngayKhoiHanh)
        {
            VanDon entity = new VanDon();
            entity.ID = id;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.HangRoi = hangRoi;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.MaHangVT = maHangVT;
            entity.TenHangVT = tenHangVT;
            entity.TenPTVT = tenPTVT;
            entity.QuocTichPTVT = quocTichPTVT;
            entity.NuocXuat_ID = nuocXuat_ID;
            entity.MaNguoiNhanHang = maNguoiNhanHang;
            entity.TenNguoiNhanHang = tenNguoiNhanHang;
            entity.MaNguoiGiaoHang = maNguoiGiaoHang;
            entity.TenNguoiGiaoHang = tenNguoiGiaoHang;
            entity.CuaKhauNhap_ID = cuaKhauNhap_ID;
            entity.CuaKhauXuat = cuaKhauXuat;
            entity.MaNguoiNhanHangTrungGian = maNguoiNhanHangTrungGian;
            entity.TenNguoiNhanHangTrungGian = tenNguoiNhanHangTrungGian;
            entity.MaCangXepHang = maCangXepHang;
            entity.TenCangXepHang = tenCangXepHang;
            entity.MaCangDoHang = maCangDoHang;
            entity.TenCangDoHang = tenCangDoHang;
            entity.TKMD_ID = tKMD_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.DiaDiemGiaoHang = diaDiemGiaoHang;
            entity.NoiDi = noiDi;
            entity.SoHieuChuyenDi = soHieuChuyenDi;
            entity.NgayKhoiHanh = ngayKhoiHanh;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteVanDon(long id)
        {
            VanDon entity = new VanDon();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}