﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepManageForm : BaseForm
    {
        public KDT_VNACC_GiayPhep_SEA giayPhep = new KDT_VNACC_GiayPhep_SEA();
        public List<KDT_VNACC_GiayPhep_SEA> listGiayPhep = new List<KDT_VNACC_GiayPhep_SEA>();
        public List<KDT_VNACC_HangGiayPhep> listHangGP = null;
        public VNACC_GiayPhepManageForm()
        {
            InitializeComponent();
        }

        private void gridEX1_DoubleClick(object sender, EventArgs e)
        {
            //epError.Clear();
            //GridEXSelectedItemCollection items = dgList.SelectedItems;
            //if (items.Count == 0) return;
        }

        private void VNACC_GiayPhep_TheoDoi_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.Text = "Theo dõi Giấy phép - SEA";

                Globals.SetControlTrangThaiXuLy(cboTrangThai);

                cboPhanLoaiXuatNhap.SelectedIndex = 1;

                SetContextMenuStrip(grdList);

                //Fill ValueList
                this.grdList.RootTable.Columns["ChucNangChungTu"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.grdList.RootTable.Columns["ChucNangChungTu"].ValueList;
                System.Data.DataView view = VNACC_Category_Common.SelectDynamic("ReferenceDB = 'E025'", "").Tables[0].DefaultView;
                view.Sort = "Code ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["Code"].ToString(), row["Name_VN"].ToString()));
                }

                btnTimKiem.PerformClick();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            int id = Convert.ToInt32(e.Row.Cells["ID"].Value);

            VNACC_GiayPhepForm_SEA f = new VNACC_GiayPhepForm_SEA();
            f.GiayPhep = getGiayPhepID(id);
            f.ShowDialog();



        }

        private KDT_VNACC_GiayPhep_SEA getGiayPhepID(long id)
        {
            listGiayPhep = KDT_VNACC_GiayPhep_SEA.SelectCollectionAll();
            foreach (KDT_VNACC_GiayPhep_SEA gp in listGiayPhep)
            {
                if (gp.ID == id)
                {
                    string where = "GiayPhep_ID =" + gp.ID;
                    listHangGP = new List<KDT_VNACC_HangGiayPhep>();
                    listHangGP = KDT_VNACC_HangGiayPhep.SelectCollectionDynamic(where, "ID", EGiayPhep.SEA);
                    foreach (KDT_VNACC_HangGiayPhep hang in listHangGP)
                    {
                        gp.HangCollection.Add(hang);
                    }
                    return gp;
                }
            }
            return null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            DeleteGiayPhep();
        }

        private void grdList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            DeleteGiayPhep();
        }

        private void DeleteGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ShowMessage("Bạn có muốn xóa giấy phép này không?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = grdList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            int id = Convert.ToInt32(i.GetRow().Cells["ID"].Value);
                            giayPhep = getGiayPhepID(id);

                            foreach (KDT_VNACC_HangGiayPhep hang in giayPhep.HangCollection)
                            {
                                hang.Delete();
                            }
                            giayPhep.Delete();
                        }
                    }

                    btnTimKiem.PerformClick();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DateTime dtNgayKhaiBao = Convert.ToDateTime(e.Row.Cells["NgayKhaiBao"].Text);
                if (dtNgayKhaiBao.Year <= 1900)
                    e.Row.Cells["NgayKhaiBao"].Text = "";

                #region Begin TrangThaiXuLy
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case 0:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case 1:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        break;
                    case 2:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã xác nhận khai báo";
                        break;
                    case 3:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Thông quan";
                        break;
                    case 4:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                        break;
                    case 5:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                }
                #endregion End TrangThaiXuLy
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTN = txtSoGiayPhep.Text.Trim();
                string soGP = txtSoGiayPhep.Text.Trim();
                string loaiHinh = cboPhanLoaiXuatNhap.SelectedValue.ToString();

                string where = String.Empty;
                if (soGP != "")
                    where += " SoDonXinCapPhep like '" + soGP + "%'";

                if (cboTrangThai.SelectedValue != null && cboTrangThai.SelectedValue.ToString() != "-1")
                    where += (where != "" ? " And " : "") + " TrangThaiXuLy = " + cboTrangThai.SelectedValue.ToString();

                if (loaiHinh != "")
                    where += (where != "" ? " And " : "") + " LoaiHinhXNK = '" + loaiHinh + "'";

                listGiayPhep.Clear();
                if (where == "")
                {
                    listGiayPhep = KDT_VNACC_GiayPhep_SEA.SelectCollectionAll();
                }
                else
                {
                    listGiayPhep = KDT_VNACC_GiayPhep_SEA.SelectCollectionDynamic(where, "ID");
                }

                grdList.DataSource = listGiayPhep;
                grdList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        #region CONTEXT MENU

        private void SetContextMenuStrip(Janus.Windows.GridEX.GridEX grid)
        {
            grid.ContextMenuStrip = contextMenuStrip1;

            contextMenuStrip1.ItemClicked += new ToolStripItemClickedEventHandler(contextMenuStrip1_ItemClicked);
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name == "mniCopy")
            {
                mniSaoChep(grdList);
            }
            else if (e.ClickedItem.Name == "mniPrint")
            {
            }
        }

        private void mniSaoChep(Janus.Windows.GridEX.GridEX grid)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!(grid.GetRow() != null && grid.GetRow().RowType == RowType.Record))
                    return;

                long id = Convert.ToInt64(grid.GetRow().Cells["ID"].Value);

                //Copy obj
                KDT_VNACC_GiayPhep_SEA obj = getGiayPhepID(id);
                KDT_VNACC_GiayPhep_SEA obj2 = new KDT_VNACC_GiayPhep_SEA();
                HelperVNACCS.UpdateObject<KDT_VNACC_GiayPhep_SEA>(obj2, obj, false);
                //Reset info
                obj2.SoDonXinCapPhep = 0;
                obj2.NgayKhaiBao = new DateTime(1900, 1, 1);
                //Save new obj
                obj2.InsertUpdateFull();

                //Reload info
                btnTimKiem.PerformClick();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void mniIn(Janus.Windows.GridEX.GridEX grid)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!(grid.GetRow() != null && grid.GetRow().RowType == RowType.Record))
                    return;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        #endregion
    }
}
