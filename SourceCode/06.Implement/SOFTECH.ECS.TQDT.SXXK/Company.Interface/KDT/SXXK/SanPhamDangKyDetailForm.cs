﻿using System;
using Company.BLL;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamDangKyDetailForm : BaseForm
    {
        public SanPhamDangKy SPDangKy = new SanPhamDangKy();

        //-----------------------------------------------------------------------------------------
        public SanPhamDangKyDetailForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        private void SanPhamDangKyDetailForm_Load(object sender, EventArgs e)
        {
            this.SPDangKy.LoadSPCollection();
            dgList.DataSource = this.SPDangKy.SPCollection;
            switch (this.SPDangKy.TrangThaiXuLy)
            {
                case -1:
                   // lblTrangThai.Text = "Chưa gửi thông tin";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa gửi thông tin";
                    }
                    else
                    {
                        lblTrangThai.Text = "Information has not yet sent";
                    }
                    break;
                case 0:
                    //lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to approval";
                    }
                    break;
                case 1:
                    //lblTrangThai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Approved";
                    }
                    break;
                case 2:
                    //lblTrangThai.Text = "Không phê duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not Approved";
                    }
                    break;

            }
         
            txtSoTiepNhan.Text = this.SPDangKy.SoTiepNhan.ToString();
            txtMaHaiQuan.Text = this.SPDangKy.MaHaiQuan;
            txtTenHaiQuan.Text = DonViHaiQuan.GetName(this.SPDangKy.MaHaiQuan);
            if (this.OpenType == OpenFormType.View)
            {
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            else
            {
                TopRebar1.Visible = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            MsgSend msg = new MsgSend();
            msg.master_id = this.SPDangKy.ID;
            msg.LoaiHS = "SP";
            if (msg.Load())
            {
                //lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
                }
                else
                {
                    lblTrangThai.Text = "Not Confirm information to Customs";
                }
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới sản phẩm.
        /// </summary>
        private void add()
        {
            SanPhamEditForm f = new SanPhamEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = txtMaHaiQuan.Text;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.SPCollection = this.SPDangKy.SPCollection;
            f.spDangKy = SPDangKy;
            f.ShowDialog();

            if (f.SPDetail != null)
            {
                SanPham sp = f.SPDetail;
                sp.Master_ID = this.SPDangKy.ID;
                this.SPDangKy.SPCollection.Add(sp);                
                sp.Insert();

                BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                spSXXK.Ma = sp.Ma;
                spSXXK.MaHS = sp.MaHS;
                spSXXK.Ten = sp.Ten;
                spSXXK.DVT_ID = sp.DVT_ID;
                spSXXK.Insert();
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        #endregion

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.OpenType != OpenFormType.View)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhamEditForm f = new SanPhamEditForm();
                        f.MaHaiQuan = txtMaHaiQuan.Text;
                        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        f.SPDetail = (SanPham)e.Row.DataRow;
                        f.SPCollection = this.SPDangKy.SPCollection;
                        f.SPCollection.RemoveAt(i.Position);
                        f.OpenType = OpenFormType.Edit;
                        f.spDangKy = SPDangKy;
                        f.ShowDialog();                       
                        if (f.SPDetail != null)
                        {
                            this.SPDangKy.SPCollection.Insert(i.Position, f.SPDetail);                            
                        }
                    }
                    dgList.Refetch();
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.add();
                    break;
            }
        }
    

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPham sp = (SanPham)i.GetRow().DataRow;
                        if (sp.ID > 0)
                        {
                            sp.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                        }
                    }
                }                
            }
            else
            {
                e.Cancel = true;
            }            
        }
    }
}