﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;

namespace Company.Interface.Report
{

    public partial class ReportViewDinhMucForm : BaseForm
    {
        public DinhMucDangKy DMDangKy;
        ReportDinhMuc_TT196_2013 reportDM = new ReportDinhMuc_TT196_2013();
        ReportDinhMuc_TT128_2013 reportDMTT194 = new ReportDinhMuc_TT128_2013();
        public BLL.SXXK.SanPhamCollection spCollection = new Company.BLL.SXXK.SanPhamCollection();

        public ReportViewDinhMucForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            chkNKvaND.Checked = GlobalSettings.inNKvaND;
            cboSanPham.Items.Clear();
            if (spCollection.Count == 0)
            {
                DataSet ds = DMDangKy.getSanPhamOfDinhMuc(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    cboSanPham.Items.Add(row[2].ToString(), row[2].ToString());
                }
            }
            else
            {
                foreach (BLL.SXXK.SanPham sp in spCollection)
                {
                    cboSanPham.Items.Add(sp.Ma, sp.Ma);
                }
            }
            cboSanPham.SelectedIndex = cboSanPham.Items.Count > 0 ? 0 : -1;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            reportDM = new ReportDinhMuc_TT196_2013();
            reportDMTT194 = new ReportDinhMuc_TT128_2013();
            this.reportDM.DMDangKy = this.reportDMTT194.DMDangKy = DMDangKy;
            BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
            sp.Ma = cboSanPham.SelectedItem.Value.ToString();
            sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            sp.Load();
            this.reportDM.MaSP = this.reportDMTT194.MaSP = cboSanPham.SelectedItem.Text;
            this.reportDM.TenSP = this.reportDMTT194.TenSP = sp.Ten;
            this.reportDM.DVT = this.reportDMTT194.DVT = DonViTinh_GetName(sp.DVT_ID);
            GlobalSettings.inNKvaND = chkNKvaND.Checked;
            if (DMDangKy != null)
            {
                this.reportDM.HopDong = this.reportDMTT194.HopDong = txtHopDong.Text;
                SetText(txtHopDong.Text, txtToKhai.Text);
                this.reportDM.inNKvaND = this.reportDMTT194.inNKvaND = chkNKvaND.Checked;
                if (GlobalSettings.InDinhMucTT196)
                    this.reportDM.BindReport();
                else
                    this.reportDMTT194.BindReport();
            }
            else
            {
                this.reportDM.HopDong = this.reportDMTT194.HopDong = txtHopDong.Text;
                this.reportDM.inNKvaND = this.reportDMTT194.inNKvaND = chkNKvaND.Checked;
                if (GlobalSettings.InDinhMucTT196)
                    this.reportDM.BindReportDinhMucDaDangKy();
                else
                    this.reportDMTT194.BindReportDinhMucDaDangKy();
            }
            if (GlobalSettings.InDinhMucTT196)
            {
                printControl1.PrintingSystem = reportDM.PrintingSystem;

                this.reportDM.CreateDocument();
                reportDM.GetText = new ReportDinhMuc_TT196_2013.GetObjectInfo(SetText);
            }
            else
            {
                printControl1.PrintingSystem = reportDMTT194.PrintingSystem;
                this.reportDMTT194.CreateDocument();
                //reportDMTT194.GetText = new ReportDinhMuc_TT194.GetObjectInfo(SetText);
            }

        }
        private void SetText(string hopdong, string tokhai)
        {
            txtHopDong.Text = hopdong;
            txtToKhai.Text = tokhai;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                if (GlobalSettings.InDinhMucTT196)
                {

                    this.reportDM.CreateDocument();
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                else
                {
                    this.reportDMTT194.CreateDocument();
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (UIComboBoxItem item in cboSanPham.Items)
            {
                reportDM = new ReportDinhMuc_TT196_2013();
                this.reportDM.DMDangKy = this.reportDMTT194.DMDangKy = DMDangKy;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = item.Text;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.Load();
                this.reportDM.MaSP = this.reportDMTT194.MaSP = item.Text;
                this.reportDM.TenSP = this.reportDMTT194.TenSP = sp.Ten;
                this.reportDM.DVT = this.reportDMTT194.DVT = DonViTinh_GetName(sp.DVT_ID);
                GlobalSettings.inNKvaND = chkNKvaND.Checked;
                if (DMDangKy != null)
                {
                    if (GlobalSettings.InDinhMucTT196)
                    {

                        SetText(txtHopDong.Text, txtToKhai.Text);
                        this.reportDM.inNKvaND = chkNKvaND.Checked;
                        this.reportDM.BindReport();
                        this.reportDM.HopDong = txtHopDong.Text;
                    }
                    else
                    {
                        //SetText(txtHopDong.Text, txtToKhai.Text);
                        this.reportDMTT194.inNKvaND = chkNKvaND.Checked;
                        this.reportDMTT194.BindReport();
                        this.reportDMTT194.HopDong = txtHopDong.Text;
                    }
                }
                else
                {
                    if (GlobalSettings.InDinhMucTT196)
                    {
                        this.reportDM.BindReportDinhMucDaDangKy();
                        this.reportDM.inNKvaND = chkNKvaND.Checked;
                    }
                    else
                    {
                        this.reportDMTT194.BindReportDinhMucDaDangKy();
                        this.reportDMTT194.inNKvaND = chkNKvaND.Checked;
                    }
                }
                if (GlobalSettings.InDinhMucTT196)
                {
                    printControl1.PrintingSystem = reportDM.PrintingSystem;
                    this.reportDM.CreateDocument();
                    reportDM.Print();
                }
                else
                {
                    printControl1.PrintingSystem = reportDMTT194.PrintingSystem;
                    this.reportDMTT194.CreateDocument();
                    reportDMTT194.Print();
                }
            }

        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                if (GlobalSettings.InDinhMucTT196)
                {
                    this.reportDM.CreateDocument();
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                else
                {
                    this.reportDMTT194.CreateDocument();
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }


            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (GlobalSettings.InDinhMucTT196)
                reportDM.SetText(txtHopDong.Text, txtToKhai.Text);
            else
                reportDM.SetText(txtHopDong.Text, txtToKhai.Text);
        }
        public enum Type_Print
        {
            TT194, // In theo TT 194
            TT196 // In theo TT 196 - 2013
        }

    }
}