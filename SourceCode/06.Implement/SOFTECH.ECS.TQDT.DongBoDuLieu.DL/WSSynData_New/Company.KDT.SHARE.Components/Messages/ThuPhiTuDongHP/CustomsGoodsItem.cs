﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
  public  class CustomsGoodsItem
    {
        /// <summary>
        /// Mã khoản mục phí.
        /// </summary>
      [XmlElement("tariffCode")]
      public string TariffCode { get; set; }

        /// <summary>
        /// Tên khoản mục phí
        /// </summary>
      [XmlElement("tariffName")]
      public string TariffName { get; set; }

      /// <summary>
      /// Số vận đơn hoặc số container
      /// </summary>
      [XmlElement("billOrContainerNo")]
      public string BillOrContainerNo { get; set; }

        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
      [XmlElement("unitCode")]
      public string UnitCode { get; set; }

        /// <summary>
        /// Tên đơn vị tính
        /// </summary>
      [XmlElement("unitName")]
      public string UnitName { get; set; }

        /// <summary>
        /// Số lượng/ Trọng lượng hàng hóa
        /// </summary>
      [XmlElement("quantityOrGrossMass")]
      public string QuantityOrGrossMass { get; set; }

      /// <summary>
      /// Đơn giá khoản mục phí
      /// </summary>
      [XmlElement("price")]
      public string Price { get; set; }

      /// <summary>
      /// Thành tiền
      /// </summary>
      [XmlElement("amount")]
      public string Amount { get; set; }

      /// <summary>
      /// Diễn giải chi tiết
      /// </summary>
      [XmlElement("journalMemo")]
      public string JournalMemo { get; set; }

      /// <summary>
      /// Số vận đơn
      /// </summary>
      [XmlElement("billOfLading")]
      public string BillOfLading { get; set; }

      /// <summary>
      /// Số container
      /// </summary>
      [XmlElement("container")]
      public string Container { get; set; }

      /// <summary>
      /// Số seal
      /// </summary>
      [XmlElement("seal")]
      public string Seal { get; set; }

      /// <summary>
      /// Loại container Danh mục loại container
      /// 20 - Container 20 feet
      /// 40 - Container 40 feet
      /// 99 - Container loại khác
      /// </summary>
      [XmlElement("containerType")]
      public string ContainerType { get; set; }

      /// <summary>
      /// Tính chất container
      /// 0 - Container thường
      /// 1 - Container hàng khô
      /// 2 - Container hàng lạnh
      /// </summary>
      [XmlElement("containerKind")]
      public string ContainerKind { get; set; }


    }
}
