INSERT INTO t_KDT_ToKhaiMauDich
                      (NgayTiepNhan,NgayDangKy,MaHaiQuan, SoToKhai, MaLoaiHinh, MaDoanhNghiep, TenDoanhNghiep, MaDaiLyTTHQ, TenDaiLyTTHQ, TenDonViDoiTac, ChiTietDonViDoiTac, SoGiayPhep, 
                      NgayGiayPhep, NgayHetHanGiayPhep, SoHopDong, NgayHopDong, NgayHetHanHopDong, SoHoaDonThuongMai, NgayHoaDonThuongMai, PTVT_ID, SoHieuPTVT, 
                      QuocTichPTVT_ID, LoaiVanDon, SoVanDon, NgayVanDon, NuocXK_ID, NuocNK_ID, DiaDiemXepHang, CuaKhau_ID, DKGH_ID, NguyenTe_ID, TyGiaTinhThue, 
                      TyGiaUSD, PTTT_ID, SoHang, SoLuongPLTK, TenChuHang, SoContainer20, SoContainer40, SoKien, TrongLuong, TongTriGiaKhaiBao, TongTriGiaTinhThue, 
                      LoaiToKhaiGiaCong, LePhiHaiQuan, PhiBaoHiem, PhiVanChuyen, PhiXepDoHang, PhiKhac, HeSoNhan, MaDonViUT, TrongLuongNet, SoTienKhoan, NamDK)
SELECT     CONVERT(DATETIME, SXXK.NgayDangKy, 102),CONVERT(DATETIME, SXXK.NgayDangKy, 102),SXXK.MaHaiQuan, SXXK.SoToKhai, SXXK.MaLoaiHinh, SXXK.MaDoanhNghiep, SXXK.TenDoanhNghiep, SXXK.MaDaiLyTTHQ, SXXK.TenDaiLyTTHQ, SXXK.TenDonViDoiTac, SXXK.ChiTietDonViDoiTac, SXXK.SoGiayPhep, 
                      CONVERT(DATETIME, SXXK.NgayGiayPhep, 102) AS Expr1, CONVERT(DATETIME, SXXK.NgayHetHanGiayPhep, 102) AS Expr2, SXXK.SoHopDong, CONVERT(DATETIME, SXXK.NgayHopDong, 102) AS Expr3, 
                      CONVERT(DATETIME, SXXK.NgayHetHanHopDong, 102) AS Expr4, SXXK.SoHoaDonThuongMai, CONVERT(DATETIME, SXXK.NgayHoaDonThuongMai, 102) AS Expr5, SXXK.PTVT_ID, SXXK.SoHieuPTVT, 
                      SXXK.QuocTichPTVT_ID, SXXK.LoaiVanDon, SXXK.SoVanDon, CONVERT(DATETIME, SXXK.NgayVanDon, 102) AS Expr6, SXXK.NuocXK_ID, SXXK.NuocNK_ID, SXXK.DiaDiemXepHang, SXXK.CuaKhau_ID, SXXK.DKGH_ID, 
                      SXXK.NguyenTe_ID, SXXK.TyGiaTinhThue, SXXK.TyGiaUSD, SXXK.PTTT_ID, SXXK.SoHang, SXXK.SoLuongPLTK, SXXK.TenChuHang, SXXK.SoContainer20, SXXK.SoContainer40, SXXK.SoKien, SXXK.TrongLuong, SXXK.TongTriGiaKhaiBao, 
                      SXXK.TongTriGiaTinhThue, SXXK.LoaiToKhaiGiaCong, SXXK.LePhiHaiQuan, SXXK.PhiBaoHiem, SXXK.PhiVanChuyen, SXXK.PhiXepDoHang, SXXK.PhiKhac, SXXK.HesoNhan, SXXK.MaDonViUT, SXXK.TrongLuongNet, 
                      SXXK.SoTienKhoan, SXXK.NamDangKy
FROM         t_SXXK_ToKhaiMauDich AS SXXK left outer join t_KDT_ToKhaiMauDich as KDT
				ON SXXK.MaHaiQuan = KDT.MaHaiQuan AND SXXK.MaLoaiHinh = KDT.MaLoaiHinh AND 
				SXXK.SoToKhai = KDT.SoToKhai AND SXXK.MaDoanhNghiep = KDT.MaDoanhNghiep AND
				SXXK.NamDangKy = KDT.NamDK
Where KDT.SoToKhai is null