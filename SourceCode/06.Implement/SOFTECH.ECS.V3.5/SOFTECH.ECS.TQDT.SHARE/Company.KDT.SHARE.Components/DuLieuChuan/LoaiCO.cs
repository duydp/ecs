﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public partial class LoaiCO
    {

        public static string GetName(string id)
        {
            try
            {
                string query = string.Format("[Ma] = '{0}'", id.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["LoaiCO"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

    }
}
