﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.VNACCS;
using Company.BLL.KDT;

namespace Company.Interface.KDT.SXXK
{
    public partial class PhuKienSuaMaHang : BaseForm
    {
        public PhuKienSuaMaHang()
        {
            InitializeComponent();
                    }
        public KDT_SXXK_LoaiPhuKien loaiPK = new KDT_SXXK_LoaiPhuKien();
        //public List<KDT_SXXK_PhuKienDetail> loaiPK.ListPhuKienDeTail = new List<KDT_SXXK_PhuKienDetail>();
        private KDT_SXXK_PhuKienDetail detail = new KDT_SXXK_PhuKienDetail();
        public long TKMD_ID;
        public string SoTKVNACCS;
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetPhuKien()
        {
            if (detail == null)
                detail = new KDT_SXXK_PhuKienDetail();
            txtSoTT.Value = detail.STTHang;
            txtMaHangCu.Text = detail.MaHangCu;
            txtMaHangMoi.Text = detail.MaHangMoi;
            ctrMaHQCu.Code = detail.MaHQDangKyCu;
            ctrMaHQMoi.Code = detail.MaHQDangKyMoi;
            cmbPhanLoaiCu.SelectedValue = detail.LoaiHangCu;
            cmbPhanLoaiMoi.SelectedValue = detail.LoaiHangMoi;
        }

        private void SetPhuKien()
        {
            if (detail == null)
                detail = new KDT_SXXK_PhuKienDetail();
            detail.STTHang = Convert.ToInt32(txtSoTT.Value);
            detail.MaHangCu = txtMaHangCu.Text;
            detail.MaHangMoi = txtMaHangMoi.Text;
            detail.MaHQDangKyCu = ctrMaHQCu.Code;
            detail.MaHQDangKyMoi = ctrMaHQMoi.Code;
            detail.LoaiHangCu = cmbPhanLoaiCu.SelectedValue==null? "": cmbPhanLoaiCu.SelectedValue.ToString();
            detail.LoaiHangMoi = cmbPhanLoaiMoi.SelectedValue == null ? "" : cmbPhanLoaiMoi.SelectedValue.ToString();
            detail.Master_ID = loaiPK.ID;
            
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
            
            if (string.IsNullOrEmpty(txtSoTT.Text) || Convert.ToInt32(txtSoTT.Value) == 0)
            {
                this.ShowMessage("Vui lòng chọn số thứ tự hàng muốn khai sửa đổi", false);
                return;
            }
           
            detail = loaiPK.ListPhuKienDeTail.Find(x => Convert.ToInt32(x.STTHang) == Convert.ToInt32(txtSoTT.Value));

            if (detail != null )
            {
                SetPhuKien();
                if (detail.ID > 0)
                    detail.Update();
                else
                    detail.Insert();
            }
            else
            {
                SetPhuKien();
                detail.Insert();
                loaiPK.ListPhuKienDeTail.Add(detail);
            }
            grvHang.DataSource = loaiPK.ListPhuKienDeTail;
            grvHang.Refetch();
            detail = new KDT_SXXK_PhuKienDetail();
            GetPhuKien();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void txtSoTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (SoTKVNACCS.Length > 10)
                {
                    KDT_VNACC_ToKhaiMauDich tkmd = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(SoTKVNACCS);

                    if (tkmd == null)
                    {
                        this.ShowMessage("Số tờ khai VNACCS không tồn tại trên hệ thống. Vui lòng kiểm tra lại", false);
                        return;
                    }
                    VNACC_ListHangHMD fhmd = new VNACC_ListHangHMD(cbxChonNhieuHang.Checked ? VNACC_ListHangHMD.SelectTion.MultiSelect : VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { tkmd.ID });
                    fhmd.ShowDialog(this);
                    if (fhmd.DialogResult == DialogResult.OK)
                    {
                        if (fhmd.SelectMode== VNACC_ListHangHMD.SelectTion.MultiSelect)
                        {
                            foreach (KDT_VNACC_HangMauDich item in fhmd.listHangMD)
                            {
                                detail = loaiPK.ListPhuKienDeTail.Find(x => Convert.ToInt32(x.STTHang) == Convert.ToInt32(item.SoDong));
                                if (detail == null || detail.ID == 0)
                                {
                                    detail = new KDT_SXXK_PhuKienDetail();
                                    detail.STTHang = Convert.ToInt16(item.SoDong);
                                    detail.MaHangCu = item.MaHangHoa;
                                    detail.MaHQDangKyCu = GlobalSettings.MA_HAI_QUAN_VNACCS;
                                    detail.MaHangMoi = item.MaHangHoa;
                                    detail.MaHQDangKyMoi = GlobalSettings.MA_HAI_QUAN_VNACCS;
                                    if (tkmd.LoaiHang != null)
                                    {
                                        if (tkmd.LoaiHang.Trim() == "N")
                                        {
                                            detail.LoaiHangCu = "1";
                                            detail.LoaiHangMoi = "1";
                                        }
                                        else if (tkmd.LoaiHang.Trim() == "S")
                                        {
                                            detail.LoaiHangCu = "2";
                                            detail.LoaiHangMoi = "2";
                                        }
                                        else
                                        {
                                            detail.LoaiHangCu = "";
                                            detail.LoaiHangMoi = "";
                                        }
                                    }
                                    detail.Master_ID = loaiPK.ID;
                                    detail.Insert();
                                    loaiPK.ListPhuKienDeTail.Add(detail);
                                }
                            }
                        }
                        else
                        {
                        detail = convertFromHangVNACCS(fhmd.HangMD);
                        GetPhuKien();
                        }
                        grvHang.DataSource = loaiPK.ListPhuKienDeTail;
                        grvHang.Refetch();
                    }
                }
                else
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    tk.ID = TKMD_ID;
                    tk.Load();
                    SelectHangMauDichForm frm = new SelectHangMauDichForm();
                    frm.TKMD = tk;
                    frm.ShowDialog();
                    foreach (HangMauDich item in frm.HMDTMPCollection)
                    {
                        detail = loaiPK.ListPhuKienDeTail.Find(x => Convert.ToInt32(x.STTHang) == Convert.ToInt32(item.SoThuTuHang));
                        if (detail==null ||detail.ID == 0)
                        {
                            detail = new KDT_SXXK_PhuKienDetail();
                            detail.STTHang = item.SoThuTuHang;
                            detail.MaHangCu = item.MaPhu;
                            detail.MaHQDangKyCu = GlobalSettings.MA_HAI_QUAN_VNACCS;
                            detail.MaHangMoi = item.MaPhu;
                            detail.MaHQDangKyMoi = GlobalSettings.MA_HAI_QUAN_VNACCS;
                            detail.Master_ID = loaiPK.ID;
                            detail.Insert();
                            loaiPK.ListPhuKienDeTail.Add(detail);
                        }
                    }
                    grvHang.DataSource = loaiPK.ListPhuKienDeTail;
                    grvHang.Refetch();
                                     
                }



            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private KDT_SXXK_PhuKienDetail convertFromHangVNACCS(KDT_VNACC_HangMauDich hmd)
        {
            KDT_SXXK_PhuKienDetail newdetail = new KDT_SXXK_PhuKienDetail()
            {
                STTHang = string.IsNullOrEmpty(hmd.SoDong) ? 0 : System.Convert.ToInt16(hmd.SoDong),
                MaHangCu = hmd.MaHangHoa,
                MaHangMoi = hmd.MaHangHoa,
                MaHQDangKyMoi = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(GlobalSettings.MA_HAI_QUAN)
            };
            if (!string.IsNullOrEmpty(hmd.MaQuanLy) && hmd.MaQuanLy.Length > 5)
            {
                newdetail.LoaiHangCu = hmd.MaQuanLy.Substring(0, 1);
                newdetail.MaHQDangKyCu = hmd.MaQuanLy.Substring(1, 6);
            }
            return newdetail;

        }

        private void grvHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
            
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                detail = (KDT_SXXK_PhuKienDetail)e.Row.DataRow;
            GetPhuKien();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grvHang_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
           
             Janus.Windows.GridEX.GridEXSelectedItemCollection items = grvHang.SelectedItems;
            if (items.Count > 0)
            {
                if (ShowMessage("Bạn có muốn xóa các dòng hàng này không?", true) == "Yes")
                {
                    foreach (Janus.Windows.GridEX.GridEXSelectedItem item in items)
                    {
                        if(item.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                KDT_SXXK_PhuKienDetail pkdetail = (KDT_SXXK_PhuKienDetail)item.GetRow().DataRow;
                                if (detail.ID > 0)
                                    detail.Delete();
                                loaiPK.ListPhuKienDeTail.RemoveAt(loaiPK.ListPhuKienDeTail.FindIndex(x => x.STTHang == detail.STTHang));
                            }
                    }
                }
                grvHang.DataSource = loaiPK.ListPhuKienDeTail;
                grvHang.Refetch();
            }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void PhuKienSuaMaHang_Load(object sender, EventArgs e)
        {
            if (loaiPK == null)
                loaiPK = new KDT_SXXK_LoaiPhuKien();
            else
            {
                grvHang.DataSource = loaiPK.ListPhuKienDeTail;
            }
           
            
        }
        
    }
}
