using System.Text;
using System.Security.Cryptography;
using System.Xml;
using System;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components.Utils;

namespace Company.GC.BLL
{
    public class WebServiceConnection
    {
       

        public static string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }
        private static string ConfigPhongBi(int type, int function, string maHQ, string MaDV)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(maHQ);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = MaDV;

            return doc.InnerXml;
        }

        public static string DoiMatKhau(string user, string passOld, string passNew, string maHQ, string maDV)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(0, MessgaseFunction.DoiMatKhau, maHQ, maDV));
            XmlDocument docNPL = new XmlDocument();
            docNPL.Load("B03GiaCong\\DoimatKhau.xml");
            docNPL.SelectSingleNode("Root/DU_LIEU").Attributes["TEN_TRUY_NHAP"].Value = user;
            docNPL.SelectSingleNode("Root/DU_LIEU").Attributes["MAT_KHAU_CU"].Value = GetMD5Value(maDV + "+" + passOld);
            docNPL.SelectSingleNode("Root/DU_LIEU").Attributes["MAT_KHAU_MOI"].Value = GetMD5Value(maDV + "+" + passNew);

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            
            string kq = kdt.Request(doc.InnerXml, GetMD5Value(maDV + "+" + passOld));
            docNPL = new XmlDocument();
            docNPL.LoadXml(kq);
            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return "";
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return kq;
        }

       
        public static void sendThongTinDN(string MaDoanhNghiep, string TenDoanhNghiep, string DiaChi, string GhiChu, string LoaiHinh)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                XmlElement madn = doc.CreateElement("MaDoanhNghiep");
                madn.InnerText = MaDoanhNghiep;
                XmlElement tendn = doc.CreateElement("TenDoanhNghiep");
                tendn.InnerText = TenDoanhNghiep;
                XmlElement diachidn = doc.CreateElement("DiaChi");
                diachidn.InnerText = DiaChi;
                XmlElement loaihinhdn = doc.CreateElement("LoaiHinh");
                loaihinhdn.InnerText = LoaiHinh;
                XmlElement ghichudn = doc.CreateElement("GhiChu");
                ghichudn.InnerText = GhiChu;

                root.AppendChild(madn);
                root.AppendChild(tendn);
                root.AppendChild(diachidn);
                root.AppendChild(loaihinhdn);
                root.AppendChild(ghichudn);
                doc.AppendChild(root);
                WS.AutoUpdate.Update a = new Company.GC.BLL.WS.AutoUpdate.Update();
                a.Activate(doc.InnerXml, GetMD5Value("ECSHCM12345ABCD"));
            }
            catch { }
        }
        public static string checkKhaiBao(string MaDoanhNghiep, string tenDoanhNghiep, string diaChi, string maHQ)
        {
            WS.AutoUpdate.Update u = new Company.GC.BLL.WS.AutoUpdate.Update();
            string st = u.CheckDoanhNghiepKhaiBao(MaDoanhNghiep, tenDoanhNghiep, diaChi, maHQ, GetMD5Value("ECSHCM12345ABCD"));
            return st;
        }
    }
}