﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.Interface.VNACCS;
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class VNACC_TKBoSung_ThueHangHoaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
        public long TKMD_ID { get; set; }
        public VNACC_TKBoSung_ThueHangHoaForm()
        {
            InitializeComponent();

            base.SetHandler(this);

            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.AMA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                cmdInTamAMA.Visible = cmdInTamAMA1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                SetMaxLengthControl();
                // Hai quan
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ucCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                if (TKBoSung.ID != 0)
                {
                    TKMD_ID = KDT_VNACC_ToKhaiMauDich.GetID(TKBoSung.SoToKhai, TKBoSung.NgayKhaiBao.Year);
                    SetTKSuaDoi();
                }
                else if (TKBoSung.SoToKhai != 0)
                {
                    SetTKSuaDoi();
                }

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();
                ucCoQuanHaiQuan_Leave(null, null);
                TuDongCapNhatThongTin();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void setCommandStatus()
        {
            if (TKBoSung.TrangThaiXuLy == Convert.ToInt16(EnumTrangThaiXuLy.ChuaKhaiBao) || TKBoSung.TrangThaiXuLy == null )
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAMA.Enabled = cmdAMA1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdAMC.Enabled = cmdAMC1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendVoucher.Enabled = cmdSendVoucher1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdate.Enabled = cmdUpdate1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                this.OpenType = OpenFormType.Edit;
            }
            else if (TKBoSung.TrangThaiXuLy == Convert.ToInt16(EnumTrangThaiXuLy.KhaiBaoTam))
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAMA.Enabled = cmdAMA1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAMC.Enabled = cmdAMC1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSendVoucher.Enabled = cmdSendVoucher1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdate.Enabled = cmdUpdate1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                this.OpenType = OpenFormType.Edit;
            }
            else if (TKBoSung.TrangThaiXuLy == Convert.ToInt16(EnumTrangThaiXuLy.KhaiBaoChinhThuc))
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAMA.Enabled = cmdAMA1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAMC.Enabled = cmdAMC1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSendVoucher.Enabled = cmdSendVoucher1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdate.Enabled = cmdUpdate1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.View;

            }
            else if (TKBoSung.TrangThaiXuLy == Convert.ToInt16(EnumTrangThaiXuLy.ThongQuan))
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAMA.Enabled = cmdAMA1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAMC.Enabled = cmdAMC1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendVoucher.Enabled = cmdSendVoucher1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdUpdate.Enabled = cmdUpdate1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.View;
            }

        }
        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowKetQuaTraVe();
                    break;
                case "cmdAMC":
                    SendVnaccsIDC(false);
                    break;
                case "cmdAMA":
                    SendVnaccs(false);
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdInTamAMA":
                    ShowInTamAMA();
                    break;
                case "cmdUpdate":
                    UpdateAMAToVNACSS();
                    break;
                case "cmdSendVoucher":
                    SendVouchers();
                    break;
            }
        }

        private void SendVouchers()
        {
            VNACC_VouchersForm f = new VNACC_VouchersForm();
            f.FormType = "TKBS";
            f.TKBoSung = TKBoSung;
            f.ShowDialog(this);
        }

        private void UpdateAMAToVNACSS()
        {
            try
            {
                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(TKMD_ID);
                TKMD.SoQuanLyNoiBoDN = TKBoSung.SoQuanLyTrongNoiBoDoanhNghiep;
                TKMD.Update();
                TKMD.HangCollection = KDT_VNACC_HangMauDich.SelectCollectionDynamic("TKMD_ID = " + TKMD_ID, "");
                List<KDT_VNACC_TK_PhanHoi_TyGia> TyGiaCollection = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID = " + TKMD_ID, "");
                int STT;
                string MaHangHoaTruoc = "";
                string MaHangHoaSau = "";
                string TenHangHoaTruoc = "";
                string TenHangHoaSau = "";
                decimal SoLuongTruoc = 0;
                decimal SoLuongSau = 0;
                decimal TriGiaTruoc = 0;
                decimal TriGiaSau = 0;
                string MoTaTruoc = "";
                string MoTaSau = "";
                string MaHSTruoc = "";
                string MaHSSau = "";
                string DVTTruoc = "";
                string DVTSau = "";
                string NuocXXTruoc = "";
                string NuocXXSau = "";
                string MaHangHoaDelete = "";
                string TenHangHoaDelete = "";
                decimal SoLuongDelete = 0;
                decimal TriGiaDelete = 0;
                string MaHSDelete = "";
                string DVTDelete = "";
                string NuocXXDelete = "";
                foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hmdBS in TKBoSung.HangCollection)
                {
                    STT = Convert.ToInt32(hmdBS.SoThuTuDongHangTrenToKhaiGoc.ToString());
                    MoTaTruoc = hmdBS.MoTaHangHoaTruocKhiKhaiBoSung;
                    MoTaSau = hmdBS.MoTaHangHoaSauKhiKhaiBoSung;

                    SoLuongTruoc = hmdBS.SoLuongTinhThueTruocKhiKhaiBoSung;
                    SoLuongSau = hmdBS.SoLuongTinhThueSauKhiKhaiBoSung;

                    TriGiaTruoc = hmdBS.TriGiaTinhThueTruocKhiKhaiBoSung;
                    TriGiaSau = hmdBS.TriGiaTinhThueSauKhiKhaiBoSung;

                    MaHSTruoc = hmdBS.MaSoHangHoaTruocKhiKhaiBoSung;
                    MaHSSau = hmdBS.MaSoHangHoaSauKhiKhaiBoSung;

                    DVTTruoc = hmdBS.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung;
                    DVTSau = hmdBS.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung;

                    NuocXXTruoc = hmdBS.MaNuocXuatXuTruocKhiKhaiBoSung;
                    NuocXXSau = hmdBS.MaNuocXuatXuSauKhiKhaiBoSung;
                    if (MoTaTruoc.Contains("#&"))
                    {
                        string[] temp = MoTaTruoc.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            MaHangHoaTruoc = temp[0];
                            TenHangHoaTruoc = temp[1];
                        }

                    }
                    else
                    {
                        MaHangHoaTruoc = String.Empty;
                        TenHangHoaTruoc = String.Empty;
                    }

                    if (MoTaSau.Contains("#&"))
                    {
                        string[] temp = MoTaSau.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            MaHangHoaSau = temp[0];
                            TenHangHoaSau = temp[1];
                        }
                    }
                    else
                    {
                        MaHangHoaSau = String.Empty;
                        TenHangHoaSau = String.Empty;
                    }
                    foreach (KDT_VNACC_HangMauDich hmdVNACC in TKMD.HangCollection)
                    {
                        // Sửa dòng hàng
                        int SoDong = Convert.ToInt32(hmdVNACC.SoDong);
                        if (STT == SoDong && MaHangHoaSau != String.Empty)
                        {
                            try
                            {
                                hmdVNACC.MaHangHoa = MaHangHoaSau;
                                hmdVNACC.MaSoHang = MaHSSau;
                                hmdVNACC.TenHang = TenHangHoaSau;
                                hmdVNACC.SoLuong1 = SoLuongSau;
                                hmdVNACC.DVTLuong1 = DVTSau;
                                hmdVNACC.DVTLuong2 = DVTSau;
                                if (SoLuongSau != SoLuongTruoc)
                                {
                                    if (TriGiaSau != TriGiaTruoc)
                                    {
                                        if (TyGiaCollection.Count > 0)
                                        {
                                            hmdVNACC.DonGiaHoaDon = (TriGiaSau / TyGiaCollection[0].TyGiaTinhThue) / SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau / TyGiaCollection[0].TyGiaTinhThue;
                                            hmdVNACC.MaTTDonGia = TyGiaCollection[0].MaTTTyGiaTinhThue.ToString().Trim();
                                        }
                                        else
                                        {
                                            hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau;
                                            hmdVNACC.MaTTDonGia = "VND";
                                        }
                                    }
                                    else
                                    {
                                        if (TyGiaCollection.Count > 0)
                                        {
                                            hmdVNACC.DonGiaHoaDon = (TriGiaSau / TyGiaCollection[0].TyGiaTinhThue) / SoLuongSau;
                                        }
                                        else
                                        {
                                            hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                        }
                                    }
                                }
                                else if (TriGiaSau !=TriGiaTruoc)
                                {
                                    if (SoLuongSau != SoLuongTruoc)
                                    {
                                        if (TyGiaCollection.Count > 0)
                                        {
                                            hmdVNACC.DonGiaHoaDon = (TriGiaSau / TyGiaCollection[0].TyGiaTinhThue) / SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau / TyGiaCollection[0].TyGiaTinhThue;
                                            hmdVNACC.MaTTDonGia = TyGiaCollection[0].MaTTTyGiaTinhThue.ToString().Trim();
                                        }
                                        else
                                        {
                                            hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau;
                                            hmdVNACC.MaTTDonGia = "VND";
                                        }
                                    }
                                    else
                                    {
                                        if (TyGiaCollection.Count > 0)
                                        {
                                            hmdVNACC.DonGiaHoaDon = (TriGiaSau / TyGiaCollection[0].TyGiaTinhThue) / SoLuongSau;
                                        }
                                        else
                                        {
                                            hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                        }
                                    }
                                }
                                hmdVNACC.DVTDonGia = DVTSau;
                                hmdVNACC.DonGiaTinhThue = TriGiaSau / SoLuongSau;
                                hmdVNACC.TriGiaTinhThue = TriGiaSau;
                                hmdVNACC.TriGiaTinhThueS = TriGiaSau;
                                hmdVNACC.NuocXuatXu = NuocXXSau;
                                //hmdVNACC.ThueThuKhacCollection = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id= "+ hmdVNACC.ID ,"");
                                //foreach (KDT_VNACC_HangMauDich_ThueThuKhac hmdThueKhac in hmdVNACC.ThueThuKhacCollection)
                                //{

                                //}
                                hmdVNACC.Update();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        // Xóa dòng hàng
                        else if (STT == SoDong && MaHangHoaSau == String.Empty || STT == SoDong && MaHangHoaSau.ToUpper() == "N/A")
                        {
                            try
                            {
                                MaHangHoaDelete = hmdVNACC.MaHangHoa;
                                TenHangHoaDelete = hmdVNACC.TenHang;
                                MaHSDelete = hmdVNACC.MaSoHang;
                                SoLuongDelete = hmdVNACC.SoLuong1;
                                DVTDelete = hmdVNACC.DVTLuong1;
                                TriGiaDelete = hmdVNACC.TriGiaHoaDon;
                                NuocXXDelete = hmdVNACC.NuocXuatXu;
                                hmdVNACC.Delete();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        //Thêm dòng hàng mới
                        else if (STT != SoDong && MaHangHoaTruoc == String.Empty || STT != SoDong && MaHangHoaTruoc.ToUpper() == "N/A")
                        {
                            try
                            {
                                List<KDT_VNACC_HangMauDich> HangCollectionCheck = KDT_VNACC_HangMauDich.SelectCollectionDynamic("TKMD_ID = " + TKMD_ID, "");
                                foreach (KDT_VNACC_HangMauDich item in HangCollectionCheck)
                                {
                                    if (item.MaHangHoa ==MaHangHoaDelete)
                                    {
                                        HangCollectionCheck.Remove(item);
                                    }
                                }
                                foreach (KDT_VNACC_HangMauDich hmdCheck in HangCollectionCheck)
                                {
                                    if (SoDong!=Convert.ToInt32(hmdCheck.SoDong) && MaHangHoaSau !=hmdCheck.MaHangHoa && TenHangHoaSau!=hmdCheck.TenHang && SoLuongSau!=hmdCheck.SoLuong1 && MaHSSau!=hmdCheck.MaSoHang && DVTSau!=hmdCheck.DVTLuong1)
                                    {
                                        hmdVNACC.TKMD_ID = TKMD.ID;
                                        hmdVNACC.MaHangHoa = MaHangHoaSau;
                                        hmdVNACC.MaSoHang = MaHSSau;
                                        hmdVNACC.TenHang = TenHangHoaSau;
                                        hmdVNACC.SoLuong1 = SoLuongSau;
                                        hmdVNACC.SoLuong2 = SoLuongSau;
                                        if (TyGiaCollection.Count > 0)
                                        {
                                            hmdVNACC.DonGiaHoaDon = (TriGiaSau / TyGiaCollection[0].TyGiaTinhThue)/ SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau/TyGiaCollection[0].TyGiaTinhThue;
                                            hmdVNACC.MaTTDonGia = TyGiaCollection[0].MaTTTyGiaTinhThue.ToString().Trim();
                                        }
                                        else
                                        {
                                            hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                            hmdVNACC.TriGiaHoaDon = TriGiaSau;
                                            hmdVNACC.MaTTDonGia = "VND";
                                        }
                                        hmdVNACC.DVTDonGia = DVTSau;
                                        hmdVNACC.DonGiaHoaDon = TriGiaSau / SoLuongSau;
                                        hmdVNACC.TriGiaHoaDon = TriGiaSau;
                                        hmdVNACC.DVTLuong1 = DVTSau;
                                        hmdVNACC.DVTLuong2 = DVTSau;
                                        hmdVNACC.DV_SL_TrongDonGiaTinhThue = DVTSau;
                                        hmdVNACC.DonGiaTinhThue = TriGiaSau / SoLuongSau;
                                        hmdVNACC.TriGiaTinhThue = TriGiaSau;
                                        hmdVNACC.TriGiaTinhThueS = TriGiaSau;
                                        hmdVNACC.NuocXuatXu = NuocXXSau;
                                        hmdVNACC.SoDong = hmdBS.SoThuTuDongHangTrenToKhaiGoc;
                                        hmdVNACC.Insert();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                }
#if GC_V4
                if (GlobalSettings.MA_DON_VI == "4000395355")
                {
                    decimal sotk = System.Convert.ToDecimal(TKMD.SoToKhai.ToString().Substring(0, 11));
                    if (TKMD.MaLoaiHinh.Contains("E23") || TKMD.MaLoaiHinh.Contains("E54"))
                    {
                        Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.LoadToKhaiDaChuyenDoiVNACC(sotk);
                        if (tkct.MaLoaiHinh.Contains("X"))
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                            {
                                ShowMessageTQDT("Tờ khai chuyển tiếp xuất  : " + TKMD.SoToKhai + " này đã được phân bổ nên không chỉnh sửa dữ liệu được.Doanh nghiệp hãy xoá phân bổ trước khi cập nhật cho tờ khai này .", false);
                            }
                        }
                        else if (tkct.MaLoaiHinh.Substring(0, 1) == "N")
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)tkct.SoToKhai, tkct.MaLoaiHinh, tkct.MaHaiQuanTiepNhan, (short)tkct.NgayDangKy.Year, tkct.IDHopDong))
                            {
                                ShowMessageTQDT("Tờ khai chuyển tiếp nhập  : " + TKMD.SoToKhai + " này đã được phân bổ nên không chỉnh sửa dữ liệu được.Doanh nghiệp hãy xoá phân bổ trước khi cập nhật cho tờ khai này .", false);
                            }
                        }
                    }
                    else
                    {
                        Company.GC.BLL.KDT.ToKhaiMauDich tkmd = Company.GC.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                        if (tkmd.MaLoaiHinh.Contains("XGC"))
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
                            {
                                ShowMessageTQDT("Tờ khai  : " + TKMD.SoToKhai + " này đã được phân bổ nên không chỉnh sửa dữ liệu được.Doanh nghiệp hãy xoá phân bổ trước khi cập nhật cho tờ khai này .", false);
                            }
                        }
                        else if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, TKMD.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year, tkmd.IDHopDong))
                            {
                                ShowMessageTQDT("Tờ khai  : " + TKMD.SoToKhai + " này đã được phân bổ nên không chỉnh sửa dữ liệu được.Doanh nghiệp hãy xoá phân bổ trước khi cập nhật cho tờ khai này .", false);
                            }
                        }


                    }
                }
                Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(TKMD, GlobalSettings.MA_DON_VI != "4000395355");
#elif SXXK_V4
                Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(TKMD);
#endif
                ShowMessage("Cập nhật thành công !",false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ShowInTamAMA()
        {
            try
            {
                MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKBoSung.InputMessageID, "AMA");
                if (msg != null && msg.ID > 0)
                    ProcessReport.ShowReport(msg.ID, "VAL8000", new ResponseVNACCS(msg.Log_Messages).ResponseData);
                else
                    ShowMessage("Không tìm thấy bản khai tạm", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKBoSung.ID, "TKBoSung");
                //f.master_id = TKMD.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowChiThiHaiQuan()
        {
            try
            {
                VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
                f.Master_ID = TKBoSung.ID;
                f.LoaiThongTin = ELoaiThongTin.TK_KhaiBoSung;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

      
              


     
        private void ShowThemHang()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                VNACC_TKBoSung_ThueHangHoa_HangForm f = new VNACC_TKBoSung_ThueHangHoa_HangForm();
                f.TKBoSung = TKBoSung;
                f.IDTKMD = this.TKMD_ID;
                f.OpenType = this.OpenType;
                f.ShowDialog();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình:\r\n " + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                GetTKsuaDoi();

                TKBoSung.InsertUpdateFull();
                ShowMessage("Lưu tờ khai thành công", false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi lưu tờ khai:\r\n " +ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetTKsuaDoi()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                TKBoSung.SoToKhaiBoSung = Convert.ToDecimal(txtSoToKhaiBoSung.Value);
                TKBoSung.CoQuanHaiQuan = ucCoQuanHaiQuan.Code;
                TKBoSung.NhomXuLyHoSo = ucNhomXuLyHoSo.Code;
                TKBoSung.PhanLoaiXuatNhapKhau = txtPhanLoaiXuatNhapKhau.Text;
                TKBoSung.SoToKhai = Convert.ToDecimal(txtSoToKhai.Value);
                TKBoSung.MaLoaiHinh = ucMaLoaiHinh.Code;
                TKBoSung.NgayKhaiBao = clcNgayKhaiBao.Value;
                TKBoSung.NgayCapPhep = clcNgayCapPhep.Value;
                TKBoSung.ThoiHanTaiNhapTaiXuat = clcThoiHanTaiNhapTaiXuat.Value;
                TKBoSung.MaNguoiKhai = txtMaNguoiKhai.Text;
                TKBoSung.TenNguoiKhai = txtTenNguoiKhai.Text;
                TKBoSung.MaBuuChinh = txtMaBuuChinhNguoiKhai.Text;
                TKBoSung.DiaChiNguoiKhai = txtDiaChiNguoiKhai.Text;
                TKBoSung.SoDienThoaiNguoiKhai = txtSoDienThoaiNguoiKhai.Text;
                TKBoSung.MaLyDoKhaiBoSung = ucMaLyDoKhaiBoSung.Code;
                TKBoSung.MaTienTeTienThue = ucMaTienTeTienThue.Code;
                TKBoSung.MaNganHangTraThueThay = txtMaNganHangTraThueThay.Text;
                TKBoSung.NamPhatHanhHanMuc = Convert.ToDecimal(txtNamPhatHanhHanMuc.Value);
                TKBoSung.KiHieuChungTuPhatHanhHanMuc = txtKiHieuChungTuPhatHanhHanMuc.Text;
                TKBoSung.SoChungTuPhatHanhHanMuc = txtSoChungPhatHanhHanMuc.Text;
                TKBoSung.MaXacDinhThoiHanNopThue = txtMaXacDinhThoiHanNopThue.Text;
                TKBoSung.MaNganHangBaoLanh = txtMaNganHangBaoLanh.Text;
                TKBoSung.NamPhatHanhBaoLanh = Convert.ToDecimal(txtNamPhatHanh.Value);
                TKBoSung.KyHieuPhatHanhChungTuBaoLanh = txtKyHieuChungTuBaoLanh.Text;
                TKBoSung.SoHieuPhatHanhChungTuBaoLanh = txtSoChungTuBaoLanh.Text;
                TKBoSung.MaTienTeTruocKhiKhaiBoSung = ucMaTienTeTruocKhiKhaiBoSung.Code;
                TKBoSung.TyGiaHoiDoaiTruocKhiKhaiBoSung = Convert.ToDecimal(txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value);
                TKBoSung.MaTienTeSauKhiKhaiBoSung = ucMaTienTeSauKhiKhaiBoSung.Code;
                TKBoSung.TyGiaHoiDoaiSauKhiKhaiBoSung = Convert.ToDecimal(txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value);
                TKBoSung.SoQuanLyTrongNoiBoDoanhNghiep = txtSoQuanLyTrongNoiBoDoanhNghiep.Text;
                TKBoSung.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text;
                TKBoSung.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi mở tờ khai :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetTKSuaDoi()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtSoToKhaiBoSung.Value = TKBoSung.SoToKhaiBoSung;
                ucCoQuanHaiQuan.Code = TKBoSung.CoQuanHaiQuan;
                ucNhomXuLyHoSo.Code = TKBoSung.NhomXuLyHoSo;
                txtPhanLoaiXuatNhapKhau.Text = TKBoSung.PhanLoaiXuatNhapKhau;
                txtSoToKhai.Value = TKBoSung.SoToKhai;
                ucMaLoaiHinh.Code = TKBoSung.MaLoaiHinh;
                clcNgayKhaiBao.Value = TKBoSung.NgayKhaiBao;
                clcNgayCapPhep.Value = TKBoSung.NgayCapPhep;
                clcNgayCapPhep.Text = TKBoSung.NgayCapPhep.ToString();
                clcThoiHanTaiNhapTaiXuat.Value = TKBoSung.ThoiHanTaiNhapTaiXuat;
                //dtThoiHanTaiNhapTaiXuat.Text = TKBoSung.ThoiHanTaiNhapTaiXuat.ToString();
                txtMaNguoiKhai.Text = TKBoSung.MaNguoiKhai;
                txtTenNguoiKhai.Text = TKBoSung.TenNguoiKhai;
                txtMaBuuChinhNguoiKhai.Text = TKBoSung.MaBuuChinh;
                txtDiaChiNguoiKhai.Text = TKBoSung.DiaChiNguoiKhai;
                txtSoDienThoaiNguoiKhai.Text = TKBoSung.SoDienThoaiNguoiKhai;
                ucMaLyDoKhaiBoSung.Code = TKBoSung.MaLyDoKhaiBoSung;
                ucMaTienTeTienThue.Code = TKBoSung.MaTienTeTienThue;
                txtMaNganHangTraThueThay.Text = TKBoSung.MaNganHangTraThueThay;
                txtNamPhatHanhHanMuc.Value = TKBoSung.NamPhatHanhHanMuc;
                txtKiHieuChungTuPhatHanhHanMuc.Text = TKBoSung.KiHieuChungTuPhatHanhHanMuc;
                txtSoChungPhatHanhHanMuc.Text = TKBoSung.SoChungTuPhatHanhHanMuc;
                txtMaXacDinhThoiHanNopThue.Text = TKBoSung.MaXacDinhThoiHanNopThue;
                txtMaNganHangBaoLanh.Text = TKBoSung.MaNganHangBaoLanh;
                txtNamPhatHanh.Value = TKBoSung.NamPhatHanhBaoLanh;
                txtKyHieuChungTuBaoLanh.Text = TKBoSung.KyHieuPhatHanhChungTuBaoLanh;
                txtSoChungTuBaoLanh.Text = TKBoSung.SoHieuPhatHanhChungTuBaoLanh;
                ucMaTienTeTruocKhiKhaiBoSung.Code = TKBoSung.MaTienTeTruocKhiKhaiBoSung;
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value = TKBoSung.TyGiaHoiDoaiTruocKhiKhaiBoSung;
                ucMaTienTeSauKhiKhaiBoSung.Code = TKBoSung.MaTienTeSauKhiKhaiBoSung;
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value = TKBoSung.TyGiaHoiDoaiSauKhiKhaiBoSung;
                txtSoQuanLyTrongNoiBoDoanhNghiep.Text = TKBoSung.SoQuanLyTrongNoiBoDoanhNghiep;
                if (string.IsNullOrEmpty(TKBoSung.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung))
                    txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text = KDT_VNACC_ToKhaiMauDich.Load(TKMD_ID).GhiChu;
                else
                    txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text = TKBoSung.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung;
                if (string.IsNullOrEmpty(TKBoSung.GhiChuNoiDungLienQuanSauKhiKhaiBoSung))
                    txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text = txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text;
                else
                    txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text = TKBoSung.GhiChuNoiDungLienQuanSauKhiKhaiBoSung;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi mở tờ khai :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucMaQuocGia_EditValueChanged(object sender, EventArgs e)
        {
            //ucCuaKhauXuatNhap1.CountryCode = ucMaQuocGia.Code;
            //ucCuaKhauXuatNhap1.Code = "";
            //ucCuaKhauXuatNhap1.ReLoadData();
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhaiBoSung.Tag = "SYN"; //Số tờ khai bổ sung
                ucCoQuanHaiQuan.TagName = "CHS"; //Cơ quan Hải quan
                ucNhomXuLyHoSo.TagName = "CHB"; //Nhóm xử lý hồ sơ
                txtPhanLoaiXuatNhapKhau.Tag = "YNK"; //Phân loại xuất nhập khẩu
                txtSoToKhai.Tag = "ICN"; //Số tờ khai
                ucMaLoaiHinh.TagName = "ICB"; //Mã loại hình
                clcNgayKhaiBao.TagName = "IDD"; //Ngày khai báo xuất nhập khẩu
                clcNgayCapPhep.TagName = "IPD"; //Ngày cấp phép xuất nhập khẩu
                clcThoiHanTaiNhapTaiXuat.TagName = "RED"; //Thời hạn tái nhập/ tái xuất
                txtMaNguoiKhai.Tag = "IMC"; //Mã người khai
                txtTenNguoiKhai.Tag = "IMN"; //Tên người khai
                txtMaBuuChinhNguoiKhai.Tag = "IMY"; //Mã bưu chính
                txtDiaChiNguoiKhai.Tag = "IMA"; //Địa chỉ của người khai
                txtSoDienThoaiNguoiKhai.Tag = "IMT"; //Số điện thoại của người khai
                ucMaLyDoKhaiBoSung.TagName = "REC"; //Mã lý do khai bổ sung
                ucMaTienTeTienThue.TagName = "TTC"; //Mã tiền tệ của tiền thuế
                txtMaNganHangTraThueThay.Tag = "BRC"; //Mã ngân hàng trả thuế thay
                txtNamPhatHanhHanMuc.Tag = "BYA"; //Năm phát hành hạn mức
                txtKiHieuChungTuPhatHanhHanMuc.Tag = "BCM"; //Kí hiệu chứng từ phát hành hạn mức
                txtSoChungPhatHanhHanMuc.Tag = "BCN"; //Số chứng từ phát hành hạn mức
                txtMaXacDinhThoiHanNopThue.Tag = "ENC"; //Mã xác định thời hạn nộp thuế
                txtMaNganHangBaoLanh.Tag = "SBC"; //Mã ngân hàng bảo lãnh
                txtNamPhatHanh.Tag = "RYA"; //Năm phát hành bảo lãnh
                txtKyHieuChungTuBaoLanh.Tag = "SCM"; //Ký hiệu phát hành chứng từ bảo lãnh
                txtSoChungTuBaoLanh.Tag = "SCN"; //Số hiệu phát hành chứng từ bảo lãnh
                ucMaTienTeTruocKhiKhaiBoSung.TagName = "CDB"; //Mã tiền tệ trước khi khai bổ sung
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Tag = "RTB"; //Tỷ giá hối đoái trước khi khai bổ sung
                ucMaTienTeSauKhiKhaiBoSung.TagName = "CDA"; //Mã tiền tệ sau khi khai bổ sung
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.Tag = "RTA"; //Tỷ giá hối đoái sau khi khai bổ sung
                txtSoQuanLyTrongNoiBoDoanhNghiep.Tag = "REF"; //Số quản lý trong nội bộ doanh nghiệp
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Tag = "NTB"; //Ghi chú nội dung liên quan trước khi khai bổ sung
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Tag = "NTA"; //Ghi chú nội dung liên quan sau khi khai bổ sung
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucPhanLoaiNopThue.SetValidate = false;

                ucCoQuanHaiQuan.SetValidate = !isOnlyWarning; ucCoQuanHaiQuan.IsOnlyWarning = isOnlyWarning;
                isValid &= ucCoQuanHaiQuan.IsValidate; //"Cơ quan Hải quan");
                ucNhomXuLyHoSo.SetValidate = !isOnlyWarning; ucNhomXuLyHoSo.IsOnlyWarning = isOnlyWarning;
                //isValid &= ucNhomXuLyHoSo.IsValidate; //"Nhóm xử lý hồ sơ");
                isValid &= ValidateControl.ValidateNull(txtPhanLoaiXuatNhapKhau, errorProvider, "Phân loại xuất nhập khẩu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoToKhai, errorProvider, "Số tờ khai", isOnlyWarning);
                ucMaLoaiHinh.SetValidate = !isOnlyWarning; ucMaLoaiHinh.IsOnlyWarning = isOnlyWarning;
                isValid &= ucMaLoaiHinh.IsValidate; //"Mã loại hình");
                
                //isValid &= ValidateControl.ValidateNull(clcNgayKhaiBao, errorProvider, "Ngày khai báo xuất nhập khẩu", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(dtNgayCapPhep, errorProvider, "Ngày cấp phép xuất nhập khẩu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);
                ucMaLyDoKhaiBoSung.SetValidate = !isOnlyWarning; ucMaLyDoKhaiBoSung.IsOnlyWarning = isOnlyWarning;
                isValid &= ucMaLyDoKhaiBoSung.IsValidate; //"Mã lý do khai bổ sung");
                ucMaTienTeTienThue.SetValidate = !isOnlyWarning; ucMaTienTeTienThue.IsOnlyWarning = isOnlyWarning;
                isValid &= ucMaTienTeTienThue.IsValidate; //"Mã tiền tệ của tiền thuế");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhaiBoSung.MaxLength = 12;
                //ucCoQuanHaiQuan.MaxLength = 6;
                //ucNhomXuLyHoSo.MaxLength = 2;
                txtPhanLoaiXuatNhapKhau.MaxLength = 1;
                txtSoToKhaiBoSung.MaxLength = 12;
                //ucMaLoaiHinh.MaxLength = 3;
                //dtNgayKhaiBao.MaxLength = 8;
                //dtNgayCapPhep.MaxLength = 8;
                //dtThoiHanTaiNhapTaiXuat.MaxLength = 8;
                txtMaNguoiKhai.MaxLength = 13;
                txtTenNguoiKhai.MaxLength = 300;
                txtMaBuuChinhNguoiKhai.MaxLength = 7;
                txtDiaChiNguoiKhai.MaxLength = 300;
                txtSoDienThoaiNguoiKhai.MaxLength = 20;
                //ucMaLyDoKhaiBoSung.MaxLength = 1;
                //ucMaTienTeTienThue.MaxLength = 3;
                txtMaNganHangTraThueThay.MaxLength = 11;
                txtNamPhatHanhHanMuc.MaxLength = 4;
                txtKiHieuChungTuPhatHanhHanMuc.MaxLength = 10;
                txtSoChungTuBaoLanh.MaxLength = 10;
                txtMaXacDinhThoiHanNopThue.MaxLength = 1;
                txtMaNganHangBaoLanh.MaxLength = 11;
                txtNamPhatHanh.MaxLength = 4;
                txtKyHieuChungTuBaoLanh.MaxLength = 10;
                txtSoChungTuBaoLanh.MaxLength = 10;
                //ucMaTienTeTruocKhiKhaiBoSung.MaxLength = 3;
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.MaxLength = 9;
                //ucMaTienTeSauKhiKhaiBoSung.MaxLength = 3;
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.MaxLength = 9;
                txtSoQuanLyTrongNoiBoDoanhNghiep.MaxLength = 20;
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.MaxLength = 300;
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.MaxLength = 300;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCoQuanHaiQuan_EditValueChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor = Cursors.WaitCursor;

            //    ucNhomXuLyHoSo.WhereCondition = string.Format("CustomsCode like '{0}%'", ucCoQuanHaiQuan.Code);
            //    ucNhomXuLyHoSo.ReLoadData();
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            //}
            //finally { Cursor = Cursors.Default; }
        }

        private void txtPhanLoaiXuatNhapKhau_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ucMaLoaiHinh.CategoryType = txtPhanLoaiXuatNhapKhau.Text.Trim() == "I" ? ECategory.E001 : ECategory.E002;
                ucMaLoaiHinh.ReLoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi khi xử lý chương trình :\r\n" + ex.Message, false);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (TKBoSung.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    //TKBoSung.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    //TKBoSung.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg = null; //Form khai báo
                    AMC seaObj = VNACCMaperFromObject.AMCMapper(TKBoSung.SoToKhaiBoSung); //Set Mapper
                    if (seaObj == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    msg = MessagesSend.Load<AMC>(seaObj, TKBoSung.InputMessageID);

                    MsgLog.SaveMessages(msg, TKBoSung.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = TKBoSung.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai sửa: " + soTiepNhan;

                                TKBoSung.SoToKhaiBoSung = soTiepNhan;
                                TKBoSung.Update();
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TKBoSung.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    AMA AMA = VNACCMaperFromObject.AMAMapper(TKBoSung);
                    if (AMA == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKBoSung.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKBoSung.InsertUpdateFull();
                        msg = MessagesSend.Load<AMA>(AMA, TKBoSung.InputMessageID);

                    }
                    else
                    {
                        //msg = MessagesSend.Load<AMA>(AMA, true, EnumNghiepVu.AMA01, TKBoSung.InputMessageID);
                        msg = MessagesSend.Load<AMA>(AMA, TKBoSung.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKBoSung.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKBoSung.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        if (f.msgFeedBack.ToString().Equals("Khai báo thông tin thành công."))
                        {
                            string ketqua = "Khai báo thông tin thành công";
                            if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                            {
                                try
                                {
                                    decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                    ketqua = ketqua + Environment.NewLine;
                                    ketqua += "Số tờ khai: " + SoToKhai;
                                    TKBoSung.TrangThaiXuLy = System.Convert.ToInt16(EnumTrangThaiXuLy.KhaiBaoTam);
                                    TKBoSung.SoToKhaiBoSung = SoToKhai;
                                    TKBoSung.InsertUpdateFull();
                                }
                                catch (System.Exception ex)
                                {
                                    ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            this.ShowMessageTQDT(ketqua, false);
                            setCommandStatus();
                        }

                    }
                    //else
                    //{
                    //}
                    //else
                    //{
                    //    string ketqua = "Khai báo thông tin thành công";
                    //    //minhnd
                    //    try
                    //    {
                    //        CapNhatThongTin(f.feedback.ResponseData);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Logger.LocalLogger.Instance().WriteMessage(ex);
                    //        //throw;
                    //    }
                        
                        
                    //    if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                    //    {
                    //        try
                    //        {
                    //            decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                    //            ketqua = ketqua + Environment.NewLine;
                    //            ketqua += "Số tờ khai: " + SoToKhai;
                    //            TKBoSung.TrangThaiXuLy = System.Convert.ToInt16(EnumTrangThaiXuLy.KhaiBaoTam);
                    //            TKBoSung.SoToKhaiBoSung = SoToKhai;
                    //            TKBoSung.InsertUpdateFull();
                    //        }
                    //        catch (System.Exception ex)
                    //        {
                    //            ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                    //            Logger.LocalLogger.Instance().WriteMessage(ex);
                    //        }
                    //    }
                    //    CapNhatThongTin(f.feedback.ResponseData);
                    //    //this.ShowMessageTQDT(ketqua, false);

                    //}
                }
                SetTKSuaDoi();
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }




        #endregion

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }

        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            ProcessMessages.GetDataResult_TKBoSung(msgResult, MaNV, TKBoSung);
            TKBoSung.InsertUpdateFull();
            SetTKSuaDoi();
            setCommandStatus();
            //setCommandStatus();
            //ProcessMessages.GetDataResult_TKBoSung(msgResult, MaNV, TKBoSung);
            ////TKBoSung.TrangThaiXuLy = System.Convert.ToInt32(EnumTrangThaiXuLy.KhaiBaoChinhThuc);
            ////TKBoSung.SoToKhai = System.Convert.ToDecimal(ama.SYN.GetValue().ToString());
            //SetTKSuaDoi();
            //TKBoSung.InsertUpdateFull();
        }

        private void TuDongCapNhatThongTin()
        {
            if (TKBoSung != null && TKBoSung.SoToKhai > 0 && TKBoSung.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", TKBoSung.SoToKhaiBoSung.ToString(), TKBoSung.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtSoToKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucNhomXuLyHoSo.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhanLoaiXuatNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoToKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaLoaiHinh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNgayKhaiBao.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNgayCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtThoiHanTaiNhapTaiXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaBuuChinh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaChiNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaLyDoKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeTienThue.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNganHangTraThueThay.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNamPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKiHieuChungTuPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoChungPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaXacDinhThoiHanNopThue.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNganHangBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNamPhatHanhBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKyHieuChungTuBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoChungTuBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTyGiaHoiDoaiTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTyGiaHoiDoaiSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoQuanLyTrongNoiBoDoanhNghiep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtSoToKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucCoQuanHaiQuan.IsUpperCase = true;
            //ucNhomXuLyHoSo.IsUpperCase = true;
            txtPhanLoaiXuatNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoToKhai.CharacterCasing = CharacterCasing.Upper;
            ucMaLoaiHinh.IsUpperCase = true;
            //txtNgayKhaiBao.CharacterCasing = CharacterCasing.Upper;
            //txtNgayCapPhep.CharacterCasing = CharacterCasing.Upper;
            //txtThoiHanTaiNhapTaiXuat.CharacterCasing = CharacterCasing.Upper;
            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtTenNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtDiaChiNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtSoDienThoaiNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            ucMaLyDoKhaiBoSung.IsUpperCase = true;
            ucMaTienTeTienThue.IsUpperCase = true;
            txtMaNganHangTraThueThay.CharacterCasing = CharacterCasing.Upper;
            txtNamPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtKiHieuChungTuPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtSoChungPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtMaXacDinhThoiHanNopThue.CharacterCasing = CharacterCasing.Upper;
            txtMaNganHangBaoLanh.CharacterCasing = CharacterCasing.Upper;
            //txtNamPhatHanhBaoLanh.CharacterCasing = CharacterCasing.Upper;
            txtKyHieuChungTuBaoLanh.CharacterCasing = CharacterCasing.Upper;
            txtSoChungTuBaoLanh.CharacterCasing = CharacterCasing.Upper;
            ucMaTienTeTruocKhiKhaiBoSung.IsUpperCase = true;
            txtTyGiaHoiDoaiTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucMaTienTeSauKhiKhaiBoSung.IsUpperCase = true;
            txtTyGiaHoiDoaiSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoQuanLyTrongNoiBoDoanhNghiep.CharacterCasing = CharacterCasing.Upper;
            txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
        }
        private void txtSoToKhai_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                f.IsShowChonToKhai = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    KDT_VNACC_ToKhaiMauDich TKMD = f.TKMDDuocChon;
                    txtSoToKhai.Value = TKMD.SoToKhai;
                    this.TKMD_ID = TKMD.ID;
                    SetFromToKhai(TKMD);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }            
        }

        private void ucCoQuanHaiQuan_Leave(object sender, EventArgs e)
        {
            try
            {
                ucNhomXuLyHoSo.CustomsCode = ucCoQuanHaiQuan.Code;
                ucNhomXuLyHoSo.ReLoadData();
                if (TKBoSung != null && !string.IsNullOrEmpty(TKBoSung.NhomXuLyHoSo))
                    ucNhomXuLyHoSo.Code = TKBoSung.NhomXuLyHoSo;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetFromToKhai(KDT_VNACC_ToKhaiMauDich TKMD)
        {
            try
            {
                ucCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
                ucNhomXuLyHoSo.Code = TKMD.NhomXuLyHS;
                if (TKMD.isToKhaiNhap())
                    txtPhanLoaiXuatNhapKhau.Text = "I";
                else
                    txtPhanLoaiXuatNhapKhau.Text = "E";
                ucMaLoaiHinh.Code = TKMD.MaLoaiHinh;
                clcNgayKhaiBao.Value = TKMD.NgayDangKy;
                clcNgayCapPhep.Value = TKMD.NgayCapPhep;
                clcThoiHanTaiNhapTaiXuat.Value = TKMD.ThoiHanTaiNhapTaiXuat;
                txtMaNguoiKhai.Text = TKMD.MaDonVi;
                txtTenNguoiKhai.Text = TKMD.TenDonVi;
                txtMaBuuChinhNguoiKhai.Text = TKMD.MaBuuChinhDonVi;
                txtDiaChiNguoiKhai.Text = TKMD.DiaChiDonVi;
                txtSoDienThoaiNguoiKhai.Text = TKMD.SoDienThoaiDonVi;

                txtMaDaiLyHaiQuan.Text = TKMD.MaDaiLyHQ;
                txtTenDaiLyHaiQuan.Text = TKMD.TenDaiLyHaiQuan;
                ucMaTienTeTienThue.Code = "VND";
                txtSoQuanLyTrongNoiBoDoanhNghiep.Text = TKMD.SoQuanLyNoiBoDN;
                txtMaNganHangTraThueThay.Text = TKMD.MaNHTraThueThay;
                //txtTenNganHangTraThueThay.Text = TKMD.MaNHTraThueThay/
                txtNamPhatHanh.Value = TKMD.NamPhatHanhHM;
                txtKiHieuChungTuPhatHanhHanMuc.Text = TKMD.KyHieuCTHanMuc;
                txtSoChungPhatHanhHanMuc.Text = TKMD.SoCTHanMuc;
                txtMaXacDinhThoiHanNopThue.Text = TKMD.MaXDThoiHanNopThue;
                txtMaNganHangBaoLanh.Text = TKMD.MaNHBaoLanh;
                txtNamPhatHanh.Value = TKMD.NamPhatHanhBL;
                txtKyHieuChungTuBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
                txtSoChungTuBaoLanh.Text = TKMD.SoCTBaoLanh;
                if (TKMD.TyGiaCollection != null && TKMD.TyGiaCollection.Count > 0)
                {
                    ucMaTienTeSauKhiKhaiBoSung.Code = ucMaTienTeTruocKhiKhaiBoSung.Code = TKMD.TyGiaCollection[0].MaTTTyGiaTinhThue;
                    txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value = txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value = TKMD.TyGiaCollection[0].TyGiaTinhThue;
                }
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text = TKMD.GhiChu;
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text = TKMD.GhiChu;
                foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue item in TKBoSung.HangCollection)
                {
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac ite in item.HangThuKhacCollection)
                    {
                        if (ite.ID > 0)
                            ite.Delete();
                    }
                    if (item.ID > 0)
                        item.Delete();
                }
                TKBoSung.HangCollection.Clear();
                foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                {
                    KDT_VNACC_HangMauDich_KhaiBoSung_Thue HangThue = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
                    HangThue.SoThuTuDongHangTrenToKhaiGoc = hmd.SoDong;

                    HangThue.MoTaHangHoaTruocKhiKhaiBoSung = string.IsNullOrEmpty(hmd.MaHangHoa) ? hmd.TenHang : hmd.MaHangHoa + "#&" + hmd.TenHang;
                    HangThue.MoTaHangHoaSauKhiKhaiBoSung = string.IsNullOrEmpty(hmd.MaHangHoa) ? hmd.TenHang : hmd.MaHangHoa + "#&" + hmd.TenHang;

                    HangThue.MaNuocXuatXuTruocKhiKhaiBoSung = hmd.NuocXuatXu == "" ? "VN" : hmd.NuocXuatXu;
                    HangThue.MaNuocXuatXuSauKhiKhaiBoSung = hmd.NuocXuatXu == "" ? "VN" : hmd.NuocXuatXu;

                    HangThue.TriGiaTinhThueTruocKhiKhaiBoSung = hmd.TriGiaTinhThueS;
                    HangThue.TriGiaTinhThueSauKhiKhaiBoSung = hmd.TriGiaTinhThueS;

                    HangThue.SoLuongTinhThueTruocKhiKhaiBoSung = hmd.SoLuong1;
                    HangThue.SoLuongTinhThueSauKhiKhaiBoSung = hmd.SoLuong1;

                    HangThue.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = hmd.DVTLuong1;
                    HangThue.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = hmd.DVTLuong1;

                    HangThue.MaSoHangHoaTruocKhiKhaiBoSung = hmd.MaSoHang;
                    HangThue.MaSoHangHoaSauKhiKhaiBoSung = hmd.MaSoHang;
                    //duydp 30/12/2015
                    HangThue.ThueSuatTruocKhiKhaiBoSung = (string.IsNullOrEmpty(hmd.ThueSuatThue) ) ? (hmd.ThueSuatThue == "0") ? "0%" : "KCT" : hmd.ThueSuatThue.ToString();
                    HangThue.ThueSuatSauKhiKhaiBoSung = (string.IsNullOrEmpty(hmd.ThueSuatThue)) ? (hmd.ThueSuatThue == "0") ? "0%" : "KCT" : hmd.ThueSuatThue.ToString();
                    //duydp 30/12/2015
                    HangThue.SoTienThueTruocKhiKhaiBoSung = hmd.SoTienThue.ToString();
                    HangThue.SoTienThueSauKhiKhaiBoSung = hmd.SoTienThue.ToString();
                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in hmd.ThueThuKhacCollection)
                    {
                        KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac HangThueThuKhac = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
                        HangThueThuKhac.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = item.TriGiaTinhThueVaThuKhac;
                        HangThueThuKhac.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = item.TriGiaTinhThueVaThuKhac;

                        HangThueThuKhac.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = (item.SoLuongTinhThueVaThuKhac == 0) ? hmd.SoLuong1 : item.SoLuongTinhThueVaThuKhac;
                        HangThueThuKhac.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = (item.SoLuongTinhThueVaThuKhac == 0) ? hmd.SoLuong1 : item.SoLuongTinhThueVaThuKhac;

                        HangThueThuKhac.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = (String.IsNullOrEmpty(item.MaDVTDanhThueVaThuKhac)) ? hmd.DVTLuong1 : item.MaDVTDanhThueVaThuKhac;
                        HangThueThuKhac.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = (String.IsNullOrEmpty(item.MaDVTDanhThueVaThuKhac)) ? hmd.DVTLuong1 : item.MaDVTDanhThueVaThuKhac;

                        HangThueThuKhac.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = item.MaTSThueThuKhac;
                        HangThueThuKhac.MaApDungThueSuatSauKhiKhaiBoSungThuKhac= item.MaTSThueThuKhac;

                        HangThueThuKhac.ThueSuatTruocKhiKhaiBoSungThuKhac = String.IsNullOrEmpty(item.ThueSuatThueVaThuKhac) ? "0%" : item.ThueSuatThueVaThuKhac.ToString();
                        HangThueThuKhac.ThueSuatSauKhiKhaiBoSungThuKhac = String.IsNullOrEmpty(item.ThueSuatThueVaThuKhac) ? "0%" : item.ThueSuatThueVaThuKhac.ToString();

                        HangThueThuKhac.SoTienThueTruocKhiKhaiBoSungThuKhac = item.SoTienThueVaThuKhac.ToString();
                        HangThueThuKhac.SoTienThueSauKhiKhaiBoSungThuKhac = item.SoTienThueVaThuKhac.ToString();
                        HangThue.HangThuKhacCollection.Add(HangThueThuKhac);

                    }
                    TKBoSung.HangCollection.Add(HangThue);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void ShowKetQuaTraVe()
        {
            try
            { 
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.inputMSGID = TKBoSung.InputMessageID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }        
    }
}
