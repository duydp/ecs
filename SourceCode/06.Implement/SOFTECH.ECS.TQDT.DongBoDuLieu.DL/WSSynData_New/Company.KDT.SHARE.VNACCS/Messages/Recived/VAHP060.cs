﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAHP060 : BasicVNACC
    {
        public void LoadVAHP060(string strResult)
        {
            try
            {
                this.GetObject<VAHP060>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAHP060, false, VAHP060.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAHP060.TongSoByte);
                
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
