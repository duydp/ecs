﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class LoaiHinhMauDichVControl : UserControl
    {
        public LoaiHinhMauDichVControl()
        {
            this.InitializeComponent();
        }

        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(5);
            }
            get { return this.txtMa.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtMa.ReadOnly = value;
                this.cbTen.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        private string _Nhom = string.Empty;        
        public string Nhom
        {
            set
            {
                this._Nhom = value;
                if (!this.DesignMode)
                {
                    this.loadData();
                }

            }
            get { return this._Nhom; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtMa.VisualStyleManager = value;
                this.cbTen.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            if (this._Nhom.Trim().Length == 0) this._Nhom = GlobalSettings.NHOM_LOAI_HINH;
            if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.LOAI_HINH;

            this.dtLoaiHinhMauDich.Clear();
            DataTable dt;
            if (this._Nhom.Trim().Length > 0)
            {
                //Mac dinh lay du lieu loai hinh GC.
                if (!this._Nhom.Trim().Contains("NGC") || !this._Nhom.Trim().Contains("XGC"))
                {
                    this._Nhom = this._Nhom.Trim().Substring(0, 1) + "GC";
                }

                dt = LoaiHinhMauDich.SelectBy_Nhom(this._Nhom);
            }
            else
                dt = LoaiHinhMauDich.SelectAll();

            dt.DefaultView.Sort = "ID asc";
            dtLoaiHinhMauDich.Rows.Clear();
            string id = "";
            foreach (DataRowView row in dt.DefaultView)
            {
                //Kiem tra va loai tru ma loai hinh GCCT: NGC18, NGC19, NGC20, XGC18, XGC19, XGC20
                id = row["ID"].ToString();
                if (id != "NGC18" && id != "NGC19" && id != "NGC20" && id != "XGC18" && id != "XGC19" && id != "XGC20")
                    this.dtLoaiHinhMauDich.Rows.Add(new object[] { row["ID"], row["Ten"] });
            }

            //Hungtq, 14/10/2011. Bo sung loai hinh TAM NHAP TAI XUAT cho Gia cong
            if (this._Nhom.Trim().Contains("NGC"))
            {
                //Ma: NTA01, Ten: Tạm Nhập Tái Xuất (Nhập Phải Tái Xuất), Ten viet tat: NTX
                //Ma: NTA25, Ten: Tạm Nhập Tái Chế, Ten viet tat: TNTC
                //dt = LoaiHinhMauDich.SelectBy_Nhom("NTA");

                //foreach (DataRow row in dt.Rows)
                //{
                //    if (row["ID"].ToString().Trim() == "NTA01" || row["ID"].ToString().Trim() == "NTA25")
                //        this.dtLoaiHinhMauDich.Rows.Add(new object[] { row["ID"], row["Ten"] });
                //}
            }
            else if (this._Nhom.Trim().Contains("XGC"))
            {  
                //Ma: XTA19, Ten: Tái xuất Tái chế, Ten viet tat: XT-TC
                //dt = LoaiHinhMauDich.SelectBy_Nhom("XTA");

                //foreach (DataRow row in dt.Rows)
                //{
                //    if (row["ID"].ToString().Trim() == "XTA19")
                //        this.dtLoaiHinhMauDich.Rows.Add(new object[] { row["ID"], row["Ten"] });
                //}
            }

            this.cbTen.Value = txtMa.Text.PadRight(5);
        }

        private void cbLoaiHinhMauDich_ValueChanged(object sender, EventArgs e)
        {
            if (cbTen.Value == null) return;
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMaLoaiHinh_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (this.dtLoaiHinhMauDich.Select("ID='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                this.txtMa.Text = GlobalSettings.LOAI_HINH;
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(5);
        }
    }
}