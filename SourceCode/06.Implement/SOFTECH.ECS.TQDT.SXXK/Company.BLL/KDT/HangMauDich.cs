
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
namespace Company.BLL.KDT
{
    public partial class HangMauDich
    {
        public bool Load(long tkmdID, string maHS, string maPhu, string tenhang, string nuocXXID, string dvtID, decimal soLuong, decimal donGiaKB, decimal thueXNK)
        {
            string spName = "p_KDT_HangMauDich_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmdID);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, maHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, tenhang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, nuocXXID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, dvtID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, donGiaKB);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, thueXNK);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) this._ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));

                reader.Close();
                return true;
            }
            return false;
        }

        public HangMauDich Load(long tkmdID, string maHS, string maPhu, string tenhang, string nuocXXID, string dvtID, decimal soLuong, decimal donGiaKB, SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_LoadBy2";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmdID);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, maHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, tenhang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, nuocXXID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, dvtID);

            HangMauDich entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity._SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) entity._ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
            }
            reader.Close();

            return entity;
        }

        //---------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdateBy()
        {
            return this.InsertUpdateTransactionBy(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction)
        {
            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            this.db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            this.db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            this.db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            this.db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, this._ThueSuatGiam);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, this._SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, this._Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, this._DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, this._SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, this._ThueSuatGiam);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateBy(HangMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.InsertUpdateTransactionBy(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        public void TinhThue(decimal tygiaTT)
        {
            //decimal dongia_TT = this._DonGiaKB*tygiaTT;
            //decimal trigiaNT = this._DonGiaKB*this._SoLuong;
            ////decimal trigiaTT_XNK = (dongia_TT*this._SoLuong) + (this._ASEAN_KhoanPhaiCong*tygiaTT) - (this._ASEAN_KhoanPhaiTru*tygiaTT);
            //decimal trigiaTT_XNK = this.TriGiaTT;
            //decimal tienthue_XNK = trigiaTT_XNK*this._ThueSuatXNK/100;
            //decimal tienthue_TTDB = (trigiaTT_XNK + tienthue_XNK)*this._ThueSuatTTDB/100;
            //decimal tienthue_GTGT = (trigiaTT_XNK + tienthue_XNK + tienthue_TTDB)*this._ThueSuatGTGT/100;
            //decimal tienthukhac = this._TyLeThuKhac*trigiaTT_XNK/100;


            //this._DonGiaTT = dongia_TT;
            //this._TriGiaKB = trigiaNT;
            //this._TriGiaTT = trigiaTT_XNK;
            //this._TriGiaKB_VND = trigiaNT*tygiaTT;
            //this._ThueXNK = tienthue_XNK;
            //this._ThueTTDB = tienthue_TTDB;
            //this._ThueGTGT = tienthue_GTGT;
            //this._TriGiaThuKhac = tienthukhac;
        }

        //-----------------------------------------------------------------------------------------

        //public HangMauDichInfo ExportToInfo()
        //{
        //    HangMauDichInfo entityInfo = new HangMauDichInfo();                        
        //    entityInfo.SoThuTuHang = this._SoThuTuHang;
        //    entityInfo.MaHS = this._MaHS;
        //    entityInfo.MaPhu = this._MaPhu;
        //    entityInfo.TenHang = this._TenHang;
        //    entityInfo.NuocXX_ID = this._NuocXX_ID;
        //    entityInfo.DVT_ID = this._DVT_ID;
        //    entityInfo.SoLuong = this._SoLuong;            
        //    entityInfo.DonGiaKB = this._DonGiaKB;
        //    entityInfo.DonGiaTT = this._DonGiaTT;
        //    entityInfo.TriGiaKB = this._TriGiaKB;
        //    entityInfo.TriGiaTT = this._TriGiaTT;
        //    entityInfo.TriGiaKB_VND = this._TriGiaKB_VND;
        //    entityInfo.ThueSuatXNK = this._ThueSuatXNK;
        //    entityInfo.ThueSuatTTDB = this._ThueSuatTTDB;
        //    entityInfo.ThueSuatGTGT = this._ThueSuatGTGT;
        //    entityInfo.ThueXNK = this._ThueXNK;
        //    entityInfo.ThueTTDB = this._ThueTTDB;
        //    entityInfo.ThueGTGT = this._ThueGTGT;
        //    entityInfo.PhuThu = this._PhuThu;
        //    entityInfo.TyLeThuKhac = this._TyLeThuKhac;
        //    entityInfo.TriGiaThuKhac = this._TriGiaThuKhac;
        //    entityInfo.MienThue = this._MienThue;
        //    entityInfo.MA_NPL_SP = this.MaPhu;
        //    return entityInfo;
        //}

        public HangMauDichCollection SelectCollectionBy_TKMD_ID(SqlTransaction trans, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            HangMauDichCollection collection = new HangMauDichCollection();
            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity._Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity._DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity._SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) entity._ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

    }
}