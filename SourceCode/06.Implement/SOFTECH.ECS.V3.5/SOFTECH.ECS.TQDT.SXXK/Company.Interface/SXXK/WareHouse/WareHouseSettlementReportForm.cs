﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.Report.GC.ThongTu38;
using System.Threading;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSettlementReportForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho;
        List<T_KHOKETOAN_PHIEUXNKHO> PHIEUXNKCollection = new List<T_KHOKETOAN_PHIEUXNKHO>();
        public WareHouseSettlementReportForm()
        {
            InitializeComponent();

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";
        }
        private void SetStatus(int valueProcess, string sts)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess; }));
            }
            else
            {
                lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess;
            }
        }
        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["LOAICHUNGTU"].Value != null)
                {
                    switch (e.Row.Cells["LOAICHUNGTU"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho";
                            break;
                        case "X":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho";
                            break;
                    }
                }
                if (e.Row.Cells["LOAIHANGHOA"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANGHOA"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Nguyên phụ liệu";
                            break;
                        case "S":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Sản phẩm";
                            break;
                        case "T":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Thiết bị";
                            break;
                        case "H":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Hàng mẫu";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
                PhieuXNKho = (T_KHOKETOAN_PHIEUXNKHO)e.Row.DataRow;
                PhieuXNKho.HangCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(PhieuXNKho.ID);
                WareHouseExportForm f = new WareHouseExportForm();
                f.PhieuXNKho = PhieuXNKho;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaKho_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaKho.Value != null)
                {
                    List<T_KHOKETOAN_DANHSACHKHO> KHOCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
                    foreach (T_KHOKETOAN_DANHSACHKHO item in KHOCollection)
                    {
                        if (item.MAKHO == cbbMaKho.Value.ToString())
                        {
                            txtTenKho.Text = item.TENKHO.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                grList.Refresh();
                grList.DataSource = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic(" MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "'", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataDetail()
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                dgList.Refresh();
                dgList.DataSource = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectDynamicBCQT("PHIEUXNKHO_ID IN (SELECT ID FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "')", "ID").Tables[0];
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnView_Click(object sender, EventArgs e)
        {
            BindReport();
        }
        private void DeleteDataOld()
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindReport()
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                List<T_KHOKETOAN_BCQT> NPLKHOBCQT = T_KHOKETOAN_BCQT.SelectCollectionDynamic(" MAKHO='" + cbbMaKho.Value.ToString() + "' AND TUNGAY >= '" + DateFrom + "' AND DENNGAY <='" + DateTo + "' AND LOAIHANGHOA= 1", "ID");
                List<T_KHOKETOAN_BCQT> SPKHOBCQT = T_KHOKETOAN_BCQT.SelectCollectionDynamic(" MAKHO='" + cbbMaKho.Value.ToString() + "' AND TUNGAY >= '" + DateFrom + "' AND DENNGAY <='" + DateTo + "' AND LOAIHANGHOA= 2", "ID");
                if (cbbLoaiBC.SelectedValue.ToString() == "N")
                {
                    BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                    report.NAMQUYETTOAN = clcTuNgay.Value.Year;
                    report.isNumber = true;
                    report.BinReportBCQTKho(true, NPLKHOBCQT, SPKHOBCQT);
                    report.ShowPreview();
                }
                else
                {
                    BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                    report.NAMQUYETTOAN = clcTuNgay.Value.Year;
                    report.isNumber = false;
                    report.BinReportBCQTKho(false, NPLKHOBCQT, SPKHOBCQT);
                    report.ShowPreview();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");

                DateTime dateFrom = clcTuNgay.Value;
                DateTime dateTo = clcDenNgay.Value;
                // XÓA DỮ LIỆU CŨ 
                T_KHOKETOAN_BCQT.DeleteDynamic(" MAKHO='" + cbbMaKho.Value.ToString() + "' AND TUNGAY >= '" + DateFrom + "' AND DENNGAY <='" + DateTo + "'");

                SetStatus(0, "LẤY DANH SÁCH TẤT CẢ CÁC PHIẾU XUẤT NHẬP KHO");

                //PHIEUXNKCollection = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value.ToString() + " AND MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "'", "ID");
                PHIEUXNKCollection = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic("MAKHO='" + cbbMaKho.Value.ToString() + "'", "ID");
                #region XỬ LÝ NGUYÊN PHỤ LIỆU
                SetStatus(4, "XỬ LÝ NGUYÊN PHỤ LIỆU");
                List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionAll();
                List<T_KHOKETOAN_BCQT> NPLSPQTCollection = new List<T_KHOKETOAN_BCQT>();
                int i = 1;
                foreach (T_KHOKETOAN_NGUYENPHULIEU item in NPLCollection)
                {
                    T_KHOKETOAN_BCQT NPLSP_Detail = new T_KHOKETOAN_BCQT();
                    NPLSP_Detail.STT = i;
                    NPLSP_Detail.LOAIHANGHOA = 1;
                    NPLSP_Detail.TAIKHOAN = 152;
                    NPLSP_Detail.MAHANGHOA = item.MANPL;
                    NPLSP_Detail.TENHANGHOA = item.TENNPL;
                    NPLSP_Detail.DVT = VNACCS_Mapper.GetCodeVNACC(item.DVT);
                    NPLSP_Detail.LUONGTONDAU = 0;
                    NPLSP_Detail.TRIGIATONDAU = 0;
                    NPLSP_Detail.LUONGNHAP = 0;
                    NPLSP_Detail.TRIGIANHAP = 0;
                    NPLSP_Detail.LUONGXUAT = 0;
                    NPLSP_Detail.TRIGIAXUAT = 0;
                    NPLSP_Detail.LUONGTONCUOI = 0;
                    NPLSP_Detail.TRIGIATONCUOI = 0;
                    NPLSP_Detail.GHICHU = "";
                    
                    NPLSP_Detail.TUNGAY = dateFrom;
                    NPLSP_Detail.DENNGAY = dateTo;
                    NPLSP_Detail.MAKHO = cbbMaKho.Value.ToString();
                    NPLSP_Detail.TENKHO = txtTenKho.Text.ToString();
                    NPLSPQTCollection.Add(NPLSP_Detail);
                    i++;

                }
                #endregion XỬ LÝ NGUYÊN PHỤ LIỆU
                #region XỬ LÝ SẢN PHẨM
                SetStatus(6, "XỬ LÝ SẢN PHẨM");
                List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionAll();
                foreach (T_KHOKETOAN_SANPHAM item in SPCollection)
                {
                    T_KHOKETOAN_BCQT NPLSP_Detail = new T_KHOKETOAN_BCQT();
                    NPLSP_Detail.STT = i;
                    NPLSP_Detail.LOAIHANGHOA = 2;
                    NPLSP_Detail.TAIKHOAN = 155;
                    NPLSP_Detail.MAHANGHOA = item.MASP;
                    NPLSP_Detail.TENHANGHOA = item.TENSP;
                    NPLSP_Detail.DVT = VNACCS_Mapper.GetCodeVNACC(item.DVT);
                    NPLSP_Detail.LUONGTONDAU = 0;
                    NPLSP_Detail.TRIGIATONDAU = 0;
                    NPLSP_Detail.LUONGNHAP = 0;
                    NPLSP_Detail.TRIGIANHAP = 0;
                    NPLSP_Detail.LUONGXUAT = 0;
                    NPLSP_Detail.TRIGIAXUAT = 0;
                    NPLSP_Detail.LUONGTONCUOI = 0;
                    NPLSP_Detail.TRIGIATONCUOI = 0;
                    NPLSP_Detail.GHICHU = "";

                    NPLSP_Detail.TUNGAY = dateFrom;
                    NPLSP_Detail.DENNGAY = dateTo;
                    NPLSP_Detail.MAKHO = cbbMaKho.Value.ToString();
                    NPLSP_Detail.TENKHO = txtTenKho.Text.ToString();
                    NPLSPQTCollection.Add(NPLSP_Detail);
                    i++;
                }
                #endregion XỬ LÝ SẢN PHẨM

                #region XỬ LÝ TỒN ĐẦU KỲ CỦA NPL VÀ SP
                List<T_KHOKETOAN_TONDAUKY> TONDAUKYCollection = T_KHOKETOAN_TONDAUKY.SelectCollectionDynamic(" MAKHO='" + cbbMaKho.Value.ToString() + "' AND NAMQT=" + (clcTuNgay.Value.Year -1) +"", "ID");
                //foreach (T_KHOKETOAN_BCQT item in NPLSPQTCollection)
                //{
                //    foreach (T_KHOKETOAN_TONDAUKY items in TONDAUKYCollection)
                //    {
                //        if (item.MAHANGHOA==items.MAHANGHOA)
                //        {
                //            item.LUONGTONDAU = items.LUONGTON;
                //            item.TRIGIATONDAU = items.TRIGIA;
                //        }
                //    }
                //}
                #endregion
                #region XỬ LÝ PHIẾU XUẤT NHẬP KHO
                SetStatus(12, "XỬ LÝ PHIẾU XUẤT NHẬP KHO");
                int index = 0;
                foreach (T_KHOKETOAN_PHIEUXNKHO PHIEUXNKHO in PHIEUXNKCollection)
                {
                    index++;
                    SetStatus((index * 80 / PHIEUXNKCollection.Count) + 12, "XỬ LÝ PHIẾU XUẤT NHẬP KHO");
                    PHIEUXNKHO.HangCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(PHIEUXNKHO.ID);

                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA HANGHOA in PHIEUXNKHO.HangCollection)
                    {
                        if (PHIEUXNKHO.LOAICHUNGTU == "N")
                        {
                            if (PHIEUXNKHO.LOAIHANGHOA == "N")
                            {
                                #region NHẬP KHO NGUYÊN PHỤ LIỆU
                                foreach (T_KHOKETOAN_BCQT item in NPLSPQTCollection)
                                {
                                    if (HANGHOA.MAHANG == item.MAHANGHOA)
                                    {
                                        if (PHIEUXNKHO.NGAYCT <dateFrom)
                                        {
                                            item.LUONGTONDAU += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONDAU += HANGHOA.THANHTIEN;
                                        }
                                        else if (PHIEUXNKHO.NGAYCT >=dateFrom && PHIEUXNKHO.NGAYCT <=dateTo)
                                        {
                                            item.LUONGNHAP += HANGHOA.SOLUONGQD;
                                            item.TRIGIANHAP += HANGHOA.THANHTIEN;
                                        }
                                        else
                                        {
                                            item.LUONGTONCUOI += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONCUOI += HANGHOA.THANHTIEN;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (PHIEUXNKHO.LOAIHANGHOA == "S")
                            {
                                #region NHẬP KHO SẢN PHẨM
                                foreach (T_KHOKETOAN_BCQT item in NPLSPQTCollection)
                                {
                                    if (HANGHOA.MAHANG == item.MAHANGHOA)
                                    {
                                        if (PHIEUXNKHO.NGAYCT < dateFrom)
                                        {
                                            item.LUONGTONDAU += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONDAU += HANGHOA.THANHTIEN;
                                        }
                                        else if (PHIEUXNKHO.NGAYCT >= dateFrom && PHIEUXNKHO.NGAYCT <= dateTo)
                                        {
                                            item.LUONGNHAP += HANGHOA.SOLUONGQD;
                                            item.TRIGIANHAP += HANGHOA.THANHTIEN;
                                        }
                                        else
                                        {
                                            item.LUONGTONCUOI += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONCUOI += HANGHOA.THANHTIEN;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            if (PHIEUXNKHO.LOAIHANGHOA == "N")
                            {
                                #region XUẤT KHO NGUYÊN PHỤ LIỆU
                                foreach (T_KHOKETOAN_BCQT item in NPLSPQTCollection)
                                {
                                    if (HANGHOA.MAHANG == item.MAHANGHOA)
                                    {
                                        if (PHIEUXNKHO.NGAYCT < dateFrom)
                                        {
                                            item.LUONGTONDAU -= HANGHOA.SOLUONGQD;
                                            item.TRIGIATONDAU -= HANGHOA.THANHTIEN;
                                        }
                                        else if (PHIEUXNKHO.NGAYCT >= dateFrom && PHIEUXNKHO.NGAYCT <= dateTo)
                                        {
                                            item.LUONGXUAT += HANGHOA.SOLUONGQD;
                                            item.TRIGIAXUAT += HANGHOA.THANHTIEN;
                                        }
                                        else
                                        {
                                            item.LUONGTONCUOI += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONCUOI += HANGHOA.THANHTIEN;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (PHIEUXNKHO.LOAIHANGHOA == "S")
                            {
                                #region XUẤT KHO SẢN PHẨM
                                foreach (T_KHOKETOAN_BCQT item in NPLSPQTCollection)
                                {
                                    if (HANGHOA.MAHANG == item.MAHANGHOA)
                                    {
                                        if (PHIEUXNKHO.NGAYCT < dateFrom)
                                        {
                                            item.LUONGTONDAU -= HANGHOA.SOLUONGQD;
                                            item.TRIGIATONDAU -= HANGHOA.THANHTIEN;
                                        }
                                        else if (PHIEUXNKHO.NGAYCT >= dateFrom && PHIEUXNKHO.NGAYCT <= dateTo)
                                        {
                                            item.LUONGXUAT += HANGHOA.SOLUONGQD;
                                            item.TRIGIAXUAT += HANGHOA.THANHTIEN;
                                        }
                                        else
                                        {
                                            item.LUONGTONCUOI += HANGHOA.SOLUONGQD;
                                            item.TRIGIATONCUOI += HANGHOA.THANHTIEN;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {

                            }
                        }
                    }
                }
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                #endregion XỬ LÝ PHIẾU XUẤT NHẬP KHO
                SetStatus(95, "LƯU DỮ LIỆU");
                bool Overrite = false;
                bool IsDifferent = false;
                foreach (T_KHOKETOAN_BCQT NPLSP_Detail in NPLSPQTCollection)
                {
                    // KIỂM TRA TỒN ĐẦU KỲ NHẬP VÀO TỒN ĐẦU KỲ TÍNH TOÁN 
                    if (TONDAUKYCollection.Count == 0)
                    {
                        //TẠO DỮ LIỆU TỒN ĐẦU KỲ
                        T_KHOKETOAN_TONDAUKY TONDAUKY = new T_KHOKETOAN_TONDAUKY();
                        TONDAUKY.MAKHO = NPLSP_Detail.MAKHO;
                        TONDAUKY.TENKHO = NPLSP_Detail.TENKHO;
                        TONDAUKY.MAHANGHOA = NPLSP_Detail.MAHANGHOA;
                        TONDAUKY.TENHANGHOA = NPLSP_Detail.TENHANGHOA;
                        TONDAUKY.LOAIHANGHOA = NPLSP_Detail.LOAIHANGHOA;
                        TONDAUKY.LUONGTON = NPLSP_Detail.LUONGTONDAU;
                        TONDAUKY.DONGIA = NPLSP_Detail.LUONGTONDAU !=0 ? NPLSP_Detail.TRIGIATONDAU / NPLSP_Detail.LUONGTONDAU : 0;
                        TONDAUKY.TRIGIA = NPLSP_Detail.TRIGIATONDAU;
                        TONDAUKY.TYGIA = 1;
                        TONDAUKY.NGUYENTE = "";
                        TONDAUKY.THUESUATXNK = 0;
                        TONDAUKY.DINHKHOAN = String.Empty;
                        TONDAUKY.GHICHU = String.Empty;
                        TONDAUKY.DVT = NPLSP_Detail.DVT;
                        TONDAUKY.NAMQT = clcTuNgay.Value.Year -1;
                        TONDAUKY.InsertUpdate();
                    }
                    else
                    {
                        foreach (T_KHOKETOAN_TONDAUKY item in TONDAUKYCollection)
                        {
                            if (item.MAHANGHOA==NPLSP_Detail.MAHANGHOA)
                            {
                                if (item.LUONGTON != NPLSP_Detail.LUONGTONDAU)
                                {
                                    IsDifferent = true;
                                }
                            }
                        }
                    }
                }
                if (IsDifferent)
                {
                    if (ShowMessage("DỮ LIỆU TỒN ĐẦU KỲ NHẬP VÀO KHÁC VỚI DỮ LIỆU TỒN ĐẦU KỲ TÍNH TOÁN . BẠN CÓ MUỐN SỬ DỤNG TỒN ĐẦU KỲ TÍNH TOÁN KHÔNG ?", true) == "Yes")
                    {
                        Overrite = true;
                    }   
                }
                // SỬ DỤNG TỒN ĐẦU KỲ TÍNH TOÁN HAY NHẬP DỮ LIỆU VÀO
                foreach (T_KHOKETOAN_BCQT NPLSP_Detail in NPLSPQTCollection)
                {
                    if (!Overrite)
                    {
                        foreach (T_KHOKETOAN_TONDAUKY item in TONDAUKYCollection)
                        {
                            if (item.MAHANGHOA == NPLSP_Detail.MAHANGHOA)
                            {
                                NPLSP_Detail.LUONGTONDAU = item.LUONGTON;
                                NPLSP_Detail.TRIGIATONDAU = item.TRIGIA;                           
                            }
                        }
                    }
                    else
                    {
                        foreach (T_KHOKETOAN_TONDAUKY item in TONDAUKYCollection)
                        {
                            if (item.MAHANGHOA == NPLSP_Detail.MAHANGHOA)
                            {
                                item.LUONGTON = NPLSP_Detail.LUONGTONDAU;
                                item.TRIGIA = NPLSP_Detail.TRIGIATONDAU;
                                item.Update();
                            }
                        }
                    }
                }
                foreach (T_KHOKETOAN_BCQT NPLSP_Detail in NPLSPQTCollection)
                {
                    NPLSP_Detail.LUONGTONCUOI = NPLSP_Detail.LUONGTONDAU + NPLSP_Detail.LUONGNHAP - NPLSP_Detail.LUONGXUAT;
                    NPLSP_Detail.TRIGIATONCUOI = NPLSP_Detail.TRIGIATONDAU + NPLSP_Detail.TRIGIANHAP - NPLSP_Detail.TRIGIAXUAT;
                    NPLSP_Detail.InsertUpdate(null);      
                }
                //using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                //{
                //    connection.Open();
                //    SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                //    try
                //    {
                //        foreach (T_KHOKETOAN_BCQT NPLSP_Detail in NPLSPQTCollection)
                //        {
                //            NPLSP_Detail.LUONGTONCUOI = NPLSP_Detail.LUONGTONDAU + NPLSP_Detail.LUONGNHAP - NPLSP_Detail.LUONGXUAT;
                //            NPLSP_Detail.TRIGIATONCUOI = NPLSP_Detail.TRIGIATONDAU + NPLSP_Detail.TRIGIANHAP - NPLSP_Detail.TRIGIAXUAT;
                //            NPLSP_Detail.Insert();
                //        }
                //        transaction.Commit();
                //    }
                //    catch (Exception ex)
                //    {
                //        transaction.Rollback();
                //        Logger.LocalLogger.Instance().WriteMessage(ex);
                //    }
                //    finally
                //    {
                //        connection.Close();
                //    }
                //}
                SetStatus(100, "THÀNH CÔNG");
            }
            catch (System.Exception ex)
            {
                SetStatus(95, ex.ToString());
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //private void DoWork(object obj)
        //{
        //    try
        //    {
        //        string DateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
        //        string DateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");

        //        DateTime dateFrom = clcTuNgay.Value;
        //        DateTime dateTo = clcDenNgay.Value;
        //        SetStatus(0, "LẤY DANH SÁCH TẤT CẢ CÁC PHIẾU XUẤT NHẬP KHO");

        //        PHIEUXNKCollection = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value.ToString() + " AND MAKHO='" + cbbMaKho.Value.ToString() + "' AND NGAYCT BETWEEN '" + DateFrom + "' AND '" + DateTo + "'", "ID");
        //        #region XỬ LÝ NGUYÊN PHỤ LIỆU
        //        SetStatus(4, "XỬ LÝ NGUYÊN PHỤ LIỆU");

        //        List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value.ToString(),"ID");
        //        List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail> NPLSPQTCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail>();
        //        int i = 1;
        //        foreach (T_KHOKETOAN_NGUYENPHULIEU item in NPLCollection)
        //        {
        //            KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail NPLSP_Detail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
        //            NPLSP_Detail.STT = i;
        //            NPLSP_Detail.LoaiHangHoa = 1;
        //            NPLSP_Detail.TaiKhoan = 152;
        //            NPLSP_Detail.MaHangHoa = item.MANPL;
        //            NPLSP_Detail.TenHangHoa = item.TENNPL;
        //            NPLSP_Detail.DVT = VNACCS_Mapper.GetCodeVNACC(item.DVT);
        //            NPLSP_Detail.TonDauKy = 0;
        //            NPLSP_Detail.NhapTrongKy = 0;
        //            NPLSP_Detail.XuatTrongKy = 0;
        //            NPLSP_Detail.TonCuoiKy = 0;
        //            NPLSP_Detail.GhiChu = "";
        //            NPLSPQTCollection.Add(NPLSP_Detail);
        //            i++;

        //        }
        //        #endregion XỬ LÝ NGUYÊN PHỤ LIỆU
        //        #region XỬ LÝ SẢN PHẨM
        //        SetStatus(6, "XỬ LÝ SẢN PHẨM");
        //        List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value.ToString(), "ID");
        //        foreach (T_KHOKETOAN_SANPHAM item in SPCollection)
        //        {
        //            KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail NPLSP_Detail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
        //            NPLSP_Detail.STT = i;
        //            NPLSP_Detail.LoaiHangHoa = 2;
        //            NPLSP_Detail.TaiKhoan = 155;
        //            NPLSP_Detail.MaHangHoa = item.MASP;
        //            NPLSP_Detail.TenHangHoa = item.TENSP;
        //            NPLSP_Detail.DVT = VNACCS_Mapper.GetCodeVNACC(item.DVT);
        //            NPLSP_Detail.TonDauKy = 0;
        //            NPLSP_Detail.NhapTrongKy = 0;
        //            NPLSP_Detail.XuatTrongKy = 0;
        //            NPLSP_Detail.TonCuoiKy = 0;
        //            NPLSP_Detail.GhiChu = "";
        //            NPLSPQTCollection.Add(NPLSP_Detail);
        //            i++;
        //        }
        //        #endregion XỬ LÝ SẢN PHẨM
        //        #region XỬ LÝ PHIẾU XUẤT NHẬP KHO
        //        SetStatus(12, "XỬ LÝ PHIẾU XUẤT NHẬP KHO");
        //        int index = 0;
        //        foreach (T_KHOKETOAN_PHIEUXNKHO PHIEUXNKHO in PHIEUXNKCollection)
        //        {
        //            index++;
        //            SetStatus((index * 80 / PHIEUXNKCollection.Count) + 12, "XỬ LÝ PHIẾU XUẤT NHẬP KHO");
        //            PHIEUXNKHO.HangCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(PHIEUXNKHO.ID);

        //            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA HANGHOA in PHIEUXNKHO.HangCollection)
        //            {
        //                if (PHIEUXNKHO.LOAICHUNGTU=="N")
        //                {
        //                    if (PHIEUXNKHO.LOAIHANGHOA=="N")
        //                    {
        //                        #region NHẬP KHO NGUYÊN PHỤ LIỆU
        //                        foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in NPLSPQTCollection)
        //                        {
        //                            if (HANGHOA.MAHANG == item.MaHangHoa)
        //                            {
        //                                item.NhapTrongKy += HANGHOA.SOLUONGQD;
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    else if (PHIEUXNKHO.LOAIHANGHOA=="S")
        //                    {
        //                        #region NHẬP KHO SẢN PHẨM
        //                        foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in NPLSPQTCollection)
        //                        {
        //                            if (HANGHOA.MAHANG == item.MaHangHoa)
        //                            {
        //                                item.NhapTrongKy += HANGHOA.SOLUONGQD;
        //                            }
        //                        }
        //                        #endregion                                
        //                    }
        //                    else
        //                    {

        //                    }
        //                }
        //                else
        //                {
        //                    if (PHIEUXNKHO.LOAIHANGHOA == "N")
        //                    {
        //                        #region XUẤT KHO NGUYÊN PHỤ LIỆU
        //                        foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in NPLSPQTCollection)
        //                        {
        //                            if (HANGHOA.MAHANG == item.MaHangHoa)
        //                            {
        //                                item.XuatTrongKy += HANGHOA.SOLUONGQD;
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    else if (PHIEUXNKHO.LOAIHANGHOA == "S")
        //                    {
        //                        #region XUẤT KHO SẢN PHẨM
        //                        foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in NPLSPQTCollection)
        //                        {
        //                            if (HANGHOA.MAHANG == item.MaHangHoa)
        //                            {
        //                                item.XuatTrongKy += HANGHOA.SOLUONGQD;
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    else
        //                    {

        //                    }
        //                }
        //            }
        //        }
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        #endregion XỬ LÝ PHIẾU XUẤT NHẬP KHO
        //        SetStatus(95, "LƯU DỮ LIỆU");
        //        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        //            try
        //            {
        //                foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail NPLSP_Detail in NPLSPQTCollection)
        //                {
        //                    NPLSP_Detail.TonCuoiKy = NPLSP_Detail.TonDauKy + NPLSP_Detail.NhapTrongKy - NPLSP_Detail.XuatTrongKy;
        //                    NPLSP_Detail.GoodItem_ID = 5;
        //                    NPLSP_Detail.InsertUpdate();
        //                }
        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                Logger.LocalLogger.Instance().WriteMessage(ex);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //        SetStatus(100, "THÀNH CÔNG");
        //    }
        //    catch (System.Exception ex)
        //    {
        //        SetStatus(95, ex.ToString());
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                //if (e.Row.Cells["PHIEUXNKHO_ID"].Value != null)
                //{
                //    long ID = Convert.ToInt64(e.Row.Cells["PHIEUXNKHO_ID"].Value);
                //    PhieuXNKho = T_KHOKETOAN_PHIEUXNKHO.Load(ID);
                //    e.Row.Cells["PHIEUXNKHO_ID"].Text = PhieuXNKho.SOCT;
                //    e.Row.Cells["NGAYCT"].Text = PhieuXNKho.NGAYCT.ToString();
                //    e.Row.Cells["LOAICHUNGTU"].Text = PhieuXNKho.LOAICHUNGTU;
                //}
                if (e.Row.Cells["LOAIHANG"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANG"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAIHANG"].Text = "Nguyên phụ liệu";
                            break;
                        case "S":
                            e.Row.Cells["LOAIHANG"].Text = "Sản phẩm";
                            break;
                        case "T":
                            e.Row.Cells["LOAIHANG"].Text = "Thiết bị";
                            break;
                        case "H":
                            e.Row.Cells["LOAIHANG"].Text = "Hàng mẫu";
                            break;
                    }
                }
                if (e.Row.Cells["LOAICHUNGTU"].Text != String.Empty)
                {
                    switch (e.Row.Cells["LOAICHUNGTU"].Text.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho";
                            break;
                        case "X":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (uiTab1.SelectedTab.Name == "uiTabPage1")
                {
                    SaveFileDialog sfNPL = new SaveFileDialog();
                    sfNPL.FileName = "Danh sách Phiếu xuất nhập kho Từ ngày (" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ") Đến ngày (" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ").xls";
                    sfNPL.Filter = "Excel files| *.xls";
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    SaveFileDialog sfNPL = new SaveFileDialog();
                    sfNPL.FileName = "Danh sách Hàng hóa Phiếu xuất nhập kho Từ ngày (" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ") Đến ngày (" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ").xls";
                    sfNPL.Filter = "Excel files| *.xls";
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateDate(clcTuNgay, errorProvider, "Từ ngày");
                isValid &= ValidateControl.ValidateDate(clcDenNgay, errorProvider, "Đến ngày");
                isValid &= ValidateControl.ValidateNull(cbbLoaiBC, errorProvider, "Loại báo cáo");
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, "Mã kho");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
            BindData();
            BindDataDetail();
        }
    }
}
