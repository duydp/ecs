using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD0AP0 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A03 { get; set; }
public PropertiesAttribute ADF { get; set; }
public PropertiesAttribute ADG { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A01 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A10 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute RED { get; set; }
public PropertiesAttribute AAA { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute A20 { get; set; }
public PropertiesAttribute A21 { get; set; }
public PropertiesAttribute A22 { get; set; }
public PropertiesAttribute A26 { get; set; }
public PropertiesAttribute A27 { get; set; }
public PropertiesAttribute A28 { get; set; }
public PropertiesAttribute A29 { get; set; }
public PropertiesAttribute A30 { get; set; }
public PropertiesAttribute A31 { get; set; }
public PropertiesAttribute A32 { get; set; }
public PropertiesAttribute A33 { get; set; }
public PropertiesAttribute A34 { get; set; }
public PropertiesAttribute A35 { get; set; }
public PropertiesAttribute A36 { get; set; }
public PropertiesAttribute A37 { get; set; }
public PropertiesAttribute A38 { get; set; }
public PropertiesAttribute A39 { get; set; }
public PropertiesAttribute A40 { get; set; }
public GroupAttribute KA1 { get; set; }
public PropertiesAttribute A49 { get; set; }
public PropertiesAttribute A50 { get; set; }
public PropertiesAttribute A53 { get; set; }
public PropertiesAttribute A54 { get; set; }
public PropertiesAttribute A57 { get; set; }
public PropertiesAttribute A51 { get; set; }
public PropertiesAttribute A52 { get; set; }
public PropertiesAttribute A42 { get; set; }
public PropertiesAttribute A43 { get; set; }
public PropertiesAttribute A44 { get; set; }
public PropertiesAttribute A45 { get; set; }
public PropertiesAttribute A46 { get; set; }
public PropertiesAttribute A47 { get; set; }
public PropertiesAttribute A48 { get; set; }
public PropertiesAttribute A62 { get; set; }
public PropertiesAttribute A58 { get; set; }
public GroupAttribute KB1 { get; set; }
public GroupAttribute KC1 { get; set; }
public PropertiesAttribute A72 { get; set; }
public PropertiesAttribute A73 { get; set; }
public PropertiesAttribute A74 { get; set; }
public PropertiesAttribute IVD { get; set; }
public PropertiesAttribute IVP { get; set; }
public PropertiesAttribute A75 { get; set; }
public PropertiesAttribute A76 { get; set; }
public PropertiesAttribute A77 { get; set; }
public PropertiesAttribute A78 { get; set; }
public PropertiesAttribute A87 { get; set; }
public PropertiesAttribute A97 { get; set; }
public PropertiesAttribute A98 { get; set; }
public PropertiesAttribute B02 { get; set; }
public PropertiesAttribute A88 { get; set; }
public PropertiesAttribute A89 { get; set; }
public PropertiesAttribute A94 { get; set; }
public PropertiesAttribute A95 { get; set; }
public PropertiesAttribute A96 { get; set; }
public PropertiesAttribute ADD { get; set; }
public PropertiesAttribute ADE { get; set; }
public PropertiesAttribute A79 { get; set; }
public PropertiesAttribute A80 { get; set; }
public PropertiesAttribute A81 { get; set; }
public PropertiesAttribute A82 { get; set; }
public PropertiesAttribute A83 { get; set; }
public PropertiesAttribute A84 { get; set; }
public PropertiesAttribute A85 { get; set; }
public GroupAttribute VR1 { get; set; }
public PropertiesAttribute VLD { get; set; }
public GroupAttribute KE1 { get; set; }
public PropertiesAttribute B03 { get; set; }
public PropertiesAttribute B04 { get; set; }
public GroupAttribute KI1 { get; set; }
public PropertiesAttribute B07 { get; set; }
public PropertiesAttribute B12 { get; set; }
public PropertiesAttribute TPM { get; set; }
public PropertiesAttribute B13 { get; set; }
public PropertiesAttribute B14 { get; set; }
public GroupAttribute EA1 { get; set; }
public PropertiesAttribute B15 { get; set; }
public PropertiesAttribute B19 { get; set; }
public PropertiesAttribute B17 { get; set; }
public PropertiesAttribute B99 { get; set; }
public GroupAttribute D01 { get; set; }
public PropertiesAttribute ADH { get; set; }
public PropertiesAttribute ADO { get; set; }
public PropertiesAttribute ADP { get; set; }
public PropertiesAttribute ADQ { get; set; }
public PropertiesAttribute ADI { get; set; }
public PropertiesAttribute ADJ { get; set; }
public PropertiesAttribute ADK { get; set; }
public PropertiesAttribute ADL { get; set; }
public PropertiesAttribute ADM { get; set; }
public PropertiesAttribute B21 { get; set; }
public PropertiesAttribute AD4 { get; set; }
public PropertiesAttribute B22 { get; set; }
public PropertiesAttribute AD5 { get; set; }
public PropertiesAttribute B23 { get; set; }
public PropertiesAttribute B24 { get; set; }
public PropertiesAttribute AD6 { get; set; }
public PropertiesAttribute B25 { get; set; }
public PropertiesAttribute AD7 { get; set; }
public PropertiesAttribute B26 { get; set; }
public PropertiesAttribute B31 { get; set; }
public PropertiesAttribute AD8 { get; set; }
public PropertiesAttribute B32 { get; set; }
public PropertiesAttribute AD9 { get; set; }
public PropertiesAttribute B34 { get; set; }
public PropertiesAttribute ADA { get; set; }
public PropertiesAttribute B35 { get; set; }
public PropertiesAttribute B42 { get; set; }
public GroupAttribute KL1 { get; set; }
public PropertiesAttribute ADX { get; set; }
public PropertiesAttribute ADY { get; set; }
public PropertiesAttribute ADZ { get; set; }
public PropertiesAttribute B27 { get; set; }
public GroupAttribute ST1 { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute B28 { get; set; }

public VAD0AP0()
        {
ICN = new PropertiesAttribute(12, typeof(decimal));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(decimal));
DNO = new PropertiesAttribute(2, typeof(decimal));
TDN = new PropertiesAttribute(12, typeof(decimal));
A07 = new PropertiesAttribute(3, typeof(string));
A03 = new PropertiesAttribute(3, typeof(string));
ADF = new PropertiesAttribute(1, typeof(string));
ADG = new PropertiesAttribute(1, typeof(string));
A06 = new PropertiesAttribute(1, typeof(string));
A01 = new PropertiesAttribute(4, typeof(string));
A08 = new PropertiesAttribute(10, typeof(string));
A09 = new PropertiesAttribute(2, typeof(string));
A10 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(decimal));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(decimal));
RED = new PropertiesAttribute(8, typeof(DateTime));
AAA = new PropertiesAttribute(1, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
A20 = new PropertiesAttribute(300, typeof(string));
A21 = new PropertiesAttribute(7, typeof(string));
A22 = new PropertiesAttribute(300, typeof(string));
A26 = new PropertiesAttribute(20, typeof(string));
A27 = new PropertiesAttribute(13, typeof(string));
A28 = new PropertiesAttribute(300, typeof(string));
A29 = new PropertiesAttribute(13, typeof(string));
A30 = new PropertiesAttribute(70, typeof(string));
A31 = new PropertiesAttribute(9, typeof(string));
A32 = new PropertiesAttribute(35, typeof(string));
A33 = new PropertiesAttribute(35, typeof(string));
A34 = new PropertiesAttribute(35, typeof(string));
A35 = new PropertiesAttribute(35, typeof(string));
A36 = new PropertiesAttribute(2, typeof(string));
A37 = new PropertiesAttribute(70, typeof(string));
A38 = new PropertiesAttribute(5, typeof(string));
A39 = new PropertiesAttribute(50, typeof(string));
A40 = new PropertiesAttribute(5, typeof(string));
A49 = new PropertiesAttribute(8, typeof(decimal));
A50 = new PropertiesAttribute(3, typeof(string));
A53 = new PropertiesAttribute(10, typeof(decimal));
A54 = new PropertiesAttribute(3, typeof(string));
A57 = new PropertiesAttribute(3, typeof(decimal));
A51 = new PropertiesAttribute(7, typeof(string));
A52 = new PropertiesAttribute(20, typeof(string));
A42 = new PropertiesAttribute(6, typeof(string));
A43 = new PropertiesAttribute(35, typeof(string));
A44 = new PropertiesAttribute(5, typeof(string));
A45 = new PropertiesAttribute(35, typeof(string));
A46 = new PropertiesAttribute(9, typeof(string));
A47 = new PropertiesAttribute(35, typeof(string));
A48 = new PropertiesAttribute(8, typeof(DateTime));
A62 = new PropertiesAttribute(140, typeof(string));
A58 = new PropertiesAttribute(8, typeof(DateTime));
A72 = new PropertiesAttribute(1, typeof(string));
A73 = new PropertiesAttribute(35, typeof(string));
A74 = new PropertiesAttribute(12, typeof(decimal));
IVD = new PropertiesAttribute(8, typeof(DateTime));
IVP = new PropertiesAttribute(7, typeof(string));
A75 = new PropertiesAttribute(1, typeof(string));
A76 = new PropertiesAttribute(3, typeof(string));
A77 = new PropertiesAttribute(3, typeof(string));
A78 = new PropertiesAttribute(20, typeof(decimal));
A87 = new PropertiesAttribute(19, typeof(decimal));
A97 = new PropertiesAttribute(20, typeof(decimal));
A98 = new PropertiesAttribute(1, typeof(string));
B02 = new PropertiesAttribute(1, typeof(string));
A88 = new PropertiesAttribute(1, typeof(string));
A89 = new PropertiesAttribute(9, typeof(string));
A94 = new PropertiesAttribute(1, typeof(string));
A95 = new PropertiesAttribute(2, typeof(string));
A96 = new PropertiesAttribute(22, typeof(string));
ADD = new PropertiesAttribute(3, typeof(string));
ADE = new PropertiesAttribute(20, typeof(decimal));
A79 = new PropertiesAttribute(1, typeof(string));
A80 = new PropertiesAttribute(3, typeof(string));
A81 = new PropertiesAttribute(18, typeof(decimal));
A82 = new PropertiesAttribute(1, typeof(string));
A83 = new PropertiesAttribute(3, typeof(string));
A84 = new PropertiesAttribute(16, typeof(decimal));
A85 = new PropertiesAttribute(6, typeof(string));
VLD = new PropertiesAttribute(840, typeof(string));
B03 = new PropertiesAttribute(11, typeof(decimal));
B04 = new PropertiesAttribute(11, typeof(decimal));
B07 = new PropertiesAttribute(1, typeof(string));
B12 = new PropertiesAttribute(1, typeof(string));
TPM = new PropertiesAttribute(1, typeof(string));
B13 = new PropertiesAttribute(2, typeof(decimal));
B14 = new PropertiesAttribute(2, typeof(decimal));
B15 = new PropertiesAttribute(300, typeof(string));
B19 = new PropertiesAttribute(20, typeof(string));
B17 = new PropertiesAttribute(5, typeof(string));
B99 = new PropertiesAttribute(1, typeof(string));
ADH = new PropertiesAttribute(11, typeof(string));
ADO = new PropertiesAttribute(4, typeof(decimal));
ADP = new PropertiesAttribute(10, typeof(string));
ADQ = new PropertiesAttribute(10, typeof(string));
ADI = new PropertiesAttribute(1, typeof(string));
ADJ = new PropertiesAttribute(11, typeof(string));
ADK = new PropertiesAttribute(4, typeof(decimal));
ADL = new PropertiesAttribute(10, typeof(string));
ADM = new PropertiesAttribute(10, typeof(string));
B21 = new PropertiesAttribute(8, typeof(DateTime));
AD4 = new PropertiesAttribute(6, typeof(decimal));
B22 = new PropertiesAttribute(8, typeof(DateTime));
AD5 = new PropertiesAttribute(6, typeof(decimal));
B23 = new PropertiesAttribute(2, typeof(string));
B24 = new PropertiesAttribute(8, typeof(DateTime));
AD6 = new PropertiesAttribute(6, typeof(decimal));
B25 = new PropertiesAttribute(8, typeof(DateTime));
AD7 = new PropertiesAttribute(6, typeof(decimal));
B26 = new PropertiesAttribute(2, typeof(decimal));
B31 = new PropertiesAttribute(8, typeof(DateTime));
AD8 = new PropertiesAttribute(6, typeof(decimal));
B32 = new PropertiesAttribute(8, typeof(DateTime));
AD9 = new PropertiesAttribute(6, typeof(decimal));
B34 = new PropertiesAttribute(8, typeof(DateTime));
ADA = new PropertiesAttribute(6, typeof(decimal));
B35 = new PropertiesAttribute(5, typeof(string));
B42 = new PropertiesAttribute(81, typeof(string));
ADX = new PropertiesAttribute(1, typeof(string));
ADY = new PropertiesAttribute(27, typeof(string));
ADZ = new PropertiesAttribute(8, typeof(DateTime));
B27 = new PropertiesAttribute(8, typeof(DateTime));
ARP = new PropertiesAttribute(7, typeof(string));
B28 = new PropertiesAttribute(8, typeof(DateTime));
#region KA1
List<PropertiesAttribute> listKA1 = new List<PropertiesAttribute>();
listKA1.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAD0AP0_KA1, typeof(string)));
KA1 = new GroupAttribute("KA1", 5, listKA1);
#endregion KA1
#region KB1
List<PropertiesAttribute> listKB1 = new List<PropertiesAttribute>();
listKB1.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAD0AP0_KB1, typeof(string)));
KB1 = new GroupAttribute("KB1", 5, listKB1);
#endregion KB1
#region KC1
List<PropertiesAttribute> listKC1 = new List<PropertiesAttribute>();
listKC1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAD0AP0_KC1, typeof(string)));
listKC1.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD0AP0_KD1, typeof(string)));
KC1 = new GroupAttribute("KC1", 5, listKC1);
#endregion KC1
#region VR1
List<PropertiesAttribute> listVR1 = new List<PropertiesAttribute>();
listVR1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAD0AP0_VR1, typeof(string)));
listVR1.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAD0AP0_VI1, typeof(string)));
listVR1.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAD0AP0_VC1, typeof(string)));
listVR1.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD0AP0_VP1, typeof(int)));
listVR1.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD0AP0_VT1, typeof(int)));
VR1 = new GroupAttribute("VR1", 5, listVR1);
#endregion VR1
#region KE1
List<PropertiesAttribute> listKE1 = new List<PropertiesAttribute>();
listKE1.Add(new PropertiesAttribute(1, 6, EnumGroupID.VAD0AP0_KE1, typeof(string)));
listKE1.Add(new PropertiesAttribute(27, 6, EnumGroupID.VAD0AP0_KF1, typeof(string)));
listKE1.Add(new PropertiesAttribute(11, 6, EnumGroupID.VAD0AP0_KG1, typeof(int)));
listKE1.Add(new PropertiesAttribute(2, 6, EnumGroupID.VAD0AP0_KH1, typeof(int)));
KE1 = new GroupAttribute("KE1", 6, listKE1);
#endregion KE1
#region KI1
List<PropertiesAttribute> listKI1 = new List<PropertiesAttribute>();
listKI1.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAD0AP0_KI1, typeof(string)));
listKI1.Add(new PropertiesAttribute(9, 3, EnumGroupID.VAD0AP0_KJ1, typeof(int)));
KI1 = new GroupAttribute("KI1", 3, listKI1);
#endregion KI1
#region EA1
List<PropertiesAttribute> listEA1 = new List<PropertiesAttribute>();
listEA1.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAD0AP0_EA1, typeof(string)));
listEA1.Add(new PropertiesAttribute(12, 3, EnumGroupID.VAD0AP0_EN1, typeof(int)));
EA1 = new GroupAttribute("EA1", 3, listEA1);
#endregion EA1
#region D01
List<PropertiesAttribute> listD01 = new List<PropertiesAttribute>();
listD01.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAD0AP0_D01, typeof(DateTime)));
listD01.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAD0AP0_T01, typeof(string)));
listD01.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAD0AP0_I01, typeof(string)));
D01 = new GroupAttribute("D01", 10, listD01);
#endregion D01
#region KL1
List<PropertiesAttribute> listKL1 = new List<PropertiesAttribute>();
listKL1.Add(new PropertiesAttribute(1, 6, EnumGroupID.VAD0AP0_KL1, typeof(string)));
listKL1.Add(new PropertiesAttribute(27, 6, EnumGroupID.VAD0AP0_KM1, typeof(string)));
listKL1.Add(new PropertiesAttribute(8, 6, EnumGroupID.VAD0AP0_KN1, typeof(DateTime)));
KL1 = new GroupAttribute("KL1", 6, listKL1);
#endregion KL1
#region ST1
List<PropertiesAttribute> listST1 = new List<PropertiesAttribute>();
listST1.Add(new PropertiesAttribute(7, 3, EnumGroupID.VAD0AP0_ST1, typeof(string)));
listST1.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAD0AP0_BD1, typeof(DateTime)));
listST1.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAD0AP0_CD1, typeof(DateTime)));
ST1 = new GroupAttribute("ST1", 3, listST1);
#endregion ST1
TongSoByte = 10664;
}

}public partial class EnumGroupID
    {
public static readonly string VAD0AP0_KA1 = "VAD0AP0_KA1";
public static readonly string VAD0AP0_KB1 = "VAD0AP0_KB1";
public static readonly string VAD0AP0_KC1 = "VAD0AP0_KC1";
public static readonly string VAD0AP0_KD1 = "VAD0AP0_KD1";
public static readonly string VAD0AP0_VR1 = "VAD0AP0_VR1";
public static readonly string VAD0AP0_VI1 = "VAD0AP0_VI1";
public static readonly string VAD0AP0_VC1 = "VAD0AP0_VC1";
public static readonly string VAD0AP0_VP1 = "VAD0AP0_VP1";
public static readonly string VAD0AP0_VT1 = "VAD0AP0_VT1";
public static readonly string VAD0AP0_KE1 = "VAD0AP0_KE1";
public static readonly string VAD0AP0_KF1 = "VAD0AP0_KF1";
public static readonly string VAD0AP0_KG1 = "VAD0AP0_KG1";
public static readonly string VAD0AP0_KH1 = "VAD0AP0_KH1";
public static readonly string VAD0AP0_KI1 = "VAD0AP0_KI1";
public static readonly string VAD0AP0_KJ1 = "VAD0AP0_KJ1";
public static readonly string VAD0AP0_EA1 = "VAD0AP0_EA1";
public static readonly string VAD0AP0_EN1 = "VAD0AP0_EN1";
public static readonly string VAD0AP0_D01 = "VAD0AP0_D01";
public static readonly string VAD0AP0_T01 = "VAD0AP0_T01";
public static readonly string VAD0AP0_I01 = "VAD0AP0_I01";
public static readonly string VAD0AP0_KL1 = "VAD0AP0_KL1";
public static readonly string VAD0AP0_KM1 = "VAD0AP0_KM1";
public static readonly string VAD0AP0_KN1 = "VAD0AP0_KN1";
public static readonly string VAD0AP0_ST1 = "VAD0AP0_ST1";
public static readonly string VAD0AP0_BD1 = "VAD0AP0_BD1";
public static readonly string VAD0AP0_CD1 = "VAD0AP0_CD1";
}

}
