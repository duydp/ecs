﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Linq;

namespace Utils
{
    public class Utilities
    {
        public static int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        /// <summary>
        /// Lấy danh sách tên WorkSheet đọc từ file Excel.
        /// </summary>
        /// <param name="fileName">Đường dẫn và tên file đầy đủ</param>
        /// <param name="errorReturn">Kết quả lỗi trả về</param>
        /// <returns>List<String></returns>
        public static List<string> GetWorkSheetName(string fileName, ref string errorReturn)
        {
            List<string> result = new List<string>();

            try
            {
                Infragistics.Excel.Workbook wb = Infragistics.Excel.Workbook.Load(fileName, true);

                foreach (Infragistics.Excel.Worksheet ws in wb.Worksheets)
                {
                    result.Add(ws.Name);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                if (ex.Message.Contains("a format which is not supported"))
                    errorReturn = "Lỗi khi đọc file: định dạng dữ liệu không hỗ trợ. \nBạn hãy thử 'Sao chép dữ liệu sang một file Excel mới'";
                else
                    errorReturn = "Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.";
            }

            return result;
        }

        /// <summary>
        /// Đọc dữ liệu từ file Excel.
        /// </summary>
        /// <param name="fileName">Đường dẫn và tên file đầy đủ</param>
        /// <param name="sheetName">Tên WorkSheet</param>
        /// <param name="beginRow">Dòng bắt đầu đọc dữ liệu</param>
        /// <param name="columnNameCheck">Tên cột cần kiểm tra</param>
        /// <param name="errorReturn">Kết quả lỗi trả về</param>
        /// <param name="datetype">Định dạng ngày tháng: 0: English, 1: Vietnamese</param>
        /// <returns>List<String></returns>        
        public static List<ElementEntity> ReadExcel(string fileName, string sheetName, int beginRow, ref string errorReturn, int datetype)
        {
            List<ElementEntity> listBangKeChungTu = new List<ElementEntity>();

            try
            {
                int beginRowIndex = beginRow - 1;

                if (beginRowIndex < 0) { errorReturn = "Dòng bắt đầu phải lớn hơn 0"; return null; }

                Infragistics.Excel.Workbook wb = new Infragistics.Excel.Workbook();
                Infragistics.Excel.Worksheet ws = null;

                try
                {
                    wb = Infragistics.Excel.Workbook.Load(fileName, true);
                }
                catch (Exception ex) { errorReturn = "Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc."; return null; }

                try
                {
                    ws = wb.Worksheets[sheetName];
                }
                catch (Exception ex) { errorReturn = "Không tồn tại sheet \"" + sheetName + "\""; return null; }

                Infragistics.Excel.WorksheetRowCollection wsrc = ws.Rows;

                //Hungtq update 23/03/2011.
                System.Globalization.CultureInfo cul = null;
                if (datetype == 0) //"MM//dd/yyyy" - Tieng Anh
                    cul = new System.Globalization.CultureInfo("en-US");
                else if (datetype == 1 || datetype == -1) // "dd/MM/yyyy" - Tieng Viet
                    cul = new System.Globalization.CultureInfo("vi-VN");

                ElementEntity bangKe = null;
                foreach (Infragistics.Excel.WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRowIndex)
                    {
                        try
                        {
                            if (wsr.Cells[ColumnExcel.IntNo].Value == null)
                                continue;

                            bangKe = ConvertWorksheetRowToEntity(cul, wsr);

                            if (bangKe != null)
                                listBangKeChungTu.Add(bangKe);
                        }
                        catch (Exception ex)
                        {
                            errorReturn = "Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ. " + ex.Message;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return listBangKeChungTu;
        }

        private static ElementEntity ConvertWorksheetRowToEntity(System.Globalization.CultureInfo culture, Infragistics.Excel.WorksheetRow wsr)
        {
            try
            {
                ElementEntity newObj = new ElementEntity();

                //Kieu Ngay
                //if (wsr.Cells[ColumnExcel.IntNgayHoaDon].Value.GetType().Name == "String")
                //    newObj.NgayHoaDon = Convert.ToDateTime(wsr.Cells[ColumnExcel.IntNgayHoaDon].Value, culture);
                //else if (wsr.Cells[ColumnExcel.IntNgayHoaDon].Value.GetType().Name == "Double")
                //    newObj.NgayHoaDon = System.DateTime.FromOADate((double)wsr.Cells[ColumnExcel.IntNgayHoaDon].Value);

                newObj.No = wsr.Cells[ColumnExcel.IntNo].Value != null ? wsr.Cells[ColumnExcel.IntNo].Value.ToString() : "";
                newObj.Column = wsr.Cells[ColumnExcel.IntColumn].Value != null ? wsr.Cells[ColumnExcel.IntColumn].Value.ToString() : "";
                newObj.ElementNameJP = wsr.Cells[ColumnExcel.IntElementNameJP].Value != null ? wsr.Cells[ColumnExcel.IntElementNameJP].Value.ToString() : "";
                newObj.ElementNameEN = wsr.Cells[ColumnExcel.IntElementNameEN].Value != null ? wsr.Cells[ColumnExcel.IntElementNameEN].Value.ToString() : "";
                newObj.ElementNameVN = wsr.Cells[ColumnExcel.IntElementNameVN].Value != null ? wsr.Cells[ColumnExcel.IntElementNameVN].Value.ToString() : "";
                newObj.ID = wsr.Cells[ColumnExcel.IntID].Value != null ? wsr.Cells[ColumnExcel.IntID].Value.ToString() : "";
                newObj.Attribute = wsr.Cells[ColumnExcel.IntAttribute].Value != null ? wsr.Cells[ColumnExcel.IntAttribute].Value.ToString() : "";
                newObj.Digit = wsr.Cells[ColumnExcel.IntDigit].Value != null ? wsr.Cells[ColumnExcel.IntDigit].Value.ToString() : "";
                newObj.Repetition1 = wsr.Cells[ColumnExcel.IntRepetition1].Value != null ? wsr.Cells[ColumnExcel.IntRepetition1].Value.ToString() : "";
                newObj.Condition1 = wsr.Cells[ColumnExcel.IntCondition1].Value != null ? wsr.Cells[ColumnExcel.IntCondition1].Value.ToString() : "";
                newObj.Condition2 = wsr.Cells[ColumnExcel.IntCondition2].Value != null ? wsr.Cells[ColumnExcel.IntCondition2].Value.ToString() : "";
                newObj.Condition3 = wsr.Cells[ColumnExcel.IntCondition3].Value != null ? wsr.Cells[ColumnExcel.IntCondition3].Value.ToString() : "";
                newObj.Code = wsr.Cells[ColumnExcel.IntCode].Value != null ? wsr.Cells[ColumnExcel.IntCode].Value.ToString() : "";
                newObj.Format = wsr.Cells[ColumnExcel.IntFormat].Value != null ? wsr.Cells[ColumnExcel.IntFormat].Value.ToString() : "";
                newObj.ReferenceDB = wsr.Cells[ColumnExcel.IntReferenceDB].Value != null ? wsr.Cells[ColumnExcel.IntReferenceDB].Value.ToString() : "";
                newObj.InputConditionJP = wsr.Cells[ColumnExcel.IntInputConditionJP].Value != null ? wsr.Cells[ColumnExcel.IntInputConditionJP].Value.ToString() : "";
                if (ColumnExcel.IntInputConditionEN > 0)
                    newObj.InputConditionEN = wsr.Cells[ColumnExcel.IntInputConditionEN].Value != null ? wsr.Cells[ColumnExcel.IntInputConditionEN].Value.ToString() : "";
                else
                    newObj.InputConditionEN = "";

                return newObj;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }

            return null;
        }

        #region SAVE NODE XML

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlAppSettings(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                result = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return result;
        }

        public static void SaveNodeXmlSetting(string name, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string s = "//setting[@name='" + name + "']";
                doc.SelectSingleNode(s).SelectSingleNode("value").ChildNodes[0].Value = value.ToString();
                doc.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[0].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion
    }
}
