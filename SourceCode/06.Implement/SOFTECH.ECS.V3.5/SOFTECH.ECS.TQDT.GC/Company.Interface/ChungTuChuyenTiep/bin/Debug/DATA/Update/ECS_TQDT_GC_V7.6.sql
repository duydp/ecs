/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V4_DONGKINH    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 02/26/2013 10:15:48 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_GC_ThanhKhoan]'
GO
ALTER TABLE [dbo].[t_KDT_GC_ThanhKhoan] ADD
[LoaiChungTu] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThamChieuCT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MoTaCT] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_Insert]
	@HopDong_ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NgayDangKy datetime,
	@GhiChu nvarchar(2000),
	@GUIDSTR nvarchar(50),
	@TrangThaiXuLy int,
	@ActionStatus int,
	@LyDoHuy nvarchar(256),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(200),
	@MaHaiQuan nvarchar(10),
	@LoaiChungTu nvarchar(35),
	@ThamChieuCT nvarchar(50),
	@MoTaCT nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_ThanhKhoan]
(
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NgayDangKy],
	[GhiChu],
	[GUIDSTR],
	[TrangThaiXuLy],
	[ActionStatus],
	[LyDoHuy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LoaiChungTu],
	[ThamChieuCT],
	[MoTaCT]
)
VALUES 
(
	@HopDong_ID,
	@SoTiepNhan,
	@NgayTiepNhan,
	@NgayDangKy,
	@GhiChu,
	@GUIDSTR,
	@TrangThaiXuLy,
	@ActionStatus,
	@LyDoHuy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaHaiQuan,
	@LoaiChungTu,
	@ThamChieuCT,
	@MoTaCT
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_Update]
	@ID bigint,
	@HopDong_ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NgayDangKy datetime,
	@GhiChu nvarchar(2000),
	@GUIDSTR nvarchar(50),
	@TrangThaiXuLy int,
	@ActionStatus int,
	@LyDoHuy nvarchar(256),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(200),
	@MaHaiQuan nvarchar(10),
	@LoaiChungTu nvarchar(35),
	@ThamChieuCT nvarchar(50),
	@MoTaCT nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_GC_ThanhKhoan]
SET
	[HopDong_ID] = @HopDong_ID,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[NgayDangKy] = @NgayDangKy,
	[GhiChu] = @GhiChu,
	[GUIDSTR] = @GUIDSTR,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[ActionStatus] = @ActionStatus,
	[LyDoHuy] = @LyDoHuy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[LoaiChungTu] = @LoaiChungTu,
	[ThamChieuCT] = @ThamChieuCT,
	[MoTaCT] = @MoTaCT
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_InsertUpdate]
	@ID bigint,
	@HopDong_ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NgayDangKy datetime,
	@GhiChu nvarchar(2000),
	@GUIDSTR nvarchar(50),
	@TrangThaiXuLy int,
	@ActionStatus int,
	@LyDoHuy nvarchar(256),
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(200),
	@MaHaiQuan nvarchar(10),
	@LoaiChungTu nvarchar(35),
	@ThamChieuCT nvarchar(50),
	@MoTaCT nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_ThanhKhoan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_ThanhKhoan] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[NgayDangKy] = @NgayDangKy,
			[GhiChu] = @GhiChu,
			[GUIDSTR] = @GUIDSTR,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[ActionStatus] = @ActionStatus,
			[LyDoHuy] = @LyDoHuy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[LoaiChungTu] = @LoaiChungTu,
			[ThamChieuCT] = @ThamChieuCT,
			[MoTaCT] = @MoTaCT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_ThanhKhoan]
		(
			[HopDong_ID],
			[SoTiepNhan],
			[NgayTiepNhan],
			[NgayDangKy],
			[GhiChu],
			[GUIDSTR],
			[TrangThaiXuLy],
			[ActionStatus],
			[LyDoHuy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaHaiQuan],
			[LoaiChungTu],
			[ThamChieuCT],
			[MoTaCT]
		)
		VALUES 
		(
			@HopDong_ID,
			@SoTiepNhan,
			@NgayTiepNhan,
			@NgayDangKy,
			@GhiChu,
			@GUIDSTR,
			@TrangThaiXuLy,
			@ActionStatus,
			@LyDoHuy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaHaiQuan,
			@LoaiChungTu,
			@ThamChieuCT,
			@MoTaCT
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_ThanhKhoan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_DeleteBy_HopDong_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_ThanhKhoan]
WHERE
	[HopDong_ID] = @HopDong_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NgayDangKy],
	[GhiChu],
	[GUIDSTR],
	[TrangThaiXuLy],
	[ActionStatus],
	[LyDoHuy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LoaiChungTu],
	[ThamChieuCT],
	[MoTaCT]
FROM
	[dbo].[t_KDT_GC_ThanhKhoan]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_SelectBy_HopDong_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NgayDangKy],
	[GhiChu],
	[GUIDSTR],
	[TrangThaiXuLy],
	[ActionStatus],
	[LyDoHuy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LoaiChungTu],
	[ThamChieuCT],
	[MoTaCT]
FROM
	[dbo].[t_KDT_GC_ThanhKhoan]
WHERE
	[HopDong_ID] = @HopDong_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NgayDangKy],
	[GhiChu],
	[GUIDSTR],
	[TrangThaiXuLy],
	[ActionStatus],
	[LyDoHuy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LoaiChungTu],
	[ThamChieuCT],
	[MoTaCT]
FROM
	[dbo].[t_KDT_GC_ThanhKhoan]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_GC_HangThanhKhoan]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HangThanhKhoan] ADD
[LuongXuat] [numeric] (18, 4) NULL CONSTRAINT [DF_t_KDT_GC_HangThanhKhoan_LuongXuat] DEFAULT ((0)),
[LuongCU] [numeric] (18, 4) NULL CONSTRAINT [DF_t_KDT_GC_HangThanhKhoan_LuongCU] DEFAULT ((0)),
[LuongTon] [numeric] (18, 4) NULL CONSTRAINT [DF_t_KDT_GC_HangThanhKhoan_LuongTon] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_KDT_GC_HangThanhKhoan] ALTER COLUMN [SoLuong] [numeric] (14, 4) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Insert]
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HangThanhKhoan]
(
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon]
)
VALUES 
(
	@Master_ID,
	@LoaiHang,
	@MaHang,
	@TenHang,
	@MaHS,
	@SoLuong,
	@DVT_ID,
	@GhiChu,
	@LuongXuat,
	@LuongCU,
	@LuongTon
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Update]
	@ID bigint,
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_HangThanhKhoan]
SET
	[Master_ID] = @Master_ID,
	[LoaiHang] = @LoaiHang,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[SoLuong] = @SoLuong,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[LuongXuat] = @LuongXuat,
	[LuongCU] = @LuongCU,
	[LuongTon] = @LuongTon
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@LoaiHang int,
	@MaHang nvarchar(30),
	@TenHang nvarchar(256),
	@MaHS nvarchar(14),
	@SoLuong numeric(14, 4),
	@DVT_ID char(3),
	@GhiChu nvarchar(256),
	@LuongXuat numeric(18, 4),
	@LuongCU numeric(18, 4),
	@LuongTon numeric(18, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HangThanhKhoan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangThanhKhoan] 
		SET
			[Master_ID] = @Master_ID,
			[LoaiHang] = @LoaiHang,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[SoLuong] = @SoLuong,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[LuongXuat] = @LuongXuat,
			[LuongCU] = @LuongCU,
			[LuongTon] = @LuongTon
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HangThanhKhoan]
		(
			[Master_ID],
			[LoaiHang],
			[MaHang],
			[TenHang],
			[MaHS],
			[SoLuong],
			[DVT_ID],
			[GhiChu],
			[LuongXuat],
			[LuongCU],
			[LuongTon]
		)
		VALUES 
		(
			@Master_ID,
			@LoaiHang,
			@MaHang,
			@TenHang,
			@MaHS,
			@SoLuong,
			@DVT_ID,
			@GhiChu,
			@LuongXuat,
			@LuongCU,
			@LuongTon
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[Master_ID] = @Master_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]
WHERE
	[Master_ID] = @Master_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon]
FROM
	[dbo].[t_KDT_GC_HangThanhKhoan]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_ThanhKhoan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ThanhKhoan_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThanhKhoan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ThanhKhoan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NgayDangKy],
	[GhiChu],
	[GUIDSTR],
	[TrangThaiXuLy],
	[ActionStatus],
	[LyDoHuy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LoaiChungTu],
	[ThamChieuCT],
	[MoTaCT]
FROM [dbo].[t_KDT_GC_ThanhKhoan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangThanhKhoan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 26, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_HangThanhKhoan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[LoaiHang],
	[MaHang],
	[TenHang],
	[MaHS],
	[SoLuong],
	[DVT_ID],
	[GhiChu],
	[LuongXuat],
	[LuongCU],
	[LuongTon]
FROM [dbo].[t_KDT_GC_HangThanhKhoan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_KDT_GC_HangThanhKhoan]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HangThanhKhoan] ADD CONSTRAINT [DF_t_KDT_GC_HangThanhKhoan_SoLuong] DEFAULT ((0)) FOR [SoLuong]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
UPDATE [dbo].[t_HaiQuan_Version]
SET [Version] = '7.6'
  ,[Date] = getdate()
  ,[Notes] = ''