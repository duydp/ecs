﻿IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TINHBIENAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Tịnh Biên' WHERE TableID='A014A' AND CustomsCode = 'TINHBIENAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TINHBIENAG','00',N'Chi cục HQ CK Tịnh Biên')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HOIDONGAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vĩnh Hội Đông' WHERE TableID='A014A' AND CustomsCode = 'HOIDONGAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HOIDONGAG','00',N'Chi cục HQ Vĩnh Hội Đông')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VXUONGAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Vĩnh Xương' WHERE TableID='A014A' AND CustomsCode = 'VXUONGAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VXUONGAG','00',N'Chi cục HQ CK Vĩnh Xương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BACDAIAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Đai' WHERE TableID='A014A' AND CustomsCode = 'BACDAIAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BACDAIAG','00',N'Chi cục HQ Bắc Đai')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KBINHAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Khánh Bình' WHERE TableID='A014A' AND CustomsCode = 'KBINHAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KBINHAG','00',N'Chi cục HQ Khánh Bình')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CMYTHOIAG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng Mỹ Thới' WHERE TableID='A014A' AND CustomsCode = 'CMYTHOIAG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CMYTHOIAG','00',N'Chi cục HQ Cảng Mỹ Thới')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCATLOVT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng Cát Lở' WHERE TableID='A014A' AND CustomsCode = 'CCATLOVT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCATLOVT','00',N'Chi cục HQ Cảng Cát Lở')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KNQPMVTAU' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK  Kho ngoại quan' WHERE TableID='A014A' AND CustomsCode = 'KNQPMVTAU' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KNQPMVTAU','00',N'Đội Thủ tục hàng hóa XNK  Kho ngoại quan')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PSAPMVTAU' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục SP-PSA' WHERE TableID='A014A' AND CustomsCode = 'PSAPMVTAU' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PSAPMVTAU','00',N'Đội Thủ tục SP-PSA')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSANBAYVT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu' WHERE TableID='A014A' AND CustomsCode = 'CSANBAYVT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSANBAYVT','00',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CONDAOVT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Côn Đảo' WHERE TableID='A014A' AND CustomsCode = 'CONDAOVT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CONDAOVT','00',N'Chi cục HQ Côn Đảo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCAIMEPVT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK cảng Cái Mép' WHERE TableID='A014A' AND CustomsCode = 'CCAIMEPVT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCAIMEPVT','00',N'Chi cục HQ CK cảng Cái Mép')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DKCNYPBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ quản lý KCN Yên Phong' WHERE TableID='A014A' AND CustomsCode = 'DKCNYPBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DKCNYPBN','00',N'Đội TTHQ quản lý KCN Yên Phong')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DKCNQVBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ quản lý KCN Quế Võ' WHERE TableID='A014A' AND CustomsCode = 'DKCNQVBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DKCNQVBN','00',N'Đội TTHQ quản lý KCN Quế Võ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVCCHQBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVCCHQBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVCCHQBN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTNBNINH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVTNBNINH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTNBNINH','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'YBINHTNBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục KCN Yên Bình' WHERE TableID='A014A' AND CustomsCode = 'YBINHTNBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','YBINHTNBN','00',N'Đội Thủ tục KCN Yên Bình')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BACGIANGBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý các KCN Bắc Giang' WHERE TableID='A014A' AND CustomsCode = 'BACGIANGBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BACGIANGBN','00',N'Chi cục HQ Quản lý các KCN Bắc Giang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TIENSONBN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cảng nội địa Tiên Sơn' WHERE TableID='A014A' AND CustomsCode = 'TIENSONBN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TIENSONBN','00',N'Chi cục HQ Cảng nội địa Tiên Sơn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'QUINHONBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Qui Nhơn' WHERE TableID='A014A' AND CustomsCode = 'QUINHONBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','QUINHONBD','00',N'Chi cục HQ CK Cảng Qui Nhơn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PHUYENBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Yên' WHERE TableID='A014A' AND CustomsCode = 'PHUYENBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PHUYENBD','00',N'Chi cục HQ Phú Yên')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTHOPBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng tổng hợp Bình Dương' WHERE TableID='A014A' AND CustomsCode = 'CTHOPBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTHOPBD','00',N'Chi cục HQ CK Cảng tổng hợp Bình Dương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SONGTHANBD' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sóng Thần' WHERE TableID='A014A' AND CustomsCode = 'SONGTHANBD' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SONGTHANBD','02',N'Chi cục HQ Sóng Thần')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVCCMPBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = 'DNVCCMPBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVCCMPBD','00',N'Đội Nghiệp vụ - HQ Mỹ Phước')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DKLHCCMPBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TT Khu liên hợp - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = 'DKLHCCMPBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DKLHCCMPBD','00',N'Đội TT Khu liên hợp - HQ Mỹ Phước')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTDCCMPBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TT Tân Định - HQ Mỹ Phước' WHERE TableID='A014A' AND CustomsCode = 'DTDCCMPBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTDCCMPBD','00',N'Đội TT Tân Định - HQ Mỹ Phước')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCNSTHANBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Sóng Thần' WHERE TableID='A014A' AND CustomsCode = 'KCNSTHANBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCNSTHANBD','00',N'Chi cục HQ KCN Sóng Thần')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCNVNSGBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Việt Nam - Singapore' WHERE TableID='A014A' AND CustomsCode = 'KCNVNSGBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCNVNSGBD','00',N'Chi cục HQ KCN Việt Nam - Singapore')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VHUONGBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Việt Hương' WHERE TableID='A014A' AND CustomsCode = 'VHUONGBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VHUONGBD','00',N'Chi cục HQ KCN Việt Hương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NGOAIKCNBD' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN' WHERE TableID='A014A' AND CustomsCode = 'NGOAIKCNBD' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NGOAIKCNBD','00',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HOALUBP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tổng hợp            ' WHERE TableID='A014A' AND CustomsCode = 'HOALUBP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HOALUBP','00',N'Đội Nghiệp vụ Tổng hợp            ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HOALUBP' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ CK Tà Vát' WHERE TableID='A014A' AND CustomsCode = 'HOALUBP' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HOALUBP','01',N'Đội TTHQ CK Tà Vát')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTHANHBP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'CTHANHBP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTHANHBP','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTHANHBP' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = 'CTHANHBP' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTHANHBP','01',N'Đội Nghiệp vụ 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HDIEUBP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tổng hợp            ' WHERE TableID='A014A' AND CustomsCode = 'HDIEUBP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HDIEUBP','00',N'Đội Nghiệp vụ Tổng hợp            ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HDIEUBP' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiêp vụ CK Tân Tiến' WHERE TableID='A014A' AND CustomsCode = 'HDIEUBP' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HDIEUBP','01',N'Đội Nghiêp vụ CK Tân Tiến')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HOATRUNGCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hòa Trung' WHERE TableID='A014A' AND CustomsCode = 'HOATRUNGCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HOATRUNGCM','00',N'Chi cục HQ Hòa Trung')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CNAMCANCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Năm Căn' WHERE TableID='A014A' AND CustomsCode = 'CNAMCANCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CNAMCANCM','00',N'Chi cục HQ CK Cảng Năm Căn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CANGCANTHO' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cần Thơ' WHERE TableID='A014A' AND CustomsCode = 'CANGCANTHO' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CANGCANTHO','00',N'Chi cục HQ CK Cảng Cần Thơ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VINHLONGCT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Vĩnh Long' WHERE TableID='A014A' AND CustomsCode = 'VINHLONGCT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VINHLONGCT','00',N'Chi cục HQ CK Vĩnh Long')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TAYDOCT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Tây Đô' WHERE TableID='A014A' AND CustomsCode = 'TAYDOCT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TAYDOCT','00',N'Chi cục HQ Tây Đô')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SOCTRANGCT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sóc Trăng' WHERE TableID='A014A' AND CustomsCode = 'SOCTRANGCT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SOCTRANGCT','00',N'Chi cục HQ Sóc Trăng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTLCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Tà Lùng' WHERE TableID='A014A' AND CustomsCode = 'DNVTLCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTLCB','00',N'Chi cục HQ CK Tà Lùng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNV2TLCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội NV số 2 Nà Lạn                ' WHERE TableID='A014A' AND CustomsCode = 'DNV2TLCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNV2TLCB','00',N'Đội NV số 2 Nà Lạn                ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TRALINHCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Trà Lĩnh' WHERE TableID='A014A' AND CustomsCode = 'TRALINHCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TRALINHCB','00',N'Chi cục HQ CK Trà Lĩnh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SOCGIANGCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Sóc Giang' WHERE TableID='A014A' AND CustomsCode = 'SOCGIANGCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SOCGIANGCB','00',N'Chi cục HQ CK Sóc Giang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'POPEOCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Pò Peo' WHERE TableID='A014A' AND CustomsCode = 'POPEOCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','POPEOCB','00',N'Chi cục HQ CK Pò Peo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVBHCBANG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bí Hà' WHERE TableID='A014A' AND CustomsCode = 'DNVBHCBANG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVBHCBANG','00',N'Chi cục HQ CK Bí Hà')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DLVBHCBANG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội NV Lý Vạn' WHERE TableID='A014A' AND CustomsCode = 'DLVBHCBANG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DLVBHCBANG','00',N'Đội NV Lý Vạn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BACKANCB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Kạn' WHERE TableID='A014A' AND CustomsCode = 'BACKANCB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BACKANCB','00',N'Chi cục HQ Bắc Kạn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SBQTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = 'SBQTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SBQTDN','00',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SBQTDN' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Bưu phẩm bưu kiện' WHERE TableID='A014A' AND CustomsCode = 'SBQTDN' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SBQTDN','01',N'Đội Bưu phẩm bưu kiện')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTGCDANANG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A014A' AND CustomsCode = 'DTGCDANANG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTGCDANANG','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CANGDANANG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = 'CANGDANANG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CANGDANANG','00',N'Chi cục HQ CK Cảng Đà Nẵng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HKHANHDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu' WHERE TableID='A014A' AND CustomsCode = 'HKHANHDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HKHANHDN','00',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCNDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Đà Nẵng' WHERE TableID='A014A' AND CustomsCode = 'KCNDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCNDN','00',N'Chi cục HQ KCN Đà Nẵng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVIBRDL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK BupRăng' WHERE TableID='A014A' AND CustomsCode = 'DNVIBRDL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVIBRDL','00',N'Chi cục HQ CK BupRăng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BMTHUOTDL' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Buôn Mê Thuột' WHERE TableID='A014A' AND CustomsCode = 'BMTHUOTDL' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BMTHUOTDL','01',N'Chi cục HQ Buôn Mê Thuột')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVDLATDL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Đà Lạt' WHERE TableID='A014A' AND CustomsCode = 'DNVDLATDL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVDLATDL','00',N'Chi cục HQ Đà Lạt')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTTRGDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVTTRGDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTTRGDB','00',N'Đội nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHPUOCTTDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Huổi Puốc' WHERE TableID='A014A' AND CustomsCode = 'DHPUOCTTDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHPUOCTTDB','00',N'Đội Thủ tục Huổi Puốc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LONGSAPDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Lóng Sập' WHERE TableID='A014A' AND CustomsCode = 'LONGSAPDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LONGSAPDB','00',N'Chi cục HQ CK Lóng Sập')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CKHUONGDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Chiềng Khương' WHERE TableID='A014A' AND CustomsCode = 'CKHUONGDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CKHUONGDB','00',N'Chi cục HQ CK Chiềng Khương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HQSONLADB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Thị xã Sơn La' WHERE TableID='A014A' AND CustomsCode = 'HQSONLADB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HQSONLADB','00',N'HQ Thị xã Sơn La')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NACAISLDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ HQCK Nà Cài         ' WHERE TableID='A014A' AND CustomsCode = 'NACAISLDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NACAISLDB','00',N'Đội Nghiệp vụ HQCK Nà Cài         ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVMLTDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVMLTDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVMLTDB','00',N'Đội nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DPOTOMLTDB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Hải quan Pô Tô                ' WHERE TableID='A014A' AND CustomsCode = 'DPOTOMLTDB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DPOTOMLTDB','00',N'Đội Hải quan Pô Tô                ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVCCLTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ ' WHERE TableID='A014A' AND CustomsCode = 'DNVCCLTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVCCLTDN','00',N'Đội nghiệp vụ ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNV2CCLTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = 'DNV2CCLTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNV2CCLTDN','00',N'Đội nghiệp vụ 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNV3CCLTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 3' WHERE TableID='A014A' AND CustomsCode = 'DNV3CCLTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNV3CCLTDN','00',N'Đội nghiệp vụ 3')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVLBTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVLBTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVLBTDN','00',N'Đội nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNV2LBTDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ 2' WHERE TableID='A014A' AND CustomsCode = 'DNV2LBTDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNV2LBTDN','00',N'Đội nghiệp vụ 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BIENHOADN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục tàu' WHERE TableID='A014A' AND CustomsCode = 'BIENHOADN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BIENHOADN','00',N'Đội Thủ tục tàu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BIENHOADN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Biên Hoà' WHERE TableID='A014A' AND CustomsCode = 'BIENHOADN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BIENHOADN','00',N'Chi cục HQ Biên Hoà')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TNHATDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Thống Nhất' WHERE TableID='A014A' AND CustomsCode = 'TNHATDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TNHATDN','00',N'Chi cục HQ Thống Nhất')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NTRACHDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Nhơn Trạch' WHERE TableID='A014A' AND CustomsCode = 'NTRACHDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NTRACHDN','00',N'Chi cục HQ Nhơn Trạch')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BTHUANDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ QL KCN Bình Thuận' WHERE TableID='A014A' AND CustomsCode = 'BTHUANDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BTHUANDN','00',N'Chi cục HQ QL KCN Bình Thuận')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCXLBINHDN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCX Long Bình' WHERE TableID='A014A' AND CustomsCode = 'KCXLBINHDN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCXLBINHDN','00',N'Chi cục HQ KCX Long Bình')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'THPHUOCDT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thường Phước' WHERE TableID='A014A' AND CustomsCode = 'THPHUOCDT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','THPHUOCDT','00',N'Chi cục HQ CK Thường Phước')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SOTHUONGDT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Sở Thượng' WHERE TableID='A014A' AND CustomsCode = 'SOTHUONGDT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SOTHUONGDT','00',N'Chi cục HQ Sở Thượng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'THBINHDT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ  Thông Bình' WHERE TableID='A014A' AND CustomsCode = 'THBINHDT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','THBINHDT','00',N'Chi cục HQ  Thông Bình')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DINHBADT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Dinh Bà' WHERE TableID='A014A' AND CustomsCode = 'DINHBADT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DINHBADT','00',N'Chi cục HQ CK Dinh Bà')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAOLANHCDT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đồng Tháp - KV Cao Lãnh' WHERE TableID='A014A' AND CustomsCode = 'CAOLANHCDT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAOLANHCDT','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Cao Lãnh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SADECCDT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Đồng Tháp - KV Sa Đéc' WHERE TableID='A014A' AND CustomsCode = 'SADECCDT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SADECCDT','00',N'Chi cục HQ CK Cảng Đồng Tháp - KV Sa Đéc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTHLTGL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ tổng hợp' WHERE TableID='A014A' AND CustomsCode = 'DNVTHLTGL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTHLTGL','00',N'Đội Nghiệp vụ tổng hợp')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTTLTGL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục' WHERE TableID='A014A' AND CustomsCode = 'DTTLTGL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTTLTGL','00',N'Đội Thủ tục')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BOYGL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bờ Y' WHERE TableID='A014A' AND CustomsCode = 'BOYGL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BOYGL','00',N'Chi cục HQ CK Bờ Y')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KONTUMGL' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Kon Tum' WHERE TableID='A014A' AND CustomsCode = 'KONTUMGL' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KONTUMGL','00',N'Chi cục HQ Kon Tum')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TTHUYHG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A014A' AND CustomsCode = 'TTHUYHG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TTHUYHG','00',N'Chi cục HQ CK Thanh Thủy')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'XINMANHG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Xín Mần' WHERE TableID='A014A' AND CustomsCode = 'XINMANHG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','XINMANHG','00',N'Chi cục HQ CK Xín Mần')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PHOBANGHG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Phó Bảng' WHERE TableID='A014A' AND CustomsCode = 'PHOBANGHG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PHOBANGHG','00',N'Chi cục HQ CK Phó Bảng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SAMPUNHG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Săm Pun' WHERE TableID='A014A' AND CustomsCode = 'SAMPUNHG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SAMPUNHG','00',N'Chi cục HQ CK Săm Pun')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TQUANGHG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Tuyên Quang' WHERE TableID='A014A' AND CustomsCode = 'TQUANGHG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TQUANGHG','00',N'Chi cục HQ Tuyên Quang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHHXNBHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa xuất' WHERE TableID='A014A' AND CustomsCode = 'DHHXNBHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHHXNBHN','00',N'Đội Thủ tục hàng hóa xuất')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DCPNNBHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục chuyển phát nhanh' WHERE TableID='A014A' AND CustomsCode = 'DCPNNBHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DCPNNBHN','00',N'Đội Thủ tục chuyển phát nhanh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHHNNBHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa nhập' WHERE TableID='A014A' AND CustomsCode = 'DHHNNBHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHHNNBHN','00',N'Đội Thủ tục hàng hóa nhập')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHLNKNBHN ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hành lý nhập khẩu' WHERE TableID='A014A' AND CustomsCode = 'DHLNKNBHN ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHLNKNBHN ','00',N'Đội thủ tục hành lý nhập khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHLXKNBHN ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hành lý xuất khẩu' WHERE TableID='A014A' AND CustomsCode = 'DHLXKNBHN ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHLXKNBHN ','00',N'Đội thủ tục hành lý xuất khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'YENBAIHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Yên Bái' WHERE TableID='A014A' AND CustomsCode = 'YENBAIHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','YENBAIHN','00',N'Chi cục HQ Yên Bái')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'MYDINHBDHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK liên tỉnh' WHERE TableID='A014A' AND CustomsCode = 'MYDINHBDHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','MYDINHBDHN','00',N'Đội Thủ tục HH XNK liên tỉnh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'FEDEXBDHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK CPN - FeDex        ' WHERE TableID='A014A' AND CustomsCode = 'FEDEXBDHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','FEDEXBDHN','00',N'Đội Thủ tục HH XNK CPN - FeDex        ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'UPSBDHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK CPN - UPS' WHERE TableID='A014A' AND CustomsCode = 'UPSBDHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','UPSBDHN','00',N'Đội Thủ tục HH XNK CPN - UPS')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVBHNHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVBHNHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVBHNHN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHKBHNHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Hàng không' WHERE TableID='A014A' AND CustomsCode = 'DHKBHNHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHKBHNHN','00',N'Đội Hàng không')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DCPNBHNHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục chuyển phát nhanh' WHERE TableID='A014A' AND CustomsCode = 'DCPNBHNHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DCPNBHNHN','00',N'Đội Thủ tục chuyển phát nhanh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'GIATHUYHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Gia Thụy' WHERE TableID='A014A' AND CustomsCode = 'GIATHUYHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','GIATHUYHN','00',N'Chi cục HQ Gia Thụy')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHDHTHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ Hà Đông' WHERE TableID='A014A' AND CustomsCode = 'DHDHTHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHDHTHN','00',N'Đội TTHQ Hà Đông')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CNCHTHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ Khu CNC Hòa Lạc' WHERE TableID='A014A' AND CustomsCode = 'CNCHTHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CNCHTHN','00',N'Đội TTHQ Khu CNC Hòa Lạc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BTLONGHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ KCN Bắc Thăng Long' WHERE TableID='A014A' AND CustomsCode = 'BTLONGHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BTLONGHN','00',N'Chi cục HQ KCN Bắc Thăng Long')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VIETTRIHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Thọ' WHERE TableID='A014A' AND CustomsCode = 'VIETTRIHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VIETTRIHN','00',N'Chi cục HQ Phú Thọ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HQQLDTGCHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A014A' AND CustomsCode = 'HQQLDTGCHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HQQLDTGCHN','00',N'Chi cục HQ Quản lý hàng đầu tư - gia công')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VINHPHUCHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vĩnh Phúc' WHERE TableID='A014A' AND CustomsCode = 'VINHPHUCHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VINHPHUCHN','00',N'Chi cục HQ Vĩnh Phúc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQCPNHN' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục XNK 1' WHERE TableID='A014A' AND CustomsCode = 'CCHQCPNHN' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQCPNHN','01',N'Đội thủ tục XNK 1')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQCPNHN' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục XNK 2' WHERE TableID='A014A' AND CustomsCode = 'CCHQCPNHN' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQCPNHN','02',N'Đội thủ tục XNK 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'GAYVIENHN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ga đường sắt quốc tế Yên Viên' WHERE TableID='A014A' AND CustomsCode = 'GAYVIENHN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','GAYVIENHN','00',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAUTREOHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Cầu Treo' WHERE TableID='A014A' AND CustomsCode = 'CAUTREOHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAUTREOHT','00',N'Chi cục HQ CK Quốc tế Cầu Treo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HONGLINHHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hồng Lĩnh' WHERE TableID='A014A' AND CustomsCode = 'HONGLINHHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HONGLINHHT','00',N'Chi cục HQ Hồng Lĩnh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KKTCTREOHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ khu kinh tế CK Cầu Treo' WHERE TableID='A014A' AND CustomsCode = 'KKTCTREOHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KKTCTREOHT','00',N'Chi cục HQ khu kinh tế CK Cầu Treo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CXHAIHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Xuân Hải' WHERE TableID='A014A' AND CustomsCode = 'CXHAIHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CXHAIHT','00',N'Chi cục HQ CK Cảng Xuân Hải')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVVANGHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A014A' AND CustomsCode = 'DNVVANGHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVVANGHT','00',N'Chi cục HQ CK Cảng Vũng Áng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SDUONGVAHT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ cảng Sơn Dương' WHERE TableID='A014A' AND CustomsCode = 'SDUONGVAHT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SDUONGVAHT','00',N'Đội Nghiệp vụ cảng Sơn Dương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CANGHPKVI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK          ' WHERE TableID='A014A' AND CustomsCode = 'CANGHPKVI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CANGHPKVI','00',N'Đội Thủ tục hàng hóa XNK          ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'THAIBINHHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ                     ' WHERE TableID='A014A' AND CustomsCode = 'THAIBINHHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','THAIBINHHP','00',N'Đội Nghiệp vụ                     ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CANGHPKVII' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK          ' WHERE TableID='A014A' AND CustomsCode = 'CANGHPKVII' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CANGHPKVII','00',N'Đội Thủ tục hàng hóa XNK          ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CDINHVUHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK          ' WHERE TableID='A014A' AND CustomsCode = 'CDINHVUHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CDINHVUHP','00',N'Đội Thủ tục hàng hóa XNK          ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCXKCNHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ                     ' WHERE TableID='A014A' AND CustomsCode = 'KCXKCNHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCXKCNHP','00',N'Đội Nghiệp vụ                     ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTGCHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng gia công         ' WHERE TableID='A014A' AND CustomsCode = 'DTGCHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTGCHP','00',N'Đội Thủ tục hàng gia công         ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTGCHP' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng đầu tư           ' WHERE TableID='A014A' AND CustomsCode = 'DTGCHP' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTGCHP','01',N'Đội Thủ tục hàng đầu tư           ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HAIDUONGHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ                     ' WHERE TableID='A014A' AND CustomsCode = 'HAIDUONGHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HAIDUONGHP','00',N'Đội Nghiệp vụ                     ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HUNGYENHP' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ                     ' WHERE TableID='A014A' AND CustomsCode = 'HUNGYENHP' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HUNGYENHP','00',N'Đội Nghiệp vụ                     ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHPKVIII' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK          ' WHERE TableID='A014A' AND CustomsCode = 'CHPKVIII' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHPKVIII','00',N'Đội Thủ tục hàng hóa XNK          ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHPKVIII' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội GS tàu, kho bãi và KSHQ' WHERE TableID='A014A' AND CustomsCode = 'CHPKVIII' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHPKVIII','01',N'Đội GS tàu, kho bãi và KSHQ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NTHUANKH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ninh Thuận' WHERE TableID='A014A' AND CustomsCode = 'NTHUANKH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NTHUANKH','00',N'Chi cục HQ Ninh Thuận')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NHATRANGKH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Nha Trang' WHERE TableID='A014A' AND CustomsCode = 'NHATRANGKH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NHATRANGKH','00',N'Chi cục HQ CK Cảng Nha Trang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAMRANHKH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cam Ranh' WHERE TableID='A014A' AND CustomsCode = 'CAMRANHKH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAMRANHKH','00',N'Chi cục HQ CK Cảng Cam Ranh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQCKSBCR' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK sân bay quốc tế Cam Ranh' WHERE TableID='A014A' AND CustomsCode = 'CCHQCKSBCR' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQCKSBCR','00',N'Chi cục HQ CK sân bay quốc tế Cam Ranh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VANPHONGKH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vân Phong' WHERE TableID='A014A' AND CustomsCode = 'VANPHONGKH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VANPHONGKH','00',N'Chi cục HQ Vân Phong')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HATIENKG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc Tế Hà Tiên' WHERE TableID='A014A' AND CustomsCode = 'HATIENKG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HATIENKG','00',N'Chi cục HQ CK Quốc Tế Hà Tiên')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'GTHANHKG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Giang Thành' WHERE TableID='A014A' AND CustomsCode = 'GTHANHKG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','GTHANHKG','00',N'Chi cục HQ CK Giang Thành')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHCHONGKG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Hòn Chông' WHERE TableID='A014A' AND CustomsCode = 'CHCHONGKG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHCHONGKG','00',N'Chi cục HQ CK Cảng Hòn Chông')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PHUQUOCKG' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phú Quốc' WHERE TableID='A014A' AND CustomsCode = 'PHUQUOCKG' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PHUQUOCKG','00',N'Chi cục HQ Phú Quốc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVHNLSON' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Hữu Nghị' WHERE TableID='A014A' AND CustomsCode = 'DNVHNLSON' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVHNLSON','00',N'Đội Nghiệp vụ Hữu Nghị')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVCSLSON' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Co Sâu              ' WHERE TableID='A014A' AND CustomsCode = 'DNVCSLSON' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVCSLSON','00',N'Đội Nghiệp vụ Co Sâu              ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVPNLSON ' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Pò Nhùng' WHERE TableID='A014A' AND CustomsCode = 'DNVPNLSON ' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVPNLSON ','00',N'Đội Nghiệp vụ Pò Nhùng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHIMALS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Chi Ma' WHERE TableID='A014A' AND CustomsCode = 'CHIMALS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHIMALS','00',N'Chi cục HQ CK Chi Ma')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'COCNAMLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Cốc Nam' WHERE TableID='A014A' AND CustomsCode = 'COCNAMLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','COCNAMLS','00',N'Chi cục HQ Cốc Nam')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NHINHTTLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Na Hình             ' WHERE TableID='A014A' AND CustomsCode = 'NHINHTTLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NHINHTTLS','00',N'Đội Nghiệp vụ Na Hình             ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NNUATTLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Nà Nưa' WHERE TableID='A014A' AND CustomsCode = 'NNUATTLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NNUATTLS','00',N'Đội Nghiệp vụ Nà Nưa')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BNGHITTLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Bình Nghi' WHERE TableID='A014A' AND CustomsCode = 'BNGHITTLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BNGHITTLS','00',N'Đội Nghiệp vụ Bình Nghi')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTTLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Tân Thanh' WHERE TableID='A014A' AND CustomsCode = 'DNVTTLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTTLS','00',N'Đội Nghiệp vụ Tân Thanh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DONGDANGLS' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Ga Đồng Đăng' WHERE TableID='A014A' AND CustomsCode = 'DONGDANGLS' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DONGDANGLS','00',N'Chi cục HQ Ga Đồng Đăng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCCKLAOCAI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK 1' WHERE TableID='A014A' AND CustomsCode = 'CCCKLAOCAI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCCKLAOCAI','00',N'Đội Thủ tục HH XNK 1')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCCKLAOCAI' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục HH XNK 2' WHERE TableID='A014A' AND CustomsCode = 'CCCKLAOCAI' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCCKLAOCAI','00',N'Đội Thủ tục HH XNK 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'M.KHUONGLC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Mường Khương' WHERE TableID='A014A' AND CustomsCode = 'M.KHUONGLC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','M.KHUONGLC','00',N'Chi cục HQ CK Mường Khương')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BATXATLC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Bát Xát' WHERE TableID='A014A' AND CustomsCode = 'BATXATLC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BATXATLC','00',N'Chi cục HQ CK Bát Xát')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVDSATLC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVDSATLC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVDSATLC','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VNLDSATLC' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'ICD Vinalines' WHERE TableID='A014A' AND CustomsCode = 'VNLDSATLC' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VNLDSATLC','00',N'ICD Vinalines')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'MQTAYLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Mỹ Quý Tây' WHERE TableID='A014A' AND CustomsCode = 'MQTAYLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','MQTAYLA','00',N'Chi cục HQ CK Mỹ Quý Tây')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BINHHIEPLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Bình Hiệp' WHERE TableID='A014A' AND CustomsCode = 'BINHHIEPLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BINHHIEPLA','00',N'Chi cục HQ CK Quốc tế Bình Hiệp')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HUNGDIENLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Hưng Điền' WHERE TableID='A014A' AND CustomsCode = 'HUNGDIENLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HUNGDIENLA','00',N'Chi cục HQ Hưng Điền')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DUCHOALA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Đức Hòa' WHERE TableID='A014A' AND CustomsCode = 'DUCHOALA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DUCHOALA','00',N'Chi cục HQ Đức Hòa')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'MYTHOLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'MYTHOLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','MYTHOLA','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LHAUBLLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ KCN Long Hậu' WHERE TableID='A014A' AND CustomsCode = 'LHAUBLLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LHAUBLLA','00',N'Đội Nghiệp vụ KCN Long Hậu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTTBLLA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục- Chi cục HQ Bến Lức' WHERE TableID='A014A' AND CustomsCode = 'DTTBLLA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTTBLLA','00',N'Đội Thủ tục- Chi cục HQ Bến Lức')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NAMCANNA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Nậm Cắn' WHERE TableID='A014A' AND CustomsCode = 'NAMCANNA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NAMCANNA','00',N'Chi cục HQ CK Quốc tế Nậm Cắn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TTHUYNA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A014A' AND CustomsCode = 'TTHUYNA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TTHUYNA','00',N'Chi cục HQ CK Thanh Thủy')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CANGNGHEAN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng' WHERE TableID='A014A' AND CustomsCode = 'CANGNGHEAN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CANGNGHEAN','00',N'Chi cục HQ CK Cảng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VINHNA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Vinh' WHERE TableID='A014A' AND CustomsCode = 'VINHNA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VINHNA','00',N'Chi cục HQ Vinh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHALOQB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cha Lo' WHERE TableID='A014A' AND CustomsCode = 'CHALOQB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHALOQB','00',N'Chi cục HQ CK Cha Lo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAROONGQB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cà Roòng' WHERE TableID='A014A' AND CustomsCode = 'CAROONGQB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAROONGQB','00',N'Chi cục HQ CK Cà Roòng')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHONLAHLQB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Cảng Hòn La         ' WHERE TableID='A014A' AND CustomsCode = 'CHONLAHLQB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHONLAHLQB','00',N'Đội Nghiệp vụ Cảng Hòn La         ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DHOIHLQB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Đồng Hới            ' WHERE TableID='A014A' AND CustomsCode = 'DHOIHLQB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DHOIHLQB','00',N'Đội Nghiệp vụ Đồng Hới            ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CGIANHHLQB' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Cảng Gianh          ' WHERE TableID='A014A' AND CustomsCode = 'CGIANHHLQB' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CGIANHHLQB','00',N'Đội Nghiệp vụ Cảng Gianh          ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NAMGIANGQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Nam Giang' WHERE TableID='A014A' AND CustomsCode = 'NAMGIANGQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NAMGIANGQN','00',N'Chi cục HQ CK Nam Giang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNAMDNGCQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNAMDNGCQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNAMDNGCQN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNAMDNGCQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Tây Giang' WHERE TableID='A014A' AND CustomsCode = 'DNAMDNGCQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNAMDNGCQN','00',N'Đội Tây Giang')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KYHAQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Kỳ Hà' WHERE TableID='A014A' AND CustomsCode = 'KYHAQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KYHAQN','00',N'Chi cục HQ CK Cảng Kỳ Hà')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CDQUATQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Dung Quất' WHERE TableID='A014A' AND CustomsCode = 'CDQUATQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CDQUATQN','00',N'Chi cục HQ CK Cảng Dung Quất')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KCNQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ các KCN Quảng Ngãi' WHERE TableID='A014A' AND CustomsCode = 'KCNQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KCNQN','00',N'Chi cục HQ các KCN Quảng Ngãi')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'BLUANMCQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Cửa khẩu Bắc Luân              ' WHERE TableID='A014A' AND CustomsCode = 'BLUANMCQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','BLUANMCQN','00',N'HQ Cửa khẩu Bắc Luân              ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KLONGMCQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ Cửa khẩu Ka Long               ' WHERE TableID='A014A' AND CustomsCode = 'KLONGMCQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KLONGMCQN','00',N'HQ Cửa khẩu Ka Long               ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HOANHMOQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Hoành Mô' WHERE TableID='A014A' AND CustomsCode = 'HOANHMOQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HOANHMOQN','00',N'Chi cục HQ CK Hoành Mô')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PSINHQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Bắc Phong Sinh' WHERE TableID='A014A' AND CustomsCode = 'PSINHQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PSINHQN','00',N'Chi cục HQ Bắc Phong Sinh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAILANQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cái Lân' WHERE TableID='A014A' AND CustomsCode = 'CAILANQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAILANQN','00',N'Chi cục HQ CK Cảng Cái Lân')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'VANGIAQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Vạn Gia' WHERE TableID='A014A' AND CustomsCode = 'VANGIAQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','VANGIAQN','00',N'Chi cục HQ CK Cảng Vạn Gia')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HONGAIQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Hòn Gai' WHERE TableID='A014A' AND CustomsCode = 'HONGAIQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HONGAIQN','00',N'Chi cục HQ CK Cảng Hòn Gai')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CAMPHAQN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cẩm Phả' WHERE TableID='A014A' AND CustomsCode = 'CAMPHAQN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CAMPHAQN','00',N'Chi cục HQ CK Cảng Cẩm Phả')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LAOBAOQT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ -Tổng hợp           ' WHERE TableID='A014A' AND CustomsCode = 'LAOBAOQT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LAOBAOQT','00',N'Đội Nghiệp vụ -Tổng hợp           ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LALAYQT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK La Lay' WHERE TableID='A014A' AND CustomsCode = 'LALAYQT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LALAYQT','00',N'Chi cục HQ CK La Lay')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KTMAILBQT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ -Tổng hợp           ' WHERE TableID='A014A' AND CustomsCode = 'KTMAILBQT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KTMAILBQT','00',N'Đội Nghiệp vụ -Tổng hợp           ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCUAVIETQT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Cửa Việt' WHERE TableID='A014A' AND CustomsCode = 'CCUAVIETQT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCUAVIETQT','00',N'Chi cục HQ CK Cảng Cửa Việt')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KSOATHQQT' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Kiẻm soát HQ Quảng Trị        ' WHERE TableID='A014A' AND CustomsCode = 'KSOATHQQT' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KSOATHQQT','00',N'Đội Kiẻm soát HQ Quảng Trị        ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVMBAITN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVMBAITN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVMBAITN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KTMCNMBTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Quản lý Khu TM - CN Mộc Bài   ' WHERE TableID='A014A' AND CustomsCode = 'KTMCNMBTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KTMCNMBTN','00',N'Đội Quản lý Khu TM - CN Mộc Bài   ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PHUOCTANTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Phước Tân' WHERE TableID='A014A' AND CustomsCode = 'PHUOCTANTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PHUOCTANTN','00',N'Chi cục HQ Phước Tân')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'KATUMTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Kà Tum' WHERE TableID='A014A' AND CustomsCode = 'KATUMTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','KATUMTN','00',N'Chi cục HQ CK Kà Tum')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVXAMATTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVXAMATTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVXAMATTN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DCRXAMATTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục Hải quan Chàng Riệc   ' WHERE TableID='A014A' AND CustomsCode = 'DCRXAMATTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DCRXAMATTN','00',N'Đội thủ tục Hải quan Chàng Riệc   ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVTBANGTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'DNVTBANGTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVTBANGTN','00',N'Đội Nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'PDONGTBTN' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ KCN Phước Đông' WHERE TableID='A014A' AND CustomsCode = 'PDONGTBTN' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','PDONGTBTN','00',N'Đội TTHQ KCN Phước Đông')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DNVNMEOTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A014A' AND CustomsCode = 'DNVNMEOTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DNVNMEOTH','00',N'Chi cục HQ CK Quốc tế Na Mèo')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTTNMEOTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội TTHQ CK Tén Tằn' WHERE TableID='A014A' AND CustomsCode = 'DTTNMEOTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTTNMEOTH','00',N'Đội TTHQ CK Tén Tằn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTHANHHOA' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Thanh Hóa' WHERE TableID='A014A' AND CustomsCode = 'CTHANHHOA' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTHANHHOA','00',N'Chi cục HQ CK Cảng Thanh Hóa')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CNGSONTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Nghi Sơn' WHERE TableID='A014A' AND CustomsCode = 'CNGSONTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CNGSONTH','00',N'Chi cục HQ CK Cảng Nghi Sơn')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'HANAMTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Quản lý các KCN Hà Nam' WHERE TableID='A014A' AND CustomsCode = 'HANAMTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','HANAMTH','00',N'Chi cục HQ Quản lý các KCN Hà Nam')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NINHBINHTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiêp vụ ' WHERE TableID='A014A' AND CustomsCode = 'NINHBINHTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NINHBINHTH','00',N'Đội Nghiêp vụ ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NINHBINHTH' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'HQ cảng Ninh Phúc' WHERE TableID='A014A' AND CustomsCode = 'NINHBINHTH' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NINHBINHTH','02',N'HQ cảng Ninh Phúc')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'NAMDINHTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Nam Định' WHERE TableID='A014A' AND CustomsCode = 'NAMDINHTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','NAMDINHTH','00',N'Chi cục HQ Nam Định')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'ADOTTTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ - Chi cục HQ CK A Đớt' WHERE TableID='A014A' AND CustomsCode = 'ADOTTTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','ADOTTTH','00',N'Đội Nghiệp vụ - Chi cục HQ CK A Đớt')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'ADOTTTH' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ Hồng Vân' WHERE TableID='A014A' AND CustomsCode = 'ADOTTTH' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','ADOTTTH','01',N'Đội Nghiệp vụ Hồng Vân')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTANTTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Thuận An' WHERE TableID='A014A' AND CustomsCode = 'CTANTTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTANTTH','00',N'Chi cục HQ CK Cảng Thuận An')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCMAYTTH' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CK Cảng Chân Mây' WHERE TableID='A014A' AND CustomsCode = 'CCMAYTTH' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCMAYTTH','00',N'Chi cục HQ CK Cảng Chân Mây')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'THUYANTTH' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ Thủy An' WHERE TableID='A014A' AND CustomsCode = 'THUYANTTH' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','THUYANTTH','02',N'Chi cục HQ Thủy An')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 1' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','01',N'Đội Thủ tục hàng hóa XNK 1')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 2' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','02',N'Đội Thủ tục hàng hóa XNK 2')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='03') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 3' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='03' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','03',N'Đội Thủ tục hàng hóa XNK 3')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='04') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 4' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='04' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','04',N'Đội Thủ tục hàng hóa XNK 4')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A1') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội DHL' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A1' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A1',N'Chi cục HQ CPN - Đội DHL')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A2') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội FEDEX' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A2' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A2',N'Chi cục HQ CPN - Đội FEDEX')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A3') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội UPS' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A3' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A3',N'Chi cục HQ CPN - Đội UPS')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A4') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội TNT' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A4' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A4',N'Chi cục HQ CPN - Đội TNT')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A5') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội EMS' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A5' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A5',N'Chi cục HQ CPN - Đội EMS')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A6') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội THANTOC' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A6' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A6',N'Chi cục HQ CPN - Đội THANTOC')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A7') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội KERRY' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A7' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A7',N'Chi cục HQ CPN - Đội KERRY')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A8') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Chi cục HQ CPN - Đội HOPNHAT' WHERE TableID='A014A' AND CustomsCode = 'CPNHANHHCM' AND CustomsSubSectionCode='A8' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CPNHANHHCM','A8',N'Chi cục HQ CPN - Đội HOPNHAT')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHPHUOCHCM' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa nhập khẩu' WHERE TableID='A014A' AND CustomsCode = 'CHPHUOCHCM' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHPHUOCHCM','01',N'Đội Thủ tục hàng hóa nhập khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CHPHUOCHCM' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa xuất khẩu' WHERE TableID='A014A' AND CustomsCode = 'CHPHUOCHCM' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CHPHUOCHCM','02',N'Đội Thủ tục hàng hóa xuất khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa Nhập khẩu' WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSGONKVI','01',N'Đội thủ tục hàng hóa Nhập khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa Xuất khẩu' WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSGONKVI','02',N'Đội thủ tục hàng hóa Xuất khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='03') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = 'CSGONKVI' AND CustomsSubSectionCode='03' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSGONKVI','03',N'Đội Giám sát')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSGONKVII' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = 'CSGONKVII' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSGONKVII','01',N'Đội thủ tục hàng hóa XNK')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CSGONKVII' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = 'CSGONKVII' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CSGONKVII','02',N'Đội Giám sát')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CBNSGKVIII' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 1 (cảng Bến Nghé)' WHERE TableID='A014A' AND CustomsCode = 'CBNSGKVIII' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CBNSGKVIII','00',N'Đội Thủ tục hàng hóa XNK 1 (cảng Bến Nghé)')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'GSXDKVIII' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội thủ tục và Giám sát Xăng dầu (kho Xăng dầu)' WHERE TableID='A014A' AND CustomsCode = 'GSXDKVIII' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','GSXDKVIII','00',N'Đội thủ tục và Giám sát Xăng dầu (kho Xăng dầu)')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CVICTKVIII' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK 2 (cảng VICT)' WHERE TableID='A014A' AND CustomsCode = 'CVICTKVIII' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CVICTKVIII','00',N'Đội Thủ tục hàng hóa XNK 2 (cảng VICT)')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCSGKVIV' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa Nhập khẩu' WHERE TableID='A014A' AND CustomsCode = 'CCSGKVIV' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCSGKVIV','01',N'Đội Thủ tục hàng hóa Nhập khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCSGKVIV' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa Xuất khẩu' WHERE TableID='A014A' AND CustomsCode = 'CCSGKVIV' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCSGKVIV','02',N'Đội Thủ tục hàng hóa Xuất khẩu')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'DTCSTSNHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK – TCS' WHERE TableID='A014A' AND CustomsCode = 'DTCSTSNHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','DTCSTSNHCM','00',N'Đội Thủ tục hàng hóa XNK – TCS')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'SCSCTSNHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK – SCSC' WHERE TableID='A014A' AND CustomsCode = 'SCSCTSNHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','SCSCTSNHCM','00',N'Đội Thủ tục hàng hóa XNK – SCSC')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CTCANGHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng hóa XNK' WHERE TableID='A014A' AND CustomsCode = 'CTCANGHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CTCANGHCM','00',N'Đội Thủ tục hàng hóa XNK')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LTILTHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 1 (Linh Trung)' WHERE TableID='A014A' AND CustomsCode = 'LTILTHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LTILTHCM','00',N'Đội Nghiệp vụ 1 (Linh Trung)')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LTILTHCM' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Giám sát' WHERE TableID='A014A' AND CustomsCode = 'LTILTHCM' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LTILTHCM','01',N'Đội Giám sát')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'LTIILTHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Nghiệp vụ 2 (Linh Trung 2)' WHERE TableID='A014A' AND CustomsCode = 'LTIILTHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','LTIILTHCM','00',N'Đội Nghiệp vụ 2 (Linh Trung 2)')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CNCLTHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Khu Công nghệ cao' WHERE TableID='A014A' AND CustomsCode = 'CNCLTHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CNCLTHCM','00',N'Đội Thủ tục Khu Công nghệ cao')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'TTHUANHCM' AND CustomsSubSectionCode='00') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội nghiệp vụ' WHERE TableID='A014A' AND CustomsCode = 'TTHUANHCM' AND CustomsSubSectionCode='00' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','TTHUANHCM','00',N'Đội nghiệp vụ')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQDTHCM' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Đầu tư và Kinh doanh' WHERE TableID='A014A' AND CustomsCode = 'CCHQDTHCM' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQDTHCM','01',N'Đội Thủ tục hàng Đầu tư và Kinh doanh')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQDTHCM' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Sản xuất xuất khẩu và Gia công' WHERE TableID='A014A' AND CustomsCode = 'CCHQDTHCM' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQDTHCM','02',N'Đội Thủ tục hàng Sản xuất xuất khẩu và Gia công')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQGCHCM' AND CustomsSubSectionCode='01') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục hàng Gia công' WHERE TableID='A014A' AND CustomsCode = 'CCHQGCHCM' AND CustomsSubSectionCode='01' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQGCHCM','01',N'Đội Thủ tục hàng Gia công')    
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsSubSection] WHERE TableID='A014A' AND CustomsCode = 'CCHQGCHCM' AND CustomsSubSectionCode='02') 
BEGIN
 UPDATE [t_VNACC_Category_CustomsSubSection] SET Name=N'Đội Thủ tục Hàng sản xuất xuất khẩu' WHERE TableID='A014A' AND CustomsCode = 'CCHQGCHCM' AND CustomsSubSectionCode='02' 
END
ELSE
BEGIN
    INSERT INTO [t_VNACC_Category_CustomsSubSection](TableID,CustomsCode,CustomsSubSectionCode,Name) VALUES('A014A','CCHQGCHCM','02',N'Đội Thủ tục Hàng sản xuất xuất khẩu')    
END

DELETE FROM dbo.t_VNACC_Category_CustomsSubSection WHERE TableID='A014A' AND CustomsCode='01B2' AND CustomsSubSectionCode='00'

DELETE FROM dbo.t_VNACC_Category_CustomsSubSection WHERE TableID='A014A' AND CustomsCode='01D1' AND CustomsSubSectionCode='00'

DELETE FROM dbo.t_VNACC_Category_CustomsSubSection WHERE TableID='A014A' AND CustomsCode='01D2' AND CustomsSubSectionCode='00'

DELETE FROM dbo.t_VNACC_Category_CustomsSubSection WHERE TableID='A014A' AND CustomsCode='01D3' AND CustomsSubSectionCode='00'

DELETE FROM dbo.t_VNACC_Category_CustomsSubSection WHERE TableID='A014A' AND CustomsCode='01E3' AND CustomsSubSectionCode='00'

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '23.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('23.0',GETDATE(), N'Cập nhật đội thủ tục HQ ')
END	