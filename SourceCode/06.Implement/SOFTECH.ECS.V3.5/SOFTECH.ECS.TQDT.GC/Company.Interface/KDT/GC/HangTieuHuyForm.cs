﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.Interface.GC;

namespace Company.Interface.KDT.GC
{
    public partial class HangTieuHuyForm : BaseForm
    {
        public List<HangGSTieuHuy> HangTieuHuys = new List<HangGSTieuHuy>();
        public HangGSTieuHuy hangTieuHuy = new HangGSTieuHuy();
        public bool IsEnable { private get; set; }
        public long IdHopDong;
        public String Caption;
        public bool IsChange;
        public GiamSatTieuHuy dnGSTieuHuy;

        public HangTieuHuyForm()
        {
            IsEnable = true;
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void HangTieuHuyForm_Load(object sender, EventArgs e)
        {
            btnAdd.Enabled = btnXoa.Enabled = IsEnable;
            dgList.AllowDelete = IsEnable ? InheritableBoolean.True : InheritableBoolean.False;

            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            Caption = this.Text;

            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            Set(hangTieuHuy);
            dgList.DataSource = HangTieuHuys;
            

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            int i = Convert.ToInt32(e.Row.Cells["LoaiHang"].Value);
            string loaiHang = string.Empty;
            if (i == 1) loaiHang = "Nguyên phụ liệu";
            else if (i == 2) loaiHang = "Sản phẩm";
            else if (i == 3) loaiHang = "Thiết bị";
            else if (i == 4) loaiHang = "Hàng mẫu";
            e.Row.Cells["LoaiHang"].Text = loaiHang;
        }
        private void Set(HangGSTieuHuy hangTieuHuy)
        {
            txtMa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMa.Text = hangTieuHuy.MaHang;
            txtMa.TextChanged += new EventHandler(txt_TextChanged);

            txtTen.TextChanged -= new EventHandler(txt_TextChanged);
            txtTen.Text = hangTieuHuy.TenHang;
            txtTen.TextChanged += new EventHandler(txt_TextChanged);

            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Value = hangTieuHuy.SoLuong;
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHS.Text = hangTieuHuy.MaHS;
            txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = hangTieuHuy.DVT_ID;
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
            txtGhiChu.Text = hangTieuHuy.GhiChu;
            txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            if (hangTieuHuy.LoaiHang > 0)
            {
                cbLoaiHang.TextChanged -= new EventHandler(txt_TextChanged);
                cbLoaiHang.SelectedValue = hangTieuHuy.LoaiHang;
                cbLoaiHang.TextChanged += new EventHandler(txt_TextChanged);
            }
            #region Đơn vị tính quy đổi
            if (hangTieuHuy.DVT_QuyDoi != null)
            {
                donViTinhQuyDoiControl1.DVT_QuyDoi = hangTieuHuy.DVT_QuyDoi;
                donViTinhQuyDoiControl1.Master_Id = 0;
            }
            else if (hangTieuHuy.ID > 0)
            {
                donViTinhQuyDoiControl1.Master_Id = hangTieuHuy.ID;
                donViTinhQuyDoiControl1.Type = "YCGSTH";
            }
            else
                donViTinhQuyDoiControl1.DVT_QuyDoi = null;
            donViTinhQuyDoiControl1.DonViTinhQuyDoiControl_Load(null, null);
            #endregion

        }
        private void Get(HangGSTieuHuy hangTieuHuy)
        {
            hangTieuHuy.MaHang = txtMa.Text;
            hangTieuHuy.TenHang = txtTen.Text;
            hangTieuHuy.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            hangTieuHuy.MaHS = txtMaHS.Text;
            hangTieuHuy.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hangTieuHuy.GhiChu = txtGhiChu.Text;
            hangTieuHuy.LoaiHang = Convert.ToInt32(cbLoaiHang.SelectedValue);
            #region Đơn vị tính quy đổi
            if (donViTinhQuyDoiControl1.IsEnable)
            {
                hangTieuHuy.DVT_QuyDoi = donViTinhQuyDoiControl1.DVT_QuyDoi;
                hangTieuHuy.DVT_QuyDoi.IsUse = true;
            }
            else if (donViTinhQuyDoiControl1.DVT_QuyDoi != null)
            {
                hangTieuHuy.DVT_QuyDoi = donViTinhQuyDoiControl1.DVT_QuyDoi;
                hangTieuHuy.DVT_QuyDoi.IsUse = false;
            }
            else
                hangTieuHuy.DVT_QuyDoi = null;
            #endregion
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool isValidate = false;
            isValidate = ValidateControl.ValidateNull(txtMa, error, "Mã hàng");
            isValidate &= ValidateControl.ValidateNull(txtTen, error, "Tên hàng");
            isValidate &= ValidateControl.ValidateNull(cbDonViTinh, error, "Đơn vị tính");
            isValidate &= ValidateControl.ValidateNull(txtMaHS, error, "Mã HS");
            isValidate &= ValidateControl.ValidateNull(cbLoaiHang, error, "Loại hàng");
            isValidate &= ValidateControl.ValidateNull(txtSoLuong, error, "Số lượng");
             
            if (!isValidate) return;
            if (Convert.ToDecimal(txtSoLuong.Value) == 0)
            {
                error.SetError(txtSoLuong, "Số lượng");
                return;
            }
            HangTieuHuys.Remove(hangTieuHuy);
            Get(hangTieuHuy);
            //hangTieuHuy.InsertUpdate();
            HangTieuHuys.Add(hangTieuHuy);
            hangTieuHuy = new HangGSTieuHuy();

            txtMa.Focus();

            txtMa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMa.Text = String.Empty;
            txtMa.TextChanged += new EventHandler(txt_TextChanged);

            txtTen.TextChanged -= new EventHandler(txt_TextChanged);
            txtTen.Text = String.Empty;
            txtTen.TextChanged += new EventHandler(txt_TextChanged);

            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Value = String.Empty;
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHS.Text = String.Empty;
            txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = String.Empty;
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
            txtGhiChu.Text = String.Empty;
            txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);
            this.SetChange(true);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            hangTieuHuy = (HangGSTieuHuy)e.Row.DataRow;
            if (hangTieuHuy != null)
                Set(hangTieuHuy);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count >0 && ShowMessage("Bạn có muốn xóa hàng không?",true)!="Yes" )
            {
                return;
            }
            foreach (GridEXSelectedItem item in items)
            {
                hangTieuHuy = (HangGSTieuHuy)item.GetRow().DataRow;
                if (hangTieuHuy != null)
                    HangTieuHuys.Remove(hangTieuHuy);
            }
            this.SetChange(true);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            if (cbLoaiHang.SelectedValue != null)
            {
                if (cbLoaiHang.SelectedValue.ToString() == "2")
                {
                    SanPhamRegistedForm f = new SanPhamRegistedForm();
                    f.isBrower = true;
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.SanPhamSelected.HopDong_ID = IdHopDong;
                    f.ShowDialog();
                    if (!string.IsNullOrEmpty(f.SanPhamSelected.Ma))
                    {
                        txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMa.Text = f.SanPhamSelected.Ma;
                        txtMa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTen.Text = f.SanPhamSelected.Ten;
                        txtTen.TextChanged += new EventHandler(txt_TextChanged);

                        txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHS.Text = f.SanPhamSelected.MaHS;
                        txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = f.SanPhamSelected.DVT_ID.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                    }
                }
                else if (cbLoaiHang.SelectedValue.ToString() == "1")
                {
                    NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                    f.isBrower = true;
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.NguyenPhuLieuSelected.HopDong_ID = IdHopDong;
                    f.ShowDialog();
                    if (!string.IsNullOrEmpty(f.NguyenPhuLieuSelected.Ma))
                    {
                        txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMa.Text = f.NguyenPhuLieuSelected.Ma;
                        txtMa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTen.Text = f.NguyenPhuLieuSelected.Ten;
                        txtTen.TextChanged += new EventHandler(txt_TextChanged);

                        txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                        txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                }
                else if (cbLoaiHang.SelectedValue.ToString() == "3")
                {
                    ThietBiRegistedForm f = new ThietBiRegistedForm();
                    f.isBrower = true;
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.ThietBiSelected.HopDong_ID = IdHopDong;
                    f.ShowDialog();
                    if (!string.IsNullOrEmpty(f.ThietBiSelected.Ma))
                    {
                        txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMa.Text = f.ThietBiSelected.Ma;
                        txtMa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTen.Text = f.ThietBiSelected.Ten;
                        txtTen.TextChanged += new EventHandler(txt_TextChanged);

                        txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHS.Text = f.ThietBiSelected.MaHS;
                        txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = f.ThietBiSelected.DVT_ID.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                }
                else if (cbLoaiHang.SelectedValue.ToString() == "4")
                {
                    HangMauRegistedForm f = new HangMauRegistedForm();
                    f.isBrower = true;
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.HangMauSelected.HopDong_ID = IdHopDong;
                    f.ShowDialog();
                    if (!string.IsNullOrEmpty(f.HangMauSelected.Ma))
                    {
                        txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMa.Text = f.HangMauSelected.Ma;
                        txtMa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTen.Text = f.HangMauSelected.Ten;
                        txtTen.TextChanged += new EventHandler(txt_TextChanged);

                        txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHS.Text = f.HangMauSelected.MaHS;
                        txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = f.HangMauSelected.DVT_ID.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                }
            }
            else
                ShowMessage("Bạn chưa chọn loại hàng", false);
        }

        private void donViTinhQuyDoiControl1_Load(object sender, EventArgs e)
        {

        }
    }
}
