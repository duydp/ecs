﻿namespace Company.Interface
{
    partial class VNACC_ChuyenCuaKhau_TrungChuyenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChuyenCuaKhau_TrungChuyenForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.clcNgayDiThucTe3 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDiDuKien3 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenThucTe3 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenDuKien3 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDiThucTe2 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDiDuKien2 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenThucTe2 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenDuKien2 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDiThucTe1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDiDuKien1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenThucTe1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayDenDuKien1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrMaTrungChuyen3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrMaTrungChuyen2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrMaTrungChuyen1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 184), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 184);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 160);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 160);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(602, 184);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiThucTe3);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiDuKien3);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenThucTe3);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenDuKien3);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiThucTe2);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiDuKien2);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenThucTe2);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenDuKien2);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiThucTe1);
            this.uiGroupBox2.Controls.Add(this.clcNgayDiDuKien1);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenThucTe1);
            this.uiGroupBox2.Controls.Add(this.clcNgayDenDuKien1);
            this.uiGroupBox2.Controls.Add(this.ctrMaTrungChuyen3);
            this.uiGroupBox2.Controls.Add(this.ctrMaTrungChuyen2);
            this.uiGroupBox2.Controls.Add(this.ctrMaTrungChuyen1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(602, 144);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(616, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Ngày đi thực tế";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(494, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Ngày đi dự kiến";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(378, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày đến thực tế";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày đến dự kiến";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Địa điểm trung chuyển";
            // 
            // clcNgayDiThucTe3
            // 
            this.clcNgayDiThucTe3.Location = new System.Drawing.Point(619, 104);
            this.clcNgayDiThucTe3.Name = "clcNgayDiThucTe3";
            this.clcNgayDiThucTe3.ReadOnly = false;
            this.clcNgayDiThucTe3.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiThucTe3.TabIndex = 1;
            this.clcNgayDiThucTe3.TagName = "";
            this.clcNgayDiThucTe3.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiThucTe3.WhereCondition = "";
            // 
            // clcNgayDiDuKien3
            // 
            this.clcNgayDiDuKien3.Location = new System.Drawing.Point(497, 104);
            this.clcNgayDiDuKien3.Name = "clcNgayDiDuKien3";
            this.clcNgayDiDuKien3.ReadOnly = false;
            this.clcNgayDiDuKien3.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiDuKien3.TabIndex = 1;
            this.clcNgayDiDuKien3.TagName = "";
            this.clcNgayDiDuKien3.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiDuKien3.WhereCondition = "";
            // 
            // clcNgayDenThucTe3
            // 
            this.clcNgayDenThucTe3.Location = new System.Drawing.Point(381, 104);
            this.clcNgayDenThucTe3.Name = "clcNgayDenThucTe3";
            this.clcNgayDenThucTe3.ReadOnly = false;
            this.clcNgayDenThucTe3.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenThucTe3.TabIndex = 1;
            this.clcNgayDenThucTe3.TagName = "";
            this.clcNgayDenThucTe3.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenThucTe3.WhereCondition = "";
            // 
            // clcNgayDenDuKien3
            // 
            this.clcNgayDenDuKien3.Location = new System.Drawing.Point(264, 104);
            this.clcNgayDenDuKien3.Name = "clcNgayDenDuKien3";
            this.clcNgayDenDuKien3.ReadOnly = false;
            this.clcNgayDenDuKien3.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenDuKien3.TabIndex = 1;
            this.clcNgayDenDuKien3.TagName = "";
            this.clcNgayDenDuKien3.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenDuKien3.WhereCondition = "";
            // 
            // clcNgayDiThucTe2
            // 
            this.clcNgayDiThucTe2.Location = new System.Drawing.Point(619, 77);
            this.clcNgayDiThucTe2.Name = "clcNgayDiThucTe2";
            this.clcNgayDiThucTe2.ReadOnly = false;
            this.clcNgayDiThucTe2.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiThucTe2.TabIndex = 1;
            this.clcNgayDiThucTe2.TagName = "";
            this.clcNgayDiThucTe2.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiThucTe2.WhereCondition = "";
            // 
            // clcNgayDiDuKien2
            // 
            this.clcNgayDiDuKien2.Location = new System.Drawing.Point(497, 77);
            this.clcNgayDiDuKien2.Name = "clcNgayDiDuKien2";
            this.clcNgayDiDuKien2.ReadOnly = false;
            this.clcNgayDiDuKien2.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiDuKien2.TabIndex = 1;
            this.clcNgayDiDuKien2.TagName = "";
            this.clcNgayDiDuKien2.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiDuKien2.WhereCondition = "";
            // 
            // clcNgayDenThucTe2
            // 
            this.clcNgayDenThucTe2.Location = new System.Drawing.Point(381, 77);
            this.clcNgayDenThucTe2.Name = "clcNgayDenThucTe2";
            this.clcNgayDenThucTe2.ReadOnly = false;
            this.clcNgayDenThucTe2.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenThucTe2.TabIndex = 1;
            this.clcNgayDenThucTe2.TagName = "";
            this.clcNgayDenThucTe2.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenThucTe2.WhereCondition = "";
            // 
            // clcNgayDenDuKien2
            // 
            this.clcNgayDenDuKien2.Location = new System.Drawing.Point(264, 77);
            this.clcNgayDenDuKien2.Name = "clcNgayDenDuKien2";
            this.clcNgayDenDuKien2.ReadOnly = false;
            this.clcNgayDenDuKien2.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenDuKien2.TabIndex = 1;
            this.clcNgayDenDuKien2.TagName = "";
            this.clcNgayDenDuKien2.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenDuKien2.WhereCondition = "";
            // 
            // clcNgayDiThucTe1
            // 
            this.clcNgayDiThucTe1.Location = new System.Drawing.Point(619, 50);
            this.clcNgayDiThucTe1.Name = "clcNgayDiThucTe1";
            this.clcNgayDiThucTe1.ReadOnly = false;
            this.clcNgayDiThucTe1.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiThucTe1.TabIndex = 1;
            this.clcNgayDiThucTe1.TagName = "";
            this.clcNgayDiThucTe1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiThucTe1.WhereCondition = "";
            // 
            // clcNgayDiDuKien1
            // 
            this.clcNgayDiDuKien1.Location = new System.Drawing.Point(497, 50);
            this.clcNgayDiDuKien1.Name = "clcNgayDiDuKien1";
            this.clcNgayDiDuKien1.ReadOnly = false;
            this.clcNgayDiDuKien1.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDiDuKien1.TabIndex = 1;
            this.clcNgayDiDuKien1.TagName = "";
            this.clcNgayDiDuKien1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDiDuKien1.WhereCondition = "";
            // 
            // clcNgayDenThucTe1
            // 
            this.clcNgayDenThucTe1.Location = new System.Drawing.Point(381, 50);
            this.clcNgayDenThucTe1.Name = "clcNgayDenThucTe1";
            this.clcNgayDenThucTe1.ReadOnly = false;
            this.clcNgayDenThucTe1.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenThucTe1.TabIndex = 1;
            this.clcNgayDenThucTe1.TagName = "";
            this.clcNgayDenThucTe1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenThucTe1.WhereCondition = "";
            // 
            // clcNgayDenDuKien1
            // 
            this.clcNgayDenDuKien1.Location = new System.Drawing.Point(264, 50);
            this.clcNgayDenDuKien1.Name = "clcNgayDenDuKien1";
            this.clcNgayDenDuKien1.ReadOnly = false;
            this.clcNgayDenDuKien1.Size = new System.Drawing.Size(96, 21);
            this.clcNgayDenDuKien1.TabIndex = 1;
            this.clcNgayDenDuKien1.TagName = "";
            this.clcNgayDenDuKien1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDenDuKien1.WhereCondition = "";
            // 
            // ctrMaTrungChuyen3
            // 
            this.ctrMaTrungChuyen3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTrungChuyen3.Appearance.Options.UseBackColor = true;
            this.ctrMaTrungChuyen3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaTrungChuyen3.Code = "";
            this.ctrMaTrungChuyen3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTrungChuyen3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTrungChuyen3.IsOnlyWarning = false;
            this.ctrMaTrungChuyen3.IsValidate = true;
            this.ctrMaTrungChuyen3.Location = new System.Drawing.Point(21, 104);
            this.ctrMaTrungChuyen3.Name = "ctrMaTrungChuyen3";
            this.ctrMaTrungChuyen3.Name_VN = "";
            this.ctrMaTrungChuyen3.SetOnlyWarning = false;
            this.ctrMaTrungChuyen3.SetValidate = false;
            this.ctrMaTrungChuyen3.ShowColumnCode = true;
            this.ctrMaTrungChuyen3.ShowColumnName = true;
            this.ctrMaTrungChuyen3.Size = new System.Drawing.Size(220, 21);
            this.ctrMaTrungChuyen3.TabIndex = 0;
            this.ctrMaTrungChuyen3.TagCode = "";
            this.ctrMaTrungChuyen3.TagName = "";
            this.ctrMaTrungChuyen3.Where = null;
            this.ctrMaTrungChuyen3.WhereCondition = "";
            // 
            // ctrMaTrungChuyen2
            // 
            this.ctrMaTrungChuyen2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTrungChuyen2.Appearance.Options.UseBackColor = true;
            this.ctrMaTrungChuyen2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaTrungChuyen2.Code = "";
            this.ctrMaTrungChuyen2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTrungChuyen2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTrungChuyen2.IsOnlyWarning = false;
            this.ctrMaTrungChuyen2.IsValidate = true;
            this.ctrMaTrungChuyen2.Location = new System.Drawing.Point(21, 77);
            this.ctrMaTrungChuyen2.Name = "ctrMaTrungChuyen2";
            this.ctrMaTrungChuyen2.Name_VN = "";
            this.ctrMaTrungChuyen2.SetOnlyWarning = false;
            this.ctrMaTrungChuyen2.SetValidate = false;
            this.ctrMaTrungChuyen2.ShowColumnCode = true;
            this.ctrMaTrungChuyen2.ShowColumnName = true;
            this.ctrMaTrungChuyen2.Size = new System.Drawing.Size(220, 21);
            this.ctrMaTrungChuyen2.TabIndex = 0;
            this.ctrMaTrungChuyen2.TagCode = "";
            this.ctrMaTrungChuyen2.TagName = "";
            this.ctrMaTrungChuyen2.Where = null;
            this.ctrMaTrungChuyen2.WhereCondition = "";
            // 
            // ctrMaTrungChuyen1
            // 
            this.ctrMaTrungChuyen1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTrungChuyen1.Appearance.Options.UseBackColor = true;
            this.ctrMaTrungChuyen1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaTrungChuyen1.Code = "";
            this.ctrMaTrungChuyen1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTrungChuyen1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTrungChuyen1.IsOnlyWarning = false;
            this.ctrMaTrungChuyen1.IsValidate = true;
            this.ctrMaTrungChuyen1.Location = new System.Drawing.Point(21, 50);
            this.ctrMaTrungChuyen1.Name = "ctrMaTrungChuyen1";
            this.ctrMaTrungChuyen1.Name_VN = "";
            this.ctrMaTrungChuyen1.SetOnlyWarning = false;
            this.ctrMaTrungChuyen1.SetValidate = false;
            this.ctrMaTrungChuyen1.ShowColumnCode = true;
            this.ctrMaTrungChuyen1.ShowColumnName = true;
            this.ctrMaTrungChuyen1.Size = new System.Drawing.Size(220, 21);
            this.ctrMaTrungChuyen1.TabIndex = 0;
            this.ctrMaTrungChuyen1.TagCode = "";
            this.ctrMaTrungChuyen1.TagName = "";
            this.ctrMaTrungChuyen1.Where = null;
            this.ctrMaTrungChuyen1.WhereCondition = "";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 144);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(602, 40);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(524, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(443, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // VNACC_ChuyenCuaKhau_TrungChuyenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 190);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChuyenCuaKhau_TrungChuyenForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin trung chuyển";
            this.Load += new System.EventHandler(this.VNACC_ChuyenCuaKhau_TrungChuyenForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiThucTe3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiDuKien3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenThucTe3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenDuKien3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiThucTe2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiDuKien2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenThucTe2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenDuKien2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiThucTe1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDiDuKien1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenThucTe1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDenDuKien1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaTrungChuyen3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaTrungChuyen2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaTrungChuyen1;
    }
}