﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Configuration;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ChuyenTrangThaiMultiForm : BaseForm
    {
        ToKhaiMauDich TKMD;
        ToKhaiMauDichCollection TKMDColl = new ToKhaiMauDichCollection();
        public ChuyenTrangThaiMultiForm(ToKhaiMauDichCollection TKColl)
        {
            InitializeComponent();
            this.TKMDColl = TKColl;
            BindData();
            foreach (ToKhaiMauDich tkmd in TKMDColl)
            {
                if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    dgList.RootTable.Columns["NgayDangKy"].EditType = EditType.NoEdit;
                }

            }
        }

        public void BindData()
        {
            dgList.DataSource = TKMDColl;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXRow[] arr = dgList.GetRows();
                for (int i = 0; i < arr.Length; i++)
                {
                    ToKhaiMauDich obj = (ToKhaiMauDich)arr[i].DataRow;

                    if (obj.SoToKhai == 0 || obj.NgayDangKy.Equals(new DateTime(1900, 1, 1)))
                    {
                        arr[i].Cells["TrangThaiXuLy"].Text = "Không hợp lệ";
                        continue;
                    }

                    obj.LoadHMDCollection();
                    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkDaDangKy = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDaDangKy.MaHaiQuan = obj.MaHaiQuan;
                    tkDaDangKy.NamDangKy = (short)obj.NgayDangKy.Year;
                    tkDaDangKy.MaLoaiHinh = obj.MaLoaiHinh;
                    tkDaDangKy.SoToKhai = obj.SoToKhai;
                    tkDaDangKy.NgayDangKy = obj.NgayDangKy;
                    if (obj.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        obj.NgayTiepNhan = obj.NgayDangKy;

                    if (!tkDaDangKy.Load())
                    {
                        obj.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        obj.TransgferDataToSXXK();
                        //BindData();
                        arr[i].Cells["TrangThaiXuLy"].Text = arr[i].Cells["TrangThaiXuLy"].ToolTipText = "Chuyển trạng thái thành công";
                    }
                    else
                    {
                        arr[i].Cells["TrangThaiXuLy"].Text = arr[i].Cells["TrangThaiXuLy"].ToolTipText = "Thông tin tờ khai này trùng trong danh sách đăng ký!";
                    }
                    Company.KDT.SHARE.Components.Globals.XmlSaveMessage(string.Empty, obj.ID, Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay, arr[i].Cells["TrangThaiXuLy"].Text);

                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi chuyển trạng thái Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "";
                    break;
                case 1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "";
                    break;
            }
        }
        
    }
}