﻿namespace Company.Interface.SXXK.BangKeThanhLy
{
    partial class ImportExcelBK03Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkFileExcel = new System.Windows.Forms.LinkLabel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.btnReadFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRowIndex = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.linkFileExcel);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSelectFile);
            this.grbMain.Controls.Add(this.btnReadFile);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.txtSheet);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.txtSoTK);
            this.grbMain.Controls.Add(this.txtLuong);
            this.grbMain.Controls.Add(this.txtDVT);
            this.grbMain.Controls.Add(this.txtSTT);
            this.grbMain.Controls.Add(this.txtTenNPL);
            this.grbMain.Controls.Add(this.txtMaNPL);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.label6);
            this.grbMain.Controls.Add(this.label10);
            this.grbMain.Controls.Add(this.label13);
            this.grbMain.Controls.Add(this.txtRowIndex);
            this.grbMain.Controls.Add(this.label14);
            this.grbMain.Size = new System.Drawing.Size(717, 176);
            // 
            // linkFileExcel
            // 
            this.linkFileExcel.AutoSize = true;
            this.linkFileExcel.BackColor = System.Drawing.Color.Transparent;
            this.linkFileExcel.Location = new System.Drawing.Point(23, 154);
            this.linkFileExcel.Name = "linkFileExcel";
            this.linkFileExcel.Size = new System.Drawing.Size(86, 13);
            this.linkFileExcel.TabIndex = 139;
            this.linkFileExcel.TabStop = true;
            this.linkFileExcel.Text = "FILE EXCEL MẪU";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(356, 150);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 25);
            this.btnClose.TabIndex = 138;
            this.btnClose.Text = "ĐÓNG";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnSelectFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnSelectFile.ForeColor = System.Drawing.Color.White;
            this.btnSelectFile.Location = new System.Drawing.Point(619, 113);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(90, 25);
            this.btnSelectFile.TabIndex = 136;
            this.btnSelectFile.Text = "CHỌN FILE";
            this.btnSelectFile.UseVisualStyleBackColor = false;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // btnReadFile
            // 
            this.btnReadFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnReadFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReadFile.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnReadFile.ForeColor = System.Drawing.Color.White;
            this.btnReadFile.Location = new System.Drawing.Point(260, 150);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(90, 25);
            this.btnReadFile.TabIndex = 137;
            this.btnReadFile.Text = "ĐỌC FILE";
            this.btnReadFile.UseVisualStyleBackColor = false;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 133;
            this.label3.Text = "STT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(523, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 135;
            this.label2.Text = "ĐVT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(424, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 134;
            this.label1.Text = "Lượng";
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(26, 26);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(184, 21);
            this.txtSheet.TabIndex = 112;
            this.txtSheet.Text = "Sheet1";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtFilePath);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(21, 95);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(592, 53);
            this.uiGroupBox1.TabIndex = 113;
            this.uiGroupBox1.Text = "Đường dẫn file Excel cần nhập";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(5, 20);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(587, 21);
            this.txtFilePath.TabIndex = 0;
            // 
            // txtSoTK
            // 
            this.txtSoTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTK.Location = new System.Drawing.Point(128, 66);
            this.txtSoTK.MaxLength = 1;
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(90, 21);
            this.txtSoTK.TabIndex = 126;
            this.txtSoTK.Text = "B";
            // 
            // txtLuong
            // 
            this.txtLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLuong.Location = new System.Drawing.Point(427, 65);
            this.txtLuong.MaxLength = 1;
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.Size = new System.Drawing.Size(90, 21);
            this.txtLuong.TabIndex = 127;
            this.txtLuong.Text = "E";
            // 
            // txtDVT
            // 
            this.txtDVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVT.Location = new System.Drawing.Point(523, 66);
            this.txtDVT.MaxLength = 1;
            this.txtDVT.Name = "txtDVT";
            this.txtDVT.Size = new System.Drawing.Size(90, 21);
            this.txtDVT.TabIndex = 129;
            this.txtDVT.Text = "F";
            // 
            // txtSTT
            // 
            this.txtSTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSTT.Location = new System.Drawing.Point(26, 66);
            this.txtSTT.MaxLength = 1;
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.Size = new System.Drawing.Size(90, 21);
            this.txtSTT.TabIndex = 128;
            this.txtSTT.Text = "A";
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenNPL.Location = new System.Drawing.Point(331, 66);
            this.txtTenNPL.MaxLength = 1;
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.Size = new System.Drawing.Size(90, 21);
            this.txtTenNPL.TabIndex = 122;
            this.txtTenNPL.Text = "D";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNPL.Location = new System.Drawing.Point(235, 66);
            this.txtMaNPL.MaxLength = 1;
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(90, 21);
            this.txtMaNPL.TabIndex = 121;
            this.txtMaNPL.Text = "C";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 111;
            this.label5.Text = "Tên Sheet";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(125, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 117;
            this.label6.Text = "Số tờ khai";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(215, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 115;
            this.label10.Text = "Dòng bắt đầu";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(232, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 131;
            this.label13.Text = "Mã NPL";
            // 
            // txtRowIndex
            // 
            this.txtRowIndex.DecimalDigits = 0;
            this.txtRowIndex.Location = new System.Drawing.Point(218, 26);
            this.txtRowIndex.MaxLength = 6;
            this.txtRowIndex.Name = "txtRowIndex";
            this.txtRowIndex.Size = new System.Drawing.Size(90, 21);
            this.txtRowIndex.TabIndex = 116;
            this.txtRowIndex.Text = "1";
            this.txtRowIndex.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(328, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 132;
            this.label14.Text = "Tên NPL";
            // 
            // ImportExcelBK03Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(717, 176);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximumSize = new System.Drawing.Size(733, 215);
            this.MinimumSize = new System.Drawing.Size(733, 215);
            this.Name = "ImportExcelBK03Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "NHẬP EXCEL NGUYÊN PHỤ LIỆU CHƯA THANH LÝ";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkFileExcel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Button btnReadFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSTT;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowIndex;
        private System.Windows.Forms.Label label14;


    }
}