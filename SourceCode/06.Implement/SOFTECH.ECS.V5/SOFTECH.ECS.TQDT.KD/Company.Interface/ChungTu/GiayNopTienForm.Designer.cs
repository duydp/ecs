﻿namespace Company.Interface
{
    partial class GiayNopTienForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListDetail_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListChungTu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayNopTienForm));
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDVNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDVNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNganHangNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNganHangNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoTKNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTKNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDVNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaDVNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaNganHangNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaNganHangNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaNguoiNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiNop = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSoLenh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayPhatLenh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.PNLChiTiet = new Janus.Windows.UI.Dock.UIPanel();
            this.PNLChiTietContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgListDetail = new Janus.Windows.GridEX.GridEX();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.PNLChungTu = new Janus.Windows.UI.Dock.UIPanel();
            this.PNLChungTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgListChungTu = new Janus.Windows.GridEX.GridEX();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdLayAnDinhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayAnDinhThue");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiTiet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiTiet");
            this.cmdChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTu");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdXoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXoa");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDuLieu");
            this.cmdChiTiet = new Janus.Windows.UI.CommandBars.UICommand("cmdChiTiet");
            this.cmdChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDuLieu");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdXoa = new Janus.Windows.UI.CommandBars.UICommand("cmdXoa");
            this.cmdLayAnDinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdLayAnDinhThue");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnThuTucHQTruocDo = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PNLChiTiet)).BeginInit();
            this.PNLChiTiet.SuspendLayout();
            this.PNLChiTietContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PNLChungTu)).BeginInit();
            this.PNLChungTu.SuspendLayout();
            this.PNLChungTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListChungTu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Location = new System.Drawing.Point(3, 35);
            this.grbMain.Size = new System.Drawing.Size(811, 360);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.label16);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Controls.Add(this.label12);
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(3, 3);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(808, 53);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(620, 17);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(315, 17);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(93, 17);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(123, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(474, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Trạng thái";
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lbTrangThai.Location = new System.Drawing.Point(535, 22);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(76, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Chưa khai báo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(229, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ngày tiếp nhận";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số tiếp nhận";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnThuTucHQTruocDo);
            this.uiGroupBox2.Controls.Add(this.txtTenDVNhan);
            this.uiGroupBox2.Controls.Add(this.txtTenDVNop);
            this.uiGroupBox2.Controls.Add(this.txtTenNganHangNhan);
            this.uiGroupBox2.Controls.Add(this.txtTenNganHangNop);
            this.uiGroupBox2.Controls.Add(this.txtTenNguoiNop);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtSoTKNhan);
            this.uiGroupBox2.Controls.Add(this.txtSoTKNop);
            this.uiGroupBox2.Controls.Add(this.txtMaDVNhan);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtMaDVNop);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaNganHangNhan);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.txtMaNganHangNop);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtMaNguoiNop);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.txtDiaChiNguoiNop);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtSoLenh);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayPhatLenh);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 62);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(808, 281);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDVNhan
            // 
            this.txtTenDVNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVNhan.Location = new System.Drawing.Point(350, 171);
            this.txtTenDVNhan.MaxLength = 255;
            this.txtTenDVNhan.Name = "txtTenDVNhan";
            this.txtTenDVNhan.Size = new System.Drawing.Size(190, 21);
            this.txtTenDVNhan.TabIndex = 11;
            this.txtTenDVNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDVNop
            // 
            this.txtTenDVNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVNop.Location = new System.Drawing.Point(350, 115);
            this.txtTenDVNop.MaxLength = 255;
            this.txtTenDVNop.Name = "txtTenDVNop";
            this.txtTenDVNop.Size = new System.Drawing.Size(190, 21);
            this.txtTenDVNop.TabIndex = 6;
            this.txtTenDVNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNganHangNhan
            // 
            this.txtTenNganHangNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNganHangNhan.Location = new System.Drawing.Point(350, 198);
            this.txtTenNganHangNhan.MaxLength = 255;
            this.txtTenNganHangNhan.Name = "txtTenNganHangNhan";
            this.txtTenNganHangNhan.Size = new System.Drawing.Size(402, 21);
            this.txtTenNganHangNhan.TabIndex = 14;
            this.txtTenNganHangNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNganHangNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNganHangNop
            // 
            this.txtTenNganHangNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNganHangNop.Location = new System.Drawing.Point(350, 144);
            this.txtTenNganHangNop.MaxLength = 255;
            this.txtTenNganHangNop.Name = "txtTenNganHangNop";
            this.txtTenNganHangNop.Size = new System.Drawing.Size(402, 21);
            this.txtTenNganHangNop.TabIndex = 9;
            this.txtTenNganHangNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNganHangNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiNop
            // 
            this.txtTenNguoiNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNop.Location = new System.Drawing.Point(439, 44);
            this.txtTenNguoiNop.MaxLength = 255;
            this.txtTenNguoiNop.Name = "txtTenNguoiNop";
            this.txtTenNguoiNop.Size = new System.Drawing.Size(313, 21);
            this.txtTenNguoiNop.TabIndex = 3;
            this.txtTenNguoiNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(248, 177);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Tên đơn vị nhận";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(248, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Tên đơn vị nộp";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(542, 177);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Số tài khoản nhận";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(542, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Số tài khoản nộp";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(247, 204);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 13);
            this.label18.TabIndex = 20;
            this.label18.Text = "Tên ngân hàng nhận";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(247, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Tên ngân hàng nộp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(355, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Tên người nộp";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(120, 230);
            this.txtGhiChu.MaxLength = 255;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(632, 36);
            this.txtGhiChu.TabIndex = 15;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Ghi chú";
            // 
            // txtSoTKNhan
            // 
            this.txtSoTKNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKNhan.Location = new System.Drawing.Point(641, 171);
            this.txtSoTKNhan.MaxLength = 255;
            this.txtSoTKNhan.Name = "txtSoTKNhan";
            this.txtSoTKNhan.Size = new System.Drawing.Size(111, 21);
            this.txtSoTKNhan.TabIndex = 12;
            this.txtSoTKNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTKNop
            // 
            this.txtSoTKNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKNop.Location = new System.Drawing.Point(641, 115);
            this.txtSoTKNop.MaxLength = 255;
            this.txtSoTKNop.Name = "txtSoTKNop";
            this.txtSoTKNop.Size = new System.Drawing.Size(111, 21);
            this.txtSoTKNop.TabIndex = 7;
            this.txtSoTKNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDVNhan
            // 
            this.txtMaDVNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVNhan.Location = new System.Drawing.Point(120, 172);
            this.txtMaDVNhan.MaxLength = 255;
            this.txtMaDVNhan.Name = "txtMaDVNhan";
            this.txtMaDVNhan.Size = new System.Drawing.Size(125, 21);
            this.txtMaDVNhan.TabIndex = 10;
            this.txtMaDVNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Mã đơn vị nhận";
            // 
            // txtMaDVNop
            // 
            this.txtMaDVNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVNop.Location = new System.Drawing.Point(120, 116);
            this.txtMaDVNop.MaxLength = 255;
            this.txtMaDVNop.Name = "txtMaDVNop";
            this.txtMaDVNop.Size = new System.Drawing.Size(125, 21);
            this.txtMaDVNop.TabIndex = 5;
            this.txtMaDVNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Mã đơn vị nộp";
            // 
            // txtMaNganHangNhan
            // 
            this.txtMaNganHangNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNganHangNhan.Location = new System.Drawing.Point(120, 199);
            this.txtMaNganHangNhan.MaxLength = 255;
            this.txtMaNganHangNhan.Name = "txtMaNganHangNhan";
            this.txtMaNganHangNhan.Size = new System.Drawing.Size(125, 21);
            this.txtMaNganHangNhan.TabIndex = 13;
            this.txtMaNganHangNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNganHangNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 204);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Mã ngân hàng nhận";
            // 
            // txtMaNganHangNop
            // 
            this.txtMaNganHangNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNganHangNop.Location = new System.Drawing.Point(120, 145);
            this.txtMaNganHangNop.MaxLength = 255;
            this.txtMaNganHangNop.Name = "txtMaNganHangNop";
            this.txtMaNganHangNop.Size = new System.Drawing.Size(125, 21);
            this.txtMaNganHangNop.TabIndex = 8;
            this.txtMaNganHangNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNganHangNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Mã ngân hàng nộp";
            // 
            // txtMaNguoiNop
            // 
            this.txtMaNguoiNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNop.Location = new System.Drawing.Point(120, 47);
            this.txtMaNguoiNop.MaxLength = 255;
            this.txtMaNguoiNop.Name = "txtMaNguoiNop";
            this.txtMaNguoiNop.Size = new System.Drawing.Size(225, 21);
            this.txtMaNguoiNop.TabIndex = 2;
            this.txtMaNguoiNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "MST người nộp";
            // 
            // txtDiaChiNguoiNop
            // 
            this.txtDiaChiNguoiNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiNop.Location = new System.Drawing.Point(120, 74);
            this.txtDiaChiNguoiNop.MaxLength = 255;
            this.txtDiaChiNguoiNop.Multiline = true;
            this.txtDiaChiNguoiNop.Name = "txtDiaChiNguoiNop";
            this.txtDiaChiNguoiNop.Size = new System.Drawing.Size(632, 36);
            this.txtDiaChiNguoiNop.TabIndex = 4;
            this.txtDiaChiNguoiNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiNguoiNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 21);
            this.label2.TabIndex = 14;
            this.label2.Text = "Địa chỉ người nộp";
            // 
            // txtSoLenh
            // 
            this.txtSoLenh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLenh.Location = new System.Drawing.Point(120, 17);
            this.txtSoLenh.MaxLength = 255;
            this.txtSoLenh.Name = "txtSoLenh";
            this.txtSoLenh.Size = new System.Drawing.Size(225, 21);
            this.txtSoLenh.TabIndex = 0;
            this.txtSoLenh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLenh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLenh.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(355, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày phát lệnh";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số lệnh";
            // 
            // ccNgayPhatLenh
            // 
            // 
            // 
            // 
            this.ccNgayPhatLenh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayPhatLenh.DropDownCalendar.Name = "";
            this.ccNgayPhatLenh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayPhatLenh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayPhatLenh.IsNullDate = true;
            this.ccNgayPhatLenh.Location = new System.Drawing.Point(439, 17);
            this.ccNgayPhatLenh.Name = "ccNgayPhatLenh";
            this.ccNgayPhatLenh.Nullable = true;
            this.ccNgayPhatLenh.NullButtonText = "Xóa";
            this.ccNgayPhatLenh.ShowNullButton = true;
            this.ccNgayPhatLenh.Size = new System.Drawing.Size(101, 21);
            this.ccNgayPhatLenh.TabIndex = 1;
            this.ccNgayPhatLenh.TodayButtonText = "Hôm nay";
            this.ccNgayPhatLenh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayPhatLenh.VisualStyleManager = this.vsmMain;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.PNLChiTiet.Id = new System.Guid("b27d0c2d-f38a-48b7-9b07-e53cfa68d817");
            this.uiPanelManager1.Panels.Add(this.PNLChiTiet);
            this.PNLChungTu.Id = new System.Guid("de0bd47b-a330-4337-88eb-4f014b0a944d");
            this.uiPanelManager1.Panels.Add(this.PNLChungTu);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b27d0c2d-f38a-48b7-9b07-e53cfa68d817"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(803, 200), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("de0bd47b-a330-4337-88eb-4f014b0a944d"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(779, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b27d0c2d-f38a-48b7-9b07-e53cfa68d817"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("de0bd47b-a330-4337-88eb-4f014b0a944d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // PNLChiTiet
            // 
            this.PNLChiTiet.AutoHide = true;
            this.PNLChiTiet.InnerContainer = this.PNLChiTietContainer;
            this.PNLChiTiet.Location = new System.Drawing.Point(3, 194);
            this.PNLChiTiet.Name = "PNLChiTiet";
            this.PNLChiTiet.Size = new System.Drawing.Size(803, 200);
            this.PNLChiTiet.TabIndex = 4;
            this.PNLChiTiet.Text = "Chi tiết giấy nộp tiền";
            // 
            // PNLChiTietContainer
            // 
            this.PNLChiTietContainer.Controls.Add(this.dgListDetail);
            this.PNLChiTietContainer.Location = new System.Drawing.Point(1, 27);
            this.PNLChiTietContainer.Name = "PNLChiTietContainer";
            this.PNLChiTietContainer.Size = new System.Drawing.Size(801, 172);
            this.PNLChiTietContainer.TabIndex = 0;
            // 
            // dgListDetail
            // 
            this.dgListDetail.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDetail.AlternatingColors = true;
            this.dgListDetail.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDetail.ColumnAutoResize = true;
            dgListDetail_DesignTimeLayout.LayoutString = resources.GetString("dgListDetail_DesignTimeLayout.LayoutString");
            this.dgListDetail.DesignTimeLayout = dgListDetail_DesignTimeLayout;
            this.dgListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDetail.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDetail.FrozenColumns = 3;
            this.dgListDetail.GroupByBoxVisible = false;
            this.dgListDetail.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDetail.ImageList = this.imageList1;
            this.dgListDetail.Location = new System.Drawing.Point(0, 0);
            this.dgListDetail.Name = "dgListDetail";
            this.dgListDetail.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDetail.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDetail.Size = new System.Drawing.Size(801, 172);
            this.dgListDetail.TabIndex = 8;
            this.dgListDetail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListDetail.VisualStyleManager = this.vsmMain;
            this.dgListDetail.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListDetail_RowDoubleClick);
            this.dgListDetail.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgListDetail_DeletingRecords);
            this.dgListDetail.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListDetail_LoadingRow);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ChungTuKemTheo1.Icon.ico");
            this.imageList1.Images.SetKeyName(1, "cmdChuyenCuaKhau1.Icon.ico");
            this.imageList1.Images.SetKeyName(2, "TruyenDuLieu.Icon.ico");
            this.imageList1.Images.SetKeyName(3, "NhanDuLieu.Icon.ico");
            this.imageList1.Images.SetKeyName(4, "cmdSave.Icon.ico");
            this.imageList1.Images.SetKeyName(5, "shell32_240.ico");
            // 
            // PNLChungTu
            // 
            this.PNLChungTu.AutoHide = true;
            this.PNLChungTu.InnerContainer = this.PNLChungTuContainer;
            this.PNLChungTu.Location = new System.Drawing.Point(3, 160);
            this.PNLChungTu.Name = "PNLChungTu";
            this.PNLChungTu.Size = new System.Drawing.Size(779, 200);
            this.PNLChungTu.TabIndex = 4;
            this.PNLChungTu.Text = "Chứng từ giấy nộp tiền";
            // 
            // PNLChungTuContainer
            // 
            this.PNLChungTuContainer.Controls.Add(this.dgListChungTu);
            this.PNLChungTuContainer.Location = new System.Drawing.Point(1, 27);
            this.PNLChungTuContainer.Name = "PNLChungTuContainer";
            this.PNLChungTuContainer.Size = new System.Drawing.Size(777, 172);
            this.PNLChungTuContainer.TabIndex = 0;
            // 
            // dgListChungTu
            // 
            this.dgListChungTu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListChungTu.AlternatingColors = true;
            this.dgListChungTu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListChungTu.ColumnAutoResize = true;
            dgListChungTu_DesignTimeLayout.LayoutString = resources.GetString("dgListChungTu_DesignTimeLayout.LayoutString");
            this.dgListChungTu.DesignTimeLayout = dgListChungTu_DesignTimeLayout;
            this.dgListChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListChungTu.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgListChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListChungTu.FrozenColumns = 3;
            this.dgListChungTu.GroupByBoxVisible = false;
            this.dgListChungTu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListChungTu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListChungTu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListChungTu.ImageList = this.imageList1;
            this.dgListChungTu.Location = new System.Drawing.Point(0, 0);
            this.dgListChungTu.Name = "dgListChungTu";
            this.dgListChungTu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListChungTu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListChungTu.Size = new System.Drawing.Size(777, 172);
            this.dgListChungTu.TabIndex = 9;
            this.dgListChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListChungTu.VisualStyleManager = this.vsmMain;
            this.dgListChungTu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListChungTu_RowDoubleClick);
            this.dgListChungTu.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgListChungTu_DeletingRecords);
            this.dgListChungTu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListChungTu_LoadingRow);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdChiTiet,
            this.cmdChungTu,
            this.cmdKhaiBao,
            this.cmdNhanDuLieu,
            this.cmdLuu,
            this.cmdXoa,
            this.cmdLayAnDinhThue});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.imageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLayAnDinhThue1,
            this.Separator2,
            this.cmdChiTiet1,
            this.cmdChungTu1,
            this.cmdLuu1,
            this.cmdXoa1,
            this.Separator1,
            this.cmdKhaiBao1,
            this.cmdNhanDuLieu1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "cmdBar";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(817, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdLayAnDinhThue1
            // 
            this.cmdLayAnDinhThue1.Key = "cmdLayAnDinhThue";
            this.cmdLayAnDinhThue1.Name = "cmdLayAnDinhThue1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdChiTiet1
            // 
            this.cmdChiTiet1.Key = "cmdChiTiet";
            this.cmdChiTiet1.Name = "cmdChiTiet1";
            // 
            // cmdChungTu1
            // 
            this.cmdChungTu1.Key = "cmdChungTu";
            this.cmdChungTu1.Name = "cmdChungTu1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdXoa1
            // 
            this.cmdXoa1.Key = "cmdXoa";
            this.cmdXoa1.Name = "cmdXoa1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdNhanDuLieu1
            // 
            this.cmdNhanDuLieu1.Key = "cmdNhanDuLieu";
            this.cmdNhanDuLieu1.Name = "cmdNhanDuLieu1";
            // 
            // cmdChiTiet
            // 
            this.cmdChiTiet.ImageIndex = 1;
            this.cmdChiTiet.Key = "cmdChiTiet";
            this.cmdChiTiet.Name = "cmdChiTiet";
            this.cmdChiTiet.Text = "Thêm chi tiết";
            // 
            // cmdChungTu
            // 
            this.cmdChungTu.ImageIndex = 1;
            this.cmdChungTu.Key = "cmdChungTu";
            this.cmdChungTu.Name = "cmdChungTu";
            this.cmdChungTu.Text = "Thêm chứng từ";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.ImageIndex = 2;
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdNhanDuLieu
            // 
            this.cmdNhanDuLieu.ImageIndex = 3;
            this.cmdNhanDuLieu.Key = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Name = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdXoa
            // 
            this.cmdXoa.ImageIndex = 5;
            this.cmdXoa.Key = "cmdXoa";
            this.cmdXoa.Name = "cmdXoa";
            this.cmdXoa.Text = "Xóa";
            // 
            // cmdLayAnDinhThue
            // 
            this.cmdLayAnDinhThue.ImageKey = "cmdChuyenCuaKhau1.Icon.ico";
            this.cmdLayAnDinhThue.Key = "cmdLayAnDinhThue";
            this.cmdLayAnDinhThue.Name = "cmdLayAnDinhThue";
            this.cmdLayAnDinhThue.Text = "Lấy thông tin từ Ấn định thuế";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(817, 32);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // btnThuTucHQTruocDo
            // 
            this.btnThuTucHQTruocDo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThuTucHQTruocDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuTucHQTruocDo.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThuTucHQTruocDo.Icon")));
            this.btnThuTucHQTruocDo.Location = new System.Drawing.Point(565, 15);
            this.btnThuTucHQTruocDo.Name = "btnThuTucHQTruocDo";
            this.btnThuTucHQTruocDo.Size = new System.Drawing.Size(187, 23);
            this.btnThuTucHQTruocDo.TabIndex = 2;
            this.btnThuTucHQTruocDo.Text = "Thủ tục HQ Trước đó";
            this.btnThuTucHQTruocDo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThuTucHQTruocDo.Click += new System.EventHandler(this.btnThuTucHQTruocDo_Click);
            // 
            // GiayNopTienForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 416);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "GiayNopTienForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Giấy nộp tiền";
            this.Load += new System.EventHandler(this.GiayNopTienForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PNLChiTiet)).EndInit();
            this.PNLChiTiet.ResumeLayout(false);
            this.PNLChiTietContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PNLChungTu)).EndInit();
            this.PNLChungTu.ResumeLayout(false);
            this.PNLChungTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListChungTu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLenh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayPhatLenh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNop;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNop;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiNop;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVNop;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTKNop;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVNop;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangNop;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangNop;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVNhan;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTKNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVNhan;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel PNLChiTiet;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer PNLChiTietContainer;
        private Janus.Windows.UI.Dock.UIPanel PNLChungTu;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer PNLChungTuContainer;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiTiet;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiTiet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTu1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDuLieu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoa;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoa1;
        private Janus.Windows.GridEX.GridEX dgListDetail;
        private Janus.Windows.GridEX.GridEX dgListChungTu;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangNhan;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangNhan;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayAnDinhThue;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayAnDinhThue1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.EditControls.UIButton btnThuTucHQTruocDo;
    }
}