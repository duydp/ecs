//using Infragistics.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Infragistics.Excel;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class CanDoiNhapXuatForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        private DataTable dt = new DataTable();
        public CanDoiNhapXuatForm()
        {
            InitializeComponent();
        }

        private void CanDoiNhapXuatForm_Load(object sender, EventArgs e)
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                this.dgList.Tables[0].Columns["LuongNPLNhap"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                this.dgList.Tables[0].Columns["LuongNPLXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                this.dgList.Tables[0].Columns["LuongNPLTon"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                saveFileDialog1.InitialDirectory = Application.StartupPath;
                this.reloadData();
                

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void reloadData()
        {
            this.dt = this.HSTL.GetCanDoiNhapXuat(GlobalSettings.SoThapPhan.LuongNPL); ;
            this.dgList.DataSource = this.dt;
            dgList.Refetch();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Refresh":
                    this.reloadData();
                    break;
                default: XuatFileExcel();
                    break;                 
            }
            
        }

        private void XemTKNNPLItem_Click(object sender, EventArgs e)
        {
            XemToKhaiNhapNPLForm f = new XemToKhaiNhapNPLForm();
            f.HSTL = this.HSTL;
            f.MaNPL = dgList.GetRow().Cells["MaNPL"].Text;
            f.ShowDialog(this);
        }
        private void XuatFileExcel()
        {
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("CanDoiNPLNhapXuatTon");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 10000;
            ws.Columns[2].Width = 5000;
            ws.Columns[3].Width = 5000;
            ws.Columns[4].Width = 5000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Mã NPL";
            wsr0.Cells[1].Value = "Tên NPL";
            wsr0.Cells[2].Value = "Lượng NPL Nhập";
            wsr0.Cells[3].Value = "Lượng NPL Xuất";
            wsr0.Cells[4].Value = "Lượng NPl Tồn";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = dt.Rows[i]["MaNPL"].ToString();
                wsr.Cells[1].Value = dt.Rows[i]["TenNPL"].ToString();
                wsr.Cells[2].Value = Convert.ToDecimal(dt.Rows[i]["LuongNPLNhap"]);
                wsr.Cells[3].Value = Convert.ToDecimal(dt.Rows[i]["LuongNPLXuat"]);
                wsr.Cells[4].Value = Convert.ToDecimal(dt.Rows[i]["LuongNPLTon"]);
            }
            DialogResult rs = saveFileDialog1.ShowDialog(this);
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
            }
        }
        private void xemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XemDinhMucNPLForm f = new XemDinhMucNPLForm();
            f.HSTL = this.HSTL;
            f.MaNPL = dgList.GetRow().Cells["MaNPL"].Text;
            f.ShowDialog(this);
        }

        private void xemDSTKN_Click(object sender, EventArgs e)
        {
            frmThemTKBK01 obj = new frmThemTKBK01();

            GridEXRow dr = dgList.SelectedItems[0].GetRow();
            obj.MaNPL = dr.Cells["MaNPL"].Text;
            obj.lblMaNPL.Text = dr.Cells["MaNPL"].Text;
            obj.lblTenNPL.Text = dr.Cells["TenNPL"].Text;
            obj.lblLuongNhap.Text = dr.Cells["LuongNPLNhap"].Text;
            obj.lblLuongXuat.Text = dr.Cells["LuongNPLXuat"].Text;
            obj.lblLuongTon.Text = dr.Cells["LuongNPLTon"].Text;
            obj.HSTL = this.HSTL;
            obj.ShowDialog(this);
            this.reloadData();
        }

    }
}

