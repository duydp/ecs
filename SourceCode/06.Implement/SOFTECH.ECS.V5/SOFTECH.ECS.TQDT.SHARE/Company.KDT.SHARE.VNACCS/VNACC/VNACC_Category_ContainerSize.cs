using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_ContainerSize
    {
        protected static List<VNACC_Category_ContainerSize> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_ContainerSize> collection = new List<VNACC_Category_ContainerSize>();
            while (reader.Read())
            {
                VNACC_Category_ContainerSize entity = new VNACC_Category_ContainerSize();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
                if (!reader.IsDBNull(reader.GetOrdinal("ContainerSizeCode"))) entity.ContainerSizeCode = reader.GetString(reader.GetOrdinal("ContainerSizeCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("ContainerSizeName"))) entity.ContainerSizeName = reader.GetString(reader.GetOrdinal("ContainerSizeName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_ContainerSize> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [ContainerSizeCode], [ContainerSizeName], NumberOfKeyItems FROM [dbo].[t_VNACC_Category_ContainerSize]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_ContainerSize> SelectCollectionBy(List<VNACC_Category_ContainerSize> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_ContainerSize o)
            {
                return o.TableID.Contains(tableID);
            });
        }
    }
}