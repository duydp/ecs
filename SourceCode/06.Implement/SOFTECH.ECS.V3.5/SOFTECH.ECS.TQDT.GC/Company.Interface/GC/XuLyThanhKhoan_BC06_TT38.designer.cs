﻿namespace Company.Interface
{
    partial class XuLyThanhKhoan_BC06_TT38
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgDinhMuc_SanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XuLyThanhKhoan_BC06_TT38));
            Janus.Windows.GridEX.GridEXLayout dgNPLCU_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNPLHuy_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatExcelNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenChuHang2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTimKiemNPL = new Janus.Windows.EditControls.UIButton();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.btnInBaoCao = new Janus.Windows.EditControls.UIButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnBCTong = new Janus.Windows.EditControls.UIButton();
            this.btnXuatExcel = new Janus.Windows.EditControls.UIButton();
            this.btnXuLyXNT_Mau15 = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLayDuLieu = new Janus.Windows.EditControls.UIButton();
            this.lblPercen = new System.Windows.Forms.Label();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lblXuLy = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgDinhMuc_SanPham = new Janus.Windows.GridEX.GridEX();
            this.mnuGridDM = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatExcelDM = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.btnChuyenNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.dgNPLCU = new Janus.Windows.GridEX.GridEX();
            this.mnnGridNPL_CU = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatEXNPL_CU = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLHuy = new Janus.Windows.GridEX.GridEX();
            this.btnChuyenNPLHuy = new Janus.Windows.EditControls.UIButton();
            this.mnnGrid_NPLHuy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatEX_NPLHuy = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDinhMuc_SanPham)).BeginInit();
            this.mnuGridDM.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).BeginInit();
            this.mnnGridNPL_CU.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLHuy)).BeginInit();
            this.mnnGrid_NPLHuy.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.groupBox1);
            this.grbMain.Size = new System.Drawing.Size(1060, 519);
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatExcelNPL});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatExcelNPL
            // 
            this.toolStripXuatExcelNPL.Name = "toolStripXuatExcelNPL";
            this.toolStripXuatExcelNPL.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatExcelNPL.Text = "Xuất excel";
            this.toolStripXuatExcelNPL.Click += new System.EventHandler(this.toolStripXuatExcelNPL_Click);
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.SheetName = "NPLTon";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtTenChuHang2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.btnTimKiemNPL);
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 19);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1035, 51);
            this.uiGroupBox5.TabIndex = 193;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChuHang2
            // 
            this.txtTenChuHang2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang2.Location = new System.Drawing.Point(136, 18);
            this.txtTenChuHang2.Name = "txtTenChuHang2";
            this.txtTenChuHang2.Size = new System.Drawing.Size(220, 20);
            this.txtTenChuHang2.TabIndex = 17;
            this.txtTenChuHang2.Text = "*";
            this.txtTenChuHang2.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tên chủ hàng";
            // 
            // btnTimKiemNPL
            // 
            this.btnTimKiemNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiemNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTimKiemNPL.Location = new System.Drawing.Point(362, 16);
            this.btnTimKiemNPL.Name = "btnTimKiemNPL";
            this.btnTimKiemNPL.Size = new System.Drawing.Size(84, 23);
            this.btnTimKiemNPL.TabIndex = 18;
            this.btnTimKiemNPL.Text = "Tìm kiếm";
            this.btnTimKiemNPL.VisualStyleManager = this.vsmMain;
            this.btnTimKiemNPL.WordWrap = false;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(12, 76);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(1035, 409);
            this.dgListNPL.TabIndex = 192;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPL.VisualStyleManager = this.vsmMain;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.dtpTuNgay);
            this.groupBox1.Controls.Add(this.dtpDenNgay);
            this.groupBox1.Controls.Add(this.btnInBaoCao);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.btnBCTong);
            this.groupBox1.Controls.Add(this.btnXuatExcel);
            this.groupBox1.Controls.Add(this.btnXuLyXNT_Mau15);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnLayDuLieu);
            this.groupBox1.Controls.Add(this.lblPercen);
            this.groupBox1.Controls.Add(this.btnDong);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblXuLy);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 440);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1060, 79);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Xử lý báo cáo";
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTuNgay.Location = new System.Drawing.Point(628, 22);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(94, 21);
            this.dtpTuNgay.TabIndex = 10;
            this.dtpTuNgay.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDenNgay.Location = new System.Drawing.Point(803, 20);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(94, 21);
            this.dtpDenNgay.TabIndex = 10;
            this.dtpDenNgay.Value = new System.DateTime(2015, 12, 31, 0, 0, 0, 0);
            // 
            // btnInBaoCao
            // 
            this.btnInBaoCao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInBaoCao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInBaoCao.Location = new System.Drawing.Point(204, 51);
            this.btnInBaoCao.Name = "btnInBaoCao";
            this.btnInBaoCao.Size = new System.Drawing.Size(97, 23);
            this.btnInBaoCao.TabIndex = 7;
            this.btnInBaoCao.Text = "Xuất BC chi tiết";
            this.btnInBaoCao.VisualStyleManager = this.vsmMain;
            this.btnInBaoCao.Click += new System.EventHandler(this.btnXuatMau15_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.Location = new System.Drawing.Point(3, 22);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(298, 23);
            this.progressBar1.TabIndex = 9;
            // 
            // btnBCTong
            // 
            this.btnBCTong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBCTong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBCTong.Location = new System.Drawing.Point(310, 51);
            this.btnBCTong.Name = "btnBCTong";
            this.btnBCTong.Size = new System.Drawing.Size(97, 23);
            this.btnBCTong.TabIndex = 7;
            this.btnBCTong.Text = "Xuất BC Tổng";
            this.btnBCTong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnBCTong.Click += new System.EventHandler(this.btnBCTong_Click);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnXuatExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXuatExcel.ImageIndex = 0;
            this.btnXuatExcel.ImageList = this.imageList1;
            this.btnXuatExcel.Location = new System.Drawing.Point(5, 51);
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.Size = new System.Drawing.Size(91, 23);
            this.btnXuatExcel.TabIndex = 6;
            this.btnXuatExcel.Text = "Xuất Excel";
            this.btnXuatExcel.VisualStyleManager = this.vsmMain;
            this.btnXuatExcel.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // btnXuLyXNT_Mau15
            // 
            this.btnXuLyXNT_Mau15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnXuLyXNT_Mau15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuLyXNT_Mau15.Location = new System.Drawing.Point(102, 51);
            this.btnXuLyXNT_Mau15.Name = "btnXuLyXNT_Mau15";
            this.btnXuLyXNT_Mau15.Size = new System.Drawing.Size(96, 23);
            this.btnXuLyXNT_Mau15.TabIndex = 7;
            this.btnXuLyXNT_Mau15.Text = "Xử lý";
            this.btnXuLyXNT_Mau15.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXuLyXNT_Mau15.Click += new System.EventHandler(this.btnXuLyXNT_Mau15_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(561, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "Từ ngày:";
            // 
            // btnLayDuLieu
            // 
            this.btnLayDuLieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayDuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayDuLieu.Location = new System.Drawing.Point(918, 21);
            this.btnLayDuLieu.Name = "btnLayDuLieu";
            this.btnLayDuLieu.Size = new System.Drawing.Size(125, 23);
            this.btnLayDuLieu.TabIndex = 7;
            this.btnLayDuLieu.Text = "Lấy dữ liệu";
            this.btnLayDuLieu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayDuLieu.Click += new System.EventHandler(this.btnLayDuLieu_Click);
            // 
            // lblPercen
            // 
            this.lblPercen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPercen.AutoSize = true;
            this.lblPercen.BackColor = System.Drawing.Color.Transparent;
            this.lblPercen.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercen.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblPercen.Location = new System.Drawing.Point(307, 27);
            this.lblPercen.Name = "lblPercen";
            this.lblPercen.Size = new System.Drawing.Size(38, 17);
            this.lblPercen.TabIndex = 8;
            this.lblPercen.Text = "0 %";
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Location = new System.Drawing.Point(980, 51);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(63, 23);
            this.btnDong.TabIndex = 8;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(728, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Đến ngày:";
            // 
            // lblXuLy
            // 
            this.lblXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblXuLy.AutoSize = true;
            this.lblXuLy.BackColor = System.Drawing.Color.Transparent;
            this.lblXuLy.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblXuLy.Location = new System.Drawing.Point(366, 27);
            this.lblXuLy.Name = "lblXuLy";
            this.lblXuLy.Size = new System.Drawing.Size(82, 17);
            this.lblXuLy.TabIndex = 8;
            this.lblXuLy.Text = "Chưa xử lý";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgDinhMuc_SanPham);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1058, 418);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "NPL Xuất theo định mức";
            // 
            // dgDinhMuc_SanPham
            // 
            this.dgDinhMuc_SanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgDinhMuc_SanPham.ContextMenuStrip = this.mnuGridDM;
            dgDinhMuc_SanPham_DesignTimeLayout.LayoutString = resources.GetString("dgDinhMuc_SanPham_DesignTimeLayout.LayoutString");
            this.dgDinhMuc_SanPham.DesignTimeLayout = dgDinhMuc_SanPham_DesignTimeLayout;
            this.dgDinhMuc_SanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDinhMuc_SanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgDinhMuc_SanPham.FrozenColumns = 5;
            this.dgDinhMuc_SanPham.GroupByBoxVisible = false;
            this.dgDinhMuc_SanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDinhMuc_SanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDinhMuc_SanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgDinhMuc_SanPham.Location = new System.Drawing.Point(0, 0);
            this.dgDinhMuc_SanPham.Name = "dgDinhMuc_SanPham";
            this.dgDinhMuc_SanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgDinhMuc_SanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgDinhMuc_SanPham.Size = new System.Drawing.Size(1058, 418);
            this.dgDinhMuc_SanPham.TabIndex = 2;
            this.dgDinhMuc_SanPham.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgDinhMuc_SanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgDinhMuc_SanPham.VisualStyleManager = this.vsmMain;
            // 
            // mnuGridDM
            // 
            this.mnuGridDM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatExcelDM});
            this.mnuGridDM.Name = "mnuGrid";
            this.mnuGridDM.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatExcelDM
            // 
            this.toolStripXuatExcelDM.Name = "toolStripXuatExcelDM";
            this.toolStripXuatExcelDM.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatExcelDM.Text = "Xuất excel";
            this.toolStripXuatExcelDM.Click += new System.EventHandler(this.toolStripXuatExcelDM_Click);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgList);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1058, 418);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "NPL nhập khẩu";
            // 
            // dgList
            // 
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.mnuGrid;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 5;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1058, 418);
            this.dgList.TabIndex = 1;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1060, 440);
            this.uiTab1.TabIndex = 2;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.btnChuyenNPLCungUng);
            this.uiTabPage3.Controls.Add(this.dgNPLCU);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1058, 418);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "NPL cần cung ứng";
            // 
            // btnChuyenNPLCungUng
            // 
            this.btnChuyenNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChuyenNPLCungUng.Location = new System.Drawing.Point(738, 3);
            this.btnChuyenNPLCungUng.Name = "btnChuyenNPLCungUng";
            this.btnChuyenNPLCungUng.Size = new System.Drawing.Size(97, 39);
            this.btnChuyenNPLCungUng.TabIndex = 8;
            this.btnChuyenNPLCungUng.Text = "Đăng ký NPL cung ứng";
            this.btnChuyenNPLCungUng.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChuyenNPLCungUng.Click += new System.EventHandler(this.btnChuyenNPLCungUng_Click);
            // 
            // dgNPLCU
            // 
            this.dgNPLCU.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCU.ContextMenuStrip = this.mnnGridNPL_CU;
            dgNPLCU_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCU_DesignTimeLayout.LayoutString");
            this.dgNPLCU.DesignTimeLayout = dgNPLCU_DesignTimeLayout;
            this.dgNPLCU.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgNPLCU.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCU.FrozenColumns = 5;
            this.dgNPLCU.GroupByBoxVisible = false;
            this.dgNPLCU.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCU.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCU.Location = new System.Drawing.Point(0, 0);
            this.dgNPLCU.Name = "dgNPLCU";
            this.dgNPLCU.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCU.Size = new System.Drawing.Size(732, 418);
            this.dgNPLCU.TabIndex = 3;
            this.dgNPLCU.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNPLCU.VisualStyleManager = this.vsmMain;
            // 
            // mnnGridNPL_CU
            // 
            this.mnnGridNPL_CU.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatEXNPL_CU});
            this.mnnGridNPL_CU.Name = "mnuGrid";
            this.mnnGridNPL_CU.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatEXNPL_CU
            // 
            this.toolStripXuatEXNPL_CU.Name = "toolStripXuatEXNPL_CU";
            this.toolStripXuatEXNPL_CU.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatEXNPL_CU.Text = "Xuất excel";
            this.toolStripXuatEXNPL_CU.Click += new System.EventHandler(this.toolStripXuatEXNPL_CU_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgNPLHuy);
            this.uiTabPage4.Controls.Add(this.btnChuyenNPLHuy);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1058, 418);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "NPL Hủy";
            // 
            // dgNPLHuy
            // 
            this.dgNPLHuy.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLHuy.ContextMenuStrip = this.mnnGridNPL_CU;
            dgNPLHuy_DesignTimeLayout.LayoutString = resources.GetString("dgNPLHuy_DesignTimeLayout.LayoutString");
            this.dgNPLHuy.DesignTimeLayout = dgNPLHuy_DesignTimeLayout;
            this.dgNPLHuy.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgNPLHuy.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLHuy.FrozenColumns = 5;
            this.dgNPLHuy.GroupByBoxVisible = false;
            this.dgNPLHuy.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLHuy.Location = new System.Drawing.Point(0, 0);
            this.dgNPLHuy.Name = "dgNPLHuy";
            this.dgNPLHuy.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLHuy.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLHuy.Size = new System.Drawing.Size(668, 418);
            this.dgNPLHuy.TabIndex = 4;
            this.dgNPLHuy.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLHuy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNPLHuy.VisualStyleManager = this.vsmMain;
            // 
            // btnChuyenNPLHuy
            // 
            this.btnChuyenNPLHuy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChuyenNPLHuy.Location = new System.Drawing.Point(674, 3);
            this.btnChuyenNPLHuy.Name = "btnChuyenNPLHuy";
            this.btnChuyenNPLHuy.Size = new System.Drawing.Size(97, 39);
            this.btnChuyenNPLHuy.TabIndex = 7;
            this.btnChuyenNPLHuy.Text = "Đăng ký NPL Hủy";
            this.btnChuyenNPLHuy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChuyenNPLHuy.Click += new System.EventHandler(this.btnChuyenNPLHuy_Click);
            // 
            // mnnGrid_NPLHuy
            // 
            this.mnnGrid_NPLHuy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatEX_NPLHuy});
            this.mnnGrid_NPLHuy.Name = "mnuGrid";
            this.mnnGrid_NPLHuy.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatEX_NPLHuy
            // 
            this.toolStripXuatEX_NPLHuy.Name = "toolStripXuatEX_NPLHuy";
            this.toolStripXuatEX_NPLHuy.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatEX_NPLHuy.Text = "Xuất excel";
            this.toolStripXuatEX_NPLHuy.Click += new System.EventHandler(this.toolStripXuatEX_NPLHuy_Click);
            // 
            // XuLyThanhKhoan_BC06_TT38
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1060, 519);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "XuLyThanhKhoan_BC06_TT38";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Xử lý báo cáo 06";
            this.Load += new System.EventHandler(this.XuatNhapTonForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDinhMuc_SanPham)).EndInit();
            this.mnuGridDM.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCU)).EndInit();
            this.mnnGridNPL_CU.ResumeLayout(false);
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLHuy)).EndInit();
            this.mnnGrid_NPLHuy.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem toolStripXuatExcelNPL;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnTimKiemNPL;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private Janus.Windows.EditControls.UIButton btnInBaoCao;
        private System.Windows.Forms.ProgressBar progressBar1;
        private Janus.Windows.EditControls.UIButton btnBCTong;
        private Janus.Windows.EditControls.UIButton btnXuatExcel;
        private Janus.Windows.EditControls.UIButton btnXuLyXNT_Mau15;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnLayDuLieu;
        private System.Windows.Forms.Label lblPercen;
        private Janus.Windows.EditControls.UIButton btnDong;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblXuLy;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX dgDinhMuc_SanPham;
        private System.Windows.Forms.ContextMenuStrip mnuGridDM;
        private System.Windows.Forms.ToolStripMenuItem toolStripXuatExcelDM;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgNPLCU;
        private System.Windows.Forms.ContextMenuStrip mnnGridNPL_CU;
        private System.Windows.Forms.ToolStripMenuItem toolStripXuatEXNPL_CU;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgNPLHuy;
        private System.Windows.Forms.ContextMenuStrip mnnGrid_NPLHuy;
        private System.Windows.Forms.ToolStripMenuItem toolStripXuatEX_NPLHuy;
        private Janus.Windows.EditControls.UIButton btnChuyenNPLHuy;
        private Janus.Windows.EditControls.UIButton btnChuyenNPLCungUng;

    }
}