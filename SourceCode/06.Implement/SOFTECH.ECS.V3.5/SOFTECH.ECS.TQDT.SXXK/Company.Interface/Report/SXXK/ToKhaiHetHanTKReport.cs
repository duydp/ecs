﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiHetHanTKReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable dt = new DataTable();
        private int STT = 0;
        public ToKhaiHetHanTKReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            this.dt.TableName = "ToKhaiSapHH";
            this.DataSource = this.dt;
     
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblLuongNPLNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Luong", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ton", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblThueNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueXNK" ,"{0:n0}");
            lblThueTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueTon", "{0:n0}");
            lbSoHD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHopDong");
        }

        private void ToKhaiHetHanTKReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel1.Text += " " + this.STT + " tờ khai";
        }

        private void lblToKhai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string MaloaiHinh = string.Empty;
            if (GetCurrentColumnValue("MaLoaiHinh").ToString().Contains("V"))
                MaloaiHinh = GetCurrentColumnValue("MaLoaiHinh").ToString().Substring(2, 3);
            else
                MaloaiHinh = GetCurrentColumnValue("MaLoaiHinh").ToString();
            lblToKhai.Text = " Tờ khai số " + GetCurrentColumnValue("SoToKhaiVNACCS").ToString() + " / " + MaloaiHinh + " / " + GetCurrentColumnValue("NamDangKy") + " / " + GetCurrentColumnValue("SoNgay").ToString();
        }


    }
}
