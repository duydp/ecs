﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class PDFCompressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PDFCompressForm));
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnOpenFolder = new Janus.Windows.EditControls.UIButton();
            this.btnCompression = new Janus.Windows.EditControls.UIButton();
            this.nmrQuantity = new Janus.Windows.GridEX.EditControls.IntegerUpDown();
            this.chkRemoveFormFields = new Janus.Windows.EditControls.UICheckBox();
            this.chkIncrementalUpdate = new Janus.Windows.EditControls.UICheckBox();
            this.chkRemoveMetadata = new Janus.Windows.EditControls.UICheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkRemoveAnnotation = new Janus.Windows.EditControls.UICheckBox();
            this.chkOptimizeFont = new Janus.Windows.EditControls.UICheckBox();
            this.chkCompressPageContents = new Janus.Windows.EditControls.UICheckBox();
            this.chkCompressImage = new Janus.Windows.EditControls.UICheckBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtFolderCompress = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSelectFile = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFileSizeAfter = new System.Windows.Forms.Label();
            this.lblFileSizeBefore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(812, 485);
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbMain.VisualStyleManager = null;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnOpenFolder);
            this.uiGroupBox6.Controls.Add(this.btnCompression);
            this.uiGroupBox6.Controls.Add(this.nmrQuantity);
            this.uiGroupBox6.Controls.Add(this.chkRemoveFormFields);
            this.uiGroupBox6.Controls.Add(this.chkIncrementalUpdate);
            this.uiGroupBox6.Controls.Add(this.chkRemoveMetadata);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.chkRemoveAnnotation);
            this.uiGroupBox6.Controls.Add(this.chkOptimizeFont);
            this.uiGroupBox6.Controls.Add(this.chkCompressPageContents);
            this.uiGroupBox6.Controls.Add(this.chkCompressImage);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 95);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(812, 226);
            this.uiGroupBox6.TabIndex = 7;
            this.uiGroupBox6.Text = "Tùy chọn";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFolder.Image")));
            this.btnOpenFolder.ImageSize = new System.Drawing.Size(20, 20);
            this.btnOpenFolder.Location = new System.Drawing.Point(334, 186);
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(226, 22);
            this.btnOpenFolder.TabIndex = 8;
            this.btnOpenFolder.Text = "Mở thư mục chứa File PDF sau khi nén";
            this.btnOpenFolder.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // btnCompression
            // 
            this.btnCompression.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompression.Image = ((System.Drawing.Image)(resources.GetObject("btnCompression.Image")));
            this.btnCompression.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCompression.Location = new System.Drawing.Point(253, 186);
            this.btnCompression.Name = "btnCompression";
            this.btnCompression.Size = new System.Drawing.Size(75, 22);
            this.btnCompression.TabIndex = 7;
            this.btnCompression.Text = "Nén";
            this.btnCompression.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCompression.Click += new System.EventHandler(this.btnCompression_Click);
            // 
            // nmrQuantity
            // 
            this.nmrQuantity.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrQuantity.Location = new System.Drawing.Point(379, 27);
            this.nmrQuantity.Name = "nmrQuantity";
            this.nmrQuantity.Size = new System.Drawing.Size(100, 22);
            this.nmrQuantity.TabIndex = 3;
            this.nmrQuantity.Value = 50;
            this.nmrQuantity.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkRemoveFormFields
            // 
            this.chkRemoveFormFields.AutoSize = true;
            this.chkRemoveFormFields.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoveFormFields.Location = new System.Drawing.Point(253, 139);
            this.chkRemoveFormFields.Name = "chkRemoveFormFields";
            this.chkRemoveFormFields.Size = new System.Drawing.Size(155, 19);
            this.chkRemoveFormFields.TabIndex = 1;
            this.chkRemoveFormFields.Text = "Xóa các trường biểu mẫu";
            // 
            // chkIncrementalUpdate
            // 
            this.chkIncrementalUpdate.AutoSize = true;
            this.chkIncrementalUpdate.Checked = true;
            this.chkIncrementalUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncrementalUpdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncrementalUpdate.Location = new System.Drawing.Point(253, 100);
            this.chkIncrementalUpdate.Name = "chkIncrementalUpdate";
            this.chkIncrementalUpdate.Size = new System.Drawing.Size(184, 19);
            this.chkIncrementalUpdate.TabIndex = 1;
            this.chkIncrementalUpdate.Text = "Vô hiệu hóa Cập nhật gia tăng";
            // 
            // chkRemoveMetadata
            // 
            this.chkRemoveMetadata.AutoSize = true;
            this.chkRemoveMetadata.Checked = true;
            this.chkRemoveMetadata.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRemoveMetadata.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoveMetadata.Location = new System.Drawing.Point(253, 63);
            this.chkRemoveMetadata.Name = "chkRemoveMetadata";
            this.chkRemoveMetadata.Size = new System.Drawing.Size(158, 19);
            this.chkRemoveMetadata.TabIndex = 1;
            this.chkRemoveMetadata.Text = "Xóa thông tin siêu dữ liệu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(250, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Chất lượng hình ảnh : ";
            // 
            // chkRemoveAnnotation
            // 
            this.chkRemoveAnnotation.AutoSize = true;
            this.chkRemoveAnnotation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoveAnnotation.Location = new System.Drawing.Point(15, 139);
            this.chkRemoveAnnotation.Name = "chkRemoveAnnotation";
            this.chkRemoveAnnotation.Size = new System.Drawing.Size(92, 19);
            this.chkRemoveAnnotation.TabIndex = 1;
            this.chkRemoveAnnotation.Text = "Xóa chú thích";
            // 
            // chkOptimizeFont
            // 
            this.chkOptimizeFont.AutoSize = true;
            this.chkOptimizeFont.Checked = true;
            this.chkOptimizeFont.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOptimizeFont.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOptimizeFont.Location = new System.Drawing.Point(15, 100);
            this.chkOptimizeFont.Name = "chkOptimizeFont";
            this.chkOptimizeFont.Size = new System.Drawing.Size(141, 19);
            this.chkOptimizeFont.TabIndex = 1;
            this.chkOptimizeFont.Text = "Tối ưu hóa phông chữ";
            // 
            // chkCompressPageContents
            // 
            this.chkCompressPageContents.AutoSize = true;
            this.chkCompressPageContents.Checked = true;
            this.chkCompressPageContents.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCompressPageContents.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCompressPageContents.Location = new System.Drawing.Point(15, 63);
            this.chkCompressPageContents.Name = "chkCompressPageContents";
            this.chkCompressPageContents.Size = new System.Drawing.Size(162, 19);
            this.chkCompressPageContents.TabIndex = 1;
            this.chkCompressPageContents.Text = "Tối ưu hóa nội dung trang";
            // 
            // chkCompressImage
            // 
            this.chkCompressImage.AutoSize = true;
            this.chkCompressImage.Checked = true;
            this.chkCompressImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCompressImage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCompressImage.Location = new System.Drawing.Point(15, 30);
            this.chkCompressImage.Name = "chkCompressImage";
            this.chkCompressImage.Size = new System.Drawing.Size(90, 19);
            this.chkCompressImage.TabIndex = 1;
            this.chkCompressImage.Text = "Nén hình ảnh";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtFolderCompress);
            this.uiGroupBox3.Controls.Add(this.txtSelectFile);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(812, 95);
            this.uiGroupBox3.TabIndex = 6;
            this.uiGroupBox3.Text = "Cấu hình";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtFolderCompress
            // 
            this.txtFolderCompress.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFolderCompress.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderCompress.Location = new System.Drawing.Point(253, 55);
            this.txtFolderCompress.Name = "txtFolderCompress";
            this.txtFolderCompress.Size = new System.Drawing.Size(404, 22);
            this.txtFolderCompress.TabIndex = 3;
            this.txtFolderCompress.VisualStyleManager = this.vsmMain;
            this.txtFolderCompress.ButtonClick += new System.EventHandler(this.txtFolderCompress_ButtonClick);
            // 
            // txtSelectFile
            // 
            this.txtSelectFile.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSelectFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSelectFile.Location = new System.Drawing.Point(253, 23);
            this.txtSelectFile.Name = "txtSelectFile";
            this.txtSelectFile.Size = new System.Drawing.Size(404, 22);
            this.txtSelectFile.TabIndex = 1;
            this.txtSelectFile.VisualStyleManager = this.vsmMain;
            this.txtSelectFile.ButtonClick += new System.EventHandler(this.txtSelectFile_ButtonClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(230, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Chọn thư mục chứa File PDF sau khi Nén";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chọn File PDF cần nén";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.lblFileSizeAfter);
            this.uiGroupBox1.Controls.Add(this.lblFileSizeBefore);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 321);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(812, 164);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.Text = "Kết quả";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Kích thước tài liệu nén :";
            // 
            // lblFileSizeAfter
            // 
            this.lblFileSizeAfter.AutoSize = true;
            this.lblFileSizeAfter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileSizeAfter.ForeColor = System.Drawing.Color.Blue;
            this.lblFileSizeAfter.Location = new System.Drawing.Point(166, 76);
            this.lblFileSizeAfter.Name = "lblFileSizeAfter";
            this.lblFileSizeAfter.Size = new System.Drawing.Size(29, 14);
            this.lblFileSizeAfter.TabIndex = 0;
            this.lblFileSizeAfter.Text = "{0}";
            // 
            // lblFileSizeBefore
            // 
            this.lblFileSizeBefore.AutoSize = true;
            this.lblFileSizeBefore.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileSizeBefore.ForeColor = System.Drawing.Color.Blue;
            this.lblFileSizeBefore.Location = new System.Drawing.Point(166, 37);
            this.lblFileSizeBefore.Name = "lblFileSizeBefore";
            this.lblFileSizeBefore.Size = new System.Drawing.Size(29, 14);
            this.lblFileSizeBefore.TabIndex = 0;
            this.lblFileSizeBefore.Text = "{0}";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kích thước tài liệu gốc :";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PDFCompressForm
            // 
            this.ClientSize = new System.Drawing.Size(812, 485);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PDFCompressForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Nén File PDF";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UICheckBox chkCompressImage;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtFolderCompress;
        private Janus.Windows.GridEX.EditControls.EditBox txtSelectFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UICheckBox chkRemoveFormFields;
        private Janus.Windows.EditControls.UICheckBox chkIncrementalUpdate;
        private Janus.Windows.EditControls.UICheckBox chkRemoveMetadata;
        private Janus.Windows.EditControls.UICheckBox chkRemoveAnnotation;
        private Janus.Windows.EditControls.UICheckBox chkOptimizeFont;
        private Janus.Windows.EditControls.UICheckBox chkCompressPageContents;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.IntegerUpDown nmrQuantity;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private System.Windows.Forms.Label lblFileSizeAfter;
        private System.Windows.Forms.Label lblFileSizeBefore;
        private Janus.Windows.EditControls.UIButton btnOpenFolder;
        private Janus.Windows.EditControls.UIButton btnCompression;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
