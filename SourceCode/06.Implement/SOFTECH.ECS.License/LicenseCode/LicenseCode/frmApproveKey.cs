﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeyServer;
using KeyServer.User;
using KeySecurity;

namespace LicenseCode
{
    public partial class frmApproveKey : Form
    {
        public frmApproveKey()
        {
            InitializeComponent();

        }
        public List<Key> listKey = new List<Key>();
        private ProductKey.ProductKey productServices = new ProductKey.ProductKey();
        public User User = new User();


        private void frmApproveKey_Load(object sender, EventArgs e)
        {
            productServices.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            productServices.Timeout = 60000;
            string result;
            try
            {
                result = productServices.SeachKey(User.UserName, KeyCode.ConvertToBase64(User.PassWord), "-1", false, new DateTime(1900,1,1), DateTime.MaxValue, string.Empty);
                if (string.IsNullOrEmpty(result))
                    return;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Lỗi kết nối" + ex.Message);
                return;
            }
            try
            {
                listKey = new List<Key>();
                listKey = KeyCode.Deserialize<List<Key>>(KeyCode.ConvertFromBase64(result));
                LoadListView();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Lỗi " + result);
            }
        }
        private bool GroupExit(ListViewGroup lvgroup)
        {
            foreach (ListViewGroup item in listView1.Groups)
            {
                if (item.Header == lvgroup.Header)
                    return true;
            }
            return false;
        }
        private void LoadListView()
        {
            try
            {
                listView1.Items.Clear();
                foreach (Key item in listKey)
                {
                    ListViewItem lvitem = new ListViewItem();
                    ListViewGroup lvgroup = new ListViewGroup("lvgRequestedDate", "Ngày gửi yêu cầu: " + item.RequestedDate.ToString("dd/MM/yyyy"));
                    if (!GroupExit(lvgroup))
                    {
                        listView1.Groups.Add(lvgroup);
                        lvitem.Group = lvgroup;
                    }
                    else
                        lvitem.Group = listView1.Groups[listView1.Groups.Count - 1];
                    lvitem.SubItems[0].Text = item.RequestedBy;
                    lvitem.SubItems.Add(item.ProductId);
                    lvitem.SubItems.Add(item.SerialNumber);
                    lvitem.SubItems.Add(item.Days.ToString());
                    lvitem.SubItems.Add(item.CustomerCode);
                    lvitem.SubItems.Add(item.CustomerName);
                    lvitem.SubItems.Add(item.CustomerDescription);
                    lvitem.SubItems.Add(item.RequestedDate.ToString("dd/MM/yyyy"));
                    listView1.Items.Add(lvitem);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
             
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string result = string.Empty;
            try
            {
                if (e.ClickedItem.Name.Equals("Approve"))
                {
                    //MessageBox.Show(e.ClickedItem.Text);//Write your copy code here  
                    string serial = listView1.SelectedItems[0].SubItems[2].Text;
                    result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, true);
                    if (string.IsNullOrEmpty(result))
                        MessageBox.Show("Đã chấp nhận");
                    else
                        MessageBox.Show("Lỗi xảy ra khi thực hiện: " + result);
                }
                else if (e.ClickedItem.Name.Equals("DisApprove"))
                {
                    string serial = listView1.SelectedItems[0].SubItems[2].Text;
                    result = productServices.ApproveKeyLicense(User.UserName, KeyCode.ConvertToBase64(User.PassWord), serial, false);
                    if (string.IsNullOrEmpty(result))
                        MessageBox.Show("Đã từ chối");
                    else
                        MessageBox.Show("Lỗi xảy ra khi thực hiện: " + result);
                }
                else
                    return;
                if (string.IsNullOrEmpty(result))
                    listView1.Items.Remove(listView1.SelectedItems[0]);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show();
            }  
        }


    }
}
