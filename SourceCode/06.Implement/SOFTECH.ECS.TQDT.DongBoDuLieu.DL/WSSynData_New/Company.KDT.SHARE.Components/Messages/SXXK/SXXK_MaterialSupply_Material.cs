﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_MaterialSupply_Material
    {

        /// <summary>
        // Nhóm lý do giải trình - 1: TCU, 9: Loại khác
        /// </summary>
        [XmlElement("group")]
        public string Group { get; set; }

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Tên Sản phẩm
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }
        /// <summary>
        /// Giải trình
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }

        /// <summary>
        ///
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }


        /// <summary>
        ///
        /// </summary>
        [XmlElement("DetailMaterial")]
        public SXXK_MaterialSupply_DetailMaterial DetailMaterial { get; set; }
    }
}
