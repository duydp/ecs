﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_THONGBAO_PHI : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TB_ID { set; get; }
		public string MaKhoanMuc { set; get; }
		public string TenKhoanMuc { set; get; }
		public string SoVanDonOrContainer { set; get; }
		public string MaDVT { set; get; }
		public string TenDVT { set; get; }
		public decimal SoLuongOrTrongLuong { set; get; }
		public decimal DonGia { set; get; }
		public decimal ThanhTien { set; get; }
		public string DienGiai { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_THONGBAO_PHI> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_THONGBAO_PHI> collection = new List<T_KDT_THUPHI_THONGBAO_PHI>();
			while (reader.Read())
			{
				T_KDT_THUPHI_THONGBAO_PHI entity = new T_KDT_THUPHI_THONGBAO_PHI();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TB_ID"))) entity.TB_ID = reader.GetInt64(reader.GetOrdinal("TB_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhoanMuc"))) entity.MaKhoanMuc = reader.GetString(reader.GetOrdinal("MaKhoanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoanMuc"))) entity.TenKhoanMuc = reader.GetString(reader.GetOrdinal("TenKhoanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDonOrContainer"))) entity.SoVanDonOrContainer = reader.GetString(reader.GetOrdinal("SoVanDonOrContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT"))) entity.MaDVT = reader.GetString(reader.GetOrdinal("MaDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDVT"))) entity.TenDVT = reader.GetString(reader.GetOrdinal("TenDVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongOrTrongLuong"))) entity.SoLuongOrTrongLuong = reader.GetDecimal(reader.GetOrdinal("SoLuongOrTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhTien"))) entity.ThanhTien = reader.GetDecimal(reader.GetOrdinal("ThanhTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DienGiai"))) entity.DienGiai = reader.GetString(reader.GetOrdinal("DienGiai"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_THONGBAO_PHI> collection, long id)
        {
            foreach (T_KDT_THUPHI_THONGBAO_PHI item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_THONGBAO_PHI VALUES(@TB_ID, @MaKhoanMuc, @TenKhoanMuc, @SoVanDonOrContainer, @MaDVT, @TenDVT, @SoLuongOrTrongLuong, @DonGia, @ThanhTien, @DienGiai)";
            string update = "UPDATE T_KDT_THUPHI_THONGBAO_PHI SET TB_ID = @TB_ID, MaKhoanMuc = @MaKhoanMuc, TenKhoanMuc = @TenKhoanMuc, SoVanDonOrContainer = @SoVanDonOrContainer, MaDVT = @MaDVT, TenDVT = @TenDVT, SoLuongOrTrongLuong = @SoLuongOrTrongLuong, DonGia = @DonGia, ThanhTien = @ThanhTien, DienGiai = @DienGiai WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_THONGBAO_PHI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TB_ID", SqlDbType.BigInt, "TB_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, "SoVanDonOrContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TB_ID", SqlDbType.BigInt, "TB_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, "SoVanDonOrContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_THONGBAO_PHI VALUES(@TB_ID, @MaKhoanMuc, @TenKhoanMuc, @SoVanDonOrContainer, @MaDVT, @TenDVT, @SoLuongOrTrongLuong, @DonGia, @ThanhTien, @DienGiai)";
            string update = "UPDATE T_KDT_THUPHI_THONGBAO_PHI SET TB_ID = @TB_ID, MaKhoanMuc = @MaKhoanMuc, TenKhoanMuc = @TenKhoanMuc, SoVanDonOrContainer = @SoVanDonOrContainer, MaDVT = @MaDVT, TenDVT = @TenDVT, SoLuongOrTrongLuong = @SoLuongOrTrongLuong, DonGia = @DonGia, ThanhTien = @ThanhTien, DienGiai = @DienGiai WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_THONGBAO_PHI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TB_ID", SqlDbType.BigInt, "TB_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, "SoVanDonOrContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TB_ID", SqlDbType.BigInt, "TB_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMuc", SqlDbType.NVarChar, "MaKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMuc", SqlDbType.NVarChar, "TenKhoanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, "SoVanDonOrContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.NVarChar, "MaDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDVT", SqlDbType.NVarChar, "TenDVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, "SoLuongOrTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGia", SqlDbType.Decimal, "DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_THONGBAO_PHI Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_THONGBAO_PHI> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_THONGBAO_PHI> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_THONGBAO_PHI> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KDT_THUPHI_THONGBAO_PHI> SelectCollectionBy_TB_ID(long tB_ID)
		{
            IDataReader reader = SelectReaderBy_TB_ID(tB_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TB_ID(long tB_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, tB_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TB_ID(long tB_ID)
		{
			const string spName = "p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, tB_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_THONGBAO_PHI(long tB_ID, string maKhoanMuc, string tenKhoanMuc, string soVanDonOrContainer, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai)
		{
			T_KDT_THUPHI_THONGBAO_PHI entity = new T_KDT_THUPHI_THONGBAO_PHI();	
			entity.TB_ID = tB_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.SoVanDonOrContainer = soVanDonOrContainer;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, TB_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, SoVanDonOrContainer);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_THONGBAO_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO_PHI item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_THONGBAO_PHI(long id, long tB_ID, string maKhoanMuc, string tenKhoanMuc, string soVanDonOrContainer, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai)
		{
			T_KDT_THUPHI_THONGBAO_PHI entity = new T_KDT_THUPHI_THONGBAO_PHI();			
			entity.ID = id;
			entity.TB_ID = tB_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.SoVanDonOrContainer = soVanDonOrContainer;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_THONGBAO_PHI_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, TB_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, SoVanDonOrContainer);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_THONGBAO_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO_PHI item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_THONGBAO_PHI(long id, long tB_ID, string maKhoanMuc, string tenKhoanMuc, string soVanDonOrContainer, string maDVT, string tenDVT, decimal soLuongOrTrongLuong, decimal donGia, decimal thanhTien, string dienGiai)
		{
			T_KDT_THUPHI_THONGBAO_PHI entity = new T_KDT_THUPHI_THONGBAO_PHI();			
			entity.ID = id;
			entity.TB_ID = tB_ID;
			entity.MaKhoanMuc = maKhoanMuc;
			entity.TenKhoanMuc = tenKhoanMuc;
			entity.SoVanDonOrContainer = soVanDonOrContainer;
			entity.MaDVT = maDVT;
			entity.TenDVT = tenDVT;
			entity.SoLuongOrTrongLuong = soLuongOrTrongLuong;
			entity.DonGia = donGia;
			entity.ThanhTien = thanhTien;
			entity.DienGiai = dienGiai;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, TB_ID);
			db.AddInParameter(dbCommand, "@MaKhoanMuc", SqlDbType.NVarChar, MaKhoanMuc);
			db.AddInParameter(dbCommand, "@TenKhoanMuc", SqlDbType.NVarChar, TenKhoanMuc);
			db.AddInParameter(dbCommand, "@SoVanDonOrContainer", SqlDbType.NVarChar, SoVanDonOrContainer);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.NVarChar, MaDVT);
			db.AddInParameter(dbCommand, "@TenDVT", SqlDbType.NVarChar, TenDVT);
			db.AddInParameter(dbCommand, "@SoLuongOrTrongLuong", SqlDbType.Decimal, SoLuongOrTrongLuong);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_THONGBAO_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO_PHI item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_THONGBAO_PHI(long id)
		{
			T_KDT_THUPHI_THONGBAO_PHI entity = new T_KDT_THUPHI_THONGBAO_PHI();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TB_ID(long tB_ID)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteBy_TB_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TB_ID", SqlDbType.BigInt, tB_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_THONGBAO_PHI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_THONGBAO_PHI item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}