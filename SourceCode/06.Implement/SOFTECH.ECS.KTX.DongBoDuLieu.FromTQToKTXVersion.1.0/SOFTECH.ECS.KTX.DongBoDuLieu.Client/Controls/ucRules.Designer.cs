namespace DongBo.Controls
{
    partial class ucRules
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkAgree2 = new System.Windows.Forms.CheckBox();
            this.chkAgree1 = new System.Windows.Forms.CheckBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.chkAgree2);
            this.pnlBottom.Controls.Add(this.chkAgree1);
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.richTextBox1);
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(190, 22);
            this.lblCaption.Text = "Quy định của Softech";
            // 
            // chkAgree2
            // 
            this.chkAgree2.AutoSize = true;
            this.chkAgree2.Checked = true;
            this.chkAgree2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAgree2.Location = new System.Drawing.Point(6, 28);
            this.chkAgree2.Name = "chkAgree2";
            this.chkAgree2.Size = new System.Drawing.Size(304, 17);
            this.chkAgree2.TabIndex = 4;
            this.chkAgree2.Text = "Hãy chắc chắn bạn đồng ý tuân theo những quy định trên";
            this.chkAgree2.UseVisualStyleBackColor = true;
            this.chkAgree2.CheckedChanged += new System.EventHandler(this.chkAgree2_CheckedChanged);
            // 
            // chkAgree1
            // 
            this.chkAgree1.AutoSize = true;
            this.chkAgree1.Checked = true;
            this.chkAgree1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAgree1.Location = new System.Drawing.Point(6, 4);
            this.chkAgree1.Name = "chkAgree1";
            this.chkAgree1.Size = new System.Drawing.Size(292, 17);
            this.chkAgree1.TabIndex = 3;
            this.chkAgree1.Text = "Hãy chắc chắn rằng bạn đã đọc kỹ những quy định trên";
            this.chkAgree1.UseVisualStyleBackColor = true;
            this.chkAgree1.CheckedChanged += new System.EventHandler(this.chkAgree1_CheckedChanged);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(5, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(478, 265);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // ucRules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucRules";
            this.Load += new System.EventHandler(this.ucRules_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkAgree2;
        private System.Windows.Forms.CheckBox chkAgree1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}
