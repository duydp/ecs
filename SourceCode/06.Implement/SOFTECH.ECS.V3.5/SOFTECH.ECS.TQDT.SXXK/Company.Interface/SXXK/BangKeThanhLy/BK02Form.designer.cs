namespace Company.Interface.SXXK.BangKe
{
    partial class BK02Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKXKhongTheTK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKXTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK02Form));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoTo = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhaiTL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGoToTL = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnImportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTabTKX = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageChuaTK = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.uiTabPageKhongTheTK = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKXKhongTheTK = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKXTL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabTKX)).BeginInit();
            this.uiTabTKX.SuspendLayout();
            this.uiTabPageChuaTK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.uiTabPageKhongTheTK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXKhongTheTK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1223, 648);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "pictureBox1.Image.png");
            this.ImageList1.Images.SetKeyName(5, "pictureBox2.Image.png");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(6, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Danh sách tờ khai xuất chưa thanh khoản";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(514, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(286, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "Danh sách tờ khai xuất đưa vào thanh khoản";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(128, 12);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(150, 21);
            this.txtSoToKhai.TabIndex = 1;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = ((ulong)(0ul));
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnGoTo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // btnGoTo
            // 
            this.btnGoTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoTo.Image = ((System.Drawing.Image)(resources.GetObject("btnGoTo.Image")));
            this.btnGoTo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGoTo.Location = new System.Drawing.Point(284, 11);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(89, 23);
            this.btnGoTo.TabIndex = 2;
            this.btnGoTo.Text = "Tìm";
            this.btnGoTo.VisualStyleManager = this.vsmMain;
            this.btnGoTo.Click += new System.EventHandler(this.btnGoTo_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(25, 25);
            this.btnAdd.Location = new System.Drawing.Point(6, 202);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(25, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtSoToKhaiTL
            // 
            this.txtSoToKhaiTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiTL.Location = new System.Drawing.Point(130, 14);
            this.txtSoToKhaiTL.Name = "txtSoToKhaiTL";
            this.txtSoToKhaiTL.Size = new System.Drawing.Size(153, 21);
            this.txtSoToKhaiTL.TabIndex = 1;
            this.txtSoToKhaiTL.Text = "0";
            this.txtSoToKhaiTL.Value = ((ulong)(0ul));
            this.txtSoToKhaiTL.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoToKhaiTL.VisualStyleManager = this.vsmMain;
            this.txtSoToKhaiTL.TextChanged += new System.EventHandler(this.btnGoToTL_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(8, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm đến tờ khai số:";
            // 
            // btnGoToTL
            // 
            this.btnGoToTL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoToTL.Image = ((System.Drawing.Image)(resources.GetObject("btnGoToTL.Image")));
            this.btnGoToTL.Location = new System.Drawing.Point(293, 14);
            this.btnGoToTL.Name = "btnGoToTL";
            this.btnGoToTL.Size = new System.Drawing.Size(89, 23);
            this.btnGoToTL.TabIndex = 2;
            this.btnGoToTL.Text = "Tìm";
            this.btnGoToTL.VisualStyleManager = this.vsmMain;
            this.btnGoToTL.Click += new System.EventHandler(this.btnGoToTL_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.ImageSize = new System.Drawing.Size(20, 20);
            this.cmdSave.Location = new System.Drawing.Point(537, 18);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(107, 23);
            this.cmdSave.TabIndex = 5;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnImportExcel);
            this.uiGroupBox4.Controls.Add(this.btnExportExcel);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.cmdSave);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1223, 54);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnImportExcel.Image")));
            this.btnImportExcel.ImageList = this.ImageList1;
            this.btnImportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnImportExcel.Location = new System.Drawing.Point(745, 18);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(89, 23);
            this.btnImportExcel.TabIndex = 9;
            this.btnImportExcel.Text = "Nhập Exel";
            this.btnImportExcel.VisualStyleManager = this.vsmMain;
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageList = this.ImageList1;
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(650, 18);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(89, 23);
            this.btnExportExcel.TabIndex = 8;
            this.btnExportExcel.Text = "Xuất Exel";
            this.btnExportExcel.VisualStyleManager = this.vsmMain;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(438, 18);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            this.ccToDate.CustomFormat = "dd/MM/yyyy";
            this.ccToDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(328, 20);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(99, 21);
            this.ccToDate.TabIndex = 3;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            this.ccToDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(270, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            this.ccFromDate.CustomFormat = "dd/MM/yyyy";
            this.ccFromDate.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(149, 18);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(99, 21);
            this.ccFromDate.TabIndex = 1;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            this.ccFromDate.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(6, 231);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(25, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 54);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1223, 33);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox6.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 87);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(582, 561);
            this.uiGroupBox6.TabIndex = 10;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiTabTKX);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 50);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(576, 508);
            this.uiGroupBox2.TabIndex = 10;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiTabTKX
            // 
            this.uiTabTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTabTKX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTabTKX.Location = new System.Drawing.Point(3, 8);
            this.uiTabTKX.Name = "uiTabTKX";
            this.uiTabTKX.Size = new System.Drawing.Size(570, 497);
            this.uiTabTKX.TabIndex = 4;
            this.uiTabTKX.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageChuaTK,
            this.uiTabPageKhongTheTK});
            this.uiTabTKX.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageChuaTK
            // 
            this.uiTabPageChuaTK.Controls.Add(this.dgTKX);
            this.uiTabPageChuaTK.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageChuaTK.Name = "uiTabPageChuaTK";
            this.uiTabPageChuaTK.Size = new System.Drawing.Size(568, 475);
            this.uiTabPageChuaTK.TabStop = true;
            this.uiTabPageChuaTK.Text = "Chưa thanh khoản hết";
            // 
            // dgTKX
            // 
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.ImageList = this.ImageList1;
            this.dgTKX.Location = new System.Drawing.Point(0, 0);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RecordNavigator = true;
            this.dgTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(568, 475);
            this.dgTKX.TabIndex = 3;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKX_RowDoubleClick);
            this.dgTKX.RecordUpdated += new System.EventHandler(this.dgTKX_RecordUpdated);
            this.dgTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKX_LoadingRow);
            // 
            // uiTabPageKhongTheTK
            // 
            this.uiTabPageKhongTheTK.Controls.Add(this.dgTKXKhongTheTK);
            this.uiTabPageKhongTheTK.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageKhongTheTK.Name = "uiTabPageKhongTheTK";
            this.uiTabPageKhongTheTK.Size = new System.Drawing.Size(568, 475);
            this.uiTabPageKhongTheTK.TabStop = true;
            this.uiTabPageKhongTheTK.Text = "Không thể thanh khoản";
            // 
            // dgTKXKhongTheTK
            // 
            this.dgTKXKhongTheTK.AlternatingColors = true;
            this.dgTKXKhongTheTK.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKXKhongTheTK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKXKhongTheTK.ColumnAutoResize = true;
            dgTKXKhongTheTK_DesignTimeLayout.LayoutString = resources.GetString("dgTKXKhongTheTK_DesignTimeLayout.LayoutString");
            this.dgTKXKhongTheTK.DesignTimeLayout = dgTKXKhongTheTK_DesignTimeLayout;
            this.dgTKXKhongTheTK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKXKhongTheTK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKXKhongTheTK.GroupByBoxVisible = false;
            this.dgTKXKhongTheTK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXKhongTheTK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXKhongTheTK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKXKhongTheTK.ImageList = this.ImageList1;
            this.dgTKXKhongTheTK.Location = new System.Drawing.Point(0, 0);
            this.dgTKXKhongTheTK.Name = "dgTKXKhongTheTK";
            this.dgTKXKhongTheTK.RecordNavigator = true;
            this.dgTKXKhongTheTK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKXKhongTheTK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXKhongTheTK.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKXKhongTheTK.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXKhongTheTK.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKXKhongTheTK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKXKhongTheTK.Size = new System.Drawing.Size(568, 475);
            this.dgTKXKhongTheTK.TabIndex = 5;
            this.dgTKXKhongTheTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKXKhongTheTK.VisualStyleManager = this.vsmMain;
            this.dgTKXKhongTheTK.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKXKhongTheTK_RowDoubleClick);
            this.dgTKXKhongTheTK.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKXKhongTheTK_LoadingRow);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.btnGoTo);
            this.uiGroupBox8.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox8.Controls.Add(this.label2);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(576, 42);
            this.uiGroupBox8.TabIndex = 9;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnAdd);
            this.uiGroupBox5.Controls.Add(this.btnDelete);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(582, 87);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(40, 561);
            this.uiGroupBox5.TabIndex = 11;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(622, 87);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(601, 561);
            this.uiGroupBox7.TabIndex = 12;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKXTL);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 50);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(595, 508);
            this.uiGroupBox3.TabIndex = 10;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKXTL
            // 
            this.dgTKXTL.AlternatingColors = true;
            this.dgTKXTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKXTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKXTL.ColumnAutoResize = true;
            dgTKXTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKXTL_DesignTimeLayout.LayoutString");
            this.dgTKXTL.DesignTimeLayout = dgTKXTL_DesignTimeLayout;
            this.dgTKXTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKXTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKXTL.GroupByBoxVisible = false;
            this.dgTKXTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKXTL.ImageList = this.ImageList1;
            this.dgTKXTL.Location = new System.Drawing.Point(3, 8);
            this.dgTKXTL.Name = "dgTKXTL";
            this.dgTKXTL.RecordNavigator = true;
            this.dgTKXTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKXTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKXTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKXTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKXTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKXTL.Size = new System.Drawing.Size(589, 497);
            this.dgTKXTL.TabIndex = 3;
            this.dgTKXTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKXTL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKXTL_RowDoubleClick);
            this.dgTKXTL.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKXTL_DeletingRecords);
            this.dgTKXTL.RecordUpdated += new System.EventHandler(this.dgTKXTL_RecordUpdated);
            this.dgTKXTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKXTL_LoadingRow);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.txtSoToKhaiTL);
            this.uiGroupBox9.Controls.Add(this.btnGoToTL);
            this.uiGroupBox9.Controls.Add(this.label1);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(595, 42);
            this.uiGroupBox9.TabIndex = 9;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // BK02Form
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1223, 648);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK02Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê tờ khai xuất khẩu thanh lý";
            this.Load += new System.EventHandler(this.BK02Form_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BK02Form_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTabTKX)).EndInit();
            this.uiTabTKX.ResumeLayout(false);
            this.uiTabPageChuaTK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.uiTabPageKhongTheTK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXKhongTheTK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKXTL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnGoTo;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnImportExcel;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTL;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnGoToTL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.UI.Tab.UITab uiTabTKX;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageChuaTK;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageKhongTheTK;
        private Janus.Windows.GridEX.GridEX dgTKXKhongTheTK;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKXTL;
    }
}