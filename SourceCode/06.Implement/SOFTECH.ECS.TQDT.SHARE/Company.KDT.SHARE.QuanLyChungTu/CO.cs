﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class CO
    {
        public CO LoadCO(long iD)
        {
            const string spName = "[dbo].[p_KDT_CO_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            CO entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
            }
            reader.Close();
            return entity;
        }

        public static CO Load(string maDoanhNghiep, string soCO, DateTime ngayCO, string loaiCO, long idTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, soCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, ngayCO);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, loaiCO);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            CO entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1900 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1900 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year == 1900 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1900 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public static bool checkSoCOExit(string soCO, string MaDoanhNghiep, long TKMD_ID, long coID)
        {
            string sql = "select SoCO from t_KDT_CO where SoCO=@SoCO and MaDoanhNghiep=@MaDoanhNghiep and TKMD_ID=@TKMD_ID and ID!=" + coID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@SoCO", SqlDbType.VarChar, soCO);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbcommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            object o = db.ExecuteScalar(dbcommand);
            return o != null;
        }

        public static List<CO> SelectListCOByMaDanhNghiepAndKhacTKMD(long TKMD_ID, string MaDoanhNghiep)
        {
            List<CO> collection = new List<CO>();
            string sql = "select * from t_KDT_CO where TKMD_ID<>@TKMD_ID and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbcommand);
            while (reader.Read())
            {
                CO entity = new CO();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static XmlNode ConvertCollectionCOToXML(XmlDocument doc, List<CO> COCollection, long TKMD_ID)
        {
            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = CO.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_CO = doc.CreateElement("CHUNG_TU_CO");
            foreach (CO co in COCollection)
            {
                XmlElement CoItem = doc.CreateElement("CHUNG_TU_CO.ITEM");
                CHUNG_TU_CO.AppendChild(CoItem);

                XmlAttribute SO_CO = doc.CreateAttribute("SO_CO");
                SO_CO.Value = FontConverter.Unicode2TCVN(co.SoCO);
                CoItem.Attributes.Append(SO_CO);

                XmlAttribute NUOC_CO = doc.CreateAttribute("NUOC_CO");
                NUOC_CO.Value = co.NuocCapCO;
                CoItem.Attributes.Append(NUOC_CO);

                XmlAttribute NGAY_CAP_CO = doc.CreateAttribute("NGAY_CAP_CO");
                NGAY_CAP_CO.Value = co.NgayCO.ToString("yyyy-MM-dd");
                CoItem.Attributes.Append(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlAttribute NGUOI_KY = doc.CreateAttribute("NGUOI_KY");
                NGUOI_KY.Value = FontConverter.Unicode2TCVN(co.NguoiKy);
                CoItem.Attributes.Append(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlAttribute NOI_PHAT_HANH = doc.CreateAttribute("NOI_PHAT_HANH");
                NOI_PHAT_HANH.Value = FontConverter.Unicode2TCVN(co.ToChucCap);
                CoItem.Attributes.Append(NOI_PHAT_HANH);

                XmlAttribute MA_LOAI_CO = doc.CreateAttribute("MA_LOAI_CO");
                MA_LOAI_CO.Value = co.LoaiCO;
                CoItem.Attributes.Append(MA_LOAI_CO);

                XmlAttribute NGUOI_XUAT = doc.CreateAttribute("NGUOI_XUAT");
                NGUOI_XUAT.Value = FontConverter.Unicode2TCVN(co.TenDiaChiNguoiXK.Trim());
                CoItem.Attributes.Append(NGUOI_XUAT);

                XmlAttribute MA_NUOC_XUAT = doc.CreateAttribute("MA_NUOC_XUAT");
                MA_NUOC_XUAT.Value = co.MaNuocXKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_XUAT);

                XmlAttribute NGUOI_NHAP = doc.CreateAttribute("NGUOI_NHAP");
                NGUOI_NHAP.Value = FontConverter.Unicode2TCVN(co.TenDiaChiNguoiNK.Trim());
                CoItem.Attributes.Append(NGUOI_NHAP);

                XmlAttribute MA_NUOC_NHAP = doc.CreateAttribute("MA_NUOC_NHAP");
                MA_NUOC_NHAP.Value = co.MaNuocNKTrenCO.Trim(); ;
                CoItem.Attributes.Append(MA_NUOC_NHAP);

                XmlAttribute NOI_DUNG = doc.CreateAttribute("NOI_DUNG");
                NOI_DUNG.Value = FontConverter.Unicode2TCVN(co.ThongTinMoTaChiTiet.Trim());
                CoItem.Attributes.Append(NOI_DUNG);

                XmlAttribute NGAY_KHOI_HANH = doc.CreateAttribute("NGAY_KHOI_HANH");
                NGAY_KHOI_HANH.Value = "";
                CoItem.Attributes.Append(NGAY_KHOI_HANH);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = "";
                CoItem.Attributes.Append(GHI_CHU);

                // CHUA CO PHAI BO SUNG
                XmlAttribute NOP_SAU = doc.CreateAttribute("NOP_SAU");
                NOP_SAU.Value = co.NoCo.ToString();
                CoItem.Attributes.Append(NOP_SAU);

                // CHUA CO PHAI BO SUNG
                //XmlAttribute NO_CO = doc.CreateAttribute("NO_CO");
                //NO_CO.Value = co.NoCo.ToString();
                //CoItem.Attributes.Append(NO_CO);

                // CHUA CO PHAI BO SUNG
                XmlAttribute THOI_HAN_NOP = doc.CreateAttribute("THOI_HAN_NOP");
                if (co.NoCo == 1)
                    THOI_HAN_NOP.Value = co.ThoiHanNop.ToString("yyyy-MM-dd");
                else
                    THOI_HAN_NOP.Value = "";

                CoItem.Attributes.Append(THOI_HAN_NOP);
            }
            if (CHUNG_TU_CO.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_CO;

        }

        public string send(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.ChildNodes[0].Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.ChildNodes[0].Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<CO> listCO = new List<CO>();
            listCO.Add(this);
            nodeDulieu.AppendChild(CO.ConvertCollectionCOToXML(doc, listCO, TKMD_ID));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoBoSungCO(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GuidStr;

            this.Update();
            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<CO> listCO = new List<CO>();
            listCO.Add(this);
            xmlNodeDulieu.AppendChild(CO.ConvertCollectionCOToXML(xmlDocument, listCO, TKMD_ID));
            // Lưu ý: Nếu không thay thế sẽ bị lỗi.
            xmlDocument.InnerXml = xmlDocument.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {                
             
                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
                xmlDocument.InnerXml.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungCO);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        //XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");

                        //kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanhoiCO);
                        return true;
                    }
                }
                else
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }

            return false;

        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GuidStr = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }

                if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLaySoTiepNhanCO, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty) kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;

            }
            return false;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }
          
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQTuChoiCO, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlNodeResult.InnerText));

                    // Cập lại số tiếp nhận
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();

                    return true;
                }
                else if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQChapNhanBoSungCO, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlNodeResult.InnerText));

                    return true;
                }
                else if (xmlNodeResult.Attributes["SOTN"] != null)
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHQChapNhanBoSungCO, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlNodeResult.InnerText));
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty) kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
           
        }
        public string LayPhanHoi(string password, string maHQ)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            //if (GuidStr == "")
            //{
            //    GuidStr = (System.Guid.NewGuid().ToString()); ;
            //    nodeReference.InnerText = this.GuidStr;
            //    this.Update();
            //}

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;




            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.GuidStr;

            KDTService kdt = WebService.GetWS();

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            XmlDocument docNPL = new XmlDocument();
            for (i = 1; i <= 1; ++i)
            {
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, password);
                }
                catch
                {                    
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }

                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            return "";
        }

    }
}