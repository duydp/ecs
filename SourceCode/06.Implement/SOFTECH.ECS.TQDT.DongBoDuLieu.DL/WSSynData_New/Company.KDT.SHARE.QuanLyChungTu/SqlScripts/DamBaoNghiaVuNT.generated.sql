-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Insert]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Update]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Delete]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Load]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectAll]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_kdt_DamBaoNghiaVuNT_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Insert]
	@ID bigint,
	@TKMD_ID bigint,
	@IsDamBao bit,
	@HinhThuc nvarchar(255),
	@TriGiaDB decimal(18, 4),
	@NgayBatDau datetime,
	@NgayKetThuc datetime
AS
INSERT INTO [dbo].[t_kdt_DamBaoNghiaVuNT]
(
	[ID],
	[TKMD_ID],
	[IsDamBao],
	[HinhThuc],
	[TriGiaDB],
	[NgayBatDau],
	[NgayKetThuc]
)
VALUES
(
	@ID,
	@TKMD_ID,
	@IsDamBao,
	@HinhThuc,
	@TriGiaDB,
	@NgayBatDau,
	@NgayKetThuc
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@IsDamBao bit,
	@HinhThuc nvarchar(255),
	@TriGiaDB decimal(18, 4),
	@NgayBatDau datetime,
	@NgayKetThuc datetime
AS

UPDATE
	[dbo].[t_kdt_DamBaoNghiaVuNT]
SET
	[TKMD_ID] = @TKMD_ID,
	[IsDamBao] = @IsDamBao,
	[HinhThuc] = @HinhThuc,
	[TriGiaDB] = @TriGiaDB,
	[NgayBatDau] = @NgayBatDau,
	[NgayKetThuc] = @NgayKetThuc
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@IsDamBao bit,
	@HinhThuc nvarchar(255),
	@TriGiaDB decimal(18, 4),
	@NgayBatDau datetime,
	@NgayKetThuc datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_kdt_DamBaoNghiaVuNT] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_kdt_DamBaoNghiaVuNT] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[IsDamBao] = @IsDamBao,
			[HinhThuc] = @HinhThuc,
			[TriGiaDB] = @TriGiaDB,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_kdt_DamBaoNghiaVuNT]
	(
			[ID],
			[TKMD_ID],
			[IsDamBao],
			[HinhThuc],
			[TriGiaDB],
			[NgayBatDau],
			[NgayKetThuc]
	)
	VALUES
	(
			@ID,
			@TKMD_ID,
			@IsDamBao,
			@HinhThuc,
			@TriGiaDB,
			@NgayBatDau,
			@NgayKetThuc
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_kdt_DamBaoNghiaVuNT]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_kdt_DamBaoNghiaVuNT]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_kdt_DamBaoNghiaVuNT] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsDamBao],
	[HinhThuc],
	[TriGiaDB],
	[NgayBatDau],
	[NgayKetThuc]
FROM
	[dbo].[t_kdt_DamBaoNghiaVuNT]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsDamBao],
	[HinhThuc],
	[TriGiaDB],
	[NgayBatDau],
	[NgayKetThuc]
FROM
	[dbo].[t_kdt_DamBaoNghiaVuNT]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[IsDamBao],
	[HinhThuc],
	[TriGiaDB],
	[NgayBatDau],
	[NgayKetThuc]
FROM [dbo].[t_kdt_DamBaoNghiaVuNT] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_kdt_DamBaoNghiaVuNT_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_kdt_DamBaoNghiaVuNT_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsDamBao],
	[HinhThuc],
	[TriGiaDB],
	[NgayBatDau],
	[NgayKetThuc]
FROM
	[dbo].[t_kdt_DamBaoNghiaVuNT]	

GO

