﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamManageForm : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private SanPhamDangKyCollection spDangKyCollection = new SanPhamDangKyCollection();
        private SanPhamDangKyCollection tmpCollection = new SanPhamDangKyCollection();
        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        private readonly SanPhamDangKy currentSPDangKy = new SanPhamDangKy();
        private SanPhamDangKy spdk = new SanPhamDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;

        public SanPhamManageForm()
        {
            InitializeComponent();

        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                txtNamTiepNhan.Value = DateTime.Today.Year;
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        //-----------------------------------------------------------------------------------------
        private void SanPhamManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                cbStatus.SelectedIndex = 0;

                //An nut Xac nhan
                XacNhan.Visible = InheritableBoolean.False;
                txtNamTiepNhan.Value = DateTime.Today.Year;
                //setCommandStatus();
                setCommandStatusNew();
                khoitao_DuLieuChuan();
                if (ckbTimKiem.Checked)
                    grbtimkiem.Enabled = true;
                else
                    grbtimkiem.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (txtMaSP.Text == "")
                {
                    this.search();
                }
                else
                    this.searchSanPham(txtMaSP.Text);

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                
            }

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.spDangKyCollection = SanPhamDangKy.SelectCollectionDynamic(where, "ID desc");
                dgList.DataSource = this.spDangKyCollection;
                try
                {
                    this.tmpCollection = SanPhamDangKy.SelectCollectionDynamicAll(where, "ID desc");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                //this.setCommandStatus();
                setCommandStatusNew();
                this.currentSPDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        //Phiph-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu theo ma SP
        /// </summary>
        private void searchSanPham(string masanpham)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = " SP.Ma like '%" + masanpham + "%'";
                where += " AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.spDangKyCollection = SanPhamDangKy.SelectCollectionSanPham(where, "ID desc");
                dgList.DataSource = this.spDangKyCollection;
                try
                {
                    this.tmpCollection = SanPhamDangKy.SelectCollectionDynamicAll(where, "ID desc");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                //this.setCommandStatus();
                setCommandStatusNew();

                this.currentSPDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void setCommandStatusNew()
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET) 
            {
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSP.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
            {
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXoa.Enabled = cmdXoa1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
        }
        private void setCommandStatus()
        {
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_HUY)
            {
                InheritableBoolean cho_duyet = Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET ? InheritableBoolean.True : InheritableBoolean.False;
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = cho_duyet;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled  = InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                XacNhan.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.True;//Sua sp
                //btnSuaSP.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = false;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = false;
                cmdSend.Enabled  = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled  = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = true;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_HUY)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled =  InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                DongBoDuLieu.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = true;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
            XacNhan.Enabled = InheritableBoolean.False;
            cmdSingleDownload.Enabled = InheritableBoolean.False;
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                    switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                    {
                        case -1:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                            }
                            break;
                        case 0:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                            }
                            break;
                        case 1:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                            }
                            break;
                        case 2:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                            }
                            break;
                        case 11:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ Hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Wait for canceled";

                            }
                            break;
                        case 10:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã Hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Canceled";

                            }
                            break;
                        case -3:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                        case -2:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt sửa";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Waiting Edit";

                            }
                            break;
                        case -4:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                        case 4:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                    }


                    //TODO: Cao Hữu Tú updated:15-09-2011
                    //Contents: bổ sung cột mã sản phẩm vào Grid Danh sách sản phẩm


                    Company.BLL.KDT.SXXK.SanPhamCollection SanPhamCollection = new SanPhamCollection();
                    int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                    string TatCaMaNPL = "";

                    SanPhamCollection = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(ValueIdCell);
                    Company.BLL.KDT.SXXK.SanPham entitySanPham = new SanPham();

                    if (SanPhamCollection.Count > 0)
                    {
                        //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                        entitySanPham = SanPhamCollection[0];
                        e.Row.Cells["MaSanPham"].Text = entitySanPham.Ma;

                        //lấy mã sản phẩm bắt đầu từ phần tử thứ 2 đưa vào 
                        foreach (Company.BLL.KDT.SXXK.SanPham EntitySP in SanPhamCollection)
                        {

                            TatCaMaNPL = TatCaMaNPL + EntitySP.Ma + "\n";

                        }
                        e.Row.Cells["MaSanPham"].ToolTipText = TatCaMaNPL;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdInSP":
                    InSanPham();
                    break;
                case "cmdSuaSP":
                    this.SuaSPDaDuyet();
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null,null);
                    break;
                case "cmdUpdateStatus":
                    mnuUpdateStatus_Click(null, null);
                    break;
            }
        }

        private void InSanPham()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                SanPhamDangKy spdk = (SanPhamDangKy)dgList.GetRow().DataRow;

                if (spdk != null)
                {
                    if (spdk.SPCollection.Count == 0)
                        spdk.LoadSPCollection();

                    BLL.SXXK.SanPhamCollection SPCollectionSelect = new BLL.SXXK.SanPhamCollection();

                    foreach (SanPham item in spdk.SPCollection)
                    {
                        BLL.SXXK.SanPham NPLSelect = new BLL.SXXK.SanPham();
                        NPLSelect.Ma = item.Ma;
                        NPLSelect.Ten = item.Ten;
                        NPLSelect.DVT_ID = item.DVT_ID;
                        NPLSelect.MaHS = item.MaHS;
                        SPCollectionSelect.Add(NPLSelect);
                    }
                    Company.Interface.Report.SXXK.ReportSanPham_TT128_2013 f2 = new Company.Interface.Report.SXXK.ReportSanPham_TT128_2013();
                    if (SPCollectionSelect.Count > 0)
                    {
                        f2.SPCollection = SPCollectionSelect;
                        f2.BindReportDinhMucDaDangKy();
                        f2.ShowPreview();
                    }
                }
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SuaSPDaDuyet()
        {
            try
            {
                SanPhamDangKy spdk = new SanPhamDangKy();
                spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
                spdk.LoadSPCollection();
                if (SanPhamDangKySUA.SelectCollectionBy_IDSPDK(spdk.ID).Count == 0)
                {
                    string msg = "DOANH NGHIỆP CÓ MUỐN CHUYỂN SANG TRẠNG THÁI SỬA SẢN PHẨM KHÔNG?";
                    msg += "\n\nSỐ TIẾP NHẬN: " + spdk.SoTiepNhan.ToString();
                    msg += "\n----------------------";
                    msg += "\nCÓ " + spdk.SPCollection.Count.ToString() + " SẢN PHẨM ĐĂNG KÝ ĐÃ ĐƯỢC DUYỆT";
                    if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG",msg,true) == "Yes")
                    {
                        SanPhamSuaForm f = new SanPhamSuaForm();
                        f.SPDangKy = spdk;
                        f.masterId = spdk.ID;
                        f.spdkSUA = new SanPhamDangKySUA() { TrangThaiXuLy = -1 };
                        f.ShowDialog(this);
                    }
                }
                else
                {
                    SanPhamSuaListForm listForm = new SanPhamSuaListForm();
                    listForm.spdk = spdk;
                    listForm.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "SẢN PHẨM";
                Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = spDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = spDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    SanPhamDangKyCollection spDKColl = new SanPhamDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        spDKColl.Add((SanPhamDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO SẢN PHẨM NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < spDKColl.Count; i++)
                    {
                        if (spDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            spDKColl[i].LoadSPCollection();
                            msgAccept += "[" + (i + 1) + "]-[" + spDKColl[i].ID + "]-[" + spDKColl[i].SoTiepNhan + "]-[" + spDKColl[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(spDKColl[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (SanPhamDangKy item in spDKColl)
                        {
                            item.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            item.TransgferDataToSXXK();
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ", false);
                    btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        public void XoaSanPhamDangKy(GridEXSelectedItemCollection items)
        {

            try
            {
                if (items.Count == 0) return;
                List<SanPhamDangKy> itemsDelete = new List<SanPhamDangKy>();
                int k = 1;
                string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN XÓA DANH SÁCH KHAI BÁO SẢN PHẨM NÀY KHÔNG ?\n";
                string msgDelete = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhamDangKy npldk = (SanPhamDangKy)i.GetRow().DataRow;
                        npldk.LoadSPCollection();
                        msgDelete += "[" + (k) + "]-[" + npldk.ID + "]-[" + npldk.SoTiepNhan + "]-[" + npldk.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(npldk.TrangThaiXuLy).ToUpper() + "]\n";
                        k++;
                        itemsDelete.Add(npldk);
                    }
                }
                if (itemsDelete.Count >= 1)
                {
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgDelete + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (SanPhamDangKy item in itemsDelete)
                        {
                            foreach (SanPham itemDelete in item.SPCollection)
                            {
                                try
                                {
                                    Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                    SP.Ma = itemDelete.Ma;
                                    SP.MaHaiQuan = item.MaHaiQuan;
                                    SP.MaDoanhNghiep = item.MaDoanhNghiep;
                                    SP.Delete();
                                    itemDelete.Delete();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            item.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaSanPhamDangKy(items);
            search();
        }
        private string checkDataHangImport(SanPhamDangKy spDangky)
        {
            string st = "";
            foreach (SanPham sp in spDangky.SPCollection)
            {
                BLL.SXXK.SanPham spDaDuyet = new Company.BLL.SXXK.SanPham();
                spDaDuyet.Ma = sp.Ma;
                spDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                spDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (spDaDuyet.Load())
                {
                    //if (sp.STTHang == 1)
                    //    st = "Danh sách có ID='" + spDangky.ID + "'\n";
                    //st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    if (sp.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + spDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + spDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Product has code '" + sp.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(SanPhamDangKyCollection collection)
        {
            string st = "";
            foreach (SanPhamDangKy spDangky in collection)
            {
                SanPhamDangKy spInDatabase = SanPhamDangKy.Load(spDangky.ID);
                if (spInDatabase != null)
                {
                    if (spInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        // st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + spDangky.ID + " already approved.\n";
                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(spDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(spDangky);
                    }
                }
                else
                {
                    if (spInDatabase.ID > 0)
                        spInDatabase.ID = 0;
                    tmpCollection.Add(spDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    SanPhamDangKyCollection spDKCollection = (SanPhamDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(spDKCollection);
                    if (st != "")
                    {
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                ShowMessage("CHƯA CHỌN DANH SÁCH SẢN PHẨM", false);
                return;
            }
            try
            {
                SanPhamDangKyCollection col = new SanPhamDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                            spDangKySelected.LoadSPCollection();
                            col.Add(spDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhamSendForm f = new SanPhamSendForm();
                spdk = (SanPhamDangKy)e.Row.DataRow;
                spdk.SPCollection = SanPham.SelectCollectionBy_Master_ID(spdk.ID);
                f.spDangKy = spdk;
                f.ShowDialog(this);
                btnSearch_Click(null, null);
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        //-----------------------------------------------------------------------------------------


        private void SanPhamManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void uiContextMenu1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void btnSuaSP_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow() != null)
                {
                    this.SuaSPDaDuyet();
                }
                else
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN THÔNG TIN ĐỂ SỬA.", false);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "DANH SÁCH SẢN PHẨM_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XUẤT KÈM THEO THÔNG TIN SP KHÔNG ? ", true) == "No")
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {

                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    try
                    {
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            else
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {
                    try
                    {
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ma", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ SP" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("Ten", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN SP" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ HS" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐƠN VỊ TÍNH" });
                        dgList.DataSource = tmpCollection;
                        dgList.Refetch();

                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        dgList.Tables[0].Columns.Remove("Ma");
                        dgList.Tables[0].Columns.Remove("Ten");
                        dgList.Tables[0].Columns.Remove("MaHS");
                        dgList.Tables[0].Columns.Remove("DVT");
                        btnSearch_Click(null,null);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void mnuUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        SanPhamDangKyCollection spdkColl = new SanPhamDangKyCollection();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            spdkColl.Add((SanPhamDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < spdkColl.Count; i++)
                        {
                            spdkColl[i].LoadSPCollection();
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.spDangKy = spdkColl[i];
                            f.formType = "SP";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAutoSend_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "SP";
            f.ShowDialog(this);
        }


    }
}