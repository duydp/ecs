/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 12/12/2011 1:54:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] DROP CONSTRAINT [PK_t_HaiQuan_CuaKhau]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[NguyenPhuLieuDangKyCopy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[NguyenPhuLieuDangKyCopy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Delete
	
	SELECT     t_KDT_SXXK_NguyenPhuLieu.*
	FROM         t_KDT_SXXK_NguyenPhuLieu INNER JOIN
						  t_KDT_SXXK_NguyenPhuLieuDangKy ON t_KDT_SXXK_NguyenPhuLieu.Master_ID = t_KDT_SXXK_NguyenPhuLieuDangKy.ID
	WHERE     (t_KDT_SXXK_NguyenPhuLieuDangKy.ID = @ID)    
	
	INSERT INTO t_KDT_SXXK_NguyenPhuLieuDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_NguyenPhuLieuDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ALTER COLUMN [ID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_CuaKhau] on [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD CONSTRAINT [PK_t_HaiQuan_CuaKhau] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Insert]
	@ID char(10),
	@Ten nvarchar(50)
AS
INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
(
	[ID],
	[Ten]
)
VALUES
(
	@ID,
	@Ten
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Update]
	@ID char(10),
	@Ten nvarchar(50)
AS

UPDATE
	[dbo].[t_HaiQuan_CuaKhau]
SET
	[Ten] = @Ten
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
	@ID char(10),
	@Ten nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_CuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_CuaKhau] 
		SET
			[Ten] = @Ten
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
	(
			[ID],
			[Ten]
	)
	VALUES
	(
			@ID,
			@Ten
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Delete]
	@ID char(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Load]
	@ID char(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_CuaKhau]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceID],
	[ItemID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay],
	[XMLSend]
FROM [dbo].[t_DongBoDuLieu_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_DeleteDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_DongBoDuLieu_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_Update]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime,
	@XMLSend nvarchar(max)
AS

UPDATE
	[dbo].[t_DongBoDuLieu_KetQuaXuLy]
SET
	[ReferenceID] = @ReferenceID,
	[ItemID] = @ItemID,
	[LoaiChungTu] = @LoaiChungTu,
	[LoaiThongDiep] = @LoaiThongDiep,
	[NoiDung] = @NoiDung,
	[Ngay] = @Ngay,
	[XMLSend] = @XMLSend
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectAll]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[ItemID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay],
	[XMLSend]
FROM
	[dbo].[t_DongBoDuLieu_KetQuaXuLy]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_Load]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[ItemID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay],
	[XMLSend]
FROM
	[dbo].[t_DongBoDuLieu_KetQuaXuLy]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_InsertUpdate]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime,
	@XMLSend nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_DongBoDuLieu_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_DongBoDuLieu_KetQuaXuLy] 
		SET
			[ReferenceID] = @ReferenceID,
			[ItemID] = @ItemID,
			[LoaiChungTu] = @LoaiChungTu,
			[LoaiThongDiep] = @LoaiThongDiep,
			[NoiDung] = @NoiDung,
			[Ngay] = @Ngay,
			[XMLSend] = @XMLSend
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_DongBoDuLieu_KetQuaXuLy]
		(
			[ReferenceID],
			[ItemID],
			[LoaiChungTu],
			[LoaiThongDiep],
			[NoiDung],
			[Ngay],
			[XMLSend]
		)
		VALUES 
		(
			@ReferenceID,
			@ItemID,
			@LoaiChungTu,
			@LoaiThongDiep,
			@NoiDung,
			@Ngay,
			@XMLSend
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_Insert]
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime,
	@XMLSend nvarchar(max),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_DongBoDuLieu_KetQuaXuLy]
(
	[ReferenceID],
	[ItemID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay],
	[XMLSend]
)
VALUES 
(
	@ReferenceID,
	@ItemID,
	@LoaiChungTu,
	@LoaiThongDiep,
	@NoiDung,
	@Ngay,
	@XMLSend
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DongBoDuLieu_KetQuaXuLy_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DongBoDuLieu_KetQuaXuLy_Delete]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 31, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DongBoDuLieu_KetQuaXuLy_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_DongBoDuLieu_KetQuaXuLy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_CuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 11, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten]
FROM [dbo].[t_HaiQuan_CuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.2.7', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.2.7', getdate(), null)
	end