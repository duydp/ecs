﻿namespace Microsoft.Xml.Serialization.GeneratedAssembly
{
    using Naccs.Common.JobConfig;
    using Naccs.Core.BatchDoc;
    using Naccs.Core.Classes;
    using Naccs.Core.DataView;
    using Naccs.Core.DataView.Arrange;
    using Naccs.Core.Job;
    using Naccs.Core.Main;
    using Naccs.Core.Option;
    using Naccs.Core.Print;
    using Naccs.Core.Settings;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Xml;
    using System.Xml.Serialization;

    public class XmlSerializationWriter1 : XmlSerializationWriter
    {
        protected override void InitCallbacks()
        {
        }

        private void Write10_DefaultInfoClass(string n, string ns, DefaultInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DefaultInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DefaultInfoClass", "");
                }
                if (o.Mode)
                {
                    base.WriteElementStringRaw("Mode", "", XmlConvert.ToString(o.Mode));
                }
                if ((o.Printer != null) && (o.Printer.Length != 0))
                {
                    base.WriteElementString("Printer", "", o.Printer);
                }
                if (o.BinNo != 0)
                {
                    base.WriteElementStringRaw("BinNo", "", XmlConvert.ToString(o.BinNo));
                }
                if ((o.BinName != null) && (o.BinName.Length != 0))
                {
                    base.WriteElementString("BinName", "", o.BinName);
                }
                if (!o.SizeWarning)
                {
                    base.WriteElementStringRaw("SizeWarning", "", XmlConvert.ToString(o.SizeWarning));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write100_TermClass(string n, string ns, TermClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TermClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TermClass", "");
                }
                base.WriteAttribute("Id", "", XmlConvert.ToString(o.Id));
                base.WriteElementStringRaw("Priority", "", XmlConvert.ToString(o.Priority));
                base.WriteElementString("UserCode", "", o.UserCode);
                base.WriteElementString("JobCoode", "", o.JobCoode);
                base.WriteElementString("OutCode", "", o.OutCode);
                base.WriteElementStringRaw("UnOpen", "", XmlConvert.ToString(o.UnOpen));
                base.WriteElementString("BankNumber", "", o.BankNumber);
                base.WriteElementStringRaw("BankWithdrawday", "", XmlConvert.ToString(o.BankWithdrawday));
                base.WriteElementString("Folder", "", o.Folder);
                base.WriteEndElement(o);
            }
        }

        private void Write101_DataViewClass(string n, string ns, DataViewClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DataViewClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DataViewClass", "");
                }
                base.WriteElementString("upTime", "", o.upTime);
                if (o.AutoBackup)
                {
                    base.WriteElementStringRaw("AutoBackup", "", XmlConvert.ToString(o.AutoBackup));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write102_MessageClassify(string n, string ns, MessageClassify o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(MessageClassify)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("MessageClassify", "");
                }
                TermListClass termList = o.TermList;
                if (termList != null)
                {
                    base.WriteStartElement("TermList", "", null, false);
                    for (int i = 0; i < termList.Count; i++)
                    {
                        this.Write100_TermClass("Term", "", termList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                StringList folders = o.Folders;
                if (folders != null)
                {
                    base.WriteStartElement("Folders", "", null, false);
                    for (int j = 0; j < folders.Count; j++)
                    {
                        base.WriteNullableStringLiteral("UserFolder", "", folders[j]);
                    }
                    base.WriteEndElement();
                }
                this.Write101_DataViewClass("DataView", "", o.DataView, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write103_UserKeySet(string n, string ns, UserKeySet o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(UserKeySet)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("UserKeySet", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write104_TypeClass(string n, string ns, TypeClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TypeClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TypeClass", "");
                }
                base.WriteAttribute("DataType", "", o.DataType);
                base.WriteElementStringRaw("FileSaveMode", "", XmlConvert.ToString(o.FileSaveMode));
                base.WriteElementString("Dir", "", o.Dir);
                base.WriteEndElement(o);
            }
        }

        private void Write105_GStampInfoClass(string n, string ns, GStampInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(GStampInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("GStampInfoClass", "");
                }
                if (o.GStampOn)
                {
                    base.WriteElementStringRaw("GStompOn", "", XmlConvert.ToString(o.GStampOn));
                }
                if (o.TopItem != "日本銀行歳入代理店")
                {
                    base.WriteElementString("TopItem", "", o.TopItem);
                }
                if ((o.DateItem != null) && (o.DateItem.Length != 0))
                {
                    base.WriteElementString("DateItem", "", o.DateItem);
                }
                if (o.DateAlignment != "Left")
                {
                    base.WriteElementString("DateAlignment", "", o.DateAlignment);
                }
                if ((o.UnderItem1 != null) && (o.UnderItem1.Length != 0))
                {
                    base.WriteElementString("UnderItem1", "", o.UnderItem1);
                }
                if ((o.UnderItem2 != null) && (o.UnderItem2.Length != 0))
                {
                    base.WriteElementString("UnderItem2", "", o.UnderItem2);
                }
                if ((o.UnderItem3 != null) && (o.UnderItem3.Length != 0))
                {
                    base.WriteElementString("UnderItem3", "", o.UnderItem3);
                }
                base.WriteEndElement(o);
            }
        }

        private void Write106_GStampSettings(string n, string ns, GStampSettings o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(GStampSettings)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("GStampSettings", "");
                }
                this.Write105_GStampInfoClass("GStampInfo", "", o.GStampInfo, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write107_AllFileSaveClass(string n, string ns, AllFileSaveClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(AllFileSaveClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("AllFileSaveClass", "");
                }
                if (o.Mode)
                {
                    base.WriteElementStringRaw("Mode", "", XmlConvert.ToString(o.Mode));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write108_FileNameClass(string n, string ns, FileNameClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(FileNameClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("FileNameClass", "");
                }
                if (o.FileNameRule != "1234")
                {
                    base.WriteElementString("FileNameRule", "", o.FileNameRule);
                }
                base.WriteEndElement(o);
            }
        }

        private void Write109_SendFileClass(string n, string ns, SendFileClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(SendFileClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("SendFileClass", "");
                }
                base.WriteElementString("SendUser", "", o.SendUser);
                base.WriteEndElement(o);
            }
        }

        private void Write11_BinClass(string n, string ns, BinClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(BinClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("BinClass", "");
                }
                base.WriteAttribute("No", "", XmlConvert.ToString(o.No));
                base.WriteElementString("Name", "", o.Name);
                if (o.L != 0)
                {
                    base.WriteElementStringRaw("L", "", XmlConvert.ToString(o.L));
                }
                if (o.T != 0)
                {
                    base.WriteElementStringRaw("T", "", XmlConvert.ToString(o.T));
                }
                if ((o.Size != null) && (o.Size.Length != 0))
                {
                    base.WriteElementString("Size", "", o.Size);
                }
                if ((o.Orientation != null) && (o.Orientation.Length != 0))
                {
                    base.WriteElementString("Orientation", "", o.Orientation);
                }
                base.WriteEndElement(o);
            }
        }

        private void Write110_FileSave(string n, string ns, FileSave o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(FileSave)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("FileSave", "");
                }
                this.Write107_AllFileSaveClass("AllFileSave", "", o.AllFileSave, false, false);
                TypeListClass typeList = o.TypeList;
                if (typeList != null)
                {
                    base.WriteStartElement("TypeList", "", null, false);
                    for (int i = 0; i < typeList.Count; i++)
                    {
                        this.Write104_TypeClass("Type", "", typeList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                this.Write108_FileNameClass("FileNaming", "", o.FileNaming, false, false);
                this.Write109_SendFileClass("SendFile", "", o.SendFile, false, false);
                DetailsListClass detailsList = o.DetailsList;
                if (detailsList != null)
                {
                    base.WriteStartElement("DetailsList", "", null, false);
                    for (int j = 0; j < detailsList.Count; j++)
                    {
                        this.Write64_DetailsClass("Details", "", detailsList[j], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        private string Write111_ListType(ListType v)
        {
            switch (v)
            {
                case ListType.UserList:
                    return "UserList";

                case ListType.MailList:
                    return "MailList";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Main.ListType");
        }

        private void Write112_TermMessage(string n, string ns, TermMessage o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TermMessage)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TermMessage", "");
                }
                TermMessageSetListClass termMessageSetList = o.TermMessageSetList;
                if (termMessageSetList != null)
                {
                    base.WriteStartElement("TermMessageSetList", "", null, false);
                    for (int i = 0; i < termMessageSetList.Count; i++)
                    {
                        this.Write36_TermMessageSetClass("TermMessageSet", "", termMessageSetList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        private void Write113_ResourceManager(string n, string ns, ResourceManager o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ResourceManager)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ResourceManager", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write114_JobStyle(JobStyle v)
        {
            switch (v)
            {
                case JobStyle.normal:
                    return "normal";

                case JobStyle.combi:
                    return "combi";

                case JobStyle.divide:
                    return "divide";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Common.JobConfig.JobStyle");
        }

        private void Write115_NaccsData(string n, string ns, NaccsData o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(NaccsData)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("NaccsData", "");
                }
                base.WriteElementString("ID", "", o.ID);
                base.WriteElementString("Style", "", this.Write114_JobStyle(o.Style));
                base.WriteElementString("Status", "", this.Write95_DataStatus(o.Status));
                base.WriteElementStringRaw("TimeStamp", "", XmlSerializationWriter.FromDateTime(o.TimeStamp));
                base.WriteElementString("UserFolder", "", o.UserFolder);
                base.WriteElementString("Contained", "", this.Write97_ContainedType(o.Contained));
                base.WriteElementString("AttachFolder", "", o.AttachFolder);
                string[] attachFile = o.AttachFile;
                if (attachFile != null)
                {
                    base.WriteStartElement("Attach", "", null, false);
                    for (int i = 0; i < attachFile.Length; i++)
                    {
                        base.WriteNullableStringLiteral("FileName", "", attachFile[i]);
                    }
                    base.WriteEndElement();
                }
                base.WriteElementStringRaw("IsDeleted", "", XmlConvert.ToString(o.IsDeleted));
                base.WriteElementStringRaw("IsRead", "", XmlConvert.ToString(o.IsRead));
                base.WriteElementStringRaw("IsSaved", "", XmlConvert.ToString(o.IsSaved));
                base.WriteElementStringRaw("IsPrinted", "", XmlConvert.ToString(o.IsPrinted));
                this.Write34_NaccsHeader("Header", "", o.NaccsHeader, false, false);
                List<string> items = o.Items;
                if (items != null)
                {
                    base.WriteStartElement("JobData", "", null, false);
                    for (int j = 0; j < items.Count; j++)
                    {
                        base.WriteNullableStringLiteral("item", "", items[j]);
                    }
                    base.WriteEndElement();
                }
                base.WriteElementStringRaw("Signflg", "", XmlConvert.ToString(o.Signflg));
                base.WriteElementString("SignFileFolder", "", o.SignFileFolder);
                base.WriteEndElement(o);
            }
        }

        private string Write116_ButtonPatern(ButtonPatern v)
        {
            switch (v)
            {
                case ButtonPatern.OK_ONLY:
                    return "OK_ONLY";

                case ButtonPatern.OK_CANCEL:
                    return "OK_CANCEL";

                case ButtonPatern.YES_NO:
                    return "YES_NO";

                case ButtonPatern.YES_NO_CANCEL:
                    return "YES_NO_CANCEL";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.ButtonPatern");
        }

        private string Write117_MessageKind(MessageKind v)
        {
            switch (v)
            {
                case MessageKind.Information:
                    return "Information";

                case MessageKind.Error:
                    return "Error";

                case MessageKind.Warning:
                    return "Warning";

                case MessageKind.Confirmation:
                    return "Confirmation";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.MessageKind");
        }

        public void Write118_AbstractSettings(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("AbstractSettings", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write2_AbstractSettings("AbstractSettings", "", (AbstractSettings) o, true, false);
            }
        }

        public void Write119_AbstractConfig(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("AbstractConfig", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write3_AbstractConfig("AbstractConfig", "", (AbstractConfig) o, true, false);
            }
        }

        private void Write12_PrinterInfoClass(string n, string ns, PrinterInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(PrinterInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("PrinterInfoClass", "");
                }
                base.WriteAttribute("Name", "", o.Name);
                base.WriteElementStringRaw("DoublePrint", "", XmlConvert.ToString(o.DoublePrint));
                BinListClass binList = o.BinList;
                if (binList != null)
                {
                    base.WriteStartElement("BinList", "", null, false);
                    for (int i = 0; i < binList.Count; i++)
                    {
                        this.Write11_BinClass("Bin", "", binList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        public void Write120_ReceiveNotice(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ReceiveNotice", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write9_ReceiveNotice("ReceiveNotice", "", (ReceiveNotice) o, true, false);
            }
        }

        public void Write121_ListItem(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ListItem", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write4_ListItem("ListItem", "", (ListItem) o, true, false);
            }
        }

        public void Write122_ReceiveInform(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ReceiveInform", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write5_ReceiveInformClass("ReceiveInform", "", (ReceiveInformClass) o, true, false);
            }
        }

        public void Write123_Sound(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Sound", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write6_SoundClass("Sound", "", (SoundClass) o, true, false);
            }
        }

        public void Write124_WarningInform(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("WarningInform", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write7_WarningInformClass("WarningInform", "", (WarningInformClass) o, true, false);
            }
        }

        public void Write125_Inform(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Inform", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write8_InformClass("Inform", "", (InformClass) o, true, false);
            }
        }

        public void Write126_Printer(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Printer", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write15_PrintSetups("Printer", "", (PrintSetups) o, true, false);
            }
        }

        public void Write127_DefaultInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DefaultInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write10_DefaultInfoClass("DefaultInfo", "", (DefaultInfoClass) o, true, false);
            }
        }

        public void Write128_PrinterInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("PrinterInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write12_PrinterInfoClass("PrinterInfo", "", (PrinterInfoClass) o, true, false);
            }
        }

        public void Write129_Bin(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Bin", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write11_BinClass("Bin", "", (BinClass) o, true, false);
            }
        }

        private void Write13_OutCodeClass(string n, string ns, OutCodeClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(OutCodeClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("OutCodeClass", "");
                }
                base.WriteAttribute("Name", "", o.Name);
                base.WriteElementStringRaw("Mode", "", XmlConvert.ToString(o.Mode));
                base.WriteElementStringRaw("Count", "", XmlConvert.ToString(o.Count));
                base.WriteElementString("Printer", "", o.Printer);
                base.WriteElementStringRaw("BinNo", "", XmlConvert.ToString(o.BinNo));
                base.WriteElementString("BinName", "", o.BinName);
                base.WriteEndElement(o);
            }
        }

        public void Write130_Forms(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Forms", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write14_FormsClass("Forms", "", (FormsClass) o, true, false);
            }
        }

        public void Write131_OutCode(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("OutCode", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write13_OutCodeClass("OutCode", "", (OutCodeClass) o, true, false);
            }
        }

        public void Write132_path(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("path", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write18_PathInfo("path", "", (PathInfo) o, true, false);
            }
        }

        public void Write133_PathItem(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("PathItem", "");
            }
            else
            {
                this.Write17_PathItem("PathItem", "", (PathItem) o, false);
            }
        }

        public void Write134_PathType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("PathType", "");
            }
            else
            {
                base.WriteElementString("PathType", "", this.Write16_PathType((PathType) o));
            }
        }

        public void Write135_ProtocolType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ProtocolType", "");
            }
            else
            {
                base.WriteElementString("ProtocolType", "", this.Write19_ProtocolType((ProtocolType) o));
            }
        }

        public void Write136_SettingDivType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("SettingDivType", "");
            }
            else
            {
                base.WriteElementString("SettingDivType", "", this.Write20_SettingDivType((SettingDivType) o));
            }
        }

        public void Write137_UserKindType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("UserKindType", "");
            }
            else
            {
                base.WriteElementString("UserKindType", "", this.Write21_UserKindType((UserKindType) o));
            }
        }

        public void Write138_KeyListClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("KeyListClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write24_KeyListClass("KeyListClass", "", (KeyListClass) o, true, false);
            }
        }

        public void Write139_UserKeyType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("UserKeyType", "");
            }
            else
            {
                base.WriteElementString("UserKeyType", "", this.Write25_UserKeyType((KeyListClass.UserKeyType) o));
            }
        }

        private void Write14_FormsClass(string n, string ns, FormsClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(FormsClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("FormsClass", "");
                }
                OutCodeListClass outCodeList = o.OutCodeList;
                if (outCodeList != null)
                {
                    base.WriteStartElement("OutCodeList", "", null, false);
                    for (int i = 0; i < outCodeList.Count; i++)
                    {
                        this.Write13_OutCodeClass("OutCode", "", outCodeList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        public void Write140_ArrayOfJobKeyClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfJobKeyClass", "");
            }
            else
            {
                base.TopLevelElement();
                JobKeyListClass class2 = (JobKeyListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfJobKeyClass", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfJobKeyClass", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write23_JobKeyClass("JobKeyClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write141_UserEnvironment(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("UserEnvironment", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write32_UserEnvironment("UserEnvironment", "", (UserEnvironment) o, true, false);
            }
        }

        public void Write142_TerminalInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("TerminalInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write26_TerminalInfoClass("TerminalInfo", "", (TerminalInfoClass) o, true, false);
            }
        }

        public void Write143_DebugFunction(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DebugFunction", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write27_DebugFunctionClass("DebugFunction", "", (DebugFunctionClass) o, true, false);
            }
        }

        public void Write144_InteractiveInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("InteractiveInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write28_InteractiveInfoClass("InteractiveInfo", "", (InteractiveInfoClass) o, true, false);
            }
        }

        public void Write145_MailInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("MailInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write29_MailInfoClass("MailInfo", "", (MailInfoClass) o, true, false);
            }
        }

        public void Write146_HttpOption(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HttpOption", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write30_HttpOptionClass("HttpOption", "", (HttpOptionClass) o, true, false);
            }
        }

        public void Write147_LogFileLength(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("LogFileLength", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write31_LogFileLengthClass("LogFileLength", "", (LogFileLengthClass) o, true, false);
            }
        }

        public void Write148_ArrayOfOutCodeClass1(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfOutCodeClass1", "");
            }
            else
            {
                base.TopLevelElement();
                OutCodeListClass class2 = (OutCodeListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfOutCodeClass1", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfOutCodeClass1", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write13_OutCodeClass("OutCodeClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write149_ArrayOfBinClass1(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfBinClass1", "");
            }
            else
            {
                base.TopLevelElement();
                BinListClass class2 = (BinListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfBinClass1", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfBinClass1", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write11_BinClass("BinClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        private void Write15_PrintSetups(string n, string ns, PrintSetups o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(PrintSetups)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("PrintSetups", "");
                }
                this.Write10_DefaultInfoClass("DefaultInfo", "", o.DefaultInfo, false, false);
                PrinterInfoListClass printerInfoList = o.PrinterInfoList;
                if (printerInfoList != null)
                {
                    base.WriteStartElement("PrinterInfoList", "", null, false);
                    for (int i = 0; i < printerInfoList.Count; i++)
                    {
                        this.Write12_PrinterInfoClass("PrinterInfo", "", printerInfoList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                this.Write14_FormsClass("Forms", "", o.Forms, false, false);
                base.WriteEndElement(o);
            }
        }

        public void Write150_WriterLog(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("WriterLog", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write33_WriterLog("WriterLog", "", (WriterLog) o, true, false);
            }
        }

        public void Write151_NaccsHeader(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("NaccsHeader", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write34_NaccsHeader("NaccsHeader", "", (NaccsHeader) o, true, false);
            }
        }

        public void Write152_DataControl(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DataControl", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write35_DataControl("DataControl", "", (DataControl) o, true, false);
            }
        }

        public void Write153_ArrayOfTermMessageSetClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfTermMessageSetClass", "");
            }
            else
            {
                base.TopLevelElement();
                TermMessageSetListClass class2 = (TermMessageSetListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfTermMessageSetClass", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfTermMessageSetClass", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write36_TermMessageSetClass("TermMessageSetClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write154_SystemEnvironment(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("SystemEnvironment", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write44_SystemEnvironment("SystemEnvironment", "", (SystemEnvironment) o, true, false);
            }
        }

        public void Write155_TerminalInfoSysClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("TerminalInfoSysClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write37_TerminalInfoSysClass("TerminalInfoSysClass", "", (TerminalInfoSysClass) o, true, false);
            }
        }

        public void Write156_InteractiveServerClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("InteractiveServerClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write38_InteractiveServerClass("InteractiveServerClass", "", (InteractiveServerClass) o, true, false);
            }
        }

        public void Write157_InteractiveInfoSysClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("InteractiveInfoSysClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write39_InteractiveInfoSysClass("InteractiveInfoSysClass", "", (InteractiveInfoSysClass) o, true, false);
            }
        }

        public void Write158_MailServerClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("MailServerClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write40_MailServerClass("MailServerClass", "", (MailServerClass) o, true, false);
            }
        }

        public void Write159_MailInfoSysClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("MailInfoSysClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write41_MailInfoSysClass("MailInfoSysClass", "", (MailInfoSysClass) o, true, false);
            }
        }

        private string Write16_PathType(PathType v)
        {
            switch (v)
            {
                case PathType.User:
                    return "User";

                case PathType.Protocol:
                    return "Protocol";

                case PathType.Install:
                    return "Install";

                case PathType.Environment:
                    return "Environment";

                case PathType.MyDocuments:
                    return "MyDocuments";

                case PathType.Debug:
                    return "Debug";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Settings.PathType");
        }

        public void Write160_BatchdocInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("BatchdocInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write42_BatchdocInfoSysClass("BatchdocInfo", "", (BatchdocInfoSysClass) o, true, false);
            }
        }

        public void Write161_LogFileLengthSysClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("LogFileLengthSysClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write43_LogFileLengthSysClass("LogFileLengthSysClass", "", (LogFileLengthSysClass) o, true, false);
            }
        }

        public void Write162_ListRec(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ListRec", "");
            }
            else
            {
                this.Write45_ListRec("ListRec", "", (UJobMenu.ListRec) o, false);
            }
        }

        public void Write163_NodeSorter(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("NodeSorter", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write46_NodeSorter("NodeSorter", "", (UDataView.NodeSorter) o, true, false);
            }
        }

        public void Write164_IndexRec(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("IndexRec", "");
            }
            else
            {
                this.Write47_IndexRec("IndexRec", "", (DataCompress.IndexRec) o, false);
            }
        }

        public void Write165_TestServer(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("TestServer", "");
            }
            else
            {
                base.WriteElementString("TestServer", "", this.Write48_TestServer((TestServer) o));
            }
        }

        public void Write166_NetTestMode(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("NetTestMode", "");
            }
            else
            {
                base.WriteElementString("NetTestMode", "", this.Write49_NetTestMode((NetTestMode) o));
            }
        }

        public void Write167_User(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("User", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write59_User("User", "", (User) o, true, false);
            }
        }

        public void Write168_Application(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Application", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write50_ApplicationClass("Application", "", (ApplicationClass) o, true, false);
            }
        }

        public void Write169_ToolBar(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ToolBar", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write51_ToolBarClass("ToolBar", "", (ToolBarClass) o, true, false);
            }
        }

        private void Write17_PathItem(string n, string ns, PathItem o, bool needType)
        {
            if (!needType && (o.GetType() != typeof(PathItem)))
            {
                throw base.CreateUnknownTypeException(o);
            }
            base.WriteStartElement(n, ns, o, false, null);
            if (needType)
            {
                base.WriteXsiType("PathItem", "");
            }
            base.WriteAttribute("type", "", this.Write16_PathType(o.PathType));
            if (o.Value != null)
            {
                base.WriteValue(o.Value);
            }
            base.WriteEndElement(o);
        }

        public void Write170_Window(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Window", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write52_WindowClass("Window", "", (WindowClass) o, true, false);
            }
        }

        public void Write171_LastLogon(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("LastLogon", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write53_LastLogonClass("LastLogon", "", (LastLogonClass) o, true, false);
            }
        }

        public void Write172_HistoryJobs(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HistoryJobs", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write60_HistoryJobsClass("HistoryJobs", "", (HistoryJobsClass) o, true, false);
            }
        }

        public void Write173_ColumnWidth(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ColumnWidth", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write54_ColumnWidthClass("ColumnWidth", "", (ColumnWidthClass) o, true, false);
            }
        }

        public void Write174_DataViewForm(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DataViewForm", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write55_DataViewFormClass("DataViewForm", "", (DataViewFormClass) o, true, false);
            }
        }

        public void Write175_ResultCodeColor(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ResultCodeColor", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write56_ResultCodeColorClass("ResultCodeColor", "", (ResultCodeColorClass) o, true, false);
            }
        }

        public void Write176_JobOption(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("JobOption", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write57_JobOptionClass("JobOption", "", (JobOptionClass) o, true, false);
            }
        }

        public void Write177_BatchSend(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("BatchSend", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write58_BatchSendClass("BatchSend", "", (BatchSendClass) o, true, false);
            }
        }

        public void Write178_HistoryUser(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HistoryUser", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write61_HistoryUserClass("HistoryUser", "", (HistoryUserClass) o, true, false);
            }
        }

        public void Write179_HistoryAddress(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HistoryAddress", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write62_HistoryAddressClass("HistoryAddress", "", (HistoryAddressClass) o, true, false);
            }
        }

        private void Write18_PathInfo(string n, string ns, PathInfo o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(PathInfo)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("PathInfo", "");
                }
                this.Write17_PathItem("project-path", "", o._projectpath, false);
                this.Write17_PathItem("custompath", "", o._custompath, false);
                this.Write17_PathItem("job-history", "", o._jobHistoryName, false);
                this.Write17_PathItem("input-history-root", "", o._input_history_root, false);
                this.Write17_PathItem("outcode-table", "", o._outCodeTable, false);
                this.Write17_PathItem("default-help-path", "", o._default_help_path, false);
                this.Write17_PathItem("default-gym-err-path", "", o._default_gym_err_path, false);
                this.Write17_PathItem("system-error-message", "", o._system_error_message, false);
                this.Write17_PathItem("default-term-message-path", "", o._default_term_msg_path, false);
                this.Write17_PathItem("support-file", "", o._supportFile, false);
                this.Write17_PathItem("web-sites", "", o._webSites, false);
                this.Write17_PathItem("guide-root", "", o._guideRoot, false);
                this.Write17_PathItem("option-certification-setup", "", o._optionCertificationSetup, false);
                this.Write17_PathItem("user-environment-setup", "", o._userEnvironmentSetup, false);
                this.Write17_PathItem("system-environment-setup", "", o._systemEnvironmentSetup, false);
                this.Write17_PathItem("printer-setup", "", o._printerSetup, false);
                this.Write17_PathItem("message-classify-setup", "", o._messageClassifySetup, false);
                this.Write17_PathItem("dataview-setup", "", o._dataviewSetup, false);
                this.Write17_PathItem("file-save-setup", "", o._file_save_setup, false);
                this.Write17_PathItem("user-setup", "", o._user_setup, false);
                this.Write17_PathItem("receive-notice-setup", "", o._receive_notice_setup, false);
                this.Write17_PathItem("help-setup", "", o._help_setup, false);
                this.Write17_PathItem("user_key_setup", "", o._user_key_setup, false);
                this.Write17_PathItem("demo-root", "", o._demoRoot, false);
                this.Write17_PathItem("scenario-root", "", o._scenarioRoot, false);
                this.Write17_PathItem("gym_guidelines", "", o._gym_guidelines, false);
                this.Write17_PathItem("job-setup", "", o._Job_setup, false);
                this.Write17_PathItem("template-root", "", o._templateRoot, false);
                this.Write17_PathItem("customize-root", "", o._customizeRoot, false);
                this.Write17_PathItem("jobtree-root", "", o._jobtreeRoot, false);
                this.Write17_PathItem("general-app-link", "", o._generalApp, false);
                this.Write17_PathItem("custom-list-link", "", o._customList, false);
                this.Write17_PathItem("dataview-path", "", o._dataView, false);
                this.Write17_PathItem("backup-path", "", o._backup, false);
                this.Write17_PathItem("attach-path", "", o._attach, false);
                this.Write17_PathItem("signfile-path", "", o._sFPath, false);
                this.Write17_PathItem("log-path", "", o._logPath, false);
                this.Write17_PathItem("code-list", "", o._codeList, false);
                this.Write17_PathItem("gstomp-info", "", o._gstampInfo, false);
                this.Write17_PathItem("guidance-root", "", o._guidanceRoot, false);
                this.Write17_PathItem("kiosklink-root", "", o._kiosklinkRoot, false);
                this.Write17_PathItem("kiosktables-root", "", o._kiosktablesRoot, false);
                this.Write17_PathItem("jetras-exe", "", o._jetras, false);
                this.Write17_PathItem("file-save-root", "", o._file_save_root, false);
                this.Write17_PathItem("printname-list", "", o._printNameList, false);
                base.WriteEndElement(o);
            }
        }

        public void Write180_HistorySearch(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HistorySearch", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write63_HistorySearchClass("HistorySearch", "", (HistorySearchClass) o, true, false);
            }
        }

        public void Write181_ArrayOfDetailsClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfDetailsClass", "");
            }
            else
            {
                base.TopLevelElement();
                DetailsListClass class2 = (DetailsListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfDetailsClass", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfDetailsClass", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write64_DetailsClass("DetailsClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write182_GatewaySettingClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("GatewaySettingClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write65_GatewaySettingClass("GatewaySettingClass", "", (GatewaySettingClass) o, true, false);
            }
        }

        public void Write183_DatasetRevert(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DatasetRevert", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write66_DatasetRevert("DatasetRevert", "", (DatasetRevert) o, true, false);
            }
        }

        public void Write184_PrintStatus(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("PrintStatus", "");
            }
            else
            {
                base.WriteElementString("PrintStatus", "", this.Write67_PrintStatus((PrintStatus) o));
            }
        }

        public void Write185_RevertEventArgs(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("RevertEventArgs", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write69_RevertEventArgs("RevertEventArgs", "", (RevertEventArgs) o, true, false);
            }
        }

        public void Write186_DatasetStorage(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DatasetStorage", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write70_DatasetStorage("DatasetStorage", "", (DatasetStorage) o, true, false);
            }
        }

        public void Write187_ArrangeDataTable(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrangeDataTable", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write71_ArrangeDataTable("ArrangeDataTable", "", (ArrangeDataTable) o, true, false);
            }
        }

        public void Write188_CustomizeMenu(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("CustomizeMenu", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write72_CustomizeMenu("CustomizeMenu", "", (CustomizeMenu) o, true, false);
            }
        }

        public void Write189_UserPathNames(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("UserPathNames", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write74_UserPathNames("UserPathNames", "", (UserPathNames) o, true, false);
            }
        }

        private string Write19_ProtocolType(ProtocolType v)
        {
            switch (v)
            {
                case ProtocolType.Interactive:
                    return "Interactive";

                case ProtocolType.Mail:
                    return "Mail";

                case ProtocolType.netNACCS:
                    return "netNACCS";

                case ProtocolType.KIOSK:
                    return "KIOSK";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Settings.ProtocolType");
        }

        public void Write190_CommonPath(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("CommonPath", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write73_CommonPathClass("CommonPath", "", (CommonPathClass) o, true, false);
            }
        }

        public void Write191_RemoveWorkerArgs(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("RemoveWorkerArgs", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write75_RemoveWorkerArgs("RemoveWorkerArgs", "", (RemoveWorkerArgs) o, true, false);
            }
        }

        public void Write192_Help(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Help", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write77_HelpSettings("Help", "", (HelpSettings) o, true, false);
            }
        }

        public void Write193_HelpPath(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("HelpPath", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write76_HelpPathClass("HelpPath", "", (HelpPathClass) o, true, false);
            }
        }

        public void Write194_ConfigFiles(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ConfigFiles", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write78_ConfigFiles("ConfigFiles", "", (ConfigFiles) o, true, false);
            }
        }

        public void Write195_RemoveWorkerResult(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("RemoveWorkerResult", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write79_RemoveWorkerResult("RemoveWorkerResult", "", (RemoveWorkerResult) o, true, false);
            }
        }

        public void Write196_DatasetRemove(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DatasetRemove", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write80_DatasetRemove("DatasetRemove", "", (DatasetRemove) o, true, false);
            }
        }

        public void Write197_OptionCertification(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("OptionCertification", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write82_OptionCertification("OptionCertification", "", (OptionCertification) o, true, false);
            }
        }

        public void Write198_AllCertification(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("AllCertification", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write81_AllCertificationClass("AllCertification", "", (AllCertificationClass) o, true, false);
            }
        }

        public void Write199_ComboManager(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ComboManager", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write83_ComboManager("ComboManager", "", (ComboManager) o, true, false);
            }
        }

        private void Write2_AbstractSettings(string n, string ns, AbstractSettings o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType)
                {
                    Type type = o.GetType();
                    if (type != typeof(AbstractSettings))
                    {
                        if (type == typeof(SendFileClass))
                        {
                            this.Write109_SendFileClass(n, ns, (SendFileClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(FileNameClass))
                        {
                            this.Write108_FileNameClass(n, ns, (FileNameClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(AllFileSaveClass))
                        {
                            this.Write107_AllFileSaveClass(n, ns, (AllFileSaveClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(GStampInfoClass))
                        {
                            this.Write105_GStampInfoClass(n, ns, (GStampInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(DataViewClass))
                        {
                            this.Write101_DataViewClass(n, ns, (DataViewClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(AllCertificationClass))
                        {
                            this.Write81_AllCertificationClass(n, ns, (AllCertificationClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HelpPathClass))
                        {
                            this.Write76_HelpPathClass(n, ns, (HelpPathClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(CommonPathClass))
                        {
                            this.Write73_CommonPathClass(n, ns, (CommonPathClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(BatchSendClass))
                        {
                            this.Write58_BatchSendClass(n, ns, (BatchSendClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(JobOptionClass))
                        {
                            this.Write57_JobOptionClass(n, ns, (JobOptionClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(ResultCodeColorClass))
                        {
                            this.Write56_ResultCodeColorClass(n, ns, (ResultCodeColorClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(DataViewFormClass))
                        {
                            this.Write55_DataViewFormClass(n, ns, (DataViewFormClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(ColumnWidthClass))
                        {
                            this.Write54_ColumnWidthClass(n, ns, (ColumnWidthClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(LastLogonClass))
                        {
                            this.Write53_LastLogonClass(n, ns, (LastLogonClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(WindowClass))
                        {
                            this.Write52_WindowClass(n, ns, (WindowClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(ToolBarClass))
                        {
                            this.Write51_ToolBarClass(n, ns, (ToolBarClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(ApplicationClass))
                        {
                            this.Write50_ApplicationClass(n, ns, (ApplicationClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(LogFileLengthClass))
                        {
                            this.Write31_LogFileLengthClass(n, ns, (LogFileLengthClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HttpOptionClass))
                        {
                            this.Write30_HttpOptionClass(n, ns, (HttpOptionClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(MailInfoClass))
                        {
                            this.Write29_MailInfoClass(n, ns, (MailInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(InteractiveInfoClass))
                        {
                            this.Write28_InteractiveInfoClass(n, ns, (InteractiveInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(DebugFunctionClass))
                        {
                            this.Write27_DebugFunctionClass(n, ns, (DebugFunctionClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(TerminalInfoClass))
                        {
                            this.Write26_TerminalInfoClass(n, ns, (TerminalInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(FormsClass))
                        {
                            this.Write14_FormsClass(n, ns, (FormsClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(DefaultInfoClass))
                        {
                            this.Write10_DefaultInfoClass(n, ns, (DefaultInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(InformClass))
                        {
                            this.Write8_InformClass(n, ns, (InformClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(WarningInformClass))
                        {
                            this.Write7_WarningInformClass(n, ns, (WarningInformClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(SoundClass))
                        {
                            this.Write6_SoundClass(n, ns, (SoundClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(AbstractConfig))
                        {
                            this.Write3_AbstractConfig(n, ns, (AbstractConfig) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(TermMessage))
                        {
                            this.Write112_TermMessage(n, ns, (TermMessage) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(FileSave))
                        {
                            this.Write110_FileSave(n, ns, (FileSave) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(GStampSettings))
                        {
                            this.Write106_GStampSettings(n, ns, (GStampSettings) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(MessageClassify))
                        {
                            this.Write102_MessageClassify(n, ns, (MessageClassify) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(UserKey))
                        {
                            this.Write85_UserKey(n, ns, (UserKey) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(OptionCertification))
                        {
                            this.Write82_OptionCertification(n, ns, (OptionCertification) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HelpSettings))
                        {
                            this.Write77_HelpSettings(n, ns, (HelpSettings) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(UserPathNames))
                        {
                            this.Write74_UserPathNames(n, ns, (UserPathNames) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(User))
                        {
                            this.Write59_User(n, ns, (User) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(SystemEnvironment))
                        {
                            this.Write44_SystemEnvironment(n, ns, (SystemEnvironment) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(UserEnvironment))
                        {
                            this.Write32_UserEnvironment(n, ns, (UserEnvironment) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(PrintSetups))
                        {
                            this.Write15_PrintSetups(n, ns, (PrintSetups) o, isNullable, true);
                            return;
                        }
                        if (type != typeof(ReceiveNotice))
                        {
                            throw base.CreateUnknownTypeException(o);
                        }
                        this.Write9_ReceiveNotice(n, ns, (ReceiveNotice) o, isNullable, true);
                        return;
                    }
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("AbstractSettings", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write20_SettingDivType(SettingDivType v)
        {
            switch (v)
            {
                case SettingDivType.User:
                    return "User";

                case SettingDivType.AllUsers:
                    return "AllUsers";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Settings.SettingDivType");
        }

        public void Write200_DataFactory(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DataFactory", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write84_DataFactory("DataFactory", "", (DataFactory) o, true, false);
            }
        }

        public void Write201_UserKey(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("UserKey", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write85_UserKey("UserKey", "", (UserKey) o, true, false);
            }
        }

        public void Write202_JobKey(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("JobKey", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write23_JobKeyClass("JobKey", "", (JobKeyClass) o, true, false);
            }
        }

        public void Write203_Key(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Key", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write22_KeyClass("Key", "", (KeyClass) o, true, false);
            }
        }

        public void Write204_ArrayOfPrinterInfoClass1(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfPrinterInfoClass1", "");
            }
            else
            {
                base.TopLevelElement();
                PrinterInfoListClass class2 = (PrinterInfoListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfPrinterInfoClass1", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfPrinterInfoClass1", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write12_PrinterInfoClass("PrinterInfoClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write205_ArrayOfString4(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfString4", "");
            }
            else
            {
                base.TopLevelElement();
                StringList list = (StringList) o;
                if (list == null)
                {
                    base.WriteNullTagLiteral("ArrayOfString4", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfString4", "", null, false);
                    for (int i = 0; i < list.Count; i++)
                    {
                        base.WriteNullableStringLiteral("string", "", list[i]);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write206_JobFormFactory(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("JobFormFactory", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write86_JobFormFactory("JobFormFactory", "", (JobFormFactory) o, true, false);
            }
        }

        public void Write207_ULogClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ULogClass", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write87_ULogClass("ULogClass", "", (ULogClass) o, true, false);
            }
        }

        public void Write208_ModeFont(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ModeFont", "");
            }
            else
            {
                base.WriteElementString("ModeFont", "", this.Write88_ModeFont((ModeFont) o));
            }
        }

        public void Write209_JobPanelStatus(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("JobPanelStatus", "");
            }
            else
            {
                base.WriteElementString("JobPanelStatus", "", this.Write89_JobPanelStatus((JobPanelStatus) o));
            }
        }

        private string Write21_UserKindType(UserKindType v)
        {
            switch (v)
            {
                case UserKindType.General:
                    return "General";

                case UserKindType.Bank:
                    return "Bank";

                case UserKindType.Center:
                    return "Center";

                case UserKindType.Private:
                    return "Private";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Settings.UserKindType");
        }

        public void Write210_VersionuptoolUpdater(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("VersionuptoolUpdater", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write90_VersionuptoolUpdater("VersionuptoolUpdater", "", (VersionuptoolUpdater) o, true, false);
            }
        }

        public void Write211_ProgresType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ProgresType", "");
            }
            else
            {
                base.WriteElementString("ProgresType", "", this.Write91_ProgresType((ArrangeProgress.ProgresType) o));
            }
        }

        public void Write212_VersionSettings(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("VersionSettings", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write92_VersionSettings("VersionSettings", "", (VersionSettings) o, true, false);
            }
        }

        public void Write213_PrintManager(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("PrintManager", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write93_PrintManager("PrintManager", "", (PrintManager) o, true, false);
            }
        }

        public void Write214_ReportStatus(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ReportStatus", "");
            }
            else
            {
                base.WriteElementString("ReportStatus", "", this.Write94_ReportStatus((USendReportDlg.ReportStatus) o));
            }
        }

        public void Write215_DataStatus(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("DataStatus", "");
            }
            else
            {
                base.WriteElementString("DataStatus", "", this.Write95_DataStatus((DataStatus) o));
            }
        }

        public void Write216_Signflg(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("Signflg", "");
            }
            else
            {
                base.WriteElementString("Signflg", "", this.Write96_Signflg((Signflg) o));
            }
        }

        public void Write217_ContainedType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ContainedType", "");
            }
            else
            {
                base.WriteElementString("ContainedType", "", this.Write97_ContainedType((ContainedType) o));
            }
        }

        public void Write218_OutcodeTbl(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("OutcodeTbl", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write99_OutcodeTbl("OutcodeTbl", "", (OutcodeTbl) o, true, false);
            }
        }

        public void Write219_OutcodeRec(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("OutcodeRec", "");
            }
            else
            {
                this.Write98_OutcodeRec("OutcodeRec", "", (OutcodeTbl.OutcodeRec) o, false);
            }
        }

        private void Write22_KeyClass(string n, string ns, KeyClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(KeyClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("KeyClass", "");
                }
                base.WriteElementString("Name", "", o.Name);
                base.WriteElementString("Sckey", "", o.Sckey);
                base.WriteEndElement(o);
            }
        }

        public void Write220_MessageClassify(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("MessageClassify", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write102_MessageClassify("MessageClassify", "", (MessageClassify) o, true, false);
            }
        }

        public void Write221_Term(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Term", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write100_TermClass("Term", "", (TermClass) o, true, false);
            }
        }

        public void Write222_DataView(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("DataView", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write101_DataViewClass("DataView", "", (DataViewClass) o, true, false);
            }
        }

        public void Write223_ArrayOfTermClass1(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfTermClass1", "");
            }
            else
            {
                base.TopLevelElement();
                TermListClass class2 = (TermListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfTermClass1", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfTermClass1", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write100_TermClass("TermClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write224_UserKeySet(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("UserKeySet", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write103_UserKeySet("UserKeySet", "", (UserKeySet) o, true, false);
            }
        }

        public void Write225_ArrayOfTypeClass(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfTypeClass", "");
            }
            else
            {
                base.TopLevelElement();
                TypeListClass class2 = (TypeListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfTypeClass", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfTypeClass", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write104_TypeClass("TypeClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write226_GStompSettings(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("GStompSettings", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write106_GStampSettings("GStompSettings", "", (GStampSettings) o, true, false);
            }
        }

        public void Write227_GStompInfo(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("GStompInfo", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write105_GStampInfoClass("GStompInfo", "", (GStampInfoClass) o, true, false);
            }
        }

        public void Write228_FileSave(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("FileSave", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write110_FileSave("FileSave", "", (FileSave) o, true, false);
            }
        }

        public void Write229_AllFileSave(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("AllFileSave", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write107_AllFileSaveClass("AllFileSave", "", (AllFileSaveClass) o, true, false);
            }
        }

        private void Write23_JobKeyClass(string n, string ns, JobKeyClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(JobKeyClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("JobKeyClass", "");
                }
                base.WriteAttribute("Kind", "", XmlConvert.ToString(o.Kind));
                List<KeyClass> keyList = o.KeyList;
                if (keyList != null)
                {
                    base.WriteStartElement("KeyList", "", null, false);
                    for (int i = 0; i < keyList.Count; i++)
                    {
                        this.Write22_KeyClass("Key", "", keyList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        public void Write230_Type(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Type", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write104_TypeClass("Type", "", (TypeClass) o, true, false);
            }
        }

        public void Write231_FileName(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("FileName", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write108_FileNameClass("FileName", "", (FileNameClass) o, true, false);
            }
        }

        public void Write232_SendFile(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("SendFile", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write109_SendFileClass("SendFile", "", (SendFileClass) o, true, false);
            }
        }

        public void Write233_Details(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("Details", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write64_DetailsClass("Details", "", (DetailsClass) o, true, false);
            }
        }

        public void Write234_ListType(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ListType", "");
            }
            else
            {
                base.WriteElementString("ListType", "", this.Write111_ListType((ListType) o));
            }
        }

        public void Write235_TermMessage(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("TermMessage", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write112_TermMessage("TermMessage", "", (TermMessage) o, true, false);
            }
        }

        public void Write236_TermMessageSet(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("TermMessageSet", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write36_TermMessageSetClass("TermMessageSet", "", (TermMessageSetClass) o, true, false);
            }
        }

        public void Write237_ArrayOfReceiveInformClass1(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ArrayOfReceiveInformClass1", "");
            }
            else
            {
                base.TopLevelElement();
                ReceiveInformListClass class2 = (ReceiveInformListClass) o;
                if (class2 == null)
                {
                    base.WriteNullTagLiteral("ArrayOfReceiveInformClass1", "");
                }
                else
                {
                    base.WriteStartElement("ArrayOfReceiveInformClass1", "", null, false);
                    for (int i = 0; i < class2.Count; i++)
                    {
                        this.Write5_ReceiveInformClass("ReceiveInformClass", "", class2[i], true, false);
                    }
                    base.WriteEndElement();
                }
            }
        }

        public void Write238_ResourceManager(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ResourceManager", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write113_ResourceManager("ResourceManager", "", (ResourceManager) o, true, false);
            }
        }

        public void Write239_ExData(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteNullTagLiteral("ExData", "");
            }
            else
            {
                base.TopLevelElement();
                this.Write115_NaccsData("ExData", "", (NaccsData) o, true, false);
            }
        }

        private void Write24_KeyListClass(string n, string ns, KeyListClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(KeyListClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("KeyListClass", "");
                }
                this.Write23_JobKeyClass("KeyInfoList", "", o.KeyInfoList, false, false);
                base.WriteEndElement(o);
            }
        }

        public void Write240_ButtonPatern(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("ButtonPatern", "");
            }
            else
            {
                base.WriteElementString("ButtonPatern", "", this.Write116_ButtonPatern((ButtonPatern) o));
            }
        }

        public void Write241_MessageKind(object o)
        {
            base.WriteStartDocument();
            if (o == null)
            {
                base.WriteEmptyTag("MessageKind", "");
            }
            else
            {
                base.WriteElementString("MessageKind", "", this.Write117_MessageKind((MessageKind) o));
            }
        }

        private string Write25_UserKeyType(KeyListClass.UserKeyType v)
        {
            switch (v)
            {
                case KeyListClass.UserKeyType.Jkey:
                    return "Jkey";

                case KeyListClass.UserKeyType.SKey:
                    return "SKey";

                case KeyListClass.UserKeyType.RKey:
                    return "RKey";

                case KeyListClass.UserKeyType.GKey:
                    return "GKey";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Settings.KeyListClass.UserKeyType");
        }

        private void Write26_TerminalInfoClass(string n, string ns, TerminalInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TerminalInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TerminalInfoClass", "");
                }
                base.WriteElementString("TermLogicalName", "", o.TermLogicalName);
                base.WriteElementString("TermAccessKey", "", o.TermAccessKey);
                if (o.SaveTerm != 30)
                {
                    base.WriteElementStringRaw("SaveTerm", "", XmlConvert.ToString(o.SaveTerm));
                }
                if (o.DiskWarning != 100)
                {
                    base.WriteElementStringRaw("DiskWarning", "", XmlConvert.ToString(o.DiskWarning));
                }
                if (o.AutoBackup)
                {
                    base.WriteElementStringRaw("AutoBackup", "", XmlConvert.ToString(o.AutoBackup));
                }
                if (o.Versionup)
                {
                    base.WriteElementStringRaw("Versionup", "", XmlConvert.ToString(o.Versionup));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write27_DebugFunctionClass(string n, string ns, DebugFunctionClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DebugFunctionClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DebugFunctionClass", "");
                }
                if (!o.Check_Off)
                {
                    base.WriteElementStringRaw("Check_Off", "", XmlConvert.ToString(o.Check_Off));
                }
                if (o.Control != 0)
                {
                    base.WriteElementStringRaw("Control", "", XmlConvert.ToString(o.Control));
                }
                if (o.UserKindFlag)
                {
                    base.WriteElementStringRaw("UserKindFlag", "", XmlConvert.ToString(o.UserKindFlag));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write28_InteractiveInfoClass(string n, string ns, InteractiveInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(InteractiveInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("InteractiveInfoClass", "");
                }
                if (o.ServerConnect != "MAIN")
                {
                    base.WriteElementString("ServerConnect", "", o.ServerConnect);
                }
                if (o.AutoSendRecvMode)
                {
                    base.WriteElementStringRaw("AutoSendRecvMode", "", XmlConvert.ToString(o.AutoSendRecvMode));
                }
                if (o.AutoSendRecvTm != 10)
                {
                    base.WriteElementStringRaw("AutoSendRecvTm", "", XmlConvert.ToString(o.AutoSendRecvTm));
                }
                if (o.JobConnect)
                {
                    base.WriteElementStringRaw("JobConnect", "", XmlConvert.ToString(o.JobConnect));
                }
                if (o.TraceOutput)
                {
                    base.WriteElementStringRaw("TraceOutput", "", XmlConvert.ToString(o.TraceOutput));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write29_MailInfoClass(string n, string ns, MailInfoClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(MailInfoClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("MailInfoClass", "");
                }
                if (o.ServerConnect != "MAIN")
                {
                    base.WriteElementString("ServerConnect", "", o.ServerConnect);
                }
                if (o.UseGateway)
                {
                    base.WriteElementStringRaw("UseGateway", "", XmlConvert.ToString(o.UseGateway));
                }
                base.WriteElementString("GatewaySMTPServerName", "", o.GatewaySMTPServerName);
                if (o.GatewaySMTPServerPort != 0x19)
                {
                    base.WriteElementStringRaw("GatewaySMTPServerPort", "", XmlConvert.ToString(o.GatewaySMTPServerPort));
                }
                base.WriteElementString("GatewayPOPServerName", "", o.GatewayPOPServerName);
                if (o.GatewayPOPServerPort != 110)
                {
                    base.WriteElementStringRaw("GatewayPOPServerPort", "", XmlConvert.ToString(o.GatewayPOPServerPort));
                }
                base.WriteElementString("GatewayMailBox", "", o.GatewayMailBox);
                base.WriteElementString("GatewaySMTPDomainName", "", o.GatewaySMTPDomainName);
                base.WriteElementString("GatewayPOPDomainName", "", o.GatewayPOPDomainName);
                if (!o.GatewayAddDomainName)
                {
                    base.WriteElementStringRaw("GatewayAddDomainName", "", XmlConvert.ToString(o.GatewayAddDomainName));
                }
                if (!o.AddDomainName)
                {
                    base.WriteElementStringRaw("AddDomainName", "", XmlConvert.ToString(o.AddDomainName));
                }
                if (o.AutoMode)
                {
                    base.WriteElementStringRaw("AutoMode", "", XmlConvert.ToString(o.AutoMode));
                }
                if (o.AutoTm != 3)
                {
                    base.WriteElementStringRaw("AutoTm", "", XmlConvert.ToString(o.AutoTm));
                }
                if (o.AutoTmKind != 1)
                {
                    base.WriteElementStringRaw("AutoTmKind", "", XmlConvert.ToString(o.AutoTmKind));
                }
                if (o.MaxRetreiveCnt != 0x270f)
                {
                    base.WriteElementStringRaw("MaxRetreiveCnt", "", XmlConvert.ToString(o.MaxRetreiveCnt));
                }
                if (o.TraceOutput)
                {
                    base.WriteElementStringRaw("TraceOutput", "", XmlConvert.ToString(o.TraceOutput));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write3_AbstractConfig(string n, string ns, AbstractConfig o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else if (!needType)
            {
                Type type = o.GetType();
                if (type != typeof(AbstractConfig))
                {
                    if (type == typeof(TermMessage))
                    {
                        this.Write112_TermMessage(n, ns, (TermMessage) o, isNullable, true);
                    }
                    else if (type == typeof(FileSave))
                    {
                        this.Write110_FileSave(n, ns, (FileSave) o, isNullable, true);
                    }
                    else if (type == typeof(GStampSettings))
                    {
                        this.Write106_GStampSettings(n, ns, (GStampSettings) o, isNullable, true);
                    }
                    else if (type == typeof(MessageClassify))
                    {
                        this.Write102_MessageClassify(n, ns, (MessageClassify) o, isNullable, true);
                    }
                    else if (type == typeof(UserKey))
                    {
                        this.Write85_UserKey(n, ns, (UserKey) o, isNullable, true);
                    }
                    else if (type == typeof(OptionCertification))
                    {
                        this.Write82_OptionCertification(n, ns, (OptionCertification) o, isNullable, true);
                    }
                    else if (type == typeof(HelpSettings))
                    {
                        this.Write77_HelpSettings(n, ns, (HelpSettings) o, isNullable, true);
                    }
                    else if (type == typeof(UserPathNames))
                    {
                        this.Write74_UserPathNames(n, ns, (UserPathNames) o, isNullable, true);
                    }
                    else if (type == typeof(User))
                    {
                        this.Write59_User(n, ns, (User) o, isNullable, true);
                    }
                    else if (type == typeof(SystemEnvironment))
                    {
                        this.Write44_SystemEnvironment(n, ns, (SystemEnvironment) o, isNullable, true);
                    }
                    else if (type == typeof(UserEnvironment))
                    {
                        this.Write32_UserEnvironment(n, ns, (UserEnvironment) o, isNullable, true);
                    }
                    else if (type == typeof(PrintSetups))
                    {
                        this.Write15_PrintSetups(n, ns, (PrintSetups) o, isNullable, true);
                    }
                    else
                    {
                        if (type != typeof(ReceiveNotice))
                        {
                            throw base.CreateUnknownTypeException(o);
                        }
                        this.Write9_ReceiveNotice(n, ns, (ReceiveNotice) o, isNullable, true);
                    }
                }
            }
        }

        private void Write30_HttpOptionClass(string n, string ns, HttpOptionClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HttpOptionClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HttpOptionClass", "");
                }
                base.WriteElementString("CertificateHash", "", o.CertificateHash);
                if (o.UseProxy)
                {
                    base.WriteElementStringRaw("UseProxy", "", XmlConvert.ToString(o.UseProxy));
                }
                base.WriteElementString("ProxyName", "", o.ProxyName);
                if (o.ProxyPort != 0x1f90)
                {
                    base.WriteElementStringRaw("ProxyPort", "", XmlConvert.ToString(o.ProxyPort));
                }
                if (o.UseProxyAccount)
                {
                    base.WriteElementStringRaw("UseProxyAccount", "", XmlConvert.ToString(o.UseProxyAccount));
                }
                base.WriteElementString("ProxyAccount", "", o.ProxyAccount);
                base.WriteElementStringRaw("ProxyPassword", "", XmlSerializationWriter.FromByteArrayBase64(o.EncryptProxyPassword));
                base.WriteEndElement(o);
            }
        }

        private void Write31_LogFileLengthClass(string n, string ns, LogFileLengthClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(LogFileLengthClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("LogFileLengthClass", "");
                }
                if (o.ComLog != 0x3e8)
                {
                    base.WriteElementStringRaw("ComLog", "", XmlConvert.ToString(o.ComLog));
                }
                if (o.MsgLog != 100)
                {
                    base.WriteElementStringRaw("MsgLog", "", XmlConvert.ToString(o.MsgLog));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write32_UserEnvironment(string n, string ns, UserEnvironment o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(UserEnvironment)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("UserEnvironment", "");
                }
                this.Write26_TerminalInfoClass("TerminalInfo", "", o.TerminalInfo, false, false);
                this.Write27_DebugFunctionClass("DebugFunction", "", o.DebugFunction, false, false);
                this.Write28_InteractiveInfoClass("InteractiveInfo", "", o.InteractiveInfo, false, false);
                this.Write29_MailInfoClass("MailInfo", "", o.MailInfo, false, false);
                this.Write30_HttpOptionClass("HttpOption", "", o.HttpOption, false, false);
                this.Write31_LogFileLengthClass("LogFileLength", "", o.LogFileLength, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write33_WriterLog(string n, string ns, WriterLog o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(WriterLog)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("WriterLog", "");
                }
                base.WriteElementString("LogFile", "", o.LogFile);
                base.WriteElementStringRaw("LogSize", "", XmlConvert.ToString(o.LogSize));
                base.WriteEndElement(o);
            }
        }

        private void Write34_NaccsHeader(string n, string ns, NaccsHeader o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(NaccsHeader)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("NaccsHeader", "");
                }
                base.WriteElementString("Control", "", o.Control);
                base.WriteElementString("JobCode", "", o.JobCode);
                base.WriteElementString("OutCode", "", o.OutCode);
                base.WriteElementString("ServerRecvTime", "", o.ServerRecvTime);
                base.WriteElementString("UserId", "", o.UserId);
                base.WriteElementString("Path", "", o.Path);
                base.WriteElementString("MailAddress", "", o.MailAddress);
                base.WriteElementString("Subject", "", o.Subject);
                base.WriteElementString("RtpInfo", "", o.RtpInfo);
                base.WriteElementString("ServerInfo", "", o.ServerInfo);
                base.WriteElementString("DataInfo", "", o.DataInfo);
                base.WriteElementString("Div", "", o.Div);
                base.WriteElementString("EndFlag", "", o.EndFlag);
                base.WriteElementString("DataType", "", o.DataType);
                base.WriteElementString("SendGuard", "", o.SendGuard);
                base.WriteElementString("InputInfo", "", o.InputInfo);
                base.WriteElementString("IndexInfo", "", o.IndexInfo);
                base.WriteElementString("Pattern", "", o.Pattern);
                base.WriteElementString("System", "", o.System);
                base.WriteElementString("DispCode", "", o.DispCode);
                base.WriteElementStringRaw("Length", "", XmlConvert.ToString(o.Length));
                base.WriteEndElement(o);
            }
        }

        private void Write35_DataControl(string n, string ns, DataControl o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DataControl)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DataControl", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write36_TermMessageSetClass(string n, string ns, TermMessageSetClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TermMessageSetClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TermMessageSetClass", "");
                }
                base.WriteAttribute("Code", "", o.Code);
                base.WriteElementString("Message", "", o.Message);
                base.WriteElementString("Button", "", o.Button);
                base.WriteElementString("Description", "", o.Description);
                base.WriteElementString("Disposition", "", o.Disposition);
                base.WriteEndElement(o);
            }
        }

        private void Write37_TerminalInfoSysClass(string n, string ns, TerminalInfoSysClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(TerminalInfoSysClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("TerminalInfoSysClass", "");
                }
                base.WriteElementStringRaw("UserKind", "", XmlConvert.ToString(o.UserKind));
                base.WriteElementString("Protocol", "", this.Write19_ProtocolType(o.Protocol));
                base.WriteElementStringRaw("SendTimeOut", "", XmlConvert.ToString(o.SendTimeOut));
                base.WriteElementStringRaw("A2MinimamDelay", "", XmlConvert.ToString(o.A2MinimamDelay));
                base.WriteElementStringRaw("A2MaximamDelay", "", XmlConvert.ToString(o.A2MaximamDelay));
                base.WriteElementStringRaw("A2TimeOut", "", XmlConvert.ToString(o.A2TimeOut));
                base.WriteElementStringRaw("CertificateDate", "", XmlConvert.ToString(o.CertificateDate));
                base.WriteElementString("BackStatusStr", "", o.BackStatusStr);
                base.WriteElementString("TestStatusStr", "", o.TestStatusStr);
                base.WriteElementStringRaw("Debug", "", XmlConvert.ToString(o.Debug));
                base.WriteElementStringRaw("UseInternet", "", XmlConvert.ToString(o.UseInternet));
                base.WriteElementString("URL", "", o.URL);
                base.WriteElementString("PortalURL", "", o.PortalURL);
                base.WriteElementString("SupportURL", "", o.SupportURL);
                base.WriteElementString("PrintStampMark", "", o.PrintStampMark);
                base.WriteElementStringRaw("Demo", "", XmlConvert.ToString(o.Demo));
                base.WriteElementStringRaw("Jetras", "", XmlConvert.ToString(o.Jetras));
                base.WriteElementStringRaw("Receiver", "", XmlConvert.ToString(o.Receiver));
                base.WriteEndElement(o);
            }
        }

        private void Write38_InteractiveServerClass(string n, string ns, InteractiveServerClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(InteractiveServerClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("InteractiveServerClass", "");
                }
                base.WriteAttribute("name", "", o.name);
                base.WriteAttribute("display", "", o.display);
                base.WriteElementString("ServerName", "", o.ServerName);
                base.WriteElementString("ObjectName", "", o.ObjectName);
                base.WriteElementStringRaw("ServerPort", "", XmlConvert.ToString(o.ServerPort));
                base.WriteElementString("PingPoint", "", o.PingPoint);
                base.WriteEndElement(o);
            }
        }

        private void Write39_InteractiveInfoSysClass(string n, string ns, InteractiveInfoSysClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(InteractiveInfoSysClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("InteractiveInfoSysClass", "");
                }
                List<InteractiveServerClass> server = o.Server;
                if (server != null)
                {
                    for (int i = 0; i < server.Count; i++)
                    {
                        this.Write38_InteractiveServerClass("Server", "", server[i], false, false);
                    }
                }
                base.WriteElementString("TraceFileName", "", o.TraceFileName);
                base.WriteElementStringRaw("TraceFileSize", "", XmlConvert.ToString(o.TraceFileSize));
                base.WriteEndElement(o);
            }
        }

        private void Write4_ListItem(string n, string ns, ListItem o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType)
                {
                    Type type = o.GetType();
                    if (type != typeof(ListItem))
                    {
                        if (type == typeof(TypeClass))
                        {
                            this.Write104_TypeClass(n, ns, (TypeClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(TermClass))
                        {
                            this.Write100_TermClass(n, ns, (TermClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(DetailsClass))
                        {
                            this.Write64_DetailsClass(n, ns, (DetailsClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HistorySearchClass))
                        {
                            this.Write63_HistorySearchClass(n, ns, (HistorySearchClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HistoryAddressClass))
                        {
                            this.Write62_HistoryAddressClass(n, ns, (HistoryAddressClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HistoryUserClass))
                        {
                            this.Write61_HistoryUserClass(n, ns, (HistoryUserClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(HistoryJobsClass))
                        {
                            this.Write60_HistoryJobsClass(n, ns, (HistoryJobsClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(TermMessageSetClass))
                        {
                            this.Write36_TermMessageSetClass(n, ns, (TermMessageSetClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(JobKeyClass))
                        {
                            this.Write23_JobKeyClass(n, ns, (JobKeyClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(OutCodeClass))
                        {
                            this.Write13_OutCodeClass(n, ns, (OutCodeClass) o, isNullable, true);
                            return;
                        }
                        if (type == typeof(PrinterInfoClass))
                        {
                            this.Write12_PrinterInfoClass(n, ns, (PrinterInfoClass) o, isNullable, true);
                            return;
                        }
                        if (type != typeof(ReceiveInformClass))
                        {
                            throw base.CreateUnknownTypeException(o);
                        }
                        this.Write5_ReceiveInformClass(n, ns, (ReceiveInformClass) o, isNullable, true);
                        return;
                    }
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ListItem", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write40_MailServerClass(string n, string ns, MailServerClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(MailServerClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("MailServerClass", "");
                }
                base.WriteAttribute("name", "", o.name);
                base.WriteAttribute("display", "", o.display);
                base.WriteElementString("SMTPServerName", "", o.SMTPServerName);
                base.WriteElementStringRaw("SMTPServerPort", "", XmlConvert.ToString(o.SMTPServerPort));
                base.WriteElementString("POPServerName", "", o.POPServerName);
                base.WriteElementStringRaw("POPServerPort", "", XmlConvert.ToString(o.POPServerPort));
                base.WriteElementString("MailBox", "", o.MailBox);
                base.WriteElementString("DomainName", "", o.DomainName);
                base.WriteElementString("PingPoint", "", o.PingPoint);
                base.WriteEndElement(o);
            }
        }

        private void Write41_MailInfoSysClass(string n, string ns, MailInfoSysClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(MailInfoSysClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("MailInfoSysClass", "");
                }
                List<MailServerClass> server = o.Server;
                if (server != null)
                {
                    for (int i = 0; i < server.Count; i++)
                    {
                        this.Write40_MailServerClass("Server", "", server[i], false, false);
                    }
                }
                base.WriteElementStringRaw("SendTimeOut", "", XmlConvert.ToString(o.SendTimeOut));
                base.WriteElementStringRaw("RecvTimeOut", "", XmlConvert.ToString(o.RecvTimeOut));
                base.WriteElementStringRaw("POPServerCnctRetry", "", XmlConvert.ToString(o.POPServerCnctRetry));
                base.WriteElementString("TraceFileName", "", o.TraceFileName);
                base.WriteElementStringRaw("TraceFileSize", "", XmlConvert.ToString(o.TraceFileSize));
                base.WriteElementStringRaw("ReConnectTime", "", XmlConvert.ToString(o.ReConnectTime));
                base.WriteEndElement(o);
            }
        }

        private void Write42_BatchdocInfoSysClass(string n, string ns, BatchdocInfoSysClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(BatchdocInfoSysClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("BatchdocInfoSysClass", "");
                }
                List<InteractiveServerClass> server = o.Server;
                if (server != null)
                {
                    for (int i = 0; i < server.Count; i++)
                    {
                        this.Write38_InteractiveServerClass("Server", "", server[i], false, false);
                    }
                }
                base.WriteElementString("TraceFileName", "", o.TraceFileName);
                base.WriteElementStringRaw("TraceFileSize", "", XmlConvert.ToString(o.TraceFileSize));
                base.WriteEndElement(o);
            }
        }

        private void Write43_LogFileLengthSysClass(string n, string ns, LogFileLengthSysClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(LogFileLengthSysClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("LogFileLengthSysClass", "");
                }
                base.WriteElementString("NACCS_ComLog1", "", o.NACCS_ComLog1);
                base.WriteElementString("NACCS_ComLog2", "", o.NACCS_ComLog2);
                base.WriteEndElement(o);
            }
        }

        private void Write44_SystemEnvironment(string n, string ns, SystemEnvironment o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(SystemEnvironment)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("SystemEnvironment", "");
                }
                this.Write37_TerminalInfoSysClass("TerminalInfo", "", o.TerminalInfo, false, false);
                this.Write39_InteractiveInfoSysClass("InteractiveInfo", "", o.InteractiveInfo, false, false);
                this.Write41_MailInfoSysClass("MailInfo", "", o.MailInfo, false, false);
                this.Write42_BatchdocInfoSysClass("BatchdocInfo", "", o.BatchdocInfo, false, false);
                this.Write43_LogFileLengthSysClass("LogFileLength", "", o.LogFileLength, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write45_ListRec(string n, string ns, UJobMenu.ListRec o, bool needType)
        {
            if (!needType && (o.GetType() != typeof(UJobMenu.ListRec)))
            {
                throw base.CreateUnknownTypeException(o);
            }
            base.WriteStartElement(n, ns, o, false, null);
            if (needType)
            {
                base.WriteXsiType("ListRec", "");
            }
            base.WriteElementString("Name", "", o.Name);
            base.WriteElementString("URL", "", o.URL);
            base.WriteEndElement(o);
        }

        private void Write46_NodeSorter(string n, string ns, UDataView.NodeSorter o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(UDataView.NodeSorter)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("NodeSorter", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write47_IndexRec(string n, string ns, DataCompress.IndexRec o, bool needType)
        {
            if (!needType && (o.GetType() != typeof(DataCompress.IndexRec)))
            {
                throw base.CreateUnknownTypeException(o);
            }
            base.WriteStartElement(n, ns, o, false, null);
            if (needType)
            {
                base.WriteXsiType("IndexRec", "");
            }
            base.WriteElementStringRaw("Offset", "", XmlConvert.ToString(o.Offset));
            base.WriteElementStringRaw("Size", "", XmlConvert.ToString(o.Size));
            base.WriteEndElement(o);
        }

        private string Write48_TestServer(TestServer v)
        {
            switch (v)
            {
                case TestServer.Interactive_MainCenter:
                    return "Interactive_MainCenter";

                case TestServer.Interactive_BackupCenter:
                    return "Interactive_BackupCenter";

                case TestServer.Interactive_Test:
                    return "Interactive_Test";

                case TestServer.netNACCS_MainNode_MainCenter:
                    return "netNACCS_MainNode_MainCenter";

                case TestServer.netNACCS_MainNode_BackupCenter:
                    return "netNACCS_MainNode_BackupCenter";

                case TestServer.netNACCS_MainNode_Test:
                    return "netNACCS_MainNode_Test";

                case TestServer.netNACCS_BackupNode_MainCenter:
                    return "netNACCS_BackupNode_MainCenter";

                case TestServer.netNACCS_BackupNode_BackupCenter:
                    return "netNACCS_BackupNode_BackupCenter";

                case TestServer.netNACCS_BackupNode_Test:
                    return "netNACCS_BackupNode_Test";

                case TestServer.Mail_MainCenter:
                    return "Mail_MainCenter";

                case TestServer.Mail_BackupCenter:
                    return "Mail_BackupCenter";

                case TestServer.Mail_Test:
                    return "Mail_Test";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.TestServer");
        }

        private string Write49_NetTestMode(NetTestMode v)
        {
            switch (v)
            {
                case NetTestMode.All:
                    return "All";

                case NetTestMode.ping:
                    return "ping";

                case NetTestMode.ipconfig:
                    return "ipconfig";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.NetTestMode");
        }

        private void Write5_ReceiveInformClass(string n, string ns, ReceiveInformClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ReceiveInformClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ReceiveInformClass", "");
                }
                base.WriteAttribute("OutCode", "", o.OutCode);
                base.WriteElementStringRaw("Inform", "", XmlConvert.ToString(o.Inform));
                base.WriteElementStringRaw("InformSound", "", XmlConvert.ToString(o.InformSound));
                base.WriteEndElement(o);
            }
        }

        private void Write50_ApplicationClass(string n, string ns, ApplicationClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ApplicationClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ApplicationClass", "");
                }
                if (o.Left != -1)
                {
                    base.WriteElementStringRaw("Left", "", XmlConvert.ToString(o.Left));
                }
                if (o.Top != -1)
                {
                    base.WriteElementStringRaw("Top", "", XmlConvert.ToString(o.Top));
                }
                if (o.Right != -1)
                {
                    base.WriteElementStringRaw("Right", "", XmlConvert.ToString(o.Right));
                }
                if (o.Bottom != -1)
                {
                    base.WriteElementStringRaw("Bottom", "", XmlConvert.ToString(o.Bottom));
                }
                if (o.MainSplitter != 200)
                {
                    base.WriteElementStringRaw("MainSplitter", "", XmlConvert.ToString(o.MainSplitter));
                }
                if (o.DataViewSplitter != 150)
                {
                    base.WriteElementStringRaw("DataViewSplitter", "", XmlConvert.ToString(o.DataViewSplitter));
                }
                if (o.Maximized)
                {
                    base.WriteElementStringRaw("Maximized", "", XmlConvert.ToString(o.Maximized));
                }
                if (o.CloseOnAppend)
                {
                    base.WriteElementStringRaw("CloseOnAppend", "", XmlConvert.ToString(o.CloseOnAppend));
                }
                if (o.Clock)
                {
                    base.WriteElementStringRaw("Clock", "", XmlConvert.ToString(o.Clock));
                }
                if (o.MaxRetreiveCnt != 0x270f)
                {
                    base.WriteElementStringRaw("MaxRetreiveCnt", "", XmlConvert.ToString(o.MaxRetreiveCnt));
                }
                if (o.DataViewFontSize != 2)
                {
                    base.WriteElementStringRaw("DataViewFontSize", "", XmlConvert.ToString(o.DataViewFontSize));
                }
                if (!o.LogoffConfirm)
                {
                    base.WriteElementStringRaw("LogoffConfirm", "", XmlConvert.ToString(o.LogoffConfirm));
                }
                if (o.SettingsDiv != SettingDivType.User)
                {
                    base.WriteElementString("SettingsDiv", "", this.Write20_SettingDivType(o.SettingsDiv));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write51_ToolBarClass(string n, string ns, ToolBarClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ToolBarClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ToolBarClass", "");
                }
                if (!o.Standard)
                {
                    base.WriteElementStringRaw("Standard", "", XmlConvert.ToString(o.Standard));
                }
                if (!o.Job)
                {
                    base.WriteElementStringRaw("Job", "", XmlConvert.ToString(o.Job));
                }
                if (!o.FunctionBar)
                {
                    base.WriteElementStringRaw("FunctionBar", "", XmlConvert.ToString(o.FunctionBar));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write52_WindowClass(string n, string ns, WindowClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(WindowClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("WindowClass", "");
                }
                if (!o.UserInput)
                {
                    base.WriteElementStringRaw("UserInput", "", XmlConvert.ToString(o.UserInput));
                }
                if (!o.JobInput)
                {
                    base.WriteElementStringRaw("JobInput", "", XmlConvert.ToString(o.JobInput));
                }
                if (!o.JobMenu)
                {
                    base.WriteElementStringRaw("JobMenu", "", XmlConvert.ToString(o.JobMenu));
                }
                if (!o.DataView)
                {
                    base.WriteElementStringRaw("DataView", "", XmlConvert.ToString(o.DataView));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write53_LastLogonClass(string n, string ns, LastLogonClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(LastLogonClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("LastLogonClass", "");
                }
                if ((o.APP != null) && (o.APP.Length != 0))
                {
                    base.WriteElementString("APP", "", o.APP);
                }
                if (o.SSO)
                {
                    base.WriteElementStringRaw("SSO", "", XmlConvert.ToString(o.SSO));
                }
                if ((o.POP != null) && (o.POP.Length != 0))
                {
                    base.WriteElementString("POP", "", o.POP);
                }
                if (!o.UPPERCASE)
                {
                    base.WriteElementStringRaw("UPPERCASE", "", XmlConvert.ToString(o.UPPERCASE));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write54_ColumnWidthClass(string n, string ns, ColumnWidthClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ColumnWidthClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ColumnWidthClass", "");
                }
                if (o.JobCode != 80)
                {
                    base.WriteElementStringRaw("JobCode", "", XmlConvert.ToString(o.JobCode));
                }
                if (o.OutCode != 60)
                {
                    base.WriteElementStringRaw("OutCode", "", XmlConvert.ToString(o.OutCode));
                }
                if (o.InputInfo != 80)
                {
                    base.WriteElementStringRaw("InputInfo", "", XmlConvert.ToString(o.InputInfo));
                }
                if (o.ResCode != 0x69)
                {
                    base.WriteElementStringRaw("ResCode", "", XmlConvert.ToString(o.ResCode));
                }
                if (o.Pattern != 40)
                {
                    base.WriteElementStringRaw("Pattern", "", XmlConvert.ToString(o.Pattern));
                }
                if (o.JobInfo != 170)
                {
                    base.WriteElementStringRaw("JobInfo", "", XmlConvert.ToString(o.JobInfo));
                }
                if (o.JonInfo1 != 0x91)
                {
                    base.WriteElementStringRaw("JonInfo1", "", XmlConvert.ToString(o.JonInfo1));
                }
                if (o.JonInfo2 != 0x24)
                {
                    base.WriteElementStringRaw("JonInfo2", "", XmlConvert.ToString(o.JonInfo2));
                }
                if (o.JonInfo3 != 0x72)
                {
                    base.WriteElementStringRaw("JonInfo3", "", XmlConvert.ToString(o.JonInfo3));
                }
                if (o.TimeStamp != 0x80)
                {
                    base.WriteElementStringRaw("TimeStamp", "", XmlConvert.ToString(o.TimeStamp));
                }
                if (o.DataType != 40)
                {
                    base.WriteElementStringRaw("DataType", "", XmlConvert.ToString(o.DataType));
                }
                if (o.EndFlag != 40)
                {
                    base.WriteElementStringRaw("EndFlag", "", XmlConvert.ToString(o.EndFlag));
                }
                if (o.TVFolder != 0xaf)
                {
                    base.WriteElementStringRaw("TVFolder", "", XmlConvert.ToString(o.TVFolder));
                }
                if (o.AirSea != 30)
                {
                    base.WriteElementStringRaw("AirSea", "", XmlConvert.ToString(o.AirSea));
                }
                if (o.PrintName != 300)
                {
                    base.WriteElementStringRaw("PrintName", "", XmlConvert.ToString(o.PrintName));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write55_DataViewFormClass(string n, string ns, DataViewFormClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DataViewFormClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DataViewFormClass", "");
                }
                if (!o.ReadVisible)
                {
                    base.WriteElementStringRaw("ReadVisible", "", XmlConvert.ToString(o.ReadVisible));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write56_ResultCodeColorClass(string n, string ns, ResultCodeColorClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ResultCodeColorClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ResultCodeColorClass", "");
                }
                if (o.NormalColorName != "White")
                {
                    base.WriteElementString("Normal", "", o.NormalColorName);
                }
                if (o.ErrorColorName != "Yellow")
                {
                    base.WriteElementString("Error", "", o.ErrorColorName);
                }
                if (o.WarningColorName != "Fuchsia")
                {
                    base.WriteElementString("Warning", "", o.WarningColorName);
                }
                if (o.CMessageColorName != "Lime")
                {
                    base.WriteElementString("Cmessage", "", o.CMessageColorName);
                }
                base.WriteEndElement(o);
            }
        }

        private void Write57_JobOptionClass(string n, string ns, JobOptionClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(JobOptionClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("JobOptionClass", "");
                }
                if (!o.QueryFieldClear)
                {
                    base.WriteElementStringRaw("QueryFieldClear", "", XmlConvert.ToString(o.QueryFieldClear));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write58_BatchSendClass(string n, string ns, BatchSendClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(BatchSendClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("BatchSendClass", "");
                }
                if (o.Sleep != 0)
                {
                    base.WriteElementStringRaw("Sleep", "", XmlConvert.ToString(o.Sleep));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write59_User(string n, string ns, User o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(User)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("User", "");
                }
                this.Write50_ApplicationClass("Application", "", o.Application, false, false);
                this.Write51_ToolBarClass("ToolBar", "", o.ToolBar, false, false);
                this.Write52_WindowClass("Window", "", o.Window, false, false);
                this.Write53_LastLogonClass("LastLogon", "", o.LastLogon, false, false);
                StringList historyJobsList = o.HistoryJobsList;
                if (historyJobsList != null)
                {
                    base.WriteStartElement("HistoryJobs", "", null, false);
                    for (int i = 0; i < historyJobsList.Count; i++)
                    {
                        base.WriteNullableStringLiteral("Job", "", historyJobsList[i]);
                    }
                    base.WriteEndElement();
                }
                this.Write54_ColumnWidthClass("ColumnWidth", "", o.ColumnWidth, false, false);
                this.Write55_DataViewFormClass("DataViewForm", "", o.DataViewForm, false, false);
                this.Write56_ResultCodeColorClass("ResultCodeColor", "", o.ResultCodeColor, false, false);
                this.Write57_JobOptionClass("JobOption", "", o.JobOption, false, false);
                this.Write58_BatchSendClass("BatchSend", "", o.BatchSend, false, false);
                StringList historyUserList = o.HistoryUserList;
                if (historyUserList != null)
                {
                    base.WriteStartElement("HistroyUser", "", null, false);
                    for (int j = 0; j < historyUserList.Count; j++)
                    {
                        base.WriteNullableStringLiteral("UserCode", "", historyUserList[j]);
                    }
                    base.WriteEndElement();
                }
                StringList historyAddressList = o.HistoryAddressList;
                if (historyAddressList != null)
                {
                    base.WriteStartElement("HistoryAddress", "", null, false);
                    for (int k = 0; k < historyAddressList.Count; k++)
                    {
                        base.WriteNullableStringLiteral("Address", "", historyAddressList[k]);
                    }
                    base.WriteEndElement();
                }
                StringList historySearchList = o.HistorySearchList;
                if (historySearchList != null)
                {
                    base.WriteStartElement("HistorySearch", "", null, false);
                    for (int m = 0; m < historySearchList.Count; m++)
                    {
                        base.WriteNullableStringLiteral("SearchInfo", "", historySearchList[m]);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        private void Write6_SoundClass(string n, string ns, SoundClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(SoundClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("SoundClass", "");
                }
                base.WriteElementString("SoundFile", "", o.SoundFile);
                base.WriteEndElement(o);
            }
        }

        private void Write60_HistoryJobsClass(string n, string ns, HistoryJobsClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HistoryJobsClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HistoryJobsClass", "");
                }
                base.WriteAttribute("No", "", XmlConvert.ToString(o.No));
                base.WriteElementString("JobCode", "", o.JobCode);
                base.WriteElementStringRaw("FontSize", "", XmlConvert.ToString(o.FontSize));
                base.WriteEndElement(o);
            }
        }

        private void Write61_HistoryUserClass(string n, string ns, HistoryUserClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HistoryUserClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HistoryUserClass", "");
                }
                base.WriteAttribute("No", "", XmlConvert.ToString(o.No));
                base.WriteElementString("UserCode", "", o.UserCode);
                base.WriteEndElement(o);
            }
        }

        private void Write62_HistoryAddressClass(string n, string ns, HistoryAddressClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HistoryAddressClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HistoryAddressClass", "");
                }
                base.WriteAttribute("No", "", XmlConvert.ToString(o.No));
                base.WriteElementString("Address", "", o.Address);
                base.WriteEndElement(o);
            }
        }

        private void Write63_HistorySearchClass(string n, string ns, HistorySearchClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HistorySearchClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HistorySearchClass", "");
                }
                base.WriteAttribute("No", "", XmlConvert.ToString(o.No));
                base.WriteElementString("SerchInfo", "", o.SerchInfo);
                base.WriteEndElement(o);
            }
        }

        private void Write64_DetailsClass(string n, string ns, DetailsClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DetailsClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DetailsClass", "");
                }
                base.WriteAttribute("OutCode", "", o.OutCode);
                base.WriteElementString("Dir", "", o.Dir);
                base.WriteElementStringRaw("AutoSave", "", XmlConvert.ToString(o.AutoSave));
                base.WriteEndElement(o);
            }
        }

        private void Write65_GatewaySettingClass(string n, string ns, GatewaySettingClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(GatewaySettingClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("GatewaySettingClass", "");
                }
                base.WriteElementStringRaw("UseGateway", "", XmlConvert.ToString(o.UseGateway));
                base.WriteElementString("GatewaySMTPServerName", "", o.GatewaySMTPServerName);
                base.WriteElementStringRaw("GatewaySMTPServerPort", "", XmlConvert.ToString(o.GatewaySMTPServerPort));
                base.WriteElementString("GatewayPOPServerName", "", o.GatewayPOPServerName);
                base.WriteElementStringRaw("GatewayPOPServerPort", "", XmlConvert.ToString(o.GatewayPOPServerPort));
                base.WriteElementString("GatewayMailBox", "", o.GatewayMailBox);
                base.WriteElementString("GatewaySMTPDomainName", "", o.GatewaySMTPDomainName);
                base.WriteElementString("GatewayPOPDomainName", "", o.GatewayPOPDomainName);
                base.WriteElementStringRaw("GatewayAddDomainName", "", XmlConvert.ToString(o.GatewayAddDomainName));
                base.WriteEndElement(o);
            }
        }

        private void Write66_DatasetRevert(string n, string ns, DatasetRevert o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DatasetRevert)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DatasetRevert", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write67_PrintStatus(PrintStatus v)
        {
            switch (v)
            {
                case PrintStatus.Print:
                    return "Print";

                case PrintStatus.HeardcopyPrint:
                    return "HeardcopyPrint";

                case PrintStatus.DataviewPrint:
                    return "DataviewPrint";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Print.PrintStatus");
        }

        private void Write69_RevertEventArgs(string n, string ns, RevertEventArgs o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(RevertEventArgs)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("RevertEventArgs", "");
                }
                base.WriteElementString("PathRoot", "", o.PathRoot);
                string[] fileNames = o.FileNames;
                if (fileNames != null)
                {
                    base.WriteStartElement("FileNames", "", null, false);
                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        base.WriteNullableStringLiteral("string", "", fileNames[i]);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        private void Write7_WarningInformClass(string n, string ns, WarningInformClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(WarningInformClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("WarningInformClass", "");
                }
                if (!o.WaringMode)
                {
                    base.WriteElementStringRaw("WaringMode", "", XmlConvert.ToString(o.WaringMode));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write70_DatasetStorage(string n, string ns, DatasetStorage o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DatasetStorage)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DatasetStorage", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write71_ArrangeDataTable(string n, string ns, ArrangeDataTable o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ArrangeDataTable)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ArrangeDataTable", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write72_CustomizeMenu(string n, string ns, CustomizeMenu o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(CustomizeMenu)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("CustomizeMenu", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write73_CommonPathClass(string n, string ns, CommonPathClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(CommonPathClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("CommonPathClass", "");
                }
                if (o.Specify)
                {
                    base.WriteElementStringRaw("Specify", "", XmlConvert.ToString(o.Specify));
                }
                base.WriteElementString("Folder", "", o.Folder);
                base.WriteEndElement(o);
            }
        }

        private void Write74_UserPathNames(string n, string ns, UserPathNames o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(UserPathNames)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("UserPathNames", "");
                }
                this.Write73_CommonPathClass("CommonPath", "", o.CommonPath, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write75_RemoveWorkerArgs(string n, string ns, RemoveWorkerArgs o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(RemoveWorkerArgs)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("RemoveWorkerArgs", "");
                }
                base.WriteSerializable(o.Table, "Table", "", false, true);
                base.WriteElementString("RemoveDate", "", o.RemoveDate);
                base.WriteElementStringRaw("ResetID", "", XmlConvert.ToString(o.ResetID));
                base.WriteEndElement(o);
            }
        }

        private void Write76_HelpPathClass(string n, string ns, HelpPathClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HelpPathClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HelpPathClass", "");
                }
                base.WriteElementString("JobErrCodeHtml", "", o.GymErrIndexHtmlUser);
                base.WriteElementString("TrmErrCodeHtml", "", o.TermMsgIndexHtmlUser);
                base.WriteElementString("GuideIndex", "", o.GuideIndexHtmlUser);
                base.WriteEndElement(o);
            }
        }

        private void Write77_HelpSettings(string n, string ns, HelpSettings o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(HelpSettings)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("HelpSettings", "");
                }
                this.Write76_HelpPathClass("HelpPath", "", o.HelpPath, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write78_ConfigFiles(string n, string ns, ConfigFiles o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ConfigFiles)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ConfigFiles", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write79_RemoveWorkerResult(string n, string ns, RemoveWorkerResult o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(RemoveWorkerResult)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("RemoveWorkerResult", "");
                }
                base.WriteElementStringRaw("IsCancel", "", XmlConvert.ToString(o.IsCancel));
                base.WriteElementStringRaw("RemoveCount", "", XmlConvert.ToString(o.RemoveCount));
                base.WriteEndElement(o);
            }
        }

        private void Write8_InformClass(string n, string ns, InformClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(InformClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("InformClass", "");
                }
                if (o.InfoMode)
                {
                    base.WriteElementStringRaw("InfoMode", "", XmlConvert.ToString(o.InfoMode));
                }
                base.WriteEndElement(o);
            }
        }

        private void Write80_DatasetRemove(string n, string ns, DatasetRemove o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DatasetRemove)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DatasetRemove", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write81_AllCertificationClass(string n, string ns, AllCertificationClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(AllCertificationClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("AllCertificationClass", "");
                }
                base.WriteElementStringRaw("Password", "", XmlSerializationWriter.FromByteArrayBase64(o.EncryptPassword));
                base.WriteEndElement(o);
            }
        }

        private void Write82_OptionCertification(string n, string ns, OptionCertification o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(OptionCertification)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("OptionCertification", "");
                }
                this.Write81_AllCertificationClass("AllCertification", "", o.AllCertification, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write83_ComboManager(string n, string ns, ComboManager o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ComboManager)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ComboManager", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write84_DataFactory(string n, string ns, DataFactory o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(DataFactory)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("DataFactory", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write85_UserKey(string n, string ns, UserKey o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(UserKey)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("UserKey", "");
                }
                JobKeyListClass jobKeyList = o.JobKeyList;
                if (jobKeyList != null)
                {
                    base.WriteStartElement("JobKeyList", "", null, false);
                    for (int i = 0; i < jobKeyList.Count; i++)
                    {
                        this.Write23_JobKeyClass("JobKey", "", jobKeyList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }

        private void Write86_JobFormFactory(string n, string ns, JobFormFactory o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(JobFormFactory)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("JobFormFactory", "");
                }
                base.WriteEndElement(o);
            }
        }

        private void Write87_ULogClass(string n, string ns, ULogClass o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ULogClass)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ULogClass", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write88_ModeFont(ModeFont v)
        {
            switch (v)
            {
                case ModeFont.normal:
                    return "normal";

                case ModeFont.large:
                    return "large";

                case ModeFont.small:
                    return "small";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Job.ModeFont");
        }

        private string Write89_JobPanelStatus(JobPanelStatus v)
        {
            switch (v)
            {
                case JobPanelStatus.Open:
                    return "Open";

                case JobPanelStatus.Closed:
                    return "Closed";

                case JobPanelStatus.Error:
                    return "Error";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Job.JobPanelStatus");
        }

        private void Write9_ReceiveNotice(string n, string ns, ReceiveNotice o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(ReceiveNotice)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("ReceiveNotice", "");
                }
                ReceiveInformListClass receiveInformList = o.ReceiveInformList;
                if (receiveInformList != null)
                {
                    base.WriteStartElement("ReceiveInformList", "", null, false);
                    for (int i = 0; i < receiveInformList.Count; i++)
                    {
                        this.Write5_ReceiveInformClass("ReceiveInform", "", receiveInformList[i], true, false);
                    }
                    base.WriteEndElement();
                }
                this.Write6_SoundClass("Sound", "", o.Sound, false, false);
                this.Write7_WarningInformClass("WarningInform", "", o.WarningInform, false, false);
                this.Write8_InformClass("Inform", "", o.Inform, false, false);
                base.WriteEndElement(o);
            }
        }

        private void Write90_VersionuptoolUpdater(string n, string ns, VersionuptoolUpdater o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(VersionuptoolUpdater)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("VersionuptoolUpdater", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write91_ProgresType(ArrangeProgress.ProgresType v)
        {
            switch (v)
            {
                case ArrangeProgress.ProgresType.Remove:
                    return "Remove";

                case ArrangeProgress.ProgresType.Revert:
                    return "Revert";

                case ArrangeProgress.ProgresType.NoType:
                    return "NoType";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.DataView.Arrange.ArrangeProgress.ProgresType");
        }

        private void Write92_VersionSettings(string n, string ns, VersionSettings o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(VersionSettings)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("VersionSettings", "");
                }
                base.WriteElementStringRaw("Application", "", XmlConvert.ToString(o.Application));
                base.WriteElementStringRaw("Template", "", XmlConvert.ToString(o.Template));
                base.WriteEndElement(o);
            }
        }

        private void Write93_PrintManager(string n, string ns, PrintManager o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(PrintManager)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("PrintManager", "");
                }
                base.WriteEndElement(o);
            }
        }

        private string Write94_ReportStatus(USendReportDlg.ReportStatus v)
        {
            switch (v)
            {
                case USendReportDlg.ReportStatus.None:
                    return "None";

                case USendReportDlg.ReportStatus.Wait:
                    return "Wait";

                case USendReportDlg.ReportStatus.Sent:
                    return "Sent";

                case USendReportDlg.ReportStatus.Error:
                    return "Error";

                case USendReportDlg.ReportStatus.Delete:
                    return "Delete";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.DataView.USendReportDlg.ReportStatus");
        }

        private string Write95_DataStatus(DataStatus v)
        {
            switch (v)
            {
                case DataStatus.Send:
                    return "Send";

                case DataStatus.Sent:
                    return "Sent";

                case DataStatus.Recept:
                    return "Recept";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.DataStatus");
        }

        private string Write96_Signflg(Signflg v)
        {
            switch (v)
            {
                case Signflg.NoSigned:
                    return "NoSigned";

                case Signflg.Signed:
                    return "Signed";

                case Signflg.VerOK:
                    return "VerOK";

                case Signflg.VerNG:
                    return "VerNG";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.Signflg");
        }

        private string Write97_ContainedType(ContainedType v)
        {
            switch (v)
            {
                case ContainedType.SendOnly:
                    return "SendOnly";

                case ContainedType.ReceiveOnly:
                    return "ReceiveOnly";

                case ContainedType.SendReceive:
                    return "SendReceive";
            }
            long num = (long) v;
            throw base.CreateInvalidEnumValueException(num.ToString(CultureInfo.InvariantCulture), "Naccs.Core.Classes.ContainedType");
        }

        private void Write98_OutcodeRec(string n, string ns, OutcodeTbl.OutcodeRec o, bool needType)
        {
            if (!needType && (o.GetType() != typeof(OutcodeTbl.OutcodeRec)))
            {
                throw base.CreateUnknownTypeException(o);
            }
            base.WriteStartElement(n, ns, o, false, null);
            if (needType)
            {
                base.WriteXsiType("OutcodeRec", "");
            }
            base.WriteElementString("OutCode", "", o.OutCode);
            base.WriteElementString("DocName", "", o.DocName);
            base.WriteEndElement(o);
        }

        private void Write99_OutcodeTbl(string n, string ns, OutcodeTbl o, bool isNullable, bool needType)
        {
            if (o == null)
            {
                if (isNullable)
                {
                    base.WriteNullTagLiteral(n, ns);
                }
            }
            else
            {
                if (!needType && (o.GetType() != typeof(OutcodeTbl)))
                {
                    throw base.CreateUnknownTypeException(o);
                }
                base.WriteStartElement(n, ns, o, false, null);
                if (needType)
                {
                    base.WriteXsiType("OutcodeTbl", "");
                }
                List<OutcodeTbl.OutcodeRec> outcodeList = o.OutcodeList;
                if (outcodeList != null)
                {
                    base.WriteStartElement("OutcodeList", "", null, false);
                    for (int i = 0; i < outcodeList.Count; i++)
                    {
                        this.Write98_OutcodeRec("OutcodeRec", "", outcodeList[i], false);
                    }
                    base.WriteEndElement();
                }
                base.WriteEndElement(o);
            }
        }
    }
}

