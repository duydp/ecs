﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace Company.KDT.SHARE.Components.Messages.PhieuKho
{
    [XmlRoot("DeclarationList", Namespace = "")]
    public class Envelope<T>
    {
        /// <summary>
        /// Header
        /// </summary>
        [XmlElement("Header")]
        public MessageHeader Header { get; set; }
        /// <summary>
        /// Body
        /// </summary>
        [XmlElement("Body")]
        public MessageBody<T> MessageBody { get; set; }
    }
}
