﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class MessageContents
    {
        public const string ChuaNhanDuocPhanHoi = "Chưa nhận được phản hồi từ hệ thống Hải quan";
        
    }

    public partial class Message
    {
     

        public static List<Message> SelectCollectionBy_ItemID(long itemID)
        {
            string whereCondition = string.Format("ItemID = {0}", itemID);
            string orderByExpression = "ItemID DESC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static List<Message> SelectCollectionBy_ReferenceID(Guid referenceID)
        {
            string whereCondition = string.Format("ReferenceID = '{0}'", referenceID);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<Message> SelectCollectionBy_ReferenceID_LoaiThongDiep(Guid referenceID, string tieuDeThongBao)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND TieuDeThongBao = N'{1}'", referenceID, tieuDeThongBao);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<Message> SelectCollectionBy_ItemID_LoaiThongDiep(long itemID, string tieuDeThongBao)
        {
            string whereCondition = string.Format("ItemID = {0} AND TieuDeThongBao = N'{1}'", itemID, tieuDeThongBao);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static string LayThongDiep(Guid referenceID, string tieuDeThongBao)
        {
            IList<Message> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(referenceID, tieuDeThongBao);
            string msg = string.Empty;
            if (list.Count > 0)
            {
                msg = list[list.Count - 1].TieuDeThongBao + ":\r\n\r\n";
                msg += list[list.Count - 1].NoiDungThongBao;
            }
            return msg;
        }

        public static string LayThongDiep(long itemID, string loaiThongDiep)
        {
            IList<Message> list = SelectCollectionBy_ItemID(itemID);
                //SelectCollectionBy_ItemID_LoaiThongDiep(itemID, loaiThongDiep);
            string msg = string.Empty;

            if (list.Count > 0)
            {
                msg = list[list.Count - 1].TieuDeThongBao + ":\r\n\r\n";
                msg += list[list.Count - 1].NoiDungThongBao;
            }
            return msg;
        }

        /// <summary>
        /// Lấy nội dung Xml.
        /// </summary>
        /// <param name="referenceID">Reference ID</param>
        /// <param name="messageType">Message Type</param>
        /// <param name="messageFunction">Message Function</param>
        /// <returns></returns>
        public static string LayNoiDungXml(Guid referenceID, MessageTypes messageType, MessageFunctions messageFunction)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND MessageType = {1} AND MessageFunction = {2}", referenceID, (int) messageType, (int) messageFunction);
            string orderByExpression = "ID DESC";
            IList<Message> collection = SelectCollectionDynamic(whereCondition, orderByExpression);
            if (collection.Count > 0)
            {
                return collection[0].MessageContent;
            }
            return string.Empty;
        }

        public static string CheckNoiDungXml(Guid referenceID, MessageTypes messageType, MessageFunctions messageFunction, string msgTitle)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND MessageType = {1} AND MessageFunction = {2} AND TieuDeThongBao = N'{3}'", referenceID, (int)messageType, (int)messageFunction, msgTitle);
            string orderByExpression = "ID DESC";
            IList<Message> collection = SelectCollectionDynamic(whereCondition, orderByExpression);
            if (collection.Count > 0)
            {
                return collection[0].MessageContent;
            }
            return string.Empty;
        }
        
    }
}
