﻿namespace Company.Interface
{
    partial class FrmDongBoDuLieuSimple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnTransfer = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnTransfer);
            this.grbMain.Controls.Add(this.txtNamTiepNhan);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.txtSoTiepNhan);
            this.grbMain.Size = new System.Drawing.Size(400, 57);
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(241, 19);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtNamTiepNhan.TabIndex = 3;
            this.txtNamTiepNhan.Text = "2008";
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(2008));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số tờ khai";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(150, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Năm tiếp nhận";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(81, 17);
            this.txtSoTiepNhan.MaxLength = 5;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransfer.Image = global::Company.Interface.Properties.Resources.Notepad;
            this.btnTransfer.Location = new System.Drawing.Point(296, 17);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(92, 23);
            this.btnTransfer.TabIndex = 4;
            this.btnTransfer.Text = "Đồng bộ";
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // FrmDongBoDuLieuSimple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 57);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmDongBoDuLieuSimple";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đồng bộ dữ liệu tờ khai thông quan sang khai từ xa";
            this.Load += new System.EventHandler(this.FrmDongBoDuLieuSimple_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Janus.Windows.EditControls.UIButton btnTransfer;
    }
}