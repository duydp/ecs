using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMDBoSung_ID { set; get; }
		public string SoDong { set; get; }
		public string MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac { set; get; }
		public string TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac { set; get; }
		public string HienThiTongSoTienTangGiamThuKhac { set; get; }
		public decimal TongSoTienTangGiamThuKhac { set; get; }
		public string MaTienTeTongSoTienTangGiamThuKhac { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection = new List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMDBoSung_ID"))) entity.TKMDBoSung_ID = reader.GetInt64(reader.GetOrdinal("TKMDBoSung_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac"))) entity.MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = reader.GetString(reader.GetOrdinal("MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac"))) entity.TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = reader.GetString(reader.GetOrdinal("TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiTongSoTienTangGiamThuKhac"))) entity.HienThiTongSoTienTangGiamThuKhac = reader.GetString(reader.GetOrdinal("HienThiTongSoTienTangGiamThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienTangGiamThuKhac"))) entity.TongSoTienTangGiamThuKhac = reader.GetDecimal(reader.GetOrdinal("TongSoTienTangGiamThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTeTongSoTienTangGiamThuKhac"))) entity.MaTienTeTongSoTienTangGiamThuKhac = reader.GetString(reader.GetOrdinal("MaTienTeTongSoTienTangGiamThuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac VALUES(@TKMDBoSung_ID, @SoDong, @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac, @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, @HienThiTongSoTienTangGiamThuKhac, @TongSoTienTangGiamThuKhac, @MaTienTeTongSoTienTangGiamThuKhac)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac SET TKMDBoSung_ID = @TKMDBoSung_ID, SoDong = @SoDong, MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac, TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, HienThiTongSoTienTangGiamThuKhac = @HienThiTongSoTienTangGiamThuKhac, TongSoTienTangGiamThuKhac = @TongSoTienTangGiamThuKhac, MaTienTeTongSoTienTangGiamThuKhac = @MaTienTeTongSoTienTangGiamThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, "TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, "TongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, "TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, "TongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac VALUES(@TKMDBoSung_ID, @SoDong, @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac, @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, @HienThiTongSoTienTangGiamThuKhac, @TongSoTienTangGiamThuKhac, @MaTienTeTongSoTienTangGiamThuKhac)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac SET TKMDBoSung_ID = @TKMDBoSung_ID, SoDong = @SoDong, MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac, TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, HienThiTongSoTienTangGiamThuKhac = @HienThiTongSoTienTangGiamThuKhac, TongSoTienTangGiamThuKhac = @TongSoTienTangGiamThuKhac, MaTienTeTongSoTienTangGiamThuKhac = @MaTienTeTongSoTienTangGiamThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, "TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, "TongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, "TKMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, "TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, "TongSoTienTangGiamThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac(long tKMDBoSung_ID, string soDong, string maKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string hienThiTongSoTienTangGiamThuKhac, decimal tongSoTienTangGiamThuKhac, string maTienTeTongSoTienTangGiamThuKhac)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac();	
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = maKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.HienThiTongSoTienTangGiamThuKhac = hienThiTongSoTienTangGiamThuKhac;
			entity.TongSoTienTangGiamThuKhac = tongSoTienTangGiamThuKhac;
			entity.MaTienTeTongSoTienTangGiamThuKhac = maTienTeTongSoTienTangGiamThuKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, HienThiTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, TongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac(long id, long tKMDBoSung_ID, string soDong, string maKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string hienThiTongSoTienTangGiamThuKhac, decimal tongSoTienTangGiamThuKhac, string maTienTeTongSoTienTangGiamThuKhac)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac();			
			entity.ID = id;
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = maKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.HienThiTongSoTienTangGiamThuKhac = hienThiTongSoTienTangGiamThuKhac;
			entity.TongSoTienTangGiamThuKhac = tongSoTienTangGiamThuKhac;
			entity.MaTienTeTongSoTienTangGiamThuKhac = maTienTeTongSoTienTangGiamThuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, HienThiTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, TongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac(long id, long tKMDBoSung_ID, string soDong, string maKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac, string hienThiTongSoTienTangGiamThuKhac, decimal tongSoTienTangGiamThuKhac, string maTienTeTongSoTienTangGiamThuKhac)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac();			
			entity.ID = id;
			entity.TKMDBoSung_ID = tKMDBoSung_ID;
			entity.SoDong = soDong;
			entity.MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac = maKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac = tenKhoanMucTiepNhanTongSoTienTangGiamThuKhac;
			entity.HienThiTongSoTienTangGiamThuKhac = hienThiTongSoTienTangGiamThuKhac;
			entity.TongSoTienTangGiamThuKhac = tongSoTienTangGiamThuKhac;
			entity.MaTienTeTongSoTienTangGiamThuKhac = maTienTeTongSoTienTangGiamThuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMDBoSung_ID", SqlDbType.BigInt, TKMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac", SqlDbType.NVarChar, TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThuKhac", SqlDbType.VarChar, HienThiTongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThuKhac", SqlDbType.Decimal, TongSoTienTangGiamThuKhac);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThuKhac", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac(long id)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}