/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 03/05/2012 10:27:42 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]'
GO
   
    
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    
@MaDoanhNghiep VARCHAR (14),    
@MaHaiQuan char(6),    
@TuNgay datetime,    
@DenNgay datetime    
AS  
SELECT     
  A.MaHaiQuan,    
     A.SoToKhai,    
  A.MaLoaiHinh,    
  A.NamDangKy,    
  A.NgayDangKy,    
  A.NGAY_THN_THX,   
  A.NgayHoanThanh    
  ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.         
FROM t_SXXK_ToKhaiMauDich A    
 LEFT JOIN (    
   SELECT DISTINCT    
    SoToKhai,    
    MaLoaiHinh,    
    NamDangKy,    
    MaHaiQuan    
   FROM t_KDT_SXXK_BKToKhaiXuat    
   WHERE    
    MaHaiQuan = @MaHaiQuan) D ON    
  A.SoToKhai=D.SoToKhai AND    
  A.MaLoaiHinh=D.MaLoaiHinh AND    
  A.NamDangKy=D.NamDangKy AND    
  A.MaHaiQuan=D.MaHaiQuan  
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)   
WHERE    
 A.MaDoanhNghiep = @MaDoanhNghiep AND    
 A.MaHaiQuan = @MaHaiQuan AND    
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')    
 AND A.NgayDangKy between @TuNgay and @DenNgay    
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )    
 AND D.SoToKhai IS NULL   
 AND a.TrangThai != 10  --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThai to khai != 10 (To khai Huy)  
    
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh  
  
/* Comment Script  
  
SELECT     
 A.MaHaiQuan,    
 A.SoToKhai,    
 A.MaLoaiHinh,    
 A.NamDangKy,    
 A.NgayDangKy,    
 A.NGAY_THN_THX    
FROM   
(  
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1  
 SELECT   DISTINCT  
  A.MaHaiQuan,   
  A.MaDoanhNghiep,   
  A.SoToKhai,    
  A.MaLoaiHinh,    
  A.NamDangKy,    
  A.NgayDangKy,    
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy   
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd  
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep  
) A    
 LEFT JOIN (    
   SELECT DISTINCT    
    SoToKhai,    
    MaLoaiHinh,    
    NamDangKy,    
    MaHaiQuan    
   FROM t_KDT_SXXK_BKToKhaiXuat    
   WHERE    
    MaHaiQuan = @MaHaiQuan) D ON    
  A.SoToKhai=D.SoToKhai AND    
  A.MaLoaiHinh=D.MaLoaiHinh AND    
  A.NamDangKy=D.NamDangKy AND    
  A.MaHaiQuan=D.MaHaiQuan    
WHERE    
 A.MaDoanhNghiep = @MaDoanhNghiep AND    
 A.MaHaiQuan = @MaHaiQuan AND    
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')    
 AND A.NgayDangKy between @TuNgay and @DenNgay    
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )    
 AND D.SoToKhai IS NULL     
 AND A.TrangThaiXuLy = 1  
     
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh    
*/    
    
    
    
    
    
    
    
    
    
    
    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]'
GO
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]              
@MaDoanhNghiep VARCHAR (14),              
@MaHaiQuan char(6),              
@TuNgay datetime,              
@DenNgay datetime,              
@UserName varchar(50),              
@TenChuHang nvarchar(200)              
              
AS              
SELECT               
  A.MaHaiQuan,              
     A.SoToKhai,              
  A.MaLoaiHinh,              
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy AND              
  A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_BKToKhaiNhap_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]'
GO
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
  
SELECT  
 B.ID,  
 B.BangKeHoSoThanhLy_ID,  
 B.SoToKhai,  
 B.MaLoaiHinh,  
 B.NamDangKy,  
 B.MaHaiQuan,  
 B.NgayDangKy,  
 B.NgayThucNhap,  
 B.STTHang,  
 B.GhiChu,  
 B.UserName,  
 A.NgayHoanThanh
 ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM  
 [dbo].[t_KDT_SXXK_BKToKhaiNhap] B  
 inner join t_SXXK_ToKhaiMauDich A  
 on B.SoToKhai=A.SoToKhai AND B.MaLoaiHinh=A.MaLoaiHinh AND B.NamDangKy=A.NamDangKy and B.MaHaiQuan=A.MaHaiQuan  
 --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
 LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
WHERE  
 B.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID  
  
  
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]'
GO
  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 B.ID,  
 B.BangKeHoSoThanhLy_ID,  
 B.SoToKhai,  
 B.MaLoaiHinh,  
 B.NamDangKy,  
 B.MaHaiQuan,  
 B.NgayDangKy,  
 B.NgayThucXuat,  
 B.STTHang,  
 B.GhiChu,  
 A.NgayHoanThanh  
 ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM  
 [dbo].[t_KDT_SXXK_BKToKhaiXuat] B  
 INNER JOIN t_SXXK_ToKhaiMauDich A  
 on B.SoToKhai=A.SoToKhai AND B.MaLoaiHinh=A.MaLoaiHinh AND B.NamDangKy=A.NamDangKy and B.MaHaiQuan=A.MaHaiQuan  
 --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
 LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
WHERE  
 B.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID  


GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.6', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.6', getdate(), null)
	end  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
