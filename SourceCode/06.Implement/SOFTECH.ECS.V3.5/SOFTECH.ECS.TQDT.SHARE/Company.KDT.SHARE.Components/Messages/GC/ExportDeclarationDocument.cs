﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class ExportDeclarationDocument:DeclarationBase
    {
        /// <summary>
        /// Ngày hoàn thành thủ tục Xuất khẩu,bắt buộc,an..10,YYYY-MM-DD
        /// </summary>
        [XmlElement("clearance")]
        public string Clearance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        [XmlElement("Material")]
        public List<GC_Material> Materials { get; set; }
    }
}
