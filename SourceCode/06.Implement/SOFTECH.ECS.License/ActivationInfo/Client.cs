﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivationInfo
{
    public class ClientInfor
    {
        public string MaMay { get; set; }
        public string TenMay { get; set; }
        public CustomersInfor CustomersInfor { get; set; }
        public ClientDetailInfo ClientDetailInfo { get; set; }
        public int TrialLeft { get; set; }
        public string OsVersion { get; set; }
    }
}
