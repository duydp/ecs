﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.Collections;
using System.Globalization;
using Company.BLL.DuLieuChuan;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
namespace Company.BLL.KDT.SXXK
{
    public partial class HoSoThanhLyDangKy
    {
        private BangKeHoSoThanhLyCollection _BKCollection = new BangKeHoSoThanhLyCollection();

        public BangKeHoSoThanhLyCollection BKCollection
        {
            set { this._BKCollection = value; }
            get { return this._BKCollection; }
        }
        public int getBKToKhaiXuat()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLTKX")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKToKhaiNhap()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLTKN")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLChuaThanhLY()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLCHUATL")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLXinHuy()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLXH")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLNopThue()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLNT")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLTamNopThue()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLCHITIETNT")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLTuCungUng()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLTCU")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLTaiXuat()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLTX")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLXuatGiaCong()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLXGC")
                {
                    return i;
                }
            }
            return -1;
        }
        public int getBKNPLNhapKinhDoanh()
        {
            for (int i = 0; i < this.BKCollection.Count; i++)
            {
                if (this.BKCollection[i].MaBangKe == "DTLNPLNKD")
                {
                    return i;
                }
            }
            return -1;
        }
        //-----------------------------------------------------------------------------------------
        public void LoadBKCollection()
        {
            this._BKCollection = BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.ID);
        }
        public void LoadBKCollectionTransaction(SqlTransaction transaction)
        {
            this._BKCollection = BangKeHoSoThanhLy.SelectCollectionBy_MaterIDTransaction(this.ID, transaction);
        }
        //-----------------------------------------------------------------------------------------

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                    {
                        this._ID = this.InsertTransaction(transaction);
                        //new BCXuatNhapTon().DeleteAllTransaction(transaction);
                        //new BCThueXNK().DeleteAllTransaction(transaction);
                    }
                    else
                        this.UpdateTransaction(transaction);

                    foreach (BangKeHoSoThanhLy bkDetail in this.BKCollection)
                    {
                        bkDetail.MaterID = this._ID;
                        bkDetail.InsertUpdateFull(transaction);
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this._ID == 0)
                this._ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (BangKeHoSoThanhLy bkDetail in this.BKCollection)
            {
                bkDetail.MaterID = this._ID;
                bkDetail.InsertUpdateFull(transaction);
            }
        }

        public bool checkHSTLHasCreated(string userName)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT id from t_KDT_SXXK_HoSoThanhLyDangKy WHERE (id = (SELECT max(id) from t_KDT_SXXK_HoSoThanhLyDangKy Where UserName = '" + userName + "')) AND TrangThaiThanhKhoan != 401 AND MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            int id = 0;
            try
            {
                id = Convert.ToInt32(db.ExecuteScalar(cmd));
            }
            catch
            {
                id = 0;
            }

            return id > 0;
        }

        public int getHSTLMoiNhat(string userName, string maDoanhNghiep, string maHaiQuan)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            int id = 0;
            string sql = "SELECT max(id) from t_KDT_SXXK_HoSoThanhLyDangKy where UserName = '" + userName + "'AND MaDoanhNghiep ='" + maDoanhNghiep + "' AND MaHaiQuanTiepNhan='" + maHaiQuan + "'";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                id = Convert.ToInt32(db.ExecuteScalar(cmd));
            }
            catch
            {
                id = 0;
            }
            return id;

        }
        public int GetLanThanhLyMoiNhat(string maDoanhNghiep, string maHaiQuan)
        {
            string sql = "SELECT ISNUll(max(LanThanhLy),0) + 1 from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaHaiQuanTiepNhan='" + maHaiQuan + "' and madoanhnghiep='" + maDoanhNghiep + "'";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            try
            {
                return (int)db.ExecuteScalar(dbc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetLanThanhLyMoiNhatByUserName(string maDoanhNghiep, string maHaiQuan)
        {
            string sql = "SELECT Max(LanThanhLy) from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaDoanhNghiep ='" + maDoanhNghiep + "' AND MaHaiQuanTiepNhan='" + maHaiQuan + "' AND UserName = '" + this.UserName + "'";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            try
            {
                return (int)db.ExecuteScalar(dbc);
            }
            catch
            {
                return 0;
            }
        }
        public DataSet GetDanhSachHSTLByUserName(SqlTransaction transaction)
        {
            string sql = "SELECT Id from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaDoanhNghiep ='" + this.MaDoanhNghiep + "' AND UserName = '" + this.UserName + "' AND LanThanhLy >=" + this.LanThanhLy + " ORDER BY Id DESC";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbc, transaction);
        }
        public DataSet GetDanhSachHSTLByUserName()
        {
            string sql = "SELECT Id from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaDoanhNghiep ='" + this.MaDoanhNghiep + "' AND UserName = '" + this.UserName + "' AND LanThanhLy >=" + this.LanThanhLy + " ORDER BY Id DESC";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbc);
        }
        public DataSet GetDanhSachHSTLByUserName1()
        {
            string sql = "SELECT Id from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaDoanhNghiep ='" + this.MaDoanhNghiep + "' AND UserName = '" + this.UserName + "' AND LanThanhLy >=" + this.LanThanhLy + " ORDER BY Id";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbc);
        }
        public void GetIdByLanThanhLy(string userName)
        {
            string sql = "SELECT ID from t_KDT_SXXK_HoSoThanhLyDangKy WHERE MaDoanhNghiep ='" + this.MaDoanhNghiep + "' AND LanThanhLy =" + this.LanThanhLy + " AND UserName = '" + userName + "'";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);
            long ret = 0;
            try
            {
                ret = Convert.ToInt64(db.ExecuteScalar(dbc));
            }
            catch
            {
                ret = 0;
            }
            this.ID = ret;
        }
        public DataSet KhoiTaoThanhLy()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //Lấy danh sách NPL tồn của bảng kê tờ khai nhập thanh lý
            try
            {
                string sp = "p_KDT_SXXK_DanhSachNPLNhapTon";
                DbCommand cmd = db.GetStoredProcCommand(sp);
                db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", DbType.Int64, this.BKCollection[this.getBKToKhaiNhap()].ID);
                db.LoadDataSet(cmd, ds, "t_NPLNhapTon");

                sp = "p_KDT_SXXK_DanhSachNPLXuatTon";
                cmd = db.GetStoredProcCommand(sp);
                db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", DbType.Int64, this.BKCollection[this.getBKToKhaiXuat()].ID);
                db.LoadDataSet(cmd, ds, "t_NPLXuatTon");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return ds;

        }

        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon ConvertFromKDTToSXXK(Company.BLL.KDT.SXXK.NPLNhapTon nplNhapTon)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon temp = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
            temp.SoToKhai = nplNhapTon.SoToKhai;
            temp.MaLoaiHinh = nplNhapTon.MaLoaiHinh;
            temp.NamDangKy = nplNhapTon.NamDangKy;
            temp.MaHaiQuan = nplNhapTon.MaHaiQuan;
            temp.MaNPL = nplNhapTon.MaNPL;
            temp.MaDoanhNghiep = this.MaDoanhNghiep;
            temp.Luong = nplNhapTon.Luong;
            temp.Ton = nplNhapTon.TonCuoi;
            temp.ThueXNK = nplNhapTon.ThueXNK;
            temp.ThueVAT = nplNhapTon.ThueVAT;
            temp.ThueTTDB = nplNhapTon.ThueTTDB;
            temp.ThueCLGia = nplNhapTon.ThueCLGia;
            temp.PhuThu = nplNhapTon.PhuThu;
            temp.ThueXNKTon = nplNhapTon.TonCuoiThueXNK;
            return temp;
        }
        public Company.BLL.SXXK.ThanhKhoan.NPLXuatTon ConvertNPLXuatTonFromKDTToSXXK(Company.BLL.KDT.SXXK.NPLXuatTon nplXuatTon)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLXuatTon temp = new Company.BLL.SXXK.ThanhKhoan.NPLXuatTon();
            temp.SoToKhai = nplXuatTon.SoToKhai;
            temp.MaLoaiHinh = nplXuatTon.MaLoaiHinh;
            temp.NamDangKy = nplXuatTon.NamDangKy;
            temp.MaHaiQuan = nplXuatTon.MaHaiQuan;
            temp.MaNPL = nplXuatTon.MaNPL;
            temp.MaDoanhNghiep = this.MaDoanhNghiep;
            temp.MaSP = nplXuatTon.MaSP;
            temp.LuongSP = nplXuatTon.LuongSP;
            temp.LuongNPL = nplXuatTon.LuongNPL;
            temp.TonNPL = nplXuatTon.TonNPL;

            return temp;
        }
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon_TL ConvertFromKDTToSXXK_TL(Company.BLL.KDT.SXXK.NPLNhapTon nplNhapTon, int lanThanhLy)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon_TL temp = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon_TL();
            temp.LanThanhLy = lanThanhLy;
            temp.SoToKhai = nplNhapTon.SoToKhai;
            temp.MaLoaiHinh = nplNhapTon.MaLoaiHinh;
            temp.NamDangKy = nplNhapTon.NamDangKy;
            temp.MaHaiQuan = nplNhapTon.MaHaiQuan;
            temp.MaNPL = nplNhapTon.MaNPL;
            temp.MaDoanhNghiep = this.MaDoanhNghiep;
            temp.Luong = nplNhapTon.Luong;
            temp.TonDau = nplNhapTon.TonDau;
            temp.TonCuoi = nplNhapTon.TonCuoi;
            return temp;
        }
        public Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon ConvertFromKDTToSXXK_TL(Company.BLL.KDT.SXXK.BCXuatNhapTon bcXNT, int lanThanhLy, string maHaiQuan)
        {
            Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon temp = new Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon();
            temp.LanThanhLy = lanThanhLy;
            temp.MaHaiQuan = maHaiQuan;
            temp.MaDoanhNghiep = bcXNT.MaDoanhNghiep;
            temp.MaNPL = bcXNT.MaNPL;
            temp.TenNPL = bcXNT.TenNPL;
            temp.TenDVT_NPL = bcXNT.TenDVT_NPL;
            temp.SoToKhaiNhap = bcXNT.SoToKhaiNhap;
            temp.NgayDangKyNhap = bcXNT.NgayDangKyNhap;
            temp.NgayHoanThanhNhap = bcXNT.NgayHoanThanhNhap;
            temp.MaLoaiHinhNhap = bcXNT.MaLoaiHinhNhap;
            temp.LuongNhap = bcXNT.LuongNhap;
            temp.LuongTonDau = bcXNT.LuongTonDau;
            temp.MaSP = bcXNT.MaSP;
            temp.TenSP = bcXNT.TenSP;
            temp.TenDVT_SP = bcXNT.TenDVT_SP;
            temp.SoToKhaiXuat = bcXNT.SoToKhaiXuat;
            temp.NgayDangKyXuat = bcXNT.NgayDangKyXuat;
            temp.NgayHoanThanhXuat = bcXNT.NgayHoanThanhXuat;
            temp.MaLoaiHinhXuat = bcXNT.MaLoaiHinhXuat;
            temp.DinhMuc = bcXNT.DinhMuc;
            temp.LuongSPXuat = bcXNT.LuongSPXuat;
            temp.LuongNPLSuDung = bcXNT.LuongNPLSuDung;
            temp.SoToKhaiTaiXuat = bcXNT.SoToKhaiTaiXuat;
            temp.NgayTaiXuat = bcXNT.NgayTaiXuat;
            temp.LuongNPLTaiXuat = bcXNT.LuongNPLTaiXuat;
            temp.LuongTonCuoi = bcXNT.LuongTonCuoi;
            return temp;
        }
        public Company.BLL.SXXK.ThanhKhoan.BCThueXNK ConvertFromKDTToSXXK_TL(Company.BLL.KDT.SXXK.BCThueXNK bcThueXNK, int lanThanhLy, string maHaiQuan)
        {
            Company.BLL.SXXK.ThanhKhoan.BCThueXNK temp = new Company.BLL.SXXK.ThanhKhoan.BCThueXNK();
            temp.LanThanhLy = lanThanhLy;
            temp.MaHaiQuan = maHaiQuan;
            temp.MaDoanhNghiep = bcThueXNK.MaDoanhNghiep;
            temp.MaNPL = bcThueXNK.MaNPL;
            temp.TenDVT_NPL = bcThueXNK.TenDVT_NPL;
            temp.SoToKhaiNhap = bcThueXNK.SoToKhaiNhap;
            temp.NgayDangKyNhap = bcThueXNK.NgayDangKyNhap;
            temp.NgayThucNhap = bcThueXNK.NgayThucNhap;
            temp.MaLoaiHinhNhap = bcThueXNK.MaLoaiHinhNhap;
            temp.LuongNhap = bcThueXNK.LuongNhap;
            temp.DonGiaTT = bcThueXNK.DonGiaTT;
            temp.TyGiaTT = bcThueXNK.TyGiaTT;
            temp.ThueSuat = bcThueXNK.ThueSuat;
            temp.ThueNKNop = bcThueXNK.ThueNKNop;
            temp.SoToKhaiXuat = bcThueXNK.SoToKhaiXuat;
            temp.NgayDangKyXuat = bcThueXNK.NgayDangKyXuat;
            temp.NgayThucXuat = bcThueXNK.NgayThucXuat;
            temp.MaLoaiHinhXuat = bcThueXNK.MaLoaiHinhXuat;
            temp.LuongNPLSuDung = bcThueXNK.LuongNPLSuDung;
            temp.LuongNPLTon = bcThueXNK.LuongNPLTon;
            temp.TienThueHoan = bcThueXNK.TienThueHoan;
            temp.TienThueTKTiep = bcThueXNK.TienThueTKTiep;
            return temp;
        }
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon ConvertFromKDTToSXXKR(Company.BLL.KDT.SXXK.NPLNhapTon nplNhapTon)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon temp = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
            temp.SoToKhai = nplNhapTon.SoToKhai;
            temp.MaLoaiHinh = nplNhapTon.MaLoaiHinh;
            temp.NamDangKy = nplNhapTon.NamDangKy;
            temp.MaHaiQuan = nplNhapTon.MaHaiQuan;
            temp.MaNPL = nplNhapTon.MaNPL;
            temp.MaDoanhNghiep = this.MaDoanhNghiep;
            temp.Luong = nplNhapTon.Luong;
            temp.Ton = nplNhapTon.TonDau;
            temp.ThueXNK = nplNhapTon.ThueXNK;
            temp.ThueVAT = nplNhapTon.ThueVAT;
            temp.ThueTTDB = nplNhapTon.ThueTTDB;
            temp.ThueCLGia = nplNhapTon.ThueCLGia;
            temp.PhuThu = nplNhapTon.PhuThu;
            temp.ThueXNKTon = nplNhapTon.TonDauThueXNK;
            return temp;
        }
        public void DongHoSo()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string mess = "";
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    //foreach (BangKeHoSoThanhLy bk in this.BKCollection)
                    //bk.LoadChiTietBangKe();
                    //foreach (BKToKhaiXuat bktkx in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
                    //{
                    //    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    //    tkmd.SoToKhai = bktkx.SoToKhai;
                    //    tkmd.MaLoaiHinh = bktkx.MaLoaiHinh;
                    //    tkmd.NamDangKy = bktkx.NamDangKy;
                    //    tkmd.MaHaiQuan = bktkx.MaHaiQuan;
                    //    tkmd.UpdateTrangThai("H", transaction);
                    //}


                    #region cập nhật NPL Nhập tồn
                    Company.BLL.KDT.SXXK.NPLNhapTonCollection collectionKDT = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamic("LanThanhLy =" + this.LanThanhLy + " AND MaDoanhNghiep = '" + MaDoanhNghiep + "'", "");
                    foreach (Company.BLL.KDT.SXXK.NPLNhapTon nplKDT in collectionKDT)
                    {
                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplSXXK = ConvertFromKDTToSXXK(nplKDT);
                        nplSXXK.InsertUpdateTransaction(transaction);

                    }
                    #endregion

                    this.TrangThaiThanhKhoan = 401;
                    this.UpdateTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void FixDuLieuThanhKhoan()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    Company.BLL.KDT.SXXK.NPLNhapTonCollection nplNhapTonCollection = new NPLNhapTon().SelectCollectionDynamic("LanThanhLy=" + this.LanThanhLy, "");
                    foreach (NPLNhapTon nplNhapTon in nplNhapTonCollection)
                    {
                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTonSXXK = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                        nplNhapTonSXXK.SoToKhai = nplNhapTon.SoToKhai;
                        nplNhapTonSXXK.MaLoaiHinh = nplNhapTon.MaLoaiHinh;
                        nplNhapTonSXXK.NamDangKy = nplNhapTon.NamDangKy;
                        nplNhapTonSXXK.MaNPL = nplNhapTon.MaNPL;
                        nplNhapTonSXXK.MaHaiQuan = nplNhapTon.MaHaiQuan;
                        nplNhapTonSXXK.Load(transaction);
                        NPLNhapTon npl = nplNhapTon.LayLuongTonDung(transaction);
                        if (npl.Luong > 0)
                        {
                            nplNhapTonSXXK.Ton = npl.TonCuoi;
                            nplNhapTonSXXK.ThueXNKTon = npl.TonCuoiThueXNK;
                        }

                        else
                            if (nplNhapTonSXXK.PhuThu == 0)
                            {
                                nplNhapTonSXXK.Ton = nplNhapTonSXXK.Luong;
                                nplNhapTonSXXK.ThueXNKTon = nplNhapTonSXXK.ThueXNK;
                            }
                        nplNhapTonSXXK.UpdateTransaction(transaction);

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;

                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void RollBack()
        {
            if (this.TrangThaiThanhKhoan <= 400) this.TrangThaiThanhKhoan = 0;
            else
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        //foreach (BangKeHoSoThanhLy bk in this.BKCollection)
                        //    bk.LoadChiTietBangKe();

                        //foreach (BKToKhaiXuat bktkx in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
                        //{
                        //    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        //    tkmd.SoToKhai = bktkx.SoToKhai;
                        //    tkmd.MaLoaiHinh = bktkx.MaLoaiHinh;
                        //    tkmd.NamDangKy = bktkx.NamDangKy;
                        //    tkmd.MaHaiQuan = bktkx.MaHaiQuan;
                        //    tkmd.UpdateTrangThai(" ", transaction);
                        //}
                        Company.BLL.KDT.SXXK.NPLNhapTonCollection colection = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamicTransaction("LanThanhLy = " + this.LanThanhLy + " AND TonDau > TonCuoi", "", transaction);
                        foreach (Company.BLL.KDT.SXXK.NPLNhapTon nplKDT in colection)
                        {
                            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplSXXK = ConvertFromKDTToSXXKR(nplKDT);
                            nplSXXK.UpdateTransaction(transaction);
                        }

                        this.TrangThaiThanhKhoan = 400;
                        this.UpdateTransaction(transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public void RollBackTransaction(SqlTransaction transaction)
        {
            if (this.TrangThaiThanhKhoan <= 400) this.TrangThaiThanhKhoan = 0;
            else
            {

                try
                {
                    //foreach (BangKeHoSoThanhLy bk in this.BKCollection)
                    //bk.LoadChiTietBangKe();

                    //foreach (BKToKhaiXuat bktkx in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
                    //{
                    //    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    //    tkmd.SoToKhai = bktkx.SoToKhai;
                    //    tkmd.MaLoaiHinh = bktkx.MaLoaiHinh;
                    //    tkmd.NamDangKy = bktkx.NamDangKy;
                    //    tkmd.MaHaiQuan = bktkx.MaHaiQuan;
                    //    tkmd.UpdateTrangThai(" ", transaction);
                    //}
                    Company.BLL.KDT.SXXK.NPLNhapTonCollection colection = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamicTransaction("LanThanhLy = " + this.LanThanhLy + " AND TonDau > TonCuoi AND MaDoanhNghiep ='" + MaDoanhNghiep + "'", "", transaction);
                    foreach (Company.BLL.KDT.SXXK.NPLNhapTon nplKDT in colection)
                    {
                        //TEST
                        //if (nplKDT.SoToKhai == 150 && nplKDT.MaNPL == "NHANDECAN")
                        //{
                        //}

                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplSXXK = ConvertFromKDTToSXXKR(nplKDT);

                        /*HUNGTQ, 01/08/2011. Cap nhat Luong nhap cua NPL bi lech giua t_KDT_SXXK_NPLNhapTon so voi t_SXXK_NPLNhapTonThucTe
                         * Ly do: gia tri bi lech do lam tron so thap phan. Du lieu sai o bang t_KDT_SXXK_NPLNhapTon. Lay gia tri dung trong
                         * bang t_SXXK_NPLNhapTonThucTe cap nhat lai "Luong nhap" cho t_KDT_SXXK_NPLNhapTon.
                         */
                        //Company.BLL.SXXK.NPLNhapTonThucTe nplTonThucTe = Company.BLL.SXXK.NPLNhapTonThucTe.Load(nplKDT.SoToKhai, nplKDT.MaLoaiHinh, nplKDT.NamDangKy, nplKDT.MaHaiQuan, nplKDT.MaNPL);
                        //if (nplTonThucTe != null && nplTonThucTe.Luong != nplSXXK.Luong)
                        //{
                        //    nplSXXK.Luong = nplTonThucTe.Luong;
                        //    nplSXXK.ThueXNKTon = Math.Round((double)nplSXXK.Ton * nplSXXK.ThueXNK / (double)nplSXXK.Luong, 0);
                        //}

                        //Logger.LocalLogger.Instance().WriteMessage("Cap nhat Luong nhap cua NPL bi lech giua t_KDT_SXXK_NPLNhapTon so voi t_SXXK_NPLNhapTonThucTe:\r\nSoToKhai = " + nplKDT.SoToKhai + " - MaloaiHinh = " + nplKDT.MaLoaiHinh + " - NamDangKy = " + nplKDT.NamDangKy + "\r\nMaNPL = " + nplKDT.MaNPL + " - Luong = " + nplSXXK.Luong + " - ThueXNKTon = " + nplSXXK.ThueXNKTon, new Exception());

                        nplSXXK.UpdateTransaction(transaction);

                    }
                    this.TrangThaiThanhKhoan = 400;
                    this.UpdateTransaction(transaction);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }
        public void RollBackNhieuHSTK()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DataTable dt = this.GetDanhSachHSTLByUserName(transaction).Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        HoSoThanhLyDangKy HSTLDK = new HoSoThanhLyDangKy();
                        HSTLDK.ID = Convert.ToInt64(dt.Rows[i]["Id"]);
                        HSTLDK.LoadTransaction(transaction);
                        //HSTLDK.LoadBKCollectionTransaction(transaction);
                        HSTLDK.RollBackTransaction(transaction);

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    Logger.LocalLogger.Instance().WriteMessage("Thanh khoản", ex);

                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public DataSet GetDSSPChuaCoDM()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //Lấy danh sách SP chưa có định mức
            try
            {
                string sp = "SELECT  distinct  b.MaPhu as Ma,b.TenHang as Ten,b.MaHS ,b.DVT_ID " +
                            "FROM  t_KDT_SXXK_BKToKhaiXuat AS a INNER JOIN " +
                             "t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai AND a.MaLoaiHinh = d.MaLoaiHinh AND a.NamDangKy = d.NamDangKy AND " +
                             "a.MaHaiQuan = d.MaHaiQuan INNER JOIN " +
                             "t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND " +
                             "a.MaHaiQuan = b.MaHaiQuan " +
                            "WHERE (a.MaLoaiHinh LIKE 'XSX%') AND (d.Xuat_NPL_SP = 'S') AND a.BangKeHoSoThanhLy_ID=@BangKeHoSoThanhLy_ID AND b.MaPhu not in (SELECT distinct MaSanPham FROM t_SXXK_DinhMuc)" +
                            "ORDER BY b.MaPhu";
                DbCommand cmd = db.GetSqlStringCommand(sp);
                db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", DbType.Int64, this.BKCollection[this.getBKToKhaiXuat()].ID);
                db.LoadDataSet(cmd, ds, "t_NPLNhapTon");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return ds;

        }
        public DataSet GetDSSPChuaCoDM(string where)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //Lấy danh sách SP chưa có định mức
            try
            {
                string sql = "SELECT  DISTINCT  MaPhu as Ma, TenHang as Ten, MaHS, DVT_ID " +
                            "FROM " +
                            "t_View_HangMauDich " +
                            "WHERE (CAST(SoToKhai as varchar(6)) + MaLoaiHinh + CAST(NamDangKy as varchar(4))) IN (" + where + ") AND (MaLoaiHinh LIKE 'XSX%') AND (Xuat_NPL_SP = 'S') AND MaPhu not in (SELECT distinct MaSanPham FROM t_SXXK_DinhMuc)" +
                            "ORDER BY MaPhu";
                DbCommand cmd = db.GetSqlStringCommand(sql);
                db.LoadDataSet(cmd, ds, "t_NPLNhapTon");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return ds;

        }
        public void ChayThanhKhoanDungTKXGC(int SoThapPhanNPL)
        {
            SqlDatabase dbGC = (SqlDatabase)DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "SELECT * FROM V_GC_PhanBo WHERE MaLoaiHinhNhap LIKE 'NSX%' AND (";

            string where = " CAST(SoToKhaiXuat as nvarchar(5)) + Cast(MaLoaiHinhXuat as nvarchar(5)) + CAST(NamDangKyXuat as Nvarchar(4)) IN (";
            this.BKCollection[0].LoadChiTietBangKe();
            foreach (BKToKhaiXuat bktkx in this.BKCollection[0].bkTKXColletion)
            {
                where += "'" + bktkx.SoToKhai + bktkx.MaLoaiHinh + bktkx.NamDangKy + "',";
            }
            where = where.Remove(where.Length - 1);
            where += ")) ORDER BY NgayDangKyNhap,SoToKhaiNhap,MaLoaiHinhNhap,MaNPL,NgayDangKyXuat,SoToKhaiXuat,MaLoaiHinhXuat,TenSP";
            sql += where;

            DbCommand dbcommand = dbGC.GetSqlStringCommand(sql);
            DataTable dtPhanBo = dbGC.ExecuteDataSet(dbcommand).Tables[0];
            BCXuatNhapTonCollection bcXNTCollection = new BCXuatNhapTonCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    new NPLNhapTon().DeleteDynamicTransaction(transaction, " LanThanhLy=" + this.LanThanhLy + " AND MaDoanhNghiep = '" + MaDoanhNghiep + "'");
                    decimal temp = 0;
                    foreach (DataRow dr in dtPhanBo.Rows)
                    {
                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTonSXXK = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                        NPLNhapTon nplNhapTon = new NPLNhapTon();
                        nplNhapTon.LanThanhLy = this.LanThanhLy;
                        nplNhapTonSXXK.MaDoanhNghiep = nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                        nplNhapTonSXXK.MaHaiQuan = nplNhapTon.MaHaiQuan = dr["MaHaiQuanNhap"].ToString();
                        nplNhapTonSXXK.MaLoaiHinh = nplNhapTon.MaLoaiHinh = dr["MaLoaiHinhNhap"].ToString();
                        nplNhapTonSXXK.SoToKhai = nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                        nplNhapTonSXXK.NamDangKy = nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKyNhap"]);
                        nplNhapTonSXXK.MaNPL = nplNhapTon.MaNPL = Company.BLL.SXXK.NguyenPhuLieu.GetNPLTuongUng(dr["MaNPL"].ToString(), dr["IDHopDong"].ToString(), nplNhapTonSXXK);
                        nplNhapTonSXXK.Load(transaction);

                        nplNhapTon.Luong = nplNhapTonSXXK.Luong;
                        nplNhapTon.TonDau = nplNhapTonSXXK.Ton;
                        temp = Convert.ToDecimal(dr["LuongTonDau"]);
                        nplNhapTon.TonCuoi = temp - Convert.ToDecimal(dr["LuongPhanBo"]);
                        nplNhapTon.ThueXNK = nplNhapTonSXXK.ThueXNK;
                        nplNhapTon.TonDauThueXNK = nplNhapTonSXXK.ThueXNKTon;
                        if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                        else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.ThueXNK / (double)nplNhapTon.Luong, 0);
                        nplNhapTon.InsertUpdateTransaction(transaction);

                        BCXuatNhapTon bc = new BCXuatNhapTon();
                        bc.STT = 0;
                        bc.ChuyenMucDichKhac = "";
                        bc.DinhMuc = Convert.ToDecimal(dr["DinhMucChung"]);
                        bc.LanThanhLy = this.LanThanhLy;
                        bc.LuongNhap = nplNhapTonSXXK.Luong;


                        if (dr["MaLoaiHinhXuat"].ToString() == "XGC04")
                        {
                            bc.LuongSPXuat = 0;
                            bc.LuongNPLTaiXuat = Convert.ToDecimal(dr["LuongPhanBo"]);
                        }
                        else
                        {
                            bc.LuongNPLSuDung = Convert.ToDecimal(dr["LuongPhanBo"]);
                            bc.LuongNPLTaiXuat = 0;
                            bc.LuongSPXuat = bc.LuongNPLSuDung / bc.DinhMuc;
                        }
                        bc.LuongTonCuoi = nplNhapTon.TonCuoi;
                        bc.LuongTonDau = nplNhapTon.TonDau;
                        bc.MaDoanhNghiep = this.MaDoanhNghiep;
                        bc.MaNPL = nplNhapTonSXXK.MaNPL;
                        bc.TenNPL = Convert.ToString(dr["TenNPL"]);
                        bc.DonGiaTT = Convert.ToDouble(dr["DonGiaTT"]);
                        bc.TyGiaTT = Convert.ToDecimal(dr["TyGiaTT"]);
                        bc.ThueSuat = Convert.ToDecimal(dr["ThueSuat"]);
                        bc.ThueXNK = nplNhapTon.ThueXNK;
                        bc.ThueXNKTon = nplNhapTonSXXK.ThueXNKTon;

                        if (dr["MaLoaiHinhXuat"].ToString() == "XGC04")
                        {
                            bc.LuongNPLTaiXuat = Convert.ToDecimal(dr["LuongPhanBo"]);
                            bc.SoToKhaiTaiXuat = Convert.ToInt32(dr["SoToKhaiXuat"]);
                            bc.NgayTaiXuat = Convert.ToDateTime(dr["NgayDangKyXuat"]);
                            bc.LuongNPLSuDung = 0;
                        }

                        bc.MaSP = Convert.ToString(dr["MaSP"]);
                        bc.TenSP = Convert.ToString(dr["TenSP"]);
                        bc.NamThanhLy = DateTime.Today.Year;
                        bc.NgayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                        bc.NgayDangKyXuat = Convert.ToDateTime(dr["NgayDangKyXuat"]);
                        bc.NgayHoanThanhNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                        bc.NgayHoanThanhXuat = Convert.ToDateTime(dr["NgayThucXuat"]);
                        bc.MaLoaiHinhNhap = Convert.ToString(dr["MaLoaiHinhNhap"]);
                        bc.MaLoaiHinhXuat = Convert.ToString(dr["MaLoaiHinhXuat"]);
                        bc.SoToKhaiNhap = Convert.ToInt32(dr["SoToKhaiNhap"]);
                        bc.SoToKhaiXuat = Convert.ToInt32(dr["SoToKhaiXuat"]);
                        bc.TenDVT_NPL = DonViTinh.GetName(dr["DVT_NPL"]);
                        bc.TenDVT_SP = DonViTinh.GetName(dr["DVT_SP"]);

                        if (nplNhapTon.TonCuoi > 0)
                            bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                        else
                            bc.ThanhKhoanTiep = "";
                        bcXNTCollection.Add(bc);

                    }
                    this.InsertBCXNT(bcXNTCollection, transaction);
                    int soToKhaiXuat = 0;
                    string maNPL = "";
                    int soToKhaiNhap = 0;
                    DateTime ngayDangKyNhap = new DateTime(1900, 1, 1);
                    DateTime ngayDangKyXuat = new DateTime(1900, 1, 1);
                    BCThueXNKCollection bcThueXNKCollectin = new BCThueXNKCollection();
                    BCThueXNK bcThueXNK = new BCThueXNK(); ;
                    int index = getBKNPLTaiXuat();
                    decimal temp1 = 0;
                    foreach (BCXuatNhapTon bc in bcXNTCollection)
                    {

                        if (maNPL.ToUpper() == bc.MaNPL.ToUpper() && soToKhaiNhap == bc.SoToKhaiNhap && ngayDangKyNhap == bc.NgayDangKyNhap && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                        {

                            if (bc.LuongTonCuoi < 0)
                            {
                                temp1 = bc.LuongTonCuoi;
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung + bc.LuongTonCuoi;
                                bcThueXNK.LuongNPLTon = 0;
                            }
                            else
                            {
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung;
                                bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                            }
                            //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                            bcThueXNKCollectin[bcThueXNKCollectin.Count - 1] = bcThueXNK;
                        }
                        else
                        {
                            if (bc.LuongNPLTaiXuat > 0)
                            {
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiTaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayTaiXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayTaiXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;
                                bcThueXNK.LuongNPLSuDung = bc.LuongNPLTaiXuat;
                                bcThueXNK.LuongNPLTon = bc.LuongTonDau - bc.LuongNPLTaiXuat;
                                bcThueXNKCollectin.Add(bcThueXNK);
                            }
                            else
                            {
                                if (maNPL.ToUpper().Trim() != bc.MaNPL.ToUpper().Trim()) temp1 = 0;
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.TenNPL = bc.TenNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                //DataRow dr1 = GetDonGia_TyGia_ThueSuat_ThueNK(dtNPLNhapTon, bcThueXNK.SoToKhaiNhap, bcThueXNK.NgayDangKyNhap, bcThueXNK.MaNPL);
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayDangKyXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayHoanThanhXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;
                                if (temp1 < 0 && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                                {
                                    if (0 - temp1 <= bc.LuongTonDau)
                                    {
                                        bcThueXNK.LuongNPLSuDung = 0 - temp1;
                                        bcThueXNK.LuongNPLTon = bc.LuongTonDau - bcThueXNK.LuongNPLSuDung;
                                        temp1 = 0;
                                    }
                                    else
                                    {
                                        bcThueXNK.LuongNPLSuDung = bc.LuongTonDau;
                                        bcThueXNK.LuongNPLTon = 0;
                                        temp1 = temp1 + bcThueXNK.LuongNPLSuDung;
                                    }
                                }
                                else
                                {

                                    if (bc.LuongTonCuoi < 0)
                                    {
                                        temp1 = bc.LuongTonCuoi;
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung + bc.LuongTonCuoi;
                                        bcThueXNK.LuongNPLTon = 0;
                                    }
                                    else
                                    {
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung;
                                        bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                                    }
                                }
                                //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                                bcThueXNKCollectin.Add(bcThueXNK);
                                soToKhaiXuat = bcThueXNK.SoToKhaiXuat;
                                ngayDangKyXuat = bcThueXNK.NgayDangKyXuat;
                                soToKhaiNhap = bcThueXNK.SoToKhaiNhap;
                                maNPL = bcThueXNK.MaNPL;
                                ngayDangKyNhap = bc.NgayDangKyNhap;
                            }
                        }

                    }
                    this.InsertBCThueXNK(bcThueXNKCollectin, transaction);
                    this.TrangThaiThanhKhoan = 400;
                    this.UpdateTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
        public DataTable GetDanhSachNPLXuatTonByMaNPL(int SoThapPhanNPL, string maNPL)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT " +
                            "[SoToKhai] " +
                            ",[MaLoaiHinh] " +
                            ",[NamDangKy] " +
                            ",[MaHaiQuan] " +
                            ",[MaSP] " +
                            ",[TenSP] " +
                            ",[TenDVT_SP] " +
                            ",[LuongSP] " +
                            ",[MaNPL] " +
                            ",[DinhMuc] " +
                            ",round(LuongNPL,@SoThapPhanNPL) as LuongNPL  " +
                            ",round(TonNPL,@SoThapPhanNPL) as TonNPL " +
                            ",[BangKeHoSoThanhLy_ID] " +
                        "FROM " +
                            "v_KDT_SXXK_NPLXuatTon " +
                        "WHERE " +
                            "BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID " +
                            "AND MaNPL = @MaNPL " +
                        "ORDER BY NgayDangKy,SoToKhai,MaSP";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiXuat()].ID);
            db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(cmd, "@MaNPL", SqlDbType.VarChar, maNPL);
            return db.ExecuteDataSet(cmd).Tables[0];
        }
        public DataTable GetDanhSachNPLNhapTonByMaNPL(int SoThapPhanNPL, string maNPL)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT " +
                            "[SoToKhai] " +
                            ",[MaLoaiHinh] " +
                            ",[NamDangKy] " +
                            ",[MaHaiQuan] " +
                            ",[MaNPL] " +
                            ",[TenNPL] " +
                            ",round(TonDau,@SoThapPhanNPL) as TonDau " +
                            ",[BangKeHoSoThanhLy_ID] " +
                        "FROM " +
                            "v_KDT_SXXK_NPLNhapTon " +
                        "WHERE " +
                            "BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID " +
                            "AND MaNPL = @MaNPL " +
                        "ORDER BY NgayDangKy,SoToKhai";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiNhap()].ID);
            db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(cmd, "@MaNPL", SqlDbType.VarChar, maNPL);
            return db.ExecuteDataSet(cmd).Tables[0];
        }
        public void ChayThanhLy(int SoThapPhanNPL, int nplKoTK, int TKToKhaiNKD, int AmTKTiep)
        {
            DataSet ds = new DataSet();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region Khởi tạo các dữ liệu ban đầu
                    //Lấy danh sách nguyên phụ liệu tồn của các tờ khai nhập có trong bộ hồ sơ thanh khoản
                    string sp = "p_KDT_SXXK_DanhSachNPLNhapTon";
                    DbCommand cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiNhap()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLNhapTon");

                    //Lấy danh sách tất cả NPL quy đổi từ định mức sản phẩm của tất cả tờ khai xuất trong bộ hồ sợ thanh khoản
                    sp = "p_KDT_SXXK_DanhSachNPLXuatTon";
                    cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiXuat()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLXuatTon");

                    //Load danh sách nội dung tất cả bảng kê trong bộ hồ sơ thanh khoản
                    this.LoadBKCollection();
                    this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
                    BCXuatNhapTonCollection bcXNTCollection = new BCXuatNhapTonCollection();

                    DataTable dtNPLXuatTon = ds.Tables["t_NPLXuatTon"];
                    DataTable dtNPLNhapTon = ds.Tables["t_NPLNhapTon"];

                    //Kiểm tra dữ liệu trong danh sách nguyên phụ liệu tờ khai nhập
                    foreach (DataRow dr in dtNPLNhapTon.Rows)
                    {
                        if (dr["ThueXNK"] == DBNull.Value)
                            throw new Exception("Mã NPL " + dr["MaNPL"] + " của tờ khai nhập số " + dr["SoToKhai"].ToString() + "/" + dr["NamDangKy"] + " chưa có trong danh sách nguyên phụ liệu nhập tồn.");
                    }

                    //Tạo table báo cáo nhập xuất tồn
                    DataTable dtBCXuatNhapTon = new DataTable();
                    DataColumn[] cols = new DataColumn[33];
                    cols[0] = new DataColumn("LanThanhLy", typeof(int));
                    cols[1] = new DataColumn("MaDoanhNghiep", typeof(string));
                    cols[2] = new DataColumn("STT", typeof(long));
                    cols[3] = new DataColumn("MaNPL", typeof(string));
                    cols[4] = new DataColumn("SoToKhaiNhap", typeof(int));
                    cols[5] = new DataColumn("NgayDangKyNhap", typeof(DateTime));
                    cols[6] = new DataColumn("NgayHoanThanhNhap", typeof(DateTime));
                    cols[7] = new DataColumn("LuongNhap", typeof(decimal));
                    cols[8] = new DataColumn("LuongTonDau", typeof(decimal));
                    cols[9] = new DataColumn("TenDVT_NPL", typeof(string));
                    cols[10] = new DataColumn("MaSP", typeof(string));
                    cols[11] = new DataColumn("SoToKhaiXuat", typeof(int));
                    cols[12] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
                    cols[13] = new DataColumn("NgayHoanThanhXuat", typeof(DateTime));
                    cols[14] = new DataColumn("LuongSPXuat", typeof(decimal));
                    cols[15] = new DataColumn("TenDVT_SP", typeof(string));
                    cols[16] = new DataColumn("DinhMuc", typeof(decimal));
                    cols[17] = new DataColumn("LuongNPLSuDung", typeof(decimal));
                    cols[18] = new DataColumn("ToKhaiTaiXuat", typeof(decimal));
                    cols[19] = new DataColumn("LuongNPLTaiXuat", typeof(decimal));
                    cols[20] = new DataColumn("NgayTaiXuat", typeof(DateTime));
                    cols[21] = new DataColumn("LuongTonCuoi", typeof(decimal));
                    cols[22] = new DataColumn("ThanhKhoanTiep", typeof(string));
                    cols[23] = new DataColumn("ChuyenMucDichKhac", typeof(string));
                    cols[24] = new DataColumn("TenNPL", typeof(string));
                    cols[25] = new DataColumn("TenSP", typeof(string));
                    cols[26] = new DataColumn("MaLoaiHinhNhap", typeof(string));
                    cols[27] = new DataColumn("MaLoaiHinhXuat", typeof(string));
                    cols[28] = new DataColumn("DonGiaTT", typeof(double));
                    cols[29] = new DataColumn("TyGiaTT", typeof(decimal));
                    cols[30] = new DataColumn("ThueSuat", typeof(decimal));
                    cols[31] = new DataColumn("ThueXNK", typeof(double));
                    cols[32] = new DataColumn("ThueXNKTon", typeof(double));
                    dtBCXuatNhapTon.Columns.AddRange(cols);
                    #endregion

                    #region Xử lý trước cảng bảng kê trong bộ hồ sơ thanh khoản
                    //Thanh khoản bang ke NPL xin huy
                    int index = this.getBKNPLXinHuy();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXH", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXinHuy bk in this.BKCollection[index].bkNPLXHCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongHuy;
                                    dr["LuongXH"] = bk.LuongHuy;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL tai xuat
                    index = this.getBKNPLTaiXuat();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongTX", typeof(decimal));
                        DataColumn col1 = new DataColumn("SoToKhaiTX", typeof(int));
                        DataColumn col2 = new DataColumn("NgayTX", typeof(DateTime));
                        dtNPLNhapTon.Columns.Add(col);
                        dtNPLNhapTon.Columns.Add(col1);
                        dtNPLNhapTon.Columns.Add(col2);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTaiXuat bk in this.BKCollection[index].bkNPLTXCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongTaiXuat;
                                    drNhapTon["LuongTX"] = bk.LuongTaiXuat;
                                    drNhapTon["SoToKhaiTX"] = bk.SoToKhaiXuat;
                                    drNhapTon["NgayTX"] = bk.NgayDangKyXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " TÁI XUẤT";
                                    dr["SoToKhaiXuat"] = 0;
                                    dr["NgayDangKyXuat"] = new DateTime(1900, 1, 1);
                                    dr["NgayHoanThanhXuat"] = new DateTime(1900, 1, 1);
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = 0;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["ToKhaiTaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayTaiXuat"] = bk.NgayDangKyXuat;
                                    dr["LuongNPLTaiXuat"] = bk.LuongTaiXuat;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL nop thue
                    index = this.getBKNPLNopThue();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongNT", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLNopThueTieuThuNoiDia bk in this.BKCollection[index].bkNPLNTCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongNopThue;
                                    dr["LuongNT"] = bk.LuongNopThue;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL chua thanh ly
                    index = this.getBKNPLChuaThanhLY();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongCTL", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLChuaThanhLy bk in this.BKCollection[index].bkNPLCTLCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.Luong;
                                    dr["LuongCTL"] = bk.Luong;
                                    break;
                                }
                            }
                        }
                    }
                    index = this.getBKNPLTuCungUng();
                    if (index >= 0)
                    {
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTuCungUng bk in this.BKCollection[index].bkNPLTCUCollection)
                        {
                            foreach (DataRow dr in dtNPLXuatTon.Rows)
                            {
                                if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    if (bk.LuongTuCungUng <= Convert.ToDecimal(dr["TonNPL"]))
                                    {
                                        dr["TonNPL"] = Convert.ToDecimal(dr["TonNPL"]) - bk.LuongTuCungUng;
                                        break;
                                    }
                                    else
                                    {
                                        bk.LuongTuCungUng -= Convert.ToDecimal(dr["TonNPL"]);
                                        dr["TonNPL"] = 0;
                                    }

                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL xuat gia cong
                    index = this.getBKNPLXuatGiaCong();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXGC", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXuatGiaCong bk in this.BKCollection[index].bkNPLXGCCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongXuat;
                                    drNhapTon["LuongXGC"] = bk.LuongXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " ";
                                    dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                    dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = bk.LuongXuat;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }
                    //Xử lý bảng kê nhập kinh doanh
                    if (TKToKhaiNKD != 1)
                    {
                        index = this.getBKNPLNhapKinhDoanh();
                        if (index >= 0)
                        {
                            DataColumn col = new DataColumn("LuongNKD", typeof(decimal));
                            dtNPLNhapTon.Columns.Add(col);
                            this.BKCollection[index].LoadChiTietBangKe();
                            foreach (BKNPLXuatSuDungNKD bk in this.BKCollection[index].bkNPLNKDCollection)
                            {
                                foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                                {
                                    if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                        bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                    {
                                        drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongSuDung;
                                        drNhapTon["LuongNKD"] = bk.LuongSuDung;
                                        DataRow dr = dtBCXuatNhapTon.NewRow();
                                        dr["LanThanhLy"] = this.LanThanhLy;
                                        dr["MaNPL"] = bk.MaNPL;
                                        Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                        NPL.Ma = bk.MaNPL;
                                        NPL.MaDoanhNghiep = this.MaDoanhNghiep;
                                        NPL.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        NPL.Load();
                                        dr["TenNPL"] = NPL.Ten;
                                        dr["SoToKhaiNhap"] = bk.SoToKhai;
                                        dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                        dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                        dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                        dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                        dr["STT"] = 0;
                                        dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                        dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                        dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                        dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                        dr["MaSP"] = bk.MaSP;
                                        Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                        SP.Ma = bk.MaSP;
                                        SP.MaDoanhNghiep = this.MaDoanhNghiep;
                                        SP.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        SP.Load();
                                        dr["TenSP"] = SP.Ten;
                                        dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                        dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                        dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                        dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                        dr["LuongSPXuat"] = 0;
                                        dr["LuongNPLSuDung"] = bk.LuongSuDung;
                                        dr["TenDVT_SP"] = DonViTinh.GetName(SP.DVT_ID);
                                        Company.BLL.SXXK.DinhMuc DM = new Company.BLL.SXXK.DinhMuc();
                                        DM.MaSanPHam = bk.MaSP;
                                        DM.MaNguyenPhuLieu = bk.MaNPL;
                                        DM.MaDoanhNghiep = this.MaDoanhNghiep;
                                        DM.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        DM.Load();
                                        dr["DinhMuc"] = DM.DinhMucChung;
                                        dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                        dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                        dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                        dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                        dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        break;
                                    }
                                }
                                foreach (DataRow drXuatTon in dtNPLXuatTon.Rows)
                                {
                                    if (bk.SoToKhaiXuat == Convert.ToInt32(drXuatTon["SoToKhai"]) && bk.MaLoaiHinhXuat == Convert.ToString(drXuatTon["MaLoaiHinh"]) && bk.NamDangKyXuat == Convert.ToInt16(drXuatTon["NamDangKy"]) &&
                                        bk.MaHaiQuanXuat == Convert.ToString(drXuatTon["MaHaiQuan"]) && bk.MaSP.ToUpper() == Convert.ToString(drXuatTon["MaSP"]).ToUpper() && bk.MaNPL.ToUpper() == Convert.ToString(drXuatTon["MaNPL"]).ToUpper())
                                    {
                                        drXuatTon["TonNPL"] = Convert.ToDecimal(drXuatTon["TonNPL"]) - bk.LuongSuDung;
                                        drXuatTon["LuongNPL"] = Convert.ToDecimal(drXuatTon["LuongNPL"]) - bk.LuongSuDung;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Thực hiện việc thanh khoản
                    //Duyệt từng dòng trong danh sách nguyên phụ liệu xuất
                    foreach (DataRow dr1 in dtNPLXuatTon.Rows)
                    {

                        decimal luongAm = 0;//Biến chứa lượng nguyên phụ liệu âm sau khi trừ gán bằng 0 trước khi xử lý 1 dòng trong danh sách NPL xuất
                        int j = -1;//Biến đánh dấu vị trí npl nhập
                        //Duyệt từng dòng trong danh sách nguyên phụ liệu nhập
                        for (int i = 0; i < dtNPLNhapTon.Rows.Count; i++)
                        {
                            DataRow dr2 = dtNPLNhapTon.Rows[i];
                            //Nếu tờ khai nhập kinh doanh thì bỏ qua
                            //tham số TKToKhaiNKD == 1 là thanh khoản xem tờ khai NKD như NSXXK 
                            //tham số TKToKhaiNKD == 0 là bỏ qua tờ khai NKD
                            if (TKToKhaiNKD != 1)
                            {
                                if (dr2["MaLoaiHinh"].ToString().Contains("NKD"))
                                {
                                    continue;
                                }
                            }
                            //Nếu ngày thực nhập tờ khai nhập > ngày đăng ký tờ khai xuất thì bỏ qua tờ khai nhập
                            DateTime ngaytokhaixuat = Convert.ToDateTime(dr1["NgayDangKy"]);
                            if (dr1["NgayThucXuat"] != null && Convert.ToDateTime(dr1["NgayThucXuat"]).Year > 1900)
                                ngaytokhaixuat = Convert.ToDateTime(dr1["NgayThucXuat"]);
                            if (Convert.ToDateTime(dr2["NgayThucNhap"]) <= ngaytokhaixuat)
                            {


                                if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim() && Convert.ToDecimal(dr2["TonCuoi"]) > 0 && Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                {
                                    //Nếu mã NPL giống nhau trong NPL xuất và nhập và lượng tồn NPL nhập và tồn của npl xuất > 0
                                    //Tạo thêm 1 dòng mới trong báo cáo nhập xuất tồn và gán giá trị cho các cột
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                    dr["TenNPL"] = dr2["TenNPL"].ToString();
                                    dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                    dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                    dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]);
                                    dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                    dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                    dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                    dr["MaSP"] = dr1["MaSP"].ToString();
                                    dr["TenSP"] = dr1["TenSP"].ToString();
                                    dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                    dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                    dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);
                                    dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                    dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]);
                                    dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]);
                                    dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                    dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);


                                    if (Convert.ToDecimal(dr2["TonCuoi"]) >= Convert.ToDecimal(dr1["TonNPL"]))
                                    {
                                        //Nếu tồn cuối của npl nhập mà >= tồn của npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lượng âm < 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                        }
                                        else
                                        {
                                            //Ngược lại
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]);
                                        }
                                        dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 ==> đã xử lý xong dòng npl xuất này 
                                        dtBCXuatNhapTon.Rows.Add(dr);//Add thêm 1 dòng vào báo cáo
                                        if (AmTKTiep == 1)
                                        {
                                            //Am thanh khoan tiếp
                                            if (Convert.ToDecimal(dr2["TonCuoi"]) == 0)
                                            {
                                                //Nếu lượng tồn npl này hết
                                                dr2["LanDieuChinh"] = 100;//đánh dấu bằng con số 100 để biết npl của tờ khai này là đứng cuối cùng trong danh sách npl có tồn =0
                                                for (int k = 0; k < i; k++)
                                                    if (dtNPLNhapTon.Rows[k]["MaNPL"].ToString().ToUpper().Trim() == dr2["MaNPL"].ToString().ToUpper().Trim()) dtNPLNhapTon.Rows[k]["LanDieuChinh"] = 0;// Đánh dấu các npl của tờ khai trước đó bằng 0
                                            }
                                        }
                                        break;//Thoát khỏi vòng lập
                                    }
                                    else
                                    {
                                        //Nếu lượng tồn npl nhập < lượng tồn npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lương âm vẫn còn nhỏ hơn 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                        }
                                        {
                                            //Nếu lớn hơn 0
                                            decimal temp = Convert.ToDecimal(dr1["TonNPL"]);
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - temp;
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);

                                        }
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        j = i;// đánh dấu vị trị bằng i

                                    }
                                }
                                else
                                {

                                    //Am thanh khoan tiep
                                    if (AmTKTiep == 1)
                                    {
                                        if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim() && CheckNPLSau(dtNPLNhapTon, dr2["MaNPL"].ToString().ToLower(), i, Convert.ToDateTime(dr1["NgayDangKy"])) && (Convert.ToDecimal(dr2["TonCuoi"]) < 0 || Convert.ToInt32(dr2["LanDieuChinh"]) == 100) && Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                        {

                                            DataRow dr = dtBCXuatNhapTon.NewRow();
                                            dr["LanThanhLy"] = this.LanThanhLy;
                                            dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                            dr["STT"] = 0;
                                            dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                            dr["TenNPL"] = dr2["TenNPL"].ToString();
                                            dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                            dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                            dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]);
                                            dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                            dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]);
                                            dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]);
                                            dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                            dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                            dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                            dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                            dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                            dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                            dr["MaSP"] = dr1["MaSP"].ToString();
                                            dr["TenSP"] = dr1["TenSP"].ToString();
                                            dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                            dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                            dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);
                                            dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                            dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]);
                                            dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]);
                                            dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                            dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                            dr["ThanhKhoanTiep"] = "Mua tại VN";
                                            dtBCXuatNhapTon.Rows.Add(dr);
                                            dr1["TonNPL"] = 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (Convert.ToDecimal(dr1["TonNPL"]) > 0)
                        {
                            //Nếu hết tất cả tờ khai nhập mà lượng tồn xuất ra vẫn còn dương
                            if (j >= 0)
                            {
                                //Nếu biến j >0 
                                dtNPLNhapTon.Rows[j]["TonCuoi"] = 0 - Convert.ToDecimal(dr1["TonNPL"]);//Gán lại lượng tồn cuối của npl cuối cùng thanh khoản cho npl xuất này
                                dtNPLNhapTon.Rows[j]["LanDieuChinh"] = 100;
                                dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 để bỏ qua npl xuất này
                            }

                        }
                    }

                    if (dtBCXuatNhapTon.Rows.Count == 0) throw new Exception("Không có tờ khai nhập nào tham gia thanh khoản.\nHãy kiểm tra lại định mức của tờ khai nhập, kiểm tra lại ngày thực nhập của tờ khai nhập có nhỏ hơn ngày đăng ký của tờ khai xuất.");
                    #endregion

                    #region Insert dtNPLNhapTon into Database
                    new NPLNhapTon().DeleteDynamicTransaction(transaction, " LanThanhLy=" + this.LanThanhLy + " AND MaDoanhNghiep = '" + MaDoanhNghiep + "'");
                    index = this.getBKNPLChuaThanhLY();
                    //Xử lý bảng kê chưa thanh khoản khi insert vào CSDL cộng lượng tồn lại
                    if (index >= 0)
                    {
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            NPLNhapTon nplNhapTon = new NPLNhapTon();
                            nplNhapTon.LanThanhLy = this.LanThanhLy;
                            nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                            nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                            nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                            nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                            nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                            nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                            nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            if (dr["LuongCTL"].ToString() != "")
                            {
                                if (Convert.ToDecimal(dr["TonCuoi"]) >= 0)
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]) + Convert.ToDecimal(dr["LuongCTL"]);
                                }
                                else
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["LuongCTL"]);
                                }
                            }
                            else
                                nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                            if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                            nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                            nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                            if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                            else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.ThueXNK / (double)nplNhapTon.Luong, 0);
                            nplNhapTon.InsertTransaction(transaction);
                        }
                    }
                    else
                    {
                        //Insert vào bang t_KDT_SXXK_NPLNhapTon
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            NPLNhapTon nplNhapTon = new NPLNhapTon();
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTon.LanThanhLy = this.LanThanhLy;
                            nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                            nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                            nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                            nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                            nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                            nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                            nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                            nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                            if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                            nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                            nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                            if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                            else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.TonDauThueXNK / (double)nplNhapTon.TonDau, 0);//tồn cuối thuế XNK = tồn đầu thuế XNK - số tiền thuế thanh khoản
                            nplNhapTon.InsertTransaction(transaction);
                        }
                    }
                    #endregion

                    #region insert BCXuatNhapTon
                    //Xử lý để insert DataTable dtBCXuatNhapTon vào bảng t_KDT_SXXK_BCXuatNhapTon
                    dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                    if (TKToKhaiNKD != 1)
                    {

                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap LIKE 'NKD%'";
                    }
                    int STT = 0;
                    string maNPL = "";
                    int soToKhaiNhap = 0;
                    string maLoaiHinhNhap = "";
                    DateTime ngayDangKyNhap = new DateTime(1900, 1, 1);

                    for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                    {
                        DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                        if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                        BCXuatNhapTon bc = new BCXuatNhapTon();

                        bc.STT = STT;
                        if (rv["ChuyenMucDichKhac"].ToString() != "")
                            bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                        bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                        bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                        bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                        bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                        if (rv["LuongNPLTaiXuat"].ToString() != "")
                            bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                        bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                        bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                        bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                        bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                        maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                        bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                        bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                        bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                        bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                        bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                        bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                        bc.MaSP = Convert.ToString(rv["MaSP"]);
                        bc.TenSP = Convert.ToString(rv["TenSP"]);
                        bc.NamThanhLy = DateTime.Today.Year;
                        ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                        bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                        bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                        bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                        bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                        bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                        soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                        bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                        if (rv["ToKhaiTaiXuat"].ToString() != "")
                        {
                            bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                            bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                        }
                        bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                        bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                        if (rv["ThanhKhoanTiep"].ToString() != "")
                            bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                        else
                        {
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                        }
                        //Xử lý ghi chú là mua VN, chuyển lần sau TK hay chuyển TK xyz
                        if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                        {
                            if (TKToKhaiNKD == 1)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                        }
                        bcXNTCollection.Add(bc);

                    }
                    if (TKToKhaiNKD != 1)
                    {
                        dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap NOT LIKE 'NKD%'";
                        STT = 0;
                        maNPL = "";
                        soToKhaiNhap = 0;
                        maLoaiHinhNhap = "";
                        ngayDangKyNhap = new DateTime(1900, 1, 1);

                        for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                        {
                            DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                            if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                            BCXuatNhapTon bc = new BCXuatNhapTon();

                            bc.STT = STT;
                            if (rv["ChuyenMucDichKhac"].ToString() != "")
                                bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                            bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                            bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                            bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                            if (rv["LuongNPLTaiXuat"].ToString() != "")
                                bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                            bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                            bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                            bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                            bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                            maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                            bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                            bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                            bc.MaSP = Convert.ToString(rv["MaSP"]);
                            bc.TenSP = Convert.ToString(rv["TenSP"]);
                            bc.NamThanhLy = DateTime.Today.Year;
                            ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                            bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                            bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                            bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                            bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                            bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                            soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                            bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                            if (rv["ToKhaiTaiXuat"].ToString() != "")
                            {
                                bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                                bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                            }
                            bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                            bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                            if (rv["ThanhKhoanTiep"].ToString() != "")
                                bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                            else
                            {
                                if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                    bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            }
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }


                                        }


                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                            bcXNTCollection.Add(bc);

                        }
                    }

                    #region Thêm NPL ko tham gia vào thanh khoản
                    if (nplKoTK == 1)
                    {
                        DataRow[] rows = dtNPLNhapTon.Select(" TonDau = TonCuoi AND TonDau > 0");
                        for (int i = 0; i < rows.Length; i++)
                        {
                            DataRow row = rows[i];
                            if (CheckTKExist(bcXNTCollection, Convert.ToInt32(row["SoToKhai"]), row["MaLoaiHinh"].ToString(), Convert.ToDateTime(row["NgayDangKy"])))
                            {
                                BCXuatNhapTon bc = new BCXuatNhapTon();
                                bc.LanThanhLy = this.LanThanhLy;
                                bc.NamThanhLy = this.NgayBatDau.Year;
                                bc.MaDoanhNghiep = this.MaDoanhNghiep;
                                bc.MaNPL = row["MaNPL"].ToString();
                                bc.TenNPL = row["TenNPL"].ToString();
                                bc.TenDVT_NPL = row["TenDVT_NPL"].ToString();
                                bc.SoToKhaiNhap = Convert.ToInt32(row["SoToKhai"]);
                                bc.NgayDangKyNhap = Convert.ToDateTime(row["NgayDangKy"]);
                                bc.NgayHoanThanhNhap = bc.NgayDangKyNhap;
                                bc.MaLoaiHinhNhap = row["MaLoaiHinh"].ToString();
                                bc.LuongNhap = Convert.ToDecimal(row["Luong"]);
                                bc.LuongTonDau = Convert.ToDecimal(row["TonDau"]);
                                bc.LuongNPLSuDung = 0;
                                bc.LuongTonCuoi = Convert.ToDecimal(row["TonDau"]);
                                bc.DonGiaTT = Convert.ToDouble(row["DonGiaTT"]);
                                bc.TyGiaTT = Convert.ToDecimal(row["TyGiaTT"]);
                                bc.ThueSuat = Convert.ToDecimal(row["ThueSuat"]);
                                bc.ThueXNK = Convert.ToDouble(row["ThueXNK"]);
                                bc.ThueXNKTon = Convert.ToDouble(row["TonDauThueXNK"]);
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                                bcXNTCollection.Add(bc);
                            }
                        }
                    }
                    #endregion
                    this.InsertBCXNT(bcXNTCollection, transaction);

                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    maLoaiHinhNhap = "";
                    bool temp3 = false;

                    //Xử lý collection bcXNTCollection trước khi đưa vào xử lý báo cáo này để tổng hợp nên báo cáo thuế XNK
                    BCXuatNhapTonCollection bcTemp = new BCXuatNhapTonCollection();
                    for (int i = 0; i < bcXNTCollection.Count; i++)
                    {
                        if (maNPL == bcXNTCollection[i].MaNPL && soToKhaiNhap == bcXNTCollection[i].SoToKhaiNhap && ngayDangKyNhap == bcXNTCollection[i].NgayDangKyNhap && maLoaiHinhNhap == bcXNTCollection[i].MaLoaiHinhNhap)
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0)
                            {
                                if (!temp3)
                                    temp3 = true;
                                else
                                    bcTemp.Add(bcXNTCollection[i]);

                            }
                            else
                            {
                                temp3 = false;
                            }
                        }
                        else
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0) temp3 = true;
                            else temp3 = false;
                            maNPL = bcXNTCollection[i].MaNPL;
                            soToKhaiNhap = bcXNTCollection[i].SoToKhaiNhap;
                            ngayDangKyNhap = bcXNTCollection[i].NgayDangKyNhap;
                            maLoaiHinhNhap = bcXNTCollection[i].MaLoaiHinhNhap;
                        }
                    }
                    for (int i = 0; i < bcTemp.Count; i++)
                    {
                        bcXNTCollection.Remove(bcTemp[i]);
                    }

                    #endregion

                    #region insert BCThueXNK
                    int soToKhaiXuat = 0;
                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    DateTime ngayDangKyXuat = new DateTime(1900, 1, 1);
                    BCThueXNKCollection bcThueXNKCollectin = new BCThueXNKCollection();
                    BCThueXNK bcThueXNK = new BCThueXNK(); ;
                    STT = 0;
                    index = getBKNPLTaiXuat();
                    decimal temp1 = 0;//Biến đánh dấu khi âm NPL

                    //Duyệt từng dòng trong collection bcXNTCollection
                    foreach (BCXuatNhapTon bc in bcXNTCollection)
                    {

                        if (maNPL.ToUpper() == bc.MaNPL.ToUpper() && soToKhaiNhap == bc.SoToKhaiNhap && ngayDangKyNhap == bc.NgayDangKyNhap && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                        {
                            //Nếu đây là tờ khai nhập đã có trong collection bcThueXNCollection
                            if (bc.LuongTonCuoi < 0)
                            {
                                //Nếu lượng tồn cuối nhỏ hơn 0
                                temp1 = bc.LuongTonCuoi; //Gán biến temp1
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung + bc.LuongTonCuoi;
                                bcThueXNK.LuongNPLTon = 0;
                            }
                            else
                            {
                                //Nếu lớn hơn hoặc bằng 0
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung;
                                bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                            }
                            //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                            bcThueXNKCollectin[bcThueXNKCollectin.Count - 1] = bcThueXNK;
                        }
                        else
                        {
                            if (index > 0 && bc.LuongNPLTaiXuat > 0)
                            {
                                //Nếu là tờ khai tái xuất
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                //DataRow dr = GetDonGia_TyGia_ThueSuat_ThueNK(dtNPLNhapTon, bcThueXNK.SoToKhaiNhap, bcThueXNK.NgayDangKyNhap, bcThueXNK.MaNPL);
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiTaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayTaiXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayTaiXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;
                                bcThueXNK.LuongNPLSuDung = bc.LuongNPLTaiXuat;
                                bcThueXNK.LuongNPLTon = bc.LuongTonDau - bc.LuongNPLTaiXuat;
                                //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                                bcThueXNKCollectin.Add(bcThueXNK);
                            }
                            else
                            {

                                if (maNPL.ToUpper().Trim() != bc.MaNPL.ToUpper().Trim()) temp1 = 0;//Nếu đây là npl khác thì gán temp1 =0
                                //Tạo 1 dòng báo cáo thuế XNK và gán dữ liệu 
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.TenNPL = bc.TenNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayDangKyXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayHoanThanhXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;

                                if (temp1 < 0 && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                                {
                                    //Nếu temp1 <0 và tờ khai xuất dòng trên giống dòng hiện tại
                                    if (0 - temp1 <= bc.LuongTonDau)
                                    {
                                        //Nếu lượng âm mà nhỏ hơn lượng tồn đầu của dòng hiện tại
                                        bcThueXNK.LuongNPLSuDung = 0 - temp1;// Gán lượng sử dụng bằng lượng âm
                                        bcThueXNK.LuongNPLTon = bc.LuongTonDau - bcThueXNK.LuongNPLSuDung;//Gán lượng NPL tồn bằng 
                                        temp1 = 0;
                                    }
                                    else
                                    {
                                        //Nếu lượng âm nhỏ hơn
                                        bcThueXNK.LuongNPLSuDung = bc.LuongTonDau;//Gán lượng sử dụng bằng lượng tồn đầu ==> npl tờ khai nhập này dùng hết
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn = 0
                                        temp1 = temp1 + bcThueXNK.LuongNPLSuDung;// Gán lại biến temp
                                    }
                                }
                                else
                                {

                                    if (bc.LuongTonCuoi < 0)
                                    {
                                        temp1 = bc.LuongTonCuoi;//Gán lại biến temp1
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung + bc.LuongTonCuoi;//Gán npl sử dụng bằng chính nó + thêm lượng tồn cuối
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn bằng 0
                                    }
                                    else
                                    {
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung;
                                        bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                                    }
                                }
                                bcThueXNKCollectin.Add(bcThueXNK);//Thêm 1 dòng vào báo cáo thuế
                                //Lưu lại tờ khai nhập xuất đang xử lý
                                soToKhaiXuat = bcThueXNK.SoToKhaiXuat;
                                ngayDangKyXuat = bcThueXNK.NgayDangKyXuat;
                                soToKhaiNhap = bcThueXNK.SoToKhaiNhap;
                                maNPL = bcThueXNK.MaNPL;
                                ngayDangKyNhap = bc.NgayDangKyNhap;
                            }
                        }

                    }
                    this.InsertBCThueXNK(bcThueXNKCollectin, transaction);
                    #endregion

                    transaction.Commit();
                    this.TrangThaiThanhKhoan = 400;

                }
                catch (Exception ex)
                {
                    this.TrangThaiThanhKhoan = 100;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void ChayThanhLy(int SoThapPhanNPL, int nplKoTK, int TKToKhaiNKD, int AmTKTiep, int chenhLechNgay)
        {
            DataSet ds = new DataSet();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region Khởi tạo các dữ liệu ban đầu
                    //Lấy danh sách nguyên phụ liệu tồn của các tờ khai nhập có trong bộ hồ sơ thanh khoản
                    string sp = "p_KDT_SXXK_DanhSachNPLNhapTon";
                    DbCommand cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiNhap()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLNhapTon");

                    //Lấy danh sách tất cả NPL quy đổi từ định mức sản phẩm của tất cả tờ khai xuất trong bộ hồ sợ thanh khoản
                    sp = "p_KDT_SXXK_DanhSachNPLXuatTon";
                    cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiXuat()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLXuatTon");

                    //Load danh sách nội dung tất cả bảng kê trong bộ hồ sơ thanh khoản
                    this.LoadBKCollection();
                    this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
                    BCXuatNhapTonCollection bcXNTCollection = new BCXuatNhapTonCollection();

                    DataTable dtNPLXuatTon = ds.Tables["t_NPLXuatTon"];
                    DataTable dtNPLNhapTon = ds.Tables["t_NPLNhapTon"];

                    //Kiểm tra dữ liệu trong danh sách nguyên phụ liệu tờ khai nhập
                    foreach (DataRow dr in dtNPLNhapTon.Rows)
                    {
                        if (dr["ThueXNK"] == DBNull.Value)
                            throw new Exception("Mã NPL " + dr["MaNPL"] + " của tờ khai nhập số " + dr["SoToKhai"].ToString() + "/" + dr["NamDangKy"] + " chưa có trong danh sách nguyên phụ liệu nhập tồn.");
                    }

                    //Tạo table báo cáo nhập xuất tồn
                    DataTable dtBCXuatNhapTon = new DataTable();
                    DataColumn[] cols = new DataColumn[33];
                    cols[0] = new DataColumn("LanThanhLy", typeof(int));
                    cols[1] = new DataColumn("MaDoanhNghiep", typeof(string));
                    cols[2] = new DataColumn("STT", typeof(long));
                    cols[3] = new DataColumn("MaNPL", typeof(string));
                    cols[4] = new DataColumn("SoToKhaiNhap", typeof(int));
                    cols[5] = new DataColumn("NgayDangKyNhap", typeof(DateTime));
                    cols[6] = new DataColumn("NgayHoanThanhNhap", typeof(DateTime));
                    cols[7] = new DataColumn("LuongNhap", typeof(decimal));
                    cols[8] = new DataColumn("LuongTonDau", typeof(decimal));
                    cols[9] = new DataColumn("TenDVT_NPL", typeof(string));
                    cols[10] = new DataColumn("MaSP", typeof(string));
                    cols[11] = new DataColumn("SoToKhaiXuat", typeof(int));
                    cols[12] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
                    cols[13] = new DataColumn("NgayHoanThanhXuat", typeof(DateTime));
                    cols[14] = new DataColumn("LuongSPXuat", typeof(decimal));
                    cols[15] = new DataColumn("TenDVT_SP", typeof(string));
                    cols[16] = new DataColumn("DinhMuc", typeof(decimal));
                    cols[17] = new DataColumn("LuongNPLSuDung", typeof(decimal));
                    cols[18] = new DataColumn("ToKhaiTaiXuat", typeof(decimal));
                    cols[19] = new DataColumn("LuongNPLTaiXuat", typeof(decimal));
                    cols[20] = new DataColumn("NgayTaiXuat", typeof(DateTime));
                    cols[21] = new DataColumn("LuongTonCuoi", typeof(decimal));
                    cols[22] = new DataColumn("ThanhKhoanTiep", typeof(string));
                    cols[23] = new DataColumn("ChuyenMucDichKhac", typeof(string));
                    cols[24] = new DataColumn("TenNPL", typeof(string));
                    cols[25] = new DataColumn("TenSP", typeof(string));
                    cols[26] = new DataColumn("MaLoaiHinhNhap", typeof(string));
                    cols[27] = new DataColumn("MaLoaiHinhXuat", typeof(string));
                    cols[28] = new DataColumn("DonGiaTT", typeof(double));
                    cols[29] = new DataColumn("TyGiaTT", typeof(decimal));
                    cols[30] = new DataColumn("ThueSuat", typeof(decimal));
                    cols[31] = new DataColumn("ThueXNK", typeof(double));
                    cols[32] = new DataColumn("ThueXNKTon", typeof(double));
                    dtBCXuatNhapTon.Columns.AddRange(cols);
                    #endregion

                    #region Xử lý trước cảng bảng kê trong bộ hồ sơ thanh khoản
                    //Thanh khoản bang ke NPL xin huy
                    int index = this.getBKNPLXinHuy();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXH", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXinHuy bk in this.BKCollection[index].bkNPLXHCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongHuy;
                                    dr["LuongXH"] = bk.LuongHuy;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL tai xuat
                    index = this.getBKNPLTaiXuat();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongTX", typeof(decimal));
                        DataColumn col1 = new DataColumn("SoToKhaiTX", typeof(int));
                        DataColumn col2 = new DataColumn("NgayTX", typeof(DateTime));
                        dtNPLNhapTon.Columns.Add(col);
                        dtNPLNhapTon.Columns.Add(col1);
                        dtNPLNhapTon.Columns.Add(col2);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTaiXuat bk in this.BKCollection[index].bkNPLTXCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongTaiXuat;
                                    drNhapTon["LuongTX"] = bk.LuongTaiXuat;
                                    drNhapTon["SoToKhaiTX"] = bk.SoToKhaiXuat;
                                    drNhapTon["NgayTX"] = bk.NgayDangKyXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " TÁI XUẤT";
                                    dr["SoToKhaiXuat"] = 0;
                                    dr["NgayDangKyXuat"] = new DateTime(1900, 1, 1);
                                    dr["NgayHoanThanhXuat"] = new DateTime(1900, 1, 1);
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = 0;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["ToKhaiTaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayTaiXuat"] = bk.NgayDangKyXuat;
                                    dr["LuongNPLTaiXuat"] = bk.LuongTaiXuat;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL nop thue
                    index = this.getBKNPLNopThue();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongNT", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLNopThueTieuThuNoiDia bk in this.BKCollection[index].bkNPLNTCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongNopThue;
                                    dr["LuongNT"] = bk.LuongNopThue;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL chua thanh ly
                    index = this.getBKNPLChuaThanhLY();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongCTL", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLChuaThanhLy bk in this.BKCollection[index].bkNPLCTLCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.Luong;
                                    dr["LuongCTL"] = bk.Luong;
                                    break;
                                }
                            }
                        }
                    }
                    index = this.getBKNPLTuCungUng();
                    if (index >= 0)
                    {
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTuCungUng bk in this.BKCollection[index].bkNPLTCUCollection)
                        {
                            foreach (DataRow dr in dtNPLXuatTon.Rows)
                            {
                                if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    if (bk.LuongTuCungUng <= Convert.ToDecimal(dr["TonNPL"]))
                                    {
                                        dr["TonNPL"] = Convert.ToDecimal(dr["TonNPL"]) - bk.LuongTuCungUng;
                                        break;
                                    }
                                    else
                                    {
                                        bk.LuongTuCungUng -= Convert.ToDecimal(dr["TonNPL"]);
                                        dr["TonNPL"] = 0;
                                    }

                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL xuat gia cong
                    index = this.getBKNPLXuatGiaCong();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXGC", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXuatGiaCong bk in this.BKCollection[index].bkNPLXGCCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongXuat;
                                    drNhapTon["LuongXGC"] = bk.LuongXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " ";
                                    dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                    dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = bk.LuongXuat;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }
                    //Xử lý bảng kê nhập kinh doanh
                    if (TKToKhaiNKD != 1)
                    {
                        index = this.getBKNPLNhapKinhDoanh();
                        if (index >= 0)
                        {
                            DataColumn col = new DataColumn("LuongNKD", typeof(decimal));
                            dtNPLNhapTon.Columns.Add(col);
                            this.BKCollection[index].LoadChiTietBangKe();
                            foreach (BKNPLXuatSuDungNKD bk in this.BKCollection[index].bkNPLNKDCollection)
                            {
                                foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                                {
                                    if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                        bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                    {
                                        drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongSuDung;
                                        drNhapTon["LuongNKD"] = bk.LuongSuDung;
                                        DataRow dr = dtBCXuatNhapTon.NewRow();
                                        dr["LanThanhLy"] = this.LanThanhLy;
                                        dr["MaNPL"] = bk.MaNPL;
                                        Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                        NPL.Ma = bk.MaNPL;
                                        NPL.MaDoanhNghiep = this.MaDoanhNghiep;
                                        NPL.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        NPL.Load();
                                        dr["TenNPL"] = NPL.Ten;
                                        dr["SoToKhaiNhap"] = bk.SoToKhai;
                                        dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                        dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                        dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                        dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                        dr["STT"] = 0;
                                        dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                        dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                        dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                        dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                        dr["MaSP"] = bk.MaSP;
                                        Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                        SP.Ma = bk.MaSP;
                                        SP.MaDoanhNghiep = this.MaDoanhNghiep;
                                        SP.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        SP.Load();
                                        dr["TenSP"] = SP.Ten;
                                        dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                        dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                        dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                        dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                        dr["LuongSPXuat"] = 0;
                                        dr["LuongNPLSuDung"] = bk.LuongSuDung;
                                        dr["TenDVT_SP"] = DonViTinh.GetName(SP.DVT_ID);
                                        Company.BLL.SXXK.DinhMuc DM = new Company.BLL.SXXK.DinhMuc();
                                        DM.MaSanPHam = bk.MaSP;
                                        DM.MaNguyenPhuLieu = bk.MaNPL;
                                        DM.MaDoanhNghiep = this.MaDoanhNghiep;
                                        DM.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        DM.Load();
                                        dr["DinhMuc"] = DM.DinhMucChung;
                                        dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                        dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                        dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                        dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                        dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        break;
                                    }
                                }
                                foreach (DataRow drXuatTon in dtNPLXuatTon.Rows)
                                {
                                    if (bk.SoToKhaiXuat == Convert.ToInt32(drXuatTon["SoToKhai"]) && bk.MaLoaiHinhXuat == Convert.ToString(drXuatTon["MaLoaiHinh"]) && bk.NamDangKyXuat == Convert.ToInt16(drXuatTon["NamDangKy"]) &&
                                        bk.MaHaiQuanXuat == Convert.ToString(drXuatTon["MaHaiQuan"]) && bk.MaSP.ToUpper() == Convert.ToString(drXuatTon["MaSP"]).ToUpper() && bk.MaNPL.ToUpper() == Convert.ToString(drXuatTon["MaNPL"]).ToUpper())
                                    {
                                        drXuatTon["TonNPL"] = Convert.ToDecimal(drXuatTon["TonNPL"]) - bk.LuongSuDung;
                                        drXuatTon["LuongNPL"] = Convert.ToDecimal(drXuatTon["LuongNPL"]) - bk.LuongSuDung;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Thực hiện việc thanh khoản
                    //Duyệt từng dòng trong danh sách nguyên phụ liệu xuất
                    foreach (DataRow dr1 in dtNPLXuatTon.Rows)
                    {
                        decimal luongAm = 0;//Biến chứa lượng nguyên phụ liệu âm sau khi trừ gán bằng 0 trước khi xử lý 1 dòng trong danh sách NPL xuất
                        int j = -1;//Biến đánh dấu vị trí npl nhập
                        //Duyệt từng dòng trong danh sách nguyên phụ liệu nhập
                        for (int i = 0; i < dtNPLNhapTon.Rows.Count; i++)
                        {
                            DataRow dr2 = dtNPLNhapTon.Rows[i];
                            //Nếu tờ khai nhập kinh doanh thì bỏ qua
                            //tham số TKToKhaiNKD == 1 là thanh khoản xem tờ khai NKD như NSXXK 
                            //tham số TKToKhaiNKD == 0 là bỏ qua tờ khai NKD
                            if (TKToKhaiNKD != 1)
                            {
                                if (dr2["MaLoaiHinh"].ToString().Contains("NKD"))
                                {
                                    continue;
                                }
                            }
                            //Nếu ngày thực nhập tờ khai nhập > ngày đăng ký tờ khai xuất thì bỏ qua tờ khai nhập
                            DateTime ngaytokhaixuat = Convert.ToDateTime(dr1["NgayDangKy"]);
                            //Huypvt: Chinh NgayThucXuat = NgayHoanThanh
                            if (dr1["NgayHoanThanh"] != null && Convert.ToDateTime(dr1["NgayHoanThanh"]).Year > 1900)
                                ngaytokhaixuat = Convert.ToDateTime(dr1["NgayHoanThanh"]);

                            if (Convert.ToDateTime(dr2["NgayThucNhap"]).AddDays((double)chenhLechNgay) <= ngaytokhaixuat)
                            {
                                if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim() && Convert.ToDecimal(dr2["TonCuoi"]) > 0 && Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                {
                                    //Nếu mã NPL giống nhau trong NPL xuất và nhập và lượng tồn NPL nhập và tồn của npl xuất > 0
                                    //Tạo thêm 1 dòng mới trong báo cáo nhập xuất tồn và gán giá trị cho các cột
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                    dr["TenNPL"] = dr2["TenNPL"].ToString();
                                    dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                    dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                    dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]);
                                    dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                    dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                    dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                    dr["MaSP"] = dr1["MaSP"].ToString();
                                    dr["TenSP"] = dr1["TenSP"].ToString();
                                    dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                    dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                    dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayHoanThanh"]);
                                    dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                    dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]);
                                    dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]);
                                    dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                    dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);


                                    if (Convert.ToDecimal(dr2["TonCuoi"]) >= Convert.ToDecimal(dr1["TonNPL"]))
                                    {
                                        //Nếu tồn cuối của npl nhập mà >= tồn của npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lượng âm < 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                        }
                                        else
                                        {
                                            //Ngược lại
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]);
                                        }
                                        dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 ==> đã xử lý xong dòng npl xuất này 
                                        dtBCXuatNhapTon.Rows.Add(dr);//Add thêm 1 dòng vào báo cáo
                                        if (AmTKTiep == 1)
                                        {
                                            //Am thanh khoan tiếp
                                            if (Convert.ToDecimal(dr2["TonCuoi"]) == 0)
                                            {
                                                //Nếu lượng tồn npl này hết
                                                dr2["LanDieuChinh"] = 100;//đánh dấu bằng con số 100 để biết npl của tờ khai này là đứng cuối cùng trong danh sách npl có tồn =0
                                                for (int k = 0; k < i; k++)
                                                    if (dtNPLNhapTon.Rows[k]["MaNPL"].ToString().ToUpper().Trim() == dr2["MaNPL"].ToString().ToUpper().Trim()) dtNPLNhapTon.Rows[k]["LanDieuChinh"] = 0;// Đánh dấu các npl của tờ khai trước đó bằng 0
                                            }
                                        }
                                        break;//Thoát khỏi vòng lập
                                    }
                                    else
                                    {
                                        //Nếu lượng tồn npl nhập < lượng tồn npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lương âm vẫn còn nhỏ hơn 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                        }
                                        {
                                            //Nếu lớn hơn 0
                                            decimal temp = Convert.ToDecimal(dr1["TonNPL"]);
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - temp;
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);

                                        }
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        j = i;// đánh dấu vị trị bằng i

                                    }
                                }
                                else
                                {

                                    //Am thanh khoan tiep
                                    if (AmTKTiep == 1)
                                    {
                                        if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim() && CheckNPLSau(dtNPLNhapTon, dr2["MaNPL"].ToString().ToLower(), i, Convert.ToDateTime(dr1["NgayDangKy"])) && (Convert.ToDecimal(dr2["TonCuoi"]) < 0 || Convert.ToInt32(dr2["LanDieuChinh"]) == 100) && Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                        {

                                            DataRow dr = dtBCXuatNhapTon.NewRow();
                                            dr["LanThanhLy"] = this.LanThanhLy;
                                            dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                            dr["STT"] = 0;
                                            dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                            dr["TenNPL"] = dr2["TenNPL"].ToString();
                                            dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                            dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                            dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]);
                                            dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                            dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]);
                                            dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]);
                                            dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                            dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                            dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                            dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                            dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                            dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                            dr["MaSP"] = dr1["MaSP"].ToString();
                                            dr["TenSP"] = dr1["TenSP"].ToString();
                                            dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                            dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                            dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);
                                            dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                            dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]);
                                            dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]);
                                            dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                            dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                            dr["ThanhKhoanTiep"] = "Mua tại VN";
                                            dtBCXuatNhapTon.Rows.Add(dr);
                                            dr1["TonNPL"] = 0;
                                        }
                                    }
                                }
                            }
                        }
                        if (Convert.ToDecimal(dr1["TonNPL"]) > 0)
                        {
                            //Nếu hết tất cả tờ khai nhập mà lượng tồn xuất ra vẫn còn dương
                            if (j >= 0)
                            {
                                //Nếu biến j >0 
                                dtNPLNhapTon.Rows[j]["TonCuoi"] = 0 - Convert.ToDecimal(dr1["TonNPL"]);//Gán lại lượng tồn cuối của npl cuối cùng thanh khoản cho npl xuất này
                                dtNPLNhapTon.Rows[j]["LanDieuChinh"] = 100;
                                dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 để bỏ qua npl xuất này
                            }

                        }
                    }

                    if (dtBCXuatNhapTon.Rows.Count == 0) throw new Exception("Không có tờ khai nhập nào tham gia thanh khoản.\nHãy kiểm tra lại định mức của tờ khai nhập, kiểm tra lại ngày thực nhập của tờ khai nhập có nhỏ hơn ngày đăng ký của tờ khai xuất.");
                    #endregion

                    #region Insert dtNPLNhapTon into Database
                    new NPLNhapTon().DeleteDynamicTransaction(transaction, " LanThanhLy=" + this.LanThanhLy + " AND MaDoanhNghiep = '" + MaDoanhNghiep + "'");
                    index = this.getBKNPLChuaThanhLY();
                    //Xử lý bảng kê chưa thanh khoản khi insert vào CSDL cộng lượng tồn lại
                    if (index >= 0)
                    {
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            NPLNhapTon nplNhapTon = new NPLNhapTon();
                            nplNhapTon.LanThanhLy = this.LanThanhLy;
                            nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                            nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                            nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                            nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                            nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                            nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                            nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            if (dr["LuongCTL"].ToString() != "")
                            {
                                if (Convert.ToDecimal(dr["TonCuoi"]) >= 0)
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]) + Convert.ToDecimal(dr["LuongCTL"]);
                                }
                                else
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["LuongCTL"]);
                                }
                            }
                            else
                                nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                            if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                            nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                            nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                            if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                            else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.ThueXNK / (double)nplNhapTon.Luong, 0);
                            nplNhapTon.InsertTransaction(transaction);
                        }
                    }
                    else
                    {
                        //Insert vào bang t_KDT_SXXK_NPLNhapTon
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            NPLNhapTon nplNhapTon = new NPLNhapTon();
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTon.LanThanhLy = this.LanThanhLy;
                            nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                            nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                            nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                            nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                            nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                            nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                            nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                            nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                            if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                            nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                            nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                            if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                            else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.TonDauThueXNK / (double)nplNhapTon.TonDau, 0);//tồn cuối thuế XNK = tồn đầu thuế XNK - số tiền thuế thanh khoản
                            nplNhapTon.InsertTransaction(transaction);
                        }
                    }
                    #endregion

                    #region insert BCXuatNhapTon
                    //Xử lý để insert DataTable dtBCXuatNhapTon vào bảng t_KDT_SXXK_BCXuatNhapTon
                    dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                    if (TKToKhaiNKD != 1)
                    {

                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap LIKE 'NKD%'";
                    }
                    int STT = 0;
                    string maNPL = "";
                    int soToKhaiNhap = 0;
                    string maLoaiHinhNhap = "";
                    DateTime ngayDangKyNhap = new DateTime(1900, 1, 1);

                    for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                    {
                        DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                        if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                        BCXuatNhapTon bc = new BCXuatNhapTon();

                        bc.STT = STT;
                        if (rv["ChuyenMucDichKhac"].ToString() != "")
                            bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                        bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                        bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                        bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                        bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                        if (rv["LuongNPLTaiXuat"].ToString() != "")
                            bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                        bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                        bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                        bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                        bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                        maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                        bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                        bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                        bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                        bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                        bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                        bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                        bc.MaSP = Convert.ToString(rv["MaSP"]);
                        bc.TenSP = Convert.ToString(rv["TenSP"]);
                        bc.NamThanhLy = DateTime.Today.Year;
                        ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                        bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                        bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                        bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                        bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                        bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                        soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                        bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                        if (rv["ToKhaiTaiXuat"].ToString() != "")
                        {
                            bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                            bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                        }
                        bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                        bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                        if (rv["ThanhKhoanTiep"].ToString() != "")
                            bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                        else
                        {
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                        }
                        //Xử lý ghi chú là mua VN, chuyển lần sau TK hay chuyển TK xyz
                        if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                        {
                            if (TKToKhaiNKD == 1)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }


                                        }


                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                        }
                        bcXNTCollection.Add(bc);

                    }
                    if (TKToKhaiNKD != 1)
                    {
                        dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap NOT LIKE 'NKD%'";
                        STT = 0;
                        maNPL = "";
                        soToKhaiNhap = 0;
                        maLoaiHinhNhap = "";
                        ngayDangKyNhap = new DateTime(1900, 1, 1);

                        for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                        {
                            DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                            if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                            BCXuatNhapTon bc = new BCXuatNhapTon();

                            bc.STT = STT;
                            if (rv["ChuyenMucDichKhac"].ToString() != "")
                                bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                            bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                            bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                            bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                            if (rv["LuongNPLTaiXuat"].ToString() != "")
                                bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                            bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                            bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                            bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                            bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                            maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                            bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                            bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                            bc.MaSP = Convert.ToString(rv["MaSP"]);
                            bc.TenSP = Convert.ToString(rv["TenSP"]);
                            bc.NamThanhLy = DateTime.Today.Year;
                            ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                            bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                            bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                            bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                            bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                            bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                            soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                            bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                            if (rv["ToKhaiTaiXuat"].ToString() != "")
                            {
                                bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                                bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                            }
                            bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                            bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                            if (rv["ThanhKhoanTiep"].ToString() != "")
                                bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                            else
                            {
                                if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                    bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            }
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }


                                        }


                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                            bcXNTCollection.Add(bc);

                        }
                    }

                    #region Thêm NPL ko tham gia vào thanh khoản
                    if (nplKoTK == 1)
                    {
                        DataRow[] rows = dtNPLNhapTon.Select(" TonDau = TonCuoi AND TonDau > 0");
                        for (int i = 0; i < rows.Length; i++)
                        {
                            DataRow row = rows[i];
                            if (CheckTKExist(bcXNTCollection, Convert.ToInt32(row["SoToKhai"]), row["MaLoaiHinh"].ToString(), Convert.ToDateTime(row["NgayDangKy"])))
                            {
                                BCXuatNhapTon bc = new BCXuatNhapTon();
                                bc.LanThanhLy = this.LanThanhLy;
                                bc.NamThanhLy = this.NgayBatDau.Year;
                                bc.MaDoanhNghiep = this.MaDoanhNghiep;
                                bc.MaNPL = row["MaNPL"].ToString();
                                bc.TenNPL = row["TenNPL"].ToString();
                                bc.TenDVT_NPL = row["TenDVT_NPL"].ToString();
                                bc.SoToKhaiNhap = Convert.ToInt32(row["SoToKhai"]);
                                bc.NgayDangKyNhap = Convert.ToDateTime(row["NgayDangKy"]);
                                bc.NgayHoanThanhNhap = bc.NgayDangKyNhap;
                                bc.MaLoaiHinhNhap = row["MaLoaiHinh"].ToString();
                                bc.LuongNhap = Convert.ToDecimal(row["Luong"]);
                                bc.LuongTonDau = Convert.ToDecimal(row["TonDau"]);
                                bc.LuongNPLSuDung = 0;
                                bc.LuongTonCuoi = Convert.ToDecimal(row["TonDau"]);
                                bc.DonGiaTT = Convert.ToDouble(row["DonGiaTT"]);
                                bc.TyGiaTT = Convert.ToDecimal(row["TyGiaTT"]);
                                bc.ThueSuat = Convert.ToDecimal(row["ThueSuat"]);
                                bc.ThueXNK = Convert.ToDouble(row["ThueXNK"]);
                                bc.ThueXNKTon = Convert.ToDouble(row["TonDauThueXNK"]);
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                                bcXNTCollection.Add(bc);
                            }
                        }
                    }
                    #endregion
                    this.InsertBCXNT(bcXNTCollection, transaction);

                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    maLoaiHinhNhap = "";
                    bool temp3 = false;

                    //Xử lý collection bcXNTCollection trước khi đưa vào xử lý báo cáo này để tổng hợp nên báo cáo thuế XNK
                    BCXuatNhapTonCollection bcTemp = new BCXuatNhapTonCollection();
                    for (int i = 0; i < bcXNTCollection.Count; i++)
                    {
                        if (maNPL == bcXNTCollection[i].MaNPL && soToKhaiNhap == bcXNTCollection[i].SoToKhaiNhap && ngayDangKyNhap == bcXNTCollection[i].NgayDangKyNhap && maLoaiHinhNhap == bcXNTCollection[i].MaLoaiHinhNhap)
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0)
                            {
                                if (!temp3)
                                    temp3 = true;
                                else
                                    bcTemp.Add(bcXNTCollection[i]);

                            }
                            else
                            {
                                temp3 = false;
                            }
                        }
                        else
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0) temp3 = true;
                            else temp3 = false;
                            maNPL = bcXNTCollection[i].MaNPL;
                            soToKhaiNhap = bcXNTCollection[i].SoToKhaiNhap;
                            ngayDangKyNhap = bcXNTCollection[i].NgayDangKyNhap;
                            maLoaiHinhNhap = bcXNTCollection[i].MaLoaiHinhNhap;
                        }
                    }
                    for (int i = 0; i < bcTemp.Count; i++)
                    {
                        bcXNTCollection.Remove(bcTemp[i]);
                    }

                    #endregion

                    #region insert BCThueXNK
                    int soToKhaiXuat = 0;
                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    DateTime ngayDangKyXuat = new DateTime(1900, 1, 1);
                    BCThueXNKCollection bcThueXNKCollectin = new BCThueXNKCollection();
                    BCThueXNK bcThueXNK = new BCThueXNK(); ;
                    STT = 0;
                    index = getBKNPLTaiXuat();
                    decimal temp1 = 0;//Biến đánh dấu khi âm NPL

                    //Duyệt từng dòng trong collection bcXNTCollection
                    foreach (BCXuatNhapTon bc in bcXNTCollection)
                    {

                        if (maNPL.ToUpper() == bc.MaNPL.ToUpper() && soToKhaiNhap == bc.SoToKhaiNhap && ngayDangKyNhap == bc.NgayDangKyNhap && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                        {
                            //Nếu đây là tờ khai nhập đã có trong collection bcThueXNCollection
                            if (bc.LuongTonCuoi < 0)
                            {
                                //Nếu lượng tồn cuối nhỏ hơn 0
                                temp1 = bc.LuongTonCuoi; //Gán biến temp1
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung + bc.LuongTonCuoi;
                                bcThueXNK.LuongNPLTon = 0;
                            }
                            else
                            {
                                //Nếu lớn hơn hoặc bằng 0
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung;
                                bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                            }
                            //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                            bcThueXNKCollectin[bcThueXNKCollectin.Count - 1] = bcThueXNK;
                        }
                        else
                        {
                            if (index > 0 && bc.LuongNPLTaiXuat > 0)
                            {
                                //Nếu là tờ khai tái xuất
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                //DataRow dr = GetDonGia_TyGia_ThueSuat_ThueNK(dtNPLNhapTon, bcThueXNK.SoToKhaiNhap, bcThueXNK.NgayDangKyNhap, bcThueXNK.MaNPL);
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiTaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayTaiXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayTaiXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;
                                bcThueXNK.LuongNPLSuDung = bc.LuongNPLTaiXuat;
                                bcThueXNK.LuongNPLTon = bc.LuongTonDau - bc.LuongNPLTaiXuat;
                                //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                                bcThueXNKCollectin.Add(bcThueXNK);
                            }
                            else
                            {

                                if (maNPL.ToUpper().Trim() != bc.MaNPL.ToUpper().Trim()) temp1 = 0;//Nếu đây là npl khác thì gán temp1 =0
                                //Tạo 1 dòng báo cáo thuế XNK và gán dữ liệu 
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.TenNPL = bc.TenNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayDangKyXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayHoanThanhXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;

                                if (temp1 < 0 && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                                {
                                    //Nếu temp1 <0 và tờ khai xuất dòng trên giống dòng hiện tại
                                    if (0 - temp1 <= bc.LuongTonDau)
                                    {
                                        //Nếu lượng âm mà nhỏ hơn lượng tồn đầu của dòng hiện tại
                                        bcThueXNK.LuongNPLSuDung = 0 - temp1;// Gán lượng sử dụng bằng lượng âm
                                        bcThueXNK.LuongNPLTon = bc.LuongTonDau - bcThueXNK.LuongNPLSuDung;//Gán lượng NPL tồn bằng 
                                        temp1 = 0;
                                    }
                                    else
                                    {
                                        //Nếu lượng âm nhỏ hơn
                                        bcThueXNK.LuongNPLSuDung = bc.LuongTonDau;//Gán lượng sử dụng bằng lượng tồn đầu ==> npl tờ khai nhập này dùng hết
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn = 0
                                        temp1 = temp1 + bcThueXNK.LuongNPLSuDung;// Gán lại biến temp
                                    }
                                }
                                else
                                {

                                    if (bc.LuongTonCuoi < 0)
                                    {
                                        temp1 = bc.LuongTonCuoi;//Gán lại biến temp1
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung + bc.LuongTonCuoi;//Gán npl sử dụng bằng chính nó + thêm lượng tồn cuối
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn bằng 0
                                    }
                                    else
                                    {
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung;
                                        bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                                    }
                                }
                                bcThueXNKCollectin.Add(bcThueXNK);//Thêm 1 dòng vào báo cáo thuế
                                //Lưu lại tờ khai nhập xuất đang xử lý
                                soToKhaiXuat = bcThueXNK.SoToKhaiXuat;
                                ngayDangKyXuat = bcThueXNK.NgayDangKyXuat;
                                soToKhaiNhap = bcThueXNK.SoToKhaiNhap;
                                maNPL = bcThueXNK.MaNPL;
                                ngayDangKyNhap = bc.NgayDangKyNhap;
                            }
                        }

                    }
                    this.InsertBCThueXNK(bcThueXNKCollectin, transaction);
                    #endregion

                    transaction.Commit();
                    this.TrangThaiThanhKhoan = 400;

                }
                catch (Exception ex)
                {
                    this.TrangThaiThanhKhoan = 100;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        //Hoa Tho :
        public void ChayThanhLyNgayHoanThanhXuat(int SoThapPhanNPL, int nplKoTK, int TKToKhaiNKD, int AmTKTiep, int chenhLechNgay, int ToKhaiKoTK)
        {
            int soThapPhanBCXuatNhapTon_Thue = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("BCTK") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("BCTK")) : 0;


            //Lay MaNPL can DEBUG tu Config.
            //string configMaNPL = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DEBUG_HSTK_MaNPL");
            //string configMaSP = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DEBUG_HSTK_MaSP");
            //string configSoTKN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DEBUG_HSTK_SoTKN");
            //string configSoTKX = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DEBUG_HSTK_SoTKX");

            //Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("Lay thong tin tu Config: MaNPL: {0}, SoTKN: {1}, MaSP: {2}, SoTKX: {3}", configMaNPL, configSoTKN, configMaSP, configSoTKX)));

            DataSet ds = new DataSet();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region Khởi tạo các dữ liệu ban đầu
                    //Lấy danh sách nguyên phụ liệu tồn của các tờ khai nhập có trong bộ hồ sơ thanh khoản
                    string sp = "p_KDT_SXXK_DanhSachNPLNhapTon";
                    DbCommand cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiNhap()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLNhapTon");

                    //Lấy danh sách tất cả NPL quy đổi từ định mức sản phẩm của tất cả tờ khai xuất trong bộ hồ sợ thanh khoản
                    /*HUNGTQ updated 18/10/2010. Cap nhat them tieu chi sap xep theo 'Ngay hoan thanh xuat' theo cau hinh bao cao cho store procedure p_KDT_SXXK_DanhSachNPLXuatTonOver5.*/
                    sp = "p_KDT_SXXK_DanhSachNPLXuatTonOver5";
                    cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, this.BKCollection[this.getBKToKhaiXuat()].ID);
                    db.AddInParameter(cmd, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
                    db.LoadDataSet(cmd, ds, "t_NPLXuatTon");

                    //Load danh sách nội dung tất cả bảng kê trong bộ hồ sơ thanh khoản
                    this.LoadBKCollection();
                    this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
                    BCXuatNhapTonCollection bcXNTCollection = new BCXuatNhapTonCollection();

                    DataTable dtNPLXuatTon = ds.Tables["t_NPLXuatTon"];
                    DataTable dtNPLNhapTon = ds.Tables["t_NPLNhapTon"];

                    //TEST

                    //if (configMaNPL != "" || configSoTKN != "")
                    //{
                    //    DataTable cloneN = dtNPLNhapTon.Copy();
                    //    cloneN.TableName = "t_NPLNhapTon";
                    //    cloneN.WriteXml(System.Windows.Forms.Application.StartupPath + "//ThanhKhoan_t_NPLNhapTon.xml");

                    //    DataTable cloneX = dtNPLXuatTon.Copy();
                    //    cloneX.TableName = "dtNPLXuatTon";
                    //    cloneX.WriteXml(System.Windows.Forms.Application.StartupPath + "//ThanhKhoan_dtNPLXuatTon.xml");
                    //}

                    //Kiểm tra dữ liệu trong danh sách nguyên phụ liệu tờ khai nhập
                    foreach (DataRow dr in dtNPLNhapTon.Rows)
                    {
                        if (dr["ThueXNK"] == DBNull.Value)
                            throw new Exception("Mã NPL " + dr["MaNPL"] + " của tờ khai nhập số " + dr["SoToKhai"].ToString() + "/" + dr["NamDangKy"] + " chưa có trong danh sách nguyên phụ liệu nhập tồn.");
                    }

                    //Tạo table báo cáo nhập xuất tồn
                    DataTable dtBCXuatNhapTon = new DataTable();
                    DataColumn[] cols = new DataColumn[34];
                    cols[0] = new DataColumn("LanThanhLy", typeof(int));
                    cols[1] = new DataColumn("MaDoanhNghiep", typeof(string));
                    cols[2] = new DataColumn("STT", typeof(long));
                    cols[3] = new DataColumn("MaNPL", typeof(string));
                    cols[4] = new DataColumn("SoToKhaiNhap", typeof(int));
                    cols[5] = new DataColumn("NgayDangKyNhap", typeof(DateTime));
                    cols[6] = new DataColumn("NgayHoanThanhNhap", typeof(DateTime));
                    cols[7] = new DataColumn("LuongNhap", typeof(decimal));
                    cols[8] = new DataColumn("LuongTonDau", typeof(decimal));
                    cols[9] = new DataColumn("TenDVT_NPL", typeof(string));
                    cols[10] = new DataColumn("MaSP", typeof(string));
                    cols[11] = new DataColumn("SoToKhaiXuat", typeof(int));
                    cols[12] = new DataColumn("NgayDangKyXuat", typeof(DateTime));
                    cols[13] = new DataColumn("NgayHoanThanhXuat", typeof(DateTime));
                    cols[14] = new DataColumn("LuongSPXuat", typeof(decimal));
                    cols[15] = new DataColumn("TenDVT_SP", typeof(string));
                    cols[16] = new DataColumn("DinhMuc", typeof(decimal));
                    cols[17] = new DataColumn("LuongNPLSuDung", typeof(decimal));
                    cols[18] = new DataColumn("ToKhaiTaiXuat", typeof(decimal));
                    cols[19] = new DataColumn("LuongNPLTaiXuat", typeof(decimal));
                    cols[20] = new DataColumn("NgayTaiXuat", typeof(DateTime));
                    cols[21] = new DataColumn("LuongTonCuoi", typeof(decimal));
                    cols[22] = new DataColumn("ThanhKhoanTiep", typeof(string));
                    cols[23] = new DataColumn("ChuyenMucDichKhac", typeof(string));
                    cols[24] = new DataColumn("TenNPL", typeof(string));
                    cols[25] = new DataColumn("TenSP", typeof(string));
                    cols[26] = new DataColumn("MaLoaiHinhNhap", typeof(string));
                    cols[27] = new DataColumn("MaLoaiHinhXuat", typeof(string));
                    cols[28] = new DataColumn("DonGiaTT", typeof(double));
                    cols[29] = new DataColumn("TyGiaTT", typeof(decimal));
                    cols[30] = new DataColumn("ThueSuat", typeof(decimal));
                    cols[31] = new DataColumn("ThueXNK", typeof(double));
                    cols[32] = new DataColumn("ThueXNKTon", typeof(double));
                    cols[33] = new DataColumn("NgayThucXuat", typeof(DateTime));
                    dtBCXuatNhapTon.Columns.AddRange(cols);
                    #endregion

                    #region Xử lý trước cảng bảng kê trong bộ hồ sơ thanh khoản
                    //Thanh khoản bang ke NPL xin huy
                    int index = this.getBKNPLXinHuy();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXH", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXinHuy bk in this.BKCollection[index].bkNPLXHCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongHuy;
                                    dr["LuongXH"] = bk.LuongHuy;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL tai xuat
                    index = this.getBKNPLTaiXuat();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongTX", typeof(decimal));
                        DataColumn col1 = new DataColumn("SoToKhaiTX", typeof(int));
                        DataColumn col2 = new DataColumn("NgayTX", typeof(DateTime));
                        dtNPLNhapTon.Columns.Add(col);
                        dtNPLNhapTon.Columns.Add(col1);
                        dtNPLNhapTon.Columns.Add(col2);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTaiXuat bk in this.BKCollection[index].bkNPLTXCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == System.Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == System.Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == System.Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == System.Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == System.Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    if (bk.SoToKhai == 1629)
                                    { }
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongTaiXuat;
                                    drNhapTon["LuongTX"] = bk.LuongTaiXuat;
                                    drNhapTon["SoToKhaiTX"] = bk.SoToKhaiXuat;
                                    drNhapTon["NgayTX"] = bk.NgayDangKyXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " TÁI XUẤT";
                                    dr["SoToKhaiXuat"] = 0;
                                    dr["NgayDangKyXuat"] = new DateTime(1900, 1, 1);
                                    dr["NgayHoanThanhXuat"] = new DateTime(1900, 1, 1);
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = 0;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["ToKhaiTaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayTaiXuat"] = bk.NgayDangKyXuat;
                                    dr["NgayThucXuat"] = bk.NgayThucXuat;

                                    dr["LuongNPLTaiXuat"] = bk.LuongTaiXuat;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL nop thue
                    index = this.getBKNPLNopThue();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongNT", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLNopThueTieuThuNoiDia bk in this.BKCollection[index].bkNPLNTCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongNopThue;
                                    dr["LuongNT"] = bk.LuongNopThue;
                                    break;
                                }
                            }
                        }
                    }
                    //Thanh ly bang ke NPL chua thanh ly
                    index = this.getBKNPLChuaThanhLY();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongCTL", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLChuaThanhLy bk in this.BKCollection[index].bkNPLCTLCollection)
                        {
                            foreach (DataRow dr in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.Luong;
                                    dr["LuongCTL"] = bk.Luong;
                                    break;
                                }
                            }
                        }
                    }
                    index = this.getBKNPLTuCungUng();
                    if (index >= 0)
                    {
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLTuCungUng bk in this.BKCollection[index].bkNPLTCUCollection)
                        {
                            foreach (DataRow dr in dtNPLXuatTon.Rows)
                            {
                                if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                                {
                                    if (bk.LuongTuCungUng <= Convert.ToDecimal(dr["TonNPL"]))
                                    {
                                        dr["TonNPL"] = Convert.ToDecimal(dr["TonNPL"]) - bk.LuongTuCungUng;
                                        break;
                                    }
                                    else
                                    {
                                        bk.LuongTuCungUng -= Convert.ToDecimal(dr["TonNPL"]);
                                        dr["TonNPL"] = 0;
                                    }

                                }
                            }
                        }
                    }

                    //Thanh ly bang ke NPL xuat gia cong
                    index = this.getBKNPLXuatGiaCong();
                    if (index >= 0)
                    {
                        DataColumn col = new DataColumn("LuongXGC", typeof(decimal));
                        dtNPLNhapTon.Columns.Add(col);
                        this.BKCollection[index].LoadChiTietBangKe();
                        foreach (BKNPLXuatGiaCong bk in this.BKCollection[index].bkNPLXGCCollection)
                        {
                            foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                            {
                                if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                    bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                {
                                    drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongXuat;
                                    drNhapTon["LuongXGC"] = bk.LuongXuat;
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaNPL"] = bk.MaNPL;
                                    dr["TenNPL"] = bk.TenNPL;
                                    dr["SoToKhaiNhap"] = bk.SoToKhai;
                                    dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                    dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                    dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;
                                    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                    dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                    dr["MaSP"] = " ";
                                    dr["TenSP"] = " ";
                                    dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                    dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                    dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                    dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                    dr["LuongSPXuat"] = 0;
                                    dr["LuongNPLSuDung"] = bk.LuongXuat;
                                    dr["TenDVT_SP"] = " ";
                                    dr["DinhMuc"] = 0;
                                    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                    dtBCXuatNhapTon.Rows.Add(dr);
                                    break;
                                }
                            }
                        }
                    }
                    //Xử lý bảng kê nhập kinh doanh
                    if (TKToKhaiNKD != 1)
                    {
                        index = this.getBKNPLNhapKinhDoanh();
                        if (index >= 0)
                        {
                            DataColumn col = new DataColumn("LuongNKD", typeof(decimal));
                            dtNPLNhapTon.Columns.Add(col);
                            this.BKCollection[index].LoadChiTietBangKe();
                            foreach (BKNPLXuatSuDungNKD bk in this.BKCollection[index].bkNPLNKDCollection)
                            {
                                foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                                {
                                    if (bk.SoToKhai == Convert.ToInt32(drNhapTon["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(drNhapTon["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(drNhapTon["NamDangKy"]) &&
                                        bk.MaHaiQuan == Convert.ToString(drNhapTon["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(drNhapTon["MaNPL"]).ToUpper())
                                    {
                                        drNhapTon["TonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]) - bk.LuongSuDung;
                                        drNhapTon["LuongNKD"] = bk.LuongSuDung;
                                        DataRow dr = dtBCXuatNhapTon.NewRow();
                                        dr["LanThanhLy"] = this.LanThanhLy;
                                        dr["MaNPL"] = bk.MaNPL;
                                        Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                        NPL.Ma = bk.MaNPL;
                                        NPL.MaDoanhNghiep = this.MaDoanhNghiep;
                                        NPL.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        NPL.Load();
                                        dr["TenNPL"] = NPL.Ten;
                                        dr["SoToKhaiNhap"] = bk.SoToKhai;
                                        dr["MaLoaiHinhNhap"] = bk.MaLoaiHinh;
                                        dr["NgayDangKyNhap"] = bk.NgayDangKy;
                                        dr["NgayHoanThanhNhap"] = bk.NgayDangKy;
                                        dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                        dr["STT"] = 0;
                                        dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
                                        dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
                                        dr["LuongTonCuoi"] = Convert.ToDecimal(drNhapTon["TonCuoi"]);
                                        dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
                                        dr["MaSP"] = bk.MaSP;
                                        Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                        SP.Ma = bk.MaSP;
                                        SP.MaDoanhNghiep = this.MaDoanhNghiep;
                                        SP.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        SP.Load();
                                        dr["TenSP"] = SP.Ten;
                                        dr["SoToKhaiXuat"] = bk.SoToKhaiXuat;
                                        dr["NgayDangKyXuat"] = bk.NgayDangKyXuat;
                                        dr["NgayHoanThanhXuat"] = bk.NgayDangKyXuat;
                                        dr["MaLoaiHinhXuat"] = bk.MaLoaiHinhXuat;
                                        dr["LuongSPXuat"] = 0;
                                        dr["LuongNPLSuDung"] = bk.LuongSuDung;
                                        dr["TenDVT_SP"] = DonViTinh.GetName(SP.DVT_ID);
                                        Company.BLL.SXXK.DinhMuc DM = new Company.BLL.SXXK.DinhMuc();
                                        DM.MaSanPHam = bk.MaSP;
                                        DM.MaNguyenPhuLieu = bk.MaNPL;
                                        DM.MaDoanhNghiep = this.MaDoanhNghiep;
                                        DM.MaHaiQuan = this.MaHaiQuanTiepNhan;
                                        DM.Load();
                                        dr["DinhMuc"] = DM.DinhMucChung;
                                        dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
                                        dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
                                        dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
                                        dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
                                        dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        break;
                                    }
                                }
                                foreach (DataRow drXuatTon in dtNPLXuatTon.Rows)
                                {
                                    if (bk.SoToKhaiXuat == Convert.ToInt32(drXuatTon["SoToKhai"]) && bk.MaLoaiHinhXuat == Convert.ToString(drXuatTon["MaLoaiHinh"]) && bk.NamDangKyXuat == Convert.ToInt16(drXuatTon["NamDangKy"]) &&
                                        bk.MaHaiQuanXuat == Convert.ToString(drXuatTon["MaHaiQuan"]) && bk.MaSP.ToUpper() == Convert.ToString(drXuatTon["MaSP"]).ToUpper() && bk.MaNPL.ToUpper() == Convert.ToString(drXuatTon["MaNPL"]).ToUpper())
                                    {
                                        drXuatTon["TonNPL"] = Convert.ToDecimal(drXuatTon["TonNPL"]) - bk.LuongSuDung;
                                        drXuatTon["LuongNPL"] = Convert.ToDecimal(drXuatTon["LuongNPL"]) - bk.LuongSuDung;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Thực hiện việc thanh khoản

                    //Tạo table báo cáo nhập xuất tồn
                    //DataTable dtTinhAm = new DataTable();
                    //DataColumn[] dc = new DataColumn[6];
                    //dc[0] = new DataColumn("MaNPL", typeof(string));
                    //dc[1] = new DataColumn("SoTKNTruoc", typeof(long));
                    //dc[2] = new DataColumn("NgayHTTruoc", typeof(DateTime));
                    //dc[3] = new DataColumn("SoTKNSau", typeof(long));
                    //dc[4] = new DataColumn("NgayHTSau", typeof(DateTime));
                    //dc[5] = new DataColumn("Index", typeof(long));
                    //dtTinhAm.Columns.AddRange(dc);

                    int tkNhap_TinhAmKeTiep = 0;
                    string maNPL_TinhAmKeTiep = "";
                    DateTime ngayHoanThanhTKN_TinhAmKeTiep = new DateTime(1900, 1, 1);
                    int indexTK = 0;

                    //Duyệt từng dòng trong danh sách nguyên phụ liệu xuất
                    foreach (DataRow dr1 in dtNPLXuatTon.Rows)
                    {
                        //TEST
                        string nplx = dr1["MaNPL"].ToString().ToLower();
                        string spx = dr1["MaSP"].ToString().ToLower();
                        string sotkx = dr1["SoToKhai"].ToString();
                        string malhx = dr1["MaLoaiHinh"].ToString();

#if DEBUG
                        //if (sotkx == "565")
                        //{
                        //}
                        //else
                        //    continue;
#endif
                        decimal luongAm = 0;//Biến chứa lượng nguyên phụ liệu âm sau khi trừ gán bằng 0 trước khi xử lý 1 dòng trong danh sách NPL xuất
                        int j = -1;//Biến đánh dấu vị trí npl nhập
                        //Duyệt từng dòng trong danh sách nguyên phụ liệu nhập
                        for (int i = 0; i < dtNPLNhapTon.Rows.Count; i++)
                        {

                            DataRow dr2 = dtNPLNhapTon.Rows[i];

                            string npln = dr2["MaNPL"].ToString().ToLower();
                            string sotkn = dr2["SoToKhai"].ToString();
                            string malhn = dr2["MaLoaiHinh"].ToString();

                            //Nếu tờ khai nhập kinh doanh thì bỏ qua
                            //tham số TKToKhaiNKD == 1 là thanh khoản xem tờ khai NKD như NSXXK 
                            //tham số TKToKhaiNKD == 0 là bỏ qua tờ khai NKD
                            if (TKToKhaiNKD != 1)
                            {
                                if (dr2["MaLoaiHinh"].ToString().Contains("NKD"))
                                {
                                    continue;
                                }
                            }
                            //Nếu ngày thực nhập tờ khai nhập > ngày đăng ký tờ khai xuất thì bỏ qua tờ khai nhập

                            //DateTime ngaytokhaixuat = Convert.ToDateTime(dr1["NgayDangKy"]);
                            //if (dr1["NgayThucXuat"] != null && Convert.ToDateTime(dr1["NgayThucXuat"]).Year > 1900)
                            //    ngaytokhaixuat = Convert.ToDateTime(dr1["NgayThucXuat"]);  

                            DateTime ngaytokhaixuat = System.Convert.ToDateTime(dr1["NgayDangKy"]);
                            if (dr1["NgayHoanThanhXuat"] != null && Convert.ToDateTime(dr1["NgayHoanThanhXuat"]).Year > 1900)
                                ngaytokhaixuat = Convert.ToDateTime(dr1["NgayHoanThanhXuat"]);

#if DEBUG
                            //Test
                            //if (npln == "DAYTH")
                            //{
                            //    if (sotkn == "4158")
                            //    {
                            //        if (sotkx == "565")
                            //        { }
                            //    }
                            //}
                            //else
                            //    continue;
#endif

                            //Kiem tra them ngay hoan thanh cua tk nhap KE TIEP + ngay chenh lech <= ngay hoan thanh tkx.
                            //Neu thoa man dieu kien -> cho them moi.
                            bool tinhAmToKhaiTiepTheo = false;
                            //TODO: Tạm thời comment. Chương trình chạy chậm do tại đây 26/05/2012.
                            //tinhAmToKhaiTiepTheo = GetTKNKeTiep(dtNPLNhapTon, i, dr2["MaNPL"].ToString().ToLower(), System.Convert.ToInt64(sotkn), malhn, System.Convert.ToDateTime(dr2["NgayDangKy"]).Year, chenhLechNgay, ngaytokhaixuat);

                            //TODO: So sanh chenh lech ngay giua TKX & TKN
                            //TODO: Updated by Hungtq, 25/10/2011. So sanh ngay hoan thanh, khong dung ngay dang ky.
                            //Luu y: Bo gio trong ngay khi so sanh theo ngay.
                            //if (Convert.ToDateTime(dr2["NgayThucNhap"]).AddDays((double)chenhLechNgay) <= ngaytokhaixuat)
                            if (System.Convert.ToDateTime(System.Convert.ToDateTime(dr2["NgayHoanThanh"]).AddDays((double)chenhLechNgay).ToShortDateString()) <= System.Convert.ToDateTime(ngaytokhaixuat.ToShortDateString()))
                            {
                                //if (tinhAmToKhaiTiepTheo == true) continue;

                                if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim()
                                    && System.Convert.ToDecimal(dr2["TonCuoi"]) > 0 && System.Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                {
                                    #region
                                    //Nếu mã NPL giống nhau trong NPL xuất và nhập và lượng tồn NPL nhập và tồn của npl xuất > 0
                                    //Tạo thêm 1 dòng mới trong báo cáo nhập xuất tồn và gán giá trị cho các cột
                                    DataRow dr = dtBCXuatNhapTon.NewRow();
                                    dr["LanThanhLy"] = this.LanThanhLy;
                                    dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                    dr["STT"] = 0;

                                    dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                    dr["TenNPL"] = dr2["TenNPL"].ToString();
                                    dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                    dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                    //dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]); //Comment by Hungtq, 22/09/2010.
                                    dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayHoanThanh"]);
                                    dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                    //HungTQ, updaed so thap phan 11/02/2012
                                    dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                    //HungTQ, updaed so thap phan 11/02/2012
                                    dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue)); //TonDau//huypvt cap nhat TonDau=>TonCuoi choScavi
                                    dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                    dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                    dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                    dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                    dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                    dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                    dr["MaSP"] = dr1["MaSP"].ToString();
                                    dr["TenSP"] = dr1["TenSP"].ToString();
                                    dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                    dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                    //
                                    //dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);
                                    dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayHoanThanhXuat"]);
                                    dr["NgayThucXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);

                                    dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                    //HungTQ, updaed so thap phan 11/02/2012
                                    dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                    //HungTQ, updaed so thap phan 11/02/2012
                                    dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                    dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                    dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);


                                    if (Convert.ToDecimal(dr2["TonCuoi"]) >= Convert.ToDecimal(dr1["TonNPL"]))
                                    {
                                        //Nếu tồn cuối của npl nhập mà >= tồn của npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lượng âm < 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                        }
                                        else
                                        {
                                            //Ngược lại
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]);
                                        }
                                        dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 ==> đã xử lý xong dòng npl xuất này 
                                        dtBCXuatNhapTon.Rows.Add(dr);//Add thêm 1 dòng vào báo cáo
                                        if (AmTKTiep == 1)
                                        {
                                            //Am thanh khoan tiếp
                                            if (Convert.ToDecimal(dr2["TonCuoi"]) == 0)
                                            {
                                                //Nếu lượng tồn npl này hết
                                                dr2["LanDieuChinh"] = 100;//đánh dấu bằng con số 100 để biết npl của tờ khai này là đứng cuối cùng trong danh sách npl có tồn =0
                                                for (int k = 0; k < i; k++)
                                                    if (dtNPLNhapTon.Rows[k]["MaNPL"].ToString().ToUpper().Trim() == dr2["MaNPL"].ToString().ToUpper().Trim()) dtNPLNhapTon.Rows[k]["LanDieuChinh"] = 0;// Đánh dấu các npl của tờ khai trước đó bằng 0
                                            }
                                        }
                                        break;//Thoát khỏi vòng lập
                                    }
                                    else
                                    {
                                        //Nếu lượng tồn npl nhập < lượng tồn npl xuất
                                        if (luongAm < 0)
                                        {
                                            //Nếu lương âm vẫn còn nhỏ hơn 0
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) + luongAm;
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                        }
                                        else //Bo sung dong: else
                                        {
                                            //Nếu lớn hơn 0
                                            decimal temp = Convert.ToDecimal(dr1["TonNPL"]);
                                            dr1["TonNPL"] = Convert.ToDecimal(dr1["TonNPL"]) - Convert.ToDecimal(dr2["TonCuoi"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - temp;
                                            dr2["TonCuoi"] = 0;
                                            luongAm = Convert.ToDecimal(dr["LuongTonCuoi"]);

                                        }
                                        dtBCXuatNhapTon.Rows.Add(dr);
                                        j = i;// đánh dấu vị trị bằng i

                                        //========================================
                                        //Kiem tra va danh dau to khai nhap de tinh am tiep theo 
                                        //neu thoa dieu kien [Ngay hoan thanh TKN} + [chenh lech ngay] > [Ngay hoan thanh TKX]
                                        //for (int g = j + 1; g < dtNPLNhapTon.Rows.Count; g++)
                                        //{
                                        //    if (dtNPLNhapTon.Rows[g]["MaNPL"].ToString().Trim() == dr2["MaNPL"].ToString().Trim()
                                        //        && System.Convert.ToDateTime(dtNPLNhapTon.Rows[g]["NgayHoanThanh"]).AddDays(chenhLechNgay) > ngaytokhaixuat)
                                        //    {
                                        //Danh dau so to khai, ma npl, ngay hoa thanh cua tk nhap tinh am ke tiep
                                        //DataRow nr = dtTinhAm.NewRow();
                                        //nr["SoTKNTruoc"] = System.Convert.ToInt64(dr2["SoToKhai"]);
                                        //nr["NgayHTTruoc"] = System.Convert.ToDateTime(dr2["NgayHoanThanh"]);

                                        //nr["SoTKNSau"] = System.Convert.ToInt64(dtNPLNhapTon.Rows[g]["SoToKhai"]);
                                        //nr["MaNPL"] = dtNPLNhapTon.Rows[g]["MaNPL"].ToString().Trim();
                                        //nr["NgayHTSau"] = System.Convert.ToDateTime(dtNPLNhapTon.Rows[g]["NgayHoanThanh"]);
                                        //nr["Index"] = g;
                                        //dtTinhAm.Rows.Add(nr);

                                        //tkNhap_TinhAmKeTiep = System.Convert.ToInt32(dtNPLNhapTon.Rows[g]["SoToKhai"]);
                                        //maNPL_TinhAmKeTiep = dtNPLNhapTon.Rows[g]["MaNPL"].ToString().Trim();
                                        //ngayHoanThanhTKN_TinhAmKeTiep = System.Convert.ToDateTime(dtNPLNhapTon.Rows[g]["NgayHoanThanh"]);
                                        //indexTK = g;
                                        //    }
                                        //}
                                    }
                                    #endregion


                                    //if (dr["MaNPL"].ToString().ToLower() == configMaNPL.ToLower()
                                    //    || dr["SoToKhaiNhap"].ToString() == configSoTKN)
                                    //{
                                    //    string s = string.Format("MaNPL: {0}, SoToKhaiNhap: {1}, MaLoaiHinh: {2}, LuongNhap: {3}, LuongTonDau: {4}, SoToKhaiXuat: {5}, MaSP: {6}, LuongSPXuat: {7}, LuongNPLSuDung: {8}, DinhMuc: {9}",
                                    //        configMaNPL, configSoTKN, dr["MaLoaiHinhNhap"].ToString(), dr["LuongNhap"].ToString(), dr["LuongTonDau"].ToString(),
                                    //        dr["SoToKhaiXuat"].ToString(), dr["MaSP"].ToString(), dr["LuongSPXuat"].ToString(), dr["LuongNPLSuDung"].ToString(), dr["DinhMuc"].ToString());
                                    //    Logger.LocalLogger.Instance().WriteMessage(s, new Exception(""));
                                    //}
                                }
                                else
                                {
                                    #region
                                    //Am thanh khoan tiep
                                    if (AmTKTiep == 1)
                                    {
                                        //if ((sotkn == "107" || sotkn == "1131") && (sotkx == "602" || sotkx == "639") && npln.ToLower() == "daydet" && nplx.ToLower() == "daydet")
                                        //{ }

                                        //DataRow[] rowsTinhAm = dtTinhAm.Rows.Count > 0 ? dtTinhAm.Select("MaNPL = '" + dr2["MaNPL"].ToString().Trim() + "' AND SoTKNSau = " + System.Convert.ToInt64(dr2["SoToKhai"])) : null;
                                        //int indexTK = rowsTinhAm != null && rowsTinhAm.Length > 0 ? Convert.ToInt32(rowsTinhAm[0]["Index"]) : 0;

                                        if (dr2["MaNPL"].ToString().ToLower().Trim() == dr1["MaNPL"].ToString().ToLower().Trim()
                                            //&& CheckNPLSau(dtNPLNhapTon, dr2["MaNPL"].ToString().ToLower(), indexTK > 0 ? indexTK - 1 : i, System.Convert.ToDateTime(dr1["NgayHoanThanhXuat"])) //Cap nhat NgayDangky = NgayHoanThanhXuat
                                            && CheckNPLSau(dtNPLNhapTon, dr2["MaNPL"].ToString().ToLower(), i, System.Convert.ToDateTime(dr1["NgayHoanThanhXuat"])) //Cap nhat NgayDangky = NgayHoanThanhXuat
                                            && (System.Convert.ToDecimal(dr2["TonCuoi"]) < 0
                                            || System.Convert.ToInt32(dr2["LanDieuChinh"]) == 100)
                                            && System.Convert.ToDecimal(dr1["TonNPL"]) > 0)
                                        {
                                            //Kiem tra them ngay hoan thanh cua tk nhap KE TIEP + ngay chenh lech <= ngay hoan thanh tkx.
                                            //Neu thoa man dieu kien -> cho them moi.
                                            if (tinhAmToKhaiTiepTheo == true)
                                                continue;

                                            //if (dr2["MaNPL"].ToString().Trim() == maNPL_TinhAmKeTiep
                                            //    && System.Convert.ToInt32(dr2["SoToKhai"]) != tkNhap_TinhAmKeTiep)
                                            //{
                                            //    continue;
                                            //}
                                            //else if (dr2["MaNPL"].ToString().Trim() != maNPL_TinhAmKeTiep)
                                            //{
                                            //    indexTK = 0;
                                            //    tkNhap_TinhAmKeTiep = 0;
                                            //    maNPL_TinhAmKeTiep = "";
                                            //    ngayHoanThanhTKN_TinhAmKeTiep = new DateTime(1900, 1, 1);
                                            //}


                                            DataRow dr = dtBCXuatNhapTon.NewRow();
                                            dr["LanThanhLy"] = this.LanThanhLy;
                                            dr["MaDoanhNghiep"] = this.MaDoanhNghiep;
                                            dr["STT"] = 0;
                                            dr["MaNPL"] = dr2["MaNPL"].ToString().Trim();
                                            dr["TenNPL"] = dr2["TenNPL"].ToString();
                                            dr["SoToKhaiNhap"] = dr2["SoToKhai"].ToString();
                                            dr["NgayDangKyNhap"] = Convert.ToDateTime(dr2["NgayDangKy"]);
                                            //dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayThucNhap"]); //Comment by Hungtq, 22/09/2010.
                                            dr["NgayHoanThanhNhap"] = Convert.ToDateTime(dr2["NgayHoanThanh"]);
                                            dr["MaLoaiHinhNhap"] = Convert.ToString(dr2["MaLoaiHinh"]);
                                            //HungTQ, updaed so thap phan 11/02/2012
                                            dr["LuongNhap"] = Convert.ToDecimal(dr2["Luong"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                            //HungTQ, updaed so thap phan 11/02/2012
                                            dr["LuongTonDau"] = Convert.ToDecimal(dr2["TonDau"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                            dr["TenDVT_NPL"] = Convert.ToString(dr2["TenDVT_NPL"]);
                                            dr["DonGiaTT"] = Convert.ToDouble(dr2["DonGiaTT"]);
                                            dr["TyGiaTT"] = Convert.ToDecimal(dr2["TyGiaTT"]);
                                            dr["ThueSuat"] = Convert.ToDecimal(dr2["ThueSuat"]);
                                            dr["ThueXNK"] = Convert.ToDouble(dr2["ThueXNK"]);
                                            dr["ThueXNKTon"] = Convert.ToDouble(dr2["TonDauThueXNK"]);

                                            dr["MaSP"] = dr1["MaSP"].ToString();
                                            dr["TenSP"] = dr1["TenSP"].ToString();
                                            dr["SoToKhaiXuat"] = dr1["SoToKhai"].ToString();
                                            dr["NgayDangKyXuat"] = Convert.ToDateTime(dr1["NgayDangKy"]);
                                            //
                                            //dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);
                                            dr["NgayHoanThanhXuat"] = Convert.ToDateTime(dr1["NgayHoanThanhXuat"]);
                                            dr["NgayThucXuat"] = Convert.ToDateTime(dr1["NgayThucXuat"]);

                                            dr["MaLoaiHinhXuat"] = Convert.ToString(dr1["MaLoaiHinh"]);
                                            //HungTQ, updaed so thap phan 11/02/2012
                                            dr["LuongSPXuat"] = Convert.ToDecimal(dr1["LuongSP"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                            //HungTQ, updaed so thap phan 11/02/2012
                                            dr["LuongNPLSuDung"] = Convert.ToDecimal(dr1["LuongNPL"]).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(soThapPhanBCXuatNhapTon_Thue));
                                            dr["TenDVT_SP"] = Convert.ToString(dr1["TenDVT_SP"]);
                                            dr["DinhMuc"] = Convert.ToDecimal(dr1["DinhMuc"]);
                                            dr["LuongTonCuoi"] = Convert.ToDecimal(dr2["TonCuoi"]) - Convert.ToDecimal(dr1["TonNPL"]);
                                            dr2["TonCuoi"] = Convert.ToDecimal(dr["LuongTonCuoi"]);
                                            dr["ThanhKhoanTiep"] = "Mua tại VN";
                                            dtBCXuatNhapTon.Rows.Add(dr);
                                            dr1["TonNPL"] = 0;


                                            //if (dr["MaNPL"].ToString().ToLower() == configMaNPL.ToLower()
                                            //    || dr["SoToKhaiNhap"].ToString() == configSoTKN)
                                            //{
                                            //    string s = string.Format("MaNPL: {0}, SoToKhaiNhap: {1}, MaLoaiHinh: {2}, LuongNhap: {3}, LuongTonDau: {4}, SoToKhaiXuat: {5}, MaSP: {6}, LuongSPXuat: {7}, LuongNPLSuDung: {8}, DinhMuc: {9}",
                                            //        configMaNPL, configSoTKN, dr["MaLoaiHinhNhap"].ToString(), dr["LuongNhap"].ToString(), dr["LuongTonDau"].ToString(),
                                            //        dr["SoToKhaiXuat"].ToString(), dr["MaSP"].ToString(), dr["LuongSPXuat"].ToString(), dr["LuongNPLSuDung"].ToString(), dr["DinhMuc"].ToString());
                                            //    Logger.LocalLogger.Instance().WriteMessage(s, new Exception(""));
                                            //}
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        if (Convert.ToDecimal(dr1["TonNPL"]) > 0)
                        {
                            //Nếu hết tất cả tờ khai nhập mà lượng tồn xuất ra vẫn còn dương
                            if (j >= 0)
                            {
                                //Nếu biến j >0 
                                dtNPLNhapTon.Rows[j]["TonCuoi"] = 0 - Convert.ToDecimal(dr1["TonNPL"]);//Gán lại lượng tồn cuối của npl cuối cùng thanh khoản cho npl xuất này
                                dtNPLNhapTon.Rows[j]["LanDieuChinh"] = 100;
                                dr1["TonNPL"] = 0;//Gán tồn npl xuất bằng 0 để bỏ qua npl xuất này
                            }

                        }
                    }

                    if (dtBCXuatNhapTon.Rows.Count == 0) throw new Exception("Không có tờ khai nhập nào tham gia thanh khoản.\nHãy kiểm tra lại định mức của tờ khai xuất, kiểm tra lại ngày thực nhập của tờ khai nhập có nhỏ hơn ngày đăng ký của tờ khai xuất.");
                    #endregion

                    #region Insert dtNPLNhapTon into Database
                    new NPLNhapTon().DeleteDynamicTransaction(transaction, " LanThanhLy=" + this.LanThanhLy + " AND MaDoanhNghiep = '" + MaDoanhNghiep + "'");
                    index = this.getBKNPLChuaThanhLY();
                    //Xử lý bảng kê chưa thanh khoản khi insert vào CSDL cộng lượng tồn lại
                    if (index >= 0)
                    {
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            NPLNhapTon nplNhapTon = new NPLNhapTon();
                            nplNhapTon.LanThanhLy = this.LanThanhLy;
                            nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                            nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                            nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                            nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                            nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                            nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                            nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            if (dr["LuongCTL"].ToString() != "")
                            {
                                if (Convert.ToDecimal(dr["TonCuoi"]) >= 0)
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]) + Convert.ToDecimal(dr["LuongCTL"]);
                                }
                                else
                                {
                                    dr["TonCuoi"] = nplNhapTon.TonCuoi = Convert.ToDecimal(dr["LuongCTL"]);
                                }
                            }
                            else
                                nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                            if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                            nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                            nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                            if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                            else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.ThueXNK / (double)nplNhapTon.Luong, 0);
                            nplNhapTon.InsertTransaction(transaction);
                        }
                    }
                    else
                    {
                        //Insert vào bang t_KDT_SXXK_NPLNhapTon
                        foreach (DataRow dr in dtNPLNhapTon.Rows)
                        {
                            string msg = "";
                            try
                            {
                                NPLNhapTon nplNhapTon = new NPLNhapTon();
                                nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplNhapTon.LanThanhLy = this.LanThanhLy;
                                nplNhapTon.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
                                nplNhapTon.MaLoaiHinh = Convert.ToString(dr["MaLoaiHinh"]);
                                nplNhapTon.NamDangKy = Convert.ToInt16(dr["NamDangKy"]);
                                nplNhapTon.MaHaiQuan = Convert.ToString(dr["MaHaiQuan"]);
                                nplNhapTon.MaNPL = Convert.ToString(dr["MaNPL"]);
                                nplNhapTon.Luong = Convert.ToDecimal(dr["Luong"]);
                                nplNhapTon.TonDau = Convert.ToDecimal(dr["TonDau"]);
                                nplNhapTon.TonCuoi = Convert.ToDecimal(dr["TonCuoi"]);
                                if (nplNhapTon.TonCuoi < 0) nplNhapTon.TonCuoi = 0;
                                nplNhapTon.ThueXNK = Convert.ToDouble(dr["ThueXNK"]);
                                nplNhapTon.TonDauThueXNK = Convert.ToDouble(dr["TonDauThueXNK"]);
                                if (nplNhapTon.TonDau == 0) nplNhapTon.TonCuoiThueXNK = 0;
                                else nplNhapTon.TonCuoiThueXNK = nplNhapTon.TonDauThueXNK - Math.Round(((double)nplNhapTon.TonDau - (double)nplNhapTon.TonCuoi) * nplNhapTon.TonDauThueXNK / (double)nplNhapTon.TonDau, 0);//tồn cuối thuế XNK = tồn đầu thuế XNK - số tiền thuế thanh khoản

                                msg = String.Format("Số Tờ khai: {0}, Mã loại hình: {1}, Năm đăng ký: {2}, Mã NPL: {3}, Số lượng: {4}, "
                                + "Lượng tồn đầu: {5}, Lượng tồn cuối: {6}, Thuế XNK: {7}, TonDauThueXNK: {8}, TonCuoiThueXNK: {9}",
                                nplNhapTon.SoToKhai, nplNhapTon.MaLoaiHinh, nplNhapTon.NamDangKy, nplNhapTon.MaNPL, nplNhapTon.Luong,
                                nplNhapTon.TonDau, nplNhapTon.TonCuoi, nplNhapTon.ThueXNK, nplNhapTon.TonDauThueXNK, nplNhapTon.TonCuoiThueXNK);

                                nplNhapTon.InsertTransaction(transaction);

                                msg = "";
                            }
                            catch (Exception ex)
                            {
                                System.IO.StreamWriter write = System.IO.File.AppendText("ErrorBLL.log");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Lỗi khi Insert vào bang t_KDT_SXXK_NPLNhapTon. Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi tại : ");
                                write.WriteLine(msg);
                                write.WriteLine("Chi tiết : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();

                                throw ex;
                            }
                        }
                    }
                    #endregion

                    #region insert BCXuatNhapTon
                    //Xử lý để insert DataTable dtBCXuatNhapTon vào bảng t_KDT_SXXK_BCXuatNhapTon
                    dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";

                    //if (configMaNPL != "" || configSoTKN != "")
                    //{
                    //    DataTable clone = dtBCXuatNhapTon.Copy();
                    //    clone.TableName = "clone";
                    //    clone.DefaultView.RowFilter = string.Format("MaNPL = '{0}' OR SoToKhaiNhap = {1}", configMaNPL, configSoTKN);
                    //    clone.WriteXml(System.Windows.Forms.Application.StartupPath + "//ThanhKhoan_dtBCXuatNhapTon.xml");
                    //}

                    if (TKToKhaiNKD != 1)
                    {

                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap LIKE 'NKD%'";
                    }
                    int STT = 0;
                    string maNPL = "";
                    int soToKhaiNhap = 0;
                    string maLoaiHinhNhap = "";
                    DateTime ngayDangKyNhap = new DateTime(1900, 1, 1);

                    for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                    {
                        DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                        if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                        BCXuatNhapTon bc = new BCXuatNhapTon();

                        bc.STT = STT;
                        if (rv["ChuyenMucDichKhac"].ToString() != "")
                            bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                        bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                        bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                        bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                        bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                        if (rv["LuongNPLTaiXuat"].ToString() != "")
                            bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                        bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                        bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                        bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                        bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                        maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                        bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                        bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                        bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                        bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                        bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                        bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                        bc.MaSP = Convert.ToString(rv["MaSP"]);
                        bc.TenSP = Convert.ToString(rv["TenSP"]);
                        bc.NamThanhLy = DateTime.Today.Year;
                        ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                        bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                        bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                        bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);

                        //TODO: NGayThucXuat
                        try
                        {
                            if (rv["NgayThucXuat"] == null || rv["NgayThucXuat"] == DBNull.Value)
                            {
                                //throw new Exception("Ngày thực xuất không được trống!");
                                bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                            }
                            else
                                bc.NgayThucXuat = Convert.ToDateTime(rv["NgayThucXuat"]);
                        }
                        catch (Exception ex)
                        {
                            bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                            Logger.LocalLogger.Instance().WriteMessage("Lỗi thanh khoản do Ngày thực xuất", ex);
                        }

                        //
                        bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                        bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                        soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                        bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                        if (rv["ToKhaiTaiXuat"].ToString() != "")
                        {
                            bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                            bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                        }
                        bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                        bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                        if (rv["ThanhKhoanTiep"].ToString() != "")
                            bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                        else
                        {
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                        }

                        //Xử lý ghi chú là mua VN, chuyển lần sau TK hay chuyển TK xyz
                        if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                        {
                            if (TKToKhaiNKD == 1)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];

                                    //TODO: Test
                                    //string sotkx = rv1["SoToKhaiXuat"].ToString();
                                    //string npl = rv["MaNPL"].ToString();
                                    //string npl1 = rv1["MaNPL"].ToString();

                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim())
                                        && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString()
                                        || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {

                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            //TODO: Comment by Hungtq, 01/11/2011. Khong kiem tra truong hop Am ke tiep cho dieu kien so to khai > 0
                                            //if (rv1["SoToKhaiXuat"].ToString() != "0")
                                            //    bc.ThanhKhoanTiep = "Mua tại VN";
                                            //else
                                            //{
                                            int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                            if (j > 0)
                                                bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                            else
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            //}
                                        }


                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                        }
                        bcXNTCollection.Add(bc);

                    }
                    if (TKToKhaiNKD != 1)
                    {
                        dtBCXuatNhapTon.DefaultView.Sort = "MaNPL, NgayHoanThanhNhap, SoToKhaiNhap, MaLoaiHinhNhap";
                        dtBCXuatNhapTon.DefaultView.RowFilter = " MaLoaiHinhNhap NOT LIKE 'NKD%'";
                        STT = 0;
                        maNPL = "";
                        soToKhaiNhap = 0;
                        maLoaiHinhNhap = "";
                        ngayDangKyNhap = new DateTime(1900, 1, 1);

                        for (int i = 0; i < dtBCXuatNhapTon.DefaultView.Count; i++)
                        {
                            DataRowView rv = dtBCXuatNhapTon.DefaultView[i];
                            if (maNPL != Convert.ToString(rv["MaNPL"]) || soToKhaiNhap != Convert.ToInt32(rv["SoToKhaiNhap"]) || ngayDangKyNhap != Convert.ToDateTime(rv["NgayDangKyNhap"]) || maLoaiHinhNhap != Convert.ToString(rv["MaLoaiHinhNhap"])) STT++;

                            BCXuatNhapTon bc = new BCXuatNhapTon();

                            bc.STT = STT;
                            if (rv["ChuyenMucDichKhac"].ToString() != "")
                                bc.ChuyenMucDichKhac = Convert.ToString(rv["ChuyenMucDichKhac"]);
                            bc.DinhMuc = Convert.ToDecimal(rv["DinhMuc"]);
                            bc.LanThanhLy = Convert.ToInt32(rv["LanThanhLy"]);
                            bc.LuongNhap = Convert.ToDecimal(rv["LuongNhap"]);
                            bc.LuongNPLSuDung = Convert.ToDecimal(rv["LuongNPLSuDung"]);
                            if (rv["LuongNPLTaiXuat"].ToString() != "")
                                bc.LuongNPLTaiXuat = Convert.ToDecimal(rv["LuongNPLTaiXuat"]);
                            bc.LuongSPXuat = Convert.ToDecimal(rv["LuongSPXuat"]);
                            bc.LuongTonCuoi = Convert.ToDecimal(rv["LuongTonCuoi"]);
                            bc.LuongTonDau = Convert.ToDecimal(rv["LuongTonDau"]);
                            bc.MaDoanhNghiep = Convert.ToString(rv["MaDoanhNghiep"]);
                            maNPL = bc.MaNPL = Convert.ToString(rv["MaNPL"]);
                            bc.TenNPL = Convert.ToString(rv["TenNPL"]);
                            bc.DonGiaTT = Convert.ToDouble(rv["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(rv["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(rv["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(rv["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(rv["ThueXNKTon"]);

                            bc.MaSP = Convert.ToString(rv["MaSP"]);
                            bc.TenSP = Convert.ToString(rv["TenSP"]);
                            bc.NamThanhLy = DateTime.Today.Year;
                            ngayDangKyNhap = bc.NgayDangKyNhap = Convert.ToDateTime(rv["NgayDangKyNhap"]);
                            bc.NgayDangKyXuat = Convert.ToDateTime(rv["NgayDangKyXuat"]);
                            bc.NgayHoanThanhNhap = Convert.ToDateTime(rv["NgayHoanThanhNhap"]);
                            bc.NgayHoanThanhXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);

                            //TODO: NGayThucXuat
                            try
                            {
                                if (rv["NgayThucXuat"] == null || rv["NgayThucXuat"] == DBNull.Value)
                                {
                                    //throw new Exception("Ngày thực xuất không được trống!");
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                }
                                else
                                    bc.NgayThucXuat = Convert.ToDateTime(rv["NgayThucXuat"]);
                            }
                            catch (Exception ex)
                            {
                                bc.NgayThucXuat = Convert.ToDateTime(rv["NgayHoanThanhXuat"]);
                                Logger.LocalLogger.Instance().WriteMessage("Lỗi thanh khoản do Ngày thực xuất", ex);
                            }

                            //
                            bc.MaLoaiHinhNhap = Convert.ToString(rv["MaLoaiHinhNhap"]);
                            bc.MaLoaiHinhXuat = Convert.ToString(rv["MaLoaiHinhXuat"]);
                            soToKhaiNhap = bc.SoToKhaiNhap = Convert.ToInt32(rv["SoToKhaiNhap"]);
                            bc.SoToKhaiXuat = Convert.ToInt32(rv["SoToKhaiXuat"]);
                            if (rv["ToKhaiTaiXuat"].ToString() != "")
                            {
                                bc.SoToKhaiTaiXuat = Convert.ToInt32(rv["ToKhaiTaiXuat"]);
                                bc.NgayTaiXuat = Convert.ToDateTime(rv["NgayTaiXuat"]);
                            }
                            bc.TenDVT_NPL = Convert.ToString(rv["TenDVT_NPL"]);
                            bc.TenDVT_SP = Convert.ToString(rv["TenDVT_SP"]);
                            if (rv["ThanhKhoanTiep"].ToString() != "")
                                bc.ThanhKhoanTiep = Convert.ToString(rv["ThanhKhoanTiep"]);
                            else
                            {
                                if (Convert.ToDecimal(rv["LuongTonCuoi"]) > 0)
                                    bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            }
                            if (Convert.ToDecimal(rv["LuongTonCuoi"]) < 0)
                            {
                                if ((i + 1) != dtBCXuatNhapTon.DefaultView.Count)
                                {
                                    DataRowView rv1 = dtBCXuatNhapTon.DefaultView[i + 1];
                                    if ((rv1["MaNPL"].ToString().ToLower().Trim() == rv["MaNPL"].ToString().ToLower().Trim()) && (rv["SoToKhaiNhap"].ToString() != rv1["SoToKhaiNhap"].ToString() || (rv["SoToKhaiNhap"].ToString() == rv1["SoToKhaiNhap"].ToString() && rv["MaLoaiHinhNhap"].ToString() != rv1["MaLoaiHinhNhap"].ToString())))
                                    {
                                        if (rv1["SoToKhaiXuat"].ToString() == rv["SoToKhaiXuat"].ToString())
                                            bc.ThanhKhoanTiep = "Chuyển TK " + rv1["SoToKhaiNhap"];
                                        else
                                        {
                                            if (rv1["SoToKhaiXuat"].ToString() != "0")
                                                bc.ThanhKhoanTiep = "Mua tại VN";
                                            else
                                            {
                                                int j = CheckMuaTaiVN(dtBCXuatNhapTon.DefaultView, i + 1, rv["MaNPL"].ToString().ToLower().Trim(), rv["SoToKhaiXuat"].ToString(), rv["MaSP"].ToString());
                                                if (j > 0)
                                                    bc.ThanhKhoanTiep = "Chuyển TK " + dtBCXuatNhapTon.DefaultView[j]["SoToKhaiNhap"];
                                                else
                                                    bc.ThanhKhoanTiep = "Mua tại VN";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (rv1["MaNPL"].ToString().ToLower() != rv["MaNPL"].ToString().ToLower())
                                            bc.ThanhKhoanTiep = "Mua tại VN";
                                    }
                                }
                            }
                            bcXNTCollection.Add(bc);

                        }
                    }

                    #region Thêm NPL ko tham gia vào thanh khoản
                    if (nplKoTK == 1)
                    {
                        DataRow[] rows = dtNPLNhapTon.Select(" TonDau = TonCuoi AND TonDau > 0");
                        for (int i = 0; i < rows.Length; i++)
                        {
                            DataRow row = rows[i];
                            if (CheckTKExist(bcXNTCollection, Convert.ToInt32(row["SoToKhai"]), row["MaLoaiHinh"].ToString(), Convert.ToDateTime(row["NgayDangKy"])))
                            {
                                BCXuatNhapTon bc = new BCXuatNhapTon();
                                bc.LanThanhLy = this.LanThanhLy;
                                bc.NamThanhLy = this.NgayBatDau.Year;
                                bc.MaDoanhNghiep = this.MaDoanhNghiep;
                                bc.MaNPL = row["MaNPL"].ToString();
                                bc.TenNPL = row["TenNPL"].ToString();
                                bc.TenDVT_NPL = row["TenDVT_NPL"].ToString();
                                bc.SoToKhaiNhap = Convert.ToInt32(row["SoToKhai"]);
                                bc.NgayDangKyNhap = Convert.ToDateTime(row["NgayDangKy"]);
                                bc.NgayHoanThanhNhap = bc.NgayDangKyNhap;
                                bc.MaLoaiHinhNhap = row["MaLoaiHinh"].ToString();
                                bc.LuongNhap = Convert.ToDecimal(row["Luong"]);
                                bc.LuongTonDau = Convert.ToDecimal(row["TonDau"]);
                                bc.LuongNPLSuDung = 0;
                                bc.LuongTonCuoi = Convert.ToDecimal(row["TonDau"]);
                                bc.DonGiaTT = Convert.ToDouble(row["DonGiaTT"]);
                                bc.TyGiaTT = Convert.ToDecimal(row["TyGiaTT"]);
                                bc.ThueSuat = Convert.ToDecimal(row["ThueSuat"]);
                                bc.ThueXNK = Convert.ToDouble(row["ThueXNK"]);
                                bc.ThueXNKTon = Convert.ToDouble(row["TonDauThueXNK"]);
                                bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                                bcXNTCollection.Add(bc);
                            }
                        }
                    }
                    if (ToKhaiKoTK == 1)
                    {
                        DataRow[] rows = dtNPLNhapTon.Select(" TonDau = TonCuoi AND TonDau > 0");
                        for (int i = 0; i < rows.Length; i++)
                        {
                            DataRow row = rows[i];

                            //if (!CheckTKExist(bcXNTCollection, Convert.ToInt32(row["SoToKhai"]), row["MaLoaiHinh"].ToString(), Convert.ToDateTime(row["NgayDangKy"])))
                            //{
                            BCXuatNhapTon bc = new BCXuatNhapTon();
                            bc.LanThanhLy = this.LanThanhLy;
                            bc.NamThanhLy = this.NgayBatDau.Year;
                            bc.MaDoanhNghiep = this.MaDoanhNghiep;
                            bc.MaNPL = row["MaNPL"].ToString();
                            bc.TenNPL = row["TenNPL"].ToString();
                            bc.TenDVT_NPL = row["TenDVT_NPL"].ToString();
                            bc.SoToKhaiNhap = Convert.ToInt32(row["SoToKhai"]);
                            bc.NgayDangKyNhap = Convert.ToDateTime(row["NgayDangKy"]);
                            bc.NgayHoanThanhNhap = bc.NgayDangKyNhap;
                            bc.MaLoaiHinhNhap = row["MaLoaiHinh"].ToString();
                            bc.LuongNhap = Convert.ToDecimal(row["Luong"]);
                            bc.LuongTonDau = Convert.ToDecimal(row["TonDau"]);
                            bc.LuongNPLSuDung = 0;
                            bc.LuongTonCuoi = Convert.ToDecimal(row["TonDau"]);
                            bc.DonGiaTT = Convert.ToDouble(row["DonGiaTT"]);
                            bc.TyGiaTT = Convert.ToDecimal(row["TyGiaTT"]);
                            bc.ThueSuat = Convert.ToDecimal(row["ThueSuat"]);
                            bc.ThueXNK = Convert.ToDouble(row["ThueXNK"]);
                            bc.ThueXNKTon = Convert.ToDouble(row["TonDauThueXNK"]);
                            bc.ThanhKhoanTiep = "Chuyển lần sau TK";
                            bcXNTCollection.Add(bc);
                            //}
                        }
                    }
                    #endregion
                    this.InsertBCXNT(bcXNTCollection, transaction);

                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    maLoaiHinhNhap = "";
                    bool temp3 = false;

                    //Xử lý collection bcXNTCollection trước khi đưa vào xử lý báo cáo này để tổng hợp nên báo cáo thuế XNK
                    BCXuatNhapTonCollection bcTemp = new BCXuatNhapTonCollection();
                    for (int i = 0; i < bcXNTCollection.Count; i++)
                    {
                        if (maNPL == bcXNTCollection[i].MaNPL && soToKhaiNhap == bcXNTCollection[i].SoToKhaiNhap && ngayDangKyNhap == bcXNTCollection[i].NgayDangKyNhap && maLoaiHinhNhap == bcXNTCollection[i].MaLoaiHinhNhap)
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0)
                            {
                                if (!temp3)
                                    temp3 = true;
                                else
                                    bcTemp.Add(bcXNTCollection[i]);
                            }
                            else
                            {
                                temp3 = false;
                            }
                        }
                        else
                        {
                            if (bcXNTCollection[i].LuongTonCuoi < 0) temp3 = true;
                            else temp3 = false;
                            maNPL = bcXNTCollection[i].MaNPL;
                            soToKhaiNhap = bcXNTCollection[i].SoToKhaiNhap;
                            ngayDangKyNhap = bcXNTCollection[i].NgayDangKyNhap;
                            maLoaiHinhNhap = bcXNTCollection[i].MaLoaiHinhNhap;
                        }
                    }
                    for (int i = 0; i < bcTemp.Count; i++)
                    {
                        bcXNTCollection.Remove(bcTemp[i]);
                    }

                    #endregion

                    #region insert BCThueXNK
                    int soToKhaiXuat = 0;
                    maNPL = "";
                    soToKhaiNhap = 0;
                    ngayDangKyNhap = new DateTime(1900, 1, 1);
                    DateTime ngayDangKyXuat = new DateTime(1900, 1, 1);
                    BCThueXNKCollection bcThueXNKCollectin = new BCThueXNKCollection();
                    BCThueXNK bcThueXNK = new BCThueXNK(); ;
                    STT = 0;
                    index = getBKNPLTaiXuat();
                    decimal temp1 = 0;//Biến đánh dấu khi âm NPL

                    //Duyệt từng dòng trong collection bcXNTCollection
                    foreach (BCXuatNhapTon bc in bcXNTCollection)
                    {

                        if (maNPL.ToUpper() == bc.MaNPL.ToUpper() && soToKhaiNhap == bc.SoToKhaiNhap && ngayDangKyNhap == bc.NgayDangKyNhap && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                        {
                            //Nếu đây là tờ khai nhập đã có trong collection bcThueXNCollection
                            if (bc.LuongTonCuoi < 0)
                            {
                                //Nếu lượng tồn cuối nhỏ hơn 0
                                temp1 = bc.LuongTonCuoi; //Gán biến temp1
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung + bc.LuongTonCuoi;
                                bcThueXNK.LuongNPLTon = 0;
                            }
                            else
                            {
                                //Nếu lớn hơn hoặc bằng 0
                                bcThueXNK.LuongNPLSuDung += bc.LuongNPLSuDung;
                                bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                            }
                            //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                            //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                            bcThueXNKCollectin[bcThueXNKCollectin.Count - 1] = bcThueXNK;
                        }
                        else
                        {
                            if (index > 0 && bc.LuongNPLTaiXuat > 0)
                            {
                                //Nếu là tờ khai tái xuất
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                //DataRow dr = GetDonGia_TyGia_ThueSuat_ThueNK(dtNPLNhapTon, bcThueXNK.SoToKhaiNhap, bcThueXNK.NgayDangKyNhap, bcThueXNK.MaNPL);
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiTaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayTaiXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayThucXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;
                                bcThueXNK.LuongNPLSuDung = bc.LuongNPLTaiXuat;
                                bcThueXNK.LuongNPLTon = bc.LuongTonDau - bc.LuongNPLTaiXuat;
                                //bcThueXNK.TienThueHoan = Math.Round((bcThueXNK.LuongNPLSuDung / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //bcThueXNK.TienThueTKTiep = Math.Round((bcThueXNK.LuongNPLTon / bcThueXNK.LuongNhap) * bcThueXNK.ThueNKNop);
                                //if (bcThueXNK.TienThueTKTiep > 0) bcThueXNK.GhiChu = "Chuyển lần sau TK.";
                                bcThueXNKCollectin.Add(bcThueXNK);
                            }
                            else
                            {

                                if (maNPL.ToUpper().Trim() != bc.MaNPL.ToUpper().Trim()) temp1 = 0;//Nếu đây là npl khác thì gán temp1 =0
                                //Tạo 1 dòng báo cáo thuế XNK và gán dữ liệu 
                                bcThueXNK = new BCThueXNK();
                                bcThueXNK.STT = bc.STT;
                                bcThueXNK.LanThanhLy = bc.LanThanhLy;
                                bcThueXNK.NamThanhLy = bc.NamThanhLy;
                                bcThueXNK.MaDoanhNghiep = bc.MaDoanhNghiep;
                                bcThueXNK.SoToKhaiNhap = bc.SoToKhaiNhap;
                                bcThueXNK.NgayDangKyNhap = bc.NgayDangKyNhap;
                                bcThueXNK.NgayThucNhap = bc.NgayHoanThanhNhap;
                                bcThueXNK.MaLoaiHinhNhap = bc.MaLoaiHinhNhap;
                                bcThueXNK.MaNPL = bc.MaNPL;
                                bcThueXNK.TenNPL = bc.TenNPL;
                                bcThueXNK.LuongNhap = bc.LuongTonDau;//bc.LuongTonDau;//huypvt, 10/02/2011: sua luongNhap = LuongTonDau
                                bcThueXNK.TenDVT_NPL = bc.TenDVT_NPL;
                                bcThueXNK.DonGiaTT = Convert.ToDecimal(bc.DonGiaTT);
                                bcThueXNK.TyGiaTT = bc.TyGiaTT;
                                bcThueXNK.ThueSuat = bc.ThueSuat;
                                bcThueXNK.ThueXNK = Convert.ToDecimal(bc.ThueXNK);
                                bcThueXNK.ThueNKNop = Convert.ToDecimal(bc.ThueXNKTon);
                                bcThueXNK.Luong = bc.LuongNhap;
                                bcThueXNK.SoToKhaiXuat = bc.SoToKhaiXuat;
                                bcThueXNK.NgayDangKyXuat = bc.NgayDangKyXuat;
                                //bcThueXNK.NgayThucXuat = bc.NgayHoanThanhXuat;
                                bcThueXNK.NgayThucXuat = bc.NgayThucXuat;
                                bcThueXNK.MaLoaiHinhXuat = bc.MaLoaiHinhXuat;

                                if (temp1 < 0 && soToKhaiXuat == bc.SoToKhaiXuat && ngayDangKyXuat == bc.NgayDangKyXuat)
                                {
                                    //Nếu temp1 <0 và tờ khai xuất dòng trên giống dòng hiện tại
                                    if (0 - temp1 <= bc.LuongTonDau)
                                    {
                                        //Nếu lượng âm mà nhỏ hơn lượng tồn đầu của dòng hiện tại
                                        bcThueXNK.LuongNPLSuDung = 0 - temp1;// Gán lượng sử dụng bằng lượng âm
                                        bcThueXNK.LuongNPLTon = bc.LuongTonDau - bcThueXNK.LuongNPLSuDung;//Gán lượng NPL tồn bằng 
                                        temp1 = 0;
                                    }
                                    else
                                    {
                                        //Nếu lượng âm nhỏ hơn
                                        bcThueXNK.LuongNPLSuDung = bc.LuongTonDau;//Gán lượng sử dụng bằng lượng tồn đầu ==> npl tờ khai nhập này dùng hết
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn = 0
                                        temp1 = temp1 + bcThueXNK.LuongNPLSuDung;// Gán lại biến temp
                                    }
                                }
                                else
                                {
                                    if (bc.LuongTonCuoi < 0)
                                    {
                                        temp1 = bc.LuongTonCuoi;//Gán lại biến temp1
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung + bc.LuongTonCuoi;//Gán npl sử dụng bằng chính nó + thêm lượng tồn cuối
                                        bcThueXNK.LuongNPLTon = 0;//Gán lượng tồn bằng 0
                                    }
                                    else
                                    {
                                        bcThueXNK.LuongNPLSuDung = bc.LuongNPLSuDung;
                                        bcThueXNK.LuongNPLTon = bc.LuongTonCuoi;
                                    }
                                }
                                bcThueXNKCollectin.Add(bcThueXNK);//Thêm 1 dòng vào báo cáo thuế
                                //Lưu lại tờ khai nhập xuất đang xử lý
                                soToKhaiXuat = bcThueXNK.SoToKhaiXuat;
                                ngayDangKyXuat = bcThueXNK.NgayDangKyXuat;
                                soToKhaiNhap = bcThueXNK.SoToKhaiNhap;
                                maNPL = bcThueXNK.MaNPL;
                                ngayDangKyNhap = bc.NgayDangKyNhap;
                            }
                        }

                    }
                    this.InsertBCThueXNK(bcThueXNKCollectin, transaction);
                    #endregion

                    transaction.Commit();
                    this.TrangThaiThanhKhoan = 400;

                }
                catch (Exception ex)
                {
                    this.TrangThaiThanhKhoan = 100;
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        private int CheckMuaTaiVN(DataView dv, int i, string maNPL, string soToKhaiXuat, string maSP)
        {
            for (int j = i; j < dv.Count; j++)
            {
                DataRowView rv2 = dv[j];
                if (rv2["MaNPL"].ToString().ToLower().Trim() == maNPL && soToKhaiXuat == rv2["SoToKhaiXuat"].ToString() && rv2["MaSP"].ToString() == maSP)
                {
                    return j;
                }
            }
            return 0;
        }
        private bool CheckNPLSau(DataTable dt, string MaNPL, int i, DateTime ngayXuat)
        {
            for (int k = i + 1; k < dt.Rows.Count; k++)
            {
#if DEBUG
                //if (dt.Rows[k]["MaNPL"].ToString().ToLower().Trim() == "nhanchinhv"
                //    && MaNPL.Trim().ToLower() == "nhanchinhv"
                //    //&& (dt.Rows[k]["SoToKhai"].ToString().ToLower().Trim() == "107" || dt.Rows[k]["SoToKhai"].ToString().ToLower().Trim() == "1131")
                //    )
                //{
                //}
#endif
                if (dt.Rows[k]["MaNPL"].ToString().ToLower().Trim() == MaNPL.ToLower().Trim()
                    && System.Convert.ToDateTime(System.Convert.ToDateTime(dt.Rows[k]["NgayHoanThanh"]).ToShortDateString()) <= System.Convert.ToDateTime(ngayXuat.ToShortDateString()) //Khong so sanh gio & chenh lech ngay, va Cap nhat NgayThucNhap = NgayHoanThanh
                    && System.Convert.ToDecimal(dt.Rows[k]["TonCuoi"]) >= 0)
                {
                    return false;
                }

            }
            return true;

        }
        private bool GetTKNKeTiep(DataTable dt, int rowIndex, string MaNPL, long soToKhaiNhap, string maLoaiHinh, int namDangKy, int chenhLechNgay, DateTime ngayHoanThanhTKXuat)
        {
            for (int k = rowIndex + 1; k < dt.Rows.Count; k++)
            {
#if DEBUG
                //if (System.Convert.ToInt64(dt.Rows[k]["SoToKhai"]) == 1352)
                //{ }
#endif
                if (System.Convert.ToInt64(dt.Rows[k]["SoToKhai"]) == soToKhaiNhap
                    && dt.Rows[k]["MaLoaiHinh"].ToString().Trim() == maLoaiHinh.Trim()
                    && System.Convert.ToDateTime(dt.Rows[k]["NgayDangKy"]).Year == namDangKy)
                    continue;
                else
                {
                    if (dt.Rows[k]["MaNPL"].ToString().ToLower().Trim() == MaNPL.ToLower().Trim()
                        //&& System.Convert.ToDateTime(dt.Rows[k]["NgayHoanThanh"]).AddDays((double)chenhLechNgay) <= ngayHoanThanhTKXuat)
                        && System.Convert.ToDateTime(dt.Rows[k]["NgayHoanThanh"]) <= ngayHoanThanhTKXuat)
                    {
                        return true;
                    }
                }
            }
            return false;

        }
        private bool CheckTKExist(BCXuatNhapTonCollection bcCol, int soToKhaiNhap, string maLoaiHinhNhap, DateTime ngayDangKyNhap)
        {
            foreach (BCXuatNhapTon bc in bcCol)
            {
                if (bc.SoToKhaiNhap == soToKhaiNhap && bc.MaLoaiHinhNhap == maLoaiHinhNhap && bc.NgayDangKyNhap == ngayDangKyNhap)
                    return true;
            }
            return false;
        }
        private bool CheckNPLInNPLNhapTon(string maNPL, DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL) return true;
            }
            return false;
        }
        private DateTime GetNgayThucXuat(int soToKhaiXuat, DateTime ngayDangKyXuat)
        {
            BKToKhaiXuatCollection bks = this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion;
            foreach (BKToKhaiXuat bk in bks)
                if (soToKhaiXuat == bk.SoToKhai && ngayDangKyXuat == bk.NgayDangKy) return bk.NgayThucXuat;
            return new DateTime(1900, 1, 1);

        }
        private DataRow GetDonGia_TyGia_ThueSuat_ThueNK(DataTable dtNPLNhapTon, int SoToKhaiNhap, DateTime ngayDangKyNhap, string maNPL)
        {
            foreach (DataRow dr in dtNPLNhapTon.Rows)
                if (SoToKhaiNhap == Convert.ToInt32(dr["SoToKhai"]) && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKy"]) && maNPL == dr["MaNPL"].ToString()) return dr;
            return null;
        }
        private void InsertBCXNT(BCXuatNhapTonCollection bcCollection, SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                BCXuatNhapTon bc = new BCXuatNhapTon();
                bc.DeleteDynamicTransaction(transaction, bcCollection[0].LanThanhLy, this.MaDoanhNghiep);
                bc.InsertTransaction(transaction, bcCollection);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        private void InsertBCThueXNK(BCThueXNKCollection bcCollection, SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                BCThueXNK bc = new BCThueXNK();
                bc.DeleteDynamicTransaction(transaction, bcCollection[0].LanThanhLy, this.MaDoanhNghiep);
                bc.InsertTransaction(transaction, bcCollection);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public DataTable GetCanDoiNhapXuat(int SoThapPhan)
        {
            string sql = "SELECT aa.*, bb.Ten as TenNPL FROM (" +
                                "(SELECT a.MaNPL,isNULL( b.LuongNPLNhap,0)as LuongNPLNhap, a.LuongNPLXuat,  " +
                                "ISNULL(b.LuongNPLNhap - a.LuongNPLXuat, - a.LuongNPLXuat)as LuongNPLTon FROM " +
                                "(SELECT MaNPL, sum(luongNPL) as LuongNPLXuat FROM ( SELECT MaNPL, round(LuongNPL," + SoThapPhan + ") as LuongNPL FROM dbo.v_KDT_SXXK_NPLXuatTon WHERE BangKeHoSoThanhLy_ID = " + this.BKCollection[this.getBKToKhaiXuat()].ID + " ) e" +
                                " GROUP BY MaNPL)a " +
                                "LEFT JOIN " +
                                "(SELECT MaNPL,sum(TonDau) as LuongNPLNhap FROM (SELECT MaNPL,round(TonDau," + SoThapPhan + ") as TonDau FROM dbo.v_KDT_SXXK_NPLNhapTon WHERE BangKeHoSoThanhLy_ID = " + this.BKCollection[this.getBKToKhaiNhap()].ID + ") d " +
                                " GROUP BY MaNPL)b " +
                                "ON a.MaNPL = b.MaNPL) " +
                            "UNION " +
                                "(SELECT MaNPL,sum(TonDau) as LuongNPLNhap,0 as LuongNPLXuat, sum(TonDau) as LuongNPLTon " +
                                "FROM (SELECT MaNPL,round(TonDau," + SoThapPhan + ") as TonDau FROM dbo.v_KDT_SXXK_NPLNhapTon WHERE BangKeHoSoThanhLy_ID = " + this.BKCollection[this.getBKToKhaiNhap()].ID + " AND MaNPL not in " +
                                "(SELECT Distinct MaNPL FROM v_KDT_SXXK_NPLXuatTon WHERE BangKeHoSoThanhLy_ID = " + this.BKCollection[this.getBKToKhaiXuat()].ID + ") " +
                                ")e " +
                                "group by MaNPL) " +
                         ") aa" +
                         " INNER JOIN t_SXXK_NguyenPhuLieu bb ON aa.MaNPL = bb.Ma";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            int index = this.getBKNPLXinHuy();
            if (index >= 0)
            {
                this.BKCollection[index].LoadChiTietBangKe();
                foreach (BKNPLXinHuy bk in this.BKCollection[index].bkNPLXHCollection)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                        {
                            dr["LuongNPLNhap"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - bk.LuongHuy;
                            dr["LuongNPLTon"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - Convert.ToDecimal(dr["LuongNPLXuat"]);
                            break;
                        }
                    }
                }
            }
            //Thanh ly bang ke NPL tai xuat
            index = this.getBKNPLTaiXuat();
            if (index >= 0)
            {
                this.BKCollection[index].LoadChiTietBangKe();
                foreach (BKNPLTaiXuat bk in this.BKCollection[index].bkNPLTXCollection)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                        {
                            dr["LuongNPLXuat"] = Convert.ToDecimal(dr["LuongNPLXuat"]) + bk.LuongTaiXuat;
                            dr["LuongNPLTon"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - Convert.ToDecimal(dr["LuongNPLXuat"]);
                            break;
                        }
                    }
                }
            }

            //Thanh ly bang ke NPL nop thue
            //index = this.getBKNPLNopThue();
            //if (index >= 0)
            //{
            //    DataColumn col = new DataColumn("LuongNT", typeof(decimal));
            //    dtNPLNhapTon.Columns.Add(col);
            //    this.BKCollection[index].LoadChiTietBangKe();
            //    foreach (BKNPLNopThueTieuThuNoiDia bk in this.BKCollection[index].bkNPLNTCollection)
            //    {
            //        foreach (DataRow dr in dtNPLNhapTon.Rows)
            //        {
            //            if (bk.SoToKhai == Convert.ToInt32(dr["SoToKhai"]) && bk.MaLoaiHinh == Convert.ToString(dr["MaLoaiHinh"]) && bk.NamDangKy == Convert.ToInt16(dr["NamDangKy"]) &&
            //                bk.MaHaiQuan == Convert.ToString(dr["MaHaiQuan"]) && bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
            //            {
            //                dr["TonCuoi"] = Convert.ToDecimal(dr["TonCuoi"]) - bk.LuongNopThue;
            //                dr["LuongNT"] = bk.LuongNopThue;
            //                break;
            //            }
            //        }
            //    }
            //}
            //Thanh ly bang ke NPL chua thanh ly
            index = this.getBKNPLChuaThanhLY();
            if (index >= 0)
            {

                this.BKCollection[index].LoadChiTietBangKe();
                foreach (BKNPLChuaThanhLy bk in this.BKCollection[index].bkNPLCTLCollection)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                        {
                            dr["LuongNPLNhap"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - bk.Luong;
                            dr["LuongNPLTon"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - Convert.ToDecimal(dr["LuongNPLXuat"]);
                            break;
                        }
                    }
                }
            }
            index = this.getBKNPLTuCungUng();
            if (index >= 0)
            {
                this.BKCollection[index].LoadChiTietBangKe();
                foreach (BKNPLTuCungUng bk in this.BKCollection[index].bkNPLTCUCollection)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                        {
                            dr["LuongNPLNhap"] = Convert.ToDecimal(dr["LuongNPLNhap"]) + bk.LuongTuCungUng;
                            dr["LuongNPLTon"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - Convert.ToDecimal(dr["LuongNPLXuat"]);
                            break;
                        }
                    }
                }
            }
            //Thanh ly bang ke NPL xuat gia cong
            index = this.getBKNPLXuatGiaCong();
            if (index >= 0)
            {

                this.BKCollection[index].LoadChiTietBangKe();
                foreach (BKNPLXuatGiaCong bk in this.BKCollection[index].bkNPLXGCCollection)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (bk.MaNPL.ToUpper() == Convert.ToString(dr["MaNPL"]).ToUpper())
                        {
                            dr["LuongNPLNhap"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - bk.LuongXuat;
                            dr["LuongNPLTon"] = Convert.ToDecimal(dr["LuongNPLNhap"]) - Convert.ToDecimal(dr["LuongNPLXuat"]);
                            break;
                        }
                    }
                }
            }
            return dt;
        }
        public int CheckHSTKTruocDoDaDong()
        {
            string sql = "SELECT count(*) FROM t_KDT_SXXK_HoSoThanhLyDangKy WHERE LanThanhLy < " + this.LanThanhLy + " AND UserName = '" + this.UserName + "' AND TrangThaiThanhKhoan < 401 AND MaDoanhNghiep = '" + this.MaDoanhNghiep + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int t = 0;
            try
            {
                t = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch
            {
                t = 0;
            }
            return t;
        }
        public int FixDB(string maDN)
        {
            string spName = "p_FixDb";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            return db.ExecuteNonQuery(dbCommand);
        }
        public void FixOldData(string maHaiQuan, string maDoanhNghiep)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sp = "CapNhatThueXNKTon";
                    DbCommand command = db.GetStoredProcCommand(sp);
                    db.AddInParameter(command, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
                    db.ExecuteNonQuery(command, transaction);
                    sp = "LamTronThue";
                    command = db.GetStoredProcCommand(sp);
                    db.ExecuteNonQuery(command, transaction);
                    BCThueXNKCollection cols = new BCThueXNK().SelectCollectionDynamicTransaction("Luong is Null OR Luong = 0", "", transaction);
                    foreach (BCThueXNK bc in cols)
                    {
                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                        nplNhapTon.SoToKhai = bc.SoToKhaiNhap;
                        nplNhapTon.NamDangKy = Convert.ToInt16(bc.NgayDangKyNhap.Year);
                        nplNhapTon.MaLoaiHinh = bc.MaLoaiHinhNhap;
                        nplNhapTon.MaHaiQuan = maHaiQuan;
                        nplNhapTon.MaNPL = bc.MaNPL;
                        nplNhapTon.Load(transaction);

                        bc.Luong = Convert.ToDecimal(nplNhapTon.Luong);
                        bc.TenNPL = nplNhapTon.TenNPL;
                        bc.ThueXNK = Convert.ToDecimal(nplNhapTon.ThueXNK);
                        bc.UpdateTransaction(transaction);
                    }
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //#region WS.Softech
        //public HoSoThanhLyDangKyInfo ExportToInfo()
        //{
        //    HoSoThanhLyDangKyInfo hstlInfo = new HoSoThanhLyDangKyInfo();
        //    hstlInfo.Ma_DVKB = this.MaDoanhNghiep;
        //    hstlInfo.MaDoanhNghiep = this.MaDoanhNghiep;
        //    hstlInfo.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //    hstlInfo.NamTiepNhan = this.NamTiepNhan;
        //    hstlInfo.NgayTiepNhan = this.NgayTiepNhan;
        //    hstlInfo.SoTiepNhan = this.SoTiepNhan;
        //    hstlInfo.TrangThaiXL = this.TrangThaiXuLy;
        //    if (this.BKCollection == null || this._BKCollection.Count == 0)
        //        this.BKCollection = BangKeHoSoThanhLy.SelectCollectionBy_MaterID(this.ID);

        //    hstlInfo.HoSoThanhLyCollection = new HoSoThanhLyInfo[this.BKCollection.Count];
        //    int i = 0;
        //    foreach (BangKeHoSoThanhLy bk in this.BKCollection)
        //    {

        //        #region  cap nhat ho dang ky detail
        //        HoSoThanhLyInfo hs = new HoSoThanhLyInfo();
        //        hs.MaBangKe = bk.MaBangKe;
        //        hs.MaDoanhNghiep = this.MaDoanhNghiep;
        //        hs.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //        hs.MaHaiQuanTiepNhanBangKe = this.MaHaiQuanTiepNhan;
        //        hs.NamTiepNhan = this.NamTiepNhan;
        //        hs.NamTiepNhanBangKe = this.NamTiepNhan;
        //        hs.SoTiepNhan = this.SoTiepNhan;
        //        hs.SoTiepNhanBangKe = this.SoTiepNhan;
        //        hs.STTBangKe = bk.STTHang;
        //        hs.TenBangKe = bk.MaBangKe;
        //        hstlInfo.HoSoThanhLyCollection[i++] = hs;
        //        #endregion
        //        switch (bk.MaBangKe.Trim())
        //        {
        //            case "DTLNPLCHUATL":
        //                {
        //                    #region bảng kê nguyên phụ liệu chưa thanh lý
        //                    if (bk.bkNPLCTLCollection == null || bk.bkNPLCTLCollection.Count == 0)
        //                        bk.bkNPLCTLCollection = BKNPLChuaThanhLy.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLChuaThanhLyDangKyInfo info = new BKNPLChuaThanhLyDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLChuaThanhLyInFo[bk.bkNPLCTLCollection.Count];
        //                    int chuaTLIndex = 0;
        //                    foreach (BKNPLChuaThanhLy bkChuaThanhLy in bk.bkNPLCTLCollection)
        //                    {
        //                        BKNPLChuaThanhLyInFo entity = new BKNPLChuaThanhLyInFo();
        //                        entity.LUONG = bkChuaThanhLy.Luong;
        //                        entity.MA_DVT = bkChuaThanhLy.DVT_ID;
        //                        entity.MA_NPL = bkChuaThanhLy.MaNPL;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanTK = bkChuaThanhLy.MaHaiQuan;
        //                        entity.MaloaiHinh = bkChuaThanhLy.MaLoaiHinh;
        //                        entity.NamDangKy = bkChuaThanhLy.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkChuaThanhLy.NgayDangKy;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkChuaThanhLy.SoToKhai;
        //                        entity.STTHang = bkChuaThanhLy.STTHang;
        //                        entity.Ten_DVT = bkChuaThanhLy.TenDVT;
        //                        entity.TEN_NPL = bkChuaThanhLy.TenNPL;
        //                        info.NPLCollection[chuaTLIndex++] = entity;
        //                    }
        //                    hstlInfo.BKChuaThanhLy = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLCHITIETNT":
        //                {
        //                    #region bảng kê nguyên phụ liệu tạm nộp thuế
        //                    if (bk.bkNPLTNTCollection == null || bk.bkNPLTNTCollection.Count == 0)
        //                        bk.bkNPLTNTCollection = BKNPLTamNopThue.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKTamNopThueDangKyInfo info = new BKTamNopThueDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKTamNopThueInfo[bk.bkNPLTNTCollection.Count];
        //                    int tamNopThueIndex = 0;
        //                    foreach (BKNPLTamNopThue bkTamNopThue in bk.bkNPLTNTCollection)
        //                    {
        //                        BKTamNopThueInfo entity = new BKTamNopThueInfo();
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanTK = bkTamNopThue.MaHaiQuan;
        //                        entity.MaloaiHinh = bkTamNopThue.MaLoaiHinh;
        //                        entity.MaNPL = bkTamNopThue.MaNPL;
        //                        entity.NamDangKy = bkTamNopThue.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NGAY_NT = bkTamNopThue.NgayNopThue;
        //                        entity.NOP_CLGIA = bkTamNopThue.Nop_CLGia;
        //                        entity.NOP_NK = bkTamNopThue.Nop_NK;
        //                        entity.NOP_TTDB = bkTamNopThue.Nop_TTDB;
        //                        entity.NOP_VAT = bkTamNopThue.Nop_VAT;
        //                        entity.SO_TBT_QDDC = bkTamNopThue.So_TBT_QDDC;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkTamNopThue.SoToKhai;
        //                        entity.STTHang = bkTamNopThue.STTHang;
        //                        entity.TenNPL = bkTamNopThue.TenNPL;
        //                        info.NPLCollection[tamNopThueIndex++] = entity;
        //                    }
        //                    hstlInfo.BKTamNopThue = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLNPLNKD":
        //                {
        //                    #region bảng kê nguyên phụ liệu nhập kinh doanh
        //                    if (bk.bkNPLNKDCollection == null || bk.bkNPLNKDCollection.Count == 0)
        //                        bk.bkNPLNKDCollection = BKNPLXuatSuDungNKD.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLNhapKinhDoanhDangKyInFo info = new BKNPLNhapKinhDoanhDangKyInFo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLNhapKinhDoanhInfo[bk.bkNPLTNTCollection.Count];
        //                    int index = 0;
        //                    foreach (BKNPLXuatSuDungNKD bkDetail in bk.bkNPLNKDCollection)
        //                    {
        //                        BKNPLNhapKinhDoanhInfo entity = new BKNPLNhapKinhDoanhInfo();
        //                        entity.LUONG_SD = bkDetail.LuongSuDung;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.MA_NPL = bkDetail.MaNPL;
        //                        entity.MA_SP = bkDetail.MaSP;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanXuat = bkDetail.MaHaiQuanXuat;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.MaLoaiHinhXuat = bkDetail.MaLoaiHinhXuat;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamDangKyXuat = bkDetail.NamDangKyXuat;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.NgayDangKyXuat = bkDetail.NgayDangKyXuat;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.SoToKhaiXuat = bkDetail.SoToKhaiXuat;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        info.NPLCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKNhapKinhDoanh = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLNPLNT":
        //                {
        //                    #region bảng kê nguyên phụ liệu nộp thuế
        //                    if (bk.bkNPLNTCollection == null || bk.bkNPLNTCollection.Count == 0)
        //                        bk.bkNPLNTCollection = BKNPLNopThueTieuThuNoiDia.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLNopThueDangKyInfo info = new BKNPLNopThueDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLNopThueInfo[bk.bkNPLNTCollection.Count];
        //                    int index = 0;
        //                    foreach (BKNPLNopThueTieuThuNoiDia bkDetail in bk.bkNPLNTCollection)
        //                    {
        //                        BKNPLNopThueInfo entity = new BKNPLNopThueInfo();
        //                        entity.LUONG_NT = bkDetail.LuongNopThue;
        //                        entity.MA_DVT = bkDetail.DVT_ID;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanTK = bkDetail.MaHaiQuan;
        //                        entity.MaloaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.MaNPL = bkDetail.MaNPL;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NGAYDK = bkDetail.NgayDangKy;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.Ten_DVT = bkDetail.TenDVT;
        //                        entity.TenNPL = bkDetail.TenNPL;
        //                        info.NPLCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKNopThue = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLNPLTX":
        //                {
        //                    #region bảng kê nguyên phụ liệu tái xuất
        //                    if (bk.bkNPLTXCollection == null || bk.bkNPLTXCollection.Count == 0)
        //                        bk.bkNPLTXCollection = BKNPLTaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLTaiXuatDangKyInfo info = new BKNPLTaiXuatDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLTaiXuatInfo[bk.bkNPLTXCollection.Count];
        //                    int index = 0;
        //                    foreach (BKNPLTaiXuat bkDetail in bk.bkNPLTXCollection)
        //                    {
        //                        BKNPLTaiXuatInfo entity = new BKNPLTaiXuatInfo();
        //                        entity.LUONGTX = bkDetail.LuongTaiXuat;
        //                        entity.MA_DVT = bkDetail.DVT_ID;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.Ma_NPL = bkDetail.MaNPL;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanTaiXuat = bkDetail.MaHaiQuanXuat;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.MaLoaiHinhXuat = bkDetail.MaLoaiHinhXuat;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamDangKyXuat = bkDetail.NamDangKyXuat;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.NgayDangKyXuat = bkDetail.NgayDangKyXuat;
        //                        //entity.NGAYTH=bkDetail.ngay
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.SoToKhaiXuat = bkDetail.SoToKhaiXuat;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.Ten_DVT = bkDetail.TenDVT;
        //                        entity.TEN_NPL = bkDetail.TenNPL;
        //                        info.NPLCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKTaiXuat = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLNPLXGC":
        //                {
        //                    #region bảng kê nguyên phụ liệu xuất gia công
        //                    if (bk.bkNPLXGCCollection == null || bk.bkNPLXGCCollection.Count == 0)
        //                        bk.bkNPLXGCCollection = BKNPLXuatGiaCong.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLXuatGiaCongDangKyInfo info = new BKNPLXuatGiaCongDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLXuatGiaCongInfo[bk.bkNPLXGCCollection.Count];
        //                    int index = 0;
        //                    foreach (BKNPLXuatGiaCong bkDetail in bk.bkNPLXGCCollection)
        //                    {
        //                        BKNPLXuatGiaCongInfo entity = new BKNPLXuatGiaCongInfo();
        //                        entity.LUONG_XGC = bkDetail.LuongXuat;
        //                        entity.MA_DVT = bkDetail.DVT_ID;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaHaiQuanXuat = bkDetail.MaHaiQuanXuat;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.MaLoaiHinhXuat = bkDetail.MaLoaiHinhXuat;
        //                        entity.MaNPL = bkDetail.MaNPL;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamDangKyXuat = bkDetail.NamDangKyXuat;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.NgayDangKyXuat = bkDetail.NgayDangKyXuat;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.SoToKhaiXuat = bkDetail.SoToKhaiXuat;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.Ten_DVT = bkDetail.TenDVT;
        //                        entity.TenNPL = bkDetail.TenNPL;
        //                        info.NPLCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKXuatGiaCong = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLNPLXH":
        //                {
        //                    #region bảng kê nguyên phụ liệu xin hủy
        //                    if (bk.bkNPLXHCollection == null || bk.bkNPLXHCollection.Count == 0)
        //                        bk.bkNPLXHCollection = BKNPLXinHuy.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKNPLHuyBieuTangDangKyInfo info = new BKNPLHuyBieuTangDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.NPLCollection = new BKNPLHuyBieuTangInfo[bk.bkNPLXHCollection.Count];
        //                    int index = 0;
        //                    foreach (BKNPLXinHuy bkDetail in bk.bkNPLXHCollection)
        //                    {
        //                        BKNPLHuyBieuTangInfo entity = new BKNPLHuyBieuTangInfo();
        //                        entity.BienBanHuy = bkDetail.BienBanHuy;
        //                        entity.LuongHuy = bkDetail.LuongHuy;
        //                        entity.MA_DVT = bkDetail.DVT_ID;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.MaNPL = bkDetail.MaNPL;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.NgayHuy = bkDetail.NgayHuy;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.Ten_DVT = bkDetail.TenDVT;
        //                        entity.TenNPL = bkDetail.TenNPL;
        //                        info.NPLCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKHuyBieuTang = info;
        //                    #endregion
        //                }
        //                break; ;
        //            case "DTLTKN":
        //                {
        //                    #region bảng kê tờ khai nhập
        //                    if (bk.bkTKNCollection == null || bk.bkTKNCollection.Count == 0)
        //                        bk.bkTKNCollection = BKToKhaiNhap.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKToKhaiNhapDangKyInfo info = new BKToKhaiNhapDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.ToKhaiCollection = new BKToKhaiNhapInfo[bk.bkTKNCollection.Count];
        //                    int index = 0;
        //                    foreach (BKToKhaiNhap bkDetail in bk.bkTKNCollection)
        //                    {
        //                        BKToKhaiNhapInfo entity = new BKToKhaiNhapInfo();
        //                        entity.GHICHU = bkDetail.GhiChu;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.NGAYTHN = bkDetail.NgayThucNhap;
        //                        info.ToKhaiCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKToKhaiNhap = info;
        //                    #endregion
        //                }
        //                break;
        //            case "DTLTKX":
        //                {
        //                    #region bảng kê tờ khai xuất
        //                    if (bk.bkTKXColletion == null || bk.bkTKXColletion.Count == 0)
        //                        bk.bkTKXColletion = BKToKhaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID(bk.ID);
        //                    BKToKhaiXuatDangKyInfo info = new BKToKhaiXuatDangKyInfo();
        //                    info.Ngay_TNCT = new DateTime(1900, 1, 1);
        //                    info.Ma_DVKB = this.MaDoanhNghiep;
        //                    info.MaDoanhNghiep = this.MaDoanhNghiep;
        //                    info.NamTiepNhan = this.NamTiepNhan;
        //                    info.NgayTiepNhan = this.NgayTiepNhan;
        //                    info.SoHoSoThanhLy = bk.SoHSTL;
        //                    info.SoTiepNhan = this.SoTiepNhan;
        //                    info.TrangThaiXL = bk.TrangThaiXL;
        //                    info.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                    info.ToKhaiCollection = new BKToKhaiXuatInfo[bk.bkTKXColletion.Count];
        //                    int index = 0;
        //                    foreach (BKToKhaiXuat bkDetail in bk.bkTKXColletion)
        //                    {
        //                        BKToKhaiXuatInfo entity = new BKToKhaiXuatInfo();
        //                        entity.GHICHU = bkDetail.GhiChu;
        //                        entity.Ma_HQ = bkDetail.MaHaiQuan;
        //                        entity.MaHaiQuan = this.MaHaiQuanTiepNhan;
        //                        entity.MaLoaiHinh = bkDetail.MaLoaiHinh;
        //                        entity.NamDangKy = bkDetail.NamDangKy;
        //                        entity.NamTiepNhan = this.NamTiepNhan;
        //                        entity.NgayDangKy = bkDetail.NgayDangKy;
        //                        entity.SoTiepNhan = this.SoTiepNhan;
        //                        entity.SoToKhai = bkDetail.SoToKhai;
        //                        entity.STTHang = bkDetail.STTHang;
        //                        entity.NGAYTHX = bkDetail.NgayThucXuat;
        //                        info.ToKhaiCollection[index++] = entity;
        //                    }
        //                    hstlInfo.BKToKhaiXuat = info;
        //                    #endregion
        //                }
        //                break;
        //        }
        //    }
        //    return hstlInfo;
        //}

        //public void WSSend()
        //{
        //    KDT_SXXK_Service kdt = new KDT_SXXK_Service();
        //    this.SoTiepNhan = kdt.ThanhKhoan_Send(ExportToInfo());
        //    this.NgayTiepNhan = DateTime.Today;
        //    this.NamTiepNhan = (short)this.NgayTiepNhan.Year;
        //    this.TrangThaiXuLy = BLL.TrangThaiXuLy.CHO_DUYET;
        //    this.InsertUpdate();
        //}

        //public void WSCancel()
        //{
        //    KDT_SXXK_Service kdt = new KDT_SXXK_Service();
        //    kdt.ThanhKhoan_Cancel(this.SoTiepNhan, this.MaHaiQuanTiepNhan, this.NamTiepNhan);
        //    this.NgayTiepNhan = new DateTime(1900, 1, 1);
        //    this.NamTiepNhan = 0;
        //    this.TrangThaiXuLy = BLL.TrangThaiXuLy.CHUA_KHAI_BAO;
        //    this.SoTiepNhan = 0;
        //    this.InsertUpdate();
        //}

        //public void WSRequest()
        //{
        //    KDT_SXXK_Service kdt = new KDT_SXXK_Service();
        //    HoSoThanhLyDangKyInfo hstl = kdt.ThanhKhoan_RequestStatus(this.SoTiepNhan, this.MaHaiQuanTiepNhan, this.NamTiepNhan);
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            this.TrangThaiXuLy = hstl.TrangThaiXL;
        //            BLL.SXXK.ThanhKhoan.LanThanhLyBase lanthanhly = new Company.BLL.SXXK.ThanhKhoan.LanThanhLyBase();
        //            if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //            {
        //                lanthanhly.LanThanhLy = hstl.LanThanhLy;
        //                lanthanhly.MaDoanhNghiep = hstl.MaDoanhNghiep;
        //                lanthanhly.MaHaiQuan = hstl.MaHaiQuan;
        //                lanthanhly.NgayBatDau = hstl.NgayBatDauTL;
        //                lanthanhly.NamThanhLy = (short)hstl.NgayBatDauTL.Year;
        //                lanthanhly.SoHoSo = hstl.SoHoSoThanhLy;
        //                lanthanhly.TrangThai = hstl.TrangThaiCuaHSBenSXXK;
        //                lanthanhly.InsertUpdateTransaction(transaction);
        //            }
        //            if (this.BKCollection == null || this.BKCollection.Count == 0)
        //                LoadBKCollection();
        //            BangKeHoSoThanhLy bangkeDelete = new BangKeHoSoThanhLy();
        //            bangkeDelete.DeleteCollection(this.BKCollection, transaction);
        //            this.BKCollection.Clear();

        //            #region  Bảng kê tờ khai nhập
        //            if (hstl.BKToKhaiNhap != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLTKN";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê Tờ khai nhập khẩu thanh lý ";
        //                bk.TrangThaiXL = hstl.BKToKhaiNhap.TrangThaiXL;
        //                bk.bkTKNCollection = new BKToKhaiNhapCollection();
        //                foreach (BKToKhaiNhapInfo info in hstl.BKToKhaiNhap.ToKhaiCollection)
        //                {
        //                    BKToKhaiNhap entity = new BKToKhaiNhap();
        //                    entity.GhiChu = info.GHICHU;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.NgayThucNhap = info.NGAYTHN;
        //                    bk.bkTKNCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.ToKhaiNhap tkNhap = new Company.BLL.SXXK.ThanhKhoan.ToKhaiNhap();
        //                        tkNhap.LanThanhLy = lanthanhly.LanThanhLy;
        //                        tkNhap.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        tkNhap.MaHaiQuan = entity.MaHaiQuan;
        //                        tkNhap.MaLoaiHinh = entity.MaLoaiHinh;
        //                        tkNhap.NamDangKy = entity.NamDangKy;
        //                        tkNhap.SoToKhai = entity.SoToKhai;
        //                        if (!tkNhap.Load())
        //                            tkNhap.InsertTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion

        //            #region  Bảng kê tờ khai xuất
        //            if (hstl.BKToKhaiXuat != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLTKX";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê Tờ khai xuất khẩu thanh lý ";
        //                bk.TrangThaiXL = hstl.BKToKhaiXuat.TrangThaiXL;
        //                bk.bkTKXColletion = new BKToKhaiXuatCollection();
        //                foreach (BKToKhaiXuatInfo info in hstl.BKToKhaiXuat.ToKhaiCollection)
        //                {
        //                    BKToKhaiXuat entity = new BKToKhaiXuat();
        //                    entity.GhiChu = info.GHICHU;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.NgayThucXuat = info.NGAYTHX;
        //                    bk.bkTKXColletion.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.ToKhaiXuat tkXuat = new Company.BLL.SXXK.ThanhKhoan.ToKhaiXuat();
        //                        tkXuat.LanThanhLy = lanthanhly.LanThanhLy;
        //                        tkXuat.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        tkXuat.MaHaiQuan = lanthanhly.MaHaiQuan;
        //                        tkXuat.MaLoaiHinh = entity.MaLoaiHinh;
        //                        tkXuat.NamDangKy = entity.NamDangKy;
        //                        tkXuat.SoToKhai = entity.SoToKhai;
        //                        tkXuat.NgayThucXuat = entity.NgayThucXuat;
        //                        tkXuat.MaHaiQuanXuat = info.Ma_HQ;
        //                        if (!tkXuat.Load())
        //                            tkXuat.InsertTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion
        //            #region  Bảng kê chưa thanh lý
        //            if (hstl.BKChuaThanhLy != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLCHUATL";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê chi tiết lượng NPL sử dụng chưa thanh lý";
        //                bk.TrangThaiXL = hstl.BKChuaThanhLy.TrangThaiXL;
        //                bk.bkNPLCTLCollection = new BKNPLChuaThanhLyCollection();
        //                foreach (BKNPLChuaThanhLyInFo info in hstl.BKChuaThanhLy.NPLCollection)
        //                {
        //                    BKNPLChuaThanhLy entity = new BKNPLChuaThanhLy();
        //                    entity.Luong = info.LUONG;
        //                    entity.DVT_ID = info.MA_DVT;
        //                    entity.MaNPL = info.MA_NPL;
        //                    entity.MaHaiQuan = info.MaHaiQuanTK;
        //                    entity.MaLoaiHinh = info.MaloaiHinh;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenDVT = info.Ten_DVT;
        //                    entity.TenNPL = info.TEN_NPL;
        //                    bk.bkNPLCTLCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLChuaThanhLy nplChuaThanhLy = new Company.BLL.SXXK.ThanhKhoan.NPLChuaThanhLy();
        //                        nplChuaThanhLy.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplChuaThanhLy.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplChuaThanhLy.MaHaiQuan = entity.MaHaiQuan;
        //                        nplChuaThanhLy.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplChuaThanhLy.MaNPL = entity.MaNPL;
        //                        nplChuaThanhLy.NamDangKy = entity.NamDangKy;
        //                        nplChuaThanhLy.SoLuong = entity.Luong;
        //                        nplChuaThanhLy.SoToKhai = entity.SoToKhai;
        //                        nplChuaThanhLy.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion  Bảng kê chưa thanh lý

        //            #region  Bảng kê hủy biếu tặng
        //            if (hstl.BKHuyBieuTang != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLXH";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê NPL huỷ, biếu tặng";
        //                bk.TrangThaiXL = hstl.BKHuyBieuTang.TrangThaiXL;
        //                bk.bkNPLXHCollection = new BKNPLXinHuyCollection();
        //                foreach (BKNPLHuyBieuTangInfo info in hstl.BKHuyBieuTang.NPLCollection)
        //                {
        //                    BKNPLXinHuy entity = new BKNPLXinHuy();
        //                    entity.BienBanHuy = info.BienBanHuy;
        //                    entity.LuongHuy = info.LuongHuy;
        //                    entity.DVT_ID = info.MA_DVT;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.MaNPL = info.MaNPL;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.NgayHuy = info.NgayHuy;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenDVT = info.Ten_DVT;
        //                    entity.TenNPL = info.TenNPL;
        //                    bk.bkNPLXHCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLHuy nplHuy = new Company.BLL.SXXK.ThanhKhoan.NPLHuy();
        //                        nplHuy.BienBanHuy = entity.BienBanHuy;
        //                        nplHuy.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplHuy.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplHuy.MaHaiQuan = entity.MaHaiQuan;
        //                        nplHuy.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplHuy.MaNPL = entity.MaNPL;
        //                        nplHuy.NamDangKy = entity.NamDangKy;
        //                        nplHuy.NgayHuy = entity.NgayHuy;
        //                        nplHuy.SoLuongHuy = entity.LuongHuy;
        //                        nplHuy.SoToKhai = entity.SoToKhai;
        //                        nplHuy.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion  Bảng kê chưa thanh lý

        //            #region  Bảng kê nhập kinh doanh
        //            if (hstl.BKNhapKinhDoanh != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLNKD";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê NPL xuất sử dụng tờ khai NKD";
        //                bk.TrangThaiXL = hstl.BKNhapKinhDoanh.TrangThaiXL;
        //                bk.bkNPLNKDCollection = new BKNPLXuatSuDungNKDCollection();
        //                foreach (BKNPLNhapKinhDoanhInfo info in hstl.BKNhapKinhDoanh.NPLCollection)
        //                {
        //                    BKNPLXuatSuDungNKD entity = new BKNPLXuatSuDungNKD();
        //                    entity.LuongSuDung = info.LUONG_SD;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaNPL = info.MA_NPL;
        //                    entity.MaSP = info.MA_SP;
        //                    entity.MaHaiQuanXuat = info.MaHaiQuanXuat;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.MaLoaiHinhXuat = info.MaLoaiHinhXuat;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NamDangKyXuat = info.NamDangKyXuat;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.NgayDangKyXuat = info.NgayDangKyXuat;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.SoToKhaiXuat = info.SoToKhaiXuat;
        //                    entity.STTHang = info.STTHang;
        //                    bk.bkNPLNKDCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLXuatNKD nplNKD = new Company.BLL.SXXK.ThanhKhoan.NPLXuatNKD();
        //                        nplNKD.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplNKD.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplNKD.MaHaiQuan = entity.MaHaiQuan;
        //                        nplNKD.MaHaiQuanXuat = entity.MaHaiQuanXuat;
        //                        nplNKD.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplNKD.MaLoaiHinhXuat = entity.MaLoaiHinhXuat;
        //                        nplNKD.MaNPL = entity.MaNPL;
        //                        nplNKD.MaSP = entity.MaSP;
        //                        nplNKD.NamDangKy = entity.NamDangKy;
        //                        nplNKD.NamDangKyXuat = entity.NamDangKyXuat;
        //                        nplNKD.SoLuongSuDung = entity.LuongSuDung;
        //                        nplNKD.SoToKhai = entity.SoToKhai;
        //                        nplNKD.SoToKhaiXuat = entity.SoToKhaiXuat;
        //                        nplNKD.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion  Bảng kê chưa thanh lý

        //            #region  Bảng kê NPL nộp thuế
        //            if (hstl.BKNopThue != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLNT";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê NPL nộp thuế tiêu thụ nội địa ";
        //                bk.TrangThaiXL = hstl.BKNopThue.TrangThaiXL;
        //                bk.bkNPLNTCollection = new BKNPLNopThueTieuThuNoiDiaCollection();
        //                foreach (BKNPLNopThueInfo info in hstl.BKNopThue.NPLCollection)
        //                {
        //                    BKNPLNopThueTieuThuNoiDia entity = new BKNPLNopThueTieuThuNoiDia();
        //                    entity.LuongNopThue = info.LUONG_NT;
        //                    entity.DVT_ID = info.MA_DVT;
        //                    entity.MaHaiQuan = info.MaHaiQuanTK;
        //                    entity.MaLoaiHinh = info.MaloaiHinh;
        //                    entity.MaNPL = info.MaNPL;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayDangKy = info.NGAYDK;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenDVT = info.Ten_DVT;
        //                    entity.TenNPL = info.TenNPL;
        //                    bk.bkNPLNTCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLNopThue nplNopThue = new Company.BLL.SXXK.ThanhKhoan.NPLNopThue();
        //                        nplNopThue.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplNopThue.LuongNopThue = entity.LuongNopThue;
        //                        nplNopThue.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplNopThue.MaHaiQuan = entity.MaHaiQuan;
        //                        nplNopThue.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplNopThue.MaNPL = entity.MaNPL;
        //                        nplNopThue.NamDangKy = entity.NamDangKy;
        //                        nplNopThue.SoToKhai = entity.SoToKhai;
        //                        nplNopThue.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion

        //            #region  Bảng kê NPL tái xuất
        //            if (hstl.BKTaiXuat != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLTX";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê NPL tái xuất ";
        //                bk.TrangThaiXL = hstl.BKTaiXuat.TrangThaiXL;
        //                bk.bkNPLTXCollection = new BKNPLTaiXuatCollection();
        //                foreach (BKNPLTaiXuatInfo info in hstl.BKTaiXuat.NPLCollection)
        //                {
        //                    BKNPLTaiXuat entity = new BKNPLTaiXuat();
        //                    entity.LuongTaiXuat = info.LUONGTX;
        //                    entity.DVT_ID = info.MA_DVT;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaNPL = info.Ma_NPL;
        //                    entity.MaHaiQuanXuat = info.MaHaiQuanTaiXuat;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.MaLoaiHinhXuat = info.MaLoaiHinhXuat;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NamDangKyXuat = info.NamDangKyXuat;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.NgayDangKyXuat = info.NgayDangKyXuat;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.SoToKhaiXuat = info.SoToKhaiXuat;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenDVT = info.Ten_DVT;
        //                    entity.TenNPL = info.TEN_NPL;
        //                    bk.bkNPLTXCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLTaiXuat nplTaiXuat = new Company.BLL.SXXK.ThanhKhoan.NPLTaiXuat();
        //                        nplTaiXuat.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplTaiXuat.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplTaiXuat.MaHaiQuan = entity.MaHaiQuan;
        //                        nplTaiXuat.MaHaiQuanXuat = entity.MaHaiQuanXuat;
        //                        nplTaiXuat.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplTaiXuat.MaLoaiHinhXuat = entity.MaLoaiHinhXuat;
        //                        nplTaiXuat.MaNPL = entity.MaNPL;
        //                        nplTaiXuat.NamDangKy = entity.NamDangKy;
        //                        nplTaiXuat.NamDangKyXuat = entity.NamDangKyXuat;
        //                        nplTaiXuat.SoLuongTaiXuat = entity.LuongTaiXuat;
        //                        nplTaiXuat.SoToKhai = entity.SoToKhai;
        //                        nplTaiXuat.SoToKhaiXuat = entity.SoToKhaiXuat;
        //                        nplTaiXuat.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion

        //            #region  Bảng kê NPL tạm nộp thuế
        //            if (hstl.BKTamNopThue != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLCHITIETNT";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "Bảng kê NPL tạm nộp thuế ";
        //                bk.TrangThaiXL = hstl.BKTamNopThue.TrangThaiXL;
        //                bk.bkNPLTNTCollection = new BKNPLTamNopThueCollection();
        //                foreach (BKTamNopThueInfo info in hstl.BKTamNopThue.NPLCollection)
        //                {
        //                    BKNPLTamNopThue entity = new BKNPLTamNopThue();
        //                    entity.MaHaiQuan = info.MaHaiQuanTK;
        //                    entity.MaLoaiHinh = info.MaloaiHinh;
        //                    entity.MaNPL = info.MaNPL;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NgayNopThue = info.NGAY_NT;
        //                    entity.Nop_CLGia = info.NOP_CLGIA;
        //                    entity.Nop_NK = info.NOP_NK;
        //                    entity.Nop_TTDB = info.NOP_TTDB;
        //                    entity.Nop_VAT = info.NOP_VAT;
        //                    entity.So_TBT_QDDC = info.SO_TBT_QDDC;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenNPL = info.TenNPL;
        //                    bk.bkNPLTNTCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLTamNopThue nplTamNopThue = new Company.BLL.SXXK.ThanhKhoan.NPLTamNopThue();
        //                        nplTamNopThue.NopCLGia = entity.Nop_CLGia;
        //                        nplTamNopThue.NopNK = entity.Nop_NK;
        //                        nplTamNopThue.NopTTDB = entity.Nop_TTDB;
        //                        nplTamNopThue.NopVAT = entity.Nop_VAT;
        //                        nplTamNopThue.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplTamNopThue.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplTamNopThue.MaHaiQuan = entity.MaHaiQuan;
        //                        nplTamNopThue.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplTamNopThue.MaNPL = entity.MaNPL;
        //                        nplTamNopThue.NamDangKy = entity.NamDangKy;
        //                        nplTamNopThue.NgayNopThue = entity.NgayNopThue;
        //                        nplTamNopThue.SoToKhai = entity.SoToKhai;
        //                        nplTamNopThue.InsertUpdateTransaction(transaction);
        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion



        //            #region  Bảng kê tờ khai xuất gia công
        //            if (hstl.BKXuatGiaCong != null)
        //            {
        //                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
        //                bk.MaBangKe = "DTLNPLXGC";
        //                bk.MaterID = this.ID;
        //                bk.SoHSTL = hstl.SoHoSoThanhLy;
        //                bk.TenBangKe = "  Bảng kê NPL xuất theo loại hình XGC";
        //                bk.TrangThaiXL = hstl.BKXuatGiaCong.TrangThaiXL;
        //                bk.bkNPLXGCCollection = new BKNPLXuatGiaCongCollection();
        //                foreach (BKNPLXuatGiaCongInfo info in hstl.BKXuatGiaCong.NPLCollection)
        //                {
        //                    BKNPLXuatGiaCong entity = new BKNPLXuatGiaCong();
        //                    entity.LuongXuat = info.LUONG_XGC;
        //                    entity.DVT_ID = info.MA_DVT;
        //                    entity.MaHaiQuan = info.Ma_HQ;
        //                    entity.MaHaiQuanXuat = info.MaHaiQuanXuat;
        //                    entity.MaLoaiHinh = info.MaLoaiHinh;
        //                    entity.MaLoaiHinhXuat = info.MaLoaiHinhXuat;
        //                    entity.MaNPL = info.MaNPL;
        //                    entity.NamDangKy = info.NamDangKy;
        //                    entity.NamDangKyXuat = info.NamDangKyXuat;
        //                    entity.NgayDangKy = info.NgayDangKy;
        //                    entity.NgayDangKyXuat = info.NgayDangKyXuat;
        //                    entity.SoToKhai = info.SoToKhai;
        //                    entity.SoToKhaiXuat = info.SoToKhaiXuat;
        //                    entity.STTHang = info.STTHang;
        //                    entity.TenDVT = info.Ten_DVT;
        //                    entity.TenNPL = info.TenNPL;
        //                    bk.bkNPLXGCCollection.Add(entity);
        //                    if (TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                    {
        //                        BLL.SXXK.ThanhKhoan.NPLXGC nplXGC = new Company.BLL.SXXK.ThanhKhoan.NPLXGC();
        //                        nplXGC.LanThanhLy = lanthanhly.LanThanhLy;
        //                        nplXGC.MaDoanhNghiep = lanthanhly.MaDoanhNghiep;
        //                        nplXGC.MaHaiQuan = entity.MaHaiQuan;
        //                        nplXGC.MaHaiQuanXuat = entity.MaHaiQuanXuat;
        //                        nplXGC.MaLoaiHinh = entity.MaLoaiHinh;
        //                        nplXGC.MaLoaiHinhXuat = entity.MaLoaiHinhXuat;
        //                        nplXGC.MaNPL = entity.MaNPL;
        //                        nplXGC.NamDangKy = entity.NamDangKy;
        //                        nplXGC.NamDangKyXuat = entity.NamDangKyXuat;
        //                        nplXGC.SoLuongXuat = entity.LuongXuat;
        //                        nplXGC.SoToKhai = entity.SoToKhai;
        //                        nplXGC.SoToKhaiXuat = entity.SoToKhaiXuat;
        //                        nplXGC.InsertUpdateTransaction(transaction);

        //                    }
        //                }
        //                this.BKCollection.Add(bk);
        //            }
        //            #endregion
        //            this.InsertUpdateFull(transaction);
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw new Exception(ex.Message);
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
        //#endregion WS.Softech

        //#region WS.FPT

        //#region Gửi hồ sơ thanh lý
        //public void GetSoHoSoThanhLy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\DANG_KY_TL_HO_SO.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        this.SoTiepNhan = Convert.ToInt64(dataGet.Attributes["SO_HS"].Value);
        //        this.NgayTiepNhan = DateTime.Today;
        //        this.NamTiepNhan = (short)this.NgayTiepNhan.Year;
        //        this.TrangThaiXuLy = 0;
        //        this.Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    LoadBKCollection();
        //    WSSendBKToKhaiNhap(pass);
        //    WSSendBKToKhaiXuat(pass);
        //    if (this.getBKNPLChuaThanhLY() > 0)
        //        WSSendBKNPLChuaThanhLy(pass);
        //    if (this.getBKNPLNopThue() > 0)
        //        WSSendBKNPLNopThue(pass);
        //    if (this.getBKNPLXinHuy() > 0)
        //        WSSendBKNPLHuy(pass);
        //    if (this.getBKNPLTamNopThue() > 0)
        //        WSSendBKNPLTamNopThue(pass);
        //    if (this.getBKNPLNhapKinhDoanh() > 0)
        //        WSSendBKNPLNhapKinhDoanh(pass);
        //    if (this.getBKNPLXuatGiaCong() > 0)
        //        WSSendBKNPLXuatGiaCong(pass);
        //    if (this.getBKNPLTaiXuat() > 0)
        //        WSSendBKNPLTaiXuat(pass);
        //}
        //public void WSSendBKToKhaiNhap(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKN_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKN");
        //    this.BKCollection[this.getBKToKhaiNhap()].LoadChiTietBangKe();
        //    foreach (BKToKhaiNhap tkn in this.BKCollection[this.getBKToKhaiNhap()].bkTKNCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TO_KHAI_NK");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = tkn.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = tkn.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = tkn.NamDangKy.ToString();

        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        dsTK.AppendChild(tk);
        //    }

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = 0;
        //        BKCollection[this.getBKToKhaiNhap()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKToKhaiXuat(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKX_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKX");
        //    this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
        //    foreach (BKToKhaiXuat tkn in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
        //    {
        //        XmlElement tk = doc.CreateElement("TO_KHAI_XK");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = tkn.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = tkn.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = tkn.NamDangKy.ToString();

        //        XmlAttribute mahaiquan = doc.CreateAttribute("MA_HQ");
        //        mahaiquan.Value = tkn.MaHaiQuan;

        //        XmlAttribute ngayTX = doc.CreateAttribute("NGAY_THX");
        //        ngayTX.Value = tkn.NgayThucXuat.ToString("yyyy-MM-dd");

        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        tk.Attributes.Append(mahaiquan);
        //        tk.Attributes.Append(ngayTX);
        //        dsTK.AppendChild(tk);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = 0;
        //        BKCollection[this.getBKToKhaiXuat()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKNPLChuaThanhLy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_CTL_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_CHUA_TL");
        //    this.BKCollection[this.getBKNPLChuaThanhLY()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLChuaThanhLy bkCTL in this.BKCollection[this.getBKNPLChuaThanhLY()].bkNPLCTLCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_TL");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_TL.HANG");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_CTL");
        //        LuongCTL.Value = bkCTL.Luong.ToString(f);
        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(LuongCTL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_TL"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLChuaThanhLY()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKNPLNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NT_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NOP_THUE");
        //    this.BKCollection[this.getBKNPLNopThue()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLNopThueTieuThuNoiDia bkCTL in this.BKCollection[this.getBKNPLNopThue()].bkNPLNTCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_NT");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_NT.NPL_NT");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_NT");
        //        LuongCTL.Value = bkCTL.LuongNopThue.ToString(f);
        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(LuongCTL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_NT"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLNopThue()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKNPLHuy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_XH_DK.xml");

        //    //luu thong tin gui
        //    #region Thong tin chung
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();
        //    #endregion Thong Tin chung

        //    #region Load NPL
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_HUY");
        //    this.BKCollection[this.getBKNPLXinHuy()].LoadChiTietBangKe();
        //    foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_H");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_H.NPL_H");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_Hang.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_H"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    #endregion Load NPL

        //    #region Load Thong tin huy

        //    foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TT_HUY");

        //        XmlAttribute bbHuy = doc.CreateAttribute("BB_HUY");
        //        bbHuy.Value = bkCTL.BienBanHuy.ToString();

        //        XmlAttribute NgayHuy = doc.CreateAttribute("NGAY_HUY");
        //        NgayHuy.Value = bkCTL.NgayHuy.ToString("yyyy-MM-dd");

        //        XmlAttribute LUONG_HUY = doc.CreateAttribute("LUONG_HUY");
        //        LUONG_HUY.Value = bkCTL.LuongHuy.ToString(f);
        //        tk.Attributes.Append(bbHuy);
        //        tk.Attributes.Append(NgayHuy);
        //        tk.Attributes.Append(LUONG_HUY);

        //        //lay tat ca cac nut TKN_H.NPL_H cua dsTK kiem tra

        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_H.NPL_H"))
        //        {
        //            if (node.Attributes["MA_NPL"].Value == bkCTL.MaNPL)
        //            {
        //                node.AppendChild(tk);
        //                break;
        //            }
        //        }
        //    }

        //    #endregion Load Thong tin huy
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLXinHuy()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKNPLTamNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_CHI_TIET_NT_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/CHI_TIET_NT");
        //    this.BKCollection[this.getBKNPLTamNopThue()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLTamNopThue bkCTL in this.BKCollection[this.getBKNPLTamNopThue()].bkNPLTNTCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        XmlAttribute mahq = doc.CreateAttribute("MA_HQ");
        //        mahq.Value = bkCTL.MaHaiQuan;
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        tk.Attributes.Append(mahq);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN.NPL");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute NOP_NK = doc.CreateAttribute("NOP_NK");
        //        NOP_NK.Value = bkCTL.Nop_NK.ToString(f);

        //        XmlAttribute NOP_VAT = doc.CreateAttribute("NOP_VAT");
        //        NOP_VAT.Value = bkCTL.Nop_VAT.ToString(f);

        //        XmlAttribute NOP_TTDB = doc.CreateAttribute("NOP_TTDB");
        //        NOP_TTDB.Value = bkCTL.Nop_TTDB.ToString(f);

        //        XmlAttribute NOP_CLGIA = doc.CreateAttribute("NOP_CLGIA");
        //        NOP_CLGIA.Value = bkCTL.Nop_CLGia.ToString(f);

        //        XmlAttribute ngayNT = doc.CreateAttribute("NGAY_NT");
        //        ngayNT.Value = bkCTL.NgayNopThue.ToString("yyyy-MM-dd");

        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(NOP_NK);
        //        tk_Hang.Attributes.Append(NOP_VAT);
        //        tk_Hang.Attributes.Append(NOP_TTDB);
        //        tk_Hang.Attributes.Append(NOP_CLGIA);
        //        tk_Hang.Attributes.Append(ngayNT);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLTamNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSSendBKNPLNhapKinhDoanh(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NKD_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NKD");
        //    this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLXuatSuDungNKD bkCTL in this.BKCollection[this.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKNKD_TL");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKNKD_TL.HANG");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKNKD_TL"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);

        //        //thong tin hang xuat
        //        XmlElement tkHangXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

        //        XmlAttribute masp = doc.CreateAttribute("MA_SP");
        //        masp.Value = bkCTL.MaSP.ToString();

        //        XmlAttribute luong = doc.CreateAttribute("LUONG_SD");
        //        luong.Value = bkCTL.LuongSuDung.ToString(f);

        //        tkHangXuat.Attributes.Append(masp);
        //        tkHangXuat.Attributes.Append(luong);
        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);
        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tkXuat.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_SP"].Value == tkHangXuat.Attributes["MA_SP"].Value)
        //            {
        //                ok = false;
        //                tkHangXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tkXuat.AppendChild(tkHangXuat);


        //    #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLNhapKinhDoanh()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLNhapKinhDoanh()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}

        //public void WSSendBKNPLXuatGiaCong(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NKD_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_XGC");
        //    this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLXuatGiaCong bkCTL in this.BKCollection[this.getBKNPLXuatGiaCong()].bkNPLXGCCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKN_XGC");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKN_XGC.NPL_XGC");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_XGC"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKN_XGC.NPL_XGC.TK_XGC");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();


        //        XmlAttribute luong = doc.CreateAttribute("LUONG_XGC");
        //        luong.Value = bkCTL.LuongXuat.ToString(f);

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);
        //        tkXuat.Attributes.Append(luong);

        //        //thong tin hang xuat

        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);


        //        #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLXuatGiaCong()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}

        //public void WSSendBKNPLTaiXuat(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_TX_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_TAI_XUAT");
        //    this.BKCollection[this.getBKNPLTaiXuat()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLTaiXuat bkCTL in this.BKCollection[this.getBKNPLTaiXuat()].bkNPLTXCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKN_TX");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKN_TX.NPL_TX");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_TX"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKN_TX.NPL_TX.TK_TX");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();


        //        XmlAttribute luong = doc.CreateAttribute("LUONG_TX");
        //        luong.Value = bkCTL.LuongTaiXuat.ToString(f);

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);
        //        tkXuat.Attributes.Append(luong);

        //        //thong tin hang xuat

        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);


        //        #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLXuatGiaCong()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}


        //#endregion

        //#region  Nhận thông tin duyệt hồ sơ thanh lý

        ////Chưa có

        //#endregion  Nhận thông tin duyệt hồ sơ thanh lý


        //#region  Hủy thông tin duyệt hồ sơ thanh lý

        //public void HuyKhaiBaoHSTL(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\HUY_DANG_KY_TL_HO_SO.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
        //        this.Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachTKN(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKN_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = -1;
        //        BKCollection[this.getBKToKhaiNhap()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachTKX(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKX_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = -1;
        //        BKCollection[this.getBKToKhaiXuat()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLChuaThanhLy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_CTL_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLChuaThanhLY()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLNKD(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NKD_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLChuaThanhLY()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NT_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLNopThue()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLTamNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_CHI_TIET_NT_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLTamNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLTaiXuat(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_TX_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLTaiXuat()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLTaiXuat()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLXuatGiaCong(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_XGC_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLXuatGiaCong()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //public void HuyKhaiBaoDanhSachNPLXinHuy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_XH_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = -1;
        //        BKCollection[this.getBKNPLXinHuy()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}

        //#endregion  Hủy thông tin duyệt hồ sơ thanh lý

        //#region Cập nhật bảng kê hồ sơ thanh lý

        //public void WSUpdateBKToKhaiNhap(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKN_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKN");
        //    this.BKCollection[this.getBKToKhaiNhap()].LoadChiTietBangKe();
        //    foreach (BKToKhaiNhap tkn in this.BKCollection[this.getBKToKhaiNhap()].bkTKNCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TO_KHAI_NK");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = tkn.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = tkn.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = tkn.NamDangKy.ToString();

        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        dsTK.AppendChild(tk);
        //    }

        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiNhap()].TrangThaiXL = 0;
        //        BKCollection[this.getBKToKhaiNhap()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKToKhaiXuat(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_DSTKX_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/DSTKX");
        //    this.BKCollection[this.getBKToKhaiXuat()].LoadChiTietBangKe();
        //    foreach (BKToKhaiXuat tkn in this.BKCollection[this.getBKToKhaiXuat()].bkTKXColletion)
        //    {
        //        XmlElement tk = doc.CreateElement("TO_KHAI_XK");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = tkn.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = tkn.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = tkn.NamDangKy.ToString();

        //        XmlAttribute mahaiquan = doc.CreateAttribute("MA_HQ");
        //        mahaiquan.Value = tkn.MaHaiQuan;

        //        XmlAttribute ngayTX = doc.CreateAttribute("NGAY_THX");
        //        ngayTX.Value = tkn.NgayThucXuat.ToString("yyyy-MM-dd");

        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        tk.Attributes.Append(mahaiquan);
        //        tk.Attributes.Append(ngayTX);
        //        dsTK.AppendChild(tk);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKToKhaiXuat()].TrangThaiXL = 0;
        //        BKCollection[this.getBKToKhaiXuat()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKNPLChuaThanhLy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_CTL_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_CHUA_TL");
        //    this.BKCollection[this.getBKNPLChuaThanhLY()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLChuaThanhLy bkCTL in this.BKCollection[this.getBKNPLChuaThanhLY()].bkNPLCTLCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_TL");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_TL.HANG");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_CTL");
        //        LuongCTL.Value = bkCTL.Luong.ToString(f);
        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(LuongCTL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_TL"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLChuaThanhLY()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLChuaThanhLY()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKNPLNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NT_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NOP_THUE");
        //    this.BKCollection[this.getBKNPLNopThue()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLNopThueTieuThuNoiDia bkCTL in this.BKCollection[this.getBKNPLNopThue()].bkNPLNTCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_NT");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_NT.NPL_NT");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute LuongCTL = doc.CreateAttribute("LUONG_NT");
        //        LuongCTL.Value = bkCTL.LuongNopThue.ToString(f);
        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(LuongCTL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_NT"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLNopThue()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKNPLHuy(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_XH_CAPNHAT.xml");

        //    //luu thong tin gui
        //    #region Thong tin chung
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();
        //    #endregion Thong Tin chung

        //    #region Load NPL
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_HUY");
        //    this.BKCollection[this.getBKNPLXinHuy()].LoadChiTietBangKe();
        //    foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN_H");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN_H.NPL_H");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_Hang.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_H"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    #endregion Load NPL

        //    #region Load Thong tin huy

        //    foreach (BKNPLXinHuy bkCTL in this.BKCollection[this.getBKNPLXinHuy()].bkNPLXHCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TT_HUY");

        //        XmlAttribute bbHuy = doc.CreateAttribute("BB_HUY");
        //        bbHuy.Value = bkCTL.BienBanHuy.ToString();

        //        XmlAttribute NgayHuy = doc.CreateAttribute("NGAY_HUY");
        //        NgayHuy.Value = bkCTL.NgayHuy.ToString("yyyy-MM-dd");

        //        XmlAttribute LUONG_HUY = doc.CreateAttribute("LUONG_HUY");
        //        LUONG_HUY.Value = bkCTL.LuongHuy.ToString(f);
        //        tk.Attributes.Append(bbHuy);
        //        tk.Attributes.Append(NgayHuy);
        //        tk.Attributes.Append(LUONG_HUY);

        //        //lay tat ca cac nut TKN_H.NPL_H cua dsTK kiem tra

        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_H.NPL_H"))
        //        {
        //            if (node.Attributes["MA_NPL"].Value == bkCTL.MaNPL)
        //            {
        //                node.AppendChild(tk);
        //                break;
        //            }
        //        }
        //    }

        //    #endregion Load Thong tin huy
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLXinHuy()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLXinHuy()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKNPLTamNopThue(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_CHI_TIET_NT_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/CHI_TIET_NT");
        //    this.BKCollection[this.getBKNPLTamNopThue()].LoadChiTietBangKe();
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    foreach (BKNPLTamNopThue bkCTL in this.BKCollection[this.getBKNPLTamNopThue()].bkNPLTNTCollection)
        //    {
        //        XmlElement tk = doc.CreateElement("TKN");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        XmlAttribute mahq = doc.CreateAttribute("MA_HQ");
        //        mahq.Value = bkCTL.MaHaiQuan;
        //        tk.Attributes.Append(soTK);
        //        tk.Attributes.Append(loaiHinh);
        //        tk.Attributes.Append(namDK);
        //        tk.Attributes.Append(mahq);
        //        //tao 
        //        XmlElement tk_Hang = doc.CreateElement("TKN.NPL");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        XmlAttribute NOP_NK = doc.CreateAttribute("NOP_NK");
        //        NOP_NK.Value = bkCTL.Nop_NK.ToString(f);

        //        XmlAttribute NOP_VAT = doc.CreateAttribute("NOP_VAT");
        //        NOP_VAT.Value = bkCTL.Nop_VAT.ToString(f);

        //        XmlAttribute NOP_TTDB = doc.CreateAttribute("NOP_TTDB");
        //        NOP_TTDB.Value = bkCTL.Nop_TTDB.ToString(f);

        //        XmlAttribute NOP_CLGIA = doc.CreateAttribute("NOP_CLGIA");
        //        NOP_CLGIA.Value = bkCTL.Nop_CLGia.ToString(f);

        //        XmlAttribute ngayNT = doc.CreateAttribute("NGAY_NT");
        //        ngayNT.Value = bkCTL.NgayNopThue.ToString("yyyy-MM-dd");

        //        tk_Hang.Attributes.Append(maNPL);
        //        tk_Hang.Attributes.Append(NOP_NK);
        //        tk_Hang.Attributes.Append(NOP_VAT);
        //        tk_Hang.Attributes.Append(NOP_TTDB);
        //        tk_Hang.Attributes.Append(NOP_CLGIA);
        //        tk_Hang.Attributes.Append(ngayNT);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tk.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tk.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tk.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            dsTK.AppendChild(tk);
        //        else
        //            tk = (XmlElement)nodeOLD;
        //        tk.AppendChild(tk_Hang);
        //    }
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);

        //    XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        BKCollection[this.getBKNPLTamNopThue()].TrangThaiXL = 0;
        //        BKCollection[this.getBKNPLTamNopThue()].Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //}
        //public void WSUpdateBKNPLNhapKinhDoanh(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_NKD_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_NKD");
        //    this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLXuatSuDungNKD bkCTL in this.BKCollection[this.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKNKD_TL");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKNKD_TL.HANG");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKNKD_TL"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);

        //        //thong tin hang xuat
        //        XmlElement tkHangXuat = doc.CreateElement("TKNKD_TL.TKX_SD");

        //        XmlAttribute masp = doc.CreateAttribute("MA_SP");
        //        masp.Value = bkCTL.MaSP.ToString();

        //        XmlAttribute luong = doc.CreateAttribute("LUONG_SD");
        //        luong.Value = bkCTL.LuongSuDung.ToString(f);

        //        tkHangXuat.Attributes.Append(masp);
        //        tkHangXuat.Attributes.Append(luong);
        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);
        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tkXuat.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_SP"].Value == tkHangXuat.Attributes["MA_SP"].Value)
        //            {
        //                ok = false;
        //                tkHangXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tkXuat.AppendChild(tkHangXuat);


        //        #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLNhapKinhDoanh()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLNhapKinhDoanh()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}

        //public void WSUpdateBKNPLXuatGiaCong(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_XGC_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_XGC");
        //    this.BKCollection[this.getBKNPLNhapKinhDoanh()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLXuatGiaCong bkCTL in this.BKCollection[this.getBKNPLXuatGiaCong()].bkNPLXGCCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKN_XGC");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKN_XGC.NPL_XGC");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_XGC"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKN_XGC.NPL_XGC.TK_XGC");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();


        //        XmlAttribute luong = doc.CreateAttribute("LUONG_XGC");
        //        luong.Value = bkCTL.LuongXuat.ToString(f);

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);
        //        tkXuat.Attributes.Append(luong);

        //        //thong tin hang xuat

        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);


        //        #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLXuatGiaCong()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}

        //public void WSUpdateBKNPLTaiXuat(string pass)
        //{
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_TL_NPL_TX_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode dulieu = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    dulieu.Attributes["SO_HS"].Value = this.SoTiepNhan.ToString();
        //    dulieu.Attributes["NAM_TL"].Value = this.NamTiepNhan.ToString();

        //    XmlNode dsTK = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL_TAI_XUAT");
        //    this.BKCollection[this.getBKNPLTaiXuat()].LoadChiTietBangKe();

        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    #region Tạo thông tin  về tờ khai nhập
        //    foreach (BKNPLTaiXuat bkCTL in this.BKCollection[this.getBKNPLTaiXuat()].bkNPLTXCollection)
        //    {
        //        XmlElement tkNKD = doc.CreateElement("TKN_TX");

        //        XmlAttribute loaiHinh = doc.CreateAttribute("MA_LH");
        //        loaiHinh.Value = bkCTL.MaLoaiHinh.ToString();

        //        XmlAttribute soTK = doc.CreateAttribute("SO_TK");
        //        soTK.Value = bkCTL.SoToKhai.ToString();

        //        XmlAttribute namDK = doc.CreateAttribute("NAM_DK");
        //        namDK.Value = bkCTL.NamDangKy.ToString();

        //        tkNKD.Attributes.Append(soTK);
        //        tkNKD.Attributes.Append(loaiHinh);
        //        tkNKD.Attributes.Append(namDK);

        //        //tao 
        //        XmlElement tk_HangNKD = doc.CreateElement("TKN_TX.NPL_TX");

        //        XmlAttribute maNPL = doc.CreateAttribute("MA_NPL");
        //        maNPL.Value = bkCTL.MaNPL.ToString();

        //        tk_HangNKD.Attributes.Append(maNPL);

        //        //lay tat ca cac nut TKN_TL cua dsTK kiem tra
        //        bool ok = true;
        //        XmlNode nodeOLD = null;
        //        foreach (XmlNode node in doc.GetElementsByTagName("TKN_TX"))
        //        {
        //            if (node.Attributes["MA_LH"].Value == tkNKD.Attributes["MA_LH"].Value && node.Attributes["SO_TK"].Value == tkNKD.Attributes["SO_TK"].Value && node.Attributes["NAM_DK"].Value == tkNKD.Attributes["NAM_DK"].Value)
        //            {
        //                ok = false;
        //                nodeOLD = node;
        //                break;
        //            }
        //        }
        //        if (ok)//neu to khai nay chua co trong danh sach thi dua vao dong thoi dua luong hang vao
        //        {
        //            dsTK.AppendChild(tkNKD);
        //            tkNKD.AppendChild(tk_HangNKD);
        //        }
        //        else
        //        {
        //            tkNKD = (XmlElement)nodeOLD;//neu to khai nay da co thi kiem tra hang nay da co chua
        //            ok = true;
        //            foreach (XmlNode node in tkNKD.ChildNodes)
        //            {
        //                if (node.Attributes["MA_NPL"].Value == tk_HangNKD.Attributes["MA_NPL"].Value)
        //                {
        //                    ok = false;
        //                    tk_HangNKD = (XmlElement)node;
        //                    break;
        //                }
        //            }
        //            if (ok)
        //                tkNKD.AppendChild(tk_HangNKD);
        //        }

        //    #endregion Tạo thông tin  về tờ khai nhập
        //        #region Tạo thông tin  về tờ khai xuất

        //        //thong tin to khai xuat
        //        XmlElement tkXuat = doc.CreateElement("TKN_TX.NPL_TX.TK_TX");

        //        XmlAttribute loaiHinhX = doc.CreateAttribute("MA_LH");
        //        loaiHinhX.Value = bkCTL.MaLoaiHinhXuat.ToString();

        //        XmlAttribute soTKX = doc.CreateAttribute("SO_TK");
        //        soTKX.Value = bkCTL.SoToKhaiXuat.ToString();

        //        XmlAttribute namDKX = doc.CreateAttribute("NAM_DK");
        //        namDKX.Value = bkCTL.NamDangKyXuat.ToString();

        //        XmlAttribute mahqX = doc.CreateAttribute("MA_HQ");
        //        mahqX.Value = bkCTL.MaLoaiHinhXuat.ToString();


        //        XmlAttribute luong = doc.CreateAttribute("LUONG_TX");
        //        luong.Value = bkCTL.LuongTaiXuat.ToString(f);

        //        tkXuat.Attributes.Append(soTKX);
        //        tkXuat.Attributes.Append(loaiHinhX);
        //        tkXuat.Attributes.Append(namDKX);
        //        tkXuat.Attributes.Append(mahqX);
        //        tkXuat.Attributes.Append(luong);

        //        //thong tin hang xuat

        //        ok = true;
        //        foreach (XmlNode nodeConNPL in tk_HangNKD.ChildNodes)
        //        {
        //            if (nodeConNPL.Attributes["MA_LH"].Value == tkXuat.Attributes["MA_LH"].Value && nodeConNPL.Attributes["SO_TK"].Value == tkXuat.Attributes["SO_TK"].Value && nodeConNPL.Attributes["NAM_DK"].Value == tkXuat.Attributes["NAM_DK"].Value
        //                && nodeConNPL.Attributes["MA_HQ"].Value == tkXuat.Attributes["MA_HQ"].Value)
        //            {
        //                ok = false;
        //                tkXuat = (XmlElement)nodeConNPL;
        //                break;
        //            }
        //        }
        //        if (ok)
        //            tk_HangNKD.AppendChild(tkXuat);


        //        #endregion Tạo thông tin  về tờ khai xuất


        //        string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //        XmlDocument docResult = new XmlDocument();
        //        docResult.LoadXml(kq);

        //        XmlNode dataGet = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //        string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //        if (stKQ == "THANH CONG")
        //        {
        //            BKCollection[this.getBKNPLXuatGiaCong()].TrangThaiXL = 0;
        //            BKCollection[this.getBKNPLXuatGiaCong()].Update();
        //        }
        //        else
        //        {
        //            XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //            XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //            string errorSt = "";
        //            if (stMucLoi == "XML_LEVEL")
        //                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //            else if (stMucLoi == "DATA_LEVEL")
        //                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //            else if (stMucLoi == "SERVICE_LEVEL")
        //                errorSt = "Lỗi do Web service trả về ";
        //            else if (stMucLoi == "DOTNET_LEVEL")
        //                errorSt = "Lỗi do hệ thống của hải quan ";
        //            throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //        }
        //    }
        //}

        //#endregion Cập nhật bảng kê hồ sơ thanh lý
        //#endregion WS

        public string getSoHoSo(long hsID)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT max(bkx.NgayDangKy) FROM t_KDT_SXXK_BKToKhaiXuat bkx inner join t_KDT_SXXK_BangKeHoSoThanhLy as bkhs";
            sql += " on bkx.BangKeHoSoThanhLy_ID = bkhs.ID inner join t_kdt_sxxk_hosothanhlydangky as hsdk";
            sql += " on bkhs.materID = hsdk.ID";
            sql += " where hsdk.id=" + hsID;
            DbCommand cmd = db.GetSqlStringCommand(sql);
            DateTime ngayDKMax = (DateTime)db.ExecuteDataSet(cmd).Tables[0].Rows[0].ItemArray[0];
            sql = "";
            sql += "SELECT min(bkx.NgayDangKy) FROM t_KDT_SXXK_BKToKhaiXuat bkx inner join t_KDT_SXXK_BangKeHoSoThanhLy as bkhs";
            sql += " on bkx.BangKeHoSoThanhLy_ID = bkhs.ID inner join t_kdt_sxxk_hosothanhlydangky as hsdk";
            sql += " on bkhs.materID = hsdk.ID";
            sql += " where hsdk.id=" + hsID;
            cmd = db.GetSqlStringCommand(sql);
            DateTime ngayDKMin = (DateTime)db.ExecuteDataSet(cmd).Tables[0].Rows[0].ItemArray[0];
            try
            {
                if (DateTime.Compare(ngayDKMin, new DateTime(ngayDKMin.Year, 01, 01)) >= 0
                    && DateTime.Compare(ngayDKMax, new DateTime(ngayDKMax.Year, 03, 31)) <= 0)
                {
                    return "I, năm " + ngayDKMin.Year;
                }
                else if (DateTime.Compare(ngayDKMin, new DateTime(ngayDKMin.Year, 04, 01)) >= 0
                    && DateTime.Compare(ngayDKMax, new DateTime(ngayDKMax.Year, 06, 30)) <= 0)
                {
                    return "II, năm " + ngayDKMin.Year;
                }
                else if (DateTime.Compare(ngayDKMin, new DateTime(ngayDKMin.Year, 07, 01)) >= 0
                    && DateTime.Compare(ngayDKMax, new DateTime(ngayDKMax.Year, 09, 30)) <= 0)
                {
                    return "III, năm " + ngayDKMin.Year;
                }
                else if (DateTime.Compare(ngayDKMin, new DateTime(ngayDKMin.Year, 10, 01)) >= 0
                    && DateTime.Compare(ngayDKMax, new DateTime(ngayDKMax.Year, 12, 31)) <= 0)
                {
                    return "IV, năm " + ngayDKMin.Year;
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
