﻿using System;
using System.Windows.Forms;
using Company.KD.BLL.SXXK.ToKhai;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KD.BLL.KDT;
using Company.KD.BLL;
namespace Company.Interface
{
    public partial class HangTKTGForm : BaseForm
    {
        public ToKhaiTriGia TKTG;
        public HangTriGia HangTG = new HangTriGia();
        public decimal TyGiaTT;
        public Company.KD.BLL.KDT.ToKhaiMauDich TKMD;
        public HangTKTGForm()
        {
            InitializeComponent();
        }

        private void HangTKTGForm_Load(object sender, EventArgs e)
        {
            txtbanQuyen.Text = HangTG.BanQuyen.ToString();
            txtChiPhiBaoBi.Text = HangTG.ChiPhiBaoBi.ToString();
            txtChiPhiDongGoi.Text = HangTG.ChiPhiDongGoi.ToString();
            txtChiPhiPhatSinh.Text = HangTG.ChiPhiPhatSinh.ToString();
            txtChiPhiVanChuyen.Text = HangTG.ChiPhiVanChuyen.ToString();
            txtCongCu.Text = HangTG.CongCu.ToString();
            txtGiaGhiTrenHD.Text = HangTG.GiaTrenHoaDon.ToString();
            txtGiamGia.Text = HangTG.GiamGia.ToString();
            txtHoaHong.Text = HangTG.HoaHong.ToString();
            txtKhoanTTGianTiep.Text = HangTG.KhoanThanhToanGianTiep.ToString();
            txtNguyenLieu.Text = HangTG.NguyenLieu.ToString();
            txtPhiBaoHiem.Text = HangTG.PhiBaoHiem.ToString();
            txtSTT.Text = HangTG.STTHang.ToString();
            txtTenHang.Text = HangTG.TenHang.ToString();
            txtThietKe.Text = HangTG.ThietKe.ToString();
            txtTienLai.Text = HangTG.TienLai.ToString();
            txtTienThue.Text = HangTG.TienThue.ToString();
            txtTienTraSuDung.Text = HangTG.TienTraSuDung.ToString();
            txtTraTruoc.Text = HangTG.TraTruoc.ToString();
            txtTroGiup.Text = HangTG.TroGiup.ToString();
            txtVatLieu.Text = HangTG.VatLieu.ToString();
            txtDonGiaNT.Text = HangTG.TriGiaNguyenTe.ToString();
            txtDonGiaTT.Text = HangTG.TriGiaVND.ToString();
            txtTenHang.Text = HangTG.TenHang;
            txtPhiNoiDia.Text = HangTG.ChiPhiNoiDia.ToString();
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
            {
                btnGhi.Visible = false;
            }
            else if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.Edit)
            {
                btnGhi.Visible = true;
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            HangTG.BanQuyen = Convert.ToDouble(txtbanQuyen.Text);
            HangTG.ChiPhiBaoBi = Convert.ToDouble(txtChiPhiBaoBi.Text);
            HangTG.ChiPhiDongGoi = Convert.ToDouble(txtChiPhiDongGoi.Text);
            HangTG.ChiPhiPhatSinh = Convert.ToDouble(txtChiPhiPhatSinh.Text);
            HangTG.ChiPhiVanChuyen = Convert.ToDouble(txtChiPhiVanChuyen.Text);
            HangTG.CongCu = Convert.ToDouble(txtCongCu.Text);
            HangTG.CuocPhiBaoHiem = Convert.ToDouble(txtPhiBaoHiem.Text);
            HangTG.GiamGia = Convert.ToDouble(txtGiamGia.Text);
            HangTG.GiaTrenHoaDon = Convert.ToDouble(txtGiaGhiTrenHD.Text);
            HangTG.HoaHong = Convert.ToDouble(txtHoaHong.Text);
            HangTG.KhoanThanhToanGianTiep = Convert.ToDouble(txtKhoanTTGianTiep.Text);
            HangTG.NguyenLieu = Convert.ToDouble(txtNguyenLieu.Text);
            HangTG.PhiBaoHiem = Convert.ToDouble(txtPhiBaoHiem.Text);
            HangTG.STTHang = Convert.ToInt32(txtSTT.Text);
            HangTG.ThietKe = Convert.ToDouble(txtThietKe.Text);
            HangTG.TienLai = Convert.ToDouble(txtTienLai.Text);
            HangTG.TienThue = Convert.ToDouble(txtTienThue.Text);
            HangTG.TienTraSuDung = Convert.ToDouble(txtTienTraSuDung.Text);
            HangTG.TKTG_ID = TKTG.ID;
            HangTG.TraTruoc = Convert.ToDouble(txtTraTruoc.Text);
            HangTG.TriGiaNguyenTe = Convert.ToDouble(txtDonGiaNT.Text);
            HangTG.TriGiaVND = Convert.ToDouble(txtDonGiaTT.Text);
            HangTG.TroGiup = Convert.ToDouble(txtTroGiup.Text);
            HangTG.VatLieu = Convert.ToDouble(txtVatLieu.Text);
            HangTG.TenHang = txtTenHang.Text;
            HangTG.ChiPhiNoiDia = Convert.ToDouble(txtPhiNoiDia.Text);
            if (HangTG.ID == 0)
                TKTG.HTGCollection.Add(HangTG);
            this.Close();
        }

        private void txtDonGiaNT_Leave(object sender, EventArgs e)
        {
        }

        private void txtGiaGhiTrenHD_Leave(object sender, EventArgs e)
        {
            double TriGia = Convert.ToDouble(txtGiaGhiTrenHD.Text) + Convert.ToDouble(txtKhoanTTGianTiep.Text) + Convert.ToDouble(txtTraTruoc.Text) +
                            Convert.ToDouble(txtHoaHong.Text) + Convert.ToDouble(txtChiPhiBaoBi.Text) + Convert.ToDouble(txtChiPhiDongGoi.Text) +
                            Convert.ToDouble(txtTroGiup.Text) + Convert.ToDouble(txtbanQuyen.Text) +
                            Convert.ToDouble(txtTienTraSuDung.Text) + Convert.ToDouble(txtChiPhiVanChuyen.Text) + Convert.ToDouble(txtPhiBaoHiem.Text)
                            - Convert.ToDouble(txtPhiNoiDia.Text) - Convert.ToDouble(txtChiPhiPhatSinh.Text) - Convert.ToDouble(txtTienLai.Text) -
                            Convert.ToDouble(txtTienThue.Text) - Convert.ToDouble(txtGiamGia.Text);
            txtDonGiaNT.Text = Convert.ToString(Math.Round(TriGia, 8));
            txtDonGiaTT.Text = Convert.ToString(Math.Round(TriGia * Convert.ToDouble(TyGiaTT), 8));
        }

        private void txtNguyenLieu_Leave(object sender, EventArgs e)
        {
            double TroGiup = Convert.ToDouble(txtNguyenLieu.Text) + Convert.ToDouble(txtVatLieu.Text) +
                            Convert.ToDouble(txtCongCu.Text) + Convert.ToDouble(txtThietKe.Text);
            txtTroGiup.Text = TroGiup.ToString();
            txtGiaGhiTrenHD_Leave(null, null);
        }
    }
}