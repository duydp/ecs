﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.ComponentModel;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.Components
{
    public static class Globals
    {
        public static string FILE_EXCEL_TOKHAI_SUADOI_BOSUNG = "Mau_so_06._To_khai_dieu_chinh_bo_sung.xls";
        public static DateTime TuNgay_HSTK;
        public static DateTime DenNgay_HSTK;
        public static bool InTriGiaTinhThue;
        public static bool IsKhaiVNACCS;
        public static DataSet GlobalDanhMucChuanHQ = new DataSet("DanhMucChuanHQ");

        /// <summary>
        /// Khoi tao cac danh muc chuan Hai quan luu tai local.
        /// </summary>
        public static void KhoiTao_DanhMucChuanHQ()
        {
            try
            {
                if (GlobalDanhMucChuanHQ == null)
                    GlobalDanhMucChuanHQ = new DataSet("DanhMucChuanHQ");

                GlobalDanhMucChuanHQ.Tables.Clear();

                DataTable dt = DuLieuChuan.CuaKhau.SelectAll().Tables[0];
                dt.TableName = "CuaKhau";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.DieuKienGiaoHang.SelectAll().Tables[0];
                dt.TableName = "DieuKienGiaoHang";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.DonViHaiQuan.SelectAll().Tables[0];
                dt.TableName = "DonViHaiQuan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.DonViTinh.SelectAll().Tables[0];
                dt.TableName = "DonViTinh";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiChungTu.SelectAll().Tables[0];
                dt.TableName = "LoaiChungTu";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.LoaiCO.SelectAll().Tables[0];
                dt.TableName = "LoaiCO";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiHinhMauDich.SelectAll();
                dt.TableName = "LoaiHinhMauDich";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.LoaiPhiChungTuThanhToan.SelectAll().Tables[0];
                dt.TableName = "LoaiPhiChungTuThanhToan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
#if GC_V3
                dt = DuLieuChuan.LoaiPhieuChuyenTiep.SelectAll();
                dt.TableName = "LoaiPhieuChuyenTiep";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());


                dt = DuLieuChuan.LoaiPhuKien.SelectAll();
                dt.TableName = "LoaiPhuKien";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.NhomSanPham.SelectAll().Tables[0];
                dt.TableName = "NhomSanPham";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
#endif

                dt = DuLieuChuan.MaHS.SelectAll();
                dt.TableName = "MaHS";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.NguyenTe.SelectAll().Tables[0];
                dt.TableName = "NguyenTe";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.NhomCuaKhau.SelectAll().Tables[0];
                dt.TableName = "NhomCuaKhau";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                //-------------------------------

                dt = DuLieuChuan.Nuoc.SelectAll().Tables[0];
                dt.TableName = "Nuoc";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
                dt.TableName = "PhuongThucThanhToan";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());

                dt = DuLieuChuan.PhuongThucVanTai.SelectAll().Tables[0];
                dt.TableName = "PhuongThucVanTai";
                GlobalDanhMucChuanHQ.Tables.Add(dt.Copy());
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static decimal tinhDinhMucChung(decimal dm, decimal tylehaohut)
        {
            decimal dmc = dm + (dm * tylehaohut / 100);
            return dmc;
        }
        public static string Translate(this string trangthaixuly)
        {
            return Translate(int.Parse(trangthaixuly));
        }
        public static string Translate(int trangthaixuly)
        {
            switch (trangthaixuly)
            {
                case -1: return "Chưa khai báo";
                case 0: return "Chờ duyệt";
                case 1: return "Đã duyệt";
                case 2: return "Không phê duyệt";
                case -2: return "Chờ duyệt đã sửa chữa";
                case 3: return "Đã phân luồng";
                case -3: return "Đã khai báo, chưa có phản hồi";
                case -4: return "Hủy khai báo";
                case 5: return "Sửa tờ khai";
                case -5: return "Hủy tờ khai";
                case 10: return "Đã hủy";
                case 11: return "Chờ hủy";
                default:
                    return string.Empty;
            }
        }

        #region Trim Mã hải quan
        public static string trimMaHaiQuan(string maHQ)
        {
            return (maHQ.Trim());
        }
        #endregion

        #region Tỷ Giá - Exchange Rate

        public static decimal GetNguyenTe(string maNguyenTe)
        {
            try
            {
                return Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.Load(maNguyenTe).TyGiaTinhThue;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return 0;
        }

        /// <summary>
        /// Lay ty gia ngoai te Online
        /// </summary>
        public static bool GetTyGia()
        {
            try
            {
                //Update by Hungtq
                string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDFxNLczdTEwMLx2BjA09_Z29LA3dvIxNfU6B8JE55_zBjYnQ7uzt6mJj7GBi4ewU6G3i6ewVbmDgZGhgYmBLQHQ5yLX79YBNwAEcDqDwOG_xN8cqDfYfPfgszfT-P_NxU_YLc0AiDTM8sE0dFRQBQ7HBF/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfMEQ0OTdGNTQwOEFTMzBJT0NLOTBHSzI0RTI!";
                HtmlAgilityPack.HtmlDocument htmlAgilityPackDoc = new HtmlAgilityPack.HtmlWeb().Load(path);

                List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateNew(htmlAgilityPackDoc);

                if (listNgoaiTe.Count == 0)
                {
                    return false;
                }

                //Luu file XML
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                doc.AppendChild(root);
                foreach (Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe item in listNgoaiTe)
                {
                    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                    itemMaNT.InnerText = item.Ten;

                    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                    itemTenNT.InnerText = item.Ten;

                    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                    itemTyNT.InnerText = item.TyGiaTinhThue.ToString();

                    itemNT.AppendChild(itemMaNT);
                    itemNT.AppendChild(itemTenNT);
                    itemNT.AppendChild(itemTyNT);
                    root.AppendChild(itemNT);
                }
                doc.Save(System.Windows.Forms.Application.StartupPath + "\\TyGia.xml");

                //Update Data
                Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.InsertUpdateCollection(listNgoaiTe);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

            return true;
        }

        public static List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> GetUSDExchangeRateNew(HtmlAgilityPack.HtmlDocument htmlDocument)
        {
            List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe> collection = new List<Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe>();

            try
            {
                //Lấy dữ liệu từ website: http://www.sbv.gov.vn/wps/portal/vn

                //string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/pVLLboMwEPwkFtsYc3RsMGCCgyGP5hIhtYoSNQ9VUdTm6-skVaXSBg71yqfZmZ0drbf0XO3b82bdnjaHffvqLbwlXYEkUZgEBBivMWRG6AiURiRG3txbAFnVW3Ycf5wWxUWcm-2lgrJp30s5d79C47KYPM_sdMT5di3xm9N8oqsEcYqSFIHJkYSMcMsRNbgm4Y-JJhKBm8iEAJtgFQV3tlA8JWEBoPJKQKbympGRDwDBgN8b-wFuZvgfbGKCwayW144-7zeFB4_DF_4gGz_o6JtURpCxXBeNP8EJwne8L_k-f4x2-KxhseOPwkqzsQ-TXzgBlwCJo8qoWW5D2u8vQ539VIJCt5-Nq5pLhKZBRx-AXP1TkFwnSFkysL_fi-cK-ufHA_551_8f19V3Ie5-yvSwe_GOu7PWuqA2_i72CXkZa0g!/dl3/d3/L0lDU0lKSmdwcGlRb0tVUWtnQSEhL29Pb2dBRUlRaGpFQ1VJZ0FBQUl5RkFNaHdVaFM0TFVFQVVvIS80QzFiOVdfTnIwZ0NVZ3hFbVJDVXdpSkg0QSEhLzdfRjJBNjJGSDIwME5SMjBJNDNOM1BTNzAwVTcvNFd0ME40NTcwMDIzMC8xNTM0OTU1NzkyMjUvYmZfYWN0aW9uL21haW4!";

                //HtmlDocument htmlDocument = new HtmlWeb().Load(path);

                System.Globalization.CultureInfo culVN = new System.Globalization.CultureInfo("vi-VN");

                #region Lay ty gia nguyen te USD
                HtmlNodeCollection nc = htmlDocument.DocumentNode.SelectNodes("//table[@class='sbvDataTable']");

                for (int i = 0; i < nc.Count; i++)
                {
                    //DateTime ngayApDung = Convert.ToDateTime(nc[i].SelectSingleNode("//span[@name='ngayapdung']").InnerText);
                    string giatriso = nc[i].SelectSingleNode("//span[@name='Giatriso']").InnerText;
                    decimal tyGiaUSD = Convert.ToDecimal(giatriso != "" ? giatriso : "0", culVN);

                    if (tyGiaUSD != 0)
                    {
                        Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe usd = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                        usd.ID = "USD";
                        usd.Ten = "Đô la Mỹ";
                        usd.TyGiaTinhThue = tyGiaUSD;
                        usd.DateCreated = usd.DateModified = DateTime.Now;

                        collection.Add(usd);

                        break;
                    }
                }
                #endregion

                #region Lay ty gia xuat khau nguyen te khac
                //Get cung tai vi tri 2 trong danh sach
                HtmlNodeCollection ncKhac = htmlDocument.DocumentNode.SelectNodes("//table[@align='center']|//table[@width='590']")[1].SelectNodes("//tr[@name='DataContainer']");

                for (int j = 0; j < ncKhac.Count; j++)
                {
                    //DateTime apDungTuNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='startDateD']").InnerText);
                    //DateTime apDungDenNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='endDateD']").InnerText);
                    //string maNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='NGOAITE']").InnerText;
                    //string tenNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='TENGOI']").InnerText;
                    //decimal tyGiaNgoaiTe = Convert.ToDecimal(ncKhac[j].SelectSingleNode("//span[@name='GIATRI']").InnerText);
                    string[] array = ncKhac[j].InnerText.Trim().Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

                    if (array.Length != 0 && array.Length == 4)
                    {
                        Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe nt = new Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe();
                        nt.ID = array[1];
                        nt.Ten = array[2];
                        nt.TyGiaTinhThue = Convert.ToDecimal(array[3], culVN);
                        nt.DateCreated = nt.DateModified = DateTime.Now;

                        collection.Add(nt);
                    }
                }
                #endregion
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

        #endregion

        #region SAVE NODE XML
        public static void SaveNodeXmlAppSettings(string key, object value, object setting)
        {
            SaveNodeXmlAppSettings(key, value);
            try
            {
                setting = value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }

        public static string ReadNodeXmlAppSettings(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }

        public static void UpdateNodeXml(System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                result = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            return result;
        }

        public static void SaveNodeXmlSetting(string name, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string s = "//setting[@name='" + name + "']";
                doc.SelectSingleNode(s).SelectSingleNode("value").ChildNodes[0].Value = value.ToString();
                doc.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Error at key: " + name, ex); }
        }

        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[0].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void ReadNodeXmlConnectionStrings()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                string[] arr = connectionString.Split(new char[] { ';' });

                sConnectionString.Server = arr[0].Split(new char[] { '=' })[1];
                sConnectionString.Database = arr[1].Split(new char[] { '=' })[1];
                sConnectionString.User = arr[2].Split(new char[] { '=' })[1];
                sConnectionString.Pass = arr[3].Split(new char[] { '=' })[1];
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlConnectionStrings2()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                return connectionString;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }
        #endregion

        public static XmlNode GetNodeConfigDN(XmlDocument doc, string key)
        {
            try
            {
                foreach (XmlNode node in doc.GetElementsByTagName("Config"))
                {
                    if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                    {
                        return node["Value"];
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            XmlDocument docx = new XmlDocument();
            return docx.CreateNode(XmlNodeType.Element, key, "");
        }
        public static string GetConfig(XmlDocument doc, string key, string defaultValue)
        {
            XmlNode node = GetNodeConfigDN(doc, key);
            if (node == null) return defaultValue;
            else if (string.IsNullOrEmpty(node.InnerText)) return defaultValue;
            else
                return node.InnerText;
        }
        static string GetConfig(string key)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                return config.AppSettings.Settings[key].Value.ToString();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            return "";
        }
        public static bool IsSignOnLan { get; set; }
        public static string DataSignLan { get; set; }

        public static int TriGiaNT = 4;
        public static int DonGiaNT = 6;

        #region Cấu hình chữ ký số V3 LANNT
        static bool _isRemmberLogin = false;
        public static bool IsRememberLogin
        {
            get
            {
                return _isRemmberLogin;
            }
            set
            {
                _isRemmberLogin = value;
            }
        }
        static string _userNameLogin = string.Empty;
        public static string UserNameLogin
        {
            get
            {
                return _userNameLogin;
            }
            set
            {
                _userNameLogin = value;
            }
        }
        static string _passwordLogin = string.Empty;
        public static string PasswordLogin
        {
            get
            {
                return _passwordLogin;
            }
            set
            {
                _passwordLogin = value;
            }
        }

        public static bool IsRememberSignOnline;
        static string _appSend = string.Empty;
        public static string AppSend
        {
            get
            {
                return _appSend;
            }
        }
        static int _countSend = 2;
        public static int CountSend
        {
            get { return _countSend; }
            set { _countSend = value; }
        }

        static string _GetX509CertificatedName = string.Empty;
        public static string GetX509CertificatedName
        {
            get
            {
                return _GetX509CertificatedName;
            }
            set
            {
                _GetX509CertificatedName = value;
            }
        }
        static bool _isSignature = false;
        public static bool IsSignature
        {
            get
            {
                return _isSignature;
            }
            set
            {
                _isSignature = value;
            }

        }
        static string _passwordSign = string.Empty;
        public static string PasswordSign
        {
            get
            {
                return _passwordSign;
            }
            set
            {
                _passwordSign = value;
            }
        }
        public static bool IsDaiLy { get; set; }

        static bool _isRememberSign = false;
        public static bool IsRememberSign { get { return _isRememberSign; } set { _isRememberSign = value; } }
        static string _versionSend = string.Empty;
        static bool _suDungHttps = false;
        public static bool SuDungHttps
        {
            get
            {
                return _suDungHttps;
            }
            set
            {
                _suDungHttps = value;
            }

        }

        static bool _isSignRemote = false;
        public static bool IsSignRemote
        {
            get
            {
                return _isSignRemote;
            }
            set
            {
                _isSignRemote = value;
            }
        }

        static string _userNameSignRemote;
        public static string UserNameSignRemote
        {
            get
            {
                return _userNameSignRemote;
            }
            set
            {
                _userNameSignRemote = value;
            }
        }
        static string _passwordSignRemote;
        public static string PasswordSignRemote
        {
            get
            {
                return _passwordSignRemote;
            }
            set
            {
                _passwordSignRemote = value;
            }
        }
        static string _fontName = "Unicode";
        public static string FontName
        {
            get
            {
                return _fontName;
            }
            set
            {
                _fontName = value;
            }
        }
        /// <summary>
        /// Mặc định khai báo V3 là 3.00 tưng ứng với các version còn lại.
        /// </summary>
        public static string VersionSend
        {
            get
            {
                return _versionSend;
            }
            set
            {
                _versionSend = value;
            }
        }

        static int _timeConnect = 2;
        public static int TimeConnect
        {
            get
            {
                return _timeConnect;
            }
            set
            {
                _timeConnect = value;
            }
        }
        static int _timeDelay = 5;
        public static int Delay
        {
            get
            {
                return _timeDelay;
            }
            set
            {
                _timeDelay = value;
            }
        }

        static bool _laDNCX = false;
        public static bool LaDNCX
        {
            get
            {
                return _laDNCX;
            }
            set
            {
                _laDNCX = value;
            }

        }

        static bool _suDungChuKySo = false;
        
        public static bool SuDungChuKySo
        {
            get
            {
                return _suDungChuKySo;
            }
            set
            {
                _suDungChuKySo = value;
            }

        }

        /// <summary>
        /// Khởi tạo giá trị từ file cấu hình LanNT
        /// </summary>
        static Globals()
        {
            try
            {
                //_laDNCX = bool.Parse(GetConfig("LaDNCX"));

                string value = GetConfig("SuDungChuKySo");
                _suDungChuKySo = bool.Parse(value);

                _isSignature = bool.Parse(value);
                _appSend = GetConfig("AssemblyName");
                _GetX509CertificatedName = GetConfig("ChuKySo");
                _passwordSign = GetConfig("PasswordSign");
                if (!string.IsNullOrEmpty(_passwordSign))
                    _passwordSign = Helpers.DecryptString(_passwordSign, "KEYWORD").Trim('\0');
                _versionSend = GetConfig("VersionSend");
                _timeDelay = int.Parse(GetConfig("TimeDelay"));
                _isRememberSign = bool.Parse(GetConfig("IsRememberSign"));
                _timeConnect = int.Parse(GetConfig("TimeConnect"));

                _suDungHttps = bool.Parse(GetConfig("SuDungHttps"));
                _isSignRemote = bool.Parse(GetConfig("IsSignRemote"));
                _userNameSignRemote = GetConfig("UserNameSignRemote");
                _passwordSignRemote = GetConfig("PasswordSignRemote");
                if (!string.IsNullOrEmpty(_passwordSignRemote))
                    _passwordSignRemote = Helpers.DecryptString(_passwordSignRemote, "KEYWORD").Trim('\0');

                #region Login ký từ xa| Chỉ dùng cho bản ký từ xa
                try
                {
                    _isRemmberLogin = bool.Parse(GetConfig("IsRememberLogin"));
                    _userNameLogin = GetConfig("UserName");
                    _passwordLogin = GetConfig("Password");
                    if (!string.IsNullOrEmpty(_passwordLogin))
                        _passwordLogin = Helpers.DecryptString(_passwordLogin, "KEYWORD").Trim('\0');
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                #endregion
                value = GetConfig("TriGiaNT");
                if (!string.IsNullOrEmpty(value))
                    TriGiaNT = int.Parse(value);
                value = GetConfig("DonGiaNT");
                if (!string.IsNullOrEmpty(value))
                    DonGiaNT = int.Parse(value);
                value = GetConfig("CountSend");
                if (!string.IsNullOrEmpty(value))
                    _countSend = int.Parse(value);
                _fontName = GetConfig("FontName");
                if (string.IsNullOrEmpty(_fontName))
                    _fontName = "Unicode";
                string cfgName = "Config.xml";
                if (File.Exists(cfgName))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(cfgName);
                    if (doc != null)
                    {
                        XmlNode node = doc.SelectSingleNode("Root/Type");
                        if (node != null && !string.IsNullOrEmpty(node.InnerText))
                            IsDaiLy = node.InnerText.Trim() == "1" ? true : false;
                    }
                }

                value = GetConfig("IsSignOnLan");
                if (!string.IsNullOrEmpty(value))
                    IsSignOnLan = Convert.ToBoolean(value);

                DataSignLan = GetConfig("DataSignLan");

                FileInfo f = new FileInfo("Error.log");
                if (f.Exists && Math.Round(((decimal)f.Length / 1024) / 1024, 2) > 1)
                    f.Delete();
                f = new FileInfo("Error.txt");
                if (f.Exists && Math.Round(((decimal)f.Length / 1024) / 1024, 2) > 1)
                    f.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
#if (KD_V3 || SXXK_V3 || GC_V3)
                _versionSend = "3.00";
#elif (KD_V4 || SXXK_V4 || GC_V4)
                _versionSend = "4.00";
#else
                _versionSend = "3.00";
#endif
                _appSend = "DEFAULT_SOFTECH";
            }


        }

        #endregion

        public struct sConnectionString
        {
            public static string Server;
            public static string Database;
            public static string User;
            public static string Pass;
        }

        public struct IPServiceHQ
        {
            public static string DiaChi = "";
            public static string TenDichVu = "";
            public static string MaCuc = "";
            public static string TenCuc = "";
            public static string MaChiCuc = "";
            public static string TenChiCuc = "";
            public static string IP = "";
            public static string WebService = "";
        }

        /// <summary>
        /// Lay dia chi web service dua vao ma chi cuc HQ. Updated Hungtq 05/01/2013.
        /// </summary>
        /// <param name="maHQ"></param>
        /// <returns></returns>
        public static void GetIPWebService(string maChiCucHQ)
        {
            try
            {
                return;

                if (string.IsNullOrEmpty(maChiCucHQ)) return;

                string maCuc = maChiCucHQ.Substring(1, 2);
                IPServiceHQ.MaCuc = maCuc;

                Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ_Cuc = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load("Z" + maCuc + "Z");
                IPServiceHQ.TenCuc = objDonViHQ_Cuc != null ? objDonViHQ_Cuc.Ten : "";

                Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ_ChiCuc = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load(maChiCucHQ);
                IPServiceHQ.MaChiCuc = objDonViHQ_ChiCuc != null ? objDonViHQ_ChiCuc.ID : "";
                IPServiceHQ.TenChiCuc = objDonViHQ_ChiCuc != null ? objDonViHQ_ChiCuc.Ten : "";

                //Updated by Hungtq 29/03/2012.
                Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
                if (objCucHQ != null)
                {
                    IPServiceHQ.IP = objCucHQ.IPService;

                    IPServiceHQ.DiaChi = string.Format("http://{0}", (objCucHQ.IPService != "" ? objCucHQ.IPService : "?"));

                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00"
                        || Company.KDT.SHARE.Components.Globals.VersionSend == "4.00")
                    {
                        if (IsSignature)
                            IPServiceHQ.TenDichVu = objCucHQ.ServicePathCKS;
                        else
                            IPServiceHQ.TenDichVu = objCucHQ.ServicePathV3;
                    }
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    {
                        IPServiceHQ.TenDichVu = objCucHQ.ServicePathV2;
                    }
                }

                string url = IPServiceHQ.DiaChi.Trim() + "/" + IPServiceHQ.TenDichVu.Trim();
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url);

                IPServiceHQ.WebService = url;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static System.Drawing.Printing.Margins GetMargin(string margins)
        {
            string[] arr = margins.Split(new char[] { ',' });

            return new System.Drawing.Printing.Margins(
                int.Parse(arr[0].Trim()),
                int.Parse(arr[1].Trim()),
                int.Parse(arr[2].Trim()),
                int.Parse(arr[3].Trim()));
        }

        /// <summary>
        /// Lay ten 1 file xml trong danh sach trong 1 folder.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string GetFileName(string folderPath)
        {
            string[] fileNames = null;
            try
            {

                if (System.IO.Directory.Exists(folderPath))
                {
                    fileNames = System.IO.Directory.GetFiles(folderPath, "*.xml");
                }

                if (fileNames.Length > 0)
                {
                    return fileNames[0]; //Lay file xml dau tien trong danh sach (Neu co nhieu hon 1).
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }

        #region Send email
        public static bool SendMailReg(string subject, string tenDN, string maDN, string maHQ, string soDT, string nguoiLH, string diachi, string email, string loaiHinh, string appVersion, string dataversion)
        {
            return sendEmail(subject, string.Empty, string.Empty, tenDN, maDN, maHQ, soDT, string.Empty, nguoiLH, diachi, email, loaiHinh, appVersion, dataversion, string.Empty, new List<string>());
        }
        public static void sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string soDT, string soFax, string nguoiLH, string loaiHinh, string appVersion, string dataversion, string exception)
        {
            sendEmail(subject, body, imagePath, maDN, maDN, "", soDT, soFax, nguoiLH, loaiHinh, appVersion, dataversion, exception);
        }
        public static bool sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string maHQ, string soDT, string soFax, string nguoiLH, string loaiHinh, string appVersion, string dataversion, string exception)
        {
            return sendEmail(subject, body, imagePath, tenDN, maDN, maHQ, soDT, soFax, nguoiLH, string.Empty, string.Empty, loaiHinh, appVersion, dataversion, exception, new List<string>());
        }
        public static bool sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string maHQ, string soDT, string soFax, string nguoiLH, string diachi, string email, string loaiHinh, string appVersion, string dataversion, string exception, List<string> Files)
        {
            try
            {

                string ccTo = Globals.ReadNodeXmlAppSettings("FromMail");
                string toEmail = Globals.ReadNodeXmlAppSettings("ToMail");
                if (!CheckEmail(toEmail))
                    toEmail = "ecs@softech.vn";
                string fromEmail = "hotrotqdt@gmail.com";
                string content = "<font color=#1F497D><b>Hệ thống tự động báo lỗi</b><br><b>Tên doanh nghiệp:</b> " + tenDN + "<br>";
                content += string.IsNullOrEmpty(maDN) ? string.Empty : "<b>Mã doanh nghiệp: </b>" + maDN + "<br>";
                content += string.IsNullOrEmpty(diachi) ? string.Empty : "<b>Địa chỉ doanh nghiệp: </b>" + diachi + "<br>";
                content += string.IsNullOrEmpty(email) ? string.Empty : "<b>Email nhận key: </b>" + email + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Mã Hải quan: </b>" + maHQ + "<br>";
                content += string.IsNullOrEmpty(soDT) ? string.Empty : "<b>Số điện thoại: </b>" + soDT + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Số Fax: </b>" + soFax + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Người liên hệ: </b>" + nguoiLH + "<br>";
                //Hungtq update 16/07/2012.
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Loại hình khai báo: </b>" + loaiHinh + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Phiên bản phần mềm: </b>" + appVersion + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Phiên bản dữ liệu: </b>" + dataversion + "<br>";

                content += "<b>Thông báo lỗi như sau:</b> <br><br>";
                content += body + "</font><hr size=1 width=100% noshade style='color:#92D050' align=center><br><br><\t>";
                content += "<i>" + exception + "</i>";

                MailMessage messageObj = new MailMessage();
                messageObj.Subject = subject;
                messageObj.Body = content;
                messageObj.IsBodyHtml = true;
                messageObj.Priority = MailPriority.Normal;
                if (imagePath != "")
                {
                    Attachment attachment = new Attachment(imagePath);
                    messageObj.Attachments.Add(attachment);
                }
                foreach (string item in Files)
                {
                    if (File.Exists(item))
                        messageObj.Attachments.Add(new Attachment(item));
                }
                #region Send Mail

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(fromEmail, "hotrotqdt");
                client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                messageObj.From = new MailAddress(fromEmail);
                messageObj.To.Add(toEmail);
                if (CheckEmail(ccTo))
                    messageObj.CC.Add(ccTo);

                #endregion Send Mail
                client.Timeout = 15000;
                client.SendAsync(messageObj, "Sending..");
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        static void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            String token = (string)e.UserState;
            if (e.Error != null)
            {

                throw e.Error;

            }
            else
            {
                //  LoggerWrapper.Logger.Write("Message Delivered.", LogCategory.Information);
            }
        }
        private static bool CheckEmail(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }
        public static bool SaveAsImage(string filename)
        {
            try
            {
                Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
                Size s = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

                Graphics g = Graphics.FromImage(b);
                g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, s, CopyPixelOperation.SourceCopy);
                b.Save(filename, ImageFormat.Jpeg);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            //b.Save(@"c:\ERROR.png", ImageFormat.Png);  
        }

        /// <summary>
        /// Kiểm tra chuỗi nhập vào có phải là kiểu số.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isNumber(string value)
        {
            bool isValid = true;

            char[] arr = value.ToCharArray();

            for (int i = 0; i < arr.Length; i++)
            {
                isValid &= Char.IsNumber(arr[i]);
            }

            return isValid;
        }

        public static bool isEmail(string inputEmail)
        {
            return CheckEmail(inputEmail);
        }
        #endregion

        #region Save Message
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao, string noidungthongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, noidungthongbao);
        }
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao, string noidungthongbao)
        {
            if (string.IsNullOrEmpty(xml)) return false;
            try
            {
                if (string.IsNullOrEmpty(xml)) return false;
                XmlDocument doc = new XmlDocument();
                Message message = new Message();
                message.ItemID = itemId;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = xml;
                bool canLoad = false;
                try
                {
                    doc.LoadXml(xml);
                    canLoad = true;
                }
                catch { }
                if (canLoad)
                {

                    message.MessageType = (MessageTypes)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/type").InnerText);
                    message.MessageFunction = (MessageFunctions)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/function").InnerText);
                    message.ReferenceID = new Guid(doc.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
                    message.MessageFrom = doc.SelectSingleNode("Envelope/Header/From/identity").InnerText;
                    message.MessageTo = doc.SelectSingleNode("Envelope/Header/To/identity").InnerText;
                    message.CreatedTime = DateTime.Now;
                }

                message.Insert();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao)
        {
            return SaveMessage(xml, itemId, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, string messagefrom, string messageto, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageFrom = messagefrom;
                message.MessageTo = messageto;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        public static string Message2File(string messageError)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(messageError);
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                string reference = string.Empty;
                if (node != null) reference = node.InnerText;
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Errors"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Errors");
                string filename = AppDomain.CurrentDomain.BaseDirectory + "Errors\\" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss_") + reference + ".xml";
                StreamWriter sw = File.CreateText(filename);
                sw.Write(messageError);
                sw.Close();
                return filename;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(messageError, ex);
                return string.Empty;
            }
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao)
        {
            return SaveMessage(content, itemId, guidstr, messagetype, function, tieudethongbao, string.Empty);
        }
        #endregion

        public static string GetPhanLuong(string phanLuong)
        {
            if (phanLuong.Trim() == "1")
                return "Luồng xanh";
            else if (phanLuong.Trim() == "2")
                return "Luồng vàng";
            if (phanLuong.Trim() == "3")
                return "Luồng đỏ";
            else
                return "";
        }
        /// <summary>
        /// Lây tên trạng thái xử lý
        /// </summary>
        /// <param name="trangThai"></param>
        /// <returns></returns>
        public static string GetTrangThaiXuLyTK(int trangThai)
        {
            if (trangThai == TrangThaiXuLy.DA_DUYET)
                return "Đã duyệt";
            else if (trangThai == TrangThaiXuLy.CHO_DUYET)
                return "Chờ duyệt";
            if (trangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                return "Chưa khai báo";
            if (trangThai == TrangThaiXuLy.SUATKDADUYET)
                return "Sửa tờ khai";
            if (trangThai == TrangThaiXuLy.HUYTKDADUYET)
                return "Hủy tờ khai";
            if (trangThai == TrangThaiXuLy.CHO_HUY)
                return "Chờ hủy";
            if (trangThai == TrangThaiXuLy.DA_HUY)
                return "Đã hủy";
            if (trangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                return "Không phê duyệt";
            else
                return "";
        }

        /// <summary>
        /// Khong co so thap phan
        /// </summary>
        /// <returns></returns>
        public static string FormatNumber()
        {
            return "{0:###,###,##0.######}";
        }

        /// <summary>
        /// Co so thap phan
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string FormatNumber(int number)
        {
            return FormatNumber(number, false);

        }
        public static string FormatNumber(int number, bool batBuocSoThapPhan)
        {
            return "{0:###,###,##0" + Globals.GetDot(number, batBuocSoThapPhan) + "}";
        }
        /// <summary>
        /// Định dạng số thập phân
        /// chuẩn microsoft "###,###,###.########"
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static String GetPrecision(int num)
        {
            return GetPrecision(num, false);
        }
        public static String GetPrecision(int num, bool batBuocSoThapPhan)
        {
            string val = "###,###,##0" + GetDot(num, batBuocSoThapPhan);
            return val;
        }
        public static string GetDot(int decimalFormat, bool batBuocSoThapPhan)
        {
            string ret = string.Empty;
            if (decimalFormat < 0) return ret;
            else if (decimalFormat > 0)
                ret += ".";
            for (int i = 0; i < decimalFormat; i++)
            {
                if (!batBuocSoThapPhan)
                    ret += "#";
                else
                    ret += "0";
            }
            return ret;
        }

        /// <summary>
        /// Thoi gian cho gui & nhan (second). Mac dinh: 5s -> 5000 milisecond.
        /// </summary>
        /// <returns>MiliSeconds</returns>
        public static int TimeDelay()
        {
            int TimeDelay = 5;

            try
            {
                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");

                TimeDelay = delay != "" ? int.Parse(delay) : 5;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("TimeDelay", ex); }

            return TimeDelay * 1000; //1s= 1000ms.
        }

        public static string Zip(string sourceFile, string fromDirectory, string toDirectory)
        {
            string destinateFile = "";

            if (System.IO.File.Exists(sourceFile))
            {
                System.IO.FileInfo fi = new FileInfo(sourceFile);

                destinateFile = toDirectory + "\\" + fi.Name.Replace(fi.Extension, "") + ".zip";
            }
            else
                return "";

            if (System.IO.File.Exists(sourceFile))
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("zip.exe", "-j -r -9 -D \"" +
                                                         destinateFile + "\" \"" +
                                                         sourceFile + "\"");
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process ps = System.Diagnostics.Process.Start(psi);
                ps.WaitForExit();
            }

            return destinateFile;
        }

        /// <summary>
        /// Mở webrowser
        /// </summary>
        /// <param name="url"></param>
        public static void OpenWebrowser(string url)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = url;

            process.Start();
        }

        #region Menu Command - Main form

        /// <summary>
        /// Command menu Bieu thue (HS)
        /// </summary>
        /// <param name="key"></param>
        public static void OpenCommandBieuThue(string key)
        {
            string url = "";

            switch (key)
            {
                case "cmdTraCuuXNKOnline": //Tra cứu biểu thuế xuất nhập khẩu trực tuyến
                    url = "http://customs.gov.vn/Lists/BieuThue2012/TraCuu.aspx";
                    break;
                case "cmdTraCuuNoThueOnline": //Tra cứu nợ thuế trực tuyến
                    url = "http://customs.gov.vn/Lists/TraCuuNoThue/Default.aspx";
                    break;
                case "cmdTraCuuVanBanOnline": //Tra cứu thư viện văn bản trực tuyến
                    url = "http://vanbanphapquy.vn/Default.aspx";
                    break;
                case "cmdTuVanHQOnline": //Tư vấn Hải quan trực tuyến
                    url = "http://customs.gov.vn/Lists/HoTroTrucTuyen/Default.aspx";
                    break;
            }

            if (url.Length > 0)
                Company.KDT.SHARE.Components.Globals.OpenWebrowser(url);
        }

        /// <summary>
        /// Cập nhật dữ liệu danh mục Online từ Server về Doanh nghiệp.
        /// </summary>
        /// <returns></returns>
        public static string UpdateCategoryOnline()
        {
            ISyncData syndata = WebService.SyncService();

            DataSet ds = new DataSet();

            DateTime dateLastUpdated = new DateTime(1900, 1, 1);

            string msg = "";

            try
            {
                #region Get data from Internet

                // EDanhMucHaiQuan.CuaKhau:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.CuaKhau.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.CuaKhau);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.CuaKhau.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.CuaKhau.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Cửa khẩu:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.Cuc:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.Cuc.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.Cuc);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Cục Hải quan:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DieuKienGiaoHang:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DieuKienGiaoHang.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DieuKienGiaoHang);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DieuKienGiaoHang.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DieuKienGiaoHang.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Điều kiện giao hàng:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DonViHaiQuan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DonViHaiQuan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DonViHaiQuan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Đơn vị Hải quan:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.DonViTinh:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.DonViTinh.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.DonViTinh);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViTinh.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViTinh.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Đơn vị tính:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.LoaiPhiChungTuThanhToan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.LoaiPhiChungTuThanhToan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.LoaiPhiChungTuThanhToan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiPhiChungTuThanhToan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiPhiChungTuThanhToan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Loại phí chứng từ thanh toán:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.MaHS:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.MaHS.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.MaHS);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.MaHS.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.MaHS.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Mã biểu thuế (HS):\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.NguyenTe:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.NguyenTe.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.NguyenTe);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.NguyenTe.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Nguyên tệ:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.Nuoc:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.Nuoc.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.Nuoc);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.Nuoc.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.Nuoc.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Nước:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.PhuongThucThanhToan:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.PhuongThucThanhToan.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.PhuongThucThanhToan);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucThanhToan.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucThanhToan.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Phương thức thanh toán:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.PhuongThucVanTai:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.PhuongThucVanTai.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.PhuongThucVanTai);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucVanTai.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.PhuongThucVanTai.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Phương thức vận tải:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                // EDanhMucHaiQuan.LoaiHinhMauDich:
                dateLastUpdated = GetDanhMucMaxDateModified(string.Format("t_HaiQuan_{0}", EDanhMucHaiQuan.LoaiHinhMauDich.ToString()));
                ds = syndata.GetDanhMucOnline("daibangtungcanh", dateLastUpdated, EDanhMucHaiQuan.LoaiHinhMauDich);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiHinhMauDich.InsertUpdateCollection(Company.KDT.SHARE.Components.DanhMucHaiQuan.LoaiHinhMauDich.ConvertDataSetToCollection(ds)) == true && ds.Tables[0].Rows.Count > 0)
                        msg += "\r\n-Loại hình mậu dịch:\t" + ds.Tables[0].Rows.Count + " bản ghi";

                    ds = null;
                }

                #endregion

                return msg.Length != 0 ? "Đã cập nhật thông tin các Danh mục:" + msg : "";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msg = "E|" + ex.Message;
            }

            return msg;
        }

        /// <summary>
        /// Lay ngay cap nhat moi gan nhat cua Danh muc.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DateTime GetDanhMucMaxDateModified(string tableName)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT max(DateModified) as MaxDate FROM {0}", tableName);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                return reader["MaxDate"] != System.DBNull.Value ? Convert.ToDateTime(reader["MaxDate"]) : new DateTime(1900, 01, 01);
            }
            return new DateTime(1900, 1, 1);
        }

        /// <summary>
        /// Lay ngay cap nhat moi gan nhat cua Danh muc.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DateTime GetMaxDateModified(string tableName, string fieldName)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT max(" + fieldName + ") as MaxDate FROM {0}", tableName);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                return reader["MaxDate"] != System.DBNull.Value ? Convert.ToDateTime(reader["MaxDate"]) : new DateTime(1900, 01, 01);
            }
            return new DateTime(1900, 1, 1);
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số tự động. (Cắt 2 số '00' bên phải mã HS, chỉ lấy 8 số).
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Auto(string maHQ, string maDN, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_Convert_MaHS8SoAuto]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số theo giá trị mã HS mới
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="ma"></param>
        /// <param name="dvtId"></param>
        /// <param name="maHSCu"></param>
        /// <param name="maHSMoi"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Manual_ByCode(string maHQ, string maDN, string ma, string dvtId, string maHSCu, string maHSMoi, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_Convert_MaHS8So]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
                db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, ma);
                db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.NVarChar, dvtId);
                db.AddInParameter(dbCommand, "@MaHSCu", SqlDbType.NVarChar, maHSCu);
                db.AddInParameter(dbCommand, "@MaHSMoi", SqlDbType.NVarChar, maHSMoi);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Chuyển đổi mã HS 8 số theo giá trị mã HS mới
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <param name="ten"></param>
        /// <param name="dvtId"></param>
        /// <param name="maHSCu"></param>
        /// <param name="maHSMoi"></param>
        /// <param name="phanHe">KD: Kinh doanh, GC: Gia cong, SXXK: San xuat xuat khau</param>
        /// <returns></returns>
        public static bool ConvertHS8Manual_ByName(string maHQ, string maDN, string ten, string dvtId, string maHSCu, string maHSMoi, string phanHe)
        {
            try
            {
                string spName = string.Format("[dbo].[p_{0}_KD_Convert_MaHS8So_Ten]", phanHe);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                System.Data.SqlClient.SqlCommand dbCommand = (System.Data.SqlClient.SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, maHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDN);
                db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, ten);
                db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.NVarChar, dvtId);
                db.AddInParameter(dbCommand, "@MaHSCu", SqlDbType.NVarChar, maHSCu);
                db.AddInParameter(dbCommand, "@MaHSMoi", SqlDbType.NVarChar, maHSMoi);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return false;
        }

        /// <summary>
        /// Tải chương trình Image Resize từ trang codeplex.com.
        /// Tool open source, miễn phí.
        /// </summary>
        /// <returns></returns>
        public static string DownloadToolImageResize()
        {
            try
            {
                string url = "http://imageresizer.codeplex.com/downloads/get/347439";

                OpenWebrowser(url);

                //string folderPath = AppDomain.CurrentDomain.BaseDirectory + "Support";

                //if (!System.IO.Directory.Exists(folderPath))
                //    System.IO.Directory.CreateDirectory(folderPath);

                //string filePath = folderPath + "\\" + "ImageResizerSetup.exe";

                //if (!System.IO.File.Exists(filePath))
                //{
                //    WebClient clientDownload = new WebClient();

                //    clientDownload.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                //    clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                //    clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //    clientDownload.DownloadFileAsync(new Uri(url), filePath, "ImageResizerSetup.exe");

                //    clientDownload.Dispose();
                //    clientDownload = null;
                //}
                //else
                //    System.Diagnostics.Process.Start(filePath);

                return "";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }
        }

        public static string HDSDImageResize()
        {
            string folderPath = AppDomain.CurrentDomain.BaseDirectory + "Support";

            string filePath = folderPath + "\\" + "HuongDanSuDung_ImageResize.doc";

            if (!System.IO.File.Exists(filePath))
            {
                return "Thiếu tệp tin 'Hướng dẫn sử dụng giảm dung lượng ảnh' trên chương trình.";
            }
            else
            {
                System.Diagnostics.Process.Start(filePath);
                return "";
            }
        }

        #endregion

        //public static string ConvertToString(object source)
        //{
        //    string ConvertToString;
        //    DateTime dateTime;

        //    if (source == null)
        //        return String.Empty;
        //    if (String.IsNullOrEmpty(Conversions.ToString(source)))
        //        return String.Empty;
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy\uFFFD");
        //    }
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy HH:mm:ss\uFFFD");
        //    }
        //    return source.ToString();
        //}

        //public static string FormatNumeric(object obj, int percent)
        //{
        //    string FormatNumeric;
        //    decimal dec;

        //    string ret = Conversions.ToString(0);
        //    string sFormat = "#,###,##0.0";

        //    for (int i = 1; i <= percent; i++)
        //    {
        //        sFormat += "#";
        //    }
        //    if (obj is int)
        //    {
        //        int i2 = Int32.Parse(Conversions.ToString(obj));
        //        ret = i2.ToString();
        //    }
        //    else if (obj is double)
        //    {
        //        double d = Double.Parse(Conversions.ToString(obj));
        //        ret = d.ToString(sFormat);
        //    }
        //    else if (obj is decimal)
        //    {
        //        dec = Decimal.Parse(Conversions.ToString(obj));
        //        ret = dec.ToString(sFormat);
        //    }
        //    else if (obj is string)
        //    {
        //        if (String.IsNullOrEmpty(Conversions.ToString(obj)))
        //        {
        //            ret = Conversions.ToString(0);
        //        }
        //        else
        //        {
        //            dec = Decimal.Parse(Conversions.ToString(obj));
        //            ret = dec.ToString(sFormat);
        //        }
        //    }
        //    return ret;
        //}

        #region EXCEL TEMPLATE

        public static void CreateExcelTemplate_ChuyenMaHS8So()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "ChuyenMaHS8So.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Tên hàng";
                workSheet.GetCell("C1").Value = "Đơn vị tính";
                workSheet.GetCell("D1").Value = "Mã HS cũ";
                workSheet.GetCell("E1").Value = "Mã HS mới";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion

        //mau excel them hang
        #region Excel Template Hang Hoa
        public static void CreateExcelTemplate_HangHoa()
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = "HangHoa_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");

                workSheet.GetCell("A1").Value = "Mã hàng";
                workSheet.GetCell("B1").Value = "Tên hàng";
                workSheet.GetCell("C1").Value = "Mã HS";
                workSheet.GetCell("D1").Value = "Xuất xứ ";
                workSheet.GetCell("E1").Value = "Đơn vị tính";
                workSheet.GetCell("F1").Value = "Số lượng";
                workSheet.GetCell("G1").Value = "Đơn giá";
                workSheet.GetCell("H1").Value = "TGTT";
                workSheet.GetCell("I1").Value = "TS XNK";
                workSheet.GetCell("J1").Value = "TS TTDB";
                workSheet.GetCell("K1").Value = "TS VAT";
                workSheet.GetCell("L1").Value = "TLCL giá";

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        #endregion

        // mau excel SXXK
        #region Excel Template SXXK

        public static void CreateExcelTemplate_SXXK(string type)
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = type + "_Excel.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");
                if (type == "NPL")
                {
                    workSheet.GetCell("A1").Value = "Mã NPL";
                    workSheet.GetCell("B1").Value = "Tên NPL";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                }
                else if (type == "SP")
                {
                    workSheet.GetCell("A1").Value = "Mã sản phẩm";
                    workSheet.GetCell("B1").Value = "Tên sản phẩm";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                }
                else if (type == "DM")
                {
                    workSheet.GetCell("A1").Value = "Mã sản phẩm";
                    workSheet.GetCell("B1").Value = "Mã NPL";
                    workSheet.GetCell("C1").Value = "ĐMSD";
                    workSheet.GetCell("D1").Value = "TLHH";

                }
                else if (type == "HangHoa")
                {
                    workSheet.GetCell("A1").Value = "Mã hàng";
                    workSheet.GetCell("B1").Value = "Số lượng";
                    workSheet.GetCell("C1").Value = "Xuất xứ";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                    workSheet.GetCell("E1").Value = "TS XNK";
                }

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion
        // Mau Excel GC
        #region Excel Template GC

        public static void CreateExcelTemplate_GC(string type)
        {
            try
            {

                //Chọn đường dẫn file cần lưu
                string fileName = type + "ExcelTempl.xls";
                string pathTemplateFolder = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelTemplate";
                if (!System.IO.Directory.Exists(pathTemplateFolder))
                    System.IO.Directory.CreateDirectory(pathTemplateFolder);

                string filePath = pathTemplateFolder + "\\" + fileName;

                if (System.IO.File.Exists(filePath))
                    System.Diagnostics.Process.Start(filePath);

                Infragistics.Excel.Workbook workBook = new Infragistics.Excel.Workbook(Infragistics.Excel.WorkbookFormat.Excel97To2003);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets.Add("Sheet1");
                if (type == "NPL_HD")
                {
                    workSheet.GetCell("A1").Value = "Mã NPL";
                    workSheet.GetCell("B1").Value = "Tên NPL";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                    workSheet.GetCell("E1").Value = "Số lượng";
                    workSheet.GetCell("F1").Value = "Đơn giá";
                }
                else if (type == "SP_HD")
                {
                    workSheet.GetCell("A1").Value = "Mã sản phẩm";
                    workSheet.GetCell("B1").Value = "Tên sản phẩm";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                    workSheet.GetCell("E1").Value = "Số lượng";
                    workSheet.GetCell("F1").Value = "Đơn giá";
                    workSheet.GetCell("G1").Value = "Loại SPGC";
                }
                else if (type == "TB_HD")
                {
                    workSheet.GetCell("A1").Value = "Mã thiết bị";
                    workSheet.GetCell("B1").Value = "Tên thiết bị";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                    workSheet.GetCell("E1").Value = "Xuất xứ";
                    workSheet.GetCell("F1").Value = "Số lượng";
                    workSheet.GetCell("G1").Value = "Đơn giá";
                    workSheet.GetCell("H1").Value = "Nguyên tệ";
                    workSheet.GetCell("I1").Value = "Tình trạng";

                }
                else if (type == "HangHoa")
                {
                    workSheet.GetCell("A1").Value = "Mã hàng";
                    workSheet.GetCell("B1").Value = "Tên hàng";
                    workSheet.GetCell("C1").Value = "Mã HS";
                    workSheet.GetCell("D1").Value = "Đơn vị tính";
                    workSheet.GetCell("E1").Value = "Số lượng";
                    workSheet.GetCell("F1").Value = "Đơn giá";
                    workSheet.GetCell("G1").Value = "Xuất xứ";
                    workSheet.GetCell("H1").Value = "TGTT";
                    workSheet.GetCell("I1").Value = "TS XNK";
                }
                else if (type == "DM")
                {
                    workSheet.GetCell("A1").Value = "Mã sản phẩm";
                    workSheet.GetCell("B1").Value = "Mã NPL";
                    workSheet.GetCell("C1").Value = "ĐM sử dụng";
                    workSheet.GetCell("D1").Value = "Tỷ lệ hao hụt";
                    workSheet.GetCell("E1").Value = "NPL tự cung ứng";
                }
                else if (type == "HangHoa_CT")
                {
                    workSheet.GetCell("A1").Value = "Mã hàng";
                    workSheet.GetCell("B1").Value = "Số lượng";
                    workSheet.GetCell("C1").Value = "Đơn giá";
                    workSheet.GetCell("D1").Value = "Xuất xứ";
                }

                //Ghi nội dung file gốc vào file cần lưu
                Infragistics.Excel.BIFF8Writer.WriteWorkbookToFile(workBook, filePath);

                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        #endregion


        #region Thong Bao

        /// <summary>
        /// Tìm kiếm nội dung thông báo lỗi tương ứng với chuỗi lỗi
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static string[] TimThongBaoLoi(string error)
        {
            string[] array = new string[2];

            try
            {
                List<ThongBaoLoi> list = ThongBaoLoi.SelectCollectionAll();

                foreach (ThongBaoLoi i in list)
                {
                    if (error.Contains(i.Loi))
                    {
                        array[0] = i.TieuDeThongBao;
                        array[1] = i.ThongBao;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

            return array;
        }
        #endregion
    }

    public class GenericListToDataTable
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        private GenericListToDataTable()
        { }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name=”T”>Custome Class </typeparam>
        /// <param name=”lst”>List Of The Custome Class</param>
        /// <returns> Return the class datatbl </returns>
        public static DataTable ConvertTo<T>(IList<T> lst)
        {
            //create DataTable Structure
            DataTable tbl = CreateTable<T>();
            Type entType = typeof(T);

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            //get the list item and add into the list
            foreach (T item in lst)
            {
                DataRow row = tbl.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                tbl.Rows.Add(row);
            }

            return tbl;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name=”T”>Custome Class</typeparam>
        /// <returns></returns>
        public static DataTable CreateTable<T>()
        {
            //T –> ClassName
            Type entType = typeof(T);

            //set the datatable name as class name
            DataTable tbl = new DataTable(entType.Name);

            //get the property list
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (PropertyDescriptor prop in properties)
            {
                //add property as column
                tbl.Columns.Add(prop.Name, prop.PropertyType);
            }

            return tbl;
        }

    }
}
