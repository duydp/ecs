using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class BaoLanhThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string DonViBaoLanh { set; get; }
		public string SoGiayBaoLanh { set; get; }
		public int NamChungTuBaoLanh { set; get; }
		public DateTime NgayHieuLuc { set; get; }
		public string LoaiBaoLanh { set; get; }
		public decimal SoTienBaoLanh { set; get; }
		public decimal SoDuTienBaoLanh { set; get; }
		public int SoNgayDuocBaoLanh { set; get; }
		public DateTime NgayHetHieuLuc { set; get; }
		public DateTime NgayKyBaoLanh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<BaoLanhThue> ConvertToCollection(IDataReader reader)
		{
			IList<BaoLanhThue> collection = new List<BaoLanhThue>();
			while (reader.Read())
			{
				BaoLanhThue entity = new BaoLanhThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViBaoLanh"))) entity.DonViBaoLanh = reader.GetString(reader.GetOrdinal("DonViBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayBaoLanh"))) entity.SoGiayBaoLanh = reader.GetString(reader.GetOrdinal("SoGiayBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamChungTuBaoLanh"))) entity.NamChungTuBaoLanh = reader.GetInt32(reader.GetOrdinal("NamChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHieuLuc"))) entity.NgayHieuLuc = reader.GetDateTime(reader.GetOrdinal("NgayHieuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiBaoLanh"))) entity.LoaiBaoLanh = reader.GetString(reader.GetOrdinal("LoaiBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienBaoLanh"))) entity.SoTienBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDuTienBaoLanh"))) entity.SoDuTienBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoDuTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayDuocBaoLanh"))) entity.SoNgayDuocBaoLanh = reader.GetInt32(reader.GetOrdinal("SoNgayDuocBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHieuLuc"))) entity.NgayHetHieuLuc = reader.GetDateTime(reader.GetOrdinal("NgayHetHieuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKyBaoLanh"))) entity.NgayKyBaoLanh = reader.GetDateTime(reader.GetOrdinal("NgayKyBaoLanh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<BaoLanhThue> collection, long id)
        {
            foreach (BaoLanhThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_BaoLanhThue VALUES(@TKMD_ID, @DonViBaoLanh, @SoGiayBaoLanh, @NamChungTuBaoLanh, @NgayHieuLuc, @LoaiBaoLanh, @SoTienBaoLanh, @SoDuTienBaoLanh, @SoNgayDuocBaoLanh, @NgayHetHieuLuc, @NgayKyBaoLanh)";
            string update = "UPDATE t_KDT_BaoLanhThue SET TKMD_ID = @TKMD_ID, DonViBaoLanh = @DonViBaoLanh, SoGiayBaoLanh = @SoGiayBaoLanh, NamChungTuBaoLanh = @NamChungTuBaoLanh, NgayHieuLuc = @NgayHieuLuc, LoaiBaoLanh = @LoaiBaoLanh, SoTienBaoLanh = @SoTienBaoLanh, SoDuTienBaoLanh = @SoDuTienBaoLanh, SoNgayDuocBaoLanh = @SoNgayDuocBaoLanh, NgayHetHieuLuc = @NgayHetHieuLuc, NgayKyBaoLanh = @NgayKyBaoLanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_BaoLanhThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViBaoLanh", SqlDbType.NVarChar, "DonViBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, "SoGiayBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamChungTuBaoLanh", SqlDbType.Int, "NamChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHieuLuc", SqlDbType.DateTime, "NgayHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, "SoDuTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, "SoNgayDuocBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, "NgayKyBaoLanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViBaoLanh", SqlDbType.NVarChar, "DonViBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, "SoGiayBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamChungTuBaoLanh", SqlDbType.Int, "NamChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHieuLuc", SqlDbType.DateTime, "NgayHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, "SoDuTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, "SoNgayDuocBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, "NgayKyBaoLanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_BaoLanhThue VALUES(@TKMD_ID, @DonViBaoLanh, @SoGiayBaoLanh, @NamChungTuBaoLanh, @NgayHieuLuc, @LoaiBaoLanh, @SoTienBaoLanh, @SoDuTienBaoLanh, @SoNgayDuocBaoLanh, @NgayHetHieuLuc, @NgayKyBaoLanh)";
            string update = "UPDATE t_KDT_BaoLanhThue SET TKMD_ID = @TKMD_ID, DonViBaoLanh = @DonViBaoLanh, SoGiayBaoLanh = @SoGiayBaoLanh, NamChungTuBaoLanh = @NamChungTuBaoLanh, NgayHieuLuc = @NgayHieuLuc, LoaiBaoLanh = @LoaiBaoLanh, SoTienBaoLanh = @SoTienBaoLanh, SoDuTienBaoLanh = @SoDuTienBaoLanh, SoNgayDuocBaoLanh = @SoNgayDuocBaoLanh, NgayHetHieuLuc = @NgayHetHieuLuc, NgayKyBaoLanh = @NgayKyBaoLanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_BaoLanhThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViBaoLanh", SqlDbType.NVarChar, "DonViBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, "SoGiayBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamChungTuBaoLanh", SqlDbType.Int, "NamChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHieuLuc", SqlDbType.DateTime, "NgayHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, "SoDuTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, "SoNgayDuocBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, "NgayKyBaoLanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViBaoLanh", SqlDbType.NVarChar, "DonViBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, "SoGiayBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamChungTuBaoLanh", SqlDbType.Int, "NamChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHieuLuc", SqlDbType.DateTime, "NgayHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.VarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, "SoDuTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, "SoNgayDuocBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, "NgayKyBaoLanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static BaoLanhThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_BaoLanhThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<BaoLanhThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<BaoLanhThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<BaoLanhThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_BaoLanhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_BaoLanhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_BaoLanhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_BaoLanhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertBaoLanhThue(long tKMD_ID, string donViBaoLanh, string soGiayBaoLanh, int namChungTuBaoLanh, DateTime ngayHieuLuc, string loaiBaoLanh, decimal soTienBaoLanh, decimal soDuTienBaoLanh, int soNgayDuocBaoLanh, DateTime ngayHetHieuLuc, DateTime ngayKyBaoLanh)
		{
			BaoLanhThue entity = new BaoLanhThue();	
			entity.TKMD_ID = tKMD_ID;
			entity.DonViBaoLanh = donViBaoLanh;
			entity.SoGiayBaoLanh = soGiayBaoLanh;
			entity.NamChungTuBaoLanh = namChungTuBaoLanh;
			entity.NgayHieuLuc = ngayHieuLuc;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoDuTienBaoLanh = soDuTienBaoLanh;
			entity.SoNgayDuocBaoLanh = soNgayDuocBaoLanh;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.NgayKyBaoLanh = ngayKyBaoLanh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_BaoLanhThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DonViBaoLanh", SqlDbType.NVarChar, DonViBaoLanh);
			db.AddInParameter(dbCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, SoGiayBaoLanh);
			db.AddInParameter(dbCommand, "@NamChungTuBaoLanh", SqlDbType.Int, NamChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHieuLuc", SqlDbType.DateTime, NgayHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHieuLuc);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, SoDuTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, SoNgayDuocBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, NgayKyBaoLanh.Year <= 1753 ? DBNull.Value : (object) NgayKyBaoLanh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<BaoLanhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (BaoLanhThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateBaoLanhThue(long id, long tKMD_ID, string donViBaoLanh, string soGiayBaoLanh, int namChungTuBaoLanh, DateTime ngayHieuLuc, string loaiBaoLanh, decimal soTienBaoLanh, decimal soDuTienBaoLanh, int soNgayDuocBaoLanh, DateTime ngayHetHieuLuc, DateTime ngayKyBaoLanh)
		{
			BaoLanhThue entity = new BaoLanhThue();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.DonViBaoLanh = donViBaoLanh;
			entity.SoGiayBaoLanh = soGiayBaoLanh;
			entity.NamChungTuBaoLanh = namChungTuBaoLanh;
			entity.NgayHieuLuc = ngayHieuLuc;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoDuTienBaoLanh = soDuTienBaoLanh;
			entity.SoNgayDuocBaoLanh = soNgayDuocBaoLanh;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.NgayKyBaoLanh = ngayKyBaoLanh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_BaoLanhThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DonViBaoLanh", SqlDbType.NVarChar, DonViBaoLanh);
			db.AddInParameter(dbCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, SoGiayBaoLanh);
			db.AddInParameter(dbCommand, "@NamChungTuBaoLanh", SqlDbType.Int, NamChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHieuLuc", SqlDbType.DateTime, NgayHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHieuLuc);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, SoDuTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, SoNgayDuocBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, NgayKyBaoLanh.Year <= 1753 ? DBNull.Value : (object) NgayKyBaoLanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<BaoLanhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (BaoLanhThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateBaoLanhThue(long id, long tKMD_ID, string donViBaoLanh, string soGiayBaoLanh, int namChungTuBaoLanh, DateTime ngayHieuLuc, string loaiBaoLanh, decimal soTienBaoLanh, decimal soDuTienBaoLanh, int soNgayDuocBaoLanh, DateTime ngayHetHieuLuc, DateTime ngayKyBaoLanh)
		{
			BaoLanhThue entity = new BaoLanhThue();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.DonViBaoLanh = donViBaoLanh;
			entity.SoGiayBaoLanh = soGiayBaoLanh;
			entity.NamChungTuBaoLanh = namChungTuBaoLanh;
			entity.NgayHieuLuc = ngayHieuLuc;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.SoDuTienBaoLanh = soDuTienBaoLanh;
			entity.SoNgayDuocBaoLanh = soNgayDuocBaoLanh;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.NgayKyBaoLanh = ngayKyBaoLanh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_BaoLanhThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@DonViBaoLanh", SqlDbType.NVarChar, DonViBaoLanh);
			db.AddInParameter(dbCommand, "@SoGiayBaoLanh", SqlDbType.NVarChar, SoGiayBaoLanh);
			db.AddInParameter(dbCommand, "@NamChungTuBaoLanh", SqlDbType.Int, NamChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHieuLuc", SqlDbType.DateTime, NgayHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHieuLuc);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.VarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoDuTienBaoLanh", SqlDbType.Decimal, SoDuTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoNgayDuocBaoLanh", SqlDbType.Int, SoNgayDuocBaoLanh);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@NgayKyBaoLanh", SqlDbType.DateTime, NgayKyBaoLanh.Year <= 1753 ? DBNull.Value : (object) NgayKyBaoLanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<BaoLanhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (BaoLanhThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteBaoLanhThue(long id)
		{
			BaoLanhThue entity = new BaoLanhThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_BaoLanhThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_BaoLanhThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<BaoLanhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (BaoLanhThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}