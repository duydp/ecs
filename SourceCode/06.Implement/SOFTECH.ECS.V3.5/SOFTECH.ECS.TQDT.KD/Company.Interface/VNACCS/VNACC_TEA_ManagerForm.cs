﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TEA_ManagerForm : BaseForm
    {
        private List<KDT_VNACCS_TEA> listTEA = new List<KDT_VNACCS_TEA>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        private DataSet ds = new DataSet();
        private string where = "";
        public VNACC_TEA_ManagerForm()
        {
            InitializeComponent();

            this.Text = "Theo dõi hàng miễn thuế";
        }

        private void VNACC_TEA_ManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
                cbbPhanLoaiXuatNhap.SelectedValue = "0";
                cbbTrangThaiXuLy.SelectedValue = "0";
                btnTimKiem_Click(null, null);
                //where = "TrangThaiXuLy='-1'";
                //LoadData(where);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void LoadData(string where )
        {
            try
            {
                listTEA = KDT_VNACCS_TEA.SelectCollectionDynamic(where, "");
                grList.DataSource = listTEA;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private KDT_VNACCS_TEA getTEA(long id)
        {
            try
            {
                foreach (KDT_VNACCS_TEA TEA in listTEA)
                {
                    if (TEA.ID == id)
                    {
                        string query = "Master_ID =" + id;
                        List<KDT_VNACCS_TEA_HangHoa> listHang = new List<KDT_VNACCS_TEA_HangHoa>();
                        listHang = KDT_VNACCS_TEA_HangHoa.SelectCollectionDynamic(query, "ID");
                        foreach (KDT_VNACCS_TEA_HangHoa hang in listHang)
                        {
                            TEA.HangCollection.Add(hang);
                        }
                        List<KDT_VNACCS_TEADieuChinh> listDieuChinh = new List<KDT_VNACCS_TEADieuChinh>();
                        listDieuChinh = KDT_VNACCS_TEADieuChinh.SelectCollectionDynamic(query, "ID");
                        foreach (KDT_VNACCS_TEADieuChinh dc in listDieuChinh)
                        {
                            TEA.DieuChinhCollection.Add(dc);
                        }
                        List<KDT_VNACCS_TEANguoiXNK> listNguoiXNK = new List<KDT_VNACCS_TEANguoiXNK>();
                        listNguoiXNK = KDT_VNACCS_TEANguoiXNK.SelectCollectionDynamic(query, "ID");
                        foreach (KDT_VNACCS_TEANguoiXNK nguoiXNK in listNguoiXNK)
                        {
                            TEA.NguoiXNKCollection.Add(nguoiXNK);
                        }
                        return TEA;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return null;
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                if (id != 0)
                {
                    VNACC_TEAForm f = new VNACC_TEAForm();
                    KDT_VNACCS_TEA TEA =getTEA(id);
                    f.TEA = TEA;
                    f.ShowDialog();
                    btnTimKiem_Click(null, null);
                }
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

               
                string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                string maHQ = ctrMaHaiQuan.Code;
                where = String.Empty;
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                if (loaiHinh != "")
                    where = where + " and PhanLoaiXuatNhapKhau='" + loaiHinh + "' ";
                if(maHQ!="")
                    where = where + " and CoQuanHaiQuan='" + maHQ + "' ";
                
                //if(clcNgayTiepNhan.Text!="" && )

                listTEA.Clear();
                listTEA = KDT_VNACCS_TEA.SelectCollectionDynamic(where, "ID");

                grList.DataSource = listTEA;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }

        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách tờ khai đăng ký danh mục miễn thuế_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách hàng hóa không ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grList.Tables[0].Columns.Add(new GridEXColumn("MoTaHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Mô tả hàng hóa " });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuongDangKyMT", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng đăng ký" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DVTSoLuongDangKyMT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoLuongDaSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng đã dùng" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("DVTSoLuongDaSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("TriGiaDuKien", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá dự kiến" });

                        grList.DataSource = KDT_VNACCS_TEA.SelectDynamicFull(where, "ID").Tables[0];
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        grList.Tables[0].Columns.Remove("MoTaHangHoa");
                        grList.Tables[0].Columns.Remove("SoLuongDangKyMT");
                        grList.Tables[0].Columns.Remove("DVTSoLuongDangKyMT");
                        grList.Tables[0].Columns.Remove("SoLuongDaSuDung");
                        grList.Tables[0].Columns.Remove("DVTSoLuongDaSuDung");
                        grList.Tables[0].Columns.Remove("TriGia");
                        grList.Tables[0].Columns.Remove("TriGiaDuKien");
                        grList.Refresh();
                        btnTimKiem_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

        private void cbbPhanLoaiXuatNhap_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        
    }
}
