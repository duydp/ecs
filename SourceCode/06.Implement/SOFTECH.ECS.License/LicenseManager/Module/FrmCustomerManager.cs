using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmCustomerManager : DevExpress.XtraEditors.XtraForm
    {
        public FrmCustomerManager()
        {
            InitializeComponent();
        }

        private void FrmCustomerManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Cust_New"))
            {
                FrmCustomerInfo obj = new FrmCustomerInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Cust_New"].Activate();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa khách hàng: " + ID;
                    if (MessageBox.Show(Message, "Xóa khách hàng", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Customer.DeleteCustomer(ID);
                    }
                }
            }
            this.RefreshData();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridCustomer_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

        #region Adding Method
        private void DisplayGridHeader()
        {
            gridData.Columns["ID"].Caption = "Mã khách hàng";
            gridData.Columns["Name"].Caption = "Tên khách hàng";
            gridData.Columns["ContactFirstName"].Caption = "Tên";
            gridData.Columns["ContactLastName"].Caption = "Họ";
            gridData.Columns["CompanyOrDepartment"].Caption = "Công ty / Bộ phận";
            gridData.Columns["BillingAddress"].Caption = "Địa chỉ";
            gridData.Columns["City"].Caption = "Tỉnh / Thành phố";
            gridData.Columns["StateOrProvince"].Caption = "Quận / Huyện";
            gridData.Columns["PostalCode"].Caption = "Mã vùng";
            gridData.Columns["ContactTitle"].Caption = "Chức danh";
            gridData.Columns["PhoneNumber"].Caption = "Số điện thoại";
            gridData.Columns["FaxNumber"].Caption = "Số Fax:";
            gridData.Columns["Email"].Caption = "Thư điện tử";
            gridData.Columns["Notes"].Caption = "Thông tin thêm";
        }

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Cust_" + id))
            {
                FrmCustomerInfo obj = new FrmCustomerInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Cust_" + id].Activate();
            }
        }

        public void RefreshData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(Customer.SelectAll().Tables[0].Copy());
            ds.Tables[0].TableName = "tblCustomer";
            ds.Tables.Add(this.tblInvoice());
            ds.Tables.Add(this.tblInvoiceDetail());
            ds.Tables.Add(this.tblEmployee());
            ds.Tables.Add(this.tblProduct());
            ds.Tables.Add(this.tblLicense());

            try
            {
                ds.Relations.Add("Hóa đơn", ds.Tables["tblCustomer"].Columns["ID"],
                    ds.Tables["tblInvoice"].Columns["CustomerID"]);

                ds.Tables["tblInvoice"].ChildRelations.Add(
                        "Chi tiết", ds.Tables["tblInvoice"].Columns["ID"], ds.Tables["tblInvoiceDetail"].Columns["InvoiceID"]
                    );
                ds.Tables["tblInvoice"].ChildRelations.Add(
                        "Người tạo", ds.Tables["tblInvoice"].Columns["EmployeeID"], ds.Tables["tblEmployee"].Columns["ID"], false
                    );
                ds.Tables["tblInvoiceDetail"].ChildRelations.Add(
                        "Sản phẩm", ds.Tables["tblInvoiceDetail"].Columns["ProductID"], ds.Tables["tblProduct"].Columns["ID"], false
                    );
                ds.Tables["tblInvoiceDetail"].ChildRelations.Add(
                        "Bản quyền", ds.Tables["tblInvoiceDetail"].Columns["LicenseID"], ds.Tables["tblLicense"].Columns["ID"], false
                    );
            }
            catch { }
            gridCustomer.DataSource = ds.Tables["tblCustomer"];
            this.DisplayGridHeader();
            gridData.Columns["StateOrProvince"].Group();
        }

        private DataTable tblEmployee()
        {
            DataTable dt = Employee.SelectAll().Tables[0].Copy();
            dt.Columns.Remove("UserName");
            dt.Columns.Remove("PassWord");
            dt.Columns["ID"].Caption = "Mã nhân viên";
            dt.Columns["FirstName"].Caption = "Tên";
            dt.Columns["LastName"].Caption = "Họ";
            dt.Columns["Department"].Caption = "Bộ phận";
            dt.Columns["Title"].Caption = "Chức danh";
            dt.Columns["Email"].Caption = "Thư điện tử";
            dt.TableName = "tblEmployee";

            return dt;
        }

        private DataTable tblInvoice()
        {
            DataTable dt = Invoice.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã sản phẩm";
            dt.Columns["CustomerID"].Caption = "Mã khách hàng";
            dt.Columns["EmployeeID"].Caption = "Mã nhân viên";
            dt.Columns["CreateDate"].Caption = "Ngày tạo";
            dt.Columns["Notes"].Caption = "Thông tin khác";

            dt.TableName = "tblInvoice";
            return dt;
        }

        private DataTable tblProduct()
        {
            DataTable dt = Product.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã sản phẩm";
            dt.Columns["Name"].Caption = "Tên sản phẩm";
            dt.Columns["Description"].Caption = "Mô tả";
            dt.TableName = "tblProduct";

            return dt;
        }

        private DataTable tblInvoiceDetail()
        {
            DataTable dt = InvoiceDetail.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã hóa đơn";
            dt.Columns["ProductID"].Caption = "Mã sản phẩm";
            dt.Columns["LicenseID"].Caption = "Mã bản quyền";
            dt.Columns["SerialCode"].Caption = "Serial";
            dt.Columns["PaymentTerms"].Caption = "Thời hạn";
            dt.Columns["Price"].Caption = "Giá thành";

            dt.TableName = "tblInvoiceDetail";

            return dt;
        }

        private DataTable tblLicense()
        {
            DataTable dt = Activation.Server.License.SelectAll().Tables[0].Copy();
            dt.Columns["ID"].Caption = "Mã bản quyền";
            dt.Columns["Name"].Caption = "Tên bản quyền";
            dt.Columns["Description"].Caption = "Mô tả";
            dt.Columns["NumberClient"].Caption = "Số máy";
            dt.Columns["Term"].Caption = "Thời hạn";
            dt.TableName = "tblLicense";

            return dt;
        }

        #endregion

    }
}