﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Threading;

namespace Company.Interface.Report.GC
{
    public partial class PhuLucToKhaiGCCT_G_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangChuyenTiep> HCTCollection = new List<HangChuyenTiep>();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public long SoToKhai;
        public DateTime NgayDangKy;
        // public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewGCCTTQ_Giao_TT196Form report;
        public bool inMaHang = false;
        public PhuLucToKhaiGCCT_G_TT196()
        {
            InitializeComponent();
        }

        //dinh dang tri gia nguyen te
        string FormatNumber(object obj)
        {
            return Helpers.FormatNumeric(obj, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);

        }

        public void BindReport(string pls)
        {

            string loaihinh = TKCT.MaLoaiHinh.Substring(0, 1);
            if (loaihinh == "X")
                lblLoaiPhuLuc.Text = "Giao gia công chuyển tiếp";
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            DateTime minDate = new DateTime(1900, 1, 1);
           //Chi cuc hai quan
            lblChiCucHaiQuan.Text = TKCT.MaHaiQuanTiepNhan + " - " + DonViHaiQuan.GetName(TKCT.MaHaiQuanTiepNhan);
            lblChiCucHaiQuan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));



            //So to khai
            if (this.TKCT.SoToKhai > 0)
            {
                lblSoToKhai.Text = this.TKCT.SoToKhai + "";

            }
            else
            {
                lblSoToKhai.Text = "";
                labelTQDT.Image = null;
            }

            //Ngay dang ký
            if (this.TKCT.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKCT.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";


            //Loai hinh
            string stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKCT.MaLoaiHinh);
            lblLoaiHinh.Text = TKCT.MaLoaiHinh + "-" + stlh;

            // phu luc so
            lblPhuLucSo.Text = pls;

            #region Hang hoa

            if (inMaHang)
            {
                foreach (HangChuyenTiep item in HCTCollection)
                {
                    item.TenHang = item.TenHang + "/ " + item.MaHang;
                }
            }
            while (HCTCollection.Count < 20)
            {
                HCTCollection.Add(new HangChuyenTiep { SoThuTuHang = HCTCollection[HCTCollection.Count - 1].SoThuTuHang + 1 });
            }



            DetailHangHoa.DataSource = HCTCollection;
            lblSTTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
            lblMaHangHoa.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
            lblDVT.DataBindings.Add("Text", DetailHangHoa.DataSource, "ID_DVT");
            lblSoLuong.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan.LuongSP));
            lblDonGia.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGia", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.DonGiaNT));
            lblTriGia.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.TriGiaNT));
          
            #endregion
            //16. Cong
            foreach (HangChuyenTiep hct in HCTCollection)
            {
                tongTriGiaNT += Math.Round(hct.TriGia, 2, MidpointRounding.AwayFromZero);
            }
            lblTongTriGia.Text = tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));
        }


        private bool IsHaveTax()
        {
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu) > 0) return true;
            return false;
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;
            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text);
        }

        private void lblTriGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell  = (XRTableCell)sender;
            if (cell != null && cell.Text == "0")
                cell.Text = string.Empty;
        }
    }
}
