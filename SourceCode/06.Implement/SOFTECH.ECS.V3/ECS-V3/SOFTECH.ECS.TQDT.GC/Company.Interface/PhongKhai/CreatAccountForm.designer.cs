﻿namespace Company.Interface.PhongKhai
{
    partial class CreatAccountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreatAccountForm));
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRePass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            this.btnCreatAccount = new Janus.Windows.EditControls.UIButton();
            this.txtPass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblHopDong = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnEditAccount = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.cmdDelDN = new Janus.Windows.EditControls.UIButton();
            this.btnEdit = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Controls.Add(this.btnEdit);
            this.grbMain.Controls.Add(this.cmdDelDN);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnCancel);
            this.grbMain.Controls.Add(this.btnEditAccount);
            this.grbMain.Controls.Add(this.uiGroupBox11);
            this.grbMain.Controls.Add(this.btnCreatAccount);
            this.grbMain.Size = new System.Drawing.Size(581, 356);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên Doanh Nghiệp";
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(182, 47);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(277, 21);
            this.txtTenDoanhNghiep.TabIndex = 3;
            this.txtTenDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Khẳng định Mật Khẩu";
            // 
            // txtRePass
            // 
            this.txtRePass.Location = new System.Drawing.Point(182, 103);
            this.txtRePass.Name = "txtRePass";
            this.txtRePass.PasswordChar = '*';
            this.txtRePass.Size = new System.Drawing.Size(277, 21);
            this.txtRePass.TabIndex = 7;
            this.txtRePass.VisualStyleManager = this.vsmMain;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCancel.Icon")));
            this.btnCancel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnCancel.Location = new System.Drawing.Point(504, 332);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(60, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCancel.VisualStyleManager = this.vsmMain;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreatAccount
            // 
            this.btnCreatAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreatAccount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreatAccount.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCreatAccount.Icon")));
            this.btnCreatAccount.Location = new System.Drawing.Point(12, 332);
            this.btnCreatAccount.Name = "btnCreatAccount";
            this.btnCreatAccount.Size = new System.Drawing.Size(63, 23);
            this.btnCreatAccount.TabIndex = 1;
            this.btnCreatAccount.Text = "Tạo";
            this.btnCreatAccount.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCreatAccount.VisualStyleManager = this.vsmMain;
            this.btnCreatAccount.Click += new System.EventHandler(this.btnCreatAccount_Click);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(182, 76);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(277, 21);
            this.txtPass.TabIndex = 5;
            this.txtPass.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(182, 20);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(277, 21);
            this.txtMaDoanhNghiep.TabIndex = 1;
            this.txtMaDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // lblHopDong
            // 
            this.lblHopDong.AutoSize = true;
            this.lblHopDong.BackColor = System.Drawing.Color.Transparent;
            this.lblHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopDong.Location = new System.Drawing.Point(54, 81);
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.Size = new System.Drawing.Size(52, 13);
            this.lblHopDong.TabIndex = 4;
            this.lblHopDong.Text = "Mật Khẩu";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(54, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã Doanh Nghiệp";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaDoanhNghiep;
            this.rfvMa.ErrorMessage = "\"Mã doanh nghiệp\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenDoanhNghiep;
            this.rfvTen.ErrorMessage = "\"Tên Doanh Nghiệp\" bắt buộc phải nhập.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.label2);
            this.uiGroupBox11.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox11.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox11.Controls.Add(this.label1);
            this.uiGroupBox11.Controls.Add(this.label27);
            this.uiGroupBox11.Controls.Add(this.txtRePass);
            this.uiGroupBox11.Controls.Add(this.lblHopDong);
            this.uiGroupBox11.Controls.Add(this.txtPass);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(12, 6);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(552, 131);
            this.uiGroupBox11.TabIndex = 0;
            this.uiGroupBox11.Text = "Thông tin Tài Khoản Doanh Nghiệp";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // btnEditAccount
            // 
            this.btnEditAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditAccount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAccount.Icon = ((System.Drawing.Icon)(resources.GetObject("btnEditAccount.Icon")));
            this.btnEditAccount.Location = new System.Drawing.Point(418, 332);
            this.btnEditAccount.Name = "btnEditAccount";
            this.btnEditAccount.Size = new System.Drawing.Size(80, 23);
            this.btnEditAccount.TabIndex = 5;
            this.btnEditAccount.Text = "Quản trị";
            this.btnEditAccount.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnEditAccount.VisualStyleManager = this.vsmMain;
            this.btnEditAccount.Click += new System.EventHandler(this.btnEditAccount_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 151);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(552, 175);
            this.uiGroupBox1.TabIndex = 7;
            this.uiGroupBox1.Text = "Danh sách Tài khoản Doanh Nghiệp";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(546, 155);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            // 
            // cmdDelDN
            // 
            this.cmdDelDN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdDelDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDelDN.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDelDN.Icon")));
            this.cmdDelDN.Location = new System.Drawing.Point(147, 332);
            this.cmdDelDN.Name = "cmdDelDN";
            this.cmdDelDN.Size = new System.Drawing.Size(60, 23);
            this.cmdDelDN.TabIndex = 3;
            this.cmdDelDN.Text = "Xóa";
            this.cmdDelDN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdDelDN.VisualStyleManager = this.vsmMain;
            this.cmdDelDN.Click += new System.EventHandler(this.cmdDelDN_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Icon = ((System.Drawing.Icon)(resources.GetObject("btnEdit.Icon")));
            this.btnEdit.Location = new System.Drawing.Point(81, 332);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 23);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnEdit.VisualStyleManager = this.vsmMain;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(311, 332);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(101, 23);
            this.uiButton1.TabIndex = 4;
            this.uiButton1.Text = "Cấu hình HQ";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(213, 332);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(92, 23);
            this.uiButton2.TabIndex = 8;
            this.uiButton2.Text = "Đổi mã PIN";
            this.uiButton2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // CreatAccountForm
            // 
            this.ClientSize = new System.Drawing.Size(581, 356);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreatAccountForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tạo Tài Khoản Doanh Nghiệp";
            this.Load += new System.EventHandler(this.DangKyForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DangKyForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtRePass;
        private Janus.Windows.EditControls.UIButton btnCancel;
        private Janus.Windows.EditControls.UIButton btnCreatAccount;
        private Janus.Windows.GridEX.EditControls.EditBox txtPass;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label lblHopDong;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.ToolTip toolTip1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIButton btnEditAccount;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton cmdDelDN;
        private Janus.Windows.EditControls.UIButton btnEdit;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton uiButton2;
    }
}
