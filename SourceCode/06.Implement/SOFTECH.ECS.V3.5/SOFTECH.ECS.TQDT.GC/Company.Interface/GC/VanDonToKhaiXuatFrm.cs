﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using Infragistics.Excel;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.Utils;
using Infragistics.Excel;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Infragistics.Excel;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
#endif

namespace Company.Interface.GC
{
    public partial class VanDonToKhaiXuatFrm : BaseForm
    {
        private DataTable dt;
        public HopDong HD;

        public VanDonToKhaiXuatFrm()
        {
            InitializeComponent();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
//             if (cboLoaiHangHoa.SelectedValue == null)
//                 return;
// 
//             SelectHangForm f = new SelectHangForm();
//             f.LoaiHangHoa = cboLoaiHangHoa.SelectedValue.ToString();
//             f.ShowDialog(this);
// 
//             try
//             {
//                 dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo<HangMauDich>(f.HangCollectionSelected);
// 
//                 dgList.DataSource = dt;
// 
//                 dt.RowChanging += new DataRowChangeEventHandler(ds_RowChanging);
// 
//             }
//             catch (Exception ex)
//             {
//                 ShowMessage("Lỗi: " + ex.Message, false);
//             }
        }

        private void ChuyenMaHS08So_Load(object sender, EventArgs e)
        {
            //uiTab1.Visible = false;

            Cursor = Cursors.WaitCursor;

            //Tab theo ma
            //cboLoaiHangHoa.SelectedIndex = 0;
           // CreateColumnCombo(dgList);

            //Tab Excel
            //cboLoaiHangHoa2.SelectedIndex = 0;
            //CreateColumnCombo(dgList2);
            dt = VanDonToKhaiXuat.SelectToKhai(HD.ID);
            dgList.DataSource = dt;


            Cursor = Cursors.Default;
        }

        void ds_RowChanging(object sender, DataRowChangeEventArgs e)
        {
//             if (e.Action == DataRowAction.Add || e.Action == DataRowAction.Change)
//             {
//                 if (e.Row["MaHSMoi"].ToString() == "")
//                 {
//                     e.Row.SetColumnError("MaHSMoi", "Mã HS không được rỗng");
//                 }
//                 else
//                 {
//                     e.Row.SetColumnError("MaHSMoi", "");
//                 }
//             }
        }

        public void CreateColumnCombo(Janus.Windows.GridEX.GridEX grid)
        {
            //System.Data.DataTable dt = MaHS.SelectAll();

            //Create multiplevalues column

            //When using a many-to-manyn relation, the DataMember of the MultipleValues
            //column must be the name of the relation between the parent table and the 
            //relation table.

//             //Janus.Windows.GridEX.GridEXColumn col = new Janus.Windows.GridEX.GridEXColumn();
//             Janus.Windows.GridEX.GridEXColumn col = grid.RootTable.Columns["MaHSMoi"];
//             col.EditType = Janus.Windows.GridEX.EditType.Combo;
//             col.Caption = "Mã HS 8 số";
//             col.Width = 100;
//             //Since the column will be bound to a list containing DataRowView objects, we must specify which
//             //field in the DataRowView will be used as value 
//             col.MultipleValueDataMember = "HS10SO";
// 
//             //Fill the ValueList with the categories table
//             col.HasValueList = true;
//             col.ValueList.PopulateValueList(dtHS.DefaultView, "HS10SO", "HS10SO");
//             col.DefaultGroupInterval = Janus.Windows.GridEX.GroupInterval.Text;
//             col.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
          //  e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            if(e.Row.Cells["MaLoaiHinh"].Text.Contains("V"))
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhaiVNACCS"].Text.ToString();
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
            if (dt.HasErrors) return;
            DataTable dtSave = dt.GetChanges();
            if (save(dtSave)) ShowMessage("Thực hiện thành công", false);
            else
                ShowMessage("Lưu dữ liệu không thành công", false);


        }
        private bool save(DataTable dt)
        {
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    VanDonToKhaiXuat vdtkx = new VanDonToKhaiXuat();
                    vdtkx.TKMD_ID = Convert.ToInt32(dr["TKMD_ID"]);
                    vdtkx.SoVanDon = dr["SoVanDon"].ToString();
                    vdtkx.NgayVanDon = Convert.ToDateTime(dr["NgayVanDon"]);
                    vdtkx.IDHopDong = HD.ID;
                    vdtkx.InsertUpdate();
                }
                return true;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
           
        }
      

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave2_Click(object sender, EventArgs e)
        {
            
        }

      

     

       



    }
}
