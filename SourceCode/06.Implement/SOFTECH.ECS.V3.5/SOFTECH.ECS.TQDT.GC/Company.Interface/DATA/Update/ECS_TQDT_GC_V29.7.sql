IF OBJECT_ID(N'p_SelectToTalNPLNhapBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectToTalNPLNhapBy_HD_ID
IF OBJECT_ID(N'p_SelectToTalNPLXuatBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectToTalNPLXuatBy_HD_ID
IF OBJECT_ID(N'p_SelectNPLNhapQuyetToanBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectNPLNhapQuyetToanBy_HD_ID
IF OBJECT_ID(N'p_SelectNPLXuatTheoDinhMucBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectNPLXuatTheoDinhMucBy_HD_ID
IF OBJECT_ID(N'p_SelectToTalNPLNhapBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectToTalNPLNhapBy_HD_ID
IF OBJECT_ID(N'p_SelectToTalNPLNhapBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectToTalNPLNhapBy_HD_ID
IF OBJECT_ID(N'p_SelectToTalNPLNhapBy_HD_ID') IS NOT NULL
	DROP PROC p_SelectToTalNPLNhapBy_HD_ID
GO
	ALTER VIEW [dbo].[t_View_LuongNPLTrongTKXuat]      
AS      
    SELECT  CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'      
                 THEN t_KDT_ToKhaiMauDich.LoaiVanDon      
                 ELSE CONVERT(VARCHAR(12), t_KDT_ToKhaiMauDich.SoToKhai)      
            END AS SoToKhaiVNACCS ,
			TrangThaiXuLy,      
            dbo.t_KDT_ToKhaiMauDich.MaHaiQuan ,      
            dbo.t_KDT_ToKhaiMauDich.SoToKhai ,      
            dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh ,      
   dbo.t_KDT_ToKhaiMauDich.NgayDangKy ,      
            dbo.t_KDT_HangMauDich.MaPhu AS MaSP ,      
            dbo.t_KDT_HangMauDich.TenHang ,      
      dbo.t_KDT_HangMauDich.DVT_ID,      
            dbo.t_KDT_HangMauDich.SoLuong ,      
            dbo.t_GC_DinhMuc.MaNguyenPhuLieu ,      
            dbo.t_GC_NguyenPhuLieu.Ten ,      
            dbo.t_KDT_HangMauDich.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung      
                                              / 100      
                                              * dbo.t_GC_DinhMuc.TyLeHaoHut      
                                              + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS LuongNPL ,    
   TriGiaTT,    
   DonGiaTT,      
            dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep ,      
            dbo.t_KDT_ToKhaiMauDich.IDHopDong ,      
            dbo.t_KDT_ToKhaiMauDich.ID ,      
            dbo.t_GC_DinhMuc.DinhMucSuDung ,      
            dbo.t_GC_DinhMuc.TyLeHaoHut ,      
            YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK      
    FROM    dbo.t_KDT_ToKhaiMauDich      
            INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID      
            INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu      
                                           AND dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_ToKhaiMauDich.IDHopDong      
            INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID      
                                                 AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma      
    WHERE   ( dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' AND t_KDT_ToKhaiMauDich.TrangThaiXuLy NOT IN (10,11) AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XV%'  )  
    
    

	GO
CREATE PROCEDURE p_SelectToTalNPLNhapBy_HD_ID          
 @HopDong_ID BIGINT,          
 @NamQuyetToan INT          
 AS     
 BEGIN  
 SELECT  
MaNPL,  
SUM(LUONGNHAP) AS LUONGNHAP,  
SUM(TRIGIANHAP) AS TRIGIANHAP  
FROM  
(    
SELECT MaNPL ,    
 T.TenNPL,    
 T.DVT_ID AS DVT,    
SUM(LuongNhap) AS LUONGNHAP,    
SUM(TriGiaNhap) AS TRIGIANHAP    
FROM    
(    
SELECT TABLETEMP.MaPhu AS MaNPL,    
    TABLETEMP.TenHang AS TenNPL,    
    TABLETEMP.DVT_ID,    
 SUM(TABLETEMP.SoLuong) AS LuongNhap ,    
 SUM(TABLETEMP.TriGiaTT) AS TriGiaNhap    
FROM    
(    
SELECT  tkmd.ID ,    
        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon    
             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)    
        END AS SoToKhaiVNACCS ,    
        tkmd.SoToKhai ,    
        tkmd.SoTiepNhan ,    
        tkmd.MaLoaiHinh ,    
        tkmd.NgayDangKy ,    
        tkmd.NgayTiepNhan ,    
  tkmd.IDHopDong,    
        hmd.MaPhu ,    
  hmd.TenHang,    
  hmd.DVT_ID,    
        hmd.SoLuong,    
  hmd.TriGiaTT    
FROM    t_KDT_ToKhaiMauDich tkmd    
        INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID    
WHERE   --hmd.MaPhu = 'NLC001'    
        tkmd.MaLoaiHinh LIKE 'N%'    
        AND tkmd.LoaiHangHoa = 'N'    
        AND tkmd.IDHopDong = @HopDong_ID    
        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )    
  AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan    
  ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.DVT_ID --ORDER BY TABLETEMP.MaPhu    
  UNION ALL    
    SELECT TABLETEMP.MaPhu AS MaNPL,    
        TABLETEMP.TenHang AS TenNPL,    
    TABLETEMP.ID_DVT,    
 SUM(TABLETEMP.SoLuong) AS LuongNhap ,    
 SUM(TABLETEMP.TriGiaTT) AS TriGiaNhap    
FROM    
(    
                SELECT  tkmd.ID ,    
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'    
                             THEN tkmd.Huongdan_PL    
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)    
                        END AS SoToKhaiVNACCS ,    
                        tkmd.SoToKhai ,    
                        tkmd.SoTiepNhan ,    
                        tkmd.MaLoaiHinh ,    
                        tkmd.NgayDangKy ,    
                        tkmd.NgayTiepNhan ,    
      tkmd.IDHopDong,    
                        hmd.MaHang AS MaPhu ,    
      hmd.TenHang,    
      hmd.ID_DVT,    
                        hmd.SoLuong,    
      hmd.TriGiaTT    
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd    
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID    
                WHERE       
                        ( tkmd.MaLoaiHinh LIKE '%PL%'    
                              OR tkmd.MaLoaiHinh LIKE '%18%'    
                              OR tkmd.MaLoaiHinh LIKE '%NV%'    
                            )    
                        AND tkmd.IDHopDong = @HopDong_ID    
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )    
      AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan    
      ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.ID_DVT --ORDER BY TABLETEMP.MaPhu    
) T GROUP BY MaNPL,T.TenNPL,T.DVT_ID     
) TB GROUP BY TB.MaNPL ORDER BY TB.MaNPL      
 END    
  
GO
GO
 CREATE PROCEDURE p_SelectToTalNPLXuatBy_HD_ID                
 @HopDong_ID BIGINT,                
 @NamQuyetToan INT                
 AS           
 BEGIN      
 SELECT       
MaNPL,      
SUM(TB.LUONGXUAT) AS LUONGXUAT,      
SUM(TB.TRIGIAXUAT) AS TRIGIAXUAT      
FROM      
(          
SELECT MaNPL ,          
T.TenNPL,          
T.DVT,          
SUM(LuongXuat) AS LUONGXUAT,          
SUM(TriGiaXuat) AS TRIGIAXUAT          
FROM          
(          
  SELECT TABLETEMP.MaPhu AS MaNPL,          
  TABLETEMP.TenHang AS TenNPL,          
  TABLETEMP.DVT_ID  AS DVT,          
 SUM(TABLETEMP.SoLuong) AS LuongXuat,          
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat          
FROM          
(          
        SELECT  tkmd.ID ,          
                tkmd.SoToKhai ,          
                CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon          
                     ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)          
                END AS SoToKhaiVNACCS ,          
                tkmd.SoTiepNhan ,          
                tkmd.MaLoaiHinh ,          
                tkmd.NgayDangKy ,          
                tkmd.NgayTiepNhan ,          
                hmd.MaPhu ,          
    hmd.TenHang,          
    hmd.DVT_ID,          
                hmd.SoLuong,          
    hmd.TriGiaTT          
        FROM    t_KDT_ToKhaiMauDich tkmd          
                INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID          
        WHERE             
                tkmd.MaLoaiHinh LIKE 'X%'          
                AND tkmd.LoaiHangHoa = 'N'          
                AND tkmd.IDHopDong = @HopDong_ID          
                AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )          
    AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan          
    ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.DVT_ID --ORDER BY TABLETEMP.MaPhu          
    UNION ALL          
  SELECT TABLETEMP.MaPhu AS MaNPL,          
  TABLETEMP.TenHang AS TenNPL,          
  TABLETEMP.ID_DVT AS DVT,          
 SUM(TABLETEMP.SoLuong) AS LuongXuat,          
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat          
FROM          
(          
                SELECT  tkmd.ID ,          
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'          
                             THEN tkmd.Huongdan_PL          
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)          
                        END AS SoToKhaiVNACCS ,          
                        tkmd.SoToKhai ,          
                        tkmd.SoTiepNhan ,          
                        tkmd.MaLoaiHinh ,          
                        tkmd.NgayDangKy ,          
                        tkmd.NgayTiepNhan ,          
                        hmd.MaHang AS MaPhu ,          
      hmd.TenHang,          
      hmd.ID_DVT,          
                        hmd.SoLuong,          
      hmd.TriGiaTT          
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd          
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID          
                WHERE             
                        ( tkmd.MaLoaiHinh LIKE '%PL%'          
                              OR tkmd.MaLoaiHinh LIKE '%18%'          
                              OR tkmd.MaLoaiHinh LIKE '%XV%'          
                            )          
                        AND tkmd.IDHopDong = @HopDong_ID          
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )          
      AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan          
      ) TABLETEMP GROUP BY TABLETEMP.MaPhu,TABLETEMP.TenHang,TABLETEMP.ID_DVT --ORDER BY TABLETEMP.MaPhu          
      UNION ALL          
  SELECT TABLETEMP.MaNguyenPhuLieu AS MaNPL,          
  TABLETEMP.Ten AS TenNPL,        
  TABLETEMP.DVT,           
 SUM(TABLETEMP.LuongNPL) AS LuongXuat,          
 SUM(TABLETEMP.TriGiaTT) AS TriGiaXuat          
FROM          
(          
                         SELECT SoToKhaiVNACCS ,        
                                MaHaiQuan ,        
   SoToKhai ,        
                                MaLoaiHinh ,        
                                MaSP ,        
                                TenHang ,        
       DVT_ID ,        
                                SoLuong ,        
                                MaNguyenPhuLieu ,        
                                Ten ,        
         CASE WHEN Ten IS NOT NULL THEN (SELECT TOP 1 DVT_ID FROM dbo.t_GC_NguyenPhuLieu WHERE Ten=NPL.Ten) ELSE DVT_ID END  AS DVT,         
                                LuongNPL ,        
                                0 AS TriGiaTT,        
                                MaDoanhNghiep ,        
                                IDHopDong ,      
                                DinhMucSuDung ,        
                                TyLeHaoHut ,        
                                NamDK        
                 
                         FROM   t_View_LuongNPLTrongTKXuat NPL        
                         WHERE  IDHopDong = @HopDong_ID           
       AND NamDK = @NamQuyetToan          
       ) TABLETEMP GROUP BY TABLETEMP.MaNguyenPhuLieu,TABLETEMP.Ten,TABLETEMP.DVT --ORDER BY MaNguyenPhuLieu          
       ) T GROUP BY MaNPL,T.TenNPL,T.DVT               
    ) TB GROUP BY TB.MaNPL ORDER BY TB.MaNPL      
 END       
    
 GO
 GO
 CREATE PROCEDURE p_SelectNPLNhapQuyetToanBy_HD_ID            
 @HopDong_ID BIGINT,            
 @NamQuyetToan INT            
 AS       
 BEGIN    
 SELECT  
 TABLETEPM.SoToKhai,  
 TABLETEPM.SoToKhaiVNACCS,  
 TABLETEPM.MaLoaiHinh,  
 TABLETEPM.NgayDangKy,  
 TABLETEPM.MaPhu,  
 TABLETEPM.TenHang,  
 CASE WHEN TABLETEPM.DVT_ID <>0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID = TABLETEPM.DVT_ID) ELSE TABLETEPM.DVT_ID END AS DVT,  
 TABLETEPM.DonGiaTT,  
 TABLETEPM.SoLuong,  
 TABLETEPM.TriGiaTT  
 FROM  
 (  
SELECT  tkmd.ID ,      
        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon      
             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)      
        END AS SoToKhaiVNACCS ,      
        tkmd.SoToKhai ,      
        tkmd.SoTiepNhan ,      
        tkmd.MaLoaiHinh ,      
        tkmd.NgayDangKy ,      
        tkmd.NgayTiepNhan ,      
  tkmd.IDHopDong,      
        hmd.MaPhu ,      
  hmd.TenHang,      
  hmd.DVT_ID,      
  hmd.SoLuong,   
  hmd.DonGiaTT,     
  hmd.TriGiaTT      
FROM    t_KDT_ToKhaiMauDich tkmd      
        INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID      
WHERE   --hmd.MaPhu = 'NLC001'      
        tkmd.MaLoaiHinh LIKE 'N%'      
        AND tkmd.LoaiHangHoa = 'N'      
        AND tkmd.IDHopDong = @HopDong_ID      
        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )      
  AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan   
  UNION ALL      
     
                SELECT  tkmd.ID ,      
                        CASE WHEN tkmd.MaLoaiHinh LIKE '%V%'      
                             THEN tkmd.Huongdan_PL      
                             ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai)      
                        END AS SoToKhaiVNACCS ,      
                        tkmd.SoToKhai ,      
                        tkmd.SoTiepNhan ,      
                        tkmd.MaLoaiHinh ,      
                        tkmd.NgayDangKy ,      
                        tkmd.NgayTiepNhan ,      
      tkmd.IDHopDong,      
                        hmd.MaHang AS MaPhu ,      
      hmd.TenHang,      
      hmd.ID_DVT,      
                        hmd.SoLuong,  
      hmd.DonGiaTT,      
      hmd.TriGiaTT      
                FROM    t_KDT_GC_ToKhaiChuyenTiep tkmd      
                        INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID      
                WHERE         
                        ( tkmd.MaLoaiHinh LIKE '%PL%'      
                              OR tkmd.MaLoaiHinh LIKE '%18%'      
                              OR tkmd.MaLoaiHinh LIKE '%NV%'      
                            )      
                        AND tkmd.IDHopDong = @HopDong_ID      
                        AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )      
      AND YEAR(tkmd.NgayDangKy) = @NamQuyetToan  
   ) TABLETEPM ORDER BY TABLETEPM.MaPhu,TABLETEPM.NgayDangKy  
 END      
    
 GO
 
 CREATE PROCEDURE p_SelectNPLXuatTheoDinhMucBy_HD_ID                
 @HopDong_ID BIGINT,                
 @NamQuyetToan INT                
 AS           
 BEGIN      
 SELECT    
 TABLETEMP.SoToKhai,    
 TABLETEMP.SoToKhaiVNACCS,    
 TABLETEMP.MaLoaiHinh,    
 TABLETEMP.NgayDangKy,    
 TABLETEMP.MaSP,    
 TABLETEMP.TenHang,    
 CASE WHEN TABLETEMP.DVT_ID <>0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID = TABLETEMP.DVT_ID) ELSE TABLETEMP.DVT_ID END AS DVTSP,    
 TABLETEMP.SoLuong,    
 TABLETEMP.DinhMucSuDung,    
 TABLETEMP.TyLeHaoHut,    
 TABLETEMP.LuongNPL,    
 0.00 AS TriGiaXuat,    
 TABLETEMP.MaNguyenPhuLieu,    
 TABLETEMP.Ten,    
 CASE WHEN TABLETEMP.DVT <>0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID = TABLETEMP.DVT_ID) ELSE TABLETEMP.DVT END AS DVTNPL    
 FROM    
 (    
 SELECT SoToKhaiVNACCS ,        
                                MaHaiQuan ,        
                                SoToKhai ,        
                                MaLoaiHinh ,    
        NgayDangKy,        
                                MaSP ,        
                                TenHang ,        
                                DVT_ID ,        
                                SoLuong ,        
                                MaNguyenPhuLieu ,        
                                Ten ,        
         CASE WHEN Ten IS NOT NULL THEN (SELECT TOP 1 DVT_ID FROM dbo.t_GC_NguyenPhuLieu WHERE Ten=NPL.Ten) ELSE DVT_ID END  AS DVT,         
                                LuongNPL ,        
                                TriGiaTT,        
                                MaDoanhNghiep ,        
                                IDHopDong ,      
                                DinhMucSuDung ,        
                                TyLeHaoHut ,        
                                NamDK        
                 
                         FROM   t_View_LuongNPLTrongTKXuat NPL        
                         WHERE  IDHopDong = @HopDong_ID           
       AND NamDK = @NamQuyetToan          
    ) TABLETEMP    
 END 
 GO
 GO
 ALTER PROCEDURE [dbo].[p_GC_SanPham_SelectAll]        
AS        
    
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
SELECT        
 [HopDong_ID],        
 [Ma],        
 [Ten],        
 [MaHS],        
CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID =DVT_ID) ELSE DVT_ID END AS TenDVT,      
 [SoLuongDangKy],        
 [SoLuongDaXuat],        
 [NhomSanPham_ID],        
 [STTHang],        
 [TrangThai],        
 [DonGia]        
FROM        
 [dbo].[t_GC_SanPham]     

GO
------------------------------------------------------------------------------------------------------------------------        
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectAll]        
-- Database: ECS_GC        
-- Author: Ngo Thanh Tung        
-- Time created: Tuesday, September 08, 2009        
------------------------------------------------------------------------------------------------------------------------        
        
ALTER PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectAll]        
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
SELECT        
 [HopDong_ID],        
 [Ma],        
 [Ten],        
 [MaHS],        
CASE WHEN DVT_ID <> 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID =DVT_ID) ELSE DVT_ID END AS TenDVT,         
 [SoLuongDangKy],        
 [SoLuongDaNhap],        
 [SoLuongDaDung],        
 [SoLuongCungUng],        
 [TongNhuCau],        
 [STTHang],        
 [TrangThai],        
 [DonGia]        
FROM        
 [dbo].[t_GC_NguyenPhuLieu]   
 GO
 
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '29.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('29.7',GETDATE(), N'CẬP NHẬT PROCEDURE  BÁO CÁO QUYẾT TOÁN')
END