-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]
	@CodeV4 varchar(50),
	@NameV4 nvarchar(250),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(250),
	@LoaiMapper varchar(50),
	@Notes nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACCS_Mapper]
(
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
)
VALUES 
(
	@CodeV4,
	@NameV4,
	@CodeV5,
	@NameV5,
	@LoaiMapper,
	@Notes
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Update]
	@ID bigint,
	@CodeV4 varchar(50),
	@NameV4 nvarchar(250),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(250),
	@LoaiMapper varchar(50),
	@Notes nvarchar(250)
AS

UPDATE
	[dbo].[t_VNACCS_Mapper]
SET
	[CodeV4] = @CodeV4,
	[NameV4] = @NameV4,
	[CodeV5] = @CodeV5,
	[NameV5] = @NameV5,
	[LoaiMapper] = @LoaiMapper,
	[Notes] = @Notes
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]
	@ID bigint,
	@CodeV4 varchar(50),
	@NameV4 nvarchar(250),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(250),
	@LoaiMapper varchar(50),
	@Notes nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACCS_Mapper] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_Mapper] 
		SET
			[CodeV4] = @CodeV4,
			[NameV4] = @NameV4,
			[CodeV5] = @CodeV5,
			[NameV5] = @NameV5,
			[LoaiMapper] = @LoaiMapper,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACCS_Mapper]
		(
			[CodeV4],
			[NameV4],
			[CodeV5],
			[NameV5],
			[LoaiMapper],
			[Notes]
		)
		VALUES 
		(
			@CodeV4,
			@NameV4,
			@CodeV5,
			@NameV5,
			@LoaiMapper,
			@Notes
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_Mapper] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM [dbo].[t_VNACCS_Mapper] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, April 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]	

GO

