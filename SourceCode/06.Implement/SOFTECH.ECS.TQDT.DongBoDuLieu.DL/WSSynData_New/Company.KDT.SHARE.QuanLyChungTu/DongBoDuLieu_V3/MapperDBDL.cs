﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3
{
    public class MapperDBDL
    {


        public static List<Detail> MapperView(List<SyncDaTaDetail> feebackDeclaration)
        {
            List<Detail> chitiet = new List<Detail>();
            foreach (SyncDaTaDetail item in feebackDeclaration)
            {
                chitiet.Add(new Detail
                {
                    NgayDangKy = Convert.ToDateTime(item.Acceptance),
                    SoTiepNhan = Convert.ToInt32(item.CustomsReference),
                    SoToKhai = Convert.ToInt32(item.Number),
                    MaLoaiHinh = item.NatureOfTransaction,
                    GUIDSTR = item.Reference,
                    MaHaiQuan = item.DeclarationOffice,
                    MaDaiLy = item.Issuer,
                    PhanLuong = item.StreamlineContent,
                    NgayGui = Convert.ToDateTime(item.DateSend),
                    TrangThai = Convert.ToInt32(item.Status),
                });
            }



            return chitiet;
        }
    }
}
