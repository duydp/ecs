﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignRemote;
using System.Threading;
//using iTextSharp.text.pdf;
using System.IO;
//using iTextSharp.text.pdf.security;
using System.Security.Cryptography;
using System.Security;
//using iTextSharp.text.log;
using System.Security.Cryptography.X509Certificates;
//using Org.BouncyCastle.Security;
using Company.KDT.SHARE.Components;
using System.Windows.Forms;
//using iTextSharp.text;
//using Org.BouncyCastle.X509;
using Company.KDT.SHARE.QuanLyChungTu;

namespace ServerRemote
{
    class ProcessFileAPI
    {
        public event EventHandler<SignFileEventArgs> FileSign;
        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        private List<MessageSendFile> _queueMessagesFile = new List<MessageSendFile>();
        public bool _flagSign = false;
        public string FileName;
        public string FileData;
        public System.IO.FileStream fs;
        public byte[] bytes;
        public string filebase64 = "";
        public long filesize = 0;
        public long size;
        public byte[] data;
        public byte[] dataSigned;
        public bool IsAbort = false;
        ISignMessage _service = WebService.SignMessage();
        private bool _isSignLocal = false;
        public float signWidth = 300;
        public float signHeight = 50;
        public float trang = 0;
        public float ViTriKy = 0;
        public float LeNgang = 0;
        public float LeDoc = 1;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private void OnSignFile(SignFileEventArgs e)
        {
            if (FileSign != null)
            {
                FileSign(this, e);
            }
        }        
        public ProcessFileAPI(bool isSignLocal)
        {
            _isSignLocal = isSignLocal;
        }
        public void DoProcessFile()
        {
            if (!_isSignLocal)
                ThreadPool.QueueUserWorkItem(DoGetContentFile);
            else ThreadPool.QueueUserWorkItem(DoGetContentLocalFile);//ThreadPool.QueueUserWorkItem(DoGetContentLocal);

        }
        private void DoGetContentFile(object obj)
        {

            while (!IsAbort)
            {
                try
                {

                    //_service = WebService.SignMessage();
                    _queueMessagesFile.Clear();
                    MessageSendFile msgSend = new MessageSendFile()
                    {
                        From = Globals.UserNameLogin,
                        Password = Globals.PasswordLogin,
                        SendStatus = SendStatusInfo.Request
                    };
                    string msgRequest = _service.ServerSend(Helpers.Serializer(msgSend));
                    msgSend = Helpers.Deserialize<MessageSendFile>(msgRequest);

                    if (msgSend.SendStatus == SendStatusInfo.NoSign || msgSend.SendStatus == SendStatusInfo.NoSignVNACCS)
                    {
                        msgSend.TypeMessages = msgSend.SendStatus == SendStatusInfo.NoSignVNACCS ? "VNACCS" : string.Empty;
                        _queueMessagesFile.Add(msgSend);
                        if (!_flagSign)
                        {
                            DoSignContentFile();
                        }

                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));

                }
                catch (Exception ex)
                {
                    OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            #region DisConnect to server

            MessageSendFile msg = new MessageSendFile()
            {
                From = Globals.UserNameLogin,
                Password = Globals.PasswordLogin,
                SendStatus = SendStatusInfo.ServerLogout
            };
            _service = WebService.SignMessage();
            _service.ServerSend(Helpers.Serializer(msg));
            #endregion
        }
        private void DoGetContentLocalFile(object obj)
        {
            while (!IsAbort)
            {
                try
                {
                    #region Code New
                    _queueMessagesFile.Clear();
                    MSG_FILE_INBOX INBOX = MSG_FILE_INBOX.GetTopDataContent();
                    if (INBOX != null)
                    {
                        MessageSendFile msgSend = new MessageSendFile()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            SendStatus = SendStatusInfo.Request,
                            Message = new SignRemote.MessageFile()
                            {
                                FileName = INBOX.FILE_NAME,
                                FileData = INBOX.FILE_DATA,
                                GuidStr = INBOX.GUIDSTR
                            },
                            TypeMessages = INBOX.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                        };
                        _queueMessagesFile.Add(msgSend);
                        if (!_flagSign)
                        {
                            DoSignContentFile();
                        }
                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));
                    #endregion
                }
                catch (Exception ex)
                {
                    OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        private void DoSignContentFile()
        {
            _flagSign = _queueMessagesFile.Count != 0;
            while (!IsAbort && _flagSign)
            {
                try
                {
                    bool isSignVNACCS = false;
                    int index = 0;
                    _flagSign = true;
                    MessageSendFile msgSend = _queueMessagesFile[index];
                    this._lastStartTime = DateTime.Now;

                    #region SignData
                    if (!isSignVNACCS)
                    {
                        string FilePath = "";
                        string FilePathSiged = "";
                        FileName = msgSend.Message.FileName;
                        FileData = msgSend.Message.FileData;

                        string path = Application.StartupPath + "\\Temp";
                        System.IO.DirectoryInfo di = new DirectoryInfo(path);
                        try
                        {
                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        if (System.IO.Directory.Exists(path) == false)
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }
                        FilePath = path + "\\" + FileName;
                        FilePathSiged = path + "\\" + FileName.Replace(".pdf", "_Signed.pdf");
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                        if (System.IO.File.Exists(FilePathSiged))
                        {
                            System.IO.File.Delete(FilePathSiged);
                        }
                        fs = new System.IO.FileStream(FilePath, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                        bytes = System.Convert.FromBase64String(FileData);
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        X509Certificate2 x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                        if (SoftechSignLibrary.SoftechSign(x509Certificate2,FilePath, FilePathSiged))
                        {
                            _flagSign = true;
                            fs = new System.IO.FileStream(FilePathSiged, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                            size = 0;
                            size = fs.Length;
                            data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            filebase64 = System.Convert.ToBase64String(data);
                            fs.Flush();
                            fs.Close();
                            FileName = FileName.Replace(".pdf", "_Signed.pdf");
                            FileData = filebase64;
                        }
                        //if (SignFile(FilePath, FilePathSiged))
                        //{
                        //    _flagSign = true;
                        //    fs = new System.IO.FileStream(FilePathSiged, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        //    size = 0;
                        //    size = fs.Length;
                        //    data = new byte[fs.Length];
                        //    fs.Read(data, 0, data.Length);
                        //    filebase64 = System.Convert.ToBase64String(data);
                        //    fs.Flush();
                        //    fs.Close();
                        //    FileName = FileName.Replace(".pdf", "_Signed.pdf");
                        //    FileData = filebase64;
                        //}
                    }
                    if (!_isSignLocal)
                    {
                        MessageSendFile msgReturn = new MessageSendFile()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            To = msgSend.From,
                            SendStatus = SendStatusInfo.Send,
                            Message = new SignRemote.MessageFile()
                            {
                                FileName = FileName,
                                FileData = FileData,
                                GuidStr = msgSend.Message.GuidStr
                            }
                        };
                        _service.ServerSend(Helpers.Serializer(msgReturn));
                    }
                    else
                    {
                        #region Code

                        MSG_FILE_OUTBOX MSG_FILE_OUTBOX = new MSG_FILE_OUTBOX();
                        MSG_FILE_OUTBOX.MSG_FROM = msgSend.From;
                        MSG_FILE_OUTBOX.MSG_TO = msgSend.To;
                        MSG_FILE_OUTBOX.GUIDSTR = msgSend.Message.GuidStr;
                        MSG_FILE_OUTBOX.FILE_NAME = FileName;
                        MSG_FILE_OUTBOX.FILE_DATA = FileData;
                        MSG_FILE_OUTBOX.CREATE_TIME = DateTime.Now;
                        MSG_FILE_OUTBOX.InsertUpdate();

                        MSG_FILE_INBOX.DeleteDynamic("GUIDSTR='" + MSG_FILE_OUTBOX.GUIDSTR + "'");
                        #endregion
                    }
                    OnSignFile(new SignFileEventArgs(msgSend, this.TotalUsedTime, null));
                    _queueMessagesFile.RemoveAt(index);
                    if (_queueMessagesFile.Count == 0) _flagSign = false;
                    #endregion
                }
                catch (Exception ex)
                {
                    OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    IsAbort = true;
                }
            }


        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            return string.Format("{0}", name);
        }
        //public void SignPDFNew(String src, String dest,
        // ICollection<Org.BouncyCastle.X509.X509Certificate> chain, X509Certificate2 pk,
        // String digestAlgorithm, CryptoStandard subfilter,
        // String reason, String location,
        // ICollection<ICrlClient> crlList,
        // IOcspClient ocspClient,
        // ITSAClient tsaClient,
        // int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfSignatureAppearance appearance = null;
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        Dictionary<PdfName, int> exclusionSizes = new Dictionary<PdfName, int>();;
        //        // Creating the appearance

        //        X509CertificateParser parser = new X509CertificateParser();
        //        Org.BouncyCastle.X509.X509Certificate[] certChain = new Org.BouncyCastle.X509.X509Certificate[] { parser.ReadCertificate(pk.RawData) };
        //        reader = new PdfReader(src);

        //        appearance = PdfStamper.CreateSignature(reader, new FileStream(dest, FileMode.Create), '\0', null, true).SignatureAppearance;
        //        appearance.SignDate = DateTime.Now;
        //        Font font = new Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false));
        //        font.SetColor(0xff, 0, 0);
        //        appearance.Layer2Font = font;
        //        appearance.Layer2Text = "Ký bởi: " + GetName(pk.SubjectName.Name) + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

        //        float width = reader.GetPageSize(1).Width;
        //        float height = reader.GetPageSize(1).Height;
        //        float yPos = reader.GetPageSize(1).Height - 50;
        //        float xPos = 70.0f;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, null);

        //        PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
        //        {
        //            Date = new PdfDate(appearance.SignDate),
        //            Name = ""
        //        };
        //        if (appearance.Reason != null)
        //        {
        //            signature.Reason = appearance.Reason;
        //        }
        //        if (appearance.Location != null)
        //        {
        //            signature.Location = appearance.Location;
        //        }
        //        appearance.CryptoDictionary = signature;

        //        // insert the next two lines
        //        appearance.Acro6Layers = false;
        //        //appearance.Layer4Text = PdfSignatureAppearance.questionMark;

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        string password = Company.KDT.SHARE.Components.Globals.PasswordSign.ToString();
        //        if (password.Contains("=="))
        //        {
        //            password = Helpers.DecryptString(Company.KDT.SHARE.Components.Globals.PasswordSign.ToString(), "KEYWORD");
        //        }
        //        var secure = new SecureString();
        //        foreach (char c in password)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //public void SignPDF(String src, String dest,
        //         ICollection<Org.BouncyCastle.X509.X509Certificate> chain, X509Certificate2 pk,
        //         String digestAlgorithm, CryptoStandard subfilter,
        //         String reason, String location,
        //         ICollection<ICrlClient> crlList,
        //         IOcspClient ocspClient,
        //         ITSAClient tsaClient,
        //         int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        Dictionary<PdfName, int> exclusionSizes = new Dictionary<PdfName, int>();
        //        reader = new PdfReader(src);
        //        os = new FileStream(dest, FileMode.Create);
        //        stamper = PdfStamper.CreateSignature(reader, os, '\0');
        //        // Creating the appearance
        //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //        //appearance.Reason = reason;
        //        //appearance.Location = location;
        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, "Signed");
        //        //appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
        //        //appearance.Layer2Font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN);
        //        //iTextSharp.text.Font font = FontFactory.GetFont(iTextSharp.text.Font.FontFamily.TIMES_ROMAN.ToString(), 14, BaseColor.RED);
        //        //appearance.Layer2Font = font;
        //        //
        //        #region Comment
        //        //iTextSharp.text.Font font = new iTextSharp.text.Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false), 12);
        //        //font.SetColor(0xff, 0, 0);
        //        //appearance.Layer2Font = font;
        //        //appearance.Layer2Text = "Ký bởi: " + GetName(pk.SubjectName.Name) + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

        //        //float width = reader.GetPageSize(1).Width;
        //        //float height = reader.GetPageSize(1).Height;
        //        //int page = 1;
        //        //if (trang == 1)
        //        //{
        //        //    width = reader.GetPageSize(reader.NumberOfPages).Width;
        //        //    height = reader.GetPageSize(reader.NumberOfPages).Height;
        //        //    page = reader.NumberOfPages;
        //        //}
        //        //float llx = 0f;
        //        //float signHeigh = signHeight;
        //        //if (LeNgang == 1)
        //        //{
        //        //    llx = width / 2f;
        //        //}
        //        //else if (LeNgang == 2)
        //        //{
        //        //    llx = (width - 300f) / 2f;
        //        //}
        //        //if (LeDoc == 0)
        //        //{
        //        //    signHeigh = height;
        //        //}

        //        //float yPos = reader.GetPageSize(1).Height - 50;
        //        //float xPos = 70.0f;
        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, "Signed");

        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(llx, signHeigh, llx + signWidth, signHeigh - signHeight), page, "Signed");
        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(0, signWidth, 0, signHeigh), page, "Signed");
        //        #endregion
        //        Font font = new Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false));
        //        font.SetColor(0xff, 0, 0);
        //        appearance.Layer2Font = font;
        //        appearance.Layer2Text = "Ký bởi: " + GetName(pk.SubjectName.Name) + "\nNgày ký: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

        //        float width = reader.GetPageSize(1).Width;
        //        float height = reader.GetPageSize(1).Height;
        //        float yPos = reader.GetPageSize(1).Height - 50;
        //        float xPos = 70.0f;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, null);


        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, null);
        //        PdfSignature signature = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
        //        {
        //            Date = new PdfDate(appearance.SignDate),
        //            Name = ""
        //        };
        //        if (appearance.Reason != null)
        //        {
        //            signature.Reason = appearance.Reason;
        //        }
        //        if (appearance.Location != null)
        //        {
        //            signature.Location = appearance.Location;
        //        }
        //        appearance.CryptoDictionary = signature;

        //        // insert the next two lines
        //        appearance.Acro6Layers = false;
        //        //appearance.Layer4Text = PdfSignatureAppearance.questionMark;

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        string password = Company.KDT.SHARE.Components.Globals.PasswordSign.ToString();
        //        if (password.Contains("=="))
        //        {
        //            password = Helpers.DecryptString(Company.KDT.SHARE.Components.Globals.PasswordSign.ToString(), "KEYWORD");
        //        }
        //        var secure = new SecureString();
        //        foreach (char c in password)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //public void SignPDF(String src, String dest,
        //                 ICollection<Org.BouncyCastle.X509.X509Certificate> chain, X509Certificate2 pk,
        //                 String digestAlgorithm, CryptoStandard subfilter,
        //                 String reason, String location,
        //                 ICollection<ICrlClient> crlList,
        //                 IOcspClient ocspClient,
        //                 ITSAClient tsaClient,
        //                 int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        reader = new PdfReader(src);
        //        os = new FileStream(dest, FileMode.Create);
        //        stamper = PdfStamper.CreateSignature(reader, os, '\0');
        //        // Creating the appearance
        //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //        //appearance.Reason = reason;
        //        //appearance.Location = location;
        //        float width = reader.GetPageSize(1).Width;
        //        float height = reader.GetPageSize(1).Height;
        //        float yPos = reader.GetPageSize(1).Height - 50;
        //        float xPos = 70.0f;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(xPos, yPos, width - 50, yPos + 50), 1, null);

        //        //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, "Signed");
        //        appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
        //        appearance.Layer2Font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN);

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        string password = Company.KDT.SHARE.Components.Globals.PasswordSign.ToString();
        //        var secure = new SecureString();
        //        foreach (char c in password)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        //RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //private bool SignFile(String src, String dest)
        //{
        //    try
        //    {
        //        #region
        //        LoggerFactory.GetInstance().SetLogger(new SysoLogger());


        //        X509Store x509Store = new X509Store("My");
        //        x509Store.Open(OpenFlags.ReadOnly);
        //        X509Certificate2Collection certificates = x509Store.Certificates;
        //        IList<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();
        //        X509Certificate2 pk = null;
        //        string items = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChuKySo", "");//cbSigns.SelectedValue.ToString();
        //        //ItemSign items = (ItemSign)cbSigns.SelectedValue;
        //        if (certificates.Count > 0)
        //        {
        //            foreach (X509Certificate2 cer in certificates)
        //            {

        //                if (cer.SubjectName.Name == items)
        //                {
        //                    X509Certificate2Collection certificateCollection = new X509Certificate2Collection();
        //                    certificateCollection.Add(cer);
        //                    X509Certificate2Enumerator certificatesEn = certificateCollection.GetEnumerator();
        //                    certificatesEn.MoveNext();
        //                    pk = certificatesEn.Current;

        //                    X509Chain x509chain = new X509Chain();
        //                    x509chain.Build(pk);

        //                    foreach (X509ChainElement x509ChainElement in x509chain.ChainElements)
        //                    {
        //                        chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
        //                    }
        //                }
        //            }
        //        }
        //        x509Store.Close();


        //        IOcspClient ocspClient = new OcspClientBouncyCastle();
        //        ITSAClient tsaClient = null;
        //        for (int i = 0; i < chain.Count; i++)
        //        {
        //            Org.BouncyCastle.X509.X509Certificate cert = chain[i];
        //            String tsaUrl = CertificateUtil.GetTSAURL(cert);
        //            if (tsaUrl != null)
        //            {
        //                tsaClient = new TSAClientBouncyCastle(tsaUrl);
        //                break;
        //            }
        //        }
        //        IList<ICrlClient> crlList = new List<ICrlClient>();
        //        crlList.Add(new CrlClientOnline(chain));
        //        string item = Company.KDT.SHARE.Components.Globals.ChuKySo; ;//cbSigns.SelectedValue.ToString();
        //        //ItemSign item = (ItemSign)cbSigns.SelectedValue;
        //        SignPDFNew(src, dest, chain, pk, DigestAlgorithms.SHA1, CryptoStandard.CMS, "Test",
        // "Ghent",
        // crlList, ocspClient, tsaClient, 0);
        //        return true;
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
    }
}
