using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BKNPLCungUngForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public KDT_SXXK_BKNPLCungUngDangKy BKCungUng = new KDT_SXXK_BKNPLCungUngDangKy();
        private List<KDT_SXXK_BKNPLCungUng> ListBK = new List<KDT_SXXK_BKNPLCungUng>();
        KDT_SXXK_BKNPLCungUng NPL_CunngUng = new KDT_SXXK_BKNPLCungUng();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public BKNPLCungUngForm()
        {
            InitializeComponent();
        }

        private void BKNPLCungUngForm_Load(object sender, EventArgs e)
        {
           
            LoadData();
            SetCommand();
        }
        private void SetCommand()
        {
            cmdHuyBK.Enabled = (BKCungUng.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && BKCungUng.SoTiepNhan > 0) ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            if (BKCungUng.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || BKCungUng.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || BKCungUng.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY )
            {
                cmdThemMoi.Enabled = cmdThemMoi1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnAdd.Enabled = btnXoa.Enabled = false;
                cmdSuaBK.Enabled = cmdSuaBK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
            {
                cmdThemMoi.Enabled = cmdThemMoi1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnAdd.Enabled = btnXoa.Enabled = true;
                cmdSuaBK.Enabled = cmdSuaBK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }
        private void LoadData()
        {
            //BKCungUng = KDT_SXXK_BKNPLCungUngDangKy.LoadBy(HSTL.LanThanhLy);
            if (BKCungUng == null)
                BKCungUng = new KDT_SXXK_BKNPLCungUngDangKy();
            if (BKCungUng.ID == 0)
            {
                BKCungUng.TrangThaiXuLy = -1;
            }
            txtSoTiepNhan.Text = BKCungUng.SoTiepNhan.ToString();
            clcNgayTiepNhan.Value = BKCungUng.NgayTiepNhan;
            
            if (BKCungUng.TrangThaiXuLy == -1)
            {
                lblTrangThai.Text = "Chưa khai báo";
            }
            else if (BKCungUng.TrangThaiXuLy == 0)
            {
                lblTrangThai.Text = "Chờ duyệt";
            }
            else if (BKCungUng.TrangThaiXuLy == 1)
            {
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (BKCungUng.TrangThaiXuLy == 2)
            {
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if(BKCungUng.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                lblTrangThai.Text = "Đang hủy";
            }
            else if (BKCungUng.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                lblTrangThai.Text = "Chờ HQ duyệt hủy";
            }
            else if (BKCungUng.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                lblTrangThai.Text = "Đã hủy";
            }

            dgList2.DataSource = BKCungUng.TKCungUng_List;
            dgList2.Refetch();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                
                if (BKCungUng == null || BKCungUng.ID == 0)
                {
                    BKCungUng = new KDT_SXXK_BKNPLCungUngDangKy();
                    Save();
                }

                int id = this.HSTL.getBKToKhaiXuat();
                DataSet ds = new BKToKhaiXuat().getBKToKhaiXuatSP(this.HSTL.BKCollection[id].ID);
                Company.Interface.SXXK.BangKeThanhLy.DSToKhaiXuatSPForm f = new Company.Interface.SXXK.BangKeThanhLy.DSToKhaiXuatSPForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog(this);
                NPL_CunngUng = KDT_SXXK_BKNPLCungUng.LoadBy(HSTL.LanThanhLy, f.SoToKhai.ToString(), f.MaSP, f.sothutuhang,BKCungUng.ID);
                if (NPL_CunngUng == null)
                {
                    NPL_CunngUng = new KDT_SXXK_BKNPLCungUng();
                    NPL_CunngUng.Master_id = BKCungUng.ID;
                    NPL_CunngUng.SoToKhaiXuat = f.SoToKhai.ToString();
                    NPL_CunngUng.NgayDangKy = f.NgayDangKy;
                    NPL_CunngUng.MaDoanhNghiep = f.MaDoanhNghiep;
                    NPL_CunngUng.MaHaiQuan = f.MaHaiQuan;
                    NPL_CunngUng.MaLoaiHinh = f.MaLoaiHinh;
                    NPL_CunngUng.MaSanPham = f.MaSP;
                    NPL_CunngUng.TenSanPham = f.TenSP;
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = f.MaSP;
                    sp.MaDoanhNghiep = f.MaDoanhNghiep;
                    sp.MaHaiQuan = f.MaHaiQuan;
                    sp.Load();

                    NPL_CunngUng.DVT_ID = sp.DVT_ID.Trim();
                    NPL_CunngUng.SoTTDongHang = f.sothutuhang;
                    NPL_CunngUng.LanThanhLy = HSTL.LanThanhLy;
                    BKNPLCungUngDetailForm frm = new BKNPLCungUngDetailForm();
                    frm.BKNPL = NPL_CunngUng;
                    frm.luongSanPham = f.SoLuong;
                    frm.ShowDialog();
                    BKCungUng = KDT_SXXK_BKNPLCungUngDangKy.LoadByID(BKCungUng.ID.ToString());
                    dgList2.DataSource = BKCungUng.TKCungUng_List;
                    dgList2.Refetch();
                    //LoadData();
                }
                else
                {
                    ShowMessage("Mã sản phẩm " + f.MaSP + " của tờ khai xuất " + f.SoToKhai.ToString() + " đã được cung ứng trong bảng kê.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long id= Convert.ToInt32( dgList2.CurrentRow.Cells["ID"].Value);
                NPL_CunngUng = KDT_SXXK_BKNPLCungUng.LoadID(id);
                BKNPLCungUngDetailForm frm = new BKNPLCungUngDetailForm();
                frm.BKNPL = NPL_CunngUng;
                frm.ShowDialog();
                LoadData();
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdluu":
                    Save();
                    break;
                case "cmdThemMoi":
                    btnAdd_Click(null,null);
                    break;
                case"cmdKhaiBao":
                    SendV5();
                    //SendHuyV5();
                    break;
                case"cmdNhanPhanHoi":
                    FeedBackV5();
                    break;
                case"cmdKetQua":
                    KetQuaXuLyTCU();
                    break;
                case "cmdSuaBK":
                    SuaBangKe();
                    break;
                case "cmdHuyBK":
                    SendHuyV5();
                    break;


            }
        }
        private void SuaBangKe()
        {
            if (ShowMessage("Bạn muốn sửa bảng kê NPL cung ứng này? ", true) == "Yes")
            {
                BKCungUng.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                BKCungUng.InsertUpdate();
            }
            SetCommand();
        }
        private void Save()
        {
            BKCungUng.LanThanhLy = HSTL.LanThanhLy;
            BKCungUng.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            BKCungUng.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            BKCungUng.InsertUpdatFull();
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TuCungUngSendHandler(BKCungUng, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo bảng kê tự cung ứng này đến Hải quan?", true) == "Yes")
            {
                bool isKhaiSua = false;
                if (BKCungUng.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    BKCungUng = KDT_SXXK_BKNPLCungUngDangKy.LoadByID(BKCungUng.ID.ToString());
                    if(BKCungUng.TrangThaiXuLy==5)
                        isKhaiSua= true;
                    else
                        isKhaiSua= false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                sendXML.master_id = BKCungUng.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdThemMoi.Enabled= cmdThemMoi1.Enabled= cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    
                    return;
                }
                try
                {
                    if (this.BKCungUng.TKCungUng_List.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        BKCungUng.GUIDSTR = Guid.NewGuid().ToString();
                        SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity =BKCungUng.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKCungUng.MaHaiQuan)),
                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKCungUng.MaHaiQuan).Trim() : BKCungUng.MaHaiQuan
                                  //Identity = BKCungUng.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.SXXK_TUCUNGUNG,
                                  Function = isKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                  Reference = BKCungUng.GUIDSTR,
                              },
                              TuCungUng
                            );
                        //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(BKCungUng.ID, isKhaiSua ? MessageTitle.KhaiBaoSuaTCU : MessageTitle.KhaiBaoTCU);
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdThemMoi.Enabled = cmdThemMoi1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdluu.Enabled = cmdluu.Enabled= Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                            sendXML.master_id = BKCungUng.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            BKCungUng.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }


        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
            {
             //   bool isKhaiSua = false;
                if (BKCungUng.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    BKCungUng = KDT_SXXK_BKNPLCungUngDangKy.LoadByID(BKCungUng.ID.ToString());
                    BKCungUng.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    //if (BKCungUng.TrangThaiXuLy == 5)
                    //    isKhaiSua = true;
                    //else
                    //    isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                sendXML.master_id = BKCungUng.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdThemMoi.Enabled = cmdThemMoi1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.BKCungUng.TKCungUng_List.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        BKCungUng.GUIDSTR = Guid.NewGuid().ToString();
                        //SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = CancelMessageV5(BKCungUng);
                        //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(BKCungUng.ID, "Hủy khai báo bảng kê");
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdThemMoi.Enabled = cmdThemMoi1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdluu.Enabled = cmdluu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                            sendXML.master_id = BKCungUng.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            BKCungUng.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }







        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = BKCungUng.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = BKCungUng.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_TUCUNGUNG,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_TUCUNGUNG,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = BKCungUng.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BKCungUng.MaHaiQuan.Trim())),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BKCungUng.MaHaiQuan).Trim() : BKCungUng.MaHaiQuan
                                                  //Identity = BKCungUng.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }
        private void KetQuaXuLyTCU()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.BKCungUng.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXXK_TUCUNGUNG;
            form.ShowDialog(this);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(dgList2.CurrentRow.Cells["ID"].Value.ToString());

                if (ID > 0)
                {
                    if (ShowMessage("Bạn muốn xóa bảng kê cung ứng của tờ khai " + dgList2.CurrentRow.Cells["SoToKhaiXuat"].Value.ToString() + " Mã sản phẩm " + dgList2.CurrentRow.Cells["MaSanPham"].Value.ToString() + " này không?", true) == "Yes")
                    {
                        KDT_SXXK_BKNPLCungUng npl = new KDT_SXXK_BKNPLCungUng();
                        npl.DeleteFull(ID);
                    }
                }
                dgList2.CurrentRow.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        public static ObjectSend CancelMessageV5(KDT_SXXK_BKNPLCungUngDangKy BK)
        {
              string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
         string sfmtDate = "yyyy-MM-dd";
         string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
            //bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
            BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            //bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            bool IsHuyToKhai = true;
            string issuer = string.Empty;
            issuer = DeclarationIssuer.SXXK_TUCUNGUNG;

            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = BK.GUIDSTR,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
                Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan).Trim() : BK.MaHaiQuan ,
                Importer = new NameBase()
                {
                    Name = GlobalSettings.TEN_DON_VI,
                    Identity = BK.MaDoanhNghiep
                },
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "Xin hủy bảng kê NPL cung ứng do sai số liệu" }
                }
                );
            if (IsHuyToKhai)
            {
                //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
                //if (cancelContent.Count > 0)
                //{
                //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                //    declaredCancel.Reference = guidHuyTK;
                //}
            }
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(BK.MaHaiQuan),
                Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan).Trim() : BK.MaHaiQuan
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //BK.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
    }
}