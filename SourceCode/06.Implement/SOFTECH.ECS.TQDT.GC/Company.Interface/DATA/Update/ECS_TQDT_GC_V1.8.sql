/*
Run this script on:

        ecsteam.ECS_TQDT_GC_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_GC

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 03/20/2012 11:10:14 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_GC_HopDong]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HopDong] ALTER COLUMN [NgayDangKy] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_NguyenPhuLieuTon]'
GO
EXEC sp_refreshview N'[dbo].[t_View_NguyenPhuLieuTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_DinhMucChuaDangKy]'
GO
EXEC sp_refreshview N'[dbo].[t_View_DinhMucChuaDangKy]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_SanPhamHopDongCoDinhMuc]'
GO
EXEC sp_refreshview N'[dbo].[v_SanPhamHopDongCoDinhMuc]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_GC_NguyenPhuLieuTonThucTe]'
GO
EXEC sp_refreshview N'[dbo].[t_GC_NguyenPhuLieuTonThucTe]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_DinhMuc]'
GO
EXEC sp_refreshview N'[dbo].[t_View_DinhMuc]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_CanDoiXuatNhap]'
GO
EXEC sp_refreshview N'[dbo].[t_View_CanDoiXuatNhap]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[view_10_HQGC]'
GO
EXEC sp_refreshview N'[dbo].[view_10_HQGC]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_GC_ToKhaiChuyenTiep]'
GO
ALTER TABLE [dbo].[t_KDT_GC_ToKhaiChuyenTiep] ADD
[SoKien] [decimal] (10, 3) NULL CONSTRAINT [DF_t_KDT_GC_ToKhaiChuyenTiep_SoKien] DEFAULT ((0)),
[TrongLuong] [decimal] (10, 3) NULL CONSTRAINT [DF_t_KDT_GC_ToKhaiChuyenTiep_TrongLuong] DEFAULT ((0)),
[TrongLuongTinh] [decimal] (10, 3) NULL CONSTRAINT [DF_t_KDT_GC_ToKhaiChuyenTiep_TrongLuongTinh] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_HangToKhai]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_HangToKhai]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_HangChuyenTiep]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_HangChuyenTiep]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_PhanBo_TKMD]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_PhanBo_TKMD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_PhanBo]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_PhanBo]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[View_4]'
GO
EXEC sp_refreshview N'[dbo].[View_4]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_NguyenPhuLieu_DK]'
GO
EXEC sp_refreshview N'[dbo].[v_NguyenPhuLieu_DK]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HCT_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HCT_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[View_2]'
GO
EXEC sp_refreshview N'[dbo].[View_2]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[View_1]'
GO
EXEC sp_refreshview N'[dbo].[View_1]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_PhuKienDangKy]'
GO
EXEC sp_refreshview N'[dbo].[t_View_PhuKienDangKy]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_ToKhaiMauDichCoCungUng]'
GO
EXEC sp_refreshview N'[dbo].[v_ToKhaiMauDichCoCungUng]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_ToKhaiXuatDaPhanBo]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_ToKhaiXuatDaPhanBo]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HMD_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HMD_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_NPL]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_NPL]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[View_3]'
GO
EXEC sp_refreshview N'[dbo].[View_3]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_NPL_Nhap_HD]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_NPL_Nhap_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SP_Xuat_HD]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SP_Xuat_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_LuongNPLTrongTKXuat]'
GO
EXEC sp_refreshview N'[dbo].[t_View_LuongNPLTrongTKXuat]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_DanhSachPhuKienMuaVN]'
GO
EXEC sp_refreshview N'[dbo].[t_View_DanhSachPhuKienMuaVN]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC01HSTK_GC_TT117]'
GO
-- =============================================    
-- Author:  LanNT    
-- Create date:     
-- Description:     
-- =============================================    
    
ALTER PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',
--    
SELECT   ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS 'STT',   t_KDT_GC_HopDong.ID, t_KDT_GC_HopDong.MaDoanhNghiep, t_KDT_GC_HopDong.MaHaiQuan, CONVERT(varchar(50), ToKhai.SoToKhai) 
                      + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, ToKhai.NgayDangKy, t_GC_NguyenPhuLieu.Ten AS TenNguyenPhuLieu, 
                      t_GC_NguyenPhuLieu.Ma AS MaNguyenPhuLieu, ToKhai.SoLuong, t_HaiQuan_DonViTinh.Ten AS DVT
FROM         t_KDT_GC_HopDong INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'NGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, 
                                                  t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu, t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPN' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC18' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'NGC19')) AS ToKhai ON t_KDT_GC_HopDong.ID = ToKhai.IDHopDong AND 
                      t_KDT_GC_HopDong.MaHaiQuan = ToKhai.MaHaiQuan INNER JOIN
                      t_GC_NguyenPhuLieu ON ToKhai.MaNguyenPhuLieu = t_GC_NguyenPhuLieu.Ma AND ToKhai.IDHopDong = t_GC_NguyenPhuLieu.HopDong_ID INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_NguyenPhuLieu.DVT_ID = t_HaiQuan_DonViTinh.ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (t_KDT_GC_HopDong.ID = @IDHopDong) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan)
ORDER BY MaNguyenPhuLieu, CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT
    
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC02HSTK_GC_TT117]'
GO
-- =============================================    
-- Author:  LanNT    
-- Create date:     
-- Description:     
-- =============================================    
    
ALTER PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'    
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',    
    --ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT', 
SELECT DISTINCT 
                   ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',   CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, ToKhai.NgayDangKy, v_KDT_SP_Xuat_HD.MaPhu AS MaHang, 
                      ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong
FROM         v_KDT_SP_Xuat_HD INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
                                                   t_KDT_ToKhaiMauDich.MaDoanhNghiep
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S')
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan, t_KDT_GC_HangChuyenTiep.MaHang, 
                                                  t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19')) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong AND 
                      v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan)
ORDER BY MaHang 
    
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC03HSTK_GC_TT117]'
GO
-- =============================================    
-- Author:  LanNT    
-- Create date:     
-- Description:     
-- =============================================    
    
ALTER PROCEDURE [dbo].[p_GC_BC03HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'    
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',    
-- declare @IDHopDong BIGINT, @MaHaiQuan CHAR(6), @MaDoanhNghiep VARCHAR(14);  set @IDHopDong=  397;set @MaHaiQuan='C34C'; Set @MaDoanhNghiep='0400101556';    
    
    --
    
SELECT    ROW_NUMBER() OVER (ORDER BY ToKhai.MaHang) AS 'STT',  t_View_KDT_GC_NPL.IDHopDong, CONVERT(varchar(50), ToKhai.SoToKhai) + '/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT AS SoToKhai, ToKhai.MaHang, 
                      ToKhai.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ToKhai.SoLuong, '-' AS TongCong
FROM         t_View_KDT_GC_NPL INNER JOIN
                          (SELECT     t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.NgayDangKy, t_KDT_ToKhaiMauDich.IDHopDong, 
                                                   t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong
                            FROM          t_KDT_ToKhaiMauDich INNER JOIN
                                                   t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            WHERE      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
                                                   (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N') AND 
                                                   (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)
                            UNION
                            SELECT     t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 
                                                  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, t_KDT_GC_HangChuyenTiep.MaHang, t_KDT_GC_HangChuyenTiep.TenHang, 
                                                  t_KDT_GC_HangChuyenTiep.SoLuong
                            FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                                                  t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                            WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHPLX' OR
                                                  t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC18') AND (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND 
                                                  (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep)) AS ToKhai ON t_View_KDT_GC_NPL.IDHopDong = ToKhai.IDHopDong AND 
                      t_View_KDT_GC_NPL.MaPhu = ToKhai.MaHang INNER JOIN
                      t_HaiQuan_DonViTinh ON t_View_KDT_GC_NPL.ID_DVT = t_HaiQuan_DonViTinh.ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (t_View_KDT_GC_NPL.IDHopDong = @IDHopDong)
ORDER BY ToKhai.MaHang  
            
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_BC09HSTK_GC_TT117]'
GO
-- =============================================    
-- Author:  Name    
-- Create date:     
-- Description:     
-- =============================================    
ALTER PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
    --ROW_NUMBER() OVER (ORDER BY BangKe.SoNgayToKhai) AS 'STT',
SELECT     BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, BangKe.IDHopDong, BangKe.MaHaiQuan, BangKe.MaDoanhNghiep    
			FROM         (SELECT     CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  t_KDT_ToKhaiMauDich.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, 
								  CASE PTVT_ID WHEN '001' THEN '' WHEN '005' THEN '' ELSE CASE t_KDT_ToKhaiMauDich.NgayVanDon WHEN '1900-1-1' THEN t_KDT_ToKhaiMauDich.SoVanDon ELSE
								   t_KDT_ToKhaiMauDich.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_KDT_ToKhaiMauDich.NgayVanDon, 103) END END AS SoNgayBL, 
								  t_KDT_ToKhaiMauDich.IDHopDong, t_KDT_ToKhaiMauDich.MaHaiQuan, t_KDT_ToKhaiMauDich.MaDoanhNghiep
			FROM         t_KDT_ToKhaiMauDich INNER JOIN
								  t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID INNER JOIN
								  t_HaiQuan_CuaKhau ON t_KDT_ToKhaiMauDich.CuaKhau_ID = t_HaiQuan_CuaKhau.ID INNER JOIN
								  t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
			WHERE     (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1) AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong) AND 
								  (t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AND (t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep)   
                       UNION    
                       SELECT     CONVERT(NVARCHAR(20), t_KDT_GC_ToKhaiChuyenTiep.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
                      t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
                      t_KDT_GC_HangChuyenTiep.TriGia, '' AS CuaKhau, '' AS SoNgayBL, t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, 
                      t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan, t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN
                      t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID INNER JOIN
                      t_HaiQuan_LoaiHinhMauDich ON t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
WHERE     (t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1) AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X' OR
                      t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'XGC%') AND (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong) AND 
                      (t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AND (t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep))     
                      AS BangKe INNER JOIN    
                      t_KDT_GC_HopDong ON BangKe.IDHopDong = t_KDT_GC_HopDong.ID    
WHERE     (t_KDT_GC_HopDong.ID = @IDHopDong)    
    
END 

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.8', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.8', getdate(), null)
	end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_GC_PhanBo_TKCT]'
GO
EXEC sp_refreshview N'[dbo].[v_GC_PhanBo_TKCT]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
