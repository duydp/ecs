﻿namespace Company.Interface.Controls
{
    partial class ToKhaiInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiInfo));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbTitle = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbCo = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblVanDon = new System.Windows.Forms.Label();
            this.lbDeNghiChuyenCK = new System.Windows.Forms.Label();
            this.lbChungTuNo = new System.Windows.Forms.Label();
            this.lbHoaDonTM = new System.Windows.Forms.Label();
            this.lbChungTuAnh = new System.Windows.Forms.Label();
            this.lbHopDong = new System.Windows.Forms.Label();
            this.lbGiayPhep = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbSoHang = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lbSoDongHang = new System.Windows.Forms.Label();
            this.lbTriGia = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.lbTitle);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(248, 21);
            this.panel1.TabIndex = 1;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(27, 2);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(143, 16);
            this.lbTitle.TabIndex = 1;
            this.lbTitle.Text = "Thông tin về tờ khai";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbCo);
            this.groupBox2.Controls.Add(this.lblVanDon);
            this.groupBox2.Controls.Add(this.lbDeNghiChuyenCK);
            this.groupBox2.Controls.Add(this.lbChungTuNo);
            this.groupBox2.Controls.Add(this.lbHoaDonTM);
            this.groupBox2.Controls.Add(this.lbChungTuAnh);
            this.groupBox2.Controls.Add(this.lbHopDong);
            this.groupBox2.Controls.Add(this.lbGiayPhep);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(248, 96);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin chứng từ kèm";
            // 
            // lbCo
            // 
            this.lbCo.AutoSize = true;
            this.lbCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCo.ImageKey = "(none)";
            this.lbCo.ImageList = this.imageList1;
            this.lbCo.Location = new System.Drawing.Point(117, 16);
            this.lbCo.Name = "lbCo";
            this.lbCo.Size = new System.Drawing.Size(37, 13);
            this.lbCo.TabIndex = 2;
            this.lbCo.Text = "     CO";
            this.lbCo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "accept.png");
            // 
            // lblVanDon
            // 
            this.lblVanDon.AutoSize = true;
            this.lblVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVanDon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblVanDon.ImageList = this.imageList1;
            this.lblVanDon.Location = new System.Drawing.Point(8, 16);
            this.lblVanDon.Name = "lblVanDon";
            this.lblVanDon.Size = new System.Drawing.Size(61, 13);
            this.lblVanDon.TabIndex = 2;
            this.lblVanDon.Text = "     Vận đơn";
            this.lblVanDon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDeNghiChuyenCK
            // 
            this.lbDeNghiChuyenCK.AutoSize = true;
            this.lbDeNghiChuyenCK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDeNghiChuyenCK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbDeNghiChuyenCK.ImageList = this.imageList1;
            this.lbDeNghiChuyenCK.Location = new System.Drawing.Point(116, 34);
            this.lbDeNghiChuyenCK.Name = "lbDeNghiChuyenCK";
            this.lbDeNghiChuyenCK.Size = new System.Drawing.Size(113, 13);
            this.lbDeNghiChuyenCK.TabIndex = 2;
            this.lbDeNghiChuyenCK.Text = "     Đề nghị chuyển CK";
            this.lbDeNghiChuyenCK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbChungTuNo
            // 
            this.lbChungTuNo.AutoSize = true;
            this.lbChungTuNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChungTuNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbChungTuNo.ImageList = this.imageList1;
            this.lbChungTuNo.Location = new System.Drawing.Point(118, 75);
            this.lbChungTuNo.Name = "lbChungTuNo";
            this.lbChungTuNo.Size = new System.Drawing.Size(83, 13);
            this.lbChungTuNo.TabIndex = 2;
            this.lbChungTuNo.Text = "     Chứng từ nợ";
            this.lbChungTuNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHoaDonTM
            // 
            this.lbHoaDonTM.AutoSize = true;
            this.lbHoaDonTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHoaDonTM.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbHoaDonTM.ImageList = this.imageList1;
            this.lbHoaDonTM.Location = new System.Drawing.Point(8, 76);
            this.lbHoaDonTM.Name = "lbHoaDonTM";
            this.lbHoaDonTM.Size = new System.Drawing.Size(79, 13);
            this.lbHoaDonTM.TabIndex = 2;
            this.lbHoaDonTM.Text = "     Hóa đơn TM";
            this.lbHoaDonTM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbChungTuAnh
            // 
            this.lbChungTuAnh.AutoSize = true;
            this.lbChungTuAnh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChungTuAnh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbChungTuAnh.ImageList = this.imageList1;
            this.lbChungTuAnh.Location = new System.Drawing.Point(118, 54);
            this.lbChungTuAnh.Name = "lbChungTuAnh";
            this.lbChungTuAnh.Size = new System.Drawing.Size(116, 13);
            this.lbChungTuAnh.TabIndex = 2;
            this.lbChungTuAnh.Text = "     Chứng từ dạng ảnh";
            this.lbChungTuAnh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHopDong
            // 
            this.lbHopDong.AutoSize = true;
            this.lbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHopDong.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbHopDong.ImageList = this.imageList1;
            this.lbHopDong.Location = new System.Drawing.Point(8, 34);
            this.lbHopDong.Name = "lbHopDong";
            this.lbHopDong.Size = new System.Drawing.Size(68, 13);
            this.lbHopDong.TabIndex = 2;
            this.lbHopDong.Text = "     Hợp đồng";
            this.lbHopDong.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbGiayPhep
            // 
            this.lbGiayPhep.AutoSize = true;
            this.lbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiayPhep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbGiayPhep.ImageList = this.imageList1;
            this.lbGiayPhep.Location = new System.Drawing.Point(7, 55);
            this.lbGiayPhep.Name = "lbGiayPhep";
            this.lbGiayPhep.Size = new System.Drawing.Size(70, 13);
            this.lbGiayPhep.TabIndex = 2;
            this.lbGiayPhep.Text = "     Giấy phép";
            this.lbGiayPhep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbSoDongHang);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.lbTriGia);
            this.groupBox3.Controls.Add(this.lbSoHang);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(0, 117);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 71);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin hàng hóa";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Số dòng hàng";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Trạng thái xử lý";
            // 
            // lbSoHang
            // 
            this.lbSoHang.AutoSize = true;
            this.lbSoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoHang.Location = new System.Drawing.Point(99, 33);
            this.lbSoHang.Name = "lbSoHang";
            this.lbSoHang.Size = new System.Drawing.Size(45, 13);
            this.lbSoHang.TabIndex = 2;
            this.lbSoHang.Text = "số hàng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(7, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Tổng trị giá";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 33);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Tổng số hàng";
            // 
            // lbSoDongHang
            // 
            this.lbSoDongHang.AutoSize = true;
            this.lbSoDongHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoDongHang.Location = new System.Drawing.Point(99, 17);
            this.lbSoDongHang.Name = "lbSoDongHang";
            this.lbSoDongHang.Size = new System.Drawing.Size(58, 13);
            this.lbSoDongHang.TabIndex = 2;
            this.lbSoDongHang.Text = "dòng hàng";
            // 
            // lbTriGia
            // 
            this.lbTriGia.AutoSize = true;
            this.lbTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTriGia.Location = new System.Drawing.Point(99, 51);
            this.lbTriGia.Name = "lbTriGia";
            this.lbTriGia.Size = new System.Drawing.Size(34, 13);
            this.lbTriGia.TabIndex = 2;
            this.lbTriGia.Text = "trị giá";
            // 
            // ToKhaiInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Name = "ToKhaiInfo";
            this.Size = new System.Drawing.Size(248, 192);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblVanDon;
        private System.Windows.Forms.Label lbHoaDonTM;
        private System.Windows.Forms.Label lbHopDong;
        private System.Windows.Forms.Label lbGiayPhep;
        private System.Windows.Forms.Label lbCo;
        private System.Windows.Forms.Label lbDeNghiChuyenCK;
        private System.Windows.Forms.Label lbChungTuAnh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbSoHang;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbChungTuNo;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label lbSoDongHang;
        private System.Windows.Forms.Label lbTriGia;

    }
}
