﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.Report
{
    public partial class CauHinhInForm : BaseForm
    {
        public CauHinhInForm()
        {
            InitializeComponent();
        }
        public Label lblDonGiaNT { get { return _lblDonGiaNT; } }
        public Label lblTriGiaNT { get { return _lblTriGiaNT; } }
        public Label lblLuongSP { get { return _lblLuongSp; } }
        public Label lblLuongNPL { get { return _lblLuongNPL; } }
        public List<Label> Listlable = new List<Label>();
        private void CauHinhInForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (Listlable.Count > 0)
                {
                    timer1.Interval = 500;
                    timer1.Start();
                }
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                txtDinhMuc.Value = GlobalSettings.SoThapPhan.DinhMuc;
                txtLuongNPL.Value = GlobalSettings.SoThapPhan.LuongNPL;
                txtLuongSP.Value = GlobalSettings.SoThapPhan.LuongSP;
                txtTLHH.Value = GlobalSettings.SoThapPhan.TLHH;
                txtTriGiaNT.Value = GlobalSettings.TriGiaNT;
                txtDonGiaNT.Value = GlobalSettings.DonGiaNT;

                /* Comment by HungTQ, 22/12/2010.*/
                txtDiaPhuong.Text = GlobalSettings.DiaPhuong;
                rdNgayTuDong.Checked = GlobalSettings.NgayHeThong;
                rdNgayTuNhap.Checked = !rdNgayTuDong.Checked;

                cboChiPhi.SelectedValue = GlobalSettings.ChiPhiKhac;
                numFontDongHang.Value = (decimal)GlobalSettings.FontDongHang;
                numFontToKhai.Value = (decimal)GlobalSettings.FontToKhai;
                cboXuongDongThongTinDiaChi.SelectedValue = GlobalSettings.WordWrap;

                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");
                numTimeDelay.Value = Convert.ToDecimal(delay);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string dp = "";
                cvError.Validate();
                if (!cvError.IsValid) return;
                else
                {
                    GlobalSettings.Luu_SoThapPhan((int)txtDinhMuc.Value, (int)txtLuongNPL.Value, (int)txtLuongSP.Value, (int)txtTLHH.Value);

                    //Hungtq 22/12/2010. Luu cau hinh
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaPhuong", txtDiaPhuong.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgayHeThong", rdNgayTuDong.Checked.ToString());

                    if (txtDiaPhuong.Text.Trim() == "")
                        dp = ".................";
                    if (rdNgayTuDong.Checked == true)
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                    else
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày........tháng........năm........ ");

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ChiPhiKhac", (int)cboChiPhi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontTenHang", (float)numFontDongHang.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontToKhai", (float)numFontToKhai.Value);
                    //xuống dòng thông tin địa chỉ
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WordWrap", cboXuongDongThongTinDiaChi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TriGiaNT", txtTriGiaNT.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DonGiaNT", txtDonGiaNT.Text.Trim());

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay", numTimeDelay.Value.ToString());

                    GlobalSettings.RefreshKey();
                    Company.KDT.SHARE.Components.Globals.TriGiaNT = Convert.ToInt32(txtTriGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.DonGiaNT = Convert.ToInt32(txtDonGiaNT.Value);
                    //ShowMessage("Lưu thông tin cấu hình thành công.",false);
                    showMsg("MSG_0203069");
                    Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }

    }
}

