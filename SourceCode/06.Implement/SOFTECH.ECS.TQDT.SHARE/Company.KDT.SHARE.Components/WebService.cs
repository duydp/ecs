﻿//#define DEBUG
using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.ServiceModel;

namespace Company.KDT.SHARE.Components
{
    public class WebService
    {

        #region Load Config Webservice

        public static string LoadConfigure(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            return config.AppSettings.Settings[key].Value.ToString();
        }
        public static CisWS.CISServiceSoapClient GetCisService()
        {
            CisWS.CISServiceSoapClient cis = null;

            try
            {

                string url = LoadConfigure("WS_URL");
                BasicHttpBinding binding = new BasicHttpBinding();
#if DEBUG
                url = "http://203.210.158.232/kdtservice/cisservice.asmx";
#endif

                EndpointAddress address = new EndpointAddress(url);

                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
                if (!string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    binding.ProxyAddress = proxy.Address;
                }
                binding.UseDefaultWebProxy = true;
                cis = new Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient(binding, address);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
                throw new Exception("Không thể kết nối với hệ thống hải quan\nVui lòng kiểm tra lại kết nối của bạn");
            }
            return cis;
        }
        public static Company.KDT.SHARE.Components.WS.KDTService GetWS()
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = new Company.KDT.SHARE.Components.WS.KDTService();
            try
            {
                string url = LoadConfigure("WS_URL");
                kdt.Url = url;

                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");

                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");

                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsUseProxy").Equals("True") || !string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    kdt.Proxy = proxy;

                }
                else
                {
                    kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                    kdt.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    kdt.Credentials = CredentialCache.DefaultCredentials;
                }

                kdt.Timeout = 300000;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
            }

            return kdt;
        }
        #endregion
    }
}
