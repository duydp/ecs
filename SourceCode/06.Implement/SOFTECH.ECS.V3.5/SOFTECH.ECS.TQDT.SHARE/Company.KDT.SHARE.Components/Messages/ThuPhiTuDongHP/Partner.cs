﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class Partner
    {
        /// <summary>
        /// Mã doanh nghiệp nộp phí
        /// </summary>
       [XmlElement("partnerCode")]
       public string PartnerCode { get; set; }

        /// <summary>
       /// Tên doanh nghiệp nộp phí
        /// 
        /// </summary>
       [XmlElement("partnerNameInVN")]
       public string PartnerNameInVN { get; set; }

        /// <summary>
       /// Địa chỉ doanh nghiệp nộp phí
        /// </summary>
       [XmlElement("address")]
       public string Address { get; set; }

        /// <summary>
       /// Mã số thuế của doanh nghiệp nộp phí
        /// </summary>
       [XmlElement("taxCode")]
       public string TaxCode { get; set; }

        /// <summary>
       /// Tên người nộp phí
        /// </summary>
       [XmlElement("contactName")]
       public string ContactName { get; set; }
    }
}
