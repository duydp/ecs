namespace Company.Interface.Report.GC
{
    partial class ThanhKhoan07_ReiKer_TT117
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenMay = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDaXuatOrChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblConLai = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBienPhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLbl_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSP2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSP1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_HDGCS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_NgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_ThoiHanHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_SoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DiaChiBN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DiaChiBT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_DVHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLable112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_ThoiHanPLHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_NgayPLHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_MatHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_PLHDGCS = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_BenThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblH_BenNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLbl_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLbl_7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Height = 20;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(0, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(992, 20);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblTenMay,
            this.lblDVT,
            this.lblSoLuongNhap,
            this.lblDaXuatOrChuyen,
            this.lblConLai,
            this.lblBienPhap});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.StylePriority.UseTextAlignment = false;
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.Weight = 0.050403225806451624;
            // 
            // lblTenMay
            // 
            this.lblTenMay.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenMay.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenMay.Name = "lblTenMay";
            this.lblTenMay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenMay.StylePriority.UseTextAlignment = false;
            this.lblTenMay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenMay.Weight = 0.21875;
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVT.StylePriority.UseTextAlignment = false;
            this.lblDVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVT.Weight = 0.11693548387096775;
            // 
            // lblSoLuongNhap
            // 
            this.lblSoLuongNhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongNhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongNhap.Name = "lblSoLuongNhap";
            this.lblSoLuongNhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongNhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongNhap.Weight = 0.10080645161290319;
            this.lblSoLuongNhap.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblDaXuatOrChuyen
            // 
            this.lblDaXuatOrChuyen.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDaXuatOrChuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDaXuatOrChuyen.Name = "lblDaXuatOrChuyen";
            this.lblDaXuatOrChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDaXuatOrChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDaXuatOrChuyen.Weight = 0.21875000000000003;
            this.lblDaXuatOrChuyen.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblConLai
            // 
            this.lblConLai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblConLai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConLai.Name = "lblConLai";
            this.lblConLai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblConLai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblConLai.Weight = 0.10080645161290322;
            this.lblConLai.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSoLuongNhap_BeforePrint);
            // 
            // lblBienPhap
            // 
            this.lblBienPhap.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBienPhap.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienPhap.Name = "lblBienPhap";
            this.lblBienPhap.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBienPhap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblBienPhap.Weight = 0.19354838709677419;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLbl_4,
            this.xrLbl_3,
            this.lblSP2,
            this.xrLabel26,
            this.xrLabel9,
            this.lblSP1,
            this.xrLabel10});
            this.PageHeader.Height = 50;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLbl_4
            // 
            this.xrLbl_4.BackColor = System.Drawing.Color.Azure;
            this.xrLbl_4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLbl_4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_4.Location = new System.Drawing.Point(800, 0);
            this.xrLbl_4.Name = "xrLbl_4";
            this.xrLbl_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_4.Size = new System.Drawing.Size(192, 50);
            this.xrLbl_4.Text = "Biện pháp xử lý đối với máy móc, thiết bị chưa tái xuất";
            this.xrLbl_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_3
            // 
            this.xrLbl_3.BackColor = System.Drawing.Color.Azure;
            this.xrLbl_3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLbl_3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_3.Location = new System.Drawing.Point(700, 0);
            this.xrLbl_3.Name = "xrLbl_3";
            this.xrLbl_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_3.Size = new System.Drawing.Size(100, 50);
            this.xrLbl_3.Text = "Máy móc, thiết bị còn lại chưa tái xuất";
            this.xrLbl_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSP2
            // 
            this.lblSP2.BackColor = System.Drawing.Color.Azure;
            this.lblSP2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSP2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSP2.Location = new System.Drawing.Point(483, 0);
            this.lblSP2.Name = "lblSP2";
            this.lblSP2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSP2.Size = new System.Drawing.Size(217, 50);
            this.lblSP2.Text = "Đã tái xuất hoặc chuyển sang hợp đồng gia công khác trong khi thực hiện hợp đồng " +
                "GC";
            this.lblSP2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BackColor = System.Drawing.Color.Azure;
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.Location = new System.Drawing.Point(267, 0);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.Size = new System.Drawing.Size(117, 50);
            this.xrLabel26.Text = "Đơn vị tính";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BackColor = System.Drawing.Color.Azure;
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.Location = new System.Drawing.Point(0, 0);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(50, 50);
            this.xrLabel9.Text = "STT";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSP1
            // 
            this.lblSP1.BackColor = System.Drawing.Color.Azure;
            this.lblSP1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSP1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSP1.Location = new System.Drawing.Point(383, 0);
            this.lblSP1.Name = "lblSP1";
            this.lblSP1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSP1.Size = new System.Drawing.Size(100, 50);
            this.lblSP1.Text = "Số lượng tạm nhập";
            this.lblSP1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Azure;
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(50, 0);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(217, 50);
            this.xrLabel10.Text = "Tên máy móc, thiết bị tạm nhập";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel6,
            this.lblH_HDGCS,
            this.xrLabel24,
            this.xrLabel17,
            this.lblH_NgayHD,
            this.lblH_ThoiHanHD,
            this.xrLabel1,
            this.lblH_SoLuong,
            this.xrLabel34,
            this.lblH_DiaChiBN,
            this.xrLabel33,
            this.lblH_DiaChiBT,
            this.xrLabel31,
            this.lblH_DVHaiQuan,
            this.xrLable112,
            this.xrLabel15,
            this.lblH_ThoiHanPLHD,
            this.lblH_NgayPLHD,
            this.xrLabel18,
            this.lblH_MatHang,
            this.xrLabel8,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.lblH_PLHDGCS,
            this.lblH_BenThue,
            this.lblH_BenNhan,
            this.xrLabel2});
            this.ReportHeader.Height = 238;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(733, 67);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(50, 16);
            this.xrLabel7.Text = "Tờ số:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.Location = new System.Drawing.Point(792, 8);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(200, 17);
            this.xrLabel6.Text = "Mẫu: 07/HSTK-GC/2011, Khổ A4";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_HDGCS
            // 
            this.lblH_HDGCS.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_HDGCS.Location = new System.Drawing.Point(142, 100);
            this.lblH_HDGCS.Name = "lblH_HDGCS";
            this.lblH_HDGCS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_HDGCS.Size = new System.Drawing.Size(308, 20);
            this.lblH_HDGCS.Tag = "Số hợp đồng";
            this.lblH_HDGCS.Text = " ";
            this.lblH_HDGCS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_HDGCS.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.Location = new System.Drawing.Point(0, 100);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.Size = new System.Drawing.Size(142, 20);
            this.xrLabel24.Text = "Hợp đồng gia công số :";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(450, 100);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.Size = new System.Drawing.Size(50, 20);
            this.xrLabel17.Text = "Ngày :";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_NgayHD
            // 
            this.lblH_NgayHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_NgayHD.Location = new System.Drawing.Point(500, 100);
            this.lblH_NgayHD.Name = "lblH_NgayHD";
            this.lblH_NgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_NgayHD.Size = new System.Drawing.Size(200, 20);
            this.lblH_NgayHD.Tag = "Ngày HĐ";
            this.lblH_NgayHD.Text = " ";
            this.lblH_NgayHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_NgayHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_ThoiHanHD
            // 
            this.lblH_ThoiHanHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_ThoiHanHD.Location = new System.Drawing.Point(767, 100);
            this.lblH_ThoiHanHD.Name = "lblH_ThoiHanHD";
            this.lblH_ThoiHanHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_ThoiHanHD.Size = new System.Drawing.Size(225, 20);
            this.lblH_ThoiHanHD.Tag = "Hạn HĐ";
            this.lblH_ThoiHanHD.Text = " ";
            this.lblH_ThoiHanHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_ThoiHanHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(700, 100);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(67, 20);
            this.xrLabel1.Text = "Thời hạn :";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_SoLuong
            // 
            this.lblH_SoLuong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_SoLuong.Location = new System.Drawing.Point(600, 180);
            this.lblH_SoLuong.Name = "lblH_SoLuong";
            this.lblH_SoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_SoLuong.Size = new System.Drawing.Size(392, 20);
            this.lblH_SoLuong.Tag = "Lượng hàng";
            this.lblH_SoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_SoLuong.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.Location = new System.Drawing.Point(450, 180);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.Size = new System.Drawing.Size(150, 20);
            this.xrLabel34.Text = "Số lượng hàng gia công:";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblH_DiaChiBN
            // 
            this.lblH_DiaChiBN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DiaChiBN.Location = new System.Drawing.Point(508, 160);
            this.lblH_DiaChiBN.Name = "lblH_DiaChiBN";
            this.lblH_DiaChiBN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DiaChiBN.Size = new System.Drawing.Size(484, 20);
            this.lblH_DiaChiBN.Tag = "ĐC bên nhận";
            this.lblH_DiaChiBN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_DiaChiBN.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.Location = new System.Drawing.Point(450, 160);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.Size = new System.Drawing.Size(58, 20);
            this.xrLabel33.Text = "Địa chỉ :";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_DiaChiBT
            // 
            this.lblH_DiaChiBT.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DiaChiBT.Location = new System.Drawing.Point(508, 140);
            this.lblH_DiaChiBT.Name = "lblH_DiaChiBT";
            this.lblH_DiaChiBT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DiaChiBT.Size = new System.Drawing.Size(484, 20);
            this.lblH_DiaChiBT.Tag = "ĐC bên thuê";
            this.lblH_DiaChiBT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_DiaChiBT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.Location = new System.Drawing.Point(450, 140);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.Size = new System.Drawing.Size(58, 20);
            this.xrLabel31.Text = "Địa chỉ :";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_DVHaiQuan
            // 
            this.lblH_DVHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_DVHaiQuan.Location = new System.Drawing.Point(183, 200);
            this.lblH_DVHaiQuan.Name = "lblH_DVHaiQuan";
            this.lblH_DVHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_DVHaiQuan.Size = new System.Drawing.Size(809, 20);
            this.lblH_DVHaiQuan.Text = " ";
            this.lblH_DVHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLable112
            // 
            this.xrLable112.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLable112.Location = new System.Drawing.Point(0, 200);
            this.xrLable112.Name = "xrLable112";
            this.xrLable112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLable112.Size = new System.Drawing.Size(183, 20);
            this.xrLable112.Text = "Đơn vị Hải quan làm thủ tục : ";
            this.xrLable112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(700, 120);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.Size = new System.Drawing.Size(67, 20);
            this.xrLabel15.Text = "Thời hạn :";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_ThoiHanPLHD
            // 
            this.lblH_ThoiHanPLHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_ThoiHanPLHD.Location = new System.Drawing.Point(767, 120);
            this.lblH_ThoiHanPLHD.Name = "lblH_ThoiHanPLHD";
            this.lblH_ThoiHanPLHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_ThoiHanPLHD.Size = new System.Drawing.Size(225, 20);
            this.lblH_ThoiHanPLHD.Tag = "Hạn phụ lục";
            this.lblH_ThoiHanPLHD.Text = " ";
            this.lblH_ThoiHanPLHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_ThoiHanPLHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_NgayPLHD
            // 
            this.lblH_NgayPLHD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_NgayPLHD.Location = new System.Drawing.Point(500, 120);
            this.lblH_NgayPLHD.Name = "lblH_NgayPLHD";
            this.lblH_NgayPLHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_NgayPLHD.Size = new System.Drawing.Size(200, 20);
            this.lblH_NgayPLHD.Tag = "Ngày phụ lục";
            this.lblH_NgayPLHD.Text = " ";
            this.lblH_NgayPLHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_NgayPLHD.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(450, 120);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.Size = new System.Drawing.Size(50, 20);
            this.xrLabel18.Text = "Ngày :";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_MatHang
            // 
            this.lblH_MatHang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_MatHang.Location = new System.Drawing.Point(117, 180);
            this.lblH_MatHang.Name = "lblH_MatHang";
            this.lblH_MatHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_MatHang.Size = new System.Drawing.Size(333, 20);
            this.lblH_MatHang.Tag = "Mặt hàng GC";
            this.lblH_MatHang.Text = " ";
            this.lblH_MatHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_MatHang.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(0, 180);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(117, 20);
            this.xrLabel8.Text = "Mặt hàng gia công:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.Location = new System.Drawing.Point(0, 120);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(183, 20);
            this.xrLabel5.Text = "Phụ lục hợp đồng gia công số :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.Location = new System.Drawing.Point(0, 140);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(117, 20);
            this.xrLabel4.Text = "Bên thuê gia công:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(0, 160);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(117, 20);
            this.xrLabel3.Text = "Bên nhận gia công:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblH_PLHDGCS
            // 
            this.lblH_PLHDGCS.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_PLHDGCS.Location = new System.Drawing.Point(183, 120);
            this.lblH_PLHDGCS.Name = "lblH_PLHDGCS";
            this.lblH_PLHDGCS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_PLHDGCS.Size = new System.Drawing.Size(267, 20);
            this.lblH_PLHDGCS.Tag = "Phụ lục HĐ";
            this.lblH_PLHDGCS.Text = " ";
            this.lblH_PLHDGCS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_PLHDGCS.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_BenThue
            // 
            this.lblH_BenThue.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_BenThue.Location = new System.Drawing.Point(117, 140);
            this.lblH_BenThue.Name = "lblH_BenThue";
            this.lblH_BenThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_BenThue.Size = new System.Drawing.Size(333, 20);
            this.lblH_BenThue.Tag = "Bên thuê GC";
            this.lblH_BenThue.Text = " ";
            this.lblH_BenThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_BenThue.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // lblH_BenNhan
            // 
            this.lblH_BenNhan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH_BenNhan.Location = new System.Drawing.Point(117, 160);
            this.lblH_BenNhan.Name = "lblH_BenNhan";
            this.lblH_BenNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblH_BenNhan.Size = new System.Drawing.Size(333, 20);
            this.lblH_BenNhan.Tag = "Bên nhận GC";
            this.lblH_BenNhan.Text = " ";
            this.lblH_BenNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblH_BenNhan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblH_HDGCS_PreviewClick);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(0, 33);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(992, 25);
            this.xrLabel2.Text = "BẢNG THANH KHOẢN MÁY MÓC, THIẾT BỊ TẠM NHẬP, TÁI XUẤT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77});
            this.ReportFooter.Height = 157;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel74.Location = new System.Drawing.Point(0, 42);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel74.Size = new System.Drawing.Size(308, 25);
            this.xrLabel74.Text = "Công chức Hải quan kiểm tra, đối chiếu";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.Location = new System.Drawing.Point(450, 42);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel75.Size = new System.Drawing.Size(266, 25);
            this.xrLabel75.Text = "Giám đốc doanh nghiệp";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel76.Location = new System.Drawing.Point(0, 17);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel76.Size = new System.Drawing.Size(308, 25);
            this.xrLabel76.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrLabel77.Location = new System.Drawing.Point(450, 17);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel77.Size = new System.Drawing.Size(266, 25);
            this.xrLabel77.Text = "Đà Nẵng, ngày ... tháng ... năm......";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLbl_5,
            this.xrLbl_8,
            this.xrLbl_1,
            this.xrLbl_6,
            this.xrLbl_2,
            this.xrLabel22,
            this.xrLbl_7,
            this.xrLabel11,
            this.xrLabel12});
            this.ReportFooter1.Height = 130;
            this.ReportFooter1.Name = "ReportFooter1";
            this.ReportFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ReportFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLbl_5
            // 
            this.xrLbl_5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_5.Location = new System.Drawing.Point(375, 75);
            this.xrLbl_5.Name = "xrLbl_5";
            this.xrLbl_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_5.Size = new System.Drawing.Size(258, 41);
            this.xrLbl_5.Text = "(Ghi ngày tháng hoàn thành việc đối chiếu; Ký, đóng dấu công chức)";
            this.xrLbl_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_8
            // 
            this.xrLbl_8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_8.Location = new System.Drawing.Point(692, 75);
            this.xrLbl_8.Name = "xrLbl_8";
            this.xrLbl_8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_8.Size = new System.Drawing.Size(292, 25);
            this.xrLbl_8.Text = "(Lãnh đạo Chi cục ký tên, đóng dấu)";
            this.xrLbl_8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_1
            // 
            this.xrLbl_1.BackColor = System.Drawing.Color.Transparent;
            this.xrLbl_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLbl_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_1.Location = new System.Drawing.Point(8, 25);
            this.xrLbl_1.Name = "xrLbl_1";
            this.xrLbl_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_1.Size = new System.Drawing.Size(266, 25);
            this.xrLbl_1.Text = "Ngày....... tháng ........năm.......";
            this.xrLbl_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_6
            // 
            this.xrLbl_6.BackColor = System.Drawing.Color.Transparent;
            this.xrLbl_6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLbl_6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_6.Location = new System.Drawing.Point(692, 25);
            this.xrLbl_6.Name = "xrLbl_6";
            this.xrLbl_6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_6.Size = new System.Drawing.Size(291, 25);
            this.xrLbl_6.Text = "Ngày ... tháng ... năm......";
            this.xrLbl_6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_2
            // 
            this.xrLbl_2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_2.Location = new System.Drawing.Point(8, 50);
            this.xrLbl_2.Name = "xrLbl_2";
            this.xrLbl_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_2.Size = new System.Drawing.Size(266, 25);
            this.xrLbl_2.Text = "Đại diện theo pháp luật của thương nhân";
            this.xrLbl_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.Location = new System.Drawing.Point(8, 75);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.Size = new System.Drawing.Size(267, 25);
            this.xrLabel22.Text = "(Ký tên, đóng dấu)";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLbl_7
            // 
            this.xrLbl_7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLbl_7.Location = new System.Drawing.Point(692, 50);
            this.xrLbl_7.Name = "xrLbl_7";
            this.xrLbl_7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLbl_7.Size = new System.Drawing.Size(292, 25);
            this.xrLbl_7.Text = "Xác nhận hoàn thành thủ tục thanh khoản           ";
            this.xrLbl_7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(325, 50);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.Size = new System.Drawing.Size(342, 25);
            this.xrLabel11.Text = "Công chức Hải quan đối chiếu";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.Location = new System.Drawing.Point(375, 25);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.Size = new System.Drawing.Size(258, 25);
            this.xrLabel12.Text = "Ngày....... tháng ........năm.......";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // winControlContainer2
            // 
            this.winControlContainer2.Location = new System.Drawing.Point(742, 42);
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.Size = new System.Drawing.Size(61, 22);
            this.winControlContainer2.WinControl = this.label2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "label3";
            // 
            // winControlContainer3
            // 
            this.winControlContainer3.Location = new System.Drawing.Point(733, 50);
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.Size = new System.Drawing.Size(61, 22);
            this.winControlContainer3.WinControl = this.label3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "label4";
            // 
            // winControlContainer4
            // 
            this.winControlContainer4.Location = new System.Drawing.Point(742, 50);
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.Size = new System.Drawing.Size(61, 22);
            this.winControlContainer4.WinControl = this.label4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "label5";
            // 
            // winControlContainer5
            // 
            this.winControlContainer5.Location = new System.Drawing.Point(750, 42);
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.Size = new System.Drawing.Size(61, 22);
            this.winControlContainer5.WinControl = this.label5;
            // 
            // ThanhKhoan07_ReiKer_TT117
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.ReportHeader,
            this.ReportFooter1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(73, 101, 32, 47);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSP1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lblSP2;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_4;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_3;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_6;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_7;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenMay;
        private DevExpress.XtraReports.UI.XRTableCell lblConLai;
        private DevExpress.XtraReports.UI.XRTableCell lblBienPhap;
        private DevExpress.XtraReports.UI.XRLabel lblH_SoLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel lblH_DiaChiBN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblH_DiaChiBT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lblH_DVHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLable112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblH_ThoiHanPLHD;
        private DevExpress.XtraReports.UI.XRLabel lblH_NgayPLHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lblH_MatHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblH_PLHDGCS;
        private DevExpress.XtraReports.UI.XRLabel lblH_BenThue;
        private DevExpress.XtraReports.UI.XRLabel lblH_BenNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongNhap;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRTableCell lblDaXuatOrChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblH_HDGCS;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblH_NgayHD;
        private DevExpress.XtraReports.UI.XRLabel lblH_ThoiHanHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_1;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_5;
        private DevExpress.XtraReports.UI.XRLabel xrLbl_8;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
    }
}
