﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace LicenseCode
{
	public partial class LM_Customer : ICloneable
	{
		#region Properties.
		
		public string CustomerCode { set; get; }
		public string TaxCode { set; get; }
		public string CustomerName { set; get; }
		public string CustomerEmail { set; get; }
		public string CustomerPhone { set; get; }
		public string CustomerDescription { set; get; }
		public string ContactFirstName { set; get; }
		public string ContactLastName { set; get; }
		public string CompanyOrDepartment { set; get; }
		public string BillingAddress { set; get; }
		public string City { set; get; }
		public string StateOrProvince { set; get; }
		public string PostalCode { set; get; }
		public string ContactTitle { set; get; }
		public string FaxNumber { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<LM_Customer> ConvertToCollection(IDataReader reader)
		{
			List<LM_Customer> collection = new List<LM_Customer>();
			while (reader.Read())
			{
				LM_Customer entity = new LM_Customer();
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaxCode"))) entity.TaxCode = reader.GetString(reader.GetOrdinal("TaxCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerDescription"))) entity.CustomerDescription = reader.GetString(reader.GetOrdinal("CustomerDescription"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactFirstName"))) entity.ContactFirstName = reader.GetString(reader.GetOrdinal("ContactFirstName"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactLastName"))) entity.ContactLastName = reader.GetString(reader.GetOrdinal("ContactLastName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CompanyOrDepartment"))) entity.CompanyOrDepartment = reader.GetString(reader.GetOrdinal("CompanyOrDepartment"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillingAddress"))) entity.BillingAddress = reader.GetString(reader.GetOrdinal("BillingAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("City"))) entity.City = reader.GetString(reader.GetOrdinal("City"));
				if (!reader.IsDBNull(reader.GetOrdinal("StateOrProvince"))) entity.StateOrProvince = reader.GetString(reader.GetOrdinal("StateOrProvince"));
				if (!reader.IsDBNull(reader.GetOrdinal("PostalCode"))) entity.PostalCode = reader.GetString(reader.GetOrdinal("PostalCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactTitle"))) entity.ContactTitle = reader.GetString(reader.GetOrdinal("ContactTitle"));
				if (!reader.IsDBNull(reader.GetOrdinal("FaxNumber"))) entity.FaxNumber = reader.GetString(reader.GetOrdinal("FaxNumber"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<LM_Customer> collection, string customerCode)
        {
            foreach (LM_Customer item in collection)
            {
                if (item.CustomerCode == customerCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_Customers VALUES(@CustomerCode, @TaxCode, @CustomerName, @CustomerEmail, @CustomerPhone, @CustomerDescription, @ContactFirstName, @ContactLastName, @CompanyOrDepartment, @BillingAddress, @City, @StateOrProvince, @PostalCode, @ContactTitle, @FaxNumber)";
            string update = "UPDATE t_LM_Customers SET TaxCode = @TaxCode, CustomerName = @CustomerName, CustomerEmail = @CustomerEmail, CustomerPhone = @CustomerPhone, CustomerDescription = @CustomerDescription, ContactFirstName = @ContactFirstName, ContactLastName = @ContactLastName, CompanyOrDepartment = @CompanyOrDepartment, BillingAddress = @BillingAddress, City = @City, StateOrProvince = @StateOrProvince, PostalCode = @PostalCode, ContactTitle = @ContactTitle, FaxNumber = @FaxNumber WHERE CustomerCode = @CustomerCode";
            string delete = "DELETE FROM t_LM_Customers WHERE CustomerCode = @CustomerCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxCode", SqlDbType.NVarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.VarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.VarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactFirstName", SqlDbType.NVarChar, "ContactFirstName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactLastName", SqlDbType.NVarChar, "ContactLastName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, "CompanyOrDepartment", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillingAddress", SqlDbType.NVarChar, "BillingAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@City", SqlDbType.NVarChar, "City", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StateOrProvince", SqlDbType.NVarChar, "StateOrProvince", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PostalCode", SqlDbType.VarChar, "PostalCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactTitle", SqlDbType.NVarChar, "ContactTitle", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FaxNumber", SqlDbType.VarChar, "FaxNumber", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxCode", SqlDbType.NVarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.VarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.VarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactFirstName", SqlDbType.NVarChar, "ContactFirstName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactLastName", SqlDbType.NVarChar, "ContactLastName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, "CompanyOrDepartment", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillingAddress", SqlDbType.NVarChar, "BillingAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@City", SqlDbType.NVarChar, "City", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StateOrProvince", SqlDbType.NVarChar, "StateOrProvince", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PostalCode", SqlDbType.VarChar, "PostalCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactTitle", SqlDbType.NVarChar, "ContactTitle", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FaxNumber", SqlDbType.VarChar, "FaxNumber", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_Customers VALUES(@CustomerCode, @TaxCode, @CustomerName, @CustomerEmail, @CustomerPhone, @CustomerDescription, @ContactFirstName, @ContactLastName, @CompanyOrDepartment, @BillingAddress, @City, @StateOrProvince, @PostalCode, @ContactTitle, @FaxNumber)";
            string update = "UPDATE t_LM_Customers SET TaxCode = @TaxCode, CustomerName = @CustomerName, CustomerEmail = @CustomerEmail, CustomerPhone = @CustomerPhone, CustomerDescription = @CustomerDescription, ContactFirstName = @ContactFirstName, ContactLastName = @ContactLastName, CompanyOrDepartment = @CompanyOrDepartment, BillingAddress = @BillingAddress, City = @City, StateOrProvince = @StateOrProvince, PostalCode = @PostalCode, ContactTitle = @ContactTitle, FaxNumber = @FaxNumber WHERE CustomerCode = @CustomerCode";
            string delete = "DELETE FROM t_LM_Customers WHERE CustomerCode = @CustomerCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxCode", SqlDbType.NVarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.VarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.VarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactFirstName", SqlDbType.NVarChar, "ContactFirstName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactLastName", SqlDbType.NVarChar, "ContactLastName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, "CompanyOrDepartment", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillingAddress", SqlDbType.NVarChar, "BillingAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@City", SqlDbType.NVarChar, "City", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StateOrProvince", SqlDbType.NVarChar, "StateOrProvince", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PostalCode", SqlDbType.VarChar, "PostalCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ContactTitle", SqlDbType.NVarChar, "ContactTitle", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FaxNumber", SqlDbType.VarChar, "FaxNumber", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxCode", SqlDbType.NVarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.VarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.VarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerDescription", SqlDbType.NVarChar, "CustomerDescription", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactFirstName", SqlDbType.NVarChar, "ContactFirstName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactLastName", SqlDbType.NVarChar, "ContactLastName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, "CompanyOrDepartment", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillingAddress", SqlDbType.NVarChar, "BillingAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@City", SqlDbType.NVarChar, "City", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StateOrProvince", SqlDbType.NVarChar, "StateOrProvince", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PostalCode", SqlDbType.VarChar, "PostalCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ContactTitle", SqlDbType.NVarChar, "ContactTitle", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FaxNumber", SqlDbType.VarChar, "FaxNumber", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LM_Customer Load(string customerCode)
		{
			const string spName = "[dbo].[p_LM_Customer_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, customerCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<LM_Customer> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LM_Customer> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LM_Customer> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_Customer_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Customer_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_Customer_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Customer_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLM_Customer(string customerCode, string taxCode, string customerName, string customerEmail, string customerPhone, string customerDescription, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string faxNumber)
		{
			LM_Customer entity = new LM_Customer();	
            entity.CustomerCode = customerCode;
			entity.TaxCode = taxCode;
			entity.CustomerName = customerName;
			entity.CustomerEmail = customerEmail;
			entity.CustomerPhone = customerPhone;
			entity.CustomerDescription = customerDescription;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.FaxNumber = faxNumber;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Customer_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.NVarChar, TaxCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.VarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.VarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<LM_Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Customer item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLM_Customer(string customerCode, string taxCode, string customerName, string customerEmail, string customerPhone, string customerDescription, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string faxNumber)
		{
			LM_Customer entity = new LM_Customer();			
			entity.CustomerCode = customerCode;
			entity.TaxCode = taxCode;
			entity.CustomerName = customerName;
			entity.CustomerEmail = customerEmail;
			entity.CustomerPhone = customerPhone;
			entity.CustomerDescription = customerDescription;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.FaxNumber = faxNumber;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Customer_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.NVarChar, TaxCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.VarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.VarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<LM_Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Customer item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLM_Customer(string customerCode, string taxCode, string customerName, string customerEmail, string customerPhone, string customerDescription, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string faxNumber)
		{
			LM_Customer entity = new LM_Customer();			
			entity.CustomerCode = customerCode;
			entity.TaxCode = taxCode;
			entity.CustomerName = customerName;
			entity.CustomerEmail = customerEmail;
			entity.CustomerPhone = customerPhone;
			entity.CustomerDescription = customerDescription;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.FaxNumber = faxNumber;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Customer_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.NVarChar, TaxCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.VarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.VarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerDescription", SqlDbType.NVarChar, CustomerDescription);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<LM_Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Customer item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLM_Customer(string customerCode)
		{
			LM_Customer entity = new LM_Customer();
			entity.CustomerCode = customerCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Customer_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Customer_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<LM_Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Customer item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}