﻿namespace Company.Interface
{
    partial class ChungTuHQTruocDoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuHQTruocDoForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkXinNo = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTuNgay = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtNgayNoCT = new System.Windows.Forms.DateTimePicker();
            this.txtNguoiPhatHanh_Ten = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiPhatHanh_Ma = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dtNgayHHCT = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtNgayCT = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNguoiDcCap_Ten = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiDcCap_Ma = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.lblSoTK = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDenNgay = new System.Windows.Forms.Label();
            this.txtSoDKCT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNguoiKhai_Ma = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dtNgayDKCT = new System.Windows.Forms.DateTimePicker();
            this.txtNguoiKhai_Ten = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dtNgayKhaiCT = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(617, 400);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnGhi);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(617, 400);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.TextAlignment = Janus.Windows.EditControls.TextAlignment.Center;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.Click += new System.EventHandler(this.uiGroupBox3_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(537, 371);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(462, 371);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.chkXinNo);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.lblTuNgay);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.dtNgayNoCT);
            this.uiGroupBox2.Controls.Add(this.txtNguoiPhatHanh_Ten);
            this.uiGroupBox2.Controls.Add(this.txtNguoiPhatHanh_Ma);
            this.uiGroupBox2.Controls.Add(this.txtSoChungTu);
            this.uiGroupBox2.Controls.Add(this.dtNgayHHCT);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.dtNgayCT);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtNguoiDcCap_Ten);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.txtNguoiDcCap_Ma);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Location = new System.Drawing.Point(6, 126);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(598, 233);
            this.uiGroupBox2.TabIndex = 331;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkXinNo
            // 
            this.chkXinNo.AutoSize = true;
            this.chkXinNo.Location = new System.Drawing.Point(122, 210);
            this.chkXinNo.Name = "chkXinNo";
            this.chkXinNo.Size = new System.Drawing.Size(116, 17);
            this.chkXinNo.TabIndex = 8;
            this.chkXinNo.Text = "Xin nợ chứng từ";
            this.chkXinNo.UseVisualStyleBackColor = true;
            this.chkXinNo.CheckedChanged += new System.EventHandler(this.chkXinNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 313;
            this.label2.Text = "Số chứng từ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(277, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 328;
            this.label6.Text = "Ngày HH CT";
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.AutoSize = true;
            this.lblTuNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblTuNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTuNgay.Location = new System.Drawing.Point(15, 57);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(49, 13);
            this.lblTuNgay.TabIndex = 328;
            this.lblTuNgay.Text = "Ngày CT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 315;
            this.label3.Text = "Mã người phát hành";
            // 
            // dtNgayNoCT
            // 
            this.dtNgayNoCT.CustomFormat = "dd/MM/yyyy";
            this.dtNgayNoCT.Enabled = false;
            this.dtNgayNoCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayNoCT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayNoCT.Location = new System.Drawing.Point(389, 207);
            this.dtNgayNoCT.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtNgayNoCT.Name = "dtNgayNoCT";
            this.dtNgayNoCT.Size = new System.Drawing.Size(165, 20);
            this.dtNgayNoCT.TabIndex = 9;
            this.dtNgayNoCT.Value = new System.DateTime(2011, 6, 6, 0, 0, 0, 0);
            // 
            // txtNguoiPhatHanh_Ten
            // 
            this.txtNguoiPhatHanh_Ten.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiPhatHanh_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiPhatHanh_Ten.Location = new System.Drawing.Point(387, 79);
            this.txtNguoiPhatHanh_Ten.Name = "txtNguoiPhatHanh_Ten";
            this.txtNguoiPhatHanh_Ten.Size = new System.Drawing.Size(193, 20);
            this.txtNguoiPhatHanh_Ten.TabIndex = 4;
            this.txtNguoiPhatHanh_Ten.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiPhatHanh_Ten.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNguoiPhatHanh_Ma
            // 
            this.txtNguoiPhatHanh_Ma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiPhatHanh_Ma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiPhatHanh_Ma.Location = new System.Drawing.Point(122, 79);
            this.txtNguoiPhatHanh_Ma.Name = "txtNguoiPhatHanh_Ma";
            this.txtNguoiPhatHanh_Ma.Size = new System.Drawing.Size(149, 20);
            this.txtNguoiPhatHanh_Ma.TabIndex = 3;
            this.txtNguoiPhatHanh_Ma.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiPhatHanh_Ma.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoChungTu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoChungTu.Location = new System.Drawing.Point(122, 25);
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(149, 20);
            this.txtSoChungTu.TabIndex = 0;
            this.txtSoChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // dtNgayHHCT
            // 
            this.dtNgayHHCT.CustomFormat = "dd/MM/yyyy";
            this.dtNgayHHCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayHHCT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayHHCT.Location = new System.Drawing.Point(389, 51);
            this.dtNgayHHCT.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtNgayHHCT.Name = "dtNgayHHCT";
            this.dtNgayHHCT.Size = new System.Drawing.Size(165, 20);
            this.dtNgayHHCT.TabIndex = 2;
            this.dtNgayHHCT.Value = new System.DateTime(2011, 6, 6, 0, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(277, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 315;
            this.label8.Text = "Tên người phát hành";
            // 
            // dtNgayCT
            // 
            this.dtNgayCT.CustomFormat = "dd/MM/yyyy";
            this.dtNgayCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayCT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayCT.Location = new System.Drawing.Point(122, 51);
            this.dtNgayCT.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtNgayCT.Name = "dtNgayCT";
            this.dtNgayCT.Size = new System.Drawing.Size(149, 20);
            this.dtNgayCT.TabIndex = 1;
            this.dtNgayCT.Value = new System.DateTime(2011, 6, 6, 0, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 315;
            this.label11.Text = "Ghi chú";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 315;
            this.label1.Text = "Mã người được cấp";
            // 
            // txtNguoiDcCap_Ten
            // 
            this.txtNguoiDcCap_Ten.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiDcCap_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiDcCap_Ten.Location = new System.Drawing.Point(387, 106);
            this.txtNguoiDcCap_Ten.Name = "txtNguoiDcCap_Ten";
            this.txtNguoiDcCap_Ten.Size = new System.Drawing.Size(193, 20);
            this.txtNguoiDcCap_Ten.TabIndex = 6;
            this.txtNguoiDcCap_Ten.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiDcCap_Ten.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGhiChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(122, 132);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(460, 64);
            this.txtGhiChu.TabIndex = 7;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNguoiDcCap_Ma
            // 
            this.txtNguoiDcCap_Ma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiDcCap_Ma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiDcCap_Ma.Location = new System.Drawing.Point(122, 106);
            this.txtNguoiDcCap_Ma.Name = "txtNguoiDcCap_Ma";
            this.txtNguoiDcCap_Ma.Size = new System.Drawing.Size(149, 20);
            this.txtNguoiDcCap_Ma.TabIndex = 5;
            this.txtNguoiDcCap_Ma.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiDcCap_Ma.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(327, 211);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 315;
            this.label12.Text = "Ngày nộp ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(277, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 315;
            this.label4.Text = "Tên người được cấp";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox1.Controls.Add(this.lblSoTK);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.lblDenNgay);
            this.uiGroupBox1.Controls.Add(this.txtSoDKCT);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtNguoiKhai_Ma);
            this.uiGroupBox1.Controls.Add(this.dtNgayDKCT);
            this.uiGroupBox1.Controls.Add(this.txtNguoiKhai_Ten);
            this.uiGroupBox1.Controls.Add(this.dtNgayKhaiCT);
            this.uiGroupBox1.Location = new System.Drawing.Point(6, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(598, 108);
            this.uiGroupBox1.TabIndex = 330;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(122, 17);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(149, 22);
            this.donViHaiQuanControl1.TabIndex = 0;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // lblSoTK
            // 
            this.lblSoTK.AutoSize = true;
            this.lblSoTK.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTK.Location = new System.Drawing.Point(15, 24);
            this.lblSoTK.Name = "lblSoTK";
            this.lblSoTK.Size = new System.Drawing.Size(66, 13);
            this.lblSoTK.TabIndex = 323;
            this.lblSoTK.Text = "Mã hải quan";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 323;
            this.label9.Text = "Số đăng ký của CT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(277, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 329;
            this.label10.Text = "Ngày đăng ký CT";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 325;
            this.label7.Text = "Mã người khai";
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.AutoSize = true;
            this.lblDenNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblDenNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDenNgay.Location = new System.Drawing.Point(277, 24);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(100, 13);
            this.lblDenNgay.TabIndex = 329;
            this.lblDenNgay.Text = "Ngày khai chứng từ";
            // 
            // txtSoDKCT
            // 
            this.txtSoDKCT.DecimalDigits = 0;
            this.txtSoDKCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDKCT.FormatString = "#####";
            this.txtSoDKCT.Location = new System.Drawing.Point(122, 45);
            this.txtSoDKCT.MaxLength = 5;
            this.txtSoDKCT.Name = "txtSoDKCT";
            this.txtSoDKCT.Size = new System.Drawing.Size(149, 20);
            this.txtSoDKCT.TabIndex = 2;
            this.txtSoDKCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtSoDKCT.Value = ((ulong)(0ul));
            this.txtSoDKCT.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoDKCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(277, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 325;
            this.label5.Text = "Tên người khai";
            // 
            // txtNguoiKhai_Ma
            // 
            this.txtNguoiKhai_Ma.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiKhai_Ma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKhai_Ma.Location = new System.Drawing.Point(122, 72);
            this.txtNguoiKhai_Ma.Name = "txtNguoiKhai_Ma";
            this.txtNguoiKhai_Ma.Size = new System.Drawing.Size(149, 20);
            this.txtNguoiKhai_Ma.TabIndex = 4;
            this.txtNguoiKhai_Ma.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiKhai_Ma.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // dtNgayDKCT
            // 
            this.dtNgayDKCT.CustomFormat = "dd/MM/yyyy";
            this.dtNgayDKCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayDKCT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayDKCT.Location = new System.Drawing.Point(389, 46);
            this.dtNgayDKCT.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtNgayDKCT.Name = "dtNgayDKCT";
            this.dtNgayDKCT.Size = new System.Drawing.Size(165, 20);
            this.dtNgayDKCT.TabIndex = 3;
            // 
            // txtNguoiKhai_Ten
            // 
            this.txtNguoiKhai_Ten.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNguoiKhai_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKhai_Ten.Location = new System.Drawing.Point(389, 72);
            this.txtNguoiKhai_Ten.Name = "txtNguoiKhai_Ten";
            this.txtNguoiKhai_Ten.Size = new System.Drawing.Size(165, 20);
            this.txtNguoiKhai_Ten.TabIndex = 5;
            this.txtNguoiKhai_Ten.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiKhai_Ten.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // dtNgayKhaiCT
            // 
            this.dtNgayKhaiCT.CustomFormat = "dd/MM/yyyy";
            this.dtNgayKhaiCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayKhaiCT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayKhaiCT.Location = new System.Drawing.Point(389, 20);
            this.dtNgayKhaiCT.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtNgayKhaiCT.Name = "dtNgayKhaiCT";
            this.dtNgayKhaiCT.Size = new System.Drawing.Size(165, 20);
            this.dtNgayKhaiCT.TabIndex = 1;
            // 
            // ChungTuHQTruocDoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 400);
            this.Controls.Add(this.uiGroupBox3);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ChungTuHQTruocDoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chứng từ hải quan trước đó";
            this.Load += new System.EventHandler(this.ChungTuHQTruocDoForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTuNgay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtNgayNoCT;
        private System.Windows.Forms.DateTimePicker dtNgayHHCT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtNgayCT;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiDcCap_Ten;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiDcCap_Ma;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label lblSoTK;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDenNgay;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDKCT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKhai_Ma;
        private System.Windows.Forms.DateTimePicker dtNgayDKCT;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKhai_Ten;
        private System.Windows.Forms.DateTimePicker dtNgayKhaiCT;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.CheckBox chkXinNo;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiPhatHanh_Ten;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiPhatHanh_Ma;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
    }
}