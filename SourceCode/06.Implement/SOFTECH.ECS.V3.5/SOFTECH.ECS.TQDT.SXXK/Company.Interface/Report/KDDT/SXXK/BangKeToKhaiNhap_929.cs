﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiNhap_929 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        public int SoHoSo;
        public BangKeToKhaiNhap_929()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhapForReport(LanThanhLy,GlobalSettings.SoThapPhan.MauBC01,GlobalSettings.MA_DON_VI,GlobalSettings.MA_HAI_QUAN,where).Tables[0];
            
            dt.TableName = "BKToKhaiNhap";
            this.DataSource = dt;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhai");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yyyy}");
            lblNgayHoanThanh.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanh", "{0:dd/MM/yyyy}");
            xrLabel9.Text = xrLabel19.Text = GlobalSettings.TieudeNgay; 
        }

    }
}
