﻿namespace Company.Interface
{
    partial class TKMD_VanDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.txtSoVanDon5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoVanDon4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoVanDon3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSoVanDon2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoVanDon1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbMaKQKT = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoLuongCont = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ccNgayHangDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMaPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKiHieuVaSoHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(729, 399);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiButton1);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon4);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon2);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon1);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 12);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(693, 111);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Danh sách vận đơn";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(587, 78);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(91, 23);
            this.uiButton1.TabIndex = 1;
            this.uiButton1.Text = "Chọn vận đơn";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanDon5
            // 
            this.txtSoVanDon5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon5.Location = new System.Drawing.Point(471, 51);
            this.txtSoVanDon5.MaxLength = 255;
            this.txtSoVanDon5.Name = "txtSoVanDon5";
            this.txtSoVanDon5.Size = new System.Drawing.Size(207, 21);
            this.txtSoVanDon5.TabIndex = 0;
            this.txtSoVanDon5.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(385, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số vận đơn 5";
            // 
            // txtSoVanDon4
            // 
            this.txtSoVanDon4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon4.Location = new System.Drawing.Point(471, 24);
            this.txtSoVanDon4.MaxLength = 255;
            this.txtSoVanDon4.Name = "txtSoVanDon4";
            this.txtSoVanDon4.Size = new System.Drawing.Size(207, 21);
            this.txtSoVanDon4.TabIndex = 0;
            this.txtSoVanDon4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(385, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số vận đơn 4";
            // 
            // txtSoVanDon3
            // 
            this.txtSoVanDon3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon3.Location = new System.Drawing.Point(92, 78);
            this.txtSoVanDon3.MaxLength = 255;
            this.txtSoVanDon3.Name = "txtSoVanDon3";
            this.txtSoVanDon3.Size = new System.Drawing.Size(207, 21);
            this.txtSoVanDon3.TabIndex = 0;
            this.txtSoVanDon3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số vận đơn 3";
            // 
            // txtSoVanDon2
            // 
            this.txtSoVanDon2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon2.Location = new System.Drawing.Point(92, 51);
            this.txtSoVanDon2.MaxLength = 255;
            this.txtSoVanDon2.Name = "txtSoVanDon2";
            this.txtSoVanDon2.Size = new System.Drawing.Size(207, 21);
            this.txtSoVanDon2.TabIndex = 0;
            this.txtSoVanDon2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số vận đơn 2";
            // 
            // txtSoVanDon1
            // 
            this.txtSoVanDon1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon1.Location = new System.Drawing.Point(92, 24);
            this.txtSoVanDon1.MaxLength = 255;
            this.txtSoVanDon1.Name = "txtSoVanDon1";
            this.txtSoVanDon1.Size = new System.Drawing.Size(207, 21);
            this.txtSoVanDon1.TabIndex = 0;
            this.txtSoVanDon1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDon1.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 29);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(70, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số vận đơn 1";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(729, 0);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cbMaKQKT);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongCont);
            this.uiGroupBox1.Controls.Add(this.ccNgayHangDen);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.txtKiHieuVaSoHieu);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 129);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(693, 259);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Thông tin chung";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbMaKQKT
            // 
            this.cbMaKQKT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMaKQKT.DisplayMember = "ID";
            this.cbMaKQKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaKQKT.Location = new System.Drawing.Point(557, 91);
            this.cbMaKQKT.Name = "cbMaKQKT";
            this.cbMaKQKT.Size = new System.Drawing.Size(115, 21);
            this.cbMaKQKT.TabIndex = 6;
            this.cbMaKQKT.ValueMember = "ID";
            this.cbMaKQKT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoLuongCont
            // 
            this.txtSoLuongCont.DecimalDigits = 0;
            this.txtSoLuongCont.Location = new System.Drawing.Point(336, 91);
            this.txtSoLuongCont.Name = "txtSoLuongCont";
            this.txtSoLuongCont.Size = new System.Drawing.Size(113, 21);
            this.txtSoLuongCont.TabIndex = 3;
            this.txtSoLuongCont.Text = "0";
            this.txtSoLuongCont.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongCont.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHangDen
            // 
            // 
            // 
            // 
            this.ccNgayHangDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHangDen.DropDownCalendar.Name = "";
            this.ccNgayHangDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHangDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHangDen.IsNullDate = true;
            this.ccNgayHangDen.Location = new System.Drawing.Point(102, 91);
            this.ccNgayHangDen.Name = "ccNgayHangDen";
            this.ccNgayHangDen.Nullable = true;
            this.ccNgayHangDen.NullButtonText = "Xóa";
            this.ccNgayHangDen.ShowNullButton = true;
            this.ccNgayHangDen.Size = new System.Drawing.Size(122, 21);
            this.ccNgayHangDen.TabIndex = 2;
            this.ccNgayHangDen.TodayButtonText = "Hôm nay";
            this.ccNgayHangDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHangDen.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtTenXepHang);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.txtMaXepHang);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Location = new System.Drawing.Point(461, 132);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(219, 116);
            this.uiGroupBox5.TabIndex = 1;
            this.uiGroupBox5.Text = "Địa điểm xếp hàng";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTenXepHang
            // 
            this.txtTenXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenXepHang.Location = new System.Drawing.Point(64, 47);
            this.txtTenXepHang.MaxLength = 255;
            this.txtTenXepHang.Multiline = true;
            this.txtTenXepHang.Name = "txtTenXepHang";
            this.txtTenXepHang.Size = new System.Drawing.Size(149, 51);
            this.txtTenXepHang.TabIndex = 0;
            this.txtTenXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tên";
            // 
            // txtMaXepHang
            // 
            this.txtMaXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaXepHang.Location = new System.Drawing.Point(64, 20);
            this.txtMaXepHang.MaxLength = 255;
            this.txtMaXepHang.Name = "txtMaXepHang";
            this.txtMaXepHang.Size = new System.Drawing.Size(149, 21);
            this.txtMaXepHang.TabIndex = 0;
            this.txtMaXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Mã";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtTenDoHang);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtMaDoHang);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Location = new System.Drawing.Point(236, 132);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(219, 116);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Địa điểm dở hàng";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDoHang
            // 
            this.txtTenDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoHang.Location = new System.Drawing.Point(64, 47);
            this.txtTenDoHang.MaxLength = 255;
            this.txtTenDoHang.Multiline = true;
            this.txtTenDoHang.Name = "txtTenDoHang";
            this.txtTenDoHang.Size = new System.Drawing.Size(149, 51);
            this.txtTenDoHang.TabIndex = 0;
            this.txtTenDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tên";
            // 
            // txtMaDoHang
            // 
            this.txtMaDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoHang.Location = new System.Drawing.Point(64, 20);
            this.txtMaDoHang.MaxLength = 255;
            this.txtMaDoHang.Name = "txtMaDoHang";
            this.txtMaDoHang.Size = new System.Drawing.Size(149, 21);
            this.txtMaDoHang.TabIndex = 0;
            this.txtMaDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mã";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.txtTenPTVT);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtMaPTVT);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Location = new System.Drawing.Point(11, 132);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(219, 116);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Phương tiện vận chuyển";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtTenPTVT
            // 
            this.txtTenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVT.Location = new System.Drawing.Point(64, 47);
            this.txtTenPTVT.MaxLength = 255;
            this.txtTenPTVT.Multiline = true;
            this.txtTenPTVT.Name = "txtTenPTVT";
            this.txtTenPTVT.Size = new System.Drawing.Size(149, 51);
            this.txtTenPTVT.TabIndex = 0;
            this.txtTenPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Tên PTVT";
            // 
            // txtMaPTVT
            // 
            this.txtMaPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPTVT.Location = new System.Drawing.Point(64, 20);
            this.txtMaPTVT.MaxLength = 255;
            this.txtMaPTVT.Name = "txtMaPTVT";
            this.txtMaPTVT.Size = new System.Drawing.Size(149, 21);
            this.txtMaPTVT.TabIndex = 0;
            this.txtMaPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã PTVT";
            // 
            // txtKiHieuVaSoHieu
            // 
            this.txtKiHieuVaSoHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKiHieuVaSoHieu.Location = new System.Drawing.Point(102, 20);
            this.txtKiHieuVaSoHieu.MaxLength = 255;
            this.txtKiHieuVaSoHieu.Multiline = true;
            this.txtKiHieuVaSoHieu.Name = "txtKiHieuVaSoHieu";
            this.txtKiHieuVaSoHieu.Size = new System.Drawing.Size(576, 64);
            this.txtKiHieuVaSoHieu.TabIndex = 0;
            this.txtKiHieuVaSoHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKiHieuVaSoHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(458, 95);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Mã KQKT Nội dung";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(231, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số lượng Container";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ngày hàng đến";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Kí hiệu và số hiệu";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TKMD_VanDonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 399);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_VanDonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận đơn";
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon1;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtKiHieuVaSoHieu;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPTVT;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenXepHang;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaXepHang;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoHang;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoHang;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCont;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHangDen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIComboBox cbMaKQKT;
    }
}