﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
    [XmlRoot("Declaration")]
   public class ScrapInformation_VNACCS
    {

        /// <summary>
        /// Ngày khai báo
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }
        // <summary>
        /// Loại chứng từ = 331
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Chức năng của chứng từ = 8
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Tình trạng của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// Mã  
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }

        /// <summary>
        /// Số đăng ký
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Ngày đăng ký chứng từ
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }

        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        /// <summary>
        /// Thông tin về hàng hóa xin tiêu hủy
        /// </summary>
        [XmlElement("Scrap")]
        public List<Scrap> Scrap { get; set; }
    }
}
