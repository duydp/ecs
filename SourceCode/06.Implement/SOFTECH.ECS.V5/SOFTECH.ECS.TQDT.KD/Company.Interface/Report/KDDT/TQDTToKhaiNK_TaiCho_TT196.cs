﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using Company.KDT.SHARE.Components;
using System.Linq;

namespace Company.Interface.Report
{
    public partial class TQDTToKhaiNK_TaiCho_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKNTQDT_TaiCho_FormTT196 report;
        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;
        public bool inTriGiaTT = false;
        public bool inThueBVMT = false;
        public string SoLuongPhuLucTK = "0";
        public string SoHoaDonGTGT = "";
        public string NgayHoaDonGTGT = "";
        public TQDTToKhaiNK_TaiCho_TT196()
        {
            InitializeComponent();
        }

        public void BindReport()
        {
            try
            {
                ResetEmpty();
                float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
                this.PrintingSystem.ShowMarginsWarning = false;
#if KD_V3 || KD_V4
                double tongTriGiaNT = 0;
                double tongTienThueXNK = 0;
                double tongTienThueTatCa = 0;
                double tongThueGTGT = 0;
                double tongThueBVMT = 0;
                double TriGiaTTTTDB;
                double TriGiaTTGTGT;
                double tongThueTTDB = 0;
                double TriGiaTTBVM = 0;
                double TienThueBVMT = 0;
                int formatSoLuong = GlobalSettings.SoThapPhan.SoLuongHMD;
                int formatDonGiaNT = GlobalSettings.SoThapPhan.DonGiaNT;
                int formatTriGiaNT = GlobalSettings.SoThapPhan.TriGiaNT;
#elif GC_V3 || GC_V4
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;
                decimal TriGiaTTTTDB;
                decimal TriGiaTTGTGT;
                decimal TriGiaTTBVM = 0;
                decimal TienThueBVMT = 0;
                decimal tongThueTTDB = 0;
                decimal tongThueBVMT = 0;
                   int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
#elif SXXK_V3 || SXXK_V4
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongThueTTDB = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongThueGTGT = 0;
                decimal tongThueBVMT = 0;
                decimal tongTriGiaThuKhac = 0;
                decimal TriGiaTTTTDB;
                decimal TriGiaTTGTGT;
                decimal TriGiaTTBVM = 0;
                decimal TienThueBVMT = 0;
                int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
                int formatDonGiaNT = GlobalSettings.DonGiaNT;
                int formatTriGiaNT = GlobalSettings.TriGiaNT;
#endif

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh

                //Cuc HQ
                string maCuc = TKMD.MaHaiQuan.Substring(1, 2);
                lblCucHaiQuan.Text = DonViHaiQuan.GetName("Z" + maCuc + "Z");
                lblCucHaiQuan.Font = new Font("Times New Roman", sizeFont);
                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = this.TKMD.MaHaiQuan + " - " + DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", sizeFont);

                //Chi cuc Hai quan cua khau
                //lblTenChiCucHQCuaKhau.Text = GetChiCucHQCK(); /*this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);*/
                // lblTenChiCucHQCuaKhau.Font = new Font("Times New Roman", sizeFont);

                //So tham chieu
                if (this.TKMD.SoTiepNhan > 0)
                    this.lblSoThamChieu.Text = this.TKMD.SoTiepNhan.ToString();
                else
                    this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                if (this.TKMD.NgayTiepNhan > minDate)
                    lblNgaySoThamChieu.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai + "";

                //Ngày giờ đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";

                //So luong phu luc to khai
//                 if (this.TKMD.HMDCollection.Count > 3)
//                     lblSoPhuLucToKhai.Text = this.TKMD.HMDCollection.Count > 0 ? ((this.TKMD.HMDCollection.Count - 1) / 3 + 1).ToString("00") : "0";
//                 else if (this.TKMD.HMDCollection.Count > 1)
                    lblSoPhuLucToKhai.Text = SoLuongPhuLucTK;
//                 else
//                     lblSoPhuLucToKhai.Text = "00";
                //01.Nguoi Xuat khau
                lblNguoiXK.Font = lblMSTNguoiXuat.Font = new Font("Times New Roman", sizeFont);
                lblNguoiXK.Text = TKMD.TenDonViDoiTac.ToString();
                //lblMSTNguoiXuat.Text=

                //02.Nguoi nhap khau
                lblNguoiNK.Font = new Font("Times New Roman", sizeFont);
                if (Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap")))
                    lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + ".  " + GlobalSettings.DIA_CHI;
                else
                    lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + ".  " + GlobalSettings.DIA_CHI;
                //MST Nguoi Nhap khau
                lblMSTNguoiNK.Text = this.TKMD.MaDoanhNghiep;

                //03.Dơn vi uy thac
                lblTenNguoiUyThac.Font = new Font("Times New Roman", sizeFont);
                lblTenNguoiUyThac.Text = TKMD.TenDonViUT;
                lblMSTNguoiUT.Text = this.TKMD.MaDonViUT;
                lblMSTNguoiUT.Font = new Font("Times New Roman", sizeFont);
                //04.Dai ly Hai quan
                lblTenDaiLy.Font = new Font("Times New Roman", sizeFont);
                lblTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;
                lblMSTHaiQuan.Text = this.TKMD.MaDaiLyTTHQ;

                //05. Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", sizeFont);

                //06. Hoa Don thuong mai
                lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;
                lblHoaDonThuongMai.Font = new Font("Times New Roman", sizeFont);
                if (this.TKMD.NgayHoaDonThuongMai > minDate)
                    lblNgayHoaDonThuongMai.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                else
                    lblNgayHoaDonThuongMai.Text = "";
                lblNgayHoaDonThuongMai.Font = new Font("Times New Roman", sizeFont);
                //07. Hóa đơn GTGT
                lblSoHoaDonGTGT.Text = SoHoaDonGTGT;
                lblNgayHoaDonGTGT.Text = NgayHoaDonGTGT;

                //08. GiayPhep
                if (TKMD.SoGiayPhep != "")
                    lblSoGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
                else
                    lblSoGiayPhep.Text = "";
                lblSoGiayPhep.Font = new Font("Times New Roman", sizeFont);
                if (this.TKMD.NgayGiayPhep > minDate)
                    lblNgayGP.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayGP.Text = "";
                lblNgayHetHanGP.Font = new Font("Times New Roman", sizeFont);
                if (this.TKMD.NgayHetHanGiayPhep > minDate)
                    lblNgayHetHanGP.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanGP.Text = "";
                lblNgayHetHanGP.Font = new Font("Times New Roman", sizeFont);

                //09. Hop dong
                lblSoHD.Text = "" + this.TKMD.SoHopDong;
                lblSoHD.Font = new Font("Times New Roman", sizeFont);
                if (this.TKMD.NgayHopDong > minDate)
                    lblNgayHD.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHD.Text = " ";
                lblNgayHD.Font = new Font("Times New Roman", sizeFont);
                if (this.TKMD.NgayHetHanHopDong > minDate)
                    lblNgayHetHanHD.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHD.Text = " ";
                lblNgayHetHanHD.Font = new Font("Times New Roman", sizeFont);

                ////09. VanTaiDon
                //lblVanTaiDon.Text = this.TKMD.SoVanDon;
                //lblVanTaiDon.Font = new Font("Times New Roman", sizeFont);
                //if (this.TKMD.NgayVanDon > minDate)
                //    lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                //else
                //    lblNgayVanTaiDon.Text = "";
                //lblNgayVanTaiDon.Font = new Font("Times New Roman", sizeFont);

                //10. CangXepHang
                //lblCangXepHang.Text = this.TKMD.DiaDiemXepHang;
                //lblCangXepHang.Font = new Font("Times New Roman", sizeFont);
                lblMaDiaDiemNhanHang.Text = this.TKMD.CuaKhau_ID;
                lblMaDiaDiemNhanHang.Font = new Font("Times New Roman", sizeFont);
                lblDiaDiemNhanHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblDiaDiemNhanHang.Font = new Font("Times New Roman", sizeFont);
                ////11. CangDoHang
                //lblMaCangdoHang.Text = this.TKMD.CuaKhau_ID;
                //lblMaCangdoHang.Font = new Font("Times New Roman", sizeFont);
                //lblTenCangDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                //lblTenCangDoHang.Font = new Font("Times New Roman", sizeFont);

                ////12. MaPTVT
                //lblPhuongTienVanTai.Font = new Font("Times New Roman", sizeFont);
                //lblPhuongTienVanTai.Text = PhuongThucVanTai.getName(TKMD.PTVT_ID);
                ////Ten, So hieu PTVT
                //lblTenSoHieuPTVT.Font = new Font("Times New Roman", sizeFont);
                //if (TKMD.VanTaiDon != null)
                //    lblTenSoHieuPTVT.Text = TKMD.VanTaiDon.TenPTVT + ", " + this.TKMD.VanTaiDon.SoHieuPTVT;
                //else
                //    lblTenSoHieuPTVT.Text = " " + this.TKMD.SoHieuPTVT;
                ////Ngay den PTVT
                //if (this.TKMD.NgayDenPTVT > minDate)
                //    lblNgayDenPTVT.Text = this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
                //else
                //    lblNgayDenPTVT.Text = "";

                ////13. NuocXK
                //lblNuocXK.Font = new Font("Times New Roman", sizeFont);
                //lblNuocXK.Text = this.TKMD.NuocXK_ID;
                //lblTenNuoc.Font = new Font("Times New Roman", sizeFont);
                //lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);

                //14. DieuKienGiaoHang
                lblDieuKienGiaoHang.Font = new Font("Times New Roman", sizeFont);
                lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;

                //15.PhuongThucThanhToan
                lblPhuongThucThanhToan.Font = new Font("Times New Roman", sizeFont);
                lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;

                //16. DongTienThanhToan
                lblDongTienThanhToan.Font = new Font("Times New Roman", sizeFont);
                lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;

                //17. TyGiaTinhThue
                lblTyGiaTinhThue.Font = new Font("Times New Roman", sizeFont);
                lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");

                #endregion

                //18. Thong tin hang hoa va thue
                if (TKMD.HMDCollection.Count > 1)
                {
                    lblMoTaHang1.Text = "(theo phụ lục tờ khai)";
                }
                else
                {
                    #region 1 hàng

                    HangMauDich hmd = this.TKMD.HMDCollection[0];

                    //18. Mo ta hang hoa - Ten hang
                    if (!InMaHang)
                        lblMoTaHang1.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            lblMoTaHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            lblMoTaHang1.Text = hmd.TenHang;
                    }

                    lblMoTaHang1.WordWrap = true;
                    lblMoTaHang1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));

                    //19. Ma so hang hoa - Ma HS
                    lblMaHSHang1.Text = hmd.MaHS;
                    //20. Xuat xu
                    lblXuatXuHang1.Text = hmd.NuocXX_ID;
#if KD_V3 || KD_V4
                    //21. Che do uu dai
                    lblCheDoUuDaiHang1.Text = LoaiCO.GetName(hmd.CheDoUuDai);
#endif
                    //22. luong hang
                    lblLuongHang1.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, true));
                    //23.Don vi tinh
                    lblDVTHang1.Text = DonViTinh.GetName(hmd.DVT_ID);

                    //Hang thue F.O.C
                    if (hmd.FOC)
                    {
                        #region F.O.C

                        //24. Don gia nguyen te
                        lblDonGiaHang1.Text = "F.O.C";
                        //25. Tri gia nguyen te
                        lblTGNTHang1.Text = "F.O.C";

                        //26. Thue Nhap khau - NK
                        //Tri gia tinh thue
                        lblTGTTNKHang1.Text = "";
                        //Thue suat
                        lblThueSuatNKHang1.Text = "";
                        //Tien thue
                        lblTienThueNKHang1.Text = "";

                        //27. Thue thieu thu dac biet - TTDB
                        lblTGTTTTDBHang1.Text = "";
                        lblThueSuatTTDBHang1.Text = "";
                        lblTienThueTTDBHang1.Text = "";

                        //28. Thue Bao ve moi truong - BVMT
                        lblTGTTBVMTHang1.Text = "";
                        lblThueSuatBVMTHang1.Text = "";
                        lblTienThueBVMTHang1.Text = "";

                        //29. Thue gia tri gia tang - VAT
                        lblTGTTVATHang1.Text = "";
                        lblThueSuatVATHang1.Text = "";
                        lblTienThueVATHang1.Text = "";

                        #endregion
                    }
                    //Hang tinh thue 
                    else
                    {
                        //24. Don gia nguyen te
                        lblDonGiaHang1.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatDonGiaNT, true));
                        //25. Tri gia nguyen te
                        lblTGNTHang1.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));

                        //Tổng Trị gia nguyên tệ
                        lblTongTGNT.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));

                        //26. Thue Nhap khau - NK
                        //Tri gia tinh thue
                        lblTGTTNKHang1.Text = inTriGiaTT ? hmd.TriGiaTT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        if (hmd.ThueSuatXNK > 0)
                        {
                            lblTGTTNKHang1.Text = hmd.TriGiaTT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                            //Thue suat
                            if (hmd.ThueTuyetDoi)
                            {
                                lblThueSuatNKHang1.Text = "??";
                            }
                            else
                            {
                                /*   if (hmd.ThueSuatXNKGiam == 0)*/
                                lblThueSuatNKHang1.Text = hmd.ThueSuatXNK.ToString("N0");
                                //                             else
                                //                                 lblThueSuatNKHang1.Text = hmd.ThueSuatXNKGiam.ToString();
                            }
                            //Tien thue
                            lblTienThueNKHang1.Text = hmd.ThueXNK.ToString("N0");
                        }
                        //27. Thue thieu thu dac biet - TTDB 
                        TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        lblTGTTTTDBHang1.Text = inTriGiaTT ? TriGiaTTTTDB.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        if (hmd.ThueSuatTTDB > 0)
                        {
                            lblTGTTTTDBHang1.Text = TriGiaTTTTDB.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                            /* if (hmd.ThueSuatTTDBGiam == 0)*/
                            lblThueSuatTTDBHang1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //                         else
                            //                             lblThueSuatTTDBHang1.Text = hmd.ThueSuatTTDBGiam.ToString();
                            lblTienThueTTDBHang1.Text = hmd.ThueTTDB.ToString("N0");

                        }
                        //28. Thue Bao ve moi truong - BVMT
                        TriGiaTTBVM = hmd.TriGiaTT;
#if KD_V4 || GC_V4 || SXXK_V4

                        TienThueBVMT = hmd.ThueBVMT;
                        lblTGTTBVMTHang1.Text = (inTriGiaTT && inThueBVMT) ? TriGiaTTBVM.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        if (hmd.ThueSuatBVMT != 0)
                            lblThueSuatBVMTHang1.Text = hmd.ThueSuatBVMT.ToString();
                        else
                            lblThueSuatBVMTHang1.Text = "";
                        if (hmd.ThueBVMT > 0)
                        {
                            lblTienThueBVMTHang1.Text = hmd.ThueBVMT.ToString("N0");
                            lblTGTTBVMTHang1.Text = (inThueBVMT) ? TriGiaTTBVM.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        }
#else
                        lblTGTTBVMTHang1.Text = (inTriGiaTT && inThueBVMT) ? TriGiaTTBVM.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        if (hmd.TyLeThuKhac != 0)
                            lblThueSuatBVMTHang1.Text = hmd.TyLeThuKhac.ToString();
                        else
                            lblThueSuatBVMTHang1.Text = "";
                        if (hmd.TriGiaThuKhac > 0)
                        {
                            lblTienThueBVMTHang1.Text = hmd.TyLeThuKhac.ToString("N0");
                            lblTGTTBVMTHang1.Text = (inThueBVMT) ? TriGiaTTBVM.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        }

#endif

                        //29. Thue gia tri gia tang - VAT
#if KD_V4 || GC_V4 || SXXK_V4
                        TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB + TienThueBVMT;
                        lblTGTTVATHang1.Text = inTriGiaTT ? TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;

                        lblThueSuatVATHang1.Text = (hmd.ThueSuatGTGT > 0) ? hmd.ThueSuatGTGT.ToString("") : string.Empty;
                        if (hmd.ThueGTGT > 0)
                        {
                            lblTGTTVATHang1.Text = TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                            lblTienThueVATHang1.Text = hmd.ThueGTGT.ToString("N0");
                        }
                        //lblTienThueVATHang1.Text = hmd.ThueGTGT.ToString("N0");

#else
                        TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB + hmd.TriGiaThuKhac;
                        lblTGTTVATHang1.Text = inTriGiaTT ? TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false))) : string.Empty;
                        if (hmd.ThueSuatGTGT > 0)
                            lblThueSuatVATHang1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            lblThueSuatVATHang1.Text = string.Empty;
                        if (hmd.ThueGTGT > 0)
                        {
                            lblTGTTVATHang1.Text = TriGiaTTGTGT.ToString("N0") + string.Format(" / {0}", hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatSoLuong, false)));
                            lblTienThueVATHang1.Text = hmd.ThueGTGT.ToString("N0");
                        }

                        /*  if (hmd.ThueSuatVATGiam == 0)*/
                        //lblThueSuatVATHang1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //                         else
                        //                             lblThueSuatVATHang1.Text = hmd.ThueSuatVATGiam.ToString();

#endif

                    }

                    // tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                }

                foreach (HangMauDich hmd in TKMD.HMDCollection)
                {

#if KD_V4 || GC_V4 || SXXK_V4
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.ThueBVMT;
                    tongTienThueXNK = tongTienThueXNK + hmd.ThueXNK;
                    tongThueTTDB = tongThueTTDB + hmd.ThueTTDB;
                    tongThueGTGT = tongThueGTGT + hmd.ThueGTGT;
                    tongThueBVMT = tongThueBVMT + hmd.ThueBVMT;


                    //// In tổng tiển thuế khi to khai có Phu luc
                    //if (tongThueBVMT > 0)
                    //    lblTienThueBVMTHang1.Text = tongThueBVMT.ToString("N0");
                    //if (tongTienThueXNK > 0)
                    //    lblTienThueNKHang1.Text = tongTienThueXNK.ToString("N0");
                    //if (tongThueTTDB > 0)
                    //    lblTienThueTTDBHang1.Text = tongThueTTDB.ToString("N0");
                    //if (tongThueGTGT > 0)
                    //    lblTienThueVATHang1.Text = tongThueGTGT.ToString("N0");

#else
                    tongTienThueXNK = tongTienThueXNK + hmd.ThueXNK;
                    tongThueTTDB = tongThueTTDB + hmd.ThueTTDB;
                    tongThueGTGT = tongThueGTGT + hmd.ThueGTGT;
                    tongThueBVMT = tongThueBVMT + hmd.TriGiaThuKhac;

                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
#endif
                    tongTriGiaNT = tongTriGiaNT + hmd.TriGiaKB;

                }

                // In tổng tiển thuế khi to khai có Phu luc
                if (tongThueBVMT > 0)
                    lblTienThueBVMTHang1.Text = tongThueBVMT.ToString("N0");
                if (tongTienThueXNK > 0)
                    lblTienThueNKHang1.Text = tongTienThueXNK.ToString("N0");
                if (tongThueTTDB > 0)
                    lblTienThueTTDBHang1.Text = tongThueTTDB.ToString("N0");
                if (tongThueGTGT > 0)
                    lblTienThueVATHang1.Text = tongThueGTGT.ToString("N0");
                if (TKMD.HMDCollection.Count > 1)
                    lblTGNTHang1.Text = ""; // tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));

                //Sau o 25. Thong tin Tong tri gia nguyen te
                lblTongTGNT.Text = tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));

                lblTongTienThue.Text = tongTienThueTatCa.ToString("N0") + " VNĐ";
                string s = VNCurrency.ToString(tongTienThueTatCa).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                lblBangChu.Text = s.Replace("  ", " ");

                    #endregion


                //25. Thong tin Container
                ReportContainer(TKMD);

                //Sau o 25. Thong tin Le phi 
                ReportLePhi(TKMD);

                //32. Chung tu di kem
                string ctk = "";
                ctk += TKMD.VanTaiDon != null ? "Vận đơn" : "";
                ctk += (TKMD.GiayPhepCollection.Count != 0 ? "; " + string.Format("Giấy phép ({0})", TKMD.GiayPhepCollection.Count) : "");
                ctk += (TKMD.HoaDonThuongMaiCollection.Count != 0 ? "; " + string.Format("Hóa đơn ({0})", TKMD.HoaDonThuongMaiCollection.Count) : "");
                ctk += (TKMD.HopDongThuongMaiCollection.Count != 0 ? "; " + string.Format("Hợp đồng ({0})", TKMD.HopDongThuongMaiCollection.Count) : "");
                ctk += (TKMD.COCollection.Count != 0 ? "; " + string.Format("CO ({0})", TKMD.COCollection.Count) : "");
                ctk += (TKMD.listChuyenCuaKhau.Count != 0 ? "; " + string.Format("Đề nghị chuyển cửa khẩu ({0})", TKMD.listChuyenCuaKhau.Count) : "");
                ctk += (TKMD.listCTDK.Count != 0 ? "; " + string.Format("Chứng từ kèm dạng ảnh ({0})", TKMD.listCTDK.Count) : "");
                ctk += (TKMD.ChungTuNoCollection.Count != 0 ? "; " + string.Format("Chứng từ nợ ({0})", TKMD.ChungTuNoCollection.Count) : "");
#if KD_V4 || SXXK_V4 || GC_V4
                ctk += (TKMD.GiayKiemTraCollection.Count != 0 ? "; " + string.Format("Giấy kiểm tra ({0})", TKMD.GiayKiemTraCollection.Count) : "");
                ctk += (TKMD.ChungThuGD.ID != 0 ? "; " + "Chứng thư giám định" : "");
#endif
                if (ctk != "")
                {
                    ctk = ctk.Substring(0, 1).Equals(";") ? ctk.Remove(0, 1) : ctk;
                    ctk = ctk.Substring(ctk.Length - 1, 1).Equals(";") ? ctk.Remove(ctk.Length - 1, 1) : ctk;
                }

                lblChungTuDiKem.Font = new Font("Times New Roman", sizeFont);
                lblChungTuDiKem.Text = ctk;

                //33. Ky cam doan
                //Ngay thang nam in to khai
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    lblNgayIn.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayIn.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;

                //35. Ghi chep khac
                lblGhiChepKhac.Font = new Font("Times New Roman", sizeFont);
                lblGhiChepKhac.Text = "35.Ghi chép khác: " + TKMD.DeXuatKhac;

                //34. Ket qua va huong dan phan luong
                lblKetQuaPhanLuong.Font = new Font("Times New Roman", sizeFont);
                lblKetQuaPhanLuong.Text = Company.KDT.SHARE.Components.ConvertFont.TCVN3ToUnicode(TKMD.HUONGDAN);
                //if (TKMD.PhanLuong != "")
                //{
                //    lblKetQuaPhanLuong.Text += TKMD.HUONGDAN != "" ? " - " + TKMD.HUONGDAN : "";
                //}

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {
                //Cuc HQ
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lblTenChiCucHQCuaKhau.Text = "";

                //So tham chieu
                this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";

                //01.Nguoi Xuat khau
                lblNguoiXK.Text = "";
                lblMSTNguoiXuat.Text = "";
                //02.Nguoi nhap khau
                lblNguoiNK.Text = lblMSTNguoiNhapKhau.Text = "";

                //03.Dơn vi uy thac
                //lblTenNguoiUyThac.Text = lblMSTNguoiUyThac.Text = "";

                //04.Dai ly Hai quan
                lblTenDaiLy.Text = lblMSTHaiQuan.Text = "";

                //05. Ma Loai Hinh
                lblLoaiHinh.Text = "";

                //06. Hoa Don thuong mai
                lblHoaDonThuongMai.Text = lblNgayHoaDonThuongMai.Text = "";

                //07. GiayPhep
                //lableGiayPhep.Text = lblNgayHoaDonGTGT.Text = "";

                //08. Hop dong
                //lableHopDong.Text = lblNgayGP.Text = " ";

                //09. VanTaiDon
                //lblVanTaiDon.Text = lblNgayVanTaiDon.Text = "";

                //10. CangXepHang
                //lblCangXepHang.Text = "";

                //11. CangDoHang
                //lblMaCangdoHang.Text = lblTenCangDoHang.Text = "";

                //12. MaPTVT
                // lblPhuongTienVanTai.Text = lblTenSoHieuPTVT.Text = lblNgayDenPTVT.Text = "";

                //13. NuocXK
                // lblNuocXK.Text = lblTenNuoc.Text = "";

                //14. DieuKienGiaoHang
                lblDieuKienGiaoHang.Text = "";

                //15.PhuongThucThanhToan
                lblPhuongThucThanhToan.Text = "";

                //16. DongTienThanhToan
                lblDongTienThanhToan.Text = "";

                //17. TyGiaTinhThue
                lblTyGiaTinhThue.Text = "";

                //32. Chung tu di kem
                lblChungTuDiKem.Text = "";

                //33. Ky cam doan
                //Ngay thang nam in to khai
                lblNgayIn.Text = "Ngày...tháng...năm.....";

                //34. Ket qua va huong dan phan luong
                lblKetQuaPhanLuong.Text = "";

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private string ReportContainer(ToKhaiMauDich objTKMD)
        {
            string tongCont = "";
            float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
            try
            {
                if (objTKMD.VanTaiDon != null)
                {
                    if (objTKMD.VanTaiDon.ContainerCollection != null && objTKMD.VanTaiDon.ContainerCollection.Count == 0)
                        objTKMD.VanTaiDon.LoadContainerCollection();

                    if (objTKMD.VanTaiDon.ContainerCollection != null && objTKMD.VanTaiDon.ContainerCollection.Count < 4)
                    {
                        for (int i = 0; i < objTKMD.VanTaiDon.ContainerCollection.Count; i++)
                        {
                            //So hieu Contianer
                            this.xrTable4.Rows[i].Controls["lblSoHieuCont" + (i + 1)].Text = string.Format("{0}/{1}", objTKMD.VanTaiDon.ContainerCollection[i].SoHieu, objTKMD.VanTaiDon.ContainerCollection[i].Seal_No);
                            //So luong kien cua container
                            //int countSoLuongKien = objTKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == objTKMD.VanTaiDon.ContainerCollection[i].SoHieu).GroupBy(x => x.SoHieuKien).Count();
#if KD_V4 || SXXK_V4 || GC_V4
                            double countSoLuongKien = objTKMD.VanTaiDon.ContainerCollection[i].SoKien;
                            this.xrTable4.Rows[i].Controls["lblSoLuongKienCont" + (i + 1)].Text = string.Format("{0:00}", countSoLuongKien);
                            //Trong luong kien cua container
                            //double countTrongLuong = objTKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == objTKMD.VanTaiDon.ContainerCollection[i].SoHieu).Sum(x => x.TrongLuong);
                            double countTrongLuong = objTKMD.VanTaiDon.ContainerCollection[i].TrongLuong;
                            this.xrTable4.Rows[i].Controls["lblTrongLuongCont" + (i + 1)].Text = string.Format("{0:N3}", countTrongLuong);
                            //Dia diem dong hang
                            this.xrTable4.Rows[i].Controls["lblDiaDiem" + (i + 1)].Text = objTKMD.VanTaiDon.ContainerCollection[i].DiaDiemDongHang;
#endif
                        }
                    }
                    else
                        lblSoHieuCont1.Text = "(Chi tiết phụ lục đính kèm)";

#if KD_V4 || SXXK_V4 || GC_V4
                    //Tong trong luong container
                    double sumTrongLuong = objTKMD.VanTaiDon.ContainerCollection.Sum(x => x.TrongLuong);
                    lblTongTrongLuong.Text = string.Format("{0:N3}", sumTrongLuong);
#endif

                    //Tinh tong cont
                    if (objTKMD.VanTaiDon.ContainerCollection != null && objTKMD.VanTaiDon.ContainerCollection.Count != 0)
                    {
#if KD_V3 || SXXK_V3 || GC_V3 || KD_V4 || SXXK_V4 || GC_V4
                        tongCont = "";

                        double sumCont20 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container20).ToString()));
                        if (sumCont20 > 0)
                            tongCont += string.Format("Cont20: {0}", sumCont20);

                        double sumCont40 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container40).ToString()));
                        if (sumCont40 > 0)
                            tongCont += "; " + string.Format("Cont40: {0}", sumCont40);

#elif KD_V4 || SXXK_V4 || GC_V4
                    double sumCont45 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container45).ToString()));
                    if (sumCont45 > 0)
                        tongCont += "; " + string.Format("Cont45: {0}", sumCont45);

                    double sumContKhac = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.ContainerKhac).ToString()));
                    if (sumContKhac > 0)
                        tongCont += "; " + string.Format("Cont45: {0}", sumContKhac);
#endif
                    }
                    else
                    {
                        tongCont = "";
#if KD_V3 || SXXK_V3 || GC_V3
                    
                    if (TKMD.SoContainer20 != 0)
                        tongCont += string.Format("Cont20: {0}", TKMD.SoContainer20);

                    if (TKMD.SoContainer40 != 0)
                        tongCont += "; " + string.Format("Cont40: {0}", TKMD.SoContainer40);

#elif KD_V4 || SXXK_V4 || GC_V4
                        if (TKMD.SoLuongContainer.Cont20 != 0)
                            tongCont += string.Format("Cont20: {0}", TKMD.SoLuongContainer.Cont20);
                        if (TKMD.SoLuongContainer.Cont40 != 0)
                            tongCont += string.Format("Cont40: {0}", TKMD.SoLuongContainer.Cont40);
                        if (TKMD.SoLuongContainer.Cont45 != 0)
                            tongCont += string.Format("Cont45: {0}", TKMD.SoLuongContainer.Cont45);
                        if (TKMD.SoLuongContainer.ContKhac != 0)
                            tongCont += string.Format("Cont khác: {0}", TKMD.SoLuongContainer.ContKhac);
#endif
                    }
                }
                //Khong co Van don, lay thong tin cont ngoai to khai chinh
                else
                {


                    tongCont = "";
#if KD_V3 || SXXK_V3 || GC_V3
                    
                    if (TKMD.SoContainer20 != 0)
                        tongCont += string.Format("Cont20: {0}", TKMD.SoContainer20);

                    if (TKMD.SoContainer40 != 0)
                        tongCont += "; " + string.Format("Cont40: {0}", TKMD.SoContainer40);

#elif KD_V4 || SXXK_V4 || GC_V4
                    if (TKMD.SoLuongContainer.Cont20 != 0)
                        tongCont += string.Format("Cont20: {0}", TKMD.SoLuongContainer.Cont20);
                    if (TKMD.SoLuongContainer.Cont40 != 0)
                        tongCont += string.Format("Cont40: {0}", TKMD.SoLuongContainer.Cont40);
                    if (TKMD.SoLuongContainer.Cont45 != 0)
                        tongCont += string.Format("Cont45: {0}", TKMD.SoLuongContainer.Cont45);
                    if (TKMD.SoLuongContainer.ContKhac != 0)
                        tongCont += string.Format("Cont khác: {0}", TKMD.SoLuongContainer.ContKhac);
#endif
                }

                //tong container
#if KD_V3 || SXXK_V3 || GC_V3 || KD_V4 || SXXK_V4 || GC_V4
                lblTongCont.Text = RemoveCharacterString(tongCont, ";");
                lblTongCont.Font = new Font("Times New Roman", sizeFont);
                lblTongTrongLuong.Text = objTKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)";
                lblTongKien.Text = objTKMD.SoKien.ToString("N0") + "(kiện)";
#endif

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return string.Empty;
        }

        private void ReportLePhi(ToKhaiMauDich TKMD)
        {
            try
            {
                string st = "";
                float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
                if (this.TKMD.PhiBaoHiem > 0)
                    st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
                if (this.TKMD.PhiVanChuyen > 0)
                    st += "; F = " + this.TKMD.PhiVanChuyen.ToString("N2");
                if (this.TKMD.PhiKhac > 0)
                    st += "; Phí khác = " + this.TKMD.PhiKhac.ToString("N2");

                lblLePhi.Text = RemoveCharacterString(st, ";");
                lblLePhi.Font = new Font("Times New Roman", sizeFont);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string RemoveCharacterString(string value, string character)
        {
            try
            {
                //Tra ve chuoi rong neu gia tri null
                if (value == null || value == "")
                    return string.Empty;

                //loai bo dau ';' o dau chuoi (Neu co)
                if (value.Substring(0, 1) == character)
                {
                    value = value.Remove(0, 1);
                }

                //loai bo dau ';' o cuoi chuoi (Neu co)
                if (value.Substring(value.Length - 1, 1) == character)
                {
                    value = value.Remove(value.Length - 1, 1);
                }

                //Tra lai gia tri chuoi sau khi loai bo dau ';' o dau va cuoi chuoi (Neu co)
                return value;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            //Neu loi xay ra thi tra lai gia tri chinh no
            return value;
        }
    }
}
