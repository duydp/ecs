﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC03_DMDKForm : BaseForm
    {
        public DinhMucDangKy DMDangKy;
        //private BangKe03_DMDK BC03 = new BangKe03_DMDK();
        //private BangKe03_DMDK BC03All = new BangKe03_DMDK();
        private Mau03_TBDM_GC_TT74 BC03 = new Mau03_TBDM_GC_TT74();
        private Mau03_TBDM_GC_TT74 BC03All = new Mau03_TBDM_GC_TT74();
        public HopDong HD = new HopDong();
        public DataSet dsDinhMuc = new DataSet();
        public DataSet dsDM = new DataSet();
        public DataTable dtBangDinhMuc = new DataTable();
        Company.GC.BLL.KDT.GC.DinhMuc DM = new DinhMuc();
        public Company.GC.BLL.KDT.GC.NguyenPhuLieu NPL = new NguyenPhuLieu();
        //private int TongSoSPXuLy = 0;
        private int TongSoSP = 0;
        public ReportViewBC03_DMDKForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            HD.ID = DMDangKy.ID_HopDong;
            HD = HopDong.Load(HD.ID);                
            if (DMDangKy.DMCollection.Count == 0 && DMDangKy.ID > 0)
                this.DMDangKy.LoadCollection();
            foreach (DinhMuc dm in this.DMDangKy.DMCollection)
            {
                if (!CheckMaSPExist(dm.MaSanPham))
                    cbPage.Items.Add("Mã hàng : " + dm.MaSanPham, dm.MaSanPham);
            }
            if (cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;

            btnNext.Enabled = !(cbPage.SelectedIndex == (cbPage.Items.Count - 1));
            btnPrevious.Enabled = !(cbPage.SelectedIndex == 0);
            txtTo.Text = cbPage.Items.Count.ToString();
        }

        private bool CheckMaSPExist(string maSP)
        {
            foreach (UIComboBoxItem item in cbPage.Items)
                if (item.Value.ToString().Trim().ToUpper() == maSP.ToUpper().Trim()) return true;
            return false;
        }



        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC03.maSP = cbPage.SelectedValue.ToString();
            this.BC03.HD = this.HD;

            //if (dsDM.Tables["dtBangDinhMuc" + cbPage.SelectedValue] != null)
            //{
            //    this.BC03.dt = this.dsDM.Tables["dtBangDinhMuc" + cbPage.SelectedValue];
            //}
            //else
            //{
            //    dtBangDinhMuc = new DataTable("dtBangDinhMuc" + cbPage.SelectedValue);
            //    dtBangDinhMuc = this.CreatSchemaDataSetNewRP01(cbPage.SelectedValue.ToString());
            //    dsDM.Tables.Add(dtBangDinhMuc);
            //    this.BC03.dt = dtBangDinhMuc;
            //}
            dtBangDinhMuc = new DataTable("dtBangDinhMuc" + cbPage.SelectedValue);
            dtBangDinhMuc = this.CreatSchemaDataSetNewRP01(cbPage.SelectedValue.ToString());
            //dsDM.Tables.Add(dtBangDinhMuc);
            this.BC03.dt = dtBangDinhMuc;

            this.BC03.BindReport();
            printControl1.PrintingSystem = this.BC03.PrintingSystem;
            this.BC03.CreateDocument();

            btnNext.Enabled = !(cbPage.SelectedIndex == (cbPage.Items.Count - 1));
            btnPrevious.Enabled = !(cbPage.SelectedIndex == 0);
            
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }


        public DataTable CreatSchemaDataSetNewRP01(string MaSanPham)
        {
            dsDinhMuc = DM.getDinhMucSanPham(MaSanPham, DMDangKy.ID );
            DataTable dttemp = new DataTable("dtBangDinhMuc" + MaSanPham.ToString());
            DataColumn[] dcCol = new DataColumn[10];

            dcCol[0] = new DataColumn("STT", typeof(string));
            dcCol[0].Caption = "STT";
            dcCol[1] = new DataColumn("TenNPL", typeof(string));
            dcCol[1].Caption = "TenNPL";
            dcCol[2] = new DataColumn("MaNPL", typeof(string));
            dcCol[2].Caption = "MaNPL";
            dcCol[3] = new DataColumn("DVT", typeof(string));
            dcCol[3].Caption = "DVT";
            dcCol[4] = new DataColumn("DMSD", typeof(decimal));
            dcCol[4].Caption = "DMSD";
            dcCol[5] = new DataColumn("DMTH", typeof(decimal));
            dcCol[5].Caption = "DMTH";
            dcCol[6] = new DataColumn("TiLeHH", typeof(string));
            dcCol[6].Caption = "TiLeHH";
            dcCol[7] = new DataColumn("DMAll", typeof(string));
            dcCol[7].Caption = "DMAll";
            dcCol[8] = new DataColumn("Nguon", typeof(string));
            dcCol[8].Caption = "Nguon";
            dcCol[9] = new DataColumn("GhiChu", typeof(string));
            dcCol[9].Caption = "GhiChu";

            dttemp.Columns.AddRange(dcCol);

            int sothutu = 1;
            decimal dm,tlhh,dmsd;
            foreach (DataRow dr in dsDinhMuc.Tables[0].Rows)
            {
                DataRow drData = dttemp.NewRow();
                drData[0] = sothutu;
                drData[1] = dr.ItemArray[11].ToString();
                drData[2] = dr.ItemArray[2].ToString();
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.Ma = dr.ItemArray[2].ToString();
                npl.HopDong_ID = HD.ID;
                npl.Load();
                drData[3] = DonViTinh_GetName(npl.DVT_ID);
                drData["TenNPL"] = npl.Ten;
                drData[4] = dm = Decimal.Parse(dr.ItemArray[4].ToString());
                tlhh = Decimal.Parse(dr.ItemArray[5].ToString());
                drData[6] = String.Format("{0:0.0000}", tlhh);
                dmsd=dm + dm * tlhh / 100;
                drData[7] = String.Format("{0:0.000000}", dmsd);
                drData[8] = "Do nước ngoài cung cấp và mua nội địa";
                drData[9] = dr["GhiChu"];
                sothutu++;
                dttemp.Rows.Add(drData);
            }

            return dttemp;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnPrintAll_Click(object sender, EventArgs e)
        {

            int to = Convert.ToInt32(txtTo.Value);

            int from = Convert.ToInt32(txtFrom.Value);

            this.TongSoSP = to - from + 1;
            this.BC03All.maSP = cbPage.Items[from - 1].Value.ToString();
            this.BC03All.HD = this.HD;
            if (dsDM.Tables["dtBangDinhMuc" + cbPage.Items[from - 1].Value.ToString()] != null)
                this.BC03All.dt = this.dsDM.Tables["dtBangDinhMuc" + cbPage.Items[from - 1].Value.ToString()];
            else
            {
                //dtBangDinhMuc = new DataTable("dtBangDinhMuc" + 0);
                dtBangDinhMuc = this.CreatSchemaDataSetNewRP01(cbPage.Items[from - 1].Value.ToString());
                dsDM.Tables.Add(dtBangDinhMuc);
                this.BC03All.dt = dtBangDinhMuc;
            }
            this.BC03All.BindReport();
            this.BC03All.CreateDocument();
            this.TongSoSP = 1;
            for (int i = from; i < to; i++)
            {
                this.TongSoSP++;
                BangKe03_DMDK BC03Temp = new BangKe03_DMDK();
                BC03Temp.maSP = cbPage.Items[i].Value.ToString();
                BC03Temp.HD = this.HD;
                if (dsDM.Tables["dtBangDinhMuc" + cbPage.Items[i].Value.ToString()] != null)
                    BC03Temp.dt = this.dsDM.Tables["dtBangDinhMuc" + cbPage.Items[i].Value.ToString()];
                else
                {
                    //dtBangDinhMuc = new DataTable("dtBangDinhMuc" + i);
                    dtBangDinhMuc = this.CreatSchemaDataSetNewRP01(cbPage.Items[i].Value.ToString());
                    dsDM.Tables.Add(dtBangDinhMuc);
                    BC03Temp.dt = dtBangDinhMuc;
                }
                BC03Temp.BindReport();
                BC03Temp.CreateDocument();
                this.BC03All.Pages.AddRange(BC03Temp.Pages);
            }
            this.BC03All.PrintingSystem.ShowMarginsWarning = false;
            //printControl1.PrintingSystem = this.BC03All.PrintingSystem;
            this.BC03All.PrintDialog();
        }

        private void txtTo_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtTo.Value) > cbPage.Items.Count) txtTo.Value = cbPage.Items.Count;
            if (Convert.ToInt32(txtTo.Value) <1) txtTo.Value = 1;
        }

        private void txtFrom_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtFrom.Value) < 1) txtFrom.Value = 1;
        }

        private void btnApDungMoTaSP_Click(object sender, EventArgs e)
        {
            string masanpham="";
            masanpham = cbPage.SelectedValue.ToString();
            foreach (DinhMuc item in DMDangKy.DMCollection)
            {
                if (item.MaSanPham == masanpham)
                {
                    item.GhiChu = txtMoTaSP.Text;
                    item.InsertUpdate();
                }
            }
            cboToKhai_SelectedIndexChanged(null, null);
            
        }

       
      

    }
}