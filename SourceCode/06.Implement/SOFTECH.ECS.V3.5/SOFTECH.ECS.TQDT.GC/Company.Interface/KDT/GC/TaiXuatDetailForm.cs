﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.Interface.GC;
//using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class TaiXuatDetailForm : BaseForm
    {
        public KDT_GC_TaiXuat taiXuat = new KDT_GC_TaiXuat();
        public int TrangThai = -1;
        public TaiXuatDetailForm()
        {
            InitializeComponent();
        }

        private void CungUngDetailForm_Load(object sender, EventArgs e)
        {
            txtSoChungTu.Text = taiXuat.MaNPL;
            txtTenNPL.Text = taiXuat.TenNPL;
            txtSoLuong.Value = taiXuat.LuongTaiXuat;
            txtGhiChu.Text = taiXuat.GhiChu;
            if (TrangThai == 1 || TrangThai == 0)
            {
                btnGhi.Enabled = false;
            }
        }
        
        private void btnGhi_Click(object sender, EventArgs e)
        {
            taiXuat.MaNPL = txtSoChungTu.Text;
            taiXuat.TenNPL = txtTenNPL.Text;
            taiXuat.LuongTaiXuat =Convert.ToDecimal(txtSoLuong.Value);
            taiXuat.GhiChu = txtGhiChu.Text;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
    }
}
