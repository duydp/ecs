using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiXuat: DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public  Company.Interface.Report.ReportViewTKXForm report;
        public ToKhaiXuat()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            if (GlobalSettings.MaHTS == 0 || this.TKMD.LoaiHangHoa!= "S") BindReport1();
            else BindReport2();
        }
        public void BindReport1()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
    
            //xrLabel3.Text = this.TKMD.MaLoaiHinh.Substring(1, 2);
            if (this.TKMD.MaLoaiHinh.Contains("XTA20") || this.TKMD.MaLoaiHinh.Contains("XTA02") || this.TKMD.MaLoaiHinh.Contains("XTA18"))
            {
                xrDT.Visible = false;
                xrKD.Visible = false;
                xrTaiXuat.Visible = true;
            }
            else
            {
                if (this.TKMD.MaLoaiHinh.Substring(1, 2) == "KD")
                {
                    xrDT.Visible = false;
                    xrKD.Visible = true;
                }
                else
                {
                    xrDT.Visible = true;
                    xrKD.Visible = false;
                }
                xrTaiXuat.Visible = false;
            }
            if (IsHaveTax())
            {
                xrCoThue.Visible = true;
                xrKhongThue.Visible = false;

            }
            else
            {
                xrCoThue.Visible = false;
                xrKhongThue.Visible = true;
            }
            DateTime minDate = new DateTime(1900,1,1);
            lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();            
            if (this.TKMD.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if(this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if(this.TKMD.SoLuongPLTK>0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep,"   ");
            lblTenDoanhNghiep.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\n\r" + GlobalSettings.DIA_CHI.ToUpper();
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            lblSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocNK_ID, "      ");
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID).ToUpper();
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID,"      ");
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID, "      ");
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            if(TKMD.SoKien>0)
                lblSoKienTrongLuong.Text = "Tổng cộng :" + TKMD.SoKien.ToString("n0") + " kiện " + this.TKMD.TrongLuong + " kg";
            if (this.TKMD.HMDCollection.Count <= 9)
            {
                for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.TKMD.HMDCollection[i];
                    control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                    control.Text = hmd.TenHang;
                    control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;
                    control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString("G20");
                    control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G10");
                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");
                    tongTriGiaNT += hmd.TriGiaKB;
                }
            }
            else
            {
                ArrayList arr = this.GetNhomHang();
                if (arr.Count == 1 && arr[0].ToString() == "")
                {
                    string nhomHang = arr[0].ToString();
                    XRControl control = new XRControl();
                    List<HangMauDich> col = GetHangCoCungNhom(nhomHang);
                    control = this.xrTable1.Rows[0].Controls["TenHang1"];
                    control.Text = "PHỤ LỤC ĐÍNH KÈM";
                    control = this.xrTable1.Rows[0].Controls["Luong1"];
                    control.Text = GetTongSoLuong(col).ToString("G15");
                    control = this.xrTable1.Rows[0].Controls["TriGiaNT1"];
                    control.Text = GetTongTriGiaNT(col).ToString("N2");

                }
                else
                {
                    for (int i = 0; i < arr.Count; i++)
                    {
                        string nhomHang = arr[i].ToString();
                        XRControl control = new XRControl();
                        List<HangMauDich> col = GetHangCoCungNhom(nhomHang);
                        control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                        control.Text = nhomHang;
                        control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                        control.Text = GetTongSoLuong(col).ToString("G15");
                        control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                        control.Text = GetTongTriGiaNT(col).ToString("N2");
                    }
                }
                tongTriGiaNT = this.GetTongTriGiaNT(this.TKMD.HMDCollection);
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            XRControl control1 = new XRControl();
            int index = 3;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.LoaiCT == 1)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 1];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 1];
                    control1.Text = ct.SoBanSao + "";
                }
                else if (ct.LoaiCT == 3)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 2];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 2];
                    control1.Text = ct.SoBanSao + "";
                }
                else
                {
                    if (index == 7) return;
                    control1 = this.Detail.Controls["lblTenChungTu" + index];
                    control1.Text = ct.TenChungTu;
                    control1 = this.Detail.Controls["lblSoBanChinh" + index];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + index];
                    control1.Text = ct.SoBanSao + "";
                    index++;
                }
            }
        }
        public void BindReport2()
        {

            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            //xrLabel3.Text = this.TKMD.MaLoaiHinh.Substring(1, 2);
            if (this.TKMD.MaLoaiHinh.Substring(1, 2) == "KD")
            {
                xrDT.Visible = false;
                xrKD.Visible = true;
            }
            else
            {
                xrDT.Visible = true;
                xrKD.Visible = false;
            }
            if (IsHaveTax())
            {
                xrCoThue.Visible = true;
                xrKhongThue.Visible = false;

            }
            else
            {
                xrCoThue.Visible = false;
                xrKhongThue.Visible = true;
            }
            DateTime minDate = new DateTime(1900, 1, 1);
            lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            if (this.TKMD.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if (this.TKMD.SoLuongPLTK > 0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep, "   ");
            lblTenDoanhNghiep.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\n\r" + GlobalSettings.DIA_CHI.ToUpper();
            if (GlobalSettings.MaMID.Trim().Length >0)
                lblTenDoanhNghiep.Text +=  "\n\rMã MID: " + GlobalSettings.MaMID;
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac.ToUpper();
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            lblSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocNK_ID, "      ");
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID).ToUpper();
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID, "      ");
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID, "      ");
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            if (TKMD.SoKien > 0)
                lblSoKienTrongLuong.Text = "Tổng cộng :" + TKMD.SoKien.ToString("n0") + " kiện " + this.TKMD.TrongLuong + " kg";
            if (this.TKMD.HMDCollection.Count <= 9)
            {
                for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.TKMD.HMDCollection[i];
                    control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                    control.Text = hmd.Ma_HTS  + ";\n\r" + hmd.TenHang;
                    control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;
                    control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString("G15") + "\n\r(" + hmd.SoLuong_HTS.ToString("G15") ;
                    control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID) + "\n\r" + DonViTinh.GetName((object)hmd.DVT_HTS) + ")";
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G15");
                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");
                    tongTriGiaNT += hmd.TriGiaKB;
                }
            }
            else
            {
                ArrayList arr = this.GetNhomHang();
                if (arr.Count == 1 && arr[0].ToString() == "")
                {
                    string nhomHang = arr[0].ToString();
                    XRControl control = new XRControl();
                    List<HangMauDich> col = GetHangCoCungNhom(nhomHang);
                    control = this.xrTable1.Rows[0].Controls["TenHang1"];
                    control.Text = "PHỤ LỤC ĐÍNH KÈM";
                    control = this.xrTable1.Rows[0].Controls["Luong1"];
                    control.Text = GetTongSoLuong(col).ToString("G15");
                    control = this.xrTable1.Rows[0].Controls["TriGiaNT1"];
                    control.Text = GetTongTriGiaNT(col).ToString("N2");

                }
                else
                {
                    for (int i = 0; i < arr.Count; i++)
                    {
                        string nhomHang = arr[i].ToString();
                        XRControl control = new XRControl();
                        List<HangMauDich> col = GetHangCoCungNhom(nhomHang);
                        control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                        control.Text = nhomHang;
                        control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                        control.Text = GetTongSoLuong(col).ToString("G15");
                        control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                        control.Text = GetTongTriGiaNT(col).ToString("N2");
                    }
                }
                tongTriGiaNT = this.GetTongTriGiaNT(this.TKMD.HMDCollection);
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            XRControl control1 = new XRControl();
            int index = 3;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.LoaiCT == 1)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 1];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 1];
                    control1.Text = ct.SoBanSao + "";
                }
                else if (ct.LoaiCT == 3)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 2];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 2];
                    control1.Text = ct.SoBanSao + "";
                }
                else
                {
                    if (index == 7) return;
                    control1 = this.Detail.Controls["lblTenChungTu" + index];
                    control1.Text = ct.TenChungTu;
                    control1 = this.Detail.Controls["lblSoBanChinh" + index];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + index];
                    control1.Text = ct.SoBanSao + "";
                    index++;
                }
            }
        }
        private string ToStringForReport(string s, string patern)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + patern;
            temp += s[s.Length-1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            ptbImage.Visible = t;
        }
        private double GetTongTriGiaNT(List<HangMauDich> HMDCollection)
        {
            double d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.TriGiaKB;
            return d;
        }
        private decimal GetTongSoLuong(List<HangMauDich> HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.SoLuong;
            return d;
        }
        private ArrayList GetNhomHang()
        {
            ArrayList arr = new ArrayList();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (!CheckExitNhomHang(hmd.NhomHang, arr)) arr.Add(hmd.NhomHang);
            }
            return arr;
        }
        private bool CheckExitNhomHang(string nhomHang, ArrayList arr)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].ToString() == nhomHang) return true;
            }
            return false;
        }
        private List<HangMauDich> GetHangCoCungNhom(string nhomHang)
        {
            List<HangMauDich> col = new List<HangMauDich>();
            foreach(HangMauDich hmd in this.TKMD.HMDCollection)
                if(hmd.NhomHang == nhomHang) col.Add(hmd);
            return col;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
        
        public void setNhomHang(XRTableCell cell, string tenHang)
        {
            cell.Text = tenHang;
        }


    }
}
