/*
Run this script on:

192.168.72.100.ECS_TQDT_KD_V4    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_DANHMUC

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 04/03/2013 8:44:33 AM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case

Alter TABLE [dbo].[t_HaiQuan_LoaiHinhMauDich]
	add
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL
 go
delete  from  [t_HaiQuan_LoaiHinhMauDich] 
where ID in ('NCX05','NDT19','NGC17','NGC22','NGC23','NKD14','NKD16','NKD17','NKD18','NKD19','NSX08','XCX05','XDT11','XGC21','XKD12','XKD13','XKD14','XKD15','XSX08')
-- Add rows to [dbo].[t_HaiQuan_LoaiHinhMauDich]
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NCX05', N'Nhập trả hàng xuất chế xuất', 'NCX-NT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NDT19', N'Nhập trả hàng đầu tư đã xuất khẩu', 'NDT-NT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC17', N'Tạm nhập gia công tại chỗ', 'TNGC-TC', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC22', N'Nhập hàng hóa đặt Gia công ở nuớc ngoài', 'NGC-NN', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NGC23', N'Nhập hàng xuất gia công bị trả lại', 'NGC-NT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD14', N'Nhập Kinh doanh từ nuớc ngoài vào KKT', 'NKD/NN-KKT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD16', N'Nhập Kinh doanh từ nội địa về KTM', 'NKD/NÐ-KTM', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD17', N'Nhập kinh doanh từ nuớc ngoài về KTM', 'NKD/NN-KTM', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD18', N'Nhập KD giữa các Khu phi thuế quan', 'NKD-PTQ', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NKD19', N'Nhập trả hàng xuất khẩu bị trả lại', 'NKD-NT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('NSX08', N'Nhập trả hàng xuất SXXK', 'NSX-NT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XCX05', N'Xuất trả hàng nhập chế xuất', 'XCX-XT', GETDATE(), GETDATE())
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XDT11', N'Xuất trả hàng đầu tư đã nhập khẩu', 'XDT-XT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XGC21', N'Xuất trả hàng gia công nhập khẩu', 'XGC-XT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD12', N'Xuất Kinh doanh từ KTM về nội địa', 'XKD/KTM-NÐ', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD13', N'Xuất Kinh doanh từ KTM ra nuớc ngoài', 'XKD/KTM-NN', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD14', N'Xuất KD giữa các Khu phi thuế quan', 'XKD-PTQ', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XKD15', N'Xuất trả hàng đã nhập khẩu', 'XKD-XT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
INSERT INTO [dbo].[t_HaiQuan_LoaiHinhMauDich] ([ID], [Ten], [Ten_VT], [DateCreated], [DateModified]) VALUES ('XSX08', N'Xuất trả hàng nhập khẩu SXXK', 'XSX-XT', '2013-04-03 08:33:16.880', '2013-04-03 08:33:16.880')
-- Operation applied to 19 rows out of 19
-- cap nhat chi cuc moi
delete from t_HaiQuan_Cuc where ID='C27F01'
INSERT INTO [t_HaiQuan_DonViHaiQuan]([ID],[Ten],[DateCreated],[DateModified])
     VALUES('C27F01',N'Chi cục Hải quan cảng Nghi Sơn',GETDATE(),GETDATE())   
     
update t_haiquan_version  set version='8.8', notes=N'Cập nhật ma loại hinh mậu dịch'
COMMIT TRANSACTION
GO
