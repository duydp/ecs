﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class DinhMucRegistedDetailForm : BaseForm
    {
        public DinhMucCollection DMCollection = new DinhMucCollection();
        public SanPham SP;
        public DinhMucRegistedDetailForm()
        {
            InitializeComponent();
        }

        private void DinhMucRegistedDetailForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["NhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.DataSource = DMCollection;
            this.Text += " : "+SP.Ma;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                decimal DinhMuc = Convert.ToDecimal(e.Row.Cells["DinhMucSuDung"].Text);
                decimal TyLeHH = Convert.ToDecimal(e.Row.Cells["TyLeHaoHut"].Text);
                decimal DinhMucChung = DinhMuc + DinhMuc * TyLeHH / 100;
                e.Row.Cells["NhuCau"].Text = Convert.ToString(SP.SoLuongDangKy * DinhMucChung);

                if (e.Row.RowType == RowType.Record)
                {
                    Company.GC.BLL.GC.DinhMuc DM = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                    if (DM.TenNPL != null && DM.TenNPL.Trim().Length == 0)
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                        NPL.HopDong_ID = DM.HopDong_ID;
                        NPL.Ma = DM.MaNguyenPhuLieu;
                        NPL.Load();
                        DM.TenNPL = NPL.Ten;
                    }
                    try
                    {
                        KDT_LenhSanXuat lenhhXS = new KDT_LenhSanXuat();
                        lenhhXS = KDT_LenhSanXuat.Load(Convert.ToInt64(e.Row.Cells["LenhSanXuat_ID"].Value));
                        if (lenhhXS != null)
                            e.Row.Cells["LenhSanXuat_ID"].Text = lenhhXS.SoLenhSanXuat;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void menuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách Định mức của Sản phẩm _" + SP.Ma + "_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }
    }
}