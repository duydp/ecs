IF NOT EXISTS (SELECT * FROM sys.columns WHERE name IN ('SoDinhDanh'))
    ALTER TABLE dbo.t_KDT_VNACC_TK_SoVanDon
	ADD   SoDinhDanh VARCHAR(200) NULL;
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime,
	@SoDinhDanh varchar(200),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
(
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon],
	[SoDinhDanh]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@SoVanDon,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@SoVanDonChu,
	@NamVanDonChu,
	@LoaiDinhDanh,
	@NgayVanDon,
	@SoDinhDanh
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime,
	@SoDinhDanh varchar(200)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[SoVanDon] = @SoVanDon,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[SoVanDonChu] = @SoVanDonChu,
	[NamVanDonChu] = @NamVanDonChu,
	[LoaiDinhDanh] = @LoaiDinhDanh,
	[NgayVanDon] = @NgayVanDon,
	[SoDinhDanh] = @SoDinhDanh
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime,
	@SoDinhDanh varchar(200)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_SoVanDon] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[SoVanDon] = @SoVanDon,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[SoVanDonChu] = @SoVanDonChu,
			[NamVanDonChu] = @NamVanDonChu,
			[LoaiDinhDanh] = @LoaiDinhDanh,
			[NgayVanDon] = @NgayVanDon,
			[SoDinhDanh] = @SoDinhDanh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
		(
			[TKMD_ID],
			[SoTT],
			[SoVanDon],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[SoVanDonChu],
			[NamVanDonChu],
			[LoaiDinhDanh],
			[NgayVanDon],
			[SoDinhDanh]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@SoVanDon,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@SoVanDonChu,
			@NamVanDonChu,
			@LoaiDinhDanh,
			@NgayVanDon,
			@SoDinhDanh
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon],
	[SoDinhDanh]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon],
	[SoDinhDanh]
FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon],
	[SoDinhDanh]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.8',GETDATE(), N'CẬP NHẬT KHAI BÁO VẬN ĐƠN ')
END