﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.DanhMucChuan;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseXNKAutoForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public HopDong HD;
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        DataSet ds = new DataSet();
        public WareHouseXNKAutoForm()
        {
            InitializeComponent();
            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTKCo.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTKCo.DisplayMember = "MATK";
            cbbTKCo.ValueMember = "MATK";

            cbbTKNo.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTKNo.DisplayMember = "MATK";
            cbbTKNo.ValueMember = "MATK";
        }

        private void WareHouseXNKAutoForm_Load(object sender, EventArgs e)
        {
            txtSoHopDong.Text = HD.SoHopDong;
            clcNgayHopDong.Value = HD.NgayDangKy;
            cbbMaKho.SelectedIndex = 0;
            cbbTKNo.Value = "152";
            cbbTKCo.Value = "152";
            cbbLoaiChungTu.SelectedIndex = 0;
            cbbMaNT.Code = "VND";
            cbbLoaiChungTu.SelectedValue = "N";
            cbbLoaiHang.SelectedValue = "N";

            txtMa.Text = GlobalSettings.MA_DON_VI;
            txtTen.Text = GlobalSettings.TEN_DON_VI;
            txtDiaChi.Text = GlobalSettings.DIA_CHI;
            txtNguoiGiao.Text = GlobalSettings.MA_DON_VI;
            cbbMaKho.SelectedIndex = 0;
            this.AcceptButton = this.btnTimKiem;
            cbbPhanLuong.SelectedValue = 0;
            cbbTrangThai.SelectedValue = 0;
            ctrMaHaiQuan.CategoryType = ECategory.A038;
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            txtNamDangKy.Text = DateTime.Today.Year.ToString();
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }
        private void LoadData(string where)
        {
#if GC_V4
            if (HD.ID != 0)
            {
                where += " AND HopDong_ID = " + HD.ID;
                if (cbbLoaiChungTu.SelectedValue.ToString()=="N")
                {
                    where += " AND SoToKhai NOT IN (SELECT SOTK  FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE HOPDONG_ID =" + HD.ID + " AND LOAICHUNGTU='" + cbbLoaiChungTu.SelectedValue.ToString() + "' AND LOAIHANGHOA='" + cbbLoaiHang.SelectedValue.ToString() + "') AND LoaiHang ='" + cbbLoaiHang.SelectedValue.ToString() + "' ";
                }
                else
                {
                    where += " AND SoToKhai NOT IN (SELECT SOTK  FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE HOPDONG_ID =" + HD.ID + " AND LOAICHUNGTU='" + cbbLoaiChungTu.SelectedValue.ToString() + "' AND LOAIHANGHOA='" + cbbLoaiHang.SelectedValue.ToString() + "') AND SUBSTRING(CAST(SoToKhai AS VARCHAR(12)), 1, 1) IN ('3') ";
                }                
            }

#endif
            ds = VNACC_Category_Common.SelectDynamic("ReferenceDB in ('E001','E002')", null);
            ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(where, null);
            grList.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
            grList.DataSource = ListTKMD;

        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                string errorMAHANGHOAMAP = "";
                string errorDINHMUCMAP = "";
                string errorTotal = "";

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    KDT_VNACC_ToKhaiMauDich TKMD = (KDT_VNACC_ToKhaiMauDich)item.DataRow;
                    TKMD.LoadFull();
                    try
                    {
                        T_KHOKETOAN_PHIEUXNKHO PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
                        //String SoChungTu = txtSoCT.Text.ToString();
                        //SoChungTu = SoChungTu.Substring(0, SoChungTu.Length - 1);
                        //SoChungTu = SoChungTu + (i + 1);
                        PhieuXNKho.SOCT = GetConfigAuto(cbbLoaiChungTu.SelectedValue.ToString());
                        PhieuXNKho.NGAYCT = TKMD.NgayDangKy;
                        PhieuXNKho.LOAICHUNGTU = cbbLoaiChungTu.SelectedValue.ToString();
                        PhieuXNKho.SOCTGOC = String.Empty;
                        PhieuXNKho.NGAYCTGOC = TKMD.NgayDangKy;
                        PhieuXNKho.SOTK = Convert.ToDecimal(TKMD.SoToKhai.ToString());
                        PhieuXNKho.NGAYTK = TKMD.NgayDangKy;
                        PhieuXNKho.MAHQ = TKMD.CoQuanHaiQuan;
                        PhieuXNKho.SOHOADON = TKMD.SoHoaDon;
                        PhieuXNKho.NGAYHOADON = TKMD.NgayPhatHanhHD;
                        PhieuXNKho.HOPDONG_ID = HD.ID;
                        PhieuXNKho.SOHOPDONG = txtSoHopDong.Text.ToString();
                        PhieuXNKho.NGAYHOPDONG = clcNgayHopDong.Value;
                        PhieuXNKho.MAKH = txtMa.Text.ToString();
                        PhieuXNKho.TENKH = txtTen.Text.ToString();
                        PhieuXNKho.DIACHIKH = txtDiaChi.Text.ToString();
                        PhieuXNKho.MADN = GlobalSettings.MA_DON_VI;
                        PhieuXNKho.TENDN = GlobalSettings.TEN_DON_VI;
                        PhieuXNKho.DIACHIDN = GlobalSettings.DIA_CHI;
                        PhieuXNKho.DIENGIAI = txtDienGiai.Text.ToString();
                        PhieuXNKho.NGUOIVANCHUYEN = txtNguoiGiao.Text.ToString();
                        PhieuXNKho.MAKHO = cbbMaKho.Value.ToString();
                        PhieuXNKho.TENKHO = txtTenKho.Text.ToString();
                        PhieuXNKho.LOAIHANGHOA = cbbLoaiHang.SelectedValue.ToString();
                        PhieuXNKho.MANGUYENTE = TKMD.MaTTHoaDon;
                        if (TKMD.TyGiaCollection.Count >= 1)
                        {
                            PhieuXNKho.TYGIA = TKMD.TyGiaCollection[0].TyGiaTinhThue;
                        }
                        else
                        {
                            PhieuXNKho.TYGIA = Convert.ToDecimal(txtTyGia.Text.ToString());
                        }
                        PhieuXNKho.GHICHU = String.Empty;
                        decimal TongLuongHang =0 ;
                        decimal TongTriGiaHang = 0;
                        decimal TongTLQD = 0;
                        int k = 1;
                        //PhieuXNKho.PHIVANCHUYEN = Convert.ToDecimal(txtPhiVC.Text.ToString());
                        //PhieuXNKho.CHIPHI = Convert.ToDecimal(txtChiPhiKhac.Text.ToString());
                        //PhieuXNKho.PBDONGIA = Convert.ToBoolean(cbbPhanBo.SelectedValue.ToString());
                        //PhieuXNKho.PBTRIGIA = Convert.ToBoolean(cbbPhanBo.SelectedValue.ToString());
                        string LoaiHangHoa = TKMD.LoaiHang;
                        if (cbbLoaiChungTu.SelectedValue.ToString()=="N")
                        {
                            if (cbbLoaiHang.SelectedValue.ToString()=="N")
                            {
                                foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                                {
                                    TongLuongHang += HMD.SoLuong1;
                                    TongTriGiaHang += HMD.TriGiaTinhThueS;
                                    //Lấy danh sách NPL Map của kho kế toán                       
                                    List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                    if (NPLMapCollection.Count == 0)
                                    {
                                        errorMAHANGHOAMAP += "[" + (k) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]-[" + TKMD.SoToKhai + "]\n";
                                        break;
                                    }
                                    TongTLQD = 0;
                                    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP npl in NPLMapCollection)
                                    {
                                        TongTLQD += npl.TYLEQD;
                                    }
                                    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                                    {
                                        T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                        PhieuXNKHang.STT = k;
                                        PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                        PhieuXNKHang.MAHANG = NPL.MANPL;
                                        PhieuXNKHang.TENHANG = NPL.TENNPL;
                                        PhieuXNKHang.DVT = NPL.DVT;
                                        PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                        PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                        PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                        PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                        PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                        PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                        PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * NPL.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THUEXNK = (HMD.SoTienThue * NPL.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THUETTDB = 0;

                                        foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                                        {
                                            decimal TongThueThuKhac = 0;
                                            if (HMD.ID == ThueThuKhac.Master_id)
                                            {
                                                TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                                            }
                                            PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                                        }
                                        // 
                                        PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC) * NPL.TYLEQD / TongTLQD;
                                        PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                                        PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                        PhieuXNKHang.TAIKHOANNO = "152";
                                        PhieuXNKHang.TAIKHOANCO = "152";
                                        PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                                        PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                                        PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                        k++;
                                    }
                                }
                                PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                if (PhieuXNKho.HangCollection.Count >= 1)
                                {
                                    PhieuXNKho.InsertUpdateFull();
                                }
                            }
                            else if (cbbLoaiHang.SelectedValue.ToString()=="S")
                            {
                                foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                                {
                                    TongLuongHang += HMD.SoLuong1;
                                    TongTriGiaHang += HMD.TriGiaTinhThueS;
                                    //Lấy danh sách NPL Map của kho kế toán                       
                                    List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                    if (SPMapCollection.Count == 0)
                                    {
                                        errorMAHANGHOAMAP += "[" + (k) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]-[" + TKMD.SoToKhai + "]\n";
                                        break;
                                    }
                                    TongTLQD = 0;
                                    foreach (T_KHOKETOAN_SANPHAM_MAP sp in SPMapCollection)
                                    {
                                        TongTLQD += sp.TYLEQD;
                                    }
                                    foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                    {
                                        T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                        PhieuXNKHang.STT = k;
                                        PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                        PhieuXNKHang.MAHANG = SP.MASP;
                                        PhieuXNKHang.TENHANG = SP.TENSP;
                                        PhieuXNKHang.DVT = SP.DVT;
                                        PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                        PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                        PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                        PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                        PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                        PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                        PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.TRIGIANTSAUPB = (HMD.TriGiaTinhThueS * SP.TYLEQD) * TongTLQD;
                                        PhieuXNKHang.THUEXNK = (HMD.SoTienThue * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THUETTDB = 0;
                                        PhieuXNKHang.THUEKHAC = 0;
                                        PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                        PhieuXNKHang.TAIKHOANNO = "155";
                                        PhieuXNKHang.TAIKHOANCO = "155";
                                        PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                        PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                                        PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                        k++;
                                    }
                                }
                                PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                if (PhieuXNKho.HangCollection.Count >= 1)
                                {
                                    PhieuXNKho.InsertUpdateFull();
                                }
                            }
                        }
                        else if (cbbLoaiChungTu.SelectedValue.ToString()=="X")
                        {
                            if (cbbLoaiHang.SelectedValue.ToString()=="N")
                            {
                                if (TKMD.LoaiHang =="S")
                                {
                                    foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                                    {
                                        TongLuongHang += hmd.SoLuong1;
                                        TongTriGiaHang += hmd.TriGiaTinhThueS;
                                        //Lấy danh sách NPL Map của kho kế toán                       
                                        List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + hmd.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                        if (SPMapCollection.Count == 0)
                                        {
                                            errorMAHANGHOAMAP += "[" + (i) + "]-[" + hmd.MaHangHoa + "]-[" + hmd.TenHang + "]-[" + hmd.DVTLuong1 + "]\n";
                                            break;
                                        }
                                        TongTLQD = 0;
                                        foreach (T_KHOKETOAN_SANPHAM_MAP sp in SPMapCollection)
                                        {
                                            TongTLQD += sp.TYLEQD;
                                        }
                                        foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                        {
                                            //Định mức sản phẩm MAP
                                            List<T_KHOKETOAN_DINHMUC> DMCollection = T_KHOKETOAN_DINHMUC.SelectCollectionDynamic("MASP ='" + SP.MASP + "' AND HOPDONG_ID =" + HD.ID, "");
                                            if (DMCollection.Count == 0)
                                            {
                                                errorDINHMUCMAP += "[" + (k) + "]-[" + SP.MASP + "]-[" + SP.TENSP + "]-[" + SP.DVT + "]-[" + TKMD.SoToKhai + "]\n";
                                                break;
                                            }
                                            decimal TongSLQDSPToNPL = 0;
                                            decimal DonGiaNTSP = hmd.DonGiaHoaDon;
                                            decimal DonGia = hmd.DonGiaTinhThue;
                                            decimal SoLuongSPQD = (hmd.SoLuong1 * SP.TYLEQD) / TongTLQD;
                                            decimal TrigiaNT = SoLuongSPQD * DonGiaNTSP;
                                            decimal TriGia = SoLuongSPQD * DonGia;
                                            decimal ThueXNK = hmd.SoTienThue * SP.TYLEQD / TongTLQD;
                                            foreach (T_KHOKETOAN_DINHMUC dm in DMCollection)
                                            {
                                                TongSLQDSPToNPL += dm.DMCHUNG * ((hmd.SoLuong1 * SP.TYLEQD) / TongTLQD);
                                            }
                                            foreach (T_KHOKETOAN_DINHMUC DM in DMCollection)
                                            {
                                                T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                                PhieuXNKHang.STT = k;
                                                PhieuXNKHang.LOAIHANG = cbbLoaiHang.SelectedValue.ToString();
                                                PhieuXNKHang.MAHANG = DM.MANPL;
                                                PhieuXNKHang.TENHANG = DM.TENNPL;
                                                PhieuXNKHang.DVT = DM.DVTNPL;
                                                PhieuXNKHang.MAHANGMAP = DM.MANPL;
                                                PhieuXNKHang.TENHANGMAP = DM.TENNPL;
                                                PhieuXNKHang.DVTMAP = DM.DVTNPL;
                                                PhieuXNKHang.SOLUONG = SoLuongSPQD * DM.DMCHUNG;
                                                PhieuXNKHang.DONGIA = TriGia / TongSLQDSPToNPL;
                                                PhieuXNKHang.DONGIANT = TrigiaNT / TongSLQDSPToNPL;
                                                PhieuXNKHang.TRIGIANT = PhieuXNKHang.SOLUONG * PhieuXNKHang.DONGIANT;
                                                PhieuXNKHang.THANHTIEN = PhieuXNKHang.SOLUONG * PhieuXNKHang.DONGIA;
                                                PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;
                                                PhieuXNKHang.THUEXNK = ThueXNK;
                                                PhieuXNKHang.THUETTDB = 0;
                                                PhieuXNKHang.THUEKHAC = 0;
                                                PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                                PhieuXNKHang.TAIKHOANNO = "155";
                                                PhieuXNKHang.TAIKHOANCO = "155";
                                                PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                                PhieuXNKHang.SOLUONGQD = SoLuongSPQD * DM.DMCHUNG;
                                                PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                                k++;
                                            }
                                        }
                                    }
                                    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                    if (PhieuXNKho.HangCollection.Count >= 1)
                                    {
                                        PhieuXNKho.InsertUpdateFull();
                                    }
                                }
                                else if (TKMD.LoaiHang =="N")
                                {
                                    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                                    {
                                        TongLuongHang += HMD.SoLuong1;
                                        TongTriGiaHang += HMD.TriGiaTinhThueS;
                                        //Lấy danh sách NPL Map của kho kế toán                       
                                        List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                        if (NPLMapCollection.Count == 0)
                                        {
                                            errorMAHANGHOAMAP += "[" + (k) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]-[" + TKMD.SoToKhai + "]\n";
                                            break;
                                        }
                                        TongTLQD = 0;
                                        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP npl in NPLMapCollection)
                                        {
                                            TongTLQD += npl.TYLEQD;
                                        }
                                        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                                        {
                                            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                            PhieuXNKHang.STT = k;
                                            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                            PhieuXNKHang.MAHANG = NPL.MANPL;
                                            PhieuXNKHang.TENHANG = NPL.TENNPL;
                                            PhieuXNKHang.DVT = NPL.DVT;
                                            PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                            PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                            PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                            PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                            PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                            PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                            PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * NPL.TYLEQD) / TongTLQD;
                                            PhieuXNKHang.THUEXNK = (HMD.SoTienThue * NPL.TYLEQD) / TongTLQD;
                                            PhieuXNKHang.THUETTDB = 0;

                                            foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                                            {
                                                decimal TongThueThuKhac = 0;
                                                if (HMD.ID == ThueThuKhac.Master_id)
                                                {
                                                    TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                                                }
                                                PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                                            }
                                            // 
                                            PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC) * NPL.TYLEQD / TongTLQD;
                                            PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                                            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                            PhieuXNKHang.TAIKHOANNO = "152";
                                            PhieuXNKHang.TAIKHOANCO = "152";
                                            PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                                            PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                                            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                            k++;
                                        }
                                    }
                                    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                    if (PhieuXNKho.HangCollection.Count >= 1)
                                    {
                                        PhieuXNKho.InsertUpdateFull();
                                    }
                                }
                                else if (true)
                                {

                                }
                                else
                                {
                                    
                                }
                            }
                            else if (cbbLoaiHang.SelectedValue.ToString() == "S")
                            {
                                foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                                {
                                    TongLuongHang += HMD.SoLuong1;
                                    TongTriGiaHang += HMD.TriGiaTinhThueS;
                                    //Lấy danh sách SP Map của kho kế toán                       
                                    List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                    if (SPMapCollection.Count == 0)
                                    {
                                        errorMAHANGHOAMAP += "[" + (k) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]-[" + TKMD.SoToKhai + "]\n";
                                        break;
                                    }
                                    TongTLQD = 0;
                                    foreach (T_KHOKETOAN_SANPHAM_MAP sp in SPMapCollection)
                                    {
                                        TongTLQD += sp.TYLEQD;
                                    }
                                    foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                    {
                                        T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                        PhieuXNKHang.STT = k;
                                        PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                        PhieuXNKHang.MAHANG = SP.MASP;
                                        PhieuXNKHang.TENHANG = SP.TENSP;
                                        PhieuXNKHang.DVT = SP.DVT;
                                        PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                        PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                        PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                        PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                        PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                        PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                        PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.TRIGIANTSAUPB = (HMD.TriGiaTinhThueS * SP.TYLEQD) * TongTLQD;
                                        PhieuXNKHang.THUEXNK = (HMD.SoTienThue * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THUETTDB = 0;
                                        PhieuXNKHang.THUEKHAC = 0;
                                        PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                        PhieuXNKHang.TAIKHOANNO = "155";
                                        PhieuXNKHang.TAIKHOANCO = "155";
                                        PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                        PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                                        PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                        k++;
                                    }
                                }
                                PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                if (PhieuXNKho.HangCollection.Count >= 1)
                                {
                                    PhieuXNKho.InsertUpdateFull();
                                }
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                        #region Code Comment
                        //if (LoaiHangHoa == "N")
                        //{
                        //    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        //    {

                        //        TongLuongHang += HMD.SoLuong1;
                        //        TongTriGiaHang += HMD.TriGiaTinhThueS;
                        //        //Lấy danh sách NPL Map của kho kế toán                       
                        //        List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                        //        if (NPLMapCollection.Count == 0)
                        //        {
                        //            ShowMessage("NGUYÊN PHỤ LIỆU CÓ MÃ NGUYÊN PHỤ LIỆU : '" + HMD.MaHangHoa +"' CHƯA ĐƯỢC MAP VỚI NGUYÊN PHỤ LIỆU TRONG KẾ TOÁN .",false);
                        //            return;
                        //        }
                        //        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                        //        {
                        //            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                        //            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                        //            PhieuXNKHang.MAHANG = NPL.MANPL;
                        //            PhieuXNKHang.TENHANG = NPL.TENNPL;
                        //            PhieuXNKHang.DVT = NPL.DVT;
                        //            PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                        //            PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                        //            PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                        //            PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                        //            PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                        //            PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                        //            PhieuXNKHang.TRIGIANT = HMD.TriGiaHoaDon;
                        //            PhieuXNKHang.THUEXNK = HMD.SoTienThue;
                        //            PhieuXNKHang.THUETTDB = 0;

                        //            foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                        //            {
                        //                decimal TongThueThuKhac = 0;
                        //                if (HMD.ID == ThueThuKhac.Master_id)
                        //                {
                        //                    TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                        //                }
                        //                PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                        //            }
                        //            // 
                        //            PhieuXNKHang.THANHTIEN = HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC;
                        //            PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                        //            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                        //            PhieuXNKHang.TAIKHOANNO = cbbTKNo.Value.ToString();
                        //            PhieuXNKHang.TAIKHOANCO = cbbTKCo.Value.ToString();
                        //            PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                        //            PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                        //            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                        //        }
                        //    }
                        //    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                        //    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                        //    if (PhieuXNKho.HangCollection.Count >=1)
                        //    {
                        //        PhieuXNKho.InsertUpdateFull();   
                        //    }
                        //}
                        //else if (LoaiHangHoa == "S")
                        //{
                        //    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        //    {
                        //        TongLuongHang += HMD.SoLuong1;
                        //        TongTriGiaHang += HMD.TriGiaTinhThueS;
                        //        //Lấy danh sách NPL Map của kho kế toán                       
                        //        List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                        //        if (SPMapCollection.Count == 0)
                        //        {
                        //            ShowMessage("SẢN PHẨM CÓ MÃ SẢN PHẨM : '" + HMD.MaHangHoa + "' CHƯA ĐƯỢC MAP VỚI SẢN PHẨM TRONG KHO KẾ TOÁN .", false);
                        //            return;
                        //        }
                        //        foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                        //        {
                        //            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                        //            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                        //            PhieuXNKHang.MAHANG = SP.MASP;
                        //            PhieuXNKHang.TENHANG = SP.TENSP;
                        //            PhieuXNKHang.DVT = SP.DVT;
                        //            PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                        //            PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                        //            PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                        //            PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                        //            PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                        //            PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                        //            PhieuXNKHang.TRIGIANT = HMD.TriGiaHoaDon;
                        //            PhieuXNKHang.THANHTIEN = HMD.TriGiaTinhThueS;
                        //            PhieuXNKHang.TRIGIANTSAUPB = HMD.TriGiaTinhThueS;
                        //            PhieuXNKHang.THUEXNK = HMD.SoTienThue;
                        //            PhieuXNKHang.THUETTDB = 0;
                        //            PhieuXNKHang.THUEKHAC = 0;
                        //            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                        //            PhieuXNKHang.TAIKHOANNO = cbbTKNo.Value.ToString();
                        //            PhieuXNKHang.TAIKHOANCO = cbbTKCo.Value.ToString();
                        //            PhieuXNKHang.TYLEQD = SP.TYLEQD;
                        //            PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                        //            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                        //        }
                        //    }
                        //    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                        //    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                        //    if (PhieuXNKho.HangCollection.Count >= 1)
                        //    {
                        //        PhieuXNKho.InsertUpdateFull();
                        //    }
                        //}
                        //else if (LoaiHangHoa == "T")
                        //{

                        //}
                        //else if (LoaiHangHoa == "H")
                        //{

                        //}
                        //else
                        //{

                        //}
                        #endregion                        
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        LoadData(GetSeachWhere());
                        btnProcess.Enabled = true; btnClose.Enabled = true;

                    }));
                }
                else
                {
                    LoadData(GetSeachWhere());
                    btnProcess.Enabled = true; btnClose.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
                if (!String.IsNullOrEmpty(errorMAHANGHOAMAP))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[TÊN HÀNG HÓA]-[ĐVT]-[SỐ TỜ KHAI] : " + errorMAHANGHOAMAP + "CHƯA ĐƯỢC MAP VỚI MÃ HÀNG HÓA TRONG KHO KẾ TOÁN";
                if (!String.IsNullOrEmpty(errorDINHMUCMAP))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[TÊN HÀNG HÓA]-[ĐVT]-[SỐ TỜ KHAI] : " + errorDINHMUCMAP + "CHƯA ĐƯỢC MAP ĐỊNH MỨC TRONG KHO KẾ TOÁN";
                if (!String.IsNullOrEmpty(errorTotal))
                {
                    ShowMessageTQDT("DỮ LIỆU MAP TỰ ĐỘNG CỦA DANH SÁCH TỜ KHAI SAU KHÔNG THÀNH CÔNG !" + errorTotal, false);
                }

            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoCT, errorProvider, " Số chứng từ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayCT, errorProvider, " Ngày chứng từ ");
                isValid &= ValidateControl.ValidateNull(cbbLoaiChungTu, errorProvider, " Loại chứng từ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTyGia, errorProvider, " Ngày chứng từ gốc ", isOnlyWarning);

                isValid &= ValidateControl.ValidateChoose(cbbTKCo, errorProvider, " Tài khoản có ", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbTKNo, errorProvider, " Tài khoản nợ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, " Số hợp đồng ");
                isValid &= ValidateControl.ValidateNull(clcNgayHopDong, errorProvider, " Ngày hợp đồng ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, " Mã khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, " Tên khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, " Địa chỉ khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtNguoiGiao, errorProvider, " Người giao hàng ");
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, " Mã kho ");
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, " Tên kho ");
                isValid &= ValidateControl.ValidateNull(cbbLoaiHang, errorProvider, " Loại hàng hóa ");
                isValid &= ValidateControl.ValidateNull(cbbMaNT, errorProvider, " Mã nguyên tệ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnProcess_Click(object sender, EventArgs e)           
        {
            if (!ValidateForm(false))
                return;
            btnProcess.Enabled = false;
            btnClose.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string GetSeachWhere()
        {
            try
            {

                //this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string phanluong = (cbbPhanLuong.SelectedValue.ToString() == "0") ? "" : cbbPhanLuong.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "1" : cbbTrangThai.SelectedValue.ToString().Trim();
                DateTime ngayDangKy = clcTuNgay.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();

                string where = String.Empty;
                where = "MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " and TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoToKhai like '%" + soToKhai + "%' ";
                if (phanluong != "")
                    where = where + " and MaPhanLoaiKiemTra='" + phanluong + "' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan like '" + haiquan + "%' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "1")
                        where = where + " and year(NgayDangKy)=" + txtNamDangKy.Text + " ";
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!string.IsNullOrEmpty(txtSoHoaDon.Text))
                    where = where + " AND SoHoaDon like '%" + txtSoHoaDon.Text.Trim() + "%' ";
                if (!string.IsNullOrEmpty(txtSoTKDauTien.Text))
                    where = where + " AND SoToKhaiDauTien like '%" + txtSoTKDauTien.Text.Trim() + "%' ";

                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                ListTKMD.Clear();
                LoadData(GetSeachWhere());
                grList.Refetch();
                grList.DataSource = ListTKMD;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    switch (e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString())
                    {
                        case "1":
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng xanh";
                            break;
                        case "2":
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng vàng";
                            break;
                        case "3":
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng đỏ";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetConfigAuto(String LOAICT)
        {
            try
            {
                String SoChungTu = "";
                List<T_KHOKETOAN_QUYTACDANHSO> ConfigCollection = T_KHOKETOAN_QUYTACDANHSO.SelectCollectionAll();

                    foreach (T_KHOKETOAN_QUYTACDANHSO item in ConfigCollection)
                    {
                        if (item.LOAICHUNGTU == "PNK" && LOAICT == "N")
                        {
                            SoChungTu = item.HIENTHI.Substring(0, item.HIENTHI.Length - 1);
                            string GetSoCT = T_KHOKETOAN_PHIEUXNKHO.GetChungTu(LOAICT);
                            if (!String.IsNullOrEmpty(GetSoCT))
                            {
                                if (GetSoCT.Substring(0, SoChungTu.Length) != SoChungTu)
                                {
                                    SoChungTu = item.HIENTHI;
                                }
                                else
                                {
                                    int LastIndex = Convert.ToInt32(GetSoCT.Substring(SoChungTu.Length, GetSoCT.Length - SoChungTu.Length));
                                    SoChungTu = SoChungTu + (LastIndex + 1);
                                }
                            }
                            else
                            {
                                SoChungTu = item.HIENTHI;
                            }
                        }
                        else if (item.LOAICHUNGTU == "PXK" && LOAICT == "X")
                        {
                            SoChungTu = item.HIENTHI.Substring(0, item.HIENTHI.Length - 1);
                            string GetSoCT = T_KHOKETOAN_PHIEUXNKHO.GetChungTu(LOAICT);
                            if (!String.IsNullOrEmpty(GetSoCT))
                            {
                                if (GetSoCT.Substring(0, SoChungTu.Length) != SoChungTu)
                                {
                                    SoChungTu = item.HIENTHI;
                                }
                                else
                                {
                                    int LastIndex = Convert.ToInt32(GetSoCT.Substring(SoChungTu.Length, GetSoCT.Length - SoChungTu.Length));
                                    SoChungTu = SoChungTu + (LastIndex + 1);
                                }
                            }
                            else
                            {
                                SoChungTu = item.HIENTHI;
                            }
                        }
                    }
                return SoChungTu;
            }
            catch (Exception ex)
            {
                return "";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbbLoaiChungTu_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiChungTu.SelectedValue!=null)
                {
                    String LoaiChungTu = cbbLoaiChungTu.SelectedValue.ToString();
                    switch (LoaiChungTu)
                    {
                        case "N":
                            txtSoCT.Text = GetConfigAuto(LoaiChungTu);
                            btnTimKiem_Click(null,null);
                            break;
                        case "X":
                            txtSoCT.Text = GetConfigAuto(LoaiChungTu);
                            btnTimKiem_Click(null, null);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaKho_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaKho.Value != null)
                {
                    List<T_KHOKETOAN_DANHSACHKHO> KHOCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
                    foreach (T_KHOKETOAN_DANHSACHKHO item in KHOCollection)
                    {
                        if (item.MAKHO == cbbMaKho.Value.ToString())
                        {
                            txtTenKho.Text = item.TENKHO.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNguoiGiao_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    txtNguoiGiao.Text = string.IsNullOrEmpty(f.doiTac.MaCongTy) ? "" : f.doiTac.MaCongTy.Trim().ToUpper();
                    txtDienGiai.Text = string.IsNullOrEmpty(f.doiTac.TenCongTy) ? "" : f.doiTac.TenCongTy.Trim().ToUpper();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
