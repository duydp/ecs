﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Threading;
namespace CreateUpdateFile
{
    public class MakeFiles
    {
        private string GetFolderPath(string rootPath, string filePath)
        {
            string sFolder = filePath.Replace(rootPath + "\\", string.Empty);
            if (sFolder.IndexOf("\\") != -1)
            {

                string[] exeGroup = sFolder.Split('\\');
                string tempPath = string.Empty;

                for (int i = 0; i < exeGroup.Length - 1; i++)
                    tempPath += exeGroup[i] + "/";


                return tempPath;
            }
            else return string.Empty;

        }
        private string ExtDirectory(string fileName, string startDirectory, string toDirectory)
        {

            string extDir = fileName.Replace(startDirectory, string.Empty);
            if (extDir.IndexOf("\\") != -1)
            {

                string[] exeGroup = extDir.Split('\\');
                string tempPath = string.Empty;

                for (int i = 0; i < exeGroup.Length - 1; i++)
                    tempPath += exeGroup[i] + "\\";

                if (!Directory.Exists(toDirectory + "\\" + tempPath))
                    Directory.CreateDirectory(toDirectory + "\\" + tempPath);
                return "\\" + tempPath + Path.GetFileName(fileName);
            }
            return "\\" + Path.GetFileName(fileName);


        }
        private void Zip(string sourceFile, string fromDirectory, string toDirectory, ref long fileLength)
        {
            if (System.IO.File.Exists(sourceFile))
            {
                string zipFile = toDirectory + ExtDirectory(sourceFile, fromDirectory, toDirectory) + ".zip";
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("zip.exe", "-j -r -9 -D \"" +
                                                         zipFile + "\" \"" +
                                                         sourceFile + "\"");
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process ps = System.Diagnostics.Process.Start(psi);
                ps.WaitForExit();
                FileInfo fi = new FileInfo(zipFile);
                if (fi.Exists)
                    fileLength = fi.Length;
            }
        }
        public event EventHandler<ZipEventArgs> ZipHandler;
        private void OnZip(ZipEventArgs e)
        {
            if (ZipHandler != null)
            {
                ZipHandler(this, e);
            }
        }
        public void GenerateFormatXml(string folderPath, string pathSourceCompare, bool zipFile, ref int count)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument docSource = null;
            if (!string.IsNullOrEmpty(pathSourceCompare))
            {
                OnZip(new ZipEventArgs("Lấy dữ liệu so sánh", 0, 0, string.Empty));
                try
                {
                    docSource = new XmlDocument();
                    docSource.Load(pathSourceCompare);
                    XmlNode nodeDescription = docSource.DocumentElement.FirstChild;
                    docSource.DocumentElement.RemoveChild(nodeDescription);
                }
                catch
                {
                }
            }
            OnZip(new ZipEventArgs("Kiểm tra cấu hình", 0, 0, string.Empty));
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ZIP\\ListUpdate.xml"))
            {

                string sfmt = string.Format(@"<UpdateFileList url=""{0}"">
                                            <Desctiption Title=""{1}"">{2}</Desctiption>
                                          </UpdateFileList>",
                                            new object[] { MakeConfig.Install.Url, MakeConfig.Install.Title, MakeConfig.Install.Header + "\r\n" + MakeConfig.Install.Description + "\r\n" + MakeConfig.Install.Footer });
                doc.Load(new StringReader(sfmt));
            }
            else
            {
                OnZip(new ZipEventArgs("Xác nhận thông tin", 0, 0, string.Empty));
                System.Windows.Forms.MessageBox.Show("Đã tồn tại tệp tin cấu hình\nBây giờ xóa và tạo mới", "Thông báo", System.Windows.Forms.MessageBoxButtons.OK,
                     System.Windows.Forms.MessageBoxIcon.Information);
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "ZIP\\ListUpdate.xml");
                GenerateFormatXml(folderPath, pathSourceCompare, zipFile, ref count);
                return;
            }
            OnZip(new ZipEventArgs("Tìm file hệ thống", 0, 0, string.Empty));
            string[] filesNewVersion = System.IO.Directory.GetFiles(folderPath, "*", System.IO.SearchOption.AllDirectories);
            count = filesNewVersion.Length;
            System.IO.FileInfo finfo = null;
            System.Diagnostics.FileVersionInfo fvInfo = null;
            StringBuilder sbDir = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            string zip = ".zip"; //zipFile ? ".zip":string.Empty;
            OnZip(new ZipEventArgs("Xóa thông tin cũ", 0, 0, string.Empty));
            if (zipFile)
            {
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "ZIP"))
                    Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + "ZIP", true);
                Thread.Sleep(500);
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "ZIP");

            }
            //doc.Load(AppDomain.CurrentDomain.BaseDirectory + "ZIP\\ListUpdate.xml");
            try
            {

                for (int i = 0; i < filesNewVersion.Length; i++)
                {
                    string f = filesNewVersion[i];
                    finfo = new System.IO.FileInfo(f);

                    if (finfo.Extension == ".pdb"
                        || finfo.Extension == ".log"
                        || finfo.Extension == ".manifest"
                        || finfo.Extension == ".lic"
                        || finfo.Extension == ".application"
                        || finfo.Name.Contains("vshost")
                        || finfo.Name.Contains(".old")
                        || finfo.Name.Contains(".zip")
                        || finfo.Name.Contains(".rar")
                        //|| finfo.Name.Contains("Autoupdater.config")
                        //  || finfo.Name.Contains("ECS.AutoUpdater")
                        //    || finfo.Name.Contains("AppAutoUpdate")
                              || finfo.Name.Contains("Error.log")
                        || finfo.Name.Contains("Error.txt")
                        || f.Contains("Errors"))
                        continue;


                    fvInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(f);

                   
                    XmlDocument docNodeChild = new XmlDocument();
                    XmlNode node;

                    //Templte LanNT: bỏ bớt các thuộc tính nếu thấy không cần
                    //Size là size file zip còn full size là kích thước thật của tệp tin
                    //<RemoteFile path="Accessibility.dll.zip" lastver="2.0.50727.4927" url="http://ecs.softech.cloud/data/sqlfiles.zip" isDatabase="true" needRestart="false" size="10752"/>
                    string path = "";
                    path = GetFolderPath(folderPath, f) + Path.GetFileName(f) + zip;

                    bool needRestart = MakeConfig.Install.NeedRestartFile.Exists(o => o.Name.Equals(Path.GetFileName(f), StringComparison.OrdinalIgnoreCase));
                    bool isDatabase = f.ToLower().Contains(folderPath.ToLower() + "\\" + MakeConfig.Install.FolderDatabase.ToLower());

                    string sfmtNode = string.Format(@"<RemoteFile path=""{0}"" lastver=""{1}"" size=""{2}"" isDatabase=""{3}"" needRestart=""{4}"" fullSize=""{5}"" />",
                                                path,
                                                fvInfo.ProductVersion != null ? fvInfo.ProductVersion : new Version().ToString(),
                                                finfo.Length, isDatabase, needRestart, finfo.Length);
                    docNodeChild.Load(new StringReader(sfmtNode));
                    node = doc.ImportNode(docNodeChild.FirstChild, true);
                    doc.DocumentElement.AppendChild(node);
                    long zipSize = 0;
                    if (zipFile)
                    {
                        if (docSource != null)
                        {

                            XmlNode element = docSource.SelectSingleNode("UpdateFileList/RemoteFile[@path='" + path + "']");
                            if (element != null)
                            {
                                ECS.AutoUpdate.RemoteFile clientFile = new ECS.AutoUpdate.RemoteFile(node, string.Empty);
                                ECS.AutoUpdate.RemoteFile serverFile = new ECS.AutoUpdate.RemoteFile(element, string.Empty);
                                if (serverFile.Path.Contains("VOT_guide"))
                                {
                                    
                                }
                                zipSize = (long)serverFile.Size;
                                //clientFile.Size = (int)zipSize;
                                if (!clientFile.Equals(serverFile))
                                {
                                    Zip(f, folderPath + "\\", AppDomain.CurrentDomain.BaseDirectory + "ZIP", ref zipSize);
                                }

                            }
                            else
                                Zip(f, folderPath + "\\", AppDomain.CurrentDomain.BaseDirectory + "ZIP", ref zipSize);

                        }
                        else
                            Zip(f, folderPath + "\\", AppDomain.CurrentDomain.BaseDirectory + "ZIP", ref zipSize);

                    }

                    node.Attributes["size"].Value = zipSize.ToString();
                    OnZip(new ZipEventArgs(path, finfo.Length, (int)(((i + 1) * 100) / filesNewVersion.Length), doc.InnerXml));
                }
                OnZip(new ZipEventArgs(string.Empty, 0, 100, doc.InnerXml));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }
    }
}
