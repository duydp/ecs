﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
  public partial  class VAL8110 : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute A01 { get; set; }
        public PropertiesAttribute A11 { get; set; }
        public PropertiesAttribute A02 { get; set; }
        public PropertiesAttribute A03 { get; set; }
        public PropertiesAttribute A04 { get; set; }
        public PropertiesAttribute A05 { get; set; }
        public PropertiesAttribute A06 { get; set; }
        public PropertiesAttribute A07 { get; set; }
        public PropertiesAttribute A08 { get; set; }
        public PropertiesAttribute A09 { get; set; }
        public PropertiesAttribute A10 { get; set; }
        public VAL8110()
        {
            A01 = new PropertiesAttribute(12, typeof(int));
            A11 = new PropertiesAttribute(1, typeof(string));
            A02 = new PropertiesAttribute(6, typeof(string));
            A03 = new PropertiesAttribute(8, typeof(DateTime));
            A04 = new PropertiesAttribute(5, typeof(string));
            A05 = new PropertiesAttribute(50, typeof(string));
            A06 = new PropertiesAttribute(13, typeof(string));
            A07 = new PropertiesAttribute(100, typeof(string));
            A08 = new PropertiesAttribute(8, typeof(DateTime));
            A09 = new PropertiesAttribute(8, typeof(DateTime));
            A10 = new PropertiesAttribute(140, typeof(string));
        }
    }

}
