﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT;
//using Company.GC.BLL.SXXK;
namespace Company.Interface.GC
{
    public partial class ReadExcelFormNPLPhanBo : BaseForm
    {
        public DataTable table = new DataTable();
        public ReadExcelFormNPLPhanBo()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

     


       
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow,setText("Dòng bắt đầu phải lớn hơn 0","This value must be greater than 0 "));
                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {

                ShowMessage(ex.Message+" ====", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char sotkColumn = Convert.ToChar(txtsoTK.Text.Trim());
            int sotkCol = ConvertCharToInt(sotkColumn);

            char malhColumn = Convert.ToChar(txtmaLH.Text.Trim());
            int malhCol = ConvertCharToInt(malhColumn);

            char namdkColumn = Convert.ToChar(txtNamDK.Text.Trim());
            int namdkCol = ConvertCharToInt(namdkColumn);

            char manplColumn = Convert.ToChar(txtMaNPL.Text.Trim());
            int manplCol = ConvertCharToInt(manplColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuong.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);



            int rowBatDau = Convert.ToInt32(txtRow.Text)-1;

            table = new DataTable();
            table.Columns.Add("SOTK");
            table.Columns.Add("MALH");
            table.Columns.Add("NamDK");
            table.Columns.Add("MaNPL");
            table.Columns.Add("Luong");

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= rowBatDau)
                {
                    try
                    {
                        string maNPLTKX = wsr.Cells[manplCol].Value.ToString();
                        if (maNPLTKX == null || maNPLTKX == "")
                            continue;
                        DataRow row = table.NewRow();
                        row["MaNPL"] = maNPLTKX;
                        row["SOTK"] = wsr.Cells[sotkCol].Value.ToString();
                        row["MALH"] = wsr.Cells[malhCol].Value.ToString().Trim();
                        row["NamDK"] = Convert.ToInt32(wsr.Cells[namdkCol].Value);
                        row["Luong"] = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                        table.Rows.Add(row);
                    }
                    catch { }
                }
            }
            this.Close();

        }
       

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}