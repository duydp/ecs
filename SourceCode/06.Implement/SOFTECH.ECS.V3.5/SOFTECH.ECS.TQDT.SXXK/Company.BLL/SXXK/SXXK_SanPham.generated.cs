﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class SXXK_SanPham : ICloneable
	{
		#region Properties.
		
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string Ma { set; get; }
		public string Ten { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
        public int STT { set; get; }
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<SXXK_SanPham> ConvertToCollection(IDataReader reader)
		{
			List<SXXK_SanPham> collection = new List<SXXK_SanPham>();
			while (reader.Read())
			{
				SXXK_SanPham entity = new SXXK_SanPham();
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_SXXK_SanPham VALUES(@MaHaiQuan, @MaDoanhNghiep, @Ma, @Ten, @MaHS, @DVT_ID)";
            string update = "UPDATE t_SXXK_SanPham SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_SanPham WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_SXXK_SanPham VALUES(@MaHaiQuan, @MaDoanhNghiep, @Ma, @Ten, @MaHS, @DVT_ID)";
            string update = "UPDATE t_SXXK_SanPham SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_SanPham WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.NVarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SXXK_SanPham Load(string maDoanhNghiep, string ma, string maHaiQuan)
		{
			const string spName = "[dbo].[p_SXXK_SanPham_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, ma);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<SXXK_SanPham> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<SXXK_SanPham> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<SXXK_SanPham> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_SanPham_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_SanPham_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_SanPham_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_SanPham_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertSXXK_SanPham(string maHaiQuan, string maDoanhNghiep, string ma, string ten, string maHS, string dVT_ID)
		{
			SXXK_SanPham entity = new SXXK_SanPham();	
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SXXK_SanPham_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<SXXK_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_SanPham item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSXXK_SanPham(string maHaiQuan, string maDoanhNghiep, string ma, string ten, string maHS, string dVT_ID)
		{
			SXXK_SanPham entity = new SXXK_SanPham();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SXXK_SanPham_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<SXXK_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_SanPham item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSXXK_SanPham(string maHaiQuan, string maDoanhNghiep, string ma, string ten, string maHS, string dVT_ID)
		{
			SXXK_SanPham entity = new SXXK_SanPham();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_SanPham_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<SXXK_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_SanPham item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSXXK_SanPham(string maDoanhNghiep, string ma, string maHaiQuan)
		{
			SXXK_SanPham entity = new SXXK_SanPham();
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.Ma = ma;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_SanPham_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, Ma);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SXXK_SanPham_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<SXXK_SanPham> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_SanPham item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}