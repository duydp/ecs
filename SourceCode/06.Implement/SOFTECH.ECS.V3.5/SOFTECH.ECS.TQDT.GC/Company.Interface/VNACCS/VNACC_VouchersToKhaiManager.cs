﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.Interface.VNACCS.Vouchers;


#endif
#if SXXK_V4
using Company.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.SXXK;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.Interface.VNACCS.Vouchers;

#endif
#if KD_V4
using Company.KD.BLL.KDT;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.BLL.VNACCS;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.Interface.VNACCS.Vouchers;
#endif
namespace Company.Interface.VNACCS
{
    public partial class VNACC_VouchersToKhaiManager : Company.Interface.BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        DataSet ds = new DataSet();
        DataSet dsTKMD = new DataSet();
        public VNACC_VouchersToKhaiManager()
        {
            InitializeComponent();
        }
#if GC_V4
        public HopDong hd = new HopDong();
        public bool isPhieuXuat = false;
#endif
        private void VNACC_VouchersToKhaiManager_Load(object sender, EventArgs e)
        {
            this.AcceptButton = this.btnTimKiem;
            cbbPhanLuong.SelectedValue = 0;
            cbbTrangThai.SelectedValue = 0;
            ctrMaHaiQuan.CategoryType = ECategory.A038;
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            txtNamDangKy.Text = DateTime.Today.Year.ToString();
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;

#if SXXK_V4
            grList.RootTable.Columns["HopDong_So"].Visible = false;
#elif GC_V4
            grList.RootTable.Columns["HopDong_So"].Visible = true;
            if (hd.ID != 0)
            {
                txtNamDangKy.Text = hd.NamTN.ToString();
                cbbTrangThai.SelectedValue = 3;
            }
#elif KD_V4
            grList.RootTable.Columns["HopDong_So"].Visible = false;
#endif
        }
        private void LoadData(string where)
        {
#if GC_V4
            if (hd.ID != 0)
            {
                where += " And TKMD.HopDong_ID = " + hd.ID;
                if (isPhieuXuat)
                    where += " AND TKMD.MaLoaiHinh IN ('E52','E54','E56')";
                else
                {
                    where += " AND TKMD.SoToKhai NOT IN (SELECT ToKhai FROM t_KDT_GC_PhieuNhapKho WHERE SoHopDong ='" + hd.SoHopDong + "' )";
                    where += " AND TKMD.LoaiHang='N'";
                }
            }

#endif
            ds = VNACC_Category_Common.SelectDynamic("ReferenceDB in ('E001','E002')", null);
            dsTKMD = KDT_VNACC_ToKhaiMauDich.SelectDynamicVoucher(where, null);
            grList.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
            grList.DataSource = dsTKMD.Tables[0];


        }
        private string GetSeachWhere()
        {
            try
            {


                string soToKhai = txtSoToKhai.Text.Trim();
                string phanluong = (cbbPhanLuong.SelectedValue.ToString() == "0") ? "" : cbbPhanLuong.SelectedValue.ToString().Trim();
                string trangthai = (cbbTrangThai.SelectedValue == null) ? "1" : cbbTrangThai.SelectedValue.ToString().Trim();
                DateTime ngayDangKy = clcTuNgay.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();

                string where = String.Empty;
                where = " TKMD.MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " AND TKMD.TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " AND TKMD.SoToKhai like '%" + soToKhai + "%' ";
                if (phanluong != "")
                    where = where + " AND TKMD.MaPhanLoaiKiemTra='" + phanluong + "' ";
                if (haiquan != "")
                    where = where + " AND TKMD.CoQuanHaiQuan like '" + haiquan + "%' ";
                if (txtNamDangKy.Text != "" && Convert.ToInt64(txtNamDangKy.Text) > 1900)
                    if (trangthai != "0" && trangthai != "1")
                        where = where + " AND year(TKMD.NgayDangKy)=" + txtNamDangKy.Text + " ";
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND TKMD.MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND TKMD.MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!string.IsNullOrEmpty(txtSoHoaDon.Text))
                    where = where + " AND TKMD.SoHoaDon like '%" + txtSoHoaDon.Text.Trim() + "%' ";
                if (!string.IsNullOrEmpty(txtSoTKDauTien.Text))
                    where = where + " AND TKMD.SoToKhaiDauTien like '%" + txtSoTKDauTien.Text.Trim() + "%' ";

                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (TKMD.NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {

                ListTKMD.Clear();

                LoadData(GetSeachWhere());

                grList.DataSource = dsTKMD.Tables[0];
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách Tờ khai VNACCS  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + ucCalendar1.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                    if (pl == "1")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                    else if (pl == "2")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                    else if (pl == "3")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                    else
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                }
                if (e.Row.Cells["NgayPhatHanhHD"].Value != null)
                {
                    DateTime ngayHD = System.Convert.ToDateTime(e.Row.Cells["NgayPhatHanhHD"].Value.ToString());
                    if (ngayHD.Year == 1900)
                        e.Row.Cells["NgayPhatHanhHD"].Text = "";
                }
                if (e.Row.Cells["NgayDangKy"].Value != null)
                {
                    DateTime ngaydk = System.Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                    if (ngaydk.Year == 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemDoHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemDoHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemDoHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemDoHang"].Text = e.Row.Cells["MaDiaDiemDoHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemXepHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemXepHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemXepHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemXepHang"].Text = e.Row.Cells["MaDiaDiemXepHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                }
#if GC_V4
                if (e.Row.Cells["HopDong_So"].Value != null)
                {
                    try
                    {
                        int HD = 0;
                        HD = Convert.ToInt32(e.Row.Cells["HopDong_ID"].Value.ToString());
                        HopDong hd = HopDong.Load(HD);
                        e.Row.Cells["HopDong_So"].Text = hd.SoHopDong;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
#endif
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    //TKMD = TKMD.LoadToKhai(id);
                    TKMD.LoadFull();
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    btnTimKiem_Click(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void grList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnAutoFeedBack_Click(object sender, EventArgs e)
        {
            VNACC_AutoFeedBackVouchers f = new VNACC_AutoFeedBackVouchers();
            f.ShowDialog(this);
        }
    }
}
