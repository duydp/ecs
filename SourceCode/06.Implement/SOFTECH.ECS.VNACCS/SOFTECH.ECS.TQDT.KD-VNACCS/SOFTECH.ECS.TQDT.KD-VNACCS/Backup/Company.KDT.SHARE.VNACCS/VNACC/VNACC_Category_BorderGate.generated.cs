using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_BorderGate : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string BorderGateCode { set; get; }
		public string BorderGateName { set; get; }
		public string CustomsOfficeCode { set; get; }
		public string ImmigrationBureauUserCode { set; get; }
		public int ArrivalDepartureForImmigration { set; get; }
		public int PassengerForImmigration { set; get; }
		public string QuarantineOfficeUserCode { set; get; }
		public int ArrivalDepartureForQuarantine { set; get; }
		public int PassengerForQuarantine { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_BorderGate> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_BorderGate> collection = new List<VNACC_Category_BorderGate>();
			while (reader.Read())
			{
				VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("BorderGateCode"))) entity.BorderGateCode = reader.GetString(reader.GetOrdinal("BorderGateCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("BorderGateName"))) entity.BorderGateName = reader.GetString(reader.GetOrdinal("BorderGateName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCode"))) entity.CustomsOfficeCode = reader.GetString(reader.GetOrdinal("CustomsOfficeCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImmigrationBureauUserCode"))) entity.ImmigrationBureauUserCode = reader.GetString(reader.GetOrdinal("ImmigrationBureauUserCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDepartureForImmigration"))) entity.ArrivalDepartureForImmigration = reader.GetInt32(reader.GetOrdinal("ArrivalDepartureForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassengerForImmigration"))) entity.PassengerForImmigration = reader.GetInt32(reader.GetOrdinal("PassengerForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuarantineOfficeUserCode"))) entity.QuarantineOfficeUserCode = reader.GetString(reader.GetOrdinal("QuarantineOfficeUserCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDepartureForQuarantine"))) entity.ArrivalDepartureForQuarantine = reader.GetInt32(reader.GetOrdinal("ArrivalDepartureForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassengerForQuarantine"))) entity.PassengerForQuarantine = reader.GetInt32(reader.GetOrdinal("PassengerForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACC_Category_BorderGate> collection, string borderGateCode)
        {
            foreach (VNACC_Category_BorderGate item in collection)
            {
                if (item.BorderGateCode == borderGateCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_BorderGate VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @BorderGateCode, @BorderGateName, @CustomsOfficeCode, @ImmigrationBureauUserCode, @ArrivalDepartureForImmigration, @PassengerForImmigration, @QuarantineOfficeUserCode, @ArrivalDepartureForQuarantine, @PassengerForQuarantine, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_BorderGate SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, BorderGateName = @BorderGateName, CustomsOfficeCode = @CustomsOfficeCode, ImmigrationBureauUserCode = @ImmigrationBureauUserCode, ArrivalDepartureForImmigration = @ArrivalDepartureForImmigration, PassengerForImmigration = @PassengerForImmigration, QuarantineOfficeUserCode = @QuarantineOfficeUserCode, ArrivalDepartureForQuarantine = @ArrivalDepartureForQuarantine, PassengerForQuarantine = @PassengerForQuarantine, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE BorderGateCode = @BorderGateCode";
            string delete = "DELETE FROM t_VNACC_Category_BorderGate WHERE BorderGateCode = @BorderGateCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BorderGateName", SqlDbType.NVarChar, "BorderGateName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, "ImmigrationBureauUserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, "QuarantineOfficeUserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BorderGateName", SqlDbType.NVarChar, "BorderGateName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, "ImmigrationBureauUserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, "QuarantineOfficeUserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_BorderGate VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @BorderGateCode, @BorderGateName, @CustomsOfficeCode, @ImmigrationBureauUserCode, @ArrivalDepartureForImmigration, @PassengerForImmigration, @QuarantineOfficeUserCode, @ArrivalDepartureForQuarantine, @PassengerForQuarantine, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_BorderGate SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, BorderGateName = @BorderGateName, CustomsOfficeCode = @CustomsOfficeCode, ImmigrationBureauUserCode = @ImmigrationBureauUserCode, ArrivalDepartureForImmigration = @ArrivalDepartureForImmigration, PassengerForImmigration = @PassengerForImmigration, QuarantineOfficeUserCode = @QuarantineOfficeUserCode, ArrivalDepartureForQuarantine = @ArrivalDepartureForQuarantine, PassengerForQuarantine = @PassengerForQuarantine, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE BorderGateCode = @BorderGateCode";
            string delete = "DELETE FROM t_VNACC_Category_BorderGate WHERE BorderGateCode = @BorderGateCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BorderGateName", SqlDbType.NVarChar, "BorderGateName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, "ImmigrationBureauUserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, "QuarantineOfficeUserCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BorderGateName", SqlDbType.NVarChar, "BorderGateName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, "ImmigrationBureauUserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, "QuarantineOfficeUserCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@BorderGateCode", SqlDbType.VarChar, "BorderGateCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_BorderGate Load(string borderGateCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_BorderGate_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BorderGateCode", SqlDbType.VarChar, borderGateCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_BorderGate> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_BorderGate> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_BorderGate> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_BorderGate_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_BorderGate_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_BorderGate_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_BorderGate_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_BorderGate(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string borderGateName, string customsOfficeCode, string immigrationBureauUserCode, int arrivalDepartureForImmigration, int passengerForImmigration, string quarantineOfficeUserCode, int arrivalDepartureForQuarantine, int passengerForQuarantine, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.BorderGateName = borderGateName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.ImmigrationBureauUserCode = immigrationBureauUserCode;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.QuarantineOfficeUserCode = quarantineOfficeUserCode;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_BorderGate_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BorderGateCode", SqlDbType.VarChar, BorderGateCode);
			db.AddInParameter(dbCommand, "@BorderGateName", SqlDbType.NVarChar, BorderGateName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, ImmigrationBureauUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, QuarantineOfficeUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_BorderGate> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_BorderGate item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_BorderGate(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string borderGateCode, string borderGateName, string customsOfficeCode, string immigrationBureauUserCode, int arrivalDepartureForImmigration, int passengerForImmigration, string quarantineOfficeUserCode, int arrivalDepartureForQuarantine, int passengerForQuarantine, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.BorderGateCode = borderGateCode;
			entity.BorderGateName = borderGateName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.ImmigrationBureauUserCode = immigrationBureauUserCode;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.QuarantineOfficeUserCode = quarantineOfficeUserCode;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_BorderGate_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BorderGateCode", SqlDbType.VarChar, BorderGateCode);
			db.AddInParameter(dbCommand, "@BorderGateName", SqlDbType.NVarChar, BorderGateName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, ImmigrationBureauUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, QuarantineOfficeUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_BorderGate> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_BorderGate item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_BorderGate(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string borderGateCode, string borderGateName, string customsOfficeCode, string immigrationBureauUserCode, int arrivalDepartureForImmigration, int passengerForImmigration, string quarantineOfficeUserCode, int arrivalDepartureForQuarantine, int passengerForQuarantine, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.BorderGateCode = borderGateCode;
			entity.BorderGateName = borderGateName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.ImmigrationBureauUserCode = immigrationBureauUserCode;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.QuarantineOfficeUserCode = quarantineOfficeUserCode;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_BorderGate_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@BorderGateCode", SqlDbType.VarChar, BorderGateCode);
			db.AddInParameter(dbCommand, "@BorderGateName", SqlDbType.NVarChar, BorderGateName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@ImmigrationBureauUserCode", SqlDbType.VarChar, ImmigrationBureauUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@QuarantineOfficeUserCode", SqlDbType.VarChar, QuarantineOfficeUserCode);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_BorderGate> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_BorderGate item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_BorderGate(string borderGateCode)
		{
			VNACC_Category_BorderGate entity = new VNACC_Category_BorderGate();
			entity.BorderGateCode = borderGateCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_BorderGate_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BorderGateCode", SqlDbType.VarChar, BorderGateCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_BorderGate> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_BorderGate item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}