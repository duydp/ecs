ALTER TABLE T_KDT_THUPHI_TOKHAI_DSCONTAINER
ALTER COLUMN TinhChat NVARCHAR(50) NULL
GO

ALTER TABLE T_KDT_THUPHI_TOKHAI_DSCONTAINER
ALTER COLUMN Loai NVARCHAR(50) NULL

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
(
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
)
VALUES 
(
	@TK_ID,
	@SoVanDon,
	@TongTrongLuong,
	@DVT,
	@SoContainer,
	@SoSeal,
	@Loai,
	@TinhChat,
	@SoLuong,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]
	@ID bigint,
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
SET
	[TK_ID] = @TK_ID,
	[SoVanDon] = @SoVanDon,
	[TongTrongLuong] = @TongTrongLuong,
	[DVT] = @DVT,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[Loai] = @Loai,
	[TinhChat] = @TinhChat,
	[SoLuong] = @SoLuong,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]
	@ID bigint,
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] 
		SET
			[TK_ID] = @TK_ID,
			[SoVanDon] = @SoVanDon,
			[TongTrongLuong] = @TongTrongLuong,
			[DVT] = @DVT,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[Loai] = @Loai,
			[TinhChat] = @TinhChat,
			[SoLuong] = @SoLuong,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
		(
			[TK_ID],
			[SoVanDon],
			[TongTrongLuong],
			[DVT],
			[SoContainer],
			[SoSeal],
			[Loai],
			[TinhChat],
			[SoLuong],
			[GhiChu]
		)
		VALUES 
		(
			@TK_ID,
			@SoVanDon,
			@TongTrongLuong,
			@DVT,
			@SoContainer,
			@SoSeal,
			@Loai,
			@TinhChat,
			@SoLuong,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]
	@TK_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[TK_ID] = @TK_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]
	@TK_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[TK_ID] = @TK_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]	

GO



IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.8',GETDATE(), N'CẬP NHẬT PROCEDURE THU PHÍ HẢI PHÒNG ')
END