﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSPMaperForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();
        public bool isAdd = true;
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
#elif GC_V4
        SanPhamRegistedForm SPRegistedForm;
        public HopDong HD;
#endif
        public WareHouseSPMaperForm()
        {
            InitializeComponent();

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhKT.DataSource = this._DonViTinh;
            txtDonViTinhKT.DisplayMember = "Ten";
            txtDonViTinhKT.ValueMember = "Ten";
            txtDonViTinhKT.ReadOnly = false;

            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "Ten";
            txtDonViTinhSP.ReadOnly = false;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseSPMaperForm_Load(object sender, EventArgs e)
        {
            SetSP();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = SP.SPCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetSPMap()
        {
            try
            {

                SPMap.MASP = txtMaSP.Text.ToString();
                SPMap.TENSP = txtTenSP.Text.ToString();
                SPMap.DVT = txtDonViTinhSP.Text.ToString();
                SPMap.MASPMAP = txtMaSPKT.Text.ToString();
                SPMap.TENSPMAP = txtTenSPKT.Text.ToString();
                SPMap.DVTMAP = txtDonViTinhKT.Text.ToString();
                SPMap.TYLEQD = Convert.ToDecimal(txtTLQD.Text);
                SPMap.HOPDONG_ID = HD.ID;
                if (isAdd)
                    SP.SPCollection.Add(SPMap);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetSP()
        {
            SP.MASP = txtMaSPKT.Text.ToString();
            SP.TENSP = txtTenSPKT.Text.ToString();
            SP.MAHS = ctrMaSoHang.Code;
            SP.DVT = txtDonViTinhKT.Text.ToString();
            SP.TENTIENGANH = txtTenTiengAnh.Text.ToString();
            SP.GHICHU = txtGhiChu.Text.ToString();
            SP.MAKHO = cbbMaKho.Text.ToString();
            SP.TAIKHOAN = cbbTK.Text.ToString();
            SP.DONHANG = txtDonHang.Text.ToString();
            SP.NGUOIBAN = txtNguoiBan.Text.ToString();
            SP.KICHTHUOC = txtKichThuoc.Text.ToString();
            SP.MAUSAC = txtMauSac.Text.ToString();
            SP.HOPDONG_ID = HD.ID;

            GetSPMap();
        }
        private void SetSP()
        {
            txtMaSPKT.Text = SP.MASP;
            txtTenSPKT.Text = SP.TENSP;
            ctrMaSoHang.Code = SP.MAHS;
            txtDonViTinhKT.Text = SP.DVT;
            txtTenTiengAnh.Text = SP.TENTIENGANH;
            txtGhiChu.Text = SP.GHICHU;
            cbbMaKho.Value = SP.MAKHO;
            cbbTK.Value = SP.TAIKHOAN;
            txtDonHang.Text = SP.DONHANG;
            txtNguoiBan.Text = SP.NGUOIBAN;
            txtMauSac.Text = SP.MAUSAC;
            txtKichThuoc.Text = SP.KICHTHUOC;

            BindData();
        }
        private void SetSPMap()
        {
            txtMaSP.Text = SPMap.MASP;
            txtTenSP.Text = SPMap.TENSP;
            txtDonViTinhSP.Text = SPMap.DVT;
            txtTLQD.Text = SPMap.TYLEQD.ToString();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate;

                isValid &= ValidateControl.ValidateNull(txtMaSPKT, errorProvider, " Mã Sản phẩm ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSPKT, errorProvider, " Tên Sản phẩm ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhKT, errorProvider, " Đơn vị tính ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtMaSP, errorProvider, " Mã Sản phẩm ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSP, errorProvider, " Tên Sản phẩm ");
                isValid &= ValidateControl.ValidateNull(txtMaHSSP, errorProvider, " Mã HS ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhSP, errorProvider, " Đơn vị tính ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTLQD, errorProvider, " Tỷ lệ quy đổi ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
#if SXXK_V4

            switch (this.loaiHangHoa)
            {
                case "N":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                    {
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "T":
                    if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                    {
                        if (this.NPLRegistedForm_CX == null)
                            this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                        this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                        this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.NPLRegistedForm_CX.LoaiNPL = "3";
                        this.NPLRegistedForm_CX.ShowDialog(this);
                        if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                        {
                            txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                            ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                            ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        }
                    }
                    txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "S":

                    if (this.SPRegistedForm == null)
                        this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.CalledForm = "HangMauDichForm";
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog(this);
                    if (this.SPRegistedForm.SanPhamSelected.Ma != "" && this.SPRegistedForm.SanPhamSelected != null)
                    {
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
            }
#elif GC_V4
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = HD.ID;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaSP.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenSP.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHSSP.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        txtDonViTinhSP.Text = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                    }
#elif KD_V4
#endif
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
#if GC_V4
            bool isNotFound = false;
            SanPham sp = new SanPham();
            sp.HopDong_ID = HD.ID;
            sp.Ma = txtMaSP.Text.Trim();
            if (sp.Load())
            {
                txtMaSP.Text = sp.Ma;
                ctrMaSoHang.Code = sp.MaHS;
                txtTenSP.Text = sp.Ten;
                txtDonViTinhSP.Text = DonViTinh_GetName(sp.DVT_ID);
            }
            else
            {
                isNotFound = true;
            }
            if (isNotFound)
            {

                txtTenSP.Text = txtDonViTinhSP.Text = string.Empty;
                return;
            }
#elif SXXK_V4
            if (loaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.Text = npl.Ma;
                    //if (ctrMaSoHang.Code.Trim().Length == 0)
                        ctrMaSoHang.Code = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

                }


            }
            else if (loaiHangHoa == "S")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.Text = sp.Ma;
                    //if (ctrMaSoHang.Code.Trim().Length == 0)
                        ctrMaSoHang.Code = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));

                }
            }
#endif
        }
        private void CheckExitSP()
        {
            //Kiểm tra SP Map đã được khai báo .
            //if (isAdd)
            //{
            //    List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID = " + HD.ID + " AND MASP = '" + txtMaSPKT.Text.ToString().Trim() + "'", "");
            //    if (SPCollection.Count >= 1)
            //    {
            //        errorProvider.SetError(txtMaSPKT, "MÃ SẢN PHẨM  : ''" + txtMaSPKT.Text.ToString() + "'' NÀY ĐÃ MAP TRƯỚC ĐÓ .");
            //        return;
            //    }   
            //}
            if (isAdd)
            {
                //Kiểm tra SP VNACCS đã có trên lưới .
                foreach (T_KHOKETOAN_SANPHAM_MAP item in SP.SPCollection)
                {
                    if (item.MASPMAP == txtMaSP.Text.ToString().Trim())
                    {
                        errorProvider.SetError(txtMaSP, "MÃ SẢN PHẨM : ''" + txtMaSP.Text.ToString() + "'' ĐÃ CÓ TRÊN DANH SÁCH PHÍA DƯỚI .");
                        return;
                    }
                }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetSP();
                CheckExitSP();
                SP.InsertUpdateFull();
                isAdd = true;
                BindData();
                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                txtMaSP.Text = String.Empty;
                txtTenSP.Text = String.Empty;
                txtMaHSSP.Text = String.Empty;
                txtDonViTinhSP.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_SANPHAM_MAP> ItemColl = new List<T_KHOKETOAN_SANPHAM_MAP>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_SANPHAM_MAP)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_SANPHAM_MAP item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        SP.SPCollection.Remove(item);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                    SPMap = (T_KHOKETOAN_SANPHAM_MAP)e.Row.DataRow;
                    isAdd = false;
                    SetSPMap();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
