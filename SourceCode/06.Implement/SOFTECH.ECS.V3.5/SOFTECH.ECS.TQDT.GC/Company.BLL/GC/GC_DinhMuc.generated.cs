﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class GC_DinhMuc : ICloneable
	{
		#region Properties.
		
		public long HopDong_ID { set; get; }
		public string MaSanPham { set; get; }
		public string MaNguyenPhuLieu { set; get; }
		public decimal DinhMucSuDung { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public int STTHang { set; get; }
		public decimal NPL_TuCungUng { set; get; }
		public string TenSanPham { set; get; }
		public string TenNPL { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GuidString { set; get; }
		public long LenhSanXuat_ID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<GC_DinhMuc> ConvertToCollection(IDataReader reader)
		{
			List<GC_DinhMuc> collection = new List<GC_DinhMuc>();
			while (reader.Read())
			{
				GC_DinhMuc entity = new GC_DinhMuc();
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NPL_TuCungUng"))) entity.NPL_TuCungUng = reader.GetDecimal(reader.GetOrdinal("NPL_TuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidString"))) entity.GuidString = reader.GetString(reader.GetOrdinal("GuidString"));
				if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GC_DinhMuc VALUES(@HopDong_ID, @MaSanPham, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @STTHang, @NPL_TuCungUng, @TenSanPham, @TenNPL, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @GuidString, @LenhSanXuat_ID)";
            string update = "UPDATE t_GC_DinhMuc SET HopDong_ID = @HopDong_ID, MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, STTHang = @STTHang, NPL_TuCungUng = @NPL_TuCungUng, TenSanPham = @TenSanPham, TenNPL = @TenNPL, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, GuidString = @GuidString, LenhSanXuat_ID = @LenhSanXuat_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_GC_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GC_DinhMuc VALUES(@HopDong_ID, @MaSanPham, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @STTHang, @NPL_TuCungUng, @TenSanPham, @TenNPL, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @GuidString, @LenhSanXuat_ID)";
            string update = "UPDATE t_GC_DinhMuc SET HopDong_ID = @HopDong_ID, MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, STTHang = @STTHang, NPL_TuCungUng = @NPL_TuCungUng, TenSanPham = @TenSanPham, TenNPL = @TenNPL, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, GuidString = @GuidString, LenhSanXuat_ID = @LenhSanXuat_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_GC_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NPL_TuCungUng", SqlDbType.Decimal, "NPL_TuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSanPham", SqlDbType.NVarChar, "TenSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaSanPham", SqlDbType.VarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GC_DinhMuc Load(long hopDong_ID, string maSanPham, string maNguyenPhuLieu, long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, maNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<GC_DinhMuc> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<GC_DinhMuc> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<GC_DinhMuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<GC_DinhMuc> SelectCollectionBy_HopDong_ID(long hopDong_ID)
		{
            IDataReader reader = SelectReaderBy_HopDong_ID(hopDong_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GC_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GC_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "p_GC_DinhMuc_SelectBy_HopDong_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertGC_DinhMuc(long hopDong_ID, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, int sTTHang, decimal nPL_TuCungUng, string tenSanPham, string tenNPL, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, long lenhSanXuat_ID)
		{
			GC_DinhMuc entity = new GC_DinhMuc();	
			entity.HopDong_ID = hopDong_ID;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.STTHang = sTTHang;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.TenSanPham = tenSanPham;
			entity.TenNPL = tenNPL;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GC_DinhMuc_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<GC_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GC_DinhMuc item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGC_DinhMuc(long hopDong_ID, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, int sTTHang, decimal nPL_TuCungUng, string tenSanPham, string tenNPL, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, long lenhSanXuat_ID)
		{
			GC_DinhMuc entity = new GC_DinhMuc();			
			entity.HopDong_ID = hopDong_ID;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.STTHang = sTTHang;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.TenSanPham = tenSanPham;
			entity.TenNPL = tenNPL;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GC_DinhMuc_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<GC_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GC_DinhMuc item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGC_DinhMuc(long hopDong_ID, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, int sTTHang, decimal nPL_TuCungUng, string tenSanPham, string tenNPL, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string guidString, long lenhSanXuat_ID)
		{
			GC_DinhMuc entity = new GC_DinhMuc();			
			entity.HopDong_ID = hopDong_ID;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.STTHang = sTTHang;
			entity.NPL_TuCungUng = nPL_TuCungUng;
			entity.TenSanPham = tenSanPham;
			entity.TenNPL = tenNPL;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GuidString = guidString;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------
        public int UpdateLenhSanXuat()
        {
            return this.UpdateLenhSanXuat(null);
        }

        //---------------------------------------------------------------------------------------------
		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
			db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
        public int UpdateLenhSanXuat(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_DinhMuc_Update_LenhSanXuat]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@NPL_TuCungUng", SqlDbType.Decimal, NPL_TuCungUng);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
            db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<GC_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GC_DinhMuc item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGC_DinhMuc(long hopDong_ID, string maSanPham, string maNguyenPhuLieu, long lenhSanXuat_ID)
		{
			GC_DinhMuc entity = new GC_DinhMuc();
			entity.HopDong_ID = hopDong_ID;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GC_DinhMuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<GC_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GC_DinhMuc item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}