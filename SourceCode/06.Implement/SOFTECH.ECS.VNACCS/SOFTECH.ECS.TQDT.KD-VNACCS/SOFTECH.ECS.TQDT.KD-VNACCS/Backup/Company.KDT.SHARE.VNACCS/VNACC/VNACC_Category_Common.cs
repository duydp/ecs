using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_Common
    {
        protected static List<VNACC_Category_Common> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_Common> collection = new List<VNACC_Category_Common>();
            while (reader.Read())
            {
                VNACC_Category_Common entity = new VNACC_Category_Common();
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceDB"))) entity.ReferenceDB = reader.GetString(reader.GetOrdinal("ReferenceDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name_VN"))) entity.Name_VN = reader.GetString(reader.GetOrdinal("Name_VN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_Common> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [ReferenceDB], [Code], Name_VN FROM [dbo].[t_VNACC_Category_Common]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_Common> SelectCollectionBy(List<VNACC_Category_Common> collections, string referenceDB)
        {
            return collections.FindAll(delegate(VNACC_Category_Common o)
            {
                return o.ReferenceDB == referenceDB;
            });
        }
    }
}