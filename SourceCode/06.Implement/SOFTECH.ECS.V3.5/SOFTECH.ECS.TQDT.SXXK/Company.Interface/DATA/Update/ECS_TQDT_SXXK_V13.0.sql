/*Hungtq*/
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_CTTT_HopDongXuatKhau' AND column_name = 'SoToKhai') > 0
BEGIN
	ALTER TABLE dbo.t_CTTT_HopDongXuatKhau
	DROP COLUMN SoToKhai
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_CTTT_HopDongXuatKhau' AND column_name = 'MaLoaiHinh') > 0
BEGIN
	ALTER TABLE dbo.t_CTTT_HopDongXuatKhau
	DROP COLUMN MaLoaiHinh
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_CTTT_HopDongXuatKhau' AND column_name = 'NgayDangKy') > 0
BEGIN
	ALTER TABLE dbo.t_CTTT_HopDongXuatKhau
	DROP COLUMN NgayDangKy
END	
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Insert]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Update]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Delete]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Load]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_HopDongXuatKhau_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Insert]
	@SoHopDong nvarchar(150),
	@NgayKy datetime,
	@TriGia numeric(18, 8),
	@GhiChu nvarchar(255),
	@Id int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_HopDongXuatKhau]
(
	[SoHopDong],
	[NgayKy],
	[TriGia],
	[GhiChu]
)
VALUES 
(
	@SoHopDong,
	@NgayKy,
	@TriGia,
	@GhiChu
)

SET @Id = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Update]
	@Id int,
	@SoHopDong nvarchar(150),
	@NgayKy datetime,
	@TriGia numeric(18, 8),
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_CTTT_HopDongXuatKhau]
SET
	[SoHopDong] = @SoHopDong,
	[NgayKy] = @NgayKy,
	[TriGia] = @TriGia,
	[GhiChu] = @GhiChu
WHERE
	[Id] = @Id

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_InsertUpdate]
	@Id int,
	@SoHopDong nvarchar(150),
	@NgayKy datetime,
	@TriGia numeric(18, 8),
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [Id] FROM [dbo].[t_CTTT_HopDongXuatKhau] WHERE [Id] = @Id)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_HopDongXuatKhau] 
		SET
			[SoHopDong] = @SoHopDong,
			[NgayKy] = @NgayKy,
			[TriGia] = @TriGia,
			[GhiChu] = @GhiChu
		WHERE
			[Id] = @Id
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_HopDongXuatKhau]
		(
			[SoHopDong],
			[NgayKy],
			[TriGia],
			[GhiChu]
		)
		VALUES 
		(
			@SoHopDong,
			@NgayKy,
			@TriGia,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Delete]
	@Id int
AS

DELETE FROM 
	[dbo].[t_CTTT_HopDongXuatKhau]
WHERE
	[Id] = @Id

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_HopDongXuatKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_Load]
	@Id int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Id],
	[SoHopDong],
	[NgayKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_HopDongXuatKhau]
WHERE
	[Id] = @Id
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Id],
	[SoHopDong],
	[NgayKy],
	[TriGia],
	[GhiChu]
FROM [dbo].[t_CTTT_HopDongXuatKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_HopDongXuatKhau_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Id],
	[SoHopDong],
	[NgayKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_HopDongXuatKhau]	

GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_CTTT_ChungTu' AND column_name = 'DongTienThanhToan') = 0
BEGIN
	ALTER TABLE dbo.t_CTTT_ChungTu
	ADD DongTienThanhToan varchar(5) null
END	
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_Insert]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_Update]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_Delete]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_Load]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTu_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Insert]
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTu]
(
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
)
VALUES 
(
	@LanThanhLy,
	@SoChungTu,
	@NgayChungTu,
	@HinhThucThanhToan,
	@TongTriGia,
	@ConLai,
	@GhiChu,
	@DongTienThanhToan
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Update]
	@ID int,
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5)
AS

UPDATE
	[dbo].[t_CTTT_ChungTu]
SET
	[LanThanhLy] = @LanThanhLy,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[HinhThucThanhToan] = @HinhThucThanhToan,
	[TongTriGia] = @TongTriGia,
	[ConLai] = @ConLai,
	[GhiChu] = @GhiChu,
	[DongTienThanhToan] = @DongTienThanhToan
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_InsertUpdate]
	@ID int,
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChungTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTu] 
		SET
			[LanThanhLy] = @LanThanhLy,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[HinhThucThanhToan] = @HinhThucThanhToan,
			[TongTriGia] = @TongTriGia,
			[ConLai] = @ConLai,
			[GhiChu] = @GhiChu,
			[DongTienThanhToan] = @DongTienThanhToan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTu]
		(
			[LanThanhLy],
			[SoChungTu],
			[NgayChungTu],
			[HinhThucThanhToan],
			[TongTriGia],
			[ConLai],
			[GhiChu],
			[DongTienThanhToan]
		)
		VALUES 
		(
			@LanThanhLy,
			@SoChungTu,
			@NgayChungTu,
			@HinhThucThanhToan,
			@TongTriGia,
			@ConLai,
			@GhiChu,
			@DongTienThanhToan
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChungTu]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChungTu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM
	[dbo].[t_CTTT_ChungTu]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM [dbo].[t_CTTT_ChungTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM
	[dbo].[t_CTTT_ChungTu]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Insert]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Update]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Delete]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Load]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Insert]
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150),
	@IDChiTiet int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
(
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
)
VALUES 
(
	@IDCT,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@TriGia,
	@GhiChu
)

SET @IDChiTiet = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Update]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChungTuChiTiet]
SET
	[IDCT] = @IDCT,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[TriGia] = @TriGia,
	[GhiChu] = @GhiChu
WHERE
	[IDChiTiet] = @IDChiTiet

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [IDChiTiet] FROM [dbo].[t_CTTT_ChungTuChiTiet] WHERE [IDChiTiet] = @IDChiTiet)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTuChiTiet] 
		SET
			[IDCT] = @IDCT,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[TriGia] = @TriGia,
			[GhiChu] = @GhiChu
		WHERE
			[IDChiTiet] = @IDChiTiet
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
		(
			[IDCT],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[TriGia],
			[GhiChu]
		)
		VALUES 
		(
			@IDCT,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@TriGia,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Delete]
	@IDChiTiet int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDChiTiet] = @IDChiTiet

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]
	@IDCT int
AS

DELETE FROM [dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDCT] = @IDCT

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChungTuChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Load]
	@IDChiTiet int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDChiTiet] = @IDChiTiet
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDCT] = @IDCT

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM [dbo].[t_CTTT_ChungTuChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Insert]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Update]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Delete]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Load]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectAll]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]

IF OBJECT_ID(N'[dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Insert]
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
(
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
)
VALUES 
(
	@IDCT,
	@MaChiPhi,
	@TriGia,
	@MaToanTu,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Update]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChiPhiKhac]
SET
	[IDCT] = @IDCT,
	[MaChiPhi] = @MaChiPhi,
	[TriGia] = @TriGia,
	[MaToanTu] = @MaToanTu,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChiPhiKhac] 
		SET
			[IDCT] = @IDCT,
			[MaChiPhi] = @MaChiPhi,
			[TriGia] = @TriGia,
			[MaToanTu] = @MaToanTu,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
		(
			[IDCT],
			[MaChiPhi],
			[TriGia],
			[MaToanTu],
			[GhiChu]
		)
		VALUES 
		(
			@IDCT,
			@MaChiPhi,
			@TriGia,
			@MaToanTu,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
	@IDCT int
AS

DELETE FROM [dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM [dbo].[t_CTTT_ChiPhiKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]	

GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT]    Script Date: 11/28/2013 17:39:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT]    Script Date: 11/28/2013 17:39:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
  
CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate_DungChoCTTT]  
    @MaDoanhNghiep VARCHAR(14) ,  
    @MaHaiQuan CHAR(6) ,  
    @TuNgay DATETIME ,  
    @DenNgay DATETIME  
AS   

BEGIN  
    SELECT  A.MaHaiQuan ,  
            A.SoToKhai ,  
            A.MaLoaiHinh ,  
            A.NamDangKy ,  
            A.NgayDangKy ,  
            A.NGAY_THN_THX ,  
            A.NgayHoanThanh ,  
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.                 
    FROM    t_SXXK_ToKhaiMauDich A  
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.         
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai  
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh  
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)  
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep  
            AND A.MaHaiQuan = @MaHaiQuan  
            AND ( A.MaLoaiHinh LIKE 'XSX%'  
                  OR A.MaLoaiHinh LIKE 'XGC%'  
                  OR A.MaLoaiHinh LIKE 'XCX%'  
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX  
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay  
            AND ( A.NGAY_THN_THX IS NOT NULL  
                  OR YEAR(A.NGAY_THN_THX) != 1900  
                )  
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.           
            AND CAST(a.SoToKhai AS VARCHAR(50))  
            + CAST(a.MaLoaiHinh AS VARCHAR(50))  
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (  
            SELECT  CAST(SoToKhai AS VARCHAR(50))  
                    + CAST(MaLoaiHinh AS VARCHAR(50))  
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))  
            FROM    dbo.t_KDT_ToKhaiMauDich  
            WHERE   MaHaiQuan = @MaHaiQuan  
                    AND MaDoanhNghiep = @MaDoanhNghiep  
                    AND TrangThaiXuLy != 1  
                    AND SoToKhai != 0 )  
    ORDER BY A.NgayDangKy ,  
            A.SoToKhai ,  
            A.MaLoaiHinh         
              
END  

GO




IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.0', GETDATE(), N'Cap nhat bang Hop dong xuat khau, bang Chung tu - Chung tu thanh toan.')
END	
