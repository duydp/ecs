﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.BLL.VNACCS
{
    public class ConvertFromVNACCS
    {
        public static Company.BLL.KDT.ToKhaiMauDich ConvertToKhaiFromVNACCS(KDT_VNACC_ToKhaiMauDich tkmdVNACC, Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            if (tkmdVNACC != null)
            {
                try
                {
                    //tkmdVNACC.LoadToKhai(tkmdVNACC.ID);
                    tkmdVNACC.LoadFull();

                    if (TKMD == null)
                        TKMD = new Company.BLL.KDT.ToKhaiMauDich();
                    //TKMD.SoToKhai = //long.MaxValue tkmdVNACC.SoToKhai;
                    decimal SoTkVNACC = System.Convert.ToDecimal(tkmdVNACC.SoToKhai.ToString().Substring(0, 11));
                    TKMD.TrangThaiVNACCS = CapSoToKhai.CapSoToKhaiV5(SoTkVNACC, tkmdVNACC);
                    TKMD.SoToKhai = TKMD.TrangThaiVNACCS.SoTK;
                    TKMD.MaLoaiHinh = CapSoToKhai.ConvertMaLoaiHinh(tkmdVNACC.MaLoaiHinh);
                    TKMD.MaHaiQuan = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4MaHaiQuan(tkmdVNACC.CoQuanHaiQuan);
                    //if (tkmdVNACC.NgayCapPhep != null && tkmdVNACC.NgayCapPhep.Year > 1900)
                    //    TKMD.NgayDangKy = tkmdVNACC.NgayCapPhep;
                    //else
                    //    TKMD.NgayDangKy = tkmdVNACC.NgayDangKy;
                    TKMD.NgayDangKy = tkmdVNACC.NgayDangKy;

                    TKMD.NgayTiepNhan = tkmdVNACC.NgayDangKy;
                    TKMD.MaDoanhNghiep = tkmdVNACC.MaDonVi;
                    TKMD.TenDoanhNghiep = tkmdVNACC.TenDonVi;
                    TKMD.MaDonViUT = tkmdVNACC.MaUyThac;
                    TKMD.TenDonViUT = tkmdVNACC.TenUyThac;
                    TKMD.TenDonViDoiTac = tkmdVNACC.TenDoiTac;
                    TKMD.MaDaiLyTTHQ = tkmdVNACC.MaDaiLyHQ;
                    TKMD.SoKien = tkmdVNACC.SoLuong;
                    TKMD.TrongLuong = tkmdVNACC.TrongLuong;
                    TKMD.DiaDiemXepHang = tkmdVNACC.TenDiaDiemXepHang;
                    TKMD.PTTT_ID = tkmdVNACC.PhuongThucTT;
                    TKMD.DKGH_ID = tkmdVNACC.MaDieuKienGiaHD;
                    TKMD.NguyenTe_ID = tkmdVNACC.MaTTHoaDon;
                    TKMD.PhiVanChuyen = tkmdVNACC.PhiVanChuyen;
                    TKMD.PhiBaoHiem = tkmdVNACC.PhiBaoHiem;
                    TKMD.DeXuatKhac = tkmdVNACC.GhiChu;
                    TKMD.PhanLuong = string.IsNullOrEmpty(tkmdVNACC.MaPhanLoaiKiemTra) ? "" : tkmdVNACC.MaPhanLoaiKiemTra.Substring(0, 1);
                    TKMD.TrangThaiXuLy = 1;
                    TKMD.SoHieuPTVT = tkmdVNACC.TenPTVC;
                    TKMD.NgayDenPTVT = tkmdVNACC.NgayHangDen;
                    TKMD.TenChuHang = ".";
                    TKMD.ChucVu = ".";
                    TKMD.SoContainer20 = Convert.ToInt32(tkmdVNACC.SoLuongCont);
                    TKMD.SoHoaDonThuongMai = tkmdVNACC.SoHoaDon;
                    TKMD.NgayHoaDonThuongMai = tkmdVNACC.NgayPhatHanhHD;
                    //TKMD.IDHopDong = tkmdVNACC.HopDong_ID;

                    TKMD.LoaiHangHoa = tkmdVNACC.LoaiHang;
                    if (string.IsNullOrEmpty(TKMD.LoaiHangHoa))
                    {
                        if (tkmdVNACC.HangCollection != null && tkmdVNACC.HangCollection.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tkmdVNACC.HangCollection[0].MaQuanLy))
                            {
                                string loaiHang = tkmdVNACC.HangCollection[0].MaQuanLy;
                                loaiHang = loaiHang.Trim();
                                if (loaiHang.StartsWith("1"))
                                    TKMD.LoaiHangHoa = "N";
                                else if (loaiHang.StartsWith("2"))
                                    TKMD.LoaiHangHoa = "S";
                                else if (loaiHang.StartsWith("3"))
                                    TKMD.LoaiHangHoa = "T";
                                else if (loaiHang.StartsWith("4"))
                                    TKMD.LoaiHangHoa = "H";
                            }
                        }
                    }
                    TKMD.TyGiaTinhThue = (tkmdVNACC.TyGiaCollection != null && tkmdVNACC.TyGiaCollection.Count > 0) ? tkmdVNACC.TyGiaCollection[0].TyGiaTinhThue : 1;
                    TKMD.SoVanDon = (tkmdVNACC.VanDonCollection != null && tkmdVNACC.VanDonCollection.Count > 0) ? tkmdVNACC.VanDonCollection[0].SoVanDon : "";
                    TKMD.NgayVanDon = tkmdVNACC.NgayHangDen;
                    int index = 0;
                    TKMD.ActionStatus = 5000;
                    if (TKMD.HMDCollection != null && TKMD.HMDCollection.Count > 0)
                        TKMD.DeleteHangCollection(null);
                    TKMD.HMDCollection = new List<Company.BLL.KDT.HangMauDich>();
                    TKMD.LoaiVanDon = tkmdVNACC.SoToKhai.ToString();
                    foreach (KDT_VNACC_HangMauDich hmdvnacc in tkmdVNACC.HangCollection)
                    {
                        index++;
                        Company.BLL.KDT.HangMauDich hmd = new Company.BLL.KDT.HangMauDich();
                        //hmd.SoThuTuHang = index;
                        hmd.MaHS = hmdvnacc.MaSoHang;
                        hmd.TenHang = hmdvnacc.TenHang;
                        if (!string.IsNullOrEmpty(hmdvnacc.ThueSuatThue))
                        {
                            if (hmdvnacc.ThueSuatThue.Contains("%"))
                                hmd.ThueSuatXNK = System.Convert.ToDecimal(hmdvnacc.ThueSuatThue.Replace("%", "").Trim());
                            else
                                hmd.ThueSuatXNK = System.Convert.ToDecimal(hmdvnacc.ThueSuatThue);
                        }
                        //hmd.ThueSuatXNK = 
                        //hmd.ThueSuatXNK = hmdvnacc.ThueSuatTuyetDoi;
                        hmd.NuocXX_ID = string.IsNullOrEmpty(hmdvnacc.NuocXuatXu) ? "VN" : hmdvnacc.NuocXuatXu;
                        hmd.SoLuong = hmdvnacc.SoLuong1;
                        hmd.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(hmdvnacc.DVTLuong1);
                        hmd.TriGiaKB = hmdvnacc.TriGiaHoaDon;
                        hmd.DonGiaKB = hmdvnacc.DonGiaHoaDon;
                        hmd.BieuThueXNK = hmdvnacc.MaBieuThueNK;
                        hmd.ThueSuatXNKGiam = Convert.ToDouble(hmdvnacc.SoTienGiamThue);
                        //hmd.TriGiaTT = hmdvnacc.TriGiaTinhThueS;
                        hmd.TriGiaTT = hmdvnacc.TriGiaTinhThueS == 0 ? hmdvnacc.TriGiaTinhThue : hmdvnacc.TriGiaTinhThueS;
                        int stt = 0;
                        if (int.TryParse(hmdvnacc.SoDong, out stt))
                            hmd.SoThuTuHang = stt;
                        else
                            hmd.SoThuTuHang = index;

                        //hmd.DonGiaTT = hmdvnacc.DonGiaTinhThue;
                        hmd.DonGiaTT = hmd.SoLuong > 0 ? hmd.TriGiaTT / hmd.SoLuong : hmdvnacc.DonGiaTinhThue;
                        hmd.ThueXNK = hmdvnacc.SoTienThue;
                        hmd.MaPhu = hmdvnacc.MaHangHoa;
                        TKMD.HMDCollection.Add(hmd);
                    }
                    return TKMD;
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage("ConvertToKhaiFromVNACCS", ex);
                    throw ex;
                }
            }
            return null;

        }
        public static bool InsertUpdateTKMDFromVNACCS(KDT_VNACC_ToKhaiMauDich tkmdVNACC)
        {
            //Logger.LocalLogger.Instance().WriteMessage("InsertUpdateTKMDFromVNACCS BEGIN", new Exception(""));

            //Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
            decimal sotk = System.Convert.ToDecimal(tkmdVNACC.SoToKhai.ToString().Substring(0, 11));
            //Company.BLL.KDT.ToKhaiMauDichCollection listTKMD = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
            Company.BLL.KDT.ToKhaiMauDich tkmd = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
            bool isCapNhat = false;
            if (tkmd != null)
            {
                tkmd.LoadHMDCollection();
                //Logger.LocalLogger.Instance().WriteMessage("tkmd.LoadHMDCollection() DONE", new Exception(""));

                isCapNhat = true;
            }
            else
                tkmd = new Company.BLL.KDT.ToKhaiMauDich();

            tkmd = ConvertToKhaiFromVNACCS(tkmdVNACC, tkmd);
            //Logger.LocalLogger.Instance().WriteMessage("ConvertToKhaiFromVNACCS() DONE", new Exception(""));

            if (tkmd != null)
            {
                tkmd.InsertUpdateFull();
                tkmd.CapNhatThongTinHangToKhaiSua();
                //Logger.LocalLogger.Instance().WriteMessage("tkmd.CapNhatThongTinHangToKhaiSua() DONE", new Exception(""));

                //else
                //    tkmd.TransgferDataToSXXK();
                return true;
            }
            else
                return false;
        }

        public static bool InsertUpdateTKMDFromVNACCSKhaiBoSung(KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkmdVNACC)
        {
            //Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
            decimal sotk = System.Convert.ToDecimal(tkmdVNACC.SoToKhai.ToString().Substring(0, 11));
            //Company.BLL.KDT.ToKhaiMauDichCollection listTKMD = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
            Company.BLL.KDT.ToKhaiMauDich tkmd = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
            bool isCapNhat = false;
            if (tkmd != null)
            {
                tkmd.LoadHMDCollection();
                isCapNhat = true;
            }
            else
                tkmd = new Company.BLL.KDT.ToKhaiMauDich();

            foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hmdKBS in tkmdVNACC.HangCollection)
            {
                KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                hmd.InsertUpdate();
            }

            if (tkmd != null)
            {
                tkmd.InsertUpdateFull();
                tkmd.CapNhatThongTinHangToKhaiSua();

                //else
                //    tkmd.TransgferDataToSXXK();
                return true;
            }
            else
                return false;

        }
        // Truyền vào số tờ khai 12 số
        public static string HuyToKhai(decimal SoToKhaiVNACCS, string MahaiQuan)
        {
            try
            {

                decimal sotk = System.Convert.ToDecimal(SoToKhaiVNACCS.ToString().Substring(0, 11));
                //Company.BLL.KDT.ToKhaiMauDichCollection listTKMD = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                Company.BLL.KDT.ToKhaiMauDich tkmd = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                int soToKhaiV4 = 0;
                if (tkmd != null)
                {
                    soToKhaiV4 = tkmd.SoToKhai;
                    Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection tkDK = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().SelectCollectionDynamic("SoToKhai = " + soToKhaiV4 + " And MaLoaiHinh like '%V%'", null);
                    if (tkDK != null && tkDK.Count > 0)
                    {
                        foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich item in tkDK)
                        {
                            if (item.SoToKhai == tkmd.SoToKhai && item.MaLoaiHinh == tkmd.MaLoaiHinh)
                            {
                                if (item.MaLoaiHinh.StartsWith("X") && item.TrangThai == 0)
                                {
                                    return "Tờ khai " + item.SoToKhai + " đã được phân bổ nên không hủy được. Hãy xóa phân bổ trước khi xóa tờ khai này.";
                                }
                                else if (item.MaLoaiHinh.StartsWith("N"))
                                {
                                    Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = Company.BLL.SXXK.PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(MahaiQuan, item.MaLoaiHinh, item.SoToKhai, item.NamDangKy);
                                    if (pbTKX != null)
                                    {
                                        return "Tờ khai nhập " + item.SoToKhai + " đã được phân bổ. Không thể hủy được. Hãy xóa phân bổ trước khi hủy tờ khai này.";
                                    }
                                }
                                item.Delete();
                            }
                        }

                    }
                    for (int j = 0; j < tkmd.listCTDK.Count; j++)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                    }

                    Company.KDT.SHARE.QuanLyChungTu.AnHanThue.DeleteBy_TKMD_ID(tkmd.ID);
                    Company.KDT.SHARE.QuanLyChungTu.SoContainer.DeleteBy_TKMD_ID(tkmd.ID);
                    Company.KDT.SHARE.QuanLyChungTu.DamBaoNghiaVuNT.DeleteBy_TKMD_ID(tkmd.ID);

                    tkmd.Delete();
                }

                return string.Empty;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return ex.Message;
            }

        }
    }

}
