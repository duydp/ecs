using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_CurrencyExchange : ICloneable
	{
		#region Properties.
		
		public string ErrorCode { set; get; }
		public string PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public string MakerClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string CurrencyCode { set; get; }
		public string CurrencyName { set; get; }
		public string GenerationManagementIndication { set; get; }
		public DateTime DateOfMaintenanceUpdated { set; get; }
		public DateTime EndDate { set; get; }
		public DateTime StartDate_1 { set; get; }
		public decimal ExchangeRate_1 { set; get; }
		public DateTime StartDate_2 { set; get; }
		public decimal ExchangeRate_2 { set; get; }
		public DateTime StartDate_3 { set; get; }
		public decimal ExchangeRate_3 { set; get; }
		public DateTime StartDate_4 { set; get; }
		public decimal ExchangeRate_4 { set; get; }
		public DateTime StartDate_5 { set; get; }
		public decimal ExchangeRate_5 { set; get; }
		public DateTime StartDate_6 { set; get; }
		public decimal ExchangeRate_6 { set; get; }
		public DateTime StartDate_7 { set; get; }
		public decimal ExchangeRate_7 { set; get; }
		public DateTime StartDate_8 { set; get; }
		public decimal ExchangeRate_8 { set; get; }
		public DateTime StartDate_9 { set; get; }
		public decimal ExchangeRate_9 { set; get; }
		public DateTime StartDate_10 { set; get; }
		public decimal ExchangeRate_10 { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_CurrencyExchange> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_CurrencyExchange> collection = new List<VNACC_Category_CurrencyExchange>();
			while (reader.Read())
			{
				VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();
				if (!reader.IsDBNull(reader.GetOrdinal("ErrorCode"))) entity.ErrorCode = reader.GetString(reader.GetOrdinal("ErrorCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetString(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("MakerClassification"))) entity.MakerClassification = reader.GetString(reader.GetOrdinal("MakerClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("CurrencyCode"))) entity.CurrencyCode = reader.GetString(reader.GetOrdinal("CurrencyCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CurrencyName"))) entity.CurrencyName = reader.GetString(reader.GetOrdinal("CurrencyName"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenerationManagementIndication"))) entity.GenerationManagementIndication = reader.GetString(reader.GetOrdinal("GenerationManagementIndication"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateOfMaintenanceUpdated"))) entity.DateOfMaintenanceUpdated = reader.GetDateTime(reader.GetOrdinal("DateOfMaintenanceUpdated"));
				if (!reader.IsDBNull(reader.GetOrdinal("EndDate"))) entity.EndDate = reader.GetDateTime(reader.GetOrdinal("EndDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_1"))) entity.StartDate_1 = reader.GetDateTime(reader.GetOrdinal("StartDate_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_1"))) entity.ExchangeRate_1 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_2"))) entity.StartDate_2 = reader.GetDateTime(reader.GetOrdinal("StartDate_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_2"))) entity.ExchangeRate_2 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_3"))) entity.StartDate_3 = reader.GetDateTime(reader.GetOrdinal("StartDate_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_3"))) entity.ExchangeRate_3 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_4"))) entity.StartDate_4 = reader.GetDateTime(reader.GetOrdinal("StartDate_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_4"))) entity.ExchangeRate_4 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_5"))) entity.StartDate_5 = reader.GetDateTime(reader.GetOrdinal("StartDate_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_5"))) entity.ExchangeRate_5 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_6"))) entity.StartDate_6 = reader.GetDateTime(reader.GetOrdinal("StartDate_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_6"))) entity.ExchangeRate_6 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_7"))) entity.StartDate_7 = reader.GetDateTime(reader.GetOrdinal("StartDate_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_7"))) entity.ExchangeRate_7 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_8"))) entity.StartDate_8 = reader.GetDateTime(reader.GetOrdinal("StartDate_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_8"))) entity.ExchangeRate_8 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_9"))) entity.StartDate_9 = reader.GetDateTime(reader.GetOrdinal("StartDate_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_9"))) entity.ExchangeRate_9 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("StartDate_10"))) entity.StartDate_10 = reader.GetDateTime(reader.GetOrdinal("StartDate_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExchangeRate_10"))) entity.ExchangeRate_10 = reader.GetDecimal(reader.GetOrdinal("ExchangeRate_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        public static List<VNACC_Category_CurrencyExchange> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<VNACC_Category_CurrencyExchange> collection = new List<VNACC_Category_CurrencyExchange>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();
                entity.ErrorCode = dataSet.Tables[0].Rows[i]["ErrorCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ErrorCode"].ToString() : String.Empty;
                entity.PGNumber = dataSet.Tables[0].Rows[i]["PGNumber"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["PGNumber"].ToString() : "A527";
                entity.TableID = dataSet.Tables[0].Rows[i]["TableID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["TableID"].ToString() : String.Empty;
                entity.ProcessClassification = dataSet.Tables[0].Rows[i]["ProcessClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ProcessClassification"].ToString() : String.Empty;
                entity.MakerClassification = dataSet.Tables[0].Rows[i]["MakerClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MakerClassification"].ToString() : String.Empty;
                entity.NumberOfKeyItems = dataSet.Tables[0].Rows[i]["NumberOfKeyItems"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NumberOfKeyItems"].ToString() : String.Empty;
                entity.CurrencyCode = dataSet.Tables[0].Rows[i]["CurrencyCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CurrencyCode"].ToString() : String.Empty;
                entity.CurrencyName = dataSet.Tables[0].Rows[i]["CurrencyName"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CurrencyName"].ToString() : String.Empty;
                entity.GenerationManagementIndication = dataSet.Tables[0].Rows[i]["GenerationManagementIndication"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["GenerationManagementIndication"].ToString() : String.Empty;
                entity.DateOfMaintenanceUpdated = dataSet.Tables[0].Rows[i]["DateOfMaintenanceUpdated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateOfMaintenanceUpdated"].ToString()) : DateTime.Now;
                entity.EndDate = dataSet.Tables[0].Rows[i]["EndDate"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["EndDate"].ToString()) : DateTime.Now;
                entity.StartDate_1 = dataSet.Tables[0].Rows[i]["StartDate_1"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_1"].ToString()) : DateTime.Now;
                entity.ExchangeRate_1 = dataSet.Tables[0].Rows[i]["ExchangeRate_1"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_1"].ToString()) : 0;
                entity.StartDate_2 = dataSet.Tables[0].Rows[i]["StartDate_2"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_2"].ToString()) : DateTime.Now;
                entity.ExchangeRate_2 = dataSet.Tables[0].Rows[i]["ExchangeRate_2"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_2"].ToString()) : 0;
                entity.StartDate_3 = dataSet.Tables[0].Rows[i]["StartDate_3"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_3"].ToString()) : DateTime.Now;
                entity.ExchangeRate_3 = dataSet.Tables[0].Rows[i]["ExchangeRate_3"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_3"].ToString()) : 0;
                entity.StartDate_4 = dataSet.Tables[0].Rows[i]["StartDate_4"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_4"].ToString()) : DateTime.Now;
                entity.ExchangeRate_4 = dataSet.Tables[0].Rows[i]["ExchangeRate_4"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_4"].ToString()) : 0;
                entity.StartDate_5 = dataSet.Tables[0].Rows[i]["StartDate_5"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_5"].ToString()) : DateTime.Now;
                entity.ExchangeRate_5 = dataSet.Tables[0].Rows[i]["ExchangeRate_5"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_5"].ToString()) : 0;
                entity.StartDate_6 = dataSet.Tables[0].Rows[i]["StartDate_6"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_6"].ToString()) : DateTime.Now;
                entity.ExchangeRate_6 = dataSet.Tables[0].Rows[i]["ExchangeRate_6"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_6"].ToString()) : 0;
                entity.StartDate_7 = dataSet.Tables[0].Rows[i]["StartDate_7"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_7"].ToString()) : DateTime.Now;
                entity.ExchangeRate_7 = dataSet.Tables[0].Rows[i]["ExchangeRate_7"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_7"].ToString()) : 0;
                entity.StartDate_8 = dataSet.Tables[0].Rows[i]["StartDate_8"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_8"].ToString()) : DateTime.Now;
                entity.ExchangeRate_8 = dataSet.Tables[0].Rows[i]["ExchangeRate_8"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_8"].ToString()) : 0;
                entity.StartDate_9 = dataSet.Tables[0].Rows[i]["StartDate_9"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_9"].ToString()) : DateTime.Now;
                entity.ExchangeRate_9 = dataSet.Tables[0].Rows[i]["ExchangeRate_9"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_9"].ToString()) : 0;
                entity.StartDate_10 = dataSet.Tables[0].Rows[i]["StartDate_10"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["StartDate_10"].ToString()) : DateTime.Now;
                entity.ExchangeRate_10 = dataSet.Tables[0].Rows[i]["ExchangeRate_10"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["ExchangeRate_10"].ToString()) : 0;
                entity.Notes = dataSet.Tables[0].Rows[i]["Notes"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["Notes"].ToString() : String.Empty;
                entity.InputMessageID = dataSet.Tables[0].Rows[i]["InputMessageID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["InputMessageID"].ToString() : String.Empty;
                entity.MessageTag = dataSet.Tables[0].Rows[i]["MessageTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MessageTag"].ToString() : String.Empty;
                entity.IndexTag = dataSet.Tables[0].Rows[i]["IndexTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IndexTag"].ToString() : String.Empty;
                collection.Add(entity);
            }
            return collection;
        }
		public static bool Find(List<VNACC_Category_CurrencyExchange> collection, string currencyCode)
        {
            foreach (VNACC_Category_CurrencyExchange item in collection)
            {
                if (item.CurrencyCode == currencyCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_CurrencyExchange VALUES(@ErrorCode, @PGNumber, @TableID, @ProcessClassification, @MakerClassification, @NumberOfKeyItems, @CurrencyCode, @CurrencyName, @GenerationManagementIndication, @DateOfMaintenanceUpdated, @EndDate, @StartDate_1, @ExchangeRate_1, @StartDate_2, @ExchangeRate_2, @StartDate_3, @ExchangeRate_3, @StartDate_4, @ExchangeRate_4, @StartDate_5, @ExchangeRate_5, @StartDate_6, @ExchangeRate_6, @StartDate_7, @ExchangeRate_7, @StartDate_8, @ExchangeRate_8, @StartDate_9, @ExchangeRate_9, @StartDate_10, @ExchangeRate_10, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_CurrencyExchange SET ErrorCode = @ErrorCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, MakerClassification = @MakerClassification, NumberOfKeyItems = @NumberOfKeyItems, CurrencyName = @CurrencyName, GenerationManagementIndication = @GenerationManagementIndication, DateOfMaintenanceUpdated = @DateOfMaintenanceUpdated, EndDate = @EndDate, StartDate_1 = @StartDate_1, ExchangeRate_1 = @ExchangeRate_1, StartDate_2 = @StartDate_2, ExchangeRate_2 = @ExchangeRate_2, StartDate_3 = @StartDate_3, ExchangeRate_3 = @ExchangeRate_3, StartDate_4 = @StartDate_4, ExchangeRate_4 = @ExchangeRate_4, StartDate_5 = @StartDate_5, ExchangeRate_5 = @ExchangeRate_5, StartDate_6 = @StartDate_6, ExchangeRate_6 = @ExchangeRate_6, StartDate_7 = @StartDate_7, ExchangeRate_7 = @ExchangeRate_7, StartDate_8 = @StartDate_8, ExchangeRate_8 = @ExchangeRate_8, StartDate_9 = @StartDate_9, ExchangeRate_9 = @ExchangeRate_9, StartDate_10 = @StartDate_10, ExchangeRate_10 = @ExchangeRate_10, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE CurrencyCode = @CurrencyCode";
            string delete = "DELETE FROM t_VNACC_Category_CurrencyExchange WHERE CurrencyCode = @CurrencyCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ErrorCode", SqlDbType.Char, "ErrorCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Char, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.Char, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.Char, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MakerClassification", SqlDbType.Char, "MakerClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.Char, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CurrencyName", SqlDbType.NVarChar, "CurrencyName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GenerationManagementIndication", SqlDbType.Char, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EndDate", SqlDbType.DateTime, "EndDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_1", SqlDbType.DateTime, "StartDate_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_1", SqlDbType.Decimal, "ExchangeRate_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_2", SqlDbType.DateTime, "StartDate_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_2", SqlDbType.Decimal, "ExchangeRate_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_3", SqlDbType.DateTime, "StartDate_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_3", SqlDbType.Decimal, "ExchangeRate_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_4", SqlDbType.DateTime, "StartDate_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_4", SqlDbType.Decimal, "ExchangeRate_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_5", SqlDbType.DateTime, "StartDate_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_5", SqlDbType.Decimal, "ExchangeRate_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_6", SqlDbType.DateTime, "StartDate_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_6", SqlDbType.Decimal, "ExchangeRate_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_7", SqlDbType.DateTime, "StartDate_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_7", SqlDbType.Decimal, "ExchangeRate_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_8", SqlDbType.DateTime, "StartDate_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_8", SqlDbType.Decimal, "ExchangeRate_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_9", SqlDbType.DateTime, "StartDate_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_9", SqlDbType.Decimal, "ExchangeRate_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_10", SqlDbType.DateTime, "StartDate_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_10", SqlDbType.Decimal, "ExchangeRate_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ErrorCode", SqlDbType.Char, "ErrorCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Char, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.Char, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.Char, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MakerClassification", SqlDbType.Char, "MakerClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.Char, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CurrencyName", SqlDbType.NVarChar, "CurrencyName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GenerationManagementIndication", SqlDbType.Char, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EndDate", SqlDbType.DateTime, "EndDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_1", SqlDbType.DateTime, "StartDate_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_1", SqlDbType.Decimal, "ExchangeRate_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_2", SqlDbType.DateTime, "StartDate_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_2", SqlDbType.Decimal, "ExchangeRate_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_3", SqlDbType.DateTime, "StartDate_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_3", SqlDbType.Decimal, "ExchangeRate_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_4", SqlDbType.DateTime, "StartDate_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_4", SqlDbType.Decimal, "ExchangeRate_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_5", SqlDbType.DateTime, "StartDate_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_5", SqlDbType.Decimal, "ExchangeRate_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_6", SqlDbType.DateTime, "StartDate_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_6", SqlDbType.Decimal, "ExchangeRate_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_7", SqlDbType.DateTime, "StartDate_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_7", SqlDbType.Decimal, "ExchangeRate_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_8", SqlDbType.DateTime, "StartDate_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_8", SqlDbType.Decimal, "ExchangeRate_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_9", SqlDbType.DateTime, "StartDate_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_9", SqlDbType.Decimal, "ExchangeRate_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_10", SqlDbType.DateTime, "StartDate_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_10", SqlDbType.Decimal, "ExchangeRate_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_CurrencyExchange VALUES(@ErrorCode, @PGNumber, @TableID, @ProcessClassification, @MakerClassification, @NumberOfKeyItems, @CurrencyCode, @CurrencyName, @GenerationManagementIndication, @DateOfMaintenanceUpdated, @EndDate, @StartDate_1, @ExchangeRate_1, @StartDate_2, @ExchangeRate_2, @StartDate_3, @ExchangeRate_3, @StartDate_4, @ExchangeRate_4, @StartDate_5, @ExchangeRate_5, @StartDate_6, @ExchangeRate_6, @StartDate_7, @ExchangeRate_7, @StartDate_8, @ExchangeRate_8, @StartDate_9, @ExchangeRate_9, @StartDate_10, @ExchangeRate_10, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_CurrencyExchange SET ErrorCode = @ErrorCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, MakerClassification = @MakerClassification, NumberOfKeyItems = @NumberOfKeyItems, CurrencyName = @CurrencyName, GenerationManagementIndication = @GenerationManagementIndication, DateOfMaintenanceUpdated = @DateOfMaintenanceUpdated, EndDate = @EndDate, StartDate_1 = @StartDate_1, ExchangeRate_1 = @ExchangeRate_1, StartDate_2 = @StartDate_2, ExchangeRate_2 = @ExchangeRate_2, StartDate_3 = @StartDate_3, ExchangeRate_3 = @ExchangeRate_3, StartDate_4 = @StartDate_4, ExchangeRate_4 = @ExchangeRate_4, StartDate_5 = @StartDate_5, ExchangeRate_5 = @ExchangeRate_5, StartDate_6 = @StartDate_6, ExchangeRate_6 = @ExchangeRate_6, StartDate_7 = @StartDate_7, ExchangeRate_7 = @ExchangeRate_7, StartDate_8 = @StartDate_8, ExchangeRate_8 = @ExchangeRate_8, StartDate_9 = @StartDate_9, ExchangeRate_9 = @ExchangeRate_9, StartDate_10 = @StartDate_10, ExchangeRate_10 = @ExchangeRate_10, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE CurrencyCode = @CurrencyCode";
            string delete = "DELETE FROM t_VNACC_Category_CurrencyExchange WHERE CurrencyCode = @CurrencyCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ErrorCode", SqlDbType.Char, "ErrorCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Char, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.Char, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.Char, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MakerClassification", SqlDbType.Char, "MakerClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.Char, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CurrencyName", SqlDbType.NVarChar, "CurrencyName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GenerationManagementIndication", SqlDbType.Char, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EndDate", SqlDbType.DateTime, "EndDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_1", SqlDbType.DateTime, "StartDate_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_1", SqlDbType.Decimal, "ExchangeRate_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_2", SqlDbType.DateTime, "StartDate_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_2", SqlDbType.Decimal, "ExchangeRate_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_3", SqlDbType.DateTime, "StartDate_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_3", SqlDbType.Decimal, "ExchangeRate_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_4", SqlDbType.DateTime, "StartDate_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_4", SqlDbType.Decimal, "ExchangeRate_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_5", SqlDbType.DateTime, "StartDate_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_5", SqlDbType.Decimal, "ExchangeRate_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_6", SqlDbType.DateTime, "StartDate_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_6", SqlDbType.Decimal, "ExchangeRate_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_7", SqlDbType.DateTime, "StartDate_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_7", SqlDbType.Decimal, "ExchangeRate_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_8", SqlDbType.DateTime, "StartDate_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_8", SqlDbType.Decimal, "ExchangeRate_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_9", SqlDbType.DateTime, "StartDate_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_9", SqlDbType.Decimal, "ExchangeRate_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StartDate_10", SqlDbType.DateTime, "StartDate_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExchangeRate_10", SqlDbType.Decimal, "ExchangeRate_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ErrorCode", SqlDbType.Char, "ErrorCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Char, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.Char, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.Char, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MakerClassification", SqlDbType.Char, "MakerClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.Char, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CurrencyName", SqlDbType.NVarChar, "CurrencyName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GenerationManagementIndication", SqlDbType.Char, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EndDate", SqlDbType.DateTime, "EndDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_1", SqlDbType.DateTime, "StartDate_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_1", SqlDbType.Decimal, "ExchangeRate_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_2", SqlDbType.DateTime, "StartDate_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_2", SqlDbType.Decimal, "ExchangeRate_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_3", SqlDbType.DateTime, "StartDate_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_3", SqlDbType.Decimal, "ExchangeRate_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_4", SqlDbType.DateTime, "StartDate_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_4", SqlDbType.Decimal, "ExchangeRate_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_5", SqlDbType.DateTime, "StartDate_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_5", SqlDbType.Decimal, "ExchangeRate_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_6", SqlDbType.DateTime, "StartDate_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_6", SqlDbType.Decimal, "ExchangeRate_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_7", SqlDbType.DateTime, "StartDate_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_7", SqlDbType.Decimal, "ExchangeRate_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_8", SqlDbType.DateTime, "StartDate_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_8", SqlDbType.Decimal, "ExchangeRate_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_9", SqlDbType.DateTime, "StartDate_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_9", SqlDbType.Decimal, "ExchangeRate_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StartDate_10", SqlDbType.DateTime, "StartDate_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExchangeRate_10", SqlDbType.Decimal, "ExchangeRate_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@CurrencyCode", SqlDbType.Char, "CurrencyCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_CurrencyExchange Load(string currencyCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CurrencyCode", SqlDbType.Char, currencyCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_CurrencyExchange> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_CurrencyExchange> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_CurrencyExchange> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_CurrencyExchange(string errorCode, string pGNumber, string tableID, string processClassification, string makerClassification, string numberOfKeyItems, string currencyName, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, DateTime endDate, DateTime startDate_1, decimal exchangeRate_1, DateTime startDate_2, decimal exchangeRate_2, DateTime startDate_3, decimal exchangeRate_3, DateTime startDate_4, decimal exchangeRate_4, DateTime startDate_5, decimal exchangeRate_5, DateTime startDate_6, decimal exchangeRate_6, DateTime startDate_7, decimal exchangeRate_7, DateTime startDate_8, decimal exchangeRate_8, DateTime startDate_9, decimal exchangeRate_9, DateTime startDate_10, decimal exchangeRate_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();	
			entity.ErrorCode = errorCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.MakerClassification = makerClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.CurrencyName = currencyName;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.EndDate = endDate;
			entity.StartDate_1 = startDate_1;
			entity.ExchangeRate_1 = exchangeRate_1;
			entity.StartDate_2 = startDate_2;
			entity.ExchangeRate_2 = exchangeRate_2;
			entity.StartDate_3 = startDate_3;
			entity.ExchangeRate_3 = exchangeRate_3;
			entity.StartDate_4 = startDate_4;
			entity.ExchangeRate_4 = exchangeRate_4;
			entity.StartDate_5 = startDate_5;
			entity.ExchangeRate_5 = exchangeRate_5;
			entity.StartDate_6 = startDate_6;
			entity.ExchangeRate_6 = exchangeRate_6;
			entity.StartDate_7 = startDate_7;
			entity.ExchangeRate_7 = exchangeRate_7;
			entity.StartDate_8 = startDate_8;
			entity.ExchangeRate_8 = exchangeRate_8;
			entity.StartDate_9 = startDate_9;
			entity.ExchangeRate_9 = exchangeRate_9;
			entity.StartDate_10 = startDate_10;
			entity.ExchangeRate_10 = exchangeRate_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ErrorCode", SqlDbType.Char, ErrorCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Char, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.Char, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.Char, ProcessClassification);
			db.AddInParameter(dbCommand, "@MakerClassification", SqlDbType.Char, MakerClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.Char, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@CurrencyCode", SqlDbType.Char, CurrencyCode);
			db.AddInParameter(dbCommand, "@CurrencyName", SqlDbType.NVarChar, CurrencyName);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.Char, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@EndDate", SqlDbType.DateTime, EndDate.Year <= 1753 ? DBNull.Value : (object) EndDate);
			db.AddInParameter(dbCommand, "@StartDate_1", SqlDbType.DateTime, StartDate_1.Year <= 1753 ? DBNull.Value : (object) StartDate_1);
			db.AddInParameter(dbCommand, "@ExchangeRate_1", SqlDbType.Decimal, ExchangeRate_1);
			db.AddInParameter(dbCommand, "@StartDate_2", SqlDbType.DateTime, StartDate_2.Year <= 1753 ? DBNull.Value : (object) StartDate_2);
			db.AddInParameter(dbCommand, "@ExchangeRate_2", SqlDbType.Decimal, ExchangeRate_2);
			db.AddInParameter(dbCommand, "@StartDate_3", SqlDbType.DateTime, StartDate_3.Year <= 1753 ? DBNull.Value : (object) StartDate_3);
			db.AddInParameter(dbCommand, "@ExchangeRate_3", SqlDbType.Decimal, ExchangeRate_3);
			db.AddInParameter(dbCommand, "@StartDate_4", SqlDbType.DateTime, StartDate_4.Year <= 1753 ? DBNull.Value : (object) StartDate_4);
			db.AddInParameter(dbCommand, "@ExchangeRate_4", SqlDbType.Decimal, ExchangeRate_4);
			db.AddInParameter(dbCommand, "@StartDate_5", SqlDbType.DateTime, StartDate_5.Year <= 1753 ? DBNull.Value : (object) StartDate_5);
			db.AddInParameter(dbCommand, "@ExchangeRate_5", SqlDbType.Decimal, ExchangeRate_5);
			db.AddInParameter(dbCommand, "@StartDate_6", SqlDbType.DateTime, StartDate_6.Year <= 1753 ? DBNull.Value : (object) StartDate_6);
			db.AddInParameter(dbCommand, "@ExchangeRate_6", SqlDbType.Decimal, ExchangeRate_6);
			db.AddInParameter(dbCommand, "@StartDate_7", SqlDbType.DateTime, StartDate_7.Year <= 1753 ? DBNull.Value : (object) StartDate_7);
			db.AddInParameter(dbCommand, "@ExchangeRate_7", SqlDbType.Decimal, ExchangeRate_7);
			db.AddInParameter(dbCommand, "@StartDate_8", SqlDbType.DateTime, StartDate_8.Year <= 1753 ? DBNull.Value : (object) StartDate_8);
			db.AddInParameter(dbCommand, "@ExchangeRate_8", SqlDbType.Decimal, ExchangeRate_8);
			db.AddInParameter(dbCommand, "@StartDate_9", SqlDbType.DateTime, StartDate_9.Year <= 1753 ? DBNull.Value : (object) StartDate_9);
			db.AddInParameter(dbCommand, "@ExchangeRate_9", SqlDbType.Decimal, ExchangeRate_9);
			db.AddInParameter(dbCommand, "@StartDate_10", SqlDbType.DateTime, StartDate_10.Year <= 1753 ? DBNull.Value : (object) StartDate_10);
			db.AddInParameter(dbCommand, "@ExchangeRate_10", SqlDbType.Decimal, ExchangeRate_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_CurrencyExchange> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_CurrencyExchange item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_CurrencyExchange(string errorCode, string pGNumber, string tableID, string processClassification, string makerClassification, string numberOfKeyItems, string currencyCode, string currencyName, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, DateTime endDate, DateTime startDate_1, decimal exchangeRate_1, DateTime startDate_2, decimal exchangeRate_2, DateTime startDate_3, decimal exchangeRate_3, DateTime startDate_4, decimal exchangeRate_4, DateTime startDate_5, decimal exchangeRate_5, DateTime startDate_6, decimal exchangeRate_6, DateTime startDate_7, decimal exchangeRate_7, DateTime startDate_8, decimal exchangeRate_8, DateTime startDate_9, decimal exchangeRate_9, DateTime startDate_10, decimal exchangeRate_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();			
			entity.ErrorCode = errorCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.MakerClassification = makerClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.CurrencyCode = currencyCode;
			entity.CurrencyName = currencyName;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.EndDate = endDate;
			entity.StartDate_1 = startDate_1;
			entity.ExchangeRate_1 = exchangeRate_1;
			entity.StartDate_2 = startDate_2;
			entity.ExchangeRate_2 = exchangeRate_2;
			entity.StartDate_3 = startDate_3;
			entity.ExchangeRate_3 = exchangeRate_3;
			entity.StartDate_4 = startDate_4;
			entity.ExchangeRate_4 = exchangeRate_4;
			entity.StartDate_5 = startDate_5;
			entity.ExchangeRate_5 = exchangeRate_5;
			entity.StartDate_6 = startDate_6;
			entity.ExchangeRate_6 = exchangeRate_6;
			entity.StartDate_7 = startDate_7;
			entity.ExchangeRate_7 = exchangeRate_7;
			entity.StartDate_8 = startDate_8;
			entity.ExchangeRate_8 = exchangeRate_8;
			entity.StartDate_9 = startDate_9;
			entity.ExchangeRate_9 = exchangeRate_9;
			entity.StartDate_10 = startDate_10;
			entity.ExchangeRate_10 = exchangeRate_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_CurrencyExchange_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ErrorCode", SqlDbType.Char, ErrorCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Char, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.Char, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.Char, ProcessClassification);
			db.AddInParameter(dbCommand, "@MakerClassification", SqlDbType.Char, MakerClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.Char, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@CurrencyCode", SqlDbType.Char, CurrencyCode);
			db.AddInParameter(dbCommand, "@CurrencyName", SqlDbType.NVarChar, CurrencyName);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.Char, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@EndDate", SqlDbType.DateTime, EndDate.Year <= 1753 ? DBNull.Value : (object) EndDate);
			db.AddInParameter(dbCommand, "@StartDate_1", SqlDbType.DateTime, StartDate_1.Year <= 1753 ? DBNull.Value : (object) StartDate_1);
			db.AddInParameter(dbCommand, "@ExchangeRate_1", SqlDbType.Decimal, ExchangeRate_1);
			db.AddInParameter(dbCommand, "@StartDate_2", SqlDbType.DateTime, StartDate_2.Year <= 1753 ? DBNull.Value : (object) StartDate_2);
			db.AddInParameter(dbCommand, "@ExchangeRate_2", SqlDbType.Decimal, ExchangeRate_2);
			db.AddInParameter(dbCommand, "@StartDate_3", SqlDbType.DateTime, StartDate_3.Year <= 1753 ? DBNull.Value : (object) StartDate_3);
			db.AddInParameter(dbCommand, "@ExchangeRate_3", SqlDbType.Decimal, ExchangeRate_3);
			db.AddInParameter(dbCommand, "@StartDate_4", SqlDbType.DateTime, StartDate_4.Year <= 1753 ? DBNull.Value : (object) StartDate_4);
			db.AddInParameter(dbCommand, "@ExchangeRate_4", SqlDbType.Decimal, ExchangeRate_4);
			db.AddInParameter(dbCommand, "@StartDate_5", SqlDbType.DateTime, StartDate_5.Year <= 1753 ? DBNull.Value : (object) StartDate_5);
			db.AddInParameter(dbCommand, "@ExchangeRate_5", SqlDbType.Decimal, ExchangeRate_5);
			db.AddInParameter(dbCommand, "@StartDate_6", SqlDbType.DateTime, StartDate_6.Year <= 1753 ? DBNull.Value : (object) StartDate_6);
			db.AddInParameter(dbCommand, "@ExchangeRate_6", SqlDbType.Decimal, ExchangeRate_6);
			db.AddInParameter(dbCommand, "@StartDate_7", SqlDbType.DateTime, StartDate_7.Year <= 1753 ? DBNull.Value : (object) StartDate_7);
			db.AddInParameter(dbCommand, "@ExchangeRate_7", SqlDbType.Decimal, ExchangeRate_7);
			db.AddInParameter(dbCommand, "@StartDate_8", SqlDbType.DateTime, StartDate_8.Year <= 1753 ? DBNull.Value : (object) StartDate_8);
			db.AddInParameter(dbCommand, "@ExchangeRate_8", SqlDbType.Decimal, ExchangeRate_8);
			db.AddInParameter(dbCommand, "@StartDate_9", SqlDbType.DateTime, StartDate_9.Year <= 1753 ? DBNull.Value : (object) StartDate_9);
			db.AddInParameter(dbCommand, "@ExchangeRate_9", SqlDbType.Decimal, ExchangeRate_9);
			db.AddInParameter(dbCommand, "@StartDate_10", SqlDbType.DateTime, StartDate_10.Year <= 1753 ? DBNull.Value : (object) StartDate_10);
			db.AddInParameter(dbCommand, "@ExchangeRate_10", SqlDbType.Decimal, ExchangeRate_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_CurrencyExchange> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_CurrencyExchange item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_CurrencyExchange(string errorCode, string pGNumber, string tableID, string processClassification, string makerClassification, string numberOfKeyItems, string currencyCode, string currencyName, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, DateTime endDate, DateTime startDate_1, decimal exchangeRate_1, DateTime startDate_2, decimal exchangeRate_2, DateTime startDate_3, decimal exchangeRate_3, DateTime startDate_4, decimal exchangeRate_4, DateTime startDate_5, decimal exchangeRate_5, DateTime startDate_6, decimal exchangeRate_6, DateTime startDate_7, decimal exchangeRate_7, DateTime startDate_8, decimal exchangeRate_8, DateTime startDate_9, decimal exchangeRate_9, DateTime startDate_10, decimal exchangeRate_10, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();			
			entity.ErrorCode = errorCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.MakerClassification = makerClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.CurrencyCode = currencyCode;
			entity.CurrencyName = currencyName;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.EndDate = endDate;
			entity.StartDate_1 = startDate_1;
			entity.ExchangeRate_1 = exchangeRate_1;
			entity.StartDate_2 = startDate_2;
			entity.ExchangeRate_2 = exchangeRate_2;
			entity.StartDate_3 = startDate_3;
			entity.ExchangeRate_3 = exchangeRate_3;
			entity.StartDate_4 = startDate_4;
			entity.ExchangeRate_4 = exchangeRate_4;
			entity.StartDate_5 = startDate_5;
			entity.ExchangeRate_5 = exchangeRate_5;
			entity.StartDate_6 = startDate_6;
			entity.ExchangeRate_6 = exchangeRate_6;
			entity.StartDate_7 = startDate_7;
			entity.ExchangeRate_7 = exchangeRate_7;
			entity.StartDate_8 = startDate_8;
			entity.ExchangeRate_8 = exchangeRate_8;
			entity.StartDate_9 = startDate_9;
			entity.ExchangeRate_9 = exchangeRate_9;
			entity.StartDate_10 = startDate_10;
			entity.ExchangeRate_10 = exchangeRate_10;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ErrorCode", SqlDbType.Char, ErrorCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Char, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.Char, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.Char, ProcessClassification);
			db.AddInParameter(dbCommand, "@MakerClassification", SqlDbType.Char, MakerClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.Char, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@CurrencyCode", SqlDbType.Char, CurrencyCode);
			db.AddInParameter(dbCommand, "@CurrencyName", SqlDbType.NVarChar, CurrencyName);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.Char, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@EndDate", SqlDbType.DateTime, EndDate.Year <= 1753 ? DBNull.Value : (object) EndDate);
			db.AddInParameter(dbCommand, "@StartDate_1", SqlDbType.DateTime, StartDate_1.Year <= 1753 ? DBNull.Value : (object) StartDate_1);
			db.AddInParameter(dbCommand, "@ExchangeRate_1", SqlDbType.Decimal, ExchangeRate_1);
			db.AddInParameter(dbCommand, "@StartDate_2", SqlDbType.DateTime, StartDate_2.Year <= 1753 ? DBNull.Value : (object) StartDate_2);
			db.AddInParameter(dbCommand, "@ExchangeRate_2", SqlDbType.Decimal, ExchangeRate_2);
			db.AddInParameter(dbCommand, "@StartDate_3", SqlDbType.DateTime, StartDate_3.Year <= 1753 ? DBNull.Value : (object) StartDate_3);
			db.AddInParameter(dbCommand, "@ExchangeRate_3", SqlDbType.Decimal, ExchangeRate_3);
			db.AddInParameter(dbCommand, "@StartDate_4", SqlDbType.DateTime, StartDate_4.Year <= 1753 ? DBNull.Value : (object) StartDate_4);
			db.AddInParameter(dbCommand, "@ExchangeRate_4", SqlDbType.Decimal, ExchangeRate_4);
			db.AddInParameter(dbCommand, "@StartDate_5", SqlDbType.DateTime, StartDate_5.Year <= 1753 ? DBNull.Value : (object) StartDate_5);
			db.AddInParameter(dbCommand, "@ExchangeRate_5", SqlDbType.Decimal, ExchangeRate_5);
			db.AddInParameter(dbCommand, "@StartDate_6", SqlDbType.DateTime, StartDate_6.Year <= 1753 ? DBNull.Value : (object) StartDate_6);
			db.AddInParameter(dbCommand, "@ExchangeRate_6", SqlDbType.Decimal, ExchangeRate_6);
			db.AddInParameter(dbCommand, "@StartDate_7", SqlDbType.DateTime, StartDate_7.Year <= 1753 ? DBNull.Value : (object) StartDate_7);
			db.AddInParameter(dbCommand, "@ExchangeRate_7", SqlDbType.Decimal, ExchangeRate_7);
			db.AddInParameter(dbCommand, "@StartDate_8", SqlDbType.DateTime, StartDate_8.Year <= 1753 ? DBNull.Value : (object) StartDate_8);
			db.AddInParameter(dbCommand, "@ExchangeRate_8", SqlDbType.Decimal, ExchangeRate_8);
			db.AddInParameter(dbCommand, "@StartDate_9", SqlDbType.DateTime, StartDate_9.Year <= 1753 ? DBNull.Value : (object) StartDate_9);
			db.AddInParameter(dbCommand, "@ExchangeRate_9", SqlDbType.Decimal, ExchangeRate_9);
			db.AddInParameter(dbCommand, "@StartDate_10", SqlDbType.DateTime, StartDate_10.Year <= 1753 ? DBNull.Value : (object) StartDate_10);
			db.AddInParameter(dbCommand, "@ExchangeRate_10", SqlDbType.Decimal, ExchangeRate_10);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_CurrencyExchange> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_CurrencyExchange item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_CurrencyExchange(string currencyCode)
		{
			VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();
			entity.CurrencyCode = currencyCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CurrencyCode", SqlDbType.Char, CurrencyCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_CurrencyExchange> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_CurrencyExchange item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}