﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class ChuyenCuaKhauFormGCCT : BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiChuyenTiep TKCT;
        public DeNghiChuyenCuaKhau deNghiChuyen = new DeNghiChuyenCuaKhau();
        public List<DeNghiChuyenCuaKhau> ListDeNghiChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
        public ChuyenCuaKhauFormGCCT()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            deNghiChuyen.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
            if (isKhaiBoSung)
                deNghiChuyen.LoaiKB = 1;
            else
                deNghiChuyen.LoaiKB = 0;

            deNghiChuyen.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            deNghiChuyen.NgayVanDon = ccNgayVanDon.Value;
            deNghiChuyen.SoVanDon = txtSoVanDon.Text;
            deNghiChuyen.ThoiGianDen = ccThoiHanDen.Value;
            deNghiChuyen.ThongTinKhac = txtThongTinKhac.Text.Trim();
            deNghiChuyen.TuyenDuong = txtTuyenDuong.Text;
            deNghiChuyen.ID_TK = TKCT.ID;
            deNghiChuyen.LoaiTK= "TKCT";
            bool isUpdate = false;
            try
            {
                if (deNghiChuyen.ID == 0)
                    deNghiChuyen.Insert();
                else
                {
                    deNghiChuyen.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKCT.DeNghiChuyenCuaKhau.Add(deNghiChuyen);
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void CoForm_Load(object sender, EventArgs e)
        {
            if (deNghiChuyen != null)
                if(deNghiChuyen.SoVanDon != null)
            {
                txtSoVanDon.Text = deNghiChuyen.SoVanDon;
                ccNgayVanDon.Value = deNghiChuyen.NgayVanDon;
                ccThoiHanDen.Value = deNghiChuyen.ThoiGianDen;
                txtSoTiepNhan.Text = deNghiChuyen.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = deNghiChuyen.NgayTiepNhan;
            }
            else
            {
                deNghiChuyen = new DeNghiChuyenCuaKhau { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
                if (TKCT.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    txtSoVanDon.Text = "";
                    ccNgayVanDon.Enabled = true;
                    ccNgayVanDon.Text = DateTime.Today.ToShortDateString();
                    ccThoiHanDen.Enabled = true;
                    ccThoiHanDen.Text = DateTime.Today.ToShortDateString();
                }
                else
                {
                   //txtSoVanDon.Text = TKCT.DeNghiChuyenCuaKhau[0].SoVanDon;
                   ccNgayVanDon.Text = DateTime.Today.ToShortDateString();
                   ccThoiHanDen.Text = DateTime.Today.ToShortDateString();
                }
            }

            txtDiaDiemKiemTra.Text = deNghiChuyen.DiaDiemKiemTra;
            txtTuyenDuong.Text = deNghiChuyen.TuyenDuong;
            txtThongTinKhac.Text = deNghiChuyen.ThongTinKhac;
            SetButtonStateCHUYENCUAKHAU(TKCT, isKhaiBoSung, deNghiChuyen);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID > 0)
            {
                deNghiChuyen.Delete();
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if ((deNghiChuyen.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (deNghiChuyen.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    deNghiChuyen.GuidStr = Guid.NewGuid().ToString();
                else if (deNghiChuyen.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }
                
                ObjectSend msgSend = SingleMessage.BoSungChungTuGCCT(TKCT, deNghiChuyen,GlobalSettings.TEN_DON_VI);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend)
                {
                    deNghiChuyen.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    sendMessageForm.Message.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCK);
                    deNghiChuyen.Update();
                    SetButtonStateCHUYENCUAKHAU(TKCT, isKhaiBoSung, deNghiChuyen);
                    Company.GC.BLL.KDT.SXXK.MsgSend XmlSend = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    btnLayPhanHoi_Click(null, null);
                    
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                deNghiChuyen.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
               
            }
            


        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            try
            {

                ObjectSend msgSend = SingleMessage.FeedBackGCCT(TKCT,deNghiChuyen.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                sendMessageForm.DoSend(msgSend);
            
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUYENCUAKHAU.
        /// </summary>
        /// <param name="TKCT"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUYENCUAKHAU(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau chuyenCuaKhau)
        {
            if (chuyenCuaKhau == null)
                return false;

            switch (chuyenCuaKhau.TrangThai)
            {
                case -1:
                    lbTrangThai.Text = "Chưa Khai Báo";
                    break;
                case -3:
                    lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                    break;
                case 0:
                    lbTrangThai.Text = "Chờ Duyệt";
                    break;
                case 1:
                    lbTrangThai.Text = "Đã Duyệt";
                    break;
                case 2:
                    lbTrangThai.Text = "Hải quan không phê duyệt";
                    break;
            }
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (TKCT.SoToKhai > 0 && TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (deNghiChuyen.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     deNghiChuyen.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = deNghiChuyen.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   deNghiChuyen.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   deNghiChuyen.TrangThai == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = chuyenCuaKhau.SoTiepNhan > 0 ? chuyenCuaKhau.SoTiepNhan.ToString() : string.Empty;
            if (chuyenCuaKhau.NgayTiepNhan.Year > 1900)
                ccNgayTiepNhan.Value = chuyenCuaKhau.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.GuidStr != null && deNghiChuyen.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                f.ItemID = deNghiChuyen.ID;
                f.ShowDialog();
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                btnKhaiBao.Enabled = true;
                                btnLayPhanHoi.Enabled = false;
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                            {
                                GlobalSettings.IsRemember = false;
                            }
                            noidung = noidung.Replace(string.Format("Message [{0}]", deNghiChuyen.GuidStr), string.Empty);
                            this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                deNghiChuyen.SoTiepNhan = long.Parse(vals[0].Trim());
                                deNghiChuyen.NamTiepNhan = int.Parse(vals[1].Trim());
                                deNghiChuyen.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                this.ShowMessageTQDT("Được cấp số tiếp nhận.\n" + deNghiChuyen.SoTiepNhan.ToString() + "\n" + deNghiChuyen.NgayTiepNhan.ToString(), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong, noidung);
                                deNghiChuyen.TrangThai = TrangThaiXuLy.DA_DUYET;
                                this.ShowMessageTQDT("Chứng từ bổ sung đã được duyệt", false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(deNghiChuyen.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        deNghiChuyen.Update();
                        SetButtonStateCHUYENCUAKHAU(TKCT, isKhaiBoSung, deNghiChuyen);
                    }

                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}

