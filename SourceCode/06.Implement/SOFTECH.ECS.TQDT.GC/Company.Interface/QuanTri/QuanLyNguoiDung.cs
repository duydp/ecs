﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Janus.Windows.GridEX;
using Company.GC.BLL;

namespace Company.Interface.QuanTri
{
    public partial class QuanLyNguoiDung : BaseForm
    {
        UserCollection collection = new UserCollection();
        User user = new User();
        public QuanLyNguoiDung()
        {
            InitializeComponent();
        }

        private void QuanLyNguoiDung_Load(object sender, EventArgs e)
        {
            collection = user.SelectCollectionAll();
            dgList.DataSource = collection;            
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.CreateUser)))
            {
                TaoMoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Permission)))
            {
                PhanQuyen.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }  
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.DeleteUser)))
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "TaoMoi")
            {
                NguoiDungEditForm f = new NguoiDungEditForm();
                f.ShowDialog();
                collection = user.SelectCollectionAll();
                dgList.DataSource = collection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            else
            {
                QuanLyPhanQuyen f = new QuanLyPhanQuyen();
                f.ShowDialog();
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            User user = (User)e.Row.DataRow;
            NguoiDungEditForm f = new NguoiDungEditForm();
            f.user = user;
            f.ShowDialog();
            collection = user.SelectCollectionAll();
            dgList.DataSource = collection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa người dùng này không ?","Do you want to delete this user"), true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        User u = (User)i.GetRow().DataRow;
                        if (u.isAdmin != true)
                            u.Delete();
                        else
                        {
                            string msg = setText("Đây là người dùng mặc định của hệ thống. Không xóa người dùng này được.", "This is default user,so can not delete");
                            ShowMessage(msg, false);
                            e.Cancel = true;
                        }
                    }
                }
            }
            else
                e.Cancel = true;
        }
    }
}

