﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.SXXK;

namespace DongBo.Controls
{
    public partial class ucInsertSXXK : ucDesign
    {
        frmMain frmParent = null;

        delegate void SetTextCallback(string text);

        private void setText(string str)
        {
            if (lblCaption.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { str });
            }
            else
            {
                lblContent.Text = str;
            }
        }

        delegate void SetProgessBarCallback(int percent);
        private void setProgessBar(int percent)
        {
            if (pBar.InvokeRequired)
            {
                SetProgessBarCallback d = new SetProgessBarCallback(setProgessBar);
                this.Invoke(d, new object[] { percent });
            }
            else
            {
                pBar.Value = percent;

                pBar.Text = percent + " %";
            }
        }

        public ucInsertSXXK()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void ucInsertSXXK_Load(object sender, EventArgs e)
        {
            frmParent.btnPrev.Enabled = false;
            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (GlobalSettings.dsDNNew[0] != null && GlobalSettings.dsDNNew[0].Tables[0].Rows.Count > 0)
                {
                    this.setText("Đang cập nhật danh sách Nguyên Phụ Liệu vào dữ liệu Doanh Nghiệp");

                    if (GlobalSettings.PhienBan == "KTX")
                        NPL.InsertDuLieu(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[0], GlobalSettings.ConnectionStringTarget);
                    else if (GlobalSettings.PhienBan == "TQDT")
                        NPL.InsertDuLieuTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[0], GlobalSettings.ConnectionStringTarget);
                    GlobalSettings.dsDNNew[0].Clear();

                    //TODO
                    if (!bgWorker.CancellationPending)
                        bgWorker.ReportProgress((int)(((double)(1) / (double)4) * 100));
                }
                if (GlobalSettings.dsDNNew[1] != null && GlobalSettings.dsDNNew[1].Tables[0].Rows.Count > 0)
                {
                    this.setText("Đang cập nhật danh sách Sản Phẩm vào dữ liệu Doanh Nghiệp");

                    if (GlobalSettings.PhienBan == "KTX")
                        SP.InsertDuLieu(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[1], GlobalSettings.ConnectionStringTarget);
                    else if (GlobalSettings.PhienBan == "TQDT")
                        SP.InsertDuLieuTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[1], GlobalSettings.ConnectionStringTarget);
                    GlobalSettings.dsDNNew[1].Clear();

                    //TODO
                    if (!bgWorker.CancellationPending)
                        bgWorker.ReportProgress((int)(((double)(2) / (double)4) * 100));
                }
                if (GlobalSettings.dsDNNew[2] != null && GlobalSettings.dsDNNew[2].Tables[0].Rows.Count > 0)
                {
                    this.setText("Đang cập nhật danh sách Định Mức vào dữ liệu Doanh Nghiệp");

                    if (GlobalSettings.PhienBan == "KTX")
                        DM.InsertDuLieuMoi(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[2], GlobalSettings.ConnectionStringTarget);
                    else if (GlobalSettings.PhienBan == "TQDT")
                        DM.InsertDuLieuTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[2], GlobalSettings.ConnectionStringTarget);
                    GlobalSettings.dsDNNew[2].Clear();

                    //TODO
                    if (!bgWorker.CancellationPending)
                        bgWorker.ReportProgress((int)(((double)(3) / (double)4) * 100));
                }
                if (GlobalSettings.dsDNNew[3] != null && GlobalSettings.dsDNNew[3].Tables[0].Rows.Count > 0)
                {
                    this.setText("Đang cập nhật danh sách Tờ Khai vào dữ liệu Doanh Nghiệp");

                    if (GlobalSettings.PhienBan == "KTX")
                        //TK.InsertDuLieuMoi(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[3], GlobalSettings.dsDNNew[4], GlobalSettings.ConnectionStringTarget);
                        TK.InsertDuLieuKTX(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[3], GlobalSettings.dsDNNew[4], GlobalSettings.ConnectionStringTarget);
                    else if (GlobalSettings.PhienBan == "TQDT")
                        TK.InsertDuLieuTQDT(GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, GlobalSettings.dsDNNew[3], GlobalSettings.dsDNNew[4], GlobalSettings.ConnectionStringTarget);

                    GlobalSettings.dsDNNew[3].Clear();
                    GlobalSettings.dsDNNew[4].Clear();

                    //TODO
                    if (!bgWorker.CancellationPending)
                        bgWorker.ReportProgress((int)(((double)(4) / (double)4) * 100));
                }
            }
            catch (Exception ex)
            {
                //Update by HUngtq 17/08/2012. Xuat du lieu luu tam ra file XML.
                if (GlobalSettings.dsHQ[0].Tables[0].Rows.Count > 0)
                {
                    GlobalSettings.dsHQ[0].WriteXml(Application.StartupPath + string.Format("\\{0}_{1}_SXXK_{2}_{3}.xml", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, "NguyenPhuLieu", DateTime.Today.ToString("yyyyMMdd")));
                }

                if (GlobalSettings.dsHQ[1].Tables[0].Rows.Count > 0)
                {
                    GlobalSettings.dsHQ[1].WriteXml(Application.StartupPath + string.Format("\\{0}_{1}_SXXK_{2}_{3}.xml", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, "SanPham", DateTime.Today.ToString("yyyyMMdd")));
                }

                if (GlobalSettings.dsHQ[2].Tables[0].Rows.Count > 0)
                {
                    GlobalSettings.dsHQ[2].WriteXml(Application.StartupPath + string.Format("\\{0}_{1}_SXXK_{2}_{3}.xml", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, "DinhMuc", DateTime.Today.ToString("yyyyMMdd")));
                }

                if (GlobalSettings.dsHQ[3].Tables[0].Rows.Count > 0)
                {
                    GlobalSettings.dsHQ[3].WriteXml(Application.StartupPath + string.Format("\\{0}_{1}_SXXK_{2}_{3}.xml", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, "ToKhaiMauDich", DateTime.Today.ToString("yyyyMMdd")));
                }

                if (GlobalSettings.dsHQ[4].Tables[0].Rows.Count > 0)
                {
                    GlobalSettings.dsHQ[4].WriteXml(Application.StartupPath + string.Format("\\{0}_{1}_SXXK_{2}_{3}.xml", GlobalSettings.MaHaiQuan, GlobalSettings.MaDoanhNghiep, "HangMauDich", DateTime.Today.ToString("yyyyMMdd")));
                }

                this.setText(ex.Message);
                e.Cancel = true;
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true) return;

            this.setText("Cập nhật dữ liệu doanh nghiệp thành công");
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Maximum;
            frmParent.btnNext.Enabled = true;
        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (bgWorker.CancellationPending == true) return;

            setProgessBar(e.ProgressPercentage);
        }

    }
}
