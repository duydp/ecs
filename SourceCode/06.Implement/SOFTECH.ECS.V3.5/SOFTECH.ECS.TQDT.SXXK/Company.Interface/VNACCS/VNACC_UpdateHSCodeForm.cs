﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_UpdateHSCodeForm : BaseForm
    {
        public VNACC_UpdateHSCodeForm()
        {
            InitializeComponent();
        }

        private void uiGroupBox4_Click(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                //Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //showMsg("MSG_0203008");
                    //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow <= 0)
            {
                MLMessages("Dòng bắt đầu phải > 0","", "", false);
            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
            }
            catch
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                // ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char MaHangHoaColumn = Convert.ToChar(txtMaHangColumn.Text);
            int MaHangHoaCol = ConvertCharToInt(MaHangHoaColumn);
            char MaHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int MaHSCol = ConvertCharToInt(MaHSColumn);
            char SoHopDongColumn = Convert.ToChar(txtSoHopDong.Text);
            int SoHopDongCol = ConvertCharToInt(SoHopDongColumn);
            DataTable dtTable = new DataTable("UPDATEHSCODE");
            DataColumn[] dtColumn = new DataColumn[3];
            dtColumn[0] = new DataColumn("MaHangHoa", Type.GetType("System.String"));
            dtColumn[1] = new DataColumn("MaHS", Type.GetType("System.String"));
            dtColumn[2] = new DataColumn("SoHopDong", Type.GetType("System.String"));
            dtTable.Columns.AddRange(dtColumn);
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        DataRow dr = dtTable.NewRow();
                        dr["MaHangHoa"] = Convert.ToString(wsr.Cells[MaHangHoaCol].Value).Trim();
                        dr["MaHS"] = Convert.ToString(wsr.Cells[MaHSCol].Value).Trim();
                        dr["SoHopDong"] = Convert.ToString(wsr.Cells[SoHopDongCol].Value).Trim();
                        dtTable.Rows.Add(dr);
                    }
                    catch
                    {
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            return;
                        }
                    }
                }
            }

            if (cbbLoaiHinh.SelectedValue.ToString()=="1")
            {
#if GC_V4
                #region Cập nhật mã HS cho GC_V5
                
                if (cbbLoaiHangHoa.SelectedValue.ToString()=="1")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                            SP.Ma = dr["MaHangHoa"].ToString();
                            SP.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            SP = SP.Load(SP.Ma, SP.HopDong_ID);
                            SP.MaHS = dr["MaHS"].ToString();
                            SP.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.KDT.GC.SanPham SP = new Company.GC.BLL.KDT.GC.SanPham();
                            SP.Ma = dr["MaHangHoa"].ToString();
                            SP.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            SP = SP.Load(SP.Ma, SP.HopDong_ID);
                            SP.MaHS = dr["MaHS"].ToString();
                            SP.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "2")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                            NPL.Ma = dr["MaHangHoa"].ToString();
                            NPL.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            NPL = NPL.Load(NPL.Ma, NPL.HopDong_ID);
                            NPL.MaHS = dr["MaHS"].ToString();
                            NPL.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieu NPL = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
                            NPL.Ma = dr["MaHangHoa"].ToString();
                            NPL.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            NPL = NPL.Load(NPL.Ma, NPL.HopDong_ID);
                            NPL.MaHS = dr["MaHS"].ToString();
                            NPL.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "3")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.GC.ThietBi TB = new Company.GC.BLL.GC.ThietBi();
                            TB.Ma = dr["MaHangHoa"].ToString();
                            TB.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            TB = TB.Load(TB.Ma, TB.HopDong_ID);
                            TB.MaHS = dr["MaHS"].ToString();
                            TB.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.KDT.GC.ThietBi TB = new Company.GC.BLL.KDT.GC.ThietBi();
                            TB.Ma = dr["MaHangHoa"].ToString();
                            TB.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            TB = TB.Load(TB.Ma, TB.HopDong_ID);
                            TB.MaHS = dr["MaHS"].ToString();
                            TB.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "4")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.GC.HangMau HM = new Company.GC.BLL.GC.HangMau();
                            HM.Ma = dr["MaHangHoa"].ToString();
                            HM.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            HM = HM.Load(HM.Ma, HM.HopDong_ID);
                            HM.MaHS = dr["MaHS"].ToString();
                            HM.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.GC.BLL.KDT.GC.HangMau HM = new Company.GC.BLL.KDT.GC.HangMau();
                            HM.Ma = dr["MaHangHoa"].ToString();
                            HM.HopDong_ID = Company.GC.BLL.KDT.GC.HopDong.GetID(dr["SoHopDong"].ToString());
                            HM = HM.Load(HM.Ma,HM.HopDong_ID);
                            HM.MaHS = dr["MaHS"].ToString();
                            HM.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                #endregion
#endif
            }
            else if (cbbLoaiHinh.SelectedValue.ToString()=="2")
            {
#if SXXK_V4
                #region Cập nhật mã HS cho SXXK_V5
                // Nguyên phụ liệu
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "2")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                            NPL.Ma = dr["MaHangHoa"].ToString();
                            NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            NPL = NPL.Load(NPL.Ma);
                            NPL.MaHS = dr["MaHS"].ToString();
                            NPL.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.BLL.KDT.SXXK.NguyenPhuLieu NPL = new Company.BLL.KDT.SXXK.NguyenPhuLieu();
                            NPL.Ma = dr["MaHangHoa"].ToString();
                            NPL= NPL.Load(NPL.Ma,"");
                            NPL.MaHS = dr["MaHS"].ToString();
                            NPL.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                //Sản phẩm
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "1")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                            SP.Ma = dr["MaHangHoa"].ToString();
                            SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            SP = SP.Load(SP.Ma);
                            SP.MaHS = dr["MaHS"].ToString();
                            SP.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.BLL.KDT.SXXK.SanPham SP = new Company.BLL.KDT.SXXK.SanPham(); ;
                            SP.Ma = dr["MaHangHoa"].ToString();
                            SP = SP.LoadSP(SP.Ma);
                            SP.MaHS = dr["MaHS"].ToString();
                            SP.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                #endregion
#endif
            }
            else if (cbbLoaiHinh.SelectedValue.ToString()=="3")
            {
#if SXXK_V4
                #region Cập nhật mã HS cho DNCX_V5
                // Hàng đưa vào
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "2")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.KDT.SHARE.QuanLyChungTu.CX.HangDuaVao NPL = new Company.KDT.SHARE.QuanLyChungTu.CX.HangDuaVao();
                            NPL.Ma = dr["MaHangHoa"].ToString();
                            NPL= NPL.Load(NPL.Ma);
                            NPL.MaHS = dr["MaHS"].ToString();
                            NPL.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                //Hàng đưa ra
                else if (cbbLoaiHangHoa.SelectedValue.ToString() == "1")
                {
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        try
                        {
                            Company.KDT.SHARE.QuanLyChungTu.CX.HangDuaRa SP = new Company.KDT.SHARE.QuanLyChungTu.CX.HangDuaRa();
                            SP.Ma = dr["MaHangHoa"].ToString();
                            SP = SP.Load(SP.Ma);
                            SP.MaHS = dr["MaHS"].ToString();
                            SP.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                #endregion
#endif
            }
            ShowMessage("Cập nhật thành công .",false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblLinkExcelTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_HangHoaUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_UpdateHSCodeForm_Load(object sender, EventArgs e)
        {
#if GC_V4
            cbbLoaiHinh.SelectedIndex = 0;
            cbbLoaiHangHoa.SelectedIndex = 0;
#elif SXXK_V4 
            cbbLoaiHinh.SelectedIndex = 1;
            cbbLoaiHangHoa.SelectedIndex = 0;
#endif

        }
    }
}
