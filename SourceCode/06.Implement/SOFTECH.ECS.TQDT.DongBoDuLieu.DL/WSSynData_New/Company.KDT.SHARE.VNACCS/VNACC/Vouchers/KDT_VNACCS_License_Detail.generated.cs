﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_License_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long License_ID { set; get; }
		public string NguoiCapGP { set; get; }
		public string SoGP { set; get; }
		public DateTime NgayCapGP { set; get; }
		public string NoiCapGP { set; get; }
		public string LoaiGP { set; get; }
		public DateTime NgayHetHanGP { set; get; }
        public List<KDT_VNACCS_License_Detail> ListLicense = new List<KDT_VNACCS_License_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_License_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_License_Detail> collection = new List<KDT_VNACCS_License_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_License_Detail entity = new KDT_VNACCS_License_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("License_ID"))) entity.License_ID = reader.GetInt64(reader.GetOrdinal("License_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiCapGP"))) entity.NguoiCapGP = reader.GetString(reader.GetOrdinal("NguoiCapGP"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGP"))) entity.SoGP = reader.GetString(reader.GetOrdinal("SoGP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapGP"))) entity.NgayCapGP = reader.GetDateTime(reader.GetOrdinal("NgayCapGP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiCapGP"))) entity.NoiCapGP = reader.GetString(reader.GetOrdinal("NoiCapGP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGP"))) entity.LoaiGP = reader.GetString(reader.GetOrdinal("LoaiGP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGP"))) entity.NgayHetHanGP = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGP"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_License_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_License_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_License_Details VALUES(@License_ID, @NguoiCapGP, @SoGP, @NgayCapGP, @NoiCapGP, @LoaiGP, @NgayHetHanGP)";
            string update = "UPDATE t_KDT_VNACCS_License_Details SET License_ID = @License_ID, NguoiCapGP = @NguoiCapGP, SoGP = @SoGP, NgayCapGP = @NgayCapGP, NoiCapGP = @NoiCapGP, LoaiGP = @LoaiGP, NgayHetHanGP = @NgayHetHanGP WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_License_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@License_ID", SqlDbType.BigInt, "License_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapGP", SqlDbType.NVarChar, "NguoiCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGP", SqlDbType.NVarChar, "SoGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGP", SqlDbType.DateTime, "NgayCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGP", SqlDbType.NVarChar, "NoiCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGP", SqlDbType.NVarChar, "LoaiGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanGP", SqlDbType.DateTime, "NgayHetHanGP", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@License_ID", SqlDbType.BigInt, "License_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapGP", SqlDbType.NVarChar, "NguoiCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGP", SqlDbType.NVarChar, "SoGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGP", SqlDbType.DateTime, "NgayCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGP", SqlDbType.NVarChar, "NoiCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGP", SqlDbType.NVarChar, "LoaiGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanGP", SqlDbType.DateTime, "NgayHetHanGP", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_License_Details VALUES(@License_ID, @NguoiCapGP, @SoGP, @NgayCapGP, @NoiCapGP, @LoaiGP, @NgayHetHanGP)";
            string update = "UPDATE t_KDT_VNACCS_License_Details SET License_ID = @License_ID, NguoiCapGP = @NguoiCapGP, SoGP = @SoGP, NgayCapGP = @NgayCapGP, NoiCapGP = @NoiCapGP, LoaiGP = @LoaiGP, NgayHetHanGP = @NgayHetHanGP WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_License_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@License_ID", SqlDbType.BigInt, "License_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapGP", SqlDbType.NVarChar, "NguoiCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGP", SqlDbType.NVarChar, "SoGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapGP", SqlDbType.DateTime, "NgayCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCapGP", SqlDbType.NVarChar, "NoiCapGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGP", SqlDbType.NVarChar, "LoaiGP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanGP", SqlDbType.DateTime, "NgayHetHanGP", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@License_ID", SqlDbType.BigInt, "License_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapGP", SqlDbType.NVarChar, "NguoiCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGP", SqlDbType.NVarChar, "SoGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapGP", SqlDbType.DateTime, "NgayCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCapGP", SqlDbType.NVarChar, "NoiCapGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGP", SqlDbType.NVarChar, "LoaiGP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanGP", SqlDbType.DateTime, "NgayHetHanGP", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_License_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_License_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_License_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_License_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_License_Detail> SelectCollectionBy_License_ID(long license_ID)
		{
            IDataReader reader = SelectReaderBy_License_ID(license_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_License_ID(long license_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_SelectBy_License_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, license_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_License_ID(long license_ID)
		{
			const string spName = "p_KDT_VNACCS_License_Detail_SelectBy_License_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, license_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_License_Detail(long license_ID, string nguoiCapGP, string soGP, DateTime ngayCapGP, string noiCapGP, string loaiGP, DateTime ngayHetHanGP)
		{
			KDT_VNACCS_License_Detail entity = new KDT_VNACCS_License_Detail();	
			entity.License_ID = license_ID;
			entity.NguoiCapGP = nguoiCapGP;
			entity.SoGP = soGP;
			entity.NgayCapGP = ngayCapGP;
			entity.NoiCapGP = noiCapGP;
			entity.LoaiGP = loaiGP;
			entity.NgayHetHanGP = ngayHetHanGP;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, License_ID);
			db.AddInParameter(dbCommand, "@NguoiCapGP", SqlDbType.NVarChar, NguoiCapGP);
			db.AddInParameter(dbCommand, "@SoGP", SqlDbType.NVarChar, SoGP);
			db.AddInParameter(dbCommand, "@NgayCapGP", SqlDbType.DateTime, NgayCapGP.Year <= 1753 ? DBNull.Value : (object) NgayCapGP);
			db.AddInParameter(dbCommand, "@NoiCapGP", SqlDbType.NVarChar, NoiCapGP);
			db.AddInParameter(dbCommand, "@LoaiGP", SqlDbType.NVarChar, LoaiGP);
			db.AddInParameter(dbCommand, "@NgayHetHanGP", SqlDbType.DateTime, NgayHetHanGP.Year <= 1753 ? DBNull.Value : (object) NgayHetHanGP);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_License_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_License_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_License_Detail(long id, long license_ID, string nguoiCapGP, string soGP, DateTime ngayCapGP, string noiCapGP, string loaiGP, DateTime ngayHetHanGP)
		{
			KDT_VNACCS_License_Detail entity = new KDT_VNACCS_License_Detail();			
			entity.ID = id;
			entity.License_ID = license_ID;
			entity.NguoiCapGP = nguoiCapGP;
			entity.SoGP = soGP;
			entity.NgayCapGP = ngayCapGP;
			entity.NoiCapGP = noiCapGP;
			entity.LoaiGP = loaiGP;
			entity.NgayHetHanGP = ngayHetHanGP;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_License_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, License_ID);
			db.AddInParameter(dbCommand, "@NguoiCapGP", SqlDbType.NVarChar, NguoiCapGP);
			db.AddInParameter(dbCommand, "@SoGP", SqlDbType.NVarChar, SoGP);
			db.AddInParameter(dbCommand, "@NgayCapGP", SqlDbType.DateTime, NgayCapGP.Year <= 1753 ? DBNull.Value : (object) NgayCapGP);
			db.AddInParameter(dbCommand, "@NoiCapGP", SqlDbType.NVarChar, NoiCapGP);
			db.AddInParameter(dbCommand, "@LoaiGP", SqlDbType.NVarChar, LoaiGP);
			db.AddInParameter(dbCommand, "@NgayHetHanGP", SqlDbType.DateTime, NgayHetHanGP.Year <= 1753 ? DBNull.Value : (object) NgayHetHanGP);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_License_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_License_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_License_Detail(long id, long license_ID, string nguoiCapGP, string soGP, DateTime ngayCapGP, string noiCapGP, string loaiGP, DateTime ngayHetHanGP)
		{
			KDT_VNACCS_License_Detail entity = new KDT_VNACCS_License_Detail();			
			entity.ID = id;
			entity.License_ID = license_ID;
			entity.NguoiCapGP = nguoiCapGP;
			entity.SoGP = soGP;
			entity.NgayCapGP = ngayCapGP;
			entity.NoiCapGP = noiCapGP;
			entity.LoaiGP = loaiGP;
			entity.NgayHetHanGP = ngayHetHanGP;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, License_ID);
			db.AddInParameter(dbCommand, "@NguoiCapGP", SqlDbType.NVarChar, NguoiCapGP);
			db.AddInParameter(dbCommand, "@SoGP", SqlDbType.NVarChar, SoGP);
			db.AddInParameter(dbCommand, "@NgayCapGP", SqlDbType.DateTime, NgayCapGP.Year <= 1753 ? DBNull.Value : (object) NgayCapGP);
			db.AddInParameter(dbCommand, "@NoiCapGP", SqlDbType.NVarChar, NoiCapGP);
			db.AddInParameter(dbCommand, "@LoaiGP", SqlDbType.NVarChar, LoaiGP);
			db.AddInParameter(dbCommand, "@NgayHetHanGP", SqlDbType.DateTime, NgayHetHanGP.Year <= 1753 ? DBNull.Value : (object) NgayHetHanGP);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_License_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_License_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_License_Detail(long id)
		{
			KDT_VNACCS_License_Detail entity = new KDT_VNACCS_License_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_License_ID(long license_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_DeleteBy_License_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@License_ID", SqlDbType.BigInt, license_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_License_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_License_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_License_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}