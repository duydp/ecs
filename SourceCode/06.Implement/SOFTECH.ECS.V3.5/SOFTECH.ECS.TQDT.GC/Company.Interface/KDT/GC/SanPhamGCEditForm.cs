﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class SanPhamGCEditForm : BaseFormHaveGuidPanel
    {
        public SanPham SPDetail;
        public HopDong HD = new HopDong();
        public bool isBrower = false;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public SanPhamGCEditForm()
        {
            InitializeComponent();
            SPDetail = new SanPham();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.SPDetail = null;
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = HD.SPCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void reset()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = "";
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = "";
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = "";
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = "0";
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGia.Text = "0";
                txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

                error.SetError(txtMaHS, null);
                error.SetError(txtMa, null);
                error.SetError(txtTen, null);
                BindData();
                SPDetail = new SanPham();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void setErro()
        {
            try
            {
                error.Clear();
                error.SetError(txtMa, null);
                error.SetError(txtTen, null);
                error.SetError(txtMaHS, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên/Mô tả sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtSoLuong, errorProvider, "Số lượng");
                isValid &= ValidateControl.ValidateZero(txtDonGia, errorProvider, "Đơn giá");
                isValid &= ValidateControl.ValidateChoose(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbNhomSP, errorProvider, "Nhóm sản phẩm", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
            return isValid;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                txtMa.Focus();
                if (!ValidateForm(false))
                    return;
                if (!MaHS.Validate(txtMaHS.Text.Trim(), 8))
                {
                    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                    return;
                }
                checkExitsSanPhamAndSTTHang();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void LoadLoaiSanPham()
        {
            //DataTable loaiSp = Company.GC.BLL.KDT.GC.NhomSanPham.SelectBy_HopDong_ID(HD.ID).Tables[0];
            //cbNhomSP.DataSource = loaiSp;
            //cbNhomSP.DisplayMember = "TenSanPham";
            //cbNhomSP.ValueMember = "ID";
            //if (loaiSp.Rows.Count > 0)
            //    cbNhomSP.SelectedIndex = 0;
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = SPDetail.Ma.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = SPDetail.Ten.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = SPDetail.MaHS.Trim();
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = SPDetail.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = SPDetail.SoLuongDangKy.ToString();
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGia.Text = SPDetail.DonGia.ToString();
                txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

                cbNhomSP.TextChanged -= new EventHandler(txt_TextChanged);
                cbNhomSP.SelectedValue = SPDetail.NhomSanPham_ID.ToString();
                cbNhomSP.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtDonGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                Caption = this.Text;
                if (!isBrower)
                {
                    txtMa.Focus();
                    this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                    cbDonViTinh.DataSource = this._DonViTinh;
                    cbDonViTinh.DisplayMember = "Ten";
                    cbDonViTinh.ValueMember = "ID";

                    cbNhomSP.DataSource = HD.NhomSPCollection;
                    cbNhomSP.DisplayMember = "TenSanPham";
                    cbNhomSP.ValueMember = "MaSanPham";
                    if (SPDetail !=null)
                    {
                        SetData();
                    }
                    else
                    {
                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                    if (this.OpenType == OpenFormType.View)
                    {
                        btnAdd.Enabled = false;
                        btnXoa.Enabled = false;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    }      
                }
                else
                {
                    uiGroupBox2.Visible = false;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    this.Width = dgList.Width;
                    this.Height = dgList.Height;
                    btnAdd.Visible = false;
                    btnClose.Visible = false;
                    HD.LoadCollection();
                }
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                dgList.DataSource = HD.SPCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private void checkExitsSanPhamAndSTTHang()
        {
            try
            {
                HD.SPCollection.Remove(SPDetail);
                foreach (SanPham sp in HD.SPCollection)
                {
                    if (sp.Ma.Trim().ToUpper() == txtMa.Text.Trim().ToUpper())
                    {
                        showMsg("MSG_2702055");
                        //string st = MLMessages("Đã có sản phẩm này trong danh sách.","MSG_WRN06","", false);
                        if (SPDetail.Ma.Trim() != "")
                            HD.SPCollection.Add(SPDetail);
                        txtMa.Focus();
                        return;
                    }

                }
                SPDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Text);
                SPDetail.MaHS = txtMaHS.Text.Trim();
                SPDetail.Ten = txtTen.Text.Trim();
                SPDetail.NhomSanPham_ID = cbNhomSP.SelectedValue.ToString();
                SPDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                SPDetail.Ma = txtMa.Text.Trim();
                SPDetail.HopDong_ID = HD.ID;
                SPDetail.DonGia = Convert.ToDecimal(txtDonGia.Text);
                HD.SPCollection.Add(SPDetail);
                reset();
                this.setErro();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    SPDetail = (SanPham)e.Row.DataRow;
                    if (!isBrower)
                    {
                        SetData();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                    e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                    e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Edit)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {

                        GridEXSelectedItemCollection items = dgList.SelectedItems;
                        SanPhamCollection ItemDelete = new SanPhamCollection();
                        foreach (GridEXSelectedItem row in items)
                        {
                            if (row.RowType == RowType.Record)
                            {
                                SanPham sp1 = (SanPham)row.GetRow().DataRow;
                                if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, HD.ID))
                                {
                                    if (showMsg("MSG_0203076", sp1.Ma, true) == "Yes")
                                    {
                                        sp1.Delete();
                                        Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                        HD.SPCollection.Remove(sp1);
                                        ItemDelete.Add(sp1);
                                    }
                                }
                                else
                                {
                                    sp1.Delete();
                                    Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                    HD.SPCollection.Remove(sp1);
                                    ItemDelete.Add(sp1);
                                }
                            }
                        }
                        if (ItemDelete.Count >= 1)
                            Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        BindData();
                        this.SetChange(true);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {

                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            SanPham sp1 = (SanPham)row.GetRow().DataRow;
                            if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, HD.ID))
                            {
                                if (showMsg("MSG_0203076", sp1.Ma, true) == "Yes")
                                {
                                    sp1.Delete();
                                    Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                    SPCollectionTMP.Add(sp1);
                                }
                            }
                            else
                            {
                                sp1.Delete();
                                Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                SPCollectionTMP.Add(sp1);
                            }
                        }
                    }
                    foreach (SanPham spDelete in SPCollectionTMP)
                        HD.SPCollection.Remove(spDelete);
                    if (SPCollectionTMP.Count >= 1)
                        Log.LogHDGC(HD, SPCollectionTMP, MessageTitle.XoaSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindData();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SPDetail = (SanPham)i.GetRow().DataRow;
                            if (!isBrower)
                            {
                                SetData();
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


    }
}