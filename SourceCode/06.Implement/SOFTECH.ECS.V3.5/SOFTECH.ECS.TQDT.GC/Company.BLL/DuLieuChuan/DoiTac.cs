using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.DuLieuChuan
{
    public partial class DoiTac
    {
        protected static SqlDatabase db2 = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public void Update(DataSet ds, string MaDoanhNghiep)
        {
            string spNameInsert = "p_HaiQuan_DoiTac_Insert";
            DbCommand dbCommandInsert = this.db.GetStoredProcCommand(spNameInsert);
            this.db.AddOutParameter(dbCommandInsert, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommandInsert, "@MaCongTy", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@TenCongTy", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@DiaChi", SqlDbType.NVarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@DienThoai", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@Email", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@Fax", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@GhiChu", SqlDbType.NVarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandInsert, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            string spNameUpdate = "p_HaiQuan_DoiTac_Update";
            DbCommand dbCommandUpdate = this.db.GetStoredProcCommand(spNameUpdate);

            this.db.AddInParameter(dbCommandUpdate, "@ID", SqlDbType.BigInt, DataRowVersion.Default);
            this.db.AddInParameter(dbCommandUpdate, "@MaCongTy", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@TenCongTy", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@DiaChi", SqlDbType.NVarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@DienThoai", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@Email", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@Fax", SqlDbType.VarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@GhiChu", SqlDbType.NVarChar, DataRowVersion.Current);
            this.db.AddInParameter(dbCommandUpdate, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            string spNameDelete = "delete from t_HaiQuan_DoiTac where id=@ID";
            DbCommand dbCommandDelete = this.db.GetSqlStringCommand(spNameDelete);

            this.db.AddInParameter(dbCommandDelete, "@ID", SqlDbType.BigInt, DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, dbCommandInsert, dbCommandUpdate, dbCommandDelete, UpdateBehavior.Standard);
        }

        public void InsertUpdate(DoiTacCollection collection, string MaDoanhNghiep)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DoiTac item in collection)
                    {
                        item.MaDoanhNghiep = MaDoanhNghiep;
                        if (item.ID > 0)
                            item.Update();
                        else
                            item.Insert();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// HungTQ, Update 24052010.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetName(string maDN)
        {
            string query = string.Format("SELECT TenCongTy FROM t_HaiQuan_DoiTac WHERE [MaCongTy] = '{0}'", maDN);
            DbCommand dbCommand = db2.GetSqlStringCommand(query);
            IDataReader reader = db2.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                return reader["TenCongTy"].ToString();
            }
            return string.Empty;
        }
    }
}