﻿namespace Company.Interface
{
    partial class ThongTinDNAndHQForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongTinDNAndHQForm2));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkOnlyMe = new Janus.Windows.EditControls.UICheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoTienKhoanTKX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienKhoanTKN = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtThongBaoHetHan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFax = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMailDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiLienhe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenNganHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNganHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiLienHe = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvEmailDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDienThoai = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDienThoai)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(409, 478);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(409, 478);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ImageIndex = 0;
            this.btnSave.ImageList = this.imageList1;
            this.btnSave.ImageVerticalAlignment = Janus.Windows.EditControls.ImageVerticalAlignment.Near;
            this.btnSave.Location = new System.Drawing.Point(126, 448);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "disk.png");
            this.imageList1.Images.SetKeyName(1, "exit.png");
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ImageIndex = 1;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.ImageVerticalAlignment = Janus.Windows.EditControls.ImageVerticalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(207, 448);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.uiGroupBox4.Controls.Add(this.chkOnlyMe);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtSoTienKhoanTKX);
            this.uiGroupBox4.Controls.Add(this.txtSoTienKhoanTKN);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.txtThongBaoHetHan);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaMid);
            this.uiGroupBox4.Controls.Add(this.txtFax);
            this.uiGroupBox4.Controls.Add(this.txtMailDN);
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienhe);
            this.uiGroupBox4.Controls.Add(this.txtDienThoai);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 176);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(389, 266);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.Text = "Thông tin doanh nghiệp";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // chkOnlyMe
            // 
            this.chkOnlyMe.BackColor = System.Drawing.Color.Transparent;
            this.chkOnlyMe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOnlyMe.ForeColor = System.Drawing.Color.Red;
            this.chkOnlyMe.Location = new System.Drawing.Point(272, 239);
            this.chkOnlyMe.Name = "chkOnlyMe";
            this.chkOnlyMe.Size = new System.Drawing.Size(96, 23);
            this.chkOnlyMe.TabIndex = 24;
            this.chkOnlyMe.Text = "Dành cho tôi";
            this.chkOnlyMe.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(264, 215);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "TKX";
            // 
            // txtSoTienKhoanTKX
            // 
            this.txtSoTienKhoanTKX.DecimalDigits = 0;
            this.txtSoTienKhoanTKX.Location = new System.Drawing.Point(295, 210);
            this.txtSoTienKhoanTKX.Name = "txtSoTienKhoanTKX";
            this.txtSoTienKhoanTKX.Size = new System.Drawing.Size(75, 21);
            this.txtSoTienKhoanTKX.TabIndex = 20;
            this.txtSoTienKhoanTKX.Text = "0";
            this.txtSoTienKhoanTKX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienKhoanTKX.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTienKhoanTKN
            // 
            this.txtSoTienKhoanTKN.DecimalDigits = 0;
            this.txtSoTienKhoanTKN.Location = new System.Drawing.Point(154, 211);
            this.txtSoTienKhoanTKN.Name = "txtSoTienKhoanTKN";
            this.txtSoTienKhoanTKN.Size = new System.Drawing.Size(75, 21);
            this.txtSoTienKhoanTKN.TabIndex = 18;
            this.txtSoTienKhoanTKN.Text = "0";
            this.txtSoTienKhoanTKN.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienKhoanTKN.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(234, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "ngày";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(108, 215);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "TKN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(4, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Thông báo hết hạn trước";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(7, 215);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Số tiền khoán";
            // 
            // txtThongBaoHetHan
            // 
            this.txtThongBaoHetHan.DecimalDigits = 0;
            this.txtThongBaoHetHan.Location = new System.Drawing.Point(154, 239);
            this.txtThongBaoHetHan.Name = "txtThongBaoHetHan";
            this.txtThongBaoHetHan.Size = new System.Drawing.Size(75, 21);
            this.txtThongBaoHetHan.TabIndex = 22;
            this.txtThongBaoHetHan.Text = "0";
            this.txtThongBaoHetHan.Value = 0;
            this.txtThongBaoHetHan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtThongBaoHetHan.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(5, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Mã MID";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(154, 183);
            this.txtMaMid.MaxLength = 20;
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(216, 21);
            this.txtMaMid.TabIndex = 15;
            this.txtMaMid.VisualStyleManager = this.vsmMain;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(281, 104);
            this.txtFax.MaxLength = 200;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(90, 21);
            this.txtFax.TabIndex = 9;
            this.txtFax.VisualStyleManager = this.vsmMain;
            // 
            // txtMailDN
            // 
            this.txtMailDN.Location = new System.Drawing.Point(154, 157);
            this.txtMailDN.MaxLength = 200;
            this.txtMailDN.Name = "txtMailDN";
            this.txtMailDN.Size = new System.Drawing.Size(216, 21);
            this.txtMailDN.TabIndex = 13;
            this.txtMailDN.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiLienhe
            // 
            this.txtNguoiLienhe.Location = new System.Drawing.Point(154, 131);
            this.txtNguoiLienhe.MaxLength = 200;
            this.txtNguoiLienhe.Name = "txtNguoiLienhe";
            this.txtNguoiLienhe.Size = new System.Drawing.Size(217, 21);
            this.txtNguoiLienhe.TabIndex = 11;
            this.txtNguoiLienhe.VisualStyleManager = this.vsmMain;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(154, 104);
            this.txtDienThoai.MaxLength = 200;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(90, 21);
            this.txtDienThoai.TabIndex = 7;
            this.txtDienThoai.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(4, 161);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(144, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Email doanh nghiệp/ cá nhân";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(155, 77);
            this.txtDiaChi.MaxLength = 200;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(216, 21);
            this.txtDiaChi.TabIndex = 5;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(5, 136);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Người liên hệ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(253, 109);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Fax";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(5, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Điện thoại";
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(155, 50);
            this.txtTenDN.MaxLength = 200;
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(216, 21);
            this.txtTenDN.TabIndex = 3;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên doanh nghiệp";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(155, 23);
            this.txtMaDN.MaxLength = 15;
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(216, 21);
            this.txtMaDN.TabIndex = 1;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã doanh nghiệp";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.txtMaCuc);
            this.uiGroupBox3.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtTenNganHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 9);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(389, 161);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Thông tin hải quan";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(6, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Mã cục Hải Quan";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(154, 20);
            this.txtMaCuc.MaxLength = 6;
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(216, 21);
            this.txtMaCuc.TabIndex = 1;
            this.txtMaCuc.VisualStyleManager = this.vsmMain;
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(154, 130);
            this.txtMailHaiQuan.MaxLength = 100;
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(216, 21);
            this.txtMailHaiQuan.TabIndex = 9;
            this.txtMailHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(6, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Mail Hải quan";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(154, 75);
            this.txtTenCucHQ.MaxLength = 200;
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(216, 21);
            this.txtTenCucHQ.TabIndex = 5;
            this.txtTenCucHQ.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(6, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tên cục hải quan";
            // 
            // txtTenNganHQ
            // 
            this.txtTenNganHQ.Location = new System.Drawing.Point(154, 102);
            this.txtTenNganHQ.MaxLength = 40;
            this.txtTenNganHQ.Name = "txtTenNganHQ";
            this.txtTenNganHQ.Size = new System.Drawing.Size(216, 21);
            this.txtTenNganHQ.TabIndex = 7;
            this.txtTenNganHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(6, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tên ngắn hải quan";
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(154, 47);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(230, 22);
            this.donViHaiQuanControl1.TabIndex = 3;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            this.donViHaiQuanControl1.Leave += new System.EventHandler(this.donViHaiQuanControl1_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(6, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Chọn hải quan khai báo";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChi
            // 
            this.rfvDiaChi.ControlToValidate = this.txtDiaChi;
            this.rfvDiaChi.ErrorMessage = "\"Địa chỉ\" không được để trống";
            this.rfvDiaChi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChi.Icon")));
            this.rfvDiaChi.Tag = "rfvDiaChi";
            // 
            // rfvMaDN
            // 
            this.rfvMaDN.ControlToValidate = this.txtMaDN;
            this.rfvMaDN.ErrorMessage = "\"Mã doanh nghiệp\" không được trống";
            this.rfvMaDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDN.Icon")));
            this.rfvMaDN.Tag = "rfvMaDN";
            // 
            // rfvTenDN
            // 
            this.rfvTenDN.ControlToValidate = this.txtTenDN;
            this.rfvTenDN.ErrorMessage = "\"Tên doanh nghiệp \" không được trống";
            this.rfvTenDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDN.Icon")));
            this.rfvTenDN.Tag = "rfvTenDN";
            // 
            // rfvTenNganHQ
            // 
            this.rfvTenNganHQ.ControlToValidate = this.txtTenNganHQ;
            this.rfvTenNganHQ.ErrorMessage = "\"Tên ngắn chi cục Hải quan\" không được trống";
            this.rfvTenNganHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNganHQ.Icon")));
            this.rfvTenNganHQ.Tag = "rfvTenNganHQ";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rfvTenCuc
            // 
            this.rfvTenCuc.ControlToValidate = this.txtTenCucHQ;
            this.rfvTenCuc.ErrorMessage = "\"Tên cục Hải quan\" không được trống";
            this.rfvTenCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenCuc.Icon")));
            this.rfvTenCuc.Tag = "rfvTenCuc";
            // 
            // rfvMaCuc
            // 
            this.rfvMaCuc.ControlToValidate = this.txtMaCuc;
            this.rfvMaCuc.ErrorMessage = "\"Mã cục Hải quan\" không được trống";
            this.rfvMaCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaCuc.Icon")));
            this.rfvMaCuc.Tag = "rfvMaCuc";
            // 
            // rfvNguoiLienHe
            // 
            this.rfvNguoiLienHe.ControlToValidate = this.txtNguoiLienhe;
            this.rfvNguoiLienHe.ErrorMessage = "\"Người liên hệ\" không được trống";
            this.rfvNguoiLienHe.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiLienHe.Icon")));
            this.rfvNguoiLienHe.Tag = "rfvNguoiLienHe";
            // 
            // rfvEmailDN
            // 
            this.rfvEmailDN.ControlToValidate = this.txtMailDN;
            this.rfvEmailDN.ErrorMessage = "\"Email doanh nghiệp\" không được trống";
            this.rfvEmailDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvEmailDN.Icon")));
            this.rfvEmailDN.Tag = "rfvEmailDN";
            // 
            // rfvDienThoai
            // 
            this.rfvDienThoai.ControlToValidate = this.txtDienThoai;
            this.rfvDienThoai.ErrorMessage = "\"Điện thoại\" không được trống";
            this.rfvDienThoai.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDienThoai.Icon")));
            this.rfvDienThoai.Tag = "rfvDienThoai";
            // 
            // ThongTinDNAndHQForm2
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(409, 478);
            this.Controls.Add(this.uiGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ThongTinDNAndHQForm2";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin doanh nghiệp và hải quan";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDienThoai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNganHQ;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHQ;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label5;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThongBaoHetHan;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienKhoanTKX;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienKhoanTKN;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private System.Windows.Forms.Label label14;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtFax;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienhe;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDN;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ImageList imageList1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiLienHe;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvEmailDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDienThoai;
        protected Janus.Windows.EditControls.UICheckBox chkOnlyMe;

    }
}