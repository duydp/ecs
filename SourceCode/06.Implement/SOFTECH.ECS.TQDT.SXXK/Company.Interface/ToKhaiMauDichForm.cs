﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.BLL.KDT.SXXK;
using System.Net.Mail;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;
using Infragistics.Excel;
using Company.KDT.SHARE.Components.AnDinhThue;

namespace Company.Interface
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public string NhomLoaiHinh = string.Empty;
        private string xmlCurrent = "";
        public long pTKMD_ID = 0;
        public bool _bNew = true;
        public static string tenPTVT = "";
        private static int i;
        public bool luu = false;
        public static int soDongHang;
        private static bool isAdded = false;
        //public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();
        #region Biến phục vụ cho việc In tờ khai sửa đổi bổ sung
        //BEGIN: DATLMQ bổ sung các biến dùng để lưu giá trị TK cũ 16/02/2011
        private string nguoiNhapKhau = string.Empty;
        private string nguoiXuatKhau = string.Empty;
        private string nguoiUyThac = string.Empty;
        private string PTVT = string.Empty;
        private string soHieuPTVT = string.Empty;
        private DateTime ngayDenPTVT = new DateTime(1900, 1, 1);
        private string nuocXuatKhau = string.Empty;
        private string nuocNhapKhau = string.Empty;
        private string dieuKienGiaoHang = string.Empty;
        private string phuongThucThanhToan = string.Empty;
        private string soGiayPhep = string.Empty;
        private DateTime ngayGiayPhep = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanGiayPhep = new DateTime(1900, 1, 1);
        private string hoaDonTM = string.Empty;
        private DateTime ngayHoaDonTM = new DateTime(1900, 1, 1);
        private string diaDiemDoHang = string.Empty;
        private string nguyenTe = string.Empty;
        private decimal tyGiaTinhThue = 0;
        private decimal tyGiaUSD = 0;
        private string soHopDong = string.Empty;
        private DateTime ngayHopDong = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanHopDong = new DateTime(1900, 1, 1);
        private string soVanDon = string.Empty;
        private DateTime ngayVanDon = new DateTime(1900, 1, 1);
        private string diaDiemXepHang = string.Empty;
        private decimal soContainer20;
        private decimal soContainer40;
        private decimal soKienHang;
        private decimal trongLuong;
        private double trongLuongNet;
        private decimal lePhiHaiQuan;
        private decimal phiBaoHiem;
        private decimal phiVanChuyen;
        private decimal phiKhac;
        //END

        //DATLMQ bổ sung biến phục vụ việc tự động lưu nội dung sửa đổi bổ sung 04/03/2011
        //public static List<Company.KD.BLL.KDT.HangMauDich> ListHangMauDichEdit = new List<Company.KD.BLL.KDT.HangMauDich>();
        //private static Company.KD.BLL.KDT.HangMauDich hangMDTK = new Company.KD.BLL.KDT.HangMauDich();
        public static string maHS_Edit = string.Empty;
        public static string tenHang_Edit = string.Empty;
        public static string maHang_Edit = string.Empty;
        public static string xuatXu_Edit = string.Empty;
        public static decimal soLuong_Edit;
        public static string dvt_Edit = string.Empty;
        public static decimal dongiaNT_Edit;
        public static decimal trigiaNT_Edit;
        //END
        #endregion

        public bool isTuChoi = false;
        public ToKhaiMauDichForm()
        {
            InitializeComponent();

            SetEvent_TextBox_VanDon();
            SetEvent_TextBox_GiayPhep();
            SetEvent_TextBox_HopDong();
            SetEvent_TextBox_HoaDong();
        }

        private void loadTKMDData()
        {

            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;

            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;

            // Đại lý TTHQ.
            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

            //Don vi uy thac
            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            txtTenDonViUyThac.Text = this.TKMD.TenDonViUT;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccNgayDen.Text = "";
            // Nước.
            if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
                ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;

            // Giấy phép.
            if (this.TKMD.SoGiayPhep.Trim().Length > 0) ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = false;
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToShortDateString();
            else ccNgayGiayPhep.Text = "";
            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            else ccNgayHHGiayPhep.Text = "";
            //if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            if (this.TKMD.SoHoaDonThuongMai.Trim().Length > 0) ccNgayHDTM.ReadOnly = false;
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            else ccNgayHDTM.Text = "";
            //if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;

            // Hợp đồng.
            if (this.TKMD.SoHopDong.Trim().Length > 0) ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = false;
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Text = this.TKMD.NgayHopDong.ToShortDateString();
            else ccNgayHopDong.Text = "";
            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToShortDateString();
            else ccNgayHHHopDong.Text = "";
            //if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.SoVanDon.Trim().Length > 0) ccNgayVanTaiDon.ReadOnly = false;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
            else ccNgayVanTaiDon.Text = "";
            // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;

            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;

            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;

            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;

            //Phí ngân hàng
            txtPhiNganHang.Value = this.TKMD.PhiKhac;

            txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;

            if (this.TKMD.ID > 0)
            {
                if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                    this.TKMD.LoadHMDCollection();
                if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                    this.TKMD.LoadChungTuTKCollection();
                this.TKMD.LoadChungTuHaiQuan();
                if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                    this.TKMD.LoadCO();

            }

            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();

            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            this.radTB.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            this.radNPL.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            this.radSP.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            if (this.TKMD.LoaiHangHoa == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (this.TKMD.LoaiHangHoa == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }
            this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);

            //chung tu kem
            txtChungTu.Text = this.TKMD.GiayTo;
            //so to khai 
            if (this.TKMD.SoToKhai > 0)
                txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();

            if (this.TKMD.SoTiepNhan > 0)
                txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();

            txtDeXuatKhac.Text = this.TKMD.DeXuatKhac;
            txtLyDoSua.Text = this.TKMD.LyDoSua;

            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chờ duyệt";
                }
                else
                {
                    lblTrangThai.Text = "Wait for approval";
                }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chưa khai báo";
                }
                else
                {
                    lblTrangThai.Text = " Not yet declarated";
                }

            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Không được phê duyệt";
                }
                else
                {
                    lblTrangThai.Text = " Not yet Approved";
                }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Sửa tờ khai";
                }
                else
                {
                    lblTrangThai.Text = " Edit ";
                }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Đã duyệt";
                }
                else
                {
                    lblTrangThai.Text = " Approved";
                }
            }

            if (TKMD.PhanLuong != "")
            {
                if (TKMD.PhanLuong == "1")
                    lblTrangThai.Text = "Luồng xanh";
                else if (TKMD.PhanLuong == "2")
                    lblTrangThai.Text = "Luồng vàng";
                else if (TKMD.PhanLuong == "3")
                    lblTrangThai.Text = "Luồng đỏ";
            }
            else
                lblTrangThai.Text = "Tờ khai chưa được phân luồng";

        }

        private void khoitao_DuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            if (this.NhomLoaiHinh.StartsWith("N"))
            {
                ctrCuaKhau.Ma = GlobalSettings.DIA_DIEM_DO_HANG;
                ctrNuocXuatKhau.Ma = GlobalSettings.NUOC;
            }
            else
            {
                ctrCuaKhau.Ma = GlobalSettings.CUA_KHAU_XUAT_HANG;
                ctrNuocXuatKhau.Ma = GlobalSettings.NUOCNK;

            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            System.Data.DataTable dt = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDanhSachDoiTac(GlobalSettings.MA_DON_VI);
            foreach (System.Data.DataRow dr in dt.Rows)
                col.Add(dr["TenDonViDoiTac"].ToString());
            txtTenDonViDoiTac.AutoCompleteCustomSource = col;
        }

        private void khoitao_GiaoDienToKhai()
        {
            this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);
            //if (NhomLoaiHinh != "NSX" && NhomLoaiHinh != "XSX" && NhomLoaiHinh != "NGC" && NhomLoaiHinh != "XGC")
            //    ctrDonViHaiQuan.ReadOnly = false;
            //grbHopDong.Enabled = chkHopDong.Enabled = true;
            //chkGiayPhep.Enabled = gbGiayPhep.Enabled = true;
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai nhập khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Import declaration (" + this.NhomLoaiHinh + ")";
                }
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                // Tiêu đề.

                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Export Declaration (" + this.NhomLoaiHinh + ")";
                }
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                // grbNguoiNK.Text = "Người xuất khẩu";
                // grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNguoiNK.Text = "Người xuất khẩu";
                    grbNguoiXK.Text = "Người nhập khẩu";
                }
                else
                {
                    grbNguoiNK.Text = "Importer";
                    grbNguoiXK.Text = "Exporter";
                }


                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = false;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                grbHoaDonThuongMai.Enabled = true;
                //chkVanTaiDon.Visible = 

                // Vận tải đơn.
                //Hungtq update 07/02/2012. Hien thi Van don, Hoa don.
                grbVanTaiDon.Enabled = true;

                // Nước XK.
                //grbNuocXK.Text = "Nước nhập khẩu";
                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                //grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                //chkDiaDiemXepHang.Enabled = 
                grbDiaDiemXepHang.Enabled = false;
                // chkDiaDiemXepHang.Visible = false;
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNuocXK.Text = "Nước nhập khẩu";
                    grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                }
                else
                {
                    grbNuocXK.Text = "Import country";
                    grbDiaDiemDoHang.Text = "Export store";

                }
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;

            }

            // Loại hình gia công.
            if (this.NhomLoaiHinh == "NSX" || this.NhomLoaiHinh == "XSX")
            {
                if (this.NhomLoaiHinh == "NSX")
                {
                    grpLoaiHangHoa.Visible = false;
                    radNPL.Checked = true;
                    radSP.Checked = false;
                    radTB.Checked = false;

                }
                else
                {
                    radTB.Visible = false;
                }
            }
            else
                grpLoaiHangHoa.Visible = false;

        }

        //-----------------------------------------------------------------------------------------

        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            lblTongTGNT.Text = "0 (USD)";
            lblTongThue.Text = "0 (VND)";
            lblTongSoHang.Text = "0";

            //InitialProgess();

            if (this.DesignMode)
                return;

            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            ccNgayDen.Value = DateTime.Today;

            this.khoitao_DuLieuChuan();
            switch (this.OpenType)
            {
                case OpenFormType.View:
                    this.loadTKMDData();
                    this.ViewTKMD();
                    this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Edit:
                    this.loadTKMDData();
                    this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Insert:
                    txtTyGiaUSD.Value = Company.KDT.SHARE.Components.Globals.GetNguyenTe("USD");//HungTQ Update 13/12/2010.
                    break;
            }

            this.khoitao_GiaoDienToKhai();
            setCommandStatus();
            if (TKMD.ID > 0)
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "TK_SUA";
                sendXML.master_id = TKMD.ID;
                if (sendXML.Load())
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đang chờ xác nhận hải quan";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait for approval";
                    }
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    lblTrangThai.Text = "Chưa khai báo";
                }

                tinhTongTriGiaKhaiBao();
            }
            if (GlobalSettings.TuDongTinhThue == "0")
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)) && !MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
            //AVN 0707
            //cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        }

        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //ctrDonViHaiQuan.ReadOnly = true;
            txtSoLuongPLTK.ReadOnly = true;
            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;

            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;

            // Container 20.
            txtSoContainer20.ReadOnly = true;

            // Container 40.
            txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            //grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            //gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            //grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            // grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            // grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        private void ReadExcel()
        {
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            //ReadExcelForm reform = new ReadExcelForm();
            ReadExcelForm2 reform = new ReadExcelForm2();
            reform.TiGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            reform.hmdCollection = this.TKMD.HMDCollection;
            reform.TKMD = this.TKMD;
            if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
            reform.ShowDialog();
            this.TKMD.HMDCollection = reform.hmdCollection;
            if (GlobalSettings.TuDongTinhThue == "1")
            {
                tinhLaiThue();
            }
            else
            {
                tinhTongTriGiaKhaiBao();
            }
            try
            {
                dgList.DataSource = this.TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            setCommandStatus();
        }
        //-----------------------------------------------------------------------------------------

        private void themHang()
        {

            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            HangMauDichForm f = new HangMauDichForm();
            f.OpenType = OpenFormType.Edit;
            f.TKMD = this.TKMD;
            f.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

            f.NhomLoaiHinh = this.NhomLoaiHinh;
            if (radNPL.Checked) f.LoaiHangHoa = "N";
            if (radSP.Checked) f.LoaiHangHoa = "S";
            if (radTB.Checked) f.LoaiHangHoa = "T";
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            f.HMDCollection = this.TKMD.HMDCollection;
            f.MaNguyenTe = ctrNguyenTe.Ma;
            f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.TKMD.HMDCollection = f.HMDCollection;
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            if (GlobalSettings.TuDongTinhThue == "1")
            {
                tinhLaiThue();
            }
            else
            {
                tinhTongTriGiaKhaiBao();
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------

        private Company.BLL.KDT.HangMauDich FindHangMauDichBy(Company.BLL.KDT.HangMauDichCollection list, long idTKMD, string maPhu)
        {
            foreach (Company.BLL.KDT.HangMauDich item in list)
            {
                if (item.TKMD_ID == idTKMD && item.MaPhu == maPhu)
                {
                    return item;
                }
            }

            return null;
        }

        public static Company.BLL.KDT.HangMauDichCollection deleteHMDCollection = new Company.BLL.KDT.HangMauDichCollection();
        public static Company.BLL.KDT.HangMauDichCollection insertHMDCollection = new Company.BLL.KDT.HangMauDichCollection();
        private bool AutoInsertHMDToTKEdit(string _maHS_Old, string _tenHang_Old, string _maHang_Old, string _xuatXu_Old, decimal _soLuong_Old, string _dvt_Old, decimal _donGiaNT_Old, decimal _triGiaNT_Old, ToKhaiMauDich toKhaiMauDich)
        {
            try
            {
                //Company.KD.BLL.KDT.HangMauDich HMD_Edit = new Company.KD.BLL.KDT.HangMauDich();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail noiDungEditDetail = new Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail();
                HangMauDichEditForm hmdEditForm = new HangMauDichEditForm();
                if (toKhaiMauDich.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    #region 21. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "21. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHS_Edit = _maHS_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHS_Edit = _maHS_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "20. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            tenHang_Edit = _tenHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                tenHang_Edit = _tenHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "20. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHang_Edit = _maHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHang_Edit = _maHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 22. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "22. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                            xuatXu_Edit = _xuatXu_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                                xuatXu_Edit = _xuatXu_Old;
                            }
                        }
                    }
                    #endregion

                    #region 23. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "23. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            soLuong_Edit = _soLuong_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                soLuong_Edit = _soLuong_Old;
                            }
                        }
                    }
                    #endregion

                    #region 24. Đơn vị tính
                    if (dvt_Edit.Trim() != _dvt_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "24. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                            dvt_Edit = _dvt_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                                dvt_Edit = _dvt_Old;
                            }
                        }
                    }
                    #endregion

                    #region 25. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "25. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            dongiaNT_Edit = _donGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                dongiaNT_Edit = _donGiaNT_Old;
                            }
                        }
                    }
                    #endregion

                    #region 26. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "26. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            trigiaNT_Edit = _triGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                trigiaNT_Edit = _triGiaNT_Old;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 18. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "18. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "17. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "17. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 19. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "19. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "20. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 21. Đơn vị tính
                    if (dvt_Edit.Trim() != _dvt_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "21. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "22. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "23. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception e)
            {
                ShowMessage("Có lỗi trong quá trình ghi dữ liệu vào tờ khai sửa đổi, bổ sung.\r\nChi tiết lỗi: " + e.Message, false);
                return false;
            }
        }

        //DATLMQ bổ sung AutoInsertAddHMD ngày 12/08/2011
        private bool checkHMDEsistsInTKMD(long _TKMD_ID, string _maHang)
        {
            Company.BLL.KDT.HangMauDich hmd = new Company.BLL.KDT.HangMauDich();
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _maHang);
            HangMauDichCollection hmdColl = hmd.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }

        private void AutoInsertAddHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            foreach (Company.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
            {
                if (!checkHMDEsistsInTKMD(TKMD.ID, hangMD.MaPhu))
                {
                    insertHMDCollection.Add(hangMD);
                    listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                    if (listNoiDungTK.Count == 0)
                    {
                        noiDungTK.Ma = "MaHang";
                        noiDungTK.Ten = "20. Mã hàng";
                        noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                        noiDungTK.InsertUpdate();

                        foreach (Company.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                    else
                    {
                        foreach (Company.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                }
            }
        }

        //DATLMQ bổ sung AutoInsertDeleteHMD ngày 12/08/2011
        private void AutoInsertDeleteHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            if (HangMauDichForm.isDeleted)
            {
                listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                if (listNoiDungTK.Count == 0)
                {
                    noiDungTK.Ma = "MaHang";
                    noiDungTK.Ten = "20. Mã hàng";
                    noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungTK.InsertUpdate();

                    foreach (Company.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin " + noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
                else
                {
                    foreach (Company.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = "Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
            }
        }

        /// <summary>
        /// Lưu thông tin tờ khai.
        /// </summary>
        public void Save()
        {
            bool valid = false;
            bool isKhaibaoSua = this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;

            //if (this.NhomLoaiHinh.Substring(0, 1) != "N")
            //    if (!Globals.ValidateNull(txtSoVanTaiDon, epError, "vận tải đơn"))
            //    {
            //        txtSoVanTaiDon.Focus();
            //        return;
            //    }

            #region To khai sua

            if (isKhaibaoSua)
            {
                if (txtLyDoSua.Text.Trim() == "")
                {
                    MLMessages("Chưa nhập lý do sửa tờ khai", "MSG_SAV06", "", false);
                    return;
                }
                #region OldCode
                //else if (NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID).Count == 0)
                //{
                //    if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //    {
                //        NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //        noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //        noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
                //        noiDungChinhSuaTKForm.ShowDialog();
                //    }
                //    return;
                //}
                //else
                //{
                //    int count = 0;
                //    NoiDungChinhSuaTKDetailForm ndDieuChinhTKChiTietForm = new NoiDungChinhSuaTKDetailForm();
                //    ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                //    foreach (NoiDungDieuChinhTK nddctk in ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK)
                //    {
                //        if (nddctk.TrangThai != 1)
                //            ++count;
                //    }
                //    if (count == 0)
                //    {
                //        if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //        {
                //            NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //            noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //            noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
                //            noiDungChinhSuaTKForm.ShowDialog();
                //        }
                //        return;
                //    }
                //}
                #endregion

                #region commnet
                //if (ShowMessage("Bạn có muốn chương trình tự động lưu nội dung sửa đổi, bổ sung không?", true).Equals("No"))
                //{
                //    #region Người sử dụng nhập nội dung sửa đổi bằng tay
                //    string whereCondition = "TKMD_ID = " + TKMD.ID + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh.Trim() + "'";
                //    if (NoiDungDieuChinhTK.SelectCollectionDynamic(whereCondition, "").Count == 0)
                //    {
                //        if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //        {
                //            NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //            noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //            noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                //            noiDungChinhSuaTKForm.ShowDialog();
                //        }
                //        else
                //            return;
                //    }
                //    else
                //    {
                //        int count = 0;
                //        NoiDungChinhSuaTKDetailForm ndDieuChinhTKChiTietForm = new NoiDungChinhSuaTKDetailForm();
                //        string where = "TKMD_ID = " + TKMD.ID + " AND MALOAIHINH = '" + TKMD.MaLoaiHinh.Trim() + "' ";
                //        ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
                //        foreach (NoiDungDieuChinhTK nddctk in ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK)
                //        {
                //            if (nddctk.TrangThai != 1)
                //                ++count;
                //        }
                //        if (count == 0)
                //        {
                //            if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //            {
                //                NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //                noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //                noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                //                noiDungChinhSuaTKForm.ShowDialog();
                //            }
                //            else
                //                return;
                //        }
                //    }
                //    #endregion
                //}
                //else
                //{
                #endregion commnet

                //DATLMQ bổ sung phần tự động lưu dữ liệu vào TK sửa đổi bổ sung khi có thay đổi 19/05/2011
                try
                {
                    ToKhaiMauDich TKMD_SuaDoi = new ToKhaiMauDich();
                    NoiDungToKhai noiDungTK = new NoiDungToKhai();
                    List<NoiDungToKhai> listNoiDungTK = new List<NoiDungToKhai>();
                    NoiDungDieuChinhTKDetail noiDungEditDetail = new NoiDungDieuChinhTKDetail();
                    NoiDungDieuChinhTK noiDungEdit = new NoiDungDieuChinhTK();
                    noiDungEdit.TKMD_ID = TKMD.ID;
                    noiDungEdit.SoTK = TKMD.SoToKhai;
                    noiDungEdit.NgayDK = TKMD.NgayDangKy;
                    noiDungEdit.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungEdit.NgaySua = DateTime.Now;
                    i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
                    if (i == 0)
                    {
                        i++;
                        noiDungEdit.SoDieuChinh = i;
                        noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        noiDungEdit.InsertUpdate();
                        luu = true;
                    }
                    else
                    {
                        if (luu == false)
                        {
                            i++;
                            noiDungEdit.SoDieuChinh = i;
                            noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            //if (isEdited == true)
                            noiDungEdit.InsertUpdate();
                            luu = true;
                        }
                    }

                    #region Lưu tạm giá trị của TK cũ vào các biến tương ứng
                    //Người nhập khẩu & Người xuất khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nguoiNhapKhau = TKMD.TenDoanhNghiep;
                        nguoiXuatKhau = TKMD.TenDonViDoiTac;
                    }
                    else
                    {
                        nguoiNhapKhau = TKMD.TenDonViDoiTac;
                        nguoiXuatKhau = TKMD.TenDoanhNghiep;
                    }
                    //Người ủy thác
                    nguoiUyThac = TKMD.TenDonViUT;
                    //PTVT
                    PTVT = TKMD.PTVT_ID;
                    tenPTVT = TKMD.SoHieuPTVT;
                    ngayDenPTVT = TKMD.NgayDenPTVT;
                    //Nước xuất khẩu & Nước nhập khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nuocXuatKhau = TKMD.NuocXK_ID;
                    }
                    else
                    {
                        nuocNhapKhau = TKMD.NuocNK_ID;
                    }
                    //Điều kiện giao hàng
                    dieuKienGiaoHang = TKMD.DKGH_ID;
                    //Phương thức thanh toán
                    phuongThucThanhToan = TKMD.PTTT_ID;
                    //Giấy phép
                    soGiayPhep = TKMD.SoGiayPhep;
                    ngayGiayPhep = TKMD.NgayGiayPhep;
                    ngayHetHanGiayPhep = TKMD.NgayHetHanGiayPhep;
                    //Hóa đơn thương mại
                    hoaDonTM = TKMD.SoHoaDonThuongMai;
                    ngayHoaDonTM = TKMD.NgayHoaDonThuongMai;
                    //Địa điểm dở hàng & Địa điểm xếp hàng
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        diaDiemDoHang = TKMD.CuaKhau_ID;
                        diaDiemXepHang = TKMD.DiaDiemXepHang;
                    }
                    else
                    {
                        diaDiemXepHang = TKMD.CuaKhau_ID;
                        diaDiemDoHang = TKMD.DiaDiemXepHang;
                    }
                    //Nguyên tệ
                    nguyenTe = TKMD.NguyenTe_ID;
                    //Tỷ giá tính thuế
                    tyGiaTinhThue = TKMD.TyGiaTinhThue;
                    //Tỷ giá USD
                    tyGiaUSD = TKMD.TyGiaUSD;
                    //Hợp đồng
                    soHopDong = TKMD.SoHopDong;
                    ngayHopDong = TKMD.NgayHopDong;
                    ngayHetHanHopDong = TKMD.NgayHetHanHopDong;
                    //Vận tải đơn
                    soVanDon = TKMD.SoVanDon;
                    ngayVanDon = TKMD.NgayVanDon;
                    //Tổng trọng lượng: số Cont20, số Cont40, số kiện hàng
                    soContainer20 = TKMD.SoContainer20;
                    soContainer40 = TKMD.SoContainer40;
                    soKienHang = TKMD.SoKien;
                    //Trọng lượng
                    trongLuong = TKMD.TrongLuong;
                    //Trọng lượng tịnh
                    trongLuongNet = TKMD.TrongLuongNet;
                    //Lệ phí HQ
                    lePhiHaiQuan = TKMD.LePhiHaiQuan;
                    //Phí vận chuyện
                    phiVanChuyen = TKMD.PhiVanChuyen;
                    //Phí bảo hiểm
                    phiBaoHiem = TKMD.PhiBaoHiem;
                    //Phí khác
                    phiKhac = TKMD.PhiKhac;
                    #endregion

                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        #region 1. Người xuất khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbNguoiXK.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbNguoiXK.Tag.ToString();
                                noiDungTK.Ten = "1. Người xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Hóa đơn Thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "6. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "7. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "8. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Vận tải đơn
                        //Số vận tải đơn
                        TKMD_SuaDoi.SoVanDon = txtSoVanTaiDon.Text;
                        if (TKMD_SuaDoi.SoVanDon != TKMD.SoVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label19.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label19.Tag.ToString();
                                noiDungTK.Ten = "9. Vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soVanDon;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soVanDon;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày vận tải đơn
                        if (ccNgayVanTaiDon.Text.Equals(""))
                            TKMD_SuaDoi.NgayVanDon = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayVanDon = Convert.ToDateTime(ccNgayVanTaiDon.Text);
                        if (TKMD_SuaDoi.NgayVanDon != TKMD.NgayVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label20.Tag.ToString();
                                noiDungTK.Ten = "9. Ngày vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayVanDon.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayVanDon.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Cảng xếp hàng
                        TKMD_SuaDoi.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemXepHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemXepHang.Tag.ToString();
                                noiDungTK.Ten = "10. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Cảng dở hàng
                        TKMD_SuaDoi.CuaKhau_ID = ctrCuaKhau.Ma.Trim();
                        if (TKMD_SuaDoi.CuaKhau_ID != TKMD.CuaKhau_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemDoHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemDoHang.Tag.ToString();
                                noiDungTK.Ten = "11. Cảng dở hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemDoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemDoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương tiện vận tải
                        //Loại Phương tiện vận tải
                        TKMD_SuaDoi.PTVT_ID = cbPTVT.SelectedValue.ToString();
                        if (TKMD_SuaDoi.PTVT_ID != TKMD.PTVT_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label17.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label17.Tag.ToString();
                                noiDungTK.Ten = "12. Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + PTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + PTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Số hiệu Phương tiện vận tải
                        TKMD_SuaDoi.SoHieuPTVT = txtSoHieuPTVT.Text;
                        if (TKMD_SuaDoi.SoHieuPTVT != TKMD.SoHieuPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblSoHieuPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblSoHieuPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Số hiệu Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHieuPTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHieuPTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày đến Phương tiện vận tải
                        if (ccNgayDen.Text.Equals(""))
                            TKMD_SuaDoi.NgayDenPTVT = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayDenPTVT = Convert.ToDateTime(ccNgayDen.Text);
                        if (TKMD_SuaDoi.NgayDenPTVT != TKMD.NgayDenPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblNgayDenPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblNgayDenPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Ngày đến Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayDenPTVT.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Nước xuất khẩu
                        TKMD_SuaDoi.NuocXK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocXK_ID != TKMD.NuocXK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNuocXuatKhau.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNuocXuatKhau.Tag.ToString();
                                noiDungTK.Ten = "13. Nước xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "14. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 15. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "15. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 16. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma.Trim();
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "16. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 17. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "17. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region 2. Người nhập khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NguoiNhapKhau" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NguoiNhapKhau";
                                noiDungTK.Ten = "2. Người nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "6. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "7. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hóa đơn thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "8. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Cảng xếp hàng
                        TKMD_SuaDoi.DiaDiemXepHang = Company.BLL.DuLieuChuan.CuaKhau.GetName(ctrCuaKhau.Ma).Trim();
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "CangXepHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "CangXepHang";
                                noiDungTK.Ten = "9. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Nước nhập khẩu
                        TKMD_SuaDoi.NuocNK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocNK_ID != TKMD.NuocNK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocNK" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NuocNK";
                                noiDungTK.Ten = "10. Nước nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "11. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "12. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma.Trim();
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "13. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "14. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }

                    //hangMDTK = TKMD.HMDCollection[soDongHang];
                    bool ok = AutoInsertHMDToTKEdit(HangMauDichEditForm.maHS_Old, HangMauDichEditForm.tenHang_Old, HangMauDichEditForm.maHang_Old, HangMauDichEditForm.xuatXu_Old,
                                          HangMauDichEditForm.soLuong_Old, HangMauDichEditForm.dvt_Old, HangMauDichEditForm.donGiaNT_Old, HangMauDichEditForm.triGiaNT_Old, TKMD);
                    //DATLMQ bổ sung tự động Lưu thông tin Xóa và Thêm Hàng hóa ngày 12/08/2011
                    AutoInsertDeleteHMD();
                    AutoInsertAddHMD();

                    if (!ok)
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình lưu tờ khai sửa: " + ex.Message, false);
                    return;
                }
                //}
            }
            #endregion To khai sua

            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                //cvError.ContainerToValidate = this.txtTenDonViDoiTac;
                //cvError.Validate();
                //bool valid1 = cvError.IsValid;
                bool valid1 = true;//temp
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }

            if (valid)
            {
                if (this.TKMD.HMDCollection.Count == 0)
                {
                    // Message("MSG_SAV01", "", false);
                    //  ShowMessage("Chưa nhập thông tin hàng của tờ khai", false);
                    MLMessages("Chưa nhập thông tin hàng của tờ khai", "MSG_SAV01", "", false);
                    return;
                }

                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, "Tỷ giá tính thuế không hợp lệ.");
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                this.TKMD.SoTiepNhan = this.TKMD.SoTiepNhan;
                this.TKMD.SoToKhai = this.TKMD.SoToKhai;
                //this.TKMD.ActionStatus = 0;

                //Set chổ này lưu lung tung dữ liệu
                //LanNT Comment
                #region Khong cho phep cap nhat trang thai
                //if (!isKhaibaoSua)
                //{
                //    if (this.TKMD.SoTiepNhan > 0)
                //    {
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                //        this.TKMD.ActionStatus = 0;
                //    }
                //    else
                //    {
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                //        this.TKMD.ActionStatus = -1;
                //        this.TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                //    }

                //    if (this.TKMD.SoToKhai > 0)
                //        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                //    else
                //    {
                //        this.TKMD.PhanLuong = "";
                //        this.TKMD.HUONGDAN = "";
                //        this.TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                //    }
                //}
                #endregion
                // Đề xuất khác
                this.TKMD.DeXuatKhac = txtDeXuatKhac.Text;
                //Lý do sửa
                if (isKhaibaoSua)
                    this.TKMD.LyDoSua = txtLyDoSua.Text;

                this.TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Loại hình mậu dịch.
                this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    if (!ccNgayGiayPhep.IsNullDate)
                        this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    else
                        this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    if (!ccNgayHHGiayPhep.IsNullDate)
                        this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                    else
                        this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }
                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                if (ccNgayDen.IsNullDate)
                    this.TKMD.NgayDenPTVT = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }
                // Nước.
                if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                this.TKMD.TongTriGiaKhaiBao = 0;
                //
                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);


                this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");
                if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
                if (GlobalSettings.TuDongTinhThue == "1")
                    this.tinhLaiThue();
                else
                    this.tinhTongTriGiaKhaiBao();

                this.TKMD.GiayTo = txtChungTu.Text;

                try
                {

                    if (this.TKMD.ID == 0)
                        this.TKMD.GUIDSTR = Guid.NewGuid().ToString();
                    this.TKMD.InsertUpdateFull();

                    //Neu la chinh sua, cap nhat lai ID luu tam la ID cua TKMD dang mo: tranh bi copy trung du lieu cua TK cu.
                    if (_bNew == false)
                        pTKMD_ID = TKMD.ID;

                    //copy :
                    //Lypt update date 22/01/2010
                    if (_bNew)
                    {
                        //Cap nhat lai trang thai sau khi them moi
                        _bNew = false;

                        //Copy Van don                        
                        List<VanDon> VanDoncollection = new List<VanDon>();
                        VanDoncollection = VanDon.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (VanDon vd in VanDoncollection)
                        {
                            long vdResult = VanDon.InsertVanDon(vd.SoVanDon, vd.NgayVanDon, vd.HangRoi, vd.SoHieuPTVT, vd.NgayDenPTVT, vd.MaHangVT, vd.TenHangVT, vd.TenPTVT, vd.QuocTichPTVT, vd.NuocXuat_ID, vd.MaNguoiNhanHang,
                                vd.TenNguoiNhanHang, vd.MaNguoiGiaoHang, vd.TenNguoiNhanHang, vd.CuaKhauNhap_ID, vd.CuaKhauXuat, vd.MaNguoiNhanHangTrungGian, vd.TenNguoiNhanHangTrungGian, vd.MaCangXepHang, vd.TenCangXepHang,
                                vd.MaCangDoHang, vd.TenCangDoHang, this.TKMD.ID, vd.DKGH_ID, vd.DiaDiemGiaoHang, vd.NoiDi, vd.SoHieuChuyenDi, vd.NgayKhoiHanh);
                            List<Container> ContColl = Company.KDT.SHARE.QuanLyChungTu.Container.SelectCollectionBy_VanDon_ID(vd.ID);
                            for (int v = 0; v < ContColl.Count; v++)
                            {
                                ContColl[v].VanDon_ID = vdResult;
                            }
                            Company.KDT.SHARE.QuanLyChungTu.Container.InsertCollection(ContColl);
                        }
                        //copy Hop dong
                        List<HopDongThuongMai> HopDongCollection = new List<HopDongThuongMai>();
                        HopDongCollection = HopDongThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (HopDongThuongMai hd in HopDongCollection)
                        {
                            //Copy hop dong
                            long hdResult = HopDongThuongMai.InsertHopDongThuongMai(hd.SoHopDongTM, hd.NgayHopDongTM, hd.ThoiHanThanhToan, hd.NguyenTe_ID, hd.PTTT_ID, hd.DKGH_ID, hd.DiaDiemGiaoHang, hd.MaDonViMua, hd.TenDonViMua,
                                hd.MaDonViBan, hd.TenDonViBan, hd.TongTriGia, hd.ThongTinKhac, this.TKMD.ID, hd.GuidStr, hd.LoaiKB, hd.SoTiepNhan, hd.NgayTiepNhan, hd.TrangThai, hd.NamTiepNhan, hd.MaDoanhNghiep);

                            //---Hang hop dong   
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hop dong
                            List<HopDongThuongMaiDetail> HopDongDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hd.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HopDongThuongMaiDetail> HopDongDTMColl = new List<HopDongThuongMaiDetail>();

                            for (int l = 0; l < HopDongDetailUpdateCollection.Count; l++)
                            {
                                HopDongThuongMaiDetail hopDongCopy = HopDongDetailUpdateCollection[l];

                                if (hopDongCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hopDongCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HopDongThuongMaiDetail hdtmDetail = new HopDongThuongMaiDetail();
                                    hdtmDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdtmDetail.HopDongTM_ID = hdResult;

                                    hdtmDetail.SoThuTuHang = hopDongCopy.SoThuTuHang;
                                    hdtmDetail.MaHS = hopDongCopy.MaHS;
                                    hdtmDetail.MaPhu = hopDongCopy.MaPhu;
                                    hdtmDetail.TenHang = hopDongCopy.TenHang;
                                    hdtmDetail.NuocXX_ID = hopDongCopy.NuocXX_ID;
                                    hdtmDetail.DVT_ID = hopDongCopy.DVT_ID;
                                    hdtmDetail.SoLuong = hopDongCopy.SoLuong;
                                    hdtmDetail.DonGiaKB = hopDongCopy.DonGiaKB;
                                    hdtmDetail.TriGiaKB = hopDongCopy.TriGiaKB;

                                    HopDongDTMColl.Add(hdtmDetail);
                                }
                            }

                            HopDongThuongMaiDetail.InsertCollection(HopDongDTMColl);
                        }

                        //copy Giay phep
                        List<GiayPhep> GPColl = new List<GiayPhep>();
                        GPColl = GiayPhep.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (GiayPhep gp in GPColl)
                        {
                            long gpResult = GiayPhep.InsertGiayPhep(gp.SoGiayPhep, gp.NgayGiayPhep, gp.NgayHetHan, gp.NguoiCap, gp.NoiCap, gp.MaDonViDuocCap, gp.TenDonViDuocCap, gp.MaCoQuanCap, gp.TenQuanCap, gp.ThongTinKhac, gp.MaDoanhNghiep,
                                this.TKMD.ID, gp.GuidStr, gp.LoaiKB, gp.SoTiepNhan, gp.NgayTiepNhan, gp.TrangThai, gp.NamTiepNhan);

                            //--Hang Giay phep
                            //Lay hang mau dich co cap nhat thong tin rieng cua Giay phep
                            List<HangGiayPhepDetail> GiayPhepDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HangGiayPhepDetail> HangGP = new List<HangGiayPhepDetail>();

                            for (int k = 0; k < GiayPhepDetailUpdateCollection.Count; k++)
                            {
                                HangGiayPhepDetail giayPhepCopy = GiayPhepDetailUpdateCollection[k];

                                if (giayPhepCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, giayPhepCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HangGiayPhepDetail gpDetail = new HangGiayPhepDetail();
                                    gpDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    gpDetail.GiayPhep_ID = gpResult;

                                    gpDetail.SoThuTuHang = giayPhepCopy.SoThuTuHang;
                                    gpDetail.MaHS = giayPhepCopy.MaHS;
                                    gpDetail.MaPhu = giayPhepCopy.MaPhu;
                                    gpDetail.TenHang = giayPhepCopy.TenHang;
                                    gpDetail.NuocXX_ID = giayPhepCopy.NuocXX_ID;
                                    gpDetail.DVT_ID = giayPhepCopy.DVT_ID;
                                    gpDetail.SoLuong = giayPhepCopy.SoLuong;
                                    gpDetail.DonGiaKB = giayPhepCopy.DonGiaKB;
                                    gpDetail.TriGiaKB = giayPhepCopy.TriGiaKB;

                                    gpDetail.MaChuyenNganh = giayPhepCopy.MaChuyenNganh;
                                    gpDetail.MaNguyenTe = giayPhepCopy.MaNguyenTe;

                                    HangGP.Add(gpDetail);
                                }
                            }
                            HangGiayPhepDetail.InsertCollection(HangGP);
                        }

                        // copy Hoa don thuong mai
                        List<HoaDonThuongMai> HDTMColl = new List<HoaDonThuongMai>();
                        HDTMColl = HoaDonThuongMai.SelectCollectionBy_TKMD_ID(pTKMD_ID);

                        foreach (HoaDonThuongMai hdtm in HDTMColl)
                        {
                            long hdonResult = HoaDonThuongMai.InsertHoaDonThuongMai(hdtm.SoHoaDon, hdtm.NgayHoaDon, hdtm.NguyenTe_ID, hdtm.PTTT_ID, hdtm.DKGH_ID, hdtm.MaDonViMua, hdtm.TenDonViMua, hdtm.MaDonViBan, hdtm.TenDonViBan,
                                hdtm.ThongTinKhac, this.TKMD.ID, hdtm.GuidStr, hdtm.LoaiKB, hdtm.SoTiepNhan, hdtm.NgayTiepNhan, hdtm.TrangThai, hdtm.NamTiepNhan, hdtm.MaDoanhNghiep);

                            //--Hang hoa don thuong mai
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hoa don
                            List<HoaDonThuongMaiDetail> HoaDonDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hdtm.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HoaDonThuongMaiDetail> HoaDonTMColl = new List<HoaDonThuongMaiDetail>();

                            for (int l = 0; l < HoaDonDetailUpdateCollection.Count; l++)
                            {
                                HoaDonThuongMaiDetail hoaDonCopy = HoaDonDetailUpdateCollection[l];

                                if (hoaDonCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hoaDonCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HoaDonThuongMaiDetail hdonDetail = new HoaDonThuongMaiDetail();
                                    hdonDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdonDetail.HoaDonTM_ID = hdonResult;

                                    hdonDetail.SoThuTuHang = hoaDonCopy.SoThuTuHang;
                                    hdonDetail.MaHS = hoaDonCopy.MaHS;
                                    hdonDetail.MaPhu = hoaDonCopy.MaPhu;
                                    hdonDetail.TenHang = hoaDonCopy.TenHang;
                                    hdonDetail.NuocXX_ID = hoaDonCopy.NuocXX_ID;
                                    hdonDetail.DVT_ID = hoaDonCopy.DVT_ID;
                                    hdonDetail.SoLuong = hoaDonCopy.SoLuong;
                                    hdonDetail.DonGiaKB = hoaDonCopy.DonGiaKB;
                                    hdonDetail.TriGiaKB = hoaDonCopy.TriGiaKB;

                                    HoaDonTMColl.Add(hdonDetail);
                                }
                            }
                            HoaDonThuongMaiDetail.InsertCollection(HoaDonTMColl);
                        }                        // copy CO
                        List<CO> COColl = new List<CO>();
                        COColl = Company.KDT.SHARE.QuanLyChungTu.CO.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (CO co in COColl)
                        {
                            Company.KDT.SHARE.QuanLyChungTu.CO.InsertCO(co.SoCO, co.NgayCO, co.ToChucCap, co.NuocCapCO, co.MaNuocXKTrenCO, co.MaNuocNKTrenCO, co.TenDiaChiNguoiXK, co.TenDiaChiNguoiNK, co.LoaiCO,
                                co.ThongTinMoTaChiTiet, co.MaDoanhNghiep, co.NguoiKy, co.NoCo, co.ThoiHanNop, this.TKMD.ID, co.GuidStr, co.LoaiKB, co.SoTiepNhan, co.NgayTiepNhan, co.TrangThai, co.NamTiepNhan);
                        }
                        //Copy De nghi chuyen cua khau
                        List<DeNghiChuyenCuaKhau> chuyenCK = new List<DeNghiChuyenCuaKhau>();
                        chuyenCK = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(pTKMD_ID);
                        foreach (DeNghiChuyenCuaKhau ck in chuyenCK)
                        {
                            DeNghiChuyenCuaKhau.InsertDeNghiChuyenCuaKhau(ck.ThongTinKhac, ck.MaDoanhNghiep, this.TKMD.ID, ck.GuidStr, ck.LoaiKB, ck.SoTiepNhan, ck.NgayTiepNhan, ck.TrangThai,
                                ck.NamTiepNhan, ck.SoVanDon, ck.NgayVanDon, ck.ThoiGianDen, ck.DiaDiemKiemTra, ck.TuyenDuong);
                        }

                        //Copy Chung tu kem
                        List<ChungTuKem> chungTuKem = new List<ChungTuKem>();
                        chungTuKem = ChungTuKem.SelectCollectionBy_TKMDID(pTKMD_ID);
                        long ctkID = 0;
                        foreach (ChungTuKem ctk in chungTuKem)
                        {
                            ctkID = ChungTuKem.InsertChungTuKem(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.DIENGIAI, ctk.LoaiKB, ctk.TrangThaiXuLy, ctk.Tempt, ctk.MessageID, ctk.GUIDSTR, ctk.KDT_WAITING, ctk.KDT_LASTINFO, ctk.SOTN, ctk.NGAYTN, ctk.TotalSize, ctk.Phanluong, ctk.HuongDan, this.TKMD.ID);

                            ctk.LoadListCTChiTiet();
                            foreach (ChungTuKemChiTiet chiTiet in ctk.listCTChiTiet)
                            {
                                ChungTuKemChiTiet.InsertChungTuKemChiTiet(ctkID, chiTiet.FileName, chiTiet.FileSize, chiTiet.NoiDung);
                            }
                        }

                        if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                            this.TKMD.LoadHMDCollection();
                        if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                            this.TKMD.LoadChungTuTKCollection();
                        this.TKMD.LoadChungTuHaiQuan();
                        if (this.TKMD.ListCO == null || this.TKMD.ListCO.Count == 0)
                            this.TKMD.LoadCO();


                    }
                    //Het Lypt Update

                    this.TKMD.LoadChungTuTKCollection();
                    this.TKMD.LoadHMDCollection();
                    dgList.DataSource = this.TKMD.HMDCollection;
                    dgList.Refetch();
                    gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                            log.ID_DK = TKMD.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                        try
                        {
                            string where = "1 = 1";
                            where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                            List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                            if (listLog.Count > 0)
                            {
                                long idLog = listLog[0].IDLog;
                                string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                long idDK = listLog[0].ID_DK;
                                string guidstr = listLog[0].GUIDSTR_DK;
                                string userKhaiBao = listLog[0].UserNameKhaiBao;
                                DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                string userSuaDoi = GlobalSettings.UserLog;
                                DateTime ngaySuaDoi = DateTime.Now;
                                string ghiChu = listLog[0].GhiChu;
                                bool isDelete = listLog[0].IsDelete;
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                            userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                            }
                            else
                            {
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                                log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                                log.ID_DK = TKMD.ID;
                                log.GUIDSTR_DK = "";
                                log.UserNameKhaiBao = GlobalSettings.UserLog;
                                log.NgayKhaiBao = DateTime.Now;
                                log.UserNameSuaDoi = GlobalSettings.UserLog;
                                log.NgaySuaDoi = DateTime.Now;
                                log.GhiChu = "";
                                log.IsDelete = false;
                                log.Insert();
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                            return;
                        }
                    }
                    #endregion

                    // ShowMessage("Lưu tờ khai thành công.", false);
                    MLMessages("Lưu tờ khai thành công!", "MSG_SAV02", "", false);
                    setCommandStatus();
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }
            }
        }

        private void tinhLaiThue()
        {
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Value);

            decimal tongSoLuongHang = 0;

            if (this.TKMD.HMDCollection.Count == 0)
            {
                lblTongTGNT.Text = string.Format("{0} ({1})", "0", ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", "0");
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));

                return;
            }

            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
            decimal TriGiaTTMotDong = 0;
            TriGiaTTMotDong = Phi == 0 ? TKMD.TyGiaTinhThue : (1 + Phi / TongTriGiaHang) * TKMD.TyGiaTinhThue;
            //if (Phi == 0)
            //{
            //    TriGiaTTMotDong = Convert.ToDecimal(TKMD.HeSoNhan) * TKMD.TyGiaTinhThue;
            //}
            decimal tongThue = 0;
            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tongSoLuongHang += hmd.SoLuong;
                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = hmd.DonGiaTT * hmd.SoLuong;
                hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                tongThue += hmd.ThueXNK + hmd.PhuThu;
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGNT.Text = string.Format("{0} ({1})", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));
            }
            else
            {
                //lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1}). Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma, tongThue.ToString("N"));
            }
        }
        private decimal tinhTongTriGiaKhaiBao()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            decimal tongThue = 0;
            decimal tongSoLuongHang = 0;

            foreach (Company.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tongSoLuongHang += hmd.SoLuong;

                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
                tongThue += hmd.ThueXNK + hmd.PhuThu;
            }
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
                lblTongTGNT.Text = string.Format("{0} ({1})", TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT)), ctrNguyenTe.Ma);
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1})         Tổng thuế : {2} (VND)", TKMD.TongTriGiaKhaiBao.ToString("N"), ctrNguyenTe.Ma, tongThue.ToString("N"));
            }

            return tongThue;
        }
        //-----------------------------------------------------------------------------------------

        private void inToKhai()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        private void inToKhaiTT15()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNTQDTFormTT15 f = new ReportViewTKNTQDTFormTT15();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXTQDTFormTT15 f1 = new ReportViewTKXTQDTFormTT15();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        private void inToKhaiA4()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        /// <summary>
        /// In tờ khai Thông Quan Điện tử
        /// </summary>
        private void inToKhaiTQDT()
        {
            TKMD.Load();
            TKMD.LoadHMDCollection();
            TKMD.LoadChungTuTKCollection();

            if (this.TKMD.HMDCollection.Count == 0)
                return;
            if (this.TKMD.ID == 0)
            {
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                    f.TKMD = this.TKMD;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXTQDTForm f1 = new ReportViewTKXTQDTForm();
                    f1.TKMD = this.TKMD;
                    f1.Show();
                    break;
            }

        }
        private void setCommandStatus()
        {
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = "Chưa khai báo";
                    lblPhanLuong.Text = "Chưa phân luồng";
                }
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = txtSoToKhai.Text = "";

                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.PhanLuong == "1")
                {
                    lblPhanLuong.Text = "Luồng Xanh";
                }
                else if (TKMD.PhanLuong == "2")
                {
                    lblPhanLuong.Text = "Luồng Vàng";
                }
                else if (TKMD.PhanLuong == "3")
                {
                    lblPhanLuong.Text = "Luồng Đỏ";
                }
                else
                {
                    lblPhanLuong.Text = "Chưa phân luồng";
                }

                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                //Hungtq, Update 20/07/2010
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (TKMD.PhanLuong != "")
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }

                // lblTrangThai.Text = "Đã duyệt";     
                if (TKMD.PhanLuong == "1")
                {
                    lblPhanLuong.Text = "Luồng Xanh";
                }
                else if (TKMD.PhanLuong == "2")
                {
                    lblPhanLuong.Text = "Luồng Vàng";
                }
                else if (TKMD.PhanLuong == "3")
                {
                    lblPhanLuong.Text = "Luồng Đỏ";
                }
                else
                {
                    lblPhanLuong.Text = "Chưa phân luồng";
                }

                lblTrangThai.Text = "Đã duyệt";

                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";

                if (TKMD.PhanLuong != "")
                {
                    if (TKMD.PhanLuong == "1")
                        lblPhanLuong.Text = "Luồng xanh";
                    else if (TKMD.PhanLuong == "2")
                        lblPhanLuong.Text = "Luồng vàng";
                    else if (TKMD.PhanLuong == "3")
                        lblPhanLuong.Text = "Luồng đỏ";
                }
                else
                    lblPhanLuong.Text = "Chưa phân luồng";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }

            SetCommandStatusSuaHuyToKhai();

            //Hungtq update 04/01/2012. An Van don, Hoa don dong nhat voi to khai chinh.
            //Hungtq update 07/02/2012. Hien thi Van don, Hoa don cho tơ khai xuat.
            //if (this.NhomLoaiHinh.Substring(0, 1) == "X")
            //{
            //    cmdVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    //cmdHoaDonThuongMai.Enabled = Janus.Windows.UI.InheritableBoolean.False;

            //    cmdVanDonBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    //cmdHoaDonThuongMaiBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}
        }

        private void KetQuaXuLy()
        {
            Globals.KetQuaXuLy(this.TKMD);
        }

        private void InContainer()
        {
            if (TKMD.ID == 0)
                return;

            Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }

        #region SUA - HUY TO KHAI

        private void SetCommandStatusSuaHuyToKhai()
        {
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUYTKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Đã hủy";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //Nếu tờ khai chờ duyệt có số tờ khai thì không cho phép hủy khai báo
                if (TKMD.SoToKhai > 0)
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }

            //Hungtq updated 24/02/2012.
            #region Cho phép xem thông tin Chứng từ kèm khi Tờ khai ở trạng thái: Chờ duyệt, Đã duyệt, Sửa tờ khai, Hủy tờ khai, Không phê duyệt, Chờ hủy tờ khai, Hủy tờ khai.

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                )
            {
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            #endregion
        }

        private void HuyToKhaiDaDuyet()
        {
            //TKMD.TrangThaiXuLy = 1;
            //TKMD.ActionStatus = 1;
            //TKMD.Update();
            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + this.TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + this.TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = this.TKMD;
                    f.ShowDialog();

                    //datlmq update 24/07/2010; Bổ sung History Kết quả xử lý
                    Company.KD.BLL.KDT.KetQuaXuLy kqxl = new KD.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.TKMD.ID;
                    kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Hủy tờ khai";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                    kqxl.NoiDung = "Khai báo hủy";//string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", TKMD.SoTiepNhan, TKMD.NgayTiepNhan.ToShortDateString()); ;
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = this.TKMD;
                f.ShowDialog();
            }
        }

        #region Bổ sung Khai báo sửa tờ khai
        private void NhanPhanHoiSuaTK()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i < 3; i++)
                {
                    LayPhanHoiSuaTK(password);
                }
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiSuaTK(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    //LayPhanHoiSuaTK(pass);
                    return;
                }

                //}
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + Company.KD.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KD.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void SuaToKhaiDaDuyet()
        {
            //TODO: Hungtq noted 30/03/2011.
            /*
             * Kiểm tra tờ khai có tham gia hồ sơ thanh khoản nào không?. Hồ sơ thanh khoản đã đóng hay chưa đóng hồ sơ?.
             */
            try
            {
                if (this.TKMD.IsToKhaiDaThanhKhoanVaDongHoSo())
                {
                    ShowMessage(string.Format("Tờ khai: '{0}'\nMã loại hình: {1}\nNgày đăng ký: {2} \nđã tham gia thanh khoản và hồ sơ đã đóng, không thể sửa tờ khai này được.", this.TKMD.SoToKhai.ToString(), TKMD.MaLoaiHinh, TKMD.NgayDangKy), false);
                    return;
                }
                else if (this.TKMD.IsToKhaiDangThanhKhoan())
                {
                    ShowMessage(string.Format("Tờ khai: '{0}'\nMã loại hình: {1}\nNgày đăng ký: {2} \nđã tham gia thanh khoản và đang chạy hồ sơ, không thể sửa tờ khai này được.\nĐể có thể sửa tờ khai này được, hãy xóa tờ khai này trong bảng kê tờ khai của hồ sơ thanh khoản.", this.TKMD.SoToKhai.ToString(), TKMD.MaLoaiHinh, TKMD.NgayDangKy), false);
                    return;
                }

                //DATLMQ bổ sung Request hết thông tin trả về từ Hải quan trước khi chuyển sang trạng thái Sửa TK 24/05/2011
                if ((!TKMD.GUIDSTR.Equals("")) && TKMD.SoTiepNhan > 0)
                    NhanPhanHoiSuaTK();

                string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
                msg += "\n\nSố tờ khai: " + this.TKMD.SoToKhai.ToString();
                msg += "\n----------------------";
                msg += "\nCó " + this.TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    //TODO: Hungtq updated 02/03/2012. Truoc khi Chuyen trang thai Sua to khai, xoa MessageSend da luu (Neu co).
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = TKMD.ID;
                    sendXML.Load();

                    if (sendXML != null && sendXML.msg != "")
                        sendXML.Delete();

                    long id = this.TKMD.ID;
                    this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                    //Cap nhat trang thai cho to khai
                    this.TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;

                    this.TKMD.Update();

                    //Cap nhat trang thai sua to khai
                    Company.KD.BLL.KDT.KetQuaXuLy kqxl = new KD.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.TKMD.ID;
                    kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Chuyển trạng thái sửa tờ khai"; // Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                    kqxl.NoiDung = "";
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                    if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                        msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                    Company.KDT.SHARE.Components.Globals.SaveMessage(
                        string.Empty, TKMD.ID, TKMD.GUIDSTR,
                         msgType, Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                          Company.KDT.SHARE.Components.MessageTitle.SuaToKhai);

                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    try
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = TKMD.GUIDSTR;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                        return;
                    }

                    SetCommandStatusSuaHuyToKhai();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi trong quá trình thực hiện chuyển trạng thái Sửa tờ khai.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        #endregion

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, Update 06/11/2011
                case "cmdInAnDinhThue":
                    InAnDinhThue(this.TKMD.ID, this.TKMD.GUIDSTR);
                    break;

                //Hungtq, Update 14072010
                case "cmdHuyToKhaiDaDuyet":
                    HuyToKhaiDaDuyet();
                    break;
                case "cmdSuaToKhaiDaDuyet":
                    SuaToKhaiDaDuyet();
                    break;
                case "cmdInBangKeKemTheo":
                    InContainer();
                    break;
                case "cmdKetQuaXuLy":
                    this.KetQuaXuLy();
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    //ShowMessage("Tổng trị giá khai báo là : " + this.TKMD.TongTriGiaKhaiBao, false);
                    MLMessages("Tổng trị giá khai báo là : " + this.TKMD.TongTriGiaKhaiBao, "MSG_ADD06", "" + this.TKMD.TongTriGiaKhaiBao, false);
                    break;
                case "cmdReadExcel":
                    this.ReadExcel();
                    break;
                case "cmdThemHang":
                    this.themHang();
                    break;
                case "ThemHang":
                    this.themHang();
                    break;
                case "InToKhai":
                    this.inToKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdInToKhaiTT15":
                    this.inToKhaiTT15();
                    break;
                case "cmdSend":
                    this.Send();
                    checkTrangThaiXuLyTK();
                    break;
                case "ChungTuKemTheo":
                    this.AddChungTu();
                    break;
                case "NhanDuLieu":// đang dùng hàm này để nhận duyệt/từ chối
                    this.NhanPhanLuong();
                    checkTrangThaiXuLyTK();
                    break;
                case "XacNhan":
                    this.NhanPhanLuong();
                    checkTrangThaiXuLyTK();
                    break;
                case "FileDinhKem":
                    this.GetFileDinhKem();
                    break;
                case "cmdPrint":
                    this.inToKhai();
                    break;
                case "cmdPrintA4":
                    this.inToKhaiA4();
                    break;
                case "cmdInToKhaiTQDT":
                    this.inToKhaiTQDT();
                    break;
                case "cmdInTKDTSuaDoiBoSung":
                    InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "cmdVanDon":
                    this.txtSoVanTaiDon_ButtonClick(null, null);
                    break;
                case "cmdHoaDonThuongMai":
                    this.txtSoHoaDonThuongMai_ButtonClick(null, null);
                    break;
                case "cmdCO":
                    this.QuanLyCO("");
                    break;
                case "cmdHopDong":
                    this.txtSoHopDong_ButtonClick(null, null);
                    break;
                case "cmdGiayPhep":
                    this.txtSoGiayPhep_ButtonClick(null, null);
                    break;
                case "cmdChuyenCuaKhau":
                    this.ShowChuyenCuaKhau("");
                    break;
                case "cmdVanDonBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoVanTaiDon_ButtonClick("1", null);
                    break;
                case "cmdHoaDonThuongMaiBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                    break;
                case "CO":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QuanLyCO("1");
                    break;
                case "cmdHopDongBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHopDong_ButtonClick("1", null);
                    break;
                case "cmdGiayPhepBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoGiayPhep_ButtonClick("1", null);
                    break;
                case "cmdChuyenCuaKhauBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.ShowChuyenCuaKhau("1");
                    break;

                case "cmdChungTuDangAnh":
                    AddChungTuAnh();
                    break;
                case "cmdChungTuDangAnhBS":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.AddChungTuAnh_BoSung();
                    break;

                case "cmdHuyKhaiBao":
                    HuyKhaiBao();
                    break;

            }
        }

        //datlmq bo sung ham KiemTraTrangThaiToKhai 20/09/2010
        private void checkTrangThaiXuLyTK()
        {
            try
            {
                NoiDungChinhSuaTKForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKForm();
                noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                //Nếu là tờ khai sửa đã được duyệt
                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
                //Nếu là tờ khai sửa đang chờ duyệt
                else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
                //Nếu là tờ khai sửa mới
                else
                {
                    foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                    {
                        if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                        {
                            ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            ndDieuChinhTK.Update();
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void InToKhaiDienTuSuaDoiBoSung()
        {
            bool ok = true;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    ShowMessage("Thông tin sửa đổi, bổ sung chưa được duyệt. Không thể in tờ khai sửa đổi bổ sung", false);
                    ok = false;
                }
            }
            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "Mau_so_8._Mau_To_khai_sua_doi_bo_sung.xls";
                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Workbook workBook = Workbook.Load(sourcePath);
                    Worksheet workSheet = workBook.Worksheets[0];

                    workSheet.Rows[5].Cells[6].Value = GlobalSettings.TEN_HAI_QUAN;
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Height = 10 * 20;
                    //KhanhHN - sửa nội dung in tờ khai sửa đổi bổ sung : Không in đơn vị đối tác
                    //if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                    //{
                        workSheet.Rows[7].Cells[6].Value = GlobalSettings.TEN_DON_VI;
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    //Mã số thuế đơn vị
                        workSheet.Rows[8].Cells[1].Value = GlobalSettings.MA_DON_VI;
                        workSheet.Rows[8].Cells[1].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[8].Cells[1].CellFormat.Font.Height = 10 * 20;
                    //}
                    //else
                    //{
                        //workSheet.Rows[7].Cells[6].Value = TKMD.TenDonViDoiTac;
                        //workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        //workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    //}

                    workSheet.Rows[9].Cells[3].Value = TKMD.SoToKhai;
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[9].Value = TKMD.MaLoaiHinh;
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[10].Cells[4].Value = TKMD.NgayTiepNhan;
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[15].Value = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Height = 10 * 20;

                    //Gán giá trị tương ứng vào các ô trong bảng nội dung sửa đổi
                    fillNoiDungTKChinh(workSheet, 13, 1);
                    fillNoiDungTKSua(workSheet, 13, 1);

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                    saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKMD.SoToKhai + "_NamDK" + TKMD.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                    return;
                }
            }
        }

        private void fillNoiDungTKChinh(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                        int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //    }
            }
        }

        private void fillNoiDungTKSua(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column + 11].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                        int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                            worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //    }
            }
        }

        #region Bổ sung hủy khai báo : DATLMQ 26/05/2011
        private void LayPhanHoiHuyKhaiBao(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + Company.KD.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                isTuChoi = true;
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KD.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void HuyKhaiBao()
        {
            ToKhaiMauDich tkmd = this.TKMD;
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();

            if (tkmd != null)
            {
                tkmd = this.TKMD;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

            this.Cursor = Cursors.WaitCursor;
            //DATLMQ bổ sung kiểm tra TK đã được HQ duyệt chưa 25/05/2011
            for (int i = 0; i < 2; i++)
            {
                LayPhanHoiHuyKhaiBao(password);
            }
            if (TKMD.SoToKhai > 0)
            {
                ShowMessage("Tờ khai đã được duyệt, không thể hủy khai báo", false);
                isTuChoi = false;
                return;
            }
            else if (isTuChoi)
            {
                ShowMessage("Tờ khai đã bị từ chối. Không thể hủy khai báo", false);
                return;
            }
            try
            {
                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    xmlCurrent = tkmd.WSCancelXMLNhap(password);
                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                    xmlCurrent = tkmd.WSCancelXMLXuat(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                sendXML.InsertUpdate();

                //xmlCurrent = tkmd.LayPhanHoiTQDTKhaiBao(password, xmlCurrent); // LINH CHUYỂN QUA DÙNG HÀM WSRequestPhanLuong()
                xmlCurrent = tkmd.WSRequestPhanLuong(password);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                string mess = "";
                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }

                    mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = tkmd.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = tkmd.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong khi hủy khai báo Tờ khai : " + msg[0], false, true, ex.ToString());
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định khi hủy khai báo Tờ khai.", false, true, ex.ToString());
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void AddChungTuAnh()
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }

            ListChungTuKemForm ctForm = new ListChungTuKemForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog();

        }

        private void AddChungTuAnh_BoSung()
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }

            ListChungTuKemForm f = new ListChungTuKemForm();
            f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        private void ShowChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListDeNghiChuyenCuaKhauTKMDForm f = new ListDeNghiChuyenCuaKhauTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        private void QuanLyCO(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListCOTKMDForm f = new ListCOTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();

        }
        private void inPhieuTN()
        {
            if (this.TKMD.SoTiepNhan == 0) return;
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "TỜ KHAI";
            phieuTN.soTN = this.TKMD.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = TKMD.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void GetFileDinhKem()
        {
            SendMailChungTuForm f = new SendMailChungTuForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        /*private void NhanDuLieuToKhai()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            string password = "";
            if (sendXML.Load())
            {
                // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.WSRequestXML(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK"; ;
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //  if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                                if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }*/

        //---------------------------
        private void NhanPhanLuong()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                //TODO: DoWork(30);
                LayPhanHoiPhanLuong(password);
                //TODO: DoExit();
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //---------------------------

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = OpenFormType.Insert;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();

        }

        private void Send()
        {
            if (TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin trước khi gửi!", "MSG_SEN14", "", false);
                return;
            }
            string password = "";
            this.Cursor = Cursors.Default;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.\nHãy chọn chức năng \"Nhận dữ liệu\" để lấy thông tin phản hồi.", "MSG_SEN03", "", false);

                //Hien thi nut Nhan du lieu
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                return;
            }
            int intHoaDon = TKMD.HoaDonThuongMaiCollection.Count;
            int intHopDong = TKMD.HopDongThuongMaiCollection.Count;
            int intGiayPhep = TKMD.GiayPhepCollection.Count;
            int intCO = TKMD.COCollection.Count;
            int intCTK = TKMD.ChungTuKemCollection.Count;
            int intVanDon = TKMD.VanTaiDon != null ? 1 : 0;
            int intDeNghiCCK = TKMD.listChuyenCuaKhau.Count;
            string thongBao = "Các chứng từ kèm theo tờ khai gồm: \n\t";
            thongBao += intCO + " CO \n\t";
            thongBao += intHoaDon + " Hóa đơn, ";
            thongBao += intHopDong + " Hợp đồng \n\t";
            thongBao += intGiayPhep + " Giấy phép, ";
            thongBao += intVanDon + " Vận đơn \n\t";
            thongBao += intDeNghiCCK + " Đề nghị chuyển cửa khẩu \n\t";
            thongBao += intCTK + " Chứng từ kèm dạng ảnh. \n";
            thongBao += "Bạn có muốn gởi thông tin tới Hải quan không?";
            string kq = "";// MLMessages(thongBao, "", "", true);
            kq = ShowMessage(thongBao, true); //MLMessages(thongBao, "", "", true);
            if (kq == "No")
            {
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                //TODO: DoWork(30);

                //if (MainForm.versionHD != 2)
                //{
                //    try
                //    {
                //        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                //        if (st != "")
                //        {
                //            ShowMessage(st, false);
                //            this.Cursor = Cursors.Default;
                //            return;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        //Logger.LocalLogger.Instance().WriteMessage(ex);
                //    }
                //}


                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    xmlCurrent = TKMD.WSKhaiBaoToKhaiSua(password, GlobalSettings.MaMID);
                }
                else
                {
                    if (TKMD.MaLoaiHinh.StartsWith("NSX"))
                    {
                        xmlCurrent = TKMD.WSSendXMLNHAP(password);
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("XSX"))
                    {
                        xmlCurrent = TKMD.WSSendXMLXuat(password, GlobalSettings.MaMID);
                    }
                }

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                dgList.AllowDelete = InheritableBoolean.False;

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công. Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    //hangdoi hd = new hangdoi();
                            //    //hd.id = tkmd.id;
                            //    //hd.loaitokhai = loaitokhai.to_khai_mau_dich;
                            //    //hd.trangthai = tkmd.trangthaixuly;
                            //    //hd.chucnang = chucnang.khai_bao;
                            //    //hd.password = password;
                            //    //mainform.addtoqueueform(hd);
                            //    //mainform.showqueueform();
                            //}
                            //ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                            //DATLMQ comment ngày 29/03/2011
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                        }
                        else
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Có lỗi khi khai báo Tờ khai : " + msg[0], false, true, ex.ToString());
                            ShowMessage("Có lỗi khi khai báo Tờ khai : " + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                                sendXML.Delete();
                        }
                    }
                    else
                    {
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString() + " " + ex.StackTrace);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();


            }
            finally
            {
                //TODO: DoExit();

                this.Cursor = Cursors.Default;
            }

        }
        private void LaySoTiepNhanDT()
        {
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_TK06", "", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    //xmlCurrent = TKMD.LayPhanHoi(password, sendXML.msg);
                    xmlCurrent = TKMD.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa nhận được phản hồi từ hệ thống hải quan", "MSG_STN02", "", true);
                    //if (kq == "Yes")
                    //{
                    //    this.Refresh();
                    //    LayPhanHoi(password);
                    //}
                    return;
                }
                if (sendXML.func == 1)
                {
                    sendMailChungTu();
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan, "MSG_SEN05", "" + TKMD.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait for approval";
                    }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    // ShowMessage("Hủy khai báo thành công", false);
                    MLMessages("Hủy khai báo thành công", "MSG_CNL02", "", false);
                    txtSoTiepNhan.Text = "";
                    //lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa khai báo";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not yet declarated";
                    }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        // this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }

                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                        //try
                        //{
                        //    TKMD.TinhToanCanDoiNhapXuat();
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Có lỗi khi cập nhật dữ liệu tồn vào hệ thống : " + ex.Message, false);
                        //}
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        // this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt. ", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                        }
                    }
                    else
                    {
                        // ShowMessage("Xảy ra lỗi không xác định.\nLỗi: " + ex.Message, false);                      
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendMailChungTu()
        {
            this.Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    //ShowMessage("Chưa cấu hình mail của doanh nghiệp", false);
                    MLMessages("Chưa cấu hình mail của doanh nghiệp", "MSG_MAL01", "", false);
                if (GlobalSettings.MailHaiQuan == "")
                    MLMessages("Chưa cấu hình mail của hải quan tiếp nhận", "MSG_MAL02", "", false);
                // ShowMessage("Chưa cấu hình mail của hải quan tiếp nhận", false);
                return;
            }
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8);
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            if (GlobalSettings.NGON_NGU == "0")
            {
                mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString();
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = "Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString(); ;
                mail.Body += "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = false;
                mail.Priority = MailPriority.High;
            }
            else
            {
                mail.Subject = "Electronic Declarate - " + GlobalSettings.MA_DON_VI + "- electronic receipt number:  : " + this.TKMD.SoTiepNhan.ToString();
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = "Declaration form have receipt number: : " + this.TKMD.SoTiepNhan.ToString(); ;
                mail.Body += "Eclectronic Customs - Approved declaration - Voucher ";
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = false;
                mail.Priority = MailPriority.High;
            }

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        Attachment att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (ct.SoBanChinh > 0)
                                mail.Body += "Số bản chính : " + ct.SoBanChinh;
                            if (ct.SoBanSao > 0)
                                mail.Body += "Số bản sao : " + ct.SoBanSao;
                        }
                        else
                        {
                            if (ct.SoBanChinh > 0)
                                mail.Body += "Quantity of main form : " + ct.SoBanChinh;
                            if (ct.SoBanSao > 0)
                                mail.Body += "Quantity of copy form : " + ct.SoBanSao;
                        }
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Credentials = new System.Net.NetworkCredential("haiquandientusoftech@gmail.com", "haiquandientu");
                    smtp.Send(mail);
                    //ShowMessage("Gửi mail thành công.", false);
                    MLMessages("Gửi mail thành công.", "MSG_MAL05", "", false);
                    this.Cursor = Cursors.Default;
                    //this.Close();
                }
                //this.Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                // ShowMessage("Không gửi chứng từ tới hải quan được.", false);
                MLMessages("Không gửi chứng từ tới hải quan được.", "MSG_MAL06", "", false);
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    return;

                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();

                //xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);// LINH CHUYỂN QUA DÙNG HÀM WSRequestPhanLuong(pass)
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string msg = string.Empty;
                    try
                    {
                        XmlDocument docRet = new XmlDocument();
                        docRet.LoadXml(xmlCurrent);
                        msg = "Thông báo từ hải quan: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docRet.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "\n";
                    }
                    catch { }
                    if (string.IsNullOrEmpty(msg))
                        msg = "Chưa có phản hồi từ hải quan.";
                    msg += "\nBạn có muốn tiếp tục lấy xác nhận thông tin không?";
                    string kq = MLMessages(msg, "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";

                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                            TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                            TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                            TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                            TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG);
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                        }
                        else
                        {
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            //DATLMQ comment ngày 29/03/2011
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false, true, ex.ToString());
                            ShowMessage("Có lỗi trong khai báo : " + msg[0], false, true, ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi : " + ex.Message.ToString(), "MSG_SEN20", "", false);
                        //DATLMQ comment ngày 29/03/2011
                        //ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString() + " " + ex.StackTrace);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiPhanLuong(string pass)
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string msg = string.Empty;
                    try
                    {
                        XmlDocument docRet = new XmlDocument();
                        docRet.LoadXml(xmlCurrent);
                        msg = "Thông báo từ hệ thống hải quan: " + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(docRet.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "\n";

                    }
                    catch
                    {
                    }
                    if (string.IsNullOrEmpty(msg))
                    {
                        msg = "Chưa có phản hồi từ hải quan.";
                    }
                    msg += "\nBạn có muốn tiếp tục lấy xác nhận thông tin không?";
                    string kq = MLMessages(msg, "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoiPhanLuong(pass);
                    }
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                sendXML.Delete();

                                string msg = "Tờ khai bị từ chối: " +
                                    Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                //Company.KD.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);

                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        InAnDinhThue();
                    }
                }



                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void InAnDinhThue()
        {
            try
            {
                Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                    TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                    TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                    TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                    TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void InAnDinhThue(long tkmdID, string guidstr)
        {
            try
            {
                AnDinhThue anDinhThue = new AnDinhThue();
                List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (dsADT.Count > 0)
                {
                    anDinhThue = dsADT[0];
                    anDinhThue.LoadChiTiet();

                    Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                    anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                    anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                    anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                    anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue);
                }
                else
                {
                    //Hungtq updated 21/02/2012.
                    //Không có thông tin Ấn định thuế hoặc Có thông tin ấn định thuế từ kết quả Hải quan trả về nhưng chưa lưu được xuống database.
                    Company.Interface.Globals.AnDinhThue_InsertFromXML(this.TKMD);

                    //InAnDinhThue
                    dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThue = dsADT[0];
                        anDinhThue.LoadChiTiet();

                        Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                        anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                        anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                        anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                        anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDForm f = new ListHopDongTKMDForm();
            if (sender != null && sender.GetType().Name != "EditBox")
            {
                f.isKhaiBoSung = true;
            }

            TKMD.SoHopDong = txtSoHopDong.Text;
            TKMD.NgayHopDong = ccNgayHopDong.Value;
            TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;

            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;

            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.TKMD.SoHopDong != "")
            {
                txtSoHopDong.Text = f.TKMD.SoHopDong;
                ccNgayHopDong.Text = f.TKMD.NgayHopDong.ToShortDateString();
                ccNgayHHHopDong.Text = f.TKMD.NgayHetHanHopDong.ToShortDateString();
            }

            //switch (this.NhomLoaiHinh)
            //{
            //    case "NGC":
            //        HopDongRegistedForm f = new HopDongRegistedForm();
            //        f.HopDongSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f.IsBrowseForm = true;
            //        f.ShowDialog();
            //        if (f.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f.HopDongSelected;
            //            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f.HopDongSelected.NgayHetHan;
            //            txtTenDonViDoiTac.Text = f.HopDongSelected.DonViDoiTac;
            //        }
            //        break;
            //    case "XGC":
            //        HopDongRegistedForm f1 = new HopDongRegistedForm();
            //        f1.HopDongSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f1.IsBrowseForm = true;
            //        f1.ShowDialog();     
            //        if (f1.HopDongSelected.SoHopDong != "")
            //        {
            //            this.HDGC = f1.HopDongSelected;
            //            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
            //            ccNgayHopDong.Value = f1.HopDongSelected.NgayKy;
            //            ccNgayHHHopDong.Value = f1.HopDongSelected.NgayHetHan;
            //        }
            //        break;
            //}
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichEditForm f = new HangMauDichEditForm();
                    f.HMD = this.TKMD.HMDCollection[i.Position];
                    soDongHang = i.Position + 1;
                    f.collection = this.TKMD.HMDCollection;
                    if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                    if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                    if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
                    f.LoaiHangHoa = this.TKMD.LoaiHangHoa;
                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.MaHaiQuan = this.TKMD.MaHaiQuan;
                    f.MaNguyenTe = ctrNguyenTe.Ma;
                    f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                    this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
                    this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                    this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
                    f.TKMD = this.TKMD;
                    if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.Edit;
                    else
                        f.OpenType = OpenFormType.View;
                    f.ShowDialog();
                    if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        if (f.IsEdited)
                        {
                            if (f.HMD.TKMD_ID > 0)
                                f.HMD.Update();
                            else
                                this.TKMD.HMDCollection[i.Position] = f.HMD;
                        }
                        else if (f.IsDeleted)
                        {
                            if (f.HMD.TKMD_ID > 0)
                                f.HMD.Delete();
                            this.TKMD.HMDCollection.RemoveAt(i.Position);
                        }
                    }
                    dgList.DataSource = this.TKMD.HMDCollection;
                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        tinhLaiThue();
                    }
                    else
                    {
                        tinhTongTriGiaKhaiBao();
                    }
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.DataSource = this.TKMD.HMDCollection; }
                    setCommandStatus();
                }
                break;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    // ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                    e.Cancel = true;
                    return;
                }
                // if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa thông tin này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    Company.BLL.KDT.HangMauDich hmd = (Company.BLL.KDT.HangMauDich)e.Row.DataRow;

                    //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                    if (HangMauDichForm.checkExistHMD(TKMD.ID, hmd.MaPhu))
                    {
                        ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                        HangMauDichForm.isDeleted = true;
                    }

                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        this.TKMD.Load();
                        this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.TKMD.Update();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuForm f = new ChungTuForm();
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            ctDetail.TenChungTu = e.Row.Cells["TenChungTu"].Text;
            ctDetail.SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text);
            ctDetail.SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text);
            //        ctDetail.STTHang = Convert.ToInt16(e.Row.Cells["STTHang"].Text);
            ctDetail.Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text);
            ctDetail.FileUpLoad = e.Row.Cells["FileUpLoad"].Text;
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    this.TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            //   f.MaLoaiHinh = this.TKMD.MaLoaiHinh;
            f.MaLoaiHinh = this.NhomLoaiHinh;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();
        }

        private void gridEX1_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                //ShowMessage("Tờ khai đã được duyệt không được sửa",false);
                MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                e.Cancel = true;
                return;
            }
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            // if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa thông tin này không?", "MSG_DEL01", "", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (true)
            {
                if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    // ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_WRN08", "", false);
                    e.Cancel = true;
                    return;
                }
                //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.BLL.KDT.HangMauDich hmd = (Company.BLL.KDT.HangMauDich)i.GetRow().DataRow;
                            if (hmd.ID > 0)
                            {

                                /*Xoa cac NPL trong cac chung tu kem theo: Hopdong, Hoa don TM, Giay phep*/
                                /*
                                //Giay phep
                                List<HangGiayPhepDetail> hangGP = HangGiayPhepDetail.SelectCollectionDynamic("HMD_ID = " + hmd.ID, "");

                                if (hangGP.Count > 0)
                                {
                                    hangGP[0].Delete();
                                }

                                //HoaDon thuong mai
                                List<HoaDonThuongMaiDetail> hoadonTM = HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID);
                                if (hoadonTM.Count > 0)
                                {
                                    hoadonTM[0].Delete();
                                }

                                //Hop dong
                                List<HopDongThuongMaiDetail> hopdongTM = HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID);
                                if (hopdongTM.Count > 0)
                                {
                                    hopdongTM[0].Delete();
                                }
                                */

                                if (TKMD.GiayPhepCollection.Count > 0 || TKMD.HopDongThuongMaiCollection.Count > 0 || TKMD.HoaDonThuongMaiCollection.Count > 0)
                                {
                                    this.ShowMessageTQDT("Thông báo", "Thông tin nguyên phụ liệu '" + i.GetRow().Cells["MaPhu"].Text + "'" +
                                    " này đang sử dụng trong các chứng từ: \n Hợp đồng \n Hóa đơn thương mại \n Giấy phép.\n\nKhông thể xóa!.", false);
                                    return;
                                }

                                //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                                if (HangMauDichForm.checkExistHMD(TKMD.ID, hmd.MaPhu))
                                {
                                    ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                                    HangMauDichForm.isDeleted = true;
                                }

                                hmd.Delete();

                                //dgList.Refetch();
                            }
                            else
                                ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                        }
                    }


                    foreach (Company.BLL.KDT.HangMauDich hmdDelete in ToKhaiMauDichForm.deleteHMDCollection)
                    {
                        this.TKMD.HMDCollection.Remove(hmdDelete);
                    }

                    dgList.DataSource = this.TKMD.HMDCollection;

                    try
                    {
                        dgList.Refetch();
                    }
                    catch (Exception ex)
                    {
                        dgList.Refresh();
                    }


                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        tinhLaiThue();
                    }
                    else
                    {
                        tinhTongTriGiaKhaiBao();
                    }
                    if (TKMD.ID > 0)
                        this.TKMD.Update();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            setCommandStatus();
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            this.TKMD.HMDCollection.Clear();
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = this.TKMD.ChungTuTKCollection;
            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtSoHopDong_Leave(object sender, EventArgs e)
        {
            if (txtSoHopDong.Text.Trim().Length > 0)
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = false;
            else
            {
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = true;
                ccNgayHopDong.Text = ccNgayHHHopDong.Text = "";
            }
        }

        private void txtSoHoaDonThuongMai_Leave(object sender, EventArgs e)
        {
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                ccNgayHDTM.ReadOnly = false;
            else
            {
                ccNgayHDTM.ReadOnly = true;
                ccNgayHDTM.Text = "";
            }
        }

        private void txtSoVanTaiDon_Leave(object sender, EventArgs e)
        {
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
                ccNgayVanTaiDon.ReadOnly = false;
            else
            {
                ccNgayVanTaiDon.ReadOnly = true;
                ccNgayVanTaiDon.Text = "";

            }
        }

        private void txtSoGiayPhep_Leave(object sender, System.EventArgs e)
        {
            if (txtSoGiayPhep.Text.Trim().Length > 0)
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = false;
            else
            {
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = true;
                ccNgayGiayPhep.Text = ccNgayHHGiayPhep.Text = "";
            }
        }

        private void txtTenDonViDoiTac_ButtonClick(object sender, EventArgs e)
        {
            DonViDoiTacForm f = new DonViDoiTacForm();
            f.isBrower = true;
            f.ShowDialog();
            if (f.doiTac != null && f.doiTac.TenCongTy != "")
            {
                txtTenDonViDoiTac.Text = f.doiTac.TenCongTy + ".\r\n " + f.doiTac.DiaChi + "\r\n" + f.doiTac.GhiChu;
            }
        }

        private void ctrNguyenTe_ValueChanged(object sender, EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia + "";
            if (ctrNguyenTe.Ma == "USD")
            {
                txtTyGiaUSD.Text = ctrNguyenTe.TyGia + "";
            }
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            if (sender != null && sender.GetType().Name != "EditBox")
            {
                f.isKhaiBoSung = true;
            }

            TKMD.SoGiayPhep = txtSoGiayPhep.Text;
            TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
            TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;

            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.TKMD.SoGiayPhep != "")
            {
                txtSoGiayPhep.Text = f.TKMD.SoGiayPhep;
                ccNgayGiayPhep.Text = f.TKMD.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = f.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            }
        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();
            if (sender != null && sender.GetType().Name != "EditBox")
                f.isKhaiBoSung = true;

            TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
            TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;

            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;

            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.TKMD.SoHoaDonThuongMai != "")
            {
                txtSoHoaDonThuongMai.Text = f.TKMD.SoHoaDonThuongMai;
                ccNgayHDTM.Text = f.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            }
        }

        private void txtSoVanTaiDon_ButtonClick(object sender, EventArgs e)
        {
            VanTaiDonForm f = new VanTaiDonForm();
            //f.isKhaiBoSung = this.isKhaiBoSung;
            TKMD.SoVanDon = txtSoVanTaiDon.Text;
            TKMD.NgayVanDon = ccNgayVanTaiDon.Value;

            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            TKMD.NgayDenPTVT = ccNgayDen.Value;

            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            TKMD.CuaKhau_ID = ctrCuaKhau.Ma;
            TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;

            f.TKMD = TKMD;
            f.ShowDialog();
            if (f.TKMD.VanTaiDon != null && f.TKMD.VanTaiDon.SoVanDon.Trim() != "")
            {
                txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
                if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";

                // ĐKGH.
                cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
                if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToShortDateString();
                else ccNgayVanTaiDon.Text = "";
                // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
                //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                // Container 20.
                txtSoContainer20.Value = this.TKMD.SoContainer20;

                // Container 40.
                txtSoContainer40.Value = this.TKMD.SoContainer40;
                // linh thêm nước xuất lấy từ vận đơn 14/01/2011
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            }
            else if (TKMD.VanTaiDon == null)
            {
                TKMD.SoVanDon = string.Empty;
                TKMD.NgayVanDon = new DateTime(1900,01,01);
                if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
                {
                    TKMD.PTVT_ID = string.Empty;
                    TKMD.QuocTichPTVT_ID = string.Empty;
                    TKMD.SoHieuPTVT = string.Empty;
                    this.txtSoHieuPTVT.Text = string.Empty;
                }
                this.ccNgayVanTaiDon.Text = string.Empty;
                this.txtSoVanTaiDon.Text = string.Empty;
            }
        }

        #region Validate Khai bo sung

        /// <summary>
        /// Kiem tra thong tin truoc khi khai bo sung chung tu.
        /// </summary>
        /// <param name="soToKhai"></param>
        /// <returns></returns>
        /// HUNGTQ, Update 07/06/2010.
        private bool ValidateKhaiBoSung(int soToKhai)
        {
            if (soToKhai == 0)
            {
                string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                //Globals.ShowMessageTQDT(msg, false);
                ShowMessage(msg, false);

                return false;
            }

            return true;
        }

        #endregion

        #region Begin ButtonClick TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Phuong tien van tai.
        /// </summary>
        /// Hungtq, Update 13072010
        private void SetEvent_TextBox_VanDon()
        {
            txtSoHieuPTVT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtSoHieuPTVT.ButtonClick += new EventHandler(txtSoHieuPTVT_ButtonClick2);

            txtSoVanTaiDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_GiayPhep()
        {
            txtSoGiayPhep.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HopDong()
        {
            txtSoHopDong.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HoaDong()
        {
            txtSoHoaDonThuongMai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void txtSoHieuPTVT_ButtonClick2(object sender, EventArgs e)
        {
            Company.Interface.VanTaiDonForm frmVanDon = new VanTaiDonForm();

            if (TKMD != null)
            {
                TKMD.SoVanDon = txtSoVanTaiDon.Text;
                TKMD.NgayVanDon = ccNgayVanTaiDon.Value;

                TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                TKMD.NgayDenPTVT = ccNgayDen.Value;

                frmVanDon.TKMD = TKMD;

                frmVanDon.ShowDialog();

                if (TKMD.VanTaiDon != null)
                {
                    txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                    ccNgayDen.Text = TKMD.VanTaiDon.NgayDenPTVT.ToShortDateString();

                    if (txtSoVanTaiDon.Text != "")
                    {
                        if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số vận đơn' không?.", true) == "Yes")
                        {
                            txtSoVanTaiDon.Text = TKMD.VanTaiDon.SoVanDon;
                            ccNgayVanTaiDon.Text = TKMD.NgayVanDon.ToShortDateString();
                        }
                    }
                }

            }
        }

        #endregion

        private void btnNoiDungDieuChinhTKForm_Click(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKForm noidungchinhsuatk = new NoiDungChinhSuaTKForm();
            noidungchinhsuatk.TKMD = this.TKMD;
            noidungchinhsuatk.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
            noidungchinhsuatk.ShowDialog();
        }


        //private void btnNoiDung_Click(object sender, EventArgs e)
        //{
        //    ListNoiDungDieuChinhForm f = new ListNoiDungDieuChinhForm();
        //    f.TKMD = this.TKMD;
        //    f.Show();
        //}

        /*
        #region ADD COMMAND ON MENU TOOLBAR

        private Janus.Windows.UI.CommandBars.UICommand Separator3;
        private Janus.Windows.UI.CommandBars.UICommand Separator4;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaToKhaiDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaToKhaiDaDuyet2;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyToKhaiDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyToKhaiDaDuyet2;

        private void AddCommand()
        {
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");

            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");

            this.cmdSuaToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdSuaToKhaiDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");

            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdHuyToKhaiDaDuyet2
            // 
            this.cmdHuyToKhaiDaDuyet2.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet2.Name = "cmdHuyToKhaiDaDuyet2";
            // 
            // cmdSuaToKhaiDaDuyet
            // 
            this.cmdSuaToKhaiDaDuyet.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Name = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Text = "Sửa tờ khai đã duyệt";
            // 
            // cmdSuaToKhaiDaDuyet2
            // 
            this.cmdSuaToKhaiDaDuyet2.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet2.Name = "cmdSuaToKhaiDaDuyet2";
            // 
            // cmMain
            // 
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {        
            this.cmdHuyToKhaiDaDuyet,
            this.cmdSuaToKhaiDaDuyet});
            //this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {            
            this.Separator3,
            this.cmdSuaToKhaiDaDuyet2,
            this.cmdHuyToKhaiDaDuyet2,
            this.Separator4});
            
        }

        #endregion
        */

        /*
        #region BACKGROUND WORKER

        private System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
        private Company.Interface.FrmNotificationProgress progessForm = new FrmNotificationProgress();
        private System.Windows.Forms.Timer timeProgess = new System.Windows.Forms.Timer();
        private int _totalSeconds = 0;

        public int TotalSeconds
        {
            get { return _totalSeconds; }
            set { _totalSeconds = value; }
        }

        private void InitialProgess()
        {
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;

            bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            bw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(bw_ProgressChanged);
            bw.DoWork += new System.ComponentModel.DoWorkEventHandler(bw_DoWork);

            timeProgess.Interval = 1000;
            timeProgess.Tick += new EventHandler(timeProgess_Tick);
        }

        private void DoWork(int seconds)
        {
            if (!bw.IsBusy)
            {
                TotalSeconds = seconds;
                bw.RunWorkerAsync();
            }
        }

        private void DoExit()
        {
            if (timeProgess.Enabled == true)
            {
                timeProgess.Stop();
                timeProgess.Enabled = false;
            }
            if (progessForm.Visible)
                progessForm.HideForm(progessForm);
        }

        private void timeProgess_Tick(object sender, EventArgs e)
        {
            int _cnt = 0;
            int percent = 0;

            while (_cnt < TotalSeconds)
            {
                _cnt += 1;

                percent = Convert.ToInt32((_cnt * 100) / _totalSeconds);
                bw.ReportProgress(percent);

                if (bw.CancellationPending == true)
                {
                    timeProgess.Stop();
                    timeProgess.Enabled = false;
                    break;
                }
            }

        }

        private void bw_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (!e.Cancel)
                {
                    if (!timeProgess.Enabled)
                        timeProgess.Start();

                    if (!progessForm.Visible)
                    {
                        progessForm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                bw.CancelAsync();
            }
        }

        private void bw_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (bw.CancellationPending == true) return;

            progessForm.UpdateProgress(e.ProgressPercentage);
        }

        private void bw_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            timeProgess.Stop();
            timeProgess.Enabled = false;
            _totalSeconds = 0;
            progessForm.UpdateProgress(100);
        }
        #endregion
        */
    }
}

