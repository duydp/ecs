﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE2LD0 : BasicVNACC
    {
        public List<VAE2LD0_HANG> HangMD { get; set; }
        public void LoadVAE2LD0(string strResult)
        {
            try
            {
                this.GetObject<VAE2LD0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE2LD0, false, VAE2LD0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE2LD0.TongSoByte);
                while (true)
                {
                    VAE2LD0_HANG vad1agHang = new VAE2LD0_HANG();
                    vad1agHang.GetObject<VAE2LD0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE2LD0_HANG", false, VAE2LD0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE2LD0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE2LD0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE2LD0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE2LD0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

    }
}
