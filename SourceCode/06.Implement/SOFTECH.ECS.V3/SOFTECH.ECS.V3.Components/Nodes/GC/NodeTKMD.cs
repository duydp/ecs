﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    class NodeTKMD
    {
        /// <summary>
        /// Khai bao.
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (Danh mục chuẩn)
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu chứng từ 
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai chứng từ 
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng của chứng từ (Danh mục chuẩn)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái chứng từ (Danh mục chuẩn)
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số đăng ký  
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký chứng từ 
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Mã hải quan  (Danh mục chuẩn) 
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Thông tin người khai hải quan
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên 
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã
        /// </summary>
        public const string Identity = "Identity";
        /// <summary>
        /// Thông tin thương nhân gia công hàng hóa
        /// </summary>
        public const string Importer = "Importer";
        public const string DeclarationDocument = "DeclarationDocument";
        /// <summary>
        /// Mã LH ( Danh mục chuẩn )
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Mã loại hóa đơn thương mại	(Danh mục chuẩn)
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// Số lượng hàng
        /// </summary>
        public const string GoodsItem = "GoodsItem";
        /// <summary>
        /// Số lượng chứng từ, phụ lục đính kèm
        /// </summary>
        public const string loadingList = "loadingList";
        /// <summary>
        /// Trọng lượng (kg)
        /// </summary>
        public const string totalGrossMass = "totalGrossMass";
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";
        public const string statisticalValue = "statisticalValue";
        /// <summary>
        /// Nguyên tuệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Số kiện
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Mã nguyên tệ ( Danh mục chuẩn )
        /// </summary>
        public const string CurrencyType = "CurrencyType";
        /// <summary>
        /// Ngày TK
        /// </summary>
        public const string rate = "rate";
        /// <summary>
        /// Số kiện
        /// </summary>
        public const string DeclarationPackaging = "DeclarationPackaging";
        /// <summary>
        /// Mã phương thức thanh toán
        /// </summary>
        public const string Payment = "Payment";
        /// <summary>
        /// Người đại diện doanh nghiệp
        /// </summary>
        public const string RepresentativePerson = "RepresentativePerson";
        /// <summary>
        /// Người xuất khẩu
        /// </summary>
        public const string Exporter = "Exporter";
        /// <summary>
        /// hợp đồng
        /// </summary>
        public const string ContractReference = "ContractReference";
        /// <summary>
        /// Ghi chú khác
        /// </summary>
        public const string AdditionalInfomation = "AdditionalInfomation";

    }
}
