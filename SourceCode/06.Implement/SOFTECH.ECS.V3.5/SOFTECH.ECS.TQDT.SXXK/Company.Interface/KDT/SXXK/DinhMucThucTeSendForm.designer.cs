﻿namespace Company.Interface.KDT.SXXK
{
    partial class DinhMucThucTeSendForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout cbbLenhSanXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grListSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListSP_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListNPL_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DinhMucThucTeSendForm));
            Janus.Windows.GridEX.GridEXLayout cbbMaNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLenhSanXuat = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.cbbTinhTrang = new Janus.Windows.EditControls.UIComboBox();
            this.txtPO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGuidString = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayHĐ = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayHH = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.lblTotalRow = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelect = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteSP = new Janus.Windows.EditControls.UIButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAddExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddExcel");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdAddExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdAddExcel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdUpdateResult = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListSP = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaHSNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDonViTinhNPL = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbMaNPL = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaNPL)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 643), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 643);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 619);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 619);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox9);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox10);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(1132, 643);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cbbLenhSanXuat);
            this.uiGroupBox1.Controls.Add(this.cbbTinhTrang);
            this.uiGroupBox1.Controls.Add(this.txtPO);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.clcNgayHĐ);
            this.uiGroupBox1.Controls.Add(this.clcTuNgay);
            this.uiGroupBox1.Controls.Add(this.clcNgayHH);
            this.uiGroupBox1.Controls.Add(this.clcDenNgay);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1132, 179);
            this.uiGroupBox1.TabIndex = 344;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbLenhSanXuat
            // 
            cbbLenhSanXuat_DesignTimeLayout.LayoutString = resources.GetString("cbbLenhSanXuat_DesignTimeLayout.LayoutString");
            this.cbbLenhSanXuat.DesignTimeLayout = cbbLenhSanXuat_DesignTimeLayout;
            this.cbbLenhSanXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLenhSanXuat.Location = new System.Drawing.Point(106, 23);
            this.cbbLenhSanXuat.Name = "cbbLenhSanXuat";
            this.cbbLenhSanXuat.SelectedIndex = -1;
            this.cbbLenhSanXuat.SelectedItem = null;
            this.cbbLenhSanXuat.Size = new System.Drawing.Size(477, 21);
            this.cbbLenhSanXuat.TabIndex = 1;
            this.cbbLenhSanXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbLenhSanXuat.ValueChanged += new System.EventHandler(this.cbbLenhSanXuat_ValueChanged);
            // 
            // cbbTinhTrang
            // 
            this.cbbTinhTrang.BackColor = System.Drawing.Color.White;
            this.cbbTinhTrang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTinhTrang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tạo mới";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đang sản xuất";
            uiComboBoxItem2.Value = "1";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã hoàn thành";
            uiComboBoxItem3.Value = "2";
            this.cbbTinhTrang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cbbTinhTrang.Location = new System.Drawing.Point(318, 83);
            this.cbbTinhTrang.Name = "cbbTinhTrang";
            this.cbbTinhTrang.ReadOnly = true;
            this.cbbTinhTrang.Size = new System.Drawing.Size(265, 22);
            this.cbbTinhTrang.TabIndex = 5;
            this.cbbTinhTrang.ValueMember = "ID";
            this.cbbTinhTrang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtPO
            // 
            this.txtPO.BackColor = System.Drawing.Color.White;
            this.txtPO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPO.Location = new System.Drawing.Point(106, 83);
            this.txtPO.Name = "txtPO";
            this.txtPO.ReadOnly = true;
            this.txtPO.Size = new System.Drawing.Size(131, 22);
            this.txtPO.TabIndex = 4;
            this.txtPO.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(245, 87);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tình trạng";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(12, 87);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Số đơn hàng";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox5.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox5.Controls.Add(this.lblTrangThai);
            this.uiGroupBox5.Controls.Add(this.label12);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Controls.Add(this.label13);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.txtGuidString);
            this.uiGroupBox5.Location = new System.Drawing.Point(594, 11);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(522, 157);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.Text = "Thông tin hải quan";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // clcNgayTiepNhan
            // 
            this.clcNgayTiepNhan.CustomFormat = "dd/MM/yyyy";
            this.clcNgayTiepNhan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayTiepNhan.DropDownCalendar.Name = "";
            this.clcNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(376, 47);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.ReadOnly = true;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(97, 21);
            this.clcNgayTiepNhan.TabIndex = 11;
            this.clcNgayTiepNhan.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(120, 46);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(123, 22);
            this.txtSoTiepNhan.TabIndex = 10;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Blue;
            this.lblTrangThai.Location = new System.Drawing.Point(120, 19);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(95, 14);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(22, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "Trạng thái : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(22, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tham chiếu : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(249, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ngày tiếp nhận :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Số tiếp nhận : ";
            // 
            // txtGuidString
            // 
            this.txtGuidString.BackColor = System.Drawing.Color.White;
            this.txtGuidString.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGuidString.Location = new System.Drawing.Point(120, 76);
            this.txtGuidString.Name = "txtGuidString";
            this.txtGuidString.Size = new System.Drawing.Size(353, 22);
            this.txtGuidString.TabIndex = 12;
            this.txtGuidString.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGuidString.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayHĐ
            // 
            this.clcNgayHĐ.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHĐ.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHĐ.DropDownCalendar.Name = "";
            this.clcNgayHĐ.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHĐ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHĐ.Location = new System.Drawing.Point(318, 114);
            this.clcNgayHĐ.Name = "clcNgayHĐ";
            this.clcNgayHĐ.ReadOnly = true;
            this.clcNgayHĐ.Size = new System.Drawing.Size(89, 21);
            this.clcNgayHĐ.TabIndex = 7;
            this.clcNgayHĐ.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcNgayHĐ.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(106, 54);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.ReadOnly = true;
            this.clcTuNgay.Size = new System.Drawing.Size(132, 21);
            this.clcTuNgay.TabIndex = 2;
            this.clcTuNgay.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayHH
            // 
            this.clcNgayHH.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHH.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHH.DropDownCalendar.Name = "";
            this.clcNgayHH.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHH.Location = new System.Drawing.Point(486, 114);
            this.clcNgayHH.Name = "clcNgayHH";
            this.clcNgayHH.ReadOnly = true;
            this.clcNgayHH.Size = new System.Drawing.Size(97, 21);
            this.clcNgayHH.TabIndex = 8;
            this.clcNgayHH.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcNgayHH.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(318, 54);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.ReadOnly = true;
            this.clcDenNgay.Size = new System.Drawing.Size(89, 21);
            this.clcDenNgay.TabIndex = 3;
            this.clcDenNgay.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(106, 146);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(477, 22);
            this.txtGhiChu.TabIndex = 9;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(106, 113);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.ReadOnly = true;
            this.txtSoHopDong.Size = new System.Drawing.Size(131, 22);
            this.txtSoHopDong.TabIndex = 6;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(413, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Ngày HH :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(245, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày HĐ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(245, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Đến ngày :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Từ ngày :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Ghi chú : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số HĐ :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Lệnh sản xuất";
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox10.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 584);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1132, 59);
            this.uiGroupBox10.TabIndex = 348;
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnDelete);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.lblTotalRow);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(591, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(538, 48);
            this.uiGroupBox2.TabIndex = 9;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(357, 16);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 23);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Xóa NPL";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(454, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblTotalRow
            // 
            this.lblTotalRow.AutoSize = true;
            this.lblTotalRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRow.ForeColor = System.Drawing.Color.Blue;
            this.lblTotalRow.Location = new System.Drawing.Point(141, 21);
            this.lblTotalRow.Name = "lblTotalRow";
            this.lblTotalRow.Size = new System.Drawing.Size(29, 14);
            this.lblTotalRow.TabIndex = 0;
            this.lblTotalRow.Text = "{0}";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(127, 14);
            this.label22.TabIndex = 0;
            this.label22.Text = "Tổng số dòng hàng : ";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnSelect);
            this.uiGroupBox7.Controls.Add(this.btnDeleteSP);
            this.uiGroupBox7.Controls.Add(this.panel1);
            this.uiGroupBox7.Controls.Add(this.label18);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(588, 48);
            this.uiGroupBox7.TabIndex = 8;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelect.Location = new System.Drawing.Point(9, 16);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(118, 23);
            this.btnSelect.TabIndex = 23;
            this.btnSelect.Text = "Chọn sản phẩm";
            this.btnSelect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnDeleteSP
            // 
            this.btnDeleteSP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSP.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSP.Image")));
            this.btnDeleteSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteSP.Location = new System.Drawing.Point(475, 16);
            this.btnDeleteSP.Name = "btnDeleteSP";
            this.btnDeleteSP.Size = new System.Drawing.Size(105, 23);
            this.btnDeleteSP.TabIndex = 24;
            this.btnDeleteSP.Text = "Xóa SP";
            this.btnDeleteSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteSP.Click += new System.EventHandler(this.btnDeleteSP_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.GreenYellow;
            this.panel1.Location = new System.Drawing.Point(289, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(22, 22);
            this.panel1.TabIndex = 24;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(315, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 14);
            this.label18.TabIndex = 0;
            this.label18.Text = "Chưa nhập định mức";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddExcel,
            this.cmdSave,
            this.cmdSend,
            this.cmdSendEdit,
            this.cmdCancel,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdUpdateGuidString,
            this.cmdPrint,
            this.cmdUpdateResult,
            this.cmdHistory});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddExcel1,
            this.cmdSave1,
            this.cmdSend1,
            this.cmdSendEdit1,
            this.cmdCancel1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1,
            this.cmdPrint1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1338, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdAddExcel1
            // 
            this.cmdAddExcel1.Key = "cmdAddExcel";
            this.cmdAddExcel1.Name = "cmdAddExcel1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdAddExcel
            // 
            this.cmdAddExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddExcel.Image")));
            this.cmdAddExcel.Key = "cmdAddExcel";
            this.cmdAddExcel.Name = "cmdAddExcel";
            this.cmdAddExcel.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.cmdAddExcel.Text = "Nhập từ File Excel";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.F2;
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.F3;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Shortcut = System.Windows.Forms.Shortcut.F4;
            this.cmdSendEdit.Text = "Khai báo sửa";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.cmdCancel.Text = "Khai báo hủy";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Shortcut = System.Windows.Forms.Shortcut.F6;
            this.cmdFeedback.Text = "Nhận phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdUpdateResult1,
            this.cmdHistory1});
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Shortcut = System.Windows.Forms.Shortcut.F7;
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdUpdateResult1
            // 
            this.cmdUpdateResult1.Key = "cmdUpdateResult";
            this.cmdUpdateResult1.Name = "cmdUpdateResult1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Shortcut = System.Windows.Forms.Shortcut.F8;
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In định mức";
            // 
            // cmdUpdateResult
            // 
            this.cmdUpdateResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateResult.Image")));
            this.cmdUpdateResult.Key = "cmdUpdateResult";
            this.cmdUpdateResult.Name = "cmdUpdateResult";
            this.cmdUpdateResult.Text = "Cập nhật Thông tin";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistory.Image")));
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Lịch sử Khai báo";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1338, 32);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 179);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(591, 405);
            this.uiGroupBox3.TabIndex = 349;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListSP);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(585, 394);
            this.uiGroupBox8.TabIndex = 0;
            this.uiGroupBox8.Text = "Danh sách sản phẩm";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListSP
            // 
            this.grListSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListSP.ColumnAutoResize = true;
            grListSP_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListSP_DesignTimeLayout_Reference_0.Instance")));
            grListSP_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListSP_DesignTimeLayout_Reference_0});
            grListSP_DesignTimeLayout.LayoutString = resources.GetString("grListSP_DesignTimeLayout.LayoutString");
            this.grListSP.DesignTimeLayout = grListSP_DesignTimeLayout;
            this.grListSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListSP.FrozenColumns = 5;
            this.grListSP.GroupByBoxVisible = false;
            this.grListSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListSP.Hierarchical = true;
            this.grListSP.Location = new System.Drawing.Point(3, 67);
            this.grListSP.Name = "grListSP";
            this.grListSP.RecordNavigator = true;
            this.grListSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListSP.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grListSP.Size = new System.Drawing.Size(579, 324);
            this.grListSP.TabIndex = 87;
            this.grListSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListSP.SelectionChanged += new System.EventHandler(this.grListSP_SelectionChanged);
            this.grListSP.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListSP_LoadingRow);
            this.grListSP.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListSP_FormattingRow);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label16);
            this.uiGroupBox6.Controls.Add(this.txtMaSP);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(579, 50);
            this.uiGroupBox6.TabIndex = 86;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(407, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Mã SP";
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.BackColor = System.Drawing.Color.White;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(100, 17);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(301, 21);
            this.txtMaSP.TabIndex = 1;
            this.txtMaSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.dgListNPL);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Location = new System.Drawing.Point(591, 179);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(541, 405);
            this.uiGroupBox9.TabIndex = 0;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            dgListNPL_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListNPL_DesignTimeLayout_Reference_0.Instance")));
            dgListNPL_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListNPL_DesignTimeLayout_Reference_0});
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(3, 198);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(535, 204);
            this.dgListNPL.TabIndex = 343;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNPL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListNPL_RowDoubleClick);
            this.dgListNPL.SelectionChanged += new System.EventHandler(this.dgListNPL_SelectionChanged);
            this.dgListNPL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPL_LoadingRow);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtMaHSNPL);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.txtDonViTinhNPL);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.cbbMaNPL);
            this.uiGroupBox4.Controls.Add(this.btnAdd);
            this.uiGroupBox4.Controls.Add(this.txtTenNPL);
            this.uiGroupBox4.Controls.Add(this.txtGhiChuNPL);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(535, 190);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin nguyên phụ liệu";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtMaHSNPL
            // 
            this.txtMaHSNPL.BackColor = System.Drawing.Color.White;
            this.txtMaHSNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSNPL.Location = new System.Drawing.Point(119, 156);
            this.txtMaHSNPL.Name = "txtMaHSNPL";
            this.txtMaHSNPL.Size = new System.Drawing.Size(121, 22);
            this.txtMaHSNPL.TabIndex = 20;
            this.txtMaHSNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(22, 160);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 14);
            this.label21.TabIndex = 0;
            this.label21.Text = "Mã HS";
            // 
            // txtDonViTinhNPL
            // 
            this.txtDonViTinhNPL.BackColor = System.Drawing.Color.White;
            this.txtDonViTinhNPL.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.txtDonViTinhNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViTinhNPL.Location = new System.Drawing.Point(376, 96);
            this.txtDonViTinhNPL.Name = "txtDonViTinhNPL";
            this.txtDonViTinhNPL.Size = new System.Drawing.Size(97, 22);
            this.txtDonViTinhNPL.TabIndex = 18;
            this.txtDonViTinhNPL.ValueMember = "ID";
            this.txtDonViTinhNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(299, 100);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "Đơn vị tính";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên NPL";
            // 
            // cbbMaNPL
            // 
            cbbMaNPL_DesignTimeLayout.LayoutString = resources.GetString("cbbMaNPL_DesignTimeLayout.LayoutString");
            this.cbbMaNPL.DesignTimeLayout = cbbMaNPL_DesignTimeLayout;
            this.cbbMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaNPL.Location = new System.Drawing.Point(120, 26);
            this.cbbMaNPL.Name = "cbbMaNPL";
            this.cbbMaNPL.SelectedIndex = -1;
            this.cbbMaNPL.SelectedItem = null;
            this.cbbMaNPL.Size = new System.Drawing.Size(353, 21);
            this.cbbMaNPL.TabIndex = 15;
            this.cbbMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaNPL.ValueChanged += new System.EventHandler(this.cbbMaNPL_ValueChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(246, 156);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(82, 23);
            this.btnAdd.TabIndex = 21;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.BackColor = System.Drawing.Color.White;
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(120, 56);
            this.txtTenNPL.Multiline = true;
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.Size = new System.Drawing.Size(353, 32);
            this.txtTenNPL.TabIndex = 16;
            this.txtTenNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuNPL
            // 
            this.txtGhiChuNPL.BackColor = System.Drawing.Color.White;
            this.txtGhiChuNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuNPL.Location = new System.Drawing.Point(120, 126);
            this.txtGhiChuNPL.Name = "txtGhiChuNPL";
            this.txtGhiChuNPL.Size = new System.Drawing.Size(353, 22);
            this.txtGhiChuNPL.TabIndex = 19;
            this.txtGhiChuNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChuNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ghi chú";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(22, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Định mức";
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.BackColor = System.Drawing.Color.White;
            this.txtDinhMuc.DecimalDigits = 8;
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.Location = new System.Drawing.Point(120, 96);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDinhMuc.Size = new System.Drawing.Size(161, 22);
            this.txtDinhMuc.TabIndex = 17;
            this.txtDinhMuc.Text = "0.00000000";
            this.txtDinhMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDinhMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtDinhMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã NPL";
            // 
            // DinhMucThucTeSendForm
            // 
            this.ClientSize = new System.Drawing.Size(1338, 681);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DinhMucThucTeSendForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Khai báo định mức thực tế của sản phẩm";
            this.Load += new System.EventHandler(this.LenhSanXuatGCForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaNPL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHĐ;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHH;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGuidString;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private System.Windows.Forms.Label lblTrangThai;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaNPL;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnSelect;
        private Janus.Windows.EditControls.UIButton btnDeleteSP;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuNPL;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.GridEX grListSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private Janus.Windows.EditControls.UIComboBox txtDonViTinhNPL;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIComboBox cbbTinhTrang;
        private Janus.Windows.GridEX.EditControls.EditBox txtPO;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSNPL;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbLenhSanXuat;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel1;
        private System.Windows.Forms.Label lblTotalRow;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
    }
}
