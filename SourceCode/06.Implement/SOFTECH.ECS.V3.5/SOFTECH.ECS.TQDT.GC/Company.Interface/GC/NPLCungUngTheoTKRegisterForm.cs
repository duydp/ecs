using System;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Company.Interface.GC;

namespace Company.Interface.KDT.GC
{
    public partial class NPLCungUngTheoTKRegisterForm : BaseForm
    {
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public ToKhaiMauDich TKMD = null;
        public ToKhaiChuyenTiep TKCT = null;
        public HopDong HD = new HopDong();
        public bool BoolFlag = false;
        public NPLCungUngTheoTKRegisterForm()
        {
            InitializeComponent();
        }

        private void txtSoTiepNhanTK_ButtonClick(object sender, EventArgs e)
        {           
            ToKhaiMauDichDaDuyetForm f = new ToKhaiMauDichDaDuyetForm();
            f.IdHopDong = HD.ID;
            f.ShowDialog();
            if(f.Tkct == null && f.Tkmd ==null ) return;
            
            //HopDong HD=new HopDong();  
             
            //if (BKCU.ID > 0)
            //{   
            //    HD = new HopDong();              
            //    if(TKMD!=null)
            //        HD.ID=TKMD.IDHopDong;
            //    else
            //        HD.ID=TKCT.IDHopDong;
            //    HD = HopDong.Load(HD.ID);         
                
            //    foreach (SanPhanCungUng spCungUng in BKCU.SanPhamCungUngCollection)
            //    {
            //        if(spCungUng.ID>0)
            //            spCungUng.Delete(HD);
            //    }
            //    BKCU.SanPhamCungUngCollection.Clear();
            //    try
            //    {
            //        dgList.DataSource = BKCU.SanPhamCungUngCollection;
            //        dgList.Refetch();
            //    }
            //    catch
            //    {
            //        dgList.Refresh();
            //    }
            //}

            if (f.Tkmd != null)
            {
                BKCungUngDangKyCollection bkucoll = new BKCungUngDangKyCollection();
                bkucoll =  BKCU.SelectCollectionDynamic("TKMD_ID = " + f.Tkmd.ID, null);
                if (bkucoll.Count > 0)
                    this.BKCU = bkucoll[0];
                else
                {
                    this.BKCU = new BKCungUngDangKy();
                    this.BKCU.TKMD_ID = f.Tkmd.ID;
                    this.BKCU.TKCT_ID = 0;
                }

                txtSoTiepNhanTK.Text = f.Tkmd.SoTiepNhan + "";
                txtNamDangKyTK.Text = f.Tkmd.NgayTiepNhan.Year + "";
                txtLoaiHinh.Text =this.LoaiHinhMauDich_GetName(f.Tkmd.MaLoaiHinh);
                ctrHQTiepNhanTK.Ma = f.Tkmd.MaHaiQuan;
                //txtSoHopDong.Text = f.Tkmd.SoHopDong + " (" + f.Tkmd.NgayHopDong.ToString("dd/MM/yyyy") + ")";
                this.TKMD = f.Tkmd;
                this.TKCT = null;
                this.TKMD.LoadHMDCollection();
                grbToKhai.Text = setText("Tờ khai xuất gia công", "Outsourcing Export declaration");
                BKCU.LoadSanPhamCungUngCollection();
                dgList.DataSource = BKCU.SanPhamCungUngCollection;
            }
            else if(f.Tkct!=null)
            {
                BKCungUngDangKyCollection bkucoll = new BKCungUngDangKyCollection();
                bkucoll = BKCU.SelectCollectionDynamic("TKCT_ID = " + f.Tkct.ID, null);
                if (bkucoll.Count > 0)
                    this.BKCU = bkucoll[0];
                else
                {
                    this.BKCU = new BKCungUngDangKy();
                    this.BKCU.TKMD_ID = 0;
                    this.BKCU.TKCT_ID = f.Tkct.ID;
                }
                txtSoTiepNhanTK.Text = f.Tkct.SoTiepNhan + "";
                txtNamDangKyTK.Text = f.Tkct.NgayTiepNhan.Year + "";
                txtLoaiHinh.Text =LoaiHinhChuyenTiep_GetName(f.Tkct.MaLoaiHinh);
                ctrHQTiepNhanTK.Ma = f.Tkct.MaHaiQuanTiepNhan;
                txtSoHopDong.Text = f.Tkct.SoHopDongDV + " (" + f.Tkct.NgayHDDV.ToString("dd/MM/yyyy") + ")";
                this.TKCT = f.Tkct;
                this.TKCT.LoadHCTCollection();
                this.TKMD = null;
                grbToKhai.Text = setText("Tờ khai chuyển tiếp xuất sản phẩm", "Export transition declaration");
                dgList.DataSource = BKCU.SanPhamCungUngCollection;
            }
            try
            {
                dgList.Refetch();
            }
            catch (System.Exception ex)
            {
                dgList.Refetch();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
         
        }

        private void NPLCungUngTheoTKSendForm_Load(object sender, EventArgs e)
        {
            txtSoHopDong.Text = HD.SoHopDong;
            txtNgayKyHopDong.Text = HD.NgayKy.ToString("dd/MM/yyyy");
            txtNgayHetHanHD.Text = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan.ToString("dd/MM/yyyy") : HD.NgayGiaHan.ToString("dd/MM/yyyy");
            dgList.Tables[0].Columns["LuongXK"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongCUSanPham"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            if (BKCU.ID == 0 && this.BKCU.SanPhamCungUngCollection==null)
                this.BKCU.SanPhamCungUngCollection = new SanPhanCungUngCollection();
            else
            {
                if(BKCU.ID >0)
                    BKCU.LoadSanPhamCungUngCollection();
                if (TKMD != null)
                {
                    TKMD.Load();
                    TKMD.LoadHMDCollection();
                    if (TKMD.SoTiepNhan > 0)
                    {
                        txtNamDangKyTK.Text = TKMD.NgayTiepNhan.Year.ToString();
                        txtSoTiepNhanTK.Text = TKMD.SoTiepNhan.ToString();
                    }
                    txtSoHopDong.Text = TKMD.SoHopDong;                    
                    txtLoaiHinh.Text= this.LoaiHinhMauDich_GetName(TKMD.MaLoaiHinh);
                    grbToKhai.Text = setText("Tờ khai xuất gia công", "Outsourcing Export declaration");
                    TKCT = null;
                }
                else
                {
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    TKCT.LoadHCTCollection();
                    txtNamDangKyTK.Text = TKCT.NgayTiepNhan.Year.ToString();
                    txtSoHopDong.Text = TKCT.SoHopDongDV;
                    txtSoTiepNhanTK.Text = TKCT.SoTiepNhan.ToString();
                    txtLoaiHinh.Text = LoaiHinhChuyenTiep_GetName(TKCT.MaLoaiHinh);
                    grbToKhai.Text = setText("Tờ khai chuyển tiếp xuất sản phẩm", "Export transition declaration");
                    TKMD = null;
                }
            }
            dgList.DataSource = this.BKCU.SanPhamCungUngCollection;           
        }

        private void cmMainNPLCungUng_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            switch (e.Command.Key)
            {
                case "cmdSave":
                    Save();
                    break;
                case "cmdAddSP":
                    ShowThemSPCungUngForm();
                    break;
               
            }
        }

        private void Save()
        {
            
            try
            {
                if (BKCU.SanPhamCungUngCollection.Count == 0)
                {
                    showMsg("MSG_2702013");
                    //ShowMessage("Bạn chưa chọn sản phẩm để khai cung ứng.", false);
                    return;
                }
                BKCU.InsertUpdateFull();
                showMsg("MSG_SAV02");
                //ShowMessage("Lưu thành công.", false);
                this.Close();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi: "+ ex.Message, false);
            }

        }

        private void ShowThemSPCungUngForm()
        {
            
            if (txtSoTiepNhanTK.Text.Trim().Length == 0)
                showMsg("MSG_2702014");
                //ShowMessage("Bạn hãy chọn tờ khai xuất gia công.", false);
            else
            {
                SanPhamCungUngRegisterForm f = new SanPhamCungUngRegisterForm();
                f.BKCU = this.BKCU;
                if (this.TKMD != null)
                {
                    f.TKMD = this.TKMD;
                    f.TKCT = null;
                }
                else
                {
                    f.TKCT = this.TKCT;
                    f.TKMD = null;
                }
                f.ShowDialog();
                if (f.SPCU.MaSanPham != "")
                {
                    this.BKCU.SanPhamCungUngCollection.Add(f.SPCU);
                }
                dgList.DataSource = this.BKCU.SanPhamCungUngCollection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }
        private HangMauDich GetHMDCuaToKhaiByMaHang(string maHang)
        {
            TKMD.LoadHMDCollection();
            foreach(HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private HangChuyenTiep GetHCTCuaToKhaiByMaHang(string maHang)
        {
            TKCT.LoadHCTCollection();
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            string maSP = e.Row.Cells["MaSanPham"].Text;
            if (TKMD != null)
            {
                HangMauDich hmd = GetHMDCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.DVT_ID);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
            else
            {
                HangChuyenTiep hmd = GetHCTCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.ID_DVT);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
        }
     
        

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhanCungUng SPCU = (SanPhanCungUng)e.Row.DataRow;
                SanPhamCungUngRegisterEditForm f = new SanPhamCungUngRegisterEditForm();
                f.BKCU = this.BKCU;
                f.SPCU = SPCU;
                if (SPCU.NPLCungUngCollection == null || SPCU.NPLCungUngCollection.Count == 0)
                {
                    SPCU.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
                    SPCU.LoadNPLCungUngCollection();
                }
                f.ShowDialog();
                SPCU = f.SPCU;
            }
        }

        private void NPLCungUngTheoTKSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            HopDong HD=new HopDong();             
            if(BKCU.TKCT_ID>0)
            {               
                HD.ID=TKCT.IDHopDong;
            }            
            else
            {                
                HD.ID=TKMD.IDHopDong;
            }
            if (showMsg("MSG_DEL01") == "Yes")
            //if (ShowMessage("Bạn có muốn xóa các sản phẩm này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhanCungUng spCungUng = (SanPhanCungUng)i.GetRow().DataRow;
                        if(spCungUng.ID>0)
                            spCungUng.Delete(HD);
                    }
                }
            }
            else
                e.Cancel = true;
        }

        private void grbToKhai_Click(object sender, EventArgs e)
        {

        }
    }
}