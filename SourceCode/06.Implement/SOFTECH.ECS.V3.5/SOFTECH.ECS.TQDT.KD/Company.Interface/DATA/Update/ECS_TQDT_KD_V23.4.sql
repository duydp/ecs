GO
IF OBJECT_ID(N'[dbo].[p_KDT_UpdateGUIDSTR]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_UpdateGUIDSTR]
GO
CREATE PROCEDURE [dbo].[p_KDT_UpdateGUIDSTR]
 @table varchar(max),  
 @guidstr nvarchar(max),  
 @where varchar(max)   
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = ' UPDATE '+@table+' SET GUIDSTR =N'+@guidstr+'  WHERE ' + @where      
              
EXEC sp_executesql @SQL   
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '23.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('23.4',GETDATE(), N'CẬP NHẬT UPDATE GUIDSTRING')
END