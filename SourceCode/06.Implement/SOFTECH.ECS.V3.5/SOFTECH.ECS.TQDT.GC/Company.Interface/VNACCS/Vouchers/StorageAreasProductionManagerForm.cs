﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class StorageAreasProductionManagerForm : BaseForm
    {
        public StorageAreasProductionManagerForm()
        {
            InitializeComponent();
        }

        private void StorageAreasProductionManagerForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            ctrCoQuanHaiQuan.BackColor = Color.Transparent;
            //BindData();
            cbStatus.SelectedValue = 0;
            btnTimKiem_Click(null,null);
        }
        public void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_StorageAreasProduction.SelectCollectionAll();
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN LIKE '%" + txtSoTiepNhan.Text +"%'";
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTN) = " + txtNamTiepNhan.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
                return null;
            }
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "-2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt khai báo sửa";
                        break;
                    case "-3":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        break;
                    case "4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                        break;
                    case "5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                        break;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_StorageAreasProduction storageAreasProduction = new KDT_VNACCS_StorageAreasProduction();
                storageAreasProduction = (KDT_VNACCS_StorageAreasProduction)e.Row.DataRow;
                ManufactureFactories f = new ManufactureFactories();
                f.storageAreasProduction = storageAreasProduction;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_StorageAreasProduction.SelectCollectionDynamic(GetSearchWhere(),null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdUpdateStatus":
                    UpdateStatus();
                    break;
            } 
        }
        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<KDT_VNACCS_StorageAreasProduction> StorageAreasProductionCollection = new List<KDT_VNACCS_StorageAreasProduction>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            StorageAreasProductionCollection.Add((KDT_VNACCS_StorageAreasProduction)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < StorageAreasProductionCollection.Count; i++)
                        {
                            Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                            f.CSSX = StorageAreasProductionCollection[i];
                            f.formType = "CSSX";
                            f.ShowDialog(this);
                        }
                        btnTimKiem_Click(null, null);
                    }
                    else
                    {
                        //showMsg("MSG_2702040", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
