/****** Object:  Table [dbo].[t_KDT_CX_TaiXuatDangKy]    Script Date: 11/10/2014 14:53:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_CX_TaiXuatDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](20) NULL,
	[LanThanhLy] [int] NULL,
	[TrangThaiXuLy] [int] NULL,
	[GUIDSTR] [nvarchar](200) NULL,
 CONSTRAINT [PK_T_KDT_CX_TaiXuatDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuatDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_CX_TaiXuatDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@LanThanhLy,
	@TrangThaiXuLy,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200)
AS

UPDATE
	[dbo].[t_KDT_CX_TaiXuatDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[LanThanhLy] = @LanThanhLy,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CX_TaiXuatDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CX_TaiXuatDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[LanThanhLy] = @LanThanhLy,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CX_TaiXuatDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[LanThanhLy],
			[TrangThaiXuLy],
			[GUIDSTR]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@LanThanhLy,
			@TrangThaiXuLy,
			@GUIDSTR
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CX_TaiXuatDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CX_TaiXuatDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_CX_TaiXuatDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM [dbo].[t_KDT_CX_TaiXuatDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuatDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuatDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_CX_TaiXuatDangKy]	

GO

/****** Object:  Table [dbo].[t_KDT_CX_TaiXuat]    Script Date: 11/10/2014 14:53:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_CX_TaiXuat](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NULL,
	[TKMD_ID] [bigint] NULL,
	[SoToKhai] [numeric](18, 0) NULL,
	[MaLoaiHinh] [varchar](6) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaHang] [nvarchar](80) NULL,
	[TenHang] [nvarchar](255) NULL,
	[LuongTaiXuat] [decimal](24, 6) NULL,
	[LoaiHang] [int] NULL,
	[DVT] [varchar](6) NULL,
	[Temp] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_CX_TaiXuat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_TaiXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_TaiXuat_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Insert]
	@Master_id bigint,
	@TKMD_ID bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTaiXuat decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_CX_TaiXuat]
(
	[Master_id],
	[TKMD_ID],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTaiXuat],
	[LoaiHang],
	[DVT],
	[Temp]
)
VALUES 
(
	@Master_id,
	@TKMD_ID,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHaiQuan,
	@MaHang,
	@TenHang,
	@LuongTaiXuat,
	@LoaiHang,
	@DVT,
	@Temp
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Update]
	@ID bigint,
	@Master_id bigint,
	@TKMD_ID bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTaiXuat decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_CX_TaiXuat]
SET
	[Master_id] = @Master_id,
	[TKMD_ID] = @TKMD_ID,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHaiQuan] = @MaHaiQuan,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[LuongTaiXuat] = @LuongTaiXuat,
	[LoaiHang] = @LoaiHang,
	[DVT] = @DVT,
	[Temp] = @Temp
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@TKMD_ID bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTaiXuat decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CX_TaiXuat] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CX_TaiXuat] 
		SET
			[Master_id] = @Master_id,
			[TKMD_ID] = @TKMD_ID,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHaiQuan] = @MaHaiQuan,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[LuongTaiXuat] = @LuongTaiXuat,
			[LoaiHang] = @LoaiHang,
			[DVT] = @DVT,
			[Temp] = @Temp
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CX_TaiXuat]
		(
			[Master_id],
			[TKMD_ID],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHaiQuan],
			[MaHang],
			[TenHang],
			[LuongTaiXuat],
			[LoaiHang],
			[DVT],
			[Temp]
		)
		VALUES 
		(
			@Master_id,
			@TKMD_ID,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHaiQuan,
			@MaHang,
			@TenHang,
			@LuongTaiXuat,
			@LoaiHang,
			@DVT,
			@Temp
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CX_TaiXuat]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CX_TaiXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[TKMD_ID],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTaiXuat],
	[LoaiHang],
	[DVT],
	[Temp]
FROM
	[dbo].[t_KDT_CX_TaiXuat]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[TKMD_ID],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTaiXuat],
	[LoaiHang],
	[DVT],
	[Temp]
FROM [dbo].[t_KDT_CX_TaiXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_TaiXuat_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_TaiXuat_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[TKMD_ID],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTaiXuat],
	[LoaiHang],
	[DVT],
	[Temp]
FROM
	[dbo].[t_KDT_CX_TaiXuat]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.4',GETDATE(), N' Bo sung bang ke tai xuat')
END