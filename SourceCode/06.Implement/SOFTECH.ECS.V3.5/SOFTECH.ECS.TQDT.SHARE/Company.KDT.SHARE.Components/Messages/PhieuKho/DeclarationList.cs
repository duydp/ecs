﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.PhieuKho
{
    [XmlRoot("DeclarationList")]
    public class DeclarationList
    {
        /// <summary>
        /// Số Tờ Khai
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }
        /// <summary>
        /// Ngày đăng ký tờ khai
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
        /// <summary>
        /// Mã HQ đăng ký TK
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
        /// <summary>
        /// Mã cục hq đăng ký tk
        /// </summary>
        [XmlElement("declarationHeadOffice")]
        public string DeclarationHeadOffice { get; set; }
        /// <summary>
        /// Mã loại hình
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        /// <summary>
        /// Mã HS đại diện cho tờ khai
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Tên hàng đại diện cho tờ khai
        /// </summary>
        [XmlElement("identificationname")]
        public string Identificationname { get; set; }
        /// <summary>
        /// Tổng số kiện
        /// </summary>
        [XmlElement("packagingQuanlity")]
        public string PackagingQuanlity { get; set; }
        /// <summary>
        /// DVT kiện
        /// </summary>
        [XmlElement("packagingUnit")]
        public string PackagingUnit { get; set; }
        /// <summary>
        /// Tổng trọng lượng
        /// </summary>
        [XmlElement("totalGrossMass")]
        public string TotalGrossMass { get; set; }
        /// <summary>
        /// DVT trọng lượng
        /// </summary>
        [XmlElement("totalGrossMassUnit")]
        public string TotalGrossMassUnit { get; set; }
        /// <summary>
        /// Tổng trị giá khai báo
        /// </summary>
        [XmlElement("totalcustomsValue")]
        public string TotalcustomsValue { get; set; }
        /// <summary>
        /// Đồng tiền thanh toán
        /// </summary>
        [XmlElement("currencyType")]
        public string CurrencyType { get; set; }
        /// <summary>
        /// Tỷ giá giữa đồng tiền thanh toán và VND
        /// </summary>
        [XmlElement("rateVND")]
        public string RateVND { get; set; }
        /// <summary>
        /// Tỷ giá giữa đồn tiền thanh toán và USD
        /// </summary>
        [XmlElement("rateUSD")]
        public string RateUSD { get; set; }
        /// <summary>
        /// Mã địa điểm xếp hàng
        /// </summary>
        [XmlElement("queueDestination")]
        public string QueueDestination { get; set; }
        /// <summary>
        /// Địa điểm đích vận chuyển bảo thuế
        /// </summary>
        [XmlElement("deliveryDestination")]
        public string DeliveryDestination { get; set; }
        /// <summary>
        /// Ngày đến địa điểm đích
        /// </summary>
        [XmlElement("deliveryDestinationDate")]
        public string DeliveryDestinationDate { get; set; }

        /// <summary>
        /// Số Invoid
        /// </summary>
        [XmlElement("invoiceNo")]
        public string InvoiceNo { get; set; }
        /// <summary>
        /// Ngày invoice
        /// </summary>
        [XmlElement("invoiceDate")]
        public string InvoiceDate { get; set; }

        /// <summary>
        /// Luồng của tờ khai
        /// </summary>
        [XmlElement("pluongInformation")]
        public string PluongInformation { get; set; }
    }
    
}
