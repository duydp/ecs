﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class MessageInfo
    {
        public bool IsError { get; set; }
        //public string MessageID { get; set; }
        public string SoTiepNhan { get; set; }
        public DateTime NgayTiepNhan { get; set; }
        public DateTime NgayDangKy { get; set; }
        public int SoToKhai { get; set; }
        public int TrangThaiXuly { get; set; }
        public string PhanLuong { get; set; }
        public string HuongDan { get; set; }
        public string ThongBao { get; set; }
        public int NamDK { get; set; }
        //public string MaHaiQuan { get; set; }
        //public string MaDoanhNghiep { get; set; }
        public string GUIDSTR { get; set; }
        public bool IsAnDinhThue { get; set; }
       // public AnDinhThue AnDinhThue { get; set; }
        [XmlElement("Header")]
        public MessageHeader MessageHeader { get; set; }

       
    }    
}
