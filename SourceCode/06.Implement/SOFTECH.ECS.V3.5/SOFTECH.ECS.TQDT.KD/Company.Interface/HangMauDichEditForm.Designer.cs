﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class HangMauDichEditForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox uiGroupBox4;
        private UIButton btnGhi;
        private Label label18;
        private ToolTip toolTip1;
        private Label lblTyGiaTT;
        private Label lblNguyenTe_DGNT;
        private Label lblNguyenTe_TGNT;
        private ErrorProvider epError;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangMauDichEditForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkMienThue = new Janus.Windows.EditControls.UICheckBox();
            this.grbThue = new Janus.Windows.EditControls.UIGroupBox();
            this.txtPhuThu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTyLeThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTSVatGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSTTDBGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSXNKGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTongSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTienThue_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTien_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTL_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.lblTyGiaTT = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDonGiaTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTriGiaKB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.ctrNuocXX = new Company.Interface.Controls.NuocHControl();
            this.lblNguyenTe_TGNT = new System.Windows.Forms.Label();
            this.lblNguyenTe_DGNT = new System.Windows.Forms.Label();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkThueTuyetDoi = new Janus.Windows.EditControls.UICheckBox();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.chkHangFOC = new Janus.Windows.EditControls.UICheckBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.revMaHS = new Company.Controls.CustomValidation.RegularExpressionValidator();
            this.rvDGNT = new Company.Controls.CustomValidation.RangeValidator();
            this.vrTGTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator4 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator3 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator2 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator1 = new Company.Controls.CustomValidation.RangeValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.rvLuong = new Company.Controls.CustomValidation.RangeValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).BeginInit();
            this.grbThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrTGTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(724, 368);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.chkMienThue);
            this.uiGroupBox1.Controls.Add(this.grbThue);
            this.uiGroupBox1.Controls.Add(this.btnDelete);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.lblTyGiaTT);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(724, 368);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkMienThue
            // 
            this.chkMienThue.BackColor = System.Drawing.Color.Transparent;
            this.chkMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMienThue.ForeColor = System.Drawing.Color.Red;
            this.chkMienThue.Location = new System.Drawing.Point(621, 3);
            this.chkMienThue.Name = "chkMienThue";
            this.chkMienThue.Size = new System.Drawing.Size(78, 23);
            this.chkMienThue.TabIndex = 8;
            this.chkMienThue.Text = "Miễn thuế";
            this.chkMienThue.VisualStyleManager = this.vsmMain;
            this.chkMienThue.CheckedChanged += new System.EventHandler(this.chkMienThue_CheckedChanged_1);
            // 
            // grbThue
            // 
            this.grbThue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbThue.BackColor = System.Drawing.Color.Transparent;
            this.grbThue.Controls.Add(this.txtPhuThu);
            this.grbThue.Controls.Add(this.label29);
            this.grbThue.Controls.Add(this.txtTyLeThuKhac);
            this.grbThue.Controls.Add(this.label30);
            this.grbThue.Controls.Add(this.txtTSVatGiam);
            this.grbThue.Controls.Add(this.txtTSTTDBGiam);
            this.grbThue.Controls.Add(this.txtTSXNKGiam);
            this.grbThue.Controls.Add(this.label21);
            this.grbThue.Controls.Add(this.label24);
            this.grbThue.Controls.Add(this.label25);
            this.grbThue.Controls.Add(this.txtTongSoTienThue);
            this.grbThue.Controls.Add(this.label20);
            this.grbThue.Controls.Add(this.txtTienThue_TTDB);
            this.grbThue.Controls.Add(this.txtTienThue_GTGT);
            this.grbThue.Controls.Add(this.txtTien_CLG);
            this.grbThue.Controls.Add(this.txtTienThue_NK);
            this.grbThue.Controls.Add(this.txtTGTT_TTDB);
            this.grbThue.Controls.Add(this.txtTGTT_GTGT);
            this.grbThue.Controls.Add(this.txtCLG);
            this.grbThue.Controls.Add(this.txtTGTT_NK);
            this.grbThue.Controls.Add(this.txtTL_CLG);
            this.grbThue.Controls.Add(this.txtTS_TTDB);
            this.grbThue.Controls.Add(this.txtTS_GTGT);
            this.grbThue.Controls.Add(this.txtTS_NK);
            this.grbThue.Controls.Add(this.label16);
            this.grbThue.Controls.Add(this.label17);
            this.grbThue.Controls.Add(this.label19);
            this.grbThue.Controls.Add(this.label1);
            this.grbThue.Controls.Add(this.label5);
            this.grbThue.Controls.Add(this.label9);
            this.grbThue.Controls.Add(this.label10);
            this.grbThue.Controls.Add(this.label11);
            this.grbThue.Controls.Add(this.label12);
            this.grbThue.Controls.Add(this.label13);
            this.grbThue.Controls.Add(this.label14);
            this.grbThue.Controls.Add(this.label15);
            this.grbThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThue.Location = new System.Drawing.Point(356, 8);
            this.grbThue.Name = "grbThue";
            this.grbThue.Size = new System.Drawing.Size(356, 269);
            this.grbThue.TabIndex = 9;
            this.grbThue.Text = "Phần thuế (VND)";
            this.grbThue.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbThue.VisualStyleManager = this.vsmMain;
            // 
            // txtPhuThu
            // 
            this.txtPhuThu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPhuThu.DecimalDigits = 3;
            this.txtPhuThu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuThu.Location = new System.Drawing.Point(185, 215);
            this.txtPhuThu.Name = "txtPhuThu";
            this.txtPhuThu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhuThu.Size = new System.Drawing.Size(164, 21);
            this.txtPhuThu.TabIndex = 39;
            this.txtPhuThu.TabStop = false;
            this.txtPhuThu.Text = "0.000";
            this.txtPhuThu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuThu.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtPhuThu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhuThu.VisualStyleManager = this.vsmMain;
            this.txtPhuThu.Leave += new System.EventHandler(this.txtPhuThu_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(186, 199);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 38;
            this.label29.Text = "Tiền thu khác";
            // 
            // txtTyLeThuKhac
            // 
            this.txtTyLeThuKhac.DecimalDigits = 8;
            this.txtTyLeThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeThuKhac.Location = new System.Drawing.Point(18, 215);
            this.txtTyLeThuKhac.Name = "txtTyLeThuKhac";
            this.txtTyLeThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyLeThuKhac.Size = new System.Drawing.Size(158, 21);
            this.txtTyLeThuKhac.TabIndex = 37;
            this.txtTyLeThuKhac.TabStop = false;
            this.txtTyLeThuKhac.Text = "0.00000000";
            this.txtTyLeThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyLeThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtTyLeThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeThuKhac.VisualStyleManager = this.vsmMain;
            this.txtTyLeThuKhac.Leave += new System.EventHandler(this.txtTyLeThuKhac_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(18, 199);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 13);
            this.label30.TabIndex = 36;
            this.label30.Text = "Tỷ lệ thu khác";
            // 
            // txtTSVatGiam
            // 
            this.txtTSVatGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSVatGiam.Location = new System.Drawing.Point(183, 131);
            this.txtTSVatGiam.MaxLength = 80;
            this.txtTSVatGiam.Name = "txtTSVatGiam";
            this.txtTSVatGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSVatGiam.TabIndex = 21;
            this.txtTSVatGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSVatGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSVatGiam.VisualStyleManager = this.vsmMain;
            // 
            // txtTSTTDBGiam
            // 
            this.txtTSTTDBGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSTTDBGiam.Location = new System.Drawing.Point(183, 85);
            this.txtTSTTDBGiam.MaxLength = 80;
            this.txtTSTTDBGiam.Name = "txtTSTTDBGiam";
            this.txtTSTTDBGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSTTDBGiam.TabIndex = 13;
            this.txtTSTTDBGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSTTDBGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSTTDBGiam.VisualStyleManager = this.vsmMain;
            // 
            // txtTSXNKGiam
            // 
            this.txtTSXNKGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSXNKGiam.Location = new System.Drawing.Point(182, 38);
            this.txtTSXNKGiam.MaxLength = 80;
            this.txtTSXNKGiam.Name = "txtTSXNKGiam";
            this.txtTSXNKGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSXNKGiam.TabIndex = 5;
            this.txtTSXNKGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSXNKGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSXNKGiam.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(182, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "TS Giảm";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(182, 69);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "TS Giảm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(182, 115);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "TS Giảm";
            // 
            // txtTongSoTienThue
            // 
            this.txtTongSoTienThue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongSoTienThue.DecimalDigits = 0;
            this.txtTongSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThue.Location = new System.Drawing.Point(135, 239);
            this.txtTongSoTienThue.Name = "txtTongSoTienThue";
            this.txtTongSoTienThue.ReadOnly = true;
            this.txtTongSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThue.Size = new System.Drawing.Size(214, 21);
            this.txtTongSoTienThue.TabIndex = 31;
            this.txtTongSoTienThue.TabStop = false;
            this.txtTongSoTienThue.Text = "0";
            this.txtTongSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoTienThue.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 247);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Tổng số tiền thuế";
            // 
            // txtTienThue_TTDB
            // 
            this.txtTienThue_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_TTDB.DecimalDigits = 0;
            this.txtTienThue_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_TTDB.Location = new System.Drawing.Point(239, 85);
            this.txtTienThue_TTDB.Name = "txtTienThue_TTDB";
            this.txtTienThue_TTDB.ReadOnly = true;
            this.txtTienThue_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_TTDB.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_TTDB.TabIndex = 15;
            this.txtTienThue_TTDB.TabStop = false;
            this.txtTienThue_TTDB.Text = "0";
            this.txtTienThue_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_GTGT
            // 
            this.txtTienThue_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_GTGT.DecimalDigits = 0;
            this.txtTienThue_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_GTGT.Location = new System.Drawing.Point(239, 131);
            this.txtTienThue_GTGT.Name = "txtTienThue_GTGT";
            this.txtTienThue_GTGT.ReadOnly = true;
            this.txtTienThue_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_GTGT.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_GTGT.TabIndex = 23;
            this.txtTienThue_GTGT.TabStop = false;
            this.txtTienThue_GTGT.Text = "0";
            this.txtTienThue_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtTien_CLG
            // 
            this.txtTien_CLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTien_CLG.DecimalDigits = 0;
            this.txtTien_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTien_CLG.Location = new System.Drawing.Point(239, 177);
            this.txtTien_CLG.Name = "txtTien_CLG";
            this.txtTien_CLG.ReadOnly = true;
            this.txtTien_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTien_CLG.Size = new System.Drawing.Size(110, 21);
            this.txtTien_CLG.TabIndex = 29;
            this.txtTien_CLG.TabStop = false;
            this.txtTien_CLG.Text = "0";
            this.txtTien_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTien_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTien_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTien_CLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(239, 38);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.ReadOnly = true;
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(107, 21);
            this.txtTienThue_NK.TabIndex = 7;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_NK.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_TTDB
            // 
            this.txtTGTT_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_TTDB.DecimalDigits = 0;
            this.txtTGTT_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_TTDB.Location = new System.Drawing.Point(18, 85);
            this.txtTGTT_TTDB.Name = "txtTGTT_TTDB";
            this.txtTGTT_TTDB.ReadOnly = true;
            this.txtTGTT_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_TTDB.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_TTDB.TabIndex = 9;
            this.txtTGTT_TTDB.TabStop = false;
            this.txtTGTT_TTDB.Text = "0";
            this.txtTGTT_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_GTGT
            // 
            this.txtTGTT_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_GTGT.DecimalDigits = 0;
            this.txtTGTT_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_GTGT.Location = new System.Drawing.Point(18, 131);
            this.txtTGTT_GTGT.Name = "txtTGTT_GTGT";
            this.txtTGTT_GTGT.ReadOnly = true;
            this.txtTGTT_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_GTGT.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_GTGT.TabIndex = 17;
            this.txtTGTT_GTGT.TabStop = false;
            this.txtTGTT_GTGT.Text = "0";
            this.txtTGTT_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtCLG
            // 
            this.txtCLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCLG.DecimalDigits = 0;
            this.txtCLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCLG.Location = new System.Drawing.Point(18, 177);
            this.txtCLG.Name = "txtCLG";
            this.txtCLG.ReadOnly = true;
            this.txtCLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCLG.Size = new System.Drawing.Size(108, 21);
            this.txtCLG.TabIndex = 25;
            this.txtCLG.TabStop = false;
            this.txtCLG.Text = "0";
            this.txtCLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtCLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(18, 38);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_NK.TabIndex = 1;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_NK.VisualStyleManager = this.vsmMain;
            this.txtTGTT_NK.Leave += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // txtTL_CLG
            // 
            this.txtTL_CLG.DecimalDigits = 3;
            this.txtTL_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTL_CLG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTL_CLG.Location = new System.Drawing.Point(135, 177);
            this.txtTL_CLG.MaxLength = 10;
            this.txtTL_CLG.Name = "txtTL_CLG";
            this.txtTL_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTL_CLG.Size = new System.Drawing.Size(41, 21);
            this.txtTL_CLG.TabIndex = 27;
            this.txtTL_CLG.Text = "0";
            this.txtTL_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTL_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTL_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTL_CLG.VisualStyleManager = this.vsmMain;
            this.txtTL_CLG.Leave += new System.EventHandler(this.txtTS_CLG_Leave);
            // 
            // txtTS_TTDB
            // 
            this.txtTS_TTDB.DecimalDigits = 3;
            this.txtTS_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_TTDB.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_TTDB.Location = new System.Drawing.Point(135, 85);
            this.txtTS_TTDB.MaxLength = 10;
            this.txtTS_TTDB.Name = "txtTS_TTDB";
            this.txtTS_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_TTDB.Size = new System.Drawing.Size(41, 21);
            this.txtTS_TTDB.TabIndex = 11;
            this.txtTS_TTDB.Text = "0";
            this.txtTS_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_TTDB.VisualStyleManager = this.vsmMain;
            this.txtTS_TTDB.Leave += new System.EventHandler(this.txtTS_TTDB_Leave);
            // 
            // txtTS_GTGT
            // 
            this.txtTS_GTGT.DecimalDigits = 3;
            this.txtTS_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_GTGT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_GTGT.Location = new System.Drawing.Point(135, 131);
            this.txtTS_GTGT.MaxLength = 10;
            this.txtTS_GTGT.Name = "txtTS_GTGT";
            this.txtTS_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_GTGT.Size = new System.Drawing.Size(41, 21);
            this.txtTS_GTGT.TabIndex = 19;
            this.txtTS_GTGT.Text = "0";
            this.txtTS_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_GTGT.VisualStyleManager = this.vsmMain;
            this.txtTS_GTGT.Leave += new System.EventHandler(this.txtTS_GTGT_Leave);
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 3;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_NK.Location = new System.Drawing.Point(135, 38);
            this.txtTS_NK.MaxLength = 10;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(41, 21);
            this.txtTS_NK.TabIndex = 3;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_NK.VisualStyleManager = this.vsmMain;
            this.txtTS_NK.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Trị giá tính thuế NK";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(236, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Tiền thuế NK";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(135, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "TS (%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Trị giá tính thuế TTĐB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Trị giá tính thuế GTGT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Chênh lệch giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(135, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "TS (%)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(135, 115);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "TS (%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(135, 161);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Tỷ lệ (%)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(236, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Tiền thuế TTĐB";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(236, 115);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Tiền thuế GTGT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(236, 161);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Tiền chênh lệch giá";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(358, 298);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(644, 298);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTyGiaTT.AutoSize = true;
            this.lblTyGiaTT.BackColor = System.Drawing.Color.Transparent;
            this.lblTyGiaTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyGiaTT.Location = new System.Drawing.Point(356, 337);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Size = new System.Drawing.Size(61, 13);
            this.lblTyGiaTT.TabIndex = 4;
            this.lblTyGiaTT.Text = "Tỷ giá TT:";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.txtDonGiaTuyetDoi);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaKB);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_TGNT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_DGNT);
            this.uiGroupBox4.Controls.Add(this.txtLuong);
            this.uiGroupBox4.Controls.Add(this.txtDGNT);
            this.uiGroupBox4.Controls.Add(this.txtTGNT);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 162);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(338, 180);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(246, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 16;
            this.label26.Text = "(USD)";
            // 
            // txtDonGiaTuyetDoi
            // 
            this.txtDonGiaTuyetDoi.DecimalDigits = 20;
            this.txtDonGiaTuyetDoi.Enabled = false;
            this.txtDonGiaTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTuyetDoi.Location = new System.Drawing.Point(115, 70);
            this.txtDonGiaTuyetDoi.Name = "txtDonGiaTuyetDoi";
            this.txtDonGiaTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTuyetDoi.Size = new System.Drawing.Size(128, 21);
            this.txtDonGiaTuyetDoi.TabIndex = 8;
            this.txtDonGiaTuyetDoi.Text = "0";
            this.txtDonGiaTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDonGiaTuyetDoi, "Đơn giá khai báo");
            this.txtDonGiaTuyetDoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTuyetDoi.VisualStyleManager = this.vsmMain;
            this.txtDonGiaTuyetDoi.Leave += new System.EventHandler(this.txtDonGiaTuyetDoi_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 13);
            this.label28.TabIndex = 7;
            this.label28.Text = "Đơn giá tuyệt đối";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(246, 129);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 15;
            this.label23.Text = "(VNĐ)";
            // 
            // txtTriGiaKB
            // 
            this.txtTriGiaKB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaKB.DecimalDigits = 0;
            this.txtTriGiaKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKB.Location = new System.Drawing.Point(115, 124);
            this.txtTriGiaKB.Name = "txtTriGiaKB";
            this.txtTriGiaKB.ReadOnly = true;
            this.txtTriGiaKB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKB.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaKB.TabIndex = 14;
            this.txtTriGiaKB.TabStop = false;
            this.txtTriGiaKB.Text = "0";
            this.txtTriGiaKB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTriGiaKB, "Trị giá khai báo");
            this.txtTriGiaKB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaKB.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(16, 129);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 13;
            this.label22.Text = "Trị giá khai báo";
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXX.ErrorMessage = "\"Nước xuất xứ\" bắt buộc phải chọn.";
            this.ctrNuocXX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXX.Location = new System.Drawing.Point(115, 151);
            this.ctrNuocXX.Ma = "";
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.ReadOnly = false;
            this.ctrNuocXX.Size = new System.Drawing.Size(204, 22);
            this.ctrNuocXX.TabIndex = 1;
            this.ctrNuocXX.Tag = "NuocXuatXuHMD";
            this.ctrNuocXX.VisualStyleManager = this.vsmMain;
            // 
            // lblNguyenTe_TGNT
            // 
            this.lblNguyenTe_TGNT.AutoSize = true;
            this.lblNguyenTe_TGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_TGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_TGNT.Location = new System.Drawing.Point(246, 102);
            this.lblNguyenTe_TGNT.Name = "lblNguyenTe_TGNT";
            this.lblNguyenTe_TGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_TGNT.TabIndex = 12;
            this.lblNguyenTe_TGNT.Text = "Mã nguyên tệ";
            // 
            // lblNguyenTe_DGNT
            // 
            this.lblNguyenTe_DGNT.AutoSize = true;
            this.lblNguyenTe_DGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_DGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_DGNT.Location = new System.Drawing.Point(246, 49);
            this.lblNguyenTe_DGNT.Name = "lblNguyenTe_DGNT";
            this.lblNguyenTe_DGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_DGNT.TabIndex = 6;
            this.lblNguyenTe_DGNT.Text = "Mã nguyên tệ";
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 20;
            this.txtLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuong.Location = new System.Drawing.Point(115, 17);
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong.Size = new System.Drawing.Size(128, 21);
            this.txtLuong.TabIndex = 1;
            this.txtLuong.Tag = "SoLuongHMD";
            this.txtLuong.Text = "0";
            this.txtLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuong.VisualStyleManager = this.vsmMain;
            this.txtLuong.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // txtDGNT
            // 
            this.txtDGNT.DecimalDigits = 20;
            this.txtDGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDGNT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDGNT.Location = new System.Drawing.Point(115, 44);
            this.txtDGNT.Name = "txtDGNT";
            this.txtDGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDGNT.Size = new System.Drawing.Size(128, 21);
            this.txtDGNT.TabIndex = 5;
            this.txtDGNT.Tag = "DonGiaNTHMD";
            this.txtDGNT.Text = "0";
            this.txtDGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDGNT, "Đơn giá khai báo");
            this.txtDGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDGNT.VisualStyleManager = this.vsmMain;
            this.txtDGNT.Click += new System.EventHandler(this.txtDGNT_Click);
            this.txtDGNT.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // txtTGNT
            // 
            this.txtTGNT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGNT.DecimalDigits = 20;
            this.txtTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGNT.FormatString = "g20";
            this.txtTGNT.Location = new System.Drawing.Point(115, 97);
            this.txtTGNT.Name = "txtTGNT";
            this.txtTGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGNT.Size = new System.Drawing.Size(128, 21);
            this.txtTGNT.TabIndex = 11;
            this.txtTGNT.Tag = "TriGiaNTHMD";
            this.txtTGNT.Text = "0";
            this.txtTGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTGNT, "Trị giá khai báo");
            this.txtTGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGNT.VisualStyleManager = this.vsmMain;
            this.txtTGNT.Leave += new System.EventHandler(this.txtTGNT_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Đơn giá nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(16, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Trị giá nguyên tệ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 156);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Nước xuất xứ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.chkThueTuyetDoi);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.chkHangFOC);
            this.uiGroupBox2.Controls.Add(this.txtMaHang);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(338, 152);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // chkThueTuyetDoi
            // 
            this.chkThueTuyetDoi.BackColor = System.Drawing.Color.Transparent;
            this.chkThueTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkThueTuyetDoi.ForeColor = System.Drawing.Color.Blue;
            this.chkThueTuyetDoi.Location = new System.Drawing.Point(175, 124);
            this.chkThueTuyetDoi.Name = "chkThueTuyetDoi";
            this.chkThueTuyetDoi.Size = new System.Drawing.Size(107, 23);
            this.chkThueTuyetDoi.TabIndex = 28;
            this.chkThueTuyetDoi.Text = "Thuế tuyệt đối";
            this.chkThueTuyetDoi.VisualStyleManager = this.vsmMain;
            this.chkThueTuyetDoi.CheckedChanged += new System.EventHandler(this.chkThueTuyetDoi_CheckedChanged);
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(85, 97);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(234, 21);
            this.cbDonViTinh.TabIndex = 9;
            this.cbDonViTinh.Tag = "DonViTinhHMD";
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // chkHangFOC
            // 
            this.chkHangFOC.BackColor = System.Drawing.Color.Transparent;
            this.chkHangFOC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHangFOC.ForeColor = System.Drawing.Color.Blue;
            this.chkHangFOC.Location = new System.Drawing.Point(85, 123);
            this.chkHangFOC.Name = "chkHangFOC";
            this.chkHangFOC.Size = new System.Drawing.Size(74, 23);
            this.chkHangFOC.TabIndex = 27;
            this.chkHangFOC.Text = "Hàng FOC";
            this.chkHangFOC.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHang
            // 
            this.txtMaHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(85, 17);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(234, 21);
            this.txtMaHang.TabIndex = 1;
            this.txtMaHang.Tag = "MaHang";
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaHang_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã HS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(16, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(85, 44);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(234, 21);
            this.txtTenHang.TabIndex = 3;
            this.txtTenHang.Tag = "TenHang";
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHS
            // 
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(85, 71);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(234, 21);
            this.txtMaHS.TabIndex = 5;
            this.txtMaHS.Tag = "MaHSHMD";
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            this.txtMaHS.Leave += new System.EventHandler(this.txtMaHS_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Đơn vị tính";
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(563, 298);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 5;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenHang;
            this.rfvTen.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // revMaHS
            // 
            this.revMaHS.ControlToValidate = this.txtMaHS;
            this.revMaHS.ErrorMessage = "\"Mã HS\" không hợp lệ.";
            this.revMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("revMaHS.Icon")));
            this.revMaHS.Tag = "revMaHS";
            this.revMaHS.ValidationExpression = "\\d{10}";
            // 
            // rvDGNT
            // 
            this.rvDGNT.ControlToValidate = this.txtDGNT;
            this.rvDGNT.ErrorMessage = "Giá trị không hợp lệ";
            this.rvDGNT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvDGNT.Icon")));
            this.rvDGNT.MaximumValue = "1000000000000";
            this.rvDGNT.MinimumValue = "0";
            this.rvDGNT.Tag = "rvLuong";
            this.rvDGNT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // vrTGTT
            // 
            this.vrTGTT.ControlToValidate = this.txtTGTT_NK;
            this.vrTGTT.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.vrTGTT.Icon = ((System.Drawing.Icon)(resources.GetObject("vrTGTT.Icon")));
            this.vrTGTT.MaximumValue = "1000000000000";
            this.vrTGTT.MinimumValue = "0";
            this.vrTGTT.Tag = "vrTGTT";
            this.vrTGTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator4
            // 
            this.rangeValidator4.ControlToValidate = this.txtTS_NK;
            this.rangeValidator4.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator4.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator4.Icon")));
            this.rangeValidator4.MaximumValue = "1000000000000";
            this.rangeValidator4.MinimumValue = "0";
            this.rangeValidator4.Tag = "rangeValidator4";
            this.rangeValidator4.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator3
            // 
            this.rangeValidator3.ControlToValidate = this.txtTL_CLG;
            this.rangeValidator3.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator3.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator3.Icon")));
            this.rangeValidator3.MaximumValue = "1000000000000";
            this.rangeValidator3.MinimumValue = "0";
            this.rangeValidator3.Tag = "rangeValidator3";
            this.rangeValidator3.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator2
            // 
            this.rangeValidator2.ControlToValidate = this.txtTS_TTDB;
            this.rangeValidator2.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator2.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator2.Icon")));
            this.rangeValidator2.MaximumValue = "1000000000000";
            this.rangeValidator2.MinimumValue = "0";
            this.rangeValidator2.Tag = "rangeValidator2";
            this.rangeValidator2.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator1
            // 
            this.rangeValidator1.ControlToValidate = this.txtTS_GTGT;
            this.rangeValidator1.ErrorMessage = "Gía trị nhập chưa hợp lệ";
            this.rangeValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator1.Icon")));
            this.rangeValidator1.MaximumValue = "1000000000000";
            this.rangeValidator1.MinimumValue = "0";
            this.rangeValidator1.Tag = "rangeValidator1";
            this.rangeValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // rvLuong
            // 
            this.rvLuong.ControlToValidate = this.txtLuong;
            this.rvLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.rvLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rvLuong.Icon")));
            this.rvLuong.MaximumValue = "1000000000000";
            this.rvLuong.MinimumValue = "0";
            this.rvLuong.Tag = "rvLuong";
            this.rvLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // HangMauDichEditForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(724, 368);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HangMauDichEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sửa thông tin hàng hóa";
            this.Load += new System.EventHandler(this.HangMauDichEditForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).EndInit();
            this.grbThue.ResumeLayout(false);
            this.grbThue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrTGTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private ImageList ImageList1;
        private UIButton btnClose;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvTen;
        private RequiredFieldValidator rfvMaHS;
        private RegularExpressionValidator revMaHS;
        private UIButton btnDelete;
        private Label label23;
        private NumericEditBox txtTriGiaKB;
        private Label label22;
        private RangeValidator rvDGNT;
        private UICheckBox chkHangFOC;
        private UICheckBox chkThueTuyetDoi;
        private UIGroupBox grbThue;
        private EditBox txtTSVatGiam;
        private EditBox txtTSTTDBGiam;
        private EditBox txtTSXNKGiam;
        private Label label21;
        private Label label24;
        private Label label25;
        private NumericEditBox txtTongSoTienThue;
        private Label label20;
        private NumericEditBox txtTienThue_TTDB;
        private NumericEditBox txtTienThue_GTGT;
        private NumericEditBox txtTien_CLG;
        private NumericEditBox txtTienThue_NK;
        private NumericEditBox txtTGTT_TTDB;
        private NumericEditBox txtTGTT_GTGT;
        private NumericEditBox txtCLG;
        private NumericEditBox txtTGTT_NK;
        private NumericEditBox txtTL_CLG;
        private NumericEditBox txtTS_TTDB;
        private NumericEditBox txtTS_GTGT;
        private NumericEditBox txtTS_NK;
        private Label label16;
        private Label label17;
        private Label label19;
        private Label label1;
        private Label label5;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        public UICheckBox chkMienThue;
        private NumericEditBox txtDonGiaTuyetDoi;
        private Label label28;
        private RangeValidator vrTGTT;
        private RangeValidator rangeValidator4;
        private RangeValidator rangeValidator3;
        private RangeValidator rangeValidator2;
        private RangeValidator rangeValidator1;
        private CompareValidator cvSoLuong;
        private RangeValidator rvLuong;
        private Label label26;
        private EditBox txtMaHang;
        private EditBox txtTenHang;
        private EditBox txtMaHS;
        private UIComboBox cbDonViTinh;
        private NumericEditBox txtLuong;
        private NumericEditBox txtDGNT;
        private NumericEditBox txtTGNT;
        private NuocHControl ctrNuocXX;
        private NumericEditBox txtPhuThu;
        private Label label29;
        private NumericEditBox txtTyLeThuKhac;
        private Label label30;
    }
}
