﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using System.Threading;
using System.Timers;

namespace Company.Interface
{
    public partial class ThongBaoFormVNACCS : BaseForm
    {
        System.Timers.Timer timer1 = new System.Timers.Timer();
        #region design
        public ThongBaoFormVNACCS()
        {
            InitializeComponent();
            this.Deactivate += new EventHandler(ThongBaoForm_Deactivate);
            //this.Activated += new EventHandler(ThongBaoForm_Activated);
            timer1.Elapsed +=new ElapsedEventHandler(timer1_Tick);
        }
        void ThongBaoForm_Activated(object sender, EventArgs e)
        {
            //Load the Form At Position of Main Form

            int WidthOfMain = Application.OpenForms["MainForm"].Width;

            int HeightofMain = Application.OpenForms["MainForm"].Height;

            int LocationMainX = Application.OpenForms["MainForm"].Location.X;

            int locationMainy = Application.OpenForms["MainForm"].Location.Y;



            //Set the Location

            this.Location = new Point(LocationMainX + WidthOfMain - this.Width - 5, HeightofMain/2 - this.Height/2);



            //Animate form
           
            AnimateWindow(this.Handle, 500, AW_ACTIVATE | AW_SLIDE | AW_HOR_NEGATIVE);
            //this.Show();
           
            this.Focus();
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            if (this.Visible)
            {
                ThongBaoForm_Activated(this,e);
            }
            base.OnVisibleChanged(e);
        }
        public void ThongBaoForm_Deactivate(object sender, EventArgs e)
        {
            AnimateWindow(this.Handle, 500, AW_HIDE | AW_BLEND | AW_HOR_POSITIVE);
            this.Hide();
            this.Visible = false;
        }
        //Constants

        const int AW_SLIDE = 0X40000;

        const int AW_HOR_POSITIVE = 0X1;

        const int AW_HOR_NEGATIVE = 0x00000002;

        const int AW_BLEND = 0X80000;
        const int AW_HIDE = 0x00010000;
        const int AW_ACTIVATE = 0x00020000;
        //const int AW_CENTER = 0x00000010;
        //const int AW_BLEND = 0x00080000;
        [DllImport("user32")]
        static extern bool AnimateWindow(IntPtr hwnd, int time, int flags);
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        
        #endregion




        #region Flag property
        private bool isAutoRequest;
        private bool isStop;
        private int TimerRequestContinue = 0;
        private int ThoiGianConLai = 0;
        #endregion

        private void Setstatus(string status, bool isError)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        lblStatus.Text = status;
                        if (isError)
                            lblStatus.ForeColor = Color.Red;
                        else
                            lblStatus.ForeColor = Color.Black;
                    }));
            }
            else
                lblStatus.Text = status;
        }
        private void SetStatusImage(bool visible)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    iconStatus.Visible = visible;
                }));
            }
            else
                iconStatus.Visible = visible;
        }
         

        private void ThongBaoForm_Load(object sender, EventArgs e)
        {
            txtTimer.Value = GlobalVNACC.TimerRequest;
            txtTimer.Text = GlobalVNACC.TimerRequest.ToString();
            btnTuDongLayPH.Enabled = true;
            btnDungTuDongLayPH.Enabled = false;
            btnDungLayPhanHoi.Enabled = false;
            btnLayPhanHoi.Enabled = true;
            SetStatusImage(false);
            ucRespone1.UcEvent += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_UcEvent);
            ucRespone1.bindingThongBao();
            ucRespone1.HandlerID += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_HandlerID);
        }

        void ucRespone1_HandlerID(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            ProcessReport.ShowReport(Convert.ToInt64(e.Data), e.Message);
        }

        void ucRespone1_UcEvent(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            Setstatus(e.Message, e.isError);
            if (e.isError) SetStatusImage(false);
            if(!e.isError && e.isFinish) 
            {
                if (isAutoRequest)
                {
                    timer1.Enabled = true;
                    ThoiGianConLai = TimerRequestContinue;
                }
                else
                {
                    
                }
                //timer1.Start();
            }
        }

        private void btnTuDongLayPH_Click(object sender, EventArgs e)
        {
            isAutoRequest = true;
            isStop = false;
            TimerRequestContinue = Convert.ToInt32(txtTimer.Value) * 60;
            if(TimerRequestContinue < 180)
            {
                this.ShowMessage("Thời gian tối thiểu giữa các lần lấy tự động là 3 phút", false);
                return;
            }
            btnTuDongLayPH.Enabled = false;
            btnDungTuDongLayPH.Enabled = true;
            btnDungLayPhanHoi.Enabled = false;
            btnLayPhanHoi.Enabled = false;
            GlobalVNACC.isStopRespone = false;
            timer1.Interval = 1000;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!GlobalVNACC.isStopRespone)
            {
                if (isStop)
                {
                    Setstatus("Dừng lấy phản hồi", false);
                    //timer1.Stop();
                    timer1.Enabled = false;
                    SetStatusbtn(true);
                }
                else
                {
                    if (ThoiGianConLai == 0)
                    {
                        //timer1.Stop();
                        timer1.Enabled = false;
                        ThreadPool.QueueUserWorkItem(ucRespone1.DoWork);
                    }
                    else
                    {
                        ThoiGianConLai--;
                        TimeSpan time = new TimeSpan(0, 0, ThoiGianConLai);
                        string status = string.Format("TG còn lại trước khi nhận phản hồi: {0}", time.ToString());
                        Setstatus(status, false);
                    }
                }
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            SetStatusbtn(false);
            isAutoRequest = false;
            ThreadPool.QueueUserWorkItem(ucRespone1.DoWork);
        }

        private void btnDungTuDongLayPH_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    btnDungTuDongLayPH.Enabled = false;
                    isStop = true;
                    ucRespone1.isStop = true;
                    timer1.Enabled = false;
                }));
            }
            else
            {
                btnDungTuDongLayPH.Enabled = false;
                isStop = true;
                timer1.Enabled = false;
            }
        }

        private void btnDungLayPhanHoi_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    btnDungLayPhanHoi.Enabled = false;
                    isStop = true;
                    ucRespone1.isStop = true;
                    timer1.Enabled = false;
                }));
            }
            else
            {
                btnDungLayPhanHoi.Enabled = false;
                isStop = true;
                ucRespone1.isStop = true;
                timer1.Enabled = false;
            }
        }
        private void SetStatusbtn(bool isDungLayPH)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    if (isDungLayPH)
                    {
                        btnTuDongLayPH.Enabled = true;
                        btnDungTuDongLayPH.Enabled = false;
                        btnDungLayPhanHoi.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }
                    else
                    {
                        btnTuDongLayPH.Enabled = false;
                        btnDungTuDongLayPH.Enabled = false;
                        btnDungLayPhanHoi.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                        isAutoRequest = false;
                        isStop = false;
                        isAutoRequest = false;
                        timer1.Enabled = false;
                    }
                }));
            }
            else
            {
                if (isDungLayPH)
                {
                    btnTuDongLayPH.Enabled = true;
                    btnDungTuDongLayPH.Enabled = false;
                    btnDungLayPhanHoi.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnTuDongLayPH.Enabled = false;
                    btnDungTuDongLayPH.Enabled = false;
                    btnDungLayPhanHoi.Enabled = true;
                    btnLayPhanHoi.Enabled = false;
                    isAutoRequest = false;
                    isStop = false;
                    isAutoRequest = false;
                }
            }
        }

    }
}
