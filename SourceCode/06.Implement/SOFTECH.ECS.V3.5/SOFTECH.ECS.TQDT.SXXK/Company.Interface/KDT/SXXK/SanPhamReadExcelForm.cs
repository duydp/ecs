using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamReadExcelForm : BaseForm
    {
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public SanPhamDangKy spDangKy = new SanPhamDangKy();
        public bool isEdit = false;
        public bool isCancel = false;
        public SanPhamReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC SHEET. DOANH NGHIỆP HÃY KIỂM TRA LẠI TÊN SHEET TRƯỚC KHI CHỌN FILE.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private int checkSPExit(string maSP)
        {
            for (int i = 0; i < this.SPCollection.Count; i++)
            {
                if (this.SPCollection[i].Ma == maSP) return i;
            }
            return -1;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {

                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                }
                else
                {
                    error.SetError(txtRow, "BEGIN ROW MUST BE GREATER THAN ZERO ");
                }
                error.SetIconPadding(txtRow, -8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
            }
            catch
            {
                MLMessages("LỖI KHI ĐỌC FILE. BẠN HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("KHÔNG TỒN TẠI SHEET \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaSpecialChar = "";
            string errorMaHangHoaExits = "";
            string errorMaHangHoaNotExits = "";
            string errorMaHangHoaValid = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";

            string errorSP = "";

            List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
            List<SanPham> Collection = new List<SanPham>();
            Company.BLL.SXXK.SanPhamCollection SPCollection = Company.BLL.SXXK.SanPham.SelectCollectionDynamicBy(" MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        SanPham sp = new SanPham();
                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            sp.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                            if (sp.Ma.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                isAdd = false;
                            }
                            else if (Helpers.ValidateSpecialChar(sp.Ma))
                            {
                                errorMaHangHoaSpecialChar += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                isAdd = false;                                
                            }
                            else
                            {
                                // KIỂM TRA MÃ SP ĐÃ ĐĂNG KÝ
                                //Company.BLL.SXXK.SanPhamCollection SPCollection = Company.BLL.SXXK.SanPham.SelectCollectionDynamicBy("Ma='" + sp.Ma + "'", "");
                                if (isEdit || isCancel)
                                {
                                    foreach (Company.BLL.SXXK.SanPham item in SPCollection)
                                    {
                                        if (sp.Ma == item.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ SP ĐÃ ĐƯỢC NHẬP TRƯỚC ĐÓ BỎ QUA ĐÃ HỦY , ĐÃ DUYỆT , CHỜ DUYỆT
                                        SanPhamDangKyCollection SPDKCollection = SanPhamDangKy.SelectCollectionDynamicAll("MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND SP.Ma ='" + sp.Ma + "' AND SP_DK.ID NOT IN (" + spDangKy.ID + ") AND SP_DK.TrangThaiXuLy NOT IN (10,0,1)", "");
                                        if (SPDKCollection.Count >= 1)
                                        {
                                            errorSP += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]-[" + SPDKCollection[0].SoTiepNhan + "]-[" + SPDKCollection[0].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(SPDKCollection[0].TrangThaiXuLy).ToUpper() + "]\n";
                                            isExits = true;
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ SP ĐÃ NHẬP TRÊN LƯỚI
                                        foreach (SanPham item in spDangKy.SPCollection)
                                        {
                                            if (sp.Ma == item.Ma)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (isExits)
                                        {
                                            errorMaHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMaHangHoaNotExits += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                                else
                                {
                                    foreach (Company.BLL.SXXK.SanPham item in SPCollection)
                                    {
                                        if (sp.Ma == item.Ma)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ SP ĐÃ ĐƯỢC NHẬP TRƯỚC ĐÓ BỎ QUA ĐÃ HỦY
                                        SanPhamDangKyCollection SPDKCollection = SanPhamDangKy.SelectCollectionDynamicAll("MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND SP.Ma ='" + sp.Ma + "' AND SP_DK.ID NOT IN (" + spDangKy.ID + ") AND SP_DK.TrangThaiXuLy NOT IN (10)", "");
                                        if (SPDKCollection.Count >= 1)
                                        {
                                            errorSP += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]-[" + SPDKCollection[0].SoTiepNhan + "]-[" + SPDKCollection[0].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(SPDKCollection[0].TrangThaiXuLy).ToUpper() + "]\n";
                                            isExits = true;
                                            isAdd = false;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        // KIỂM TRA MÃ SP ĐÃ NHẬP TRÊN LƯỚI
                                        foreach (SanPham item in spDangKy.SPCollection)
                                        {
                                            if (sp.Ma == item.Ma)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (isExits)
                                        {
                                            errorMaHangHoaValid += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + sp.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            sp.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                            if (sp.Ten.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + sp.Ten + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + sp.Ten + "]\n";
                            isAdd = false;  
                        }
                        try
                        {
                            isExits = false;
                            sp.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (sp.MaHS.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + sp.MaHS + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_HSCode item in HSCollection)
                                {
                                    if (item.HSCode == sp.MaHS)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + sp.MaHS + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + sp.MaHS + "]\n";
                            isAdd = false;
                        }
                        string DonViTinh = String.Empty;
                        try
                        {
                            isExits = false;
                            DonViTinh = Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper());
                            if (DonViTinh.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (HaiQuan_DonViTinh item in DVTCollection)
                                {
                                    if (item.Ten == DonViTinh)
                                    {
                                        sp.DVT_ID = DonViTinh_GetID(DonViTinh);
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                            isAdd = false;
                        }
                        if (isAdd)
                            Collection.Add(sp);
                    }
                    catch (Exception ex)
                    {
                        this.SaveDefault();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoaSpecialChar))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG ";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " ĐÃ ĐƯỢC ĐĂNG KÝ\n ";
            if (!String.IsNullOrEmpty(errorMaHangHoaNotExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaNotExits + " CHƯA ĐƯỢC ĐĂNG KÝ .MÃ HÀNG HÓA PHẢI ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY\n ";
            if (!String.IsNullOrEmpty(errorMaHangHoaValid))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaValid + " ĐÃ NHẬP LIỆU CÓ TRÊN LƯỚI \n";
            if (!String.IsNullOrEmpty(errorSP))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorSP + " ĐÃ ĐƯỢC NHẬP LIỆU TRƯỚC ĐÓ .\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ \n";
            if (!String.IsNullOrEmpty(errorMaHS))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHSExits))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " KHÔNG HỢP LỆ\n";
            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (SanPham item in Collection)
                {
                    this.SPCollection.Add(item);
                }
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG ", false);
            }
            else
            {
                ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }
            this.SaveDefault();
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultSP(cbbSheetName, txtRow, txtMaHangColumn, txtTenHangColumn, txtMaHSColumn, txtDVTColumn, null, null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigSP(txtRow.Text, txtMaHangColumn.Text, txtTenHangColumn.Text, txtMaHSColumn.Text, txtDVTColumn.Text, String.Empty, String.Empty, String.Empty);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_SXXK("SP");
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}