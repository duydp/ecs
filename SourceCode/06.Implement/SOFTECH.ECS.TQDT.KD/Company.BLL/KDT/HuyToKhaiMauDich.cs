﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.QuanLyChungTu.WS;
using Company.KDT.SHARE.Components;

namespace Company.KD.BLL.KDT
{
    public partial class HuyToKhai
    {
        #region Huy va chinh sua to khai da duyet

        //public string huyToKhai(string pass,ToKhaiMauDich tkmd)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    if (tkmd.MaLoaiHinh.StartsWith("N"))
        //        doc.LoadXml(tkmd.GenerateToXmlEnvelope(MessgaseType.ToKhaiNhap, MessgaseFunction.KhaiBao, new System.Guid()));
        //    else
        //        doc.LoadXml(tkmd.GenerateToXmlEnvelope(MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao, new System.Guid()));

        //    XmlDocument xmlDocument = new XmlDocument();
        //    string path = EntityBase.GetPathProgram();
        //    xmlDocument.Load(path + @"\XMLChungTu\HuyTKDaDuyet.xml");

        //    XmlNode root = doc.ImportNode(xmlDocument.SelectSingleNode("Root"), true);

        //    root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = tkmd.MaLoaiHinh;
        //    root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = tkmd.MaHaiQuan;

        //    root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = tkmd.SoToKhai.ToString("N0");
        //    root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = tkmd.NgayDangKy.ToString("MM/dd/yyyy"); ;
        //    root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = tkmd.NgayDangKy.Year.ToString("N0");

        //    root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = tkmd.MaDoanhNghiep;
        //    root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = tkmd.MaDoanhNghiep;

        //    XmlNode xmlNodeContent = doc.GetElementsByTagName("Content")[0];
        //    xmlNodeContent.AppendChild(root);

        //    WS.KhaiDienTu.KDTService kdt = new Company.KD.BLL.WS.KhaiDienTu.KDTService();
        //    kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            
        //    string kq = "";
        //    try
        //    {
        //        kq = kdt.Send(doc.InnerXml, pass);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
        //    }
        //    XmlDocument xmlDocumentResult = new XmlDocument();
        //    xmlDocumentResult.LoadXml(kq);
        //    if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
        //    {
        //        if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
        //        {
        //            return doc.InnerXml;
        //        }
        //    }
        //    else
        //    {
        //        throw new Exception(Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
        //    }
        //    return "";
        //}


        public string ConfigPhongBi(int type, int function, ToKhaiMauDich tkmd)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(tkmd.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = tkmd.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            string Guid = (System.Guid.NewGuid().ToString()); ;
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            if(this.Guid=="")
                this.Guid = Guid;
            nodeReference.InnerText = this.Guid;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = Guid;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = tkmd.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;

        }


        public string HuyToKhaiMD(string pass, ToKhaiMauDich tkmd)
        {
            //XmlDocument doc = new XmlDocument();
            //if (tkmd.MaLoaiHinh.StartsWith("N"))
            //    doc.LoadXml(ConfigPhongBi(MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao,tkmd));
            //else
            //    doc.LoadXml(ConfigPhongBi(MessageTypes.ToKhaiXuat, MessageFunctions.KhaiBao,tkmd));
            //XmlDocument docNPL = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            //docNPL.Load(path + @"\XMLChungTu\HuyTKDaDuyet.xml");

            //XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            ////root.SelectSingleNode("SoTKHQTC").InnerText = tkmd.SoTiepNhan.ToString();

            //root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = tkmd.MaLoaiHinh;
            //root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = tkmd.MaHaiQuan;

            //root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = tkmd.SoToKhai + "";
            //root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = tkmd.NgayDangKy.ToString("MM/dd/yyyy"); ;
            //root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = tkmd.NgayDangKy.Year + "";

            //root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = tkmd.MaDoanhNghiep.ToString();
            //root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = tkmd.MaDoanhNghiep;

            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);

            //WS.KhaiDienTu.KDTService kdt = new Company.KD.BLL.WS.KhaiDienTu.KDTService();
            //kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            //string kq = "";
            //try
            //{
            //    kq = kdt.Send(doc.InnerXml, pass);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            //}
            //XmlDocument docResult = new XmlDocument();
            //docResult.LoadXml(kq);
            //if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            //{
            //    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
            //    {
            //        return doc.InnerXml;
            //    }
            //}
            //else
            //{
            //    throw new Exception(Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            //}

            return "";
        }

        public string LayPhanHoi(string pass, ToKhaiMauDich tkmd)
        {
            //XmlDocument doc = new XmlDocument();
            //if (tkmd.MaLoaiHinh.StartsWith("N"))
            //    doc.LoadXml(ConfigPhongBi(MessgaseType.ThongTin, MessageFunction.LayPhanHoi, tkmd));
            //else
            //    doc.LoadXml(ConfigPhongBi(MessgaseType.ThongTin, MessageFunction.LayPhanHoi, tkmd));
            //XmlDocument docNPL = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            //docNPL.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");

            //XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            ////root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            //root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = tkmd.MaDoanhNghiep;
            //root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = tkmd.MaDoanhNghiep;

            //root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = tkmd.MaHaiQuan;
            //root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = tkmd.MaHaiQuan;

            //root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.Guid;

            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);

            //WS.KhaiDienTu.KDTService kdt = new Company.KD.BLL.WS.KhaiDienTu.KDTService();
            //kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            //string kq = "";
            //try
            //{
            //    kq = kdt.Send(doc.InnerXml, pass);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            //}
            //XmlDocument docResult = new XmlDocument();
            //docResult.LoadXml(kq);
            //if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            //{
            //    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "no")
            //    {
            //        return doc.InnerXml;
            //    }
            //}
            //else
            //{
            //    throw new Exception(Company.KD.BLL.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            //}
            //this.SoTiepNhan = Convert.ToInt32(docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTN"].Value);
            //this.NgayTiepNhan = Convert.ToDateTime(docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Ngay_TN"].Value);
            //this.Update();

            return "";

        }

        public bool WSLaySoTiepNhan(string password)
        {
            KDTService kdt = new KDTService();
            kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.Guid), MessageTypes.ThongTin, MessageFunctions.PhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(5000);
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        public bool WSLayPhanHoi(string password)
        {
            KDTService kdt = new KDTService();
            kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.PhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(5000);
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.PhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }

            return false;
        }

        #endregion Huy va chinh sua to khai da duyet
    }
}