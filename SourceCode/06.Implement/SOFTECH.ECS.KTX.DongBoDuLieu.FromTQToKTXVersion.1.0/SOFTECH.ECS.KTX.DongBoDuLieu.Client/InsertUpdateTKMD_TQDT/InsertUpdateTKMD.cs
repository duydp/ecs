﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using DongBo.InsertUpdateTKMD_TQDT;
using System.Threading;

namespace DongBo
{
    public partial class InsertUpdateTKMD : Form
    {
        public InsertUpdateTKMD()
        {
            InitializeComponent();
        }
   
        private void frmProcess_Load(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(DoUpdateData);
        }
        private void DoUpdateData(object obj)
        {
            InsertUpdateTQDT update = new InsertUpdateTQDT();
            update.InsertUpdateEventArgs += new EventHandler<InsertUpdateEventArgs>(update_InsertUpdateEventArgs);
            if (update.InsertTKMDFromSXXK(GlobalSettings.ConnectionStringTarget, Application.StartupPath + "\\InsertUpdateTKMD_TQDT"))
                update.InsertUpdate(GlobalSettings.ConnectionStringTarget);
        }

        void update_InsertUpdateEventArgs(object sender, InsertUpdateEventArgs e)
        {
            this.Invoke(new EventHandler<InsertUpdateEventArgs>(UpdateHandler),sender,e);
        }
        void UpdateHandler(object sender, InsertUpdateEventArgs e)
        {
            if (e.Error != null)
            {
                lblTrangThai.Text = "ERROR";
                lblError.Text = e.Error.Message;
            }
            else
            {
                lblNoiDung.Text = e.Message;
                prcbar.Value = e.Percent;
                lblTrangThai.Text = e.Percent + " %";
                lblSoToKhai.Text = e.SoToKhaiDuocCapNhat.ToString();
            }
        }
    }
}
