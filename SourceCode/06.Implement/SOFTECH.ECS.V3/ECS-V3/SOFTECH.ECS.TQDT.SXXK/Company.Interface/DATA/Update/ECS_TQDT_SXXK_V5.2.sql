/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/02/2012 8:54:25 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_VanDon]'
GO
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [SoVanDon] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [NgayVanDon] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [SoHieuPTVT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [NgayDenPTVT] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [QuocTichPTVT] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VanDon] ALTER COLUMN [CuaKhauNhap_ID] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[[p_GC_NguyenPhuLieu_GetNPLDinhMuc]]
-- Database: TQDT_SXXK_V3
-- Author: Huynh Ngoc Khanh
-- Time created: Monday, July 14, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_NguyenPhuLieu_GetNPLDinhMuc]
	@IDDinhMuc INT,
	@MaSanPham nvarchar(250),
	@MaDoanhNghiep nvarchar(20)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT     t_SXXK_NguyenPhuLieu.MaHS, 
           t_SXXK_NguyenPhuLieu.DVT_ID, 
           t_SXXK_NguyenPhuLieu.Ma, 
           t_SXXK_NguyenPhuLieu.Ten, 
           t_KDT_SXXK_DinhMuc.DinhMucSuDung, 
           t_KDT_SXXK_DinhMuc.TyLeHaoHut
           FROM    t_SXXK_NguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMuc ON t_SXXK_NguyenPhuLieu.Ma = t_KDT_SXXK_DinhMuc.MaNguyenPhuLieu INNER JOIN
           t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID
WHERE    (t_KDT_SXXK_DinhMucDangKy.ID = @IDDinhMuc) AND (t_KDT_SXXK_DinhMuc.MaSanPham = @MaSanPham) AND (dbo.t_SXXK_NguyenPhuLieu.MaDoanhNghiep = @MaDoanhNghiep)
ORDER BY t_SXXK_NguyenPhuLieu.Ma
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version where [Version] ='5.2') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('5.2', getdate(), N'Cập nhật dữ liệu loại hàng hóa')
	end 
	GO