﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.GC
{
    public partial class FormSuaHopDong : BaseForm
    {
        public KDT_SXXK_LoaiPhuKien loaiPK = new KDT_SXXK_LoaiPhuKien();
      //  public List<KDT_SXXK_PhuKienDetail> ListPKDetail = new List<KDT_SXXK_PhuKienDetail>();
        public KDT_SXXK_PhuKienDetail pkdt = new KDT_SXXK_PhuKienDetail();
       

        public DateTime NgayHoanThanh;
        public FormSuaHopDong()
        {
            InitializeComponent();
        }
        bool isAdd = false;
        private void FormNgayHoanThanh_Load(object sender, EventArgs e)
        {
            if (loaiPK.ListPhuKienDeTail.Count > 0)
                pkdt = loaiPK.ListPhuKienDeTail[0];
            if (pkdt != null && pkdt.ID > 0)
            {
                txtSoHDCu.Text = pkdt.SoHopDongCu;
                clcNgayHDCu.Value = pkdt.NgayHopDongCu;
                txtSoHDMoi.Text = pkdt.SoHopDongMoi;
                clcNgayHDMoi.Value = pkdt.NgayHopDongMoi;
                isAdd = false;
            }
            else
            {
                //clcNgayHoanThanhCu.Value = NgayHoanThanh;
                isAdd = true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                pkdt.SoHopDongCu = txtSoHDCu.Text;
                pkdt.NgayHopDongCu = clcNgayHDCu.Value;
                pkdt.SoHopDongMoi = txtSoHDMoi.Text;
                pkdt.NgayHopDongMoi = clcNgayHDMoi.Value;

                
                if (isAdd)
                {
                    pkdt.Master_ID = loaiPK.ID;
                    pkdt.Insert();
                    loaiPK.ListPhuKienDeTail.Add(pkdt);
                }
                else
                {
                    pkdt.Update();
                }
                MessageBox.Show("Lưu thành công", "Thông Báo", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }
     
    }
}
