using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.SXXK
{
    public partial class ThietBiReadExcelForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public List<ThietBi> TBCollection = new List<ThietBi>();
        public String FormImport = String.Empty;
        public int FormatSoLuong = GlobalSettings.SoThapPhan.LuongNPL;
        public int FormatDonGia = GlobalSettings.SoThapPhan.LuongNPL;
        public bool ImportExcelSucces = false;
        public ThietBiReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtFilePath, errorProvider, "ĐƯỜNG DẪN FILE EXCEL", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbSheetName, errorProvider, "TÊN SHEET", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHangColumn, errorProvider, "MÃ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangColumn, errorProvider, "TÊN/MÔ TẢ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSColumn, errorProvider, "MÃ HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDVTColumn, errorProvider, "ĐƠN VỊ TÍNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "SỐ LƯỢNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDonGia, errorProvider, "ĐƠN GIÁ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtRow, errorProvider, "DÒNG BẮT ĐẦU");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }
            try
            {
               ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn;
            int maHangCol = 0;
            char tenHangColumn;
            int tenHangCol = 0;
            char maHSColumn;
            int maHSCol = 0;
            char dvtColumn;
            int dvtCol = 0;
            char XuatXuColumn;
            int XuatXuCol = 0;
            char SoLuongColumn;
            int SoLuongCol = 0;
            char DonGiaColumn;
            int DonGiaCol = 0;
            char NguyenTeColumn;
            int NguyenTeCol = 0;
            char TinhTrangColumn;
            int TinhTrangCol = 0;
            maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            maHangCol = ConvertCharToInt(maHangColumn);
            tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            tenHangCol = ConvertCharToInt(tenHangColumn);
            maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            maHSCol = ConvertCharToInt(maHSColumn);
            dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            dvtCol = ConvertCharToInt(dvtColumn);

            XuatXuColumn = Convert.ToChar(txtXuatXu.Text);
            XuatXuCol = ConvertCharToInt(XuatXuColumn);
            SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
            SoLuongCol = ConvertCharToInt(SoLuongColumn);
            DonGiaColumn = Convert.ToChar(txtDonGia.Text);
            DonGiaCol = ConvertCharToInt(DonGiaColumn);
            NguyenTeColumn = Convert.ToChar(txtNguyenTe.Text);
            NguyenTeCol = ConvertCharToInt(NguyenTeColumn);
            TinhTrangColumn = Convert.ToChar(txtghiChu.Text);
            TinhTrangCol = ConvertCharToInt(TinhTrangColumn);
            int j = 0;
            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorMaHangSpecialChar = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorXuatXu = "";
            string errorXuatXuExits = "";
            string errorSoLuong = "";
            string errorSoluongValid = "";
            string errorDonGia = "";
            string errorDonGiaValid = "";
            string errorTriGia = "";
            string errorTriGiaValid = "";
            string errorNguyenTe = "";
            string errorNguyenTeValid = "";
            string errorTinhTrang = "";
            string errorTinhTrangValid = "";
            List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
            List<VNACC_Category_CurrencyExchange> NTCollection = VNACC_Category_CurrencyExchange.SelectCollectionAll();
            List<ThietBi> TBCollectionAdd = new List<ThietBi>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    j = wsr.Index + 1;
                    try
                    {
                        ThietBi tb = new ThietBi();
                        bool isAdd = true;
                        bool isExits = false;
                        try
                        {
                            tb.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                            if (tb.Ma.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + tb.Ma + "]\n";
                                isAdd = false;
                            }
                            else if (Helpers.ValidateSpecialChar(tb.Ma))
                            {
                                errorMaHangSpecialChar += "[" + (wsr.Index + 1) + "]-[" + tb.Ma + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (ThietBi item in HD.TBCollection)
                                {
                                    if (tb.Ma == item.Ma)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (isExits)
                            {
                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + tb.Ma + "]\n";
                                isAdd = false;
                                break;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + tb.Ma + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            tb.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                            if (tb.Ten.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + tb.Ten + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + tb.Ten + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            tb.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (tb.MaHS.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + tb.MaHS + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_HSCode item in HSCollection)
                                {
                                    if (item.HSCode == tb.MaHS)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + tb.MaHS + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + tb.MaHS + "]\n";
                            isAdd = false;
                        }
                        string DonViTinh = String.Empty;
                        try
                        {
                            isExits = false;
                            DonViTinh = Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper()).Trim();
                            if (DonViTinh.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (HaiQuan_DonViTinh item in DVTCollection)
                                {
                                    if (item.Ten == DonViTinh)
                                    {
                                        tb.DVT_ID = DonViTinh_GetID(DonViTinh);
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + DonViTinh + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            tb.NuocXX_ID = Convert.ToString(wsr.Cells[XuatXuCol].Value).Trim().ToUpper();
                            if (tb.NuocXX_ID.Length == 0)
                            {
                                errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + tb.NuocXX_ID + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_Nation item in XuatXuCollection)
                                {
                                    if (item.NationCode == tb.NuocXX_ID)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorXuatXuExits += "[" + (wsr.Index + 1) + "]-[" + tb.NuocXX_ID + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + tb.NuocXX_ID + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            tb.SoLuongDangKy = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                            if (tb.SoLuongDangKy.ToString().Trim().Length == 0)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + tb.SoLuongDangKy + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(tb.SoLuongDangKy, GlobalSettings.SoThapPhan.LuongSP) != tb.SoLuongDangKy)
                                {
                                    errorSoluongValid += "[" + (wsr.Index + 1) + "]-[" + tb.SoLuongDangKy + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + tb.SoLuongDangKy + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            tb.DonGia = Convert.ToDouble(wsr.Cells[DonGiaCol].Value);
                            if (tb.DonGia.ToString().Trim().Length == 0)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + tb.DonGia + "]\n";
                                isAdd = false;
                            }
                            tb.TriGia = Convert.ToDouble(tb.SoLuongDangKy) * tb.DonGia;
                            if (tb.TriGia.ToString().Trim().Length == 0)
                            {
                                errorTriGia += "[" + (wsr.Index + 1) + "]-[" + tb.TriGia + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (tb.TriGia.ToString().Length > 10)
                                {
                                    errorTriGiaValid += "[" + (wsr.Index + 1) + "]-[" + tb.TriGia + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorDonGia += "[" + (wsr.Index + 1) + "]-[" + tb.DonGia + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            tb.NguyenTe_ID = (Convert.ToString(wsr.Cells[NguyenTeCol].Value.ToString().ToUpper())).PadRight(3);
                            isExits = false;
                            if (tb.NguyenTe_ID.ToString().Trim().Length == 0)
                            {
                                errorNguyenTe += "[" + (wsr.Index + 1) + "]-[" + tb.NguyenTe_ID + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_CurrencyExchange item in NTCollection)
                                {
                                    if (item.CurrencyCode == tb.NguyenTe_ID)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (isExits)
                            {
                                errorNguyenTeValid += "[" + (wsr.Index + 1) + "]-[" + tb.NguyenTe_ID + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorNguyenTe += "[" + (wsr.Index + 1) + "]-[" + tb.NguyenTe_ID + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            tb.TinhTrang = Convert.ToString(wsr.Cells[TinhTrangCol].Value).Trim();
                            if (tb.TinhTrang.ToString().Trim().Length == 0)
                            {
                                errorTinhTrang += "[" + (wsr.Index + 1) + "]-[" + tb.TinhTrang + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                // 0 : Mới , 1 : Cũ
                                if (tb.TinhTrang.ToString() != "1" || tb.TinhTrang.ToString() != "0")
                                {
                                    errorTinhTrangValid += "[" + (wsr.Index + 1) + "]-[" + tb.TinhTrang + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorTinhTrang += "[" + (wsr.Index + 1) + "]-[" + tb.TinhTrang + "]\n";
                            isAdd = false;
                        }
                        if (isAdd)
                            TBCollectionAdd.Add(tb);
                    }
                    catch (Exception ex)
                    {
                        this.SaveDefault();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangSpecialChar))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG \n";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " ĐÃ ĐƯỢC ĐĂNG KÝ\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ \n";
            if (!String.IsNullOrEmpty(errorMaHS))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHSExits))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " KHÔNG HỢP LỆ\n";
            if (!String.IsNullOrEmpty(errorSoluongValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG] : \n" + errorSoluongValid + " DOANH NGHIỆP CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatSoLuong + " SỐ THẬP PHÂN.ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
            if (!String.IsNullOrEmpty(errorSoLuong))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG] : " + errorSoLuong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDonGia))
                errorTotal += "\n - [STT] -[ĐƠN GIÁ] : \n" + errorDonGia + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTriGia))
                errorTotal += "\n - [STT] -[TRỊ GIÁ] : \n" + errorTriGia + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTriGiaValid))
                errorTotal += "\n - [STT] -[TRỊ GIÁ] : \n" + errorTriGiaValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA ĐỘ DÀI 10 SỐ\n";
            if (!String.IsNullOrEmpty(errorNguyenTe))
                errorTotal += "\n - [STT] -[NGUYÊN TỆ] : \n" + errorNguyenTe + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorNguyenTeValid))
                errorTotal += "\n - [STT] -[NGUYÊN TỆ] : \n" + errorNguyenTeValid + " KHÔNG HỢP LỆ .\n";
            if (!String.IsNullOrEmpty(errorTinhTrang))
                errorTotal += "\n - [STT] -[TÌNH TRẠNG] : \n" + errorTinhTrang + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTinhTrangValid))
                errorTotal += "\n - [STT] -[TÌNH TRẠNG] : \n" + errorTinhTrangValid + " KHÔNG HỢP LỆ .NHÂP :0 NẾU LÀ HÀNG MỚI .NHẬP : 1 NẾU LÀ HÀNG CŨ\n";

            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (ThietBi item in TBCollectionAdd)
                {
                    if (FormImport=="PK")
                    {
                        TBCollection.Add(item);
                    }
                    else
                    {
                        HD.TBCollection.Add(item);
                    }
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG ", false);
                ImportExcelSucces = true;
            }
            else
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                ImportExcelSucces = false;
                return;
            }
            this.SaveDefault();
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultTB(cbbSheetName, txtRow, txtMaHangColumn, txtTenHangColumn, txtMaHSColumn, txtDVTColumn,txtXuatXu,txtSoLuong, txtDonGia,txtNguyenTe,txtghiChu);
                FormatSoLuong = f.FormatLuongTB;
                FormatDonGia = f.FormatDonGiaTB;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigTB(txtRow.Text, txtMaHangColumn.Text, txtTenHangColumn.Text, txtMaHSColumn.Text, txtDVTColumn.Text, txtXuatXu.Text, txtSoLuong.Text, txtDonGia.Text, txtNguyenTe.Text, txtghiChu.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("TB_HD");
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}