using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_GiayPhep_SEA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaNguoiKhai { set; get; }
		public decimal SoDonXinCapPhep { set; get; }
		public int ChucNangChungTu { set; get; }
		public string LoaiGiayPhep { set; get; }
		public string LoaiHinhXNK { set; get; }
		public string MaDV_CapPhep { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string QuyetDinhSo { set; get; }
		public string SoGiayChungNhan { set; get; }
		public string NoiCap { set; get; }
		public DateTime NgayCap { set; get; }
		public string MaBuuChinh { set; get; }
		public string DiaChi { set; get; }
		public string MaQuocGia { set; get; }
		public string SoDienThoai { set; get; }
		public string SoFax { set; get; }
		public string Email { set; get; }
		public string SoHopDongMua { set; get; }
		public DateTime NgayHopDongMua { set; get; }
		public string SoHopDongBan { set; get; }
		public DateTime NgayHopDongBan { set; get; }
		public string PhuongTienVanChuyen { set; get; }
		public string MaCuaKhauXuatNhap { set; get; }
		public string TenCuaKhauXuatNhap { set; get; }
		public DateTime NgayBatDau { set; get; }
		public DateTime NgayKetThuc { set; get; }
		public string HoSoLienQuan { set; get; }
		public string GhiChu { set; get; }
		public string TenGiamDoc { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string HoSoKemTheo_1 { set; get; }
		public string HoSoKemTheo_2 { set; get; }
		public string HoSoKemTheo_3 { set; get; }
		public string HoSoKemTheo_4 { set; get; }
		public string HoSoKemTheo_5 { set; get; }
		public string HoSoKemTheo_6 { set; get; }
		public string HoSoKemTheo_7 { set; get; }
		public string HoSoKemTheo_8 { set; get; }
		public string HoSoKemTheo_9 { set; get; }
		public string HoSoKemTheo_10 { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string KetQuaXuLySoGiayPhep { set; get; }
		public DateTime KetQuaXuLyNgayCap { set; get; }
		public DateTime KetQuaXuLyHieuLucTuNgay { set; get; }
		public DateTime KetQuaXuLyHieuLucDenNgay { set; get; }
		public string GhiChuDanhChoCoQuanCapPhep { set; get; }
		public string DaiDienDonViCapPhep { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string TenNguoiKhai { set; get; }
		public string TenDonViCapPhep { set; get; }
		public string TenPhuongTienVanChuyen { set; get; }
		public int TrangThaiXuLy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_GiayPhep_SEA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_GiayPhep_SEA> collection = new List<KDT_VNACC_GiayPhep_SEA>();
			while (reader.Read())
			{
				KDT_VNACC_GiayPhep_SEA entity = new KDT_VNACC_GiayPhep_SEA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonXinCapPhep"))) entity.SoDonXinCapPhep = reader.GetDecimal(reader.GetOrdinal("SoDonXinCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChucNangChungTu"))) entity.ChucNangChungTu = reader.GetInt32(reader.GetOrdinal("ChucNangChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGiayPhep"))) entity.LoaiGiayPhep = reader.GetString(reader.GetOrdinal("LoaiGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinhXNK"))) entity.LoaiHinhXNK = reader.GetString(reader.GetOrdinal("LoaiHinhXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDV_CapPhep"))) entity.MaDV_CapPhep = reader.GetString(reader.GetOrdinal("MaDV_CapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyetDinhSo"))) entity.QuyetDinhSo = reader.GetString(reader.GetOrdinal("QuyetDinhSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayChungNhan"))) entity.SoGiayChungNhan = reader.GetString(reader.GetOrdinal("SoGiayChungNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCap"))) entity.NgayCap = reader.GetDateTime(reader.GetOrdinal("NgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinh"))) entity.MaBuuChinh = reader.GetString(reader.GetOrdinal("MaBuuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGia"))) entity.MaQuocGia = reader.GetString(reader.GetOrdinal("MaQuocGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoai"))) entity.SoDienThoai = reader.GetString(reader.GetOrdinal("SoDienThoai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFax"))) entity.SoFax = reader.GetString(reader.GetOrdinal("SoFax"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongMua"))) entity.SoHopDongMua = reader.GetString(reader.GetOrdinal("SoHopDongMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongMua"))) entity.NgayHopDongMua = reader.GetDateTime(reader.GetOrdinal("NgayHopDongMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongBan"))) entity.SoHopDongBan = reader.GetString(reader.GetOrdinal("SoHopDongBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongBan"))) entity.NgayHopDongBan = reader.GetDateTime(reader.GetOrdinal("NgayHopDongBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongTienVanChuyen"))) entity.PhuongTienVanChuyen = reader.GetString(reader.GetOrdinal("PhuongTienVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCuaKhauXuatNhap"))) entity.MaCuaKhauXuatNhap = reader.GetString(reader.GetOrdinal("MaCuaKhauXuatNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCuaKhauXuatNhap"))) entity.TenCuaKhauXuatNhap = reader.GetString(reader.GetOrdinal("TenCuaKhauXuatNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoLienQuan"))) entity.HoSoLienQuan = reader.GetString(reader.GetOrdinal("HoSoLienQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenGiamDoc"))) entity.TenGiamDoc = reader.GetString(reader.GetOrdinal("TenGiamDoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_1"))) entity.HoSoKemTheo_1 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_2"))) entity.HoSoKemTheo_2 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_3"))) entity.HoSoKemTheo_3 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_4"))) entity.HoSoKemTheo_4 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_5"))) entity.HoSoKemTheo_5 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_6"))) entity.HoSoKemTheo_6 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_7"))) entity.HoSoKemTheo_7 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_8"))) entity.HoSoKemTheo_8 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_9"))) entity.HoSoKemTheo_9 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_10"))) entity.HoSoKemTheo_10 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLySoGiayPhep"))) entity.KetQuaXuLySoGiayPhep = reader.GetString(reader.GetOrdinal("KetQuaXuLySoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyNgayCap"))) entity.KetQuaXuLyNgayCap = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyNgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"))) entity.KetQuaXuLyHieuLucTuNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"))) entity.KetQuaXuLyHieuLucDenNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"))) entity.GhiChuDanhChoCoQuanCapPhep = reader.GetString(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDonViCapPhep"))) entity.DaiDienDonViCapPhep = reader.GetString(reader.GetOrdinal("DaiDienDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViCapPhep"))) entity.TenDonViCapPhep = reader.GetString(reader.GetOrdinal("TenDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPhuongTienVanChuyen"))) entity.TenPhuongTienVanChuyen = reader.GetString(reader.GetOrdinal("TenPhuongTienVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_GiayPhep_SEA> collection, long id)
        {
            foreach (KDT_VNACC_GiayPhep_SEA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SEA VALUES(@MaNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @LoaiHinhXNK, @MaDV_CapPhep, @MaDoanhNghiep, @TenDoanhNghiep, @QuyetDinhSo, @SoGiayChungNhan, @NoiCap, @NgayCap, @MaBuuChinh, @DiaChi, @MaQuocGia, @SoDienThoai, @SoFax, @Email, @SoHopDongMua, @NgayHopDongMua, @SoHopDongBan, @NgayHopDongBan, @PhuongTienVanChuyen, @MaCuaKhauXuatNhap, @TenCuaKhauXuatNhap, @NgayBatDau, @NgayKetThuc, @HoSoLienQuan, @GhiChu, @TenGiamDoc, @Notes, @InputMessageID, @MessageTag, @IndexTag, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @GhiChuDanhChoCoQuanCapPhep, @DaiDienDonViCapPhep, @NgayKhaiBao, @TenNguoiKhai, @TenDonViCapPhep, @TenPhuongTienVanChuyen, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SEA SET MaNguoiKhai = @MaNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, LoaiHinhXNK = @LoaiHinhXNK, MaDV_CapPhep = @MaDV_CapPhep, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, QuyetDinhSo = @QuyetDinhSo, SoGiayChungNhan = @SoGiayChungNhan, NoiCap = @NoiCap, NgayCap = @NgayCap, MaBuuChinh = @MaBuuChinh, DiaChi = @DiaChi, MaQuocGia = @MaQuocGia, SoDienThoai = @SoDienThoai, SoFax = @SoFax, Email = @Email, SoHopDongMua = @SoHopDongMua, NgayHopDongMua = @NgayHopDongMua, SoHopDongBan = @SoHopDongBan, NgayHopDongBan = @NgayHopDongBan, PhuongTienVanChuyen = @PhuongTienVanChuyen, MaCuaKhauXuatNhap = @MaCuaKhauXuatNhap, TenCuaKhauXuatNhap = @TenCuaKhauXuatNhap, NgayBatDau = @NgayBatDau, NgayKetThuc = @NgayKetThuc, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, TenGiamDoc = @TenGiamDoc, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, NgayKhaiBao = @NgayKhaiBao, TenNguoiKhai = @TenNguoiKhai, TenDonViCapPhep = @TenDonViCapPhep, TenPhuongTienVanChuyen = @TenPhuongTienVanChuyen, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SEA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhXNK", SqlDbType.VarChar, "LoaiHinhXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDV_CapPhep", SqlDbType.VarChar, "MaDV_CapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyetDinhSo", SqlDbType.VarChar, "QuyetDinhSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayChungNhan", SqlDbType.VarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.VarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGia", SqlDbType.VarChar, "MaQuocGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai", SqlDbType.VarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFax", SqlDbType.VarChar, "SoFax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongMua", SqlDbType.VarChar, "SoHopDongMua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongMua", SqlDbType.DateTime, "NgayHopDongMua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongBan", SqlDbType.VarChar, "SoHopDongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongBan", SqlDbType.DateTime, "NgayHopDongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, "MaCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, "TenCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenGiamDoc", SqlDbType.NVarChar, "TenGiamDoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, "TenPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhXNK", SqlDbType.VarChar, "LoaiHinhXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDV_CapPhep", SqlDbType.VarChar, "MaDV_CapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyetDinhSo", SqlDbType.VarChar, "QuyetDinhSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayChungNhan", SqlDbType.VarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.VarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGia", SqlDbType.VarChar, "MaQuocGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai", SqlDbType.VarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFax", SqlDbType.VarChar, "SoFax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongMua", SqlDbType.VarChar, "SoHopDongMua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongMua", SqlDbType.DateTime, "NgayHopDongMua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongBan", SqlDbType.VarChar, "SoHopDongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongBan", SqlDbType.DateTime, "NgayHopDongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, "MaCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, "TenCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenGiamDoc", SqlDbType.NVarChar, "TenGiamDoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, "TenPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SEA VALUES(@MaNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @LoaiHinhXNK, @MaDV_CapPhep, @MaDoanhNghiep, @TenDoanhNghiep, @QuyetDinhSo, @SoGiayChungNhan, @NoiCap, @NgayCap, @MaBuuChinh, @DiaChi, @MaQuocGia, @SoDienThoai, @SoFax, @Email, @SoHopDongMua, @NgayHopDongMua, @SoHopDongBan, @NgayHopDongBan, @PhuongTienVanChuyen, @MaCuaKhauXuatNhap, @TenCuaKhauXuatNhap, @NgayBatDau, @NgayKetThuc, @HoSoLienQuan, @GhiChu, @TenGiamDoc, @Notes, @InputMessageID, @MessageTag, @IndexTag, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @GhiChuDanhChoCoQuanCapPhep, @DaiDienDonViCapPhep, @NgayKhaiBao, @TenNguoiKhai, @TenDonViCapPhep, @TenPhuongTienVanChuyen, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SEA SET MaNguoiKhai = @MaNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, LoaiHinhXNK = @LoaiHinhXNK, MaDV_CapPhep = @MaDV_CapPhep, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, QuyetDinhSo = @QuyetDinhSo, SoGiayChungNhan = @SoGiayChungNhan, NoiCap = @NoiCap, NgayCap = @NgayCap, MaBuuChinh = @MaBuuChinh, DiaChi = @DiaChi, MaQuocGia = @MaQuocGia, SoDienThoai = @SoDienThoai, SoFax = @SoFax, Email = @Email, SoHopDongMua = @SoHopDongMua, NgayHopDongMua = @NgayHopDongMua, SoHopDongBan = @SoHopDongBan, NgayHopDongBan = @NgayHopDongBan, PhuongTienVanChuyen = @PhuongTienVanChuyen, MaCuaKhauXuatNhap = @MaCuaKhauXuatNhap, TenCuaKhauXuatNhap = @TenCuaKhauXuatNhap, NgayBatDau = @NgayBatDau, NgayKetThuc = @NgayKetThuc, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, TenGiamDoc = @TenGiamDoc, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, NgayKhaiBao = @NgayKhaiBao, TenNguoiKhai = @TenNguoiKhai, TenDonViCapPhep = @TenDonViCapPhep, TenPhuongTienVanChuyen = @TenPhuongTienVanChuyen, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SEA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhXNK", SqlDbType.VarChar, "LoaiHinhXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDV_CapPhep", SqlDbType.VarChar, "MaDV_CapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyetDinhSo", SqlDbType.VarChar, "QuyetDinhSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayChungNhan", SqlDbType.VarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.VarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGia", SqlDbType.VarChar, "MaQuocGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai", SqlDbType.VarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFax", SqlDbType.VarChar, "SoFax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongMua", SqlDbType.VarChar, "SoHopDongMua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongMua", SqlDbType.DateTime, "NgayHopDongMua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongBan", SqlDbType.VarChar, "SoHopDongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongBan", SqlDbType.DateTime, "NgayHopDongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, "MaCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, "TenCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenGiamDoc", SqlDbType.NVarChar, "TenGiamDoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, "TenPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhXNK", SqlDbType.VarChar, "LoaiHinhXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDV_CapPhep", SqlDbType.VarChar, "MaDV_CapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyetDinhSo", SqlDbType.VarChar, "QuyetDinhSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayChungNhan", SqlDbType.VarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiCap", SqlDbType.NVarChar, "NoiCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.VarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGia", SqlDbType.VarChar, "MaQuocGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai", SqlDbType.VarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFax", SqlDbType.VarChar, "SoFax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.VarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongMua", SqlDbType.VarChar, "SoHopDongMua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongMua", SqlDbType.DateTime, "NgayHopDongMua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongBan", SqlDbType.VarChar, "SoHopDongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongBan", SqlDbType.DateTime, "NgayHopDongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, "MaCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, "TenCuaKhauXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThuc", SqlDbType.DateTime, "NgayKetThuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenGiamDoc", SqlDbType.NVarChar, "TenGiamDoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, "TenPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_GiayPhep_SEA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_GiayPhep_SEA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_GiayPhep_SEA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_GiayPhep_SEA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_GiayPhep_SEA(string maNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string loaiHinhXNK, string maDV_CapPhep, string maDoanhNghiep, string tenDoanhNghiep, string quyetDinhSo, string soGiayChungNhan, string noiCap, DateTime ngayCap, string maBuuChinh, string diaChi, string maQuocGia, string soDienThoai, string soFax, string email, string soHopDongMua, DateTime ngayHopDongMua, string soHopDongBan, DateTime ngayHopDongBan, string phuongTienVanChuyen, string maCuaKhauXuatNhap, string tenCuaKhauXuatNhap, DateTime ngayBatDau, DateTime ngayKetThuc, string hoSoLienQuan, string ghiChu, string tenGiamDoc, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienDonViCapPhep, DateTime ngayKhaiBao, string tenNguoiKhai, string tenDonViCapPhep, string tenPhuongTienVanChuyen, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SEA entity = new KDT_VNACC_GiayPhep_SEA();	
			entity.MaNguoiKhai = maNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.LoaiHinhXNK = loaiHinhXNK;
			entity.MaDV_CapPhep = maDV_CapPhep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.QuyetDinhSo = quyetDinhSo;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.NoiCap = noiCap;
			entity.NgayCap = ngayCap;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChi = diaChi;
			entity.MaQuocGia = maQuocGia;
			entity.SoDienThoai = soDienThoai;
			entity.SoFax = soFax;
			entity.Email = email;
			entity.SoHopDongMua = soHopDongMua;
			entity.NgayHopDongMua = ngayHopDongMua;
			entity.SoHopDongBan = soHopDongBan;
			entity.NgayHopDongBan = ngayHopDongBan;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauXuatNhap = maCuaKhauXuatNhap;
			entity.TenCuaKhauXuatNhap = tenCuaKhauXuatNhap;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TenGiamDoc = tenGiamDoc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.TenPhuongTienVanChuyen = tenPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@LoaiHinhXNK", SqlDbType.VarChar, LoaiHinhXNK);
			db.AddInParameter(dbCommand, "@MaDV_CapPhep", SqlDbType.VarChar, MaDV_CapPhep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@QuyetDinhSo", SqlDbType.VarChar, QuyetDinhSo);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.VarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.VarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaQuocGia", SqlDbType.VarChar, MaQuocGia);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.VarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@SoFax", SqlDbType.VarChar, SoFax);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@SoHopDongMua", SqlDbType.VarChar, SoHopDongMua);
			db.AddInParameter(dbCommand, "@NgayHopDongMua", SqlDbType.DateTime, NgayHopDongMua.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMua);
			db.AddInParameter(dbCommand, "@SoHopDongBan", SqlDbType.VarChar, SoHopDongBan);
			db.AddInParameter(dbCommand, "@NgayHopDongBan", SqlDbType.DateTime, NgayHopDongBan.Year <= 1753 ? DBNull.Value : (object) NgayHopDongBan);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, MaCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, TenCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TenGiamDoc", SqlDbType.NVarChar, TenGiamDoc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, TenPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_GiayPhep_SEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SEA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_GiayPhep_SEA(long id, string maNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string loaiHinhXNK, string maDV_CapPhep, string maDoanhNghiep, string tenDoanhNghiep, string quyetDinhSo, string soGiayChungNhan, string noiCap, DateTime ngayCap, string maBuuChinh, string diaChi, string maQuocGia, string soDienThoai, string soFax, string email, string soHopDongMua, DateTime ngayHopDongMua, string soHopDongBan, DateTime ngayHopDongBan, string phuongTienVanChuyen, string maCuaKhauXuatNhap, string tenCuaKhauXuatNhap, DateTime ngayBatDau, DateTime ngayKetThuc, string hoSoLienQuan, string ghiChu, string tenGiamDoc, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienDonViCapPhep, DateTime ngayKhaiBao, string tenNguoiKhai, string tenDonViCapPhep, string tenPhuongTienVanChuyen, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SEA entity = new KDT_VNACC_GiayPhep_SEA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.LoaiHinhXNK = loaiHinhXNK;
			entity.MaDV_CapPhep = maDV_CapPhep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.QuyetDinhSo = quyetDinhSo;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.NoiCap = noiCap;
			entity.NgayCap = ngayCap;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChi = diaChi;
			entity.MaQuocGia = maQuocGia;
			entity.SoDienThoai = soDienThoai;
			entity.SoFax = soFax;
			entity.Email = email;
			entity.SoHopDongMua = soHopDongMua;
			entity.NgayHopDongMua = ngayHopDongMua;
			entity.SoHopDongBan = soHopDongBan;
			entity.NgayHopDongBan = ngayHopDongBan;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauXuatNhap = maCuaKhauXuatNhap;
			entity.TenCuaKhauXuatNhap = tenCuaKhauXuatNhap;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TenGiamDoc = tenGiamDoc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.TenPhuongTienVanChuyen = tenPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_GiayPhep_SEA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@LoaiHinhXNK", SqlDbType.VarChar, LoaiHinhXNK);
			db.AddInParameter(dbCommand, "@MaDV_CapPhep", SqlDbType.VarChar, MaDV_CapPhep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@QuyetDinhSo", SqlDbType.VarChar, QuyetDinhSo);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.VarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.VarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaQuocGia", SqlDbType.VarChar, MaQuocGia);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.VarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@SoFax", SqlDbType.VarChar, SoFax);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@SoHopDongMua", SqlDbType.VarChar, SoHopDongMua);
			db.AddInParameter(dbCommand, "@NgayHopDongMua", SqlDbType.DateTime, NgayHopDongMua.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMua);
			db.AddInParameter(dbCommand, "@SoHopDongBan", SqlDbType.VarChar, SoHopDongBan);
			db.AddInParameter(dbCommand, "@NgayHopDongBan", SqlDbType.DateTime, NgayHopDongBan.Year <= 1753 ? DBNull.Value : (object) NgayHopDongBan);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, MaCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, TenCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TenGiamDoc", SqlDbType.NVarChar, TenGiamDoc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, TenPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_GiayPhep_SEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SEA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_GiayPhep_SEA(long id, string maNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string loaiHinhXNK, string maDV_CapPhep, string maDoanhNghiep, string tenDoanhNghiep, string quyetDinhSo, string soGiayChungNhan, string noiCap, DateTime ngayCap, string maBuuChinh, string diaChi, string maQuocGia, string soDienThoai, string soFax, string email, string soHopDongMua, DateTime ngayHopDongMua, string soHopDongBan, DateTime ngayHopDongBan, string phuongTienVanChuyen, string maCuaKhauXuatNhap, string tenCuaKhauXuatNhap, DateTime ngayBatDau, DateTime ngayKetThuc, string hoSoLienQuan, string ghiChu, string tenGiamDoc, string notes, string inputMessageID, string messageTag, string indexTag, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienDonViCapPhep, DateTime ngayKhaiBao, string tenNguoiKhai, string tenDonViCapPhep, string tenPhuongTienVanChuyen, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SEA entity = new KDT_VNACC_GiayPhep_SEA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.LoaiHinhXNK = loaiHinhXNK;
			entity.MaDV_CapPhep = maDV_CapPhep;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.QuyetDinhSo = quyetDinhSo;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.NoiCap = noiCap;
			entity.NgayCap = ngayCap;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChi = diaChi;
			entity.MaQuocGia = maQuocGia;
			entity.SoDienThoai = soDienThoai;
			entity.SoFax = soFax;
			entity.Email = email;
			entity.SoHopDongMua = soHopDongMua;
			entity.NgayHopDongMua = ngayHopDongMua;
			entity.SoHopDongBan = soHopDongBan;
			entity.NgayHopDongBan = ngayHopDongBan;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauXuatNhap = maCuaKhauXuatNhap;
			entity.TenCuaKhauXuatNhap = tenCuaKhauXuatNhap;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.TenGiamDoc = tenGiamDoc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.TenPhuongTienVanChuyen = tenPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@LoaiHinhXNK", SqlDbType.VarChar, LoaiHinhXNK);
			db.AddInParameter(dbCommand, "@MaDV_CapPhep", SqlDbType.VarChar, MaDV_CapPhep);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@QuyetDinhSo", SqlDbType.VarChar, QuyetDinhSo);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.VarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@NoiCap", SqlDbType.NVarChar, NoiCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.VarChar, DiaChi);
			db.AddInParameter(dbCommand, "@MaQuocGia", SqlDbType.VarChar, MaQuocGia);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.VarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@SoFax", SqlDbType.VarChar, SoFax);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@SoHopDongMua", SqlDbType.VarChar, SoHopDongMua);
			db.AddInParameter(dbCommand, "@NgayHopDongMua", SqlDbType.DateTime, NgayHopDongMua.Year <= 1753 ? DBNull.Value : (object) NgayHopDongMua);
			db.AddInParameter(dbCommand, "@SoHopDongBan", SqlDbType.VarChar, SoHopDongBan);
			db.AddInParameter(dbCommand, "@NgayHopDongBan", SqlDbType.DateTime, NgayHopDongBan.Year <= 1753 ? DBNull.Value : (object) NgayHopDongBan);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauXuatNhap", SqlDbType.VarChar, MaCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauXuatNhap", SqlDbType.VarChar, TenCuaKhauXuatNhap);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TenGiamDoc", SqlDbType.NVarChar, TenGiamDoc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenPhuongTienVanChuyen", SqlDbType.NVarChar, TenPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_GiayPhep_SEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SEA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_GiayPhep_SEA(long id)
		{
			KDT_VNACC_GiayPhep_SEA entity = new KDT_VNACC_GiayPhep_SEA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_GiayPhep_SEA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SEA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}