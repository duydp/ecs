﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_TIA_HangForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TIA TIA = new KDT_VNACCS_TIA();
        public KDT_VNACCS_TIA_HangHoa Hang ;
        bool isAddNew = true;
        public VNACC_TIA_HangForm()
        {
            InitializeComponent();
        }

        private void VNACC_TIA_HangForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                grListHang.Refetch();
                grListHang.DataSource = TIA.HangCollection;
                grListHang.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(ctrMaSoHang, errorProvider, "Mã số hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongBanDau, errorProvider, "Số lượng ban đầu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrSoLuongBanDau, errorProvider, "ĐVT", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtSoLuongDaTNTX, errorProvider, "Số lượng đã Tạm nhập /Tái xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrSoLuongDaTNTX, errorProvider, "ĐVT", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Get()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Hang.MaSoHangHoa = ctrMaSoHang.Code;
                Hang.SoLuongBanDau = Convert.ToDecimal(txtSoLuongBanDau.Text.ToString());
                Hang.DVTSoLuongBanDau = ctrSoLuongBanDau.Code;
                Hang.SoLuongDaTaiNhapTaiXuat = Convert.ToDecimal(txtSoLuongDaTNTX.Text.ToString());
                Hang.DVTSoLuongDaTaiNhapTaiXuat = ctrSoLuongDaTNTX.Code;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Set()
        {
            try
            {
                ctrMaSoHang.Code = Hang.MaSoHangHoa;
                txtSoLuongBanDau.Text = Hang.SoLuongBanDau.ToString();
                ctrSoLuongBanDau.Code = Hang.DVTSoLuongBanDau;
                txtSoLuongDaTNTX.Text = Hang.SoLuongDaTaiNhapTaiXuat.ToString();
                ctrSoLuongDaTNTX.Code = Hang.DVTSoLuongDaTaiNhapTaiXuat;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (Hang == null)
                {
                    Hang = new KDT_VNACCS_TIA_HangHoa();
                    isAddNew = true;
                }
                Get();
                if (isAddNew)
                    TIA.HangCollection.Add(Hang);
                Hang = new KDT_VNACCS_TIA_HangHoa();
                BindData();
                ctrMaSoHang.Code = String.Empty;
                txtSoLuongBanDau.Text = String.Empty;
                ctrSoLuongBanDau.Code = String.Empty;
                txtSoLuongDaTNTX.Text = String.Empty;
                ctrSoLuongDaTNTX.Code = String.Empty;
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListHang.SelectedItems;
                List<KDT_VNACCS_TIA_HangHoa> ItemColl = new List<KDT_VNACCS_TIA_HangHoa>();
                if (grListHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TIA_HangHoa)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TIA_HangHoa item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TIA.HangCollection.Remove(item);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grListHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (grListHang.GetRows().Length < 0) return;
                Hang = (KDT_VNACCS_TIA_HangHoa)grListHang.CurrentRow.DataRow;
                Set();
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
