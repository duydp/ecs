﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepForm_SEA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_GiayPhep_SEA GiayPhep = new KDT_VNACC_GiayPhep_SEA();

        public VNACC_GiayPhepForm_SEA()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SEA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            SetMaxLengthControl();

            if (GiayPhep.ID != 0)
            {
                SetGiayPhep();
            }

            ValidateControl(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowChiThiHaiQuan();
                    break;

                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
            }
        }

        private void ShowChiThiHaiQuan()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = GiayPhep.ID;
            f.LoaiThongTin = ELoaiThongTin.GP_SEA;
            f.ShowDialog();
        }

        private void ShowThemHang()
        {
            VNACC_GiayPhep_SEA_HangForm f = new VNACC_GiayPhep_SEA_HangForm();
            f.GiayPhep_SEA = GiayPhep;
            f.ShowDialog();
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateControl(false))
                    return;

                GetGiayPhep();

                GiayPhep.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                GiayPhep.MaNguoiKhai = txtMaNguoiKhai.Text;
                GiayPhep.SoDonXinCapPhep = txtSoDonXinCapPhep.Text != "" ? Convert.ToInt32(txtSoDonXinCapPhep.Text) : 0;
                GiayPhep.ChucNangChungTu = Convert.ToInt32(ucChucNangChungTu.Code);

                GiayPhep.LoaiGiayPhep = txtLoaiGiayPhep.Text;
                GiayPhep.LoaiHinhXNK = cboLoaiHinhXNK.SelectedValue.ToString();
                GiayPhep.MaDV_CapPhep = txtMaDonViCapPhep.Text;

                GiayPhep.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                GiayPhep.TenDoanhNghiep = txtTenDN.Text;

                GiayPhep.QuyetDinhSo = txtQuyetDinhSo.Text;
                GiayPhep.SoGiayChungNhan = txtSoGiayChungNhan.Text;
                GiayPhep.NoiCap = txtNoiCap.Text;
                GiayPhep.NgayCap = dtNgayCap.Value;

                GiayPhep.MaBuuChinh = txtMaBuuChinh.Text;
                GiayPhep.DiaChi = txtDiaChi.Text;
                GiayPhep.MaQuocGia = ucMaQuocGia.Code;
                GiayPhep.SoDienThoai = txtDienThoai.Text;
                GiayPhep.SoFax = txtFax.Text;
                GiayPhep.Email = txtEmail.Text;

                GiayPhep.SoHopDongMua = txtSoHopDongMua.Text;
                GiayPhep.NgayHopDongMua = clcNgay_HD_Mua.Value;
                GiayPhep.SoHopDongBan = txtSoHopDongBan.Text;
                GiayPhep.NgayHopDongBan = clcNgay_HD_Ban.Value;

                GiayPhep.PhuongTienVanChuyen = ucPhuongTienVanChuyen.Code;
                GiayPhep.MaCuaKhauXuatNhap = ucCuaKhauXuatNhap.Code.ToString();
                GiayPhep.TenCuaKhauXuatNhap = ucCuaKhauXuatNhap.Name_VN.ToString();
                GiayPhep.NgayBatDau = clcTuNgay.Value;
                GiayPhep.NgayKetThuc = clcDenNgay.Value;

                GiayPhep.HoSoLienQuan = txtHoSoLienQuan.Text;
                GiayPhep.GhiChu = txtGhiChu.Text;
                GiayPhep.TenGiamDoc = txtTenGiamDoc.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtMaNguoiKhai.Text = GiayPhep.MaNguoiKhai;
                txtSoDonXinCapPhep.Text = GiayPhep.SoDonXinCapPhep.ToString();
                ucChucNangChungTu.Code = GiayPhep.ChucNangChungTu.ToString();

                txtLoaiGiayPhep.Text = GiayPhep.LoaiGiayPhep;
                cboLoaiHinhXNK.SelectedValue = GiayPhep.LoaiHinhXNK;
                txtMaDonViCapPhep.Text = GiayPhep.MaDV_CapPhep;

                txtMaDoanhNghiep.Text = GiayPhep.MaDoanhNghiep;
                txtTenDN.Text = GiayPhep.TenDoanhNghiep;

                txtQuyetDinhSo.Text = GiayPhep.QuyetDinhSo;
                txtSoGiayChungNhan.Text = GiayPhep.SoGiayChungNhan;
                txtNoiCap.Text = GiayPhep.NoiCap;
                dtNgayCap.Value = GiayPhep.NgayCap;

                txtMaBuuChinh.Text = GiayPhep.MaBuuChinh;
                txtDiaChi.Text = GiayPhep.DiaChi;
                ucMaQuocGia.Code = GiayPhep.MaQuocGia;
                txtDienThoai.Text = GiayPhep.SoDienThoai;
                txtFax.Text = GiayPhep.SoFax;
                txtEmail.Text = GiayPhep.Email;

                txtSoHopDongMua.Text = GiayPhep.SoHopDongMua;
                clcNgay_HD_Mua.Value = GiayPhep.NgayHopDongMua;
                txtSoHopDongBan.Text = GiayPhep.SoHopDongBan;
                clcNgay_HD_Ban.Value = GiayPhep.NgayHopDongBan;

                ucPhuongTienVanChuyen.Code = GiayPhep.PhuongTienVanChuyen;
                ucCuaKhauXuatNhap.Code = GiayPhep.MaCuaKhauXuatNhap;
                clcTuNgay.Value = GiayPhep.NgayBatDau;
                clcDenNgay.Value = GiayPhep.NgayKetThuc;

                txtHoSoLienQuan.Text = GiayPhep.HoSoLienQuan;
                txtGhiChu.Text = GiayPhep.GhiChu;
                txtTenGiamDoc.Text = GiayPhep.TenGiamDoc;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucMaQuocGia_EditValueChanged(object sender, EventArgs e)
        {
            ucCuaKhauXuatNhap.CountryCode = ucMaQuocGia.Code;
            ucCuaKhauXuatNhap.Code = "";
            ucCuaKhauXuatNhap.ReLoadData();
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.Tag = "SMC";
                txtSoDonXinCapPhep.Tag = "APN";
                ucChucNangChungTu.TagName = "FNC";
                txtLoaiGiayPhep.Tag = "APT";
                cboLoaiHinhXNK.Tag = "EIC";
                txtMaDonViCapPhep.Tag = "APP";

                txtMaDoanhNghiep.Tag = "ENC";
                txtTenDN.Tag = "ENN";
                txtQuyetDinhSo.Tag = "EDN";
                txtSoGiayChungNhan.Tag = "RCN";
                txtNoiCap.Tag = "IP";
                dtNgayCap.Tag = "ID";
                txtMaBuuChinh.Tag = "BAP";
                txtDiaChi.Tag = "IMA";
                ucMaQuocGia.TagName = "BCC";
                txtDienThoai.Tag = "BAN";
                txtFax.Tag = "BAF";
                txtEmail.Tag = "BAM";
                txtSoHopDongMua.Tag = "PCN";
                clcNgay_HD_Mua.Tag = "PCD";
                txtSoHopDongBan.Tag = "SCN";
                clcNgay_HD_Ban.Tag = "SCD";
                ucPhuongTienVanChuyen.TagName = "MTT";
                ucCuaKhauXuatNhap.TagCode = "IPC"; //Ma
                ucCuaKhauXuatNhap.TagName = "IPN"; //Ten
                clcTuNgay.Tag = "ESI";
                clcDenNgay.Tag = "EEI";
                txtHoSoLienQuan.Tag = "AD";
                txtGhiChu.Tag = "RMK";
                txtTenGiamDoc.Tag = "PIC";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= Globals.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);
                ucChucNangChungTu.SetValidate = true;
                isValid &= ucChucNangChungTu.IsValidate; //"Chức năng của chứng từ");
                isValid &= Globals.ValidateNull(txtLoaiGiayPhep, errorProvider, "Loại giấy phép", isOnlyWarning);
                isValid &= Globals.ValidateNull(cboLoaiHinhXNK, errorProvider, "Phân loại xuất/nhập khẩu", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtMaDonViCapPhep, errorProvider, "Mã đơn vị cấp phép", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtMaDoanhNghiep, errorProvider, "Mã doanh nghiệp xuất/nhập khẩu", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtSoGiayChungNhan, errorProvider, "Số của giấy chứng nhận ĐKKD", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtNoiCap, errorProvider, "Đơn vị cấp chứng nhận ĐKKD", isOnlyWarning);
                isValid &= Globals.ValidateDate(dtNgayCap, errorProvider, "Ngày cấp chứng nhận ĐKKD", isOnlyWarning);
                ucCuaKhauXuatNhap.SetValidate = true;
                isValid &= ucCuaKhauXuatNhap.IsValidate; //"Mã cửa khẩu xuất/nhập hàng");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.MaxLength = 13;
                txtSoDonXinCapPhep.MaxLength = 12;
                //txtChucNangChungTu.MaxLength = 1;
                txtLoaiGiayPhep.MaxLength = 4;
                //txtLoaiHinhXNK.MaxLength = 1;
                txtMaDonViCapPhep.MaxLength = 6;
                txtMaDoanhNghiep.MaxLength = 13;
                txtTenDN.MaxLength = 300;
                txtQuyetDinhSo.MaxLength = 17;
                txtSoGiayChungNhan.MaxLength = 17;
                txtNoiCap.MaxLength = 35;
                //txtNgayCap.MaxLength = 8;
                txtMaBuuChinh.MaxLength = 7;
                txtDiaChi.MaxLength = 300;
                //txtMaQuocGia.MaxLength = 2;
                txtDienThoai.MaxLength = 20;
                txtFax.MaxLength = 20;
                txtEmail.MaxLength = 210;
                txtSoHopDongMua.MaxLength = 17;
                //txtNgayHopDongMua.MaxLength = 8;
                txtSoHopDongBan.MaxLength = 17;
                //txtNgayHopDongBan.MaxLength = 8;
                //txtPhuongTienVanChuyen.MaxLength = 2;
                //txtMaCuaKhauXuatNhap.MaxLength = 6;
                //txtTenCuaKhauXuatNhap.MaxLength = 35;
                //txtNgayBatDau.MaxLength = 8;
                //txtNgayKetThuc.MaxLength = 8;
                txtHoSoLienQuan.MaxLength = 750;
                txtGhiChu.MaxLength = 996;
                txtTenGiamDoc.MaxLength = 100;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (GiayPhep.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    GiayPhep.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    GiayPhep.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg; //Form khai báo
                    SEA seaObj = VNACCMaperFromObject.SEAMapper(GiayPhep); //Set Mapper
                    if (seaObj == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    msg = MessagesSend.Load<SEA>(seaObj, GiayPhep.InputMessageID);

                    MsgLog.SaveMessages(msg, GiayPhep.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = GiayPhep.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số đơn xin cấp phép: " + soTiepNhan;

                                GiayPhep.SoDonXinCapPhep = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số giấp phép: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_GiayPhep(msgResult, "", GiayPhep);
            GiayPhep.InsertUpdateFull();
            SetGiayPhep();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (GiayPhep != null && GiayPhep.SoDonXinCapPhep > 0 && GiayPhep.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", GiayPhep.SoDonXinCapPhep.ToString(), GiayPhep.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtLoaiGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtLoaiHinhXNK.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaDonViCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaDoanhNghiep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuyetDinhSo.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoGiayChungNhan.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNoiCap.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaQuocGia.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDienThoai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtFax.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoHopDongMua.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoHopDongBan.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtPhuongTienVanChuyen.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaCuaKhauXuatNhap.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtTenCuaKhauXuatNhap.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtLoaiGiayPhep.CharacterCasing = CharacterCasing.Upper;
            //txtLoaiHinhXNK.CharacterCasing = CharacterCasing.Upper;
            txtMaDonViCapPhep.CharacterCasing = CharacterCasing.Upper;
            txtMaDoanhNghiep.CharacterCasing = CharacterCasing.Upper;
            txtQuyetDinhSo.CharacterCasing = CharacterCasing.Upper;
            txtSoGiayChungNhan.CharacterCasing = CharacterCasing.Upper;
            txtNoiCap.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinh.CharacterCasing = CharacterCasing.Upper;
            //txtMaQuocGia.CharacterCasing = CharacterCasing.Upper;
            txtDienThoai.CharacterCasing = CharacterCasing.Upper;
            txtFax.CharacterCasing = CharacterCasing.Upper;
            txtSoHopDongMua.CharacterCasing = CharacterCasing.Upper;
            txtSoHopDongBan.CharacterCasing = CharacterCasing.Upper;
            //txtPhuongTienVanChuyen.CharacterCasing = CharacterCasing.Upper;
            //txtMaCuaKhauXuatNhap.CharacterCasing = CharacterCasing.Upper;
            //txtTenCuaKhauXuatNhap.CharacterCasing = CharacterCasing.Upper;

        }

    }
}
