﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS;

namespace Company.BLL.VNACCS
{
    public partial class MsgLog
    {


        public static IList<MsgLog> SelectCollectionDynamicByItemID(int itemID)
        {
            return SelectCollectionDynamic("Item_id = " + itemID, "CreatedTime");
        }
        
        public static bool SaveMessages(string Msg,string MaNghiepVu, long itemid, string TieuDeThongBao, string NoiDungThongBao, string messtag, string inputMsgID, string indexTag)
        {
            try
            {
                MsgLog mess = new MsgLog
                    {
                        Log_Messages = Msg,
                        MaNghiepVu = MaNghiepVu,
                        Item_id = itemid,
                        TieuDeThongBao = TieuDeThongBao,
                        NoiDungThongBao = NoiDungThongBao,
                        MessagesTag = messtag,
                        InputMessagesID = inputMsgID,
                        IndexTag = indexTag,
                        CreatedTime = DateTime.Now,
                    };  
                return mess.Insert() == 1;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception("Msg"));
                return false;
            }
         
          
        }
        public static bool SaveMessages(MessagesSend msgSend, long itemid, string TieuDe, string NoiDungTB)
        {
            try
            {
                MsgLog mess = new MsgLog
                    {
                        Log_Messages = HelperVNACCS.BuildEdiMessages(msgSend).ToString(),
                        MaNghiepVu = msgSend.Header.MaNghiepVu.GetValue().ToString(),
                        Item_id = itemid,
                        TieuDeThongBao = TieuDe,
                        NoiDungThongBao = NoiDungTB,
                        MessagesTag = msgSend.Header.MessagesTag.GetValue().ToString(),
                        InputMessagesID = msgSend.Header.InputMessagesID.GetValue().ToString(),
                        IndexTag = msgSend.Header.IndexTag.GetValue().ToString(),
                        CreatedTime = DateTime.Now,
                    };
                return mess.Insert() == 1;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }
       

        public static DataTable GetKetQuaXuLy(long itemID,string typeMsg)
        {
            string sql = string.Format(@"SELECT ID,TieuDeThongBao,CreatedTime,
	                                       InputMessagesID
	                                       ,MaNghiepVu
                                    FROM t_KDT_VNACCS_MsgLog 
                                    WHERE InputMessagesID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0} AND IndexTag IN ('','{1}'))
                                    UNION
                                    SELECT Master_ID AS ID,'' AS TieuDeThongBao,CreatedTime ,
		                                    MessagesInputID AS InputMessagesID,
	                                       MaNghiepVu
                                      FROM t_KDT_VNACCS_MsgPhanBo
                                        WHERE t_KDT_VNACCS_MsgPhanBo.MessagesInputID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0} AND IndexTag IN ('','{1}')) 

                                    ORDER BY CreatedTime ",itemID,typeMsg);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@itemID", SqlDbType.BigInt, itemID);
            //db.AddInParameter(dbCommand, "@type", SqlDbType.VarChar, typeMsg);

            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public static MsgLog GetThongTinToKhaiTam(string inputMSg, string MaNghiepVu)
        {
            string sql = string.Format(@"SELECT TOP 1 * FROM [t_KDT_VNACCS_MsgLog]
WHERE InputMessagesID = '{0}' AND MaNghiepVu LIKE '%{1}%'
ORDER BY id desc ", inputMSg, MaNghiepVu);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<MsgLog> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
    }
}
