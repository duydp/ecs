﻿namespace Naccs.Common.Function
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class Logging
    {
        private static Logging log = null;
        private static WriteDelegate _OnWrite;

        public static  event WriteDelegate OnWrite
        {
            add
            {
                WriteDelegate delegate3;
                WriteDelegate onWrite = _OnWrite;
                do
                {
                    delegate3 = onWrite;
                    WriteDelegate delegate4 = (WriteDelegate) Delegate.Combine(delegate3, value);
                    onWrite = Interlocked.CompareExchange<WriteDelegate>(ref _OnWrite, delegate4, delegate3);
                }
                while (onWrite != delegate3);
            }
            remove
            {
                WriteDelegate delegate3;
                WriteDelegate onWrite = _OnWrite;
                do
                {
                    delegate3 = onWrite;
                    WriteDelegate delegate4 = (WriteDelegate) Delegate.Remove(delegate3, value);
                    onWrite = Interlocked.CompareExchange<WriteDelegate>(ref _OnWrite, delegate4, delegate3);
                }
                while (onWrite != delegate3);
            }
        }

        public static Logging getInstace()
        {
            if (log == null)
            {
                log = new Logging();
            }
            return log;
        }

        public static void Write(object sender, string title, string description)
        {
            getInstace().writelog(sender, title, description);
        }

        private void writelog(object sender, string title, string description)
        {
            if (_OnWrite != null)
            {
                LogEventArgs e = new LogEventArgs(sender, title, description);
                _OnWrite(e);
            }
        }

        public delegate void WriteDelegate(LogEventArgs e);
    }
}

