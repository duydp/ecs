namespace Company.Interface.Report.VNACCS
{
    partial class GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenHangHoa11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThuoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoatChat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTieuChuanChatLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanDung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenHoatChatGayNghien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCongDung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoKLHoatChatGayNghien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTKhoiLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiaNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiaBanBuon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiaBanLe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhaCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongXaCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuanHuyenCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTinhCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaCongTyXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhaCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongXaCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuanHuyenCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTinhCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaCongTySX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhaCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongXaCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuanHuyenCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTinhCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaCongTyCC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDonViUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhaDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongXaDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuanHuyenDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTinhDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuocGiaDVUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTLuongTonKhoKyTruoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTonKhoKyTruoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongNhapTrongKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoXuatTrongKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTonKhoDenNgay = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChuChiTiet = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHuHao = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 15.04173F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(60.42309F, 24.95842F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(673.9484F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "GIẤY ĐĂNG KÝ CẤP PHÉP NHẬP KHẨU THUỐC CÓ CHỨA THUỐC GÂY NGHIỆN";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTongSoTrang,
            this.xrLabel2,
            this.lblSoTrang,
            this.lblTenThongTinXuat});
            this.PageHeader.HeightF = 63.50009F;
            this.PageHeader.Name = "PageHeader";
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(764.1946F, 0F);
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrang.SizeF = new System.Drawing.SizeF(25.9303F, 20F);
            this.lblTongSoTrang.Text = "X";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(754.1946F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(10F, 20F);
            this.xrLabel2.Text = "/";
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(738.5696F, 0F);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(15.62506F, 20F);
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "2";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTrang.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.lblSoTrang_PrintOnPage);
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 895F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow43,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow44,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow45,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow46,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42});
            this.xrTable1.SizeF = new System.Drawing.SizeF(777F, 849F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.xrTableCell4,
            this.lblTenHangHoa11});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.72;
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.StylePriority.UseBorders = false;
            this.lblSTT.StylePriority.UseTextAlignment = false;
            this.lblSTT.Text = "01";
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT.Weight = 0.0891248489438797;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Weight = 0.044884140427048136;
            // 
            // lblTenHangHoa11
            // 
            this.lblTenHangHoa11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTenHangHoa11.Name = "lblTenHangHoa11";
            this.lblTenHangHoa11.StylePriority.UseBorders = false;
            this.lblTenHangHoa11.Text = "Tên thuốc, hàm lượng, dạng bào chế, quy cách đóng gói:";
            this.lblTenHangHoa11.Weight = 2.865991010629072;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3,
            this.lblTenThuoc});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 2.2;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Weight = 0.0891248489438797;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.044884140427048136;
            // 
            // lblTenThuoc
            // 
            this.lblTenThuoc.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenThuoc.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThuoc.Name = "lblTenThuoc";
            this.lblTenThuoc.StylePriority.UseBorders = false;
            this.lblTenThuoc.StylePriority.UseFont = false;
            this.lblTenThuoc.Text = resources.GetString("lblTenThuoc.Text");
            this.lblTenThuoc.Weight = 2.865991010629072;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.lblMaSoHH,
            this.xrTableCell13,
            this.xrTableCell15});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.71999999999999975;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Weight = 0.0891248489438797;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 0.044884140427048136;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Text = "Mã số hàng hóa:";
            this.xrTableCell8.Weight = 0.339124819486758;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Weight = 0.0386101564385255;
            // 
            // lblMaSoHH
            // 
            this.lblMaSoHH.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaSoHH.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSoHH.Name = "lblMaSoHH";
            this.lblMaSoHH.StylePriority.UseBorders = false;
            this.lblMaSoHH.StylePriority.UseFont = false;
            this.lblMaSoHH.Text = "XXXX.XX.XX.X1XE";
            this.lblMaSoHH.Weight = 0.62206400867594713;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Weight = 0.045587959436836356;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 1.8206040665910053;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell14,
            this.lblHoatChat,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.71999999999999975;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 0.0891248489438797;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.044884140427048136;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Hoạt chất:";
            this.xrTableCell12.Weight = 0.339124819486758;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 0.0386101564385255;
            // 
            // lblHoatChat
            // 
            this.lblHoatChat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoatChat.Name = "lblHoatChat";
            this.lblHoatChat.StylePriority.UseFont = false;
            this.lblHoatChat.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblHoatChat.Weight = 1.2977396843516229;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.045587841608349389;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Weight = 1.1449285087438166;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.lblTieuChuanChatLuong,
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 0.71999999999999975;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Weight = 0.0891248489438797;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.044884140427048136;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "Tiêu chuẩn chất lượng:";
            this.xrTableCell20.Weight = 0.49597810134003983;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.038610038610038581;
            // 
            // lblTieuChuanChatLuong
            // 
            this.lblTieuChuanChatLuong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTieuChuanChatLuong.Name = "lblTieuChuanChatLuong";
            this.lblTieuChuanChatLuong.StylePriority.UseFont = false;
            this.lblTieuChuanChatLuong.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTieuChuanChatLuong.Weight = 1.1408865203268279;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.045587841608349389;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Weight = 1.1449285087438166;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.lblSoDangKy,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.71999999999999975;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.0891248489438797;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 0.044884140427048136;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "Số đăng ký:";
            this.xrTableCell27.Weight = 0.339124819486758;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.038610156438525556;
            // 
            // lblSoDangKy
            // 
            this.lblSoDangKy.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDangKy.Name = "lblSoDangKy";
            this.lblSoDangKy.StylePriority.UseFont = false;
            this.lblSoDangKy.Text = "XXXXXXXXX1XXXXXXE";
            this.lblSoDangKy.Weight = 1.2977396843516229;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 0.045587841608349389;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 1.1449285087438166;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.lblHanDung,
            this.xrTableCell37,
            this.xrTableCell38});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.71999999999999975;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Weight = 0.0891248489438797;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.044884140427048136;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "Hạn dùng:";
            this.xrTableCell34.Weight = 0.339124819486758;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 0.038610156438525556;
            // 
            // lblHanDung
            // 
            this.lblHanDung.Name = "lblHanDung";
            this.lblHanDung.Text = "dd/MM/yyyy";
            this.lblHanDung.Weight = 1.2977396843516229;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.045587841608349389;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Weight = 1.1449285087438166;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.lblSoLuong,
            this.xrTableCell46,
            this.lblDVTSoLuong,
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.71999999999999975;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.0891248489438797;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Weight = 0.044884140427048136;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "Số lượng";
            this.xrTableCell41.Weight = 0.339124819486758;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.038610156438525556;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.StylePriority.UseTextAlignment = false;
            this.lblSoLuong.Text = "12.345.678";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong.Weight = 0.24801920003412314;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.038610053338599404;
            // 
            // lblDVTSoLuong
            // 
            this.lblDVTSoLuong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTSoLuong.Name = "lblDVTSoLuong";
            this.lblDVTSoLuong.StylePriority.UseFont = false;
            this.lblDVTSoLuong.Text = "XXE";
            this.lblDVTSoLuong.Weight = 1.0111104309789003;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.045587841608349389;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 1.1449285087438166;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblTenHoatChatGayNghien});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.71999999999999975;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 0.0891248489438797;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.044884140427048136;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Tên hoạt chất gây nghiện:";
            this.xrTableCell50.Weight = 0.53458813995007848;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.038610038610038588;
            // 
            // lblTenHoatChatGayNghien
            // 
            this.lblTenHoatChatGayNghien.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenHoatChatGayNghien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenHoatChatGayNghien.Name = "lblTenHoatChatGayNghien";
            this.lblTenHoatChatGayNghien.StylePriority.UseBorders = false;
            this.lblTenHoatChatGayNghien.StylePriority.UseFont = false;
            this.lblTenHoatChatGayNghien.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenHoatChatGayNghien.Weight = 2.2927928320689555;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.lblCongDung});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.71999999999999975;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Weight = 0.0891248489438797;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 0.044884140427048136;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "Công dụng:";
            this.xrTableCell55.Weight = 0.339124819486758;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 0.038610156438525556;
            // 
            // lblCongDung
            // 
            this.lblCongDung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblCongDung.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCongDung.Name = "lblCongDung";
            this.lblCongDung.StylePriority.UseBorders = false;
            this.lblCongDung.StylePriority.UseFont = false;
            this.lblCongDung.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblCongDung.Weight = 2.488256034703789;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.lblTongSoKLHoatChatGayNghien,
            this.xrTableCell78,
            this.lblDVTKhoiLuong});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.71999999999999975;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Weight = 0.0891248489438797;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.044884140427048136;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "Tổng số khối lượng hoạt chất gây nghiện:";
            this.xrTableCell60.Weight = 0.84132691607972376;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.038610038610038672;
            // 
            // lblTongSoKLHoatChatGayNghien
            // 
            this.lblTongSoKLHoatChatGayNghien.Name = "lblTongSoKLHoatChatGayNghien";
            this.lblTongSoKLHoatChatGayNghien.StylePriority.UseTextAlignment = false;
            this.lblTongSoKLHoatChatGayNghien.Text = "1.234.567.890";
            this.lblTongSoKLHoatChatGayNghien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongSoKLHoatChatGayNghien.Weight = 0.34508735332709944;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.038610038610038588;
            // 
            // lblDVTKhoiLuong
            // 
            this.lblDVTKhoiLuong.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblDVTKhoiLuong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTKhoiLuong.Name = "lblDVTKhoiLuong";
            this.lblDVTKhoiLuong.StylePriority.UseBorders = false;
            this.lblDVTKhoiLuong.StylePriority.UseFont = false;
            this.lblDVTKhoiLuong.Text = "XXE";
            this.lblDVTKhoiLuong.Weight = 1.602356664002172;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.lblGiaNK});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.71999999999999975;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Weight = 0.0891248489438797;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 0.044884140427048136;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "Giá nhập khẩu (VNĐ):";
            this.xrTableCell65.Weight = 0.49597810134003989;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 0.038609920781551593;
            // 
            // lblGiaNK
            // 
            this.lblGiaNK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblGiaNK.Name = "lblGiaNK";
            this.lblGiaNK.StylePriority.UseBorders = false;
            this.lblGiaNK.Text = "1.234.567.890.123.456.789";
            this.lblGiaNK.Weight = 2.331402988507481;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.lblGiaBanBuon});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.71999999999999975;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Weight = 0.0891248489438797;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.044884140427048136;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Text = "Giá bán buôn (VNĐ):";
            this.xrTableCell70.Weight = 0.49597810134003989;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.038609920781551593;
            // 
            // lblGiaBanBuon
            // 
            this.lblGiaBanBuon.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblGiaBanBuon.Name = "lblGiaBanBuon";
            this.lblGiaBanBuon.StylePriority.UseBorders = false;
            this.lblGiaBanBuon.Text = "1.234.567.890.123.456.789";
            this.lblGiaBanBuon.Weight = 2.331402988507481;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.lblGiaBanLe});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.0399999999999994;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Weight = 0.0891248489438797;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.044884140427048136;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "Giá bán lẻ (VNĐ):";
            this.xrTableCell75.Weight = 0.49597810134003989;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.038609920781551593;
            // 
            // lblGiaBanLe
            // 
            this.lblGiaBanLe.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblGiaBanLe.Name = "lblGiaBanLe";
            this.lblGiaBanLe.StylePriority.UseBorders = false;
            this.lblGiaBanLe.Text = "1.234.567.890.123.456.789";
            this.lblGiaBanLe.Weight = 2.331402988507481;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.59999999999999987;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Weight = 0.0891248489438797;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.044884140427048136;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseFont = false;
            this.xrTableCell82.Text = "Công ty xuất khẩu";
            this.xrTableCell82.Weight = 0.49597810134003989;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.038609920781551593;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Weight = 2.331402988507481;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell244,
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.32000000000000017;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 0.0891248489438797;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Weight = 0.044884140427048136;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Weight = 0.49597810134003989;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(100F, 2F);
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Weight = 0.038609920781551593;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Weight = 2.331402988507481;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.lblTenCongTyXK});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.71999999999999953;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.0891248489438797;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.044884140427048136;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Text = "Tên:";
            this.xrTableCell87.Weight = 0.33912481948675804;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 0.038610156438525528;
            // 
            // lblTenCongTyXK
            // 
            this.lblTenCongTyXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCongTyXK.Name = "lblTenCongTyXK";
            this.lblTenCongTyXK.StylePriority.UseBorders = false;
            this.lblTenCongTyXK.StylePriority.UseFont = false;
            this.lblTenCongTyXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenCongTyXK.Weight = 2.488256034703789;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.lblMaBuuChinhCongTyXK});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.71999999999999975;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Weight = 0.0891248489438797;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 0.044884140427048136;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Text = "Mã bưu chính:";
            this.xrTableCell92.Weight = 0.33912481948675804;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.038610156438525528;
            // 
            // lblMaBuuChinhCongTyXK
            // 
            this.lblMaBuuChinhCongTyXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaBuuChinhCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhCongTyXK.Name = "lblMaBuuChinhCongTyXK";
            this.lblMaBuuChinhCongTyXK.StylePriority.UseBorders = false;
            this.lblMaBuuChinhCongTyXK.StylePriority.UseFont = false;
            this.lblMaBuuChinhCongTyXK.Text = "XXXXXXXXE";
            this.lblMaBuuChinhCongTyXK.Weight = 2.488256034703789;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell98,
            this.lblSoNhaCongTyXK,
            this.lblPhuongXaCongTyXK});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.71999999999999975;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.0891248489438797;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.044884140427048136;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Text = "Địa chỉ:";
            this.xrTableCell97.Weight = 0.33912481948675804;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.038610156438525528;
            // 
            // lblSoNhaCongTyXK
            // 
            this.lblSoNhaCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhaCongTyXK.Name = "lblSoNhaCongTyXK";
            this.lblSoNhaCongTyXK.StylePriority.UseFont = false;
            this.lblSoNhaCongTyXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoNhaCongTyXK.Weight = 1.2441280173518945;
            // 
            // lblPhuongXaCongTyXK
            // 
            this.lblPhuongXaCongTyXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblPhuongXaCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongXaCongTyXK.Name = "lblPhuongXaCongTyXK";
            this.lblPhuongXaCongTyXK.StylePriority.UseBorders = false;
            this.lblPhuongXaCongTyXK.StylePriority.UseFont = false;
            this.lblPhuongXaCongTyXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblPhuongXaCongTyXK.Weight = 1.2441280173518945;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103,
            this.lblQuanHuyenCongTyXK,
            this.lblTinhCongTyXK});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.71999999999999975;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Weight = 0.0891248489438797;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.044884140427048136;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.33912481948675804;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.038610156438525528;
            // 
            // lblQuanHuyenCongTyXK
            // 
            this.lblQuanHuyenCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanHuyenCongTyXK.Name = "lblQuanHuyenCongTyXK";
            this.lblQuanHuyenCongTyXK.StylePriority.UseFont = false;
            this.lblQuanHuyenCongTyXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblQuanHuyenCongTyXK.Weight = 1.2441280173518945;
            // 
            // lblTinhCongTyXK
            // 
            this.lblTinhCongTyXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTinhCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinhCongTyXK.Name = "lblTinhCongTyXK";
            this.lblTinhCongTyXK.StylePriority.UseBorders = false;
            this.lblTinhCongTyXK.StylePriority.UseFont = false;
            this.lblTinhCongTyXK.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTinhCongTyXK.Weight = 1.2441280173518945;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.lblMaQuocGiaCongTyXK});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.99999999999999956;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.0891248489438797;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.044884140427048136;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "Mã quốc gia:";
            this.xrTableCell107.Weight = 0.33912481948675804;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 0.038610156438525528;
            // 
            // lblMaQuocGiaCongTyXK
            // 
            this.lblMaQuocGiaCongTyXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaQuocGiaCongTyXK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaCongTyXK.Name = "lblMaQuocGiaCongTyXK";
            this.lblMaQuocGiaCongTyXK.StylePriority.UseBorders = false;
            this.lblMaQuocGiaCongTyXK.StylePriority.UseFont = false;
            this.lblMaQuocGiaCongTyXK.Text = "XE";
            this.lblMaQuocGiaCongTyXK.Weight = 2.488256034703789;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.59999999999999987;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Weight = 0.0891248489438797;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.044884140427048136;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell114.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseFont = false;
            this.xrTableCell114.Text = "Công ty sản xuất";
            this.xrTableCell114.Weight = 0.41634501453532224;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.079632968976230692;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 2.3700130271175195;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell249,
            this.xrTableCell250,
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell253});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.32000000000000006;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Weight = 0.0891248489438797;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Weight = 0.044884140427048136;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Weight = 0.41634501453532224;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(5.722046E-06F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(97.83336F, 2.083344F);
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.079632968976230692;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Weight = 2.3700130271175195;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120,
            this.lblTenCongTySX});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.71999999999999953;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.0891248489438797;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.044884140427048136;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Text = "Tên:";
            this.xrTableCell119.Weight = 0.33912481948675804;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 0.038610156438525528;
            // 
            // lblTenCongTySX
            // 
            this.lblTenCongTySX.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCongTySX.Name = "lblTenCongTySX";
            this.lblTenCongTySX.StylePriority.UseBorders = false;
            this.lblTenCongTySX.StylePriority.UseFont = false;
            this.lblTenCongTySX.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenCongTySX.Weight = 2.488256034703789;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.lblMaBuuChinhCongTySX});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.71999999999999953;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Weight = 0.0891248489438797;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.044884140427048136;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Mã bưu chính:";
            this.xrTableCell124.Weight = 0.33912481948675804;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.038610156438525528;
            // 
            // lblMaBuuChinhCongTySX
            // 
            this.lblMaBuuChinhCongTySX.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaBuuChinhCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhCongTySX.Name = "lblMaBuuChinhCongTySX";
            this.lblMaBuuChinhCongTySX.StylePriority.UseBorders = false;
            this.lblMaBuuChinhCongTySX.StylePriority.UseFont = false;
            this.lblMaBuuChinhCongTySX.Text = "XXXXXXXXE";
            this.lblMaBuuChinhCongTySX.Weight = 2.488256034703789;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.lblSoNhaCongTySX,
            this.lblPhuongXaCongTySX});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.71999999999999953;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 0.0891248489438797;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.044884140427048136;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Địa chỉ:";
            this.xrTableCell129.Weight = 0.33912481948675804;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.038610156438525528;
            // 
            // lblSoNhaCongTySX
            // 
            this.lblSoNhaCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhaCongTySX.Name = "lblSoNhaCongTySX";
            this.lblSoNhaCongTySX.StylePriority.UseFont = false;
            this.lblSoNhaCongTySX.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoNhaCongTySX.Weight = 1.2441280173518945;
            // 
            // lblPhuongXaCongTySX
            // 
            this.lblPhuongXaCongTySX.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblPhuongXaCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongXaCongTySX.Name = "lblPhuongXaCongTySX";
            this.lblPhuongXaCongTySX.StylePriority.UseBorders = false;
            this.lblPhuongXaCongTySX.StylePriority.UseFont = false;
            this.lblPhuongXaCongTySX.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblPhuongXaCongTySX.Weight = 1.2441280173518945;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.lblQuanHuyenCongTySX,
            this.lblTinhCongTySX});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.71999999999999953;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.0891248489438797;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Weight = 0.044884140427048136;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.33912481948675804;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 0.038610156438525528;
            // 
            // lblQuanHuyenCongTySX
            // 
            this.lblQuanHuyenCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanHuyenCongTySX.Name = "lblQuanHuyenCongTySX";
            this.lblQuanHuyenCongTySX.StylePriority.UseFont = false;
            this.lblQuanHuyenCongTySX.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblQuanHuyenCongTySX.Weight = 1.2441280173518945;
            // 
            // lblTinhCongTySX
            // 
            this.lblTinhCongTySX.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTinhCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinhCongTySX.Name = "lblTinhCongTySX";
            this.lblTinhCongTySX.StylePriority.UseBorders = false;
            this.lblTinhCongTySX.StylePriority.UseFont = false;
            this.lblTinhCongTySX.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTinhCongTySX.Weight = 1.2441280173518945;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.lblMaQuocGiaCongTySX});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.99999999999999956;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Weight = 0.0891248489438797;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.044884140427048136;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "Mã quốc gia:";
            this.xrTableCell139.Weight = 0.33912481948675804;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.038610156438525528;
            // 
            // lblMaQuocGiaCongTySX
            // 
            this.lblMaQuocGiaCongTySX.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaQuocGiaCongTySX.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaCongTySX.Name = "lblMaQuocGiaCongTySX";
            this.lblMaQuocGiaCongTySX.StylePriority.UseBorders = false;
            this.lblMaQuocGiaCongTySX.StylePriority.UseFont = false;
            this.lblMaQuocGiaCongTySX.Text = "XE";
            this.lblMaQuocGiaCongTySX.Weight = 2.488256034703789;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.xrTableCell145,
            this.xrTableCell146});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.59999999999999987;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Weight = 0.0891248489438797;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.044884140427048136;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseFont = false;
            this.xrTableCell144.Text = "Công ty cung cấp";
            this.xrTableCell144.Weight = 0.41634501453532224;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 0.079632968976230692;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 2.3700130271175195;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 0.32;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Weight = 0.0891248489438797;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Weight = 0.044884140427048136;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3});
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Weight = 0.41634501453532224;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(97.83336F, 2.083344F);
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Weight = 0.079632968976230692;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 2.3700130271175195;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150,
            this.lblTenCongTyCC});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.71999999999999975;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.0891248489438797;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.044884140427048136;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Text = "Tên:";
            this.xrTableCell149.Weight = 0.33912481948675804;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.038610156438525528;
            // 
            // lblTenCongTyCC
            // 
            this.lblTenCongTyCC.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCongTyCC.Name = "lblTenCongTyCC";
            this.lblTenCongTyCC.StylePriority.UseBorders = false;
            this.lblTenCongTyCC.StylePriority.UseFont = false;
            this.lblTenCongTyCC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenCongTyCC.Weight = 2.488256034703789;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell155,
            this.lblMaBuuChinhCongTyCC});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.71999999999999975;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.0891248489438797;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 0.044884140427048136;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "Mã bưu chính:";
            this.xrTableCell154.Weight = 0.33912481948675804;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 0.038610156438525528;
            // 
            // lblMaBuuChinhCongTyCC
            // 
            this.lblMaBuuChinhCongTyCC.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaBuuChinhCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhCongTyCC.Name = "lblMaBuuChinhCongTyCC";
            this.lblMaBuuChinhCongTyCC.StylePriority.UseBorders = false;
            this.lblMaBuuChinhCongTyCC.StylePriority.UseFont = false;
            this.lblMaBuuChinhCongTyCC.Text = "XXXXXXXXE";
            this.lblMaBuuChinhCongTyCC.Weight = 2.488256034703789;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.lblSoNhaCongTyCC,
            this.lblPhuongXaCongTyCC});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.71999999999999975;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.Weight = 0.0891248489438797;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 0.044884140427048136;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Text = "Địa chỉ:";
            this.xrTableCell161.Weight = 0.33912481948675804;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.038610156438525528;
            // 
            // lblSoNhaCongTyCC
            // 
            this.lblSoNhaCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhaCongTyCC.Name = "lblSoNhaCongTyCC";
            this.lblSoNhaCongTyCC.StylePriority.UseFont = false;
            this.lblSoNhaCongTyCC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoNhaCongTyCC.Weight = 1.2441280173518945;
            // 
            // lblPhuongXaCongTyCC
            // 
            this.lblPhuongXaCongTyCC.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblPhuongXaCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongXaCongTyCC.Name = "lblPhuongXaCongTyCC";
            this.lblPhuongXaCongTyCC.StylePriority.UseBorders = false;
            this.lblPhuongXaCongTyCC.StylePriority.UseFont = false;
            this.lblPhuongXaCongTyCC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblPhuongXaCongTyCC.Weight = 1.2441280173518945;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167,
            this.lblQuanHuyenCongTyCC,
            this.lblTinhCongTyCC});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.71999999999999975;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Weight = 0.0891248489438797;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Weight = 0.044884140427048136;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Weight = 0.33912481948675804;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Weight = 0.038610156438525528;
            // 
            // lblQuanHuyenCongTyCC
            // 
            this.lblQuanHuyenCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanHuyenCongTyCC.Name = "lblQuanHuyenCongTyCC";
            this.lblQuanHuyenCongTyCC.StylePriority.UseFont = false;
            this.lblQuanHuyenCongTyCC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblQuanHuyenCongTyCC.Weight = 1.2441280173518945;
            // 
            // lblTinhCongTyCC
            // 
            this.lblTinhCongTyCC.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTinhCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinhCongTyCC.Name = "lblTinhCongTyCC";
            this.lblTinhCongTyCC.StylePriority.UseBorders = false;
            this.lblTinhCongTyCC.StylePriority.UseFont = false;
            this.lblTinhCongTyCC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTinhCongTyCC.Weight = 1.2441280173518945;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172,
            this.lblMaQuocGiaCongTyCC});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.99999999999999933;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.Weight = 0.0891248489438797;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.044884140427048136;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "Mã quốc gia:";
            this.xrTableCell171.Weight = 0.33912481948675804;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 0.038610156438525528;
            // 
            // lblMaQuocGiaCongTyCC
            // 
            this.lblMaQuocGiaCongTyCC.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaQuocGiaCongTyCC.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaCongTyCC.Name = "lblMaQuocGiaCongTyCC";
            this.lblMaQuocGiaCongTyCC.StylePriority.UseBorders = false;
            this.lblMaQuocGiaCongTyCC.StylePriority.UseFont = false;
            this.lblMaQuocGiaCongTyCC.Text = "XE";
            this.lblMaQuocGiaCongTyCC.Weight = 2.488256034703789;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.6;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Weight = 0.0891248489438797;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.044884140427048136;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell176.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.StylePriority.UseFont = false;
            this.xrTableCell176.Text = "Đơn vị ủy thác";
            this.xrTableCell176.Weight = 0.33912481948675804;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.038610156438525528;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Weight = 2.488256034703789;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell261,
            this.xrTableCell262,
            this.xrTableCell263});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.32000000000000028;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Weight = 0.0891248489438797;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Weight = 0.044884140427048136;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine4});
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Weight = 0.33912481948675804;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(5.722046E-06F, 0F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(77.83333F, 2.166687F);
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Weight = 0.038610156438525528;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 2.488256034703789;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.lblTenDonViUyThac});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.71999999999999953;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.Weight = 0.0891248489438797;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Weight = 0.044884140427048136;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "Tên:";
            this.xrTableCell181.Weight = 0.33912481948675804;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Weight = 0.038610156438525528;
            // 
            // lblTenDonViUyThac
            // 
            this.lblTenDonViUyThac.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenDonViUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDonViUyThac.Name = "lblTenDonViUyThac";
            this.lblTenDonViUyThac.StylePriority.UseBorders = false;
            this.lblTenDonViUyThac.StylePriority.UseFont = false;
            this.lblTenDonViUyThac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenDonViUyThac.Weight = 2.488256034703789;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.lblMaBuuChinhDVUyThac});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.72;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.0891248489438797;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.044884140427048136;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Text = "Mã bưu chính:";
            this.xrTableCell186.Weight = 0.33912481948675804;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.038610156438525528;
            // 
            // lblMaBuuChinhDVUyThac
            // 
            this.lblMaBuuChinhDVUyThac.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaBuuChinhDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhDVUyThac.Name = "lblMaBuuChinhDVUyThac";
            this.lblMaBuuChinhDVUyThac.StylePriority.UseBorders = false;
            this.lblMaBuuChinhDVUyThac.StylePriority.UseFont = false;
            this.lblMaBuuChinhDVUyThac.Text = "XXXXXXXXE";
            this.lblMaBuuChinhDVUyThac.Weight = 2.488256034703789;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.lblSoNhaDVUyThac,
            this.lblPhuongXaDVUyThac});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.72;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.Weight = 0.0891248489438797;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Weight = 0.044884140427048136;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "Địa chỉ:";
            this.xrTableCell191.Weight = 0.33912481948675804;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.038610156438525528;
            // 
            // lblSoNhaDVUyThac
            // 
            this.lblSoNhaDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhaDVUyThac.Name = "lblSoNhaDVUyThac";
            this.lblSoNhaDVUyThac.StylePriority.UseFont = false;
            this.lblSoNhaDVUyThac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoNhaDVUyThac.Weight = 1.2441280173518945;
            // 
            // lblPhuongXaDVUyThac
            // 
            this.lblPhuongXaDVUyThac.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblPhuongXaDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongXaDVUyThac.Name = "lblPhuongXaDVUyThac";
            this.lblPhuongXaDVUyThac.StylePriority.UseBorders = false;
            this.lblPhuongXaDVUyThac.StylePriority.UseFont = false;
            this.lblPhuongXaDVUyThac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblPhuongXaDVUyThac.Weight = 1.2441280173518945;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199,
            this.lblQuanHuyenDVUyThac,
            this.lblTinhDVUyThac});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.72;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.0891248489438797;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Weight = 0.044884140427048136;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 0.33912481948675804;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.038610156438525528;
            // 
            // lblQuanHuyenDVUyThac
            // 
            this.lblQuanHuyenDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanHuyenDVUyThac.Name = "lblQuanHuyenDVUyThac";
            this.lblQuanHuyenDVUyThac.StylePriority.UseFont = false;
            this.lblQuanHuyenDVUyThac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblQuanHuyenDVUyThac.Weight = 1.2441280173518945;
            // 
            // lblTinhDVUyThac
            // 
            this.lblTinhDVUyThac.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTinhDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTinhDVUyThac.Name = "lblTinhDVUyThac";
            this.lblTinhDVUyThac.StylePriority.UseBorders = false;
            this.lblTinhDVUyThac.StylePriority.UseFont = false;
            this.lblTinhDVUyThac.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTinhDVUyThac.Weight = 1.2441280173518945;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.lblMaQuocGiaDVUyThac});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.99999999999999933;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseBorders = false;
            this.xrTableCell201.Weight = 0.0891248489438797;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.044884140427048136;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Text = "Mã quốc gia:";
            this.xrTableCell203.Weight = 0.33912481948675804;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 0.038610156438525528;
            // 
            // lblMaQuocGiaDVUyThac
            // 
            this.lblMaQuocGiaDVUyThac.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaQuocGiaDVUyThac.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaQuocGiaDVUyThac.Name = "lblMaQuocGiaDVUyThac";
            this.lblMaQuocGiaDVUyThac.StylePriority.UseBorders = false;
            this.lblMaQuocGiaDVUyThac.StylePriority.UseFont = false;
            this.lblMaQuocGiaDVUyThac.Text = "XE";
            this.lblMaQuocGiaDVUyThac.Weight = 2.488256034703789;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209,
            this.lblDVTLuongTonKhoKyTruoc});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.71999999999999953;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseBorders = false;
            this.xrTableCell206.Weight = 0.0891248489438797;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Weight = 0.044884140427048136;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Text = "Đơn vị tính:";
            this.xrTableCell208.Weight = 0.33912481948675804;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Weight = 0.038610156438525528;
            // 
            // lblDVTLuongTonKhoKyTruoc
            // 
            this.lblDVTLuongTonKhoKyTruoc.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblDVTLuongTonKhoKyTruoc.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVTLuongTonKhoKyTruoc.Name = "lblDVTLuongTonKhoKyTruoc";
            this.lblDVTLuongTonKhoKyTruoc.StylePriority.UseBorders = false;
            this.lblDVTLuongTonKhoKyTruoc.StylePriority.UseFont = false;
            this.lblDVTLuongTonKhoKyTruoc.Text = "XXE";
            this.lblDVTLuongTonKhoKyTruoc.Weight = 2.488256034703789;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell216,
            this.lblSoLuongTonKhoKyTruoc,
            this.xrTableCell231,
            this.xrTableCell228,
            this.lblSoLuongNhapTrongKy,
            this.xrTableCell234,
            this.xrTableCell233,
            this.lblTongSo,
            this.xrTableCell217});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.71999999999999953;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseBorders = false;
            this.xrTableCell213.Weight = 0.0891248489438797;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Weight = 0.044884140427048136;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "Lượng tồn kho kỳ trước:";
            this.xrTableCell215.Weight = 0.49597804242579641;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 0.038610274267012495;
            // 
            // lblSoLuongTonKhoKyTruoc
            // 
            this.lblSoLuongTonKhoKyTruoc.Name = "lblSoLuongTonKhoKyTruoc";
            this.lblSoLuongTonKhoKyTruoc.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTonKhoKyTruoc.Text = "12.345.678";
            this.lblSoLuongTonKhoKyTruoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongTonKhoKyTruoc.Weight = 0.2702702702702704;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 0.11368846525096529;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Lượng nhập trong kỳ:";
            this.xrTableCell228.Weight = 0.45395946134471532;
            // 
            // lblSoLuongNhapTrongKy
            // 
            this.lblSoLuongNhapTrongKy.Name = "lblSoLuongNhapTrongKy";
            this.lblSoLuongNhapTrongKy.StylePriority.UseTextAlignment = false;
            this.lblSoLuongNhapTrongKy.Text = "12.345.678";
            this.lblSoLuongNhapTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongNhapTrongKy.Weight = 0.24935653863266177;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.1378096650466035;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Text = "Tổng số:";
            this.xrTableCell233.Weight = 0.22014768151242758;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseBorders = false;
            this.xrTableCell217.Weight = 0.59659532230333034;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell221,
            this.lblTongSoXuatTrongKy,
            this.xrTableCell236,
            this.xrTableCell237,
            this.lblSoLuongTonKhoDenNgay,
            this.xrTableCell240,
            this.xrTableCell241,
            this.lblHuHao,
            this.xrTableCell243});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.71999999999999953;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBorders = false;
            this.xrTableCell218.Weight = 0.0891248489438797;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 0.044884140427048136;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Text = "Tổng số xuất trong kỳ:";
            this.xrTableCell220.Weight = 0.49597804242579641;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 0.038610274267012495;
            // 
            // lblTongSoXuatTrongKy
            // 
            this.lblTongSoXuatTrongKy.Name = "lblTongSoXuatTrongKy";
            this.lblTongSoXuatTrongKy.StylePriority.UseTextAlignment = false;
            this.lblTongSoXuatTrongKy.Text = "123.456.789";
            this.lblTongSoXuatTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongSoXuatTrongKy.Weight = 0.27027021135602691;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 0.11368852416520878;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Text = "Số lượng tồn kho:";
            this.xrTableCell237.Weight = 0.45395946134471532;
            // 
            // lblSoLuongTonKhoDenNgay
            // 
            this.lblSoLuongTonKhoDenNgay.Name = "lblSoLuongTonKhoDenNgay";
            this.lblSoLuongTonKhoDenNgay.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTonKhoDenNgay.Text = "12.345.678";
            this.lblSoLuongTonKhoDenNgay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongTonKhoDenNgay.Weight = 0.24935653863266177;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 0.13780966504660347;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Text = "Hư hao:";
            this.xrTableCell241.Weight = 0.22014768151242758;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.Weight = 0.59659555796030417;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell223,
            this.xrTableCell224,
            this.xrTableCell225,
            this.xrTableCell226,
            this.lblGhiChuChiTiet});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.71999999999999953;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseBorders = false;
            this.xrTableCell223.Weight = 0.0891248489438797;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.Weight = 0.044884140427048136;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Text = "Ghi chú:";
            this.xrTableCell225.Weight = 0.33912481948675804;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.038610156438525528;
            // 
            // lblGhiChuChiTiet
            // 
            this.lblGhiChuChiTiet.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGhiChuChiTiet.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChuChiTiet.Name = "lblGhiChuChiTiet";
            this.lblGhiChuChiTiet.StylePriority.UseBorders = false;
            this.lblGhiChuChiTiet.StylePriority.UseFont = false;
            this.lblGhiChuChiTiet.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWE";
            this.lblGhiChuChiTiet.Weight = 2.488256034703789;
            // 
            // lblTongSo
            // 
            this.lblTongSo.Name = "lblTongSo";
            this.lblTongSo.StylePriority.UseTextAlignment = false;
            this.lblTongSo.Text = "123.456.789";
            this.lblTongSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongSo.Weight = 0.28957528957528961;
            // 
            // lblHuHao
            // 
            this.lblHuHao.Name = "lblHuHao";
            this.lblHuHao.StylePriority.UseTextAlignment = false;
            this.lblHuHao.Text = "12.345.678";
            this.lblHuHao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblHuHao.Weight = 0.28957505391831562;
            // 
            // GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(29, 24, 9, 15);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblTenHangHoa11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHH;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTenThuoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell lblHoatChat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell lblTieuChuanChatLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblHanDung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblTenHoatChatGayNghien;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell lblCongDung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTKhoiLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoKLHoatChatGayNghien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell lblGiaNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell lblGiaBanBuon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblGiaBanLe;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCongTyXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhCongTyXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhaCongTyXK;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongXaCongTyXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblQuanHuyenCongTyXK;
        private DevExpress.XtraReports.UI.XRTableCell lblTinhCongTyXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaCongTyXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCongTySX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhCongTySX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhaCongTySX;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongXaCongTySX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell lblQuanHuyenCongTySX;
        private DevExpress.XtraReports.UI.XRTableCell lblTinhCongTySX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaCongTySX;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCongTyCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhCongTyCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhaCongTyCC;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongXaCongTyCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell lblQuanHuyenCongTyCC;
        private DevExpress.XtraReports.UI.XRTableCell lblTinhCongTyCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaCongTyCC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDonViUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhDVUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhaDVUyThac;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongXaDVUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblQuanHuyenDVUyThac;
        private DevExpress.XtraReports.UI.XRTableCell lblTinhDVUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuocGiaDVUyThac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTLuongTonKhoKyTruoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTonKhoKyTruoc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongNhapTrongKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChuChiTiet;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoXuatTrongKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTonKhoDenNgay;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSo;
        private DevExpress.XtraReports.UI.XRTableCell lblHuHao;
    }
}
