﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class PhuongThucVanTai : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_PhuongThucVanTai ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string getName(string id)
        {
            //string query = "SELECT Ten FROM t_HaiQuan_PhuongThucVanTai where id='"+id+"'";
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();

            try
            {
                string query = string.Format("[ID] = '{0}'", id.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["PhuongThucVanTai"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }
        public static void Update(DataSet ds)
        {
            string insert = "INSERT INTO t_HaiQuan_PhuongThucVanTai VALUES(@Id,@Ten,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_PhuongThucVanTai SET Ten = @Ten, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_PhuongThucVanTai WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}