﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_LenhSanXuat_QuyTac : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string TienTo { set; get; }
		public string TinhTrang { set; get; }
		public string LoaiHinh { set; get; }
		public long GiaTriPhanSo { set; get; }
		public long DoDaiSo { set; get; }
		public DateTime Nam { set; get; }
		public string SoDonHang { set; get; }
		public string HienThi { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_LenhSanXuat_QuyTac> ConvertToCollection(IDataReader reader)
		{
			List<KDT_LenhSanXuat_QuyTac> collection = new List<KDT_LenhSanXuat_QuyTac>();
			while (reader.Read())
			{
				KDT_LenhSanXuat_QuyTac entity = new KDT_LenhSanXuat_QuyTac();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TienTo"))) entity.TienTo = reader.GetString(reader.GetOrdinal("TienTo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinh"))) entity.LoaiHinh = reader.GetString(reader.GetOrdinal("LoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaTriPhanSo"))) entity.GiaTriPhanSo = reader.GetInt64(reader.GetOrdinal("GiaTriPhanSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("DoDaiSo"))) entity.DoDaiSo = reader.GetInt64(reader.GetOrdinal("DoDaiSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nam"))) entity.Nam = reader.GetDateTime(reader.GetOrdinal("Nam"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonHang"))) entity.SoDonHang = reader.GetString(reader.GetOrdinal("SoDonHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetString(reader.GetOrdinal("HienThi"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_LenhSanXuat_QuyTac> collection, long id)
        {
            foreach (KDT_LenhSanXuat_QuyTac item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat_QuyTac VALUES(@TienTo, @TinhTrang, @LoaiHinh, @GiaTriPhanSo, @DoDaiSo, @Nam, @SoDonHang, @HienThi)";
            string update = "UPDATE t_KDT_LenhSanXuat_QuyTac SET TienTo = @TienTo, TinhTrang = @TinhTrang, LoaiHinh = @LoaiHinh, GiaTriPhanSo = @GiaTriPhanSo, DoDaiSo = @DoDaiSo, Nam = @Nam, SoDonHang = @SoDonHang, HienThi = @HienThi WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat_QuyTac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienTo", SqlDbType.NVarChar, "TienTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinh", SqlDbType.NVarChar, "LoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaTriPhanSo", SqlDbType.BigInt, "GiaTriPhanSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DoDaiSo", SqlDbType.BigInt, "DoDaiSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nam", SqlDbType.DateTime, "Nam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.NVarChar, "HienThi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienTo", SqlDbType.NVarChar, "TienTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinh", SqlDbType.NVarChar, "LoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaTriPhanSo", SqlDbType.BigInt, "GiaTriPhanSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DoDaiSo", SqlDbType.BigInt, "DoDaiSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nam", SqlDbType.DateTime, "Nam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.NVarChar, "HienThi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat_QuyTac VALUES(@TienTo, @TinhTrang, @LoaiHinh, @GiaTriPhanSo, @DoDaiSo, @Nam, @SoDonHang, @HienThi)";
            string update = "UPDATE t_KDT_LenhSanXuat_QuyTac SET TienTo = @TienTo, TinhTrang = @TinhTrang, LoaiHinh = @LoaiHinh, GiaTriPhanSo = @GiaTriPhanSo, DoDaiSo = @DoDaiSo, Nam = @Nam, SoDonHang = @SoDonHang, HienThi = @HienThi WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat_QuyTac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TienTo", SqlDbType.NVarChar, "TienTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinh", SqlDbType.NVarChar, "LoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaTriPhanSo", SqlDbType.BigInt, "GiaTriPhanSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DoDaiSo", SqlDbType.BigInt, "DoDaiSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nam", SqlDbType.DateTime, "Nam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.NVarChar, "HienThi", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TienTo", SqlDbType.NVarChar, "TienTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.NVarChar, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinh", SqlDbType.NVarChar, "LoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaTriPhanSo", SqlDbType.BigInt, "GiaTriPhanSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DoDaiSo", SqlDbType.BigInt, "DoDaiSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nam", SqlDbType.DateTime, "Nam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.NVarChar, "HienThi", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
        public static string GetLenhSanXuat(string TinhTrang)
        {
            string spName = "SELECT TOP(1) SoLenhSanXuat FROM dbo.t_KDT_LenhSanXuat WHERE TinhTrang=N'" + TinhTrang + "' ORDER BY ID DESC";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return "";
            else
                return (String)obj;
        }
		public static KDT_LenhSanXuat_QuyTac Load(long id)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_LenhSanXuat_QuyTac> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_LenhSanXuat_QuyTac> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_LenhSanXuat_QuyTac> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_LenhSanXuat_QuyTac(string tienTo, string tinhTrang, string loaiHinh, long giaTriPhanSo, long doDaiSo, DateTime nam, string soDonHang, string hienThi)
		{
			KDT_LenhSanXuat_QuyTac entity = new KDT_LenhSanXuat_QuyTac();	
			entity.TienTo = tienTo;
			entity.TinhTrang = tinhTrang;
			entity.LoaiHinh = loaiHinh;
			entity.GiaTriPhanSo = giaTriPhanSo;
			entity.DoDaiSo = doDaiSo;
			entity.Nam = nam;
			entity.SoDonHang = soDonHang;
			entity.HienThi = hienThi;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TienTo", SqlDbType.NVarChar, TienTo);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.NVarChar, LoaiHinh);
			db.AddInParameter(dbCommand, "@GiaTriPhanSo", SqlDbType.BigInt, GiaTriPhanSo);
			db.AddInParameter(dbCommand, "@DoDaiSo", SqlDbType.BigInt, DoDaiSo);
			db.AddInParameter(dbCommand, "@Nam", SqlDbType.DateTime, Nam.Year <= 1753 ? DBNull.Value : (object) Nam);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.NVarChar, HienThi);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_LenhSanXuat_QuyTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_QuyTac item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_LenhSanXuat_QuyTac(long id, string tienTo, string tinhTrang, string loaiHinh, long giaTriPhanSo, long doDaiSo, DateTime nam, string soDonHang, string hienThi)
		{
			KDT_LenhSanXuat_QuyTac entity = new KDT_LenhSanXuat_QuyTac();			
			entity.ID = id;
			entity.TienTo = tienTo;
			entity.TinhTrang = tinhTrang;
			entity.LoaiHinh = loaiHinh;
			entity.GiaTriPhanSo = giaTriPhanSo;
			entity.DoDaiSo = doDaiSo;
			entity.Nam = nam;
			entity.SoDonHang = soDonHang;
			entity.HienThi = hienThi;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_LenhSanXuat_QuyTac_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TienTo", SqlDbType.NVarChar, TienTo);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.NVarChar, LoaiHinh);
			db.AddInParameter(dbCommand, "@GiaTriPhanSo", SqlDbType.BigInt, GiaTriPhanSo);
			db.AddInParameter(dbCommand, "@DoDaiSo", SqlDbType.BigInt, DoDaiSo);
			db.AddInParameter(dbCommand, "@Nam", SqlDbType.DateTime, Nam.Year <= 1753 ? DBNull.Value : (object) Nam);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.NVarChar, HienThi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_LenhSanXuat_QuyTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_QuyTac item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_LenhSanXuat_QuyTac(long id, string tienTo, string tinhTrang, string loaiHinh, long giaTriPhanSo, long doDaiSo, DateTime nam, string soDonHang, string hienThi)
		{
			KDT_LenhSanXuat_QuyTac entity = new KDT_LenhSanXuat_QuyTac();			
			entity.ID = id;
			entity.TienTo = tienTo;
			entity.TinhTrang = tinhTrang;
			entity.LoaiHinh = loaiHinh;
			entity.GiaTriPhanSo = giaTriPhanSo;
			entity.DoDaiSo = doDaiSo;
			entity.Nam = nam;
			entity.SoDonHang = soDonHang;
			entity.HienThi = hienThi;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TienTo", SqlDbType.NVarChar, TienTo);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
			db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.NVarChar, LoaiHinh);
			db.AddInParameter(dbCommand, "@GiaTriPhanSo", SqlDbType.BigInt, GiaTriPhanSo);
			db.AddInParameter(dbCommand, "@DoDaiSo", SqlDbType.BigInt, DoDaiSo);
			db.AddInParameter(dbCommand, "@Nam", SqlDbType.DateTime, Nam.Year <= 1753 ? DBNull.Value : (object) Nam);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.NVarChar, HienThi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_LenhSanXuat_QuyTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_QuyTac item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_LenhSanXuat_QuyTac(long id)
		{
			KDT_LenhSanXuat_QuyTac entity = new KDT_LenhSanXuat_QuyTac();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_LenhSanXuat_QuyTac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat_QuyTac item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}