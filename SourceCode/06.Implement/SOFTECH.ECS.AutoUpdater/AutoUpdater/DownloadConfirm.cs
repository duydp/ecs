/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   Ê¥µîÆïÊ¿£¨Knights Warrior£© 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ECS.AutoUpdate
{
    public partial class DownloadConfirm : Form
    {
        #region The private fields
        List<DownloadFileInfo> downloadFileList = null;
        #endregion

        #region The constructor of DownloadConfirm
        public DownloadConfirm(List<DownloadFileInfo> downloadfileList, string note)
        {
            InitializeComponent();

            downloadFileList = downloadfileList;
            txtDescription.Text = Config.Install.Description + "\n"+ note;
            Text = "Auto Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() +" - " + Config.Install.TitleUpdate;
        }
        #endregion

        #region The private method
        private void OnLoad(object sender, EventArgs e)
        {
            foreach (DownloadFileInfo file in this.downloadFileList)
            {
                ListViewItem item = new ListViewItem(new string[] { file.FileName, file.LastVer, file.Size.ToString() });
            }
            chEnabled.Checked = !Config.Install.Enabled;
            this.Activate();
            this.Focus();
        }
        #endregion

        private void chEnabled_Click(object sender, EventArgs e)
        {
            if (chEnabled.Checked)
            {
                if (MessageBox.Show("Bạn có thật sự chắc chắn tắt chức năng cập nhật không?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Config.Install.Enabled = false;
                }
                else
                {
                    Config.Install.Enabled = true;
                    chEnabled.Checked = false;
                }
            }
            
            Config.Install.SaveConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));

        }
    }
}