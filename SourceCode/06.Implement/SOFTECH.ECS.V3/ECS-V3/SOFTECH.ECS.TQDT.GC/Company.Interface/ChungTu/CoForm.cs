﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface
{
    public partial class CoForm : BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public CO Co = new CO();
        public CoForm()
        {
            InitializeComponent();

            SetEvent_TextBox_DiaChi();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (CO.checkSoCOExit(txtSoCO.Text.Trim(), GlobalSettings.MA_DON_VI, TKMD.ID, Co.ID))
            {
                ShowMessage("Số CO này đã tồn tại.", false);
                return;
            }
            if (chkNoCo.Checked)
            {
                if (ccNgayNopCO.Text == "" || ccNgayNopCO.Value.Year <= 1900)
                {
                    ShowMessage("Nhập thông tin ngày nộp CO.", false);
                    return;
                }
            }

            if (!ValidateCO())
                return;

            Co.LoaiCO = cbLoaiCO.SelectedValue.ToString();
            Co.MaNuocNKTrenCO = ctrMaNuocNK.Ma;
            Co.MaNuocXKTrenCO = ctrMaNuocXK.Ma;
            Co.NgayCO = ccNgayCO.Value;
            Co.NuocCapCO = ctrNuocCapCO.Ma;
            Co.SoCO = txtSoCO.Text.Trim();
            Co.TenDiaChiNguoiNK = txtDiaChiNguoiNK.Text.Trim();
            Co.TenDiaChiNguoiXK = txtDiaChiNguoiXK.Text.Trim();
            Co.ThongTinMoTaChiTiet = txtThongTinMoTa.Text.Trim();
            Co.ToChucCap = txtToChucCap.Text.Trim();
            Co.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            Co.TKMD_ID = TKMD.ID;
            Co.NguoiKy = txtNguoiKy.Text.Trim();
            Co.ThoiHanNop = ccNgayNopCO.Value;
            if (string.IsNullOrEmpty(Co.GuidStr)) Co.GuidStr = Guid.NewGuid().ToString();
            if (chkNoCo.Checked)
                Co.NoCo = 1;
            else
                Co.NoCo = 0;
            if (isKhaiBoSung)
                Co.LoaiKB = 1;
            else
                Co.LoaiKB = 0;
            bool isUpdate = false;
            try
            {
                if (Co.ID == 0)
                    Co.Insert();
                else
                {
                    Co.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKMD.COCollection.Add(Co);
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void CoForm_Load(object sender, EventArgs e)
        {
            ccNgayCO.Text = DateTime.Today.ToShortDateString();

            btnKhaiBao.Enabled = true;
            btnLayPhanHoi.Enabled = false;

            cbLoaiCO.DataSource = Company.GC.BLL.DuLieuChuan.LoaiCO.SelectAll().Tables[0];
            cbLoaiCO.ValueMember = "Ma";
            cbLoaiCO.DisplayMember = "Ten";
            cbLoaiCO.SelectedIndex = cbLoaiCO.Items.Count > 0 ? 0 : -1;

            if (Co != null && Co.ID > 0)
            {
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Value = Co.ThoiHanNop;
                }
            }
            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (Co.SoTiepNhan > 0 && Co.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = Co.SoTiepNhan + "";
                ccNgayTiepNhan.Text = Co.NgayTiepNhan.ToShortDateString();
            }
            else if (Co.SoTiepNhan > 0 && Co.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
                txtSoTiepNhan.Text = Co.SoTiepNhan + "";
                ccNgayTiepNhan.Text = Co.NgayTiepNhan.ToShortDateString();
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateCO(TKMD, isKhaiBoSung, Co);

        }

        private void btnChonCO_Click(object sender, EventArgs e)
        {
            ManageCOForm f = new ManageCOForm();
            f.isBrower = true;
            f.TKMD_ID = TKMD.ID;
            f.ShowDialog();
            if (f.Co != null)
            {
                Co = f.Co;
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                Co.ID = 0;
                Co.GuidStr = "";
                Co.SoTiepNhan = 0;
                Co.NgayTiepNhan = new DateTime(1900, 1, 1);
                Co.NamTiepNhan = 0;
                Co.TKMD_ID = TKMD.ID;
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Value = Co.ThoiHanNop;
                }

            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void chkNoCo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoCo.Checked)
            {
                lblNgayNopCo.Visible = true;
                ccNgayNopCO.Visible = true;
            }
            else
            {
                lblNgayNopCo.Visible = false;
                ccNgayNopCO.Visible = false;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (Co != null && Co.ID > 0)
            {
                Co.Delete();
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (Co.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool thanhcong = Co.WSKhaiBaoBoSungCO(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy.Year, TKMD.ID, TKMD.MaDoanhNghiep, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao);
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Khai báo không thành công : " + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                if (this.Co.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.Co.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Nội dung :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = Co.WSLaySoTiepNhan(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCOThanhCong);
                    if (string.IsNullOrEmpty(message))
                        message = string.Format("Số tiếp nhận {0}, ngày tiếp nhận {1}", Co.SoTiepNhan, Co.NgayTiepNhan);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.Co.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.Co.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.Co.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = Co.WSLayPhanHoi(password, Company.GC.BLL.EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCODuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    if (string.IsNullOrEmpty(message)) message = "Lấy phản hồi thành công";
                    this.ShowMessage(message, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Dia chi TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick cho các TextBox Địa chỉ người nhập khẩu, xuất khẩu.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DiaChi()
        {
            txtDiaChiNguoiNK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtDiaChiNguoiXK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtDiaChiNguoiNK.ButtonClick += new EventHandler(txtDiaChiNguoiNK_ButtonClick);

            txtDiaChiNguoiXK.ButtonClick += new EventHandler(txtDiaChiNguoiXK_ButtonClick);
        }

        private void txtDiaChiNguoiNK_ButtonClick(object sender, EventArgs e)
        {
            txtDiaChiNguoiNK.Text = Globals.GetMaDonViString();
        }

        private void txtDiaChiNguoiXK_ButtonClick(object sender, EventArgs e)
        {
            txtDiaChiNguoiXK.Text = Globals.GetMaDonViString();
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE CO

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateCO()
        {
            bool isValid = true;

            //So_CO	varchar(50)
            isValid &= Globals.ValidateNull(txtSoCO, err, "Số CO");
            if (isValid)
                isValid &= Globals.ValidateLength(txtSoCO, 50, err, "Số CO");

            //Ngay cap CO
            isValid &= Globals.ValidateNull(ccNgayCO, err, "Ngày cấp CO");

            //Ma_Loai_CO	char(3)
            isValid &= Globals.ValidateNull(cbLoaiCO, err, "Mã loại CO");
            if (isValid)
                isValid &= Globals.ValidateLength(cbLoaiCO, 3, err, "Mã loại CO");

            //NGUOI_XUAT	nvarchar(255)
            isValid &= Globals.ValidateLength(txtDiaChiNguoiXK, 255, err, "Địa chỉ người xuất khẩu CO");

            //NGUOI_NHAP	nvarchar(255)
            isValid &= Globals.ValidateLength(txtDiaChiNguoiNK, 255, err, "Địa chỉ người nhập khẩu CO");

            return isValid;
        }

        #endregion End VALIDATE CO

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCO(Company.GC.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.CO co)
        {
            if (co == null)
                return false;
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                bool status = false;

                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkmd.SoTiepNhan == 0
                        || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                        || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);

                    btnXoa.Enabled = status;
                    btnChonGP.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = false;
                    btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkmd.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        if (co.SoTiepNhan > 0)
                            status = false;
                        else
                            status = true;

                        btnXoa.Enabled = status;
                        btnChonGP.Enabled = status;
                        btnGhi.Enabled = status;
                        btnKetQuaXuLy.Enabled = true;
                        //Neu hop dong chua co so tiep nhan -> phai khai bao
                        if (co.SoTiepNhan == 0)
                        {
                            btnKhaiBao.Enabled = true;
                            btnLayPhanHoi.Enabled = false;
                        }
                        //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                        else
                        {
                            btnKhaiBao.Enabled = false;
                            //btnLayPhanHoi.Enabled = true;
                        }
                    }
                }
            }
            else
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (Co.GuidStr != null && Co.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(Co.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

    }
}

