﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_CertificateOfOrigin : BaseForm
    {
        public KDT_VNACCS_CertificateOfOrigin certificateOfOrigin = new KDT_VNACCS_CertificateOfOrigin();
        public List<KDT_VNACC_CertificateOfOrigin> ListcertificateOfOrigin = new List<KDT_VNACC_CertificateOfOrigin>();
        public KDT_VNACC_CertificateOfOrigin_Detail certificateOfOriginDetail = new KDT_VNACC_CertificateOfOrigin_Detail();
        public List<KDT_VNACC_CertificateOfOrigin_Detail> ListCertificateOfOriginDetail = new List<KDT_VNACC_CertificateOfOrigin_Detail>();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public VNACC_CertificateOfOrigin()
        {
            InitializeComponent();
        }
        private void Save()
        {
            if (ListcertificateOfOrigin.Count == 0 || ListcertificateOfOrigin == null)
            {
                ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                return;
            }
            if (!ValidateForm(false))
                return;
            getCertificateOfOrigin();
            certificateOfOrigin.InsertUpdateFull();
            BindData();

        }
        public void getCertificateOfOrigin()
        {
            //certificateOfOrigin.TKMD_ID = TKMD.ID;
            //certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            //certificateOfOrigin.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            //certificateOfOrigin.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //certificateOfOrigin.GhiChuKhac = txtGhiChu.Text.ToString();
            //KDT_VNACC_CertificateOfOrigin co = (KDT_VNACC_CertificateOfOrigin)dgList.GetRow().DataRow;
            //certificateOfOrigin.FileName = co.FileName;
            //certificateOfOrigin.FileSize = co.FileSize;
            //certificateOfOrigin.Content = co.Content;
            //certificateOfOriginDetail.SoCO = txtSoCO.Text.ToString();
            //certificateOfOriginDetail.LoaiCO = cbbLoaiCO.SelectedValue.ToString();
            //certificateOfOriginDetail.NgayCap = dateNgayCap.Value;
            //certificateOfOriginDetail.NuocCap = txtNuocCap.Ma.ToString();
            //certificateOfOriginDetail.ToChucCap = txtToChucCap.Text.ToString();
            //certificateOfOriginDetail.NguoiCap = txtNguoiCap.Text.ToString();
            //certificateOfOrigin.ListCertificateOfOrigin.Add(certificateOfOriginDetail);
        }

        public void setCertificateOfOrigin()
        {
            txtSoCO.Text = certificateOfOriginDetail.SoCO;
            cbbLoaiCO.SelectedValue = certificateOfOriginDetail.LoaiCO;
            dateNgayCap.Value = certificateOfOriginDetail.NgayCap;
            txtNuocCap.Ma = certificateOfOriginDetail.NuocCap;
            txtToChucCap.Text = certificateOfOriginDetail.ToChucCap;
            txtNguoiCap.Text = certificateOfOriginDetail.NguoiCap;
            txtGhiChu.Text = certificateOfOrigin.GhiChuKhac;
        }
        public void BindData()
        {
            dgList.DataSource = ListcertificateOfOrigin;
            try
            {
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtSoCO, errorProvider, "Số CO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiCO, errorProvider, "Loại CO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtToChucCap, errorProvider, "Tổ chức cấp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNguoiCap, errorProvider, "Người cấp", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtGhiChu, errorProvider, "Ghi chú khác ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Send()
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (certificateOfOriginDetail.ID < 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    certificateOfOriginDetail = KDT_VNACC_CertificateOfOrigin_Detail.Load(certificateOfOriginDetail.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                sendXML.master_id = certificateOfOrigin.ID;

                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    certificateOfOrigin.GuidStr = Guid.NewGuid().ToString();
                    CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS();
                    //certificateOfOrigins_VNACCS = Mapper_V4.ToDataTransferCertificateOfOrigin(certificateOfOrigin, certificateOfOriginDetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = certificateOfOrigin.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ)),
                              Identity = certificateOfOrigin.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = certificateOfOrigins_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = certificateOfOrigin.GuidStr,
                          },
                          certificateOfOrigins_VNACCS
                        );
                    certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.KhaiBaoBSContainer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        btnAdd.Enabled = false;
                        btnDelete.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                        sendXML.master_id = certificateOfOrigin.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        certificateOfOrigin.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = certificateOfOrigin.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CertificateOfOrigin,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CertificateOfOrigin,
                };
                subjectBase.Type = DeclarationIssuer.CertificateOfOrigin;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = certificateOfOrigin.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ.Trim())),
                                                  Identity = certificateOfOrigin.MaHQ
                                              }, subjectBase, null);
                if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();

            OpenFileDialog.FileName = "";
            OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            OpenFileDialog.Multiselect = false;
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                  && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                  && fin.Extension.ToUpper() != ".gif".ToUpper()
                  && fin.Extension.ToUpper() != ".tiff".ToUpper()
                  && fin.Extension.ToUpper() != ".txt".ToUpper()
                  && fin.Extension.ToUpper() != ".xml".ToUpper()
                  && fin.Extension.ToUpper() != ".xsl".ToUpper()
                  && fin.Extension.ToUpper() != ".csv".ToUpper()
                  && fin.Extension.ToUpper() != ".doc".ToUpper()
                  && fin.Extension.ToUpper() != ".mdb".ToUpper()
                  && fin.Extension.ToUpper() != ".pdf".ToUpper()
                  && fin.Extension.ToUpper() != ".ppt".ToUpper()
                  && fin.Extension.ToUpper() != ".xls".ToUpper())
                {
                    ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    long size = 0;
                    size = fs.Length;
                    byte[] data = new byte[fs.Length];
                    fs.Read(data, 0, data.Length);
                    filebase64 = System.Convert.ToBase64String(data);
                    filesize = fs.Length;
                    KDT_VNACC_CertificateOfOrigin certificateOfOrigin = new KDT_VNACC_CertificateOfOrigin();
                    certificateOfOrigin.FileName = fin.Name;
                    certificateOfOrigin.FileSize = filesize;
                    certificateOfOrigin.Content = data;
                    ListcertificateOfOrigin.Add(certificateOfOrigin);
                    dgList.DataSource = ListcertificateOfOrigin;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch (Exception ex)
                    {
                        dgList.Refresh();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                if (dgList.GetRow() == null)
                {
                    ShowMessage("Bạn chưa chọn File để xem.", false);
                    return;
                }

                KDT_VNACC_CertificateOfOrigin fileData = (KDT_VNACC_CertificateOfOrigin)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.Content, 0, fileData.Content.Length);
                System.Diagnostics.Process.Start(fileName);

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0)
            {
                return;
            }
            if (items.Count < 0)
            {
                return;
            }
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    KDT_VNACC_CertificateOfOrigin co = new KDT_VNACC_CertificateOfOrigin();
                    co = (KDT_VNACC_CertificateOfOrigin)dgList.GetRow().DataRow;
                    if (co == null)
                    {
                        continue;
                    }
                    if (co.ID > 0)
                    {
                        co.Delete();
                    }
                    ListcertificateOfOrigin.Remove(co);
                }
            }
            BindData();
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.CertificateOfOriginSendHandler(certificateOfOrigin, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void uiCommandManager1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //To khai VNACC
                case "cmdSave":
                    this.Save();
                    break;
            }
        }

        private void VNACC_CertificateOfOrigin_Load(object sender, EventArgs e)
        {
            //gridList.DataSource = certificateOfOrigin.LoadBy_TKMD_ID(TKMD.ID).Tables[0];
        }

        private void gridList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (e.Row.RowType == RowType.Record)
                {
                    //certificateOfOrigin.ID = Convert.ToInt64(gridList.GetRow().Cells["ID"].Value.ToString());
                    //certificateOfOrigin.GhiChuKhac = gridList.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    //certificateOfOrigin.FileName = gridList.GetRow().Cells["FileName"].Value.ToString();
                    //certificateOfOrigin.FileSize = Convert.ToDecimal(gridList.GetRow().Cells["FileSize"].Value.ToString());
                    ////certificateOfOrigin.Content = Convert.FromBase64String(gridList.GetRow().Cells["Content"].Value.ToString());
                    //ListcertificateOfOrigin.Add(certificateOfOrigin);
                    //dgList.DataSource = certificateOfOrigin;
                    //certificateOfOriginDetail.SoCO = gridList.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    //certificateOfOriginDetail.LoaiCO = gridList.GetRow().Cells["LoaiCO"].Value.ToString();
                    //certificateOfOriginDetail.NgayCap = Convert.ToDateTime(gridList.GetRow().Cells["NgayCap"].Value);
                    //certificateOfOriginDetail.NuocCap = gridList.GetRow().Cells["NuocCap"].Value.ToString();
                    //certificateOfOriginDetail.ToChucCap = gridList.GetRow().Cells["ToChucCap"].Value.ToString();
                    ////certificateOfOriginDetail.NguoiCap = gridList.GetRow().Cells["NguoiCap"].Value.ToString();
                    //setCertificateOfOrigin();
                    //BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
    }
}
