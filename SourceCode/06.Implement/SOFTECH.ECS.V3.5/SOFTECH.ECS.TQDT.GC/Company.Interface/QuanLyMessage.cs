﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.SXXK;
using System.Xml;
using System.IO;


namespace Company.Interface
{
    public partial class QuanLyMessage : BaseForm
    {
        public bool isAdd = true;
        public MsgSend msgSend = new MsgSend();
        public QuanLyMessage()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        MsgSend msg = (MsgSend)i.GetRow().DataRow;
                        try
                        {
                            msg.Delete();                       
                        }
                        catch { }
                    }
                }
                BindData();
            }
        }

        private void QuanLyMessage_Load(object sender, EventArgs e)
        {
            cbbFuntion.SelectedValue = "8";
            cbbLoaiCT.SelectedValue = "DM";
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = MsgSend.SelectCollectionAll();
                dgList.Refetch();
            }
            catch (Exception exx)
            {
                Logger.LocalLogger.Instance().WriteMessage(exx);
            }
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if(e.Row.RowType==RowType.Record)
            {
                string chucnang = e.Row.Cells["func"].Value.ToString();
                if (chucnang.Trim() == "1")
                    e.Row.Cells["func"].Text = "Hủy";
                else if (chucnang.Trim() == "5")
                    e.Row.Cells["func"].Text = "Sửa";
                else if (chucnang.Trim() == "8")
                    e.Row.Cells["func"].Text = "Khai báo";
                else if (chucnang.Trim() == "12")
                    e.Row.Cells["func"].Text = "Chưa xử lý";
                else if (chucnang.Trim() == "13")
                    e.Row.Cells["func"].Text = "Hỏi trạng thái";
                else if (chucnang.Trim() == "27")
                    e.Row.Cells["func"].Text = "Không chấp nhận";
                else if (chucnang.Trim() == "29")
                    e.Row.Cells["func"].Text = "Cấp số tiếp nhận";
                else if (chucnang.Trim() == "30")
                    e.Row.Cells["func"].Text = "Cấp số tờ khai";
                else if (chucnang.Trim() == "31")
                    e.Row.Cells["func"].Text = "Duyệt chứng từ";

                string loaihs = e.Row.Cells["LoaiHS"].Value.ToString();
                if(loaihs.Trim()=="TK")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai";
                else if (loaihs.Trim() == "HD")
                    e.Row.Cells["LoaiHS"].Text = "Hợp đồng";
                else if (loaihs.Trim() == "PK")
                    e.Row.Cells["LoaiHS"].Text = "Phụ kiện";
                else if (loaihs.Trim() == "DMCU")
                    e.Row.Cells["LoaiHS"].Text = "Định mức cung ứng";
                else if (loaihs.Trim() == "TKCT")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai chuyển tiếp";
                else if (loaihs.Trim() == "DM")
                    e.Row.Cells["LoaiHS"].Text = "Định mức";
                else if (loaihs.Trim() == "License")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Giấy phép";
                else if (loaihs.Trim() == "ContractDocument")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Hợp đồng";
                else if (loaihs.Trim() == "ContractReference")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Hợp đồng";
                else if (loaihs.Trim() == "CommercialInvoice")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Hóa đơn";
                else if (loaihs.Trim() == "CertificateOfOrigin")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ C/O";
                else if (loaihs.Trim() == "BillOfLading")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Vận đơn";
                else if (loaihs.Trim() == "CapSoDinhDanh")
                    e.Row.Cells["LoaiHS"].Text = "Cấp số định danh";
                else if (loaihs.Trim() == "Container")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ Container";
                else if (loaihs.Trim() == "AdditionalDocument")
                    e.Row.Cells["LoaiHS"].Text = "Chứng từ khác";
                else if (loaihs.Trim() == "OverTime")
                    e.Row.Cells["LoaiHS"].Text = "Đăng ký làm ngoài giờ";
                else if (loaihs.Trim() == "GoodItem")
                    e.Row.Cells["LoaiHS"].Text = "Báo cáo quyết toán";
                else if (loaihs.Trim() == "GoodItemContract")
                    e.Row.Cells["LoaiHS"].Text = "Báo cáo quyết toán HĐGC";
                else if (loaihs.Trim() == "StorageAreasProduction")
                    e.Row.Cells["LoaiHS"].Text = "Thông báo Cơ sở sản xuất";
                else if (loaihs.Trim() == "ScrapInformation")
                    e.Row.Cells["LoaiHS"].Text = "Thông tin tiêu hủy";
                else if (loaihs.Trim() == "TransportEquipment")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai vận chuyển độc lập qua KVGS";
                else if (loaihs.Trim() == "WareHouseImport")
                    e.Row.Cells["LoaiHS"].Text = "Phiếu nhập kho";
                else if (loaihs.Trim() == "WareHouseExport")
                    e.Row.Cells["LoaiHS"].Text = "Phiếu xuất kho";
                else if (loaihs.Trim() == "TotalInventoryReport")
                    e.Row.Cells["LoaiHS"].Text = "Báo cáo chốt tồn";
                else if (loaihs.Trim() == "RegisterInformaton")
                    e.Row.Cells["LoaiHS"].Text = "Đăng ký thông tin doanh nghiệp";
                else if (loaihs.Trim() == "RegisterHangContainer")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai nộp phí hàng Container";
                else if (loaihs.Trim() == "RegisterHangRoi")
                    e.Row.Cells["LoaiHS"].Text = "Tờ khai nộp phí hàng rời , lỏng";
                else if (loaihs.Trim() == "SearchInformaton")
                    e.Row.Cells["LoaiHS"].Text = "Tra cứu biên lai";
                else if (loaihs.Trim() == "NPL")
                    e.Row.Cells["LoaiHS"].Text = "Nguyên phụ liệu";
                else if (loaihs.Trim() == "SP")
                    e.Row.Cells["LoaiHS"].Text = "Sản phẩm";
                else if (loaihs.Trim() == "ContKVGS")
                    e.Row.Cells["LoaiHS"].Text = "Danh sách hàng qua KVGS";
            }
        }
        /// <summary>
        /// Formats the provided XML so it's indented and humanly-readable.
        /// </summary>
        /// <param name="inputXml">The input XML to format.</param>
        /// <returns></returns>
        public static string FormatXml(string inputXml)
        {
            XmlDocument document = new XmlDocument();
            document.Load(new StringReader(inputXml));

            XmlNode body = document.SelectNodes("Envelope/Body/Content")[0];
            if (body != null)
            {
                body.InnerXml = Company.KDT.SHARE.Components.Helpers.ConvertFromBase64(body.InnerText);   
            }
            StringBuilder builder = new StringBuilder();
            using (XmlTextWriter writer = new XmlTextWriter(new StringWriter(builder)))
            {
                writer.Formatting = Formatting.Indented;
                document.Save(writer);
            }

            return builder.ToString();
        }
        public class HighlightColors
        {
            public static Color HC_NODE = Color.Firebrick;
            public static Color HC_STRING = Color.Blue;
            public static Color HC_ATTRIBUTE = Color.Red;
            public static Color HC_COMMENT = Color.GreenYellow;
            public static Color HC_INNERTEXT = Color.Black;
        }
        public static void HighlightRTF(RichTextBox rtb)
        {
            int k = 0;

            string str = rtb.Text;

            int st, en;
            int lasten = -1;
            while (k < str.Length)
            {
                st = str.IndexOf('<', k);

                if (st < 0)
                    break;

                if (lasten > 0)
                {
                    rtb.Select(lasten + 1, st - lasten - 1);
                    rtb.SelectionColor = HighlightColors.HC_INNERTEXT;
                }

                en = str.IndexOf('>', st + 1);
                if (en < 0)
                    break;

                k = en + 1;
                lasten = en;

                if (str[st + 1] == '!')
                {
                    rtb.Select(st + 1, en - st - 1);
                    rtb.SelectionColor = HighlightColors.HC_COMMENT;
                    continue;

                }
                String nodeText = str.Substring(st + 1, en - st - 1);


                bool inString = false;

                int lastSt = -1;
                int state = 0;
                /* 0 = before node name
                 * 1 = in node name
                   2 = after node name
                   3 = in attribute
                   4 = in string
                   */
                int startNodeName = 0, startAtt = 0;
                for (int i = 0; i < nodeText.Length; ++i)
                {
                    if (nodeText[i] == '"')
                        inString = !inString;

                    if (inString && nodeText[i] == '"')
                        lastSt = i;
                    else
                        if (nodeText[i] == '"')
                        {
                            rtb.Select(lastSt + st + 2, i - lastSt - 1);
                            rtb.SelectionColor = HighlightColors.HC_STRING;
                        }

                    switch (state)
                    {
                        case 0:
                            if (!Char.IsWhiteSpace(nodeText, i))
                            {
                                startNodeName = i;
                                state = 1;
                            }
                            break;
                        case 1:
                            if (Char.IsWhiteSpace(nodeText, i))
                            {
                                rtb.Select(startNodeName + st, i - startNodeName + 1);
                                rtb.SelectionColor = HighlightColors.HC_NODE;
                                state = 2;
                            }
                            break;
                        case 2:
                            if (!Char.IsWhiteSpace(nodeText, i))
                            {
                                startAtt = i;
                                state = 3;
                            }
                            break;

                        case 3:
                            if (Char.IsWhiteSpace(nodeText, i) || nodeText[i] == '=')
                            {
                                rtb.Select(startAtt + st, i - startAtt + 1);
                                rtb.SelectionColor = HighlightColors.HC_ATTRIBUTE;
                                state = 4;
                            }
                            break;
                        case 4:
                            if (nodeText[i] == '"' && !inString)
                                state = 2;
                            break;


                    }

                }
                if (state == 1)
                {
                    rtb.Select(st + 1, nodeText.Length);
                    rtb.SelectionColor = HighlightColors.HC_NODE;
                }
            }
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    MsgSend msg = (MsgSend)e.Row.DataRow;
                    txtSoThamChieu.Text = msg.master_id.ToString();
                    cbbFuntion.SelectedValue = msg.func.ToString();
                    cbbLoaiCT.SelectedValue = msg.LoaiHS;
                    richTextBox1.Text = FormatXml(msg.msg);
                    HighlightRTF(richTextBox1);
                    //XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(msg.msg));
                    //while (reader.Read())
                    //{

                    //    switch (reader.NodeType)
                    //    {

                    //        case XmlNodeType.Element: // The node is an element.

                    //            this.richTextBox1.SelectionColor = Color.Blue;

                    //            this.richTextBox1.AppendText("<");

                    //            this.richTextBox1.SelectionColor = Color.Brown;

                    //            this.richTextBox1.AppendText(reader.Name);

                    //            this.richTextBox1.SelectionColor = Color.Blue;

                    //            this.richTextBox1.AppendText(">");

                    //            break;

                    //        case XmlNodeType.Text: //Display the text in each element.

                    //            this.richTextBox1.SelectionColor = Color.Black;

                    //            this.richTextBox1.AppendText(reader.Value);

                    //            break;

                    //        case XmlNodeType.EndElement: //Display the end of the element.

                    //            this.richTextBox1.SelectionColor = Color.Blue;

                    //            this.richTextBox1.AppendText("</");

                    //            this.richTextBox1.SelectionColor = Color.Brown;

                    //            this.richTextBox1.AppendText(reader.Name);

                    //            this.richTextBox1.SelectionColor = Color.Blue;

                    //            this.richTextBox1.AppendText(">");

                    //            this.richTextBox1.AppendText("\n");

                    //            break;

                    //    }

                    //}
                    //richTextBox1.Text = FormatXml(msg.msg);
                    //treeView1.Nodes.Clear();
                    //// Load the XML Document
                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(msg.msg);
                    //ConvertXmlNodeToTreeNode(doc, treeView1.Nodes);
                    //treeView1.Nodes[0].ExpandAll();
                    isAdd = false;
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {

            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "ATTRIBUTE: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoThamChieu, errorProvider, "Số tham chiếu", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbFuntion, errorProvider, "Chức năng", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbLoaiCT, errorProvider, "Loại chứng từ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (msgSend == null)
                    msgSend = new MsgSend();
                if (!ValidateForm(false))
                    return;
                msgSend.master_id = Convert.ToInt32(txtSoThamChieu.Text);
                msgSend.LoaiHS = cbbLoaiCT.SelectedValue.ToString();
                msgSend.func = Convert.ToInt32(cbbFuntion.SelectedValue.ToString());
                msgSend.msg = richTextBox1.Text.ToString();
                if (!String.IsNullOrEmpty(richTextBox1.Text) && richTextBox1.Text.ToString().TrimStart().StartsWith("<"))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(richTextBox1.Text);
                    XmlNode body = xmlDoc.SelectNodes("Envelope/Body/Content")[0];
                    if (body != null)
                    {
                        body.InnerText = Company.KDT.SHARE.Components.Helpers.ConvertToBase64(body.InnerXml);
                    }
                    msgSend.msg = xmlDoc.InnerXml;   
                }
                msgSend.InsertUpdate();
                msgSend = new MsgSend();
                txtSoThamChieu.Text = String.Empty;
                isAdd = true;
                richTextBox1.Text = String.Empty;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (isAdd)
                {
                    string where = " 1 = 1";
                    if (!String.IsNullOrEmpty(txtSoThamChieu.Text.ToString()))
                        where += "AND master_id = " + txtSoThamChieu.Text.ToString();
                    if (cbbLoaiCT.SelectedValue != null)
                        where += "AND LoaiHS = '" + cbbLoaiCT.SelectedValue.ToString() + "'";
                    if(cbbFuntion.SelectedValue != null)
                        where += "AND func = " + cbbFuntion.SelectedValue.ToString();
                    dgList.Refresh();
                    dgList.DataSource = MsgSend.SelectCollectionDynamic(where, "");
                    dgList.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadTreeFromXmlDocument(XmlDocument dom)
        {
            try
            {
                // SECTION 2. Initialize the TreeView control.
                treeView1.Nodes.Clear();

                // SECTION 3. Populate the TreeView with the DOM nodes.
                foreach (XmlNode node in dom.DocumentElement.ChildNodes)
                {
                    if (node.Name == "namespace" && node.ChildNodes.Count == 0 && string.IsNullOrEmpty(GetAttributeText(node, "name")))
                        continue;
                    AddNode(treeView1.Nodes, node);
                }

                treeView1.ExpandAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        static string GetAttributeText(XmlNode inXmlNode, string name)
        {
            XmlAttribute attr = (inXmlNode.Attributes == null ? null : inXmlNode.Attributes[name]);
            return attr == null ? null : attr.Value;
        }

        private void AddNode(TreeNodeCollection nodes, XmlNode inXmlNode)
        {
            if (inXmlNode.HasChildNodes)
            {
                string text = GetAttributeText(inXmlNode, "name");
                if (string.IsNullOrEmpty(text))
                    text = inXmlNode.Name;
                TreeNode newNode = nodes.Add(text);
                XmlNodeList nodeList = inXmlNode.ChildNodes;
                for (int i = 0; i <= nodeList.Count - 1; i++)
                {
                    XmlNode xNode = inXmlNode.ChildNodes[i];
                    AddNode(newNode.Nodes, xNode);
                }
            }
            else
            {
                // If the node has an attribute "name", use that.  Otherwise display the entire text of the node.
                string text = GetAttributeText(inXmlNode, "name");
                if (string.IsNullOrEmpty(text))
                    text = (inXmlNode.OuterXml).Trim();
                TreeNode newNode = nodes.Add(text);
            }
        }
    }
}

