﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ToKhaiVanChuyen
    {
        List<KDT_VNACC_TKVC_VanDon> _ListVanDon = new List<KDT_VNACC_TKVC_VanDon>();
        List<KDT_VNACC_TKVC_Container> _ListContainer = new List<KDT_VNACC_TKVC_Container>();
        List<KDT_VNACC_TKVC_TKXuat> _ListTKXuat = new List<KDT_VNACC_TKVC_TKXuat>();
        List<KDT_VNACC_TKVC_TrungChuyen> _ListTrungChuyen = new List<KDT_VNACC_TKVC_TrungChuyen>();

        public List<KDT_VNACC_TKVC_VanDon> VanDonCollection
        {
            set { this._ListVanDon = value; }
            get { return this._ListVanDon; }
        }
        public List<KDT_VNACC_TKVC_Container> ContainerCollection
         {
             set { this._ListContainer = value; }
             get { return this._ListContainer; }
         }
        public List<KDT_VNACC_TKVC_TKXuat> TKXuatCollection
        {
            set { this._ListTKXuat = value; }
            get { return this._ListTKXuat; }
        }
        public List<KDT_VNACC_TKVC_TrungChuyen> TrungChuyenCollection
        {
            set { this._ListTrungChuyen = value; }
            get { return this._ListTrungChuyen; }
        }

        public bool InsertUpdateFull()
         {
             bool ret;
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {
                     if (this.ID == 0)
                     {
                         this.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                         this.ID = this.Insert( transaction);
                     }
                     else
                         this.Update(transaction);

                     // luu hang to khai
                     foreach (KDT_VNACC_TKVC_VanDon item in this.VanDonCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert(transaction);
                         }
                         else
                         {
                             item.Update(transaction);
                         }
                     }
                     // luu container vao to khai
                     foreach (KDT_VNACC_TKVC_Container item in this.ContainerCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert(transaction);
                         }
                         else
                         {
                             item.Update(transaction);
                         }
                     }
                     // luu so tơ khai xuat vao to khai
                     foreach (KDT_VNACC_TKVC_TKXuat item in this.TKXuatCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert(transaction);
                         }
                         else
                         {
                             item.Update(transaction);
                         }
                     }
                     // luu dia diem trung chuyen
                     foreach (KDT_VNACC_TKVC_TrungChuyen item in this.TrungChuyenCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert(transaction);
                         }
                         else
                         {
                             item.Update(transaction);
                         }
                     }
                     transaction.Commit();
                     ret = true;
                 }
                 catch (Exception ex)
                 {
                     transaction.Rollback();
                     this.ID = 0;
                     throw new Exception(ex.Message);
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
             return ret;
         }
         public KDT_VNACC_ToKhaiVanChuyen LoadToKhai(long id)
         {
             List<KDT_VNACC_ToKhaiVanChuyen> ListTKVC = new List<KDT_VNACC_ToKhaiVanChuyen>();
             ListTKVC = KDT_VNACC_ToKhaiVanChuyen.SelectCollectionAll();
             foreach (KDT_VNACC_ToKhaiVanChuyen TKVC in ListTKVC)
             {
                 if (TKVC.ID == id)
                 {
                     string where = "Master_ID=" + TKVC.ID;
                     //Hang mau dich Collection
                     List<KDT_VNACC_TKVC_VanDon> listVanDon = new List<KDT_VNACC_TKVC_VanDon>();
                     listVanDon = KDT_VNACC_TKVC_VanDon.SelectCollectionDynamic(where, "ID");
                     foreach (KDT_VNACC_TKVC_VanDon item in listVanDon)
                     {
                         TKVC.VanDonCollection.Add(item);
                     }
                     // giay phep Collection
                     List<KDT_VNACC_TKVC_Container> listContainer = new List<KDT_VNACC_TKVC_Container>();
                     listContainer = KDT_VNACC_TKVC_Container.SelectCollectionDynamic(where, "ID");
                     foreach (KDT_VNACC_TKVC_Container item in listContainer)
                     {
                         TKVC.ContainerCollection.Add(item);
                     }
                     // so dien tu dinh kem
                     List<KDT_VNACC_TKVC_TKXuat> listTKXuat = new List<KDT_VNACC_TKVC_TKXuat>();
                     listTKXuat = KDT_VNACC_TKVC_TKXuat.SelectCollectionDynamic(where, "ID");
                     foreach (KDT_VNACC_TKVC_TKXuat item in listTKXuat)
                     {
                         TKVC.TKXuatCollection.Add(item);
                     }
                     // so dien tu dinh kem
                     List<KDT_VNACC_TKVC_TrungChuyen> listTrungChuyen = new List<KDT_VNACC_TKVC_TrungChuyen>();
                     listTrungChuyen = KDT_VNACC_TKVC_TrungChuyen.SelectCollectionDynamic(where, "ID");
                     foreach (KDT_VNACC_TKVC_TrungChuyen item in listTrungChuyen)
                     {
                         TKVC.TrungChuyenCollection.Add(item);
                     }
                    
                     return TKVC;
                 }
             }
             return null;
         }
         public bool DeleteFull(long TKVC_ID)
         {
             bool ret =false;
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {

                     string where_TKVC = "TKVC_ID =" + TKVC_ID;
                     string where_Master = "Master_ID =" + TKVC_ID;
                     // Xóa hàng
                     KDT_VNACC_TKVC_VanDon.DeleteDynamic(where_Master);
                     KDT_VNACC_TKVC_Container.DeleteDynamic(where_Master);
                     KDT_VNACC_TKVC_TKXuat.DeleteDynamic(where_Master);
                     KDT_VNACC_TKVC_TrungChuyen.DeleteDynamic(where_Master);
                     KDT_VNACC_ToKhaiVanChuyen.DeleteDynamic("ID="+TKVC_ID);
                     transaction.Commit();
                     ret = true;
                 }
                 catch (Exception ex)
                 {
                     transaction.Rollback();
                     this.ID = 0;
                     connection.Close();
                     throw new Exception(ex.Message);
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
             return ret;
         }
    }
}
