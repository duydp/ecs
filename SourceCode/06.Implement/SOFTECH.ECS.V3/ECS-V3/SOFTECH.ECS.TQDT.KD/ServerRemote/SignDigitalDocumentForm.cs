﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.Reflection;
using System.Configuration;
//using Microsoft.Office.Core;
using System.Security.Cryptography.X509Certificates;
using iTextSharp.text.log;
using Org.BouncyCastle.Security;
//using iTextSharp.text.pdf.security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Security;
using iTextSharp.text;
using iTextSharp.text.log;
using iTextSharp.text.pdf;
//using iTextSharp.text.pdf.security;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;
using System.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Xml;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Security.Cryptography.Xml;
//using System.IO.Packaging;
using Company.KDT.SHARE.Components;
using KeySecurity;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class SignDigitalDocumentForm : BaseForm
    {
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public DataTable dt;
        public bool IsSelectFolder = true;
        public IntPtr parentFormHandler;
        public byte[] dataSigned;
        public string FilePath;
        public byte[] bytes;
        public SignDigitalDocumentForm()
        {
            InitializeComponent();
        }

        private void SignDigitalDocumentForm_Load(object sender, EventArgs e)
        {
            try
            {
                btnRefresh_Click(null,null);
                cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                chkIsUseSign.Checked = Company.KDT.SHARE.Components.Globals.IsSignature;
                chIsRemember.Checked = Company.KDT.SHARE.Components.Globals.IsRememberSign;
                cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                txtPassword.Text = Company.KDT.SHARE.Components.Globals.PasswordSign;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save(ConfigurationSaveMode.Full);

            ConfigurationManager.RefreshSection("appSettings");
        }
        private DataTable CreateDataTable()
        {
            DataTable dtFile = new DataTable();
            DataColumn dtColFileName = new DataColumn("FileName");
            dtColFileName.DataType = typeof(System.String);

            DataColumn dtColFileNameSign = new DataColumn("FileNameSigned");
            dtColFileNameSign.DataType = typeof(System.String);

            DataColumn dtColFilePath = new DataColumn("FilePath");
            dtColFilePath.DataType = typeof(System.String);

            dtFile.Columns.Add(dtColFileName);
            dtFile.Columns.Add(dtColFileNameSign);
            dtFile.Columns.Add(dtColFilePath);

            return dtFile;
        }
        private void btnFolderSign_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsSelectFolder)
                {
                    FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
                    if (targetDirectory.ShowDialog() != DialogResult.OK)
                        return;
                    txtFolderSign.Text = targetDirectory.SelectedPath;
                    txtFolderSigned.Text = targetDirectory.SelectedPath;
                }
                //if (chkFolder.Checked)
                //{
                //    UpdateSetting("FolderSign", txtFolderSign.Text);
                //    UpdateSetting("FolderSigned", txtFolderSigned.Text);
                //}
                dt = CreateDataTable();
                string[] getFile = Directory.GetFiles(txtFolderSign.Text);
                foreach (string filePath in getFile)
                {
                    DataRow dr = dt.NewRow();
                    string fileName = Path.GetFileName(filePath);
                    if (fileName.ToLower().Contains("pdf"))
                    {
                        //Check Exits Signed
                        if (SoftechSignLibrary.CheckExitsSignature(filePath))
                        {
                            PdfReader reader = new PdfReader(filePath);
                            dr["FileName"] = fileName;
                            string fileSigned = fileName.Replace(".pdf", "-Signed.pdf");
                            dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;//filePath.Replace(fileName,fileSigned);
                            dr["FilePath"] = filePath;
                            dt.Rows.Add(dr);
                        }
                    }
                    else if (fileName.Contains(".docx"))
                    {
                        // Check Exits Signed
                        if (!SoftechSignLibrary.isSignedExcel(filePath))
                        {
                            object TemFilePath = filePath;
                            dr["FileName"] = fileName;
                            string fileSigned = fileName.Replace(".docx", "-Signed.docx");
                            dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;
                            dr["FilePath"] = filePath;
                            dt.Rows.Add(dr);
                        }
                    }
                    else if (fileName.Contains(".xlsx"))
                    {
                        // Check Exits Signed
                        if (!SoftechSignLibrary.isSignedExcel(filePath))
                        {
                            object TemFilePath = filePath;
                            dr["FileName"] = fileName;
                            string fileSigned = fileName.Replace(".xlsx", "-Signed.xlsx");
                            dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + fileSigned;
                            dr["FilePath"] = filePath;
                            dt.Rows.Add(dr);
                        }
                    }
                    else
                    {

                    }
                }
                if (dt.Rows.Count == 0)
                {
                    ServerRemote.MessageBoxControl.ShowMessage("Không tìm thấy File  trong thư mục lưu File cần ký .Vui lòng kiểm tra lại thư mục (File cần ký phải đúng định dạng là : .docx || .xlsx || .pdf) ");
                }
                dgListFileSign.DataSource = dt;
                IsSelectFolder = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Lỗi lấy danh sách File  trong thư mục lưu file cần ký. \n\r" + ex,false);
            }
        }

        private void btnFolderSigned_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
            if (targetDirectory.ShowDialog() != DialogResult.OK)
                return;
            txtFolderSigned.Text = targetDirectory.SelectedPath;
            foreach (DataRow dr in dt.Rows)
            {
                dr["FileNameSigned"] = txtFolderSigned.Text + "\\" + dr["FileName"];
            }
        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            cbSigns.Items.Clear();
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                string name = "";
                name = GetName(signName);
                cbSigns.Items.Add(new Janus.Windows.EditControls.UIComboBoxItem() { Text = name, Value = signName });
            }
        }
        private string Check()
        {
            string msg = string.Empty;
            try
            {
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = cbSigns.SelectedValue.ToString();
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignature = true;
                Helpers.GetSignature("Test");
                btnCheckConnect.ImageIndex = 0;
                return "Mật khẩu được xác nhận là hợp lệ";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return ex.Message;
            }
        }
        private void btnCheckConnect_Click(object sender, EventArgs e)
        {
            ServerRemote.MessageBoxControl.ShowMessage(Check());
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                //isValid &= ValidateControl.ValidateNull(txtFolderSign, errorProvider, "Thư mục lưu file cần ký", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtFolderSigned, errorProvider, "Thư mục lưu file đã ký", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtPassword, errorProvider, "Password", isOnlyWarning);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        //public void Sign(String src, String dest,
        //                 ICollection<X509Certificate> chain, X509Certificate2 pk,
        //                 String digestAlgorithm, CryptoStandard subfilter,
        //                 String reason, String location,
        //                 ICollection<ICrlClient> crlList,
        //                 IOcspClient ocspClient,
        //                 ITSAClient tsaClient,
        //                 int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        reader = new PdfReader(src);
        //        os = new FileStream(dest, FileMode.Create);
        //        stamper = PdfStamper.CreateSignature(reader, os, '\0');
        //        // Creating the appearance
        //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //        //appearance.Reason = reason;
        //        //appearance.Location = location;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, "sig");
        //        appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
        //        appearance.Layer2Font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN);

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        var secure = new SecureString();
        //        foreach (char c in txtPassword.Text)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //private void btnSign_Click(object sender, EventArgs e)
        //{
        //    //btnCheckConnect_Click(null, null);
        //    if (!ValidateForm(false))
        //        return;
        //    if (dt.Rows.Count == 0)
        //    {
        //        ShowMessageTQDT("Không tìm thấy File trong thư mục lưu File cần ký .Vui lòng kiểm tra lại thư mục ",false);
        //        return;
        //    }

        //    try
        //    {
        //        #region
        //        LoggerFactory.GetInstance().SetLogger(new SysoLogger());


        //        X509Store x509Store = new X509Store("My");
        //        x509Store.Open(OpenFlags.ReadOnly);
        //        X509Certificate2Collection certificates = x509Store.Certificates;
        //        IList<X509Certificate> chain = new List<X509Certificate>();
        //        X509Certificate2 pk = null;
        //        string items = cbSigns.SelectedValue.ToString();
        //        //ItemSign items = (ItemSign)cbSigns.SelectedValue;
        //        if (certificates.Count > 0)
        //        {
        //            foreach (X509Certificate2 cer in certificates)
        //            {

        //                if (cer.SubjectName.Name == items)
        //                {
        //                    X509Certificate2Collection certificateCollection = new X509Certificate2Collection();
        //                    certificateCollection.Add(cer);
        //                    X509Certificate2Enumerator certificatesEn = certificateCollection.GetEnumerator();
        //                    certificatesEn.MoveNext();
        //                    pk = certificatesEn.Current;

        //                    X509Chain x509chain = new X509Chain();
        //                    x509chain.Build(pk);

        //                    foreach (X509ChainElement x509ChainElement in x509chain.ChainElements)
        //                    {
        //                        chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
        //                    }
        //                }
        //            }
        //        }
        //        x509Store.Close();


        //        IOcspClient ocspClient = new OcspClientBouncyCastle();
        //        ITSAClient tsaClient = null;
        //        for (int i = 0; i < chain.Count; i++)
        //        {
        //            X509Certificate cert = chain[i];
        //            String tsaUrl = CertificateUtil.GetTSAURL(cert);
        //            if (tsaUrl != null)
        //            {
        //                tsaClient = new TSAClientBouncyCastle(tsaUrl);
        //                break;
        //            }
        //        }
        //        IList<ICrlClient> crlList = new List<ICrlClient>();
        //        crlList.Add(new CrlClientOnline(chain));
        //        string item = cbSigns.SelectedValue.ToString();
        //        //ItemSign item = (ItemSign)cbSigns.SelectedValue;
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            if (pk.SubjectName.Name == item)
        //            {
        //                // Check Digital Signed
        //                PdfReader reader = new PdfReader(dr["FilePath"].ToString());
        //                List<String> signatureNames = reader.AcroFields.GetSignatureNames();
        //                if (dr["FileName"].ToString().ToLower().Contains(".pdf"))
        //                {
        //                    Sign(dr["FilePath"].ToString(), dr["FileNameSigned"].ToString(), chain, pk, DigestAlgorithms.SHA1, CryptoStandard.CMS, "Test",
        //                             "Ghent",
        //                             crlList, ocspClient, tsaClient, 0);

        //                }
        //                else if (dr["FileName"].ToString().Contains(".docx"))
        //                {
        //                    SignWord(dr["FilePath"].ToString(), dr["FileName"].ToString(), pk);
        //                }
        //                else if (dr["FileName"].ToString().Contains(".xlsx"))
        //                {
        //                    SignExcel(dr["FilePath"].ToString(), dr["FileName"].ToString(), pk);
        //                }
        //                else if (dr["FileName"].ToString().Contains(".xml"))
        //                {
        //                    SignXmlFile(dr["FileName"].ToString(), dr["FilePath"].ToString(), item);
        //                }
        //            }
        //        }
        //        #endregion
        //       ShowMessageTQDT("Ký  thành công ",false);
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessageTQDT("Ký không thành công \r\n" + ex.Message,false);
        //    }
        //}
        //private bool SignWord(string FileTempName, string FileName, X509Certificate2 pk)
        //{
        //    object Visible = false;
        //    object readonlyfile = false;

        //    //string FolderTemp = System.Windows.Forms.Application.StartupPath + "\\FolderTemp";
        //    //if (!Directory.Exists(FolderTemp))
        //    //{
        //    //    Directory.CreateDirectory(FolderTemp);
        //    //}
        //    var source = Path.Combine(txtFolderSign.Text, FileName);
        //    var destination = Path.Combine(txtFolderSigned.Text, FileName.Replace(".docx", " Signed.docx"));
        //    object TempFilePath = txtFolderSigned.Text + "\\" + FileName.Replace(".docx", " Signed.docx");
        //    if (File.Exists(TempFilePath.ToString()))
        //    {
        //        File.Delete(TempFilePath.ToString());
        //    }
        //    File.Copy(source, destination);
        //    try
        //    {
        //        object missing = System.Reflection.Missing.Value;
        //        Microsoft.Office.Interop.Word.ApplicationClass wordapp = new
        //        Microsoft.Office.Interop.Word.ApplicationClass();
        //        Microsoft.Office.Interop.Word.Document wordDocument = wordapp.Documents.Open(ref
        //        TempFilePath, ref missing,
        //        ref readonlyfile, ref missing, ref missing,
        //        ref missing, ref missing, ref missing,
        //        ref missing, ref missing, ref missing,
        //        ref Visible, ref missing, ref missing,
        //        ref missing, ref missing);
        //        SignatureSet signatureSet = wordDocument.Signatures;
        //        wordapp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;
        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        var secure = new SecureString();
        //        foreach (char c in txtPassword.Text)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);

        //        rsa.PersistKeyInCsp = true;
        //        this.MinimumSize = new System.Drawing.Size(623, 603);
        //        Microsoft.Office.Core.Signature objSignature = signatureSet.Add();
        //        wordapp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
        //        if (objSignature == null)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            signatureSet.Commit();
        //            object saveChanges = true;
        //            wordDocument.Close(ref saveChanges, ref missing, ref missing);
        //            wordapp.Quit(ref missing, ref missing, ref missing);
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        //private bool SignExcel(string FileTempName, string FileName, X509Certificate2 pk)
        //{
        //    var source = Path.Combine(txtFolderSign.Text, FileName);
        //    var destination = Path.Combine(txtFolderSigned.Text, FileName.Replace(".xlsx", " Signed.xlsx"));
        //    object TempFilePath = txtFolderSigned.Text + "\\" + FileName.Replace(".xlsx", " Signed.xlsx");
        //    if (File.Exists(TempFilePath.ToString()))
        //    {
        //        File.Delete(TempFilePath.ToString());
        //    }
        //    File.Copy(source, destination);
        //    Microsoft.Office.Interop.Excel.Application xlApp;
        //    Microsoft.Office.Interop.Excel.Workbooks xlWBs;
        //    Microsoft.Office.Interop.Excel.Workbook xlWB;
        //    Microsoft.Office.Interop.Excel.Sheets xlSheets;
        //    Microsoft.Office.Interop.Excel.Worksheet xlWS;
        //    Microsoft.Office.Interop.Excel.PivotTable xlPT;

        //    xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    xlApp.Visible = true;
        //    xlWBs = xlApp.Workbooks;

        //    xlWB = xlWBs.Open(TempFilePath.ToString(), Missing.Value, Missing.Value,
        //    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
        //    Missing.Value, Missing.Value, Missing.Value, Missing.Value);

        //    //Microsoft.Office.Interop.Excel.Application xlapp = null;
        //    //xlapp = new Microsoft.Office.Interop.Excel.Application();
        //    //Microsoft.Office.Interop.Excel.Workbook xlwb = xlapp.Workbooks.Add("");
        //    //object sigID = "{00000000-0000-0000-0000-000000000000}";
        //    //xlapp.DisplayAlerts = false;
        //    //xlwb.Signatures.AddNonVisibleSignature(sigID);
        //    //xlapp.DisplayAlerts = true;
        //    SignatureSet signatureSet = xlWB.Signatures;

        //    RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;
        //    CspParameters cspp = new CspParameters();
        //    cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //    cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;

        //    cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //    var secure = new SecureString();
        //    foreach (char c in txtPassword.Text)
        //    {
        //        secure.AppendChar(c);
        //    }

        //    cspp.KeyPassword = secure;
        //    cspp.Flags = CspProviderFlags.UseExistingKey;

        //    RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);

        //    rsa.PersistKeyInCsp = true;
        //    this.MinimumSize = new System.Drawing.Size(623, 603);
        //    Microsoft.Office.Core.Signature objSignature = signatureSet.Add();
        //    if (objSignature == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        signatureSet.Commit();
        //        object saveChanges = true;
        //        xlWB.Close(Missing.Value, Missing.Value, Missing.Value);
        //        xlApp.Quit();
        //        return true;
        //    }
        //}
        public  X509Certificate2 GetCertificateBySubject(string CertificateSubject)
        {
            // Check the args.
            if (null == CertificateSubject)
                ServerRemote.MessageBoxControl.ShowMessage("CertificateSubject");
            //throw new ArgumentNullException("CertificateSubject");


            // Load the certificate from the certificate store.
            X509Certificate2 cert = null;

            X509Store store = new X509Store("My", StoreLocation.CurrentUser);

            try
            {
                // Open the store.
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

                // Get the certs from the store.
                X509Certificate2Collection CertCol = store.Certificates;

                // Find the certificate with the specified subject.
                foreach (X509Certificate2 c in CertCol)
                {
                    if (c.Subject == CertificateSubject)
                    {
                        cert = c;
                        break;
                    }
                }

                // Throw an exception of the certificate was not found.
                if (cert == null)
                {
                    ServerRemote.MessageBoxControl.ShowMessage("The certificate could not be found.");
                    //throw new CryptographicException("The certificate could not be found.");
                }
            }
            catch (Exception ex)
            {
                ServerRemote.MessageBoxControl.ShowMessage(ex.Message);
            }
            finally
            {
                // Close the store even if an exception was thrown.
                store.Close();
            }

            return cert;
        }
        public void SignXmlFile(string FileName, string SignedFileName, string SubjectName)
        {
            if (null == FileName)
                throw new ArgumentNullException("FileName");
            if (null == SignedFileName)
                throw new ArgumentNullException("SignedFileName");
            if (null == SubjectName)
                throw new ArgumentNullException("SubjectName");

            // Load the certificate from the certificate store.
            X509Certificate2 cert = GetCertificateBySubject(SubjectName);

            // Create a new XML document.
            XmlDocument doc = new XmlDocument();

            // Format the document to ignore white spaces.
            doc.PreserveWhitespace = false;

            // Load the passed XML file using it's name.
            doc.Load(new XmlTextReader(FileName));

            // Create a SignedXml object.
            SignedXml signedXml = new SignedXml(doc);

            // Add the key to the SignedXml document. 
            signedXml.SigningKey = cert.PrivateKey;

            // Create a reference to be signed.
            System.Security.Cryptography.Xml.Reference reference = new System.Security.Cryptography.Xml.Reference();
            reference.Uri = "";

            // Add an enveloped transformation to the reference.
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);

            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            // Create a new KeyInfo object.
            System.Security.Cryptography.Xml.KeyInfo keyInfo = new System.Security.Cryptography.Xml.KeyInfo();

            // Load the certificate into a KeyInfoX509Data object
            // and add it to the KeyInfo object.
            keyInfo.AddClause(new KeyInfoX509Data(cert));

            // Add the KeyInfo object to the SignedXml object.
            signedXml.KeyInfo = keyInfo;

            // Compute the signature.
            signedXml.ComputeSignature();

            // Get the XML representation of the signature and save
            // it to an XmlElement object.
            XmlElement xmlDigitalSignature = signedXml.GetXml();

            // Append the element to the XML document.
            doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));


            if (doc.FirstChild is XmlDeclaration)
            {
                doc.RemoveChild(doc.FirstChild);
            }

            // Save the signed XML document to a file specified
            // using the passed string.
            using (XmlTextWriter xmltw = new XmlTextWriter(SignedFileName, new UTF8Encoding(false)))
            {
                doc.WriteTo(xmltw);
                xmltw.Close();
            }

        }
        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            if (String.Empty!=txtFolderSigned.Text)
            {
                Process.Start(txtFolderSigned.Text);
            }
        }

        private void cbSigns_SelectedValueChanged(object sender, EventArgs e)
        {
            string signName = cbSigns.SelectedValue.ToString();
            X509Certificate2 x059 = Cryptography.GetStoreX509Certificate2(signName);

            if (x059 != null)
            {
                lblNhaCungCap.Text = GetName(x059.IssuerName.Name);
            }
        }

        private void dgListFileSign_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                DataRowView dr = (DataRowView)dgListFileSign.GetRow().DataRow;
                String filePath = dr["FilePath"].ToString();
                String fileName = dr["FileName"].ToString();
                Cursor = Cursors.WaitCursor;

                string path = System.Windows.Forms.Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                System.IO.FileStream fs;
                byte[] bytes;
                fileName = path + "\\" + fileName;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                System.IO.FileStream fsRead = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                size = 0;
                size = fsRead.Length;
                data = new byte[fsRead.Length];
                fsRead.Read(data, 0, data.Length);
                filebase64 = System.Convert.ToBase64String(data);

                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                bytes = System.Convert.FromBase64String(filebase64);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkHDSD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=1ZShUo3fBI4&list=PLFhBUYQraey7iikiKodZ9B3NoB5plC25c");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            //PDFCompressForm f = new PDFCompressForm();
            //f.ShowDialog(this);
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                if (dt.Rows.Count == 0)
                {
                    ServerRemote.MessageBoxControl.ShowMessage("Không tìm thấy File trong thư mục lưu File cần ký .Vui lòng kiểm tra lại thư mục ");
                    return;
                }
                foreach (DataRow dr in dt.Rows)
                {
                    X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                    if (dr["FileName"].ToString().ToLower().Contains(".pdf"))
                    {
                        if (SoftechSignLibrary.SoftechSign(x509Certificate2, dr["FilePath"].ToString(), txtFolderSigned.Text.ToString()))
                        {
                            count ++;
                        }
                    }
                    else if (dr["FileName"].ToString().Contains(".docx"))
                    {
                        if (SoftechSignLibrary.SignOfficeDocument(dr["FilePath"].ToString(), x509Certificate2))
                        {
                            count++;
                        }
                    }
                    else if (dr["FileName"].ToString().Contains(".xlsx"))
                    {
                        if (SoftechSignLibrary.SignOfficeDocument(dr["FilePath"].ToString(), x509Certificate2))
                        {
                            count++;
                        }
                    }
                    else if (dr["FileName"].ToString().Contains(".xml"))
                    {
                        //SignXmlFile(dr["FileName"].ToString(), dr["FilePath"].ToString(), item);
                    }
                }
                ServerRemote.MessageBoxControl.ShowMessage("Tổng số File ký thành công : " + count + " File");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
