﻿namespace Company.Interface
{
    partial class GiayKiemTraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayKiemTraForm));
            Janus.Windows.GridEX.GridEXLayout dgListChungTu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiGiayKT = new Janus.Windows.EditControls.UIComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTenNguoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaNguoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoGiayKT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayGiayKT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtKetQuaKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenCoQuanKT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaCoQuanKT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDiaDiemKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnChonHang = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvPTTT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDVMua = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgListChungTu = new Janus.Windows.GridEX.GridEX();
            this.btnAddChungTu = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkChungNoiDungKiemTra = new Janus.Windows.EditControls.UICheckBox();
            this.rfvNguoiDuocCap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDVMua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListChungTu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiDuocCap)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnAddChungTu);
            this.grbMain.Controls.Add(this.btnChonHang);
            this.grbMain.Location = new System.Drawing.Point(3, 3);
            this.grbMain.Size = new System.Drawing.Size(814, 385);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbLoaiGiayKT);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtTenNguoiCap);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaNguoiCap);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtSoGiayKT);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayGiayKT);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(10, 80);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(792, 93);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiGiayKT
            // 
            this.cbLoaiGiayKT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiGiayKT.DisplayMember = "Name";
            this.cbLoaiGiayKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Giấy đăng ký kiểm tra";
            uiComboBoxItem3.Value = "DK";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Giấy kết quả kiểm tra";
            uiComboBoxItem4.Value = "KQ";
            this.cbLoaiGiayKT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbLoaiGiayKT.Location = new System.Drawing.Point(137, 12);
            this.cbLoaiGiayKT.Name = "cbLoaiGiayKT";
            this.cbLoaiGiayKT.Size = new System.Drawing.Size(134, 21);
            this.cbLoaiGiayKT.TabIndex = 0;
            this.cbLoaiGiayKT.ValueMember = "ID";
            this.cbLoaiGiayKT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbLoaiGiayKT.VisualStyleManager = this.vsmMain;
            this.cbLoaiGiayKT.SelectedIndexChanged += new System.EventHandler(this.cbLoaiGiayKT_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Loại giấy";
            // 
            // txtTenNguoiCap
            // 
            this.txtTenNguoiCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiCap.Location = new System.Drawing.Point(442, 66);
            this.txtTenNguoiCap.MaxLength = 255;
            this.txtTenNguoiCap.Name = "txtTenNguoiCap";
            this.txtTenNguoiCap.Size = new System.Drawing.Size(125, 21);
            this.txtTenNguoiCap.TabIndex = 5;
            this.txtTenNguoiCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenNguoiCap.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(315, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Tên người được cấp";
            // 
            // txtMaNguoiCap
            // 
            this.txtMaNguoiCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiCap.Location = new System.Drawing.Point(137, 66);
            this.txtMaNguoiCap.MaxLength = 255;
            this.txtMaNguoiCap.Name = "txtMaNguoiCap";
            this.txtMaNguoiCap.Size = new System.Drawing.Size(134, 21);
            this.txtMaNguoiCap.TabIndex = 4;
            this.txtMaNguoiCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaNguoiCap.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Mã người được cấp";
            // 
            // txtSoGiayKT
            // 
            this.txtSoGiayKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayKT.Location = new System.Drawing.Point(137, 39);
            this.txtSoGiayKT.MaxLength = 255;
            this.txtSoGiayKT.Name = "txtSoGiayKT";
            this.txtSoGiayKT.Size = new System.Drawing.Size(134, 21);
            this.txtSoGiayKT.TabIndex = 1;
            this.txtSoGiayKT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayKT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoGiayKT.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(315, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày giấy kiểm tra";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 44);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(83, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số giấy kiểm tra";
            // 
            // ccNgayGiayKT
            // 
            // 
            // 
            // 
            this.ccNgayGiayKT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayKT.DropDownCalendar.Name = "";
            this.ccNgayGiayKT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayKT.IsNullDate = true;
            this.ccNgayGiayKT.Location = new System.Drawing.Point(442, 39);
            this.ccNgayGiayKT.Name = "ccNgayGiayKT";
            this.ccNgayGiayKT.Nullable = true;
            this.ccNgayGiayKT.NullButtonText = "Xóa";
            this.ccNgayGiayKT.ShowNullButton = true;
            this.ccNgayGiayKT.Size = new System.Drawing.Size(125, 21);
            this.ccNgayGiayKT.TabIndex = 2;
            this.ccNgayGiayKT.TodayButtonText = "Hôm nay";
            this.ccNgayGiayKT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayKT.VisualStyleManager = this.vsmMain;
            // 
            // txtKetQuaKiemTra
            // 
            this.txtKetQuaKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKetQuaKiemTra.Location = new System.Drawing.Point(136, 85);
            this.txtKetQuaKiemTra.MaxLength = 2000;
            this.txtKetQuaKiemTra.Multiline = true;
            this.txtKetQuaKiemTra.Name = "txtKetQuaKiemTra";
            this.txtKetQuaKiemTra.Size = new System.Drawing.Size(430, 71);
            this.txtKetQuaKiemTra.TabIndex = 4;
            this.txtKetQuaKiemTra.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKetQuaKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtKetQuaKiemTra.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Kết quả kiểm tra";
            // 
            // txtTenCoQuanKT
            // 
            this.txtTenCoQuanKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCoQuanKT.Location = new System.Drawing.Point(385, 58);
            this.txtTenCoQuanKT.MaxLength = 255;
            this.txtTenCoQuanKT.Name = "txtTenCoQuanKT";
            this.txtTenCoQuanKT.Size = new System.Drawing.Size(181, 21);
            this.txtTenCoQuanKT.TabIndex = 3;
            this.txtTenCoQuanKT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenCoQuanKT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenCoQuanKT.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(298, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Tên cơ quan KT";
            // 
            // txtMaCoQuanKT
            // 
            this.txtMaCoQuanKT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaCoQuanKT.Location = new System.Drawing.Point(136, 58);
            this.txtMaCoQuanKT.MaxLength = 255;
            this.txtMaCoQuanKT.Name = "txtMaCoQuanKT";
            this.txtMaCoQuanKT.Size = new System.Drawing.Size(134, 21);
            this.txtMaCoQuanKT.TabIndex = 2;
            this.txtMaCoQuanKT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaCoQuanKT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaCoQuanKT.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Mã cơ quan kiểm tra";
            // 
            // txtDiaDiemKiemTra
            // 
            this.txtDiaDiemKiemTra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemKiemTra.Location = new System.Drawing.Point(136, 31);
            this.txtDiaDiemKiemTra.MaxLength = 255;
            this.txtDiaDiemKiemTra.Name = "txtDiaDiemKiemTra";
            this.txtDiaDiemKiemTra.Size = new System.Drawing.Size(430, 21);
            this.txtDiaDiemKiemTra.TabIndex = 1;
            this.txtDiaDiemKiemTra.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaDiemKiemTra.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Địa điểm kiểm tra";
            this.toolTip1.SetToolTip(this.label2, "Địa điểm tiến hành đăng ký kiểm tra");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(567, 354);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(79, 23);
            this.btnXoa.TabIndex = 6;
            this.btnXoa.Text = "Xoá hàng";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(739, 354);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnChonHang
            // 
            this.btnChonHang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonHang.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonHang.Icon")));
            this.btnChonHang.Location = new System.Drawing.Point(475, 354);
            this.btnChonHang.Name = "btnChonHang";
            this.btnChonHang.Size = new System.Drawing.Size(86, 23);
            this.btnChonHang.TabIndex = 5;
            this.btnChonHang.Text = "Chọn hàng";
            this.btnChonHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonHang.VisualStyleManager = this.vsmMain;
            this.btnChonHang.Click += new System.EventHandler(this.btnChonHang_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(652, 354);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(81, 23);
            this.btnGhi.TabIndex = 7;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(812, 175);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgList_UpdatingCell);
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayHopDong
            // 
            this.rfvNgayHopDong.ControlToValidate = this.ccNgayGiayKT;
            this.rfvNgayHopDong.ErrorMessage = "\"Ngày kiểm tra\" không được bỏ trống.";
            this.rfvNgayHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHopDong.Icon")));
            this.rfvNgayHopDong.Tag = "rfvNgayHopDong";
            // 
            // rfvSoHopDong
            // 
            this.rfvSoHopDong.ControlToValidate = this.txtSoGiayKT;
            this.rfvSoHopDong.ErrorMessage = "\"Số giấy đăng ký\" không được để trống.";
            this.rfvSoHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHopDong.Icon")));
            this.rfvSoHopDong.Tag = "rfvSoHopDong";
            // 
            // rfvPTTT
            // 
            this.rfvPTTT.ControlToValidate = this.cbLoaiGiayKT;
            this.rfvPTTT.ErrorMessage = "\"Loại giấy kiểm tra\" không được để trống.";
            this.rfvPTTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvPTTT.Icon")));
            this.rfvPTTT.Tag = "rfvPTTT";
            // 
            // rfvMaDVMua
            // 
            this.rfvMaDVMua.ControlToValidate = this.txtMaNguoiCap;
            this.rfvMaDVMua.ErrorMessage = "\"Mã người được cấp\" không được để trống.";
            this.rfvMaDVMua.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDVMua.Icon")));
            this.rfvMaDVMua.Tag = "rfvPTTT";
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label12);
            this.grpTiepNhan.Controls.Add(this.label13);
            this.grpTiepNhan.Controls.Add(this.label15);
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(9, 7);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(795, 68);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(387, 39);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(355, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(142, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lbTrangThai.Location = new System.Drawing.Point(118, 50);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Trạng thái";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(269, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Ngày tiếp nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số tiếp nhận";
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(11, 354);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(78, 23);
            this.btnKhaiBao.TabIndex = 2;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(95, 354);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(98, 23);
            this.btnLayPhanHoi.TabIndex = 3;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.Tag = null;
            this.uiPanel0.Id = new System.Guid("64d17a01-0c08-42a4-a1c7-c25eae9e0af6");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel1.Id = new System.Guid("40643b23-37be-4974-bdd6-3bd01ac6fddc");
            this.uiPanelManager1.Panels.Add(this.uiPanel1);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("64d17a01-0c08-42a4-a1c7-c25eae9e0af6"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(814, 203), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("40643b23-37be-4974-bdd6-3bd01ac6fddc"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(782, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("64d17a01-0c08-42a4-a1c7-c25eae9e0af6"), new System.Drawing.Point(44, 44), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("40643b23-37be-4974-bdd6-3bd01ac6fddc"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(44, 44);
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(3, 388);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(814, 203);
            this.uiPanel0.TabIndex = 1;
            this.uiPanel0.Text = "Hàng đăng ký kiểm tra";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.dgList);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(812, 175);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // uiPanel1
            // 
            this.uiPanel1.AutoHide = true;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(3, 357);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(782, 200);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Danh sách chứng từ kèm theo";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgListChungTu);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(780, 172);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgListChungTu
            // 
            this.dgListChungTu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListChungTu.AlternatingColors = true;
            this.dgListChungTu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListChungTu.ColumnAutoResize = true;
            dgListChungTu_DesignTimeLayout.LayoutString = resources.GetString("dgListChungTu_DesignTimeLayout.LayoutString");
            this.dgListChungTu.DesignTimeLayout = dgListChungTu_DesignTimeLayout;
            this.dgListChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListChungTu.FrozenColumns = 3;
            this.dgListChungTu.GroupByBoxVisible = false;
            this.dgListChungTu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListChungTu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListChungTu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListChungTu.ImageList = this.ImageList1;
            this.dgListChungTu.Location = new System.Drawing.Point(0, 0);
            this.dgListChungTu.Name = "dgListChungTu";
            this.dgListChungTu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListChungTu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListChungTu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListChungTu.Size = new System.Drawing.Size(780, 172);
            this.dgListChungTu.TabIndex = 9;
            this.dgListChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListChungTu.VisualStyleManager = this.vsmMain;
            this.dgListChungTu.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgListChungTu_DeletingRecord);
            this.dgListChungTu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListChungTu_RowDoubleClick);
            this.dgListChungTu.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgListChungTu_DeletingRecords);
            this.dgListChungTu.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListChungTu_FormattingRow);
            // 
            // btnAddChungTu
            // 
            this.btnAddChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddChungTu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddChungTu.Icon")));
            this.btnAddChungTu.Location = new System.Drawing.Point(377, 354);
            this.btnAddChungTu.Name = "btnAddChungTu";
            this.btnAddChungTu.Size = new System.Drawing.Size(92, 23);
            this.btnAddChungTu.TabIndex = 4;
            this.btnAddChungTu.Text = "Chứng Từ";
            this.btnAddChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddChungTu.VisualStyleManager = this.vsmMain;
            this.btnAddChungTu.Click += new System.EventHandler(this.btnAddChungTu_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkChungNoiDungKiemTra);
            this.uiGroupBox1.Controls.Add(this.txtDiaDiemKiemTra);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtMaCoQuanKT);
            this.uiGroupBox1.Controls.Add(this.txtTenCoQuanKT);
            this.uiGroupBox1.Controls.Add(this.txtKetQuaKiemTra);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Location = new System.Drawing.Point(11, 179);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(778, 169);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkChungNoiDungKiemTra
            // 
            this.chkChungNoiDungKiemTra.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkChungNoiDungKiemTra.Location = new System.Drawing.Point(0, -5);
            this.chkChungNoiDungKiemTra.Name = "chkChungNoiDungKiemTra";
            this.chkChungNoiDungKiemTra.Size = new System.Drawing.Size(194, 23);
            this.chkChungNoiDungKiemTra.TabIndex = 0;
            this.chkChungNoiDungKiemTra.Text = "Hàng dùng chung nội dung kiểm tra";
            this.chkChungNoiDungKiemTra.VisualStyleManager = this.vsmMain;
            this.chkChungNoiDungKiemTra.CheckedChanged += new System.EventHandler(this.chkChungNoiDungKiemTra_CheckedChanged);
            // 
            // rfvNguoiDuocCap
            // 
            this.rfvNguoiDuocCap.ControlToValidate = this.txtTenNguoiCap;
            this.rfvNguoiDuocCap.ErrorMessage = "\"Tên người được cấp\" không được để trống.";
            this.rfvNguoiDuocCap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiDuocCap.Icon")));
            this.rfvNguoiDuocCap.Tag = "rfvSoHopDong";
            // 
            // GiayKiemTraForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(820, 612);
            this.Controls.Add(this.uiPanel0);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "GiayKiemTraForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Giấy đăng ký kiểm tra";
            this.Load += new System.EventHandler(this.HopDongForm_Load);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDVMua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListChungTu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiDuocCap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayKT;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayGiayKT;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemKiemTra;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiCap;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiCap;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCoQuanKT;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCoQuanKT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtKetQuaKiemTra;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnChonHang;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIComboBox cbLoaiGiayKT;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayHopDong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHopDong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvPTTT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDVMua;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private System.Windows.Forms.Label lbTrangThai;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.UI.Dock.UIPanel uiPanel1;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel1Container;
        private Janus.Windows.GridEX.GridEX dgListChungTu;
        private Janus.Windows.EditControls.UIButton btnAddChungTu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UICheckBox chkChungNoiDungKiemTra;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiDuocCap;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
