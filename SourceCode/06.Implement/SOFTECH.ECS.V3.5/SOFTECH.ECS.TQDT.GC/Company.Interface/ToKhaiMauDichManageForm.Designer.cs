﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    partial class ToKhaiMauDichManageForm
    {
        private UIGroupBox uiGroupBox1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichManageForm));
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.print = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiA4MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiTQDT196_TaiCho = new System.Windows.Forms.ToolStripMenuItem();
            this.InToKhaiSuaDoiBoSung = new System.Windows.Forms.ToolStripMenuItem();
            this.InContainer = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInBangKe_HD = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInAnDinhThue = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInPhieuKiemTra_CTGiay = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInPhieuKiemTra_HH = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCSDaDuyet = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportMSG = new System.Windows.Forms.ToolStripMenuItem();
            this.mniSuaToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniHuyToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdCapNhatNPLTonTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUpdateTKGiay = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timToKhai1 = new Company.Interface.Controls.TimToKhai();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdDelete1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdNPLCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.SaoChep3 = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.cmdViewPhanBoToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdViewPhanBoToKhai");
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdPrint2 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.SaoChepToKhaiHang1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.SaoChepALL1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepALL = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepToKhaiHang = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.cmdInA41 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.cmdNPLCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdViewPhanBoToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdViewPhanBoToKhai");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1049, 367);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1049, 367);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 123);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1049, 244);
            this.dgList.TabIndex = 306;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaoChepCha,
            this.print,
            this.mnuCSDaDuyet,
            this.mniImportTK,
            this.mniImportMSG,
            this.mniSuaToKhai,
            this.mniHuyToKhai,
            this.cmdCapNhatNPLTonTK,
            this.mnuUpdateTKGiay});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(343, 202);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH});
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(342, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // saoChepTK
            // 
            this.saoChepTK.Image = ((System.Drawing.Image)(resources.GetObject("saoChepTK.Image")));
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(154, 22);
            this.saoChepTK.Text = "Tờ khai";
            this.saoChepTK.Click += new System.EventHandler(this.saoChepTK_Click);
            // 
            // saoChepHH
            // 
            this.saoChepHH.Image = ((System.Drawing.Image)(resources.GetObject("saoChepHH.Image")));
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(154, 22);
            this.saoChepHH.Text = "Tờ khai + hàng";
            this.saoChepHH.Click += new System.EventHandler(this.saoChepHH_Click);
            // 
            // print
            // 
            this.print.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToKhaiA4MenuItem,
            this.mniInToKhaiTQDT196_TaiCho,
            this.InToKhaiSuaDoiBoSung,
            this.InContainer,
            this.mniInBangKe_HD,
            this.mniInAnDinhThue,
            this.mniInPhieuKiemTra_CTGiay,
            this.mniInPhieuKiemTra_HH});
            this.print.Image = ((System.Drawing.Image)(resources.GetObject("print.Image")));
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(342, 22);
            this.print.Text = "In ";
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // ToKhaiA4MenuItem
            // 
            this.ToKhaiA4MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ToKhaiA4MenuItem.Image")));
            this.ToKhaiA4MenuItem.Name = "ToKhaiA4MenuItem";
            this.ToKhaiA4MenuItem.Size = new System.Drawing.Size(288, 22);
            this.ToKhaiA4MenuItem.Text = "In tờ khai thông quan (Thông tư 196)";
            this.ToKhaiA4MenuItem.Click += new System.EventHandler(this.ToKhaiA4MenuItem_Click);
            // 
            // mniInToKhaiTQDT196_TaiCho
            // 
            this.mniInToKhaiTQDT196_TaiCho.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiTQDT196_TaiCho.Image")));
            this.mniInToKhaiTQDT196_TaiCho.Name = "mniInToKhaiTQDT196_TaiCho";
            this.mniInToKhaiTQDT196_TaiCho.Size = new System.Drawing.Size(288, 22);
            this.mniInToKhaiTQDT196_TaiCho.Text = "In tờ khai tại chỗ (Thông tư 196)";
            this.mniInToKhaiTQDT196_TaiCho.Click += new System.EventHandler(this.mniInToKhaiTQDT196_TaiCho_Click);
            // 
            // InToKhaiSuaDoiBoSung
            // 
            this.InToKhaiSuaDoiBoSung.Image = ((System.Drawing.Image)(resources.GetObject("InToKhaiSuaDoiBoSung.Image")));
            this.InToKhaiSuaDoiBoSung.Name = "InToKhaiSuaDoiBoSung";
            this.InToKhaiSuaDoiBoSung.Size = new System.Drawing.Size(288, 22);
            this.InToKhaiSuaDoiBoSung.Text = "In tờ khai sửa đổi bổ sung";
            this.InToKhaiSuaDoiBoSung.Click += new System.EventHandler(this.InToKhaiSuaDoiBoSung_Click);
            // 
            // InContainer
            // 
            this.InContainer.Image = ((System.Drawing.Image)(resources.GetObject("InContainer.Image")));
            this.InContainer.Name = "InContainer";
            this.InContainer.Size = new System.Drawing.Size(288, 22);
            this.InContainer.Text = "In bảng kê Container";
            this.InContainer.Click += new System.EventHandler(this.InContainer_Click);
            // 
            // mniInBangKe_HD
            // 
            this.mniInBangKe_HD.Image = ((System.Drawing.Image)(resources.GetObject("mniInBangKe_HD.Image")));
            this.mniInBangKe_HD.Name = "mniInBangKe_HD";
            this.mniInBangKe_HD.Size = new System.Drawing.Size(288, 22);
            this.mniInBangKe_HD.Text = "In bảng kê hợp đồng/ đơn hàng";
            this.mniInBangKe_HD.Click += new System.EventHandler(this.mniInBangKe_HD_Click);
            // 
            // mniInAnDinhThue
            // 
            this.mniInAnDinhThue.Image = ((System.Drawing.Image)(resources.GetObject("mniInAnDinhThue.Image")));
            this.mniInAnDinhThue.Name = "mniInAnDinhThue";
            this.mniInAnDinhThue.Size = new System.Drawing.Size(288, 22);
            this.mniInAnDinhThue.Text = "In ấn định thuế";
            this.mniInAnDinhThue.Click += new System.EventHandler(this.mniInAnDinhThue_Click);
            // 
            // mniInPhieuKiemTra_CTGiay
            // 
            this.mniInPhieuKiemTra_CTGiay.Image = ((System.Drawing.Image)(resources.GetObject("mniInPhieuKiemTra_CTGiay.Image")));
            this.mniInPhieuKiemTra_CTGiay.Name = "mniInPhieuKiemTra_CTGiay";
            this.mniInPhieuKiemTra_CTGiay.Size = new System.Drawing.Size(288, 22);
            this.mniInPhieuKiemTra_CTGiay.Text = "Phiếu ghi kết quả kiểm tra chứng từ giấy";
            this.mniInPhieuKiemTra_CTGiay.Click += new System.EventHandler(this.mniInPhieuKiemTra_CTGiay_Click);
            // 
            // mniInPhieuKiemTra_HH
            // 
            this.mniInPhieuKiemTra_HH.Image = ((System.Drawing.Image)(resources.GetObject("mniInPhieuKiemTra_HH.Image")));
            this.mniInPhieuKiemTra_HH.Name = "mniInPhieuKiemTra_HH";
            this.mniInPhieuKiemTra_HH.Size = new System.Drawing.Size(288, 22);
            this.mniInPhieuKiemTra_HH.Text = "Phiếu ghi kết quả kiểm tra hàng hóa";
            this.mniInPhieuKiemTra_HH.Click += new System.EventHandler(this.mniInPhieuKiemTra_HH_Click);
            // 
            // mnuCSDaDuyet
            // 
            this.mnuCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("mnuCSDaDuyet.Image")));
            this.mnuCSDaDuyet.Name = "mnuCSDaDuyet";
            this.mnuCSDaDuyet.Size = new System.Drawing.Size(342, 22);
            this.mnuCSDaDuyet.Text = "Chuyển Trạng Thái";
            this.mnuCSDaDuyet.Visible = false;
            this.mnuCSDaDuyet.Click += new System.EventHandler(this.mnuCSDaDuyet_Click);
            // 
            // mniImportTK
            // 
            this.mniImportTK.Image = ((System.Drawing.Image)(resources.GetObject("mniImportTK.Image")));
            this.mniImportTK.Name = "mniImportTK";
            this.mniImportTK.Size = new System.Drawing.Size(342, 22);
            this.mniImportTK.Text = "Import tờ khai";
            // 
            // mniImportMSG
            // 
            this.mniImportMSG.Image = ((System.Drawing.Image)(resources.GetObject("mniImportMSG.Image")));
            this.mniImportMSG.Name = "mniImportMSG";
            this.mniImportMSG.Size = new System.Drawing.Size(342, 22);
            this.mniImportMSG.Text = "Import MSG";
            // 
            // mniSuaToKhai
            // 
            this.mniSuaToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mniSuaToKhai.Image")));
            this.mniSuaToKhai.Name = "mniSuaToKhai";
            this.mniSuaToKhai.Size = new System.Drawing.Size(342, 22);
            this.mniSuaToKhai.Text = "Sửa tờ khai";
            this.mniSuaToKhai.Click += new System.EventHandler(this.mniSuaToKhai_Click);
            // 
            // mniHuyToKhai
            // 
            this.mniHuyToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mniHuyToKhai.Image")));
            this.mniHuyToKhai.Name = "mniHuyToKhai";
            this.mniHuyToKhai.Size = new System.Drawing.Size(342, 22);
            this.mniHuyToKhai.Text = "Hủy tờ khai";
            this.mniHuyToKhai.Click += new System.EventHandler(this.mniHuyToKhai_Click);
            // 
            // cmdCapNhatNPLTonTK
            // 
            this.cmdCapNhatNPLTonTK.Image = ((System.Drawing.Image)(resources.GetObject("cmdCapNhatNPLTonTK.Image")));
            this.cmdCapNhatNPLTonTK.Name = "cmdCapNhatNPLTonTK";
            this.cmdCapNhatNPLTonTK.Size = new System.Drawing.Size(342, 22);
            this.cmdCapNhatNPLTonTK.Text = "Cập nhật nguyên phụ liệu tồn của tờ khai đã duyệt";
            this.cmdCapNhatNPLTonTK.Click += new System.EventHandler(this.cmdCapNhatNPLTonTK_Click);
            // 
            // mnuUpdateTKGiay
            // 
            this.mnuUpdateTKGiay.Image = ((System.Drawing.Image)(resources.GetObject("mnuUpdateTKGiay.Image")));
            this.mnuUpdateTKGiay.Name = "mnuUpdateTKGiay";
            this.mnuUpdateTKGiay.Size = new System.Drawing.Size(342, 22);
            this.mnuUpdateTKGiay.Text = "Cập nhật TK Giấy";
            this.mnuUpdateTKGiay.Click += new System.EventHandler(this.mnuUpdateTKGiay_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "page_excel.png");
            this.ImageList1.Images.SetKeyName(5, "report.png");
            this.ImageList1.Images.SetKeyName(6, "shell32_240.ico");
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox2.Controls.Add(this.cbHopDong);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.timToKhai1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1049, 123);
            this.uiGroupBox2.TabIndex = 305;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(99, 16);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(447, 22);
            this.donViHaiQuanControl1.TabIndex = 302;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHopDong.Location = new System.Drawing.Point(99, 50);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(447, 21);
            this.cbHopDong.TabIndex = 304;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbHopDong.ValueChanged += new System.EventHandler(this.cbHopDong_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 295;
            this.label2.Text = "Mã hải quan";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 295;
            this.label1.Text = "Số hợp đồng";
            // 
            // timToKhai1
            // 
            this.timToKhai1.BackColor = System.Drawing.Color.Transparent;
            this.timToKhai1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timToKhai1.Location = new System.Drawing.Point(18, 85);
            this.timToKhai1.Name = "timToKhai1";
            this.timToKhai1.Size = new System.Drawing.Size(794, 23);
            this.timToKhai1.TabIndex = 298;
            this.timToKhai1.Search += new System.EventHandler<Company.Interface.Controls.StringEventArgs>(this.timToKhai1_Search);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.SaoChep,
            this.SaoChepALL,
            this.SaoChepToKhaiHang,
            this.cmdPrint,
            this.XacNhan,
            this.Export,
            this.Import,
            this.cmdCSDaDuyet,
            this.cmdXuatToKhai,
            this.ToKhaiViet,
            this.InPhieuTN2,
            this.cmdInA4,
            this.cmdDelete,
            this.cmdHistory,
            this.cmdNPLCungUng,
            this.cmdViewPhanBoToKhai,
            this.cmdExportExcel});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDelete1,
            this.cmdNPLCungUng1,
            this.cmdHistory1,
            this.SaoChep3,
            this.cmdViewPhanBoToKhai1,
            this.cmdCSDaDuyet1,
            this.cmdPrint2,
            this.cmdExportExcel1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.ImageList1;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1049, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandBar1_CommandClick);
            // 
            // cmdDelete1
            // 
            this.cmdDelete1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDelete1.Image")));
            this.cmdDelete1.Key = "cmdDelete";
            this.cmdDelete1.Name = "cmdDelete1";
            // 
            // cmdNPLCungUng1
            // 
            this.cmdNPLCungUng1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNPLCungUng1.Image")));
            this.cmdNPLCungUng1.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng1.Name = "cmdNPLCungUng1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistory1.Image")));
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // SaoChep3
            // 
            this.SaoChep3.Key = "SaoChep";
            this.SaoChep3.Name = "SaoChep3";
            this.SaoChep3.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.SaoChep3.Text = "&Sao chép";
            // 
            // cmdViewPhanBoToKhai1
            // 
            this.cmdViewPhanBoToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("cmdViewPhanBoToKhai1.Image")));
            this.cmdViewPhanBoToKhai1.Key = "cmdViewPhanBoToKhai";
            this.cmdViewPhanBoToKhai1.Name = "cmdViewPhanBoToKhai1";
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet1.Image")));
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "&Chuyển Trạng Thái";
            // 
            // cmdPrint2
            // 
            this.cmdPrint2.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint2.Image")));
            this.cmdPrint2.Key = "cmdPrint";
            this.cmdPrint2.Name = "cmdPrint2";
            this.cmdPrint2.Text = "&In";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSingleDownload.Icon")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu mới của tờ khai đang chọn (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + S)";
            // 
            // SaoChep
            // 
            this.SaoChep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.SaoChepToKhaiHang1,
            this.SaoChepALL1});
            this.SaoChep.Image = ((System.Drawing.Image)(resources.GetObject("SaoChep.Image")));
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép";
            // 
            // SaoChepToKhaiHang1
            // 
            this.SaoChepToKhaiHang1.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepToKhaiHang1.Image")));
            this.SaoChepToKhaiHang1.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang1.Name = "SaoChepToKhaiHang1";
            this.SaoChepToKhaiHang1.Text = "Tờ khai";
            // 
            // SaoChepALL1
            // 
            this.SaoChepALL1.Key = "SaoChepALL";
            this.SaoChepALL1.Name = "SaoChepALL1";
            this.SaoChepALL1.Text = "Tờ khai + hàng";
            // 
            // SaoChepALL
            // 
            this.SaoChepALL.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepALL.Image")));
            this.SaoChepALL.Key = "SaoChepALL";
            this.SaoChepALL.Name = "SaoChepALL";
            this.SaoChepALL.Text = "Tờ khai + hàng";
            // 
            // SaoChepToKhaiHang
            // 
            this.SaoChepToKhaiHang.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Name = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Text = "Tờ khai + hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.InPhieuTN1,
            this.ToKhai1,
            this.cmdInA41});
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN1.Image")));
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // ToKhai1
            // 
            this.ToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("ToKhai1.Image")));
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            this.ToKhai1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInA41
            // 
            this.cmdInA41.Image = ((System.Drawing.Image)(resources.GetObject("cmdInA41.Image")));
            this.cmdInA41.Key = "cmdInA4";
            this.cmdInA41.Name = "cmdInA41";
            this.cmdInA41.Text = "In tờ khai thông quan điện tử";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // Import
            // 
            this.Import.Key = "Import";
            this.Import.Name = "Import";
            this.Import.Text = "Import dữ liệu";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // cmdInA4
            // 
            this.cmdInA4.Key = "cmdInA4";
            this.cmdInA4.Name = "cmdInA4";
            this.cmdInA4.Text = "In Trang A4";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Kết quả xử lý";
            // 
            // cmdNPLCungUng
            // 
            this.cmdNPLCungUng.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng.Name = "cmdNPLCungUng";
            this.cmdNPLCungUng.Text = "Xem NPL Cung ứng";
            // 
            // cmdViewPhanBoToKhai
            // 
            this.cmdViewPhanBoToKhai.Key = "cmdViewPhanBoToKhai";
            this.cmdViewPhanBoToKhai.Name = "cmdViewPhanBoToKhai";
            this.cmdViewPhanBoToKhai.Shortcut = System.Windows.Forms.Shortcut.ShiftF1;
            this.cmdViewPhanBoToKhai.Text = "Kiểm tra tờ khai chưa phân bổ";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1049, 32);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "ECS files|*.ECS";
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ToKhaiMauDichManageForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1049, 399);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi tờ khai mậu dịch";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep3;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private ToolStripMenuItem print;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand Import;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private ToolStripMenuItem mnuCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint2;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA4;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA41;
        private ToolStripMenuItem ToKhaiA4MenuItem;
        private ColorDialog colorDialog1;
        private ToolStripMenuItem mniImportTK;
        private ToolStripMenuItem mniImportMSG;
        private ToolStripMenuItem mniSuaToKhai;
        private ToolStripMenuItem mniHuyToKhai;
        private Company.KDT.SHARE.Components.WS.KDTService kdtService1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhaiViet;
        private ImageList ImageList1;
        private Label label2;
        private Company.Interface.Controls.TimToKhai timToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private ToolStripMenuItem InToKhaiSuaDoiBoSung;
        private ToolStripMenuItem InContainer;
        private ToolStripMenuItem cmdCapNhatNPLTonTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdViewPhanBoToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdViewPhanBoToKhai;
        private ToolStripMenuItem mniInToKhaiTQDT196_TaiCho;
        private ToolStripMenuItem mniInBangKe_HD;
        private ToolStripMenuItem mniInPhieuKiemTra_CTGiay;
        private ToolStripMenuItem mniInPhieuKiemTra_HH;
        private ToolStripMenuItem mniInAnDinhThue;
        private ToolStripMenuItem mnuUpdateTKGiay;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbHopDong;
        private Label label1;
        private UIGroupBox uiGroupBox2;
        private GridEX dgList;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel;
    }
}
