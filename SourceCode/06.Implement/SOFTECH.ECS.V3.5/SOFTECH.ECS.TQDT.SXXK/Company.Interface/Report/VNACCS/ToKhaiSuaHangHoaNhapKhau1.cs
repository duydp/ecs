﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.LogMessages;
using System.Collections.Generic;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiSuaHangHoaNhapKhau1 : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        //public bool IsKhaiBaoSua;
        public ToKhaiSuaHangHoaNhapKhau1()
        {
            InitializeComponent();
        }
        List<MsgPhanBo> _ListMsg = new List<MsgPhanBo>();
        public List<MsgPhanBo> MsgCollection
        {
            set { this._ListMsg = value; }
            get { return this._ListMsg; }
        }
        public void BindingReport(VAD2AE0 vad2ae)
        {
            //if(IsKhaiBaoSua== true)
            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
            lblSoTrang.Text = "1/" + vad2ae.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad2ae.ICN.GetValue().ToString().ToUpper();
            lblSoToKhaiDauTien.Text = vad2ae.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad2ae.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad2ae.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad2ae.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad2ae.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad2ae.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad2ae.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad2ae.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad2ae.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad2ae.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad2ae.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad2ae.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ae.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad2ae.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";

            }
            if (vad2ae.AD1.GetValue().ToString().ToUpper() != "" && vad2ae.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad2ae.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ae.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ae.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ae.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad2ae.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad2ae.AD3.GetValue().ToString().ToUpper() != "" && vad2ae.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad2ae.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad2ae.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad2ae.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad2ae.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad2ae.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad2ae.AAA.GetValue().ToString().ToUpper();
            lblMaNguoiNhapKhau.Text = vad2ae.IMC.GetValue().ToString().ToUpper();
            lblTenNguoiNhapKhau.Text = vad2ae.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNhapKhau.Text = vad2ae.IMY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiNhapKhau.Text = vad2ae.IMA.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiNhapKhau.Text = vad2ae.IMT.GetValue().ToString().ToUpper();
            lblMaNguoiUyThacNhapKhau.Text = vad2ae.NMC.GetValue().ToString().ToUpper();
            lblTenNguoiUyThacNhapKhau.Text = vad2ae.NMN.GetValue().ToString().ToUpper();
            lblMaNguoiXuatKhau.Text = vad2ae.EPC.GetValue().ToString().ToUpper();
            lblTenNguoiXuatKhau.Text = vad2ae.EPN.GetValue().ToString().ToUpper();
            lblMaBuuChinhXuatKhau.Text = vad2ae.EPY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau1.Text = vad2ae.EPA.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau2.Text = vad2ae.EP2.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau3.Text = vad2ae.EP3.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau4.Text = vad2ae.EP4.GetValue().ToString().ToUpper();
            lblMaNuoc.Text = vad2ae.EPO.GetValue().ToString().ToUpper();
            lblNguoiUyThacXuatKhau.Text = vad2ae.ENM.GetValue().ToString().ToUpper();
            lblMaDaiLyHaiQuan.Text = vad2ae.A37.GetValue().ToString().ToUpper();
            lblTenDaiLyHaiQuan.Text = vad2ae.A38.GetValue().ToString().ToUpper();
            lblMaNhanVienHaiQuan.Text = vad2ae.A39.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ae.BL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoVanDon1.Text = vad2ae.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoVanDon2.Text = vad2ae.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoVanDon3.Text = vad2ae.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoVanDon4.Text = vad2ae.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoVanDon5.Text = vad2ae.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblSoLuong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad2ae.NO.GetValue().ToString().ToUpper().ToString().Replace(".", ",")));
            lblMaDonViTinh.Text = vad2ae.NOT.GetValue().ToString().ToUpper();
            //string a = vad2ae.GW.GetValue().ToString().Replace(".", ",");
            lblTongTrongLuongHang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad2ae.GW.GetValue().ToString().Replace(".", ",")));
            lblMaDonViTinhTrongLuong.Text = vad2ae.GWT.GetValue().ToString().ToUpper();
            lblSoLuongContainer.Text = vad2ae.COC.GetValue().ToString().ToUpper();
            lblMaDiaDiemLuuKhoHang.Text = vad2ae.ST.GetValue().ToString().ToUpper();
            lblTenDiaDiemLuuKhoHang.Text = vad2ae.A51.GetValue().ToString().ToUpper();
            lblMaDiaDiemDoHang.Text = vad2ae.DST.GetValue().ToString().ToUpper();
            lblTenDiaDiemDoHang.Text = vad2ae.DSN.GetValue().ToString().ToUpper();
            lblMaDiaDiemXepHang.Text = vad2ae.PSC.GetValue().ToString().ToUpper();
            lblTenDiaDiemXepHang.Text = vad2ae.PSN.GetValue().ToString().ToUpper();
            lblMaPhuongTienVanChuyen.Text = vad2ae.VSC.GetValue().ToString().ToUpper();
            lblTenPhuongTienVanChuyen.Text = vad2ae.VSN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ae.ARR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHangDen.Text = Convert.ToDateTime(vad2ae.ARR.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHangDen.Text = "";
            }
            lblKyHieuSoHieu.Text = vad2ae.MRK.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ae.ISD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuocPhepNhapKho.Text = Convert.ToDateTime(vad2ae.ISD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuocPhepNhapKho.Text = "";
            }
            for (int i = 0; i < vad2ae.OL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaVanBanPhapQuyKhac1.Text = vad2ae.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaVanBanPhapQuyKhac2.Text = vad2ae.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaVanBanPhapQuyKhac3.Text = vad2ae.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaVanBanPhapQuyKhac4.Text = vad2ae.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaVanBanPhapQuyKhac5.Text = vad2ae.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanLoaiHinhThucHoaDon.Text = vad2ae.IV1.GetValue().ToString().ToUpper();
            lblSoHoaDon.Text = vad2ae.IV3.GetValue().ToString().ToUpper();
            lblSoTiepNhanHoaDonDienTu.Text = vad2ae.IV2.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad2ae.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayPhatHanh.Text = Convert.ToDateTime(vad2ae.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayPhatHanh.Text = "";
            }
            lblPhuongThucThanhToan.Text = vad2ae.IVP.GetValue().ToString().ToUpper();
            lblMaPhanLoaiGiaHoaDon.Text = vad2ae.IP1.GetValue().ToString().ToUpper();
            lblMaDieuKienGiaHoaDon.Text = vad2ae.IP2.GetValue().ToString().ToUpper();
            lblMaDongTienHoaDon.Text = vad2ae.IP3.GetValue().ToString().ToUpper();
            lblTongTriGiaHoaDon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.IP4.GetValue().ToString().Replace(".", ",")));
            lblTongTriGiaTinhThue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.A86.GetValue().ToString().Replace(".", ",")));
            // Fix

           // MsgPhanBo msgPhanBo = MsgPhanBo.getMsgPhanBo(lblSoToKhai.Text.ToString());
            //if (msgPhanBo.MaNghiepVu == "VAD2AE0" && msgPhanBo.SoTiepNhan == lblSoToKhai.Text)
            //{
            //    lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa nhập khẩu (người khai Hải quan thực hiện)";
            //}
            //int lastIndexTK = Convert.ToInt32(lblSoToKhai.Text.ToString().Substring(11, 1));
            //if (lastIndexTK < 1)
            //{
            //    Int64 soToKhaiVNACC = Convert.ToInt64(lblSoToKhai.Text.ToString()) + 1;
            //    string sotoKhaiVNACCFull = Convert.ToString(soToKhaiVNACC);
            //    TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(sotoKhaiVNACCFull);
            //}
            //else {

            //    TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSoToKhai.Text.ToString());
            //}
            //if (lastIndexTK < 1)
            //{
            //    lblTenThongTinXuat.Text = "Tờ khai hàng hóa nhập khẩu (thông báo kết quả phân luồng)";
            //}
            //if (TKMD.TrangThaiXuLy == "1")
            //{
            //    lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa nhập khẩu";
            //}
            //if (TKMD.TrangThaiXuLy == "2" && lastIndexTK >= 1)
            //{
            //    lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa nhập khẩu (người khai Hải quan thực hiện)";
            //}
            //if (TKMD.TrangThaiXuLy == "3" && lastIndexTK >= 1)
            //{
            //    lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa nhập khẩu (người khai Hải quan thực hiện)";
            //}
            //minhnd Fix Trị giá Tờ khai
            //KDT_VNACC_ToKhaiMauDich  TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSoToKhai.Text.ToString());
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTongHeSoPhanBoTriGia.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai
            //lblTongHeSoPhanBoTriGia.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.TP.GetValue().ToString().Replace(".", ",")));

            lblMaPhanLoaiNhapLieu.Text = vad2ae.A97.GetValue().ToString().ToUpper();
            lblMaKetQuaKiemTraNoiDung.Text = vad2ae.N4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ae.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiGiayPhepNhapKhau1.Text = vad2ae.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep1.Text = vad2ae.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiGiayPhepNhapKhau2.Text = vad2ae.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep2.Text = vad2ae.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiGiayPhepNhapKhau3.Text = vad2ae.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep3.Text = vad2ae.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblPhanLoaiGiayPhepNhapKhau4.Text = vad2ae.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep4.Text = vad2ae.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblPhanLoaiGiayPhepNhapKhau5.Text = vad2ae.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep5.Text = vad2ae.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblMaPhanLoaiKhaiTriGia.Text = vad2ae.VD1.GetValue().ToString().ToUpper();
            lblSoTiepNhanKhaiTriGiaTongHop.Text = vad2ae.VD2.GetValue().ToString().ToUpper() == "0" ? "" : vad2ae.VD2.GetValue().ToString().ToUpper();
            lblPhanLoaiCongThucChuan.Text = vad2ae.A93.GetValue().ToString().ToUpper();
            lblMaPhanLoaiDieuChinhTriGia.Text = vad2ae.A94.GetValue().ToString().ToUpper();
            lblPhuongPhapDieuChinhTriGia.Text = vad2ae.A95.GetValue().ToString().ToUpper();
            lblMaTienTe.Text = vad2ae.VCC.GetValue().ToString().ToUpper();
            lblGiaCoSo.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VPC.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiPhiVanChuyen.Text = vad2ae.FR1.GetValue().ToString().ToUpper();
            lblMaTienTePhiVanChuyen.Text = vad2ae.FR2.GetValue().ToString().ToUpper();
            lblPhiVanChuyen.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.FR3.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiBaoHiem.Text = vad2ae.IN1.GetValue().ToString().ToUpper();
            lblMaTienTeCuaTienBaoHiem.Text = vad2ae.IN2.GetValue().ToString().ToUpper();
            lblPhiBaoHiem.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.IN3.GetValue().ToString().Replace(".", ",")));
            lblSoDangKyBaoHiemTongHop.Text = vad2ae.IN4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ae.VR_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaTenKhoanDieuChinh1.Text = vad2ae.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh1.Text = vad2ae.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia1.Text = vad2ae.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaTenKhoanDieuChinh2.Text = vad2ae.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh2.Text = vad2ae.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia2.Text = vad2ae.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaTenKhoanDieuChinh3.Text = vad2ae.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh3.Text = vad2ae.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia3.Text = vad2ae.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 3:
                        lblMaTenKhoanDieuChinh4.Text = vad2ae.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh4.Text = vad2ae.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia4.Text = vad2ae.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 4:
                        lblMaTenKhoanDieuChinh5.Text = vad2ae.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh5.Text = vad2ae.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia5.Text = vad2ae.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblChiTietKhaiTriGia.Text = vad2ae.VLD.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad2ae.KF1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThue1.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue1.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong1.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaSacThue2.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue2.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong2.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaSacThue3.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue3.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong3.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaSacThue4.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue4.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong4.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaSacThue5.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue5.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong5.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblMaSacThue6.Text = vad2ae.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue6.Text = vad2ae.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue6.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong6.Text = vad2ae.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblTongTienThuePhaiNop.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.B02.GetValue().ToString().Replace(".", ",")));
            lblSoTienBaoLanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.B03.GetValue().ToString().Replace(".", ",")));
            for (int i = 0; i < vad2ae.KJ1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaDongTienTyGiaTinhThue1.Text = vad2ae.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaDongTienTyGiaTinhThue2.Text = vad2ae.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaDongTienTyGiaTinhThue3.Text = vad2ae.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad2ae.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblMaXacDinhThoiHanNopThue.Text = vad2ae.ENC.GetValue().ToString().ToUpper();
            lblNguoiNopThue.Text = vad2ae.TPM.GetValue().ToString().ToUpper();
            lblMaLyDoDeNghi.Text = vad2ae.BP.GetValue().ToString().ToUpper();
            lblPhanLoaiNopThue.Text = vad2ae.B08.GetValue().ToString().ToUpper();
            lblTongSoTrangToKhai.Text = vad2ae.B12.GetValue().ToString().ToUpper();
            lblTongSoDongHang.Text = vad2ae.B13.GetValue().ToString().ToUpper();

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }
    }
}
