using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using System.Data;

namespace Company.Interface.Report.WareHouse
{
    public partial class ReportViewBCXNK_KhoSP : DevExpress.XtraReports.UI.XtraReport
    {
        private DataTable dt = new DataTable();
        private string MaNPLGroup = "";
        private string MaNPLTempGroup = "";
        private decimal SumTongLuongNhap = 0;
        private decimal SumTongLuongTonDau = 0;
        private decimal SumTongLuongTonCuoi = 0;
        private decimal SumTongLuongXuat = 0;

        private decimal SumTongTriGiaNhap = 0;
        private decimal SumTongTriGiaTonDau = 0;
        private decimal SumTongTriGiaTonCuoi = 0;
        private decimal SumTongTriGiaXuat = 0;
        public ReportViewBCXNK_KhoSP()
        {
            InitializeComponent();
        }
        public void BindReport(String SoHopDong, String MaKho, String TenKho, DateTime DateFrom, DateTime DateTo)
        {
            try
            {
                dt = KDT_KhoCFS_DangKy.SelectDynamic("", "").Tables[0];
                int i = 1;
                string MaHangHoa = "";
                string TenHangHoa = "";
                DateTime ngayDangKyNhap = DateTime.Today;
                long soToKhaiNhap = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (MaHangHoa == dr["MaHangHoa"].ToString() && TenHangHoa == dr["TenHangHoa"].ToString())
                    {
                        dr["STT"] = i;
                    }
                    else
                    {

                        MaHangHoa = dr["MaHangHoa"].ToString();
                        TenHangHoa = dr["TenHangHoa"].ToString();
                        i++;
                        dr["STT"] = i;
                    }
                }
                dt.TableName = "BCXuatNhapTon_KhoSP";
                this.DataSource = dt;
                lblTongNhap.Summary.Running = SummaryRunning.Group;
                lblTongNhap.Summary.Func = SummaryFunc.Sum;
                lblTongNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTriGiaNhap.Summary.Running = SummaryRunning.Group;
                lblTongTriGiaNhap.Summary.Func = SummaryFunc.Sum;
                lblTongTriGiaNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTonDau.Summary.Running = SummaryRunning.Group;
                lblTongTonDau.Summary.Func = SummaryFunc.Sum;
                lblTongTonDau.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTriGiaTonCuoi.Summary.Running = SummaryRunning.Group;
                lblTongTriGiaTonCuoi.Summary.Func = SummaryFunc.Sum;
                lblTongTriGiaTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongXuat.Summary.Running = SummaryRunning.Group;
                lblTongXuat.Summary.Func = SummaryFunc.Sum;
                lblTongXuat.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTriGiaXuat.Summary.Running = SummaryRunning.Group;
                lblTongTriGiaXuat.Summary.Func = SummaryFunc.Sum;
                lblTongTriGiaXuat.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTonCuoi.Summary.Running = SummaryRunning.Group;
                lblTongTonCuoi.Summary.Func = SummaryFunc.Sum;
                lblTongTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                lblTongTriGiaTonCuoi.Summary.Running = SummaryRunning.Group;
                lblTongTriGiaTonCuoi.Summary.Func = SummaryFunc.Sum;
                lblTongTriGiaTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";


                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("TenHang")
                });

                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;

                lblTuNgay.Text = DateFrom.ToString("dd/MM/yyyy");
                lblDenNgay.Text = DateFrom.ToString("dd/MM/yyyy");

                lblMaKho.Text = MaKho.ToString();
                lblTenKho.Text = TenKho.ToString();

                
                lblSoHD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHD");
                lblNgayHD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHD", "{0:dd/MM/yy}");
                lblNgayHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHH", "{0:dd/MM/yy}");

                lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
                lblMaHangHoa.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaHangHoa");
                lblTenHangHoa.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHangHoa");
                lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");

                lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDauKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaTonDauKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhapKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaNhapKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuatKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaXuatKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
