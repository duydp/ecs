using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC
{
    public partial class ThanhKhoanManageForm : BaseForm
    {
        public bool IsBrowseForm = false;
        List<HopDong>  hdcoll = new List<HopDong> ();
        ThanhKhoan thanhKhoan = new ThanhKhoan();
        List<ThanhKhoan> tkCollection = new List<ThanhKhoan>();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public ThanhKhoanManageForm()
        {
            InitializeComponent();
            cbStatus.SelectedIndex = 0;
        }


        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------

        private void ThanhKhoanManageForm_Load(object sender, EventArgs e)
        {
            //An nut Xac nhan
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;

            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            //BindData(Convert.ToInt16(cbStatus.SelectedItem.Value));
            BindHopDong();
            btnSearch_Click(null, null);

        }

        //-----------------------------------------------------------------------------------------



        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            thanhKhoan = (ThanhKhoan)dgList.GetRow().DataRow;
            ThanhKhoanHDGCSendForm f = new ThanhKhoanHDGCSendForm();
            f.HD.ID = thanhKhoan.HopDong_ID;
            f.KhaiBaoTK = thanhKhoan;
            f.ShowDialog();
            
            
        }
        private void BindHopDong()
        {
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong>  collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
            }
            collection.Add(hd);
            cbHopDong.DataSource = collection;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = collection.Count - 1;
        }
        //private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        //{
        //    //this.BindData();
        //    BindHopDong();
        //    cbHopDong.Text = "";
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["NgayTiepNhan"].Value != DBNull.Value)
                {
                    if (Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value).Year <= 1900)
                    {
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                    }
                }
                if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHUA_KHAI_BAO)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_HUY)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_HUY)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.KHONG_PHE_DUYET)
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                HopDong hd = new HopDong();
                hd.ID = (long)Convert.ToInt64(e.Row.Cells["ID_HopDong"].Value);
                if (hd.ID != 0)
                {
                    hd = HopDong.Load(hd.ID);
                    e.Row.Cells["SoHopDong"].Text = hd.SoHopDong;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and SoTiepNhan like '%" + txtSoTiepNhan.Text + "%'";
            }

            try
            {
                long idHopDong = Convert.ToInt64(cbHopDong.Value);
            }
            catch
            {
                showMsg("MSG_240205");
                //ShowMessage("Không có hợp đồng này trong hệ thống.", false);
                return;
            }
            if (cbHopDong.Text.Length > 0)
            {
                where += " and HopDong_ID like '%" + cbHopDong.Value.ToString() + "%'";
            }

            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(NgayTiepNhan)=" + txtNamTiepNhan.Text;
            }
            if (cbStatus.Text.Length > 0)
                where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
            tkCollection = (List<ThanhKhoan>)ThanhKhoan.SelectCollectionDynamic(where, null);
            dgList.DataSource = tkCollection;
            setCommandStatus();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            ThanhKhoan tk = new ThanhKhoan();
            tk = (ThanhKhoan)e.Row.DataRow;
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa định mức này không?", true) == "Yes")
                {
                    if (tk.ID > 0)
                    {
                        tk.Delete();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void setCommandStatus()
        {
            if (cbStatus.SelectedValue.ToString() == "-1" || cbStatus.SelectedValue.ToString() == "2" || cbStatus.SelectedValue.ToString() == "10")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = cbStatus.SelectedValue.ToString() == "-1"? Janus.Windows.UI.InheritableBoolean.True:
                     Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;

            }
            else if (cbStatus.SelectedValue.ToString() == "0" || cbStatus.SelectedValue.ToString() == "11")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = cbStatus.SelectedValue.ToString() == "0"? Janus.Windows.UI.InheritableBoolean.True:
                     Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
            else
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatDinhMuc.Enabled = cmdXuatDinhMuc1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "KhaiBao": this.SendV3();/* khaibaoHD()*/; break;
                case "NhanDuLieu": this.FeedBackV3();/* nhandulieuHD()*/; break;
                case "Huy": this.CanceldV3();/* HuyNhieuHD()*/; break;
                //DATLMQ comment ngày 24/03/2011
                //case "XacNhan": LaySoTiepNhanDT(); break;
                case "XacNhan": this.FeedBackV3() /*nhandulieuHD()*/; break;
                case "cmdCSDaDuyet": ChuyenTrangThai(); break;
                //case "cmdXuatDinhMuc": XuatDinhMucChoPhongKhai();
                //    break;
                case "InPhieuTN": this.inPhieuTN(); break;
                case "cmdUpdateGUIDSTR": this.updatestr(); break;
                    
            }
        }

        public void updatestr()
        {
            int ID = Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value.ToString());
            frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_ThanhKhoan", "", ID);
            frm.ShowDialog();

            btnSearch_Click(null, null);
        }
        private void inPhieuTN()
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "THANH KHOẢN HỢP ";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ThanhKhoan thanhKhoanSelect = (ThanhKhoan)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = thanhKhoanSelect.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = thanhKhoanSelect.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                List<ThanhKhoan> tkColl = new List<ThanhKhoan>();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    tkColl.Add((ThanhKhoan)grItem.GetRow().DataRow);
                }
                for (int i = 0; i < tkColl.Count; i++)
                {
                    //string msg = "Bạn có muốn chuyển trạng thái của định mức đăng ký được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự của định mức đăng ký: " + dmdkColl[i].ID.ToString();
                    //dmdkColl[i].LoadCollection();
                    //msg += "\nCó tổng số " + dmdkColl[i].DMCollection.Count.ToString() + " định mức.";

                    tkColl[i].LoadHangCollection();
                    string[] args = new string[2];
                    args[0] = tkColl[i].ID.ToString();
                    args[1] = tkColl[i].HangCollection.Count.ToString();

                    if (showMsg("MSG_0203071", args, true) == "Yes")
                    {
                        if (tkColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            tkColl[i].NgayTiepNhan = DateTime.Today;
                        }
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                            tkColl[i].ID,
                            tkColl[i].GUIDSTR,
                            (MessageTypes)int.Parse(DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD),
                            Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                             Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,
                             string.Format("Trước khi chuyển ID={0},GUIDSTR={1}", tkColl[i].ID, tkColl[i].GUIDSTR));
                        tkColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    }
                }
                this.btnSearch_Click(null, null);
            }
            else
            {
                showMsg("MSG_2702040", false);
                //ShowMessage("Chưa có dữ liệu được chọn!", false);
            }
        }

        //private void XuatDinhMucChoPhongKhai()
        //{
        //    GridEXSelectedItemCollection items = dgList.SelectedItems;
        //    if (items.Count == 0)
        //    {
        //        showMsg("MSG_2702038");
        //        //ShowMessage("Chưa có danh sách định mức cần xuất ra file.", false);
        //        return;
        //    }
        //    try
        //    {
        //        DinhMucDangKyCollection col = new DinhMucDangKyCollection();
        //        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
        //            FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
        //            int sodinhmuc = 0;
        //            foreach (GridEXSelectedItem i in items)
        //            {
        //                if (i.RowType == RowType.Record)
        //                {
        //                    DinhMucDangKy DM = (DinhMucDangKy)i.GetRow().DataRow;
        //                    HopDong HD = new HopDong();
        //                    HD.ID = DM.ID_HopDong;

        //                    HD = HopDong.Load(HD.ID);
        //                    DM.SoHopDong = HD.SoHopDong;
        //                    DM.LoadCollection();
        //                    col.Add(DM);
        //                    sodinhmuc++;
        //                }
        //            }
        //            serializer.Serialize(fs, col);
        //            showMsg("MSG_2702039", sodinhmuc);
        //            //ShowMessage("Xuất ra file thành công " + sodinhmuc + " định mức.", false);
        //            fs.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        showMsg("MSG_2702004", ex.Message);
        //        //ShowMessage("Lỗi : " + ex.Message, false);
        //    }
        //}


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa danh sách khai báo thanh khoản này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    ThanhKhoan tk = (ThanhKhoan)row.GetRow().DataRow;
                    Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                    sendXML.master_id = tk.ID;
                    if (sendXML.Load())
                    {
                        int so = row.Position + 1;
                        string st = showMsg("MSG_240236", so.ToString(), true);
                        //string st = ShowMessage("Danh sách thứ " + (row.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", true);
                        if (st == "Yes")
                        {
                            if (tk.ID > 0)
                            {
                                try
                                {

                                    tk.Delete();

                                }
                                catch (Exception ex)
                                {
                                    showMsg("MSG_2702004", ex.Message);
                                    //ShowMessage("Lỗi: " + ex, false);
                                }
                            }
                        }
                    }
                    if (tk.ID > 0)
                    {
                        try
                        {
                            tk.Delete();
                        }
                        catch (Exception ex)
                        {
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage("Lỗi: " + ex, false);
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        //-----------------------------------------------------------------------------------------
    
      

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void btnInDMSP_Click(object sender, EventArgs e)
        {
        //    GridEXRow row = dgList.GetRow();
        //    if (row == null || row.RowType != RowType.Record) return;
        //    DinhMucDangKy DMDK = (DinhMucDangKy)row.DataRow;
        //    Report.ReportViewBC03_DMDKForm reportForm = new Company.Interface.Report.ReportViewBC03_DMDKForm();
        //    reportForm.DMDangKy = DMDK;
        //    reportForm.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<ThanhKhoan> tkCo = new List<ThanhKhoan>();
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa danh sách định mức của sản phẩm này không?", true) == "Yes")
            {

                foreach (GridEXSelectedItem row in items)
                {
                    ThanhKhoan tk = (ThanhKhoan)row.GetRow().DataRow;
                    Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                    sendXML.master_id = tk.ID;
                    if (sendXML.Load())
                    {
                        string st = showMsg("MSG_240236", row.Position + 1, true);
                        //string st = ShowMessage("Danh sách thứ " + (row.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", true);
                        if (st == "Yes")
                        {
                            if (tk.ID > 0)
                            {
                                tk.Delete();
                            }
                        }
                    }
                    if (tk.ID > 0)
                    {
                        tk.Delete();
                    }

                    //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    try
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", tk.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ThanhKhoanHopDong);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = true;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                        return;
                    }

                    tkCo.Add(tk);

                }
                foreach (ThanhKhoan tk in tkCo)
                {
                    tkCollection.Remove(tk);
                }
                dgList.DataSource = tkCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
                //this.BindData(Convert.ToInt16(this.cbStatus.SelectedItem.Value));
            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702040");
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            ThanhKhoan tkSelect = (ThanhKhoan)dgList.GetRow().DataRow;
            //Globals.ShowKetQuaXuLyBoSung(dmdkSelect.GUIDSTR);
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = tkSelect.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD;
            form.ShowDialog(this);
        }
        #region V3 - 14/03/2012
        private void SendV3()
        {
            if (dgList.GetRow() == null)
            {
                ShowMessage("Bạn chưa chọn tờ khai thanh khoản", false);
                return;
            }
            thanhKhoan = (ThanhKhoan)dgList.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                sendXML.master_id = thanhKhoan.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = NhanDuLieu2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }
            if (thanhKhoan.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi khai báo", false);
                return;
            }
            thanhKhoan.LoadHangCollection();
            if (thanhKhoan.HangCollection.Count == 0)
            {
                showMsg("MSG_2702015");
                //MLMessages("Chưa nhập phụ kiện","MSG_WRN19","", false);
                return;
            }
            HopDong HD = new HopDong();
            HD.ID = thanhKhoan.HopDong_ID;
            HD.LoadDataHopDong(HD.ID, null);
            try
            {
                thanhKhoan.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_ThanhKhoan tk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferThanhKhoan(thanhKhoan, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = thanhKhoan.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(thanhKhoan.MaHaiQuan),
                                     Identity = thanhKhoan.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                                    Function = tk.Function,
                                    Reference = thanhKhoan.GUIDSTR,
                                }
                                ,
                                tk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(thanhKhoan.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoThanhKhoanHopDong);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
                    sendXML.master_id = thanhKhoan.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.ThanhKhoanHDSendHandler(thanhKhoan, ref msgInfor, e);

        }
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            if (dgList.GetRow() == null) return;
            thanhKhoan = (ThanhKhoan)dgList.GetRow().DataRow;
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                Reference = thanhKhoan.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = thanhKhoan.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(thanhKhoan.MaHaiQuan.Trim()),
                                              Identity = thanhKhoan.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        this.setCommandStatus();
                }
            }
        }
        private void CanceldV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ThanhKhoanHopDong;
            sendXML.master_id = thanhKhoan.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            Company.KDT.SHARE.Components.DeclarationBase dinhmuc = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD, thanhKhoan.GUIDSTR, thanhKhoan.SoTiepNhan, thanhKhoan.MaHaiQuan, thanhKhoan.NgayTiepNhan);
            ObjectSend msgSend = new  ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = thanhKhoan.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(thanhKhoan.MaHaiQuan),
                                 Identity = thanhKhoan.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_YEU_CAU_THANH_KHOAN_HD,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = thanhKhoan.GUIDSTR,
                            }
                            ,
                            null);
            SendMessageForm sendForm = new SendMessageForm();
            thanhKhoan.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && thanhKhoan.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(thanhKhoan.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
                sendXML.func = 3;
                sendXML.InsertUpdate();
                thanhKhoan.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
    }
}
