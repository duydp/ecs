﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;

namespace Company.KDT.SHARE.Components
{
    public class CustomsGoodsItem
    {
        /// <summary>
        /// Trị giá khai báo (nguyên tệ) n..16,2
        /// </summary>
        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }

        /// <summary>
        /// Số thứ tự hàng n..5
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }
        /// <summary>
        /// Trị Giá tính thuế n..16
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }
        /// <summary>
        /// Đơn giá nguyên tệ n..16,2
        /// </summary>
        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }
        /// <summary>
        /// Đơn giá TT
        /// </summary>
        [XmlElement("statisticalUnitPrice")]
        public string StatisticalUnitPrice { get; set; }
        /// <summary>
        /// Hãng sản xuất 
        /// </summary>
        [XmlElement("Manufacturer")]
        public NameBase Manufacturer { get; set; }
        /// <summary>
        /// xuất xứ
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Chi tiết hàng, thuế
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        /// <summary>
        /// C/0
        /// </summary>
        [XmlElement("CertificateOfOrigin")]
        public CertificateOfOrigin CertificateOfOrigin { get; set; }
        /// <summary>
        /// Giấy phép
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }
        /// <summary>
        /// Trị giá hải quan
        /// </summary>
        [XmlElement("CustomsValuation")]
        public CustomsValuation CustomsValuation { get; set; }
        /// <summary>
        /// Thông tin tờ khai trị giá
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public List<AdditionalInformation> AdditionalInformations { get; set; }
        /// <summary>
        /// Các giá trị điều chỉnh - Hàng trong tờ khai trị giá 
        /// </summary>
      [XmlElement("ValuationAdjustment")]
        public List<ValuationAdjustment> ValuationAdjustments { get; set; }
        /// <summary>
        /// 
        /// </summary>
      [XmlElement("SpecializedManagement")]
      public SpecializedManagement SpecializedManagement { get; set; }
        [XmlElement("conversionRate")]
      public string ConversionRate { get; set; }

        /// <summary>
        /// So Van Ban Mien Giam Thue
        /// </summary>
        [XmlElement("ReduceTax")]
        public List<ReduceTax> ReduceTax { get; set; }
    }
}
