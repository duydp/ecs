using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_AnDinhThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string TenCoQuanHaiQuan { set; get; }
		public string TenChiCucHaiQuanNoiMoToKhai { set; get; }
		public decimal SoChungTu { set; get; }
		public decimal SoToKhai { set; get; }
		public DateTime NgayDangKyToKhai { set; get; }
		public string TenDonViXuatNhapKhau { set; get; }
		public string MaDonViXuatNhapKhau { set; get; }
		public string MaBuuChinh { set; get; }
		public string DiaChiNguoiXuatNhapKhau { set; get; }
		public string SoDienThoaiNguoiXuatNhapKhau { set; get; }
		public string TenNganHangBaoLanh { set; get; }
		public string MaNganHangBaoLanh { set; get; }
		public string KiHieuChungTuBaoLanh { set; get; }
		public string SoChungTuBaoLanh { set; get; }
		public string LoaiBaoLanh { set; get; }
		public string TenNganHangTraThay { set; get; }
		public string MaNganHangTraThay { set; get; }
		public string KiHieuChungTuPhatHanhHanMuc { set; get; }
		public string SoHieuPhatHanhHanMuc { set; get; }
		public decimal TongSoThueKhaiBao { set; get; }
		public decimal TongSoThueAnDinh { set; get; }
		public decimal TongSoThueChenhLech { set; get; }
		public string MaTienTe { set; get; }
		public decimal TyGiaQuyDoi { set; get; }
		public decimal SoNgayDuocAnHan { set; get; }
		public DateTime NgayHetHieuLucTamNhapTaiXuat { set; get; }
		public string SoTaiKhoanKhoBac { set; get; }
		public string TenKhoBac { set; get; }
		public string LaiSuatPhatChamNop { set; get; }
		public DateTime NgayPhatHanhChungTu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection = new List<KDT_VNACC_ToKhaiMauDich_AnDinhThue>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_AnDinhThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanHaiQuan"))) entity.TenCoQuanHaiQuan = reader.GetString(reader.GetOrdinal("TenCoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChiCucHaiQuanNoiMoToKhai"))) entity.TenChiCucHaiQuanNoiMoToKhai = reader.GetString(reader.GetOrdinal("TenChiCucHaiQuanNoiMoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetDecimal(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyToKhai"))) entity.NgayDangKyToKhai = reader.GetDateTime(reader.GetOrdinal("NgayDangKyToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViXuatNhapKhau"))) entity.TenDonViXuatNhapKhau = reader.GetString(reader.GetOrdinal("TenDonViXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViXuatNhapKhau"))) entity.MaDonViXuatNhapKhau = reader.GetString(reader.GetOrdinal("MaDonViXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinh"))) entity.MaBuuChinh = reader.GetString(reader.GetOrdinal("MaBuuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiXuatNhapKhau"))) entity.DiaChiNguoiXuatNhapKhau = reader.GetString(reader.GetOrdinal("DiaChiNguoiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNguoiXuatNhapKhau"))) entity.SoDienThoaiNguoiXuatNhapKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiNguoiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangBaoLanh"))) entity.TenNganHangBaoLanh = reader.GetString(reader.GetOrdinal("TenNganHangBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangBaoLanh"))) entity.MaNganHangBaoLanh = reader.GetString(reader.GetOrdinal("MaNganHangBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiHieuChungTuBaoLanh"))) entity.KiHieuChungTuBaoLanh = reader.GetString(reader.GetOrdinal("KiHieuChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTuBaoLanh"))) entity.SoChungTuBaoLanh = reader.GetString(reader.GetOrdinal("SoChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiBaoLanh"))) entity.LoaiBaoLanh = reader.GetString(reader.GetOrdinal("LoaiBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangTraThay"))) entity.TenNganHangTraThay = reader.GetString(reader.GetOrdinal("TenNganHangTraThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangTraThay"))) entity.MaNganHangTraThay = reader.GetString(reader.GetOrdinal("MaNganHangTraThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"))) entity.KiHieuChungTuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPhatHanhHanMuc"))) entity.SoHieuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("SoHieuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoThueKhaiBao"))) entity.TongSoThueKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongSoThueKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoThueAnDinh"))) entity.TongSoThueAnDinh = reader.GetDecimal(reader.GetOrdinal("TongSoThueAnDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoThueChenhLech"))) entity.TongSoThueChenhLech = reader.GetDecimal(reader.GetOrdinal("TongSoThueChenhLech"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTe"))) entity.MaTienTe = reader.GetString(reader.GetOrdinal("MaTienTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaQuyDoi"))) entity.TyGiaQuyDoi = reader.GetDecimal(reader.GetOrdinal("TyGiaQuyDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayDuocAnHan"))) entity.SoNgayDuocAnHan = reader.GetDecimal(reader.GetOrdinal("SoNgayDuocAnHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHieuLucTamNhapTaiXuat"))) entity.NgayHetHieuLucTamNhapTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayHetHieuLucTamNhapTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTaiKhoanKhoBac"))) entity.SoTaiKhoanKhoBac = reader.GetString(reader.GetOrdinal("SoTaiKhoanKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKhoBac"))) entity.TenKhoBac = reader.GetString(reader.GetOrdinal("TenKhoBac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LaiSuatPhatChamNop"))) entity.LaiSuatPhatChamNop = reader.GetString(reader.GetOrdinal("LaiSuatPhatChamNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanhChungTu"))) entity.NgayPhatHanhChungTu = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanhChungTu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_AnDinhThue VALUES(@TenCoQuanHaiQuan, @TenChiCucHaiQuanNoiMoToKhai, @SoChungTu, @SoToKhai, @NgayDangKyToKhai, @TenDonViXuatNhapKhau, @MaDonViXuatNhapKhau, @MaBuuChinh, @DiaChiNguoiXuatNhapKhau, @SoDienThoaiNguoiXuatNhapKhau, @TenNganHangBaoLanh, @MaNganHangBaoLanh, @KiHieuChungTuBaoLanh, @SoChungTuBaoLanh, @LoaiBaoLanh, @TenNganHangTraThay, @MaNganHangTraThay, @KiHieuChungTuPhatHanhHanMuc, @SoHieuPhatHanhHanMuc, @TongSoThueKhaiBao, @TongSoThueAnDinh, @TongSoThueChenhLech, @MaTienTe, @TyGiaQuyDoi, @SoNgayDuocAnHan, @NgayHetHieuLucTamNhapTaiXuat, @SoTaiKhoanKhoBac, @TenKhoBac, @LaiSuatPhatChamNop, @NgayPhatHanhChungTu)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_AnDinhThue SET TenCoQuanHaiQuan = @TenCoQuanHaiQuan, TenChiCucHaiQuanNoiMoToKhai = @TenChiCucHaiQuanNoiMoToKhai, SoChungTu = @SoChungTu, SoToKhai = @SoToKhai, NgayDangKyToKhai = @NgayDangKyToKhai, TenDonViXuatNhapKhau = @TenDonViXuatNhapKhau, MaDonViXuatNhapKhau = @MaDonViXuatNhapKhau, MaBuuChinh = @MaBuuChinh, DiaChiNguoiXuatNhapKhau = @DiaChiNguoiXuatNhapKhau, SoDienThoaiNguoiXuatNhapKhau = @SoDienThoaiNguoiXuatNhapKhau, TenNganHangBaoLanh = @TenNganHangBaoLanh, MaNganHangBaoLanh = @MaNganHangBaoLanh, KiHieuChungTuBaoLanh = @KiHieuChungTuBaoLanh, SoChungTuBaoLanh = @SoChungTuBaoLanh, LoaiBaoLanh = @LoaiBaoLanh, TenNganHangTraThay = @TenNganHangTraThay, MaNganHangTraThay = @MaNganHangTraThay, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoHieuPhatHanhHanMuc = @SoHieuPhatHanhHanMuc, TongSoThueKhaiBao = @TongSoThueKhaiBao, TongSoThueAnDinh = @TongSoThueAnDinh, TongSoThueChenhLech = @TongSoThueChenhLech, MaTienTe = @MaTienTe, TyGiaQuyDoi = @TyGiaQuyDoi, SoNgayDuocAnHan = @SoNgayDuocAnHan, NgayHetHieuLucTamNhapTaiXuat = @NgayHetHieuLucTamNhapTaiXuat, SoTaiKhoanKhoBac = @SoTaiKhoanKhoBac, TenKhoBac = @TenKhoBac, LaiSuatPhatChamNop = @LaiSuatPhatChamNop, NgayPhatHanhChungTu = @NgayPhatHanhChungTu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_AnDinhThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, "TenChiCucHaiQuanNoiMoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.Decimal, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, "TenDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, "MaDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, "SoDienThoaiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, "TenNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, "KiHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, "TongSoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, "TongSoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, "TongSoThueChenhLech", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, "TyGiaQuyDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, "SoNgayDuocAnHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, "NgayHetHieuLucTamNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, "SoTaiKhoanKhoBac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoBac", SqlDbType.NVarChar, "TenKhoBac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, "LaiSuatPhatChamNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, "NgayPhatHanhChungTu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, "TenChiCucHaiQuanNoiMoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.Decimal, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, "TenDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, "MaDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, "SoDienThoaiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, "TenNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, "KiHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, "TongSoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, "TongSoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, "TongSoThueChenhLech", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, "TyGiaQuyDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, "SoNgayDuocAnHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, "NgayHetHieuLucTamNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, "SoTaiKhoanKhoBac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoBac", SqlDbType.NVarChar, "TenKhoBac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, "LaiSuatPhatChamNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, "NgayPhatHanhChungTu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_AnDinhThue VALUES(@TenCoQuanHaiQuan, @TenChiCucHaiQuanNoiMoToKhai, @SoChungTu, @SoToKhai, @NgayDangKyToKhai, @TenDonViXuatNhapKhau, @MaDonViXuatNhapKhau, @MaBuuChinh, @DiaChiNguoiXuatNhapKhau, @SoDienThoaiNguoiXuatNhapKhau, @TenNganHangBaoLanh, @MaNganHangBaoLanh, @KiHieuChungTuBaoLanh, @SoChungTuBaoLanh, @LoaiBaoLanh, @TenNganHangTraThay, @MaNganHangTraThay, @KiHieuChungTuPhatHanhHanMuc, @SoHieuPhatHanhHanMuc, @TongSoThueKhaiBao, @TongSoThueAnDinh, @TongSoThueChenhLech, @MaTienTe, @TyGiaQuyDoi, @SoNgayDuocAnHan, @NgayHetHieuLucTamNhapTaiXuat, @SoTaiKhoanKhoBac, @TenKhoBac, @LaiSuatPhatChamNop, @NgayPhatHanhChungTu)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_AnDinhThue SET TenCoQuanHaiQuan = @TenCoQuanHaiQuan, TenChiCucHaiQuanNoiMoToKhai = @TenChiCucHaiQuanNoiMoToKhai, SoChungTu = @SoChungTu, SoToKhai = @SoToKhai, NgayDangKyToKhai = @NgayDangKyToKhai, TenDonViXuatNhapKhau = @TenDonViXuatNhapKhau, MaDonViXuatNhapKhau = @MaDonViXuatNhapKhau, MaBuuChinh = @MaBuuChinh, DiaChiNguoiXuatNhapKhau = @DiaChiNguoiXuatNhapKhau, SoDienThoaiNguoiXuatNhapKhau = @SoDienThoaiNguoiXuatNhapKhau, TenNganHangBaoLanh = @TenNganHangBaoLanh, MaNganHangBaoLanh = @MaNganHangBaoLanh, KiHieuChungTuBaoLanh = @KiHieuChungTuBaoLanh, SoChungTuBaoLanh = @SoChungTuBaoLanh, LoaiBaoLanh = @LoaiBaoLanh, TenNganHangTraThay = @TenNganHangTraThay, MaNganHangTraThay = @MaNganHangTraThay, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoHieuPhatHanhHanMuc = @SoHieuPhatHanhHanMuc, TongSoThueKhaiBao = @TongSoThueKhaiBao, TongSoThueAnDinh = @TongSoThueAnDinh, TongSoThueChenhLech = @TongSoThueChenhLech, MaTienTe = @MaTienTe, TyGiaQuyDoi = @TyGiaQuyDoi, SoNgayDuocAnHan = @SoNgayDuocAnHan, NgayHetHieuLucTamNhapTaiXuat = @NgayHetHieuLucTamNhapTaiXuat, SoTaiKhoanKhoBac = @SoTaiKhoanKhoBac, TenKhoBac = @TenKhoBac, LaiSuatPhatChamNop = @LaiSuatPhatChamNop, NgayPhatHanhChungTu = @NgayPhatHanhChungTu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_AnDinhThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, "TenChiCucHaiQuanNoiMoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.Decimal, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, "TenDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, "MaDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, "SoDienThoaiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, "TenNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, "KiHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, "TongSoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, "TongSoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, "TongSoThueChenhLech", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, "TyGiaQuyDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, "SoNgayDuocAnHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, "NgayHetHieuLucTamNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, "SoTaiKhoanKhoBac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKhoBac", SqlDbType.NVarChar, "TenKhoBac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, "LaiSuatPhatChamNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, "NgayPhatHanhChungTu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, "TenChiCucHaiQuanNoiMoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.Decimal, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, "NgayDangKyToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, "TenDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, "MaDonViXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, "DiaChiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, "SoDienThoaiNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, "TenNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, "KiHieuChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, "SoChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, "LoaiBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, "TongSoThueKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, "TongSoThueAnDinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, "TongSoThueChenhLech", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, "TyGiaQuyDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, "SoNgayDuocAnHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, "NgayHetHieuLucTamNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, "SoTaiKhoanKhoBac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKhoBac", SqlDbType.NVarChar, "TenKhoBac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, "LaiSuatPhatChamNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, "NgayPhatHanhChungTu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_AnDinhThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_AnDinhThue(string tenCoQuanHaiQuan, string tenChiCucHaiQuanNoiMoToKhai, decimal soChungTu, decimal soToKhai, DateTime ngayDangKyToKhai, string tenDonViXuatNhapKhau, string maDonViXuatNhapKhau, string maBuuChinh, string diaChiNguoiXuatNhapKhau, string soDienThoaiNguoiXuatNhapKhau, string tenNganHangBaoLanh, string maNganHangBaoLanh, string kiHieuChungTuBaoLanh, string soChungTuBaoLanh, string loaiBaoLanh, string tenNganHangTraThay, string maNganHangTraThay, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, decimal tongSoThueKhaiBao, decimal tongSoThueAnDinh, decimal tongSoThueChenhLech, string maTienTe, decimal tyGiaQuyDoi, decimal soNgayDuocAnHan, DateTime ngayHetHieuLucTamNhapTaiXuat, string soTaiKhoanKhoBac, string tenKhoBac, string laiSuatPhatChamNop, DateTime ngayPhatHanhChungTu)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThue();	
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.TenChiCucHaiQuanNoiMoToKhai = tenChiCucHaiQuanNoiMoToKhai;
			entity.SoChungTu = soChungTu;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.TenDonViXuatNhapKhau = tenDonViXuatNhapKhau;
			entity.MaDonViXuatNhapKhau = maDonViXuatNhapKhau;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiXuatNhapKhau = diaChiNguoiXuatNhapKhau;
			entity.SoDienThoaiNguoiXuatNhapKhau = soDienThoaiNguoiXuatNhapKhau;
			entity.TenNganHangBaoLanh = tenNganHangBaoLanh;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.KiHieuChungTuBaoLanh = kiHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.TongSoThueKhaiBao = tongSoThueKhaiBao;
			entity.TongSoThueAnDinh = tongSoThueAnDinh;
			entity.TongSoThueChenhLech = tongSoThueChenhLech;
			entity.MaTienTe = maTienTe;
			entity.TyGiaQuyDoi = tyGiaQuyDoi;
			entity.SoNgayDuocAnHan = soNgayDuocAnHan;
			entity.NgayHetHieuLucTamNhapTaiXuat = ngayHetHieuLucTamNhapTaiXuat;
			entity.SoTaiKhoanKhoBac = soTaiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.LaiSuatPhatChamNop = laiSuatPhatChamNop;
			entity.NgayPhatHanhChungTu = ngayPhatHanhChungTu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, TenChiCucHaiQuanNoiMoToKhai);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.Decimal, SoChungTu);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, TenDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, MaDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, DiaChiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, SoDienThoaiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, TenNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, KiHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, TongSoThueKhaiBao);
			db.AddInParameter(dbCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, TongSoThueAnDinh);
			db.AddInParameter(dbCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, TongSoThueChenhLech);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, TyGiaQuyDoi);
			db.AddInParameter(dbCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, SoNgayDuocAnHan);
			db.AddInParameter(dbCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, NgayHetHieuLucTamNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLucTamNhapTaiXuat);
			db.AddInParameter(dbCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, SoTaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, LaiSuatPhatChamNop);
			db.AddInParameter(dbCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, NgayPhatHanhChungTu.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhChungTu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_AnDinhThue(long id, string tenCoQuanHaiQuan, string tenChiCucHaiQuanNoiMoToKhai, decimal soChungTu, decimal soToKhai, DateTime ngayDangKyToKhai, string tenDonViXuatNhapKhau, string maDonViXuatNhapKhau, string maBuuChinh, string diaChiNguoiXuatNhapKhau, string soDienThoaiNguoiXuatNhapKhau, string tenNganHangBaoLanh, string maNganHangBaoLanh, string kiHieuChungTuBaoLanh, string soChungTuBaoLanh, string loaiBaoLanh, string tenNganHangTraThay, string maNganHangTraThay, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, decimal tongSoThueKhaiBao, decimal tongSoThueAnDinh, decimal tongSoThueChenhLech, string maTienTe, decimal tyGiaQuyDoi, decimal soNgayDuocAnHan, DateTime ngayHetHieuLucTamNhapTaiXuat, string soTaiKhoanKhoBac, string tenKhoBac, string laiSuatPhatChamNop, DateTime ngayPhatHanhChungTu)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThue();			
			entity.ID = id;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.TenChiCucHaiQuanNoiMoToKhai = tenChiCucHaiQuanNoiMoToKhai;
			entity.SoChungTu = soChungTu;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.TenDonViXuatNhapKhau = tenDonViXuatNhapKhau;
			entity.MaDonViXuatNhapKhau = maDonViXuatNhapKhau;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiXuatNhapKhau = diaChiNguoiXuatNhapKhau;
			entity.SoDienThoaiNguoiXuatNhapKhau = soDienThoaiNguoiXuatNhapKhau;
			entity.TenNganHangBaoLanh = tenNganHangBaoLanh;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.KiHieuChungTuBaoLanh = kiHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.TongSoThueKhaiBao = tongSoThueKhaiBao;
			entity.TongSoThueAnDinh = tongSoThueAnDinh;
			entity.TongSoThueChenhLech = tongSoThueChenhLech;
			entity.MaTienTe = maTienTe;
			entity.TyGiaQuyDoi = tyGiaQuyDoi;
			entity.SoNgayDuocAnHan = soNgayDuocAnHan;
			entity.NgayHetHieuLucTamNhapTaiXuat = ngayHetHieuLucTamNhapTaiXuat;
			entity.SoTaiKhoanKhoBac = soTaiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.LaiSuatPhatChamNop = laiSuatPhatChamNop;
			entity.NgayPhatHanhChungTu = ngayPhatHanhChungTu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, TenChiCucHaiQuanNoiMoToKhai);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.Decimal, SoChungTu);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, TenDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, MaDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, DiaChiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, SoDienThoaiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, TenNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, KiHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, TongSoThueKhaiBao);
			db.AddInParameter(dbCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, TongSoThueAnDinh);
			db.AddInParameter(dbCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, TongSoThueChenhLech);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, TyGiaQuyDoi);
			db.AddInParameter(dbCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, SoNgayDuocAnHan);
			db.AddInParameter(dbCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, NgayHetHieuLucTamNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLucTamNhapTaiXuat);
			db.AddInParameter(dbCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, SoTaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, LaiSuatPhatChamNop);
			db.AddInParameter(dbCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, NgayPhatHanhChungTu.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhChungTu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_AnDinhThue(long id, string tenCoQuanHaiQuan, string tenChiCucHaiQuanNoiMoToKhai, decimal soChungTu, decimal soToKhai, DateTime ngayDangKyToKhai, string tenDonViXuatNhapKhau, string maDonViXuatNhapKhau, string maBuuChinh, string diaChiNguoiXuatNhapKhau, string soDienThoaiNguoiXuatNhapKhau, string tenNganHangBaoLanh, string maNganHangBaoLanh, string kiHieuChungTuBaoLanh, string soChungTuBaoLanh, string loaiBaoLanh, string tenNganHangTraThay, string maNganHangTraThay, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, decimal tongSoThueKhaiBao, decimal tongSoThueAnDinh, decimal tongSoThueChenhLech, string maTienTe, decimal tyGiaQuyDoi, decimal soNgayDuocAnHan, DateTime ngayHetHieuLucTamNhapTaiXuat, string soTaiKhoanKhoBac, string tenKhoBac, string laiSuatPhatChamNop, DateTime ngayPhatHanhChungTu)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThue();			
			entity.ID = id;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.TenChiCucHaiQuanNoiMoToKhai = tenChiCucHaiQuanNoiMoToKhai;
			entity.SoChungTu = soChungTu;
			entity.SoToKhai = soToKhai;
			entity.NgayDangKyToKhai = ngayDangKyToKhai;
			entity.TenDonViXuatNhapKhau = tenDonViXuatNhapKhau;
			entity.MaDonViXuatNhapKhau = maDonViXuatNhapKhau;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiXuatNhapKhau = diaChiNguoiXuatNhapKhau;
			entity.SoDienThoaiNguoiXuatNhapKhau = soDienThoaiNguoiXuatNhapKhau;
			entity.TenNganHangBaoLanh = tenNganHangBaoLanh;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.KiHieuChungTuBaoLanh = kiHieuChungTuBaoLanh;
			entity.SoChungTuBaoLanh = soChungTuBaoLanh;
			entity.LoaiBaoLanh = loaiBaoLanh;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.TongSoThueKhaiBao = tongSoThueKhaiBao;
			entity.TongSoThueAnDinh = tongSoThueAnDinh;
			entity.TongSoThueChenhLech = tongSoThueChenhLech;
			entity.MaTienTe = maTienTe;
			entity.TyGiaQuyDoi = tyGiaQuyDoi;
			entity.SoNgayDuocAnHan = soNgayDuocAnHan;
			entity.NgayHetHieuLucTamNhapTaiXuat = ngayHetHieuLucTamNhapTaiXuat;
			entity.SoTaiKhoanKhoBac = soTaiKhoanKhoBac;
			entity.TenKhoBac = tenKhoBac;
			entity.LaiSuatPhatChamNop = laiSuatPhatChamNop;
			entity.NgayPhatHanhChungTu = ngayPhatHanhChungTu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.NVarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenChiCucHaiQuanNoiMoToKhai", SqlDbType.NVarChar, TenChiCucHaiQuanNoiMoToKhai);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.Decimal, SoChungTu);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDangKyToKhai", SqlDbType.DateTime, NgayDangKyToKhai.Year <= 1753 ? DBNull.Value : (object) NgayDangKyToKhai);
			db.AddInParameter(dbCommand, "@TenDonViXuatNhapKhau", SqlDbType.NVarChar, TenDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaDonViXuatNhapKhau", SqlDbType.VarChar, MaDonViXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiXuatNhapKhau", SqlDbType.NVarChar, DiaChiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiXuatNhapKhau", SqlDbType.VarChar, SoDienThoaiNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNganHangBaoLanh", SqlDbType.NVarChar, TenNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuBaoLanh", SqlDbType.VarChar, KiHieuChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuBaoLanh", SqlDbType.VarChar, SoChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@LoaiBaoLanh", SqlDbType.NVarChar, LoaiBaoLanh);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@TongSoThueKhaiBao", SqlDbType.Decimal, TongSoThueKhaiBao);
			db.AddInParameter(dbCommand, "@TongSoThueAnDinh", SqlDbType.Decimal, TongSoThueAnDinh);
			db.AddInParameter(dbCommand, "@TongSoThueChenhLech", SqlDbType.Decimal, TongSoThueChenhLech);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@TyGiaQuyDoi", SqlDbType.Decimal, TyGiaQuyDoi);
			db.AddInParameter(dbCommand, "@SoNgayDuocAnHan", SqlDbType.Decimal, SoNgayDuocAnHan);
			db.AddInParameter(dbCommand, "@NgayHetHieuLucTamNhapTaiXuat", SqlDbType.DateTime, NgayHetHieuLucTamNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLucTamNhapTaiXuat);
			db.AddInParameter(dbCommand, "@SoTaiKhoanKhoBac", SqlDbType.VarChar, SoTaiKhoanKhoBac);
			db.AddInParameter(dbCommand, "@TenKhoBac", SqlDbType.NVarChar, TenKhoBac);
			db.AddInParameter(dbCommand, "@LaiSuatPhatChamNop", SqlDbType.NVarChar, LaiSuatPhatChamNop);
			db.AddInParameter(dbCommand, "@NgayPhatHanhChungTu", SqlDbType.DateTime, NgayPhatHanhChungTu.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhChungTu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_AnDinhThue(long id)
		{
			KDT_VNACC_ToKhaiMauDich_AnDinhThue entity = new KDT_VNACC_ToKhaiMauDich_AnDinhThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_AnDinhThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_AnDinhThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}