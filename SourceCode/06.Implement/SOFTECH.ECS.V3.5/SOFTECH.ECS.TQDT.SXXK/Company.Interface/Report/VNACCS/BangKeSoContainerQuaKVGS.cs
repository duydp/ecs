using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Data;
using BarcodeLib;
using ZXing.QrCode;
using ZXing;
using System.IO;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.VNACCS
{
    public partial class BangKeSoContainerQuaKVGS : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        int stt = 0;
        public BangKeSoContainerQuaKVGS()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            try
            {
                BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
                {
                    IncludeLabel = false,
                    Alignment = AlignmentPositions.CENTER,
                    Width = 175,
                    Height = 30,
                    RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                    BackColor = Color.White,
                    ForeColor = Color.Black,
                };

                Image img = barcode.Encode(TYPE.CODE128B, Text);
                return img;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GeneralQrCode(string QrCode)
        {
            try
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                picQrCode.Image = qrCode.Draw(QrCode, 5, 5);
                picQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage = null;
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                returnImage = Image.FromStream(ms);
            }
            return returnImage;
        }
        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }
        public void GeneralImage(byte[] byteArrayIn)
        {
            try
            {

                Image returnImage = null;
                using (MemoryStream ms = new MemoryStream(byteArrayIn))
                {
                    returnImage = Image.FromStream(ms);
                }
                Bitmap image;
                image = (Bitmap)returnImage;
                //Save Image
                byte[] byteArray = new byte[0];
                string path = System.Windows.Forms.Application.StartupPath + "\\QrCode";
                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    image.Save(path + "\\QR-" + lblSoHieuKienCont.Text.ToString() + ".png", System.Drawing.Imaging.ImageFormat.Png);
                    stream.Close();
                }
                using (image)
                {
                    LuminanceSource source;
                    source = new BitmapLuminanceSource(image);
                    BinaryBitmap bitmap = new BinaryBitmap(new ZXing.Common.HybridBinarizer(source));
                    Result result = new MultiFormatReader().decode(bitmap);
                    if (result != null)
                    {
                        GeneralQrCodeNew(result.Text);
                    }
                    else
                    {
                        picQrCode.Image = ByteToImage(byteArrayIn);
                        picQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GeneralQrCodeNew(string QrCode)
        {
            try
            {
                QrCodeEncodingOptions options = new QrCodeEncodingOptions();
                options = new QrCodeEncodingOptions
                {
                    DisableECI = true,
                    CharacterSet = "UTF-8",
                    Width = 350,
                    Height = 350,
                };
                var writer = new BarcodeWriter();
                writer.Format = BarcodeFormat.QR_CODE;
                writer.Options = options;

                var qr = new ZXing.BarcodeWriter();
                qr.Options = options;
                qr.Format = ZXing.BarcodeFormat.QR_CODE;
                var result = new Bitmap(qr.Write(QrCode));
                picQrCode.Image = result;
                picQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void BindReport(KDT_ContainerDangKy cont)
        {
            try
            {
                lblNotes.Text = cont.Note == null || String.IsNullOrEmpty(cont.Note.ToString()) ? "(Tờ khai không phải niêm phong)" : "(" + cont.Note.ToString() + ")";
                lblNgay.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Day.ToString() : cont.ThoiGianXuatDL.Day.ToString();
                lblThang.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Month.ToString() : cont.ThoiGianXuatDL.Month.ToString();
                lblNam.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Year.ToString() : cont.ThoiGianXuatDL.Year.ToString();
                string CodeHQV4 = "Z" + cont.MaHQ.Substring(0, 2) + "Z";
                IList<Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan> DVHQCollection = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.SelectCollectionDynamic("ID = '" + CodeHQV4 + "'", "");
                if (DVHQCollection.Count >= 1)
                {
                    lblTenCucHQ.Text = DVHQCollection[0].Ten.ToUpper();
                    lblChiCucHQ.Text = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(cont.MaHQ.Trim())).ToUpper().Trim();
                }
                else
                {
                    lblTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                    lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
                }
                List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + cont.LocationControl.Trim() + "'", "");
                if (CargoCollection.Count >= 1)
                {
                    List<VNACC_Category_CustomsOffice> CustomsOfficeCollction = VNACC_Category_CustomsOffice.SelectCollectionDynamic("CustomsCode='" + cont.LocationControl.Trim().Substring(0, 4) + "'", "");
                    lblChiCucHQGiamSat.Text = CustomsOfficeCollction[0].CustomsOfficeNameInVietnamese + " - " + cont.LocationControl.Trim() + ": " + CargoCollection[0].BondedAreaName.ToString().ToUpper().Trim();
                }
                else
                {
                    lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                }
                //lblTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                //lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN.ToUpper();
                //List<VNACC_Category_Cargo> CargoCollection = VNACC_Category_Cargo.SelectCollectionDynamic("BondedAreaCode='" + TKMD.MaDDLuuKho + "'", "");
                //if (CargoCollection.Count >= 1)
                //{
                //    lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + ": " + CargoCollection[0].BondedAreaName.ToString();
                //}
                //else
                //{
                //    lblChiCucHQGiamSat.Text = GlobalSettings.TEN_HAI_QUAN + " - " + TKMD.MaDDLuuKho + "";
                //}
                lblTenDonViXNK.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiepXNK.Text = GlobalSettings.MA_DON_VI;
                lblSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
                Zen.Barcode.Code128BarcodeDraw barCode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                picBarCode.Image = barCode.Draw(TKMD.SoToKhai.ToString(), 50);
                picBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                string CustomsStatus = cont.CustomsStatus == null || String.IsNullOrEmpty(cont.CustomsStatus.ToString()) ? "TQ" : cont.CustomsStatus.ToString();
                switch (CustomsStatus)
                {
                    case "TQ":
                        lblTrangThaiTK.Text = "Thông quan";
                        break;
                    case "MHBQ":
                        lblTrangThaiTK.Text = "Mang hàng bảo quan";
                        break;
                    case "GPH":
                        lblTrangThaiTK.Text = "Giải phòng hàng";
                        break;
                    case "CCK":
                        lblTrangThaiTK.Text = "Chuyển địa điểm kiểm tra";
                        break;
                    case "GSTP":
                        lblTrangThaiTK.Text = "Qua KVGS từng phần";
                        break;
                    case "KHH":
                        lblTrangThaiTK.Text = "Chờ thông quan sau khi kiểm hóa hộ";
                        break;
                    default:
                        break;
                }
                lblNgayKhai.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy") + " - " + (cont.ArrivalDeparture.Year == 1 ? TKMD.NgayCapPhep.ToString("dd/MM/yyyy") : cont.ArrivalDeparture.ToString("dd/MM/yyyy"));
                List<VNACC_Category_Common> CommonCollection = VNACC_Category_Common.SelectCollectionDynamic("Code='" + TKMD.MaLoaiHinh.ToString() + "'", "");
                if (CommonCollection.Count >= 1)
                {
                    lblLoaiHinh.Text = CommonCollection[0].Name_VN.ToString();
                }
                else
                {
                    lblLoaiHinh.Text = TKMD.MaLoaiHinh.ToString();
                }
                int PhanLuong = Convert.ToInt32(TKMD.MaPhanLoaiKiemTra);
                switch (PhanLuong)
                {
                    case 1:
                        lblLuong.Text = "Luồng xanh";
                        break;
                    case 2:
                        lblLuong.Text = "Luồng vàng";
                        break;
                    case 3:
                        lblLuong.Text = "Luồng đỏ";
                        break;
                    default:
                        break;
                }

                lblSoQuanLyHH.Text = cont.CargoCtrlNo == null || String.IsNullOrEmpty(cont.CargoCtrlNo) ? TKMD.VanDonCollection.Count == 0 ? "" : TKMD.VanDonCollection[0].SoDinhDanh : cont.CargoCtrlNo.ToString();

                lblDay.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Day.ToString() : cont.ThoiGianXuatDL.Day.ToString();
                lblMonth.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Month.ToString() : cont.ThoiGianXuatDL.Month.ToString();
                lblYear.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.Year.ToString() : cont.ThoiGianXuatDL.Year.ToString();
                lblHour.Text = cont.ThoiGianXuatDL.Year == 1 ? DateTime.Now.TimeOfDay.ToString() : cont.ThoiGianXuatDL.TimeOfDay.ToString();

                DataTable dt = new DataTable();
                this.DataSource = cont.ListCont;
                lblSoHieuKienCont.DataBindings.Add("Text", this.DataSource, "SoContainer");
                lblSoSeal.DataBindings.Add("Text", this.DataSource, "SoSeal");
                lblSoSealHQ.DataBindings.Add("Text", this.DataSource, "CustomsSeal");
                lblXacNhan.DataBindings.Add("Text", this.DataSource, "GhiChu");
                lblMaVach.DataBindings.Add("Text", this.DataSource, "Code");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                stt += 1;
                lblSTT.Text = stt.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblMaVach_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                //GeneralQrCodeNew(lblMaVach.Text);
                byte[] bytes;
                bytes = System.Convert.FromBase64String(lblMaVach.Text);
                GeneralImage(bytes);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
