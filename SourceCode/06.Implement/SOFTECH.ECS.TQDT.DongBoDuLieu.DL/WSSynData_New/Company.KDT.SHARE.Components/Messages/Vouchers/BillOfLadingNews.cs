﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BillOfLadingNews
    {
        /// <summary>
        /// Thông tin vận đơn
        /// </summary>
       [XmlElement("BillOfLading")]
       public List<BillOfLadingNew> BillOfLading { get; set; }
    }
}
