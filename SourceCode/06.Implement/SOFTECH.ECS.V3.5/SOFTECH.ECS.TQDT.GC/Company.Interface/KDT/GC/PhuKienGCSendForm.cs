﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienGCSendForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public PhuKienDangKy pkdk = new PhuKienDangKy();
        public KDT_GC_PhuKien_TTHopDong TTHopDong;
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        bool isadd = true;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        //Comment
        //-----------------------------------------------------------------------------------------
        public PhuKienGCSendForm()
        {
            InitializeComponent();
            createLoaiPhuKien();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            HD = HopDong.Load(HD.ID);
            HD.LoadCollection();
            txtSoHopDong.Text = HD.SoHopDong;
            createLoaiPhuKien();
        }
        private void SetCommandStatus()
        {
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = pkdk.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                lblTrangThai.Text = pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = pkdk.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Chờ duyệt", "Not declared");
                clcNgayTiepNhan.Value = pkdk.NgayTiepNhan;
                this.OpenType = OpenFormType.View;
            }

        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            Caption = this.Text;
            if (pkdk.ID > 0)
            {
                txtSoPhuKien.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoPhuKien.Text = pkdk.SoPhuKien.Trim();
                txtSoPhuKien.TextChanged += new EventHandler(txt_TextChanged);

                ccNgayPhuKien.TextChanged -= new EventHandler(txt_TextChanged);
                ccNgayPhuKien.Value = pkdk.NgayPhuKien;
                ccNgayPhuKien.TextChanged += new EventHandler(txt_TextChanged);

                txtNoiDung.TextChanged -= new EventHandler(txt_TextChanged);
                txtNoiDung.Text = pkdk.GhiChu;
                txtNoiDung.TextChanged += new EventHandler(txt_TextChanged);

                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = pkdk.NgayTiepNhan;
            }
            else
            {
                txtSoTiepNhan.Text = "100000000000";
            }
            pkdk.LoadCollection();
            BindData();
            SetCommandStatus();

        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = pkdk.PKCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo thông tin về loại phụ kiện.
        /// </summary>
        private void createLoaiPhuKien()
        {
            cbLoaiPhuKien.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.SelectAll();
            cbLoaiPhuKien.DisplayMember = "TenLoaiPhuKien";
            cbLoaiPhuKien.ValueMember = "ID_LoaiPhuKien";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới phụ kiện.
        /// </summary>
        private void Add()
        {
            if (!ValidateForm(false))
                return;
            if (pkdk.ID == 0)
            {
                pkdk.HopDong_ID = this.HD.ID;
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống ", "Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID = " + id.ToString(), false);
                    return;
                }
                pkdk.GhiChu = txtNoiDung.Text.Trim();
                pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                pkdk.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                pkdk.NgayPhuKien = ccNgayPhuKien.Value;
                pkdk.MaHaiQuan = HD.MaHaiQuan;
                pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (pkdk.ID == 0)
                {
                    pkdk.Insert();
                }
                else
                {
                    pkdk.Update();
                }
                SetCommandStatus();
                this.SetChange(true);
            }
            string id_loaiphukien = cbLoaiPhuKien.Value.ToString();
            ShowForm(id_loaiphukien);
            Log.LogPhuKien(pkdk, MessageTitle.AddPhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
        }

        private void ShowForm(string id_loaiphukien)
        {
            switch (id_loaiphukien.Trim())
            {
                case "101": { showPKHuyHD(true); } break;//Hủy hợp đồng
                case "201": { showPKGiaHanHD(false); } break;//Gia hạn hợp đồng
                #region Sản phẩm
                case "802": { showPKDieuChinhNhomSP(false, "802"); } break;//Thêm mới
                case "502": { showPKDieuChinhNhomSP(false, "502"); } break;//Sửa
                case "102": { showPKDieuChinhNhomSP(false, "102"); } break;//Hủy
                #endregion Sản phẩm

                #region Nguyên phụ liệu
                case "803": { showPKBoSungNPL(true, "803"); } break;//Thêm mới
                case "503": { showPKBoSungNPL(true, "503"); } break;//Sửa
                case "103": { showPKBoSungNPL(true, "103"); } break;//Hủy
                #endregion Nguyên phụ liệu
                #region Thiết bị
                case "804": { showPKBoSungThietBi(false, "804"); } break;//Thêm mới
                case "504": { showPKBoSungThietBi(false, "504"); } break;//Sửa
                case "104": { showPKBoSungThietBi(false, "104"); } break;//Hủy
                #endregion Thiết bị
                #region Hàng mẫu
                case "805": { showHangMau(false, "805"); } break;//Thêm mới
                case "505": { showHangMau(false, "505"); } break;//Sửa
                case "105": { showHangMau(false, "105"); } break;//Hủy
                #endregion Hàng mẫu
                #region Sửa thông tin chung HD
                case "501": { showHopDongSua(); } break;//sưa thong tin chung Hợp đồng
                
                #endregion 

            }
        }
        private void showPKDieuChinhSLSanPham(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {            
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
            ShowForm(id_loaiphukien);
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "Số hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbLoaiPhuKien, errorProvider, "Loại phụ kiện",isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNoiDung, errorProvider, "Nội dung", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(ccNgayPhuKien, errorProvider, "Ngày phụ kiện", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            return isValid;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>

        private void Save()
        {
            if (!ValidateForm(false))
                return;
            try
            {
                HD = HopDong.Load(HD.ID);
                pkdk.HopDong_ID = this.HD.ID;                
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống ", "Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID = " + id.ToString(), false);
                    return;
                }
                {
                    pkdk.GhiChu = txtNoiDung.Text.Trim();
                    pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                    pkdk.NgayTiepNhan = DateTime.Now;
                    pkdk.NgayPhuKien = ccNgayPhuKien.Value;
                    pkdk.MaHaiQuan = HD.MaHaiQuan;
                    pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    pkdk.InsertUpDateFull();
                    SetCommandStatus();
                    ShowMessage("Lưu thành công !", false);
                    Log.LogPhuKien(pkdk, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    this.SetChange(false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSend":
                    this.SendV5();
                    break;
                case "cmdEdit":
                    this.SendV5();                    
                    break;
                case "NhanDuLieuPK":
                    FeedBackV5();
                    break;
                case "XacNhanThongTin":
                    FeedBackV5();
                    break;
                case "InPhieuTN": 
                    this.inPhieuTN(); 
                    break;
                case "SuaPhuKien":
                    this.ChuyenTTPhuKien();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = pkdk.ID;
                f.loaiKhaiBao = LoaiKhaiBao.PhuKien;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdk.ID, LoaiKhaiBao.PhuKien), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessagePhuKien(pkdk);
                    pkdk.InsertUpDateFull();
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        pkdk.updateTrangThaiDuLieu();
                    }
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    SetCommandStatus();
                    BindData();
                    Log.LogPhuKien(pkdk, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateGuidString()
        {
            try
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                sendXML.master_id = pkdk.ID;
                if (!sendXML.Load())
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_PhuKienDangKy", "", Convert.ToInt32(pkdk.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = pkdk.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void ChuyenTTPhuKien() {
            string msg = "";
            msg += "-------------Thông tin phụ kiện đã khai báo-------------";
            msg += "\nSố tiếp nhận : " + pkdk.SoTiepNhan.ToString();
            msg += "\nNgày tiếp nhận : " + pkdk.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
            msg += "\nHải quan tiếp nhận : " + pkdk.MaHaiQuan.ToString();
            msg += "\nCó " + pkdk.PKCollection.Count.ToString() + " phụ kiện đăng ký đã được duyệt";
            msg += "\n--------------------Thông tin xác nhận--------------------";
            if (ShowMessageTQDT(" Thông báo từ hệ thống ",msg, true) == "Yes")
            {
                
                pkdk.ActionStatus = (short)ActionStatus.PhuKienSua;
                pkdk.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                pkdk.Update();
                SetCommandStatus();
            }

        }

        private void inPhieuTN()
        {
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "PHỤ KIỆN";
            phieuTN.soTN = this.pkdk.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.pkdk.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = pkdk.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void showPKGiaHanHD(bool isAdd)
        {
            PKGiahanHDForm f = new PKGiahanHDForm();
            f.HD = HD;
            f.OpenType = this.OpenType;
            f.pkdk = pkdk;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            BindData();
        }

        private void showPKHuyHD(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "101")
                {
                    this.ShowMessage("Loại phụ kiện \"" + pk.NoiDung + "\" đã tồn tại", false);
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "101";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                    this.SetChange(true);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            BindData();
        }
        private void showPKMoPhuKienDocLap(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H11")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H11";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            BindData();
        }
        private void showPKBoSungNPL(bool isAdd, string MaLPK)
        {
            NguyenPhuLieuGCBoSungForm f = new NguyenPhuLieuGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = MaLPK;
            f.OpenType = this.OpenType;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            BindData();
        }
        private void showPKDieuChinhSoLuongNPL(bool isAdd)
        {
            FormDieuChinhSLNPL f = new FormDieuChinhSLNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKDieuChinhDVTNPL(bool isAdd)
        {
            FormDieuChinhDVTNPL f = new FormDieuChinhDVTNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKBoSungNPLVietNam(bool isAdd)
        {
            FormNPLNhapVN f = new FormNPLNhapVN();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKDieuChinhDVTSP(bool isAdd)
        {
            FormDieuChinhDVTSP f = new FormDieuChinhDVTSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKDieuChinhMaSP(bool isAdd)
        {
            FormDieuChinhMaSP f = new FormDieuChinhMaSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKDieuChinhChiTietMaSP(bool isAdd)
        {
            SanPhamGCBoSungForm f = new SanPhamGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            BindData();
        }
        private void showPKDieuChinhNhomSP(bool isAdd, string maLoaiPK)
        {

            FormSanPham f = new FormSanPham();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.OpenType = this.OpenType;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            BindData();
        }
        private void showHopDongSua()
        {
            try
            {
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien item in pkdk.PKCollection)
                {
                    if (item.MaPhuKien.Trim() == "501")
                    {
                        isadd = false;
                        TTHopDong = KDT_GC_PhuKien_TTHopDong.LoadBy(item.ID);
                    }
                }
                EditInformationHopDongForm hopdong = new EditInformationHopDongForm();
                hopdong.HD = HD;
                hopdong.isAdd = isadd;
                hopdong.TTHD = TTHopDong;
                hopdong.PKDK = pkdk;
                hopdong.NoiDung = cbLoaiPhuKien.Text;
                hopdong.OpenType = this.OpenType;
                hopdong.ShowDialog();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void showHangMau(bool isAdd, string maLoaiPK)
        {

            FormHangMau f = new FormHangMau();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.OpenType = this.OpenType;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            BindData();
        }
        private void showPKBoSungThietBi(bool isAdd, string maLoaiPK)
        {
            ThietBiGCBoSungForm f = new ThietBiGCBoSungForm();
            f.MaLoaiPK = maLoaiPK;
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.OpenType = this.OpenType;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            BindData();
        }
        private void showPKDieuChinhThietBi(bool isAdd)
        {
            FormDieuChinhSLThietBi f = new FormDieuChinhSLThietBi();
            f.HD = HD;
            f.pkdk = pkdk;
            f.OpenType = this.OpenType;
            f.ShowDialog();
            BindData();
        }
        public bool CheckExitNPLInMuaVN()
        {
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKMuaVN = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N01")
                {
                    LoaiPK = pk;
                    break;
                }
            }

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N11")
                {
                    LoaiPKMuaVN = pk;
                    break;
                }
            }
            foreach (HangPhuKien HangPKNPLBoSung in LoaiPK.HPKCollection)
            {
                foreach (HangPhuKien HangPK in LoaiPKMuaVN.HPKCollection)
                {
                    if (HangPK.MaHang.Trim().ToUpper() == HangPKNPLBoSung.MaHang.Trim().ToUpper())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnDelete_Click(null,null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                Company.GC.BLL.KDT.GC.LoaiPhuKienCollection loaiPKColl = new LoaiPhuKienCollection();
                foreach (GridEXSelectedItem item in items)
                {
                    if (item.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)item.GetRow().DataRow;
                        LoaiPK.LoadCollection();
                        loaiPKColl.Add(LoaiPK);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien item in loaiPKColl)
                {
                    if (item.ID > 0)
                    {
                        if (item.MaPhuKien.Trim() == "N01")
                        {
                            if (CheckExitNPLInMuaVN())
                            {
                                showMsg("MSG_2702067");                                
                                return;
                            }
                        }
                        item.Delete();
                    }
                    pkdk.PKCollection.Remove(item);
                }
                Log.LogPhuKien(pkdk, loaiPKColl, MessageTitle.DeletePhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                BindData();
                this.SetChange(true);
            }
        }
        #region V5 - 14/03/2018
        private void SendV5()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    if (pkdk.TrangThaiXuLy==TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        ShowMessageTQDT("Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        return;   
                    }
                }
            }
            if (pkdk.ID == 0)
            {
                ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp Vui lòng lưu thông tin trước khi khai báo", false);
                return;
            }
            if (pkdk.PKCollection.Count == 0)
            {
                ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp chưa nhập phụ kiện hoặc chưa lưu thông tin", false);
                return;
            }
            try
            {
                pkdk.GUIDSTR = Guid.NewGuid().ToString();
                Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoPhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                Company.KDT.SHARE.Components.GC_PhuKien pk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferPhuKien(pkdk, HD, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = pkdk.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan.Trim()
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                    Function = pk.Function,
                                    Reference = pkdk.GUIDSTR,
                                }
                                ,
                                pk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoPhuKien);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                    sendXML.master_id = pkdk.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = Convert.ToInt32(pk.Function);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                        pkdk.Update();
                        SetCommandStatus();
                        pkdk.updateTrangThaiDuLieu();
                        ShowMessageTQDT(msgInfor, false);
                        Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoPhuKienHQDuyet, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        SetCommandStatus();
                        FeedBackV5();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoTuChoiPhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    sendForm.Message.XmlSaveMessage(pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoTuChoiPhuKien);
                    ShowMessageTQDT(msgInfor, false);
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.PhuKienSendHandler(pkdk, ref msgInfor, e);

        }
        private void FeedBackV5()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            sendXML.master_id = pkdk.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan hoặc Hệ thống HQ bị quá tải không nhận được phản hồi . Nếu trường hợp hệ thống HQ bị quá tải Bạn chọn Yes để tạo Message nhận phản hồi . Chọn No để bỏ qua", true) == "Yes")
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                else
                {
                    return;
                }
            }
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Reference = pkdk.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = pkdk.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan.Trim()
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoLayPhanHoiPhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        pkdk.Update();
                        pkdk.updateTrangThaiDuLieu();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                        Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoPhuKienHQDuyet, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        pkdk.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                        Log.LogPhuKien(pkdk, MessageTitle.KhaiBaoTuChoiPhuKien, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }

        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PhuKienGCSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Phụ kiện Hợp đồng có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }
    }
}
