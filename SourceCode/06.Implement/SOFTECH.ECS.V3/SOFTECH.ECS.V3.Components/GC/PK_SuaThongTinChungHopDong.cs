﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PK_SuaThongTinChungHopDong
    {
        /// <summary>
        /// ConfigPK_SuaThongTinChungHopDong
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigPK_SuaThongTinChungHopDong(PhuKienDangKy PKDK, int issuer, int function, int status)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/PK_SuaThongTinChungHopDong.xml");
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                if (HD != null && docXML != null)
                {
                    XElement Root = docXML.Root;
                    PhuKienChung.ConfigPhuKien(Root, PKDK, issuer, function, status);

                    XElement AdditionalInformation = Root.Element(NodePhuKien.AdditionalInformation);
                    XElement Content = AdditionalInformation.Element(NodePhuKien.Content);
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        NguyenTe NT = NguyenTe.Load(HD.NguyenTe_ID);
                        Nuoc nuoc;
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                AdditionalInformation = element;
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                    {
                                        Content = elementChild;
                                    }
                                }
                            }
                        }
                        foreach (XElement element in Content.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Payment.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.method.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                }
                            }
                            if (NT != null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodePhuKien.CurrencyExchange.ToUpper()))
                                    element.SetValue(NT.Ten);
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Importer.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.name.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identity.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.address.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                }
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Exporter.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.name.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identity.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.address.ToUpper()))
                                    {
                                        elementChild.SetValue("");
                                    }
                                }
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.CustomsValue.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.totalPaymentValue.ToUpper()))
                                        elementChild.SetValue("");
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.totalProductValue.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.importationCountry.ToUpper()))
                                element.SetValue("");
                            nuoc = Nuoc.Load(HD.NuocThue_ID);
                            if (nuoc != null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodePhuKien.exportationCountry.ToUpper()))
                                    element.SetValue(nuoc.Ten);
                            }

                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }
                        }
                        
                        docXML.Save(@"D:\NewPK_SuaThongChungHopDong.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }

        }
    }
}
