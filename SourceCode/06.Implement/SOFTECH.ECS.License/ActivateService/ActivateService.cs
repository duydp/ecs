using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using Components;
using Activation.Server;
using System.Collections.Generic;
using ActivationInfo;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Company.KDT.SHARE.Components;

namespace Activation
{
    /// <summary>
    /// Summary description for ActivateService
    /// </summary>
    [WebService(Namespace = "http://microsoft.com/webservices/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ActivateService : System.Web.Services.WebService
    {
        private const string AccountAuto = "10032010";
        public ActivateService()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        /**
         * ActivateSoftware dung de xac thuc cac thong so phan mem can kich hoat
         * 
         * Tham so truyen vao:
         * 
         * 
         * Serial: so CD key.
         * ProductID: Ma san pham.
         * Key: khoa nhan biet chinh may dang kich hoat.
         * */

        [WebMethod]
        public ActivateResults Activate(string Serial, string ProductID, string MachineKey)
        {
            Verify obj = new Verify(Serial, ProductID, MachineKey);
            return obj.doVerify();
        }

        [WebMethod]
        public void Activates(string xml, string pass)
        {
            if (pass.Trim().ToUpper() == GetMD5Value("ECSHCM12345ABCD").Trim().ToUpper())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                XmlNode node = doc.SelectSingleNode("Root");
                DoanhNghiep dn = new DoanhNghiep();
                dn.MaDoanhNghiep = node.SelectSingleNode("MaDoanhNghiep").InnerText;
                dn.TenDoanhNghiep = node.SelectSingleNode("MaDoanhNghiep").InnerText;
                dn.DiaChi = node.SelectSingleNode("MaDoanhNghiep").InnerText;
                dn.GhiChu = node.SelectSingleNode("MaDoanhNghiep").InnerText;
                dn.LoaiHinh = node.SelectSingleNode("MaDoanhNghiep").InnerText;
                dn.Insert();
            }
        }
        [WebMethod]
        public string GetClientInfo(string machineKey, string productID)
        {
            Client client = Client.Load(machineKey);
            if (client == null) return string.Empty;
            else
            {
                ClientInfor clientInfor = new ClientInfor()
                {
                    MaMay = client.MaMay
                };
                Customer customer = Customer.Load(client.CustomersID);
                if (customer == null) return string.Empty;
                ClientDetail clientDetail = ClientDetail.Load(productID, machineKey);
                if (clientDetail == null) clientDetail = new ClientDetail()
               {
                   NgayOnline = DateTime.Now,
                   ProductsID = productID,
                   MaMay = machineKey,
                   NgayHetHan = DateTime.Now.AddDays(30),
                   NgayDangKy = DateTime.Now

               };


                clientInfor.TrialLeft = (int)((clientDetail.NgayHetHan - DateTime.Now).Days);

                clientInfor.ClientDetailInfo = new ClientDetailInfo()
                {

                    DBName = clientDetail.DBName,
                    DiaChiHaiQuan = clientDetail.DiaChiHaiQuan,
                    HostProxy = clientDetail.HostProxy,
                    PasswordDB = clientDetail.PasswordDB,
                    PortProxy = clientDetail.PortProxy,
                    ProductsID = clientDetail.ProductsID,
                    SerialCode = clientDetail.SerialCode,
                    ServerDBName = clientDetail.ServerDBName,
                    TenDichVu = clientDetail.TenDichVu,
                    UserNameDB = clientDetail.UserNameDB,
                    NgayKichHoat = clientDetail.NgayKichHoat,
                    NgayHetHan = clientDetail.NgayHetHan,
                    MaHaiQuan = clientDetail.MaHaiQuan,
                    NgayOnline = DateTime.Now,
                    NgayDangKy = clientDetail.NgayDangKy

                };
                clientInfor.CustomersInfor = new CustomersInfor()
                {
                    ID = customer.ID,
                    BillingAddress = customer.BillingAddress,
                    City = customer.City,
                    CompanyOrDepartment = customer.CompanyOrDepartment,
                    ContactFirstName = customer.ContactFirstName,
                    ContactLastName = customer.ContactLastName,
                    ContactTitle = customer.ContactTitle,
                    Email = customer.Email,
                    FaxNumber = customer.FaxNumber,
                    MaDN = customer.MaDN,
                    Name = customer.Name,
                    PhoneNumber = customer.PhoneNumber,
                    PostalCode = customer.PostalCode,
                    StateOrProvince = customer.StateOrProvince,
                    Notes = customer.Notes

                };

                return Helpers.Serializer(clientInfor);
            }
        }

        [WebMethod]
        public bool SetClient(string sfmtClientInfo)
        {

            ActivationInfo.ClientInfor clientinf = null;
            if (string.IsNullOrEmpty(sfmtClientInfo))
                return false;
            try
            {
                clientinf = Helpers.Deserialize<ActivationInfo.ClientInfor>(sfmtClientInfo);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

            if (clientinf == null) return false;

            //License lic = null;
            //Product p = null;

            //if (!string.IsNullOrEmpty(clientinf.ClientDetailInfo.SerialCode))
            //{
            //    lic = License.Load(clientinf.ClientDetailInfo.SerialCode);
            //    if (lic == null) return false;
            //    p = Product.Load(clientinf.ClientDetailInfo.ProductsID);
            //    if (p == null) return false;
            //}

            Customer customer = null;

            if (clientinf.CustomersInfor != null && !string.IsNullOrEmpty(clientinf.CustomersInfor.ID))
            {
                customer = Customer.Load(clientinf.CustomersInfor.ID);
                if (customer == null) return false;
            }
            else
                customer = new Customer
                {
                    ID = Math.Abs(DateTime.Now.GetHashCode()).ToString(),
                    Name = "Chưa đăng ký",
                    MaDN = clientinf.CustomersInfor.MaDN,
                    Notes = "Đăng ký dùng thử miễn phí"
                };


            ClientDetail clientDetail = null;
            ClientDetailInfo clientDetailInfo = clientinf.ClientDetailInfo;

            Client client = Client.Load(clientinf.MaMay);
            if (client == null)
            {
                client = new Client()
                {
                    MaMay = clientinf.MaMay,
                    CustomersID = customer.ID,
                    TenMay = clientinf.TenMay,
                    OsVersion = clientinf.OsVersion
                };
            }

            client.TenMay = clientinf.TenMay;
            client.OsVersion = clientinf.OsVersion;
            clientDetail = ClientDetail.Load(clientinf.ClientDetailInfo.ProductsID, client.MaMay);

            if (clientDetail == null)
                clientDetail = new ClientDetail
                {
                    ProductsID = clientinf.ClientDetailInfo.ProductsID,
                    MaMay = clientinf.MaMay,
                    NgayDangKy = DateTime.Now,
                    NgayHetHan = DateTime.Now.AddDays((double)clientinf.TrialLeft),
                    NgayKichHoat = new DateTime(1900, 1, 1)
                };


            //Client Detail
            clientDetail.NgayOnline = DateTime.Now;
            clientDetail.UserNameDB = clientDetailInfo.UserNameDB;
            clientDetail.ServerDBName = clientDetailInfo.ServerDBName;
            clientDetail.TenDichVu = clientDetailInfo.TenDichVu;
            clientDetail.PortProxy = clientDetailInfo.PortProxy;
            clientDetail.PasswordDB = clientDetailInfo.PasswordDB;
            clientDetail.DBName = clientDetailInfo.DBName;
            clientDetail.DiaChiHaiQuan = clientDetailInfo.DiaChiHaiQuan;
            clientDetail.HostProxy = clientDetailInfo.HostProxy;
            clientDetail.MaHaiQuan = clientDetailInfo.MaHaiQuan;
            clientDetail.ProductsID = clientinf.ClientDetailInfo.ProductsID;

            //Customer

            customer.MaDN = clientinf.CustomersInfor.MaDN;
            customer.BillingAddress = clientinf.CustomersInfor.BillingAddress;
            customer.PhoneNumber = clientinf.CustomersInfor.PhoneNumber;
            customer.Name = clientinf.CustomersInfor.Name;
            customer.Email = clientinf.CustomersInfor.Email;
            customer.ContactFirstName = clientinf.CustomersInfor.ContactFirstName;
            customer.FaxNumber = clientinf.CustomersInfor.FaxNumber;

            #region Save
            bool commit = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        customer.InsertUpdate(transaction);
                        client.InsertUpdate(transaction);
                        clientDetail.InsertUpdate(transaction);

                        transaction.Commit();
                        commit = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        commit = false;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            #endregion Save

            return commit;
        }

        private string GenerateKey(string licenseID, string productID, string sfmtXml)
        {
            /*
            ActivationInfo.ClientInfor client = null;
            try
            {
                client = Helpers.Deserialize<ActivationInfo.ClientInfor>(sfmtXml);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return string.Empty;
            }
            if (client == null) return string.Empty;
            License lic = License.Load(client.ClientDetailInfo.SerialCode);
            if (lic == null) return string.Empty;
            Product p = Product.Load(client.ClientDetailInfo.ProductsID);
            if (p == null) return string.Empty;
            Customer customer = null;
            if (client.CustomersInfor != null)
            {
                List<Customer> cus = Customer.SelectCollectionDynamic("MaDoanhNghiep='" + client.CustomersInfor.MaDN + "'", "");
                if (cus.Count == 0) return string.Empty;
                else
                    customer = cus[0];
            }
            else
            {
                customer = new Customer { ID = Math.Abs(DateTime.Now.GetHashCode()).ToString() };
            }
            Employee emp = Employee.Load(AccountAuto);
            if (emp == null)
            {
                emp = new Employee { ID = AccountAuto, PassWord = "auto" };
            }
            #region Make Key
            string str = string.Format("{0}{1}{2}", new object[] { lic.Name, p.Name, DateTime.Now });
            Serial serial = new Serial
            {
                Code = Activation.Server.ClassLib.encryptString(str),
                ProductID = p.ID,
                LicenseID = lic.ID,
                CreateDate = DateTime.Now,
                EmployeeID = AccountAuto,
                Status = true
            };
            #endregion Make Key
            //Invoice inv = null;
            ClientDetail clientDetail = null;
            ClientDetailInfo clientinfo = client.ClientDetailInfo;
            if (client.ClientDetailInfo.SerialCode == 0)
                clientDetail = new ClientDetail
                {
                    DBName = clientinfo.DBName,
                    DiaChiHaiQuan = clientinfo.DiaChiHaiQuan,
                    HostProxy = clientinfo.HostProxy,
                    MaHaiQuan = clientinfo.MaHaiQuan,
                    NgayDangKy = DateTime.Now,
                    SerialCode = serial.Code,
                    UserNameDB = clientinfo.UserNameDB,
                    ServerDBName = clientinfo.ServerDBName,
                    TenDichVu = clientinfo.TenDichVu,
                    PortProxy = clientinfo.PortProxy,
                    PasswordDB = clientinfo.PasswordDB,
                    ProductsID = p.ID
                };


            #region Save
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            #endregion Save
            */
            return string.Empty;
        }
        private string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();
        }

    }

}