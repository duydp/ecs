﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.Reflection;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucNhomXuLy : ucBase
    {
        private bool _showColumnCode = true;
        public bool ShowColumnCode
        {
            get { return _showColumnCode; }
            set { _showColumnCode = value; }
        }

        private bool _showColumnName = true;
        public bool ShowColumnName
        {
            get { return _showColumnName; }
            set { _showColumnName = value; }
        }

        private ECategory _categoryType = ECategory.A014;
        /// <summary>
        /// Xác định loại danh mục.
        /// </summary>
        public ECategory CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        private string _referenceDB;
        /// <summary>
        /// Xác định mã loại danh mục
        /// </summary>
        public string ReferenceDB
        {
            get
            {
                _referenceDB = CategoryType.ToString();

                return _referenceDB;
            }
            //set { _referenceDB = value; }
        }

        /// <summary>
        /// Mã
        /// </summary>
        public string Code
        {
            get
            {
                if (IsValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.EditValue.ToString().Trim();
                    }
                }
                else
                    return cboCategory.EditValue != null ? cboCategory.EditValue.ToString().Trim() : "";
            }
            set
            {
                if (CategoryType == ECategory.A014 && value != "")
                    cboCategory.EditValue = value; //Convert.ToInt32(value); // Vì sao convert qua INT
                else
                    cboCategory.EditValue = value;
            }
        }

        /// <summary>
        /// Tên Tiếng việt
        /// </summary>
        public string Name_VN
        {
            get
            {
                if (IsValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Text.Trim();
                    }
                }
                else
                    return cboCategory.Text.Trim();
            }
            set { cboCategory.Text = value; }
        }

        public string TagName
        {
            get { return cboCategory.Tag != null ? cboCategory.Tag.ToString() : ""; }
            set { cboCategory.Tag = value; }
        }

        private EImportExport _importType = EImportExport.I;
        /// <summary>
        /// Xác định loại Nhập/ Xuất
        /// </summary>
        public EImportExport ImportType
        {
            get { return _importType; }
            set { _importType = value; }
        }

        /// <summary>
        /// NumberOfKeyItems: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class ContainerSize
        /// </summary>
        public string NumberOfKeyItems
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("NumberOfKeyItems").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CountryCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class CityUNLOCODE
        /// </summary>
        public string CountryCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("CountryCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CustomsSubSectionCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class Nhóm xử lý hồ sơ (Customs-Sub Section)
        /// </summary>
        public string CustomsSubSectionCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("CustomsSubSectionCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        private bool _setValidate = false;
        public bool SetValidate
        {
            get { return _setValidate; }
            set { _setValidate = value; }
        }

        private bool _isValidate = false;
        public bool IsValidate
        {
            get
            {
                return SetValidate ? (_isValidate = ValidateControl(false)) : true;
            }
            set { _isValidate = value; }
        }

        private bool _isOnlyWarning = false;
        public bool IsOnlyWarning
        {
            get
            {
                return _isOnlyWarning = ValidateControl(true);
            }
            set
            {
                if (value == true)
                    _isOnlyWarning = ValidateControl(true);
            }
        }

        private string _customsCode;
        /// <summary>
        /// Mã quốc gia
        /// </summary>
        public string CustomsCode
        {
            get { return _customsCode; }
            set { _customsCode = value; }
        }
        public ucNhomXuLy()
        {
            InitializeComponent();

            cboCategory.Properties.NullText = "";

            cboCategory.Enter += new EventHandler(cboCategory_Enter);
        }

        private void ucMaLoaiHinhIDA_Load(object sender, EventArgs e)
        {
            LoadData(false);
        }

        private void LoadData(bool isReload)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string whereCondition = "";
                string orderBy = "";
                string valueMember = "Code";
                string displayMember = "Name";
                string valueMemberCaption = "Mã";
                string displayMemberCaption = "Tên";
                whereCondition += base.WhereCondition != "" ? base.WhereCondition + " And " : "";

                switch (CategoryType)
                {
                    //case ECategory.A001:
                    //    break;

                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%' and ImportExportClassification = '{1}' and CustomsCode='{2}'", ReferenceDB.ToString(), ImportType.ToString(), (CustomsCode == null) ? "01AB" : CustomsCode.ToString());
                        orderBy = "CustomsSubSectionCode asc";
                        valueMember = "CustomsSubSectionCode"; //"CustomsSubSectionCode";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        //if (dataCustomsSubSections == null)
                        dataCustomsSubSections = VNACC_Category_CustomsSubSection.SelectCollectionByCustomsCode(ucBase.dataCustomsSubSectionGlobals, ReferenceDB.ToString(), ImportType.ToString(), (CustomsCode == null) ? "01AB" : CustomsCode.ToString());
                        VNACC_Category_CustomsSubSection ItemA014 = new VNACC_Category_CustomsSubSection();
                        dataCustomsSubSections.Add(ItemA014);
                        //Reload data
                        //if (isReload)
                        //{
                        //    dataCustomsSubSections.Clear();
                        //    dataCustomsSubSections = VNACC_Category_CustomsSubSection.SelectCollectionDynamic(whereCondition, orderBy);
                        //}
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCustomsSubSections;
                        #endregion
                        break;

                }

                //SET COLUMN
                cboCategory.Properties.Columns.Clear();
                // The field providing the editor's display text.
                if (ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (ShowColumnCode && !ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = valueMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (!ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = displayMember;
                }

                // Add two columns to the dropdown.
                LookUpColumnInfoCollection coll = cboCategory.Properties.Columns;
                // A column to display the Format field's values.
                coll.Add(new LookUpColumnInfo(valueMember, valueMemberCaption, 0));
                if (displayMember != "")
                    coll.Add(new LookUpColumnInfo(displayMember, displayMemberCaption, 0));

                //if (CategoryType == ECategory.A014)
                //    coll.Add(new LookUpColumnInfo("CustomsSubSectionCode", "Mã", 0));

                //if (CategoryType == ECategory.A016)
                //    coll.Add(new LookUpColumnInfo("CountryCode", "Mã nước", 0));

                //  Set column widths according to their contents and resize the popup, if required.
                cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                // Enable auto completion search mode.
                //cboCategory.Properties.SearchMode = SearchMode.AutoComplete; //Luu y: khi thiet lap thuoc tinh nay, khi chon Name co Code null se tu dong lay dong dau tien trong danh sach.
                // Specify the column against which to perform the search.
                cboCategory.Properties.AutoSearchColumnIndex = 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        /// <summary>
        /// Tải mới lại dữ liệu danh mục
        /// </summary>
        public void ReLoadData()
        {
            LoadData(true);
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            isValid &= ValidationControl.ValidateNullLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);

            if (isValid)
            {
                isValid &= ValidationControl.ValidateChooseLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);
            }

            return isValid;
        }

        public delegate void EditValueChangedHandle(object sender, EventArgs e);
        public event EditValueChangedHandle EditValueChanged;
        private void cboCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (EditValueChanged != null)
            {
                EditValueChanged(sender, e);
            }
        }

        public delegate void EnterHandle(object sender, EventArgs e);
        public event EnterHandle OnEnter;
        void cboCategory_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
        private void AddNewObject<T>(string valueMember, T obj, object value)
        {
            PropertyInfo pro = typeof(T).GetProperty(valueMember);
            if (pro != null)
                pro.SetValue(obj, value, null);
            if (obj != null)
                (cboCategory.Properties.DataSource as List<T>).Add(obj);
        }

        private void cboCategory_ProcessNewValue(object sender, ProcessNewValueEventArgs e)
        {
            if (MessageBox.Show(this, e.DisplayValue.ToString() +
      "' không có trong danh sách, bạn có muốn thêm vào?",
      "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string valueMember = string.Empty;
                string value = e.DisplayValue.ToString();
                switch (CategoryType)
                {
                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region  Add Item
                        valueMember = "CustomsSubSectionCode"; //"CustomsSubSectionCode";
                        VNACC_Category_CustomsSubSection ItemA014 = new VNACC_Category_CustomsSubSection();
                        ItemA014.CustomsCode = this.CustomsCode;
                        AddNewObject<VNACC_Category_CustomsSubSection>(valueMember, ItemA014, value);
                        #endregion
                        break;
                }

                e.Handled = true;
            }
        }
    }
}
