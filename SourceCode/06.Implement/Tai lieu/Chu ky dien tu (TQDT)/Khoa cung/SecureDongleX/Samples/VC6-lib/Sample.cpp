#include <windows.h>
#include <stdio.h>
#include "..\\..\\Include\\SDX.h"


void main()
{
	int retcode, handle, select, block_index, block_len;
	int bufferLength, startByte;
	DWORD uid, hid;
	char buffer[512] = {0};
	char bufferEncrypt[1000] = {0};
	char plainText[1000] = {0};
	char key[512] = {0};
	
	do
	{
		printf("\n");
		printf("1. Write Data\n");
		printf("2. Read Data\n");
		printf("3. Transform Data\n");
		printf("4. RSA Encryption\n");
		printf("5. RSA Decryption\n");
		printf("6. Write with Map\n");
		printf("7. Read with Map\n");
		printf("0. Exit\n");
		printf("Please enter selection: ");
		scanf("%d", &select);


		// Write to SecureDongle X with specified UID
		if (select == 1)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			printf("Please input write block index (0-4): ");
			scanf("%d", &block_index);
			printf("Please input data to be written (i.e. SDX - Sample Data): ");
			fflush(stdin);
			scanf("%[^\n]", buffer);
			retcode = SDX_Write(handle, block_index, buffer);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Write OK\n");


			SDX_Close(handle);
		}

        
		// Read SecureDongle X with specified UID
		else if (select == 2)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			printf("Please input Read block index (0-4): ");
			scanf("%d", &block_index);
			retcode = SDX_Read(handle, block_index, buffer);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Read: %s\n", buffer);

			SDX_Close(handle);
		}

		
		// Transform data using specified SecureDongle X
		else if (select == 3)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			ZeroMemory(buffer,512);
			printf("Please input data (i.e. SDXTransform Test): ");
			fflush(stdin);
			scanf("%[^\n]", buffer);
			block_len=strlen(buffer);
			retcode=SDX_Transform(handle, block_len, buffer);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Transform result: ");
			for(int i=0;i<block_len;i++)
			{
				printf("%c",buffer[i]);
			}
		
			SDX_Close(handle);
		}

		
		// Encrypt using RSA and store to SecureDongle X with specified UID
		else if (select == 4)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			printf("Please enter data to encrypt (i.e. SDX Test RSA Encrypt): ");
			fflush(stdin);
			gets(bufferEncrypt);
			fflush(stdin);
			printf("Please enter start index (0-2559): ");
			scanf("%d",&startByte);
			bufferLength = strlen(bufferEncrypt);

			retcode = SDX_RSAEncrypt(handle, startByte, bufferEncrypt, &bufferLength, key);			
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Write success, size: %d. Key to decrypt is stored inside key512 variable.\n",bufferLength);
			printf("You can try reading the contents of the SDX\n");

			SDX_Close(handle);
		}

        
		// Read from SecureDongle X with specified UID and decrypt using RSA
		else if (select == 5)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			fflush(stdin);
			printf("Please enter start index (0-2559): ");
			scanf("%d",&startByte);
			printf("Please enter length of data to decrypt: ");
			scanf("%d",&bufferLength);

			retcode = SDX_RSADecrypt(handle, startByte, plainText, &bufferLength, key);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Successfully read data with size: %d bytes. Decrypted data:\n",bufferLength);
			printf("%s\n",plainText);

			SDX_Close(handle);
		}


        // Write to SecureDongle X with specified UID
        // This is a sample of simple Data Mapping to put plain text data not in order
        // For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
		else if (select == 6)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			printf("Please input write block index (0-4): ");
			scanf("%d", &block_index);
			printf("Please input data to be written (i.e. SDX - Sample Data): ");
			fflush(stdin);
			scanf("%[^\n]", buffer);

            //****************** ENCRYPTION SAMPLE *******************
			int i, index, len;
			long size;
			char temp[512] = {0};
	
			len = strlen(buffer);
			printf("Data Length = %d\n", len);
			if (strlen(buffer) > 512)
			{
				printf("Error: Data size max is 512\n");
				return;
			}

			for (i=0; i<512; i++)
			{
				if(rand() % 2 == 0)	
				{
					temp[i] = (rand() % 26) + 65;
				}
				else
				{
					temp[i] = (rand() % 26) + 97;
				}
			}
			for (i=0; i<len; i++)
			{
				size = (long)i * (len-1);
				index = size % 512;
				temp[index] = buffer[i];
			}
            //********************************************************
			
			retcode = SDX_Write(handle, block_index, temp);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			printf("Write OK. You can try to read the content of the block\n");

			SDX_Close(handle);
		}

        
        // Read from SecureDongle X with specified UID
        // This is the sample to read the Mapped data
		else if (select == 7)
		{
			retcode = SDX_Find();
			if (retcode < 0)
			{
				printf("Error Finding SecureDongle X: %08x\n", retcode);
				return;
			}
			if (retcode == 0)
			{
				printf("No SecureDongle X plugged\n");
				return;
			}
			printf("Found SecureDongle X: %d\n", retcode);

			printf("Please input UID (i.e. 715400947): ");
			scanf("%d", &uid);
			hid = 0;
			retcode = SDX_Open(1, uid, &hid);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}
			handle = retcode;

			printf("Please input Read block index (0-4): ");
			scanf("%d", &block_index);
			retcode = SDX_Read(handle, block_index, buffer);
			if (retcode < 0)
			{
				printf("Error: %08x\n", retcode);
				return;
			}

            //******************* DECRYPTION **********************
			int i, index, len;
			long size;
			char temp[512];	

			printf("Please enter size of the data to be read (1-512): ");
			scanf("%d",&len);
			if (len < 1 || len > 512) {
				printf("Data size wrong.\n");
				return;
			}
			for (i=0;i<len;i++)
			{	
				size = (long)i * (len-1); 
				index = size % 512;
				temp[i]  = buffer[index];	
			}
			temp[i]='\0';
            //*****************************************************

			printf("Read: %s\n", temp);

			SDX_Close(handle);
		}	
	} while(select != 0);
}
