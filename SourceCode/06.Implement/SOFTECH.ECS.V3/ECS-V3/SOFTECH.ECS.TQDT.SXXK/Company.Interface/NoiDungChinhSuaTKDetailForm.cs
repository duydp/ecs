﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class NoiDungChinhSuaTKDetailForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public NoiDungDieuChinhTKDetail noiDungDieuChinhTKDetail = new NoiDungDieuChinhTKDetail();
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKDetail = new List<NoiDungDieuChinhTKDetail>();
        public List<NoiDungDieuChinhTK> ListNoiDungDieuChinhTK = new List<NoiDungDieuChinhTK>();
        public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();
        public bool isKhaiBoSung = false;
        public static int i;
        public bool isAddNew = true;
        public NoiDungChinhSuaTKDetailForm()
        {
            InitializeComponent();

            //SetEvent_TextBox_DoiTac();
        }

        private void BindData()
        {
            dgList.DataSource = ListNoiDungDieuChinhTKDetail;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void NoiDungChinhSuaTKDetailForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            //NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            //ndDieuChinhTKForm.BindData();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (TKMD.ID <= 0)
            {
                ShowMessage("Vui lòng nhập tờ khai trước", false);
                return;
            }

            if (ListNoiDungDieuChinhTKDetail.Count == 0)
            {
                ShowMessage("Chưa nhập vào nội dung điều chỉnh", false);
                btnAddNew.Focus();
                return;
            }

            try
            {
                if (noiDungDieuChinhTK.ID == 0)
                {
                    noiDungDieuChinhTK.TKMD_ID = this.TKMD.ID;
                    noiDungDieuChinhTK.SoTK = this.TKMD.SoToKhai;
                    noiDungDieuChinhTK.NgayDK = this.TKMD.NgayDangKy;
                    noiDungDieuChinhTK.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                    noiDungDieuChinhTK.NgaySua = DateTime.Now;
                    noiDungDieuChinhTK.SoDieuChinh = noiDungDieuChinhTK.SoDieuChinh;
                    noiDungDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    noiDungDieuChinhTK.InsertUpdate();
                    
                    foreach (NoiDungDieuChinhTKDetail nddetail in ListNoiDungDieuChinhTKDetail)
                    {
                        nddetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                    }
                    noiDungDieuChinhTK.InsertUpdateFull(ListNoiDungDieuChinhTKDetail);
                    if (isAddNew == true)
                    {
                        noiDungDieuChinhTK.ID = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        TKMD.NoiDungDieuChinhTKCollection.Add(noiDungDieuChinhTK);
                    }
                    BindData();
                    ShowMessage("Lưu thành công", false);
                }
                else
                {
                    foreach (NoiDungDieuChinhTKDetail nddetail in ListNoiDungDieuChinhTKDetail)
                    {
                        nddetail.Id_DieuChinh = noiDungDieuChinhTK.ID;
                    }
                    noiDungDieuChinhTK.InsertUpdateExistIDDieuChinh(ListNoiDungDieuChinhTKDetail, noiDungDieuChinhTK.ID);
                    //if (isAddNew == true)
                    //    TKMD.NoiDungDieuChinhTKCollection.Add(noiDungDieuChinhTK);
                    BindData();
                    ShowMessage("Lưu thành công", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }

        /*
        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVdcCap.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVdcCap.ButtonClick += new EventHandler(txtMaDVdcCap_ButtonClick);

            txtMaDVdcCap.Leave += new EventHandler(txtMaDVdcCap_Leave);
        }

        private void txtMaDVdcCap_ButtonClick(object sender, EventArgs e)
        {
            Company.GC.Company.KDT.SHARE.Components.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVdcCap.Text = objDoiTac.MaCongTy;
                txtTenDVdcCap.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVdcCap_Leave(object sender, EventArgs e)
        {
            if (txtMaDVdcCap.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVdcCap.Text.Trim())!="")
                txtTenDVdcCap.Text = DoiTac.GetName(txtMaDVdcCap.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE GIAP PHEP

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateGiayPhep()
        {
            bool isValid = true;

            //SoGP	varchar(50)
            isValid &= Globals.ValidateNull(txtChiCucHQMoTK, err, "Số giấy phép");
            if (isValid)
                isValid &= Globals.ValidateLength(txtChiCucHQMoTK, 50, err, "Số giấy phép");

            //Ngay giay phep
            isValid &= Globals.ValidateNull(ccNgayGiayPhep, err, "Ngày cấp giấy phép");

            //Noi cap
            isValid &= Globals.ValidateNull(txtNoiCap, err, "Nơi cấp giấy phép");

            //NGUOI_CAP	nvarchar(50)
            isValid &= Globals.ValidateLength(txtNguoiCap, 50, err, "Người cấp");

            //MA_NGUOI_DUOC_CAP	varchar(17)
            isValid &= Globals.ValidateLength(txtMaDVdcCap, 17, err, "Mã đơn vị được cấp");

            //MA_CQC	varchar(7)
            isValid &= Globals.ValidateLength(txtMaCQCap, 7, err, "Mã cơ quan cấp");

            //HINH_THUC_TL	varchar(50)

            return isValid;
        }

        #endregion End VALIDATE GIAP PHEP

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateGIAYPHEP(Company.GC.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep)
        {
            if (giayphep == null)
                return false;
            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                bool status = false;

                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkmd.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnChonGP.Enabled = status;
                    btnAddNew.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = false;
                    btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkmd.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnChonGP.Enabled = status;
                        btnAddNew.Enabled = status;
                        btnGhi.Enabled = status;
                        btnKetQuaXuLy.Enabled = true;
                        //Neu hop dong chua co so tiep nhan -> phai khai bao
                        if (giayphep.SoTiepNhan == 0)
                        {
                            btnKhaiBao.Enabled = true;
                            btnLayPhanHoi.Enabled = false;
                        }
                        //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                        else
                        {
                            btnKhaiBao.Enabled = false;
                            btnLayPhanHoi.Enabled = true;
                        }
                    }
                }
            }
            else {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }

            return true;
        }

        #endregion
        */

        private void NoiDungChinhSuaTKDetailForm_Load_1(object sender, EventArgs e)
        {
            txtChiCucHQMoTK.Text = GlobalSettings.TEN_HAI_QUAN;
            txtChiCucHQMoTK.ReadOnly = true;
            txtSoTK.Text = TKMD.SoToKhai.ToString();
            txtSoTK.ReadOnly = true;
            txtNgayMoTK.Text = TKMD.NgayTiepNhan.ToShortDateString();
            txtNgayMoTK.ReadOnly = true;
            txtSoDieuChinh.Text = noiDungDieuChinhTK.SoDieuChinh.ToString();
            txtSoDieuChinh.ReadOnly = true;

            //i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID);
            if (noiDungDieuChinhTK.ID > 0)
            {
                ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(noiDungDieuChinhTK.ID);
                if (noiDungDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || noiDungDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    btnAddNew.Enabled = false;
                    btnDelete.Enabled = false;
                    btnGhi.Enabled = false;
                }
            }
            BindData();
        }

        private void btnAddNew_Click_1(object sender, EventArgs e)
        {
            NoiDungDieuChinhTKDetail nddcDetail = new NoiDungDieuChinhTKDetail();
            if (txtNoiDungChinhTK.Text.Equals(""))
            {
                ShowMessage("Nội dung chính tờ khai không được để trống", false);
                return;
            }
            else if (txtNoiDungTKSua.Text.Equals(""))
            {
                ShowMessage("Nội dung sửa đổi bổ sung không được để trống", false);
                return;
            }
            else
            {
                nddcDetail.NoiDungTKChinh = txtNoiDungChinhTK.Text;
                nddcDetail.NoiDungTKSua = txtNoiDungTKSua.Text;
                ListNoiDungDieuChinhTKDetail.Add(nddcDetail);
                dgList.DataSource = ListNoiDungDieuChinhTKDetail;
                txtNoiDungChinhTK.Text = "";
                txtNoiDungTKSua.Text = "";
                try
                {
                    dgList.Refetch();
                }
                catch (Exception ex)
                {
                    dgList.Refresh();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<NoiDungDieuChinhTKDetail> NoiDungDieuChinhTKDetailCollection = new List<NoiDungDieuChinhTKDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa nội dung này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NoiDungDieuChinhTKDetail nddctkDetail = new NoiDungDieuChinhTKDetail();
                        nddctkDetail = (NoiDungDieuChinhTKDetail)i.GetRow().DataRow;
                        if (nddctkDetail == null) continue;

                        if (nddctkDetail.ID > 0)
                            nddctkDetail.Delete();

                        ListNoiDungDieuChinhTKDetail.Remove(nddctkDetail);
                    }
                }
            }
            BindData();
        }
    }
}

