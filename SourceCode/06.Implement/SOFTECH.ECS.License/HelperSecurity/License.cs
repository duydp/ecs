﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeySecurity
{
    public class License
    {
        public KeyInfo KeyInfo { get; set; }
        public string Message { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        public LicenseStatus Status { get; set; }
    }
}
