﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
//
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;
using System.Collections;
using System;
using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
namespace HaiQuan.HS
{
	/// <summary>
	/// Summary description for CommonForm.
	/// </summary>
	public class CommonForm : Form
	{
		private IContainer components = null;

		public CommonForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// CommonForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 366);
			this.Name = "CommonForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CommonForm";
			this.Load += new System.EventHandler(this.CommonForm_Load);

		}

		#endregion

		private void CommonForm_Load(object sender, System.EventArgs e)
		{
            if (Thread.CurrentThread.CurrentCulture.Equals(new CultureInfo("en-US")))
            {
            this.InitCulture("en-US", "Company.Interface.LanguageResource.ResourceEN");
            }
		}
        #region setlanguage
        // Get all controls in form.
        public List<Control> GetAllControls(IList controls)
        {
            List<Control> allControls = new List<Control>();
            foreach (Control control in controls)
            {
                allControls.Add(control);
                List<Control> subControls = GetAllControls(control.Controls);
                allControls.AddRange(subControls);
            }

            return allControls;
        }
        public List<Component> GetAllComponent()
        {
            List<Component> allComponent = new List<Component>();
            int i = this.components.Components.Count;
            foreach (Component component in this.components.Components)
            {
                allComponent.Add(component);
            }
            //int j = this.
            return allComponent;
        }
        public void forGrid(Control c, ResourceManager rm, string formName)
        {
            GridEX grid = (GridEX)c;
            GridEXColumnCollection columnColl = grid.RootTable.Columns;
            string columnKey = "";
            string key = String.Empty;
            foreach (GridEXColumn column in columnColl)
            {
                key = columnKey = "";
                columnKey = column.Key;
                key = formName + "_" + c.Name + "_" + columnKey;
                if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                {
                    column.Caption = rm.GetString(key);
                }
            }
            if (grid.RootTable.Groups != null)
            {
                foreach (GridEXGroup group in grid.RootTable.Groups)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_group" + group.Index.ToString();
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        group.HeaderCaption = rm.GetString(key);
                    }
                }
            }
            if (grid.RootTable.GroupHeaderTotals != null)
            {
                foreach (GridEXGroupHeaderTotal groupHeaderTotal in grid.RootTable.GroupHeaderTotals)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_" + groupHeaderTotal.Key;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        groupHeaderTotal.TotalSuffix = rm.GetString(key);
                    }
                }
            }

        }
        public List<UIPanelBase> getUIPanelChild(Janus.Windows.UI.Dock.UIPanelCollection panelColl, UIPanelGroup panelGroup)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            if (panelColl != null)//coll
            {
                foreach (UIPanelGroup panelG in panelColl)
                {
                    string n = panelG.Name;
                    allPanels.Add(panelG);
                    getUIPanelChild(null, panelG);
                }
            }
            if (panelGroup != null)//group
            {
                if (panelGroup.Panels.Count > 0)
                {
                    for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                    {
                        if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                        {
                            UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                            string n = tmppanelGroup.Name;
                            allPanels.Add(tmppanelGroup);
                            allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                            allPanels.AddRange(allPanels2);
                        }
                        else
                        {
                            string panelName = panelGroup.Panels[i].Name;
                            allPanels.Add(panelGroup.Panels[i]);
                        }
                    }
                }
            }
            return allPanels;
        }
        public List<UIPanelBase> getUIPanelChild2(Janus.Windows.UI.Dock.UIPanelCollection panelC, UIPanelGroup panelG)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            UIPanelGroup panelGroup;
            if (panelC != null)//coll
            {
                panelGroup = (UIPanelGroup)panelC[0];
                allPanels.Add(panelGroup);
            }
            else
            {
                panelGroup = panelG;
            }
            string na = panelGroup.Name;
            if (panelGroup.Panels.Count > 0)
            {
                for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                {
                    if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                    {
                        UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                        string n = tmppanelGroup.Name;
                        allPanels.Add(tmppanelGroup);
                        allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                        allPanels.AddRange(allPanels2);
                    }
                    else
                    {
                        string panelName = panelGroup.Panels[i].Name;
                        allPanels.Add(panelGroup.Panels[i]);
                    }
                }
            }
            return allPanels;
        }
        public void forPanel(Component c, ResourceManager rm, string formName)
        {
            if (c.GetType().ToString().Contains("PanelManager"))
            {
                Janus.Windows.UI.Dock.UIPanelManager p = (Janus.Windows.UI.Dock.UIPanelManager)c;
                Janus.Windows.UI.Dock.UIPanelCollection panelColl = p.Panels;
                List<UIPanelBase> panels = getUIPanelChild2(panelColl, null);
                foreach (Janus.Windows.UI.Dock.UIPanelBase panel in panels)
                {
                    string panelName = panel.Name;
                    string panelText = panel.Text;
                    string key = formName + "_" + panelName;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                    {
                        panel.Text = rm.GetString(key);
                    }
                }

            }
            else
            {

            }


        }// no need
        public void forExplore(Component c, ResourceManager rm, string formName)
        {

            ExplorerBar e = (ExplorerBar)c;
            string eName = e.Name;
            foreach (ExplorerBarGroup eGroup in e.Groups)
            {
                string groupKey = eGroup.Key;
                // allExploreGroup.Add(eGroup);                               
                if (rm.GetString(formName + "_" + groupKey) != null && rm.GetString(formName + "_" + groupKey).Trim() != "")
                {
                    eGroup.Text = rm.GetString(formName + "_" + groupKey);
                }

                foreach (ExplorerBarItem eItem in eGroup.Items)
                {
                    string itemKey = eItem.Key;
                    //  allExploreItem.Add(eItem);
                    string key = formName + "_" + groupKey + "_" + itemKey;
                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                    {
                        eItem.Text = rm.GetString(key);
                    }

                }


            }
        }
        public List<UICommand> forCommandBar(Control c, UICommand cm, ResourceManager rm, string formName)
        {
            List<UICommand> allCommand = new List<UICommand>();
            List<UICommand> suballCommand = new List<UICommand>();
            UICommandBar cmdBar = (UICommandBar)c;
            if (c != null)
            {
                string cmdBarKey = cmdBar.Name;
                foreach (UICommand cmd in cmdBar.Commands)
                {
                    string cmdKey = cmd.Key;
                    if (rm.GetString(formName + "_" + cmdKey) != null && rm.GetString(formName + "_" + cmdKey).Trim() != "")
                    {
                        cmd.Text = rm.GetString(formName + "_" + cmdKey);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = forCommandBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            if (cm != null)
            {
                string cmdKey = cm.Key;
                allCommand.Add(cm);
                foreach (UICommand cmd in cm.Commands)
                {
                    string Key = cmd.Key;
                    //cmd.Text;
                    if (rm.GetString(formName + "_" + Key) != null && rm.GetString(formName + "_" + Key) != "")
                    {
                        cmd.Text = rm.GetString(formName + "_" + Key);
                    }
                    if (cmd.Commands.Count > 0)
                    {
                        suballCommand = forCommandBar(null, cmd, rm, formName);
                        allCommand.AddRange(suballCommand);
                    }
                    else
                    {
                        allCommand.Add(cmd);
                    }
                }
            }
            return allCommand;

        }

        // Init culture and language.
        public void InitCulture(string language, string resourcefile)
        {
            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // Init control:
            ResourceManager rm = new ResourceManager(resourcefile, typeof(CommonForm).Assembly);
            List<Control> allControls = GetAllControls(this.Controls);
            if (this.components != null)
            {
                List<Component> allComponents = this.GetAllComponent();
            }

            int i = allControls.Count;
            string name = "";
            string key = "";
            foreach (Control c in allControls)
            {
                Form f = c.FindForm();

                Type ctrType = c.GetType();
                // if (ctrTypeName == "Janus.Window.GridEX.GridEX" )                
                if (ctrType == typeof(GridEX))
                {
                    this.forGrid(c, rm, f.Name);
                }
                else if (ctrType == typeof(Janus.Windows.ExplorerBar.ExplorerBar))
                {
                    this.forExplore(c, rm, f.Name);
                }
                else if (c.GetType() == typeof(UICommandBar))
                {
                    List<UICommand> cmdList = this.forCommandBar(c, null, rm, f.Name);
                }
                else if (c.GetType() == typeof(UIComboBox))
                {
                    UIComboBox cbo = (UIComboBox)c;
                    if (cbo.Items.Count > 0)
                    {
                        foreach (UIComboBoxItem item in cbo.Items)
                        {
                            key = "";
                            if (item.Text != null) 
                            { 
                                string text = item.Text.ToString();
                                switch (text.Trim())
                                {
                                    case "Chính xác từng từ": key = "method_correct_every"; break;
                                    case "Chính xác một trong các từ": key = "method_correct_any"; break;
                                    case "Chính xác cả cụm từ": key = "method_correct_all"; break;                                    
                                    case "VÀ": key = "method_and"; break;
                                    case "HOẶC": key  = "method_or"; break;
                                    
                                    

                                }
                                if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                                {
                                    item.Text = rm.GetString(key);
                                }
                            }
                        }

                    }

                }
                else if (c.GetType() == typeof(FilterEditor))
                {
                    FilterEditor filter = (FilterEditor)c;
                    key = "";
                    string columnKey = "";
                    Janus.Windows.GridEX.GridEX grid = (Janus.Windows.GridEX.GridEX)filter.SourceControl;
                    foreach (GridEXColumn column in filter.Table.Fields)
                    {
                        key = columnKey = "";
                        columnKey = column.Key;
                        key = f.Name + "_" + grid.Name + "_" + columnKey;
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            column.Caption = rm.GetString(key);
                        }
                    }
                    //filter.Refresh();
                    filter.Reload();

                }
                else if (c.GetType() == typeof(UIAutoHideStrip))
                {
                    UIAutoHideStrip hideCtr = (UIAutoHideStrip)c;
                    UIPanelBase[] panelbase = hideCtr.Panels;
                    foreach (UIPanelBase panel in panelbase)
                    {
                        key = String.Empty;
                        key = f.Name + "_" + panel.Name;
                        key = key.Trim();
                        try
                        {
                            if (rm.GetString(key) != null && rm.GetString(key).Length >= 1)
                            {
                                panel.Text = rm.GetString(key);
                            }
                        }
                        catch
                        {
                        }
                        if (panel.HasChildren)
                        {
                            forChildControlInpanel((UIPanelBase)panel, rm, f.Name);
                        }
                    }
                }
                else if (c.GetType() == typeof(UIStatusBar))
                {
                    UIStatusBar statusBar = (UIStatusBar)c;
                    if (statusBar.Panels != null)
                    {
                        foreach (UIStatusBarPanel statusP in statusBar.Panels)
                        {
                            key = "";
                            key = f.Name + "_" + c.Name + "_" + statusP.Key;
                            string s = statusP.Text;
                            if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                            {
                                statusP.Text = rm.GetString(key);
                                
                            }
                        }
                        statusBar.Refresh();
                    }                 
                }
                else
                {
                    key = String.Empty;
                    if (c.Name.Trim() != "")
                    {
                        key = f.Name + "_" + c.Name;
                        try
                        {
                            if (rm.GetString(key) != null && rm.GetString(key).Trim() != "") { c.Text = rm.GetString(key); }
                        }
                        catch
                        {
                        }
                    }

                }
                //if (ctrType == typeof(UIPanelManager) )
                //{
                name += ctrType.ToString() + ", ";
                //}               
            }
            // Init form title:               
            try
            {
                string text = this.Name.Trim();
                if (rm.GetString(text) != null && rm.GetString(text).Trim() != "")
                {
                    this.Text = rm.GetString(text);
                }
            }
            catch
            {

            }
        }
        public void forChildControlInpanel(UIPanelBase panel, ResourceManager rm, string formname)
        {
            if (panel.GetType() == typeof(UIPanelGroup))
            {
                UIPanelGroup panelG = (UIPanelGroup)panel;
                foreach (UIPanelBase pBase in panelG.Panels)
                {
                    forChildControlInpanel(pBase, rm, formname);
                }
            }
            else//UIPanel
            {
                string key = String.Empty;
                Control tmpCtr = panel.GetNextControl(new Control(), true);
                if (tmpCtr.GetType() == typeof(UIPanelInnerContainer))
                {
                    UIPanelInnerContainer panelContainer = (UIPanelInnerContainer)tmpCtr;
                    if (panelContainer.Controls != null)
                    {
                        foreach (Control control in panelContainer.Controls)
                        {
                            if (control.GetType() == typeof(GridEX))
                            {
                                this.forGrid(control, rm, formname);
                            }
                            else
                            {
                                key = String.Empty;
                                key = formname + "_" + panel.Name;
                                try
                                {
                                    if (rm.GetString(key) != null && rm.GetString(key) != "")
                                    {
                                        control.Text = rm.GetString(key);
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                else//UiPanel 
                {
                    forChildControlInpanel((UIPanel)tmpCtr, rm, formname);
                }
            }

        }

        #endregion

	}
}