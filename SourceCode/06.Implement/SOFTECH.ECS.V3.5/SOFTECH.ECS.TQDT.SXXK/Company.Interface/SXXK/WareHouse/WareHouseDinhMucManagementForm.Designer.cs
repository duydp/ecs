﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseDinhMucManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListDM_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListDM_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseDinhMucManagementForm));
            Janus.Windows.GridEX.GridEXLayout grListSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListSP_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListDM = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListSP = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchSP = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAuto = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListDM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1199, 697);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(514, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(685, 654);
            this.uiGroupBox2.TabIndex = 11;
            this.uiGroupBox2.Text = "Chi tiết định mức của sản phẩm ";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.grListDM);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 63);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(679, 588);
            this.uiGroupBox9.TabIndex = 6;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListDM
            // 
            this.grListDM.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListDM.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListDM.ColumnAutoResize = true;
            grListDM_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListDM_DesignTimeLayout_Reference_0.Instance")));
            grListDM_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListDM_DesignTimeLayout_Reference_0});
            grListDM_DesignTimeLayout.LayoutString = resources.GetString("grListDM_DesignTimeLayout.LayoutString");
            this.grListDM.DesignTimeLayout = grListDM_DesignTimeLayout;
            this.grListDM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListDM.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListDM.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListDM.FrozenColumns = 5;
            this.grListDM.GroupByBoxVisible = false;
            this.grListDM.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDM.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListDM.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDM.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListDM.Hierarchical = true;
            this.grListDM.Location = new System.Drawing.Point(3, 8);
            this.grListDM.Name = "grListDM";
            this.grListDM.RecordNavigator = true;
            this.grListDM.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListDM.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListDM.Size = new System.Drawing.Size(673, 577);
            this.grListDM.TabIndex = 86;
            this.grListDM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListDM.VisualStyleManager = this.vsmMain;
            this.grListDM.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListDM_RowDoubleClick);
            this.grListDM.SelectionChanged += new System.EventHandler(this.grListDM_SelectionChanged);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox7.Controls.Add(this.label2);
            this.uiGroupBox7.Controls.Add(this.txtNPL);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(679, 46);
            this.uiGroupBox7.TabIndex = 5;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(400, 15);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(75, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 88;
            this.label2.Text = "Mã NPL";
            // 
            // txtNPL
            // 
            this.txtNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNPL.BackColor = System.Drawing.Color.White;
            this.txtNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPL.Location = new System.Drawing.Point(98, 16);
            this.txtNPL.Name = "txtNPL";
            this.txtNPL.Size = new System.Drawing.Size(296, 21);
            this.txtNPL.TabIndex = 87;
            this.txtNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNPL.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(514, 654);
            this.uiGroupBox3.TabIndex = 10;
            this.uiGroupBox3.Text = "Danh sách  sản phẩm đã khai báo định mức";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListSP);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 63);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(508, 588);
            this.uiGroupBox8.TabIndex = 5;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListSP
            // 
            this.grListSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListSP.ColumnAutoResize = true;
            grListSP_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListSP_DesignTimeLayout_Reference_0.Instance")));
            grListSP_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListSP_DesignTimeLayout_Reference_0});
            grListSP_DesignTimeLayout.LayoutString = resources.GetString("grListSP_DesignTimeLayout.LayoutString");
            this.grListSP.DesignTimeLayout = grListSP_DesignTimeLayout;
            this.grListSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListSP.FrozenColumns = 5;
            this.grListSP.GroupByBoxVisible = false;
            this.grListSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListSP.Hierarchical = true;
            this.grListSP.Location = new System.Drawing.Point(3, 8);
            this.grListSP.Name = "grListSP";
            this.grListSP.RecordNavigator = true;
            this.grListSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListSP.Size = new System.Drawing.Size(502, 577);
            this.grListSP.TabIndex = 85;
            this.grListSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListSP.VisualStyleManager = this.vsmMain;
            this.grListSP.SelectionChanged += new System.EventHandler(this.grListSP_SelectionChanged);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearchSP);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.txtMaSP);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(508, 46);
            this.uiGroupBox6.TabIndex = 4;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchSP
            // 
            this.btnSearchSP.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSP.Image")));
            this.btnSearchSP.Location = new System.Drawing.Point(399, 16);
            this.btnSearchSP.Name = "btnSearchSP";
            this.btnSearchSP.Size = new System.Drawing.Size(75, 23);
            this.btnSearchSP.TabIndex = 87;
            this.btnSearchSP.Text = "Tìm kiếm";
            this.btnSearchSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchSP.Click += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 86;
            this.label1.Text = "Mã sản phẩm";
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.BackColor = System.Drawing.Color.White;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(104, 17);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(280, 21);
            this.txtMaSP.TabIndex = 85;
            this.txtMaSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnAuto);
            this.uiGroupBox4.Controls.Add(this.btnAdd);
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 654);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1199, 43);
            this.uiGroupBox4.TabIndex = 9;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnAuto
            // 
            this.btnAuto.Image = ((System.Drawing.Image)(resources.GetObject("btnAuto.Image")));
            this.btnAuto.Location = new System.Drawing.Point(9, 14);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(158, 23);
            this.btnAuto.TabIndex = 87;
            this.btnAuto.Text = "Tự động Map dữ liệu";
            this.btnAuto.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(1034, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 87;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1115, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 87;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // WareHouseDinhMucManagementForm
            // 
            this.ClientSize = new System.Drawing.Size(1199, 697);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseDinhMucManagementForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi định mức kho kế toán";
            this.Load += new System.EventHandler(this.WareHouseDinhMucManagementForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListDM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.GridEX grListDM;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnSearchNPL;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX grListSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearchSP;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnAuto;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnClose;
    }
}
