﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class ThanhKhoan07_ReiKer_TT117 : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();

        public ThanhKhoan07_ReiKer_TT117()
        {
            InitializeComponent();
        }

        public void BindReport(DataTable dt)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblH_SoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();
                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblH_MatHang.Text = loaiSP;
            lblH_DiaChiBN.Text = GlobalSettings.DIA_CHI;
            lblH_BenNhan.Text = GlobalSettings.TEN_DON_VI;
            lblH_DVHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN;

            lblH_NgayHD.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblH_ThoiHanHD.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblH_BenThue.Text = this.HD.DonViDoiTac;
            lblH_DiaChiBT.Text = this.HD.DiaChiDoiTac;
            lblH_HDGCS.Text = this.HD.SoHopDong;

            lblSTT.DataBindings.Add("Text", this.DataSource, "STT");           
            lblTenMay.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblSoLuongNhap.DataBindings.Add("Text", this.DataSource,"SoLuongTamNhap", "{0:N0}");
            lblDaXuatOrChuyen.DataBindings.Add("Text", this.DataSource, "SoLuongTaiXuat", "{0:N0}");
            lblConLai.DataBindings.Add("Text", this.DataSource, "ConLai", "{0:N0}");
            xrLbl_6.Text = GlobalSettings.TieudeNgay;
            xrLabel1.Text = string.Format("Ngày {0} tháng {1} năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        }

        private void lblH_HDGCS_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            //report.Label = label;
            //report.txtName.Text = label.Text;
            //report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }

        private void lblSoLuongNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblSoLuongNhap.Text.Equals("0")) lblSoLuongNhap.Text = "-";
            if (lblDaXuatOrChuyen.Text.Equals("0")) lblDaXuatOrChuyen.Text = "-";
            if (lblConLai.Text.Equals("0")) lblConLai.Text = "-";
        }
    }
}
