﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public class Helpers
    {
        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <param name="subject"></param>
        /// <param name="signature"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        private static string Build<T>(T content, SubjectBase subject, MessageSignature signature, string productType)
        {
            if (subject == null) throw new ArgumentNullException("SubjectBase không được null");

            MessageSend<T> msgSend = new MessageSend<T>()
            {
                MessageHeader = new MessageHeader()
                {
                    Reference = new Reference()
                    {
                        Version = GlobalSettings.VersionSend,
                        ReplyTo = string.Empty
                    },
                    SendApplication = new SendApplication()
                    {
                        Name = "ECS.SOFTECH",
                        Version = GlobalSettings.VersionSend,
                        CompanyName = "SOFTECH-DANANG",
                        CompanyIdentity = "0400392263",
                        CreateMessageIssue = Guid.NewGuid().ToString()
                    },
                    From = new NameBase()
                    {
                        Name = GlobalSettings.TEN_DON_VI,
                        Identity = GlobalSettings.MA_DON_VI
                    },
                    To = new NameBase()
                    {
                        Name = GlobalSettings.TEN_CUC_HAI_QUAN,
                        Identity = GlobalSettings.MA_HAI_QUAN
                    },
                    Subject = new Subject()

                }
                ,
                MessageBody = new MessageBody<T>()

            };
            msgSend.MessageHeader.ProcedureType = productType;
            msgSend.MessageHeader.Subject.Type = subject.Type;
            msgSend.MessageHeader.Subject.Function = subject.Function;
            msgSend.MessageHeader.Subject.Reference = subject.Reference;

            msgSend.MessageBody = new MessageBody<T>()
            {
                MessageContent = new MessageContent<T>()
                {
                    Declaration = content
                }
            };
            if (signature != null)
            {
                msgSend.MessageBody.MessageSignature = signature;
            }
            return Company.KDT.SHARE.Components.Helpers.Serializer(msgSend);
        }
        /// <summary>
        /// Message phong bì đóng gói thông tin khai báo của doanh nghiệp gửi lên Hải quan, tất cả các message gửi đến Hải quan đều được đóng gói bên ngoài bởi message này
        /// Chú ý các tính năng format kiểu dữ liệu xem tại
        /// http://msdn.microsoft.com/en-us/library/system.string.format.aspx
        /// http://msdn.microsoft.com/en-us/library/26etazsy.aspx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content">Nội dung khai báo</param>
        /// <param name="subject">Thông tin khai báo</param>
        /// <param name="signature">Nội dung chữ ký số</param>
        /// <returns>Format message</returns>
        public static string BuildRequest<T>(T content, SubjectBase subject, MessageSignature signature)
        {
            return Build<T>(content, subject, signature, "2");
        }
        /// <summary>
        /// Message chứa nội dung thông tin do Hải quan gửi về cho doanh nghiệp, mọi thông báo từ Hải quan gửi về cho Doanh nghiệp đều được đóng gói trong message này
        /// Chú ý các tính năng format kiểu dữ liệu xem tại
        /// http://msdn.microsoft.com/en-us/library/system.string.format.aspx
        /// http://msdn.microsoft.com/en-us/library/26etazsy.aspx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content">Nội dung lấy phản hồi</param>
        /// <param name="subject">Thông tin lấy phản hồi</param>
        /// <param name="signature">Nội dung chữ ký số</param>
        /// <returns>Format message</returns>
        public static string BuildResponse<T>(T content, SubjectBase subject, MessageSignature signature)
        {
            return Build<T>(content, subject, signature, null);
        }
    }
}
