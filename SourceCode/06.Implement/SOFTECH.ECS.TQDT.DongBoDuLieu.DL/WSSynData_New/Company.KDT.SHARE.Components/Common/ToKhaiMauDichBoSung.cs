﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Company.KDT.SHARE.Components.Common
{
    public partial class ToKhaiMauDichBoSung
    {
        public static int InsertUpdateByTKMD(long TKMD_ID, int PhienBan, bool ChuKySo, string GhiChu, SqlTransaction transaction)
        {
            const string spName = "p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@PhienBan", SqlDbType.Int, PhienBan);
            db.AddInParameter(dbCommand, "@ChuKySo", SqlDbType.Bit, ChuKySo);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public static int InsertUpdateByTKMD(long TKMD_ID, DateTime NgayKhaiBao, SqlTransaction transaction)
        {
            const string spName = "Update t_KDT_ToKhaiMauDichBoSung set NgayKhaiBao = NgayKhaiBao where TKMD_ID = @TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.Int, NgayKhaiBao);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}
