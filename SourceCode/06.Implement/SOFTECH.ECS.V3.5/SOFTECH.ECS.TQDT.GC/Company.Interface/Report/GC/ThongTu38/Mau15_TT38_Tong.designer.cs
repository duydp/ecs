﻿namespace Company.Interface.Report.GC
{
    partial class Mau15_TT38_Tong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTaiKhoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTenNPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtMaNPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTonDauKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtNhapTrongKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtXuatTrongKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTonCuoiKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblT_SoTien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblT_NhapTrongKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblT_TonCuoiKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblT_XuatTrongKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTonDauKy_T = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNam = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbDiaChi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbMaSo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_STT_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_SoTaiKhoan_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_TenHang_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_MaHang_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtSTT_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoTaiKhoan_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_MaNPL_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuong_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTriGiaNhapTrongKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTriGiaXuatTrongKy_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTriGiaTonCuoiKy_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoTaiKhoan_G_H_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTenTaiKhoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtSTT,
            this.txtTaiKhoan,
            this.txtTenNPL,
            this.txtMaNPL,
            this.txtTonDauKy,
            this.txtNhapTrongKy,
            this.txtXuatTrongKy,
            this.txtTonCuoiKy,
            this.txtGhiChu});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.8;
            // 
            // txtSTT
            // 
            this.txtSTT.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.StylePriority.UseFont = false;
            this.txtSTT.Weight = 0.13007106528839757;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.StylePriority.UseFont = false;
            this.txtTaiKhoan.Weight = 0.16214374570053572;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.StylePriority.UseFont = false;
            this.txtTenNPL.StylePriority.UseTextAlignment = false;
            this.txtTenNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtTenNPL.Weight = 0.61494338860182729;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.StylePriority.UseFont = false;
            this.txtMaNPL.StylePriority.UseTextAlignment = false;
            this.txtMaNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtMaNPL.Weight = 0.51629326191893776;
            // 
            // txtTonDauKy
            // 
            this.txtTonDauKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtTonDauKy.Name = "txtTonDauKy";
            this.txtTonDauKy.StylePriority.UseFont = false;
            this.txtTonDauKy.StylePriority.UseTextAlignment = false;
            this.txtTonDauKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtTonDauKy.Weight = 0.38086579285472044;
            this.txtTonDauKy.AfterPrint += new System.EventHandler(this.txtTonDauKy_AfterPrint);
            this.txtTonDauKy.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTonDauKy_BeforePrint);
            // 
            // txtNhapTrongKy
            // 
            this.txtNhapTrongKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtNhapTrongKy.Name = "txtNhapTrongKy";
            this.txtNhapTrongKy.StylePriority.UseFont = false;
            this.txtNhapTrongKy.StylePriority.UseTextAlignment = false;
            this.txtNhapTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtNhapTrongKy.Weight = 0.37787589285040407;
            this.txtNhapTrongKy.AfterPrint += new System.EventHandler(this.txtNhapTrongKy_AfterPrint);
            this.txtNhapTrongKy.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtNhapTrongKy_BeforePrint);
            // 
            // txtXuatTrongKy
            // 
            this.txtXuatTrongKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtXuatTrongKy.Name = "txtXuatTrongKy";
            this.txtXuatTrongKy.StylePriority.UseFont = false;
            this.txtXuatTrongKy.StylePriority.UseTextAlignment = false;
            this.txtXuatTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtXuatTrongKy.Weight = 0.39234595897345304;
            this.txtXuatTrongKy.AfterPrint += new System.EventHandler(this.txtXuatTrongKy_AfterPrint);
            this.txtXuatTrongKy.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtXuatTrongKy_BeforePrint);
            // 
            // txtTonCuoiKy
            // 
            this.txtTonCuoiKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtTonCuoiKy.Name = "txtTonCuoiKy";
            this.txtTonCuoiKy.StylePriority.UseFont = false;
            this.txtTonCuoiKy.StylePriority.UseTextAlignment = false;
            this.txtTonCuoiKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtTonCuoiKy.Weight = 0.33213113758042262;
            this.txtTonCuoiKy.AfterPrint += new System.EventHandler(this.txtTonCuoiKy_AfterPrint);
            this.txtTonCuoiKy.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTonCuoiKy_BeforePrint);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.StylePriority.UseFont = false;
            this.txtGhiChu.Weight = 0.40766956397494697;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 10F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblT_SoTien,
            this.xrLabel29,
            this.xrLabel24,
            this.lblT_NhapTrongKy,
            this.xrLabel20,
            this.lblT_TonCuoiKy,
            this.lblT_XuatTrongKy,
            this.xrLabel16,
            this.xrLabel15,
            this.lblTonDauKy_T,
            this.xrLabel17,
            this.lbNam,
            this.xrLabel14,
            this.xrLabel9,
            this.lbDiaChi,
            this.xrLabel13,
            this.xrLabel12,
            this.lbMaSo,
            this.xrLabel10,
            this.lbTenToChuc,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5});
            this.ReportHeader.HeightF = 267.9674F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBorders = false;
            // 
            // lblT_SoTien
            // 
            this.lblT_SoTien.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblT_SoTien.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblT_SoTien.LocationFloat = new DevExpress.Utils.PointFloat(480.5911F, 180.5504F);
            this.lblT_SoTien.Name = "lblT_SoTien";
            this.lblT_SoTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblT_SoTien.SizeF = new System.Drawing.SizeF(639.1432F, 29.37494F);
            this.lblT_SoTien.StylePriority.UseBorders = false;
            this.lblT_SoTien.StylePriority.UseFont = false;
            this.lblT_SoTien.StylePriority.UseTextAlignment = false;
            this.lblT_SoTien.Text = "Số tiền (VNĐ)";
            this.lblT_SoTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(98.65861F, 242.8839F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(207.6195F, 25.08334F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Tên Hàng";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(306.2781F, 242.8839F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(174.3128F, 25.0831F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Mã Hàng";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblT_NhapTrongKy
            // 
            this.lblT_NhapTrongKy.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblT_NhapTrongKy.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblT_NhapTrongKy.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 209.9259F);
            this.lblT_NhapTrongKy.Name = "lblT_NhapTrongKy";
            this.lblT_NhapTrongKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblT_NhapTrongKy.SizeF = new System.Drawing.SizeF(127.3837F, 58.04141F);
            this.lblT_NhapTrongKy.StylePriority.UseBorders = false;
            this.lblT_NhapTrongKy.StylePriority.UseFont = false;
            this.lblT_NhapTrongKy.StylePriority.UseTextAlignment = false;
            this.lblT_NhapTrongKy.Text = "Nhập trong kỳ";
            this.lblT_NhapTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(981.3611F, 209.9254F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(137.6373F, 58.0417F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Ghi chú";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblT_TonCuoiKy
            // 
            this.lblT_TonCuoiKy.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblT_TonCuoiKy.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblT_TonCuoiKy.LocationFloat = new DevExpress.Utils.PointFloat(869.226F, 209.9254F);
            this.lblT_TonCuoiKy.Name = "lblT_TonCuoiKy";
            this.lblT_TonCuoiKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblT_TonCuoiKy.SizeF = new System.Drawing.SizeF(112.1345F, 58.0417F);
            this.lblT_TonCuoiKy.StylePriority.UseBorders = false;
            this.lblT_TonCuoiKy.StylePriority.UseFont = false;
            this.lblT_TonCuoiKy.StylePriority.UseTextAlignment = false;
            this.lblT_TonCuoiKy.Text = "Tồn cuối kỳ";
            this.lblT_TonCuoiKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblT_XuatTrongKy
            // 
            this.lblT_XuatTrongKy.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblT_XuatTrongKy.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblT_XuatTrongKy.LocationFloat = new DevExpress.Utils.PointFloat(736.7605F, 209.9253F);
            this.lblT_XuatTrongKy.Name = "lblT_XuatTrongKy";
            this.lblT_XuatTrongKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblT_XuatTrongKy.SizeF = new System.Drawing.SizeF(132.4652F, 58.0417F);
            this.lblT_XuatTrongKy.StylePriority.UseBorders = false;
            this.lblT_XuatTrongKy.StylePriority.UseFont = false;
            this.lblT_XuatTrongKy.StylePriority.UseTextAlignment = false;
            this.lblT_XuatTrongKy.Text = "Xuất trong kỳ";
            this.lblT_XuatTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(43.91501F, 180.5505F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(54.74361F, 87.4169F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Tài khoản";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0.0002119276F, 180.5505F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(43.91508F, 87.41684F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "STT";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTonDauKy_T
            // 
            this.lblTonDauKy_T.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTonDauKy_T.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTonDauKy_T.LocationFloat = new DevExpress.Utils.PointFloat(480.591F, 209.9254F);
            this.lblTonDauKy_T.Name = "lblTonDauKy_T";
            this.lblTonDauKy_T.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTonDauKy_T.SizeF = new System.Drawing.SizeF(128.5894F, 58.04201F);
            this.lblTonDauKy_T.StylePriority.UseBorders = false;
            this.lblTonDauKy_T.StylePriority.UseFont = false;
            this.lblTonDauKy_T.StylePriority.UseTextAlignment = false;
            this.lblTonDauKy_T.Text = "Tồn đầu kỳ (01/01/2015)";
            this.lblTonDauKy_T.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(98.65861F, 180.5504F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(381.9323F, 62.33362F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Tên, quy cách nguyên vật liệu, hàng hoá";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbNam
            // 
            this.lbNam.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbNam.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNam.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbNam.LocationFloat = new DevExpress.Utils.PointFloat(555.3877F, 147.7105F);
            this.lbNam.Name = "lbNam";
            this.lbNam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNam.SizeF = new System.Drawing.SizeF(98.48871F, 23.00002F);
            this.lbNam.StylePriority.UseBorderDashStyle = false;
            this.lbNam.StylePriority.UseBorders = false;
            this.lbNam.StylePriority.UseFont = false;
            this.lbNam.StylePriority.UseTextAlignment = false;
            this.lbNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(501.4787F, 147.7105F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(53.909F, 23.00002F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Năm:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120.7101F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(1118.999F, 22.99998F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "BÁO CÁO QUYẾT TOÁN NGUYÊN LIỆU, VẬT TƯ, THÀNH PHẨM SẢN XUẤT TỪ NGUỒN NHẬP KHẨU";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbDiaChi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbDiaChi.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbDiaChi.LocationFloat = new DevExpress.Utils.PointFloat(98.65869F, 84.27054F);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbDiaChi.SizeF = new System.Drawing.SizeF(523.0219F, 23.00002F);
            this.lbDiaChi.StylePriority.UseBorderDashStyle = false;
            this.lbDiaChi.StylePriority.UseBorders = false;
            this.lbDiaChi.StylePriority.UseFont = false;
            this.lbDiaChi.StylePriority.UseTextAlignment = false;
            this.lbDiaChi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(788.5498F, 77.27052F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(152.9281F, 10.15753F);
            this.xrLabel13.StylePriority.UseBorderDashStyle = false;
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Italic);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(680.4841F, 61.27052F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(357.1233F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Độc lập - Tự do - Hạnh phúc";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbMaSo
            // 
            this.lbMaSo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbMaSo.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbMaSo.LocationFloat = new DevExpress.Utils.PointFloat(98.65866F, 61.27055F);
            this.lbMaSo.Name = "lbMaSo";
            this.lbMaSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbMaSo.SizeF = new System.Drawing.SizeF(523.0217F, 23F);
            this.lbMaSo.StylePriority.UseBorders = false;
            this.lbMaSo.StylePriority.UseFont = false;
            this.lbMaSo.StylePriority.UseTextAlignment = false;
            this.lbMaSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(680.4841F, 38.27054F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(357.1233F, 23F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTenToChuc
            // 
            this.lbTenToChuc.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbTenToChuc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTenToChuc.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbTenToChuc.LocationFloat = new DevExpress.Utils.PointFloat(188.8075F, 38.27052F);
            this.lbTenToChuc.Name = "lbTenToChuc";
            this.lbTenToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenToChuc.SizeF = new System.Drawing.SizeF(432.873F, 23F);
            this.lbTenToChuc.StylePriority.UseBorderDashStyle = false;
            this.lbTenToChuc.StylePriority.UseBorders = false;
            this.lbTenToChuc.StylePriority.UseFont = false;
            this.lbTenToChuc.StylePriority.UseTextAlignment = false;
            this.lbTenToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(24.06959F, 84.27053F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(74.58908F, 23.00001F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Địa chỉ:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(24.06959F, 61.27052F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(74.58907F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Mã số:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(24.0696F, 38.27052F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(164.7379F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên tổ chức/cá nhân:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(833.9862F, 6F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(203.0137F, 23F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mẫu số 15/BCQT/GSQL";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 63.70837F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(499.6234F, 23F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "(ký, ghi rõ họ tên, đóng dấu)";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 40.70842F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(499.6234F, 23.00001F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "ĐẠI DIỆN THEO PHÁP LUẬT CỦA TỔ CHỨC, CÁ NHÂN";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 40.70842F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(218.7566F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "(Ký, ghi rõ họ tên)";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 17.7084F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(499.6234F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Ngày…..tháng….năm….";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 17.7084F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(218.7566F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "NGƯỜI LẬP";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 23F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "Trang {0} / {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(1024.625F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(84.37494F, 23F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageHeader.HeightF = 25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell7,
            this.xrTableCell6,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell13});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "(1)";
            this.xrTableCell8.Weight = 0.13007104265082395;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "(2)";
            this.xrTableCell7.Weight = 0.16214372222642023;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "(3)";
            this.xrTableCell6.Weight = 0.61494346119491394;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "(4)";
            this.xrTableCell5.Weight = 0.51629313548825717;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "(8)";
            this.xrTableCell3.Weight = 0.38086628828813746;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "(9)";
            this.xrTableCell10.Weight = 0.3778756785504811;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "(10)";
            this.xrTableCell11.Weight = 0.39234627128633176;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "(11)";
            this.xrTableCell12.Weight = 0.33213073977738006;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "(12)";
            this.xrTableCell13.Weight = 0.40766913169044017;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader2,
            this.GroupHeader3});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupHeader1.HeightF = 25F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(3.973643E-05F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_STT_G_H,
            this.txt_SoTaiKhoan_G_H,
            this.lbl_TenHang_G_H,
            this.lbl_MaHang_G_H,
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // lbl_STT_G_H
            // 
            this.lbl_STT_G_H.Name = "lbl_STT_G_H";
            this.lbl_STT_G_H.Weight = 0.13007106528839757;
            this.lbl_STT_G_H.AfterPrint += new System.EventHandler(this.lbl_STT_G_H_AfterPrint);
            // 
            // txt_SoTaiKhoan_G_H
            // 
            this.txt_SoTaiKhoan_G_H.Name = "txt_SoTaiKhoan_G_H";
            this.txt_SoTaiKhoan_G_H.Weight = 0.16214374570053572;
            this.txt_SoTaiKhoan_G_H.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_SoTaiKhoan_G_H_BeforePrint);
            // 
            // lbl_TenHang_G_H
            // 
            this.lbl_TenHang_G_H.Name = "lbl_TenHang_G_H";
            this.lbl_TenHang_G_H.Weight = 0.61494334340717849;
            // 
            // lbl_MaHang_G_H
            // 
            this.lbl_MaHang_G_H.Name = "lbl_MaHang_G_H";
            this.lbl_MaHang_G_H.Weight = 0.51629335230823536;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.38086533616307128;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.37787643993135084;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.39234613975204824;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.33213095680182741;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 0.40766942839100057;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupFooter1.HeightF = 25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtSTT_G_F,
            this.txtSoTaiKhoan_G_F,
            this.xrTableCell17,
            this.txt_MaNPL_G_F,
            this.txtSoLuong_G_F,
            this.txtTriGiaNhapTrongKy,
            this.txtTriGiaXuatTrongKy_G_F,
            this.txtTriGiaTonCuoiKy_G_F,
            this.xrTableCell39});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // txtSTT_G_F
            // 
            this.txtSTT_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSTT_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSTT_G_F.Name = "txtSTT_G_F";
            this.txtSTT_G_F.StylePriority.UseBackColor = false;
            this.txtSTT_G_F.StylePriority.UseFont = false;
            this.txtSTT_G_F.Weight = 0.13007106528839757;
            this.txtSTT_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSTT_G_F_BeforePrint);
            // 
            // txtSoTaiKhoan_G_F
            // 
            this.txtSoTaiKhoan_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoTaiKhoan_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSoTaiKhoan_G_F.Name = "txtSoTaiKhoan_G_F";
            this.txtSoTaiKhoan_G_F.StylePriority.UseBackColor = false;
            this.txtSoTaiKhoan_G_F.StylePriority.UseFont = false;
            this.txtSoTaiKhoan_G_F.StylePriority.UseTextAlignment = false;
            this.txtSoTaiKhoan_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtSoTaiKhoan_G_F.Weight = 0.16214374570053572;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.SystemColors.Info;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Tổng :";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.61494338860182729;
            // 
            // txt_MaNPL_G_F
            // 
            this.txt_MaNPL_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_MaNPL_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txt_MaNPL_G_F.Name = "txt_MaNPL_G_F";
            this.txt_MaNPL_G_F.StylePriority.UseBackColor = false;
            this.txt_MaNPL_G_F.StylePriority.UseFont = false;
            this.txt_MaNPL_G_F.StylePriority.UseTextAlignment = false;
            this.txt_MaNPL_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt_MaNPL_G_F.Weight = 0.516293442697533;
            this.txt_MaNPL_G_F.AfterPrint += new System.EventHandler(this.txt_MaNPL_G_F_AfterPrint);
            this.txt_MaNPL_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_MaNPL_G_F_BeforePrint);
            // 
            // txtSoLuong_G_F
            // 
            this.txtSoLuong_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSoLuong_G_F.Name = "txtSoLuong_G_F";
            this.txtSoLuong_G_F.StylePriority.UseBackColor = false;
            this.txtSoLuong_G_F.StylePriority.UseFont = false;
            this.txtSoLuong_G_F.StylePriority.UseTextAlignment = false;
            this.txtSoLuong_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtSoLuong_G_F.Weight = 0.38086461304869013;
            this.txtSoLuong_G_F.AfterPrint += new System.EventHandler(this.txtSoLuong_G_F_AfterPrint);
            this.txtSoLuong_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuong_G_F_BeforePrint);
            // 
            // txtTriGiaNhapTrongKy
            // 
            this.txtTriGiaNhapTrongKy.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaNhapTrongKy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtTriGiaNhapTrongKy.Name = "txtTriGiaNhapTrongKy";
            this.txtTriGiaNhapTrongKy.StylePriority.UseBackColor = false;
            this.txtTriGiaNhapTrongKy.StylePriority.UseFont = false;
            this.txtTriGiaNhapTrongKy.StylePriority.UseTextAlignment = false;
            this.txtTriGiaNhapTrongKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtTriGiaNhapTrongKy.Weight = 0.37787734382432714;
            this.txtTriGiaNhapTrongKy.AfterPrint += new System.EventHandler(this.txtTriGiaNhapTrongKy_AfterPrint);
            this.txtTriGiaNhapTrongKy.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTriGiaNhapTrongKy_BeforePrint);
            // 
            // txtTriGiaXuatTrongKy_G_F
            // 
            this.txtTriGiaXuatTrongKy_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaXuatTrongKy_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtTriGiaXuatTrongKy_G_F.Name = "txtTriGiaXuatTrongKy_G_F";
            this.txtTriGiaXuatTrongKy_G_F.StylePriority.UseBackColor = false;
            this.txtTriGiaXuatTrongKy_G_F.StylePriority.UseFont = false;
            this.txtTriGiaXuatTrongKy_G_F.StylePriority.UseTextAlignment = false;
            this.txtTriGiaXuatTrongKy_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtTriGiaXuatTrongKy_G_F.Weight = 0.39234559741626251;
            this.txtTriGiaXuatTrongKy_G_F.AfterPrint += new System.EventHandler(this.txtTriGiaXuatTrongKy_G_F_AfterPrint);
            this.txtTriGiaXuatTrongKy_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTriGiaXuatTrongKy_G_F_BeforePrint);
            // 
            // txtTriGiaTonCuoiKy_G_F
            // 
            this.txtTriGiaTonCuoiKy_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaTonCuoiKy_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtTriGiaTonCuoiKy_G_F.Name = "txtTriGiaTonCuoiKy_G_F";
            this.txtTriGiaTonCuoiKy_G_F.StylePriority.UseBackColor = false;
            this.txtTriGiaTonCuoiKy_G_F.StylePriority.UseFont = false;
            this.txtTriGiaTonCuoiKy_G_F.StylePriority.UseTextAlignment = false;
            this.txtTriGiaTonCuoiKy_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtTriGiaTonCuoiKy_G_F.Weight = 0.33213131835901771;
            this.txtTriGiaTonCuoiKy_G_F.AfterPrint += new System.EventHandler(this.txtTriGiaTonCuoiKy_G_F_AfterPrint);
            this.txtTriGiaTonCuoiKy_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTriGiaTonCuoiKy_G_F_BeforePrint);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.BackColor = System.Drawing.SystemColors.Info;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBackColor = false;
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.Weight = 0.40766929280705416;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.GroupHeader2.HeightF = 25F;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.LightCyan;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable5.StylePriority.UseBackColor = false;
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.txtSoTaiKhoan_G_H_2,
            this.txtTenTaiKhoan,
            this.xrTableCell43,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseFont = false;
            this.xrTableCell14.Weight = 0.13007106528839757;
            // 
            // txtSoTaiKhoan_G_H_2
            // 
            this.txtSoTaiKhoan_G_H_2.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.txtSoTaiKhoan_G_H_2.Name = "txtSoTaiKhoan_G_H_2";
            this.txtSoTaiKhoan_G_H_2.StylePriority.UseFont = false;
            this.txtSoTaiKhoan_G_H_2.Weight = 0.16214374570053572;
            // 
            // txtTenTaiKhoan
            // 
            this.txtTenTaiKhoan.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.txtTenTaiKhoan.Name = "txtTenTaiKhoan";
            this.txtTenTaiKhoan.StylePriority.UseFont = false;
            this.txtTenTaiKhoan.StylePriority.UseTextAlignment = false;
            this.txtTenTaiKhoan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtTenTaiKhoan.Weight = 1.1312366617300009;
            this.txtTenTaiKhoan.AfterPrint += new System.EventHandler(this.txtTenTaiKhoan_AfterPrint);
            this.txtTenTaiKhoan.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtTenTaiKhoan_BeforePrint);
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.Weight = 0.3808661384575136;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseFont = false;
            this.xrTableCell45.Weight = 0.37787553603837504;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.Weight = 0.39234595897345292;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.Weight = 0.33213113758042262;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.Weight = 0.40766956397494697;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader3.HeightF = 25F;
            this.GroupHeader3.Level = 2;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.LightCyan;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable3.StylePriority.UseBackColor = false;
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtHopDong});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // txtHopDong
            // 
            this.txtHopDong.Font = new System.Drawing.Font("Times New Roman", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.txtHopDong.Name = "txtHopDong";
            this.txtHopDong.StylePriority.UseFont = false;
            this.txtHopDong.StylePriority.UseTextAlignment = false;
            this.txtHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtHopDong.Weight = 3.3143398077436457;
            this.txtHopDong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtHopDong_BeforePrint);
            // 
            // Mau15_TT38_Tong
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.PageFooter,
            this.PageHeader,
            this.DetailReport});
            this.DataMember = "t_KDT_GiayPhep";
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(22, 23, 10, 10);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "14.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lbTenToChuc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lbDiaChi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lbMaSo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lbNam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel lblT_NhapTrongKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblT_TonCuoiKy;
        private DevExpress.XtraReports.UI.XRLabel lblT_XuatTrongKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblTonDauKy_T;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell txtSTT;
        private DevExpress.XtraReports.UI.XRTableCell txtTaiKhoan;
        private DevExpress.XtraReports.UI.XRTableCell txtTenNPL;
        private DevExpress.XtraReports.UI.XRTableCell txtMaNPL;
        private DevExpress.XtraReports.UI.XRTableCell txtTonDauKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell txtNhapTrongKy;
        private DevExpress.XtraReports.UI.XRTableCell txtXuatTrongKy;
        private DevExpress.XtraReports.UI.XRTableCell txtTonCuoiKy;
        private DevExpress.XtraReports.UI.XRTableCell txtGhiChu;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lbl_STT_G_H;
        private DevExpress.XtraReports.UI.XRTableCell txt_SoTaiKhoan_G_H;
        private DevExpress.XtraReports.UI.XRTableCell lbl_TenHang_G_H;
        private DevExpress.XtraReports.UI.XRTableCell lbl_MaHang_G_H;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell txtSTT_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtSoTaiKhoan_G_F;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell txt_MaNPL_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuong_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtTriGiaNhapTrongKy;
        private DevExpress.XtraReports.UI.XRTableCell txtTriGiaXuatTrongKy_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtTriGiaTonCuoiKy_G_F;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell txtSoTaiKhoan_G_H_2;
        private DevExpress.XtraReports.UI.XRTableCell txtTenTaiKhoan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell txtHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblT_SoTien;
    }
}
