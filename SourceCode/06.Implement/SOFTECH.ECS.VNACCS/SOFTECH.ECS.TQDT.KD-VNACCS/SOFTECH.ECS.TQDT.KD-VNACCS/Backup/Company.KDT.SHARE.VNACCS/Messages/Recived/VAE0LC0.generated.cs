using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAE0LC0 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TNO { get; set; }
public PropertiesAttribute K07 { get; set; }
public PropertiesAttribute K03 { get; set; }
public PropertiesAttribute K05 { get; set; }
public PropertiesAttribute ADI { get; set; }
public PropertiesAttribute K01 { get; set; }
public PropertiesAttribute K08 { get; set; }
public PropertiesAttribute K09 { get; set; }
public PropertiesAttribute K10 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute RID { get; set; }
public PropertiesAttribute AAA { get; set; }
public PropertiesAttribute K16 { get; set; }
public PropertiesAttribute K17 { get; set; }
public PropertiesAttribute K18 { get; set; }
public PropertiesAttribute K19 { get; set; }
public PropertiesAttribute K23 { get; set; }
public PropertiesAttribute ADJ { get; set; }
public PropertiesAttribute ADK { get; set; }
public PropertiesAttribute K24 { get; set; }
public PropertiesAttribute K25 { get; set; }
public PropertiesAttribute K26 { get; set; }
public PropertiesAttribute K27 { get; set; }
public PropertiesAttribute K28 { get; set; }
public PropertiesAttribute K29 { get; set; }
public PropertiesAttribute K30 { get; set; }
public PropertiesAttribute K31 { get; set; }
public PropertiesAttribute K32 { get; set; }
public PropertiesAttribute K33 { get; set; }
public PropertiesAttribute K34 { get; set; }
public PropertiesAttribute K35 { get; set; }
public PropertiesAttribute K36 { get; set; }
public PropertiesAttribute K37 { get; set; }
public PropertiesAttribute K39 { get; set; }
public PropertiesAttribute K40 { get; set; }
public PropertiesAttribute K41 { get; set; }
public PropertiesAttribute K42 { get; set; }
public PropertiesAttribute K43 { get; set; }
public PropertiesAttribute K44 { get; set; }
public PropertiesAttribute K46 { get; set; }
public PropertiesAttribute K47 { get; set; }
public PropertiesAttribute K50 { get; set; }
public PropertiesAttribute K51 { get; set; }
public PropertiesAttribute K52 { get; set; }
public PropertiesAttribute K56 { get; set; }
public GroupAttribute A1 { get; set; }
public PropertiesAttribute K58 { get; set; }
public PropertiesAttribute K59 { get; set; }
public PropertiesAttribute K60 { get; set; }
public PropertiesAttribute IVD { get; set; }
public PropertiesAttribute IVP { get; set; }
public PropertiesAttribute K61 { get; set; }
public PropertiesAttribute K62 { get; set; }
public PropertiesAttribute K63 { get; set; }
public PropertiesAttribute K64 { get; set; }
public PropertiesAttribute K65 { get; set; }
public PropertiesAttribute K66 { get; set; }
public GroupAttribute RC1 { get; set; }
public PropertiesAttribute K67 { get; set; }
public PropertiesAttribute K68 { get; set; }
public PropertiesAttribute AD9 { get; set; }
public PropertiesAttribute TPM { get; set; }
public PropertiesAttribute PAY { get; set; }
public PropertiesAttribute ETA { get; set; }
public PropertiesAttribute ADA { get; set; }
public PropertiesAttribute TCO { get; set; }
public PropertiesAttribute SAM { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute K69 { get; set; }
public PropertiesAttribute K70 { get; set; }
public GroupAttribute EA1 { get; set; }
public PropertiesAttribute K79 { get; set; }
public PropertiesAttribute K81 { get; set; }
public PropertiesAttribute K82 { get; set; }
public PropertiesAttribute ADL { get; set; }
public PropertiesAttribute ADT { get; set; }
public PropertiesAttribute ADU { get; set; }
public PropertiesAttribute ADV { get; set; }
public PropertiesAttribute ADM { get; set; }
public PropertiesAttribute ADO { get; set; }
public PropertiesAttribute ADP { get; set; }
public PropertiesAttribute ADQ { get; set; }
public PropertiesAttribute ADR { get; set; }
public PropertiesAttribute CCD { get; set; }
public PropertiesAttribute AD4 { get; set; }
public PropertiesAttribute K86 { get; set; }
public PropertiesAttribute AD5 { get; set; }
public PropertiesAttribute K97 { get; set; }
public PropertiesAttribute AD6 { get; set; }
public PropertiesAttribute K98 { get; set; }
public PropertiesAttribute AD7 { get; set; }
public PropertiesAttribute K95 { get; set; }
public PropertiesAttribute AD8 { get; set; }
public PropertiesAttribute K96 { get; set; }
public PropertiesAttribute K88 { get; set; }
public GroupAttribute ST1 { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute K89 { get; set; }
public GroupAttribute E1 { get; set; }
public PropertiesAttribute K71 { get; set; }
public PropertiesAttribute K72 { get; set; }
public GroupAttribute C01 { get; set; }
public PropertiesAttribute CCM { get; set; }
public GroupAttribute D01 { get; set; }

public VAE0LC0()
        {
ECN = new PropertiesAttribute(12, typeof(decimal));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(decimal));
DNO = new PropertiesAttribute(2, typeof(decimal));
TNO = new PropertiesAttribute(12, typeof(decimal));
K07 = new PropertiesAttribute(3, typeof(string));
K03 = new PropertiesAttribute(3, typeof(string));
K05 = new PropertiesAttribute(1, typeof(string));
ADI = new PropertiesAttribute(1, typeof(string));
K01 = new PropertiesAttribute(4, typeof(string));
K08 = new PropertiesAttribute(10, typeof(string));
K09 = new PropertiesAttribute(2, typeof(string));
K10 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(decimal));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(decimal));
RID = new PropertiesAttribute(8, typeof(DateTime));
AAA = new PropertiesAttribute(1, typeof(string));
K16 = new PropertiesAttribute(13, typeof(string));
K17 = new PropertiesAttribute(300, typeof(string));
K18 = new PropertiesAttribute(7, typeof(string));
K19 = new PropertiesAttribute(300, typeof(string));
K23 = new PropertiesAttribute(20, typeof(string));
ADJ = new PropertiesAttribute(13, typeof(string));
ADK = new PropertiesAttribute(300, typeof(string));
K24 = new PropertiesAttribute(13, typeof(string));
K25 = new PropertiesAttribute(70, typeof(string));
K26 = new PropertiesAttribute(9, typeof(string));
K27 = new PropertiesAttribute(35, typeof(string));
K28 = new PropertiesAttribute(35, typeof(string));
K29 = new PropertiesAttribute(35, typeof(string));
K30 = new PropertiesAttribute(35, typeof(string));
K31 = new PropertiesAttribute(2, typeof(string));
K32 = new PropertiesAttribute(5, typeof(string));
K33 = new PropertiesAttribute(50, typeof(string));
K34 = new PropertiesAttribute(5, typeof(string));
K35 = new PropertiesAttribute(35, typeof(string));
K36 = new PropertiesAttribute(8, typeof(decimal));
K37 = new PropertiesAttribute(3, typeof(string));
K39 = new PropertiesAttribute(10, typeof(decimal));
K40 = new PropertiesAttribute(3, typeof(string));
K41 = new PropertiesAttribute(7, typeof(string));
K42 = new PropertiesAttribute(20, typeof(string));
K43 = new PropertiesAttribute(5, typeof(string));
K44 = new PropertiesAttribute(35, typeof(string));
K46 = new PropertiesAttribute(6, typeof(string));
K47 = new PropertiesAttribute(35, typeof(string));
K50 = new PropertiesAttribute(9, typeof(string));
K51 = new PropertiesAttribute(35, typeof(string));
K52 = new PropertiesAttribute(8, typeof(DateTime));
K56 = new PropertiesAttribute(140, typeof(string));
K58 = new PropertiesAttribute(1, typeof(string));
K59 = new PropertiesAttribute(35, typeof(string));
K60 = new PropertiesAttribute(12, typeof(decimal));
IVD = new PropertiesAttribute(8, typeof(DateTime));
IVP = new PropertiesAttribute(7, typeof(string));
K61 = new PropertiesAttribute(3, typeof(string));
K62 = new PropertiesAttribute(3, typeof(string));
K63 = new PropertiesAttribute(20, typeof(decimal));
K64 = new PropertiesAttribute(1, typeof(string));
K65 = new PropertiesAttribute(3, typeof(string));
K66 = new PropertiesAttribute(20, typeof(decimal));
K67 = new PropertiesAttribute(20, typeof(decimal));
K68 = new PropertiesAttribute(1, typeof(string));
AD9 = new PropertiesAttribute(1, typeof(string));
TPM = new PropertiesAttribute(1, typeof(string));
PAY = new PropertiesAttribute(1, typeof(string));
ETA = new PropertiesAttribute(11, typeof(decimal));
ADA = new PropertiesAttribute(3, typeof(string));
TCO = new PropertiesAttribute(11, typeof(decimal));
SAM = new PropertiesAttribute(11, typeof(decimal));
ADB = new PropertiesAttribute(3, typeof(string));
K69 = new PropertiesAttribute(2, typeof(decimal));
K70 = new PropertiesAttribute(2, typeof(decimal));
K79 = new PropertiesAttribute(300, typeof(string));
K81 = new PropertiesAttribute(20, typeof(string));
K82 = new PropertiesAttribute(5, typeof(string));
ADL = new PropertiesAttribute(11, typeof(string));
ADT = new PropertiesAttribute(4, typeof(decimal));
ADU = new PropertiesAttribute(10, typeof(string));
ADV = new PropertiesAttribute(10, typeof(string));
ADM = new PropertiesAttribute(1, typeof(string));
ADO = new PropertiesAttribute(11, typeof(string));
ADP = new PropertiesAttribute(4, typeof(decimal));
ADQ = new PropertiesAttribute(10, typeof(string));
ADR = new PropertiesAttribute(10, typeof(string));
CCD = new PropertiesAttribute(8, typeof(DateTime));
AD4 = new PropertiesAttribute(6, typeof(decimal));
K86 = new PropertiesAttribute(8, typeof(DateTime));
AD5 = new PropertiesAttribute(6, typeof(decimal));
K97 = new PropertiesAttribute(8, typeof(DateTime));
AD6 = new PropertiesAttribute(6, typeof(decimal));
K98 = new PropertiesAttribute(8, typeof(DateTime));
AD7 = new PropertiesAttribute(6, typeof(decimal));
K95 = new PropertiesAttribute(8, typeof(DateTime));
AD8 = new PropertiesAttribute(6, typeof(decimal));
K96 = new PropertiesAttribute(5, typeof(string));
K88 = new PropertiesAttribute(8, typeof(DateTime));
ARR = new PropertiesAttribute(7, typeof(string));
K89 = new PropertiesAttribute(8, typeof(DateTime));
K71 = new PropertiesAttribute(70, typeof(string));
K72 = new PropertiesAttribute(300, typeof(string));
CCM = new PropertiesAttribute(1, typeof(string));
#region A1
List<PropertiesAttribute> listA1 = new List<PropertiesAttribute>();
listA1.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAE0LC0_A1, typeof(string)));
listA1.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAE0LC0_B1, typeof(string)));
A1 = new GroupAttribute("A1", 5, listA1);
#endregion A1
#region RC1
List<PropertiesAttribute> listRC1 = new List<PropertiesAttribute>();
listRC1.Add(new PropertiesAttribute(3, 2, EnumGroupID.VAE0LC0_RC1, typeof(string)));
listRC1.Add(new PropertiesAttribute(9, 2, EnumGroupID.VAE0LC0_RD1, typeof(int)));
RC1 = new GroupAttribute("RC1", 2, listRC1);
#endregion RC1
#region EA1
List<PropertiesAttribute> listEA1 = new List<PropertiesAttribute>();
listEA1.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAE0LC0_EA1, typeof(string)));
listEA1.Add(new PropertiesAttribute(12, 3, EnumGroupID.VAE0LC0_EN1, typeof(int)));
EA1 = new GroupAttribute("EA1", 3, listEA1);
#endregion EA1
#region ST1
List<PropertiesAttribute> listST1 = new List<PropertiesAttribute>();
listST1.Add(new PropertiesAttribute(7, 3, EnumGroupID.VAE0LC0_ST1, typeof(string)));
listST1.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE0LC0_BD1, typeof(DateTime)));
listST1.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE0LC0_CD1, typeof(DateTime)));
ST1 = new GroupAttribute("ST1", 3, listST1);
#endregion ST1
#region E1
List<PropertiesAttribute> listE1 = new List<PropertiesAttribute>();
listE1.Add(new PropertiesAttribute(7, 5, EnumGroupID.VAE0LC0_E1, typeof(string)));
E1 = new GroupAttribute("E1", 5, listE1);
#endregion E1
#region C01
List<PropertiesAttribute> listC01 = new List<PropertiesAttribute>();
listC01.Add(new PropertiesAttribute(12, 50, EnumGroupID.VAE0LC0_C01, typeof(string)));
C01 = new GroupAttribute("C01", 50, listC01);
#endregion C01
#region D01
List<PropertiesAttribute> listD01 = new List<PropertiesAttribute>();
listD01.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAE0LC0_D01, typeof(DateTime)));
listD01.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAE0LC0_T01, typeof(string)));
listD01.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAE0LC0_I01, typeof(string)));
D01 = new GroupAttribute("D01", 10, listD01);
#endregion D01
TongSoByte = 9559;
}

}public partial class EnumGroupID
    {
public static readonly string VAE0LC0_A1 = "VAE0LC0_A1";
public static readonly string VAE0LC0_B1 = "VAE0LC0_B1";
public static readonly string VAE0LC0_RC1 = "VAE0LC0_RC1";
public static readonly string VAE0LC0_RD1 = "VAE0LC0_RD1";
public static readonly string VAE0LC0_EA1 = "VAE0LC0_EA1";
public static readonly string VAE0LC0_EN1 = "VAE0LC0_EN1";
public static readonly string VAE0LC0_ST1 = "VAE0LC0_ST1";
public static readonly string VAE0LC0_BD1 = "VAE0LC0_BD1";
public static readonly string VAE0LC0_CD1 = "VAE0LC0_CD1";
public static readonly string VAE0LC0_E1 = "VAE0LC0_E1";
public static readonly string VAE0LC0_C01 = "VAE0LC0_C01";
public static readonly string VAE0LC0_D01 = "VAE0LC0_D01";
public static readonly string VAE0LC0_T01 = "VAE0LC0_T01";
public static readonly string VAE0LC0_I01 = "VAE0LC0_I01";
}

}
