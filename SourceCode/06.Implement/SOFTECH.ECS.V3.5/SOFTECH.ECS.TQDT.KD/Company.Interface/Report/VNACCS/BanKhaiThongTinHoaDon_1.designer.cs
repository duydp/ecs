namespace Company.Interface.Report.VNACCS
{
    partial class BanKhaiThongTinHoaDon_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblTongSoKienHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrongLuongThuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhTrongLuongThuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhTongSoKienHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhTongTheTich = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTheTich = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhTongTrongLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTrongLuongHangGross = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGhiChuCuaChuHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPL = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNganHangLC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaFOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTCuaFOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienDieuChinhFOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTSoTienDieuChinhFOB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTPhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoiThanhToanPhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiPhiXepHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTChiPhiXepHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiChiPhiXepHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiPhiXepHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTChiPhiXepHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiChiPhiXepHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiVanChuyenDuongBoNoiDia = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTPhiVanChuyenDuongBoNoiDia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTCuaPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienDieuChinhPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTSoTienDieuChinhPhiBH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienKhauTru = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTCuaSoTienKhauTru = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiKhauTru = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienDieuChinhKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTSoTienDieuChinhKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiSoTienDieuChinhKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTTongTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKienGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemGiaoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChuDacBiet = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoDongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuocXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoKienHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSoLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSoLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonViDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTGiaTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMTTSoTienKhauTru_Hang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTongSoKienHang,
            this.lblTrongLuongThuan,
            this.lblMaDonViTinhTrongLuongThuan,
            this.lblMaDonViTinhTongSoKienHang,
            this.lblMaDonViTinhTongTheTich,
            this.lblTongTheTich,
            this.lblMaDonViTinhTongTrongLuong,
            this.xrLabel3,
            this.lblTongTrongLuongHangGross,
            this.xrLabel6,
            this.xrLabel9,
            this.lblGhiChuCuaChuHang,
            this.xrLabel12,
            this.xrLabel14,
            this.lblSoPL,
            this.xrLabel16});
            this.Detail.HeightF = 57.36892F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoKienHang
            // 
            this.lblTongSoKienHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoKienHang.LocationFloat = new DevExpress.Utils.PointFloat(580.0905F, 12.00001F);
            this.lblTongSoKienHang.Name = "lblTongSoKienHang";
            this.lblTongSoKienHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoKienHang.SizeF = new System.Drawing.SizeF(91.74989F, 11F);
            this.lblTongSoKienHang.StylePriority.UseFont = false;
            this.lblTongSoKienHang.StylePriority.UseTextAlignment = false;
            this.lblTongSoKienHang.Text = "NNNNNNNNNE";
            this.lblTongSoKienHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoKienHang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTrongLuongThuan
            // 
            this.lblTrongLuongThuan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTrongLuongThuan.LocationFloat = new DevExpress.Utils.PointFloat(580.0905F, 0F);
            this.lblTrongLuongThuan.Name = "lblTrongLuongThuan";
            this.lblTrongLuongThuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrongLuongThuan.SizeF = new System.Drawing.SizeF(91.74989F, 11F);
            this.lblTrongLuongThuan.StylePriority.UseFont = false;
            this.lblTrongLuongThuan.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongThuan.Text = "NNNNNNNNNE";
            this.lblTrongLuongThuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTrongLuongThuan.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDonViTinhTrongLuongThuan
            // 
            this.lblMaDonViTinhTrongLuongThuan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhTrongLuongThuan.LocationFloat = new DevExpress.Utils.PointFloat(671.8404F, 0F);
            this.lblMaDonViTinhTrongLuongThuan.Name = "lblMaDonViTinhTrongLuongThuan";
            this.lblMaDonViTinhTrongLuongThuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhTrongLuongThuan.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblMaDonViTinhTrongLuongThuan.StylePriority.UseFont = false;
            this.lblMaDonViTinhTrongLuongThuan.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhTrongLuongThuan.Text = "XXE";
            this.lblMaDonViTinhTrongLuongThuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhTongSoKienHang
            // 
            this.lblMaDonViTinhTongSoKienHang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhTongSoKienHang.LocationFloat = new DevExpress.Utils.PointFloat(671.8404F, 12.00001F);
            this.lblMaDonViTinhTongSoKienHang.Name = "lblMaDonViTinhTongSoKienHang";
            this.lblMaDonViTinhTongSoKienHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhTongSoKienHang.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblMaDonViTinhTongSoKienHang.StylePriority.UseFont = false;
            this.lblMaDonViTinhTongSoKienHang.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhTongSoKienHang.Text = "XXE";
            this.lblMaDonViTinhTongSoKienHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhTongTheTich
            // 
            this.lblMaDonViTinhTongTheTich.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhTongTheTich.LocationFloat = new DevExpress.Utils.PointFloat(265.2899F, 12F);
            this.lblMaDonViTinhTongTheTich.Name = "lblMaDonViTinhTongTheTich";
            this.lblMaDonViTinhTongTheTich.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhTongTheTich.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblMaDonViTinhTongTheTich.StylePriority.UseFont = false;
            this.lblMaDonViTinhTongTheTich.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhTongTheTich.Text = "XXE";
            this.lblMaDonViTinhTongTheTich.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongTheTich
            // 
            this.lblTongTheTich.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTheTich.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 12F);
            this.lblTongTheTich.Name = "lblTongTheTich";
            this.lblTongTheTich.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTheTich.SizeF = new System.Drawing.SizeF(91.74989F, 11F);
            this.lblTongTheTich.StylePriority.UseFont = false;
            this.lblTongTheTich.StylePriority.UseTextAlignment = false;
            this.lblTongTheTich.Text = "NNNNNNNNNE";
            this.lblTongTheTich.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongTheTich.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDonViTinhTongTrongLuong
            // 
            this.lblMaDonViTinhTongTrongLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhTongTrongLuong.LocationFloat = new DevExpress.Utils.PointFloat(265.2899F, 0F);
            this.lblMaDonViTinhTongTrongLuong.Name = "lblMaDonViTinhTongTrongLuong";
            this.lblMaDonViTinhTongTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhTongTrongLuong.SizeF = new System.Drawing.SizeF(37.49994F, 11F);
            this.lblMaDonViTinhTongTrongLuong.StylePriority.UseFont = false;
            this.lblMaDonViTinhTongTrongLuong.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhTongTrongLuong.Text = "XXE";
            this.lblMaDonViTinhTongTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(163.54F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Tổng trọng lượng hàng (Goss)";
            // 
            // lblTongTrongLuongHangGross
            // 
            this.lblTongTrongLuongHangGross.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTrongLuongHangGross.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 0F);
            this.lblTongTrongLuongHangGross.Name = "lblTongTrongLuongHangGross";
            this.lblTongTrongLuongHangGross.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTrongLuongHangGross.SizeF = new System.Drawing.SizeF(91.74989F, 11F);
            this.lblTongTrongLuongHangGross.StylePriority.UseFont = false;
            this.lblTongTrongLuongHangGross.StylePriority.UseTextAlignment = false;
            this.lblTongTrongLuongHangGross.Text = "NNNNNNNNNE";
            this.lblTongTrongLuongHangGross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongTrongLuongHangGross.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 12F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(151.54F, 11F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Tổng thể tích";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(451.4115F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(126.7917F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Trọng lượng thuần";
            // 
            // lblGhiChuCuaChuHang
            // 
            this.lblGhiChuCuaChuHang.Font = new System.Drawing.Font("Times New Roman", 6.89F);
            this.lblGhiChuCuaChuHang.LocationFloat = new DevExpress.Utils.PointFloat(148.0183F, 23.99999F);
            this.lblGhiChuCuaChuHang.Multiline = true;
            this.lblGhiChuCuaChuHang.Name = "lblGhiChuCuaChuHang";
            this.lblGhiChuCuaChuHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChuCuaChuHang.SizeF = new System.Drawing.SizeF(611.9817F, 11F);
            this.lblGhiChuCuaChuHang.StylePriority.UseFont = false;
            this.lblGhiChuCuaChuHang.StylePriority.UseTextAlignment = false;
            this.lblGhiChuCuaChuHang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblGhiChuCuaChuHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(9.999997F, 23.99999F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(115.4823F, 10.99999F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "Ghi chú của chủ hàng";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(451.4115F, 12F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(126.7917F, 11F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Tổng số kiện hàng";
            // 
            // lblSoPL
            // 
            this.lblSoPL.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoPL.LocationFloat = new DevExpress.Utils.PointFloat(173.54F, 36.04171F);
            this.lblSoPL.Name = "lblSoPL";
            this.lblSoPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPL.SizeF = new System.Drawing.SizeF(117.2091F, 11.00001F);
            this.lblSoPL.StylePriority.UseFont = false;
            this.lblSoPL.StylePriority.UseTextAlignment = false;
            this.lblSoPL.Text = "XXXXXXXXXE";
            this.lblSoPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 36.0417F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(151.54F, 11.00001F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Số P/L";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.TopMargin.HeightF = 95F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(225F, 62.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(312.4916F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Bản khai thông tin hóa đơn / kiện hàng";
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 48.50381F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLine1});
            this.Detail1.HeightF = 200F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 10F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow9,
            this.xrTableRow3,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow1,
            this.xrTableRow12,
            this.xrTableRow11,
            this.xrTableRow10,
            this.xrTableRow13});
            this.xrTable1.SizeF = new System.Drawing.SizeF(750.0001F, 175.3817F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.lblNganHangLC});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "Ngân hàng L/C";
            this.xrTableCell10.Weight = 0.46192907516039428;
            // 
            // lblNganHangLC
            // 
            this.lblNganHangLC.Name = "lblNganHangLC";
            this.lblNganHangLC.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXXE";
            this.lblNganHangLC.Weight = 2.5380709248396056;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.lblTriGiaFOB,
            this.lblMTTCuaFOB,
            this.xrTableCell68,
            this.xrTableCell70,
            this.lblSoTienDieuChinhFOB,
            this.lblMTTSoTienDieuChinhFOB});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Trị giá FOB";
            this.xrTableCell11.Weight = 0.46192907516039428;
            // 
            // lblTriGiaFOB
            // 
            this.lblTriGiaFOB.Name = "lblTriGiaFOB";
            this.lblTriGiaFOB.StylePriority.UseTextAlignment = false;
            this.lblTriGiaFOB.Text = "NNNNNNNNN1NNNNNNNE";
            this.lblTriGiaFOB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaFOB.Weight = 0.70923006008056144;
            this.lblTriGiaFOB.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTCuaFOB
            // 
            this.lblMTTCuaFOB.Name = "lblMTTCuaFOB";
            this.lblMTTCuaFOB.StylePriority.UseTextAlignment = false;
            this.lblMTTCuaFOB.Text = "XXE";
            this.lblMTTCuaFOB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTCuaFOB.Weight = 0.20552885798918802;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.17739854259045065;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Text = "Số tiền điều chỉnh FOB";
            this.xrTableCell70.Weight = 0.62500019073484769;
            // 
            // lblSoTienDieuChinhFOB
            // 
            this.lblSoTienDieuChinhFOB.Name = "lblSoTienDieuChinhFOB";
            this.lblSoTienDieuChinhFOB.Text = "NNNNNNNNN1NNE";
            this.lblSoTienDieuChinhFOB.Weight = 0.46827436578170856;
            this.lblSoTienDieuChinhFOB.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTSoTienDieuChinhFOB
            // 
            this.lblMTTSoTienDieuChinhFOB.Name = "lblMTTSoTienDieuChinhFOB";
            this.lblMTTSoTienDieuChinhFOB.Text = "XXE";
            this.lblMTTSoTienDieuChinhFOB.Weight = 0.35263890766284933;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.lblPhiVanChuyen,
            this.lblMTTPhiVanChuyen,
            this.xrTableCell20,
            this.xrTableCell22,
            this.lblNoiThanhToanPhiVanChuyen});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Phí vận chuyển";
            this.xrTableCell12.Weight = 0.46192907516039428;
            // 
            // lblPhiVanChuyen
            // 
            this.lblPhiVanChuyen.Name = "lblPhiVanChuyen";
            this.lblPhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblPhiVanChuyen.Text = "NNNNNNNNN1NNNNNNE";
            this.lblPhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblPhiVanChuyen.Weight = 0.70923006008056144;
            this.lblPhiVanChuyen.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTPhiVanChuyen
            // 
            this.lblMTTPhiVanChuyen.Name = "lblMTTPhiVanChuyen";
            this.lblMTTPhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMTTPhiVanChuyen.Text = "XXE";
            this.lblMTTPhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTPhiVanChuyen.Weight = 0.20552885798918802;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.17739854259045065;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "Nơi thanh toán phí vận chuyển";
            this.xrTableCell22.Weight = 0.62500019073484769;
            // 
            // lblNoiThanhToanPhiVanChuyen
            // 
            this.lblNoiThanhToanPhiVanChuyen.Name = "lblNoiThanhToanPhiVanChuyen";
            this.lblNoiThanhToanPhiVanChuyen.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblNoiThanhToanPhiVanChuyen.Weight = 0.82091327344455789;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.lblChiPhiXepHang1,
            this.lblMTTChiPhiXepHang1,
            this.xrTableCell60,
            this.xrTableCell62,
            this.lblLoaiChiPhiXepHang1});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Chị phí xếp hàng 1";
            this.xrTableCell13.Weight = 0.46192907516039428;
            // 
            // lblChiPhiXepHang1
            // 
            this.lblChiPhiXepHang1.Name = "lblChiPhiXepHang1";
            this.lblChiPhiXepHang1.StylePriority.UseTextAlignment = false;
            this.lblChiPhiXepHang1.Text = "NNNNNNNNNE";
            this.lblChiPhiXepHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblChiPhiXepHang1.Weight = 0.70923006008056144;
            this.lblChiPhiXepHang1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTChiPhiXepHang1
            // 
            this.lblMTTChiPhiXepHang1.Name = "lblMTTChiPhiXepHang1";
            this.lblMTTChiPhiXepHang1.StylePriority.UseTextAlignment = false;
            this.lblMTTChiPhiXepHang1.Text = "XXE";
            this.lblMTTChiPhiXepHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTChiPhiXepHang1.Weight = 0.20552885798918802;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.17739854259045065;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "Loại chi phí xếp hàng 1";
            this.xrTableCell62.Weight = 0.62500019073484769;
            // 
            // lblLoaiChiPhiXepHang1
            // 
            this.lblLoaiChiPhiXepHang1.Name = "lblLoaiChiPhiXepHang1";
            this.lblLoaiChiPhiXepHang1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiChiPhiXepHang1.Weight = 0.82091327344455789;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.lblChiPhiXepHang2,
            this.lblMTTChiPhiXepHang2,
            this.xrTableCell52,
            this.xrTableCell54,
            this.lblLoaiChiPhiXepHang2});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Chị phí xếp hàng 2";
            this.xrTableCell14.Weight = 0.46192907516039428;
            // 
            // lblChiPhiXepHang2
            // 
            this.lblChiPhiXepHang2.Name = "lblChiPhiXepHang2";
            this.lblChiPhiXepHang2.StylePriority.UseTextAlignment = false;
            this.lblChiPhiXepHang2.Text = "NNNNNNNNNE";
            this.lblChiPhiXepHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblChiPhiXepHang2.Weight = 0.70923006008056144;
            this.lblChiPhiXepHang2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTChiPhiXepHang2
            // 
            this.lblMTTChiPhiXepHang2.Name = "lblMTTChiPhiXepHang2";
            this.lblMTTChiPhiXepHang2.StylePriority.UseTextAlignment = false;
            this.lblMTTChiPhiXepHang2.Text = "XXE";
            this.lblMTTChiPhiXepHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTChiPhiXepHang2.Weight = 0.20552885798918802;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.17739854259045065;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Loại chi phí xếp hàng 2";
            this.xrTableCell54.Weight = 0.62500019073484769;
            // 
            // lblLoaiChiPhiXepHang2
            // 
            this.lblLoaiChiPhiXepHang2.Name = "lblLoaiChiPhiXepHang2";
            this.lblLoaiChiPhiXepHang2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiChiPhiXepHang2.Weight = 0.82091327344455789;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.lblPhiVanChuyenDuongBoNoiDia,
            this.lblMTTPhiVanChuyenDuongBoNoiDia,
            this.xrTableCell44,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "Phí vận chuyển đường bộ nội địa";
            this.xrTableCell15.Weight = 0.60456734378520971;
            // 
            // lblPhiVanChuyenDuongBoNoiDia
            // 
            this.lblPhiVanChuyenDuongBoNoiDia.Name = "lblPhiVanChuyenDuongBoNoiDia";
            this.lblPhiVanChuyenDuongBoNoiDia.StylePriority.UseTextAlignment = false;
            this.lblPhiVanChuyenDuongBoNoiDia.Text = "NNNNNNNNNE";
            this.lblPhiVanChuyenDuongBoNoiDia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblPhiVanChuyenDuongBoNoiDia.Weight = 0.56659191352604876;
            this.lblPhiVanChuyenDuongBoNoiDia.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTPhiVanChuyenDuongBoNoiDia
            // 
            this.lblMTTPhiVanChuyenDuongBoNoiDia.Name = "lblMTTPhiVanChuyenDuongBoNoiDia";
            this.lblMTTPhiVanChuyenDuongBoNoiDia.StylePriority.UseTextAlignment = false;
            this.lblMTTPhiVanChuyenDuongBoNoiDia.Text = "XXE";
            this.lblMTTPhiVanChuyenDuongBoNoiDia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTPhiVanChuyenDuongBoNoiDia.Weight = 0.2055287359188854;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 0.1773985425904506;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 0.62500019073484792;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 0.46827460992231373;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.35263866352224416;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.lblPhiBaoHiem,
            this.lblMTTCuaPhiBaoHiem,
            this.xrTableCell36,
            this.xrTableCell38,
            this.lblSoTienDieuChinhPhiBaoHiem,
            this.lblMTTSoTienDieuChinhPhiBH});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "Phí bảo hiểm";
            this.xrTableCell41.Weight = 0.46192907516039428;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblPhiBaoHiem.Text = "NNNNNNNNN1NNNE";
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblPhiBaoHiem.Weight = 0.70923006008056144;
            this.lblPhiBaoHiem.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTCuaPhiBaoHiem
            // 
            this.lblMTTCuaPhiBaoHiem.Name = "lblMTTCuaPhiBaoHiem";
            this.lblMTTCuaPhiBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblMTTCuaPhiBaoHiem.Text = "XXE";
            this.lblMTTCuaPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTCuaPhiBaoHiem.Weight = 0.20552885798918802;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.17739854259045065;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Số tiền điều chỉnh phí bảo hiểm";
            this.xrTableCell38.Weight = 0.62500019073484769;
            // 
            // lblSoTienDieuChinhPhiBaoHiem
            // 
            this.lblSoTienDieuChinhPhiBaoHiem.Name = "lblSoTienDieuChinhPhiBaoHiem";
            this.lblSoTienDieuChinhPhiBaoHiem.Text = "NNNNNNNNE";
            this.lblSoTienDieuChinhPhiBaoHiem.Weight = 0.46827474725140406;
            this.lblSoTienDieuChinhPhiBaoHiem.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTSoTienDieuChinhPhiBH
            // 
            this.lblMTTSoTienDieuChinhPhiBH.Name = "lblMTTSoTienDieuChinhPhiBH";
            this.lblMTTSoTienDieuChinhPhiBH.Text = "XXE";
            this.lblMTTSoTienDieuChinhPhiBH.Weight = 0.35263852619315383;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.lblSoTienKhauTru,
            this.lblMTTCuaSoTienKhauTru,
            this.xrTableCell28,
            this.xrTableCell30,
            this.lblLoaiKhauTru});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "Số tiền khấu trừ";
            this.xrTableCell73.Weight = 0.46192907516039428;
            // 
            // lblSoTienKhauTru
            // 
            this.lblSoTienKhauTru.Name = "lblSoTienKhauTru";
            this.lblSoTienKhauTru.StylePriority.UseTextAlignment = false;
            this.lblSoTienKhauTru.Text = "NNNNNNNNNE";
            this.lblSoTienKhauTru.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienKhauTru.Weight = 0.70923006008056144;
            this.lblSoTienKhauTru.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTCuaSoTienKhauTru
            // 
            this.lblMTTCuaSoTienKhauTru.Name = "lblMTTCuaSoTienKhauTru";
            this.lblMTTCuaSoTienKhauTru.StylePriority.UseTextAlignment = false;
            this.lblMTTCuaSoTienKhauTru.Text = "XXE";
            this.lblMTTCuaSoTienKhauTru.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTCuaSoTienKhauTru.Weight = 0.20552885798918802;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.17739854259045065;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Loại khấu trừ";
            this.xrTableCell30.Weight = 0.62500019073484769;
            // 
            // lblLoaiKhauTru
            // 
            this.lblLoaiKhauTru.Name = "lblLoaiKhauTru";
            this.lblLoaiKhauTru.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiKhauTru.Weight = 0.82091327344455789;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.lblSoTienDieuChinhKhac,
            this.lblMTTSoTienDieuChinhKhac,
            this.xrTableCell5,
            this.xrTableCell6,
            this.lblLoaiSoTienDieuChinhKhac});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "Số điều chỉnh khác";
            this.xrTableCell74.Weight = 0.46192907516039428;
            // 
            // lblSoTienDieuChinhKhac
            // 
            this.lblSoTienDieuChinhKhac.Name = "lblSoTienDieuChinhKhac";
            this.lblSoTienDieuChinhKhac.StylePriority.UseTextAlignment = false;
            this.lblSoTienDieuChinhKhac.Text = "NNNNNNNNNE";
            this.lblSoTienDieuChinhKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienDieuChinhKhac.Weight = 0.70923006008056144;
            this.lblSoTienDieuChinhKhac.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTSoTienDieuChinhKhac
            // 
            this.lblMTTSoTienDieuChinhKhac.Name = "lblMTTSoTienDieuChinhKhac";
            this.lblMTTSoTienDieuChinhKhac.StylePriority.UseTextAlignment = false;
            this.lblMTTSoTienDieuChinhKhac.Text = "XXE";
            this.lblMTTSoTienDieuChinhKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTSoTienDieuChinhKhac.Weight = 0.20552885798918802;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 0.17739854259045065;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Số tiền điều chỉnh khác";
            this.xrTableCell6.Weight = 0.62500019073484769;
            // 
            // lblLoaiSoTienDieuChinhKhac
            // 
            this.lblLoaiSoTienDieuChinhKhac.Name = "lblLoaiSoTienDieuChinhKhac";
            this.lblLoaiSoTienDieuChinhKhac.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiSoTienDieuChinhKhac.Weight = 0.82091327344455789;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.lblTongTriGiaHoaDon,
            this.lblMTTTongTriGiaHoaDon,
            this.xrTableCell78,
            this.xrTableCell80,
            this.lblDieuKienGiaHoaDon});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "Tổng trị giá hóa đơn";
            this.xrTableCell75.Weight = 0.46192907516039428;
            // 
            // lblTongTriGiaHoaDon
            // 
            this.lblTongTriGiaHoaDon.Name = "lblTongTriGiaHoaDon";
            this.lblTongTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaHoaDon.Text = "NNNNNNNNN1NNNNNNNE";
            this.lblTongTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaHoaDon.Weight = 0.70923006008056144;
            this.lblTongTriGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTTongTriGiaHoaDon
            // 
            this.lblMTTTongTriGiaHoaDon.Name = "lblMTTTongTriGiaHoaDon";
            this.lblMTTTongTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblMTTTongTriGiaHoaDon.Text = "XXE";
            this.lblMTTTongTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMTTTongTriGiaHoaDon.Weight = 0.20552885798918802;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.17739854259045065;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Text = "Điều kiện giá hóa đơn";
            this.xrTableCell80.Weight = 0.62500019073484769;
            // 
            // lblDieuKienGiaHoaDon
            // 
            this.lblDieuKienGiaHoaDon.Name = "lblDieuKienGiaHoaDon";
            this.lblDieuKienGiaHoaDon.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblDieuKienGiaHoaDon.Weight = 0.82091327344455789;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.lblDiaDiemGiaoHang,
            this.xrTableCell63,
            this.xrTableCell65});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "Địa điểm giao hàng";
            this.xrTableCell33.Weight = 0.46192907516039428;
            // 
            // lblDiaDiemGiaoHang
            // 
            this.lblDiaDiemGiaoHang.Name = "lblDiaDiemGiaoHang";
            this.lblDiaDiemGiaoHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblDiaDiemGiaoHang.Weight = 1.0921574606602003;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.62500019073484769;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.82091327344455789;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.lblGhiChuDacBiet,
            this.xrTableCell31});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 2.1650482990654774;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Ghi chú đặc biệt";
            this.xrTableCell4.Weight = 0.46192907516039428;
            // 
            // lblGhiChuDacBiet
            // 
            this.lblGhiChuDacBiet.Name = "lblGhiChuDacBiet";
            this.lblGhiChuDacBiet.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblGhiChuDacBiet.Weight = 2.0164363135631258;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.52163461127648036;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.lblTongSoDongHang,
            this.xrTableCell23,
            this.xrTableCell29,
            this.xrTableCell37,
            this.xrTableCell45});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.0017061633976481;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Tổng số dòng hàng";
            this.xrTableCell2.Weight = 0.46192907516039428;
            // 
            // lblTongSoDongHang
            // 
            this.lblTongSoDongHang.Name = "lblTongSoDongHang";
            this.lblTongSoDongHang.Text = "NNNE";
            this.lblTongSoDongHang.Weight = 0.70923006008056144;
            this.lblTongSoDongHang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.20552885798918802;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.17739854259045065;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.62500019073484769;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.82091327344455789;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.1971355F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(770F, 2.00002F);
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2});
            this.ReportFooter.HeightF = 6.918589F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(770F, 2.00002F);
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrLine3});
            this.Detail2.HeightF = 180F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999963F, 9.99999F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14,
            this.xrTableRow17,
            this.xrTableRow19,
            this.xrTableRow18,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow20,
            this.xrTableRow16,
            this.xrTableRow15});
            this.xrTable2.SizeF = new System.Drawing.SizeF(721F, 161.66F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.lblSoDong,
            this.xrTableCell17});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.49519996234067065;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "Số dòng";
            this.xrTableCell39.Weight = 0.5;
            // 
            // lblSoDong
            // 
            this.lblSoDong.Name = "lblSoDong";
            this.lblSoDong.Text = "NNE";
            this.lblSoDong.Weight = 2;
            this.lblSoDong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.5;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.lblMaHangHoa,
            this.xrTableCell95,
            this.lblMaSoHangHoa});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.49519996716309311;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "Mã hàng hóa";
            this.xrTableCell91.Weight = 0.5;
            // 
            // lblMaHangHoa
            // 
            this.lblMaHangHoa.Multiline = true;
            this.lblMaHangHoa.Name = "lblMaHangHoa";
            this.lblMaHangHoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblMaHangHoa.Weight = 1.5;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Text = "Mã số hàng hóa";
            this.xrTableCell95.Weight = 0.5;
            // 
            // lblMaSoHangHoa
            // 
            this.lblMaSoHangHoa.Name = "lblMaSoHangHoa";
            this.lblMaSoHangHoa.Text = "XXXXXXXXX1XE";
            this.lblMaSoHangHoa.Weight = 0.5;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.lblMoTaHangHoa,
            this.xrTableCell49});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 2.0096220920339034;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "Mô tả hàng hóa";
            this.xrTableCell103.Weight = 0.5;
            // 
            // lblMoTaHangHoa
            // 
            this.lblMoTaHangHoa.Name = "lblMoTaHangHoa";
            this.lblMoTaHangHoa.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6X" +
                "XXXXXXXX7XXXXXXXXX8XXXXXXXXX9XXXXXXXXXE";
            this.lblMoTaHangHoa.Weight = 2.2973607914451839;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.20263920855481629;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblMaNuocXuatXu,
            this.lblTenNuocXuatXu,
            this.xrTableCell102});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.49519992901612409;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Text = "Nước xuất xứ";
            this.xrTableCell97.Weight = 0.5;
            // 
            // lblMaNuocXuatXu
            // 
            this.lblMaNuocXuatXu.Name = "lblMaNuocXuatXu";
            this.lblMaNuocXuatXu.Text = "XE";
            this.lblMaNuocXuatXu.Weight = 0.25120191427377697;
            // 
            // lblTenNuocXuatXu
            // 
            this.lblTenNuocXuatXu.Name = "lblTenNuocXuatXu";
            this.lblTenNuocXuatXu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblTenNuocXuatXu.Weight = 1.7487980857262231;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.5;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.lblSoKienHang,
            this.xrTableCell119,
            this.xrTableCell120});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.49519992901612409;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Text = "Số kiện hàng";
            this.xrTableCell115.Weight = 0.5;
            // 
            // lblSoKienHang
            // 
            this.lblSoKienHang.Name = "lblSoKienHang";
            this.lblSoKienHang.Weight = 1.5;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 0.5;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 0.5;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.lblSoLuong1,
            this.lblMaDVTSoLuong1,
            this.xrTableCell124,
            this.lblSoLuong2,
            this.lblMaDVTSoLuong2});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.49519992901612409;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Text = "Số lượng (1)";
            this.xrTableCell121.Weight = 0.5;
            // 
            // lblSoLuong1
            // 
            this.lblSoLuong1.Name = "lblSoLuong1";
            this.lblSoLuong1.Text = "NNNNNNNNN1NE";
            this.lblSoLuong1.Weight = 0.42067307809682974;
            this.lblSoLuong1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMaDVTSoLuong1
            // 
            this.lblMaDVTSoLuong1.Name = "lblMaDVTSoLuong1";
            this.lblMaDVTSoLuong1.Text = "XXXE";
            this.lblMaDVTSoLuong1.Weight = 0.69592171000874736;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Số lượng (2)";
            this.xrTableCell124.Weight = 0.38340521189442289;
            // 
            // lblSoLuong2
            // 
            this.lblSoLuong2.Name = "lblSoLuong2";
            this.lblSoLuong2.Text = "NNNNNNNNN1NE";
            this.lblSoLuong2.Weight = 0.5;
            this.lblSoLuong2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMaDVTSoLuong2
            // 
            this.lblMaDVTSoLuong2.Name = "lblMaDVTSoLuong2";
            this.lblMaDVTSoLuong2.Text = "XXXE";
            this.lblMaDVTSoLuong2.Weight = 0.5;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.lblDonGiaHoaDon,
            this.lblMTTDonGiaHoaDon,
            this.lblDonViDonGiaHoaDon,
            this.xrTableCell132});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.49519992901612409;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "Đơn giá hóa đơn";
            this.xrTableCell127.Weight = 0.5;
            // 
            // lblDonGiaHoaDon
            // 
            this.lblDonGiaHoaDon.Name = "lblDonGiaHoaDon";
            this.lblDonGiaHoaDon.Text = "NNNNNNNNE";
            this.lblDonGiaHoaDon.Weight = 0.42067295602653709;
            this.lblDonGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTDonGiaHoaDon
            // 
            this.lblMTTDonGiaHoaDon.Name = "lblMTTDonGiaHoaDon";
            this.lblMTTDonGiaHoaDon.Text = "XXE";
            this.lblMTTDonGiaHoaDon.Weight = 0.18663466404058202;
            // 
            // lblDonViDonGiaHoaDon
            // 
            this.lblDonViDonGiaHoaDon.Name = "lblDonViDonGiaHoaDon";
            this.lblDonViDonGiaHoaDon.Text = "XXE";
            this.lblDonViDonGiaHoaDon.Weight = 0.39269233924278346;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 1.5000000406900975;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.lblTriGiaHoaDon,
            this.lblMTTGiaTien,
            this.xrTableCell114});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.49519992901612409;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "Trị giá hóa đơn";
            this.xrTableCell109.Weight = 0.5;
            // 
            // lblTriGiaHoaDon
            // 
            this.lblTriGiaHoaDon.Name = "lblTriGiaHoaDon";
            this.lblTriGiaHoaDon.Text = "NNNNNNNNN1NNNNNNNE";
            this.lblTriGiaHoaDon.Weight = 0.60730769127478978;
            this.lblTriGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTGiaTien
            // 
            this.lblMTTGiaTien.Name = "lblMTTGiaTien";
            this.lblMTTGiaTien.Text = "XXE";
            this.lblMTTGiaTien.Weight = 0.39269230872521016;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 1.5;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblLoaiKhauTru_Hang,
            this.xrTableCell88,
            this.lblSoTienKhauTru_Hang,
            this.lblMTTSoTienKhauTru_Hang,
            this.xrTableCell92});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.49519992901612409;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Text = "Loại khấu trừ";
            this.xrTableCell85.Weight = 0.5;
            // 
            // lblLoaiKhauTru_Hang
            // 
            this.lblLoaiKhauTru_Hang.Name = "lblLoaiKhauTru_Hang";
            this.lblLoaiKhauTru_Hang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblLoaiKhauTru_Hang.Weight = 1.116594661125363;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "Số tiền khấu trừ";
            this.xrTableCell88.Weight = 0.383405338874637;
            // 
            // lblSoTienKhauTru_Hang
            // 
            this.lblSoTienKhauTru_Hang.Name = "lblSoTienKhauTru_Hang";
            this.lblSoTienKhauTru_Hang.Text = "NNNNNNNNE";
            this.lblSoTienKhauTru_Hang.Weight = 0.5;
            this.lblSoTienKhauTru_Hang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.cell_BeforePrint);
            // 
            // lblMTTSoTienKhauTru_Hang
            // 
            this.lblMTTSoTienKhauTru_Hang.Name = "lblMTTSoTienKhauTru_Hang";
            this.lblMTTSoTienKhauTru_Hang.Text = "XXE";
            this.lblMTTSoTienKhauTru_Hang.Weight = 0.25;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.25;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell84});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.49519992901612409;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "(bao gồm cả phân loại miễn trả tiền)";
            this.xrTableCell57.Weight = 1.0000000101725244;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 1.9999999898274756;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(770F, 2.00002F);
            // 
            // BanKhaiThongTinHoaDon_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.DetailReport1});
            this.Margins = new System.Drawing.Printing.Margins(53, 27, 95, 49);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblGioKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblSoPL;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblGhiChuCuaChuHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblTongTrongLuongHangGross;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhTongTheTich;
        private DevExpress.XtraReports.UI.XRLabel lblTongTheTich;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhTongTrongLuong;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoKienHang;
        private DevExpress.XtraReports.UI.XRLabel lblTrongLuongThuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhTrongLuongThuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhTongSoKienHang;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienDieuChinhKhac;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiSoTienDieuChinhKhac;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblNganHangLC;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaFOB;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTCuaFOB;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienDieuChinhFOB;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTSoTienDieuChinhFOB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblNoiThanhToanPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblChiPhiXepHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTChiPhiXepHang1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiChiPhiXepHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblChiPhiXepHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTChiPhiXepHang2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiChiPhiXepHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiVanChuyenDuongBoNoiDia;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTPhiVanChuyenDuongBoNoiDia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTCuaPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTSoTienDieuChinhPhiBH;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienKhauTru;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTCuaSoTienKhauTru;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiKhauTru;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTSoTienDieuChinhKhac;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemGiaoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTTongTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKienGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoDongHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienDieuChinhPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChuDacBiet;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNuocXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell lblSoKienHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSoLuong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSoLuong2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblDonViDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTGiaTien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableCell lblMTTSoTienKhauTru_Hang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
    }
}
