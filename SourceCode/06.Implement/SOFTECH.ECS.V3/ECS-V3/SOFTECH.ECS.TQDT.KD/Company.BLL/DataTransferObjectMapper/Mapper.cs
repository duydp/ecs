﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
namespace Company.KD.BLL.DataTransferObjectMapper
{

    /// <summary>
    /// Ánh xạ từ BOs(Bussines object)  sang DTOs (Data Transfer Objects)    
    /// </summary>
    public class Mapper
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        static string sfmtDate = "yyyy-MM-dd";
        /// <summary>
        /// Chuyển dữ liệu từ ToKhaiMauDich BO sang KD_ToKhaiMauDich DTO
        /// </summary>
        /// <param name="tokhaimaudich">ToKhaiMauDich</param>
        /// <returns>ToKhai</returns>
        #region TransferOb tờ KD
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan, tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);

            #region Header
            ToKhai tokhai = new ToKhai()
            {
                Issuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkmd.NgayTiepNhan.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = tkmd.MaHaiQuan.Trim(),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 3),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 3),
                // Mã Loại Hình
                NatureOfTransaction = tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 3) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },

                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn 
                //AdditionalDocuments = new List<AdditionalDocument>(),

                // Hóa Ðon thuong mại
                Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },

                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = "." } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() },

                //Doanh nghiệp nhập khẩu
                Importer = !isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                new NameBase { Name = tkmd.TenDonViDoiTac, Identity = tkmd.MaDoanhNghiep.Trim() },

                // Nguười gởi (Tên chủ hàng)
                RepresentativePerson = new RepresentativePerson { ContactFunction = tkmd.ChucVu, Name = tkmd.TenChuHang },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),
                /*
                License = new List<License>(),
                ContractDocument = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigins = new List<CertificateOfOrigin>(),
                CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                AttachDocumentItem = new List<AttachDocumentItem>(),
                AdditionalDocumentEx = new List<AdditionalDocument>(),*/
                #endregion Nrr

            };
            tokhai.AdditionalInformations.Add(
                new AdditionalInformation
                {
                    Statement = "001",
                    Content = new Content() { Text = tkmd.DeXuatKhac }
                }
                );
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });


            #endregion Header

            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion

            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                 {

                     CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                     Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                     StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT, GlobalsShare.TriGiaNT),
                     UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                     StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                     Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                     Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                     GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, GlobalsShare.TriGiaNT), MeasureUnit = hmd.DVT_ID }

                 };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                // Update by KhanhHN 
                // method = phương pháp xác định trị giá. Nếu có tờ khai trị giá thì method = 1
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal dPhiVanChuyen = tkmd.PhiVanChuyen;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(dTongPhi / soluonghang, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen / soluonghang, 4),
                    OtherChargeDeduction = "0",
                    Method = string.Empty
                };
                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS,
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    DutyTaxFee = new List<DutyTaxFee>(),
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = string.Empty,
                    SpecificTaxBase = string.Empty,
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].TriGiaThuKhac, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(/*tkmd.HMDCollection[i].PhuThu*/0, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(0, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new AdditionalDocument()
                     {
                         Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                         Issuer = giayphep.NguoiCap/*Người cấp*/,
                         IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                         Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                         Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                         Name = "GP"/* Tên giấy phép*/,
                         Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                     };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion

                #region AdditionalInformations - ValuationAdjustments Thêm hàng từ tờ khai trị giá 1,2,3
                if (isToKhaiNhap)
                {
                    //Thông tin tờ khai trị giá.
                    #region Tờ khai trị giá PP1
                    if (tkmd.TKTGCollection != null)
                        if (tkmd.TKTGCollection.Count > 0)
                        {
                            ToKhaiTriGia tktg = tkmd.TKTGCollection[0];
                            foreach (HangTriGia hangtrigia in tktg.HTGCollection)
                                if (hangtrigia.STTHang == hmd.SoThuTuHang)
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    /* Fill dữ liệu hàng  trong tờ khai trị giá PP1*/
                                    //Nếu có tờ khai trị giá thì method = 1 
                                    customsGoodsItem.CustomsValuation.Method = "1";
                                    #region AdditionalInformations Nội dung tờ khai trị giá
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TO_SO",
                                        Statement = AdditionalInformationStatement.TO_SO,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.ToSo) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "NGAY_XK",
                                        Statement = AdditionalInformationStatement.NGAY_XK,
                                        Content = new Content() { Text = tktg.NgayXuatKhau.ToString(sfmtDate) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "QUYEN_SD",
                                        Statement = AdditionalInformationStatement.QUYEN_SD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.QuyenSuDung) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KHONG_XD",
                                        Statement = AdditionalInformationStatement.KHONG_XD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.KhongXacDinh) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TRA_THEM",
                                        Statement = AdditionalInformationStatement.TRA_THEM,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TraThem) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TIEN_TRA_16",
                                        Statement = AdditionalInformationStatement.TIEN_TRA_16,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TienTra) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "CO_QHDB",
                                        Statement = AdditionalInformationStatement.CO_QHDB,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.CoQuanHeDacBiet) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KIEU_QHDB",
                                        Statement = AdditionalInformationStatement.KIEU_QHDB,
                                        Content = new Content() { Text = tktg.KieuQuanHe }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "ANH_HUONG_QH",
                                        Statement = AdditionalInformationStatement.ANH_HUONG_QH,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.AnhHuongQuanHe) }
                                    });
                                    #endregion Nội dung tờ khai trị giá

                                    #region ValuationAdjustments Chi tiết hàng tờ khai trị giá pp1
                                    if (tktg.HTGCollection != null && tktg.HTGCollection.Count > 0)
                                    {
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Gia_hoa_don,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiaTrenHoaDon, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thanh_toan_gian_tiep,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.KhoanThanhToanGianTiep, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_tra_truoc,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TraTruoc, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_hoa_hong,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.HoaHong, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_bi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiBaoBi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_dong_goi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiDongGoi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_tro_giup,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TroGiup, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NVL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.NguyenLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.VatLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_cong_cu,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.CongCu, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_thiet_ke,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ThietKe, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_ban_quyen,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.BanQuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_phai_tra,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienTraSuDung, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_van_tai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiVanChuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_hiem,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.PhiBaoHiem, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_VT_BH_noi_dia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiNoiDia, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_phat_sinh,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiPhatSinh, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_lai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienLai, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thue_phi_le_phi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienThue, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_Giam_Gia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiamGia, GlobalsShare.TriGiaNT)
                                        });

                                    }
                                }


                                    #endregion Chi tiết hàng tờ khai trị giá

                        }
                    #endregion Tờ khai trị giá PP1

                    if (tkmd.TKTGPP23Collection != null)
                        if (tkmd.TKTGPP23Collection.Count > 0)
                        {
                            foreach (ToKhaiTriGiaPP23 tkpgP23 in tkmd.TKTGPP23Collection)
                            {
                                if (tkpgP23.TenHang.ToUpper().Trim() == hmd.TenHang.ToUpper().Trim())
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    string maTktg = tkpgP23.MaToKhaiTriGia.ToString();
                                    customsGoodsItem.CustomsValuation.Method = maTktg;
                                    #region AdditionalInformations Nội dung tờ khai trị giá PP23

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.LyDo },
                                        Statement = maTktg + AdditionalInformationStatement.LYDO_KAD_PP1,
                                        StatementDescription = "LYDO_KAD_PP1"

                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuat.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK23,
                                        StatementDescription = "NGAY_XK"

                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.STTHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.STTHANG_TT,
                                        StatementDescription = "STTHANG_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.SoTKHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.SOTK_TT,
                                        StatementDescription = "SOTK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayDangKyHangTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_NK_TT,
                                        StatementDescription = "NGAY_NK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaHaiQuanHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.MA_HQ_TT,
                                        StatementDescription = "MA_HQ_TT"
                                    });


                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuatTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK_TT,
                                        StatementDescription = "NGAY_XK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.GiaiTrinh },
                                        Statement = maTktg + AdditionalInformationStatement.GIAI_TRINH,
                                        StatementDescription = "GIAI_TRINH"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaLoaiHinhHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.Ma_LH,
                                        StatementDescription = "Ma_LH"
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.TenHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.HANG_TUONG_TU,
                                        StatementDescription = "Ma_LH"
                                    });
                                    #endregion Nội dung tờ khai trị giá PP23

                                    #region ValuationAdjustments Chi tiết nội dung trong tờ khai tri giá PP23

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.Tri_gia_hang_TT,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.TriGiaNguyenTeHangTT, GlobalsShare.TriGiaNT)
                                    });

                                    // Cộng ghi số dương trừ ghi số âm(+/-)
                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_cap_do_TM,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongThuongMai != 0 ? tkpgP23.DieuChinhCongThuongMai : -tkpgP23.DieuChinhTruCapDoThuongMai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_so_luong,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongSoLuong != 0 ? tkpgP23.DieuChinhCongSoLuong : -tkpgP23.DieuChinhTruSoLuong, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_khoan_khac,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongKhoanGiamGiaKhac != 0 ? tkpgP23.DieuChinhCongKhoanGiamGiaKhac : -tkpgP23.DieuChinhTruKhoanGiamGiaKhac, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_van_tai,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiVanTai != 0 ? tkpgP23.DieuChinhCongChiPhiVanTai : -tkpgP23.DieuChinhTruChiPhiVanTai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_bao_hiem,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiBaoHiem != 0 ? tkpgP23.DieuChinhCongChiPhiBaoHiem : -tkpgP23.DieuChinhTruChiPhiBaoHiem, GlobalsShare.TriGiaNT)
                                    });

                                    #endregion Chi tiết nội dung trong tờ khai tri giá PP23

                                    /* Fill dữ liệu hàng trong tờ khai trị giá PP23*/

                                }
                            }
                        }


                }
                else
                {
                    /*
                    customsGoodsItem.SpecializedManagement = new SpecializedManagement()
                    {
                        GrossMass = "",
                        Identification = "",
                        MeasureUnit = "",
                        Quantity = "",
                        Type = "",
                        UnitPrice = ""
                    };*/
                }
                #endregion thêm hàng từ tờ khai trị giá 1,2,3

                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }

            #endregion CustomGoodsItem Danh sách hàng khai báo
            #region Licenses Danh sách giấy phép XNK đi kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            {
                tokhai.License = new List<License>();
                #region License Giấy phép
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    License lic = LicenseFrom(giayPhep);
                    tokhai.License.Add(lic);
                }
                #endregion Giấy phép
            }

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            {
                tokhai.ContractDocument = new List<ContractDocument>();
                #region ContractDocument  Hợp đồng thương mại
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
                #endregion Hợp đồng thương mại
            }

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            {
                tokhai.CommercialInvoices = new List<CommercialInvoice>();
                #region CommercialInvoice Hóa đơn thương mại
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
                #endregion Hóa đơn thương mại
            }

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            {
                tokhai.CertificateOfOrigins = new List<CertificateOfOrigin>();
                #region CertificateOfOrigin Thêm CO

                foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                {
                    tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                }
                #endregion CO
            }


            if (tkmd.VanTaiDon != null && tkmd.VanTaiDon.SoVanDon.Trim() != ".")
            #region BillOfLadings Vận đơn
            {

                tokhai.BillOfLadings = new List<BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;

                BillOfLading billOfLading = new BillOfLading()
                {
                    Reference = vandon.SoVanDon,
                    Issue = vandon.NgayVanDon.ToString(sfmtDate),
                    IssueLocation = vandon.QuocTichPTVT
                };
                billOfLading.BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = vandon.SoHieuPTVT,
                    Identification = vandon.TenPTVT,
                    Journey = vandon.SoHieuChuyenDi,
                    ModeAndType = tkmd.PTVT_ID,
                    Departure = vandon.NgayKhoiHanh.ToString(sfmtDate),
                    RegistrationNationality = vandon.QuocTichPTVT.Substring(0, 2)
                };
                billOfLading.Carrier = new NameBase()
                {
                    Name = vandon.TenHangVT,
                    Identity = vandon.MaHangVT
                };



                billOfLading.Consignment = new Consignment()
                {
                    Consignor = new NameBase()
                    {
                        Name = vandon.TenNguoiGiaoHang,
                        Identity = vandon.MaNguoiGiaoHang
                    },
                    Consignee = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHang,
                        Identity = vandon.MaNguoiNhanHang
                    },
                    NotifyParty = new NameBase()
                    {
                        Name = vandon.TenNguoiNhanHangTrungGian,
                        Identity = vandon.MaNguoiNhanHangTrungGian
                    },
                    LoadingLocation = new LoadingLocation()
                    {
                        Name = vandon.TenCangXepHang,
                        Code = vandon.MaCangXepHang,
                        Loading = vandon.NgayKhoiHanh.ToString(sfmtDate)
                    },
                    UnloadingLocation = new UnloadingLocation()
                    {
                        Name = vandon.TenCangDoHang,
                        Code = vandon.MaCangDoHang,
                        Arrival = vandon.NgayDenPTVT.ToString(sfmtDate)
                    },
                    DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                        Type = vandon.LoaiKien,
                        MarkNumber = string.Empty
                    }
                };
                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng và Container trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = string.Empty,//Mã hàng
                                    TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 3),
                                    MeasureUnit = hangVanDon.DVT_ID.Substring(0, 3)
                                },
                                EquipmentIdentification = new EquipmentIdentification()
                                {
                                    identification = hangVanDon.SoHieuContainer
                                }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);

            }
            #region Tự động thêm vận đơn rỗng cho tờ khai xuất
            else if (tkmd.VanTaiDon != null && ( tkmd.VanTaiDon.SoVanDon.Trim() == "." && tkmd.MaLoaiHinh.Substring(0, 1).Trim().ToUpper() == "X"))
            {
                tokhai.BillOfLadings = new List<BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                BillOfLading billOfLading = new BillOfLading()
                {
                    Reference = " ",
                    Issue = " ",
                    IssueLocation = " "
                };
                billOfLading.BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = " ",
                    Identification = " ",
                    Journey = " ",
                    ModeAndType = tkmd.PTVT_ID,
                    Departure = " ",
                    RegistrationNationality = " "
                };
                billOfLading.Carrier = new NameBase()
                {
                    Name = " ",
                    Identity = " "
                };

                billOfLading.Consignment = new Consignment()
                {
                    Consignor = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    Consignee = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    NotifyParty = new NameBase()
                    {
                        Name = " ",
                        Identity = " "
                    },
                    LoadingLocation = new LoadingLocation()
                    {
                        Name = " ",
                        Code = "",
                        Loading = ""
                    },
                    UnloadingLocation = new UnloadingLocation()
                    {
                        Name = " ",
                        Code = " ",
                        Arrival = " "
                    },
                    DeliveryDestination = new DeliveryDestination() { Line = " " },


                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = " ",
                        Type = " ",
                        MarkNumber = string.Empty
                    }
                };
                #region Hàng và Container trong vận đơn
                if (vandon != null)
                {


                    if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                        billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                    {

                        TransportEquipment transportEquipment = new TransportEquipment()
                        {
                            Characteristic = container.LoaiContainer,
                            EquipmentIdentifications = new EquipmentIdentification()
                            {
                                identification = container.SoHieu
                            },
                            Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                            Seal = container.Seal_No
                        };

                        billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                    }


                    if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                    {
                        List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                        foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                        {
                            HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                            if (hangToKhai != null)
                                hangsVanDon.Add(new ConsignmentItem()
                                {
                                    Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                    ConsignmentItemPackaging = new Packaging()
                                    {
                                        Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                        Type = hangVanDon.LoaiKien,
                                        MarkNumber = hangVanDon.SoHieuKien,
                                    },
                                    Commodity = new Commodity()
                                    {
                                        Description = hangVanDon.TenHang,
                                        Identification = string.Empty,//Mã hàng
                                        TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                    },
                                    GoodsMeasure = new GoodsMeasure()
                                    {
                                        GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 3),
                                        MeasureUnit = hangVanDon.DVT_ID.Substring(0, 3)
                                    },
                                    EquipmentIdentification = new EquipmentIdentification()
                                    {
                                        identification = hangVanDon.SoHieuContainer
                                    }

                                });

                        }
                        billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                    }
                }
                #endregion Hàng và Container trong vận đơn
                tokhai.BillOfLadings.Add(billOfLading);
            }
            #endregion
            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
                #endregion Đề nghị chuyển cửa khẩu
            }

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                #region AttachDocumentItem Chứng từ đính kèm
                foreach (ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
                #endregion Chứng từ đính kèm
            }

            #endregion Danh sách giấy phép XNK đi kèm
            #region AdditionalDocumentNos Thêm chứng từ nợ

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }


        #endregion

        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        private static CertificateOfOrigin CertificateOfOriginFrom(CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
            {


                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() {Name = co.TenDiaChiNguoiXK, Identity = tkmd.MaDoanhNghiep} : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = string.Empty },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                LoadingLocation = new LoadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },

                UnloadingLocation = new UnloadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            {
                GoodsItem goodsItem = new GoodsItem();
                goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
                goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
                goodsItem.CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = hangCo.MaNguyenTe
                };
                goodsItem.ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
                    Type = hangCo.LoaiKien,
                    MarkNumber = hangCo.SoHieuKien
                };
                goodsItem.Commodity = new Commodity()
                {
                    Description = hangCo.TenHang,
                    Identification = hangCo.MaPhu,
                    TariffClassification = hangCo.MaHS
                };
                goodsItem.GoodsMeasure = new GoodsMeasure()
                {
                    GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, 3),
                    MeasureUnit = hangCo.DVT_ID
                };
                goodsItem.Origin = new Origin()
                {
                    OriginCountry = hangCo.NuocXX_ID.Trim()
                };
                goodsItem.Invoice = new Invoice()
                {
                    Reference = hangCo.SoHoaDon,
                    Issue = "0",
                };
                certificateOfOrigin.GoodsItems.Add(goodsItem);
            }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, ChungTuKem chungtukem)
        {

            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH,
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    //attachDocumentItem.AttachedFiles.Add(new AttachedFile
                    //{
                    //    FileName = fileDetail.FileName,
                    //    Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    //});
                };
            return attachDocumentItem;
        }
        private static License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            License lic = new License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = hangInGiayPhep.MaPhu,
                            TariffClassification = hangInGiayPhep.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static ContractDocument ContractFrom(HopDongThuongMai hdThuongMai)
        {
            ContractDocument contractDocument = new ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, GlobalsShare.TriGiaNT),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS,
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 3), MeasureUnit = hangInHdThuongMai.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        private static Receipt ReceiptFrom(GiayNopTien giayNopTien)
        {
            Receipt receipt = new Receipt()
            {
                Reference = giayNopTien.SoLenh,
                Issue = giayNopTien.NgayPhatLenh.ToString(sfmtDate),
                Payer = new Payer
                {
                    Name = giayNopTien.TenNguoiNop,
                    Identity = giayNopTien.SoCMNDNguoiNop,
                    AddressGNT = new DeliveryDestination { Line = giayNopTien.DiaChi }
                },
                TaxPayer = new Payer
                {
                    Name = giayNopTien.TenDonViNop,
                    Identity = giayNopTien.MaDonViNop,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNop,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNop, Identity = giayNopTien.MaNganHangNop }
                    }
                },
                Payee = new Payer
                {
                    Name = giayNopTien.TenDonViNhan,
                    Identity = giayNopTien.MaDonViNhan,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNhan,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNhan, Identity = giayNopTien.MaNganHangNhan }
                    }
                },
                DutyTaxFee = new List<DutyTaxFee>(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = giayNopTien.GhiChu } },
                AdditionalDocument = new List<AdditionalDocument>(),
            };
            foreach (GiayNopTienChiTiet chitiet in giayNopTien.ChiTietCollection)
            {
                receipt.DutyTaxFee.Add(DutyTaxFeeForGiayNopTien(chitiet));
            }
            foreach (GiayNopTienChungTu chungtu in giayNopTien.ChungTuCollection)
            {
                AdditionalDocument add = new AdditionalDocument
                {
                    Type = chungtu.LoaiChungTu,
                    Reference = chungtu.SoChungTu,
                    Name = chungtu.TenChungTu,
                    Issue = chungtu.NgayPhatHanh.ToString(sfmtDate),
                };
                receipt.AdditionalDocument.Add(add);
            }
            return receipt;
        }
        private static DutyTaxFee DutyTaxFeeForGiayNopTien(GiayNopTienChiTiet chiTietGiayNopTien)
        {
            DutyTaxFee tax = new DutyTaxFee
            {
                AdValoremTaxBase = Helpers.FormatNumeric(chiTietGiayNopTien.SoTien, 4),
                Deduct = Helpers.FormatNumeric(chiTietGiayNopTien.DieuChinhGiam, 4),
                Type = Helpers.FormatNumeric(chiTietGiayNopTien.SacThue),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            for (int i = 1; i < 6; i++)
            {
                AdditionalInformation add = new AdditionalInformation
                {
                    Content = new Content()
                };
                switch (i)
                {
                    case 1:
                        add.Statement = "211";
                        add.StatementDescription = "Chuong";
                        add.Content.Text = chiTietGiayNopTien.MaChuong;
                        break;
                    case 2:
                        add.Statement = "212";
                        add.StatementDescription = "Loai";
                        add.Content.Text = chiTietGiayNopTien.Loai;
                        break;
                    case 3:
                        add.Statement = "213";
                        add.StatementDescription = "Khoan";
                        add.Content.Text = chiTietGiayNopTien.Khoan;
                        break;
                    case 4:
                        add.Statement = "214";
                        add.StatementDescription = "Muc";
                        add.Content.Text = chiTietGiayNopTien.Muc;
                        break;
                    case 5:
                        add.Statement = "215";
                        add.StatementDescription = "Tieu Muc";
                        add.Content.Text = chiTietGiayNopTien.TieuMuc;
                        break;
                }
                tax.AdditionalInformations.Add(add);
            }

            return tax;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong
                }
            };
            return customsOfficeChangedRequest;
        }
        private static CommercialInvoice CommercialInvoiceFrom(HoaDonThuongMai hoaDonThuongMai)
        {
            CommercialInvoice commercialInvoice = new CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),
                //Xuat xu hang hoa Origin [4]

            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 3), MeasureUnit = hangInHoaDonThuongMai.DVT_ID },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }
        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<CertificateOfOrigin>();
                CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<License>();
                License license = LicenseFrom(giayphep);
                boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                boSungChungTuDto.ContractDocuments = new List<ContractDocument>();
                ContractDocument contract = ContractFrom(hopdong);
                boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<CommercialInvoice>();
                CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                //boSungChungTuDto.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = Helpers.Format(tkmd.SoToKhai, 0),
                //    Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                //    NatureOfTransaction = tkmd.MaLoaiHinh.Trim(),
                //    DeclarationOffice = tkmd.MaHaiQuan.Trim()
                //};
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm

            else if (NoiDungBoSung.GetType() == typeof(GiayNopTien))
            #region Giấy nộp tiền
            {
                Company.KDT.SHARE.QuanLyChungTu.GiayNopTien giayNopTien = (GiayNopTien)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_NOP_TIEN;
                boSungChungTuDto.Reference = giayNopTien.GuidStr;
                Receipt receipt = ReceiptFrom(giayNopTien);
                boSungChungTuDto.Receipt = receipt;
            }
            #endregion
            return boSungChungTuDto;
        }


        #endregion
    }
}

