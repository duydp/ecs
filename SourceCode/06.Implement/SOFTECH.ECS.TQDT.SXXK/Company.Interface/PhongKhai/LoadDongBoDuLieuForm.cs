﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.BLL;
using System.Configuration;
using Company.BLL.KDT.SXXK;
using Company.BLL.KDT;


namespace Company.Interface
{
    public partial class LoadDongBoDuLieuForm : Company.Interface.BaseForm
    {


        public LoadDongBoDuLieuForm()
        {
            InitializeComponent();
        }
        private int value = 0;
        public static bool chkErrConn = false;
        // get data from SXXK database :
        public DataSet getDataBySQL(string sql, string MaHaiQuan, string database)
        {
            DataSet ds = new DataSet();
            //Company.Interface.WS.DongBoDuLieu.SLXNKService ws = new Company.Interface.WS.DongBoDuLieu.SLXNKService();
            try
            {
                ds = new DoanhNghiep().QuerySQL(sql, "");
            }
            catch
            {
                chkErrConn = true;
                //ShowMessage(" Không kết nối được với hệ thống Hải quan, đồng bộ không thành công "  , false);
            }
            return ds;
        }
        // get data from SLXNK database :
        public DataSet GetDataSLXNK(string sql, string database)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = new DoanhNghiep().GetSLXNK(sql, "");
            }
            catch (Exception ex)
            {
                chkErrConn = true;
                //ShowMessage("Lỗi :" + ex.Message, false);
            }
            return ds;
        }
        public DataSet GetDataKDT(string sql)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = new DoanhNghiep().GetKhaiDT(sql);
            }
            catch (Exception ex)
            {
                chkErrConn = true;
                //ShowMessage("Lỗi :" + ex.Message, false);
            }
            return ds;
        }
        //Update NPL SXXK :
        public void updateNPLSXXK(string MaHaiQuan, string MaDN)
        {
            //
            string query = "SELECT NPL.Ma_HQ AS MaHaiQuan, NPL.Ma_NPL AS Ma, NPL.Ten_NPL AS Ten, NPL.Ma_HS AS MaHS, NPL.MA_DVT AS DVT_ID FROM SNPL NPL WHERE NPL.MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "'";
            DataSet ds = getDataBySQL(query, MaHaiQuan, "SXXK");
            Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            npl.UpdateRegistedToDatabase(MaHaiQuan, MaDN, ds);
            //MessageBox.Show("Đồng bộ xong nguyên phụ liệu", "Nguyên phụ liệu", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //lblTrangthai.Text = " Đã hoàn thành ";
        }
        //Update SP SXXK :
        public void updateSPSXXK(string MaHaiQuan, string MaDN)
        {
            //lblTrangthai.Text = "Đang đồng bộ Sản phẩm .....";
            string query = "SELECT SP.Ma_HQ AS MaHaiQuan, SP.Ma_SP AS Ma, SP.Ten_SP AS Ten, SP.Ma_HS AS MaHS, SP.MA_DVT AS DVT_ID FROM SSP SP WHERE SP.MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "'";
            DataSet ds = getDataBySQL(query, MaHaiQuan, "SXXK");
            Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
            sp.UpdateRegistedToDatabase(MaHaiQuan, MaDN, ds);
            //if (errBool == true)
            //    ShowMessage(" Không kết nối được với hệ thống chi cục Hải quan, đồng bộ không thành công ", false);

        }
        //Update DM SXXK :
        public void updateDMSXXK(string MaHaiQuan, string MaDN)
        {
            lblTrangthai.Text = "Định mức .....";
            string query = "SELECT Ma_HQ, Ma_DV, Ma_SP, SO_DM, NGAY_DK, NGAY_AD, NGAY_HH  FROM DDINHMUC_TT WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "'";
            DataSet ds = getDataBySQL(query, MaHaiQuan, "SXXK");
            query = "SELECT Ma_HQ, Ma_DV, Ma_SP, Ma_NPL, DM_SD, TL_HH, DM_CHUNG, GHI_CHU  FROM DDINHMUC WHERE MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDN + "'";
            DataSet dsDinhMuc = getDataBySQL(query, MaHaiQuan, "SXXK");
            Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
            dm.UpdateRegistedToDatabase(MaHaiQuan, MaDN, ds, dsDinhMuc);
            //MessageBox.Show("Cập nhật xong định mức", "Định mức", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        // Update to khai :
     
     
    

        #region Đồng bộ dữ liệu khai điên tử
        public void UpdateNPLKDT(string MaHaiQuan, string MaDN)
        {
            new NguyenPhuLieuDangKy().DeleteDynamicTransaction("MaHaiQuan = '" + MaHaiQuan + "' AND MaDoanhNghiep = '" + MaDN + "' and TrangThaiXuLy!=-1", null);
            string queryNPLDK = "SELECT * FROM DNPLDK WHERE Ma_HQTN = '" + MaHaiQuan + "' AND Ma_DV ='" + MaDN + "' AND TrangThaiXL = 0";
            DataSet dsNPLDKHaiQuan = GetDataKDT(queryNPLDK);
            foreach (DataRow dr in dsNPLDKHaiQuan.Tables[0].Rows)
            {
                long soTN = Convert.ToInt64(dr["So_TNDKDT"]);
                short namTN = Convert.ToInt16(dr["Nam_TN"]);
                string queryNPLDK_D = "SELECT * FROM DNPLDK_D WHERE So_TNDKDT = " + soTN + " AND Ma_HQTN ='" + MaHaiQuan + "' AND Nam_TN = " + namTN;
                DataSet dsNPLDK_DHaiQuan = GetDataKDT(queryNPLDK_D);

                NguyenPhuLieuDangKy NPLDK = new NguyenPhuLieuDangKy();
                NPLDK.SoTiepNhan = Convert.ToInt64(dr["So_TNDKDT"]);
                NPLDK.NgayTiepNhan = Convert.ToDateTime(dr["Ngay_TN"]);
                NPLDK.MaHaiQuan = MaHaiQuan;
                NPLDK.MaDoanhNghiep = MaDN;
                NPLDK.MaDaiLy = "";
                NPLDK.TrangThaiXuLy = Convert.ToInt32(dr["TrangThaiXL"]);
                //NPLDK.ID_Home = 0;
                NPLDK.NPLCollection = new NguyenPhuLieuCollection();
                foreach (DataRow dr2 in dsNPLDK_DHaiQuan.Tables[0].Rows)
                {
                    NguyenPhuLieu NPL = new NguyenPhuLieu();
                    NPL.Ma = dr2["Ma_NPL"].ToString();
                    NPL.Ten = dr2["Ten_NPL"].ToString();
                    NPL.MaHS = dr2["Ma_HS"].ToString();
                    NPL.DVT_ID = dr2["Ma_DVT"].ToString();
                    try
                    {
                        NPL.STTHang = Convert.ToInt32(dr2["STTHang"]);
                    }
                    catch { }
                    NPLDK.NPLCollection.Add(NPL);
                }
                NPLDK.InsertUpdateFull();
            }

        }
        public void UpdateSPKDT(string MaHaiQuan, string MaDN)
        {
            new SanPhamDangKy().DeleteDynamicTransaction("MaHaiQuan = '" + MaHaiQuan + "' AND MaDoanhNghiep = '" + MaDN + "' and TrangThaiXuLy!=-1", null);
            string queryDK = "SELECT * FROM DSPDK WHERE Ma_HQTN = '" + MaHaiQuan + "' AND Ma_DV ='" + MaDN + "' AND TrangThaiXL = 0";
            DataSet dsDKHaiQuan = GetDataKDT(queryDK);
            foreach (DataRow dr in dsDKHaiQuan.Tables[0].Rows)
            {
                long soTN = Convert.ToInt64(dr["So_TNDKDT"]);
                short namTN = Convert.ToInt16(dr["Nam_TN"]);
                string queryDK_D = "SELECT * FROM DSPDK_D WHERE So_TNDKDT = " + soTN + " AND Ma_HQTN ='" + MaHaiQuan + "' AND Nam_TN = " + namTN;
                DataSet dsDK_DHaiQuan = GetDataKDT(queryDK_D);

                SanPhamDangKy SPDK = new SanPhamDangKy();
                SPDK.SoTiepNhan = Convert.ToInt64(dr["So_TNDKDT"]);
                SPDK.NgayTiepNhan = Convert.ToDateTime(dr["Ngay_TN"]);
                SPDK.MaHaiQuan = MaHaiQuan;
                SPDK.MaDoanhNghiep = MaDN;
                SPDK.MaDaiLy = "";
                SPDK.TrangThaiXuLy = Convert.ToInt32(dr["TrangThaiXL"]);
                //SPDK.ID_Home = 0;
                SPDK.SPCollection = new SanPhamCollection();
                foreach (DataRow dr2 in dsDK_DHaiQuan.Tables[0].Rows)
                {
                    SanPham SP = new SanPham();
                    SP.Ma = dr2["Ma_SP"].ToString();
                    SP.Ten = dr2["Ten_SP"].ToString();
                    SP.MaHS = dr2["Ma_HS"].ToString();
                    SP.DVT_ID = dr2["Ma_DVT"].ToString();
                    try
                    {
                        SP.STTHang = Convert.ToInt32(dr2["STTHang"]);
                    }
                    catch { }
                    SPDK.SPCollection.Add(SP);
                }
                SPDK.InsertUpdateFull();
            }
        }
        public void UpdateDMKDT(string MaHaiQuan, string MaDN)
        {
            new DinhMucDangKy().DeleteDynamicTransaction("MaHaiQuan = '" + MaHaiQuan + "' AND MaDoanhNghiep = '" + MaDN + "' and TrangThaiXuLy!=-1", null);
            string queryDK = "SELECT * FROM DDINHMUCDK WHERE Ma_HQTN = '" + MaHaiQuan + "' AND Ma_DV ='" + MaDN + "' AND TrangThaiXL = 0";
            DataSet dsDKHaiQuan = GetDataKDT(queryDK);
            foreach (DataRow dr in dsDKHaiQuan.Tables[0].Rows)
            {
                long soTN = Convert.ToInt64(dr["So_TNDKDT"]);
                short namTN = Convert.ToInt16(dr["Nam_TN"]);
                string queryDK_D = "SELECT * FROM DDinhMucDK_D WHERE So_TNDKDT = " + soTN + " AND Ma_HQTN ='" + MaHaiQuan + "' AND Nam_TN = " + namTN;
                DataSet dsDK_DHaiQuan = GetDataKDT(queryDK_D);

                DinhMucDangKy DMDK = new DinhMucDangKy();
                DMDK.SoTiepNhan = Convert.ToInt64(dr["So_TNDKDT"]);
                DMDK.NgayTiepNhan = Convert.ToDateTime(dr["Ngay_TN"]);
                DMDK.MaHaiQuan = MaHaiQuan;
                DMDK.MaDoanhNghiep = MaDN;
                DMDK.MaDaiLy = "";
                DMDK.TrangThaiXuLy = Convert.ToInt32(dr["TrangThaiXL"]);
                try
                {
                    DMDK.SoTiepNhanChungTu = Convert.ToInt64(dr["So_TNCT"]);
                }
                catch { }
                try
                {
                    DMDK.SoDinhMuc = Convert.ToInt32(dr["So_DM"]);
                }
                catch { }
                try
                {
                    DMDK.NgayDangKy = Convert.ToDateTime(dr["Ngay_DM"]);
                }
                catch { }
                try
                {
                    DMDK.NgayApDung = Convert.ToDateTime(dr["Ngay_AD"]);
                }
                catch { }
                //DMDK.ID_Home = 0;
                DMDK.DMCollection = new DinhMucCollection();
                foreach (DataRow dr2 in dsDK_DHaiQuan.Tables[0].Rows)
                {
                    DinhMuc DM = new DinhMuc();
                    DM.MaSanPham = dr2["Ma_SP"].ToString();
                    DM.MaNguyenPhuLieu = dr2["Ma_NPL"].ToString();
                    DM.DinhMucSuDung = Convert.ToDecimal(dr2["DM_SD"]);
                    DM.TyLeHaoHut = Convert.ToDecimal(dr2["TL_HH"]);
                    DM.GhiChu = dr2["Ghi_Chu"].ToString();
                    try
                    {
                        DM.STTHang = Convert.ToInt32(dr2["STTHang"]);
                    }
                    catch { }
                    DMDK.DMCollection.Add(DM);
                }
                DMDK.InsertUpdateFull();
            }
        }
        public void UpdateTKKDT(string MaHaiQuan, string MaDN)
        {

            new ToKhaiMauDich().DeleteDynamicTransaction("MaHaiQuan = '" + MaHaiQuan + "' AND MaDoanhNghiep = '" + MaDN + "' and TrangThaiXuLy!=-1", null);
            string queryDK = "SELECT * FROM DTOKHAIMD WHERE Ma_HQTN = '" + MaHaiQuan + "' AND Ma_DV ='" + MaDN + "' AND TrangThaiXL = 0";
            DataSet dsDKHaiQuan = GetDataKDT(queryDK);
            foreach (DataRow dr in dsDKHaiQuan.Tables[0].Rows)
            {
                long soTN = Convert.ToInt64(dr["So_TNDKDT"]);
                short namTN = Convert.ToInt16(dr["Nam_TN"]);
                string queryDK_D = "SELECT * FROM DHANGMD WHERE So_TNDKDT = " + soTN + " AND Ma_HQTN ='" + MaHaiQuan + "' AND Nam_TN = " + namTN;
                DataSet dsDK_DHaiQuan = GetDataKDT(queryDK_D);

                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.SoTiepNhan = Convert.ToInt64(dr["So_TNDKDT"]);
                TKMD.NgayTiepNhan = Convert.ToDateTime(dr["Ngay_TN"]);
                TKMD.MaHaiQuan = MaHaiQuan;
                if (dr["So_TK"] != DBNull.Value)
                    TKMD.SoToKhai = Convert.ToInt32(dr["So_TK"]);
                TKMD.MaLoaiHinh = dr["Ma_LH"].ToString();
                TKMD.NgayDangKy = Convert.ToDateTime(dr["Ngay_DK"]);
                TKMD.MaDoanhNghiep = MaDN;
                TKMD.TenDoanhNghiep = dr["Ten_DV"].ToString();
                TKMD.MaDaiLyTTHQ = dr["DaiLy_TTHQ"].ToString();
                TKMD.TenDaiLyTTHQ = dr["TenDLTTHQ"].ToString();
                TKMD.TenDonViDoiTac = dr["DV_DT"].ToString();
                TKMD.ChiTietDonViDoiTac = "";
                TKMD.SoGiayPhep = dr["So_GP"].ToString();
                if (dr["Ngay_GP"] != DBNull.Value)
                    TKMD.NgayGiayPhep = Convert.ToDateTime(dr["Ngay_GP"]);

                if (dr["Ngay_HHGP"] != DBNull.Value)
                    TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(dr["Ngay_HHGP"]);
                TKMD.SoHopDong = dr["So_HD"].ToString();
                if (dr["Ngay_HD"] != DBNull.Value)
                    TKMD.NgayHopDong = Convert.ToDateTime(dr["Ngay_HD"]);
                if (dr["Ngay_HHHD"] != DBNull.Value)
                    TKMD.NgayHetHanHopDong = Convert.ToDateTime(dr["Ngay_HHHD"]);
                TKMD.SoHoaDonThuongMai = dr["So_HDTM"].ToString();
                if (dr["Ngay_HDTM"] != DBNull.Value)
                    TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(dr["Ngay_HDTM"]);

                TKMD.PTVT_ID = dr["Ma_PTVT"].ToString();
                TKMD.SoHieuPTVT = dr["Ten_PTVT"].ToString();
                if (dr["NgayDen"] != DBNull.Value)
                    TKMD.NgayDenPTVT = Convert.ToDateTime(dr["NgayDen"]);
                TKMD.SoVanDon = dr["Van_Don"].ToString();
                if (dr["Ngay_VanDon"] != DBNull.Value)
                    TKMD.NgayVanDon = Convert.ToDateTime(dr["Ngay_VanDon"]);
                TKMD.NuocNK_ID = dr["Nuoc_NK"].ToString();
                TKMD.NuocXK_ID = dr["Nuoc_XK"].ToString();
                TKMD.DiaDiemXepHang = dr["CangNN"].ToString();
                TKMD.CuaKhau_ID = dr["Ma_CK"].ToString();
                TKMD.DKGH_ID = dr["Ma_GH"].ToString();
                TKMD.NguyenTe_ID = dr["Ma_NT"].ToString();
                TKMD.TyGiaTinhThue = Convert.ToDecimal(dr["TyGia_VND"]);
                if (dr["TyGia_USD"] != DBNull.Value)
                    TKMD.TyGiaUSD = Convert.ToDecimal(dr["TyGia_USD"]);
                TKMD.PTTT_ID = dr["Ma_PTTT"].ToString();
                TKMD.SoHang = Convert.ToInt16(dr["SoHang"]);
                TKMD.SoLuongPLTK = Convert.ToInt16(dr["So_PLTK"]);
                TKMD.TenChuHang = dr["TenCH"].ToString();
                if (dr["So_Container"] != DBNull.Value)
                    TKMD.SoContainer20 = Convert.ToDecimal(dr["So_Container"]);
                if (dr["So_Container40"] != DBNull.Value)
                    TKMD.SoContainer40 = Convert.ToDecimal(dr["So_Container40"]);
                if (dr["So_Kien"] != DBNull.Value)
                    TKMD.SoKien = Convert.ToDecimal(dr["So_Kien"]);
                if (dr["Tr_Luong"] != DBNull.Value)
                    TKMD.TrongLuong = Convert.ToDecimal(dr["Tr_Luong"]);
                if (dr["TongTGKB"] != DBNull.Value)
                    TKMD.TongTriGiaKhaiBao = Convert.ToDecimal(dr["TongTGKB"]);
                if (dr["TongTGTT"] != DBNull.Value)
                    TKMD.TongTriGiaTinhThue = Convert.ToDecimal(dr["TongTGTT"]);
                TKMD.LoaiToKhaiGiaCong = dr["LoaiTKGC"].ToString();
                if (dr["So_Container40"] != DBNull.Value)
                    TKMD.LePhiHaiQuan = Convert.ToDecimal(dr["So_Container40"]);
                if (dr["Phi_BH"] != DBNull.Value)
                    TKMD.PhiBaoHiem = Convert.ToDecimal(dr["Phi_BH"]);
                if (dr["Phi_VC"] != DBNull.Value)
                    TKMD.PhiVanChuyen = Convert.ToDecimal(dr["Phi_VC"]);
                if (dr["QLMay"] != DBNull.Value)
                    TKMD.QuanLyMay = Convert.ToBoolean(dr["QLMay"]);
                if (dr["TrangThaiXL"] != DBNull.Value)
                    TKMD.TrangThaiXuLy = Convert.ToInt32(dr["TrangThaiXL"]);
                TKMD.LoaiHangHoa = dr["Xuat_NPL_SP"].ToString();
                TKMD.GiayTo = dr["GiayTo"].ToString();
                TKMD.MaDonViUT = dr["Ma_DVUT"].ToString();
                TKMD.TenDonViUT = dr["Ten_DVUT"].ToString();


                TKMD.HMDCollection = new HangMauDichCollection();
                foreach (DataRow dr2 in dsDK_DHaiQuan.Tables[0].Rows)
                {
                    HangMauDich HMD = new HangMauDich();
                    HMD.SoThuTuHang = Convert.ToInt32(dr2["STTHang"]);
                    HMD.MaHS = dr2["MaHang_KB"].ToString();
                    HMD.MaPhu = dr2["Ma_NPL_SP"].ToString();
                    HMD.TenHang = dr2["Ten_Hang"].ToString();
                    HMD.NuocXX_ID = dr2["Nuoc_XX"].ToString();
                    HMD.DVT_ID = dr2["Ma_DVT"].ToString();
                    HMD.SoLuong = Convert.ToDecimal(dr2["Luong"]);
                    HMD.DonGiaKB = Convert.ToDecimal(dr2["DGia_KB"]);
                    HMD.DonGiaTT = Convert.ToDecimal(dr2["DGia_TT"]);
                    HMD.TriGiaKB = Convert.ToDecimal(dr2["TriGia_KB"]);
                    if (dr2["TriGia_TT"] != DBNull.Value)
                        HMD.TriGiaTT = Convert.ToDecimal(dr2["TriGia_TT"]);
                    if (dr2["TGKB_VND"] != DBNull.Value)
                        HMD.TriGiaKB_VND = Convert.ToDecimal(dr2["TGKB_VND"]);
                    if (dr2["TS_XNK"] != DBNull.Value)
                        HMD.ThueSuatXNK = Convert.ToDecimal(dr2["TS_XNK"]);
                    if (dr2["TS_TTDB"] != DBNull.Value)
                        HMD.ThueSuatTTDB = Convert.ToDecimal(dr2["TS_TTDB"]);
                    if (dr2["TS_VAT"] != DBNull.Value)
                        HMD.ThueSuatGTGT = Convert.ToDecimal(dr2["TS_VAT"]);
                    if (dr2["Thue_XNK"] != DBNull.Value)
                        HMD.ThueXNK = Convert.ToDecimal(dr2["Thue_XNK"]);
                    if (dr2["Thue_TTDB"] != DBNull.Value)
                        HMD.ThueTTDB = Convert.ToDecimal(dr2["Thue_TTDB"]);
                    if (dr2["Thue_VAT"] != DBNull.Value)
                        HMD.ThueGTGT = Convert.ToDecimal(dr2["Thue_VAT"]);
                    if (dr2["Phu_Thu"] != DBNull.Value)
                        HMD.PhuThu = Convert.ToDecimal(dr2["Phu_Thu"]);
                    if (dr2["TyLe_ThuKhac"] != DBNull.Value)
                        HMD.PhuThu = Convert.ToDecimal(dr2["TyLe_ThuKhac"]);
                    if (dr2["TriGia_ThuKhac"] != DBNull.Value)
                        HMD.TriGiaThuKhac = Convert.ToDecimal(dr2["TriGia_ThuKhac"]);
                    if (dr2["MienThue"] != DBNull.Value)
                        HMD.MienThue = Convert.ToByte(dr2["MienThue"]);
                    TKMD.HMDCollection.Add(HMD);
                }
                TKMD.InsertUpdateFull();
            }
        }
        #endregion

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có chắc chắn là cập nhật không ?", true) == "Yes")
            {
                if (txtDatabase.Text.Trim() == "SXXK")
                {
                    lblTrangthai.Text = "Đang đồng bộ dữ liệu .....";
                    backgroundWorker1.RunWorkerAsync(lblTrangthai);

                    //lblTrangthai.Text = "Định mức .....";
                    //updateDMSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //lblTrangthai.Text = "Tờ khai .....";
                    //updateToKhaiSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //lblTrangthai.Text = "Điều chỉnh .....";
                    //updateHangDieuChinhSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //lblTrangthai.Text = "Hồ sơ thanh lý.....";
                    //updateHSTLSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //lblTrangthai.Text = "Nguyên phụ liệu Tồn.....";
                    //updateNPLTonSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //lblTrangthai.Text = "Nguyên phụ liệu Tồn TL.....";
                    //updateNPLTonTLSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                    //MessageBox.Show("Cập nhật xong dữ liệu", "Đồng bộ dữ liệu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //lblTrangthai.Text = "";
                }
            }
        }

     
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DongBoDuLieuByDNForm_Load(object sender, EventArgs e)
        {
            this.step = 1;
            timer1.Enabled = true;
            timer1.Start();
            lblTrangthai.Text = " ";
            txtMaHQ.Text = GlobalSettings.MA_HAI_QUAN;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtDatabase.Text = "SXXK";
            if (txtDatabase.Text.Trim() == "SXXK")
            {
                lblTrangthai.Text = "Đang đồng bộ dữ liệu từ hệ thống Hải quan...";
                backgroundWorker1.RunWorkerAsync(lblTrangthai);
            }

        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            try
            {
                updateNPLSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                updateSPSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                UpdateNPLKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                UpdateSPKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                UpdateDMKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                UpdateTKKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                if (chkErrConn == true)
                    ShowMessage("Không kết nối được với hệ thống chi cục Hải quan, đồng bộ dữ liệu không thành công", false);
                //updateDMSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
            }
            catch { }
        }
        private int step = 1;
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.step = 5;
            this.value = 100;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (uiProgressBar1.Value == 100) 
                this.Close();
            if (this.value < 100)
            {
                this.value += this.step;
                if (value > 99 && step == 1)
                    value = 99;
                else if (step > 1)
                    value = 100;

            }
            else
            {
                if (uiProgressBar1.Value < 99) this.value = uiProgressBar1.Value + 1;
            }
            uiProgressBar1.Value = this.value;

        }

        private void uiProgressBar1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void LoadDongBoDuLieuForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uiProgressBar1.Value < 100)
                e.Cancel = true;
        }
    }
}

