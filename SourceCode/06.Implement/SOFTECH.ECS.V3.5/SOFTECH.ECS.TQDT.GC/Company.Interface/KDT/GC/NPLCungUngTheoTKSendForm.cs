using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using System.IO;
using Janus.Windows.GridEX;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class NPLCungUngTheoTKSendForm : BaseForm
    {
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public ToKhaiMauDich TKMD = null;
        public ToKhaiChuyenTiep TKCT = null;
        public HopDong HD = new HopDong();
        public bool boolFlag = false;
        public NPLCungUngTheoTKSendForm()
        {
            InitializeComponent();
        }

        private void txtSoTiepNhanTK_ButtonClick(object sender, EventArgs e)
        {
            if (BKCU.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                showMsg("MSG_2702059");
                //MLMessages("Danh sách này đã được khai báo tới hải quan.Hãy hủy khai báo trước khi thay đổi thông tin.","MSG_STN08","", false);
                return;
            }
            ToKhaiMauDichDaDuyetForm f = new ToKhaiMauDichDaDuyetForm();
            f.IdHopDong = Convert.ToInt64(cbHopDong.Value.ToString());
            f.ShowDialog();
            //HopDong HD=new HopDong();  
            HD = new HopDong();
            if (BKCU.ID > 0)
            {
                if (TKMD != null)
                    HD.ID = TKMD.IDHopDong;
                else
                    HD.ID = TKCT.IDHopDong;
                HD = HopDong.Load(HD.ID);

                foreach (SanPhanCungUng spCungUng in BKCU.SanPhamCungUngCollection)
                {
                    if (spCungUng.ID > 0)
                        spCungUng.Delete(HD);
                }
                BKCU.SanPhamCungUngCollection.Clear();
                try
                {
                    dgList.DataSource = BKCU.SanPhamCungUngCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }

            if (f.Tkmd != null)
            {
                this.BKCU.TKCT_ID = 0;
                this.BKCU.TKMD_ID = f.Tkmd.ID;
                txtSoTiepNhanTK.Text = f.Tkmd.SoTiepNhan + "";
                txtNamDangKyTK.Text = f.Tkmd.NgayTiepNhan.Year + "";
                txtLoaiHinh.Text = this.LoaiHinhMauDich_GetName(f.Tkmd.MaLoaiHinh);
                txtSoHopDong.Text = f.Tkmd.SoHopDong + " (" + f.Tkmd.NgayHopDong.ToString("dd/MM/yyyy") + ")";
                this.TKMD = f.Tkmd;
                this.TKCT = null;
                this.TKMD.LoadHMDCollection();
                grbToKhai.Text = setText("Tờ khai xuất gia công", "Export Proccessing Declaration");
            }
            else if (f.Tkct != null)
            {
                this.BKCU.TKMD_ID = 0;
                this.BKCU.TKCT_ID = f.Tkct.ID;
                txtSoTiepNhanTK.Text = f.Tkct.SoTiepNhan + "";
                txtNamDangKyTK.Text = f.Tkct.NgayTiepNhan.Year + "";
                txtLoaiHinh.Text = LoaiHinhChuyenTiep_GetName(f.Tkct.MaLoaiHinh);

                txtSoHopDong.Text = f.Tkct.SoHopDongDV + " (" + f.Tkct.NgayHDDV.ToString("dd/MM/yyyy") + ")";
                this.TKCT = f.Tkct;
                this.TKCT.LoadHCTCollection();
                this.TKMD = null;
                grbToKhai.Text = setText("Tờ khai chuyển tiếp xuất sản phẩm", " Product Forward Declaration");
            }

        }
        private void BindHopDong()
        {
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format("MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
            }
            collection.Add(hd);
            cbHopDong.DataSource = collection;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            try
            {
                cbHopDong.SelectedIndex = 0;
            }
            catch { }
        }
        private void NPLCungUngTheoTKSendForm_Load(object sender, EventArgs e)
        {
            BindHopDong();
            dgList.Tables[0].Columns["LuongXK"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongCUSanPham"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            if (BKCU.ID == 0 && this.BKCU.SanPhamCungUngCollection == null)
                this.BKCU.SanPhamCungUngCollection = new SanPhanCungUngCollection();
            else
            {
                if (BKCU.ID > 0)
                    BKCU.LoadSanPhamCungUngCollection();
                if (TKMD != null)
                {
                    TKMD.Load();
                    TKMD.LoadHMDCollection();
                    if (TKMD.SoTiepNhan > 0)
                    {
                        txtNamDangKyTK.Text = TKMD.NgayTiepNhan.Year.ToString();
                        txtSoTiepNhanTK.Text = TKMD.SoTiepNhan.ToString();
                    }
                    txtSoHopDong.Text = TKMD.SoHopDong;
                    txtLoaiHinh.Text = this.LoaiHinhMauDich_GetName(TKMD.MaLoaiHinh);
                    grbToKhai.Text = setText("Tờ khai xuất gia công", "Export Proccessing Declaration");
                    TKCT = null;
                    cbHopDong.Value = TKMD.IDHopDong;
                }
                else
                {
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    TKCT.LoadHCTCollection();
                    txtNamDangKyTK.Text = TKCT.NgayTiepNhan.Year.ToString();
                    txtSoHopDong.Text = TKCT.SoHopDongDV;
                    txtSoTiepNhanTK.Text = TKCT.SoTiepNhan.ToString();
                    txtLoaiHinh.Text = LoaiHinhChuyenTiep_GetName(TKCT.MaLoaiHinh);
                    grbToKhai.Text = setText("Tờ khai chuyển tiếp xuất sản phẩm", "Transition Declaration export product");
                    TKMD = null;
                    cbHopDong.Value = TKCT.IDHopDong;
                }

                txtSoTiepNhan.Text = BKCU.SoTiepNhan.ToString();
                txtSoBangke.Text = BKCU.SoBangKe.ToString();
            }
            setCommandStatus();
            dgList.DataSource = this.BKCU.SanPhamCungUngCollection;
            if (BKCU.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                if (sendXML.Load())
                {
                    cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    lblTrangThai.Text = setText("Chờ xác nhận thông tin từ hải quan", "Wait for confirmation ");
                }
            }
            else
            {
                cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            //
            if (boolFlag == true)
            {
                cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                txtSoHopDong.Text = this.HD.SoHopDong;

            }

        }

        private void cmMainNPLCungUng_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            switch (e.Command.Key)
            {
                case "cmdSave":
                    Save();
                    break;
                case "cmdAddSP":
                    ShowThemSPCungUngForm();
                    break;
                case "cmdKhaiBao":
                    send();
                    break;
                case "cmdXacNhap":
                    LaySoTiepNhanDT();
                    break;
                case "cmdNhanDuLieu":
                    NhanDuLieuPK();
                    break;
                case "cmdHuy":
                    Huy();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.BKCU.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "NGUYÊN PHỤ LIỆU CUNG ỨNG";
            phieuTN.soTN = this.BKCU.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.BKCU.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = BKCU.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }

        private void Save()
        {
            try
            {
                if (BKCU.SanPhamCungUngCollection.Count == 0)
                {
                    showMsg("MSG_2702013");
                    //MLMessages("Bạn chưa chọn sản phẩm để khai cung ứng.", "MSG_WRN11", "sản phẩm", false);
                    return;
                }
                if (BKCU.TKCT_ID > 0)
                {
                    TKCT.ID = BKCU.TKCT_ID;
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);

                    HD.ID = TKCT.IDHopDong;
                    HD = HopDong.Load(HD.ID);

                }
                else
                {
                    TKMD.ID = BKCU.TKMD_ID;
                    TKMD.Load();
                    HD.ID = TKMD.IDHopDong;
                    HD = HopDong.Load(HD.ID);

                }
                this.BKCU.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.BKCU.MaHaiQuan = HD.MaHaiQuan;
                //this.BKCU.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (boolFlag == true)
                {
                    BKCU.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    boolFlag = false;
                }
                else
                    BKCU.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                if (string.IsNullOrEmpty(BKCU.GUIDSTR)) BKCU.GUIDSTR = Guid.NewGuid().ToString();
                this.BKCU.InsertUpdateFull();
                showMsg("MSG_SAV02");
                //MLMessages("Lưu thành công.","MSG_SAV02","", false);
                setCommandStatus();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi: "+ ex.Message, false);
            }

        }

        private void ShowThemSPCungUngForm()
        {

            if (txtSoTiepNhanTK.Text.Trim().Length == 0)
                showMsg("MSG_2702014");
            //MLMessages("Bạn hãy chọn tờ khai xuất gia công.","MSG_WRN11","Expord Declaration", false);
            else
            {
                SanPhamCungUngForm f = new SanPhamCungUngForm();
                f.BKCU = this.BKCU;
                if (this.TKMD != null)
                {
                    f.TKMD = this.TKMD;
                    f.TKCT = null;
                }
                else
                {
                    f.TKCT = this.TKCT;
                    f.TKMD = null;
                }
                f.ShowDialog();
                if (f.SPCU.MaSanPham != "")
                {
                    this.BKCU.SanPhamCungUngCollection.Add(f.SPCU);
                }
                dgList.DataSource = this.BKCU.SanPhamCungUngCollection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }
        private HangMauDich GetHMDCuaToKhaiByMaHang(string maHang)
        {
            TKMD.LoadHMDCollection();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private HangChuyenTiep GetHCTCuaToKhaiByMaHang(string maHang)
        {
            TKCT.LoadHCTCollection();
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            string maSP = e.Row.Cells["MaSanPham"].Text;
            if (TKMD != null)
            {
                HangMauDich hmd = GetHMDCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.DVT_ID);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
            else
            {
                HangChuyenTiep hmd = GetHCTCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.ID_DVT);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
        }

        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_WRN05");
                    //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.Chọn chức năng xác nhận để xác nhận thông tin với hải quan.","MSG_STN03","", false);
                    return;
                }
            }

            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = BKCU.WSCancel(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void NhanDuLieuPK()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","", false);
                    return;
                }
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = BKCU.WSRequest(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu định mức cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void send()
        {
            if (TKMD != null)
            {
                TKMD.Load();
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    showMsg("MSG_2702060");
                    //MLMessages("Phải khai báo tờ khai xuất trước khi khai báo cung ứng.","MSG_SEN13","", false);
                    return;
                }
            }
            else if (TKCT != null)
            {
                TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    showMsg("MSG_2702061");
                    //MLMessages("Phải khai báo tờ khai chuyển tiếp trước khi khai báo cung ứng.","MSG_SEN14","", false);
                    return;
                }
            }
            else
            {
                showMsg("MSG_2702062");
                //MLMessages("Hãy chọn tờ khai để khai cung ứng.", "MSG_WRN11", "tờ khai", false);
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Định mức cung ứng này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","", false);
                    return;
                }
            }

            if (BKCU.SanPhamCungUngCollection.Count == 0)
            {
                showMsg("MSG_2702063");
                //MLMessages("Chưa nhập sản phẩm cung ứng","MSG_WRN19","", false);
                return;
            }
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = BKCU.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo NPL cung ứng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //private void SendV3()
        //{
        //    if (TKMD != null)
        //    {
        //        TKMD.Load();
        //        if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
        //        {
        //            showMsg("MSG_2702060");
        //            //MLMessages("Phải khai báo tờ khai xuất trước khi khai báo cung ứng.","MSG_SEN13","", false);
        //            return;
        //        }
        //    }
        //    else if (TKCT != null)
        //    {
        //        TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
        //        if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
        //        {
        //            showMsg("MSG_2702061");
        //            //MLMessages("Phải khai báo tờ khai chuyển tiếp trước khi khai báo cung ứng.","MSG_SEN14","", false);
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        showMsg("MSG_2702062");
        //        //MLMessages("Hãy chọn tờ khai để khai cung ứng.", "MSG_WRN11", "tờ khai", false);
        //        return;
        //    }

        //    MsgSend sendXML = new MsgSend();
        //    sendXML.LoaiHS = "DMCU";
        //    sendXML.master_id = BKCU.ID;
        //    if (sendXML.Load())
        //    {
        //        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
        //        cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //        cmdNhanDuLieu.Enabled = cmdNhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //        return;
        //    }
        //    try
        //    {
        //        if (this.BKCU.DMCollection.Count > 0)
        //        {
        //            // Master.
        //            int s = CheckNPLVaSPDuyet();
        //            if (s != 2)
        //            {
        //                string msg = "";
        //                if (s == 0)
        //                    msg = "Trong danh sách này có nguyên phụ liệu chưa được hải quan duyệt.";
        //                else msg = "Trong danh sách này có sản phẩm chưa được hải quan duyệt. ";
        //                msg += "\n Bạn có muốn gửi lên hay không ?";

        //                string kq = MLMessages(msg, "MSG_PUB20", "", true);
        //                if (kq != "Yes")
        //                    return;
        //            }
        //            this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
        //            this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
        //            this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
        //            string returnMessage = string.Empty;
        //            dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
        //            SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy, false, GlobalSettings.TEN_DON_VI);
        //            //string msgSend = Helpers.BuildSend(
        //            //               new NameBase()
        //            //               {
        //            //                   Name = GlobalSettings.TEN_DON_VI,
        //            //                   Identity = dmDangKy.MaDoanhNghiep
        //            //               }
        //            //                 , new NameBase()
        //            //                 {
        //            //                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
        //            //                     Identity = dmDangKy.MaHaiQuan
        //            //                 }
        //            //              ,
        //            //                new SubjectBase()
        //            //                {
        //            //                    Type = DeclarationIssuer.SXXK_DINHMUC_SP,
        //            //                    Function = DeclarationFunction.KHAI_BAO,
        //            //                    Reference = dmDangKy.GUIDSTR,
        //            //                }
        //            //                ,
        //            //                dmSp, null, true);
        //            ObjectSend msgSend = new ObjectSend(
        //                 new NameBase()
        //                 {
        //                     Name = GlobalSettings.TEN_DON_VI,
        //                     Identity = dmDangKy.MaDoanhNghiep
        //                 },
        //                  new NameBase()
        //                  {
        //                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
        //                      Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
        //                      //Identity = dmDangKy.MaHaiQuan
        //                  },
        //                  new SubjectBase()
        //                  {
        //                      Type = dmSp.Issuer,
        //                      Function = DeclarationFunction.KHAI_BAO,
        //                      Reference = dmDangKy.GUIDSTR,
        //                  },
        //                  dmSp
        //                );
        //            dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
        //            SendMessageForm sendForm = new SendMessageForm();
        //            sendForm.Send += SendMessage;
        //            bool isSend = sendForm.DoSend(msgSend);
        //            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
        //            {
        //                sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
        //                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //                cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //                btnDelete.Enabled = false;
        //                sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
        //                sendXML.master_id = dmDangKy.ID;
        //                // sendXML.msg = msgSend;
        //                sendXML.func = 1;
        //                sendXML.InsertUpdate();
        //                dmDangKy.TransferDataToSXXK();
        //                dmDangKy.Update();
        //                FeedBackV3();
        //            }
        //            else if (!string.IsNullOrEmpty(msgInfor))
        //                ShowMessageTQDT(msgInfor, false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        
        //    }
        //}
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            {
                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Định mức cung ứng không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN03","", false);
                    return;
                }
            }
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = BKCU.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", BKCU.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + BKCU.SoTiepNhan,"MSG_SEN02",BKCU.SoTiepNhan.ToString(), false);
                    lblTrangThai.Text = "Chờ duyệt";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait to approve";
                    txtSoTiepNhan.Text = BKCU.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702058");
                    //MLMessages("Đã hủy định mức cung ứng này","MSG_CAN01","", false);
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not declared yet";
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (BKCU.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN11", new string[] { BKCU.SoBangKe.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số chứng từ : "+BKCU.SoBangKe.ToString(),"MSG_SEN03",BKCU.SoBangKe.ToString(), false);
                        txtSoBangke.Text = BKCU.SoBangKe.ToString();
                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Approved";
                    }
                    else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_SEN04","", false);
                    }
                    else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);
                        lblTrangThai.Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not approved";
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin NPL cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            try
            {

                sendXML.LoaiHS = "DMCU";
                sendXML.master_id = BKCU.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                //DATLMQ bổ sung message lấy phản hồi theo chuẩn TNTT 09/01/2011
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, BKCU.GUIDSTR.Trim()));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                //this.Update();
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = BKCU.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);
                xmlCurrent = BKCU.LayPhanHoi(pass, xml.InnerXml);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        else
                        {
                            cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", BKCU.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + BKCU.SoTiepNhan,"MSG_SEN02",BKCU.SoTiepNhan.ToString(),false);
                    lblTrangThai.Text = "Chờ duyệt";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for approval";
                    txtSoTiepNhan.Text = BKCU.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702041");
                    //MLMessages("Đã hủy danh sách định mức cung ứng này","MSG_CAN01","", false);                    
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not declared yet";
                }
                else if (sendXML.func == 2)
                {
                    if (BKCU.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN11", new string[] { BKCU.SoBangKe.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số chứng từ : " + BKCU.SoBangKe.ToString(),"MSG_SEN03",BKCU.SoBangKe.ToString(), false);
                        txtSoBangke.Text = BKCU.SoBangKe.ToString();
                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Approved";
                    }
                    else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_STN06","", false);
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin npl cung ứng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void setCommandStatus()
        {
            if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = "Chưa khai báo";
                if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not declared yet";
            }
            else if (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = "Chờ duyệt";
                if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for approval";
            }
            else
            {
                cmdHuy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdNhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddSP.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXacNhap.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = "Đã duyệt";
                if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Approved";
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhanCungUng SPCU = (SanPhanCungUng)e.Row.DataRow;
                SanPhamCungUngEditForm f = new SanPhamCungUngEditForm();
                f.BKCU = this.BKCU;
                f.SPCU = SPCU;
                if (SPCU.NPLCungUngCollection == null || SPCU.NPLCungUngCollection.Count == 0)
                {
                    SPCU.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
                    SPCU.LoadNPLCungUngCollection();
                }
                f.ShowDialog();
                SPCU = f.SPCU;
            }
        }

        private void NPLCungUngTheoTKSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            HopDong HD = new HopDong();
            if (BKCU.TKCT_ID > 0)
            {
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                HD.ID = TKMD.IDHopDong;
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa các sản phẩm này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhanCungUng spCungUng = (SanPhanCungUng)i.GetRow().DataRow;
                        if (spCungUng.ID > 0)
                            spCungUng.Delete(HD);
                    }
                }
            }
            else
                e.Cancel = true;
        }

        private void grbToKhai_Click(object sender, EventArgs e)
        {

        }

        private void txtSoTiepNhanTK_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLayTuPhanBo_Click(object sender, EventArgs e)
        {
            try
            {
                if (BKCU.SanPhamCungUngCollection.Count > 0)
                {
                    string rel = ShowMessage("Chương trình sẽ tự động xóa dữ liệu đang có trên lưới để lấy từ bảng phân bổ.Bạn có muốn tiếp tục không ?", true);
                    if (rel != "Yes")
                        return;

                    foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                        spCU.Delete(HD);
                    BKCU.SanPhamCungUngCollection.Clear();
                }
                long id = 0;
                if (TKMD != null)
                    id = TKMD.ID;
                else
                    id = TKCT.ID;
                HopDong hd = new HopDong();
                hd.ID = Convert.ToInt64(cbHopDong.Value);
                hd = HopDong.Load(hd.ID);
                BKCungUngDangKy.GetSanPhamCungUngByPhanBo(id, BKCU, hd, GlobalSettings.SoThapPhan.LuongSP);
                dgList.DataSource = BKCU.SanPhamCungUngCollection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            catch (Exception exxx)
            {
                ShowMessage(" Lỗi : " + exxx.Message, false);
            }
        }
    }
}