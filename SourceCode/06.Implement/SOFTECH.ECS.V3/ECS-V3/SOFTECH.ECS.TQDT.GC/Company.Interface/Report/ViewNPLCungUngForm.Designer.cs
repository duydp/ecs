﻿namespace Company.Interface.Report
{
    partial class ViewNPLCungUngForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewNPLCungUngForm));
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.grp1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPage = new Janus.Windows.EditControls.UIComboBox();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtTo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFrom = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnPrintAll = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grp1)).BeginInit();
            this.grp1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grp1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(805, 334);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2007 Blue";
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(12, 62);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(781, 260);
            this.printControl1.TabIndex = 1;
            // 
            // grp1
            // 
            this.grp1.BackColor = System.Drawing.Color.Transparent;
            this.grp1.Controls.Add(this.txtTo);
            this.grp1.Controls.Add(this.label3);
            this.grp1.Controls.Add(this.label2);
            this.grp1.Controls.Add(this.txtFrom);
            this.grp1.Controls.Add(this.btnPrintAll);
            this.grp1.Controls.Add(this.label1);
            this.grp1.Controls.Add(this.cbPage);
            this.grp1.Controls.Add(this.btnExportExcel);
            this.grp1.Controls.Add(this.btnPrint);
            this.grp1.Location = new System.Drawing.Point(12, 3);
            this.grp1.Name = "grp1";
            this.grp1.Size = new System.Drawing.Size(781, 53);
            this.grp1.TabIndex = 2;
            this.grp1.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 289;
            this.label1.Text = "Chọn sản phẩm";
            // 
            // cbPage
            // 
            this.cbPage.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPage.Location = new System.Drawing.Point(101, 19);
            this.cbPage.Name = "cbPage";
            this.cbPage.Size = new System.Drawing.Size(207, 21);
            this.cbPage.TabIndex = 288;
            this.cbPage.VisualStyleManager = this.vsmMain;
            this.cbPage.SelectedIndexChanged += new System.EventHandler(this.cbPage_SelectedIndexChanged);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("btnExportExcel.Icon")));
            this.btnExportExcel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcel.Location = new System.Drawing.Point(642, 20);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(86, 23);
            this.btnExportExcel.TabIndex = 287;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyleManager = this.vsmMain;
            this.btnExportExcel.WordWrap = false;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("btnPrint.Icon")));
            this.btnPrint.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnPrint.Location = new System.Drawing.Point(314, 18);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(60, 23);
            this.btnPrint.TabIndex = 286;
            this.btnPrint.Text = "In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.WordWrap = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtTo
            // 
            this.txtTo.DecimalDigits = 0;
            this.txtTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTo.FormatString = "#####";
            this.txtTo.Location = new System.Drawing.Point(516, 20);
            this.txtTo.MaxLength = 5;
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(36, 21);
            this.txtTo.TabIndex = 294;
            this.txtTo.Text = "1";
            this.txtTo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTo.Value = 1;
            this.txtTo.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTo.VisualStyleManager = this.vsmMain;
            this.txtTo.TextChanged += new System.EventHandler(this.txtTo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(498, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 17);
            this.label3.TabIndex = 293;
            this.label3.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(393, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 292;
            this.label2.Text = "Sản phẩm";
            // 
            // txtFrom
            // 
            this.txtFrom.DecimalDigits = 0;
            this.txtFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrom.FormatString = "#####";
            this.txtFrom.Location = new System.Drawing.Point(459, 20);
            this.txtFrom.MaxLength = 5;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(36, 21);
            this.txtFrom.TabIndex = 291;
            this.txtFrom.Text = "1";
            this.txtFrom.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtFrom.Value = 1;
            this.txtFrom.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtFrom.VisualStyleManager = this.vsmMain;
            this.txtFrom.TextChanged += new System.EventHandler(this.txtFrom_TextChanged);
            // 
            // btnPrintAll
            // 
            this.btnPrintAll.Icon = ((System.Drawing.Icon)(resources.GetObject("btnPrintAll.Icon")));
            this.btnPrintAll.Location = new System.Drawing.Point(561, 19);
            this.btnPrintAll.Name = "btnPrintAll";
            this.btnPrintAll.Size = new System.Drawing.Size(75, 23);
            this.btnPrintAll.TabIndex = 290;
            this.btnPrintAll.Text = "In &tất cả";
            this.btnPrintAll.VisualStyleManager = this.vsmMain;
            this.btnPrintAll.Click += new System.EventHandler(this.btnPrintAll_Click);
            // 
            // ViewNPLCungUngForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 334);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ViewNPLCungUngForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng tổng hợp nguyên liệu do bên gia công cung ứng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PreviewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grp1)).EndInit();
            this.grp1.ResumeLayout(false);
            this.grp1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox grp1;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbPage;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtFrom;
        private Janus.Windows.EditControls.UIButton btnPrintAll;
    }
}