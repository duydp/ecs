using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.QuanTri
{
	public partial class User : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string USER_NAME { set; get; }
		public string PASSWORD { set; get; }
		public string HO_TEN { set; get; }
		public string MO_TA { set; get; }
		public bool isAdmin { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<User> ConvertToCollection(IDataReader reader)
		{
			List<User> collection = new List<User>();
			while (reader.Read())
			{
				User entity = new User();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) entity.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) entity.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
				if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) entity.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) entity.MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
				if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) entity.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<User> collection, long id)
        {
            foreach (User item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO User VALUES(@USER_NAME, @PASSWORD, @HO_TEN, @MO_TA, @isAdmin)";
            string update = "UPDATE User SET USER_NAME = @USER_NAME, PASSWORD = @PASSWORD, HO_TEN = @HO_TEN, MO_TA = @MO_TA, isAdmin = @isAdmin WHERE ID = @ID";
            string delete = "DELETE FROM User WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO User VALUES(@USER_NAME, @PASSWORD, @HO_TEN, @MO_TA, @isAdmin)";
            string update = "UPDATE User SET USER_NAME = @USER_NAME, PASSWORD = @PASSWORD, HO_TEN = @HO_TEN, MO_TA = @MO_TA, isAdmin = @isAdmin WHERE ID = @ID";
            string delete = "DELETE FROM User WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static User Load(long id)
		{
			const string spName = "[dbo].[p_User_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<User> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<User> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<User> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_User_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_User_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_User_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_User_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertUser(string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin)
		{
			User entity = new User();	
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_User_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateUser(long id, string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin)
		{
			User entity = new User();			
			entity.ID = id;
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_User_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateUser(long id, string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin)
		{
			User entity = new User();			
			entity.ID = id;
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_User_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteUser(long id)
		{
			User entity = new User();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_User_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_User_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}