using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private DataTable DtGhiChu = new DataTable();
        private int SoToKhaiNhap = 0;

        public BCNPLXuatNhapTon()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = GlobalSettings.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else  if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else  if (sapxeptkx == 0)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else  if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC"; 

            }


            DataTable dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");               
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");
            xrLabel76.Text = xrLabel77.Text = GlobalSettings.TieudeNgay;
        }
        private decimal temp = 0;
        private void lblMaSP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (GlobalSettings.MA_DON_VI == "0400101242")//Rieng cong ty Luoi
            //{
            //    string[] temp = GetCurrentColumnValue("TenSP").ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //    if (temp.Length > 0)
            //        lblMaSP.Text = temp[0] + " " + GetCurrentColumnValue("MaSP").ToString();
            //}
            //else
            //{
            lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
                lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();

            //}
        }



        private void lblSoToKhaiTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0)
                lblSoToKhaiTaiXuat1.Text = "";

        }

        private void lblLuongNPLTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblLuongNPLTaiXuat1.Text = "";

        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblToKhaiNhap.Text = "Tổng lượng tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString(); ;
        }

        private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.DtGhiChu = new DataTable();
            DataColumn[] cols = new DataColumn[3];
            cols[0] = new DataColumn("MaNPL", typeof(string));
            cols[1] = new DataColumn("From", typeof(int));
            cols[2] = new DataColumn("To", typeof(int));
            this.DtGhiChu.Columns.AddRange(cols);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            if (lblSoToKhaiNhap.Text != "")
                SoToKhaiNhap = Convert.ToInt32(lblSoToKhaiNhap.Text);
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT.Text = this.STT + "";
        }

        private void lblSoToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) > 0) ((XRTableCell)sender).Text = "";
            if (((XRTableCell)sender).Name == "lblLuongSPXuat")
            {
                if (Convert.ToDecimal(GetCurrentColumnValue("LuongSPXuat")) == 0) ((XRTableCell)sender).Text = "";
            }
        }

        private void lblSoToKhaiTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToInt32(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0) lblSoToKhaiTaiXuat.Text = "";


        }

        private void lblNgayTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(GetCurrentColumnValue("NgayTaiXuat")).Year == 1900) lblNgayTaiXuat.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0) lblLuongNPLTaiXuat.Text = "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("MaSP").ToString() == "") e.Cancel = true;
        }

        private void lblThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((temp - Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"))) != Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")))
            {
                int from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap")));
                if (from > 0)
                    lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
                else
                    lblThanhKhoanTiep.Text = "";
            }
            else
                lblThanhKhoanTiep.Text = "";
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        private int GetFrom(string maNPL, int to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToInt32(dr["To"]))
                    return Convert.ToInt32(dr["From"]);
            }
            return 0;
        }

        private void lblTongThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongThanhKhoanTiep.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToInt32(lblTongThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }
        }
    }
}
