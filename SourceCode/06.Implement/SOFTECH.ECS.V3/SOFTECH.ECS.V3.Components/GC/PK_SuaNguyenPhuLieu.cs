﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PK_SuaNguyenPhuLieu
    {
        /// <summary>
        /// ConfigMaterial
        /// </summary>
        /// <param name="Material">XElement</param>
        /// <param name="NPL">NguyenPhuLieu</param>
        /// <returns>XElement</returns>
        public static XElement ConfigMaterial(XElement Material, NguyenPhuLieu NPL)
        {
            if (Material != null && Material.Elements().Count() > 0 && NPL != null)
            {
                DonViTinh DVT = DonViTinh.Load(NPL.DVT_ID);
                foreach (XElement element in Material.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.preIdentification.ToUpper()))
                        element.SetValue("");
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.description.ToUpper()))
                                elementChild.SetValue(NPL.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identification.ToUpper()))
                                elementChild.SetValue(NPL.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.tariffClassification.ToUpper()))
                                elementChild.SetValue(NPL.MaHS);
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }
                }
            }
            return Material;
        }
        /// <summary>
        /// ConfigPK_SuaNguyenPhuLieu
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigPK_SuaNguyenPhuLieu(PhuKienDangKy PKDK, int issuer, int function, int status)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/PK_SuaNguyenPhuLieu.xml");
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                if (HD != null && docXML != null)
                {
                    IList<NguyenPhuLieu> lNPL = NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                    XElement Root = docXML.Root;
                    PhuKienChung.ConfigPhuKien(Root, PKDK, issuer, function, status);

                    XElement AdditionalInformation = Root.Element(NodePhuKien.AdditionalInformation);
                    XElement Content = AdditionalInformation.Element(NodePhuKien.Content);
                    XElement Material = Content.Element(NodePhuKien.Material);
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        // Check Upper
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                AdditionalInformation = element;
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                    {
                                        Content = elementChild;
                                        foreach (XElement Child in elementChild.Elements())
                                        {
                                            if (Child.Name.ToString().ToUpper().Equals(NodePhuKien.Material.ToUpper()))
                                                Material = Child;
                                        }
                                    }
                                }
                            }
                        }
                        if (lNPL != null)
                        {
                            if (lNPL.Count == 1)
                            {
                                foreach (XElement element in Material.Elements())
                                {
                                    ConfigMaterial(Material, lNPL[0]);
                                }
                            }
                            else if (lNPL.Count > 1)
                            {
                                XElement TempMaterial = new XElement(Material);
                                ConfigMaterial(Material, lNPL[0]);
                                // Add new Material
                                for (int i = 1; i < lNPL.Count; i++)
                                {
                                    XElement NewMaterial = new XElement(TempMaterial);
                                    Content.Add(ConfigMaterial(NewMaterial, lNPL[i]));
                                }
                            }
                        }
                        docXML.Save(@"D:\NewPK_SuaNguyenPhuLieu.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }

        }
    }
}
