﻿namespace Company.Interface
{
    partial class VNACC_TK_TKTriGiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TK_TKTriGiaForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaPhanLoaiTongGiaCoBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaTTPhiVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTPhiBH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiPhiBH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiPhiVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTHieuChinhTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtChiTietKhaiTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongPhapDieuChinhTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaPhanLoaiDieuChinhTriGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhanLoaiCongThucChuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoDangKyBH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTongHeSoPhanBoTG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtGiaHieuChinhTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTiepNhanTKTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox)).BeginInit();
            this.uiGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 440), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 440);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 416);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 416);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(667, 440);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 403);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(667, 37);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(587, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(425, 6);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox
            // 
            this.uiGroupBox.AutoScroll = true;
            this.uiGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox.Controls.Add(this.txtMaPhanLoaiTongGiaCoBan);
            this.uiGroupBox.Controls.Add(this.ctrMaTTPhiVC);
            this.uiGroupBox.Controls.Add(this.ctrMaTTPhiBH);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiPhiBH);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiPhiVC);
            this.uiGroupBox.Controls.Add(this.ctrMaTTHieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.ctrMaPhanLoaiTriGia);
            this.uiGroupBox.Controls.Add(this.grList);
            this.uiGroupBox.Controls.Add(this.label7);
            this.uiGroupBox.Controls.Add(this.label10);
            this.uiGroupBox.Controls.Add(this.label3);
            this.uiGroupBox.Controls.Add(this.label4);
            this.uiGroupBox.Controls.Add(this.label12);
            this.uiGroupBox.Controls.Add(this.label11);
            this.uiGroupBox.Controls.Add(this.label9);
            this.uiGroupBox.Controls.Add(this.label2);
            this.uiGroupBox.Controls.Add(this.label6);
            this.uiGroupBox.Controls.Add(this.txtChiTietKhaiTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhuongPhapDieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtMaPhanLoaiDieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhanLoaiCongThucChuan);
            this.uiGroupBox.Controls.Add(this.label8);
            this.uiGroupBox.Controls.Add(this.txtSoDangKyBH);
            this.uiGroupBox.Controls.Add(this.label5);
            this.uiGroupBox.Controls.Add(this.label1);
            this.uiGroupBox.Controls.Add(this.txtTongHeSoPhanBoTG);
            this.uiGroupBox.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox.Controls.Add(this.label16);
            this.uiGroupBox.Controls.Add(this.txtGiaHieuChinhTriGia);
            this.uiGroupBox.Controls.Add(this.txtSoTiepNhanTKTriGia);
            this.uiGroupBox.Controls.Add(this.txtPhiVanChuyen);
            this.uiGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox.Name = "uiGroupBox";
            this.uiGroupBox.Size = new System.Drawing.Size(667, 403);
            this.uiGroupBox.TabIndex = 0;
            this.uiGroupBox.VisualStyleManager = this.vsmMain;
            // 
            // txtMaPhanLoaiTongGiaCoBan
            // 
            this.txtMaPhanLoaiTongGiaCoBan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPhanLoaiTongGiaCoBan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPhanLoaiTongGiaCoBan.Enabled = false;
            this.txtMaPhanLoaiTongGiaCoBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhanLoaiTongGiaCoBan.Location = new System.Drawing.Point(608, 166);
            this.txtMaPhanLoaiTongGiaCoBan.MaxLength = 12;
            this.txtMaPhanLoaiTongGiaCoBan.Name = "txtMaPhanLoaiTongGiaCoBan";
            this.txtMaPhanLoaiTongGiaCoBan.ReadOnly = true;
            this.txtMaPhanLoaiTongGiaCoBan.Size = new System.Drawing.Size(54, 21);
            this.txtMaPhanLoaiTongGiaCoBan.TabIndex = 74;
            this.txtMaPhanLoaiTongGiaCoBan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPhanLoaiTongGiaCoBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTPhiVC
            // 
            this.ctrMaTTPhiVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTPhiVC.Code = "";
            this.ctrMaTTPhiVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTPhiVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTPhiVC.IsValidate = false;
            this.ctrMaTTPhiVC.Location = new System.Drawing.Point(605, 111);
            this.ctrMaTTPhiVC.Name = "ctrMaTTPhiVC";
            this.ctrMaTTPhiVC.Name_VN = "";
            this.ctrMaTTPhiVC.ShowColumnCode = true;
            this.ctrMaTTPhiVC.ShowColumnName = false;
            this.ctrMaTTPhiVC.Size = new System.Drawing.Size(57, 26);
            this.ctrMaTTPhiVC.TabIndex = 5;
            this.ctrMaTTPhiVC.TagName = "";
            this.ctrMaTTPhiVC.WhereCondition = "";
            // 
            // ctrMaTTPhiBH
            // 
            this.ctrMaTTPhiBH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTPhiBH.Code = "";
            this.ctrMaTTPhiBH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTPhiBH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTPhiBH.IsValidate = false;
            this.ctrMaTTPhiBH.Location = new System.Drawing.Point(605, 135);
            this.ctrMaTTPhiBH.Name = "ctrMaTTPhiBH";
            this.ctrMaTTPhiBH.Name_VN = "";
            this.ctrMaTTPhiBH.ShowColumnCode = true;
            this.ctrMaTTPhiBH.ShowColumnName = false;
            this.ctrMaTTPhiBH.Size = new System.Drawing.Size(57, 26);
            this.ctrMaTTPhiBH.TabIndex = 8;
            this.ctrMaTTPhiBH.TagName = "";
            this.ctrMaTTPhiBH.WhereCondition = "";
            // 
            // ctrMaPhanLoaiPhiBH
            // 
            this.ctrMaPhanLoaiPhiBH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A557;
            this.ctrMaPhanLoaiPhiBH.Code = "";
            this.ctrMaPhanLoaiPhiBH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiPhiBH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiPhiBH.IsValidate = false;
            this.ctrMaPhanLoaiPhiBH.Location = new System.Drawing.Point(125, 135);
            this.ctrMaPhanLoaiPhiBH.Name = "ctrMaPhanLoaiPhiBH";
            this.ctrMaPhanLoaiPhiBH.Name_VN = "";
            this.ctrMaPhanLoaiPhiBH.ShowColumnCode = true;
            this.ctrMaPhanLoaiPhiBH.ShowColumnName = true;
            this.ctrMaPhanLoaiPhiBH.Size = new System.Drawing.Size(244, 26);
            this.ctrMaPhanLoaiPhiBH.TabIndex = 6;
            this.ctrMaPhanLoaiPhiBH.TagName = "";
            this.ctrMaPhanLoaiPhiBH.WhereCondition = "";
            // 
            // ctrMaPhanLoaiPhiVC
            // 
            this.ctrMaPhanLoaiPhiVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A557;
            this.ctrMaPhanLoaiPhiVC.Code = "";
            this.ctrMaPhanLoaiPhiVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiPhiVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiPhiVC.IsValidate = false;
            this.ctrMaPhanLoaiPhiVC.Location = new System.Drawing.Point(125, 111);
            this.ctrMaPhanLoaiPhiVC.Name = "ctrMaPhanLoaiPhiVC";
            this.ctrMaPhanLoaiPhiVC.Name_VN = "";
            this.ctrMaPhanLoaiPhiVC.ShowColumnCode = true;
            this.ctrMaPhanLoaiPhiVC.ShowColumnName = true;
            this.ctrMaPhanLoaiPhiVC.Size = new System.Drawing.Size(244, 26);
            this.ctrMaPhanLoaiPhiVC.TabIndex = 3;
            this.ctrMaPhanLoaiPhiVC.TagName = "";
            this.ctrMaPhanLoaiPhiVC.WhereCondition = "";
            // 
            // ctrMaTTHieuChinhTriGia
            // 
            this.ctrMaTTHieuChinhTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTHieuChinhTriGia.Code = "";
            this.ctrMaTTHieuChinhTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTHieuChinhTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTHieuChinhTriGia.IsValidate = false;
            this.ctrMaTTHieuChinhTriGia.Location = new System.Drawing.Point(312, 62);
            this.ctrMaTTHieuChinhTriGia.Name = "ctrMaTTHieuChinhTriGia";
            this.ctrMaTTHieuChinhTriGia.Name_VN = "";
            this.ctrMaTTHieuChinhTriGia.ShowColumnCode = true;
            this.ctrMaTTHieuChinhTriGia.ShowColumnName = false;
            this.ctrMaTTHieuChinhTriGia.Size = new System.Drawing.Size(57, 26);
            this.ctrMaTTHieuChinhTriGia.TabIndex = 73;
            this.ctrMaTTHieuChinhTriGia.TagName = "";
            this.ctrMaTTHieuChinhTriGia.WhereCondition = "";
            // 
            // ctrMaPhanLoaiTriGia
            // 
            this.ctrMaPhanLoaiTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E012;
            this.ctrMaPhanLoaiTriGia.Code = "";
            this.ctrMaPhanLoaiTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiTriGia.IsValidate = false;
            this.ctrMaPhanLoaiTriGia.Location = new System.Drawing.Point(125, 7);
            this.ctrMaPhanLoaiTriGia.Name = "ctrMaPhanLoaiTriGia";
            this.ctrMaPhanLoaiTriGia.Name_VN = "";
            this.ctrMaPhanLoaiTriGia.ShowColumnCode = true;
            this.ctrMaPhanLoaiTriGia.ShowColumnName = true;
            this.ctrMaPhanLoaiTriGia.Size = new System.Drawing.Size(178, 26);
            this.ctrMaPhanLoaiTriGia.TabIndex = 0;
            this.ctrMaPhanLoaiTriGia.TagName = "";
            this.ctrMaPhanLoaiTriGia.WhereCondition = "";
            // 
            // grList
            // 
            this.grList.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(3, 244);
            this.grList.Name = "grList";
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(661, 156);
            this.grList.TabIndex = 12;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Chi tiết khai trị giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(373, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Mã phân loại điều chỉnh trị giá";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Phí bảo hiểm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Giá cơ sở hiệu chỉnh TG";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(376, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Số tiền phí BH";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(376, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Số tiền phí VC";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(373, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Phân loại công thức ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Phí vận chuyển";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(317, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Tổng hệ số phân bổ trị giá";
            // 
            // txtChiTietKhaiTriGia
            // 
            this.txtChiTietKhaiTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtChiTietKhaiTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtChiTietKhaiTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChiTietKhaiTriGia.Location = new System.Drawing.Point(125, 193);
            this.txtChiTietKhaiTriGia.MaxLength = 12;
            this.txtChiTietKhaiTriGia.Multiline = true;
            this.txtChiTietKhaiTriGia.Name = "txtChiTietKhaiTriGia";
            this.txtChiTietKhaiTriGia.Size = new System.Drawing.Size(537, 43);
            this.txtChiTietKhaiTriGia.TabIndex = 11;
            this.txtChiTietKhaiTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChiTietKhaiTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhuongPhapDieuChinhTriGia
            // 
            this.txtPhuongPhapDieuChinhTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhuongPhapDieuChinhTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhuongPhapDieuChinhTriGia.Enabled = false;
            this.txtPhuongPhapDieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuongPhapDieuChinhTriGia.Location = new System.Drawing.Point(169, 89);
            this.txtPhuongPhapDieuChinhTriGia.MaxLength = 12;
            this.txtPhuongPhapDieuChinhTriGia.Name = "txtPhuongPhapDieuChinhTriGia";
            this.txtPhuongPhapDieuChinhTriGia.ReadOnly = true;
            this.txtPhuongPhapDieuChinhTriGia.Size = new System.Drawing.Size(435, 21);
            this.txtPhuongPhapDieuChinhTriGia.TabIndex = 9;
            this.txtPhuongPhapDieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuongPhapDieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaPhanLoaiDieuChinhTriGia
            // 
            this.txtMaPhanLoaiDieuChinhTriGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPhanLoaiDieuChinhTriGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPhanLoaiDieuChinhTriGia.Enabled = false;
            this.txtMaPhanLoaiDieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhanLoaiDieuChinhTriGia.Location = new System.Drawing.Point(528, 61);
            this.txtMaPhanLoaiDieuChinhTriGia.MaxLength = 12;
            this.txtMaPhanLoaiDieuChinhTriGia.Name = "txtMaPhanLoaiDieuChinhTriGia";
            this.txtMaPhanLoaiDieuChinhTriGia.ReadOnly = true;
            this.txtMaPhanLoaiDieuChinhTriGia.Size = new System.Drawing.Size(76, 21);
            this.txtMaPhanLoaiDieuChinhTriGia.TabIndex = 9;
            this.txtMaPhanLoaiDieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPhanLoaiDieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhanLoaiCongThucChuan
            // 
            this.txtPhanLoaiCongThucChuan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhanLoaiCongThucChuan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhanLoaiCongThucChuan.Enabled = false;
            this.txtPhanLoaiCongThucChuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoaiCongThucChuan.Location = new System.Drawing.Point(528, 34);
            this.txtPhanLoaiCongThucChuan.MaxLength = 12;
            this.txtPhanLoaiCongThucChuan.Name = "txtPhanLoaiCongThucChuan";
            this.txtPhanLoaiCongThucChuan.ReadOnly = true;
            this.txtPhanLoaiCongThucChuan.Size = new System.Drawing.Size(76, 21);
            this.txtPhanLoaiCongThucChuan.TabIndex = 9;
            this.txtPhanLoaiCongThucChuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoaiCongThucChuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Phương pháp điều chỉnh trị giá";
            // 
            // txtSoDangKyBH
            // 
            this.txtSoDangKyBH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDangKyBH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDangKyBH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDangKyBH.Location = new System.Drawing.Point(158, 166);
            this.txtSoDangKyBH.MaxLength = 12;
            this.txtSoDangKyBH.Name = "txtSoDangKyBH";
            this.txtSoDangKyBH.Size = new System.Drawing.Size(106, 21);
            this.txtSoDangKyBH.TabIndex = 9;
            this.txtSoDangKyBH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDangKyBH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Số đăng ký bảo hiểm tổng hợp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Số tiếp nhận tờ khai trị giá tổng hợp";
            // 
            // txtTongHeSoPhanBoTG
            // 
            this.txtTongHeSoPhanBoTG.DecimalDigits = 20;
            this.txtTongHeSoPhanBoTG.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongHeSoPhanBoTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongHeSoPhanBoTG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongHeSoPhanBoTG.Location = new System.Drawing.Point(459, 165);
            this.txtTongHeSoPhanBoTG.MaxLength = 15;
            this.txtTongHeSoPhanBoTG.Name = "txtTongHeSoPhanBoTG";
            this.txtTongHeSoPhanBoTG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongHeSoPhanBoTG.Size = new System.Drawing.Size(145, 21);
            this.txtTongHeSoPhanBoTG.TabIndex = 7;
            this.txtTongHeSoPhanBoTG.Text = "0";
            this.txtTongHeSoPhanBoTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongHeSoPhanBoTG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongHeSoPhanBoTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 20;
            this.txtPhiBaoHiem.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiBaoHiem.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(459, 138);
            this.txtPhiBaoHiem.MaxLength = 15;
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(145, 21);
            this.txtPhiBaoHiem.TabIndex = 7;
            this.txtPhiBaoHiem.Text = "0";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã phân loại khai trị giá";
            // 
            // txtGiaHieuChinhTriGia
            // 
            this.txtGiaHieuChinhTriGia.DecimalDigits = 20;
            this.txtGiaHieuChinhTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaHieuChinhTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaHieuChinhTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtGiaHieuChinhTriGia.Location = new System.Drawing.Point(125, 65);
            this.txtGiaHieuChinhTriGia.MaxLength = 15;
            this.txtGiaHieuChinhTriGia.Name = "txtGiaHieuChinhTriGia";
            this.txtGiaHieuChinhTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaHieuChinhTriGia.Size = new System.Drawing.Size(178, 21);
            this.txtGiaHieuChinhTriGia.TabIndex = 2;
            this.txtGiaHieuChinhTriGia.Text = "0";
            this.txtGiaHieuChinhTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaHieuChinhTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtGiaHieuChinhTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTiepNhanTKTriGia
            // 
            this.txtSoTiepNhanTKTriGia.DecimalDigits = 20;
            this.txtSoTiepNhanTKTriGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTiepNhanTKTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhanTKTriGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTiepNhanTKTriGia.Location = new System.Drawing.Point(186, 37);
            this.txtSoTiepNhanTKTriGia.MaxLength = 15;
            this.txtSoTiepNhanTKTriGia.Name = "txtSoTiepNhanTKTriGia";
            this.txtSoTiepNhanTKTriGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTiepNhanTKTriGia.Size = new System.Drawing.Size(117, 21);
            this.txtSoTiepNhanTKTriGia.TabIndex = 1;
            this.txtSoTiepNhanTKTriGia.Text = "0";
            this.txtSoTiepNhanTKTriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanTKTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTiepNhanTKTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 20;
            this.txtPhiVanChuyen.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVanChuyen.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(459, 114);
            this.txtPhiVanChuyen.MaxLength = 15;
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(145, 21);
            this.txtPhiVanChuyen.TabIndex = 4;
            this.txtPhiVanChuyen.Text = "0";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(506, 6);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 8;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // VNACC_TK_TKTriGiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 446);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TK_TKTriGiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin tờ khai trị giá";
            this.Load += new System.EventHandler(this.VNACC_TK_TKTriGiaForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox)).EndInit();
            this.uiGroupBox.ResumeLayout(false);
            this.uiGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.GridEX.GridEX grList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiVanChuyen;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtPhiBaoHiem;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDangKyBH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtChiTietKhaiTriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiTriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHieuChinhTriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiBH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiPhiBH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiPhiVC;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaHieuChinhTriGia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhanTKTriGia;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongPhapDieuChinhTriGia;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoaiDieuChinhTriGia;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiCongThucChuan;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoaiTongGiaCoBan;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHeSoPhanBoTG;
        private Janus.Windows.EditControls.UIButton btnXoa;
    }
}