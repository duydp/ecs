﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
  public  class ContractReference:IssueBase
    {
      /// <summary>
        /// Trị giá theo hợp đồng
      /// </summary>
      [XmlElement("customsValue")]
      public string CustomsValue { get; set; }
      /// <summary>
      /// 
      /// </summary>
      [XmlElement("Commodity")]
      public Commodity Commodity { get; set; }

      [XmlElement("StatisticalValue")]
      public string StatisticalValue { get; set; }
      /// <summary>
      /// Chứng từ thanh toán
      /// </summary>
      [XmlElement("PaymentDocument")]
      public PaymentDocument PaymentDocument { get; set; }
    }
}
