﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class SXXK_DinhMuc : ICloneable
	{
		#region Properties.
		
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaSanPham { set; get; }
        public string TenSanPham { set; get; }
		public string MaNguyenPhuLieu { set; get; }
        public string TenNPL { set; get; }
		public decimal DinhMucSuDung { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public decimal DinhMucChung { set; get; }
		public string GhiChu { set; get; }
		public bool IsFromVietNam { set; get; }
		public string MaDinhDanh { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long LenhSanXuat_ID { set; get; }
		public string GuidString { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<SXXK_DinhMuc> ConvertToCollection(IDataReader reader)
		{
			List<SXXK_DinhMuc> collection = new List<SXXK_DinhMuc>();
			while (reader.Read())
			{
				SXXK_DinhMuc entity = new SXXK_DinhMuc();
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsFromVietNam"))) entity.IsFromVietNam = reader.GetBoolean(reader.GetOrdinal("IsFromVietNam"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanh"))) entity.MaDinhDanh = reader.GetString(reader.GetOrdinal("MaDinhDanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidString"))) entity.GuidString = reader.GetString(reader.GetOrdinal("GuidString"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_SXXK_DinhMuc VALUES(@MaHaiQuan, @MaDoanhNghiep, @MaSanPham, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @DinhMucChung, @GhiChu, @IsFromVietNam, @MaDinhDanh, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @LenhSanXuat_ID, @GuidString)";
            string update = "UPDATE t_SXXK_DinhMuc SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, DinhMucChung = @DinhMucChung, GhiChu = @GhiChu, IsFromVietNam = @IsFromVietNam, MaDinhDanh = @MaDinhDanh, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, LenhSanXuat_ID = @LenhSanXuat_ID, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucChung", SqlDbType.Decimal, "DinhMucChung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanh", SqlDbType.NVarChar, "MaDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucChung", SqlDbType.Decimal, "DinhMucChung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanh", SqlDbType.NVarChar, "MaDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_SXXK_DinhMuc VALUES(@MaHaiQuan, @MaDoanhNghiep, @MaSanPham, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @DinhMucChung, @GhiChu, @IsFromVietNam, @MaDinhDanh, @SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @LenhSanXuat_ID, @GuidString)";
            string update = "UPDATE t_SXXK_DinhMuc SET MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaSanPham = @MaSanPham, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, DinhMucChung = @DinhMucChung, GhiChu = @GhiChu, IsFromVietNam = @IsFromVietNam, MaDinhDanh = @MaDinhDanh, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, LenhSanXuat_ID = @LenhSanXuat_ID, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM t_SXXK_DinhMuc WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DinhMucChung", SqlDbType.Decimal, "DinhMucChung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanh", SqlDbType.NVarChar, "MaDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DinhMucChung", SqlDbType.Decimal, "DinhMucChung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.VarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsFromVietNam", SqlDbType.Bit, "IsFromVietNam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanh", SqlDbType.NVarChar, "MaDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.NVarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaSanPham", SqlDbType.NVarChar, "MaSanPham", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, "LenhSanXuat_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static SXXK_DinhMuc Load(string maHaiQuan, string maDoanhNghiep, string maSanPham, string maNguyenPhuLieu, long lenhSanXuat_ID)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, maSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, maNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, lenhSanXuat_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<SXXK_DinhMuc> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<SXXK_DinhMuc> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<SXXK_DinhMuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<SXXK_DinhMuc> SelectCollectionBy_MaSanPham_MaDoanhNghiep_MaHaiQuan(string maSanPham, string maDoanhNghiep, string maHaiQuan)
		{
            IDataReader reader = SelectReaderBy_MaSanPham_MaDoanhNghiep_MaHaiQuan(maSanPham, maDoanhNghiep, maHaiQuan);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_MaSanPham_MaDoanhNghiep_MaHaiQuan(string maSanPham, string maDoanhNghiep, string maHaiQuan)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_MaDoanhNghiep_MaHaiQuan]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, maSanPham);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_DinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_SXXK_DinhMuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_MaSanPham_MaDoanhNghiep_MaHaiQuan(string maSanPham, string maDoanhNghiep, string maHaiQuan)
		{
			const string spName = "p_SXXK_DinhMuc_SelectBy_MaSanPham_MaDoanhNghiep_MaHaiQuan";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, maSanPham);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertSXXK_DinhMuc(string maHaiQuan, string maDoanhNghiep, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal dinhMucChung, string ghiChu, bool isFromVietNam, string maDinhDanh, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, long lenhSanXuat_ID, string guidString)
		{
			SXXK_DinhMuc entity = new SXXK_DinhMuc();	
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMucChung = dinhMucChung;
			entity.GhiChu = ghiChu;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanh = maDinhDanh;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GuidString = guidString;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_SXXK_DinhMuc_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanh", SqlDbType.NVarChar, MaDinhDanh);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<SXXK_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMuc item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateSXXK_DinhMuc(string maHaiQuan, string maDoanhNghiep, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal dinhMucChung, string ghiChu, bool isFromVietNam, string maDinhDanh, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, long lenhSanXuat_ID, string guidString)
		{
			SXXK_DinhMuc entity = new SXXK_DinhMuc();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMucChung = dinhMucChung;
			entity.GhiChu = ghiChu;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanh = maDinhDanh;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GuidString = guidString;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_SXXK_DinhMuc_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanh", SqlDbType.NVarChar, MaDinhDanh);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<SXXK_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMuc item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateSXXK_DinhMuc(string maHaiQuan, string maDoanhNghiep, string maSanPham, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal dinhMucChung, string ghiChu, bool isFromVietNam, string maDinhDanh, decimal soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, long lenhSanXuat_ID, string guidString)
		{
			SXXK_DinhMuc entity = new SXXK_DinhMuc();			
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.DinhMucChung = dinhMucChung;
			entity.GhiChu = ghiChu;
			entity.IsFromVietNam = isFromVietNam;
			entity.MaDinhDanh = maDinhDanh;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			entity.GuidString = guidString;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsFromVietNam", SqlDbType.Bit, IsFromVietNam);
			db.AddInParameter(dbCommand, "@MaDinhDanh", SqlDbType.NVarChar, MaDinhDanh);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.NVarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<SXXK_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMuc item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteSXXK_DinhMuc(string maHaiQuan, string maDoanhNghiep, string maSanPham, string maNguyenPhuLieu, long lenhSanXuat_ID)
		{
			SXXK_DinhMuc entity = new SXXK_DinhMuc();
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.LenhSanXuat_ID = lenhSanXuat_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.NVarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, LenhSanXuat_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_MaSanPham_MaDoanhNghiep_MaHaiQuan(string maSanPham, string maDoanhNghiep, string maHaiQuan)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_MaDoanhNghiep_MaHaiQuan]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.NVarChar, maSanPham);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_SXXK_DinhMuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<SXXK_DinhMuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (SXXK_DinhMuc item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}