-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@TaxCode,
	@TaxName,
	@IndicationOfCommonTax,
	@IndicationOfPreferantialTax,
	@IndicationOfMFNTax,
	@IndicationOfFTATax,
	@IndicationOfOutOfQuota,
	@IndicationOfSpecificDuty,
	@IndicationOfSpecificDutyAndAdValoremDuty,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_TaxClassificationCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[TaxName] = @TaxName,
	[IndicationOfCommonTax] = @IndicationOfCommonTax,
	[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
	[IndicationOfMFNTax] = @IndicationOfMFNTax,
	[IndicationOfFTATax] = @IndicationOfFTATax,
	[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
	[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
	[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[TaxCode] = @TaxCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [TaxCode] FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE [TaxCode] = @TaxCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_TaxClassificationCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[TaxName] = @TaxName,
			[IndicationOfCommonTax] = @IndicationOfCommonTax,
			[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
			[IndicationOfMFNTax] = @IndicationOfMFNTax,
			[IndicationOfFTATax] = @IndicationOfFTATax,
			[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
			[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
			[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[TaxCode] = @TaxCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[TaxCode],
			[TaxName],
			[IndicationOfCommonTax],
			[IndicationOfPreferantialTax],
			[IndicationOfMFNTax],
			[IndicationOfFTATax],
			[IndicationOfOutOfQuota],
			[IndicationOfSpecificDuty],
			[IndicationOfSpecificDutyAndAdValoremDuty],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@TaxCode,
			@TaxName,
			@IndicationOfCommonTax,
			@IndicationOfPreferantialTax,
			@IndicationOfMFNTax,
			@IndicationOfFTATax,
			@IndicationOfOutOfQuota,
			@IndicationOfSpecificDuty,
			@IndicationOfSpecificDutyAndAdValoremDuty,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
	@TaxCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
	@TaxCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_TaxClassificationCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]	

GO

