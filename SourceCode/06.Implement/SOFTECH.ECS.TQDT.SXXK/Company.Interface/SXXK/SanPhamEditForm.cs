﻿using System;
using Company.BLL;
using Company.BLL.SXXK;
using Company.BLL.Utils;
using Company.BLL.DuLieuChuan;
using System.Data;

namespace Company.Interface.SXXK
{
    public partial class SanPhamEditForm : BaseForm
    {             
        public SanPhamEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
                //error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }

            if (!cvError.IsValid) return;

            BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
            spSXXK.Ma = txtMa.Text.Trim();
            if (spSXXK.Load())
            {
               // ShowMessage("Sản phẩm này đã được đăng ký.", false);
                MLMessages("Sản phẩm này đã được đăng ký.", "MSG_PUB06", "", false);
                return;
            }

            spSXXK.Ten = txtTen.Text.Trim(); ;
            spSXXK.MaHS = txtMaHS.Text.Trim(); ;
            spSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            spSXXK.Insert();
            this.Close();
        }

        private void SanPhamEditForm_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                       
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
                //error.SetError(txtMaHS, "Mã HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
            }
            else
            {
                error.SetError(txtMaHS, string.Empty);
            }
          
        }

    }
}