﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC03TT74Form : BaseForm
    {
        ToKhaiMauDich TKMD;
        ToKhaiChuyenTiep TKCT;
        DataSet dsNPL;
        DataSet dsNPLTemp;
        public XRLabel label;
        public DataSet dsNguyenPhuLieu;
        public DataTable dtBangNguyenPhuLieu;
        private BangKe03NewTT74 BC03;
        public HopDong HD = new HopDong();
        Company.GC.BLL.GC.NguyenPhuLieu NPL;

        int sotable;
        int soLuongNPL;
        int soLuongTK;
        DataSet dsToKhaiXuat;
        ToKhaiMauDichCollection TKMDCol;
        ToKhaiChuyenTiepCollection TKCTCol;

        public ReportViewBC03TT74Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            //TKCT = new ToKhaiChuyenTiep();
            //TKMD = new ToKhaiMauDich();
            //dtBangNguyenPhuLieu = new DataTable();
            //BC03 = new BangKe03NewTT74();
            //label = new XRLabel();
            //dsNguyenPhuLieu = new DataSet();
            //NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            //dsNPL = this.HD.GetNPLMauDichVaChuyenTiepHopDong();
            //soLuongNPL = dsNPL.Tables[0].Rows.Count;
            ////sotable = (soLuongNPL - 1) / 6 + 1;
            //sotable = (soLuongTK - 1) / 5 + 1;
            //BC03.report = this;
            //for (int i = 0; i < this.sotable; i++)
            //{
            //    cbPage.Items.Add("Trang " + (i + 1), i);
            //}
            //if(cbPage.Items.Count > 0)
            //    cbPage.SelectedIndex = 0;

            //btnNext.Enabled = !this.BC03.Last;
            //btnPrivious.Enabled = !this.BC03.First;
            loadReport();
        }
        private void loadReport()
        {
            TKCT = new ToKhaiChuyenTiep();
            TKMD = new ToKhaiMauDich();
            dtBangNguyenPhuLieu = new DataTable();
            BC03 = new BangKe03NewTT74();
            label = new XRLabel();

            dsNguyenPhuLieu = new DataSet();
            NPL = new Company.GC.BLL.GC.NguyenPhuLieu();

            dsNPL = this.HD.GetNPLMauDichVaChuyenTiepHopDong();
            dsToKhaiXuat = TKMD.GetToKhaiForMau03TT74(this.HD.ID);
            TKMDCol = this.HD.GetTKNKBC03New();
            TKCTCol = this.HD.GetTKCTXuatNPL();

            soLuongNPL = dsNPL.Tables[0].Rows.Count;
            soLuongTK = dsToKhaiXuat.Tables[0].Rows.Count;
            sotable = (soLuongTK - 1) / 5 + 1;
            BC03.report = this;

            for (int i = 0; i < this.sotable; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if (cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;

            btnNext.Enabled = !this.BC03.Last;
            btnPrivious.Enabled = !this.BC03.First;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC03.First = (cbPage.SelectedIndex == 0);
            this.BC03.Last = (cbPage.SelectedIndex == (sotable - 1));
            this.BC03.HD = this.HD;

            btnNext.Enabled = !this.BC03.Last;
            btnPrivious.Enabled = !this.BC03.First;

            if (dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + cbPage.SelectedIndex] != null)
                this.BC03.BindReport(this.dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + cbPage.SelectedIndex]);
            else
            {
                dtBangNguyenPhuLieu = new DataTable("dtBangNguyenPhuLieu" + cbPage.SelectedIndex);
                dtBangNguyenPhuLieu = CreatSchemaDataSetNewRP03TT74(cbPage.SelectedIndex);
                this.BC03.BindReport(this.dtBangNguyenPhuLieu);
                dsNguyenPhuLieu.Tables.Add(dtBangNguyenPhuLieu);
                
            }

            printControl1.PrintingSystem = this.BC03.PrintingSystem;
            this.BC03.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }


        public DataTable CreatSchemaDataSetNewRP03(int BangSo)
        {
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[8];

            dcCol[0] = new DataColumn("TKXK", typeof(string));
            dcCol[0].Caption = "TKXK";

            int t = 1;
            for (int k = BangSo * 6; k < (BangSo + 1) * 6; k++)
            {

                if (k < soLuongNPL)
                {
                    dcCol[t] = new DataColumn();
                    string Ma = dsNPL.Tables[0].Rows[k].ItemArray[1].ToString();
                    dcCol[t].ColumnName = "Mã " + Ma;
                    dcCol[t].DataType = typeof(decimal);

                    /* DataSet tạm để lấy tên và đơn vị tính */
                    dsNPLTemp = NPL.GetNPL(Ma, this.HD.ID);

                    string Ten = dsNPLTemp.Tables[0].Rows[0].ItemArray[0].ToString();
                    string DVT_ID = dsNPLTemp.Tables[0].Rows[0].ItemArray[1].ToString();
                    dcCol[t].Caption = " Tên : " + Ten + "\r\n Mã : " + Ma + "\r\n ĐV tính : " + DonViTinh_GetName(DVT_ID);
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "GhiChu";
            dcCol[t].DataType = typeof(string);
            dcCol[t].Caption = "GhiChu";
            dttemp.Columns.AddRange(dcCol);


            ToKhaiMauDichCollection TKMDCol = this.HD.GetTKNKBC03New();

            foreach (ToKhaiMauDich TKMD in TKMDCol)
            {
                TKMD.LoadHMDCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    drData[0] = TKMD.SoToKhai + "/" + LoaiHinhMauDich_GetTenVT(TKMD.MaLoaiHinh) + "/" + TKMD.MaHaiQuan + "\r\n" + TKMD.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HMD.MaPhu;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HMD.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }

            ToKhaiChuyenTiepCollection TKCTCol = this.HD.GetTKCTXuatNPL();

            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
            {
                TKCT.LoadHCTCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    drData[0] = TKCT.SoToKhai + "/" + LoaiHinhMauDich_GetTenVT(TKCT.MaLoaiHinh) + "/" + TKCT.MaHaiQuanTiepNhan + "\r\n" + TKCT.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HCT.MaHang;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HCT.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }
            return dttemp;
        }
        public DataTable CreatSchemaDataSetNewRP03TT74(int BangSo)
        {
            #region TaoTable
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[10];

            dcCol[0] = new DataColumn("NPL", typeof(string));
            dcCol[0].Caption = "NPL";

            dcCol[1] = new DataColumn();
            dcCol[1].ColumnName = "DVTinh";
            dcCol[1].DataType = typeof(string);
            dcCol[1].Caption = "Đv tính";

            int t = 2;
            for (int k = BangSo * 5; k < (BangSo + 1) * 5; k++)
            {

                if (k < soLuongTK)
                {
                    DataRow dr = dsToKhaiXuat.Tables[0].Rows[k];
                    string soTK = dr.ItemArray[0].ToString();
                    string ngayTK = Convert.ToDateTime(dr.ItemArray[2].ToString()).ToShortDateString();
                    string loaiHinh = dr.ItemArray[1].ToString();
                    string namDK = Convert.ToDateTime(ngayTK).Year.ToString();
                    string maHQ = dr.ItemArray[4].ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Số TK " + soTK + loaiHinh.Trim() + namDK;
                    dcCol[t].DataType = typeof(string);
                    if (loaiHinh.StartsWith("XGC"))
                        dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "/" + LoaiHinhMauDich_GetTenVT(loaiHinh).Trim() + "/" + maHQ + "  \r\n Ngày: " + ngayTK + "\r\n ";
                    else
                        dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "-" + loaiHinh.Trim() + "-" + namDK + "  \r\n Ngày: " + ngayTK + "\r\n ";
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "TongCong";
            dcCol[t].DataType = typeof(decimal);
            dcCol[t].Caption = "Tổng cộng";


            dcCol[t + 1] = new DataColumn();
            dcCol[t + 1].ColumnName = "GhiChu";
            dcCol[t + 1].DataType = typeof(string);
            dcCol[t + 1].Caption = "GhiChu";

            dcCol[t + 2] = new DataColumn();
            dcCol[t + 2].ColumnName = "MaNPL";
            dcCol[t + 2].DataType = typeof(string);
            dcCol[t + 2].Caption = "MaNPL";

            dttemp.Columns.AddRange(dcCol);
            #endregion TaoTable
            //Company.GC.BLL.GC.NguyenPhuLieuCollection nplCol = this.HD.GetNPL();
            decimal Tong = 0;
            foreach(DataRow drNPL in dsNPL.Tables[0].Rows)
            //foreach (Company.GC.BLL.GC.NguyenPhuLieu npl in nplCol)
            {
                string maNPL = drNPL[1].ToString();
                dsNPLTemp = NPL.GetNPL(maNPL, this.HD.ID); 
                DataRow drData = dttemp.NewRow();
                drData[0] = dsNPLTemp.Tables[0].Rows[0].ItemArray[0].ToString() + " / " + maNPL;
                drData[1] = DonViTinh_GetName(dsNPLTemp.Tables[0].Rows[0].ItemArray[1].ToString());//DonViTinh_GetName(npl.DVT_ID);
                drData[9] = maNPL;
                for (int i = 2; i < 7; i++)
                {

                    foreach (ToKhaiMauDich TKMD in TKMDCol)
                    {
                        string ColName = "Số TK " + TKMD.SoToKhai + TKMD.MaLoaiHinh.Trim() + TKMD.NgayDangKy.Year;
                        if (dttemp.Columns[i].ColumnName == ColName)
                        {
                            TKMD.LoadHMDCollection();
                            foreach (HangMauDich HMD in TKMD.HMDCollection)
                            {
                                if (HMD.MaPhu == maNPL)//npl.Ma)
                                {
                                    drData[i] = HMD.SoLuong;
                                    Tong += Decimal.Parse(HMD.SoLuong.ToString());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
                    {
                        string ColName = "Số TK " + TKCT.SoToKhai + TKCT.MaLoaiHinh.Trim() + TKCT.NgayDangKy.Year;
                        if (dttemp.Columns[i].ColumnName == ColName)
                        {
                            TKCT.LoadHCTCollection();
                            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                            {
                                if (HCT.MaHang == maNPL)//npl.Ma)
                                {
                                    drData[i] = HCT.SoLuong;
                                    Tong += Decimal.Parse(HCT.SoLuong.ToString());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                dttemp.Rows.Add(drData);// addRow tất cả các NPL
            }
            #region TinhTong
            if (this.BC03.Last) {
                foreach (DataRow dr in dttemp.Rows)
                {
                    Decimal tongCong = 0;
                    foreach (ToKhaiMauDich TKMD in TKMDCol)
                    {
                        TKMD.LoadHMDCollection();
                        foreach (HangMauDich HMD in TKMD.HMDCollection)
                        {
                            if (HMD.MaPhu.Trim() == dr[9].ToString().Trim())
                            {
                                tongCong += Decimal.Parse(HMD.SoLuong.ToString());
                                dr[7] = tongCong;
                                break;
                            }
                        }
                    }
                    foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
                    {
                        TKCT.LoadHCTCollection();
                        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            if (HCT.MaHang.Trim() == dr[9].ToString().Trim())
                            {
                                tongCong += Decimal.Parse(HCT.SoLuong.ToString());
                                dr[7] = tongCong;
                                break;
                            }
                        }
                    }
                }
            }
            #endregion TinhTong

            return dttemp;
            
        }
        private void btnPrivious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC03.setText(this.label, txtName.Text);
            this.BC03.CreateDocument();
        }
    }
}