/*
Alter Column StationCode data Type varchar(5) -> varchar(6)

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] DROP CONSTRAINT [PK_t_KDT_VNACC_Stations]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] ALTER COLUMN [StationCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_VNACC_Stations] on [dbo].[t_VNACC_Category_Stations]'
GO
ALTER TABLE [dbo].[t_VNACC_Category_Stations] ADD CONSTRAINT [PK_t_KDT_VNACC_Stations] PRIMARY KEY CLUSTERED  ([StationCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

-- cập nhật Danh mục cửa khẩu đường sắt (A601)
Delete t_VNACC_Category_Stations where CountryCode='CN'
Update t_VNACC_Category_Stations set StationCode='VNDGLS', StationName='DONG DANG (LANG SON)' where StationCode='DG'
Update t_VNACC_Category_Stations set StationCode='VNLCLC', StationName='LAO CAI' where StationCode='LC'
Update t_VNACC_Category_Stations set StationCode='VNYVHN', StationName='YEN VIEN (HA NOI)' where StationCode='HN'
--- Cập nhật Danh mục mã xác định thời hạn nộp thuế (E019)
update t_VNACC_Category_Common  set Name_VN=N'Sử dụng bảo lãnh riêng' WHERE ReferenceDB='E019' and Code= 'A'
update t_VNACC_Category_Common  set Name_VN=N'Sử dụng bảo lãnh chung' WHERE ReferenceDB='E019' and Code= 'B'
update t_VNACC_Category_Common  set Name_VN=N'Không sử dụng bảo lãnh' WHERE ReferenceDB='E019' and Code= 'C'
update t_VNACC_Category_Common  set Name_VN=N'Nộp thuế ngay' WHERE ReferenceDB='E019' and Code= 'D'


GO


/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 01/08/2014 14:51:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_BC09HSTK_GC_TT117_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]
GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 01/08/2014 14:51:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Huynh Ngoc Khanh    
-- Create date:     18/04/2013
-- Description:     View BC 09/HSTK-TT117
-- =============================================    
CREATE PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;   
  
 --DECLARE
 --@IDHopDong BIGINT,    
 --@MaHaiQuan CHAR(6),    
 --@MaDoanhNghiep VARCHAR(14) 
 --SET @IDHopDong = 721
 --SET @MaHaiQuan = 'N60C'
 --SET @MaDoanhNghiep = '4000395355'
 SELECT  BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayDangKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, t_KDT_GC_HopDong.ID AS IDHopDong, t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep     FROM
 (
 (SELECT  CONVERT(NVARCHAR(20), TKMD.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKMD.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau,
								   CASE PTVT_ID 
								  WHEN '001' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  WHEN '005' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  ELSE CASE TKMD.SoVanDon 
								  WHEN '' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103)
								  ELSE
								   TKMD.SoVanDon + ', ' + CONVERT(NVARCHAR(20), TKMD.NgayVanDon, 103) 
								       END 
								   END AS SoNgayBL
			    FROM (SELECT ID,SoToKhai,NgayDangKy,MaLoaiHinh,CuaKhau_ID,SoVanDon,NgayVanDon,PTVT_ID
			            FROM t_KDT_ToKhaiMauDich  
				WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XGC%' 
				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AS TKMD
	   INNER JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKMD.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKMD.MaLoaiHinh
	   INNER JOIN t_KDT_HangMauDich ON TKMD.ID = t_KDT_HangMauDich.TKMD_ID
	   LEFT  JOIN ( select * from t_GC_BC08TT74_VanDonToKhaiXuat WHERE t_GC_BC08TT74_VanDonToKhaiXuat.IDHopDong = @IDHopDong) t_GC_BC08TT74_VanDonToKhaiXuat  ON t_GC_BC08TT74_VanDonToKhaiXuat.TKMD_ID = TKMD.ID)
 UNION
 (SELECT CONVERT(NVARCHAR(20), TKCT.SoToKhai) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKCT.NgayDangKy, 103) AS SoNgayToKhai,t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
								  t_KDT_GC_HangChuyenTiep.TriGia AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, '' AS SoNgayBL 
  FROM (SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep 
                WHERE  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'XGC%' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X')
						AND  t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep AND t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AS TKCT
	   INNER JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKCT.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKCT.MaLoaiHinh
	   INNER JOIN t_KDT_GC_HangChuyenTiep ON TKCT.ID = t_KDT_GC_HangChuyenTiep.Master_ID)
)	
AS BangKe    
INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = @IDHopDong
		

 
END

GO







           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.6', GETDATE(), N'Cap nhat Danh muc  (A601,E019)')
END	
