﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class MainForm : Form
    {
        private DanhMucNganhNgheFrm DanhMucNganhNghe;
        private CapGiayChungNhan GiayChungNhan;
        private QuyetDinhPhatFrm QuyetDinhPhat;
        private TraCuuTTDNFrm traCuu;
        public MainForm()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "DM_NganhNghe":
                    ShowDMNganhNghe();
                    break;
                case "Nhap_DoanhNghiep":
                    ShowGiayChungNhan();
                    break;
                case "Nhap_QuyetDinhPhat":
                    ShowQuyetDinh();
                    break;
                case "TraCuu":
                    ShowTraCuu();
                    break;
            }
        }
        private void ShowDMNganhNghe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DanhMucNganhNgheFrm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DanhMucNganhNghe = new DanhMucNganhNgheFrm();
            DanhMucNganhNghe.MdiParent = this;
            DanhMucNganhNghe.Show();
        }
        private void ShowGiayChungNhan()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("CapGiayChungNhan"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            MaDoanhNghiemFrm maDN = new MaDoanhNghiemFrm();
            maDN.ShowDialog(this);
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Text != null && forms[i].Text.ToString().Equals(maDN.MaDoanhNghiep))
                {
                    forms[i].Activate();
                    return;
                }
            }
            if (maDN.DialogResult == DialogResult.OK)
            {
                ThongTinDoanhNghiepfrm thongTinDoanhNghiep = new ThongTinDoanhNghiepfrm();
                thongTinDoanhNghiep.trangThai = ThongTinDoanhNghiepfrm.TrangThai.ThemGiayCN;
                thongTinDoanhNghiep.MaDoanhNghiep = maDN.MaDoanhNghiep;
                thongTinDoanhNghiep.Text = maDN.MaDoanhNghiep;
                thongTinDoanhNghiep.MdiParent = this;
                thongTinDoanhNghiep.Show();
            }
        }
        private void ShowQuyetDinh()
        {
            MaDoanhNghiemFrm maDN = new MaDoanhNghiemFrm();
            maDN.ShowDialog(this);
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Text != null && forms[i].Text.ToString().Equals(maDN.MaDoanhNghiep))
                {
                    forms[i].Activate();
                    return;
                }
            }
            //QuyetDinhPhat = new QuyetDinhPhatFrm();
            //QuyetDinhPhat.MdiParent = this;
            //QuyetDinhPhat.Show();
            if (maDN.DialogResult == DialogResult.OK)
            {
                ThongTinDoanhNghiepfrm thongTinDoanhNghiep = new ThongTinDoanhNghiepfrm();
                thongTinDoanhNghiep.trangThai = ThongTinDoanhNghiepfrm.TrangThai.ThemQDPhat;
                thongTinDoanhNghiep.MaDoanhNghiep = maDN.MaDoanhNghiep;
                thongTinDoanhNghiep.Text = maDN.MaDoanhNghiep;
                thongTinDoanhNghiep.MdiParent = this;
                thongTinDoanhNghiep.Show();
            }
        }
        private void ShowTraCuu()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TraCuuTTDNFrm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            traCuu = new TraCuuTTDNFrm();
            traCuu.MdiParent = this;
            traCuu.Show();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdCauHinh":
                    showCauHinh();
                    break;
            }
        }
        private void showCauHinh()
        {
            CauHinhFrm frm = new CauHinhFrm();
            frm.ShowDialog();
        }
        //private void MainForm_MdiChildActivate(object sender, EventArgs e)
        //{
        //    if (this.ActiveMdiChild == null)
        //        TabForm.Visible = false;
        //    // If no any child form, hide tabControl 
        //    else
        //    {
        //        this.ActiveMdiChild.WindowState =
        //        FormWindowState.Maximized;
        //        // Child form always maximized 

        //        // If child form is new and no has tabPage, 
        //        // create new tabPage 
        //        if (this.ActiveMdiChild.Tag == null)
        //        {
        //            // Add a tabPage to tabControl with child 
        //            // form caption 

        //             Janus.Windows.UI.Tab.UITabPage tp = new  Janus.Windows.UI.Tab.UITabPage(this.ActiveMdiChild
        //                                     .Text);
        //            tp.Tag = this.ActiveMdiChild;
        //            tp.Parent = TabForm;
        //            TabForm.SelectedTab = tp;

        //            this.ActiveMdiChild.Tag = tp;
        //            //this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);
        //        }

        //        if (!TabForm.Visible) TabForm.Visible = true;

        //    }

        //}










    }
}
